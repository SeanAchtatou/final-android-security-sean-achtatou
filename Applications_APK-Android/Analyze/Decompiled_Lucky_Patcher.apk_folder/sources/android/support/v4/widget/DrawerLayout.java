package android.support.v4.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.content.C0101;
import android.support.v4.widget.C0188;
import android.support.v4.ʼ.ʻ.C0288;
import android.support.v4.ˉ.C0355;
import android.support.v4.ˉ.C0386;
import android.support.v4.ˉ.C0396;
import android.support.v4.ˉ.C0414;
import android.support.v4.ˉ.C0426;
import android.support.v4.ˉ.ʻ.C0360;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowInsets;
import android.view.accessibility.AccessibilityEvent;
import java.util.ArrayList;
import java.util.List;

public class DrawerLayout extends ViewGroup {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final int[] f482 = {16842931};

    /* renamed from: ʼ  reason: contains not printable characters */
    static final boolean f483 = (Build.VERSION.SDK_INT >= 19);

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final int[] f484 = {16843828};

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final boolean f485;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private Drawable f486;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private CharSequence f487;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private Drawable f488;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private Object f489;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final C0144 f490;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private CharSequence f491;

    /* renamed from: ˆ  reason: contains not printable characters */
    private float f492;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private Drawable f493;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f494;

    /* renamed from: ˈˈ  reason: contains not printable characters */
    private Drawable f495;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f496;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private Drawable f497;

    /* renamed from: ˊ  reason: contains not printable characters */
    private float f498;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private final ArrayList<View> f499;

    /* renamed from: ˋ  reason: contains not printable characters */
    private Paint f500;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private Drawable f501;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final C0188 f502;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final C0188 f503;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final C0148 f504;

    /* renamed from: י  reason: contains not printable characters */
    private final C0148 f505;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f506;

    /* renamed from: ــ  reason: contains not printable characters */
    private boolean f507;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f508;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f509;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private float f510;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private int f511;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private Drawable f512;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private int f513;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private int f514;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private int f515;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private boolean f516;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private boolean f517;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private C0145 f518;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private List<C0145> f519;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private float f520;

    /* renamed from: android.support.v4.widget.DrawerLayout$ʽ  reason: contains not printable characters */
    public interface C0145 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m901(int i);

        /* renamed from: ʻ  reason: contains not printable characters */
        void m902(View view);

        /* renamed from: ʻ  reason: contains not printable characters */
        void m903(View view, float f);

        /* renamed from: ʼ  reason: contains not printable characters */
        void m904(View view);
    }

    static {
        boolean z = true;
        if (Build.VERSION.SDK_INT < 21) {
            z = false;
        }
        f485 = z;
    }

    public DrawerLayout(Context context) {
        this(context, null);
    }

    public DrawerLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ˉ.ᐧ.ʻ(android.view.View, int):void
     arg types: [android.support.v4.widget.DrawerLayout, int]
     candidates:
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.support.v4.ˉ.ﾞ):android.support.v4.ˉ.ﾞ
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, float):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.content.res.ColorStateList):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.graphics.PorterDuff$Mode):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.graphics.drawable.Drawable):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.support.v4.ˉ.ʼ):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.support.v4.ˉ.ـ):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, java.lang.Runnable):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, boolean):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, int):void */
    public DrawerLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f490 = new C0144();
        this.f496 = -1728053248;
        this.f500 = new Paint();
        this.f509 = true;
        this.f511 = 3;
        this.f513 = 3;
        this.f514 = 3;
        this.f515 = 3;
        this.f493 = null;
        this.f497 = null;
        this.f495 = null;
        this.f501 = null;
        setDescendantFocusability(262144);
        float f = getResources().getDisplayMetrics().density;
        this.f494 = (int) ((64.0f * f) + 0.5f);
        float f2 = 400.0f * f;
        this.f504 = new C0148(3);
        this.f505 = new C0148(5);
        this.f502 = C0188.m1066(this, 1.0f, this.f504);
        this.f502.m1086(1);
        this.f502.m1085(f2);
        this.f504.m913(this.f502);
        this.f503 = C0188.m1066(this, 1.0f, this.f505);
        this.f503.m1086(2);
        this.f503.m1085(f2);
        this.f505.m913(this.f503);
        setFocusableInTouchMode(true);
        C0414.m2218((View) this, 1);
        C0414.m2224(this, new C0143());
        C0426.m2352(this, false);
        if (C0414.m2247(this)) {
            if (Build.VERSION.SDK_INT >= 21) {
                setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
                    @TargetApi(21)
                    public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
                        ((DrawerLayout) view).m867(windowInsets, windowInsets.getSystemWindowInsetTop() > 0);
                        return windowInsets.consumeSystemWindowInsets();
                    }
                });
                setSystemUiVisibility(1280);
                TypedArray obtainStyledAttributes = context.obtainStyledAttributes(f484);
                try {
                    this.f512 = obtainStyledAttributes.getDrawable(0);
                } finally {
                    obtainStyledAttributes.recycle();
                }
            } else {
                this.f512 = null;
            }
        }
        this.f492 = f * 10.0f;
        this.f499 = new ArrayList<>();
    }

    public void setDrawerElevation(float f) {
        this.f492 = f;
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            if (m888(childAt)) {
                C0414.m2217(childAt, this.f492);
            }
        }
    }

    public float getDrawerElevation() {
        if (f485) {
            return this.f492;
        }
        return 0.0f;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m867(Object obj, boolean z) {
        this.f489 = obj;
        this.f507 = z;
        setWillNotDraw(!z && getBackground() == null);
        requestLayout();
    }

    public void setScrimColor(int i) {
        this.f496 = i;
        invalidate();
    }

    @Deprecated
    public void setDrawerListener(C0145 r2) {
        C0145 r0 = this.f518;
        if (r0 != null) {
            m873(r0);
        }
        if (r2 != null) {
            m864(r2);
        }
        this.f518 = r2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m864(C0145 r2) {
        if (r2 != null) {
            if (this.f519 == null) {
                this.f519 = new ArrayList();
            }
            this.f519.add(r2);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m873(C0145 r2) {
        List<C0145> list;
        if (r2 != null && (list = this.f519) != null) {
            list.remove(r2);
        }
    }

    public void setDrawerLockMode(int i) {
        m861(i, 3);
        m861(i, 5);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m861(int i, int i2) {
        View r4;
        int r0 = C0396.m2155(i2, C0414.m2236(this));
        if (i2 == 3) {
            this.f511 = i;
        } else if (i2 == 5) {
            this.f513 = i;
        } else if (i2 == 8388611) {
            this.f514 = i;
        } else if (i2 == 8388613) {
            this.f515 = i;
        }
        if (i != 0) {
            (r0 == 3 ? this.f502 : this.f503).m1104();
        }
        if (i == 1) {
            View r42 = m878(r0);
            if (r42 != null) {
                m891(r42);
            }
        } else if (i == 2 && (r4 = m878(r0)) != null) {
            m889(r4);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m858(int i) {
        int i2;
        int i3;
        int i4;
        int r0 = C0414.m2236(this);
        if (i == 3) {
            int i5 = this.f511;
            if (i5 != 3) {
                return i5;
            }
            int i6 = r0 == 0 ? this.f514 : this.f515;
            if (i6 != 3) {
                return i6;
            }
            return 0;
        } else if (i == 5) {
            int i7 = this.f513;
            if (i7 != 3) {
                return i7;
            }
            if (r0 == 0) {
                i2 = this.f515;
            } else {
                i2 = this.f514;
            }
            if (i2 != 3) {
                return i2;
            }
            return 0;
        } else if (i == 8388611) {
            int i8 = this.f514;
            if (i8 != 3) {
                return i8;
            }
            if (r0 == 0) {
                i3 = this.f511;
            } else {
                i3 = this.f513;
            }
            if (i3 != 3) {
                return i3;
            }
            return 0;
        } else if (i != 8388613) {
            return 0;
        } else {
            int i9 = this.f515;
            if (i9 != 3) {
                return i9;
            }
            if (r0 == 0) {
                i4 = this.f513;
            } else {
                i4 = this.f511;
            }
            if (i4 != 3) {
                return i4;
            }
            return 0;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m859(View view) {
        if (m888(view)) {
            return m858(((C0146) view.getLayoutParams()).f524);
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public CharSequence m870(int i) {
        int r2 = C0396.m2155(i, C0414.m2236(this));
        if (r2 == 3) {
            return this.f487;
        }
        if (r2 == 5) {
            return this.f491;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m862(int i, int i2, View view) {
        int r4 = this.f502.m1084();
        int r0 = this.f503.m1084();
        int i3 = 2;
        if (r4 == 1 || r0 == 1) {
            i3 = 1;
        } else if (!(r4 == 2 || r0 == 2)) {
            i3 = 0;
        }
        if (view != null && i2 == 0) {
            C0146 r42 = (C0146) view.getLayoutParams();
            if (r42.f525 == 0.0f) {
                m874(view);
            } else if (r42.f525 == 1.0f) {
                m879(view);
            }
        }
        if (i3 != this.f506) {
            this.f506 = i3;
            List<C0145> list = this.f519;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    this.f519.get(size).m901(i3);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.ʽ(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.ʽ(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.ʽ(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m874(View view) {
        View rootView;
        C0146 r0 = (C0146) view.getLayoutParams();
        if ((r0.f527 & 1) == 1) {
            r0.f527 = 0;
            List<C0145> list = this.f519;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    this.f519.get(size).m904(view);
                }
            }
            m849(view, false);
            if (hasWindowFocus() && (rootView = getRootView()) != null) {
                rootView.sendAccessibilityEvent(32);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.ʽ(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.ʽ(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.ʽ(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m879(View view) {
        C0146 r0 = (C0146) view.getLayoutParams();
        if ((r0.f527 & 1) == 0) {
            r0.f527 = 1;
            List<C0145> list = this.f519;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    this.f519.get(size).m902(view);
                }
            }
            m849(view, true);
            if (hasWindowFocus()) {
                sendAccessibilityEvent(32);
            }
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m849(View view, boolean z) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if ((z || m888(childAt)) && (!z || childAt != view)) {
                C0414.m2218(childAt, 4);
            } else {
                C0414.m2218(childAt, 1);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m865(View view, float f) {
        List<C0145> list = this.f519;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.f519.get(size).m903(view, f);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m875(View view, float f) {
        C0146 r0 = (C0146) view.getLayoutParams();
        if (f != r0.f525) {
            r0.f525 = f;
            m865(view, f);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public float m881(View view) {
        return ((C0146) view.getLayoutParams()).f525;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public int m883(View view) {
        return C0396.m2155(((C0146) view.getLayoutParams()).f524, C0414.m2236(this));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m869(View view, int i) {
        return (m883(view) & i) == i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public View m860() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if ((((C0146) childAt.getLayoutParams()).f527 & 1) == 1) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m880(View view, float f) {
        float r0 = m881(view);
        float width = (float) view.getWidth();
        int i = ((int) (width * f)) - ((int) (r0 * width));
        if (!m869(view, 3)) {
            i = -i;
        }
        view.offsetLeftAndRight(i);
        m875(view, f);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public View m878(int i) {
        int r5 = C0396.m2155(i, C0414.m2236(this)) & 7;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if ((m883(childAt) & 7) == r5) {
                return childAt;
            }
        }
        return null;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    static String m850(int i) {
        if ((i & 3) == 3) {
            return "LEFT";
        }
        return (i & 5) == 5 ? "RIGHT" : Integer.toHexString(i);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.f509 = true;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f509 = true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        if (!(mode == 1073741824 && mode2 == 1073741824)) {
            if (isInEditMode()) {
                if (mode != Integer.MIN_VALUE && mode == 0) {
                    size = 300;
                }
                if (mode2 != Integer.MIN_VALUE && mode2 == 0) {
                    size2 = 300;
                }
            } else {
                throw new IllegalArgumentException("DrawerLayout must be measured with MeasureSpec.EXACTLY.");
            }
        }
        setMeasuredDimension(size, size2);
        int i3 = 0;
        boolean z = this.f489 != null && C0414.m2247(this);
        int r7 = C0414.m2236(this);
        int childCount = getChildCount();
        int i4 = 0;
        boolean z2 = false;
        boolean z3 = false;
        while (i4 < childCount) {
            View childAt = getChildAt(i4);
            if (childAt.getVisibility() != 8) {
                C0146 r13 = (C0146) childAt.getLayoutParams();
                if (z) {
                    int r15 = C0396.m2155(r13.f524, r7);
                    if (C0414.m2247(childAt)) {
                        if (Build.VERSION.SDK_INT >= 21) {
                            WindowInsets windowInsets = (WindowInsets) this.f489;
                            if (r15 == 3) {
                                windowInsets = windowInsets.replaceSystemWindowInsets(windowInsets.getSystemWindowInsetLeft(), windowInsets.getSystemWindowInsetTop(), i3, windowInsets.getSystemWindowInsetBottom());
                            } else if (r15 == 5) {
                                windowInsets = windowInsets.replaceSystemWindowInsets(i3, windowInsets.getSystemWindowInsetTop(), windowInsets.getSystemWindowInsetRight(), windowInsets.getSystemWindowInsetBottom());
                            }
                            childAt.dispatchApplyWindowInsets(windowInsets);
                        }
                    } else if (Build.VERSION.SDK_INT >= 21) {
                        WindowInsets windowInsets2 = (WindowInsets) this.f489;
                        if (r15 == 3) {
                            windowInsets2 = windowInsets2.replaceSystemWindowInsets(windowInsets2.getSystemWindowInsetLeft(), windowInsets2.getSystemWindowInsetTop(), i3, windowInsets2.getSystemWindowInsetBottom());
                        } else if (r15 == 5) {
                            windowInsets2 = windowInsets2.replaceSystemWindowInsets(i3, windowInsets2.getSystemWindowInsetTop(), windowInsets2.getSystemWindowInsetRight(), windowInsets2.getSystemWindowInsetBottom());
                        }
                        r13.leftMargin = windowInsets2.getSystemWindowInsetLeft();
                        r13.topMargin = windowInsets2.getSystemWindowInsetTop();
                        r13.rightMargin = windowInsets2.getSystemWindowInsetRight();
                        r13.bottomMargin = windowInsets2.getSystemWindowInsetBottom();
                    }
                }
                if (m886(childAt)) {
                    childAt.measure(View.MeasureSpec.makeMeasureSpec((size - r13.leftMargin) - r13.rightMargin, 1073741824), View.MeasureSpec.makeMeasureSpec((size2 - r13.topMargin) - r13.bottomMargin, 1073741824));
                } else if (m888(childAt)) {
                    if (f485) {
                        float r2 = C0414.m2243(childAt);
                        float f = this.f492;
                        if (r2 != f) {
                            C0414.m2217(childAt, f);
                        }
                    }
                    int r22 = m883(childAt) & 7;
                    boolean z4 = r22 == 3;
                    if ((!z4 || !z2) && (z4 || !z3)) {
                        if (z4) {
                            z2 = true;
                        } else {
                            z3 = true;
                        }
                        childAt.measure(getChildMeasureSpec(i, this.f494 + r13.leftMargin + r13.rightMargin, r13.width), getChildMeasureSpec(i2, r13.topMargin + r13.bottomMargin, r13.height));
                        i4++;
                        i3 = 0;
                    } else {
                        throw new IllegalStateException("Child drawer has absolute gravity " + m850(r22) + " but this " + "DrawerLayout" + " already has a " + "drawer view along that edge");
                    }
                } else {
                    throw new IllegalStateException("Child " + childAt + " at index " + i4 + " does not have a valid layout_gravity - must be Gravity.LEFT, " + "Gravity.RIGHT or Gravity.NO_GRAVITY");
                }
            }
            i4++;
            i3 = 0;
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m851() {
        if (!f485) {
            this.f486 = m852();
            this.f488 = m853();
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private Drawable m852() {
        int r0 = C0414.m2236(this);
        if (r0 == 0) {
            Drawable drawable = this.f493;
            if (drawable != null) {
                m848(drawable, r0);
                return this.f493;
            }
        } else {
            Drawable drawable2 = this.f497;
            if (drawable2 != null) {
                m848(drawable2, r0);
                return this.f497;
            }
        }
        return this.f495;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private Drawable m853() {
        int r0 = C0414.m2236(this);
        if (r0 == 0) {
            Drawable drawable = this.f497;
            if (drawable != null) {
                m848(drawable, r0);
                return this.f497;
            }
        } else {
            Drawable drawable2 = this.f493;
            if (drawable2 != null) {
                m848(drawable2, r0);
                return this.f493;
            }
        }
        return this.f501;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m848(Drawable drawable, int i) {
        if (drawable == null || !C0288.m1708(drawable)) {
            return false;
        }
        C0288.m1709(drawable, i);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        float f;
        int i5;
        this.f508 = true;
        int i6 = i3 - i;
        int childCount = getChildCount();
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() != 8) {
                C0146 r7 = (C0146) childAt.getLayoutParams();
                if (m886(childAt)) {
                    childAt.layout(r7.leftMargin, r7.topMargin, r7.leftMargin + childAt.getMeasuredWidth(), r7.topMargin + childAt.getMeasuredHeight());
                } else {
                    int measuredWidth = childAt.getMeasuredWidth();
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (m869(childAt, 3)) {
                        float f2 = (float) measuredWidth;
                        i5 = (-measuredWidth) + ((int) (r7.f525 * f2));
                        f = ((float) (measuredWidth + i5)) / f2;
                    } else {
                        float f3 = (float) measuredWidth;
                        int i8 = i6 - ((int) (r7.f525 * f3));
                        f = ((float) (i6 - i8)) / f3;
                        i5 = i8;
                    }
                    boolean z2 = f != r7.f525;
                    int i9 = r7.f524 & 112;
                    if (i9 == 16) {
                        int i10 = i4 - i2;
                        int i11 = (i10 - measuredHeight) / 2;
                        if (i11 < r7.topMargin) {
                            i11 = r7.topMargin;
                        } else if (i11 + measuredHeight > i10 - r7.bottomMargin) {
                            i11 = (i10 - r7.bottomMargin) - measuredHeight;
                        }
                        childAt.layout(i5, i11, measuredWidth + i5, measuredHeight + i11);
                    } else if (i9 != 80) {
                        childAt.layout(i5, r7.topMargin, measuredWidth + i5, r7.topMargin + measuredHeight);
                    } else {
                        int i12 = i4 - i2;
                        childAt.layout(i5, (i12 - r7.bottomMargin) - childAt.getMeasuredHeight(), measuredWidth + i5, i12 - r7.bottomMargin);
                    }
                    if (z2) {
                        m875(childAt, f);
                    }
                    int i13 = r7.f525 > 0.0f ? 0 : 4;
                    if (childAt.getVisibility() != i13) {
                        childAt.setVisibility(i13);
                    }
                }
            }
        }
        this.f508 = false;
        this.f509 = false;
    }

    public void requestLayout() {
        if (!this.f508) {
            super.requestLayout();
        }
    }

    public void computeScroll() {
        int childCount = getChildCount();
        float f = 0.0f;
        for (int i = 0; i < childCount; i++) {
            f = Math.max(f, ((C0146) getChildAt(i).getLayoutParams()).f525);
        }
        this.f498 = f;
        boolean r0 = this.f502.m1091(true);
        boolean r1 = this.f503.m1091(true);
        if (r0 || r1) {
            C0414.m2233(this);
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private static boolean m857(View view) {
        Drawable background = view.getBackground();
        if (background == null || background.getOpacity() != -1) {
            return false;
        }
        return true;
    }

    public void setStatusBarBackground(Drawable drawable) {
        this.f512 = drawable;
        invalidate();
    }

    public Drawable getStatusBarBackgroundDrawable() {
        return this.f512;
    }

    public void setStatusBarBackground(int i) {
        this.f512 = i != 0 ? C0101.m539(getContext(), i) : null;
        invalidate();
    }

    public void setStatusBarBackgroundColor(int i) {
        this.f512 = new ColorDrawable(i);
        invalidate();
    }

    public void onRtlPropertiesChanged(int i) {
        m851();
    }

    public void onDraw(Canvas canvas) {
        Object obj;
        super.onDraw(canvas);
        if (this.f507 && this.f512 != null) {
            int systemWindowInsetTop = (Build.VERSION.SDK_INT < 21 || (obj = this.f489) == null) ? 0 : ((WindowInsets) obj).getSystemWindowInsetTop();
            if (systemWindowInsetTop > 0) {
                this.f512.setBounds(0, 0, getWidth(), systemWindowInsetTop);
                this.f512.draw(canvas);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j) {
        int i;
        View view2 = view;
        int height = getHeight();
        boolean r4 = m886(view2);
        int width = getWidth();
        int save = canvas.save();
        int i2 = 0;
        if (r4) {
            int childCount = getChildCount();
            i = width;
            int i3 = 0;
            for (int i4 = 0; i4 < childCount; i4++) {
                View childAt = getChildAt(i4);
                if (childAt != view2 && childAt.getVisibility() == 0 && m857(childAt) && m888(childAt) && childAt.getHeight() >= height) {
                    if (m869(childAt, 3)) {
                        int right = childAt.getRight();
                        if (right > i3) {
                            i3 = right;
                        }
                    } else {
                        int left = childAt.getLeft();
                        if (left < i) {
                            i = left;
                        }
                    }
                }
            }
            canvas.clipRect(i3, 0, i, getHeight());
            i2 = i3;
        } else {
            i = width;
        }
        boolean drawChild = super.drawChild(canvas, view, j);
        canvas.restoreToCount(save);
        float f = this.f498;
        if (f > 0.0f && r4) {
            int i5 = this.f496;
            this.f500.setColor((i5 & 16777215) | (((int) (((float) ((-16777216 & i5) >>> 24)) * f)) << 24));
            canvas.drawRect((float) i2, 0.0f, (float) i, (float) getHeight(), this.f500);
        } else if (this.f486 != null && m869(view2, 3)) {
            int intrinsicWidth = this.f486.getIntrinsicWidth();
            int right2 = view.getRight();
            float max = Math.max(0.0f, Math.min(((float) right2) / ((float) this.f502.m1092()), 1.0f));
            this.f486.setBounds(right2, view.getTop(), intrinsicWidth + right2, view.getBottom());
            this.f486.setAlpha((int) (max * 255.0f));
            this.f486.draw(canvas);
        } else if (this.f488 != null && m869(view2, 5)) {
            int intrinsicWidth2 = this.f488.getIntrinsicWidth();
            int left2 = view.getLeft();
            float max2 = Math.max(0.0f, Math.min(((float) (getWidth() - left2)) / ((float) this.f503.m1092()), 1.0f));
            this.f488.setBounds(left2 - intrinsicWidth2, view.getTop(), left2, view.getBottom());
            this.f488.setAlpha((int) (max2 * 255.0f));
            this.f488.draw(canvas);
        }
        return drawChild;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m886(View view) {
        return ((C0146) view.getLayoutParams()).f524 == 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m888(View view) {
        int r3 = C0396.m2155(((C0146) view.getLayoutParams()).f524, C0414.m2236(view));
        return ((r3 & 3) == 0 && (r3 & 5) == 0) ? false : true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001b, code lost:
        if (r0 != 3) goto L_0x0038;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onInterceptTouchEvent(android.view.MotionEvent r7) {
        /*
            r6 = this;
            int r0 = r7.getActionMasked()
            android.support.v4.widget.י r1 = r6.f502
            boolean r1 = r1.m1089(r7)
            android.support.v4.widget.י r2 = r6.f503
            boolean r2 = r2.m1089(r7)
            r1 = r1 | r2
            r2 = 1
            r3 = 0
            if (r0 == 0) goto L_0x003a
            if (r0 == r2) goto L_0x0031
            r7 = 2
            r4 = 3
            if (r0 == r7) goto L_0x001e
            if (r0 == r4) goto L_0x0031
            goto L_0x0038
        L_0x001e:
            android.support.v4.widget.י r7 = r6.f502
            boolean r7 = r7.m1103(r4)
            if (r7 == 0) goto L_0x0038
            android.support.v4.widget.DrawerLayout$ˆ r7 = r6.f504
            r7.m910()
            android.support.v4.widget.DrawerLayout$ˆ r7 = r6.f505
            r7.m910()
            goto L_0x0038
        L_0x0031:
            r6.m868(r2)
            r6.f516 = r3
            r6.f517 = r3
        L_0x0038:
            r7 = 0
            goto L_0x0064
        L_0x003a:
            float r0 = r7.getX()
            float r7 = r7.getY()
            r6.f520 = r0
            r6.f510 = r7
            float r4 = r6.f498
            r5 = 0
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 <= 0) goto L_0x005f
            android.support.v4.widget.י r4 = r6.f502
            int r0 = (int) r0
            int r7 = (int) r7
            android.view.View r7 = r4.m1102(r0, r7)
            if (r7 == 0) goto L_0x005f
            boolean r7 = r6.m886(r7)
            if (r7 == 0) goto L_0x005f
            r7 = 1
            goto L_0x0060
        L_0x005f:
            r7 = 0
        L_0x0060:
            r6.f516 = r3
            r6.f517 = r3
        L_0x0064:
            if (r1 != 0) goto L_0x0074
            if (r7 != 0) goto L_0x0074
            boolean r7 = r6.m854()
            if (r7 != 0) goto L_0x0074
            boolean r7 = r6.f517
            if (r7 == 0) goto L_0x0073
            goto L_0x0074
        L_0x0073:
            r2 = 0
        L_0x0074:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.DrawerLayout.onInterceptTouchEvent(android.view.MotionEvent):boolean");
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View r7;
        this.f502.m1093(motionEvent);
        this.f503.m1093(motionEvent);
        int action = motionEvent.getAction() & 255;
        if (action == 0) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            this.f520 = x;
            this.f510 = y;
            this.f516 = false;
            this.f517 = false;
        } else if (action == 1) {
            float x2 = motionEvent.getX();
            float y2 = motionEvent.getY();
            View r3 = this.f502.m1102((int) x2, (int) y2);
            if (r3 != null && m886(r3)) {
                float f = x2 - this.f520;
                float f2 = y2 - this.f510;
                int r32 = this.f502.m1101();
                if (!((f * f) + (f2 * f2) >= ((float) (r32 * r32)) || (r7 = m860()) == null || m859(r7) == 2)) {
                    z = false;
                    m868(z);
                    this.f516 = false;
                }
            }
            z = true;
            m868(z);
            this.f516 = false;
        } else if (action == 3) {
            m868(true);
            this.f516 = false;
            this.f517 = false;
        }
        return true;
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        this.f516 = z;
        if (z) {
            m868(true);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m871() {
        m868(false);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m868(boolean z) {
        boolean z2;
        int childCount = getChildCount();
        boolean z3 = false;
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            C0146 r5 = (C0146) childAt.getLayoutParams();
            if (m888(childAt) && (!z || r5.f526)) {
                int width = childAt.getWidth();
                if (m869(childAt, 3)) {
                    z2 = this.f502.m1090(childAt, -width, childAt.getTop());
                } else {
                    z2 = this.f503.m1090(childAt, getWidth(), childAt.getTop());
                }
                z3 |= z2;
                r5.f526 = false;
            }
        }
        this.f504.m910();
        this.f505.m910();
        if (z3) {
            invalidate();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.ʻ(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.ʻ(android.graphics.drawable.Drawable, int):boolean
      android.support.v4.widget.DrawerLayout.ʻ(int, int):void
      android.support.v4.widget.DrawerLayout.ʻ(int, boolean):void
      android.support.v4.widget.DrawerLayout.ʻ(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.ʻ(java.lang.Object, boolean):void
      android.support.v4.widget.DrawerLayout.ʻ(android.view.View, int):boolean
      android.support.v4.widget.DrawerLayout.ʻ(android.view.View, boolean):void */
    /* renamed from: ˉ  reason: contains not printable characters */
    public void m889(View view) {
        m866(view, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.ʽ(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.ʽ(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.ʽ(android.view.View, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m866(View view, boolean z) {
        if (m888(view)) {
            C0146 r0 = (C0146) view.getLayoutParams();
            if (this.f509) {
                r0.f525 = 1.0f;
                r0.f527 = 1;
                m849(view, true);
            } else if (z) {
                r0.f527 |= 2;
                if (m869(view, 3)) {
                    this.f502.m1090(view, 0, view.getTop());
                } else {
                    this.f503.m1090(view, getWidth() - view.getWidth(), view.getTop());
                }
            } else {
                m880(view, 1.0f);
                m862(r0.f524, 0, view);
                view.setVisibility(0);
            }
            invalidate();
            return;
        }
        throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.ʻ(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.DrawerLayout.ʻ(android.graphics.drawable.Drawable, int):boolean
      android.support.v4.widget.DrawerLayout.ʻ(int, int):void
      android.support.v4.widget.DrawerLayout.ʻ(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.ʻ(android.view.View, boolean):void
      android.support.v4.widget.DrawerLayout.ʻ(java.lang.Object, boolean):void
      android.support.v4.widget.DrawerLayout.ʻ(android.view.View, int):boolean
      android.support.v4.widget.DrawerLayout.ʻ(int, boolean):void */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m884(int i) {
        m863(i, true);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m863(int i, boolean z) {
        View r0 = m878(i);
        if (r0 != null) {
            m866(r0, z);
            return;
        }
        throw new IllegalArgumentException("No drawer view found with gravity " + m850(i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.ʼ(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.ʼ(int, boolean):void
      android.support.v4.widget.DrawerLayout.ʼ(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.ʼ(android.view.View, boolean):void */
    /* renamed from: ˊ  reason: contains not printable characters */
    public void m891(View view) {
        m876(view, true);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m876(View view, boolean z) {
        if (m888(view)) {
            C0146 r0 = (C0146) view.getLayoutParams();
            if (this.f509) {
                r0.f525 = 0.0f;
                r0.f527 = 0;
            } else if (z) {
                r0.f527 |= 4;
                if (m869(view, 3)) {
                    this.f502.m1090(view, -view.getWidth(), view.getTop());
                } else {
                    this.f503.m1090(view, getWidth(), view.getTop());
                }
            } else {
                m880(view, 0.0f);
                m862(r0.f524, 0, view);
                view.setVisibility(4);
            }
            invalidate();
            return;
        }
        throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.ʼ(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.DrawerLayout.ʼ(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.ʼ(android.view.View, boolean):void
      android.support.v4.widget.DrawerLayout.ʼ(int, boolean):void */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m885(int i) {
        m872(i, true);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m872(int i, boolean z) {
        View r0 = m878(i);
        if (r0 != null) {
            m876(r0, z);
            return;
        }
        throw new IllegalArgumentException("No drawer view found with gravity " + m850(i));
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean m892(View view) {
        if (m888(view)) {
            return (((C0146) view.getLayoutParams()).f527 & 1) == 1;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m887(int i) {
        View r1 = m878(i);
        if (r1 != null) {
            return m892(r1);
        }
        return false;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean m893(View view) {
        if (m888(view)) {
            return ((C0146) view.getLayoutParams()).f525 > 0.0f;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m890(int i) {
        View r1 = m878(i);
        if (r1 != null) {
            return m893(r1);
        }
        return false;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean m854() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (((C0146) getChildAt(i).getLayoutParams()).f526) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new C0146(-1, -1);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof C0146) {
            return new C0146((C0146) layoutParams);
        }
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? new C0146((ViewGroup.MarginLayoutParams) layoutParams) : new C0146(layoutParams);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof C0146) && super.checkLayoutParams(layoutParams);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new C0146(getContext(), attributeSet);
    }

    public void addFocusables(ArrayList<View> arrayList, int i, int i2) {
        if (getDescendantFocusability() != 393216) {
            int childCount = getChildCount();
            boolean z = false;
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt = getChildAt(i3);
                if (!m888(childAt)) {
                    this.f499.add(childAt);
                } else if (m892(childAt)) {
                    childAt.addFocusables(arrayList, i, i2);
                    z = true;
                }
            }
            if (!z) {
                int size = this.f499.size();
                for (int i4 = 0; i4 < size; i4++) {
                    View view = this.f499.get(i4);
                    if (view.getVisibility() == 0) {
                        view.addFocusables(arrayList, i, i2);
                    }
                }
            }
            this.f499.clear();
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean m855() {
        return m877() != null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public View m877() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (m888(childAt) && m893(childAt)) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m882() {
        if (!this.f517) {
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                getChildAt(i).dispatchTouchEvent(obtain);
            }
            obtain.recycle();
            this.f517 = true;
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || !m855()) {
            return super.onKeyDown(i, keyEvent);
        }
        keyEvent.startTracking();
        return true;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyUp(i, keyEvent);
        }
        View r2 = m877();
        if (r2 != null && m859(r2) == 0) {
            m871();
        }
        return r2 != null;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        View r0;
        if (!(parcelable instanceof C0147)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        C0147 r4 = (C0147) parcelable;
        super.onRestoreInstanceState(r4.m1995());
        if (!(r4.f528 == 0 || (r0 = m878(r4.f528)) == null)) {
            m889(r0);
        }
        if (r4.f529 != 3) {
            m861(r4.f529, 3);
        }
        if (r4.f530 != 3) {
            m861(r4.f530, 5);
        }
        if (r4.f531 != 3) {
            m861(r4.f531, 8388611);
        }
        if (r4.f532 != 3) {
            m861(r4.f532, 8388613);
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        C0147 r1 = new C0147(super.onSaveInstanceState());
        int childCount = getChildCount();
        int i = 0;
        while (true) {
            if (i >= childCount) {
                break;
            }
            C0146 r4 = (C0146) getChildAt(i).getLayoutParams();
            boolean z = true;
            boolean z2 = r4.f527 == 1;
            if (r4.f527 != 2) {
                z = false;
            }
            if (z2 || z) {
                r1.f528 = r4.f524;
            } else {
                i++;
            }
        }
        r1.f529 = this.f511;
        r1.f530 = this.f513;
        r1.f531 = this.f514;
        r1.f532 = this.f515;
        return r1;
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
        if (m860() != null || m888(view)) {
            C0414.m2218(view, 4);
        } else {
            C0414.m2218(view, 1);
        }
        if (!f483) {
            C0414.m2224(view, this.f490);
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static boolean m856(View view) {
        return (C0414.m2235(view) == 4 || C0414.m2235(view) == 2) ? false : true;
    }

    /* renamed from: android.support.v4.widget.DrawerLayout$ʿ  reason: contains not printable characters */
    protected static class C0147 extends C0355 {
        public static final Parcelable.Creator<C0147> CREATOR = new Parcelable.ClassLoaderCreator<C0147>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C0147 createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new C0147(parcel, classLoader);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0147 createFromParcel(Parcel parcel) {
                return new C0147(parcel, null);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0147[] newArray(int i) {
                return new C0147[i];
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        int f528 = 0;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f529;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f530;

        /* renamed from: ʿ  reason: contains not printable characters */
        int f531;

        /* renamed from: ˆ  reason: contains not printable characters */
        int f532;

        public C0147(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f528 = parcel.readInt();
            this.f529 = parcel.readInt();
            this.f530 = parcel.readInt();
            this.f531 = parcel.readInt();
            this.f532 = parcel.readInt();
        }

        public C0147(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f528);
            parcel.writeInt(this.f529);
            parcel.writeInt(this.f530);
            parcel.writeInt(this.f531);
            parcel.writeInt(this.f532);
        }
    }

    /* renamed from: android.support.v4.widget.DrawerLayout$ˆ  reason: contains not printable characters */
    private class C0148 extends C0188.C0189 {

        /* renamed from: ʼ  reason: contains not printable characters */
        private final int f534;

        /* renamed from: ʽ  reason: contains not printable characters */
        private C0188 f535;

        /* renamed from: ʾ  reason: contains not printable characters */
        private final Runnable f536 = new Runnable() {
            public void run() {
                C0148.this.m919();
            }
        };

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m922(int i) {
            return false;
        }

        C0148(int i) {
            this.f534 = i;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m913(C0188 r1) {
            this.f535 = r1;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m910() {
            DrawerLayout.this.removeCallbacks(this.f536);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m916(View view, int i) {
            return DrawerLayout.this.m888(view) && DrawerLayout.this.m869(view, this.f534) && DrawerLayout.this.m859(view) == 0;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m911(int i) {
            DrawerLayout.this.m862(this.f534, i, this.f535.m1098());
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m915(View view, int i, int i2, int i3, int i4) {
            float f;
            int width = view.getWidth();
            if (DrawerLayout.this.m869(view, 3)) {
                f = (float) (i + width);
            } else {
                f = (float) (DrawerLayout.this.getWidth() - i);
            }
            float f2 = f / ((float) width);
            DrawerLayout.this.m875(view, f2);
            view.setVisibility(f2 == 0.0f ? 4 : 0);
            DrawerLayout.this.invalidate();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m921(View view, int i) {
            ((C0146) view.getLayoutParams()).f526 = false;
            m908();
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        private void m908() {
            int i = 3;
            if (this.f534 == 3) {
                i = 5;
            }
            View r0 = DrawerLayout.this.m878(i);
            if (r0 != null) {
                DrawerLayout.this.m891(r0);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m914(View view, float f, float f2) {
            int i;
            float r8 = DrawerLayout.this.m881(view);
            int width = view.getWidth();
            if (DrawerLayout.this.m869(view, 3)) {
                i = (f > 0.0f || (f == 0.0f && r8 > 0.5f)) ? 0 : -width;
            } else {
                int width2 = DrawerLayout.this.getWidth();
                if (f < 0.0f || (f == 0.0f && r8 > 0.5f)) {
                    width2 -= width;
                }
                i = width2;
            }
            this.f535.m1088(i, view.getTop());
            DrawerLayout.this.invalidate();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m912(int i, int i2) {
            DrawerLayout.this.postDelayed(this.f536, 160);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m919() {
            View view;
            int i;
            int r0 = this.f535.m1092();
            int i2 = 0;
            boolean z = this.f534 == 3;
            if (z) {
                view = DrawerLayout.this.m878(3);
                if (view != null) {
                    i2 = -view.getWidth();
                }
                i = i2 + r0;
            } else {
                view = DrawerLayout.this.m878(5);
                i = DrawerLayout.this.getWidth() - r0;
            }
            if (view == null) {
                return;
            }
            if (((z && view.getLeft() < i) || (!z && view.getLeft() > i)) && DrawerLayout.this.m859(view) == 0) {
                this.f535.m1090(view, i, view.getTop());
                ((C0146) view.getLayoutParams()).f526 = true;
                DrawerLayout.this.invalidate();
                m908();
                DrawerLayout.this.m882();
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m920(int i, int i2) {
            View view;
            if ((i & 1) == 1) {
                view = DrawerLayout.this.m878(3);
            } else {
                view = DrawerLayout.this.m878(5);
            }
            if (view != null && DrawerLayout.this.m859(view) == 0) {
                this.f535.m1087(view, i2);
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m917(View view) {
            if (DrawerLayout.this.m888(view)) {
                return view.getWidth();
            }
            return 0;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m918(View view, int i, int i2) {
            if (DrawerLayout.this.m869(view, 3)) {
                return Math.max(-view.getWidth(), Math.min(i, 0));
            }
            int width = DrawerLayout.this.getWidth();
            return Math.max(width - view.getWidth(), Math.min(i, width));
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m909(View view, int i, int i2) {
            return view.getTop();
        }
    }

    /* renamed from: android.support.v4.widget.DrawerLayout$ʾ  reason: contains not printable characters */
    public static class C0146 extends ViewGroup.MarginLayoutParams {

        /* renamed from: ʻ  reason: contains not printable characters */
        public int f524 = 0;

        /* renamed from: ʼ  reason: contains not printable characters */
        float f525;

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean f526;

        /* renamed from: ʾ  reason: contains not printable characters */
        int f527;

        public C0146(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, DrawerLayout.f482);
            this.f524 = obtainStyledAttributes.getInt(0, 0);
            obtainStyledAttributes.recycle();
        }

        public C0146(int i, int i2) {
            super(i, i2);
        }

        public C0146(C0146 r2) {
            super((ViewGroup.MarginLayoutParams) r2);
            this.f524 = r2.f524;
        }

        public C0146(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public C0146(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }
    }

    /* renamed from: android.support.v4.widget.DrawerLayout$ʻ  reason: contains not printable characters */
    class C0143 extends C0386 {

        /* renamed from: ʽ  reason: contains not printable characters */
        private final Rect f523 = new Rect();

        C0143() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m896(View view, C0360 r5) {
            if (DrawerLayout.f483) {
                super.m2129(view, r5);
            } else {
                C0360 r0 = C0360.m2005(r5);
                super.m2129(view, r0);
                r5.m2011(view);
                ViewParent r1 = C0414.m2237(view);
                if (r1 instanceof View) {
                    r5.m2023((View) r1);
                }
                m894(r5, r0);
                r0.m2051();
                m895(r5, (ViewGroup) view);
            }
            r5.m2019((CharSequence) DrawerLayout.class.getName());
            r5.m2025(false);
            r5.m2028(false);
            r5.m2015(C0360.C0361.f1185);
            r5.m2015(C0360.C0361.f1187);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m897(View view, AccessibilityEvent accessibilityEvent) {
            super.m2130(view, accessibilityEvent);
            accessibilityEvent.setClassName(DrawerLayout.class.getName());
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m899(View view, AccessibilityEvent accessibilityEvent) {
            CharSequence r4;
            if (accessibilityEvent.getEventType() != 32) {
                return super.m2134(view, accessibilityEvent);
            }
            List<CharSequence> text = accessibilityEvent.getText();
            View r42 = DrawerLayout.this.m877();
            if (r42 == null || (r4 = DrawerLayout.this.m870(DrawerLayout.this.m883(r42))) == null) {
                return true;
            }
            text.add(r4);
            return true;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m898(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            if (DrawerLayout.f483 || DrawerLayout.m856(view)) {
                return super.m2132(viewGroup, view, accessibilityEvent);
            }
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m895(C0360 r5, ViewGroup viewGroup) {
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = viewGroup.getChildAt(i);
                if (DrawerLayout.m856(childAt)) {
                    r5.m2018(childAt);
                }
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m894(C0360 r2, C0360 r3) {
            Rect rect = this.f523;
            r3.m2010(rect);
            r2.m2017(rect);
            r3.m2022(rect);
            r2.m2027(rect);
            r2.m2030(r3.m2035());
            r2.m2012(r3.m2047());
            r2.m2019(r3.m2048());
            r2.m2024(r3.m2050());
            r2.m2040(r3.m2044());
            r2.m2036(r3.m2041());
            r2.m2025(r3.m2031());
            r2.m2028(r3.m2033());
            r2.m2032(r3.m2037());
            r2.m2034(r3.m2039());
            r2.m2038(r3.m2043());
            r2.m2009(r3.m2016());
        }
    }

    /* renamed from: android.support.v4.widget.DrawerLayout$ʼ  reason: contains not printable characters */
    static final class C0144 extends C0386 {
        C0144() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m900(View view, C0360 r2) {
            super.m2129(view, r2);
            if (!DrawerLayout.m856(view)) {
                r2.m2023((View) null);
            }
        }
    }
}
