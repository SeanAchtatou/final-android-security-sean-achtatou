package android.support.v4.widget;

import android.os.Build;
import android.view.View;
import android.widget.ListView;

/* renamed from: android.support.v4.widget.ˉ  reason: contains not printable characters */
/* compiled from: ListViewCompat */
public final class C0172 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1043(ListView listView, int i) {
        View childAt;
        if (Build.VERSION.SDK_INT >= 19) {
            listView.scrollListBy(i);
            return;
        }
        int firstVisiblePosition = listView.getFirstVisiblePosition();
        if (firstVisiblePosition != -1 && (childAt = listView.getChildAt(0)) != null) {
            listView.setSelectionFromTop(firstVisiblePosition, childAt.getTop() - i);
        }
    }
}
