package android.support.v4.widget;

import android.database.Cursor;
import android.widget.Filter;

/* renamed from: android.support.v4.widget.ʾ  reason: contains not printable characters */
/* compiled from: CursorFilter */
class C0162 extends Filter {

    /* renamed from: ʻ  reason: contains not printable characters */
    C0163 f611;

    /* renamed from: android.support.v4.widget.ʾ$ʻ  reason: contains not printable characters */
    /* compiled from: CursorFilter */
    interface C0163 {
        /* renamed from: ʻ  reason: contains not printable characters */
        Cursor m1017();

        /* renamed from: ʻ  reason: contains not printable characters */
        Cursor m1018(CharSequence charSequence);

        /* renamed from: ʻ  reason: contains not printable characters */
        void m1019(Cursor cursor);

        /* renamed from: ʽ  reason: contains not printable characters */
        CharSequence m1020(Cursor cursor);
    }

    C0162(C0163 r1) {
        this.f611 = r1;
    }

    public CharSequence convertResultToString(Object obj) {
        return this.f611.m1020((Cursor) obj);
    }

    /* access modifiers changed from: protected */
    public Filter.FilterResults performFiltering(CharSequence charSequence) {
        Cursor r3 = this.f611.m1018(charSequence);
        Filter.FilterResults filterResults = new Filter.FilterResults();
        if (r3 != null) {
            filterResults.count = r3.getCount();
            filterResults.values = r3;
        } else {
            filterResults.count = 0;
            filterResults.values = null;
        }
        return filterResults;
    }

    /* access modifiers changed from: protected */
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        Cursor r2 = this.f611.m1017();
        if (filterResults.values != null && filterResults.values != r2) {
            this.f611.m1019((Cursor) filterResults.values);
        }
    }
}
