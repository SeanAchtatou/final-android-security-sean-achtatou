package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.ˉ.C0386;
import android.support.v4.ˉ.C0407;
import android.support.v4.ˉ.C0408;
import android.support.v4.ˉ.C0409;
import android.support.v4.ˉ.C0411;
import android.support.v4.ˉ.C0414;
import android.support.v4.ˉ.ʻ.C0360;
import android.support.v4.ˉ.ʻ.C0382;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.AnimationUtils;
import android.widget.EdgeEffect;
import android.widget.FrameLayout;
import android.widget.OverScroller;
import android.widget.ScrollView;
import java.util.ArrayList;

public class NestedScrollView extends FrameLayout implements C0407, C0409 {

    /* renamed from: ﹳ  reason: contains not printable characters */
    private static final C0149 f538 = new C0149();

    /* renamed from: ﹶ  reason: contains not printable characters */
    private static final int[] f539 = {16843130};

    /* renamed from: ʻ  reason: contains not printable characters */
    private long f540;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Rect f541;

    /* renamed from: ʽ  reason: contains not printable characters */
    private OverScroller f542;

    /* renamed from: ʾ  reason: contains not printable characters */
    private EdgeEffect f543;

    /* renamed from: ʿ  reason: contains not printable characters */
    private EdgeEffect f544;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f545;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f546;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f547;

    /* renamed from: ˊ  reason: contains not printable characters */
    private View f548;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f549;

    /* renamed from: ˎ  reason: contains not printable characters */
    private VelocityTracker f550;

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f551;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f552;

    /* renamed from: י  reason: contains not printable characters */
    private int f553;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f554;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f555;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f556;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private float f557;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final int[] f558;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private C0150 f559;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private final int[] f560;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private int f561;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private int f562;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private C0151 f563;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final C0411 f564;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private final C0408 f565;

    /* renamed from: android.support.v4.widget.NestedScrollView$ʼ  reason: contains not printable characters */
    public interface C0150 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m959(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static int m931(int i, int i2, int i3) {
        if (i2 >= i3 || i < 0) {
            return 0;
        }
        return i2 + i > i3 ? i3 - i2 : i;
    }

    public boolean onStartNestedScroll(View view, View view2, int i) {
        return (i & 2) != 0;
    }

    public boolean shouldDelayChildPressedState() {
        return true;
    }

    public NestedScrollView(Context context) {
        this(context, null);
    }

    public NestedScrollView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public NestedScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f541 = new Rect();
        this.f546 = true;
        this.f547 = false;
        this.f548 = null;
        this.f549 = false;
        this.f552 = true;
        this.f556 = -1;
        this.f558 = new int[2];
        this.f560 = new int[2];
        m924();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, f539, i, 0);
        setFillViewport(obtainStyledAttributes.getBoolean(0, false));
        obtainStyledAttributes.recycle();
        this.f564 = new C0411(this);
        this.f565 = new C0408(this);
        setNestedScrollingEnabled(true);
        C0414.m2224(this, f538);
    }

    public void setNestedScrollingEnabled(boolean z) {
        this.f565.m2191(z);
    }

    public boolean isNestedScrollingEnabled() {
        return this.f565.m2192();
    }

    public boolean startNestedScroll(int i) {
        return this.f565.m2202(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m944(int i, int i2) {
        return this.f565.m2196(i, i2);
    }

    public void stopNestedScroll() {
        this.f565.m2203();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m943(int i) {
        this.f565.m2204(i);
    }

    public boolean hasNestedScrollingParent() {
        return this.f565.m2201();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m950(int i) {
        return this.f565.m2195(i);
    }

    public boolean dispatchNestedScroll(int i, int i2, int i3, int i4, int[] iArr) {
        return this.f565.m2197(i, i2, i3, i4, iArr);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m946(int i, int i2, int i3, int i4, int[] iArr, int i5) {
        return this.f565.m2198(i, i2, i3, i4, iArr, i5);
    }

    public boolean dispatchNestedPreScroll(int i, int i2, int[] iArr, int[] iArr2) {
        return this.f565.m2199(i, i2, iArr, iArr2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m947(int i, int i2, int[] iArr, int[] iArr2, int i3) {
        return this.f565.m2200(i, i2, iArr, iArr2, i3);
    }

    public boolean dispatchNestedFling(float f, float f2, boolean z) {
        return this.f565.m2194(f, f2, z);
    }

    public boolean dispatchNestedPreFling(float f, float f2) {
        return this.f565.m2193(f, f2);
    }

    public void onNestedScrollAccepted(View view, View view2, int i) {
        this.f564.m2213(view, view2, i);
        startNestedScroll(2);
    }

    public void onStopNestedScroll(View view) {
        this.f564.m2211(view);
        stopNestedScroll();
    }

    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        int scrollY = getScrollY();
        scrollBy(0, i4);
        int scrollY2 = getScrollY() - scrollY;
        dispatchNestedScroll(0, scrollY2, 0, i4 - scrollY2, null);
    }

    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
        dispatchNestedPreScroll(i, i2, iArr, null);
    }

    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        if (z) {
            return false;
        }
        m941((int) f2);
        return true;
    }

    public boolean onNestedPreFling(View view, float f, float f2) {
        return dispatchNestedPreFling(f, f2);
    }

    public int getNestedScrollAxes() {
        return this.f564.m2210();
    }

    /* access modifiers changed from: protected */
    public float getTopFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        int scrollY = getScrollY();
        if (scrollY < verticalFadingEdgeLength) {
            return ((float) scrollY) / ((float) verticalFadingEdgeLength);
        }
        return 1.0f;
    }

    /* access modifiers changed from: protected */
    public float getBottomFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        int bottom = (getChildAt(0).getBottom() - getScrollY()) - (getHeight() - getPaddingBottom());
        if (bottom < verticalFadingEdgeLength) {
            return ((float) bottom) / ((float) verticalFadingEdgeLength);
        }
        return 1.0f;
    }

    public int getMaxScrollAmount() {
        return (int) (((float) getHeight()) * 0.5f);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m924() {
        this.f542 = new OverScroller(getContext());
        setFocusable(true);
        setDescendantFocusability(262144);
        setWillNotDraw(false);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        this.f553 = viewConfiguration.getScaledTouchSlop();
        this.f554 = viewConfiguration.getScaledMinimumFlingVelocity();
        this.f555 = viewConfiguration.getScaledMaximumFlingVelocity();
    }

    public void addView(View view) {
        if (getChildCount() <= 0) {
            super.addView(view);
            return;
        }
        throw new IllegalStateException("ScrollView can host only one direct child");
    }

    public void addView(View view, int i) {
        if (getChildCount() <= 0) {
            super.addView(view, i);
            return;
        }
        throw new IllegalStateException("ScrollView can host only one direct child");
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        if (getChildCount() <= 0) {
            super.addView(view, layoutParams);
            return;
        }
        throw new IllegalStateException("ScrollView can host only one direct child");
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (getChildCount() <= 0) {
            super.addView(view, i, layoutParams);
            return;
        }
        throw new IllegalStateException("ScrollView can host only one direct child");
    }

    public void setOnScrollChangeListener(C0150 r1) {
        this.f559 = r1;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m933() {
        View childAt = getChildAt(0);
        if (childAt == null) {
            return false;
        }
        if (getHeight() < childAt.getHeight() + getPaddingTop() + getPaddingBottom()) {
            return true;
        }
        return false;
    }

    public void setFillViewport(boolean z) {
        if (z != this.f551) {
            this.f551 = z;
            requestLayout();
        }
    }

    public void setSmoothScrollingEnabled(boolean z) {
        this.f552 = z;
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        C0150 r0 = this.f559;
        if (r0 != null) {
            r0.m959(this, i, i2, i3, i4);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.f551 && View.MeasureSpec.getMode(i2) != 0 && getChildCount() > 0) {
            View childAt = getChildAt(0);
            int measuredHeight = getMeasuredHeight();
            if (childAt.getMeasuredHeight() < measuredHeight) {
                childAt.measure(getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight(), ((FrameLayout.LayoutParams) childAt.getLayoutParams()).width), View.MeasureSpec.makeMeasureSpec((measuredHeight - getPaddingTop()) - getPaddingBottom(), 1073741824));
            }
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || m948(keyEvent);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m948(KeyEvent keyEvent) {
        this.f541.setEmpty();
        int i = 130;
        if (!m933()) {
            if (!isFocused() || keyEvent.getKeyCode() == 4) {
                return false;
            }
            View findFocus = findFocus();
            if (findFocus == this) {
                findFocus = null;
            }
            View findNextFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, 130);
            if (findNextFocus == null || findNextFocus == this || !findNextFocus.requestFocus(130)) {
                return false;
            }
            return true;
        } else if (keyEvent.getAction() != 0) {
            return false;
        } else {
            int keyCode = keyEvent.getKeyCode();
            if (keyCode != 19) {
                if (keyCode != 20) {
                    if (keyCode != 62) {
                        return false;
                    }
                    if (keyEvent.isShiftPressed()) {
                        i = 33;
                    }
                    m952(i);
                    return false;
                } else if (!keyEvent.isAltPressed()) {
                    return m954(130);
                } else {
                    return m953(130);
                }
            } else if (!keyEvent.isAltPressed()) {
                return m954(33);
            } else {
                return m953(33);
            }
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean m936(int i, int i2) {
        if (getChildCount() <= 0) {
            return false;
        }
        int scrollY = getScrollY();
        View childAt = getChildAt(0);
        if (i2 < childAt.getTop() - scrollY || i2 >= childAt.getBottom() - scrollY || i < childAt.getLeft() || i >= childAt.getRight()) {
            return false;
        }
        return true;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m934() {
        VelocityTracker velocityTracker = this.f550;
        if (velocityTracker == null) {
            this.f550 = VelocityTracker.obtain();
        } else {
            velocityTracker.clear();
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m935() {
        if (this.f550 == null) {
            this.f550 = VelocityTracker.obtain();
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m937() {
        VelocityTracker velocityTracker = this.f550;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.f550 = null;
        }
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        if (z) {
            m937();
        }
        super.requestDisallowInterceptTouchEvent(z);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 2 && this.f549) {
            return true;
        }
        int i = action & 255;
        if (i != 0) {
            if (i != 1) {
                if (i == 2) {
                    int i2 = this.f556;
                    if (i2 != -1) {
                        int findPointerIndex = motionEvent.findPointerIndex(i2);
                        if (findPointerIndex == -1) {
                            Log.e("NestedScrollView", "Invalid pointerId=" + i2 + " in onInterceptTouchEvent");
                        } else {
                            int y = (int) motionEvent.getY(findPointerIndex);
                            if (Math.abs(y - this.f545) > this.f553 && (2 & getNestedScrollAxes()) == 0) {
                                this.f549 = true;
                                this.f545 = y;
                                m935();
                                this.f550.addMovement(motionEvent);
                                this.f561 = 0;
                                ViewParent parent = getParent();
                                if (parent != null) {
                                    parent.requestDisallowInterceptTouchEvent(true);
                                }
                            }
                        }
                    }
                } else if (i != 3) {
                    if (i == 6) {
                        m925(motionEvent);
                    }
                }
            }
            this.f549 = false;
            this.f556 = -1;
            m937();
            if (this.f542.springBack(getScrollX(), getScrollY(), 0, 0, 0, getScrollRange())) {
                C0414.m2233(this);
            }
            m943(0);
        } else {
            int y2 = (int) motionEvent.getY();
            if (!m936((int) motionEvent.getX(), y2)) {
                this.f549 = false;
                m937();
            } else {
                this.f545 = y2;
                this.f556 = motionEvent.getPointerId(0);
                m934();
                this.f550.addMovement(motionEvent);
                this.f542.computeScrollOffset();
                this.f549 = !this.f542.isFinished();
                m944(2, 0);
            }
        }
        return this.f549;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        ViewParent parent;
        MotionEvent motionEvent2 = motionEvent;
        m935();
        MotionEvent obtain = MotionEvent.obtain(motionEvent);
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.f561 = 0;
        }
        obtain.offsetLocation(0.0f, (float) this.f561);
        if (actionMasked != 0) {
            if (actionMasked == 1) {
                VelocityTracker velocityTracker = this.f550;
                velocityTracker.computeCurrentVelocity(1000, (float) this.f555);
                int yVelocity = (int) velocityTracker.getYVelocity(this.f556);
                if (Math.abs(yVelocity) > this.f554) {
                    m941(-yVelocity);
                } else if (this.f542.springBack(getScrollX(), getScrollY(), 0, 0, 0, getScrollRange())) {
                    C0414.m2233(this);
                }
                this.f556 = -1;
                m938();
            } else if (actionMasked == 2) {
                int findPointerIndex = motionEvent2.findPointerIndex(this.f556);
                if (findPointerIndex == -1) {
                    Log.e("NestedScrollView", "Invalid pointerId=" + this.f556 + " in onTouchEvent");
                } else {
                    int y = (int) motionEvent2.getY(findPointerIndex);
                    int i = this.f545 - y;
                    if (m947(0, i, this.f560, this.f558, 0)) {
                        i -= this.f560[1];
                        obtain.offsetLocation(0.0f, (float) this.f558[1]);
                        this.f561 += this.f558[1];
                    }
                    if (!this.f549 && Math.abs(i) > this.f553) {
                        ViewParent parent2 = getParent();
                        if (parent2 != null) {
                            parent2.requestDisallowInterceptTouchEvent(true);
                        }
                        this.f549 = true;
                        if (i > 0) {
                            i -= this.f553;
                        } else {
                            i += this.f553;
                        }
                    }
                    int i2 = i;
                    if (this.f549) {
                        this.f545 = y - this.f558[1];
                        int scrollY = getScrollY();
                        int scrollRange = getScrollRange();
                        int overScrollMode = getOverScrollMode();
                        boolean z = overScrollMode == 0 || (overScrollMode == 1 && scrollRange > 0);
                        int i3 = scrollRange;
                        int i4 = i2;
                        int i5 = findPointerIndex;
                        if (m945(0, i2, 0, getScrollY(), 0, scrollRange, 0, 0, true) && !m950(0)) {
                            this.f550.clear();
                        }
                        int scrollY2 = getScrollY() - scrollY;
                        if (m946(0, scrollY2, 0, i4 - scrollY2, this.f558, 0)) {
                            int i6 = this.f545;
                            int[] iArr = this.f558;
                            this.f545 = i6 - iArr[1];
                            obtain.offsetLocation(0.0f, (float) iArr[1]);
                            this.f561 += this.f558[1];
                        } else if (z) {
                            m939();
                            int i7 = scrollY + i4;
                            if (i7 < 0) {
                                C0164.m1021(this.f543, ((float) i4) / ((float) getHeight()), motionEvent2.getX(i5) / ((float) getWidth()));
                                if (!this.f544.isFinished()) {
                                    this.f544.onRelease();
                                }
                            } else {
                                int i8 = i5;
                                if (i7 > i3) {
                                    C0164.m1021(this.f544, ((float) i4) / ((float) getHeight()), 1.0f - (motionEvent2.getX(i8) / ((float) getWidth())));
                                    if (!this.f543.isFinished()) {
                                        this.f543.onRelease();
                                    }
                                }
                            }
                            EdgeEffect edgeEffect = this.f543;
                            if (edgeEffect != null && (!edgeEffect.isFinished() || !this.f544.isFinished())) {
                                C0414.m2233(this);
                            }
                        }
                    }
                }
            } else if (actionMasked == 3) {
                if (this.f549 && getChildCount() > 0 && this.f542.springBack(getScrollX(), getScrollY(), 0, 0, 0, getScrollRange())) {
                    C0414.m2233(this);
                }
                this.f556 = -1;
                m938();
            } else if (actionMasked == 5) {
                int actionIndex = motionEvent.getActionIndex();
                this.f545 = (int) motionEvent2.getY(actionIndex);
                this.f556 = motionEvent2.getPointerId(actionIndex);
            } else if (actionMasked == 6) {
                m925(motionEvent);
                this.f545 = (int) motionEvent2.getY(motionEvent2.findPointerIndex(this.f556));
            }
        } else if (getChildCount() == 0) {
            return false;
        } else {
            boolean z2 = !this.f542.isFinished();
            this.f549 = z2;
            if (z2 && (parent = getParent()) != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
            if (!this.f542.isFinished()) {
                this.f542.abortAnimation();
            }
            this.f545 = (int) motionEvent.getY();
            this.f556 = motionEvent2.getPointerId(0);
            m944(2, 0);
        }
        VelocityTracker velocityTracker2 = this.f550;
        if (velocityTracker2 != null) {
            velocityTracker2.addMovement(obtain);
        }
        obtain.recycle();
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m925(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.f556) {
            int i = actionIndex == 0 ? 1 : 0;
            this.f545 = (int) motionEvent.getY(i);
            this.f556 = motionEvent.getPointerId(i);
            VelocityTracker velocityTracker = this.f550;
            if (velocityTracker != null) {
                velocityTracker.clear();
            }
        }
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        if ((motionEvent.getSource() & 2) != 0 && motionEvent.getAction() == 8 && !this.f549) {
            float axisValue = motionEvent.getAxisValue(9);
            if (axisValue != 0.0f) {
                int scrollRange = getScrollRange();
                int scrollY = getScrollY();
                int verticalScrollFactorCompat = scrollY - ((int) (axisValue * getVerticalScrollFactorCompat()));
                if (verticalScrollFactorCompat < 0) {
                    verticalScrollFactorCompat = 0;
                } else if (verticalScrollFactorCompat > scrollRange) {
                    verticalScrollFactorCompat = scrollRange;
                }
                if (verticalScrollFactorCompat != scrollY) {
                    super.scrollTo(getScrollX(), verticalScrollFactorCompat);
                    return true;
                }
            }
        }
        return false;
    }

    private float getVerticalScrollFactorCompat() {
        if (this.f557 == 0.0f) {
            TypedValue typedValue = new TypedValue();
            Context context = getContext();
            if (context.getTheme().resolveAttribute(16842829, typedValue, true)) {
                this.f557 = typedValue.getDimension(context.getResources().getDisplayMetrics());
            } else {
                throw new IllegalStateException("Expected theme to define listPreferredItemHeight.");
            }
        }
        return this.f557;
    }

    /* access modifiers changed from: protected */
    public void onOverScrolled(int i, int i2, boolean z, boolean z2) {
        super.scrollTo(i, i2);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0083 A[ADDED_TO_REGION] */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean m945(int r13, int r14, int r15, int r16, int r17, int r18, int r19, int r20, boolean r21) {
        /*
            r12 = this;
            r0 = r12
            int r1 = r12.getOverScrollMode()
            int r2 = r12.computeHorizontalScrollRange()
            int r3 = r12.computeHorizontalScrollExtent()
            r4 = 0
            r5 = 1
            if (r2 <= r3) goto L_0x0013
            r2 = 1
            goto L_0x0014
        L_0x0013:
            r2 = 0
        L_0x0014:
            int r3 = r12.computeVerticalScrollRange()
            int r6 = r12.computeVerticalScrollExtent()
            if (r3 <= r6) goto L_0x0020
            r3 = 1
            goto L_0x0021
        L_0x0020:
            r3 = 0
        L_0x0021:
            if (r1 == 0) goto L_0x002a
            if (r1 != r5) goto L_0x0028
            if (r2 == 0) goto L_0x0028
            goto L_0x002a
        L_0x0028:
            r2 = 0
            goto L_0x002b
        L_0x002a:
            r2 = 1
        L_0x002b:
            if (r1 == 0) goto L_0x0034
            if (r1 != r5) goto L_0x0032
            if (r3 == 0) goto L_0x0032
            goto L_0x0034
        L_0x0032:
            r1 = 0
            goto L_0x0035
        L_0x0034:
            r1 = 1
        L_0x0035:
            int r3 = r15 + r13
            if (r2 != 0) goto L_0x003b
            r2 = 0
            goto L_0x003d
        L_0x003b:
            r2 = r19
        L_0x003d:
            int r6 = r16 + r14
            if (r1 != 0) goto L_0x0043
            r1 = 0
            goto L_0x0045
        L_0x0043:
            r1 = r20
        L_0x0045:
            int r7 = -r2
            int r2 = r2 + r17
            int r8 = -r1
            int r1 = r1 + r18
            if (r3 <= r2) goto L_0x0050
            r7 = r2
        L_0x004e:
            r2 = 1
            goto L_0x0055
        L_0x0050:
            if (r3 >= r7) goto L_0x0053
            goto L_0x004e
        L_0x0053:
            r7 = r3
            r2 = 0
        L_0x0055:
            if (r6 <= r1) goto L_0x005a
            r6 = r1
        L_0x0058:
            r1 = 1
            goto L_0x005f
        L_0x005a:
            if (r6 >= r8) goto L_0x005e
            r6 = r8
            goto L_0x0058
        L_0x005e:
            r1 = 0
        L_0x005f:
            if (r1 == 0) goto L_0x007e
            boolean r3 = r12.m950(r5)
            if (r3 != 0) goto L_0x007e
            android.widget.OverScroller r3 = r0.f542
            r8 = 0
            r9 = 0
            r10 = 0
            int r11 = r12.getScrollRange()
            r13 = r3
            r14 = r7
            r15 = r6
            r16 = r8
            r17 = r9
            r18 = r10
            r19 = r11
            r13.springBack(r14, r15, r16, r17, r18, r19)
        L_0x007e:
            r12.onOverScrolled(r7, r6, r2, r1)
            if (r2 != 0) goto L_0x0085
            if (r1 == 0) goto L_0x0086
        L_0x0085:
            r4 = 1
        L_0x0086:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.NestedScrollView.m945(int, int, int, int, int, int, int, int, boolean):boolean");
    }

    /* access modifiers changed from: package-private */
    public int getScrollRange() {
        if (getChildCount() > 0) {
            return Math.max(0, getChildAt(0).getHeight() - ((getHeight() - getPaddingBottom()) - getPaddingTop()));
        }
        return 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private View m923(boolean z, int i, int i2) {
        ArrayList focusables = getFocusables(2);
        int size = focusables.size();
        View view = null;
        boolean z2 = false;
        for (int i3 = 0; i3 < size; i3++) {
            View view2 = (View) focusables.get(i3);
            int top = view2.getTop();
            int bottom = view2.getBottom();
            if (i < bottom && top < i2) {
                boolean z3 = i < top && bottom < i2;
                if (view == null) {
                    view = view2;
                    z2 = z3;
                } else {
                    boolean z4 = (z && top < view.getTop()) || (!z && bottom > view.getBottom());
                    if (z2) {
                        if (z3) {
                            if (!z4) {
                            }
                        }
                    } else if (z3) {
                        view = view2;
                        z2 = true;
                    } else if (!z4) {
                    }
                    view = view2;
                }
            }
        }
        return view;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m952(int i) {
        boolean z = i == 130;
        int height = getHeight();
        if (z) {
            this.f541.top = getScrollY() + height;
            int childCount = getChildCount();
            if (childCount > 0) {
                View childAt = getChildAt(childCount - 1);
                if (this.f541.top + height > childAt.getBottom()) {
                    this.f541.top = childAt.getBottom() - height;
                }
            }
        } else {
            this.f541.top = getScrollY() - height;
            if (this.f541.top < 0) {
                this.f541.top = 0;
            }
        }
        Rect rect = this.f541;
        rect.bottom = rect.top + height;
        return m926(i, this.f541.top, this.f541.bottom);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m953(int i) {
        int childCount;
        boolean z = i == 130;
        int height = getHeight();
        Rect rect = this.f541;
        rect.top = 0;
        rect.bottom = height;
        if (z && (childCount = getChildCount()) > 0) {
            this.f541.bottom = getChildAt(childCount - 1).getBottom() + getPaddingBottom();
            Rect rect2 = this.f541;
            rect2.top = rect2.bottom - height;
        }
        return m926(i, this.f541.top, this.f541.bottom);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m926(int i, int i2, int i3) {
        int height = getHeight();
        int scrollY = getScrollY();
        int i4 = height + scrollY;
        boolean z = false;
        boolean z2 = i == 33;
        View r5 = m923(z2, i2, i3);
        if (r5 == null) {
            r5 = this;
        }
        if (i2 < scrollY || i3 > i4) {
            m940(z2 ? i2 - scrollY : i3 - i4);
            z = true;
        }
        if (r5 != findFocus()) {
            r5.requestFocus(i);
        }
        return z;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m954(int i) {
        int bottom;
        View findFocus = findFocus();
        if (findFocus == this) {
            findFocus = null;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, i);
        int maxScrollAmount = getMaxScrollAmount();
        if (findNextFocus == null || !m929(findNextFocus, maxScrollAmount, getHeight())) {
            if (i == 33 && getScrollY() < maxScrollAmount) {
                maxScrollAmount = getScrollY();
            } else if (i == 130 && getChildCount() > 0 && (bottom = getChildAt(0).getBottom() - ((getScrollY() + getHeight()) - getPaddingBottom())) < maxScrollAmount) {
                maxScrollAmount = bottom;
            }
            if (maxScrollAmount == 0) {
                return false;
            }
            if (i != 130) {
                maxScrollAmount = -maxScrollAmount;
            }
            m940(maxScrollAmount);
        } else {
            findNextFocus.getDrawingRect(this.f541);
            offsetDescendantRectToMyCoords(findNextFocus, this.f541);
            m940(m942(this.f541));
            findNextFocus.requestFocus(i);
        }
        if (findFocus == null || !findFocus.isFocused() || !m928(findFocus)) {
            return true;
        }
        int descendantFocusability = getDescendantFocusability();
        setDescendantFocusability(131072);
        requestFocus();
        setDescendantFocusability(descendantFocusability);
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m928(View view) {
        return !m929(view, 0, getHeight());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m929(View view, int i, int i2) {
        view.getDrawingRect(this.f541);
        offsetDescendantRectToMyCoords(view, this.f541);
        return this.f541.bottom + i >= getScrollY() && this.f541.top - i <= getScrollY() + i2;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private void m940(int i) {
        if (i == 0) {
            return;
        }
        if (this.f552) {
            m949(0, i);
        } else {
            scrollBy(0, i);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m949(int i, int i2) {
        if (getChildCount() != 0) {
            if (AnimationUtils.currentAnimationTimeMillis() - this.f540 > 250) {
                int max = Math.max(0, getChildAt(0).getHeight() - ((getHeight() - getPaddingBottom()) - getPaddingTop()));
                int scrollY = getScrollY();
                this.f542.startScroll(getScrollX(), scrollY, 0, Math.max(0, Math.min(i2 + scrollY, max)) - scrollY);
                C0414.m2233(this);
            } else {
                if (!this.f542.isFinished()) {
                    this.f542.abortAnimation();
                }
                scrollBy(i, i2);
            }
            this.f540 = AnimationUtils.currentAnimationTimeMillis();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m951(int i, int i2) {
        m949(i - getScrollX(), i2 - getScrollY());
    }

    public int computeVerticalScrollRange() {
        int childCount = getChildCount();
        int height = (getHeight() - getPaddingBottom()) - getPaddingTop();
        if (childCount == 0) {
            return height;
        }
        int bottom = getChildAt(0).getBottom();
        int scrollY = getScrollY();
        int max = Math.max(0, bottom - height);
        if (scrollY < 0) {
            return bottom - scrollY;
        }
        return scrollY > max ? bottom + (scrollY - max) : bottom;
    }

    public int computeVerticalScrollOffset() {
        return Math.max(0, super.computeVerticalScrollOffset());
    }

    public int computeVerticalScrollExtent() {
        return super.computeVerticalScrollExtent();
    }

    public int computeHorizontalScrollRange() {
        return super.computeHorizontalScrollRange();
    }

    public int computeHorizontalScrollOffset() {
        return super.computeHorizontalScrollOffset();
    }

    public int computeHorizontalScrollExtent() {
        return super.computeHorizontalScrollExtent();
    }

    /* access modifiers changed from: protected */
    public void measureChild(View view, int i, int i2) {
        view.measure(getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight(), view.getLayoutParams().width), View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    /* access modifiers changed from: protected */
    public void measureChildWithMargins(View view, int i, int i2, int i3, int i4) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        view.measure(getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + i2, marginLayoutParams.width), View.MeasureSpec.makeMeasureSpec(marginLayoutParams.topMargin + marginLayoutParams.bottomMargin, 0));
    }

    public void computeScroll() {
        if (this.f542.computeScrollOffset()) {
            this.f542.getCurrX();
            int currY = this.f542.getCurrY();
            int i = currY - this.f562;
            if (m947(0, i, this.f560, null, 1)) {
                i -= this.f560[1];
            }
            int i2 = i;
            if (i2 != 0) {
                int scrollRange = getScrollRange();
                int scrollY = getScrollY();
                int i3 = scrollY;
                m945(0, i2, getScrollX(), scrollY, 0, scrollRange, 0, 0, false);
                int scrollY2 = getScrollY() - i3;
                if (!m946(0, scrollY2, 0, i2 - scrollY2, null, 1)) {
                    int overScrollMode = getOverScrollMode();
                    if (overScrollMode == 0 || (overScrollMode == 1 && scrollRange > 0)) {
                        m939();
                        if (currY <= 0 && i3 > 0) {
                            this.f543.onAbsorb((int) this.f542.getCurrVelocity());
                        } else if (currY >= scrollRange && i3 < scrollRange) {
                            this.f544.onAbsorb((int) this.f542.getCurrVelocity());
                        }
                    }
                }
            }
            this.f562 = currY;
            C0414.m2233(this);
            return;
        }
        if (m950(1)) {
            m943(1);
        }
        this.f562 = 0;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m932(View view) {
        view.getDrawingRect(this.f541);
        offsetDescendantRectToMyCoords(view, this.f541);
        int r2 = m942(this.f541);
        if (r2 != 0) {
            scrollBy(0, r2);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m927(Rect rect, boolean z) {
        int r3 = m942(rect);
        boolean z2 = r3 != 0;
        if (z2) {
            if (z) {
                scrollBy(0, r3);
            } else {
                m949(0, r3);
            }
        }
        return z2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m942(Rect rect) {
        int i;
        int i2;
        if (getChildCount() == 0) {
            return 0;
        }
        int height = getHeight();
        int scrollY = getScrollY();
        int i3 = scrollY + height;
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        if (rect.top > 0) {
            scrollY += verticalFadingEdgeLength;
        }
        if (rect.bottom < getChildAt(0).getHeight()) {
            i3 -= verticalFadingEdgeLength;
        }
        if (rect.bottom > i3 && rect.top > scrollY) {
            if (rect.height() > height) {
                i2 = rect.top - scrollY;
            } else {
                i2 = rect.bottom - i3;
            }
            return Math.min(i2 + 0, getChildAt(0).getBottom() - i3);
        } else if (rect.top >= scrollY || rect.bottom >= i3) {
            return 0;
        } else {
            if (rect.height() > height) {
                i = 0 - (i3 - rect.bottom);
            } else {
                i = 0 - (scrollY - rect.top);
            }
            return Math.max(i, -getScrollY());
        }
    }

    public void requestChildFocus(View view, View view2) {
        if (!this.f546) {
            m932(view2);
        } else {
            this.f548 = view2;
        }
        super.requestChildFocus(view, view2);
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i, Rect rect) {
        View view;
        if (i == 2) {
            i = 130;
        } else if (i == 1) {
            i = 33;
        }
        if (rect == null) {
            view = FocusFinder.getInstance().findNextFocus(this, null, i);
        } else {
            view = FocusFinder.getInstance().findNextFocusFromRect(this, rect, i);
        }
        if (view != null && !m928(view)) {
            return view.requestFocus(i, rect);
        }
        return false;
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        rect.offset(view.getLeft() - view.getScrollX(), view.getTop() - view.getScrollY());
        return m927(rect, z);
    }

    public void requestLayout() {
        this.f546 = true;
        super.requestLayout();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.f546 = false;
        View view = this.f548;
        if (view != null && m930(view, this)) {
            m932(this.f548);
        }
        this.f548 = null;
        if (!this.f547) {
            if (this.f563 != null) {
                scrollTo(getScrollX(), this.f563.f566);
                this.f563 = null;
            }
            int max = Math.max(0, (getChildCount() > 0 ? getChildAt(0).getMeasuredHeight() : 0) - (((i4 - i2) - getPaddingBottom()) - getPaddingTop()));
            if (getScrollY() > max) {
                scrollTo(getScrollX(), max);
            } else if (getScrollY() < 0) {
                scrollTo(getScrollX(), 0);
            }
        }
        scrollTo(getScrollX(), getScrollY());
        this.f547 = true;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f547 = false;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        View findFocus = findFocus();
        if (findFocus != null && this != findFocus && m929(findFocus, 0, i4)) {
            findFocus.getDrawingRect(this.f541);
            offsetDescendantRectToMyCoords(findFocus, this.f541);
            m940(m942(this.f541));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean m930(View view, View view2) {
        if (view == view2) {
            return true;
        }
        ViewParent parent = view.getParent();
        if (!(parent instanceof ViewGroup) || !m930((View) parent, view2)) {
            return false;
        }
        return true;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m955(int i) {
        if (getChildCount() > 0) {
            m944(2, 1);
            this.f542.fling(getScrollX(), getScrollY(), 0, i, 0, 0, Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 0);
            this.f562 = getScrollY();
            C0414.m2233(this);
        }
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private void m941(int i) {
        int scrollY = getScrollY();
        boolean z = (scrollY > 0 || i > 0) && (scrollY < getScrollRange() || i < 0);
        float f = (float) i;
        if (!dispatchNestedPreFling(0.0f, f)) {
            dispatchNestedFling(0.0f, f, z);
            m955(i);
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private void m938() {
        this.f549 = false;
        m937();
        m943(0);
        EdgeEffect edgeEffect = this.f543;
        if (edgeEffect != null) {
            edgeEffect.onRelease();
            this.f544.onRelease();
        }
    }

    public void scrollTo(int i, int i2) {
        if (getChildCount() > 0) {
            View childAt = getChildAt(0);
            int r4 = m931(i, (getWidth() - getPaddingRight()) - getPaddingLeft(), childAt.getWidth());
            int r5 = m931(i2, (getHeight() - getPaddingBottom()) - getPaddingTop(), childAt.getHeight());
            if (r4 != getScrollX() || r5 != getScrollY()) {
                super.scrollTo(r4, r5);
            }
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private void m939() {
        if (getOverScrollMode() == 2) {
            this.f543 = null;
            this.f544 = null;
        } else if (this.f543 == null) {
            Context context = getContext();
            this.f543 = new EdgeEffect(context);
            this.f544 = new EdgeEffect(context);
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.f543 != null) {
            int scrollY = getScrollY();
            if (!this.f543.isFinished()) {
                int save = canvas.save();
                int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
                canvas.translate((float) getPaddingLeft(), (float) Math.min(0, scrollY));
                this.f543.setSize(width, getHeight());
                if (this.f543.draw(canvas)) {
                    C0414.m2233(this);
                }
                canvas.restoreToCount(save);
            }
            if (!this.f544.isFinished()) {
                int save2 = canvas.save();
                int width2 = (getWidth() - getPaddingLeft()) - getPaddingRight();
                int height = getHeight();
                canvas.translate((float) ((-width2) + getPaddingLeft()), (float) (Math.max(getScrollRange(), scrollY) + height));
                canvas.rotate(180.0f, (float) width2, 0.0f);
                this.f544.setSize(width2, height);
                if (this.f544.draw(canvas)) {
                    C0414.m2233(this);
                }
                canvas.restoreToCount(save2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof C0151)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        C0151 r2 = (C0151) parcelable;
        super.onRestoreInstanceState(r2.getSuperState());
        this.f563 = r2;
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        C0151 r1 = new C0151(super.onSaveInstanceState());
        r1.f566 = getScrollY();
        return r1;
    }

    /* renamed from: android.support.v4.widget.NestedScrollView$ʽ  reason: contains not printable characters */
    static class C0151 extends View.BaseSavedState {
        public static final Parcelable.Creator<C0151> CREATOR = new Parcelable.Creator<C0151>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C0151 createFromParcel(Parcel parcel) {
                return new C0151(parcel);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0151[] newArray(int i) {
                return new C0151[i];
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        public int f566;

        C0151(Parcelable parcelable) {
            super(parcelable);
        }

        C0151(Parcel parcel) {
            super(parcel);
            this.f566 = parcel.readInt();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f566);
        }

        public String toString() {
            return "HorizontalScrollView.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " scrollPosition=" + this.f566 + "}";
        }
    }

    /* renamed from: android.support.v4.widget.NestedScrollView$ʻ  reason: contains not printable characters */
    static class C0149 extends C0386 {
        C0149() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m958(View view, int i, Bundle bundle) {
            if (super.m2131(view, i, bundle)) {
                return true;
            }
            NestedScrollView nestedScrollView = (NestedScrollView) view;
            if (!nestedScrollView.isEnabled()) {
                return false;
            }
            if (i == 4096) {
                int min = Math.min(nestedScrollView.getScrollY() + ((nestedScrollView.getHeight() - nestedScrollView.getPaddingBottom()) - nestedScrollView.getPaddingTop()), nestedScrollView.getScrollRange());
                if (min == nestedScrollView.getScrollY()) {
                    return false;
                }
                nestedScrollView.m951(0, min);
                return true;
            } else if (i != 8192) {
                return false;
            } else {
                int max = Math.max(nestedScrollView.getScrollY() - ((nestedScrollView.getHeight() - nestedScrollView.getPaddingBottom()) - nestedScrollView.getPaddingTop()), 0);
                if (max == nestedScrollView.getScrollY()) {
                    return false;
                }
                nestedScrollView.m951(0, max);
                return true;
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m956(View view, C0360 r4) {
            int scrollRange;
            super.m2129(view, r4);
            NestedScrollView nestedScrollView = (NestedScrollView) view;
            r4.m2019((CharSequence) ScrollView.class.getName());
            if (nestedScrollView.isEnabled() && (scrollRange = nestedScrollView.getScrollRange()) > 0) {
                r4.m2042(true);
                if (nestedScrollView.getScrollY() > 0) {
                    r4.m2009(8192);
                }
                if (nestedScrollView.getScrollY() < scrollRange) {
                    r4.m2009(4096);
                }
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m957(View view, AccessibilityEvent accessibilityEvent) {
            super.m2130(view, accessibilityEvent);
            NestedScrollView nestedScrollView = (NestedScrollView) view;
            accessibilityEvent.setClassName(ScrollView.class.getName());
            accessibilityEvent.setScrollable(nestedScrollView.getScrollRange() > 0);
            accessibilityEvent.setScrollX(nestedScrollView.getScrollX());
            accessibilityEvent.setScrollY(nestedScrollView.getScrollY());
            C0382.m2120(accessibilityEvent, nestedScrollView.getScrollX());
            C0382.m2121(accessibilityEvent, nestedScrollView.getScrollRange());
        }
    }
}
