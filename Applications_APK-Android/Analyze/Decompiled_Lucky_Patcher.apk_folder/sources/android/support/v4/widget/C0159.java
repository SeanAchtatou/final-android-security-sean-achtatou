package android.support.v4.widget;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.support.v4.widget.C0162;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.FilterQueryProvider;
import android.widget.Filterable;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

/* renamed from: android.support.v4.widget.ʽ  reason: contains not printable characters */
/* compiled from: CursorAdapter */
public abstract class C0159 extends BaseAdapter implements C0162.C0163, Filterable {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected boolean f600;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected boolean f601;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected Cursor f602;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected Context f603;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected int f604;

    /* renamed from: ˆ  reason: contains not printable characters */
    protected C0160 f605;

    /* renamed from: ˈ  reason: contains not printable characters */
    protected DataSetObserver f606;

    /* renamed from: ˉ  reason: contains not printable characters */
    protected C0162 f607;

    /* renamed from: ˊ  reason: contains not printable characters */
    protected FilterQueryProvider f608;

    public boolean hasStableIds() {
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract View m1009(Context context, Cursor cursor, ViewGroup viewGroup);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m1012(View view, Context context, Cursor cursor);

    public C0159(Context context, Cursor cursor, boolean z) {
        m1010(context, cursor, z ? 1 : 2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1010(Context context, Cursor cursor, int i) {
        boolean z = false;
        if ((i & 1) == 1) {
            i |= 2;
            this.f601 = true;
        } else {
            this.f601 = false;
        }
        if (cursor != null) {
            z = true;
        }
        this.f602 = cursor;
        this.f600 = z;
        this.f603 = context;
        this.f604 = z ? cursor.getColumnIndexOrThrow("_id") : -1;
        if ((i & 2) == 2) {
            this.f605 = new C0160();
            this.f606 = new C0161();
        } else {
            this.f605 = null;
            this.f606 = null;
        }
        if (z) {
            C0160 r4 = this.f605;
            if (r4 != null) {
                cursor.registerContentObserver(r4);
            }
            DataSetObserver dataSetObserver = this.f606;
            if (dataSetObserver != null) {
                cursor.registerDataSetObserver(dataSetObserver);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Cursor m1007() {
        return this.f602;
    }

    public int getCount() {
        Cursor cursor;
        if (!this.f600 || (cursor = this.f602) == null) {
            return 0;
        }
        return cursor.getCount();
    }

    public Object getItem(int i) {
        Cursor cursor;
        if (!this.f600 || (cursor = this.f602) == null) {
            return null;
        }
        cursor.moveToPosition(i);
        return this.f602;
    }

    public long getItemId(int i) {
        Cursor cursor;
        if (!this.f600 || (cursor = this.f602) == null || !cursor.moveToPosition(i)) {
            return 0;
        }
        return this.f602.getLong(this.f604);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (!this.f600) {
            throw new IllegalStateException("this should only be called when the cursor is valid");
        } else if (this.f602.moveToPosition(i)) {
            if (view == null) {
                view = m1009(this.f603, this.f602, viewGroup);
            }
            m1012(view, this.f603, this.f602);
            return view;
        } else {
            throw new IllegalStateException("couldn't move cursor to position " + i);
        }
    }

    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        if (!this.f600) {
            return null;
        }
        this.f602.moveToPosition(i);
        if (view == null) {
            view = m1014(this.f603, this.f602, viewGroup);
        }
        m1012(view, this.f603, this.f602);
        return view;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public View m1014(Context context, Cursor cursor, ViewGroup viewGroup) {
        return m1009(context, cursor, viewGroup);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1011(Cursor cursor) {
        Cursor r1 = m1013(cursor);
        if (r1 != null) {
            r1.close();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Cursor m1013(Cursor cursor) {
        Cursor cursor2 = this.f602;
        if (cursor == cursor2) {
            return null;
        }
        if (cursor2 != null) {
            C0160 r1 = this.f605;
            if (r1 != null) {
                cursor2.unregisterContentObserver(r1);
            }
            DataSetObserver dataSetObserver = this.f606;
            if (dataSetObserver != null) {
                cursor2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.f602 = cursor;
        if (cursor != null) {
            C0160 r12 = this.f605;
            if (r12 != null) {
                cursor.registerContentObserver(r12);
            }
            DataSetObserver dataSetObserver2 = this.f606;
            if (dataSetObserver2 != null) {
                cursor.registerDataSetObserver(dataSetObserver2);
            }
            this.f604 = cursor.getColumnIndexOrThrow("_id");
            this.f600 = true;
            notifyDataSetChanged();
        } else {
            this.f604 = -1;
            this.f600 = false;
            notifyDataSetInvalidated();
        }
        return cursor2;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public CharSequence m1016(Cursor cursor) {
        return cursor == null ? BuildConfig.FLAVOR : cursor.toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Cursor m1008(CharSequence charSequence) {
        FilterQueryProvider filterQueryProvider = this.f608;
        if (filterQueryProvider != null) {
            return filterQueryProvider.runQuery(charSequence);
        }
        return this.f602;
    }

    public Filter getFilter() {
        if (this.f607 == null) {
            this.f607 = new C0162(this);
        }
        return this.f607;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1015() {
        Cursor cursor;
        if (this.f601 && (cursor = this.f602) != null && !cursor.isClosed()) {
            this.f600 = this.f602.requery();
        }
    }

    /* renamed from: android.support.v4.widget.ʽ$ʻ  reason: contains not printable characters */
    /* compiled from: CursorAdapter */
    private class C0160 extends ContentObserver {
        public boolean deliverSelfNotifications() {
            return true;
        }

        C0160() {
            super(new Handler());
        }

        public void onChange(boolean z) {
            C0159.this.m1015();
        }
    }

    /* renamed from: android.support.v4.widget.ʽ$ʼ  reason: contains not printable characters */
    /* compiled from: CursorAdapter */
    private class C0161 extends DataSetObserver {
        C0161() {
        }

        public void onChanged() {
            C0159 r0 = C0159.this;
            r0.f600 = true;
            r0.notifyDataSetChanged();
        }

        public void onInvalidated() {
            C0159 r0 = C0159.this;
            r0.f600 = false;
            r0.notifyDataSetInvalidated();
        }
    }
}
