package android.support.v4.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.ImageView;

/* renamed from: android.support.v4.widget.ˆ  reason: contains not printable characters */
/* compiled from: ImageViewCompat */
public class C0167 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0169 f613;

    /* renamed from: android.support.v4.widget.ˆ$ʼ  reason: contains not printable characters */
    /* compiled from: ImageViewCompat */
    interface C0169 {
        /* renamed from: ʻ  reason: contains not printable characters */
        ColorStateList m1032(ImageView imageView);

        /* renamed from: ʻ  reason: contains not printable characters */
        void m1033(ImageView imageView, ColorStateList colorStateList);

        /* renamed from: ʻ  reason: contains not printable characters */
        void m1034(ImageView imageView, PorterDuff.Mode mode);

        /* renamed from: ʼ  reason: contains not printable characters */
        PorterDuff.Mode m1035(ImageView imageView);
    }

    /* renamed from: android.support.v4.widget.ˆ$ʻ  reason: contains not printable characters */
    /* compiled from: ImageViewCompat */
    static class C0168 implements C0169 {
        C0168() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public ColorStateList m1028(ImageView imageView) {
            if (imageView instanceof C0187) {
                return ((C0187) imageView).getSupportImageTintList();
            }
            return null;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1029(ImageView imageView, ColorStateList colorStateList) {
            if (imageView instanceof C0187) {
                ((C0187) imageView).setSupportImageTintList(colorStateList);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1030(ImageView imageView, PorterDuff.Mode mode) {
            if (imageView instanceof C0187) {
                ((C0187) imageView).setSupportImageTintMode(mode);
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public PorterDuff.Mode m1031(ImageView imageView) {
            if (imageView instanceof C0187) {
                return ((C0187) imageView).getSupportImageTintMode();
            }
            return null;
        }
    }

    /* renamed from: android.support.v4.widget.ˆ$ʽ  reason: contains not printable characters */
    /* compiled from: ImageViewCompat */
    static class C0170 extends C0168 {
        C0170() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public ColorStateList m1036(ImageView imageView) {
            return imageView.getImageTintList();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1037(ImageView imageView, ColorStateList colorStateList) {
            imageView.setImageTintList(colorStateList);
            if (Build.VERSION.SDK_INT == 21) {
                Drawable drawable = imageView.getDrawable();
                boolean z = (imageView.getImageTintList() == null || imageView.getImageTintMode() == null) ? false : true;
                if (drawable != null && z) {
                    if (drawable.isStateful()) {
                        drawable.setState(imageView.getDrawableState());
                    }
                    imageView.setImageDrawable(drawable);
                }
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1038(ImageView imageView, PorterDuff.Mode mode) {
            imageView.setImageTintMode(mode);
            if (Build.VERSION.SDK_INT == 21) {
                Drawable drawable = imageView.getDrawable();
                boolean z = (imageView.getImageTintList() == null || imageView.getImageTintMode() == null) ? false : true;
                if (drawable != null && z) {
                    if (drawable.isStateful()) {
                        drawable.setState(imageView.getDrawableState());
                    }
                    imageView.setImageDrawable(drawable);
                }
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public PorterDuff.Mode m1039(ImageView imageView) {
            return imageView.getImageTintMode();
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 21) {
            f613 = new C0170();
        } else {
            f613 = new C0168();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static ColorStateList m1024(ImageView imageView) {
        return f613.m1032(imageView);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1025(ImageView imageView, ColorStateList colorStateList) {
        f613.m1033(imageView, colorStateList);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static PorterDuff.Mode m1027(ImageView imageView) {
        return f613.m1035(imageView);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1026(ImageView imageView, PorterDuff.Mode mode) {
        f613.m1034(imageView, mode);
    }
}
