package android.support.v4.widget;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: android.support.v4.widget.ˋ  reason: contains not printable characters */
/* compiled from: ResourceCursorAdapter */
public abstract class C0178 extends C0159 {

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f619;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f620;

    /* renamed from: ˏ  reason: contains not printable characters */
    private LayoutInflater f621;

    @Deprecated
    public C0178(Context context, int i, Cursor cursor, boolean z) {
        super(context, cursor, z);
        this.f620 = i;
        this.f619 = i;
        this.f621 = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ʻ  reason: contains not printable characters */
    public View m1054(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.f621.inflate(this.f619, viewGroup, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ʼ  reason: contains not printable characters */
    public View m1055(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.f621.inflate(this.f620, viewGroup, false);
    }
}
