package android.support.v4.widget;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.TextView;

/* renamed from: android.support.v4.widget.ˎ  reason: contains not printable characters */
/* compiled from: TextViewCompat */
public final class C0179 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0185 f622;

    /* renamed from: android.support.v4.widget.ˎ$ˆ  reason: contains not printable characters */
    /* compiled from: TextViewCompat */
    static class C0185 {
        C0185() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1062(TextView textView, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
            textView.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1061(TextView textView, int i) {
            textView.setTextAppearance(textView.getContext(), i);
        }
    }

    /* renamed from: android.support.v4.widget.ˎ$ʻ  reason: contains not printable characters */
    /* compiled from: TextViewCompat */
    static class C0180 extends C0185 {
        C0180() {
        }
    }

    /* renamed from: android.support.v4.widget.ˎ$ʼ  reason: contains not printable characters */
    /* compiled from: TextViewCompat */
    static class C0181 extends C0180 {
        C0181() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1058(TextView textView, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
            boolean z = true;
            if (textView.getLayoutDirection() != 1) {
                z = false;
            }
            Drawable drawable5 = z ? drawable3 : drawable;
            if (!z) {
                drawable = drawable3;
            }
            textView.setCompoundDrawables(drawable5, drawable2, drawable, drawable4);
        }
    }

    /* renamed from: android.support.v4.widget.ˎ$ʽ  reason: contains not printable characters */
    /* compiled from: TextViewCompat */
    static class C0182 extends C0181 {
        C0182() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1059(TextView textView, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
            textView.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        }
    }

    /* renamed from: android.support.v4.widget.ˎ$ʾ  reason: contains not printable characters */
    /* compiled from: TextViewCompat */
    static class C0183 extends C0182 {
        C0183() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1060(TextView textView, int i) {
            textView.setTextAppearance(i);
        }
    }

    /* renamed from: android.support.v4.widget.ˎ$ʿ  reason: contains not printable characters */
    /* compiled from: TextViewCompat */
    static class C0184 extends C0183 {
        C0184() {
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 26) {
            f622 = new C0184();
        } else if (Build.VERSION.SDK_INT >= 23) {
            f622 = new C0183();
        } else if (Build.VERSION.SDK_INT >= 18) {
            f622 = new C0182();
        } else if (Build.VERSION.SDK_INT >= 17) {
            f622 = new C0181();
        } else if (Build.VERSION.SDK_INT >= 16) {
            f622 = new C0180();
        } else {
            f622 = new C0185();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1057(TextView textView, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        f622.m1062(textView, drawable, drawable2, drawable3, drawable4);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1056(TextView textView, int i) {
        f622.m1061(textView, i);
    }
}
