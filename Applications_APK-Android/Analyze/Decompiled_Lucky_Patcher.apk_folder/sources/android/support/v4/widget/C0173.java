package android.support.v4.widget;

import android.os.Build;
import android.support.v4.ˉ.C0396;
import android.support.v4.ˉ.C0414;
import android.util.Log;
import android.view.View;
import android.widget.PopupWindow;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/* renamed from: android.support.v4.widget.ˊ  reason: contains not printable characters */
/* compiled from: PopupWindowCompat */
public final class C0173 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0177 f615;

    /* renamed from: android.support.v4.widget.ˊ$ʾ  reason: contains not printable characters */
    /* compiled from: PopupWindowCompat */
    static class C0177 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private static Method f617;

        /* renamed from: ʼ  reason: contains not printable characters */
        private static boolean f618;

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1053(PopupWindow popupWindow, boolean z) {
        }

        C0177() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1052(PopupWindow popupWindow, View view, int i, int i2, int i3) {
            if ((C0396.m2155(i3, C0414.m2236(view)) & 7) == 5) {
                i -= popupWindow.getWidth() - view.getWidth();
            }
            popupWindow.showAsDropDown(view, i, i2);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1051(PopupWindow popupWindow, int i) {
            if (!f618) {
                Class<PopupWindow> cls = PopupWindow.class;
                try {
                    f617 = cls.getDeclaredMethod("setWindowLayoutType", Integer.TYPE);
                    f617.setAccessible(true);
                } catch (Exception unused) {
                }
                f618 = true;
            }
            Method method = f617;
            if (method != null) {
                try {
                    method.invoke(popupWindow, Integer.valueOf(i));
                } catch (Exception unused2) {
                }
            }
        }
    }

    /* renamed from: android.support.v4.widget.ˊ$ʻ  reason: contains not printable characters */
    /* compiled from: PopupWindowCompat */
    static class C0174 extends C0177 {
        C0174() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1047(PopupWindow popupWindow, View view, int i, int i2, int i3) {
            popupWindow.showAsDropDown(view, i, i2, i3);
        }
    }

    /* renamed from: android.support.v4.widget.ˊ$ʼ  reason: contains not printable characters */
    /* compiled from: PopupWindowCompat */
    static class C0175 extends C0174 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private static Field f616;

        C0175() {
        }

        static {
            try {
                f616 = PopupWindow.class.getDeclaredField("mOverlapAnchor");
                f616.setAccessible(true);
            } catch (NoSuchFieldException e) {
                Log.i("PopupWindowCompatApi21", "Could not fetch mOverlapAnchor field from PopupWindow", e);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1048(PopupWindow popupWindow, boolean z) {
            Field field = f616;
            if (field != null) {
                try {
                    field.set(popupWindow, Boolean.valueOf(z));
                } catch (IllegalAccessException e) {
                    Log.i("PopupWindowCompatApi21", "Could not set overlap anchor field in PopupWindow", e);
                }
            }
        }
    }

    /* renamed from: android.support.v4.widget.ˊ$ʽ  reason: contains not printable characters */
    /* compiled from: PopupWindowCompat */
    static class C0176 extends C0175 {
        C0176() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1050(PopupWindow popupWindow, boolean z) {
            popupWindow.setOverlapAnchor(z);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1049(PopupWindow popupWindow, int i) {
            popupWindow.setWindowLayoutType(i);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 23) {
            f615 = new C0176();
        } else if (Build.VERSION.SDK_INT >= 21) {
            f615 = new C0175();
        } else if (Build.VERSION.SDK_INT >= 19) {
            f615 = new C0174();
        } else {
            f615 = new C0177();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1045(PopupWindow popupWindow, View view, int i, int i2, int i3) {
        f615.m1052(popupWindow, view, i, i2, i3);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1046(PopupWindow popupWindow, boolean z) {
        f615.m1053(popupWindow, z);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1044(PopupWindow popupWindow, int i) {
        f615.m1051(popupWindow, i);
    }
}
