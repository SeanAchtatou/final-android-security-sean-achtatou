package android.support.v4.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.widget.CompoundButton;
import java.lang.reflect.Field;

/* renamed from: android.support.v4.widget.ʼ  reason: contains not printable characters */
/* compiled from: CompoundButtonCompat */
public final class C0155 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final C0158 f597;

    static {
        if (Build.VERSION.SDK_INT >= 23) {
            f597 = new C0157();
        } else if (Build.VERSION.SDK_INT >= 21) {
            f597 = new C0156();
        } else {
            f597 = new C0158();
        }
    }

    /* renamed from: android.support.v4.widget.ʼ$ʽ  reason: contains not printable characters */
    /* compiled from: CompoundButtonCompat */
    static class C0158 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private static Field f598;

        /* renamed from: ʼ  reason: contains not printable characters */
        private static boolean f599;

        C0158() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1005(CompoundButton compoundButton, ColorStateList colorStateList) {
            if (compoundButton instanceof C0186) {
                ((C0186) compoundButton).setSupportButtonTintList(colorStateList);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1006(CompoundButton compoundButton, PorterDuff.Mode mode) {
            if (compoundButton instanceof C0186) {
                ((C0186) compoundButton).setSupportButtonTintMode(mode);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Drawable m1004(CompoundButton compoundButton) {
            if (!f599) {
                try {
                    f598 = CompoundButton.class.getDeclaredField("mButtonDrawable");
                    f598.setAccessible(true);
                } catch (NoSuchFieldException e) {
                    Log.i("CompoundButtonCompat", "Failed to retrieve mButtonDrawable field", e);
                }
                f599 = true;
            }
            Field field = f598;
            if (field != null) {
                try {
                    return (Drawable) field.get(compoundButton);
                } catch (IllegalAccessException e2) {
                    Log.i("CompoundButtonCompat", "Failed to get button drawable via reflection", e2);
                    f598 = null;
                }
            }
            return null;
        }
    }

    /* renamed from: android.support.v4.widget.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: CompoundButtonCompat */
    static class C0156 extends C0158 {
        C0156() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1001(CompoundButton compoundButton, ColorStateList colorStateList) {
            compoundButton.setButtonTintList(colorStateList);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1002(CompoundButton compoundButton, PorterDuff.Mode mode) {
            compoundButton.setButtonTintMode(mode);
        }
    }

    /* renamed from: android.support.v4.widget.ʼ$ʼ  reason: contains not printable characters */
    /* compiled from: CompoundButtonCompat */
    static class C0157 extends C0156 {
        C0157() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Drawable m1003(CompoundButton compoundButton) {
            return compoundButton.getButtonDrawable();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m999(CompoundButton compoundButton, ColorStateList colorStateList) {
        f597.m1005(compoundButton, colorStateList);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1000(CompoundButton compoundButton, PorterDuff.Mode mode) {
        f597.m1006(compoundButton, mode);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Drawable m998(CompoundButton compoundButton) {
        return f597.m1004(compoundButton);
    }
}
