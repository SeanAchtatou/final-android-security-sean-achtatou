package android.support.v4.widget;

import android.content.res.Resources;
import android.os.SystemClock;
import android.support.v4.ˉ.C0414;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;

/* renamed from: android.support.v4.widget.ʻ  reason: contains not printable characters */
/* compiled from: AutoScrollHelper */
public abstract class C0152 implements View.OnTouchListener {

    /* renamed from: ᴵ  reason: contains not printable characters */
    private static final int f567 = ViewConfiguration.getTapTimeout();

    /* renamed from: ʻ  reason: contains not printable characters */
    final C0153 f568 = new C0153();

    /* renamed from: ʼ  reason: contains not printable characters */
    final View f569;

    /* renamed from: ʽ  reason: contains not printable characters */
    boolean f570;

    /* renamed from: ʾ  reason: contains not printable characters */
    boolean f571;

    /* renamed from: ʿ  reason: contains not printable characters */
    boolean f572;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final Interpolator f573 = new AccelerateInterpolator();

    /* renamed from: ˈ  reason: contains not printable characters */
    private Runnable f574;

    /* renamed from: ˉ  reason: contains not printable characters */
    private float[] f575 = {0.0f, 0.0f};

    /* renamed from: ˊ  reason: contains not printable characters */
    private float[] f576 = {Float.MAX_VALUE, Float.MAX_VALUE};

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f577;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f578;

    /* renamed from: ˏ  reason: contains not printable characters */
    private float[] f579 = {0.0f, 0.0f};

    /* renamed from: ˑ  reason: contains not printable characters */
    private float[] f580 = {0.0f, 0.0f};

    /* renamed from: י  reason: contains not printable characters */
    private float[] f581 = {Float.MAX_VALUE, Float.MAX_VALUE};

    /* renamed from: ـ  reason: contains not printable characters */
    private boolean f582;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f583;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f584;

    /* renamed from: ʻ  reason: contains not printable characters */
    static float m963(float f, float f2, float f3) {
        return f > f3 ? f3 : f < f2 ? f2 : f;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static int m966(int i, int i2, int i3) {
        return i > i3 ? i3 : i < i2 ? i2 : i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m973(int i, int i2);

    /* renamed from: ʿ  reason: contains not printable characters */
    public abstract boolean m983(int i);

    /* renamed from: ˆ  reason: contains not printable characters */
    public abstract boolean m984(int i);

    public C0152(View view) {
        this.f569 = view;
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        float f = (float) ((int) ((displayMetrics.density * 1575.0f) + 0.5f));
        m970(f, f);
        float f2 = (float) ((int) ((displayMetrics.density * 315.0f) + 0.5f));
        m975(f2, f2);
        m971(1);
        m982(Float.MAX_VALUE, Float.MAX_VALUE);
        m980(0.2f, 0.2f);
        m978(1.0f, 1.0f);
        m976(f567);
        m979(500);
        m981(500);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0152 m972(boolean z) {
        if (this.f583 && !z) {
            m968();
        }
        this.f583 = z;
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0152 m970(float f, float f2) {
        float[] fArr = this.f581;
        fArr[0] = f / 1000.0f;
        fArr[1] = f2 / 1000.0f;
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0152 m975(float f, float f2) {
        float[] fArr = this.f580;
        fArr[0] = f / 1000.0f;
        fArr[1] = f2 / 1000.0f;
        return this;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public C0152 m978(float f, float f2) {
        float[] fArr = this.f579;
        fArr[0] = f / 1000.0f;
        fArr[1] = f2 / 1000.0f;
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0152 m971(int i) {
        this.f577 = i;
        return this;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public C0152 m980(float f, float f2) {
        float[] fArr = this.f575;
        fArr[0] = f;
        fArr[1] = f2;
        return this;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public C0152 m982(float f, float f2) {
        float[] fArr = this.f576;
        fArr[0] = f;
        fArr[1] = f2;
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0152 m976(int i) {
        this.f578 = i;
        return this;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public C0152 m979(int i) {
        this.f568.m989(i);
        return this;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public C0152 m981(int i) {
        this.f568.m991(i);
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0013, code lost:
        if (r0 != 3) goto L_0x0058;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r6, android.view.MotionEvent r7) {
        /*
            r5 = this;
            boolean r0 = r5.f583
            r1 = 0
            if (r0 != 0) goto L_0x0006
            return r1
        L_0x0006:
            int r0 = r7.getActionMasked()
            r2 = 1
            if (r0 == 0) goto L_0x001a
            if (r0 == r2) goto L_0x0016
            r3 = 2
            if (r0 == r3) goto L_0x001e
            r6 = 3
            if (r0 == r6) goto L_0x0016
            goto L_0x0058
        L_0x0016:
            r5.m968()
            goto L_0x0058
        L_0x001a:
            r5.f571 = r2
            r5.f582 = r1
        L_0x001e:
            float r0 = r7.getX()
            int r3 = r6.getWidth()
            float r3 = (float) r3
            android.view.View r4 = r5.f569
            int r4 = r4.getWidth()
            float r4 = (float) r4
            float r0 = r5.m965(r1, r0, r3, r4)
            float r7 = r7.getY()
            int r6 = r6.getHeight()
            float r6 = (float) r6
            android.view.View r3 = r5.f569
            int r3 = r3.getHeight()
            float r3 = (float) r3
            float r6 = r5.m965(r2, r7, r6, r3)
            android.support.v4.widget.ʻ$ʻ r7 = r5.f568
            r7.m988(r0, r6)
            boolean r6 = r5.f572
            if (r6 != 0) goto L_0x0058
            boolean r6 = r5.m974()
            if (r6 == 0) goto L_0x0058
            r5.m967()
        L_0x0058:
            boolean r6 = r5.f584
            if (r6 == 0) goto L_0x0061
            boolean r6 = r5.f572
            if (r6 == 0) goto L_0x0061
            r1 = 1
        L_0x0061:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.C0152.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m974() {
        C0153 r0 = this.f568;
        int r1 = r0.m995();
        int r02 = r0.m994();
        return (r1 != 0 && m984(r1)) || (r02 != 0 && m983(r02));
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m967() {
        int i;
        if (this.f574 == null) {
            this.f574 = new C0154();
        }
        this.f572 = true;
        this.f570 = true;
        if (this.f582 || (i = this.f578) <= 0) {
            this.f574.run();
        } else {
            C0414.m2227(this.f569, this.f574, (long) i);
        }
        this.f582 = true;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m968() {
        if (this.f570) {
            this.f572 = false;
        } else {
            this.f568.m990();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private float m965(int i, float f, float f2, float f3) {
        float r5 = m964(this.f575[i], f2, this.f576[i], f);
        if (r5 == 0.0f) {
            return 0.0f;
        }
        float f4 = this.f579[i];
        float f5 = this.f580[i];
        float f6 = this.f581[i];
        float f7 = f4 * f3;
        if (r5 > 0.0f) {
            return m963(r5 * f7, f5, f6);
        }
        return -m963((-r5) * f7, f5, f6);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private float m964(float f, float f2, float f3, float f4) {
        float f5;
        float r2 = m963(f * f2, 0.0f, f3);
        float r22 = m969(f2 - f4, r2) - m969(f4, r2);
        if (r22 < 0.0f) {
            f5 = -this.f573.getInterpolation(-r22);
        } else if (r22 <= 0.0f) {
            return 0.0f;
        } else {
            f5 = this.f573.getInterpolation(r22);
        }
        return m963(f5, -1.0f, 1.0f);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private float m969(float f, float f2) {
        if (f2 == 0.0f) {
            return 0.0f;
        }
        int i = this.f577;
        if (i == 0 || i == 1) {
            if (f < f2) {
                if (f >= 0.0f) {
                    return 1.0f - (f / f2);
                }
                return (!this.f572 || this.f577 != 1) ? 0.0f : 1.0f;
            }
        } else if (i == 2 && f < 0.0f) {
            return f / (-f2);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m977() {
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
        this.f569.onTouchEvent(obtain);
        obtain.recycle();
    }

    /* renamed from: android.support.v4.widget.ʻ$ʼ  reason: contains not printable characters */
    /* compiled from: AutoScrollHelper */
    private class C0154 implements Runnable {
        C0154() {
        }

        public void run() {
            if (C0152.this.f572) {
                if (C0152.this.f570) {
                    C0152 r0 = C0152.this;
                    r0.f570 = false;
                    r0.f568.m987();
                }
                C0153 r02 = C0152.this.f568;
                if (r02.m992() || !C0152.this.m974()) {
                    C0152.this.f572 = false;
                    return;
                }
                if (C0152.this.f571) {
                    C0152 r2 = C0152.this;
                    r2.f571 = false;
                    r2.m977();
                }
                r02.m993();
                C0152.this.m973(r02.m996(), r02.m997());
                C0414.m2226(C0152.this.f569, this);
            }
        }
    }

    /* renamed from: android.support.v4.widget.ʻ$ʻ  reason: contains not printable characters */
    /* compiled from: AutoScrollHelper */
    private static class C0153 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private int f585;

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f586;

        /* renamed from: ʽ  reason: contains not printable characters */
        private float f587;

        /* renamed from: ʾ  reason: contains not printable characters */
        private float f588;

        /* renamed from: ʿ  reason: contains not printable characters */
        private long f589 = Long.MIN_VALUE;

        /* renamed from: ˆ  reason: contains not printable characters */
        private long f590 = 0;

        /* renamed from: ˈ  reason: contains not printable characters */
        private int f591 = 0;

        /* renamed from: ˉ  reason: contains not printable characters */
        private int f592 = 0;

        /* renamed from: ˊ  reason: contains not printable characters */
        private long f593 = -1;

        /* renamed from: ˋ  reason: contains not printable characters */
        private float f594;

        /* renamed from: ˎ  reason: contains not printable characters */
        private int f595;

        /* renamed from: ʻ  reason: contains not printable characters */
        private float m985(float f) {
            return (-4.0f * f * f) + (f * 4.0f);
        }

        C0153() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m989(int i) {
            this.f585 = i;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m991(int i) {
            this.f586 = i;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m987() {
            this.f589 = AnimationUtils.currentAnimationTimeMillis();
            this.f593 = -1;
            this.f590 = this.f589;
            this.f594 = 0.5f;
            this.f591 = 0;
            this.f592 = 0;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m990() {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            this.f595 = C0152.m966((int) (currentAnimationTimeMillis - this.f589), 0, this.f586);
            this.f594 = m986(currentAnimationTimeMillis);
            this.f593 = currentAnimationTimeMillis;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m992() {
            return this.f593 > 0 && AnimationUtils.currentAnimationTimeMillis() > this.f593 + ((long) this.f595);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private float m986(long j) {
            if (j < this.f589) {
                return 0.0f;
            }
            long j2 = this.f593;
            if (j2 < 0 || j < j2) {
                return C0152.m963(((float) (j - this.f589)) / ((float) this.f585), 0.0f, 1.0f) * 0.5f;
            }
            long j3 = j - j2;
            float f = this.f594;
            return (1.0f - f) + (f * C0152.m963(((float) j3) / ((float) this.f595), 0.0f, 1.0f));
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public void m993() {
            if (this.f590 != 0) {
                long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
                float r2 = m985(m986(currentAnimationTimeMillis));
                this.f590 = currentAnimationTimeMillis;
                float f = ((float) (currentAnimationTimeMillis - this.f590)) * r2;
                this.f591 = (int) (this.f587 * f);
                this.f592 = (int) (f * this.f588);
                return;
            }
            throw new RuntimeException("Cannot compute scroll delta before calling start()");
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m988(float f, float f2) {
            this.f587 = f;
            this.f588 = f2;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public int m994() {
            float f = this.f587;
            return (int) (f / Math.abs(f));
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public int m995() {
            float f = this.f588;
            return (int) (f / Math.abs(f));
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public int m996() {
            return this.f591;
        }

        /* renamed from: ˉ  reason: contains not printable characters */
        public int m997() {
            return this.f592;
        }
    }
}
