package android.support.v4.widget;

import android.content.Context;
import android.support.v4.ˉ.C0414;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.OverScroller;
import java.util.Arrays;

/* renamed from: android.support.v4.widget.י  reason: contains not printable characters */
/* compiled from: ViewDragHelper */
public class C0188 {

    /* renamed from: ⁱ  reason: contains not printable characters */
    private static final Interpolator f623 = new Interpolator() {
        public float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (f2 * f2 * f2 * f2 * f2) + 1.0f;
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f624;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f625;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f626 = -1;

    /* renamed from: ʾ  reason: contains not printable characters */
    private float[] f627;

    /* renamed from: ʿ  reason: contains not printable characters */
    private float[] f628;

    /* renamed from: ˆ  reason: contains not printable characters */
    private float[] f629;

    /* renamed from: ˈ  reason: contains not printable characters */
    private float[] f630;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int[] f631;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int[] f632;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int[] f633;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f634;

    /* renamed from: ˏ  reason: contains not printable characters */
    private VelocityTracker f635;

    /* renamed from: ˑ  reason: contains not printable characters */
    private float f636;

    /* renamed from: י  reason: contains not printable characters */
    private float f637;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f638;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f639;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private OverScroller f640;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final C0189 f641;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private View f642;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f643;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private final ViewGroup f644;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private final Runnable f645 = new Runnable() {
        public void run() {
            C0188.this.m1099(0);
        }
    };

    /* renamed from: android.support.v4.widget.י$ʻ  reason: contains not printable characters */
    /* compiled from: ViewDragHelper */
    public static abstract class C0189 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public int m1105(View view) {
            return 0;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m1106(View view, int i, int i2) {
            return 0;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1107(int i) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1108(int i, int i2) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1109(View view, float f, float f2) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1110(View view, int i, int i2, int i3, int i4) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract boolean m1111(View view, int i);

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m1112(View view) {
            return 0;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m1113(View view, int i, int i2) {
            return 0;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m1114(int i, int i2) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m1115(View view, int i) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m1116(int i) {
            return false;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public int m1117(int i) {
            return i;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0188 m1067(ViewGroup viewGroup, C0189 r3) {
        return new C0188(viewGroup.getContext(), viewGroup, r3);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0188 m1066(ViewGroup viewGroup, float f, C0189 r3) {
        C0188 r1 = m1067(viewGroup, r3);
        r1.f625 = (int) (((float) r1.f625) * (1.0f / f));
        return r1;
    }

    private C0188(Context context, ViewGroup viewGroup, C0189 r4) {
        if (viewGroup == null) {
            throw new IllegalArgumentException("Parent view may not be null");
        } else if (r4 != null) {
            this.f644 = viewGroup;
            this.f641 = r4;
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            this.f638 = (int) ((context.getResources().getDisplayMetrics().density * 20.0f) + 0.5f);
            this.f625 = viewConfiguration.getScaledTouchSlop();
            this.f636 = (float) viewConfiguration.getScaledMaximumFlingVelocity();
            this.f637 = (float) viewConfiguration.getScaledMinimumFlingVelocity();
            this.f640 = new OverScroller(context, f623);
        } else {
            throw new IllegalArgumentException("Callback may not be null");
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1085(float f) {
        this.f637 = f;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m1084() {
        return this.f624;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1086(int i) {
        this.f639 = i;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m1092() {
        return this.f638;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1087(View view, int i) {
        if (view.getParent() == this.f644) {
            this.f642 = view;
            this.f626 = i;
            this.f641.m1115(view, i);
            m1099(1);
            return;
        }
        throw new IllegalArgumentException("captureChildView: parameter must be a descendant of the ViewDragHelper's tracked parent view (" + this.f644 + ")");
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public View m1098() {
        return this.f642;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m1101() {
        return this.f625;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m1104() {
        this.f626 = -1;
        m1080();
        VelocityTracker velocityTracker = this.f635;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.f635 = null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1090(View view, int i, int i2) {
        this.f642 = view;
        this.f626 = -1;
        boolean r1 = m1071(i, i2, 0, 0);
        if (!r1 && this.f624 == 0 && this.f642 != null) {
            this.f642 = null;
        }
        return r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1088(int i, int i2) {
        if (this.f643) {
            return m1071(i, i2, (int) this.f635.getXVelocity(this.f626), (int) this.f635.getYVelocity(this.f626));
        }
        throw new IllegalStateException("Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m1071(int i, int i2, int i3, int i4) {
        int left = this.f642.getLeft();
        int top = this.f642.getTop();
        int i5 = i - left;
        int i6 = i2 - top;
        if (i5 == 0 && i6 == 0) {
            this.f640.abortAnimation();
            m1099(0);
            return false;
        }
        this.f640.startScroll(left, top, i5, i6, m1065(this.f642, i5, i6, i3, i4));
        m1099(2);
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m1065(View view, int i, int i2, int i3, int i4) {
        float f;
        float f2;
        float f3;
        float f4;
        int r10 = m1074(i3, (int) this.f637, (int) this.f636);
        int r11 = m1074(i4, (int) this.f637, (int) this.f636);
        int abs = Math.abs(i);
        int abs2 = Math.abs(i2);
        int abs3 = Math.abs(r10);
        int abs4 = Math.abs(r11);
        int i5 = abs3 + abs4;
        int i6 = abs + abs2;
        if (r10 != 0) {
            f2 = (float) abs3;
            f = (float) i5;
        } else {
            f2 = (float) abs;
            f = (float) i6;
        }
        float f5 = f2 / f;
        if (r11 != 0) {
            f4 = (float) abs4;
            f3 = (float) i5;
        } else {
            f4 = (float) abs2;
            f3 = (float) i6;
        }
        float f6 = f4 / f3;
        return (int) ((((float) m1064(i, r10, this.f641.m1112(view))) * f5) + (((float) m1064(i2, r11, this.f641.m1105(view))) * f6));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* renamed from: ʻ  reason: contains not printable characters */
    private int m1064(int i, int i2, int i3) {
        int i4;
        if (i == 0) {
            return 0;
        }
        int width = this.f644.getWidth();
        float f = (float) (width / 2);
        float r1 = f + (m1073(Math.min(1.0f, ((float) Math.abs(i)) / ((float) width))) * f);
        int abs = Math.abs(i2);
        if (abs > 0) {
            i4 = Math.round(Math.abs(r1 / ((float) abs)) * 1000.0f) * 4;
        } else {
            i4 = (int) (((((float) Math.abs(i)) / ((float) i3)) + 1.0f) * 256.0f);
        }
        return Math.min(i4, 600);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int m1074(int i, int i2, int i3) {
        int abs = Math.abs(i);
        if (abs < i2) {
            return 0;
        }
        if (abs > i3) {
            return i > 0 ? i3 : -i3;
        }
        return i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private float m1063(float f, float f2, float f3) {
        float abs = Math.abs(f);
        if (abs < f2) {
            return 0.0f;
        }
        if (abs > f3) {
            return f > 0.0f ? f3 : -f3;
        }
        return f;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private float m1073(float f) {
        return (float) Math.sin((double) ((f - 0.5f) * 0.47123894f));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1091(boolean z) {
        if (this.f624 == 2) {
            boolean computeScrollOffset = this.f640.computeScrollOffset();
            int currX = this.f640.getCurrX();
            int currY = this.f640.getCurrY();
            int left = currX - this.f642.getLeft();
            int top = currY - this.f642.getTop();
            if (left != 0) {
                C0414.m2234(this.f642, left);
            }
            if (top != 0) {
                C0414.m2231(this.f642, top);
            }
            if (!(left == 0 && top == 0)) {
                this.f641.m1110(this.f642, currX, currY, left, top);
            }
            if (computeScrollOffset && currX == this.f640.getFinalX() && currY == this.f640.getFinalY()) {
                this.f640.abortAnimation();
                computeScrollOffset = false;
            }
            if (!computeScrollOffset) {
                if (z) {
                    this.f644.post(this.f645);
                } else {
                    m1099(0);
                }
            }
        }
        if (this.f624 == 2) {
            return true;
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m1068(float f, float f2) {
        this.f643 = true;
        this.f641.m1109(this.f642, f, f2);
        this.f643 = false;
        if (this.f624 == 1) {
            m1099(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(float[], float):void}
     arg types: [float[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void} */
    /* renamed from: ˆ  reason: contains not printable characters */
    private void m1080() {
        float[] fArr = this.f627;
        if (fArr != null) {
            Arrays.fill(fArr, 0.0f);
            Arrays.fill(this.f628, 0.0f);
            Arrays.fill(this.f629, 0.0f);
            Arrays.fill(this.f630, 0.0f);
            Arrays.fill(this.f631, 0);
            Arrays.fill(this.f632, 0);
            Arrays.fill(this.f633, 0);
            this.f634 = 0;
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m1079(int i) {
        if (this.f627 != null && m1094(i)) {
            this.f627[i] = 0.0f;
            this.f628[i] = 0.0f;
            this.f629[i] = 0.0f;
            this.f630[i] = 0.0f;
            this.f631[i] = 0;
            this.f632[i] = 0;
            this.f633[i] = 0;
            this.f634 = ((1 << i) ^ -1) & this.f634;
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private void m1081(int i) {
        float[] fArr = this.f627;
        if (fArr == null || fArr.length <= i) {
            int i2 = i + 1;
            float[] fArr2 = new float[i2];
            float[] fArr3 = new float[i2];
            float[] fArr4 = new float[i2];
            float[] fArr5 = new float[i2];
            int[] iArr = new int[i2];
            int[] iArr2 = new int[i2];
            int[] iArr3 = new int[i2];
            float[] fArr6 = this.f627;
            if (fArr6 != null) {
                System.arraycopy(fArr6, 0, fArr2, 0, fArr6.length);
                float[] fArr7 = this.f628;
                System.arraycopy(fArr7, 0, fArr3, 0, fArr7.length);
                float[] fArr8 = this.f629;
                System.arraycopy(fArr8, 0, fArr4, 0, fArr8.length);
                float[] fArr9 = this.f630;
                System.arraycopy(fArr9, 0, fArr5, 0, fArr9.length);
                int[] iArr4 = this.f631;
                System.arraycopy(iArr4, 0, iArr, 0, iArr4.length);
                int[] iArr5 = this.f632;
                System.arraycopy(iArr5, 0, iArr2, 0, iArr5.length);
                int[] iArr6 = this.f633;
                System.arraycopy(iArr6, 0, iArr3, 0, iArr6.length);
            }
            this.f627 = fArr2;
            this.f628 = fArr3;
            this.f629 = fArr4;
            this.f630 = fArr5;
            this.f631 = iArr;
            this.f632 = iArr2;
            this.f633 = iArr3;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m1069(float f, float f2, int i) {
        m1081(i);
        float[] fArr = this.f627;
        this.f629[i] = f;
        fArr[i] = f;
        float[] fArr2 = this.f628;
        this.f630[i] = f2;
        fArr2[i] = f2;
        this.f631[i] = m1078((int) f, (int) f2);
        this.f634 |= 1 << i;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m1077(MotionEvent motionEvent) {
        int pointerCount = motionEvent.getPointerCount();
        for (int i = 0; i < pointerCount; i++) {
            int pointerId = motionEvent.getPointerId(i);
            if (m1083(pointerId)) {
                float x = motionEvent.getX(i);
                float y = motionEvent.getY(i);
                this.f629[pointerId] = x;
                this.f630[pointerId] = y;
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m1094(int i) {
        return ((1 << i) & this.f634) != 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1099(int i) {
        this.f644.removeCallbacks(this.f645);
        if (this.f624 != i) {
            this.f624 = i;
            this.f641.m1107(i);
            if (this.f624 == 0) {
                this.f642 = null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m1096(View view, int i) {
        if (view == this.f642 && this.f626 == i) {
            return true;
        }
        if (view == null || !this.f641.m1111(view, i)) {
            return false;
        }
        this.f626 = i;
        m1087(view, i);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00dd, code lost:
        if (r12 != r11) goto L_0x00e6;
     */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean m1089(android.view.MotionEvent r17) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            int r2 = r17.getActionMasked()
            int r3 = r17.getActionIndex()
            if (r2 != 0) goto L_0x0011
            r16.m1104()
        L_0x0011:
            android.view.VelocityTracker r4 = r0.f635
            if (r4 != 0) goto L_0x001b
            android.view.VelocityTracker r4 = android.view.VelocityTracker.obtain()
            r0.f635 = r4
        L_0x001b:
            android.view.VelocityTracker r4 = r0.f635
            r4.addMovement(r1)
            r4 = 2
            r6 = 1
            if (r2 == 0) goto L_0x0104
            if (r2 == r6) goto L_0x00ff
            if (r2 == r4) goto L_0x0070
            r7 = 3
            if (r2 == r7) goto L_0x00ff
            r7 = 5
            if (r2 == r7) goto L_0x003c
            r4 = 6
            if (r2 == r4) goto L_0x0034
        L_0x0031:
            r5 = 0
            goto L_0x0135
        L_0x0034:
            int r1 = r1.getPointerId(r3)
            r0.m1079(r1)
            goto L_0x0031
        L_0x003c:
            int r2 = r1.getPointerId(r3)
            float r7 = r1.getX(r3)
            float r1 = r1.getY(r3)
            r0.m1069(r7, r1, r2)
            int r3 = r0.f624
            if (r3 != 0) goto L_0x0060
            int[] r1 = r0.f631
            r1 = r1[r2]
            int r3 = r0.f639
            r4 = r1 & r3
            if (r4 == 0) goto L_0x0031
            android.support.v4.widget.י$ʻ r4 = r0.f641
            r1 = r1 & r3
            r4.m1108(r1, r2)
            goto L_0x0031
        L_0x0060:
            if (r3 != r4) goto L_0x0031
            int r3 = (int) r7
            int r1 = (int) r1
            android.view.View r1 = r0.m1102(r3, r1)
            android.view.View r3 = r0.f642
            if (r1 != r3) goto L_0x0031
            r0.m1096(r1, r2)
            goto L_0x0031
        L_0x0070:
            float[] r2 = r0.f627
            if (r2 == 0) goto L_0x0031
            float[] r2 = r0.f628
            if (r2 != 0) goto L_0x0079
            goto L_0x0031
        L_0x0079:
            int r2 = r17.getPointerCount()
            r3 = 0
        L_0x007e:
            if (r3 >= r2) goto L_0x00fa
            int r4 = r1.getPointerId(r3)
            boolean r7 = r0.m1083(r4)
            if (r7 != 0) goto L_0x008c
            goto L_0x00f7
        L_0x008c:
            float r7 = r1.getX(r3)
            float r8 = r1.getY(r3)
            float[] r9 = r0.f627
            r9 = r9[r4]
            float r9 = r7 - r9
            float[] r10 = r0.f628
            r10 = r10[r4]
            float r10 = r8 - r10
            int r7 = (int) r7
            int r8 = (int) r8
            android.view.View r7 = r0.m1102(r7, r8)
            if (r7 == 0) goto L_0x00b0
            boolean r8 = r0.m1072(r7, r9, r10)
            if (r8 == 0) goto L_0x00b0
            r8 = 1
            goto L_0x00b1
        L_0x00b0:
            r8 = 0
        L_0x00b1:
            if (r8 == 0) goto L_0x00e6
            int r11 = r7.getLeft()
            int r12 = (int) r9
            int r13 = r11 + r12
            android.support.v4.widget.י$ʻ r14 = r0.f641
            int r12 = r14.m1113(r7, r13, r12)
            int r13 = r7.getTop()
            int r14 = (int) r10
            int r15 = r13 + r14
            android.support.v4.widget.י$ʻ r5 = r0.f641
            int r5 = r5.m1106(r7, r15, r14)
            android.support.v4.widget.י$ʻ r14 = r0.f641
            int r14 = r14.m1112(r7)
            android.support.v4.widget.י$ʻ r15 = r0.f641
            int r15 = r15.m1105(r7)
            if (r14 == 0) goto L_0x00df
            if (r14 <= 0) goto L_0x00e6
            if (r12 != r11) goto L_0x00e6
        L_0x00df:
            if (r15 == 0) goto L_0x00fa
            if (r15 <= 0) goto L_0x00e6
            if (r5 != r13) goto L_0x00e6
            goto L_0x00fa
        L_0x00e6:
            r0.m1075(r9, r10, r4)
            int r5 = r0.f624
            if (r5 != r6) goto L_0x00ee
            goto L_0x00fa
        L_0x00ee:
            if (r8 == 0) goto L_0x00f7
            boolean r4 = r0.m1096(r7, r4)
            if (r4 == 0) goto L_0x00f7
            goto L_0x00fa
        L_0x00f7:
            int r3 = r3 + 1
            goto L_0x007e
        L_0x00fa:
            r16.m1077(r17)
            goto L_0x0031
        L_0x00ff:
            r16.m1104()
            goto L_0x0031
        L_0x0104:
            float r2 = r17.getX()
            float r3 = r17.getY()
            r5 = 0
            int r1 = r1.getPointerId(r5)
            r0.m1069(r2, r3, r1)
            int r2 = (int) r2
            int r3 = (int) r3
            android.view.View r2 = r0.m1102(r2, r3)
            android.view.View r3 = r0.f642
            if (r2 != r3) goto L_0x0125
            int r3 = r0.f624
            if (r3 != r4) goto L_0x0125
            r0.m1096(r2, r1)
        L_0x0125:
            int[] r2 = r0.f631
            r2 = r2[r1]
            int r3 = r0.f639
            r4 = r2 & r3
            if (r4 == 0) goto L_0x0135
            android.support.v4.widget.י$ʻ r4 = r0.f641
            r2 = r2 & r3
            r4.m1108(r2, r1)
        L_0x0135:
            int r1 = r0.f624
            if (r1 != r6) goto L_0x013a
            r5 = 1
        L_0x013a:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.C0188.m1089(android.view.MotionEvent):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.י.ʻ(float, float):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.י.ʻ(android.view.ViewGroup, android.support.v4.widget.י$ʻ):android.support.v4.widget.י
      android.support.v4.widget.י.ʻ(android.view.View, int):void
      android.support.v4.widget.י.ʻ(int, int):boolean
      android.support.v4.widget.י.ʻ(float, float):void */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1093(MotionEvent motionEvent) {
        int i;
        int actionMasked = motionEvent.getActionMasked();
        int actionIndex = motionEvent.getActionIndex();
        if (actionMasked == 0) {
            m1104();
        }
        if (this.f635 == null) {
            this.f635 = VelocityTracker.obtain();
        }
        this.f635.addMovement(motionEvent);
        int i2 = 0;
        if (actionMasked == 0) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int pointerId = motionEvent.getPointerId(0);
            View r2 = m1102((int) x, (int) y);
            m1069(x, y, pointerId);
            m1096(r2, pointerId);
            int i3 = this.f631[pointerId];
            int i4 = this.f639;
            if ((i3 & i4) != 0) {
                this.f641.m1108(i3 & i4, pointerId);
            }
        } else if (actionMasked == 1) {
            if (this.f624 == 1) {
                m1082();
            }
            m1104();
        } else if (actionMasked != 2) {
            if (actionMasked == 3) {
                if (this.f624 == 1) {
                    m1068(0.0f, 0.0f);
                }
                m1104();
            } else if (actionMasked == 5) {
                int pointerId2 = motionEvent.getPointerId(actionIndex);
                float x2 = motionEvent.getX(actionIndex);
                float y2 = motionEvent.getY(actionIndex);
                m1069(x2, y2, pointerId2);
                if (this.f624 == 0) {
                    m1096(m1102((int) x2, (int) y2), pointerId2);
                    int i5 = this.f631[pointerId2];
                    int i6 = this.f639;
                    if ((i5 & i6) != 0) {
                        this.f641.m1108(i5 & i6, pointerId2);
                    }
                } else if (m1100((int) x2, (int) y2)) {
                    m1096(this.f642, pointerId2);
                }
            } else if (actionMasked == 6) {
                int pointerId3 = motionEvent.getPointerId(actionIndex);
                if (this.f624 == 1 && pointerId3 == this.f626) {
                    int pointerCount = motionEvent.getPointerCount();
                    while (true) {
                        if (i2 >= pointerCount) {
                            i = -1;
                            break;
                        }
                        int pointerId4 = motionEvent.getPointerId(i2);
                        if (pointerId4 != this.f626) {
                            View r5 = m1102((int) motionEvent.getX(i2), (int) motionEvent.getY(i2));
                            View view = this.f642;
                            if (r5 == view && m1096(view, pointerId4)) {
                                i = this.f626;
                                break;
                            }
                        }
                        i2++;
                    }
                    if (i == -1) {
                        m1082();
                    }
                }
                m1079(pointerId3);
            }
        } else if (this.f624 != 1) {
            int pointerCount2 = motionEvent.getPointerCount();
            while (i2 < pointerCount2) {
                int pointerId5 = motionEvent.getPointerId(i2);
                if (m1083(pointerId5)) {
                    float x3 = motionEvent.getX(i2);
                    float y3 = motionEvent.getY(i2);
                    float f = x3 - this.f627[pointerId5];
                    float f2 = y3 - this.f628[pointerId5];
                    m1075(f, f2, pointerId5);
                    if (this.f624 != 1) {
                        View r4 = m1102((int) x3, (int) y3);
                        if (m1072(r4, f, f2) && m1096(r4, pointerId5)) {
                            break;
                        }
                    } else {
                        break;
                    }
                }
                i2++;
            }
            m1077(motionEvent);
        } else if (m1083(this.f626)) {
            int findPointerIndex = motionEvent.findPointerIndex(this.f626);
            float x4 = motionEvent.getX(findPointerIndex);
            float y4 = motionEvent.getY(findPointerIndex);
            float[] fArr = this.f629;
            int i7 = this.f626;
            int i8 = (int) (x4 - fArr[i7]);
            int i9 = (int) (y4 - this.f630[i7]);
            m1076(this.f642.getLeft() + i8, this.f642.getTop() + i9, i8, i9);
            m1077(motionEvent);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m1075(float f, float f2, int i) {
        int i2 = 1;
        if (!m1070(f, f2, i, 1)) {
            i2 = 0;
        }
        if (m1070(f2, f, i, 4)) {
            i2 |= 4;
        }
        if (m1070(f, f2, i, 2)) {
            i2 |= 2;
        }
        if (m1070(f2, f, i, 8)) {
            i2 |= 8;
        }
        if (i2 != 0) {
            int[] iArr = this.f632;
            iArr[i] = iArr[i] | i2;
            this.f641.m1114(i2, i);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m1070(float f, float f2, int i, int i2) {
        float abs = Math.abs(f);
        float abs2 = Math.abs(f2);
        if ((this.f631[i] & i2) != i2 || (this.f639 & i2) == 0 || (this.f633[i] & i2) == i2 || (this.f632[i] & i2) == i2) {
            return false;
        }
        int i3 = this.f625;
        if (abs <= ((float) i3) && abs2 <= ((float) i3)) {
            return false;
        }
        if (abs < abs2 * 0.5f && this.f641.m1116(i2)) {
            int[] iArr = this.f633;
            iArr[i] = iArr[i] | i2;
            return false;
        } else if ((this.f632[i] & i2) != 0 || abs <= ((float) this.f625)) {
            return false;
        } else {
            return true;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m1072(View view, float f, float f2) {
        if (view == null) {
            return false;
        }
        boolean z = this.f641.m1112(view) > 0;
        boolean z2 = this.f641.m1105(view) > 0;
        if (z && z2) {
            int i = this.f625;
            if ((f * f) + (f2 * f2) > ((float) (i * i))) {
                return true;
            }
            return false;
        } else if (z) {
            if (Math.abs(f) > ((float) this.f625)) {
                return true;
            }
            return false;
        } else if (!z2 || Math.abs(f2) <= ((float) this.f625)) {
            return false;
        } else {
            return true;
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m1103(int i) {
        int length = this.f627.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (m1095(i, i2)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m1095(int i, int i2) {
        if (!m1094(i2)) {
            return false;
        }
        boolean z = (i & 1) == 1;
        boolean z2 = (i & 2) == 2;
        float f = this.f629[i2] - this.f627[i2];
        float f2 = this.f630[i2] - this.f628[i2];
        if (z && z2) {
            int i3 = this.f625;
            if ((f * f) + (f2 * f2) > ((float) (i3 * i3))) {
                return true;
            }
            return false;
        } else if (z) {
            if (Math.abs(f) > ((float) this.f625)) {
                return true;
            }
            return false;
        } else if (!z2 || Math.abs(f2) <= ((float) this.f625)) {
            return false;
        } else {
            return true;
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private void m1082() {
        this.f635.computeCurrentVelocity(1000, this.f636);
        m1068(m1063(this.f635.getXVelocity(this.f626), this.f637, this.f636), m1063(this.f635.getYVelocity(this.f626), this.f637, this.f636));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m1076(int i, int i2, int i3, int i4) {
        int left = this.f642.getLeft();
        int top = this.f642.getTop();
        if (i3 != 0) {
            i = this.f641.m1113(this.f642, i, i3);
            C0414.m2234(this.f642, i - left);
        }
        int i5 = i;
        if (i4 != 0) {
            i2 = this.f641.m1106(this.f642, i2, i4);
            C0414.m2231(this.f642, i2 - top);
        }
        int i6 = i2;
        if (i3 != 0 || i4 != 0) {
            this.f641.m1110(this.f642, i5, i6, i5 - left, i6 - top);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m1100(int i, int i2) {
        return m1097(this.f642, i, i2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m1097(View view, int i, int i2) {
        if (view != null && i >= view.getLeft() && i < view.getRight() && i2 >= view.getTop() && i2 < view.getBottom()) {
            return true;
        }
        return false;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public View m1102(int i, int i2) {
        for (int childCount = this.f644.getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = this.f644.getChildAt(this.f641.m1117(childCount));
            if (i >= childAt.getLeft() && i < childAt.getRight() && i2 >= childAt.getTop() && i2 < childAt.getBottom()) {
                return childAt;
            }
        }
        return null;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private int m1078(int i, int i2) {
        int i3 = i < this.f644.getLeft() + this.f638 ? 1 : 0;
        if (i2 < this.f644.getTop() + this.f638) {
            i3 |= 4;
        }
        if (i > this.f644.getRight() - this.f638) {
            i3 |= 2;
        }
        return i2 > this.f644.getBottom() - this.f638 ? i3 | 8 : i3;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean m1083(int i) {
        if (m1094(i)) {
            return true;
        }
        Log.e("ViewDragHelper", "Ignoring pointerId=" + i + " because ACTION_DOWN was not received " + "for this pointer before ACTION_MOVE. It likely happened because " + " ViewDragHelper did not receive all the events in the event stream.");
        return false;
    }
}
