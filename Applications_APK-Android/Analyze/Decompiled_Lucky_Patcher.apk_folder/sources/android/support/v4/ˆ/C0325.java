package android.support.v4.ˆ;

import android.support.v4.ˈ.C0352;
import android.util.Base64;
import java.util.List;

/* renamed from: android.support.v4.ˆ.ʻ  reason: contains not printable characters */
/* compiled from: FontRequest */
public final class C0325 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f1067;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final String f1068;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final String f1069;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final List<List<byte[]>> f1070;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final int f1071 = 0;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final String f1072 = (this.f1067 + "-" + this.f1068 + "-" + this.f1069);

    public C0325(String str, String str2, String str3, List<List<byte[]>> list) {
        this.f1067 = (String) C0352.m1969(str);
        this.f1068 = (String) C0352.m1969(str2);
        this.f1069 = (String) C0352.m1969(str3);
        this.f1070 = (List) C0352.m1969(list);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m1843() {
        return this.f1067;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m1844() {
        return this.f1068;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m1845() {
        return this.f1069;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public List<List<byte[]>> m1846() {
        return this.f1070;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m1847() {
        return this.f1071;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public String m1848() {
        return this.f1072;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("FontRequest {mProviderAuthority: " + this.f1067 + ", mProviderPackage: " + this.f1068 + ", mQuery: " + this.f1069 + ", mCertificates:");
        for (int i = 0; i < this.f1070.size(); i++) {
            sb.append(" [");
            List list = this.f1070.get(i);
            for (int i2 = 0; i2 < list.size(); i2++) {
                sb.append(" \"");
                sb.append(Base64.encodeToString((byte[]) list.get(i2), 0));
                sb.append("\"");
            }
            sb.append(" ]");
        }
        sb.append("}");
        sb.append("mCertificatesArray: " + this.f1071);
        return sb.toString();
    }
}
