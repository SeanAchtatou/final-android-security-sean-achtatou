package android.support.v4.ˆ;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import java.util.concurrent.Callable;

/* renamed from: android.support.v4.ˆ.ʽ  reason: contains not printable characters */
/* compiled from: SelfDestructiveThread */
public class C0329 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Object f1093 = new Object();

    /* renamed from: ʼ  reason: contains not printable characters */
    private HandlerThread f1094;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Handler f1095;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f1096;

    /* renamed from: ʿ  reason: contains not printable characters */
    private Handler.Callback f1097 = new Handler.Callback() {
        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 0) {
                C0329.this.m1875();
                return true;
            } else if (i != 1) {
                return true;
            } else {
                C0329.this.m1879((Runnable) message.obj);
                return true;
            }
        }
    };

    /* renamed from: ˆ  reason: contains not printable characters */
    private final int f1098;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final int f1099;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final String f1100;

    /* renamed from: android.support.v4.ˆ.ʽ$ʻ  reason: contains not printable characters */
    /* compiled from: SelfDestructiveThread */
    public interface C0330<T> {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m1882(Object obj);
    }

    public C0329(String str, int i, int i2) {
        this.f1100 = str;
        this.f1099 = i;
        this.f1098 = i2;
        this.f1096 = 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m1878(Runnable runnable) {
        synchronized (this.f1093) {
            if (this.f1094 == null) {
                this.f1094 = new HandlerThread(this.f1100, this.f1099);
                this.f1094.start();
                this.f1095 = new Handler(this.f1094.getLooper(), this.f1097);
                this.f1096++;
            }
            this.f1095.removeMessages(0);
            this.f1095.sendMessage(this.f1095.obtainMessage(1, runnable));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public <T> void m1881(final Callable callable, final C0330 r4) {
        final Handler handler = new Handler();
        m1878(new Runnable() {
            public void run() {
                final Object obj;
                try {
                    obj = callable.call();
                } catch (Exception unused) {
                    obj = null;
                }
                handler.post(new Runnable() {
                    public void run() {
                        r4.m1882(obj);
                    }
                });
            }
        });
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:9|10|11|12|(4:25|14|15|16)(1:17)) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003f */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0045 A[SYNTHETIC] */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> T m1880(java.util.concurrent.Callable r13, int r14) {
        /*
            r12 = this;
            java.util.concurrent.locks.ReentrantLock r7 = new java.util.concurrent.locks.ReentrantLock
            r7.<init>()
            java.util.concurrent.locks.Condition r8 = r7.newCondition()
            java.util.concurrent.atomic.AtomicReference r9 = new java.util.concurrent.atomic.AtomicReference
            r9.<init>()
            java.util.concurrent.atomic.AtomicBoolean r10 = new java.util.concurrent.atomic.AtomicBoolean
            r0 = 1
            r10.<init>(r0)
            android.support.v4.ˆ.ʽ$3 r11 = new android.support.v4.ˆ.ʽ$3
            r0 = r11
            r1 = r12
            r2 = r9
            r3 = r13
            r4 = r7
            r5 = r10
            r6 = r8
            r0.<init>(r2, r3, r4, r5, r6)
            r12.m1878(r11)
            r7.lock()
            boolean r13 = r10.get()     // Catch:{ all -> 0x005c }
            if (r13 != 0) goto L_0x0034
            java.lang.Object r13 = r9.get()     // Catch:{ all -> 0x005c }
            r7.unlock()
            return r13
        L_0x0034:
            java.util.concurrent.TimeUnit r13 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ all -> 0x005c }
            long r0 = (long) r14     // Catch:{ all -> 0x005c }
            long r13 = r13.toNanos(r0)     // Catch:{ all -> 0x005c }
        L_0x003b:
            long r13 = r8.awaitNanos(r13)     // Catch:{ InterruptedException -> 0x003f }
        L_0x003f:
            boolean r0 = r10.get()     // Catch:{ all -> 0x005c }
            if (r0 != 0) goto L_0x004d
            java.lang.Object r13 = r9.get()     // Catch:{ all -> 0x005c }
            r7.unlock()
            return r13
        L_0x004d:
            r0 = 0
            int r2 = (r13 > r0 ? 1 : (r13 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x0054
            goto L_0x003b
        L_0x0054:
            java.lang.InterruptedException r13 = new java.lang.InterruptedException     // Catch:{ all -> 0x005c }
            java.lang.String r14 = "timeout"
            r13.<init>(r14)     // Catch:{ all -> 0x005c }
            throw r13     // Catch:{ all -> 0x005c }
        L_0x005c:
            r13 = move-exception
            r7.unlock()
            goto L_0x0062
        L_0x0061:
            throw r13
        L_0x0062:
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.ˆ.C0329.m1880(java.util.concurrent.Callable, int):java.lang.Object");
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1879(Runnable runnable) {
        runnable.run();
        synchronized (this.f1093) {
            this.f1095.removeMessages(0);
            this.f1095.sendMessageDelayed(this.f1095.obtainMessage(0), (long) this.f1098);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1875() {
        synchronized (this.f1093) {
            if (!this.f1095.hasMessages(1)) {
                this.f1094.quit();
                this.f1094 = null;
                this.f1095 = null;
            }
        }
    }
}
