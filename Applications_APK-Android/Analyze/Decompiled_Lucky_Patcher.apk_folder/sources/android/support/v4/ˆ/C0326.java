package android.support.v4.ˆ;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.CancellationSignal;
import android.support.v4.content.ʻ.C0102;
import android.support.v4.ʼ.C0306;
import android.support.v4.ʼ.C0313;
import android.support.v4.ˆ.C0329;
import android.support.v4.ˈ.C0337;
import android.support.v4.ˈ.C0352;
import android.support.v4.ˈ.C0353;
import android.widget.TextView;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/* renamed from: android.support.v4.ˆ.ʼ  reason: contains not printable characters */
/* compiled from: FontsContractCompat */
public class C0326 {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final C0337<String, Typeface> f1073 = new C0337<>(16);

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final C0329 f1074 = new C0329("fonts", 10, 10000);
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final Object f1075 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public static final C0353<String, ArrayList<C0329.C0330<Typeface>>> f1076 = new C0353<>();

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final Comparator<byte[]> f1077 = new Comparator<byte[]>() {
        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v5, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v4, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v6, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v5, resolved type: byte} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* renamed from: ʻ  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int compare(byte[] r5, byte[] r6) {
            /*
                r4 = this;
                int r0 = r5.length
                int r1 = r6.length
                if (r0 == r1) goto L_0x0008
                int r5 = r5.length
                int r6 = r6.length
            L_0x0006:
                int r5 = r5 - r6
                return r5
            L_0x0008:
                r0 = 0
                r1 = 0
            L_0x000a:
                int r2 = r5.length
                if (r1 >= r2) goto L_0x001b
                byte r2 = r5[r1]
                byte r3 = r6[r1]
                if (r2 == r3) goto L_0x0018
                byte r5 = r5[r1]
                byte r6 = r6[r1]
                goto L_0x0006
            L_0x0018:
                int r1 = r1 + 1
                goto L_0x000a
            L_0x001b:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.ˆ.C0326.AnonymousClass4.compare(byte[], byte[]):int");
        }
    };

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public static Typeface m1859(Context context, C0325 r3, int i) {
        try {
            C0327 r32 = m1852(context, (CancellationSignal) null, r3);
            if (r32.m1868() == 0) {
                return C0306.m1780(context, null, r32.m1869(), i);
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Typeface m1851(final Context context, final C0325 r3, final TextView textView, int i, int i2, final int i3) {
        final String str = r3.m1848() + "-" + i3;
        Typeface r1 = f1073.m1932(str);
        if (r1 != null) {
            return r1;
        }
        boolean z = i == 0;
        if (z && i2 == -1) {
            return m1859(context, r3, i3);
        }
        AnonymousClass1 r12 = new Callable<Typeface>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public Typeface call() {
                Typeface r0 = C0326.m1859(context, r3, i3);
                if (r0 != null) {
                    C0326.f1073.m1933(str, r0);
                }
                return r0;
            }
        };
        if (z) {
            try {
                return (Typeface) f1074.m1880(r12, i2);
            } catch (InterruptedException unused) {
                return null;
            }
        } else {
            final WeakReference weakReference = new WeakReference(textView);
            AnonymousClass2 r5 = new C0329.C0330<Typeface>() {
                /* renamed from: ʻ  reason: contains not printable characters */
                public void m1864(Typeface typeface) {
                    if (((TextView) weakReference.get()) != null) {
                        textView.setTypeface(typeface, i3);
                    }
                }
            };
            synchronized (f1075) {
                if (f1076.containsKey(str)) {
                    f1076.get(str).add(r5);
                    return null;
                }
                ArrayList arrayList = new ArrayList();
                arrayList.add(r5);
                f1076.put(str, arrayList);
                f1074.m1881(r12, new C0329.C0330<Typeface>() {
                    /* renamed from: ʻ  reason: contains not printable characters */
                    public void m1866(Typeface typeface) {
                        ArrayList arrayList;
                        synchronized (C0326.f1075) {
                            arrayList = (ArrayList) C0326.f1076.get(str);
                            C0326.f1076.remove(str);
                        }
                        for (int i = 0; i < arrayList.size(); i++) {
                            ((C0329.C0330) arrayList.get(i)).m1882(typeface);
                        }
                    }
                });
                return null;
            }
        }
    }

    /* renamed from: android.support.v4.ˆ.ʼ$ʼ  reason: contains not printable characters */
    /* compiled from: FontsContractCompat */
    public static class C0328 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final Uri f1088;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final int f1089;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final int f1090;

        /* renamed from: ʾ  reason: contains not printable characters */
        private final boolean f1091;

        /* renamed from: ʿ  reason: contains not printable characters */
        private final int f1092;

        public C0328(Uri uri, int i, int i2, boolean z, int i3) {
            this.f1088 = (Uri) C0352.m1969(uri);
            this.f1089 = i;
            this.f1090 = i2;
            this.f1091 = z;
            this.f1092 = i3;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Uri m1870() {
            return this.f1088;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m1871() {
            return this.f1089;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public int m1872() {
            return this.f1090;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public boolean m1873() {
            return this.f1091;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public int m1874() {
            return this.f1092;
        }
    }

    /* renamed from: android.support.v4.ˆ.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: FontsContractCompat */
    public static class C0327 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final int f1086;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final C0328[] f1087;

        public C0327(int i, C0328[] r2) {
            this.f1086 = i;
            this.f1087 = r2;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m1868() {
            return this.f1086;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public C0328[] m1869() {
            return this.f1087;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Map<Uri, ByteBuffer> m1856(Context context, C0328[] r6, CancellationSignal cancellationSignal) {
        HashMap hashMap = new HashMap();
        for (C0328 r3 : r6) {
            if (r3.m1874() == 0) {
                Uri r32 = r3.m1870();
                if (!hashMap.containsKey(r32)) {
                    hashMap.put(r32, C0313.m1824(context, cancellationSignal, r32));
                }
            }
        }
        return Collections.unmodifiableMap(hashMap);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0327 m1852(Context context, CancellationSignal cancellationSignal, C0325 r4) {
        ProviderInfo r0 = m1849(context.getPackageManager(), r4, context.getResources());
        if (r0 == null) {
            return new C0327(1, null);
        }
        return new C0327(0, m1858(context, r4, r0.authority, cancellationSignal));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static ProviderInfo m1849(PackageManager packageManager, C0325 r6, Resources resources) {
        String r0 = r6.m1843();
        ProviderInfo resolveContentProvider = packageManager.resolveContentProvider(r0, 0);
        if (resolveContentProvider == null) {
            throw new PackageManager.NameNotFoundException("No package found for authority: " + r0);
        } else if (resolveContentProvider.packageName.equals(r6.m1844())) {
            List<byte[]> r5 = m1855(packageManager.getPackageInfo(resolveContentProvider.packageName, 64).signatures);
            Collections.sort(r5, f1077);
            List<List<byte[]>> r62 = m1854(r6, resources);
            for (int i = 0; i < r62.size(); i++) {
                ArrayList arrayList = new ArrayList(r62.get(i));
                Collections.sort(arrayList, f1077);
                if (m1857(r5, arrayList)) {
                    return resolveContentProvider;
                }
            }
            return null;
        } else {
            throw new PackageManager.NameNotFoundException("Found content provider " + r0 + ", but package was not " + r6.m1844());
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static List<List<byte[]>> m1854(C0325 r1, Resources resources) {
        if (r1.m1846() != null) {
            return r1.m1846();
        }
        return C0102.m546(resources, r1.m1847());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean m1857(List<byte[]> list, List<byte[]> list2) {
        if (list.size() != list2.size()) {
            return false;
        }
        for (int i = 0; i < list.size(); i++) {
            if (!Arrays.equals(list.get(i), list2.get(i))) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static List<byte[]> m1855(Signature[] signatureArr) {
        ArrayList arrayList = new ArrayList();
        for (Signature byteArray : signatureArr) {
            arrayList.add(byteArray.toByteArray());
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x013b  */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static android.support.v4.ˆ.C0326.C0328[] m1858(android.content.Context r19, android.support.v4.ˆ.C0325 r20, java.lang.String r21, android.os.CancellationSignal r22) {
        /*
            r0 = r21
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            android.net.Uri$Builder r2 = new android.net.Uri$Builder
            r2.<init>()
            java.lang.String r3 = "content"
            android.net.Uri$Builder r2 = r2.scheme(r3)
            android.net.Uri$Builder r2 = r2.authority(r0)
            android.net.Uri r2 = r2.build()
            android.net.Uri$Builder r4 = new android.net.Uri$Builder
            r4.<init>()
            android.net.Uri$Builder r3 = r4.scheme(r3)
            android.net.Uri$Builder r0 = r3.authority(r0)
            java.lang.String r3 = "file"
            android.net.Uri$Builder r0 = r0.appendPath(r3)
            android.net.Uri r0 = r0.build()
            int r4 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0137 }
            r5 = 16
            java.lang.String r7 = "font_variation_settings"
            r9 = 2
            r10 = 7
            java.lang.String r11 = "result_code"
            java.lang.String r12 = "font_italic"
            java.lang.String r13 = "font_weight"
            java.lang.String r14 = "font_ttc_index"
            java.lang.String r15 = "file_id"
            java.lang.String r3 = "_id"
            r6 = 1
            r8 = 0
            if (r4 <= r5) goto L_0x007e
            android.content.ContentResolver r4 = r19.getContentResolver()     // Catch:{ all -> 0x0137 }
            java.lang.String[] r10 = new java.lang.String[r10]     // Catch:{ all -> 0x0137 }
            r10[r8] = r3     // Catch:{ all -> 0x0137 }
            r10[r6] = r15     // Catch:{ all -> 0x0137 }
            r10[r9] = r14     // Catch:{ all -> 0x0137 }
            r5 = 3
            r10[r5] = r7     // Catch:{ all -> 0x0137 }
            r5 = 4
            r10[r5] = r13     // Catch:{ all -> 0x0137 }
            r5 = 5
            r10[r5] = r12     // Catch:{ all -> 0x0137 }
            r5 = 6
            r10[r5] = r11     // Catch:{ all -> 0x0137 }
            java.lang.String r7 = "query = ?"
            java.lang.String[] r9 = new java.lang.String[r6]     // Catch:{ all -> 0x0137 }
            java.lang.String r5 = r20.m1845()     // Catch:{ all -> 0x0137 }
            r9[r8] = r5     // Catch:{ all -> 0x0137 }
            r16 = 0
            r5 = r2
            r18 = r1
            r1 = 1
            r6 = r10
            r10 = 0
            r8 = r9
            r9 = r16
            r1 = 0
            r10 = r22
            android.database.Cursor r4 = r4.query(r5, r6, r7, r8, r9, r10)     // Catch:{ all -> 0x0137 }
            goto L_0x00ab
        L_0x007e:
            r18 = r1
            r1 = 0
            android.content.ContentResolver r4 = r19.getContentResolver()     // Catch:{ all -> 0x0137 }
            java.lang.String[] r6 = new java.lang.String[r10]     // Catch:{ all -> 0x0137 }
            r6[r1] = r3     // Catch:{ all -> 0x0137 }
            r5 = 1
            r6[r5] = r15     // Catch:{ all -> 0x0137 }
            r6[r9] = r14     // Catch:{ all -> 0x0137 }
            r5 = 3
            r6[r5] = r7     // Catch:{ all -> 0x0137 }
            r5 = 4
            r6[r5] = r13     // Catch:{ all -> 0x0137 }
            r5 = 5
            r6[r5] = r12     // Catch:{ all -> 0x0137 }
            r5 = 6
            r6[r5] = r11     // Catch:{ all -> 0x0137 }
            java.lang.String r7 = "query = ?"
            r5 = 1
            java.lang.String[] r8 = new java.lang.String[r5]     // Catch:{ all -> 0x0137 }
            java.lang.String r5 = r20.m1845()     // Catch:{ all -> 0x0137 }
            r8[r1] = r5     // Catch:{ all -> 0x0137 }
            r9 = 0
            r5 = r2
            android.database.Cursor r4 = r4.query(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x0137 }
        L_0x00ab:
            if (r4 == 0) goto L_0x0127
            int r5 = r4.getCount()     // Catch:{ all -> 0x0125 }
            if (r5 <= 0) goto L_0x0127
            int r5 = r4.getColumnIndex(r11)     // Catch:{ all -> 0x0125 }
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ all -> 0x0125 }
            r6.<init>()     // Catch:{ all -> 0x0125 }
            int r3 = r4.getColumnIndex(r3)     // Catch:{ all -> 0x0125 }
            int r7 = r4.getColumnIndex(r15)     // Catch:{ all -> 0x0125 }
            int r8 = r4.getColumnIndex(r14)     // Catch:{ all -> 0x0125 }
            int r9 = r4.getColumnIndex(r13)     // Catch:{ all -> 0x0125 }
            int r10 = r4.getColumnIndex(r12)     // Catch:{ all -> 0x0125 }
        L_0x00d0:
            boolean r11 = r4.moveToNext()     // Catch:{ all -> 0x0125 }
            if (r11 == 0) goto L_0x0129
            r11 = -1
            if (r5 == r11) goto L_0x00e0
            int r12 = r4.getInt(r5)     // Catch:{ all -> 0x0125 }
            r18 = r12
            goto L_0x00e2
        L_0x00e0:
            r18 = 0
        L_0x00e2:
            if (r8 == r11) goto L_0x00ea
            int r12 = r4.getInt(r8)     // Catch:{ all -> 0x0125 }
            r15 = r12
            goto L_0x00eb
        L_0x00ea:
            r15 = 0
        L_0x00eb:
            if (r7 != r11) goto L_0x00f6
            long r12 = r4.getLong(r3)     // Catch:{ all -> 0x0125 }
            android.net.Uri r12 = android.content.ContentUris.withAppendedId(r2, r12)     // Catch:{ all -> 0x0125 }
            goto L_0x00fe
        L_0x00f6:
            long r12 = r4.getLong(r7)     // Catch:{ all -> 0x0125 }
            android.net.Uri r12 = android.content.ContentUris.withAppendedId(r0, r12)     // Catch:{ all -> 0x0125 }
        L_0x00fe:
            r14 = r12
            if (r9 == r11) goto L_0x0108
            int r12 = r4.getInt(r9)     // Catch:{ all -> 0x0125 }
            r16 = r12
            goto L_0x010c
        L_0x0108:
            r12 = 400(0x190, float:5.6E-43)
            r16 = 400(0x190, float:5.6E-43)
        L_0x010c:
            if (r10 == r11) goto L_0x0118
            int r11 = r4.getInt(r10)     // Catch:{ all -> 0x0125 }
            r12 = 1
            if (r11 != r12) goto L_0x0119
            r17 = 1
            goto L_0x011b
        L_0x0118:
            r12 = 1
        L_0x0119:
            r17 = 0
        L_0x011b:
            android.support.v4.ˆ.ʼ$ʼ r11 = new android.support.v4.ˆ.ʼ$ʼ     // Catch:{ all -> 0x0125 }
            r13 = r11
            r13.<init>(r14, r15, r16, r17, r18)     // Catch:{ all -> 0x0125 }
            r6.add(r11)     // Catch:{ all -> 0x0125 }
            goto L_0x00d0
        L_0x0125:
            r0 = move-exception
            goto L_0x0139
        L_0x0127:
            r6 = r18
        L_0x0129:
            if (r4 == 0) goto L_0x012e
            r4.close()
        L_0x012e:
            android.support.v4.ˆ.ʼ$ʼ[] r0 = new android.support.v4.ˆ.C0326.C0328[r1]
            java.lang.Object[] r0 = r6.toArray(r0)
            android.support.v4.ˆ.ʼ$ʼ[] r0 = (android.support.v4.ˆ.C0326.C0328[]) r0
            return r0
        L_0x0137:
            r0 = move-exception
            r4 = 0
        L_0x0139:
            if (r4 == 0) goto L_0x013e
            r4.close()
        L_0x013e:
            goto L_0x0140
        L_0x013f:
            throw r0
        L_0x0140:
            goto L_0x013f
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.ˆ.C0326.m1858(android.content.Context, android.support.v4.ˆ.ʻ, java.lang.String, android.os.CancellationSignal):android.support.v4.ˆ.ʼ$ʼ[]");
    }
}
