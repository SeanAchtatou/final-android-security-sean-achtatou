package android.support.v4.media;

import android.media.MediaDescription;
import android.net.Uri;
import android.support.v4.media.C0138;

/* renamed from: android.support.v4.media.ʼ  reason: contains not printable characters */
/* compiled from: MediaDescriptionCompatApi23 */
class C0140 extends C0138 {
    /* renamed from: ˉ  reason: contains not printable characters */
    public static Uri m845(Object obj) {
        return ((MediaDescription) obj).getMediaUri();
    }

    /* renamed from: android.support.v4.media.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: MediaDescriptionCompatApi23 */
    static class C0141 extends C0138.C0139 {
        /* renamed from: ʼ  reason: contains not printable characters */
        public static void m846(Object obj, Uri uri) {
            ((MediaDescription.Builder) obj).setMediaUri(uri);
        }
    }
}
