package android.support.v4.media.session;

import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.media.session.C0135;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

public final class PlaybackStateCompat implements Parcelable {
    public static final Parcelable.Creator<PlaybackStateCompat> CREATOR = new Parcelable.Creator<PlaybackStateCompat>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public PlaybackStateCompat createFromParcel(Parcel parcel) {
            return new PlaybackStateCompat(parcel);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public PlaybackStateCompat[] newArray(int i) {
            return new PlaybackStateCompat[i];
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    final int f462;

    /* renamed from: ʼ  reason: contains not printable characters */
    final long f463;

    /* renamed from: ʽ  reason: contains not printable characters */
    final long f464;

    /* renamed from: ʾ  reason: contains not printable characters */
    final float f465;

    /* renamed from: ʿ  reason: contains not printable characters */
    final long f466;

    /* renamed from: ˆ  reason: contains not printable characters */
    final int f467;

    /* renamed from: ˈ  reason: contains not printable characters */
    final CharSequence f468;

    /* renamed from: ˉ  reason: contains not printable characters */
    final long f469;

    /* renamed from: ˊ  reason: contains not printable characters */
    List<CustomAction> f470;

    /* renamed from: ˋ  reason: contains not printable characters */
    final long f471;

    /* renamed from: ˎ  reason: contains not printable characters */
    final Bundle f472;

    /* renamed from: ˏ  reason: contains not printable characters */
    private Object f473;

    public int describeContents() {
        return 0;
    }

    PlaybackStateCompat(int i, long j, long j2, float f, long j3, int i2, CharSequence charSequence, long j4, List<CustomAction> list, long j5, Bundle bundle) {
        this.f462 = i;
        this.f463 = j;
        this.f464 = j2;
        this.f465 = f;
        this.f466 = j3;
        this.f467 = i2;
        this.f468 = charSequence;
        this.f469 = j4;
        this.f470 = new ArrayList(list);
        this.f471 = j5;
        this.f472 = bundle;
    }

    PlaybackStateCompat(Parcel parcel) {
        this.f462 = parcel.readInt();
        this.f463 = parcel.readLong();
        this.f465 = parcel.readFloat();
        this.f469 = parcel.readLong();
        this.f464 = parcel.readLong();
        this.f466 = parcel.readLong();
        this.f468 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f470 = parcel.createTypedArrayList(CustomAction.CREATOR);
        this.f471 = parcel.readLong();
        this.f472 = parcel.readBundle();
        this.f467 = parcel.readInt();
    }

    public String toString() {
        return "PlaybackState {" + "state=" + this.f462 + ", position=" + this.f463 + ", buffered position=" + this.f464 + ", speed=" + this.f465 + ", updated=" + this.f469 + ", actions=" + this.f466 + ", error code=" + this.f467 + ", error message=" + this.f468 + ", custom actions=" + this.f470 + ", active item id=" + this.f471 + "}";
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f462);
        parcel.writeLong(this.f463);
        parcel.writeFloat(this.f465);
        parcel.writeLong(this.f469);
        parcel.writeLong(this.f464);
        parcel.writeLong(this.f466);
        TextUtils.writeToParcel(this.f468, parcel, i);
        parcel.writeTypedList(this.f470);
        parcel.writeLong(this.f471);
        parcel.writeBundle(this.f472);
        parcel.writeInt(this.f467);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static PlaybackStateCompat m669(Object obj) {
        ArrayList arrayList;
        Object obj2 = obj;
        Bundle bundle = null;
        if (obj2 == null || Build.VERSION.SDK_INT < 21) {
            return null;
        }
        List<Object> r2 = C0135.m820(obj);
        if (r2 != null) {
            ArrayList arrayList2 = new ArrayList(r2.size());
            for (Object r4 : r2) {
                arrayList2.add(CustomAction.m672(r4));
            }
            arrayList = arrayList2;
        } else {
            arrayList = null;
        }
        if (Build.VERSION.SDK_INT >= 22) {
            bundle = C0137.m826(obj);
        }
        PlaybackStateCompat playbackStateCompat = new PlaybackStateCompat(C0135.m813(obj), C0135.m814(obj), C0135.m815(obj), C0135.m816(obj), C0135.m817(obj), 0, C0135.m818(obj), C0135.m819(obj), arrayList, C0135.m821(obj), bundle);
        playbackStateCompat.f473 = obj2;
        return playbackStateCompat;
    }

    public static final class CustomAction implements Parcelable {
        public static final Parcelable.Creator<CustomAction> CREATOR = new Parcelable.Creator<CustomAction>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public CustomAction createFromParcel(Parcel parcel) {
                return new CustomAction(parcel);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public CustomAction[] newArray(int i) {
                return new CustomAction[i];
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        private final String f474;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final CharSequence f475;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final int f476;

        /* renamed from: ʾ  reason: contains not printable characters */
        private final Bundle f477;

        /* renamed from: ʿ  reason: contains not printable characters */
        private Object f478;

        public int describeContents() {
            return 0;
        }

        CustomAction(String str, CharSequence charSequence, int i, Bundle bundle) {
            this.f474 = str;
            this.f475 = charSequence;
            this.f476 = i;
            this.f477 = bundle;
        }

        CustomAction(Parcel parcel) {
            this.f474 = parcel.readString();
            this.f475 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
            this.f476 = parcel.readInt();
            this.f477 = parcel.readBundle();
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f474);
            TextUtils.writeToParcel(this.f475, parcel, i);
            parcel.writeInt(this.f476);
            parcel.writeBundle(this.f477);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static CustomAction m672(Object obj) {
            if (obj == null || Build.VERSION.SDK_INT < 21) {
                return null;
            }
            CustomAction customAction = new CustomAction(C0135.C0136.m822(obj), C0135.C0136.m823(obj), C0135.C0136.m824(obj), C0135.C0136.m825(obj));
            customAction.f478 = obj;
            return customAction;
        }

        public String toString() {
            return "Action:mName='" + ((Object) this.f475) + ", mIcon=" + this.f476 + ", mExtras=" + this.f477;
        }
    }
}
