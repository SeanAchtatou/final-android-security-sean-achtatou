package android.support.v4.media;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.ʿ.C0321;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public final class MediaBrowserCompat {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final boolean f399 = Log.isLoggable("MediaBrowserCompat", 3);

    /* renamed from: android.support.v4.media.MediaBrowserCompat$ʻ  reason: contains not printable characters */
    public static abstract class C0113 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m592(String str, Bundle bundle, Bundle bundle2) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m593(String str, Bundle bundle, Bundle bundle2) {
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m594(String str, Bundle bundle, Bundle bundle2) {
        }
    }

    /* renamed from: android.support.v4.media.MediaBrowserCompat$ʼ  reason: contains not printable characters */
    public static abstract class C0114 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m595(MediaItem mediaItem) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m596(String str) {
        }
    }

    /* renamed from: android.support.v4.media.MediaBrowserCompat$ʽ  reason: contains not printable characters */
    public static abstract class C0115 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m597(String str, Bundle bundle) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m598(String str, Bundle bundle, List<MediaItem> list) {
        }
    }

    public static class MediaItem implements Parcelable {
        public static final Parcelable.Creator<MediaItem> CREATOR = new Parcelable.Creator<MediaItem>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public MediaItem createFromParcel(Parcel parcel) {
                return new MediaItem(parcel);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public MediaItem[] newArray(int i) {
                return new MediaItem[i];
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        private final int f405;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final MediaDescriptionCompat f406;

        public int describeContents() {
            return 0;
        }

        MediaItem(Parcel parcel) {
            this.f405 = parcel.readInt();
            this.f406 = MediaDescriptionCompat.CREATOR.createFromParcel(parcel);
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f405);
            this.f406.writeToParcel(parcel, i);
        }

        public String toString() {
            return "MediaItem{" + "mFlags=" + this.f405 + ", mDescription=" + this.f406 + '}';
        }
    }

    private static class ItemReceiver extends C0321 {

        /* renamed from: ʾ  reason: contains not printable characters */
        private final String f403;

        /* renamed from: ʿ  reason: contains not printable characters */
        private final C0114 f404;

        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m588(int i, Bundle bundle) {
            if (bundle != null) {
                bundle.setClassLoader(MediaBrowserCompat.class.getClassLoader());
            }
            if (i != 0 || bundle == null || !bundle.containsKey("media_item")) {
                this.f404.m596(this.f403);
                return;
            }
            Parcelable parcelable = bundle.getParcelable("media_item");
            if (parcelable == null || (parcelable instanceof MediaItem)) {
                this.f404.m595((MediaItem) parcelable);
            } else {
                this.f404.m596(this.f403);
            }
        }
    }

    private static class SearchResultReceiver extends C0321 {

        /* renamed from: ʾ  reason: contains not printable characters */
        private final String f407;

        /* renamed from: ʿ  reason: contains not printable characters */
        private final Bundle f408;

        /* renamed from: ˆ  reason: contains not printable characters */
        private final C0115 f409;

        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m591(int i, Bundle bundle) {
            if (bundle != null) {
                bundle.setClassLoader(MediaBrowserCompat.class.getClassLoader());
            }
            if (i != 0 || bundle == null || !bundle.containsKey("search_results")) {
                this.f409.m597(this.f407, this.f408);
                return;
            }
            Parcelable[] parcelableArray = bundle.getParcelableArray("search_results");
            ArrayList arrayList = null;
            if (parcelableArray != null) {
                arrayList = new ArrayList();
                for (Parcelable parcelable : parcelableArray) {
                    arrayList.add((MediaItem) parcelable);
                }
            }
            this.f409.m598(this.f407, this.f408, arrayList);
        }
    }

    private static class CustomActionResultReceiver extends C0321 {

        /* renamed from: ʾ  reason: contains not printable characters */
        private final String f400;

        /* renamed from: ʿ  reason: contains not printable characters */
        private final Bundle f401;

        /* renamed from: ˆ  reason: contains not printable characters */
        private final C0113 f402;

        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m587(int i, Bundle bundle) {
            C0113 r0 = this.f402;
            if (r0 != null) {
                if (i == -1) {
                    r0.m594(this.f400, this.f401, bundle);
                } else if (i == 0) {
                    r0.m593(this.f400, this.f401, bundle);
                } else if (i != 1) {
                    Log.w("MediaBrowserCompat", "Unknown result code: " + i + " (extras=" + this.f401 + ", resultData=" + bundle + ")");
                } else {
                    r0.m592(this.f400, this.f401, bundle);
                }
            }
        }
    }
}
