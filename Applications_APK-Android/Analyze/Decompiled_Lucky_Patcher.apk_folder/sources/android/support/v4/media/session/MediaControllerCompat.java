package android.support.v4.media.session;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.C0123;
import android.support.v4.media.session.C0126;
import android.support.v4.media.session.C0129;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.ʻ.C0217;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;

public final class MediaControllerCompat {

    /* renamed from: android.support.v4.media.session.MediaControllerCompat$ʻ  reason: contains not printable characters */
    public static abstract class C0118 implements IBinder.DeathRecipient {

        /* renamed from: ʻ  reason: contains not printable characters */
        C0119 f439;

        /* renamed from: ʼ  reason: contains not printable characters */
        boolean f440;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final Object f441;

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m626() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m627(int i) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m629(Bundle bundle) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m630(MediaMetadataCompat mediaMetadataCompat) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m631(C0122 r1) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m632(PlaybackStateCompat playbackStateCompat) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m633(CharSequence charSequence) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m634(String str, Bundle bundle) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m635(List<MediaSessionCompat.QueueItem> list) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m636(boolean z) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m637(int i) {
        }

        @Deprecated
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m638(boolean z) {
        }

        public C0118() {
            if (Build.VERSION.SDK_INT >= 21) {
                this.f441 = C0129.m799(new C0120(this));
            } else {
                this.f441 = new C0121(this);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m628(int i, Object obj, Bundle bundle) {
            C0119 r0 = this.f439;
            if (r0 != null) {
                Message obtainMessage = r0.obtainMessage(i, obj);
                obtainMessage.setData(bundle);
                obtainMessage.sendToTarget();
            }
        }

        /* renamed from: android.support.v4.media.session.MediaControllerCompat$ʻ$ʼ  reason: contains not printable characters */
        private static class C0120 implements C0129.C0130 {

            /* renamed from: ʻ  reason: contains not printable characters */
            private final WeakReference<C0118> f444;

            C0120(C0118 r2) {
                this.f444 = new WeakReference<>(r2);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m639() {
                C0118 r0 = this.f444.get();
                if (r0 != null) {
                    r0.m626();
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m644(String str, Bundle bundle) {
                C0118 r0 = this.f444.get();
                if (r0 == null) {
                    return;
                }
                if (!r0.f440 || Build.VERSION.SDK_INT >= 23) {
                    r0.m634(str, bundle);
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m643(Object obj) {
                C0118 r0 = this.f444.get();
                if (r0 != null && !r0.f440) {
                    r0.m632(PlaybackStateCompat.m669(obj));
                }
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public void m646(Object obj) {
                C0118 r0 = this.f444.get();
                if (r0 != null) {
                    r0.m630(MediaMetadataCompat.m612(obj));
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m645(List<?> list) {
                C0118 r0 = this.f444.get();
                if (r0 != null) {
                    r0.m635(MediaSessionCompat.QueueItem.m660(list));
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m642(CharSequence charSequence) {
                C0118 r0 = this.f444.get();
                if (r0 != null) {
                    r0.m633(charSequence);
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m641(Bundle bundle) {
                C0118 r0 = this.f444.get();
                if (r0 != null) {
                    r0.m629(bundle);
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m640(int i, int i2, int i3, int i4, int i5) {
                C0118 r0 = this.f444.get();
                if (r0 != null) {
                    r0.m631(new C0122(i, i2, i3, i4, i5));
                }
            }
        }

        /* renamed from: android.support.v4.media.session.MediaControllerCompat$ʻ$ʽ  reason: contains not printable characters */
        private static class C0121 extends C0123.C0124 {

            /* renamed from: ʻ  reason: contains not printable characters */
            private final WeakReference<C0118> f445;

            C0121(C0118 r2) {
                this.f445 = new WeakReference<>(r2);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m654(String str, Bundle bundle) {
                C0118 r0 = this.f445.get();
                if (r0 != null) {
                    r0.m628(1, str, bundle);
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m647() {
                C0118 r0 = this.f445.get();
                if (r0 != null) {
                    r0.m628(8, null, null);
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m652(PlaybackStateCompat playbackStateCompat) {
                C0118 r0 = this.f445.get();
                if (r0 != null) {
                    r0.m628(2, playbackStateCompat, null);
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m650(MediaMetadataCompat mediaMetadataCompat) {
                C0118 r0 = this.f445.get();
                if (r0 != null) {
                    r0.m628(3, mediaMetadataCompat, null);
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m655(List<MediaSessionCompat.QueueItem> list) {
                C0118 r0 = this.f445.get();
                if (r0 != null) {
                    r0.m628(5, list, null);
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m653(CharSequence charSequence) {
                C0118 r0 = this.f445.get();
                if (r0 != null) {
                    r0.m628(6, charSequence, null);
                }
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public void m658(boolean z) {
                C0118 r0 = this.f445.get();
                if (r0 != null) {
                    r0.m628(11, Boolean.valueOf(z), null);
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m648(int i) {
                C0118 r0 = this.f445.get();
                if (r0 != null) {
                    r0.m628(9, Integer.valueOf(i), null);
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m656(boolean z) {
                C0118 r0 = this.f445.get();
                if (r0 != null) {
                    r0.m628(10, Boolean.valueOf(z), null);
                }
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public void m657(int i) {
                C0118 r0 = this.f445.get();
                if (r0 != null) {
                    r0.m628(12, Integer.valueOf(i), null);
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m649(Bundle bundle) {
                C0118 r0 = this.f445.get();
                if (r0 != null) {
                    r0.m628(7, bundle, null);
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m651(ParcelableVolumeInfo parcelableVolumeInfo) {
                C0118 r0 = this.f445.get();
                if (r0 != null) {
                    r0.m628(4, parcelableVolumeInfo != null ? new C0122(parcelableVolumeInfo.f457, parcelableVolumeInfo.f458, parcelableVolumeInfo.f459, parcelableVolumeInfo.f460, parcelableVolumeInfo.f461) : null, null);
                }
            }
        }

        /* renamed from: android.support.v4.media.session.MediaControllerCompat$ʻ$ʻ  reason: contains not printable characters */
        private class C0119 extends Handler {

            /* renamed from: ʻ  reason: contains not printable characters */
            boolean f442;

            /* renamed from: ʼ  reason: contains not printable characters */
            final /* synthetic */ C0118 f443;

            public void handleMessage(Message message) {
                if (this.f442) {
                    switch (message.what) {
                        case 1:
                            this.f443.m634((String) message.obj, message.getData());
                            return;
                        case 2:
                            this.f443.m632((PlaybackStateCompat) message.obj);
                            return;
                        case 3:
                            this.f443.m630((MediaMetadataCompat) message.obj);
                            return;
                        case 4:
                            this.f443.m631((C0122) message.obj);
                            return;
                        case 5:
                            this.f443.m635((List) message.obj);
                            return;
                        case 6:
                            this.f443.m633((CharSequence) message.obj);
                            return;
                        case 7:
                            this.f443.m629((Bundle) message.obj);
                            return;
                        case 8:
                            this.f443.m626();
                            return;
                        case 9:
                            this.f443.m627(((Integer) message.obj).intValue());
                            return;
                        case 10:
                            this.f443.m638(((Boolean) message.obj).booleanValue());
                            return;
                        case 11:
                            this.f443.m636(((Boolean) message.obj).booleanValue());
                            return;
                        case 12:
                            this.f443.m637(((Integer) message.obj).intValue());
                            return;
                        default:
                            return;
                    }
                }
            }
        }
    }

    /* renamed from: android.support.v4.media.session.MediaControllerCompat$ʼ  reason: contains not printable characters */
    public static final class C0122 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final int f446;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final int f447;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final int f448;

        /* renamed from: ʾ  reason: contains not printable characters */
        private final int f449;

        /* renamed from: ʿ  reason: contains not printable characters */
        private final int f450;

        C0122(int i, int i2, int i3, int i4, int i5) {
            this.f446 = i;
            this.f447 = i2;
            this.f448 = i3;
            this.f449 = i4;
            this.f450 = i5;
        }
    }

    static class MediaControllerImplApi21 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final List<C0118> f435;
        /* access modifiers changed from: private */

        /* renamed from: ʼ  reason: contains not printable characters */
        public C0126 f436;

        /* renamed from: ʽ  reason: contains not printable characters */
        private HashMap<C0118, C0117> f437;

        /* access modifiers changed from: private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m618() {
            if (this.f436 != null) {
                synchronized (this.f435) {
                    for (C0118 next : this.f435) {
                        C0117 r3 = new C0117(next);
                        this.f437.put(next, r3);
                        next.f440 = true;
                        try {
                            this.f436.m708(r3);
                        } catch (RemoteException e) {
                            Log.e("MediaControllerCompat", "Dead object in registerCallback.", e);
                        }
                    }
                    this.f435.clear();
                }
            }
        }

        private static class ExtraBinderRequestResultReceiver extends ResultReceiver {

            /* renamed from: ʻ  reason: contains not printable characters */
            private WeakReference<MediaControllerImplApi21> f438;

            /* access modifiers changed from: protected */
            public void onReceiveResult(int i, Bundle bundle) {
                MediaControllerImplApi21 mediaControllerImplApi21 = this.f438.get();
                if (mediaControllerImplApi21 != null && bundle != null) {
                    C0126 unused = mediaControllerImplApi21.f436 = C0126.C0127.m749(C0217.m1182(bundle, "android.support.v4.media.session.EXTRA_BINDER"));
                    mediaControllerImplApi21.m618();
                }
            }
        }

        /* renamed from: android.support.v4.media.session.MediaControllerCompat$MediaControllerImplApi21$ʻ  reason: contains not printable characters */
        private static class C0117 extends C0118.C0121 {
            C0117(C0118 r1) {
                super(r1);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m620() {
                throw new AssertionError();
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m622(MediaMetadataCompat mediaMetadataCompat) {
                throw new AssertionError();
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m625(List<MediaSessionCompat.QueueItem> list) {
                throw new AssertionError();
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m624(CharSequence charSequence) {
                throw new AssertionError();
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m621(Bundle bundle) {
                throw new AssertionError();
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m623(ParcelableVolumeInfo parcelableVolumeInfo) {
                throw new AssertionError();
            }
        }
    }
}
