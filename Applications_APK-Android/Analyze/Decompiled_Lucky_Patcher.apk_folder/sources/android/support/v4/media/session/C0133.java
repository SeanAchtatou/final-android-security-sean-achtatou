package android.support.v4.media.session;

import android.media.session.MediaSession;

/* renamed from: android.support.v4.media.session.ʾ  reason: contains not printable characters */
/* compiled from: MediaSessionCompatApi21 */
class C0133 {

    /* renamed from: android.support.v4.media.session.ʾ$ʻ  reason: contains not printable characters */
    /* compiled from: MediaSessionCompatApi21 */
    static class C0134 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public static Object m811(Object obj) {
            return ((MediaSession.QueueItem) obj).getDescription();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public static long m812(Object obj) {
            return ((MediaSession.QueueItem) obj).getQueueId();
        }
    }
}
