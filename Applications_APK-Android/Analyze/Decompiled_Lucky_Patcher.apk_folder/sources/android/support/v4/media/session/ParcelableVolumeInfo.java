package android.support.v4.media.session;

import android.os.Parcel;
import android.os.Parcelable;

public class ParcelableVolumeInfo implements Parcelable {
    public static final Parcelable.Creator<ParcelableVolumeInfo> CREATOR = new Parcelable.Creator<ParcelableVolumeInfo>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public ParcelableVolumeInfo createFromParcel(Parcel parcel) {
            return new ParcelableVolumeInfo(parcel);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public ParcelableVolumeInfo[] newArray(int i) {
            return new ParcelableVolumeInfo[i];
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    public int f457;

    /* renamed from: ʼ  reason: contains not printable characters */
    public int f458;

    /* renamed from: ʽ  reason: contains not printable characters */
    public int f459;

    /* renamed from: ʾ  reason: contains not printable characters */
    public int f460;

    /* renamed from: ʿ  reason: contains not printable characters */
    public int f461;

    public int describeContents() {
        return 0;
    }

    public ParcelableVolumeInfo(Parcel parcel) {
        this.f457 = parcel.readInt();
        this.f459 = parcel.readInt();
        this.f460 = parcel.readInt();
        this.f461 = parcel.readInt();
        this.f458 = parcel.readInt();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f457);
        parcel.writeInt(this.f459);
        parcel.writeInt(this.f460);
        parcel.writeInt(this.f461);
        parcel.writeInt(this.f458);
    }
}
