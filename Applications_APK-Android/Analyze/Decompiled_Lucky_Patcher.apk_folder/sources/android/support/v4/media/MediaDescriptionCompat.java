package android.support.v4.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.media.C0138;
import android.support.v4.media.C0140;
import android.text.TextUtils;

public final class MediaDescriptionCompat implements Parcelable {
    public static final Parcelable.Creator<MediaDescriptionCompat> CREATOR = new Parcelable.Creator<MediaDescriptionCompat>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public MediaDescriptionCompat createFromParcel(Parcel parcel) {
            if (Build.VERSION.SDK_INT < 21) {
                return new MediaDescriptionCompat(parcel);
            }
            return MediaDescriptionCompat.m599(C0138.m827(parcel));
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public MediaDescriptionCompat[] newArray(int i) {
            return new MediaDescriptionCompat[i];
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f410;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final CharSequence f411;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final CharSequence f412;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final CharSequence f413;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final Bitmap f414;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final Uri f415;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final Bundle f416;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final Uri f417;

    /* renamed from: ˊ  reason: contains not printable characters */
    private Object f418;

    public int describeContents() {
        return 0;
    }

    MediaDescriptionCompat(String str, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, Bitmap bitmap, Uri uri, Bundle bundle, Uri uri2) {
        this.f410 = str;
        this.f411 = charSequence;
        this.f412 = charSequence2;
        this.f413 = charSequence3;
        this.f414 = bitmap;
        this.f415 = uri;
        this.f416 = bundle;
        this.f417 = uri2;
    }

    MediaDescriptionCompat(Parcel parcel) {
        this.f410 = parcel.readString();
        this.f411 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f412 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f413 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f414 = (Bitmap) parcel.readParcelable(null);
        this.f415 = (Uri) parcel.readParcelable(null);
        this.f416 = parcel.readBundle();
        this.f417 = (Uri) parcel.readParcelable(null);
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (Build.VERSION.SDK_INT < 21) {
            parcel.writeString(this.f410);
            TextUtils.writeToParcel(this.f411, parcel, i);
            TextUtils.writeToParcel(this.f412, parcel, i);
            TextUtils.writeToParcel(this.f413, parcel, i);
            parcel.writeParcelable(this.f414, i);
            parcel.writeParcelable(this.f415, i);
            parcel.writeBundle(this.f416);
            parcel.writeParcelable(this.f417, i);
            return;
        }
        C0138.m829(m600(), parcel, i);
    }

    public String toString() {
        return ((Object) this.f411) + ", " + ((Object) this.f412) + ", " + ((Object) this.f413);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Object m600() {
        if (this.f418 != null || Build.VERSION.SDK_INT < 21) {
            return this.f418;
        }
        Object r0 = C0138.C0139.m836();
        C0138.C0139.m842(r0, this.f410);
        C0138.C0139.m841(r0, this.f411);
        C0138.C0139.m843(r0, this.f412);
        C0138.C0139.m844(r0, this.f413);
        C0138.C0139.m838(r0, this.f414);
        C0138.C0139.m839(r0, this.f415);
        Bundle bundle = this.f416;
        if (Build.VERSION.SDK_INT < 23 && this.f417 != null) {
            if (bundle == null) {
                bundle = new Bundle();
                bundle.putBoolean("android.support.v4.media.description.NULL_BUNDLE_FLAG", true);
            }
            bundle.putParcelable("android.support.v4.media.description.MEDIA_URI", this.f417);
        }
        C0138.C0139.m840(r0, bundle);
        if (Build.VERSION.SDK_INT >= 23) {
            C0140.C0141.m846(r0, this.f417);
        }
        this.f418 = C0138.C0139.m837(r0);
        return this.f418;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006a  */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.support.v4.media.MediaDescriptionCompat m599(java.lang.Object r8) {
        /*
            r0 = 0
            if (r8 == 0) goto L_0x007d
            int r1 = android.os.Build.VERSION.SDK_INT
            r2 = 21
            if (r1 < r2) goto L_0x007d
            android.support.v4.media.MediaDescriptionCompat$ʻ r1 = new android.support.v4.media.MediaDescriptionCompat$ʻ
            r1.<init>()
            java.lang.String r2 = android.support.v4.media.C0138.m828(r8)
            r1.m607(r2)
            java.lang.CharSequence r2 = android.support.v4.media.C0138.m830(r8)
            r1.m606(r2)
            java.lang.CharSequence r2 = android.support.v4.media.C0138.m831(r8)
            r1.m610(r2)
            java.lang.CharSequence r2 = android.support.v4.media.C0138.m832(r8)
            r1.m611(r2)
            android.graphics.Bitmap r2 = android.support.v4.media.C0138.m833(r8)
            r1.m603(r2)
            android.net.Uri r2 = android.support.v4.media.C0138.m834(r8)
            r1.m604(r2)
            android.os.Bundle r2 = android.support.v4.media.C0138.m835(r8)
            java.lang.String r3 = "android.support.v4.media.description.MEDIA_URI"
            if (r2 != 0) goto L_0x0042
            r4 = r0
            goto L_0x0048
        L_0x0042:
            android.os.Parcelable r4 = r2.getParcelable(r3)
            android.net.Uri r4 = (android.net.Uri) r4
        L_0x0048:
            if (r4 == 0) goto L_0x0060
            java.lang.String r5 = "android.support.v4.media.description.NULL_BUNDLE_FLAG"
            boolean r6 = r2.containsKey(r5)
            if (r6 == 0) goto L_0x005a
            int r6 = r2.size()
            r7 = 2
            if (r6 != r7) goto L_0x005a
            goto L_0x0061
        L_0x005a:
            r2.remove(r3)
            r2.remove(r5)
        L_0x0060:
            r0 = r2
        L_0x0061:
            r1.m605(r0)
            if (r4 == 0) goto L_0x006a
            r1.m609(r4)
            goto L_0x0077
        L_0x006a:
            int r0 = android.os.Build.VERSION.SDK_INT
            r2 = 23
            if (r0 < r2) goto L_0x0077
            android.net.Uri r0 = android.support.v4.media.C0140.m845(r8)
            r1.m609(r0)
        L_0x0077:
            android.support.v4.media.MediaDescriptionCompat r0 = r1.m608()
            r0.f418 = r8
        L_0x007d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.media.MediaDescriptionCompat.m599(java.lang.Object):android.support.v4.media.MediaDescriptionCompat");
    }

    /* renamed from: android.support.v4.media.MediaDescriptionCompat$ʻ  reason: contains not printable characters */
    public static final class C0116 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private String f419;

        /* renamed from: ʼ  reason: contains not printable characters */
        private CharSequence f420;

        /* renamed from: ʽ  reason: contains not printable characters */
        private CharSequence f421;

        /* renamed from: ʾ  reason: contains not printable characters */
        private CharSequence f422;

        /* renamed from: ʿ  reason: contains not printable characters */
        private Bitmap f423;

        /* renamed from: ˆ  reason: contains not printable characters */
        private Uri f424;

        /* renamed from: ˈ  reason: contains not printable characters */
        private Bundle f425;

        /* renamed from: ˉ  reason: contains not printable characters */
        private Uri f426;

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0116 m607(String str) {
            this.f419 = str;
            return this;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0116 m606(CharSequence charSequence) {
            this.f420 = charSequence;
            return this;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public C0116 m610(CharSequence charSequence) {
            this.f421 = charSequence;
            return this;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public C0116 m611(CharSequence charSequence) {
            this.f422 = charSequence;
            return this;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0116 m603(Bitmap bitmap) {
            this.f423 = bitmap;
            return this;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0116 m604(Uri uri) {
            this.f424 = uri;
            return this;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0116 m605(Bundle bundle) {
            this.f425 = bundle;
            return this;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public C0116 m609(Uri uri) {
            this.f426 = uri;
            return this;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public MediaDescriptionCompat m608() {
            return new MediaDescriptionCompat(this.f419, this.f420, this.f421, this.f422, this.f423, this.f424, this.f425, this.f426);
        }
    }
}
