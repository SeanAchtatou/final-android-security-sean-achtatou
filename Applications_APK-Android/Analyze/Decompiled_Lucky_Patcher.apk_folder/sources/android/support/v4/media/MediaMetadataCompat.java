package android.support.v4.media;

import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.ˈ.C0331;

public final class MediaMetadataCompat implements Parcelable {
    public static final Parcelable.Creator<MediaMetadataCompat> CREATOR = new Parcelable.Creator<MediaMetadataCompat>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public MediaMetadataCompat createFromParcel(Parcel parcel) {
            return new MediaMetadataCompat(parcel);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public MediaMetadataCompat[] newArray(int i) {
            return new MediaMetadataCompat[i];
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0331<String, Integer> f427 = new C0331<>();

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final String[] f428 = {"android.media.metadata.TITLE", "android.media.metadata.ARTIST", "android.media.metadata.ALBUM", "android.media.metadata.ALBUM_ARTIST", "android.media.metadata.WRITER", "android.media.metadata.AUTHOR", "android.media.metadata.COMPOSER"};

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final String[] f429 = {"android.media.metadata.DISPLAY_ICON", "android.media.metadata.ART", "android.media.metadata.ALBUM_ART"};

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final String[] f430 = {"android.media.metadata.DISPLAY_ICON_URI", "android.media.metadata.ART_URI", "android.media.metadata.ALBUM_ART_URI"};

    /* renamed from: ʼ  reason: contains not printable characters */
    final Bundle f431;

    /* renamed from: ˆ  reason: contains not printable characters */
    private Object f432;

    public int describeContents() {
        return 0;
    }

    static {
        f427.put("android.media.metadata.TITLE", 1);
        f427.put("android.media.metadata.ARTIST", 1);
        f427.put("android.media.metadata.DURATION", 0);
        f427.put("android.media.metadata.ALBUM", 1);
        f427.put("android.media.metadata.AUTHOR", 1);
        f427.put("android.media.metadata.WRITER", 1);
        f427.put("android.media.metadata.COMPOSER", 1);
        f427.put("android.media.metadata.COMPILATION", 1);
        f427.put("android.media.metadata.DATE", 1);
        f427.put("android.media.metadata.YEAR", 0);
        f427.put("android.media.metadata.GENRE", 1);
        f427.put("android.media.metadata.TRACK_NUMBER", 0);
        f427.put("android.media.metadata.NUM_TRACKS", 0);
        f427.put("android.media.metadata.DISC_NUMBER", 0);
        f427.put("android.media.metadata.ALBUM_ARTIST", 1);
        f427.put("android.media.metadata.ART", 2);
        f427.put("android.media.metadata.ART_URI", 1);
        f427.put("android.media.metadata.ALBUM_ART", 2);
        f427.put("android.media.metadata.ALBUM_ART_URI", 1);
        f427.put("android.media.metadata.USER_RATING", 3);
        f427.put("android.media.metadata.RATING", 3);
        f427.put("android.media.metadata.DISPLAY_TITLE", 1);
        f427.put("android.media.metadata.DISPLAY_SUBTITLE", 1);
        f427.put("android.media.metadata.DISPLAY_DESCRIPTION", 1);
        f427.put("android.media.metadata.DISPLAY_ICON", 2);
        f427.put("android.media.metadata.DISPLAY_ICON_URI", 1);
        f427.put("android.media.metadata.MEDIA_ID", 1);
        f427.put("android.media.metadata.BT_FOLDER_TYPE", 0);
        f427.put("android.media.metadata.MEDIA_URI", 1);
        f427.put("android.media.metadata.ADVERTISEMENT", 0);
        f427.put("android.media.metadata.DOWNLOAD_STATUS", 0);
    }

    MediaMetadataCompat(Parcel parcel) {
        this.f431 = parcel.readBundle();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeBundle(this.f431);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static MediaMetadataCompat m612(Object obj) {
        if (obj == null || Build.VERSION.SDK_INT < 21) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        C0142.m847(obj, obtain, 0);
        obtain.setDataPosition(0);
        MediaMetadataCompat createFromParcel = CREATOR.createFromParcel(obtain);
        obtain.recycle();
        createFromParcel.f432 = obj;
        return createFromParcel;
    }
}
