package android.support.v4.media.session;

import android.media.session.PlaybackState;
import android.os.Bundle;
import java.util.List;

/* renamed from: android.support.v4.media.session.ʿ  reason: contains not printable characters */
/* compiled from: PlaybackStateCompatApi21 */
class C0135 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m813(Object obj) {
        return ((PlaybackState) obj).getState();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static long m814(Object obj) {
        return ((PlaybackState) obj).getPosition();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static long m815(Object obj) {
        return ((PlaybackState) obj).getBufferedPosition();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static float m816(Object obj) {
        return ((PlaybackState) obj).getPlaybackSpeed();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public static long m817(Object obj) {
        return ((PlaybackState) obj).getActions();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public static CharSequence m818(Object obj) {
        return ((PlaybackState) obj).getErrorMessage();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public static long m819(Object obj) {
        return ((PlaybackState) obj).getLastPositionUpdateTime();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public static List<Object> m820(Object obj) {
        return ((PlaybackState) obj).getCustomActions();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static long m821(Object obj) {
        return ((PlaybackState) obj).getActiveQueueItemId();
    }

    /* renamed from: android.support.v4.media.session.ʿ$ʻ  reason: contains not printable characters */
    /* compiled from: PlaybackStateCompatApi21 */
    static final class C0136 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public static String m822(Object obj) {
            return ((PlaybackState.CustomAction) obj).getAction();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public static CharSequence m823(Object obj) {
            return ((PlaybackState.CustomAction) obj).getName();
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public static int m824(Object obj) {
            return ((PlaybackState.CustomAction) obj).getIcon();
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public static Bundle m825(Object obj) {
            return ((PlaybackState.CustomAction) obj).getExtras();
        }
    }
}
