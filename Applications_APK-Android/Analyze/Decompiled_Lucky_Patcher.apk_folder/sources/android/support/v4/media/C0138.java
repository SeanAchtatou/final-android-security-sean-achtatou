package android.support.v4.media;

import android.graphics.Bitmap;
import android.media.MediaDescription;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;

/* renamed from: android.support.v4.media.ʻ  reason: contains not printable characters */
/* compiled from: MediaDescriptionCompatApi21 */
class C0138 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m828(Object obj) {
        return ((MediaDescription) obj).getMediaId();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static CharSequence m830(Object obj) {
        return ((MediaDescription) obj).getTitle();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static CharSequence m831(Object obj) {
        return ((MediaDescription) obj).getSubtitle();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static CharSequence m832(Object obj) {
        return ((MediaDescription) obj).getDescription();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public static Bitmap m833(Object obj) {
        return ((MediaDescription) obj).getIconBitmap();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public static Uri m834(Object obj) {
        return ((MediaDescription) obj).getIconUri();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public static Bundle m835(Object obj) {
        return ((MediaDescription) obj).getExtras();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m829(Object obj, Parcel parcel, int i) {
        ((MediaDescription) obj).writeToParcel(parcel, i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Object m827(Parcel parcel) {
        return MediaDescription.CREATOR.createFromParcel(parcel);
    }

    /* renamed from: android.support.v4.media.ʻ$ʻ  reason: contains not printable characters */
    /* compiled from: MediaDescriptionCompatApi21 */
    static class C0139 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public static Object m836() {
            return new MediaDescription.Builder();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static void m842(Object obj, String str) {
            ((MediaDescription.Builder) obj).setMediaId(str);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static void m841(Object obj, CharSequence charSequence) {
            ((MediaDescription.Builder) obj).setTitle(charSequence);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public static void m843(Object obj, CharSequence charSequence) {
            ((MediaDescription.Builder) obj).setSubtitle(charSequence);
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public static void m844(Object obj, CharSequence charSequence) {
            ((MediaDescription.Builder) obj).setDescription(charSequence);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static void m838(Object obj, Bitmap bitmap) {
            ((MediaDescription.Builder) obj).setIconBitmap(bitmap);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static void m839(Object obj, Uri uri) {
            ((MediaDescription.Builder) obj).setIconUri(uri);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static void m840(Object obj, Bundle bundle) {
            ((MediaDescription.Builder) obj).setExtras(bundle);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static Object m837(Object obj) {
            return ((MediaDescription.Builder) obj).build();
        }
    }
}
