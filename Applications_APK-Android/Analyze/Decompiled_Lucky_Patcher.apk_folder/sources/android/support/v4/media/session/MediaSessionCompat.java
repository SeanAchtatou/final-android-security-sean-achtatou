package android.support.v4.media.session;

import android.os.Build;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.session.C0133;
import java.util.ArrayList;
import java.util.List;

public class MediaSessionCompat {

    public static final class Token implements Parcelable {
        public static final Parcelable.Creator<Token> CREATOR = new Parcelable.Creator<Token>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public Token createFromParcel(Parcel parcel) {
                Object obj;
                if (Build.VERSION.SDK_INT >= 21) {
                    obj = parcel.readParcelable(null);
                } else {
                    obj = parcel.readStrongBinder();
                }
                return new Token(obj);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public Token[] newArray(int i) {
                return new Token[i];
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        private final Object f455;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final C0126 f456;

        public int describeContents() {
            return 0;
        }

        Token(Object obj) {
            this(obj, null);
        }

        Token(Object obj, C0126 r2) {
            this.f455 = obj;
            this.f456 = r2;
        }

        public void writeToParcel(Parcel parcel, int i) {
            if (Build.VERSION.SDK_INT >= 21) {
                parcel.writeParcelable((Parcelable) this.f455, i);
            } else {
                parcel.writeStrongBinder((IBinder) this.f455);
            }
        }

        public int hashCode() {
            Object obj = this.f455;
            if (obj == null) {
                return 0;
            }
            return obj.hashCode();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Token)) {
                return false;
            }
            Token token = (Token) obj;
            Object obj2 = this.f455;
            if (obj2 != null) {
                Object obj3 = token.f455;
                if (obj3 == null) {
                    return false;
                }
                return obj2.equals(obj3);
            } else if (token.f455 == null) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static final class QueueItem implements Parcelable {
        public static final Parcelable.Creator<QueueItem> CREATOR = new Parcelable.Creator<QueueItem>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public QueueItem createFromParcel(Parcel parcel) {
                return new QueueItem(parcel);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public QueueItem[] newArray(int i) {
                return new QueueItem[i];
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        private final MediaDescriptionCompat f451;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final long f452;

        /* renamed from: ʽ  reason: contains not printable characters */
        private Object f453;

        public int describeContents() {
            return 0;
        }

        private QueueItem(Object obj, MediaDescriptionCompat mediaDescriptionCompat, long j) {
            if (mediaDescriptionCompat == null) {
                throw new IllegalArgumentException("Description cannot be null.");
            } else if (j != -1) {
                this.f451 = mediaDescriptionCompat;
                this.f452 = j;
                this.f453 = obj;
            } else {
                throw new IllegalArgumentException("Id cannot be QueueItem.UNKNOWN_ID");
            }
        }

        QueueItem(Parcel parcel) {
            this.f451 = MediaDescriptionCompat.CREATOR.createFromParcel(parcel);
            this.f452 = parcel.readLong();
        }

        public void writeToParcel(Parcel parcel, int i) {
            this.f451.writeToParcel(parcel, i);
            parcel.writeLong(this.f452);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static QueueItem m659(Object obj) {
            if (obj == null || Build.VERSION.SDK_INT < 21) {
                return null;
            }
            return new QueueItem(obj, MediaDescriptionCompat.m599(C0133.C0134.m811(obj)), C0133.C0134.m812(obj));
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static List<QueueItem> m660(List<?> list) {
            if (list == null || Build.VERSION.SDK_INT < 21) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            for (Object r1 : list) {
                arrayList.add(m659(r1));
            }
            return arrayList;
        }

        public String toString() {
            return "MediaSession.QueueItem {Description=" + this.f451 + ", Id=" + this.f452 + " }";
        }
    }

    static final class ResultReceiverWrapper implements Parcelable {
        public static final Parcelable.Creator<ResultReceiverWrapper> CREATOR = new Parcelable.Creator<ResultReceiverWrapper>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public ResultReceiverWrapper createFromParcel(Parcel parcel) {
                return new ResultReceiverWrapper(parcel);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public ResultReceiverWrapper[] newArray(int i) {
                return new ResultReceiverWrapper[i];
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        private ResultReceiver f454;

        public int describeContents() {
            return 0;
        }

        ResultReceiverWrapper(Parcel parcel) {
            this.f454 = (ResultReceiver) ResultReceiver.CREATOR.createFromParcel(parcel);
        }

        public void writeToParcel(Parcel parcel, int i) {
            this.f454.writeToParcel(parcel, i);
        }
    }
}
