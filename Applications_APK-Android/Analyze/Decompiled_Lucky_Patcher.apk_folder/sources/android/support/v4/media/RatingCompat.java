package android.support.v4.media;

import android.os.Parcel;
import android.os.Parcelable;

public final class RatingCompat implements Parcelable {
    public static final Parcelable.Creator<RatingCompat> CREATOR = new Parcelable.Creator<RatingCompat>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public RatingCompat createFromParcel(Parcel parcel) {
            return new RatingCompat(parcel.readInt(), parcel.readFloat());
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public RatingCompat[] newArray(int i) {
            return new RatingCompat[i];
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int f433;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final float f434;

    RatingCompat(int i, float f) {
        this.f433 = i;
        this.f434 = f;
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append("Rating:style=");
        sb.append(this.f433);
        sb.append(" rating=");
        float f = this.f434;
        if (f < 0.0f) {
            str = "unrated";
        } else {
            str = String.valueOf(f);
        }
        sb.append(str);
        return sb.toString();
    }

    public int describeContents() {
        return this.f433;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f433);
        parcel.writeFloat(this.f434);
    }
}
