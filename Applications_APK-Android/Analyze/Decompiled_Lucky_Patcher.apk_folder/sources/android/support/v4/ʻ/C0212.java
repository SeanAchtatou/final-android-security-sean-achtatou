package android.support.v4.ʻ;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.ʻ.C0219;
import java.util.Set;

/* renamed from: android.support.v4.ʻ.ˆˆ  reason: contains not printable characters */
/* compiled from: RemoteInput */
public final class C0212 extends C0219.C0220 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final C0219.C0220.C0221 f710 = new C0219.C0220.C0221() {
    };

    /* renamed from: ˉ  reason: contains not printable characters */
    private static final C0213 f711;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final String f712;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final CharSequence f713;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final CharSequence[] f714;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final boolean f715;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final Bundle f716;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final Set<String> f717;

    /* renamed from: android.support.v4.ʻ.ˆˆ$ʻ  reason: contains not printable characters */
    /* compiled from: RemoteInput */
    interface C0213 {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m1176() {
        return this.f712;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public CharSequence m1177() {
        return this.f713;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public CharSequence[] m1178() {
        return this.f714;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public Set<String> m1179() {
        return this.f717;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m1180() {
        return this.f715;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public Bundle m1181() {
        return this.f716;
    }

    /* renamed from: android.support.v4.ʻ.ˆˆ$ʽ  reason: contains not printable characters */
    /* compiled from: RemoteInput */
    static class C0215 implements C0213 {
        C0215() {
        }
    }

    /* renamed from: android.support.v4.ʻ.ˆˆ$ʾ  reason: contains not printable characters */
    /* compiled from: RemoteInput */
    static class C0216 implements C0213 {
        C0216() {
        }
    }

    /* renamed from: android.support.v4.ʻ.ˆˆ$ʼ  reason: contains not printable characters */
    /* compiled from: RemoteInput */
    static class C0214 implements C0213 {
        C0214() {
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 20) {
            f711 = new C0214();
        } else if (Build.VERSION.SDK_INT >= 16) {
            f711 = new C0216();
        } else {
            f711 = new C0215();
        }
    }
}
