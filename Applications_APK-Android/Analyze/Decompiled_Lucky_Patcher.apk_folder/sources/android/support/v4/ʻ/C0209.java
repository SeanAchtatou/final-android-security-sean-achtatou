package android.support.v4.ʻ;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.ʻ.C0198;
import android.util.Log;
import android.util.SparseArray;
import android.widget.RemoteViews;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/* renamed from: android.support.v4.ʻ.ʿʿ  reason: contains not printable characters */
/* compiled from: NotificationCompatJellybean */
class C0209 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final Object f701 = new Object();

    /* renamed from: ʼ  reason: contains not printable characters */
    private static Field f702;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static boolean f703;

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final Object f704 = new Object();

    /* renamed from: android.support.v4.ʻ.ʿʿ$ʻ  reason: contains not printable characters */
    /* compiled from: NotificationCompatJellybean */
    public static class C0210 implements C0271, C0272 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private Notification.Builder f705;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final Bundle f706;

        /* renamed from: ʽ  reason: contains not printable characters */
        private List<Bundle> f707 = new ArrayList();

        /* renamed from: ʾ  reason: contains not printable characters */
        private RemoteViews f708;

        /* renamed from: ʿ  reason: contains not printable characters */
        private RemoteViews f709;

        public C0210(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z, boolean z2, int i4, CharSequence charSequence4, boolean z3, Bundle bundle, String str, boolean z4, String str2, RemoteViews remoteViews2, RemoteViews remoteViews3) {
            PendingIntent pendingIntent3;
            Notification notification2 = notification;
            Bundle bundle2 = bundle;
            String str3 = str;
            String str4 = str2;
            boolean z5 = false;
            Notification.Builder deleteIntent = new Notification.Builder(context).setWhen(notification2.when).setSmallIcon(notification2.icon, notification2.iconLevel).setContent(notification2.contentView).setTicker(notification2.tickerText, remoteViews).setSound(notification2.sound, notification2.audioStreamType).setVibrate(notification2.vibrate).setLights(notification2.ledARGB, notification2.ledOnMS, notification2.ledOffMS).setOngoing((notification2.flags & 2) != 0).setOnlyAlertOnce((notification2.flags & 8) != 0).setAutoCancel((notification2.flags & 16) != 0).setDefaults(notification2.defaults).setContentTitle(charSequence).setContentText(charSequence2).setSubText(charSequence4).setContentInfo(charSequence3).setContentIntent(pendingIntent).setDeleteIntent(notification2.deleteIntent);
            if ((notification2.flags & 128) != 0) {
                pendingIntent3 = pendingIntent2;
                z5 = true;
            } else {
                pendingIntent3 = pendingIntent2;
            }
            this.f705 = deleteIntent.setFullScreenIntent(pendingIntent3, z5).setLargeIcon(bitmap).setNumber(i).setUsesChronometer(z2).setPriority(i4).setProgress(i2, i3, z);
            this.f706 = new Bundle();
            if (bundle2 != null) {
                this.f706.putAll(bundle2);
            }
            if (z3) {
                this.f706.putBoolean("android.support.localOnly", true);
            }
            if (str3 != null) {
                this.f706.putString("android.support.groupKey", str3);
                if (z4) {
                    this.f706.putBoolean("android.support.isGroupSummary", true);
                } else {
                    this.f706.putBoolean("android.support.useSideChannel", true);
                }
            }
            if (str4 != null) {
                this.f706.putString("android.support.sortKey", str4);
            }
            this.f708 = remoteViews2;
            this.f709 = remoteViews3;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1175(C0198.C0199 r3) {
            this.f707.add(C0209.m1171(this.f705, r3));
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Notification m1174() {
            Notification build = this.f705.build();
            Bundle r1 = C0209.m1172(build);
            Bundle bundle = new Bundle(this.f706);
            for (String next : this.f706.keySet()) {
                if (r1.containsKey(next)) {
                    bundle.remove(next);
                }
            }
            r1.putAll(bundle);
            SparseArray<Bundle> r12 = C0209.m1173(this.f707);
            if (r12 != null) {
                C0209.m1172(build).putSparseParcelableArray("android.support.actionExtras", r12);
            }
            RemoteViews remoteViews = this.f708;
            if (remoteViews != null) {
                build.contentView = remoteViews;
            }
            RemoteViews remoteViews2 = this.f709;
            if (remoteViews2 != null) {
                build.bigContentView = remoteViews2;
            }
            return build;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static SparseArray<Bundle> m1173(List<Bundle> list) {
        int size = list.size();
        SparseArray<Bundle> sparseArray = null;
        for (int i = 0; i < size; i++) {
            Bundle bundle = list.get(i);
            if (bundle != null) {
                if (sparseArray == null) {
                    sparseArray = new SparseArray<>();
                }
                sparseArray.put(i, bundle);
            }
        }
        return sparseArray;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Bundle m1172(Notification notification) {
        synchronized (f701) {
            if (f703) {
                return null;
            }
            try {
                if (f702 == null) {
                    Field declaredField = Notification.class.getDeclaredField("extras");
                    if (!Bundle.class.isAssignableFrom(declaredField.getType())) {
                        Log.e("NotificationCompat", "Notification.extras field is not of type Bundle");
                        f703 = true;
                        return null;
                    }
                    declaredField.setAccessible(true);
                    f702 = declaredField;
                }
                Bundle bundle = (Bundle) f702.get(notification);
                if (bundle == null) {
                    bundle = new Bundle();
                    f702.set(notification, bundle);
                }
                return bundle;
            } catch (IllegalAccessException e) {
                Log.e("NotificationCompat", "Unable to access notification extras", e);
                f703 = true;
                return null;
            } catch (NoSuchFieldException e2) {
                Log.e("NotificationCompat", "Unable to access notification extras", e2);
                f703 = true;
                return null;
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Bundle m1171(Notification.Builder builder, C0198.C0199 r4) {
        builder.addAction(r4.m1134(), r4.m1135(), r4.m1136());
        Bundle bundle = new Bundle(r4.m1137());
        if (r4.m1140() != null) {
            bundle.putParcelableArray("android.support.remoteInputs", C0233.m1341(r4.m1140()));
        }
        if (r4.m1139() != null) {
            bundle.putParcelableArray("android.support.dataRemoteInputs", C0233.m1341(r4.m1139()));
        }
        bundle.putBoolean("android.support.allowGeneratedReplies", r4.m1138());
        return bundle;
    }
}
