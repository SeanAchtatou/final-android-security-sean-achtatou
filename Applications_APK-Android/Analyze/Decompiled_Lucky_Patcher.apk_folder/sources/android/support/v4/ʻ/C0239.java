package android.support.v4.ʻ;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;

/* renamed from: android.support.v4.ʻ.ˑ  reason: contains not printable characters */
/* compiled from: FragmentManager */
public abstract class C0239 {

    /* renamed from: android.support.v4.ʻ.ˑ$ʻ  reason: contains not printable characters */
    /* compiled from: FragmentManager */
    public static abstract class C0240 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1409(C0239 r1, C0222 r2) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1410(C0239 r1, C0222 r2, Context context) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1411(C0239 r1, C0222 r2, Bundle bundle) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1412(C0239 r1, C0222 r2, View view, Bundle bundle) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m1413(C0239 r1, C0222 r2) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m1414(C0239 r1, C0222 r2, Context context) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m1415(C0239 r1, C0222 r2, Bundle bundle) {
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m1416(C0239 r1, C0222 r2) {
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m1417(C0239 r1, C0222 r2, Bundle bundle) {
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public void m1418(C0239 r1, C0222 r2) {
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public void m1419(C0239 r1, C0222 r2, Bundle bundle) {
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public void m1420(C0239 r1, C0222 r2) {
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public void m1421(C0239 r1, C0222 r2) {
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public void m1422(C0239 r1, C0222 r2) {
        }
    }

    /* renamed from: android.support.v4.ʻ.ˑ$ʼ  reason: contains not printable characters */
    /* compiled from: FragmentManager */
    public interface C0241 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m1423();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract C0260 m1403();

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m1404(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract boolean m1405();

    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract boolean m1406();

    /* renamed from: ʾ  reason: contains not printable characters */
    public abstract List<C0222> m1407();

    /* renamed from: ʿ  reason: contains not printable characters */
    public abstract boolean m1408();
}
