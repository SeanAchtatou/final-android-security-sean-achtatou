package android.support.v4.ʻ;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.C0101;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: android.support.v4.ʻ.ˑˑ  reason: contains not printable characters */
/* compiled from: TaskStackBuilder */
public final class C0242 implements Iterable<Intent> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final C0245 f807;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ArrayList<Intent> f808 = new ArrayList<>();

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Context f809;

    /* renamed from: android.support.v4.ʻ.ˑˑ$ʻ  reason: contains not printable characters */
    /* compiled from: TaskStackBuilder */
    public interface C0243 {
        Intent getSupportParentActivityIntent();
    }

    /* renamed from: android.support.v4.ʻ.ˑˑ$ʽ  reason: contains not printable characters */
    /* compiled from: TaskStackBuilder */
    static class C0245 {
        C0245() {
        }
    }

    /* renamed from: android.support.v4.ʻ.ˑˑ$ʼ  reason: contains not printable characters */
    /* compiled from: TaskStackBuilder */
    static class C0244 extends C0245 {
        C0244() {
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f807 = new C0244();
        } else {
            f807 = new C0245();
        }
    }

    private C0242(Context context) {
        this.f809 = context;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0242 m1424(Context context) {
        return new C0242(context);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0242 m1427(Intent intent) {
        this.f808.add(intent);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0242 m1425(Activity activity) {
        Intent supportParentActivityIntent = activity instanceof C0243 ? ((C0243) activity).getSupportParentActivityIntent() : null;
        if (supportParentActivityIntent == null) {
            supportParentActivityIntent = C0270.m1653(activity);
        }
        if (supportParentActivityIntent != null) {
            ComponentName component = supportParentActivityIntent.getComponent();
            if (component == null) {
                component = supportParentActivityIntent.resolveActivity(this.f809.getPackageManager());
            }
            m1426(component);
            m1427(supportParentActivityIntent);
        }
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0242 m1426(ComponentName componentName) {
        int size = this.f808.size();
        try {
            Intent r3 = C0270.m1654(this.f809, componentName);
            while (r3 != null) {
                this.f808.add(size, r3);
                r3 = C0270.m1654(this.f809, r3.getComponent());
            }
            return this;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("TaskStackBuilder", "Bad ComponentName while traversing activity parent metadata");
            throw new IllegalArgumentException(e);
        }
    }

    @Deprecated
    public Iterator<Intent> iterator() {
        return this.f808.iterator();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1428() {
        m1429((Bundle) null);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1429(Bundle bundle) {
        if (!this.f808.isEmpty()) {
            ArrayList<Intent> arrayList = this.f808;
            Intent[] intentArr = (Intent[]) arrayList.toArray(new Intent[arrayList.size()]);
            intentArr[0] = new Intent(intentArr[0]).addFlags(268484608);
            if (!C0101.m540(this.f809, intentArr, bundle)) {
                Intent intent = new Intent(intentArr[intentArr.length - 1]);
                intent.addFlags(268435456);
                this.f809.startActivity(intent);
                return;
            }
            return;
        }
        throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
    }
}
