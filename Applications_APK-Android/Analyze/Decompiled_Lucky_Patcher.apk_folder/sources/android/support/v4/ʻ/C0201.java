package android.support.v4.ʻ;

import android.os.Build;
import android.support.v4.ʻ.C0222;
import android.support.v4.ʻ.C0246;
import android.support.v4.ˈ.C0335;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

/* renamed from: android.support.v4.ʻ.ʽ  reason: contains not printable characters */
/* compiled from: BackStackRecord */
final class C0201 extends C0260 implements C0246.C0252 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final boolean f655 = (Build.VERSION.SDK_INT >= 21);

    /* renamed from: ʼ  reason: contains not printable characters */
    final C0246 f656;

    /* renamed from: ʽ  reason: contains not printable characters */
    ArrayList<C0202> f657 = new ArrayList<>();

    /* renamed from: ʾ  reason: contains not printable characters */
    int f658;

    /* renamed from: ʿ  reason: contains not printable characters */
    int f659;

    /* renamed from: ˆ  reason: contains not printable characters */
    int f660;

    /* renamed from: ˈ  reason: contains not printable characters */
    int f661;

    /* renamed from: ˉ  reason: contains not printable characters */
    int f662;

    /* renamed from: ˊ  reason: contains not printable characters */
    int f663;

    /* renamed from: ˋ  reason: contains not printable characters */
    boolean f664;

    /* renamed from: ˎ  reason: contains not printable characters */
    boolean f665 = true;

    /* renamed from: ˏ  reason: contains not printable characters */
    String f666;

    /* renamed from: ˑ  reason: contains not printable characters */
    boolean f667;

    /* renamed from: י  reason: contains not printable characters */
    int f668 = -1;

    /* renamed from: ـ  reason: contains not printable characters */
    int f669;

    /* renamed from: ٴ  reason: contains not printable characters */
    CharSequence f670;

    /* renamed from: ᐧ  reason: contains not printable characters */
    int f671;

    /* renamed from: ᴵ  reason: contains not printable characters */
    CharSequence f672;

    /* renamed from: ᵎ  reason: contains not printable characters */
    ArrayList<String> f673;

    /* renamed from: ᵔ  reason: contains not printable characters */
    ArrayList<String> f674;

    /* renamed from: ᵢ  reason: contains not printable characters */
    boolean f675 = false;

    /* renamed from: ⁱ  reason: contains not printable characters */
    ArrayList<Runnable> f676;

    /* renamed from: android.support.v4.ʻ.ʽ$ʻ  reason: contains not printable characters */
    /* compiled from: BackStackRecord */
    static final class C0202 {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f677;

        /* renamed from: ʼ  reason: contains not printable characters */
        C0222 f678;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f679;

        /* renamed from: ʾ  reason: contains not printable characters */
        int f680;

        /* renamed from: ʿ  reason: contains not printable characters */
        int f681;

        /* renamed from: ˆ  reason: contains not printable characters */
        int f682;

        C0202() {
        }

        C0202(int i, C0222 r2) {
            this.f677 = i;
            this.f678 = r2;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.f668 >= 0) {
            sb.append(" #");
            sb.append(this.f668);
        }
        if (this.f666 != null) {
            sb.append(" ");
            sb.append(this.f666);
        }
        sb.append("}");
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.ʽ.ʻ(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      android.support.v4.ʻ.ʽ.ʻ(int, android.support.v4.ʻ.ˉ, java.lang.String):android.support.v4.ʻ.ᴵ
      android.support.v4.ʻ.ʽ.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, int, int):boolean
      android.support.v4.ʻ.ʽ.ʻ(java.lang.String, java.io.PrintWriter, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1152(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        m1153(str, printWriter, true);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1153(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.f666);
            printWriter.print(" mIndex=");
            printWriter.print(this.f668);
            printWriter.print(" mCommitted=");
            printWriter.println(this.f667);
            if (this.f662 != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.f662));
                printWriter.print(" mTransitionStyle=#");
                printWriter.println(Integer.toHexString(this.f663));
            }
            if (!(this.f658 == 0 && this.f659 == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.f658));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.f659));
            }
            if (!(this.f660 == 0 && this.f661 == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.f660));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.f661));
            }
            if (!(this.f669 == 0 && this.f670 == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.f669));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.f670);
            }
            if (!(this.f671 == 0 && this.f672 == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.f671));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.f672);
            }
        }
        if (!this.f657.isEmpty()) {
            printWriter.print(str);
            printWriter.println("Operations:");
            str + "    ";
            int size = this.f657.size();
            for (int i = 0; i < size; i++) {
                C0202 r2 = this.f657.get(i);
                switch (r2.f677) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    case 8:
                        str2 = "SET_PRIMARY_NAV";
                        break;
                    case 9:
                        str2 = "UNSET_PRIMARY_NAV";
                        break;
                    default:
                        str2 = "cmd=" + r2.f677;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(r2.f678);
                if (z) {
                    if (!(r2.f679 == 0 && r2.f680 == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(r2.f679));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(r2.f680));
                    }
                    if (r2.f681 != 0 || r2.f682 != 0) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(r2.f681));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(r2.f682));
                    }
                }
            }
        }
    }

    public C0201(C0246 r2) {
        this.f656 = r2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1150(C0202 r2) {
        this.f657.add(r2);
        r2.f679 = this.f658;
        r2.f680 = this.f659;
        r2.f681 = this.f660;
        r2.f682 = this.f661;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0260 m1145(int i, C0222 r4) {
        m1141(i, r4, (String) null, 1);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m1141(int i, C0222 r5, String str, int i2) {
        Class<?> cls = r5.getClass();
        int modifiers = cls.getModifiers();
        if (cls.isAnonymousClass() || !Modifier.isPublic(modifiers) || (cls.isMemberClass() && !Modifier.isStatic(modifiers))) {
            throw new IllegalStateException("Fragment " + cls.getCanonicalName() + " must be a public static class to be  properly recreated from" + " instance state.");
        }
        r5.f754 = this.f656;
        if (str != null) {
            if (r5.f766 == null || str.equals(r5.f766)) {
                r5.f766 = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + r5 + ": was " + r5.f766 + " now " + str);
            }
        }
        if (i != 0) {
            if (i == -1) {
                throw new IllegalArgumentException("Can't add fragment " + r5 + " with tag " + str + " to container view with no id");
            } else if (r5.f764 == 0 || r5.f764 == i) {
                r5.f764 = i;
                r5.f765 = i;
            } else {
                throw new IllegalStateException("Can't change container ID of fragment " + r5 + ": was " + r5.f764 + " now " + i);
            }
        }
        m1150(new C0202(i2, r5));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0260 m1158(int i, C0222 r3) {
        return m1146(i, r3, (String) null);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0260 m1146(int i, C0222 r3, String str) {
        if (i != 0) {
            m1141(i, r3, str, 2);
            return this;
        }
        throw new IllegalArgumentException("Must use non-zero containerViewId");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0260 m1147(C0222 r3) {
        m1150(new C0202(3, r3));
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1149(int i) {
        if (this.f664) {
            if (C0246.f810) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i);
            }
            int size = this.f657.size();
            for (int i2 = 0; i2 < size; i2++) {
                C0202 r3 = this.f657.get(i2);
                if (r3.f678 != null) {
                    r3.f678.f752 += i;
                    if (C0246.f810) {
                        Log.v("FragmentManager", "Bump nesting of " + r3.f678 + " to " + r3.f678.f752);
                    }
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1148() {
        ArrayList<Runnable> arrayList = this.f676;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                this.f676.get(i).run();
            }
            this.f676 = null;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m1156() {
        return m1143(false);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m1143(boolean z) {
        if (!this.f667) {
            if (C0246.f810) {
                Log.v("FragmentManager", "Commit: " + this);
                PrintWriter printWriter = new PrintWriter(new C0335("FragmentManager"));
                m1152("  ", (FileDescriptor) null, printWriter, (String[]) null);
                printWriter.close();
            }
            this.f667 = true;
            if (this.f664) {
                this.f668 = this.f656.m1463(this);
            } else {
                this.f668 = -1;
            }
            this.f656.m1480(this, z);
            return this.f668;
        }
        throw new IllegalStateException("commit already called");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1155(ArrayList<C0201> arrayList, ArrayList<Boolean> arrayList2) {
        if (C0246.f810) {
            Log.v("FragmentManager", "Run: " + this);
        }
        arrayList.add(this);
        arrayList2.add(false);
        if (!this.f664) {
            return true;
        }
        this.f656.m1490(this);
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m1160(int i) {
        int size = this.f657.size();
        for (int i2 = 0; i2 < size; i2++) {
            C0202 r3 = this.f657.get(i2);
            int i3 = r3.f678 != null ? r3.f678.f765 : 0;
            if (i3 != 0 && i3 == i) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1154(ArrayList<C0201> arrayList, int i, int i2) {
        if (i2 == i) {
            return false;
        }
        int size = this.f657.size();
        int i3 = -1;
        for (int i4 = 0; i4 < size; i4++) {
            C0202 r4 = this.f657.get(i4);
            int i5 = r4.f678 != null ? r4.f678.f765 : 0;
            if (!(i5 == 0 || i5 == i3)) {
                for (int i6 = i; i6 < i2; i6++) {
                    C0201 r5 = arrayList.get(i6);
                    int size2 = r5.f657.size();
                    for (int i7 = 0; i7 < size2; i7++) {
                        C0202 r8 = r5.f657.get(i7);
                        if ((r8.f678 != null ? r8.f678.f765 : 0) == i5) {
                            return true;
                        }
                    }
                }
                i3 = i5;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.ʻ.י.ʻ(android.view.View, android.support.v4.ʻ.י$ʽ):boolean
      android.support.v4.ʻ.י.ʻ(android.os.Bundle, java.lang.String):android.support.v4.ʻ.ˉ
      android.support.v4.ʻ.י.ʻ(int, android.support.v4.ʻ.ʽ):void
      android.support.v4.ʻ.י.ʻ(int, boolean):void
      android.support.v4.ʻ.י.ʻ(android.os.Parcelable, android.support.v4.ʻ.ـ):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י$ˆ, boolean):void
      android.support.v4.ʻ.י.ʻ(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.ʻ.י.ʻ(android.view.View, android.support.v4.ʻ.י$ʽ):boolean
      android.support.v4.ʻ.י.ʻ(android.os.Bundle, java.lang.String):android.support.v4.ʻ.ˉ
      android.support.v4.ʻ.י.ʻ(int, android.support.v4.ʻ.ʽ):void
      android.support.v4.ʻ.י.ʻ(android.os.Parcelable, android.support.v4.ʻ.ـ):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, boolean):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י$ˆ, boolean):void
      android.support.v4.ʻ.י.ʻ(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.ʻ.י.ʻ(int, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1161() {
        int size = this.f657.size();
        for (int i = 0; i < size; i++) {
            C0202 r4 = this.f657.get(i);
            C0222 r5 = r4.f678;
            if (r5 != null) {
                r5.m1199(this.f662, this.f663);
            }
            switch (r4.f677) {
                case 1:
                    r5.m1228(r4.f679);
                    this.f656.m1478(r5, false);
                    break;
                case 2:
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + r4.f677);
                case 3:
                    r5.m1228(r4.f680);
                    this.f656.m1518(r5);
                    break;
                case 4:
                    r5.m1228(r4.f680);
                    this.f656.m1521(r5);
                    break;
                case 5:
                    r5.m1228(r4.f679);
                    this.f656.m1523(r5);
                    break;
                case 6:
                    r5.m1228(r4.f680);
                    this.f656.m1525(r5);
                    break;
                case 7:
                    r5.m1228(r4.f679);
                    this.f656.m1527(r5);
                    break;
                case 8:
                    this.f656.m1533(r5);
                    break;
                case 9:
                    this.f656.m1533(null);
                    break;
            }
            if (!(this.f675 || r4.f677 == 1 || r5 == null)) {
                this.f656.m1508(r5);
            }
        }
        if (!this.f675) {
            C0246 r0 = this.f656;
            r0.m1469(r0.f829, true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.ʻ.י.ʻ(android.view.View, android.support.v4.ʻ.י$ʽ):boolean
      android.support.v4.ʻ.י.ʻ(android.os.Bundle, java.lang.String):android.support.v4.ʻ.ˉ
      android.support.v4.ʻ.י.ʻ(int, android.support.v4.ʻ.ʽ):void
      android.support.v4.ʻ.י.ʻ(int, boolean):void
      android.support.v4.ʻ.י.ʻ(android.os.Parcelable, android.support.v4.ʻ.ـ):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י$ˆ, boolean):void
      android.support.v4.ʻ.י.ʻ(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.ʻ.י.ʻ(android.view.View, android.support.v4.ʻ.י$ʽ):boolean
      android.support.v4.ʻ.י.ʻ(android.os.Bundle, java.lang.String):android.support.v4.ʻ.ˉ
      android.support.v4.ʻ.י.ʻ(int, android.support.v4.ʻ.ʽ):void
      android.support.v4.ʻ.י.ʻ(android.os.Parcelable, android.support.v4.ʻ.ـ):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, boolean):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י$ˆ, boolean):void
      android.support.v4.ʻ.י.ʻ(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.ʻ.י.ʻ(int, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1159(boolean z) {
        for (int size = this.f657.size() - 1; size >= 0; size--) {
            C0202 r2 = this.f657.get(size);
            C0222 r3 = r2.f678;
            if (r3 != null) {
                r3.m1199(C0246.m1457(this.f662), this.f663);
            }
            switch (r2.f677) {
                case 1:
                    r3.m1228(r2.f682);
                    this.f656.m1518(r3);
                    break;
                case 2:
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + r2.f677);
                case 3:
                    r3.m1228(r2.f681);
                    this.f656.m1478(r3, false);
                    break;
                case 4:
                    r3.m1228(r2.f681);
                    this.f656.m1523(r3);
                    break;
                case 5:
                    r3.m1228(r2.f682);
                    this.f656.m1521(r3);
                    break;
                case 6:
                    r3.m1228(r2.f681);
                    this.f656.m1527(r3);
                    break;
                case 7:
                    r3.m1228(r2.f682);
                    this.f656.m1525(r3);
                    break;
                case 8:
                    this.f656.m1533(null);
                    break;
                case 9:
                    this.f656.m1533(r3);
                    break;
            }
            if (!(this.f675 || r2.f677 == 3 || r3 == null)) {
                this.f656.m1508(r3);
            }
        }
        if (!this.f675 && z) {
            C0246 r7 = this.f656;
            r7.m1469(r7.f829, true);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0222 m1144(ArrayList<C0222> arrayList, C0222 r18) {
        ArrayList<C0222> arrayList2 = arrayList;
        C0222 r4 = r18;
        int i = 0;
        while (i < this.f657.size()) {
            C0202 r5 = this.f657.get(i);
            int i2 = r5.f677;
            if (i2 != 1) {
                if (i2 == 2) {
                    C0222 r6 = r5.f678;
                    int i3 = r6.f765;
                    C0222 r13 = r4;
                    int i4 = i;
                    boolean z = false;
                    for (int size = arrayList.size() - 1; size >= 0; size--) {
                        C0222 r14 = arrayList2.get(size);
                        if (r14.f765 == i3) {
                            if (r14 == r6) {
                                z = true;
                            } else {
                                if (r14 == r13) {
                                    this.f657.add(i4, new C0202(9, r14));
                                    i4++;
                                    r13 = null;
                                }
                                C0202 r15 = new C0202(3, r14);
                                r15.f679 = r5.f679;
                                r15.f681 = r5.f681;
                                r15.f680 = r5.f680;
                                r15.f682 = r5.f682;
                                this.f657.add(i4, r15);
                                arrayList2.remove(r14);
                                i4++;
                            }
                        }
                    }
                    if (z) {
                        this.f657.remove(i4);
                        i4--;
                    } else {
                        r5.f677 = 1;
                        arrayList2.add(r6);
                    }
                    i = i4;
                    r4 = r13;
                } else if (i2 == 3 || i2 == 6) {
                    arrayList2.remove(r5.f678);
                    if (r5.f678 == r4) {
                        this.f657.add(i, new C0202(9, r5.f678));
                        i++;
                        r4 = null;
                    }
                } else if (i2 != 7) {
                    if (i2 == 8) {
                        this.f657.add(i, new C0202(9, r4));
                        i++;
                        r4 = r5.f678;
                    }
                }
                i++;
            }
            arrayList2.add(r5.f678);
            i++;
        }
        return r4;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public C0222 m1157(ArrayList<C0222> arrayList, C0222 r6) {
        for (int i = 0; i < this.f657.size(); i++) {
            C0202 r1 = this.f657.get(i);
            int i2 = r1.f677;
            if (i2 != 1) {
                if (i2 != 3) {
                    switch (i2) {
                        case 8:
                            r6 = null;
                            break;
                        case 9:
                            r6 = r1.f678;
                            break;
                    }
                }
                arrayList.add(r1.f678);
            }
            arrayList.remove(r1.f678);
        }
        return r6;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m1162() {
        for (int i = 0; i < this.f657.size(); i++) {
            if (m1142(this.f657.get(i))) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1151(C0222.C0225 r4) {
        for (int i = 0; i < this.f657.size(); i++) {
            C0202 r1 = this.f657.get(i);
            if (m1142(r1)) {
                r1.f678.m1214(r4);
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static boolean m1142(C0202 r1) {
        C0222 r12 = r1.f678;
        return r12 != null && r12.f741 && r12.f750 != null && !r12.f753 && !r12.f767 && r12.m1299();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public String m1163() {
        return this.f666;
    }
}
