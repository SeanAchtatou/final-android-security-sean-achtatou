package android.support.v4.ʻ;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.ʻ.C0198;
import android.widget.RemoteViews;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: android.support.v4.ʻ.ᴵᴵ  reason: contains not printable characters */
/* compiled from: NotificationCompatApi21 */
class C0261 {

    /* renamed from: android.support.v4.ʻ.ᴵᴵ$ʻ  reason: contains not printable characters */
    /* compiled from: NotificationCompatApi21 */
    public static class C0262 implements C0271, C0272 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private Notification.Builder f893;

        /* renamed from: ʼ  reason: contains not printable characters */
        private Bundle f894;

        /* renamed from: ʽ  reason: contains not printable characters */
        private RemoteViews f895;

        /* renamed from: ʾ  reason: contains not printable characters */
        private RemoteViews f896;

        /* renamed from: ʿ  reason: contains not printable characters */
        private RemoteViews f897;

        /* renamed from: ˆ  reason: contains not printable characters */
        private int f898;

        public C0262(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z, boolean z2, boolean z3, int i4, CharSequence charSequence4, boolean z4, String str, ArrayList<String> arrayList, Bundle bundle, int i5, int i6, Notification notification2, String str2, boolean z5, String str3, RemoteViews remoteViews2, RemoteViews remoteViews3, RemoteViews remoteViews4, int i7) {
            PendingIntent pendingIntent3;
            Notification notification3 = notification;
            Bundle bundle2 = bundle;
            boolean z6 = true;
            Notification.Builder deleteIntent = new Notification.Builder(context).setWhen(notification3.when).setShowWhen(z2).setSmallIcon(notification3.icon, notification3.iconLevel).setContent(notification3.contentView).setTicker(notification3.tickerText, remoteViews).setSound(notification3.sound, notification3.audioStreamType).setVibrate(notification3.vibrate).setLights(notification3.ledARGB, notification3.ledOnMS, notification3.ledOffMS).setOngoing((notification3.flags & 2) != 0).setOnlyAlertOnce((notification3.flags & 8) != 0).setAutoCancel((notification3.flags & 16) != 0).setDefaults(notification3.defaults).setContentTitle(charSequence).setContentText(charSequence2).setSubText(charSequence4).setContentInfo(charSequence3).setContentIntent(pendingIntent).setDeleteIntent(notification3.deleteIntent);
            if ((notification3.flags & 128) != 0) {
                pendingIntent3 = pendingIntent2;
            } else {
                pendingIntent3 = pendingIntent2;
                z6 = false;
            }
            this.f893 = deleteIntent.setFullScreenIntent(pendingIntent3, z6).setLargeIcon(bitmap).setNumber(i).setUsesChronometer(z3).setPriority(i4).setProgress(i2, i3, z).setLocalOnly(z4).setGroup(str2).setGroupSummary(z5).setSortKey(str3).setCategory(str).setColor(i5).setVisibility(i6).setPublicVersion(notification2);
            this.f894 = new Bundle();
            if (bundle2 != null) {
                this.f894.putAll(bundle2);
            }
            Iterator<String> it = arrayList.iterator();
            while (it.hasNext()) {
                this.f893.addPerson(it.next());
            }
            this.f895 = remoteViews2;
            this.f896 = remoteViews3;
            this.f897 = remoteViews4;
            this.f898 = i7;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1571(C0198.C0199 r2) {
            C0258.m1561(this.f893, r2);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Notification m1570() {
            this.f893.setExtras(this.f894);
            Notification build = this.f893.build();
            RemoteViews remoteViews = this.f895;
            if (remoteViews != null) {
                build.contentView = remoteViews;
            }
            RemoteViews remoteViews2 = this.f896;
            if (remoteViews2 != null) {
                build.bigContentView = remoteViews2;
            }
            RemoteViews remoteViews3 = this.f897;
            if (remoteViews3 != null) {
                build.headsUpContentView = remoteViews3;
            }
            if (this.f898 != 0) {
                if (!(build.getGroup() == null || (build.flags & 512) == 0 || this.f898 != 2)) {
                    m1569(build);
                }
                if (build.getGroup() != null && (build.flags & 512) == 0 && this.f898 == 1) {
                    m1569(build);
                }
            }
            return build;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m1569(Notification notification) {
            notification.sound = null;
            notification.vibrate = null;
            notification.defaults &= -2;
            notification.defaults &= -3;
        }
    }
}
