package android.support.v4.ʻ;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/* renamed from: android.support.v4.ʻ.ᐧ  reason: contains not printable characters */
/* compiled from: Fragment */
final class C0257 implements Parcelable {
    public static final Parcelable.Creator<C0257> CREATOR = new Parcelable.Creator<C0257>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public C0257 createFromParcel(Parcel parcel) {
            return new C0257(parcel);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0257[] newArray(int i) {
            return new C0257[i];
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    final String f876;

    /* renamed from: ʼ  reason: contains not printable characters */
    final int f877;

    /* renamed from: ʽ  reason: contains not printable characters */
    final boolean f878;

    /* renamed from: ʾ  reason: contains not printable characters */
    final int f879;

    /* renamed from: ʿ  reason: contains not printable characters */
    final int f880;

    /* renamed from: ˆ  reason: contains not printable characters */
    final String f881;

    /* renamed from: ˈ  reason: contains not printable characters */
    final boolean f882;

    /* renamed from: ˉ  reason: contains not printable characters */
    final boolean f883;

    /* renamed from: ˊ  reason: contains not printable characters */
    final Bundle f884;

    /* renamed from: ˋ  reason: contains not printable characters */
    final boolean f885;

    /* renamed from: ˎ  reason: contains not printable characters */
    Bundle f886;

    /* renamed from: ˏ  reason: contains not printable characters */
    C0222 f887;

    public int describeContents() {
        return 0;
    }

    public C0257(C0222 r2) {
        this.f876 = r2.getClass().getName();
        this.f877 = r2.f729;
        this.f878 = r2.f745;
        this.f879 = r2.f764;
        this.f880 = r2.f765;
        this.f881 = r2.f766;
        this.f882 = r2.f755;
        this.f883 = r2.f753;
        this.f884 = r2.f733;
        this.f885 = r2.f767;
    }

    public C0257(Parcel parcel) {
        this.f876 = parcel.readString();
        this.f877 = parcel.readInt();
        boolean z = true;
        this.f878 = parcel.readInt() != 0;
        this.f879 = parcel.readInt();
        this.f880 = parcel.readInt();
        this.f881 = parcel.readString();
        this.f882 = parcel.readInt() != 0;
        this.f883 = parcel.readInt() != 0;
        this.f884 = parcel.readBundle();
        this.f885 = parcel.readInt() == 0 ? false : z;
        this.f886 = parcel.readBundle();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0222 m1558(C0237 r4, C0232 r5, C0222 r6, C0254 r7) {
        if (this.f887 == null) {
            Context r0 = r4.m1394();
            Bundle bundle = this.f884;
            if (bundle != null) {
                bundle.setClassLoader(r0.getClassLoader());
            }
            if (r5 != null) {
                this.f887 = r5.m1337(r0, this.f876, this.f884);
            } else {
                this.f887 = C0222.m1190(r0, this.f876, this.f884);
            }
            Bundle bundle2 = this.f886;
            if (bundle2 != null) {
                bundle2.setClassLoader(r0.getClassLoader());
                this.f887.f725 = this.f886;
            }
            this.f887.m1201(this.f877, r6);
            C0222 r52 = this.f887;
            r52.f745 = this.f878;
            r52.f749 = true;
            r52.f764 = this.f879;
            r52.f765 = this.f880;
            r52.f766 = this.f881;
            r52.f755 = this.f882;
            r52.f753 = this.f883;
            r52.f767 = this.f885;
            r52.f754 = r4.f800;
            if (C0246.f810) {
                Log.v("FragmentManager", "Instantiated fragment " + this.f887);
            }
        }
        C0222 r42 = this.f887;
        r42.f760 = r7;
        return r42;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f876);
        parcel.writeInt(this.f877);
        parcel.writeInt(this.f878 ? 1 : 0);
        parcel.writeInt(this.f879);
        parcel.writeInt(this.f880);
        parcel.writeString(this.f881);
        parcel.writeInt(this.f882 ? 1 : 0);
        parcel.writeInt(this.f883 ? 1 : 0);
        parcel.writeBundle(this.f884);
        parcel.writeInt(this.f885 ? 1 : 0);
        parcel.writeBundle(this.f886);
    }
}
