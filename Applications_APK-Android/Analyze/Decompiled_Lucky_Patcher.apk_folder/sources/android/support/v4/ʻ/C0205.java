package android.support.v4.ʻ;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.ʻ.C0201;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;

/* renamed from: android.support.v4.ʻ.ʾ  reason: contains not printable characters */
/* compiled from: BackStackRecord */
final class C0205 implements Parcelable {
    public static final Parcelable.Creator<C0205> CREATOR = new Parcelable.Creator<C0205>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public C0205 createFromParcel(Parcel parcel) {
            return new C0205(parcel);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0205[] newArray(int i) {
            return new C0205[i];
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    final int[] f684;

    /* renamed from: ʼ  reason: contains not printable characters */
    final int f685;

    /* renamed from: ʽ  reason: contains not printable characters */
    final int f686;

    /* renamed from: ʾ  reason: contains not printable characters */
    final String f687;

    /* renamed from: ʿ  reason: contains not printable characters */
    final int f688;

    /* renamed from: ˆ  reason: contains not printable characters */
    final int f689;

    /* renamed from: ˈ  reason: contains not printable characters */
    final CharSequence f690;

    /* renamed from: ˉ  reason: contains not printable characters */
    final int f691;

    /* renamed from: ˊ  reason: contains not printable characters */
    final CharSequence f692;

    /* renamed from: ˋ  reason: contains not printable characters */
    final ArrayList<String> f693;

    /* renamed from: ˎ  reason: contains not printable characters */
    final ArrayList<String> f694;

    /* renamed from: ˏ  reason: contains not printable characters */
    final boolean f695;

    public int describeContents() {
        return 0;
    }

    public C0205(C0201 r8) {
        int size = r8.f657.size();
        this.f684 = new int[(size * 6)];
        if (r8.f664) {
            int i = 0;
            int i2 = 0;
            while (i < size) {
                C0201.C0202 r3 = r8.f657.get(i);
                int i3 = i2 + 1;
                this.f684[i2] = r3.f677;
                int i4 = i3 + 1;
                this.f684[i3] = r3.f678 != null ? r3.f678.f729 : -1;
                int i5 = i4 + 1;
                this.f684[i4] = r3.f679;
                int i6 = i5 + 1;
                this.f684[i5] = r3.f680;
                int i7 = i6 + 1;
                this.f684[i6] = r3.f681;
                this.f684[i7] = r3.f682;
                i++;
                i2 = i7 + 1;
            }
            this.f685 = r8.f662;
            this.f686 = r8.f663;
            this.f687 = r8.f666;
            this.f688 = r8.f668;
            this.f689 = r8.f669;
            this.f690 = r8.f670;
            this.f691 = r8.f671;
            this.f692 = r8.f672;
            this.f693 = r8.f673;
            this.f694 = r8.f674;
            this.f695 = r8.f675;
            return;
        }
        throw new IllegalStateException("Not on back stack");
    }

    public C0205(Parcel parcel) {
        this.f684 = parcel.createIntArray();
        this.f685 = parcel.readInt();
        this.f686 = parcel.readInt();
        this.f687 = parcel.readString();
        this.f688 = parcel.readInt();
        this.f689 = parcel.readInt();
        this.f690 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f691 = parcel.readInt();
        this.f692 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f693 = parcel.createStringArrayList();
        this.f694 = parcel.createStringArrayList();
        this.f695 = parcel.readInt() != 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0201 m1166(C0246 r7) {
        C0201 r0 = new C0201(r7);
        int i = 0;
        int i2 = 0;
        while (i < this.f684.length) {
            C0201.C0202 r3 = new C0201.C0202();
            int i3 = i + 1;
            r3.f677 = this.f684[i];
            if (C0246.f810) {
                Log.v("FragmentManager", "Instantiate " + r0 + " op #" + i2 + " base fragment #" + this.f684[i3]);
            }
            int i4 = i3 + 1;
            int i5 = this.f684[i3];
            if (i5 >= 0) {
                r3.f678 = r7.f822.get(i5);
            } else {
                r3.f678 = null;
            }
            int[] iArr = this.f684;
            int i6 = i4 + 1;
            r3.f679 = iArr[i4];
            int i7 = i6 + 1;
            r3.f680 = iArr[i6];
            int i8 = i7 + 1;
            r3.f681 = iArr[i7];
            r3.f682 = iArr[i8];
            r0.f658 = r3.f679;
            r0.f659 = r3.f680;
            r0.f660 = r3.f681;
            r0.f661 = r3.f682;
            r0.m1150(r3);
            i2++;
            i = i8 + 1;
        }
        r0.f662 = this.f685;
        r0.f663 = this.f686;
        r0.f666 = this.f687;
        r0.f668 = this.f688;
        r0.f664 = true;
        r0.f669 = this.f689;
        r0.f670 = this.f690;
        r0.f671 = this.f691;
        r0.f672 = this.f692;
        r0.f673 = this.f693;
        r0.f674 = this.f694;
        r0.f675 = this.f695;
        r0.m1149(1);
        return r0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeIntArray(this.f684);
        parcel.writeInt(this.f685);
        parcel.writeInt(this.f686);
        parcel.writeString(this.f687);
        parcel.writeInt(this.f688);
        parcel.writeInt(this.f689);
        TextUtils.writeToParcel(this.f690, parcel, 0);
        parcel.writeInt(this.f691);
        TextUtils.writeToParcel(this.f692, parcel, 0);
        parcel.writeStringList(this.f693);
        parcel.writeStringList(this.f694);
        parcel.writeInt(this.f695 ? 1 : 0);
    }
}
