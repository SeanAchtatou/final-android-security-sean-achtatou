package android.support.v4.ʻ;

import android.graphics.Rect;
import android.os.Build;
import android.support.v4.ˈ.C0331;
import android.support.v4.ˉ.C0414;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/* renamed from: android.support.v4.ʻ.ᵎ  reason: contains not printable characters */
/* compiled from: FragmentTransition */
class C0263 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final int[] f899 = {0, 3, 0, 1, 5, 4, 7, 6, 9, 8};

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m1586(C0246 r7, ArrayList<C0201> arrayList, ArrayList<Boolean> arrayList2, int i, int i2, boolean z) {
        if (r7.f829 >= 1 && Build.VERSION.SDK_INT >= 21) {
            SparseArray sparseArray = new SparseArray();
            for (int i3 = i; i3 < i2; i3++) {
                C0201 r2 = arrayList.get(i3);
                if (arrayList2.get(i3).booleanValue()) {
                    m1598(r2, sparseArray, z);
                } else {
                    m1583(r2, sparseArray, z);
                }
            }
            if (sparseArray.size() != 0) {
                View view = new View(r7.f830.m1394());
                int size = sparseArray.size();
                for (int i4 = 0; i4 < size; i4++) {
                    int keyAt = sparseArray.keyAt(i4);
                    C0331<String, String> r5 = m1573(keyAt, arrayList, arrayList2, i, i2);
                    C0264 r6 = (C0264) sparseArray.valueAt(i4);
                    if (z) {
                        m1585(r7, keyAt, r6, view, r5);
                    } else {
                        m1600(r7, keyAt, r6, view, r5);
                    }
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static C0331<String, String> m1573(int i, ArrayList<C0201> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        ArrayList<String> arrayList3;
        ArrayList<String> arrayList4;
        C0331<String, String> r0 = new C0331<>();
        for (int i4 = i3 - 1; i4 >= i2; i4--) {
            C0201 r1 = arrayList.get(i4);
            if (r1.m1160(i)) {
                boolean booleanValue = arrayList2.get(i4).booleanValue();
                if (r1.f673 != null) {
                    int size = r1.f673.size();
                    if (booleanValue) {
                        arrayList3 = r1.f673;
                        arrayList4 = r1.f674;
                    } else {
                        ArrayList<String> arrayList5 = r1.f673;
                        arrayList3 = r1.f674;
                        arrayList4 = arrayList5;
                    }
                    for (int i5 = 0; i5 < size; i5++) {
                        String str = (String) arrayList4.get(i5);
                        String str2 = (String) arrayList3.get(i5);
                        String remove = r0.remove(str2);
                        if (remove != null) {
                            r0.put(str, remove);
                        } else {
                            r0.put(str, str2);
                        }
                    }
                }
            }
        }
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m1585(C0246 r16, int i, C0264 r18, View view, C0331<String, String> r20) {
        Object obj;
        C0246 r0 = r16;
        C0264 r3 = r18;
        View view2 = view;
        ViewGroup viewGroup = r0.f831.m1339() ? (ViewGroup) r0.f831.m1338(i) : null;
        if (viewGroup != null) {
            C0222 r10 = r3.f925;
            C0222 r11 = r3.f928;
            boolean z = r3.f926;
            boolean z2 = r3.f929;
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            Object r15 = m1577(r10, z);
            Object r7 = m1595(r11, z2);
            Object obj2 = r7;
            Object r02 = m1578(viewGroup, view, r20, r18, arrayList2, arrayList, r15, r7);
            if (r15 == null && r02 == null) {
                obj = obj2;
                if (obj == null) {
                    return;
                }
            } else {
                obj = obj2;
            }
            ArrayList<View> r5 = m1597(obj, r11, arrayList2, view2);
            ArrayList<View> r8 = m1597(r15, r10, arrayList, view2);
            m1601(r8, 4);
            Object r102 = m1579(r15, obj, r02, r10, z);
            if (r102 != null) {
                m1589(obj, r11, r5);
                ArrayList<String> r112 = C0265.m1606((ArrayList<View>) arrayList);
                C0265.m1615(r102, r15, r8, obj, r5, r02, arrayList);
                C0265.m1610(viewGroup, r102);
                C0265.m1608(viewGroup, arrayList2, arrayList, r112, r20);
                m1601(r8, 0);
                C0265.m1617(r02, (ArrayList<View>) arrayList2, (ArrayList<View>) arrayList);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m1589(Object obj, C0222 r2, final ArrayList<View> arrayList) {
        if (r2 != null && obj != null && r2.f741 && r2.f767 && r2.f759) {
            r2.m1262(true);
            C0265.m1628(obj, r2.m1268(), arrayList);
            C0255.m1554(r2.f728, new Runnable() {
                public void run() {
                    C0263.m1601(arrayList, 4);
                }
            });
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.ᵔ.ʻ(android.view.View, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void
     arg types: [android.view.ViewGroup, java.util.ArrayList, android.support.v4.ˈ.ʻ<java.lang.String, java.lang.String>]
     candidates:
      android.support.v4.ʻ.ᵔ.ʻ(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      android.support.v4.ʻ.ᵔ.ʻ(android.view.ViewGroup, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void
      android.support.v4.ʻ.ᵔ.ʻ(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      android.support.v4.ʻ.ᵔ.ʻ(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void
      android.support.v4.ʻ.ᵔ.ʻ(java.util.List<android.view.View>, android.view.View, int):boolean
      android.support.v4.ʻ.ᵔ.ʻ(android.view.View, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.ᵔ.ʻ(android.view.ViewGroup, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void
     arg types: [android.view.ViewGroup, java.util.ArrayList, android.support.v4.ˈ.ʻ<java.lang.String, java.lang.String>]
     candidates:
      android.support.v4.ʻ.ᵔ.ʻ(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      android.support.v4.ʻ.ᵔ.ʻ(android.view.View, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void
      android.support.v4.ʻ.ᵔ.ʻ(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      android.support.v4.ʻ.ᵔ.ʻ(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void
      android.support.v4.ʻ.ᵔ.ʻ(java.util.List<android.view.View>, android.view.View, int):boolean
      android.support.v4.ʻ.ᵔ.ʻ(android.view.ViewGroup, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void */
    /* renamed from: ʼ  reason: contains not printable characters */
    private static void m1600(C0246 r17, int i, C0264 r19, View view, C0331<String, String> r21) {
        Object obj;
        C0246 r0 = r17;
        C0264 r8 = r19;
        View view2 = view;
        C0331<String, String> r10 = r21;
        ViewGroup viewGroup = r0.f831.m1339() ? (ViewGroup) r0.f831.m1338(i) : null;
        if (viewGroup != null) {
            C0222 r13 = r8.f925;
            C0222 r14 = r8.f928;
            boolean z = r8.f926;
            boolean z2 = r8.f929;
            Object r15 = m1577(r13, z);
            Object r7 = m1595(r14, z2);
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = arrayList2;
            ArrayList arrayList4 = arrayList;
            Object obj2 = r7;
            Object r72 = m1596(viewGroup, view, r21, r19, arrayList, arrayList2, r15, r7);
            if (r15 == null && r72 == null) {
                obj = obj2;
                if (obj == null) {
                    return;
                }
            } else {
                obj = obj2;
            }
            ArrayList<View> r11 = m1597(obj, r14, arrayList4, view2);
            Object obj3 = (r11 == null || r11.isEmpty()) ? null : obj;
            C0265.m1627(r15, view2);
            Object r1 = m1579(r15, obj3, r72, r13, r8.f926);
            if (r1 != null) {
                ArrayList arrayList5 = new ArrayList();
                C0265.m1615(r1, r15, arrayList5, obj3, r11, r72, arrayList3);
                m1588(viewGroup, r13, view, arrayList3, r15, arrayList5, obj3, r11);
                ArrayList arrayList6 = arrayList3;
                C0265.m1609((View) viewGroup, (ArrayList<View>) arrayList6, (Map<String, String>) r10);
                C0265.m1610(viewGroup, r1);
                C0265.m1611(viewGroup, (ArrayList<View>) arrayList6, (Map<String, String>) r10);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m1588(ViewGroup viewGroup, C0222 r10, View view, ArrayList<View> arrayList, Object obj, ArrayList<View> arrayList2, Object obj2, ArrayList<View> arrayList3) {
        final Object obj3 = obj;
        final View view2 = view;
        final C0222 r3 = r10;
        final ArrayList<View> arrayList4 = arrayList;
        final ArrayList<View> arrayList5 = arrayList2;
        final ArrayList<View> arrayList6 = arrayList3;
        final Object obj4 = obj2;
        C0255.m1554(viewGroup, new Runnable() {
            public void run() {
                Object obj = obj3;
                if (obj != null) {
                    C0265.m1630(obj, view2);
                    arrayList5.addAll(C0263.m1597(obj3, r3, arrayList4, view2));
                }
                if (arrayList6 != null) {
                    if (obj4 != null) {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(view2);
                        C0265.m1629(obj4, (ArrayList<View>) arrayList6, (ArrayList<View>) arrayList);
                    }
                    arrayList6.clear();
                    arrayList6.add(view2);
                }
            }
        });
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Object m1576(C0222 r0, C0222 r1, boolean z) {
        Object obj;
        if (r0 == null || r1 == null) {
            return null;
        }
        if (z) {
            obj = r1.m1301();
        } else {
            obj = r0.m1300();
        }
        return C0265.m1624(C0265.m1603(obj));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Object m1577(C0222 r0, boolean z) {
        Object obj;
        if (r0 == null) {
            return null;
        }
        if (z) {
            obj = r0.m1298();
        } else {
            obj = r0.m1292();
        }
        return C0265.m1603(obj);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static Object m1595(C0222 r0, boolean z) {
        Object obj;
        if (r0 == null) {
            return null;
        }
        if (z) {
            obj = r0.m1294();
        } else {
            obj = r0.m1296();
        }
        return C0265.m1603(obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.ᵎ.ʼ(android.support.v4.ʻ.ˉ, android.support.v4.ʻ.ˉ, boolean, android.support.v4.ˈ.ʻ<java.lang.String, android.view.View>, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, android.support.v4.ʻ.ˉ, boolean, android.support.v4.ˈ.ʻ<java.lang.String, android.view.View>, int]
     candidates:
      android.support.v4.ʻ.ᵎ.ʼ(android.support.v4.ʻ.י, int, android.support.v4.ʻ.ᵎ$ʻ, android.view.View, android.support.v4.ˈ.ʻ<java.lang.String, java.lang.String>):void
      android.support.v4.ʻ.ᵎ.ʼ(android.support.v4.ʻ.ˉ, android.support.v4.ʻ.ˉ, boolean, android.support.v4.ˈ.ʻ<java.lang.String, android.view.View>, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    private static Object m1578(ViewGroup viewGroup, View view, C0331<String, String> r10, C0264 r11, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        Object obj3;
        final Rect rect;
        final View view2;
        C0222 r0 = r11.f925;
        C0222 r1 = r11.f928;
        if (r0 != null) {
            r0.m1268().setVisibility(0);
        }
        if (r0 == null || r1 == null) {
            return null;
        }
        boolean z = r11.f926;
        if (r10.isEmpty()) {
            obj3 = null;
        } else {
            obj3 = m1576(r0, r1, z);
        }
        C0331<String, View> r5 = m1593(r10, obj3, r11);
        C0331<String, View> r6 = m1602(r10, obj3, r11);
        if (r10.isEmpty()) {
            if (r5 != null) {
                r5.clear();
            }
            if (r6 != null) {
                r6.clear();
            }
            obj3 = null;
        } else {
            m1592(arrayList, r5, r10.keySet());
            m1592(arrayList2, r6, r10.values());
        }
        if (obj == null && obj2 == null && obj3 == null) {
            return null;
        }
        m1599(r0, r1, z, r5, true);
        if (obj3 != null) {
            arrayList2.add(view);
            C0265.m1614(obj3, view, arrayList);
            m1590(obj3, obj2, r5, r11.f929, r11.f930);
            Rect rect2 = new Rect();
            View r102 = m1594(r6, r11, obj, z);
            if (r102 != null) {
                C0265.m1612(obj, rect2);
            }
            rect = rect2;
            view2 = r102;
        } else {
            view2 = null;
            rect = null;
        }
        final C0222 r103 = r0;
        final C0222 r112 = r1;
        final boolean z2 = z;
        final C0331<String, View> r13 = r6;
        C0255.m1554(viewGroup, new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v4.ʻ.ᵎ.ʻ(android.support.v4.ʻ.ˉ, android.support.v4.ʻ.ˉ, boolean, android.support.v4.ˈ.ʻ, boolean):void
             arg types: [android.support.v4.ʻ.ˉ, android.support.v4.ʻ.ˉ, boolean, android.support.v4.ˈ.ʻ, int]
             candidates:
              android.support.v4.ʻ.ᵎ.ʻ(int, java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int):android.support.v4.ˈ.ʻ<java.lang.String, java.lang.String>
              android.support.v4.ʻ.ᵎ.ʻ(java.lang.Object, java.lang.Object, java.lang.Object, android.support.v4.ʻ.ˉ, boolean):java.lang.Object
              android.support.v4.ʻ.ᵎ.ʻ(android.support.v4.ʻ.ʽ, android.support.v4.ʻ.ʽ$ʻ, android.util.SparseArray<android.support.v4.ʻ.ᵎ$ʻ>, boolean, boolean):void
              android.support.v4.ʻ.ᵎ.ʻ(android.support.v4.ʻ.י, int, android.support.v4.ʻ.ᵎ$ʻ, android.view.View, android.support.v4.ˈ.ʻ<java.lang.String, java.lang.String>):void
              android.support.v4.ʻ.ᵎ.ʻ(java.lang.Object, java.lang.Object, android.support.v4.ˈ.ʻ<java.lang.String, android.view.View>, boolean, android.support.v4.ʻ.ʽ):void
              android.support.v4.ʻ.ᵎ.ʻ(android.support.v4.ʻ.ˉ, android.support.v4.ʻ.ˉ, boolean, android.support.v4.ˈ.ʻ, boolean):void */
            public void run() {
                C0263.m1599(r103, r112, z2, r13, false);
                View view = view2;
                if (view != null) {
                    C0265.m1607(view, rect);
                }
            }
        });
        return obj3;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m1592(ArrayList<View> arrayList, C0331<String, View> r4, Collection<String> collection) {
        for (int size = r4.size() - 1; size >= 0; size--) {
            View r1 = r4.m1980(size);
            if (collection.contains(C0414.m2244(r1))) {
                arrayList.add(r1);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.ᵎ.ʼ(android.support.v4.ʻ.ˉ, android.support.v4.ʻ.ˉ, boolean, android.support.v4.ˈ.ʻ<java.lang.String, android.view.View>, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, android.support.v4.ʻ.ˉ, boolean, android.support.v4.ˈ.ʻ<java.lang.String, android.view.View>, int]
     candidates:
      android.support.v4.ʻ.ᵎ.ʼ(android.support.v4.ʻ.י, int, android.support.v4.ʻ.ᵎ$ʻ, android.view.View, android.support.v4.ˈ.ʻ<java.lang.String, java.lang.String>):void
      android.support.v4.ʻ.ᵎ.ʼ(android.support.v4.ʻ.ˉ, android.support.v4.ʻ.ˉ, boolean, android.support.v4.ˈ.ʻ<java.lang.String, android.view.View>, boolean):void */
    /* renamed from: ʼ  reason: contains not printable characters */
    private static Object m1596(ViewGroup viewGroup, View view, C0331<String, String> r17, C0264 r18, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        Object obj3;
        C0331<String, String> r2;
        Object obj4;
        C0264 r3 = r18;
        ArrayList<View> arrayList3 = arrayList;
        Object obj5 = obj;
        Object obj6 = obj2;
        final C0222 r6 = r3.f925;
        final C0222 r7 = r3.f928;
        Rect rect = null;
        if (r6 == null || r7 == null) {
            return null;
        }
        final boolean z = r3.f926;
        if (r17.isEmpty()) {
            r2 = r17;
            obj3 = null;
        } else {
            obj3 = m1576(r6, r7, z);
            r2 = r17;
        }
        C0331<String, View> r5 = m1593(r2, obj3, r3);
        if (r17.isEmpty()) {
            obj4 = null;
        } else {
            arrayList3.addAll(r5.values());
            obj4 = obj3;
        }
        if (obj5 == null && obj6 == null && obj4 == null) {
            return null;
        }
        m1599(r6, r7, z, r5, true);
        if (obj4 != null) {
            rect = new Rect();
            C0265.m1614(obj4, view, arrayList3);
            m1590(obj4, obj6, r5, r3.f929, r3.f930);
            if (obj5 != null) {
                C0265.m1612(obj5, rect);
            }
        }
        Rect rect2 = rect;
        final C0331<String, String> r1 = r17;
        final Object obj7 = obj4;
        final C0264 r32 = r18;
        final ArrayList<View> arrayList4 = arrayList2;
        final View view2 = view;
        final ArrayList<View> arrayList5 = arrayList;
        final Object obj8 = obj;
        final Rect rect3 = rect2;
        C0255.m1554(viewGroup, new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v4.ʻ.ᵎ.ʻ(android.support.v4.ʻ.ˉ, android.support.v4.ʻ.ˉ, boolean, android.support.v4.ˈ.ʻ, boolean):void
             arg types: [android.support.v4.ʻ.ˉ, android.support.v4.ʻ.ˉ, boolean, android.support.v4.ˈ.ʻ, int]
             candidates:
              android.support.v4.ʻ.ᵎ.ʻ(int, java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int):android.support.v4.ˈ.ʻ<java.lang.String, java.lang.String>
              android.support.v4.ʻ.ᵎ.ʻ(java.lang.Object, java.lang.Object, java.lang.Object, android.support.v4.ʻ.ˉ, boolean):java.lang.Object
              android.support.v4.ʻ.ᵎ.ʻ(android.support.v4.ʻ.ʽ, android.support.v4.ʻ.ʽ$ʻ, android.util.SparseArray<android.support.v4.ʻ.ᵎ$ʻ>, boolean, boolean):void
              android.support.v4.ʻ.ᵎ.ʻ(android.support.v4.ʻ.י, int, android.support.v4.ʻ.ᵎ$ʻ, android.view.View, android.support.v4.ˈ.ʻ<java.lang.String, java.lang.String>):void
              android.support.v4.ʻ.ᵎ.ʻ(java.lang.Object, java.lang.Object, android.support.v4.ˈ.ʻ<java.lang.String, android.view.View>, boolean, android.support.v4.ʻ.ʽ):void
              android.support.v4.ʻ.ᵎ.ʻ(android.support.v4.ʻ.ˉ, android.support.v4.ʻ.ˉ, boolean, android.support.v4.ˈ.ʻ, boolean):void */
            public void run() {
                C0331 r0 = C0263.m1602(r1, obj7, r32);
                if (r0 != null) {
                    arrayList4.addAll(r0.values());
                    arrayList4.add(view2);
                }
                C0263.m1599(r6, r7, z, r0, false);
                Object obj = obj7;
                if (obj != null) {
                    C0265.m1617(obj, (ArrayList<View>) arrayList5, (ArrayList<View>) arrayList4);
                    View r02 = C0263.m1594(r0, r32, obj8, z);
                    if (r02 != null) {
                        C0265.m1607(r02, rect3);
                    }
                }
            }
        });
        return obj4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.ᵔ.ʻ(java.util.Map<java.lang.String, android.view.View>, android.view.View):void
     arg types: [android.support.v4.ˈ.ʻ<java.lang.String, android.view.View>, android.view.View]
     candidates:
      android.support.v4.ʻ.ᵔ.ʻ(java.util.Map, java.lang.String):java.lang.String
      android.support.v4.ʻ.ᵔ.ʻ(android.view.View, android.graphics.Rect):void
      android.support.v4.ʻ.ᵔ.ʻ(android.view.ViewGroup, java.lang.Object):void
      android.support.v4.ʻ.ᵔ.ʻ(java.lang.Object, android.graphics.Rect):void
      android.support.v4.ʻ.ᵔ.ʻ(java.lang.Object, android.view.View):void
      android.support.v4.ʻ.ᵔ.ʻ(java.lang.Object, java.util.ArrayList<android.view.View>):void
      android.support.v4.ʻ.ᵔ.ʻ(java.util.ArrayList<android.view.View>, android.view.View):void
      android.support.v4.ʻ.ᵔ.ʻ(java.util.List<android.view.View>, android.view.View):void
      android.support.v4.ʻ.ᵔ.ʻ(java.util.Map<java.lang.String, android.view.View>, android.view.View):void */
    /* renamed from: ʼ  reason: contains not printable characters */
    private static C0331<String, View> m1593(C0331<String, String> r4, Object obj, C0264 r6) {
        ArrayList<String> arrayList;
        C0230 r5;
        if (r4.isEmpty() || obj == null) {
            r4.clear();
            return null;
        }
        C0222 r52 = r6.f928;
        C0331<String, View> r0 = new C0331<>();
        C0265.m1620((Map<String, View>) r0, r52.m1268());
        C0201 r1 = r6.f930;
        if (r6.f929) {
            r5 = r52.m1293();
            arrayList = r1.f674;
        } else {
            r5 = r52.m1281();
            arrayList = r1.f673;
        }
        r0.m1884((Collection<?>) arrayList);
        if (r5 != null) {
            r5.m1334(arrayList, r0);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = (String) arrayList.get(size);
                View view = r0.get(str);
                if (view == null) {
                    r4.remove(str);
                } else if (!str.equals(C0414.m2244(view))) {
                    r4.put(C0414.m2244(view), r4.remove(str));
                }
            }
        } else {
            r4.m1884((Collection<?>) r0.keySet());
        }
        return r0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.ᵔ.ʻ(java.util.Map<java.lang.String, android.view.View>, android.view.View):void
     arg types: [android.support.v4.ˈ.ʻ<java.lang.String, android.view.View>, android.view.View]
     candidates:
      android.support.v4.ʻ.ᵔ.ʻ(java.util.Map, java.lang.String):java.lang.String
      android.support.v4.ʻ.ᵔ.ʻ(android.view.View, android.graphics.Rect):void
      android.support.v4.ʻ.ᵔ.ʻ(android.view.ViewGroup, java.lang.Object):void
      android.support.v4.ʻ.ᵔ.ʻ(java.lang.Object, android.graphics.Rect):void
      android.support.v4.ʻ.ᵔ.ʻ(java.lang.Object, android.view.View):void
      android.support.v4.ʻ.ᵔ.ʻ(java.lang.Object, java.util.ArrayList<android.view.View>):void
      android.support.v4.ʻ.ᵔ.ʻ(java.util.ArrayList<android.view.View>, android.view.View):void
      android.support.v4.ʻ.ᵔ.ʻ(java.util.List<android.view.View>, android.view.View):void
      android.support.v4.ʻ.ᵔ.ʻ(java.util.Map<java.lang.String, android.view.View>, android.view.View):void */
    /* access modifiers changed from: private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public static C0331<String, View> m1602(C0331<String, String> r4, Object obj, C0264 r6) {
        C0230 r62;
        ArrayList<String> arrayList;
        String r1;
        C0222 r0 = r6.f925;
        View r12 = r0.m1268();
        if (r4.isEmpty() || obj == null || r12 == null) {
            r4.clear();
            return null;
        }
        C0331<String, View> r5 = new C0331<>();
        C0265.m1620((Map<String, View>) r5, r12);
        C0201 r13 = r6.f927;
        if (r6.f926) {
            r62 = r0.m1281();
            arrayList = r13.f673;
        } else {
            r62 = r0.m1293();
            arrayList = r13.f674;
        }
        if (arrayList != null) {
            r5.m1884((Collection<?>) arrayList);
        }
        if (r62 != null) {
            r62.m1334(arrayList, r5);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = (String) arrayList.get(size);
                View view = r5.get(str);
                if (view == null) {
                    String r14 = m1580(r4, str);
                    if (r14 != null) {
                        r4.remove(r14);
                    }
                } else if (!str.equals(C0414.m2244(view)) && (r1 = m1580(r4, str)) != null) {
                    r4.put(r1, C0414.m2244(view));
                }
            }
        } else {
            m1587(r4, r5);
        }
        return r5;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static String m1580(C0331<String, String> r3, String str) {
        int size = r3.size();
        for (int i = 0; i < size; i++) {
            if (str.equals(r3.m1980(i))) {
                return r3.m1979(i);
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public static View m1594(C0331<String, View> r0, C0264 r1, Object obj, boolean z) {
        String str;
        C0201 r12 = r1.f927;
        if (obj == null || r0 == null || r12.f673 == null || r12.f673.isEmpty()) {
            return null;
        }
        if (z) {
            str = r12.f673.get(0);
        } else {
            str = r12.f674.get(0);
        }
        return r0.get(str);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m1590(Object obj, Object obj2, C0331<String, View> r3, boolean z, C0201 r5) {
        String str;
        if (r5.f673 != null && !r5.f673.isEmpty()) {
            if (z) {
                str = r5.f674.get(0);
            } else {
                str = r5.f673.get(0);
            }
            View view = r3.get(str);
            C0265.m1613(obj, view);
            if (obj2 != null) {
                C0265.m1613(obj2, view);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m1587(C0331<String, String> r2, C0331<String, View> r3) {
        for (int size = r2.size() - 1; size >= 0; size--) {
            if (!r3.containsKey(r2.m1980(size))) {
                r2.m1981(size);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public static void m1599(C0222 r3, C0222 r4, boolean z, C0331<String, View> r6, boolean z2) {
        C0230 r32;
        int i;
        if (z) {
            r32 = r4.m1293();
        } else {
            r32 = r3.m1293();
        }
        if (r32 != null) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (r6 == null) {
                i = 0;
            } else {
                i = r6.size();
            }
            for (int i2 = 0; i2 < i; i2++) {
                arrayList2.add(r6.m1979(i2));
                arrayList.add(r6.m1980(i2));
            }
            if (z2) {
                r32.m1333(arrayList2, arrayList, (List<View>) null);
            } else {
                r32.m1335(arrayList2, arrayList, null);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public static ArrayList<View> m1597(Object obj, C0222 r2, ArrayList<View> arrayList, View view) {
        if (obj == null) {
            return null;
        }
        ArrayList<View> arrayList2 = new ArrayList<>();
        View r22 = r2.m1268();
        if (r22 != null) {
            C0265.m1618(arrayList2, r22);
        }
        if (arrayList != null) {
            arrayList2.removeAll(arrayList);
        }
        if (arrayList2.isEmpty()) {
            return arrayList2;
        }
        arrayList2.add(view);
        C0265.m1616(obj, arrayList2);
        return arrayList2;
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public static void m1601(ArrayList<View> arrayList, int i) {
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                arrayList.get(size).setVisibility(i);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Object m1579(Object obj, Object obj2, Object obj3, C0222 r3, boolean z) {
        boolean z2;
        if (obj == null || obj2 == null || r3 == null) {
            z2 = true;
        } else {
            z2 = z ? r3.m1287() : r3.m1302();
        }
        if (z2) {
            return C0265.m1604(obj2, obj, obj3);
        }
        return C0265.m1625(obj2, obj, obj3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.ᵎ.ʻ(android.support.v4.ʻ.ʽ, android.support.v4.ʻ.ʽ$ʻ, android.util.SparseArray<android.support.v4.ʻ.ᵎ$ʻ>, boolean, boolean):void
     arg types: [android.support.v4.ʻ.ʽ, android.support.v4.ʻ.ʽ$ʻ, android.util.SparseArray<android.support.v4.ʻ.ᵎ$ʻ>, int, boolean]
     candidates:
      android.support.v4.ʻ.ᵎ.ʻ(int, java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int):android.support.v4.ˈ.ʻ<java.lang.String, java.lang.String>
      android.support.v4.ʻ.ᵎ.ʻ(java.lang.Object, java.lang.Object, java.lang.Object, android.support.v4.ʻ.ˉ, boolean):java.lang.Object
      android.support.v4.ʻ.ᵎ.ʻ(android.support.v4.ʻ.ˉ, android.support.v4.ʻ.ˉ, boolean, android.support.v4.ˈ.ʻ, boolean):void
      android.support.v4.ʻ.ᵎ.ʻ(android.support.v4.ʻ.י, int, android.support.v4.ʻ.ᵎ$ʻ, android.view.View, android.support.v4.ˈ.ʻ<java.lang.String, java.lang.String>):void
      android.support.v4.ʻ.ᵎ.ʻ(java.lang.Object, java.lang.Object, android.support.v4.ˈ.ʻ<java.lang.String, android.view.View>, boolean, android.support.v4.ʻ.ʽ):void
      android.support.v4.ʻ.ᵎ.ʻ(android.support.v4.ʻ.ʽ, android.support.v4.ʻ.ʽ$ʻ, android.util.SparseArray<android.support.v4.ʻ.ᵎ$ʻ>, boolean, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1583(C0201 r4, SparseArray<C0264> sparseArray, boolean z) {
        int size = r4.f657.size();
        for (int i = 0; i < size; i++) {
            m1582(r4, r4.f657.get(i), sparseArray, false, z);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.ᵎ.ʻ(android.support.v4.ʻ.ʽ, android.support.v4.ʻ.ʽ$ʻ, android.util.SparseArray<android.support.v4.ʻ.ᵎ$ʻ>, boolean, boolean):void
     arg types: [android.support.v4.ʻ.ʽ, android.support.v4.ʻ.ʽ$ʻ, android.util.SparseArray<android.support.v4.ʻ.ᵎ$ʻ>, int, boolean]
     candidates:
      android.support.v4.ʻ.ᵎ.ʻ(int, java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int):android.support.v4.ˈ.ʻ<java.lang.String, java.lang.String>
      android.support.v4.ʻ.ᵎ.ʻ(java.lang.Object, java.lang.Object, java.lang.Object, android.support.v4.ʻ.ˉ, boolean):java.lang.Object
      android.support.v4.ʻ.ᵎ.ʻ(android.support.v4.ʻ.ˉ, android.support.v4.ʻ.ˉ, boolean, android.support.v4.ˈ.ʻ, boolean):void
      android.support.v4.ʻ.ᵎ.ʻ(android.support.v4.ʻ.י, int, android.support.v4.ʻ.ᵎ$ʻ, android.view.View, android.support.v4.ˈ.ʻ<java.lang.String, java.lang.String>):void
      android.support.v4.ʻ.ᵎ.ʻ(java.lang.Object, java.lang.Object, android.support.v4.ˈ.ʻ<java.lang.String, android.view.View>, boolean, android.support.v4.ʻ.ʽ):void
      android.support.v4.ʻ.ᵎ.ʻ(android.support.v4.ʻ.ʽ, android.support.v4.ʻ.ʽ$ʻ, android.util.SparseArray<android.support.v4.ʻ.ᵎ$ʻ>, boolean, boolean):void */
    /* renamed from: ʼ  reason: contains not printable characters */
    public static void m1598(C0201 r3, SparseArray<C0264> sparseArray, boolean z) {
        if (r3.f656.f831.m1339()) {
            for (int size = r3.f657.size() - 1; size >= 0; size--) {
                m1582(r3, r3.f657.get(size), sparseArray, true, z);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, int, int, int, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.ˈ.ʼ<android.support.v4.ʻ.ˉ>):int
      android.support.v4.ʻ.י.ʻ(android.content.Context, float, float, float, float):android.support.v4.ʻ.י$ʽ
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י, android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0041, code lost:
        if (r10.f741 != false) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0078, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0094, code lost:
        if (r10.f767 == false) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0096, code lost:
        r1 = true;
     */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x00e9 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:96:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m1582(android.support.v4.ʻ.C0201 r16, android.support.v4.ʻ.C0201.C0202 r17, android.util.SparseArray<android.support.v4.ʻ.C0263.C0264> r18, boolean r19, boolean r20) {
        /*
            r0 = r16
            r1 = r17
            r2 = r18
            r3 = r19
            android.support.v4.ʻ.ˉ r10 = r1.f678
            if (r10 != 0) goto L_0x000d
            return
        L_0x000d:
            int r11 = r10.f765
            if (r11 != 0) goto L_0x0012
            return
        L_0x0012:
            if (r3 == 0) goto L_0x001b
            int[] r4 = android.support.v4.ʻ.C0263.f899
            int r1 = r1.f677
            r1 = r4[r1]
            goto L_0x001d
        L_0x001b:
            int r1 = r1.f677
        L_0x001d:
            r4 = 0
            r5 = 1
            if (r1 == r5) goto L_0x0089
            r6 = 3
            if (r1 == r6) goto L_0x005f
            r6 = 4
            if (r1 == r6) goto L_0x0047
            r6 = 5
            if (r1 == r6) goto L_0x0035
            r6 = 6
            if (r1 == r6) goto L_0x005f
            r6 = 7
            if (r1 == r6) goto L_0x0089
            r1 = 0
        L_0x0031:
            r12 = 0
            r13 = 0
            goto L_0x009c
        L_0x0035:
            if (r20 == 0) goto L_0x0044
            boolean r1 = r10.f759
            if (r1 == 0) goto L_0x0098
            boolean r1 = r10.f767
            if (r1 != 0) goto L_0x0098
            boolean r1 = r10.f741
            if (r1 == 0) goto L_0x0098
            goto L_0x0096
        L_0x0044:
            boolean r1 = r10.f767
            goto L_0x0099
        L_0x0047:
            if (r20 == 0) goto L_0x0056
            boolean r1 = r10.f759
            if (r1 == 0) goto L_0x007a
            boolean r1 = r10.f741
            if (r1 == 0) goto L_0x007a
            boolean r1 = r10.f767
            if (r1 == 0) goto L_0x007a
        L_0x0055:
            goto L_0x0078
        L_0x0056:
            boolean r1 = r10.f741
            if (r1 == 0) goto L_0x007a
            boolean r1 = r10.f767
            if (r1 != 0) goto L_0x007a
            goto L_0x0055
        L_0x005f:
            if (r20 == 0) goto L_0x007c
            boolean r1 = r10.f741
            if (r1 != 0) goto L_0x007a
            android.view.View r1 = r10.f750
            if (r1 == 0) goto L_0x007a
            android.view.View r1 = r10.f750
            int r1 = r1.getVisibility()
            if (r1 != 0) goto L_0x007a
            float r1 = r10.f748
            r6 = 0
            int r1 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r1 < 0) goto L_0x007a
        L_0x0078:
            r1 = 1
            goto L_0x0085
        L_0x007a:
            r1 = 0
            goto L_0x0085
        L_0x007c:
            boolean r1 = r10.f741
            if (r1 == 0) goto L_0x007a
            boolean r1 = r10.f767
            if (r1 != 0) goto L_0x007a
            goto L_0x0078
        L_0x0085:
            r13 = r1
            r1 = 0
            r12 = 1
            goto L_0x009c
        L_0x0089:
            if (r20 == 0) goto L_0x008e
            boolean r1 = r10.f746
            goto L_0x0099
        L_0x008e:
            boolean r1 = r10.f741
            if (r1 != 0) goto L_0x0098
            boolean r1 = r10.f767
            if (r1 != 0) goto L_0x0098
        L_0x0096:
            r1 = 1
            goto L_0x0099
        L_0x0098:
            r1 = 0
        L_0x0099:
            r4 = r1
            r1 = 1
            goto L_0x0031
        L_0x009c:
            java.lang.Object r6 = r2.get(r11)
            android.support.v4.ʻ.ᵎ$ʻ r6 = (android.support.v4.ʻ.C0263.C0264) r6
            if (r4 == 0) goto L_0x00ae
            android.support.v4.ʻ.ᵎ$ʻ r6 = m1572(r6, r2, r11)
            r6.f925 = r10
            r6.f926 = r3
            r6.f927 = r0
        L_0x00ae:
            r14 = r6
            r15 = 0
            if (r20 != 0) goto L_0x00d5
            if (r1 == 0) goto L_0x00d5
            if (r14 == 0) goto L_0x00bc
            android.support.v4.ʻ.ˉ r1 = r14.f928
            if (r1 != r10) goto L_0x00bc
            r14.f928 = r15
        L_0x00bc:
            android.support.v4.ʻ.י r4 = r0.f656
            int r1 = r10.f723
            if (r1 >= r5) goto L_0x00d5
            int r1 = r4.f829
            if (r1 < r5) goto L_0x00d5
            boolean r1 = r0.f675
            if (r1 != 0) goto L_0x00d5
            r4.m1512(r10)
            r6 = 1
            r7 = 0
            r8 = 0
            r9 = 0
            r5 = r10
            r4.m1474(r5, r6, r7, r8, r9)
        L_0x00d5:
            if (r13 == 0) goto L_0x00e7
            if (r14 == 0) goto L_0x00dd
            android.support.v4.ʻ.ˉ r1 = r14.f928
            if (r1 != 0) goto L_0x00e7
        L_0x00dd:
            android.support.v4.ʻ.ᵎ$ʻ r14 = m1572(r14, r2, r11)
            r14.f928 = r10
            r14.f929 = r3
            r14.f930 = r0
        L_0x00e7:
            if (r20 != 0) goto L_0x00f3
            if (r12 == 0) goto L_0x00f3
            if (r14 == 0) goto L_0x00f3
            android.support.v4.ʻ.ˉ r0 = r14.f925
            if (r0 != r10) goto L_0x00f3
            r14.f925 = r15
        L_0x00f3:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.ʻ.C0263.m1582(android.support.v4.ʻ.ʽ, android.support.v4.ʻ.ʽ$ʻ, android.util.SparseArray, boolean, boolean):void");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static C0264 m1572(C0264 r0, SparseArray<C0264> sparseArray, int i) {
        if (r0 != null) {
            return r0;
        }
        C0264 r02 = new C0264();
        sparseArray.put(i, r02);
        return r02;
    }

    /* renamed from: android.support.v4.ʻ.ᵎ$ʻ  reason: contains not printable characters */
    /* compiled from: FragmentTransition */
    static class C0264 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0222 f925;

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean f926;

        /* renamed from: ʽ  reason: contains not printable characters */
        public C0201 f927;

        /* renamed from: ʾ  reason: contains not printable characters */
        public C0222 f928;

        /* renamed from: ʿ  reason: contains not printable characters */
        public boolean f929;

        /* renamed from: ˆ  reason: contains not printable characters */
        public C0201 f930;

        C0264() {
        }
    }
}
