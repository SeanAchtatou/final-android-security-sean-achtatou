package android.support.v4.ʻ;

import android.arch.lifecycle.C0003;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.ʻ.C0190;
import android.support.v4.ˈ.C0353;
import android.support.v4.ˈ.C0354;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import net.lingala.zip4j.util.InternalZipConstants;

/* renamed from: android.support.v4.ʻ.ˊ  reason: contains not printable characters */
/* compiled from: FragmentActivity */
public class C0227 extends C0211 implements C0190.C0191, C0190.C0192 {
    static final String ALLOCATED_REQUEST_INDICIES_TAG = "android:support:request_indicies";
    static final String FRAGMENTS_TAG = "android:support:fragments";
    static final int MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS = 65534;
    static final int MSG_REALLY_STOPPED = 1;
    static final int MSG_RESUME_PENDING = 2;
    static final String NEXT_CANDIDATE_REQUEST_INDEX_TAG = "android:support:next_request_index";
    static final String REQUEST_FRAGMENT_WHO_TAG = "android:support:request_fragment_who";
    private static final String TAG = "FragmentActivity";
    boolean mCreated;
    final C0234 mFragments = C0234.m1342(new C0228());
    final Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            int i = message.what;
            if (i != 1) {
                if (i != 2) {
                    super.handleMessage(message);
                    return;
                }
                C0227.this.onResumeFragments();
                C0227.this.mFragments.m1372();
            } else if (C0227.this.mStopped) {
                C0227.this.doReallyStop(false);
            }
        }
    };
    int mNextCandidateRequestIndex;
    C0354<String> mPendingFragmentActivityResults;
    boolean mReallyStopped = true;
    boolean mRequestedPermissionsFromFragment;
    boolean mResumed;
    boolean mRetaining;
    boolean mStopped = true;

    public void onAttachFragment(C0222 r1) {
    }

    public Object onRetainCustomNonConfigurationInstance() {
        return null;
    }

    public /* bridge */ /* synthetic */ View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return super.onCreateView(view, str, context, attributeSet);
    }

    public /* bridge */ /* synthetic */ View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return super.onCreateView(str, context, attributeSet);
    }

    public /* bridge */ /* synthetic */ void startActivityForResult(Intent intent, int i, Bundle bundle) {
        super.startActivityForResult(intent, i, bundle);
    }

    public /* bridge */ /* synthetic */ void startIntentSenderForResult(IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4) {
        super.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4);
    }

    public /* bridge */ /* synthetic */ void startIntentSenderForResult(IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4, Bundle bundle) {
        super.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4, bundle);
    }

    /* renamed from: android.support.v4.ʻ.ˊ$ʼ  reason: contains not printable characters */
    /* compiled from: FragmentActivity */
    static final class C0229 {

        /* renamed from: ʻ  reason: contains not printable characters */
        Object f791;

        /* renamed from: ʼ  reason: contains not printable characters */
        C0254 f792;

        /* renamed from: ʽ  reason: contains not printable characters */
        C0353<String, C0266> f793;

        C0229() {
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        this.mFragments.m1359();
        int i3 = i >> 16;
        if (i3 != 0) {
            int i4 = i3 - 1;
            String r1 = this.mPendingFragmentActivityResults.m1984(i4);
            this.mPendingFragmentActivityResults.m1990(i4);
            if (r1 == null) {
                Log.w(TAG, "Activity result delivered for unknown Fragment.");
                return;
            }
            C0222 r2 = this.mFragments.m1343(r1);
            if (r2 == null) {
                Log.w(TAG, "Activity result no fragment exists for who: " + r1);
                return;
            }
            r2.m1200(i & InternalZipConstants.MAX_ALLOWED_ZIP_COMMENT_LENGTH, i2, intent);
            return;
        }
        super.onActivityResult(i, i2, intent);
    }

    public void onBackPressed() {
        C0239 r0 = this.mFragments.m1344();
        boolean r1 = r0.m1408();
        if (r1 && Build.VERSION.SDK_INT <= 25) {
            return;
        }
        if (r1 || !r0.m1406()) {
            super.onBackPressed();
        }
    }

    public void supportFinishAfterTransition() {
        C0190.m1123(this);
    }

    public void setEnterSharedElementCallback(C0230 r1) {
        C0190.m1121(this, r1);
    }

    public void setExitSharedElementCallback(C0230 r1) {
        C0190.m1124(this, r1);
    }

    public void supportPostponeEnterTransition() {
        C0190.m1125(this);
    }

    public void supportStartPostponedEnterTransition() {
        C0190.m1126(this);
    }

    public void onMultiWindowModeChanged(boolean z) {
        this.mFragments.m1351(z);
    }

    public void onPictureInPictureModeChanged(boolean z) {
        this.mFragments.m1357(z);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.mFragments.m1346(configuration);
    }

    public C0003 getLifecycle() {
        return super.getLifecycle();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        C0254 r1 = null;
        this.mFragments.m1348((C0222) null);
        super.onCreate(bundle);
        C0229 r0 = (C0229) getLastNonConfigurationInstance();
        if (r0 != null) {
            this.mFragments.m1349(r0.f793);
        }
        if (bundle != null) {
            Parcelable parcelable = bundle.getParcelable(FRAGMENTS_TAG);
            C0234 r4 = this.mFragments;
            if (r0 != null) {
                r1 = r0.f792;
            }
            r4.m1347(parcelable, r1);
            if (bundle.containsKey(NEXT_CANDIDATE_REQUEST_INDEX_TAG)) {
                this.mNextCandidateRequestIndex = bundle.getInt(NEXT_CANDIDATE_REQUEST_INDEX_TAG);
                int[] intArray = bundle.getIntArray(ALLOCATED_REQUEST_INDICIES_TAG);
                String[] stringArray = bundle.getStringArray(REQUEST_FRAGMENT_WHO_TAG);
                if (intArray == null || stringArray == null || intArray.length != stringArray.length) {
                    Log.w(TAG, "Invalid requestCode mapping in savedInstanceState.");
                } else {
                    this.mPendingFragmentActivityResults = new C0354<>(intArray.length);
                    for (int i = 0; i < intArray.length; i++) {
                        this.mPendingFragmentActivityResults.m1988(intArray[i], stringArray[i]);
                    }
                }
            }
        }
        if (this.mPendingFragmentActivityResults == null) {
            this.mPendingFragmentActivityResults = new C0354<>();
            this.mNextCandidateRequestIndex = 0;
        }
        this.mFragments.m1363();
    }

    public boolean onCreatePanelMenu(int i, Menu menu) {
        if (i == 0) {
            return super.onCreatePanelMenu(i, menu) | this.mFragments.m1353(menu, getMenuInflater());
        }
        return super.onCreatePanelMenu(i, menu);
    }

    /* access modifiers changed from: package-private */
    public final View dispatchFragmentsOnCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return this.mFragments.m1345(view, str, context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        doReallyStop(false);
        this.mFragments.m1370();
        this.mFragments.m1374();
    }

    public void onLowMemory() {
        super.onLowMemory();
        this.mFragments.m1371();
    }

    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        if (super.onMenuItemSelected(i, menuItem)) {
            return true;
        }
        if (i == 0) {
            return this.mFragments.m1354(menuItem);
        }
        if (i != 6) {
            return false;
        }
        return this.mFragments.m1358(menuItem);
    }

    public void onPanelClosed(int i, Menu menu) {
        if (i == 0) {
            this.mFragments.m1356(menu);
        }
        super.onPanelClosed(i, menu);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mResumed = false;
        if (this.mHandler.hasMessages(2)) {
            this.mHandler.removeMessages(2);
            onResumeFragments();
        }
        this.mFragments.m1367();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.mFragments.m1359();
    }

    public void onStateNotSaved() {
        this.mFragments.m1359();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mHandler.sendEmptyMessage(2);
        this.mResumed = true;
        this.mFragments.m1372();
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        this.mHandler.removeMessages(2);
        onResumeFragments();
        this.mFragments.m1372();
    }

    /* access modifiers changed from: protected */
    public void onResumeFragments() {
        this.mFragments.m1366();
    }

    public boolean onPreparePanel(int i, View view, Menu menu) {
        if (i != 0 || menu == null) {
            return super.onPreparePanel(i, view, menu);
        }
        return onPrepareOptionsPanel(view, menu) | this.mFragments.m1352(menu);
    }

    /* access modifiers changed from: protected */
    public boolean onPrepareOptionsPanel(View view, Menu menu) {
        return super.onPreparePanel(0, view, menu);
    }

    public final Object onRetainNonConfigurationInstance() {
        if (this.mStopped) {
            doReallyStop(true);
        }
        Object onRetainCustomNonConfigurationInstance = onRetainCustomNonConfigurationInstance();
        C0254 r1 = this.mFragments.m1362();
        C0353<String, C0266> r2 = this.mFragments.m1376();
        if (r1 == null && r2 == null && onRetainCustomNonConfigurationInstance == null) {
            return null;
        }
        C0229 r3 = new C0229();
        r3.f791 = onRetainCustomNonConfigurationInstance;
        r3.f792 = r1;
        r3.f793 = r2;
        return r3;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        markState(getSupportFragmentManager(), C0003.C0005.CREATED);
        Parcelable r0 = this.mFragments.m1361();
        if (r0 != null) {
            bundle.putParcelable(FRAGMENTS_TAG, r0);
        }
        if (this.mPendingFragmentActivityResults.m1986() > 0) {
            bundle.putInt(NEXT_CANDIDATE_REQUEST_INDEX_TAG, this.mNextCandidateRequestIndex);
            int[] iArr = new int[this.mPendingFragmentActivityResults.m1986()];
            String[] strArr = new String[this.mPendingFragmentActivityResults.m1986()];
            for (int i = 0; i < this.mPendingFragmentActivityResults.m1986(); i++) {
                iArr[i] = this.mPendingFragmentActivityResults.m1992(i);
                strArr[i] = this.mPendingFragmentActivityResults.m1993(i);
            }
            bundle.putIntArray(ALLOCATED_REQUEST_INDICIES_TAG, iArr);
            bundle.putStringArray(REQUEST_FRAGMENT_WHO_TAG, strArr);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.mStopped = false;
        this.mReallyStopped = false;
        this.mHandler.removeMessages(1);
        if (!this.mCreated) {
            this.mCreated = true;
            this.mFragments.m1364();
        }
        this.mFragments.m1359();
        this.mFragments.m1372();
        this.mFragments.m1373();
        this.mFragments.m1365();
        this.mFragments.m1375();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.mStopped = true;
        markState(getSupportFragmentManager(), C0003.C0005.CREATED);
        this.mHandler.sendEmptyMessage(1);
        this.mFragments.m1368();
    }

    public Object getLastCustomNonConfigurationInstance() {
        C0229 r0 = (C0229) getLastNonConfigurationInstance();
        if (r0 != null) {
            return r0.f791;
        }
        return null;
    }

    @Deprecated
    public void supportInvalidateOptionsMenu() {
        invalidateOptionsMenu();
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.print("Local FragmentActivity ");
        printWriter.print(Integer.toHexString(System.identityHashCode(this)));
        printWriter.println(" State:");
        String str2 = str + "  ";
        printWriter.print(str2);
        printWriter.print("mCreated=");
        printWriter.print(this.mCreated);
        printWriter.print("mResumed=");
        printWriter.print(this.mResumed);
        printWriter.print(" mStopped=");
        printWriter.print(this.mStopped);
        printWriter.print(" mReallyStopped=");
        printWriter.println(this.mReallyStopped);
        this.mFragments.m1350(str2, fileDescriptor, printWriter, strArr);
        this.mFragments.m1344().m1404(str, fileDescriptor, printWriter, strArr);
    }

    /* access modifiers changed from: package-private */
    public void doReallyStop(boolean z) {
        if (!this.mReallyStopped) {
            this.mReallyStopped = true;
            this.mRetaining = z;
            this.mHandler.removeMessages(1);
            onReallyStop();
        } else if (z) {
            this.mFragments.m1373();
            this.mFragments.m1360(true);
        }
    }

    /* access modifiers changed from: package-private */
    public void onReallyStop() {
        this.mFragments.m1360(this.mRetaining);
        this.mFragments.m1369();
    }

    public C0239 getSupportFragmentManager() {
        return this.mFragments.m1344();
    }

    public C0266 getSupportLoaderManager() {
        return this.mFragments.m1355();
    }

    public void startActivityForResult(Intent intent, int i) {
        if (!this.mStartedActivityFromFragment && i != -1) {
            checkForValidRequestCode(i);
        }
        super.startActivityForResult(intent, i);
    }

    public final void validateRequestPermissionsRequestCode(int i) {
        if (!this.mRequestedPermissionsFromFragment && i != -1) {
            checkForValidRequestCode(i);
        }
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        int i2 = (i >> 16) & InternalZipConstants.MAX_ALLOWED_ZIP_COMMENT_LENGTH;
        if (i2 != 0) {
            int i3 = i2 - 1;
            String r2 = this.mPendingFragmentActivityResults.m1984(i3);
            this.mPendingFragmentActivityResults.m1990(i3);
            if (r2 == null) {
                Log.w(TAG, "Activity result delivered for unknown Fragment.");
                return;
            }
            C0222 r3 = this.mFragments.m1343(r2);
            if (r3 == null) {
                Log.w(TAG, "Activity result no fragment exists for who: " + r2);
                return;
            }
            r3.m1202(i & InternalZipConstants.MAX_ALLOWED_ZIP_COMMENT_LENGTH, strArr, iArr);
        }
    }

    public void startActivityFromFragment(C0222 r2, Intent intent, int i) {
        startActivityFromFragment(r2, intent, i, null);
    }

    public void startActivityFromFragment(C0222 r4, Intent intent, int i, Bundle bundle) {
        this.mStartedActivityFromFragment = true;
        if (i == -1) {
            try {
                C0190.m1119(this, intent, -1, bundle);
            } finally {
                this.mStartedActivityFromFragment = false;
            }
        } else {
            checkForValidRequestCode(i);
            C0190.m1119(this, intent, ((allocateRequestIndex(r4) + 1) << 16) + (i & InternalZipConstants.MAX_ALLOWED_ZIP_COMMENT_LENGTH), bundle);
            this.mStartedActivityFromFragment = false;
        }
    }

    public void startIntentSenderFromFragment(C0222 r12, IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4, Bundle bundle) {
        int i5 = i;
        this.mStartedIntentSenderFromFragment = true;
        if (i5 == -1) {
            try {
                C0190.m1120(this, intentSender, i, intent, i2, i3, i4, bundle);
            } finally {
                this.mStartedIntentSenderFromFragment = false;
            }
        } else {
            checkForValidRequestCode(i);
            C0190.m1120(this, intentSender, ((allocateRequestIndex(r12) + 1) << 16) + (i5 & InternalZipConstants.MAX_ALLOWED_ZIP_COMMENT_LENGTH), intent, i2, i3, i4, bundle);
            this.mStartedIntentSenderFromFragment = false;
        }
    }

    private int allocateRequestIndex(C0222 r4) {
        if (this.mPendingFragmentActivityResults.m1986() < MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS) {
            while (this.mPendingFragmentActivityResults.m1994(this.mNextCandidateRequestIndex) >= 0) {
                this.mNextCandidateRequestIndex = (this.mNextCandidateRequestIndex + 1) % MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS;
            }
            int i = this.mNextCandidateRequestIndex;
            this.mPendingFragmentActivityResults.m1988(i, r4.f731);
            this.mNextCandidateRequestIndex = (this.mNextCandidateRequestIndex + 1) % MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS;
            return i;
        }
        throw new IllegalStateException("Too many pending Fragment activity results.");
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public void requestPermissionsFromFragment(C0222 r3, String[] strArr, int i) {
        if (i == -1) {
            C0190.m1122(this, strArr, i);
            return;
        }
        checkForValidRequestCode(i);
        try {
            this.mRequestedPermissionsFromFragment = true;
            C0190.m1122(this, strArr, ((allocateRequestIndex(r3) + 1) << 16) + (i & InternalZipConstants.MAX_ALLOWED_ZIP_COMMENT_LENGTH));
            this.mRequestedPermissionsFromFragment = false;
        } catch (Throwable th) {
            this.mRequestedPermissionsFromFragment = false;
            throw th;
        }
    }

    /* renamed from: android.support.v4.ʻ.ˊ$ʻ  reason: contains not printable characters */
    /* compiled from: FragmentActivity */
    class C0228 extends C0237<C0227> {
        public C0228() {
            super(C0227.this);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1320(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            C0227.this.dump(str, fileDescriptor, printWriter, strArr);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m1322(C0222 r1) {
            return !C0227.this.isFinishing();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public LayoutInflater m1323() {
            return C0227.this.getLayoutInflater().cloneInContext(C0227.this);
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m1325() {
            C0227.this.supportInvalidateOptionsMenu();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1318(C0222 r2, Intent intent, int i, Bundle bundle) {
            C0227.this.startActivityFromFragment(r2, intent, i, bundle);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1319(C0222 r2, String[] strArr, int i) {
            C0227.this.requestPermissionsFromFragment(r2, strArr, i);
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public boolean m1326() {
            return C0227.this.getWindow() != null;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public int m1327() {
            Window window = C0227.this.getWindow();
            if (window == null) {
                return 0;
            }
            return window.getAttributes().windowAnimations;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m1324(C0222 r2) {
            C0227.this.onAttachFragment(r2);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public View m1317(int i) {
            return C0227.this.findViewById(i);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m1321() {
            Window window = C0227.this.getWindow();
            return (window == null || window.peekDecorView() == null) ? false : true;
        }
    }

    private static void markState(C0239 r2, C0003.C0005 r3) {
        for (C0222 next : r2.m1407()) {
            if (next != null) {
                next.f763.m19(r3);
                markState(next.m1253(), r3);
            }
        }
    }
}
