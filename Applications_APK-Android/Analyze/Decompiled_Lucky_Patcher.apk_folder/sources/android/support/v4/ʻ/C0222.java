package android.support.v4.ʻ;

import android.animation.Animator;
import android.app.Activity;
import android.arch.lifecycle.C0003;
import android.arch.lifecycle.C0006;
import android.arch.lifecycle.C0007;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcelable;
import android.support.v4.ˈ.C0334;
import android.support.v4.ˈ.C0353;
import android.support.v4.ˉ.C0397;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;

/* renamed from: android.support.v4.ʻ.ˉ  reason: contains not printable characters */
/* compiled from: Fragment */
public class C0222 implements C0006, ComponentCallbacks, View.OnCreateContextMenuListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final Object f720 = new Object();

    /* renamed from: ﹳﹳ  reason: contains not printable characters */
    private static final C0353<String, Class<?>> f721 = new C0353<>();

    /* renamed from: ʻʻ  reason: contains not printable characters */
    boolean f722;

    /* renamed from: ʼ  reason: contains not printable characters */
    int f723 = 0;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    boolean f724 = true;

    /* renamed from: ʽ  reason: contains not printable characters */
    Bundle f725;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    boolean f726;

    /* renamed from: ʾ  reason: contains not printable characters */
    SparseArray<Parcelable> f727;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    ViewGroup f728;

    /* renamed from: ʿ  reason: contains not printable characters */
    int f729 = -1;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    boolean f730;

    /* renamed from: ˆ  reason: contains not printable characters */
    String f731;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    View f732;

    /* renamed from: ˈ  reason: contains not printable characters */
    Bundle f733;

    /* renamed from: ˈˈ  reason: contains not printable characters */
    boolean f734 = true;

    /* renamed from: ˉ  reason: contains not printable characters */
    C0222 f735;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    boolean f736;

    /* renamed from: ˊ  reason: contains not printable characters */
    int f737 = -1;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    boolean f738;

    /* renamed from: ˋ  reason: contains not printable characters */
    int f739;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    C0268 f740;

    /* renamed from: ˎ  reason: contains not printable characters */
    boolean f741;

    /* renamed from: ˎˎ  reason: contains not printable characters */
    C0223 f742;

    /* renamed from: ˏ  reason: contains not printable characters */
    boolean f743;

    /* renamed from: ˏˏ  reason: contains not printable characters */
    boolean f744;

    /* renamed from: ˑ  reason: contains not printable characters */
    boolean f745;

    /* renamed from: ˑˑ  reason: contains not printable characters */
    boolean f746;

    /* renamed from: י  reason: contains not printable characters */
    boolean f747;

    /* renamed from: יי  reason: contains not printable characters */
    float f748;

    /* renamed from: ـ  reason: contains not printable characters */
    boolean f749;

    /* renamed from: ــ  reason: contains not printable characters */
    View f750;

    /* renamed from: ٴ  reason: contains not printable characters */
    boolean f751;

    /* renamed from: ᐧ  reason: contains not printable characters */
    int f752;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    boolean f753;

    /* renamed from: ᴵ  reason: contains not printable characters */
    C0246 f754;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    boolean f755;

    /* renamed from: ᵎ  reason: contains not printable characters */
    C0237 f756;

    /* renamed from: ᵎᵎ  reason: contains not printable characters */
    LayoutInflater f757;

    /* renamed from: ᵔ  reason: contains not printable characters */
    C0246 f758;

    /* renamed from: ᵔᵔ  reason: contains not printable characters */
    boolean f759;

    /* renamed from: ᵢ  reason: contains not printable characters */
    C0254 f760;

    /* renamed from: ᵢᵢ  reason: contains not printable characters */
    boolean f761;

    /* renamed from: ⁱ  reason: contains not printable characters */
    C0222 f762;

    /* renamed from: ⁱⁱ  reason: contains not printable characters */
    C0007 f763 = new C0007(this);

    /* renamed from: ﹳ  reason: contains not printable characters */
    int f764;

    /* renamed from: ﹶ  reason: contains not printable characters */
    int f765;

    /* renamed from: ﾞ  reason: contains not printable characters */
    String f766;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    boolean f767;

    /* renamed from: android.support.v4.ʻ.ˉ$ʽ  reason: contains not printable characters */
    /* compiled from: Fragment */
    interface C0225 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m1314();

        /* renamed from: ʼ  reason: contains not printable characters */
        void m1315();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public View m1196(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Animation m1197(int i, boolean z, int i2) {
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1200(int i, int i2, Intent intent) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1202(int i, String[] strArr, int[] iArr) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1215(C0222 r1) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1216(Menu menu) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1217(Menu menu, MenuInflater menuInflater) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1219(View view, Bundle bundle) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1221(boolean z) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1224(MenuItem menuItem) {
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Animator m1226(int i, boolean z, int i2) {
        return null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1230(Menu menu) {
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m1234(MenuItem menuItem) {
        return false;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m1246(boolean z) {
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m1251(boolean z) {
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public void m1269(Bundle bundle) {
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public void m1290() {
    }

    public C0003 getLifecycle() {
        return this.f763;
    }

    /* renamed from: android.support.v4.ʻ.ˉ$ʼ  reason: contains not printable characters */
    /* compiled from: Fragment */
    public static class C0224 extends RuntimeException {
        public C0224(String str, Exception exc) {
            super(str, exc);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0222 m1190(Context context, String str, Bundle bundle) {
        try {
            Class<?> cls = f721.get(str);
            if (cls == null) {
                cls = context.getClassLoader().loadClass(str);
                f721.put(str, cls);
            }
            C0222 r5 = (C0222) cls.getConstructor(new Class[0]).newInstance(new Object[0]);
            if (bundle != null) {
                bundle.setClassLoader(r5.getClass().getClassLoader());
                r5.m1229(bundle);
            }
            return r5;
        } catch (ClassNotFoundException e) {
            throw new C0224("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an" + " empty constructor that is public", e);
        } catch (InstantiationException e2) {
            throw new C0224("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an" + " empty constructor that is public", e2);
        } catch (IllegalAccessException e3) {
            throw new C0224("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an" + " empty constructor that is public", e3);
        } catch (NoSuchMethodException e4) {
            throw new C0224("Unable to instantiate fragment " + str + ": could not find Fragment constructor", e4);
        } catch (InvocationTargetException e5) {
            throw new C0224("Unable to instantiate fragment " + str + ": calling Fragment constructor caused an exception", e5);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static boolean m1191(Context context, String str) {
        try {
            Class<?> cls = f721.get(str);
            if (cls == null) {
                cls = context.getClassLoader().loadClass(str);
                f721.put(str, cls);
            }
            return C0222.class.isAssignableFrom(cls);
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m1213(Bundle bundle) {
        SparseArray<Parcelable> sparseArray = this.f727;
        if (sparseArray != null) {
            this.f732.restoreHierarchyState(sparseArray);
            this.f727 = null;
        }
        this.f730 = false;
        m1265(bundle);
        if (!this.f730) {
            throw new C0238("Fragment " + this + " did not call through to super.onViewStateRestored()");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m1201(int i, C0222 r2) {
        this.f729 = i;
        if (r2 != null) {
            this.f731 = r2.f731 + ":" + this.f729;
            return;
        }
        this.f731 = "android:fragment:" + this.f729;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean m1223() {
        return this.f752 > 0;
    }

    public final boolean equals(Object obj) {
        return super.equals(obj);
    }

    public final int hashCode() {
        return super.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        C0334.m1918(this, sb);
        if (this.f729 >= 0) {
            sb.append(" #");
            sb.append(this.f729);
        }
        if (this.f764 != 0) {
            sb.append(" id=0x");
            sb.append(Integer.toHexString(this.f764));
        }
        if (this.f766 != null) {
            sb.append(" ");
            sb.append(this.f766);
        }
        sb.append('}');
        return sb.toString();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1229(Bundle bundle) {
        if (this.f729 < 0 || !m1232()) {
            this.f733 = bundle;
            return;
        }
        throw new IllegalStateException("Fragment already active and state has been saved");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final boolean m1232() {
        C0246 r0 = this.f754;
        if (r0 == null) {
            return false;
        }
        return r0.m1510();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final C0227 m1236() {
        C0237 r0 = this.f756;
        if (r0 == null) {
            return null;
        }
        return (C0227) r0.m1393();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final Resources m1243() {
        C0237 r0 = this.f756;
        if (r0 != null) {
            return r0.m1394().getResources();
        }
        throw new IllegalStateException("Fragment " + this + " not attached to Activity");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m1198(int i) {
        return m1243().getString(i);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final C0239 m1249() {
        return this.f754;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final C0239 m1253() {
        if (this.f758 == null) {
            m1225();
            int i = this.f723;
            if (i >= 5) {
                this.f758.m1535();
            } else if (i >= 4) {
                this.f758.m1534();
            } else if (i >= 2) {
                this.f758.m1532();
            } else if (i >= 1) {
                this.f758.m1531();
            }
        }
        return this.f758;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public C0239 m1257() {
        return this.f758;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final boolean m1263() {
        return this.f756 != null && this.f741;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m1266() {
        return this.f767;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1231(boolean z) {
        this.f755 = z;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1239(boolean z) {
        if (this.f726 != z) {
            this.f726 = z;
            if (m1263() && !m1266()) {
                this.f756.m1390();
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1208(Intent intent) {
        m1211(intent, (Bundle) null);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1211(Intent intent, Bundle bundle) {
        C0237 r0 = this.f756;
        if (r0 != null) {
            r0.m1379(this, intent, -1, bundle);
            return;
        }
        throw new IllegalStateException("Fragment " + this + " not attached to Activity");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1209(Intent intent, int i) {
        m1210(intent, i, (Bundle) null);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1210(Intent intent, int i, Bundle bundle) {
        C0237 r0 = this.f756;
        if (r0 != null) {
            r0.m1379(this, intent, i, bundle);
            return;
        }
        throw new IllegalStateException("Fragment " + this + " not attached to Activity");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m1222(String[] strArr, int i) {
        C0237 r0 = this.f756;
        if (r0 != null) {
            r0.m1380(this, strArr, i);
            return;
        }
        throw new IllegalStateException("Fragment " + this + " not attached to Activity");
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public LayoutInflater m1237(Bundle bundle) {
        return m1250(bundle);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public LayoutInflater m1244(Bundle bundle) {
        this.f757 = m1237(bundle);
        return this.f757;
    }

    @Deprecated
    /* renamed from: ʿ  reason: contains not printable characters */
    public LayoutInflater m1250(Bundle bundle) {
        C0237 r2 = this.f756;
        if (r2 != null) {
            LayoutInflater r22 = r2.m1387();
            m1253();
            C0397.m2158(r22, this.f758.m1543());
            return r22;
        }
        throw new IllegalStateException("onGetLayoutInflater() cannot be executed until the Fragment is attached to the FragmentManager.");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1207(Context context, AttributeSet attributeSet, Bundle bundle) {
        this.f730 = true;
        C0237 r2 = this.f756;
        Activity r22 = r2 == null ? null : r2.m1393();
        if (r22 != null) {
            this.f730 = false;
            m1205(r22, attributeSet, bundle);
        }
    }

    @Deprecated
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1205(Activity activity, AttributeSet attributeSet, Bundle bundle) {
        this.f730 = true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1206(Context context) {
        this.f730 = true;
        C0237 r2 = this.f756;
        Activity r22 = r2 == null ? null : r2.m1393();
        if (r22 != null) {
            this.f730 = false;
            m1204(r22);
        }
    }

    @Deprecated
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1204(Activity activity) {
        this.f730 = true;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m1254(Bundle bundle) {
        this.f730 = true;
        m1258(bundle);
        C0246 r2 = this.f758;
        if (r2 != null && !r2.m1483(1)) {
            this.f758.m1531();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m1258(Bundle bundle) {
        Parcelable parcelable;
        if (bundle != null && (parcelable = bundle.getParcelable("android:support:fragments")) != null) {
            if (this.f758 == null) {
                m1225();
            }
            this.f758.m1472(parcelable, this.f760);
            this.f760 = null;
            this.f758.m1531();
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public View m1268() {
        return this.f750;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public void m1261(Bundle bundle) {
        this.f730 = true;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public void m1265(Bundle bundle) {
        this.f730 = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.ˏ.ʻ(java.lang.String, boolean, boolean):android.support.v4.ʻ.ⁱ
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.ʻ.ˏ.ʻ(android.support.v4.ʻ.ˉ, java.lang.String[], int):void
      android.support.v4.ʻ.ˋ.ʻ(android.content.Context, java.lang.String, android.os.Bundle):android.support.v4.ʻ.ˉ
      android.support.v4.ʻ.ˏ.ʻ(java.lang.String, boolean, boolean):android.support.v4.ʻ.ⁱ */
    /* renamed from: ˎ  reason: contains not printable characters */
    public void m1271() {
        this.f730 = true;
        if (!this.f738) {
            this.f738 = true;
            if (!this.f744) {
                this.f744 = true;
                this.f740 = this.f756.m1377(this.f731, this.f738, false);
                return;
            }
            C0268 r0 = this.f740;
            if (r0 != null) {
                r0.m1638();
            }
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public void m1274() {
        this.f730 = true;
    }

    public void onConfigurationChanged(Configuration configuration) {
        this.f730 = true;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m1277() {
        this.f730 = true;
    }

    /* renamed from: י  reason: contains not printable characters */
    public void m1280() {
        this.f730 = true;
    }

    public void onLowMemory() {
        this.f730 = true;
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public void m1282() {
        this.f730 = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.ˏ.ʻ(java.lang.String, boolean, boolean):android.support.v4.ʻ.ⁱ
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.ʻ.ˏ.ʻ(android.support.v4.ʻ.ˉ, java.lang.String[], int):void
      android.support.v4.ʻ.ˋ.ʻ(android.content.Context, java.lang.String, android.os.Bundle):android.support.v4.ʻ.ˉ
      android.support.v4.ʻ.ˏ.ʻ(java.lang.String, boolean, boolean):android.support.v4.ʻ.ⁱ */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m1284() {
        this.f730 = true;
        if (!this.f744) {
            this.f744 = true;
            this.f740 = this.f756.m1377(this.f731, this.f738, false);
        }
        C0268 r0 = this.f740;
        if (r0 != null) {
            r0.m1644();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m1286() {
        this.f729 = -1;
        this.f731 = null;
        this.f741 = false;
        this.f743 = false;
        this.f745 = false;
        this.f747 = false;
        this.f749 = false;
        this.f752 = 0;
        this.f754 = null;
        this.f758 = null;
        this.f756 = null;
        this.f764 = 0;
        this.f765 = 0;
        this.f766 = null;
        this.f767 = false;
        this.f753 = false;
        this.f722 = false;
        this.f740 = null;
        this.f738 = false;
        this.f744 = false;
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public void m1288() {
        this.f730 = true;
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        m1236().onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    public Object m1292() {
        C0223 r0 = this.f742;
        if (r0 == null) {
            return null;
        }
        return r0.f781;
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    public Object m1294() {
        C0223 r0 = this.f742;
        if (r0 == null) {
            return null;
        }
        if (r0.f782 == f720) {
            return m1292();
        }
        return this.f742.f782;
    }

    /* renamed from: ⁱ  reason: contains not printable characters */
    public Object m1296() {
        C0223 r0 = this.f742;
        if (r0 == null) {
            return null;
        }
        return r0.f783;
    }

    /* renamed from: ﹳ  reason: contains not printable characters */
    public Object m1298() {
        C0223 r0 = this.f742;
        if (r0 == null) {
            return null;
        }
        if (r0.f784 == f720) {
            return m1296();
        }
        return this.f742.f784;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public Object m1300() {
        C0223 r0 = this.f742;
        if (r0 == null) {
            return null;
        }
        return r0.f785;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public Object m1301() {
        C0223 r0 = this.f742;
        if (r0 == null) {
            return null;
        }
        if (r0.f786 == f720) {
            return m1300();
        }
        return this.f742.f786;
    }

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public boolean m1302() {
        C0223 r0 = this.f742;
        if (r0 == null || r0.f788 == null) {
            return true;
        }
        return this.f742.f788.booleanValue();
    }

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public boolean m1287() {
        C0223 r0 = this.f742;
        if (r0 == null || r0.f787 == null) {
            return true;
        }
        return this.f742.f787.booleanValue();
    }

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public void m1289() {
        C0246 r0 = this.f754;
        if (r0 == null || r0.f830 == null) {
            m1192().f778 = false;
        } else if (Looper.myLooper() != this.f754.f830.m1395().getLooper()) {
            this.f754.f830.m1395().postAtFrontOfQueue(new Runnable() {
                public void run() {
                    C0222.this.m1194();
                }
            });
        } else {
            m1194();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ﹶﹶ  reason: contains not printable characters */
    public void m1194() {
        C0225 r0;
        C0223 r02 = this.f742;
        if (r02 == null) {
            r0 = null;
        } else {
            r02.f778 = false;
            r0 = r02.f779;
            this.f742.f779 = null;
        }
        if (r0 != null) {
            r0.m1314();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1220(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mFragmentId=#");
        printWriter.print(Integer.toHexString(this.f764));
        printWriter.print(" mContainerId=#");
        printWriter.print(Integer.toHexString(this.f765));
        printWriter.print(" mTag=");
        printWriter.println(this.f766);
        printWriter.print(str);
        printWriter.print("mState=");
        printWriter.print(this.f723);
        printWriter.print(" mIndex=");
        printWriter.print(this.f729);
        printWriter.print(" mWho=");
        printWriter.print(this.f731);
        printWriter.print(" mBackStackNesting=");
        printWriter.println(this.f752);
        printWriter.print(str);
        printWriter.print("mAdded=");
        printWriter.print(this.f741);
        printWriter.print(" mRemoving=");
        printWriter.print(this.f743);
        printWriter.print(" mFromLayout=");
        printWriter.print(this.f745);
        printWriter.print(" mInLayout=");
        printWriter.println(this.f747);
        printWriter.print(str);
        printWriter.print("mHidden=");
        printWriter.print(this.f767);
        printWriter.print(" mDetached=");
        printWriter.print(this.f753);
        printWriter.print(" mMenuVisible=");
        printWriter.print(this.f724);
        printWriter.print(" mHasMenu=");
        printWriter.println(this.f726);
        printWriter.print(str);
        printWriter.print("mRetainInstance=");
        printWriter.print(this.f755);
        printWriter.print(" mRetaining=");
        printWriter.print(this.f722);
        printWriter.print(" mUserVisibleHint=");
        printWriter.println(this.f734);
        if (this.f754 != null) {
            printWriter.print(str);
            printWriter.print("mFragmentManager=");
            printWriter.println(this.f754);
        }
        if (this.f756 != null) {
            printWriter.print(str);
            printWriter.print("mHost=");
            printWriter.println(this.f756);
        }
        if (this.f762 != null) {
            printWriter.print(str);
            printWriter.print("mParentFragment=");
            printWriter.println(this.f762);
        }
        if (this.f733 != null) {
            printWriter.print(str);
            printWriter.print("mArguments=");
            printWriter.println(this.f733);
        }
        if (this.f725 != null) {
            printWriter.print(str);
            printWriter.print("mSavedFragmentState=");
            printWriter.println(this.f725);
        }
        if (this.f727 != null) {
            printWriter.print(str);
            printWriter.print("mSavedViewState=");
            printWriter.println(this.f727);
        }
        if (this.f735 != null) {
            printWriter.print(str);
            printWriter.print("mTarget=");
            printWriter.print(this.f735);
            printWriter.print(" mTargetRequestCode=");
            printWriter.println(this.f739);
        }
        if (m1276() != 0) {
            printWriter.print(str);
            printWriter.print("mNextAnim=");
            printWriter.println(m1276());
        }
        if (this.f728 != null) {
            printWriter.print(str);
            printWriter.print("mContainer=");
            printWriter.println(this.f728);
        }
        if (this.f750 != null) {
            printWriter.print(str);
            printWriter.print("mView=");
            printWriter.println(this.f750);
        }
        if (this.f732 != null) {
            printWriter.print(str);
            printWriter.print("mInnerView=");
            printWriter.println(this.f750);
        }
        if (m1291() != null) {
            printWriter.print(str);
            printWriter.print("mAnimatingAway=");
            printWriter.println(m1291());
            printWriter.print(str);
            printWriter.print("mStateAfterAnimating=");
            printWriter.println(m1297());
        }
        if (this.f740 != null) {
            printWriter.print(str);
            printWriter.println("Loader Manager:");
            C0268 r0 = this.f740;
            r0.m1636(str + "  ", fileDescriptor, printWriter, strArr);
        }
        if (this.f758 != null) {
            printWriter.print(str);
            printWriter.println("Child " + this.f758 + ":");
            C0246 r02 = this.f758;
            r02.m1481(str + "  ", fileDescriptor, printWriter, strArr);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0222 m1195(String str) {
        if (str.equals(this.f731)) {
            return this;
        }
        C0246 r0 = this.f758;
        if (r0 != null) {
            return r0.m1489(str);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻʻ  reason: contains not printable characters */
    public void m1225() {
        if (this.f756 != null) {
            this.f758 = new C0246();
            this.f758.m1479(this.f756, new C0232() {
                /* renamed from: ʻ  reason: contains not printable characters */
                public View m1304(int i) {
                    if (C0222.this.f750 != null) {
                        return C0222.this.f750.findViewById(i);
                    }
                    throw new IllegalStateException("Fragment does not have a view");
                }

                /* renamed from: ʻ  reason: contains not printable characters */
                public boolean m1305() {
                    return C0222.this.f750 != null;
                }

                /* renamed from: ʻ  reason: contains not printable characters */
                public C0222 m1303(Context context, String str, Bundle bundle) {
                    return C0222.this.f756.m1337(context, str, bundle);
                }
            }, this);
            return;
        }
        throw new IllegalStateException("Fragment has not been attached yet.");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public void m1272(Bundle bundle) {
        C0246 r0 = this.f758;
        if (r0 != null) {
            r0.m1528();
        }
        this.f723 = 1;
        this.f730 = false;
        m1254(bundle);
        this.f761 = true;
        if (this.f730) {
            this.f763.m18(C0003.C0004.ON_CREATE);
            return;
        }
        throw new C0238("Fragment " + this + " did not call through to super.onCreate()");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public View m1227(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        C0246 r0 = this.f758;
        if (r0 != null) {
            r0.m1528();
        }
        this.f751 = true;
        return m1196(layoutInflater, viewGroup, bundle);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m1275(Bundle bundle) {
        C0246 r0 = this.f758;
        if (r0 != null) {
            r0.m1528();
        }
        this.f723 = 2;
        this.f730 = false;
        m1261(bundle);
        if (this.f730) {
            C0246 r3 = this.f758;
            if (r3 != null) {
                r3.m1532();
                return;
            }
            return;
        }
        throw new C0238("Fragment " + this + " did not call through to super.onActivityCreated()");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽʽ  reason: contains not printable characters */
    public void m1242() {
        C0246 r0 = this.f758;
        if (r0 != null) {
            r0.m1528();
            this.f758.m1516();
        }
        this.f723 = 4;
        this.f730 = false;
        m1271();
        if (this.f730) {
            C0246 r02 = this.f758;
            if (r02 != null) {
                r02.m1534();
            }
            C0268 r03 = this.f740;
            if (r03 != null) {
                r03.m1643();
            }
            this.f763.m18(C0003.C0004.ON_START);
            return;
        }
        throw new C0238("Fragment " + this + " did not call through to super.onStart()");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼʼ  reason: contains not printable characters */
    public void m1235() {
        C0246 r0 = this.f758;
        if (r0 != null) {
            r0.m1528();
            this.f758.m1516();
        }
        this.f723 = 5;
        this.f730 = false;
        m1274();
        if (this.f730) {
            C0246 r02 = this.f758;
            if (r02 != null) {
                r02.m1535();
                this.f758.m1516();
            }
            this.f763.m18(C0003.C0004.ON_RESUME);
            return;
        }
        throw new C0238("Fragment " + this + " did not call through to super.onResume()");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿʿ  reason: contains not printable characters */
    public void m1252() {
        C0246 r0 = this.f758;
        if (r0 != null) {
            r0.m1528();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m1255(boolean z) {
        m1246(z);
        C0246 r0 = this.f758;
        if (r0 != null) {
            r0.m1482(z);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m1259(boolean z) {
        m1251(z);
        C0246 r0 = this.f758;
        if (r0 != null) {
            r0.m1496(z);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1212(Configuration configuration) {
        onConfigurationChanged(configuration);
        C0246 r0 = this.f758;
        if (r0 != null) {
            r0.m1470(configuration);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾʾ  reason: contains not printable characters */
    public void m1248() {
        onLowMemory();
        C0246 r0 = this.f758;
        if (r0 != null) {
            r0.m1541();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m1233(Menu menu, MenuInflater menuInflater) {
        boolean z = false;
        if (this.f767) {
            return false;
        }
        if (this.f726 && this.f724) {
            m1217(menu, menuInflater);
            z = true;
        }
        C0246 r0 = this.f758;
        return r0 != null ? z | r0.m1485(menu, menuInflater) : z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m1240(Menu menu) {
        boolean z = false;
        if (this.f767) {
            return false;
        }
        if (this.f726 && this.f724) {
            m1216(menu);
            z = true;
        }
        C0246 r0 = this.f758;
        return r0 != null ? z | r0.m1484(menu) : z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m1241(MenuItem menuItem) {
        if (this.f767) {
            return false;
        }
        if (this.f726 && this.f724 && m1224(menuItem)) {
            return true;
        }
        C0246 r0 = this.f758;
        if (r0 == null || !r0.m1486(menuItem)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m1247(MenuItem menuItem) {
        if (this.f767) {
            return false;
        }
        if (m1234(menuItem)) {
            return true;
        }
        C0246 r0 = this.f758;
        if (r0 == null || !r0.m1498(menuItem)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m1245(Menu menu) {
        if (!this.f767) {
            if (this.f726 && this.f724) {
                m1230(menu);
            }
            C0246 r0 = this.f758;
            if (r0 != null) {
                r0.m1495(menu);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m1278(Bundle bundle) {
        Parcelable r0;
        m1269(bundle);
        C0246 r02 = this.f758;
        if (r02 != null && (r0 = r02.m1526()) != null) {
            bundle.putParcelable("android:support:fragments", r0);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ــ  reason: contains not printable characters */
    public void m1283() {
        this.f763.m18(C0003.C0004.ON_PAUSE);
        C0246 r0 = this.f758;
        if (r0 != null) {
            r0.m1536();
        }
        this.f723 = 4;
        this.f730 = false;
        m1277();
        if (!this.f730) {
            throw new C0238("Fragment " + this + " did not call through to super.onPause()");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆˆ  reason: contains not printable characters */
    public void m1256() {
        this.f763.m18(C0003.C0004.ON_STOP);
        C0246 r0 = this.f758;
        if (r0 != null) {
            r0.m1537();
        }
        this.f723 = 3;
        this.f730 = false;
        m1280();
        if (!this.f730) {
            throw new C0238("Fragment " + this + " did not call through to super.onStop()");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.ˏ.ʻ(java.lang.String, boolean, boolean):android.support.v4.ʻ.ⁱ
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.ʻ.ˏ.ʻ(android.support.v4.ʻ.ˉ, java.lang.String[], int):void
      android.support.v4.ʻ.ˋ.ʻ(android.content.Context, java.lang.String, android.os.Bundle):android.support.v4.ʻ.ˉ
      android.support.v4.ʻ.ˏ.ʻ(java.lang.String, boolean, boolean):android.support.v4.ʻ.ⁱ */
    /* access modifiers changed from: package-private */
    /* renamed from: ˉˉ  reason: contains not printable characters */
    public void m1264() {
        C0246 r0 = this.f758;
        if (r0 != null) {
            r0.m1538();
        }
        this.f723 = 2;
        if (this.f738) {
            this.f738 = false;
            if (!this.f744) {
                this.f744 = true;
                this.f740 = this.f756.m1377(this.f731, this.f738, false);
            }
            if (this.f740 == null) {
                return;
            }
            if (this.f756.m1398()) {
                this.f740.m1640();
            } else {
                this.f740.m1639();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈˈ  reason: contains not printable characters */
    public void m1260() {
        C0246 r0 = this.f758;
        if (r0 != null) {
            r0.m1539();
        }
        this.f723 = 1;
        this.f730 = false;
        m1282();
        if (this.f730) {
            C0268 r1 = this.f740;
            if (r1 != null) {
                r1.m1642();
            }
            this.f751 = false;
            return;
        }
        throw new C0238("Fragment " + this + " did not call through to super.onDestroyView()");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋˋ  reason: contains not printable characters */
    public void m1270() {
        this.f763.m18(C0003.C0004.ON_DESTROY);
        C0246 r0 = this.f758;
        if (r0 != null) {
            r0.m1540();
        }
        this.f723 = 0;
        this.f730 = false;
        this.f761 = false;
        m1284();
        if (this.f730) {
            this.f758 = null;
            return;
        }
        throw new C0238("Fragment " + this + " did not call through to super.onDestroy()");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊˊ  reason: contains not printable characters */
    public void m1267() {
        this.f730 = false;
        m1288();
        this.f757 = null;
        if (this.f730) {
            C0246 r1 = this.f758;
            if (r1 == null) {
                return;
            }
            if (this.f722) {
                r1.m1540();
                this.f758 = null;
                return;
            }
            throw new IllegalStateException("Child FragmentManager of " + this + " was not " + " destroyed and this fragment is not retaining instance");
        }
        throw new C0238("Fragment " + this + " did not call through to super.onDetach()");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1214(C0225 r3) {
        m1192();
        if (r3 != this.f742.f779) {
            if (r3 == null || this.f742.f779 == null) {
                if (this.f742.f778) {
                    this.f742.f779 = r3;
                }
                if (r3 != null) {
                    r3.m1315();
                    return;
                }
                return;
            }
            throw new IllegalStateException("Trying to set a replacement startPostponedEnterTransition on " + this);
        }
    }

    /* renamed from: ʻʼ  reason: contains not printable characters */
    private C0223 m1192() {
        if (this.f742 == null) {
            this.f742 = new C0223();
        }
        return this.f742;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏˏ  reason: contains not printable characters */
    public int m1276() {
        C0223 r0 = this.f742;
        if (r0 == null) {
            return 0;
        }
        return r0.f773;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1228(int i) {
        if (this.f742 != null || i != 0) {
            m1192().f773 = i;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎˎ  reason: contains not printable characters */
    public int m1273() {
        C0223 r0 = this.f742;
        if (r0 == null) {
            return 0;
        }
        return r0.f774;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1199(int i, int i2) {
        if (this.f742 != null || i != 0 || i2 != 0) {
            m1192();
            C0223 r0 = this.f742;
            r0.f774 = i;
            r0.f775 = i2;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑˑ  reason: contains not printable characters */
    public int m1279() {
        C0223 r0 = this.f742;
        if (r0 == null) {
            return 0;
        }
        return r0.f775;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵔᵔ  reason: contains not printable characters */
    public C0230 m1293() {
        C0223 r0 = this.f742;
        if (r0 == null) {
            return null;
        }
        return r0.f776;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: יי  reason: contains not printable characters */
    public C0230 m1281() {
        C0223 r0 = this.f742;
        if (r0 == null) {
            return null;
        }
        return r0.f777;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵎᵎ  reason: contains not printable characters */
    public View m1291() {
        C0223 r0 = this.f742;
        if (r0 == null) {
            return null;
        }
        return r0.f770;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1218(View view) {
        m1192().f770 = view;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1203(Animator animator) {
        m1192().f771 = animator;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵢᵢ  reason: contains not printable characters */
    public Animator m1295() {
        C0223 r0 = this.f742;
        if (r0 == null) {
            return null;
        }
        return r0.f771;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ⁱⁱ  reason: contains not printable characters */
    public int m1297() {
        C0223 r0 = this.f742;
        if (r0 == null) {
            return 0;
        }
        return r0.f772;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1238(int i) {
        m1192().f772 = i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﹳﹳ  reason: contains not printable characters */
    public boolean m1299() {
        C0223 r0 = this.f742;
        if (r0 == null) {
            return false;
        }
        return r0.f778;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴٴ  reason: contains not printable characters */
    public boolean m1285() {
        C0223 r0 = this.f742;
        if (r0 == null) {
            return false;
        }
        return r0.f780;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public void m1262(boolean z) {
        m1192().f780 = z;
    }

    /* renamed from: android.support.v4.ʻ.ˉ$ʻ  reason: contains not printable characters */
    /* compiled from: Fragment */
    static class C0223 {

        /* renamed from: ʻ  reason: contains not printable characters */
        View f770;

        /* renamed from: ʼ  reason: contains not printable characters */
        Animator f771;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f772;

        /* renamed from: ʾ  reason: contains not printable characters */
        int f773;

        /* renamed from: ʿ  reason: contains not printable characters */
        int f774;

        /* renamed from: ˆ  reason: contains not printable characters */
        int f775;

        /* renamed from: ˈ  reason: contains not printable characters */
        C0230 f776 = null;

        /* renamed from: ˉ  reason: contains not printable characters */
        C0230 f777 = null;

        /* renamed from: ˊ  reason: contains not printable characters */
        boolean f778;

        /* renamed from: ˋ  reason: contains not printable characters */
        C0225 f779;

        /* renamed from: ˎ  reason: contains not printable characters */
        boolean f780;
        /* access modifiers changed from: private */

        /* renamed from: ˏ  reason: contains not printable characters */
        public Object f781 = null;
        /* access modifiers changed from: private */

        /* renamed from: ˑ  reason: contains not printable characters */
        public Object f782 = C0222.f720;
        /* access modifiers changed from: private */

        /* renamed from: י  reason: contains not printable characters */
        public Object f783 = null;
        /* access modifiers changed from: private */

        /* renamed from: ـ  reason: contains not printable characters */
        public Object f784 = C0222.f720;
        /* access modifiers changed from: private */

        /* renamed from: ٴ  reason: contains not printable characters */
        public Object f785 = null;
        /* access modifiers changed from: private */

        /* renamed from: ᐧ  reason: contains not printable characters */
        public Object f786 = C0222.f720;
        /* access modifiers changed from: private */

        /* renamed from: ᴵ  reason: contains not printable characters */
        public Boolean f787;
        /* access modifiers changed from: private */

        /* renamed from: ᵎ  reason: contains not printable characters */
        public Boolean f788;

        C0223() {
        }
    }
}
