package android.support.v4.ʻ;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.ˈ.C0353;
import android.view.LayoutInflater;
import android.view.View;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* renamed from: android.support.v4.ʻ.ˏ  reason: contains not printable characters */
/* compiled from: FragmentHostCallback */
public abstract class C0237<E> extends C0232 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Activity f797;

    /* renamed from: ʼ  reason: contains not printable characters */
    final Context f798;

    /* renamed from: ʽ  reason: contains not printable characters */
    final int f799;

    /* renamed from: ʾ  reason: contains not printable characters */
    final C0246 f800;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final Handler f801;

    /* renamed from: ˆ  reason: contains not printable characters */
    private C0353<String, C0266> f802;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f803;

    /* renamed from: ˉ  reason: contains not printable characters */
    private C0268 f804;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f805;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f806;

    /* renamed from: ʻ  reason: contains not printable characters */
    public View m1378(int i) {
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1380(C0222 r1, String[] strArr, int i) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1383(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1385() {
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1386(C0222 r1) {
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1388(C0222 r1) {
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1390() {
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m1391() {
        return true;
    }

    C0237(C0227 r3) {
        this(r3, r3, r3.mHandler, 0);
    }

    C0237(Activity activity, Context context, Handler handler, int i) {
        this.f800 = new C0246();
        this.f797 = activity;
        this.f798 = context;
        this.f801 = handler;
        this.f799 = i;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public LayoutInflater m1387() {
        return (LayoutInflater) this.f798.getSystemService("layout_inflater");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1379(C0222 r1, Intent intent, int i, Bundle bundle) {
        if (i == -1) {
            this.f798.startActivity(intent);
            return;
        }
        throw new IllegalStateException("Starting activity with a requestCode requires a FragmentActivity host");
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m1392() {
        return this.f799;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public Activity m1393() {
        return this.f797;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public Context m1394() {
        return this.f798;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public Handler m1395() {
        return this.f801;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public C0246 m1396() {
        return this.f800;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.ˏ.ʻ(java.lang.String, boolean, boolean):android.support.v4.ʻ.ⁱ
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.ʻ.ˏ.ʻ(android.support.v4.ʻ.ˉ, java.lang.String[], int):void
      android.support.v4.ʻ.ˋ.ʻ(android.content.Context, java.lang.String, android.os.Bundle):android.support.v4.ʻ.ˉ
      android.support.v4.ʻ.ˏ.ʻ(java.lang.String, boolean, boolean):android.support.v4.ʻ.ⁱ */
    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public C0268 m1397() {
        C0268 r0 = this.f804;
        if (r0 != null) {
            return r0;
        }
        this.f805 = true;
        this.f804 = m1377("(root)", this.f806, true);
        return this.f804;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1382(String str) {
        C0268 r0;
        C0353<String, C0266> r02 = this.f802;
        if (r02 != null && (r0 = (C0268) r02.get(str)) != null && !r0.f955) {
            r0.m1644();
            this.f802.remove(str);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean m1398() {
        return this.f803;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.ˏ.ʻ(java.lang.String, boolean, boolean):android.support.v4.ʻ.ⁱ
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.ʻ.ˏ.ʻ(android.support.v4.ʻ.ˉ, java.lang.String[], int):void
      android.support.v4.ʻ.ˋ.ʻ(android.content.Context, java.lang.String, android.os.Bundle):android.support.v4.ʻ.ˉ
      android.support.v4.ʻ.ˏ.ʻ(java.lang.String, boolean, boolean):android.support.v4.ʻ.ⁱ */
    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m1399() {
        if (!this.f806) {
            this.f806 = true;
            C0268 r1 = this.f804;
            if (r1 != null) {
                r1.m1638();
            } else if (!this.f805) {
                this.f804 = m1377("(root)", this.f806, false);
                C0268 r12 = this.f804;
                if (r12 != null && !r12.f954) {
                    this.f804.m1638();
                }
            }
            this.f805 = true;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1384(boolean z) {
        this.f803 = z;
        C0268 r0 = this.f804;
        if (r0 != null && this.f806) {
            this.f806 = false;
            if (z) {
                r0.m1640();
            } else {
                r0.m1639();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m1400() {
        C0268 r0 = this.f804;
        if (r0 != null) {
            r0.m1644();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: י  reason: contains not printable characters */
    public void m1401() {
        C0353<String, C0266> r0 = this.f802;
        if (r0 != null) {
            int size = r0.size();
            C0268[] r1 = new C0268[size];
            for (int i = size - 1; i >= 0; i--) {
                r1[i] = (C0268) this.f802.m1980(i);
            }
            for (int i2 = 0; i2 < size; i2++) {
                C0268 r3 = r1[i2];
                r3.m1641();
                r3.m1643();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0268 m1377(String str, boolean z, boolean z2) {
        if (this.f802 == null) {
            this.f802 = new C0353<>();
        }
        C0268 r0 = (C0268) this.f802.get(str);
        if (r0 == null && z2) {
            C0268 r02 = new C0268(str, this, z);
            this.f802.put(str, r02);
            return r02;
        } else if (!z || r0 == null || r0.f954) {
            return r0;
        } else {
            r0.m1638();
            return r0;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ـ  reason: contains not printable characters */
    public C0353<String, C0266> m1402() {
        C0353<String, C0266> r0 = this.f802;
        int i = 0;
        if (r0 != null) {
            int size = r0.size();
            C0268[] r2 = new C0268[size];
            for (int i2 = size - 1; i2 >= 0; i2--) {
                r2[i2] = (C0268) this.f802.m1980(i2);
            }
            boolean r3 = m1398();
            int i3 = 0;
            while (i < size) {
                C0268 r5 = r2[i];
                if (!r5.f955 && r3) {
                    if (!r5.f954) {
                        r5.m1638();
                    }
                    r5.m1640();
                }
                if (r5.f955) {
                    i3 = 1;
                } else {
                    r5.m1644();
                    this.f802.remove(r5.f953);
                }
                i++;
            }
            i = i3;
        }
        if (i != 0) {
            return this.f802;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1381(C0353<String, C0266> r4) {
        if (r4 != null) {
            int size = r4.size();
            for (int i = 0; i < size; i++) {
                ((C0268) r4.m1980(i)).m1635(this);
            }
        }
        this.f802 = r4;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1389(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mLoadersStarted=");
        printWriter.println(this.f806);
        if (this.f804 != null) {
            printWriter.print(str);
            printWriter.print("Loader Manager ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this.f804)));
            printWriter.println(":");
            C0268 r0 = this.f804;
            r0.m1636(str + "  ", fileDescriptor, printWriter, strArr);
        }
    }
}
