package android.support.v4.ʻ;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcelable;
import android.support.v4.ʻ.C0222;
import android.support.v4.ʻ.C0239;
import android.support.v4.ˈ.C0332;
import android.support.v4.ˈ.C0334;
import android.support.v4.ˈ.C0335;
import android.support.v4.ˈ.C0347;
import android.support.v4.ˉ.C0414;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: android.support.v4.ʻ.י  reason: contains not printable characters */
/* compiled from: FragmentManager */
final class C0246 extends C0239 implements LayoutInflater.Factory2 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static boolean f810 = false;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    static final Interpolator f811 = new DecelerateInterpolator(2.5f);

    /* renamed from: ʾʾ  reason: contains not printable characters */
    static final Interpolator f812 = new AccelerateInterpolator(2.5f);

    /* renamed from: ʿʿ  reason: contains not printable characters */
    static final Interpolator f813 = new DecelerateInterpolator(1.5f);

    /* renamed from: ــ  reason: contains not printable characters */
    static final Interpolator f814 = new AccelerateInterpolator(1.5f);

    /* renamed from: ᐧ  reason: contains not printable characters */
    static Field f815;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    C0254 f816;

    /* renamed from: ʼ  reason: contains not printable characters */
    ArrayList<C0252> f817;

    /* renamed from: ʽ  reason: contains not printable characters */
    boolean f818;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    Runnable f819 = new Runnable() {
        public void run() {
            C0246.this.m1516();
        }
    };

    /* renamed from: ʾ  reason: contains not printable characters */
    int f820 = 0;

    /* renamed from: ʿ  reason: contains not printable characters */
    final ArrayList<C0222> f821 = new ArrayList<>();

    /* renamed from: ˆ  reason: contains not printable characters */
    SparseArray<C0222> f822;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private final CopyOnWriteArrayList<C0347<C0239.C0240, Boolean>> f823 = new CopyOnWriteArrayList<>();

    /* renamed from: ˈ  reason: contains not printable characters */
    ArrayList<C0201> f824;

    /* renamed from: ˉ  reason: contains not printable characters */
    ArrayList<C0222> f825;

    /* renamed from: ˊ  reason: contains not printable characters */
    ArrayList<C0201> f826;

    /* renamed from: ˋ  reason: contains not printable characters */
    ArrayList<Integer> f827;

    /* renamed from: ˎ  reason: contains not printable characters */
    ArrayList<C0239.C0241> f828;

    /* renamed from: ˏ  reason: contains not printable characters */
    int f829 = 0;

    /* renamed from: ˑ  reason: contains not printable characters */
    C0237 f830;

    /* renamed from: י  reason: contains not printable characters */
    C0232 f831;

    /* renamed from: ـ  reason: contains not printable characters */
    C0222 f832;

    /* renamed from: ٴ  reason: contains not printable characters */
    C0222 f833;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    SparseArray<Parcelable> f834 = null;

    /* renamed from: ᴵ  reason: contains not printable characters */
    boolean f835;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    ArrayList<C0253> f836;

    /* renamed from: ᵎ  reason: contains not printable characters */
    boolean f837;

    /* renamed from: ᵔ  reason: contains not printable characters */
    boolean f838;

    /* renamed from: ᵢ  reason: contains not printable characters */
    String f839;

    /* renamed from: ⁱ  reason: contains not printable characters */
    boolean f840;

    /* renamed from: ﹳ  reason: contains not printable characters */
    ArrayList<C0201> f841;

    /* renamed from: ﹶ  reason: contains not printable characters */
    ArrayList<Boolean> f842;

    /* renamed from: ﾞ  reason: contains not printable characters */
    ArrayList<C0222> f843;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    Bundle f844 = null;

    /* renamed from: android.support.v4.ʻ.י$ʿ  reason: contains not printable characters */
    /* compiled from: FragmentManager */
    static class C0251 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public static final int[] f862 = {16842755, 16842960, 16842961};
    }

    /* renamed from: android.support.v4.ʻ.י$ˆ  reason: contains not printable characters */
    /* compiled from: FragmentManager */
    interface C0252 {
        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m1544(ArrayList<C0201> arrayList, ArrayList<Boolean> arrayList2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int m1448(int i, boolean z) {
        if (i == 4097) {
            return z ? 1 : 2;
        }
        if (i == 4099) {
            return z ? 5 : 6;
        }
        if (i != 8194) {
            return -1;
        }
        return z ? 3 : 4;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static int m1457(int i) {
        if (i == 4097) {
            return 8194;
        }
        if (i != 4099) {
            return i != 8194 ? 0 : 4097;
        }
        return 4099;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﾞ  reason: contains not printable characters */
    public LayoutInflater.Factory2 m1543() {
        return this;
    }

    C0246() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static boolean m1444(C0249 r4) {
        if (r4.f859 instanceof AlphaAnimation) {
            return true;
        }
        if (!(r4.f859 instanceof AnimationSet)) {
            return m1443(r4.f860);
        }
        List<Animation> animations = ((AnimationSet) r4.f859).getAnimations();
        for (int i = 0; i < animations.size(); i++) {
            if (animations.get(i) instanceof AlphaAnimation) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static boolean m1443(Animator animator) {
        if (animator == null) {
            return false;
        }
        if (animator instanceof ValueAnimator) {
            PropertyValuesHolder[] values = ((ValueAnimator) animator).getValues();
            for (PropertyValuesHolder propertyName : values) {
                if ("alpha".equals(propertyName.getPropertyName())) {
                    return true;
                }
            }
        } else if (animator instanceof AnimatorSet) {
            ArrayList<Animator> childAnimations = ((AnimatorSet) animator).getChildAnimations();
            for (int i = 0; i < childAnimations.size(); i++) {
                if (m1443(childAnimations.get(i))) {
                    return true;
                }
            }
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static boolean m1445(View view, C0249 r4) {
        if (view == null || r4 == null || Build.VERSION.SDK_INT < 19 || view.getLayerType() != 0 || !C0414.m2248(view) || !m1444(r4)) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m1440(RuntimeException runtimeException) {
        Log.e("FragmentManager", runtimeException.getMessage());
        Log.e("FragmentManager", "Activity state:");
        PrintWriter printWriter = new PrintWriter(new C0335("FragmentManager"));
        C0237 r0 = this.f830;
        if (r0 != null) {
            try {
                r0.m1383("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e) {
                Log.e("FragmentManager", "Failed dumping state", e);
            }
        } else {
            try {
                m1481("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e2) {
                Log.e("FragmentManager", "Failed dumping state", e2);
            }
        }
        throw runtimeException;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0260 m1467() {
        return new C0201(this);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m1497() {
        boolean r0 = m1516();
        m1447();
        return r0;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m1503() {
        m1462();
        return m1446((String) null, -1, 0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m1446(String str, int i, int i2) {
        C0239 r1;
        m1516();
        m1454(true);
        C0222 r12 = this.f833;
        if (r12 != null && i < 0 && str == null && (r1 = r12.m1257()) != null && r1.m1406()) {
            return true;
        }
        boolean r9 = m1487(this.f841, this.f842, str, i, i2);
        if (r9) {
            this.f818 = true;
            try {
                m1451(this.f841, this.f842);
            } finally {
                m1461();
            }
        }
        m1517();
        m1453();
        return r9;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1471(Bundle bundle, String str, C0222 r6) {
        if (r6.f729 < 0) {
            m1440(new IllegalStateException("Fragment " + r6 + " is not currently in the FragmentManager"));
        }
        bundle.putInt(str, r6.f729);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0222 m1464(Bundle bundle, String str) {
        int i = bundle.getInt(str, -1);
        if (i == -1) {
            return null;
        }
        C0222 r0 = this.f822.get(i);
        if (r0 == null) {
            m1440(new IllegalStateException("Fragment no longer exists for key " + str + ": index " + i));
        }
        return r0;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public List<C0222> m1504() {
        List<C0222> list;
        if (this.f821.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        synchronized (this.f821) {
            list = (List) this.f821.clone();
        }
        return list;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        C0222 r1 = this.f832;
        if (r1 != null) {
            C0334.m1918(r1, sb);
        } else {
            C0334.m1918(this.f830, sb);
        }
        sb.append("}}");
        return sb.toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1481(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int size;
        int size2;
        int size3;
        int size4;
        int size5;
        String str2 = str + "    ";
        SparseArray<C0222> sparseArray = this.f822;
        if (sparseArray != null && (size5 = sparseArray.size()) > 0) {
            printWriter.print(str);
            printWriter.print("Active Fragments in ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this)));
            printWriter.println(":");
            for (int i = 0; i < size5; i++) {
                C0222 valueAt = this.f822.valueAt(i);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.println(valueAt);
                if (valueAt != null) {
                    valueAt.m1220(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }
        int size6 = this.f821.size();
        if (size6 > 0) {
            printWriter.print(str);
            printWriter.println("Added Fragments:");
            for (int i2 = 0; i2 < size6; i2++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.println(this.f821.get(i2).toString());
            }
        }
        ArrayList<C0222> arrayList = this.f825;
        if (arrayList != null && (size4 = arrayList.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Fragments Created Menus:");
            for (int i3 = 0; i3 < size4; i3++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i3);
                printWriter.print(": ");
                printWriter.println(this.f825.get(i3).toString());
            }
        }
        ArrayList<C0201> arrayList2 = this.f824;
        if (arrayList2 != null && (size3 = arrayList2.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Back Stack:");
            for (int i4 = 0; i4 < size3; i4++) {
                C0201 r4 = this.f824.get(i4);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i4);
                printWriter.print(": ");
                printWriter.println(r4.toString());
                r4.m1152(str2, fileDescriptor, printWriter, strArr);
            }
        }
        synchronized (this) {
            if (this.f826 != null && (size2 = this.f826.size()) > 0) {
                printWriter.print(str);
                printWriter.println("Back Stack Indices:");
                for (int i5 = 0; i5 < size2; i5++) {
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i5);
                    printWriter.print(": ");
                    printWriter.println(this.f826.get(i5));
                }
            }
            if (this.f827 != null && this.f827.size() > 0) {
                printWriter.print(str);
                printWriter.print("mAvailBackStackIndices: ");
                printWriter.println(Arrays.toString(this.f827.toArray()));
            }
        }
        ArrayList<C0252> arrayList3 = this.f817;
        if (arrayList3 != null && (size = arrayList3.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Pending Actions:");
            for (int i6 = 0; i6 < size; i6++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i6);
                printWriter.print(": ");
                printWriter.println(this.f817.get(i6));
            }
        }
        printWriter.print(str);
        printWriter.println("FragmentManager misc state:");
        printWriter.print(str);
        printWriter.print("  mHost=");
        printWriter.println(this.f830);
        printWriter.print(str);
        printWriter.print("  mContainer=");
        printWriter.println(this.f831);
        if (this.f832 != null) {
            printWriter.print(str);
            printWriter.print("  mParent=");
            printWriter.println(this.f832);
        }
        printWriter.print(str);
        printWriter.print("  mCurState=");
        printWriter.print(this.f829);
        printWriter.print(" mStateSaved=");
        printWriter.print(this.f837);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.f838);
        if (this.f835) {
            printWriter.print(str);
            printWriter.print("  mNeedMenuInvalidate=");
            printWriter.println(this.f835);
        }
        if (this.f839 != null) {
            printWriter.print(str);
            printWriter.print("  mNoTransactionsBecause=");
            printWriter.println(this.f839);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static C0249 m1432(Context context, float f, float f2, float f3, float f4) {
        AnimationSet animationSet = new AnimationSet(false);
        ScaleAnimation scaleAnimation = new ScaleAnimation(f, f2, f, f2, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(f811);
        scaleAnimation.setDuration(220);
        animationSet.addAnimation(scaleAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(f3, f4);
        alphaAnimation.setInterpolator(f813);
        alphaAnimation.setDuration(220);
        animationSet.addAnimation(alphaAnimation);
        return new C0249(animationSet);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static C0249 m1431(Context context, float f, float f2) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f, f2);
        alphaAnimation.setInterpolator(f813);
        alphaAnimation.setDuration(220);
        return new C0249(alphaAnimation);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0249 m1466(C0222 r6, int i, boolean z, int i2) {
        int r62;
        int r0 = r6.m1276();
        Animation r1 = r6.m1197(i, z, r0);
        if (r1 != null) {
            return new C0249(r1);
        }
        Animator r63 = r6.m1226(i, z, r0);
        if (r63 != null) {
            return new C0249(r63);
        }
        if (r0 != 0) {
            boolean equals = "anim".equals(this.f830.m1394().getResources().getResourceTypeName(r0));
            boolean z2 = false;
            if (equals) {
                try {
                    Animation loadAnimation = AnimationUtils.loadAnimation(this.f830.m1394(), r0);
                    if (loadAnimation != null) {
                        return new C0249(loadAnimation);
                    }
                    z2 = true;
                } catch (Resources.NotFoundException e) {
                    throw e;
                } catch (RuntimeException unused) {
                }
            }
            if (!z2) {
                try {
                    Animator loadAnimator = AnimatorInflater.loadAnimator(this.f830.m1394(), r0);
                    if (loadAnimator != null) {
                        return new C0249(loadAnimator);
                    }
                } catch (RuntimeException e2) {
                    if (!equals) {
                        Animation loadAnimation2 = AnimationUtils.loadAnimation(this.f830.m1394(), r0);
                        if (loadAnimation2 != null) {
                            return new C0249(loadAnimation2);
                        }
                    } else {
                        throw e2;
                    }
                }
            }
        }
        if (i == 0 || (r62 = m1448(i, z)) < 0) {
            return null;
        }
        switch (r62) {
            case 1:
                return m1432(this.f830.m1394(), 1.125f, 1.0f, 0.0f, 1.0f);
            case 2:
                return m1432(this.f830.m1394(), 1.0f, 0.975f, 1.0f, 0.0f);
            case 3:
                return m1432(this.f830.m1394(), 0.975f, 1.0f, 0.0f, 1.0f);
            case 4:
                return m1432(this.f830.m1394(), 1.0f, 1.075f, 1.0f, 0.0f);
            case 5:
                return m1431(this.f830.m1394(), 0.0f, 1.0f);
            case 6:
                return m1431(this.f830.m1394(), 1.0f, 0.0f);
            default:
                if (i2 == 0 && this.f830.m1391()) {
                    i2 = this.f830.m1392();
                }
                if (i2 == 0) {
                }
                return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, int, int, int, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.ˈ.ʼ<android.support.v4.ʻ.ˉ>):int
      android.support.v4.ʻ.י.ʻ(android.content.Context, float, float, float, float):android.support.v4.ʻ.י$ʽ
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י, android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1473(C0222 r8) {
        if (!r8.f736) {
            return;
        }
        if (this.f818) {
            this.f840 = true;
            return;
        }
        r8.f736 = false;
        m1474(r8, this.f829, 0, 0, false);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static void m1450(View view, C0249 r4) {
        if (view != null && r4 != null && m1445(view, r4)) {
            if (r4.f860 != null) {
                r4.f860.addListener(new C0250(view));
                return;
            }
            Animation.AnimationListener r0 = m1433(r4.f859);
            view.setLayerType(2, null);
            r4.f859.setAnimationListener(new C0247(view, r0));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Animation.AnimationListener m1433(Animation animation) {
        try {
            if (f815 == null) {
                f815 = Animation.class.getDeclaredField("mListener");
                f815.setAccessible(true);
            }
            return (Animation.AnimationListener) f815.get(animation);
        } catch (NoSuchFieldException e) {
            Log.e("FragmentManager", "No field with the name mListener is found in Animation class", e);
            return null;
        } catch (IllegalAccessException e2) {
            Log.e("FragmentManager", "Cannot access Animation's mListener field", e2);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1483(int i) {
        return this.f829 >= i;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v0, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v0, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v1, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v1, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v2, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v2, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v3, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v4, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v3, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v4, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v5, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v6, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v7, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v8, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v9, resolved type: int} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, int, int, int, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.ˈ.ʼ<android.support.v4.ʻ.ˉ>):int
      android.support.v4.ʻ.י.ʻ(android.content.Context, float, float, float, float):android.support.v4.ʻ.י$ʽ
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י, android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.content.Context, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, android.content.Context, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(android.content.Context, float, float):android.support.v4.ʻ.י$ʽ
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.support.v4.ʻ.י$ʽ, int):void
      android.support.v4.ʻ.י.ʻ(java.lang.String, int, int):boolean
      android.support.v4.ʻ.י.ʻ(android.os.Bundle, java.lang.String, android.support.v4.ʻ.ˉ):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.os.Bundle, boolean):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˏ, android.support.v4.ʻ.ˋ, android.support.v4.ʻ.ˉ):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.content.Context, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʼ(android.support.v4.ʻ.ˉ, android.content.Context, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, android.content.Context, int]
     candidates:
      android.support.v4.ʻ.י.ʼ(android.support.v4.ʻ.ˉ, android.os.Bundle, boolean):void
      android.support.v4.ʻ.י.ʼ(android.support.v4.ʻ.ˉ, android.content.Context, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.os.Bundle, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, android.os.Bundle, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(android.content.Context, float, float):android.support.v4.ʻ.י$ʽ
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.support.v4.ʻ.י$ʽ, int):void
      android.support.v4.ʻ.י.ʻ(java.lang.String, int, int):boolean
      android.support.v4.ʻ.י.ʻ(android.os.Bundle, java.lang.String, android.support.v4.ʻ.ˉ):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.content.Context, boolean):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˏ, android.support.v4.ʻ.ˋ, android.support.v4.ʻ.ˉ):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʼ(android.support.v4.ʻ.ˉ, android.os.Bundle, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, android.os.Bundle, int]
     candidates:
      android.support.v4.ʻ.י.ʼ(android.support.v4.ʻ.ˉ, android.content.Context, boolean):void
      android.support.v4.ʻ.י.ʼ(android.support.v4.ʻ.ˉ, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.view.View, android.os.Bundle, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, android.view.View, android.os.Bundle, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, boolean, int):android.support.v4.ʻ.י$ʽ
      android.support.v4.ʻ.י.ʻ(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.ʻ.ˑ.ʻ(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.view.View, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʼ(android.support.v4.ʻ.ˉ, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, int]
     candidates:
      android.support.v4.ʻ.י.ʼ(int, boolean):int
      android.support.v4.ʻ.י.ʼ(android.view.View, android.support.v4.ʻ.י$ʽ):void
      android.support.v4.ʻ.י.ʼ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.ʻ.י.ʼ(android.support.v4.ʻ.ˉ, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʽ(android.support.v4.ʻ.ˉ, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, int]
     candidates:
      android.support.v4.ʻ.י.ʽ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>):boolean
      android.support.v4.ʻ.י.ʽ(android.support.v4.ʻ.ˉ, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, boolean, int):android.support.v4.ʻ.י$ʽ
     arg types: [android.support.v4.ʻ.ˉ, int, int, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.view.View, android.os.Bundle, boolean):void
      android.support.v4.ʻ.י.ʻ(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.ʻ.ˑ.ʻ(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, boolean, int):android.support.v4.ʻ.י$ʽ */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0077, code lost:
        if (r0 != 4) goto L_0x0456;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x02a8  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x02ac  */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x045b  */
    /* JADX WARNING: Removed duplicated region for block: B:226:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x015b  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0191  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x01b0  */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m1474(android.support.v4.ʻ.C0222 r18, int r19, int r20, int r21, boolean r22) {
        /*
            r17 = this;
            r6 = r17
            r7 = r18
            boolean r0 = r7.f741
            r8 = 1
            if (r0 == 0) goto L_0x0011
            boolean r0 = r7.f753
            if (r0 == 0) goto L_0x000e
            goto L_0x0011
        L_0x000e:
            r0 = r19
            goto L_0x0016
        L_0x0011:
            r0 = r19
            if (r0 <= r8) goto L_0x0016
            r0 = 1
        L_0x0016:
            boolean r1 = r7.f743
            if (r1 == 0) goto L_0x002c
            int r1 = r7.f723
            if (r0 <= r1) goto L_0x002c
            int r0 = r7.f723
            if (r0 != 0) goto L_0x002a
            boolean r0 = r18.m1223()
            if (r0 == 0) goto L_0x002a
            r0 = 1
            goto L_0x002c
        L_0x002a:
            int r0 = r7.f723
        L_0x002c:
            boolean r1 = r7.f736
            r9 = 4
            r10 = 3
            if (r1 == 0) goto L_0x003a
            int r1 = r7.f723
            if (r1 >= r9) goto L_0x003a
            if (r0 <= r10) goto L_0x003a
            r11 = 3
            goto L_0x003b
        L_0x003a:
            r11 = r0
        L_0x003b:
            int r0 = r7.f723
            r12 = 2
            java.lang.String r13 = "FragmentManager"
            r14 = 0
            r15 = 0
            if (r0 > r11) goto L_0x02f2
            boolean r0 = r7.f745
            if (r0 == 0) goto L_0x004d
            boolean r0 = r7.f747
            if (r0 != 0) goto L_0x004d
            return
        L_0x004d:
            android.view.View r0 = r18.m1291()
            if (r0 != 0) goto L_0x0059
            android.animation.Animator r0 = r18.m1295()
            if (r0 == 0) goto L_0x006d
        L_0x0059:
            r7.m1218(r14)
            r7.m1203(r14)
            int r2 = r18.m1297()
            r3 = 0
            r4 = 0
            r5 = 1
            r0 = r17
            r1 = r18
            r0.m1474(r1, r2, r3, r4, r5)
        L_0x006d:
            int r0 = r7.f723
            if (r0 == 0) goto L_0x007e
            if (r0 == r8) goto L_0x01ab
            if (r0 == r12) goto L_0x02a6
            if (r0 == r10) goto L_0x02aa
            if (r0 == r9) goto L_0x007b
            goto L_0x0456
        L_0x007b:
            r0 = 4
            goto L_0x02cc
        L_0x007e:
            if (r11 <= 0) goto L_0x01ab
            boolean r0 = android.support.v4.ʻ.C0246.f810
            if (r0 == 0) goto L_0x0098
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "moveto CREATED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r13, r0)
        L_0x0098:
            android.os.Bundle r0 = r7.f725
            if (r0 == 0) goto L_0x00e0
            android.os.Bundle r0 = r7.f725
            android.support.v4.ʻ.ˏ r1 = r6.f830
            android.content.Context r1 = r1.m1394()
            java.lang.ClassLoader r1 = r1.getClassLoader()
            r0.setClassLoader(r1)
            android.os.Bundle r0 = r7.f725
            java.lang.String r1 = "android:view_state"
            android.util.SparseArray r0 = r0.getSparseParcelableArray(r1)
            r7.f727 = r0
            android.os.Bundle r0 = r7.f725
            java.lang.String r1 = "android:target_state"
            android.support.v4.ʻ.ˉ r0 = r6.m1464(r0, r1)
            r7.f735 = r0
            android.support.v4.ʻ.ˉ r0 = r7.f735
            if (r0 == 0) goto L_0x00cd
            android.os.Bundle r0 = r7.f725
            java.lang.String r1 = "android:target_req_state"
            int r0 = r0.getInt(r1, r15)
            r7.f739 = r0
        L_0x00cd:
            android.os.Bundle r0 = r7.f725
            java.lang.String r1 = "android:user_visible_hint"
            boolean r0 = r0.getBoolean(r1, r8)
            r7.f734 = r0
            boolean r0 = r7.f734
            if (r0 != 0) goto L_0x00e0
            r7.f736 = r8
            if (r11 <= r10) goto L_0x00e0
            r11 = 3
        L_0x00e0:
            android.support.v4.ʻ.ˏ r0 = r6.f830
            r7.f756 = r0
            android.support.v4.ʻ.ˉ r1 = r6.f832
            r7.f762 = r1
            if (r1 == 0) goto L_0x00ed
            android.support.v4.ʻ.י r0 = r1.f758
            goto L_0x00f1
        L_0x00ed:
            android.support.v4.ʻ.י r0 = r0.m1396()
        L_0x00f1:
            r7.f754 = r0
            android.support.v4.ʻ.ˉ r0 = r7.f735
            java.lang.String r5 = "Fragment "
            if (r0 == 0) goto L_0x0142
            android.util.SparseArray<android.support.v4.ʻ.ˉ> r0 = r6.f822
            android.support.v4.ʻ.ˉ r1 = r7.f735
            int r1 = r1.f729
            java.lang.Object r0 = r0.get(r1)
            android.support.v4.ʻ.ˉ r1 = r7.f735
            if (r0 != r1) goto L_0x011d
            android.support.v4.ʻ.ˉ r0 = r7.f735
            int r0 = r0.f723
            if (r0 >= r8) goto L_0x0142
            android.support.v4.ʻ.ˉ r1 = r7.f735
            r2 = 1
            r3 = 0
            r4 = 0
            r16 = 1
            r0 = r17
            r9 = r5
            r5 = r16
            r0.m1474(r1, r2, r3, r4, r5)
            goto L_0x0143
        L_0x011d:
            r9 = r5
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r9)
            r1.append(r7)
            java.lang.String r2 = " declared target fragment "
            r1.append(r2)
            android.support.v4.ʻ.ˉ r2 = r7.f735
            r1.append(r2)
            java.lang.String r2 = " that does not belong to this FragmentManager!"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0142:
            r9 = r5
        L_0x0143:
            android.support.v4.ʻ.ˏ r0 = r6.f830
            android.content.Context r0 = r0.m1394()
            r6.m1475(r7, r0, r15)
            r7.f730 = r15
            android.support.v4.ʻ.ˏ r0 = r6.f830
            android.content.Context r0 = r0.m1394()
            r7.m1206(r0)
            boolean r0 = r7.f730
            if (r0 == 0) goto L_0x0191
            android.support.v4.ʻ.ˉ r0 = r7.f762
            if (r0 != 0) goto L_0x0165
            android.support.v4.ʻ.ˏ r0 = r6.f830
            r0.m1388(r7)
            goto L_0x016a
        L_0x0165:
            android.support.v4.ʻ.ˉ r0 = r7.f762
            r0.m1215(r7)
        L_0x016a:
            android.support.v4.ʻ.ˏ r0 = r6.f830
            android.content.Context r0 = r0.m1394()
            r6.m1492(r7, r0, r15)
            boolean r0 = r7.f761
            if (r0 != 0) goto L_0x0187
            android.os.Bundle r0 = r7.f725
            r6.m1476(r7, r0, r15)
            android.os.Bundle r0 = r7.f725
            r7.m1272(r0)
            android.os.Bundle r0 = r7.f725
            r6.m1493(r7, r0, r15)
            goto L_0x018e
        L_0x0187:
            android.os.Bundle r0 = r7.f725
            r7.m1258(r0)
            r7.f723 = r8
        L_0x018e:
            r7.f722 = r15
            goto L_0x01ab
        L_0x0191:
            android.support.v4.ʻ.ˏˏ r0 = new android.support.v4.ʻ.ˏˏ
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r9)
            r1.append(r7)
            java.lang.String r2 = " did not call through to super.onAttach()"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x01ab:
            r17.m1500(r18)
            if (r11 <= r8) goto L_0x02a6
            boolean r0 = android.support.v4.ʻ.C0246.f810
            if (r0 == 0) goto L_0x01c8
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "moveto ACTIVITY_CREATED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r13, r0)
        L_0x01c8:
            boolean r0 = r7.f745
            if (r0 != 0) goto L_0x0291
            int r0 = r7.f765
            if (r0 == 0) goto L_0x0240
            int r0 = r7.f765
            r1 = -1
            if (r0 != r1) goto L_0x01f3
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Cannot create fragment "
            r1.append(r2)
            r1.append(r7)
            java.lang.String r2 = " for a container view with no id"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            r6.m1440(r0)
        L_0x01f3:
            android.support.v4.ʻ.ˋ r0 = r6.f831
            int r1 = r7.f765
            android.view.View r0 = r0.m1338(r1)
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            if (r0 != 0) goto L_0x0241
            boolean r1 = r7.f749
            if (r1 != 0) goto L_0x0241
            android.content.res.Resources r1 = r18.m1243()     // Catch:{ NotFoundException -> 0x020e }
            int r2 = r7.f765     // Catch:{ NotFoundException -> 0x020e }
            java.lang.String r1 = r1.getResourceName(r2)     // Catch:{ NotFoundException -> 0x020e }
            goto L_0x0210
        L_0x020e:
            java.lang.String r1 = "unknown"
        L_0x0210:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "No view found for id 0x"
            r3.append(r4)
            int r4 = r7.f765
            java.lang.String r4 = java.lang.Integer.toHexString(r4)
            r3.append(r4)
            java.lang.String r4 = " ("
            r3.append(r4)
            r3.append(r1)
            java.lang.String r1 = ") for fragment "
            r3.append(r1)
            r3.append(r7)
            java.lang.String r1 = r3.toString()
            r2.<init>(r1)
            r6.m1440(r2)
            goto L_0x0241
        L_0x0240:
            r0 = r14
        L_0x0241:
            r7.f728 = r0
            android.os.Bundle r1 = r7.f725
            android.view.LayoutInflater r1 = r7.m1244(r1)
            android.os.Bundle r2 = r7.f725
            android.view.View r1 = r7.m1227(r1, r0, r2)
            r7.f750 = r1
            android.view.View r1 = r7.f750
            if (r1 == 0) goto L_0x028f
            android.view.View r1 = r7.f750
            r7.f732 = r1
            android.view.View r1 = r7.f750
            r1.setSaveFromParentEnabled(r15)
            if (r0 == 0) goto L_0x0265
            android.view.View r1 = r7.f750
            r0.addView(r1)
        L_0x0265:
            boolean r0 = r7.f767
            if (r0 == 0) goto L_0x0270
            android.view.View r0 = r7.f750
            r1 = 8
            r0.setVisibility(r1)
        L_0x0270:
            android.view.View r0 = r7.f750
            android.os.Bundle r1 = r7.f725
            r7.m1219(r0, r1)
            android.view.View r0 = r7.f750
            android.os.Bundle r1 = r7.f725
            r6.m1477(r7, r0, r1, r15)
            android.view.View r0 = r7.f750
            int r0 = r0.getVisibility()
            if (r0 != 0) goto L_0x028b
            android.view.ViewGroup r0 = r7.f728
            if (r0 == 0) goto L_0x028b
            goto L_0x028c
        L_0x028b:
            r8 = 0
        L_0x028c:
            r7.f746 = r8
            goto L_0x0291
        L_0x028f:
            r7.f732 = r14
        L_0x0291:
            android.os.Bundle r0 = r7.f725
            r7.m1275(r0)
            android.os.Bundle r0 = r7.f725
            r6.m1501(r7, r0, r15)
            android.view.View r0 = r7.f750
            if (r0 == 0) goto L_0x02a4
            android.os.Bundle r0 = r7.f725
            r7.m1213(r0)
        L_0x02a4:
            r7.f725 = r14
        L_0x02a6:
            if (r11 <= r12) goto L_0x02aa
            r7.f723 = r10
        L_0x02aa:
            if (r11 <= r10) goto L_0x007b
            boolean r0 = android.support.v4.ʻ.C0246.f810
            if (r0 == 0) goto L_0x02c4
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "moveto STARTED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r13, r0)
        L_0x02c4:
            r18.m1242()
            r6.m1494(r7, r15)
            goto L_0x007b
        L_0x02cc:
            if (r11 <= r0) goto L_0x0456
            boolean r0 = android.support.v4.ʻ.C0246.f810
            if (r0 == 0) goto L_0x02e6
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "moveto RESUMED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r13, r0)
        L_0x02e6:
            r18.m1235()
            r6.m1502(r7, r15)
            r7.f725 = r14
            r7.f727 = r14
            goto L_0x0456
        L_0x02f2:
            int r0 = r7.f723
            if (r0 <= r11) goto L_0x0456
            int r0 = r7.f723
            if (r0 == r8) goto L_0x03e2
            if (r0 == r12) goto L_0x0364
            if (r0 == r10) goto L_0x0347
            r1 = 4
            if (r0 == r1) goto L_0x0326
            r1 = 5
            if (r0 == r1) goto L_0x0306
            goto L_0x0456
        L_0x0306:
            if (r11 >= r1) goto L_0x0326
            boolean r0 = android.support.v4.ʻ.C0246.f810
            if (r0 == 0) goto L_0x0320
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "movefrom RESUMED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r13, r0)
        L_0x0320:
            r18.m1283()
            r6.m1507(r7, r15)
        L_0x0326:
            r0 = 4
            if (r11 >= r0) goto L_0x0347
            boolean r0 = android.support.v4.ʻ.C0246.f810
            if (r0 == 0) goto L_0x0341
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "movefrom STARTED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r13, r0)
        L_0x0341:
            r18.m1256()
            r6.m1509(r7, r15)
        L_0x0347:
            if (r11 >= r10) goto L_0x0364
            boolean r0 = android.support.v4.ʻ.C0246.f810
            if (r0 == 0) goto L_0x0361
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "movefrom STOPPED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r13, r0)
        L_0x0361:
            r18.m1264()
        L_0x0364:
            if (r11 >= r12) goto L_0x03e2
            boolean r0 = android.support.v4.ʻ.C0246.f810
            if (r0 == 0) goto L_0x037e
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "movefrom ACTIVITY_CREATED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r13, r0)
        L_0x037e:
            android.view.View r0 = r7.f750
            if (r0 == 0) goto L_0x0391
            android.support.v4.ʻ.ˏ r0 = r6.f830
            boolean r0 = r0.m1386(r7)
            if (r0 == 0) goto L_0x0391
            android.util.SparseArray<android.os.Parcelable> r0 = r7.f727
            if (r0 != 0) goto L_0x0391
            r17.m1529(r18)
        L_0x0391:
            r18.m1260()
            r6.m1513(r7, r15)
            android.view.View r0 = r7.f750
            if (r0 == 0) goto L_0x03da
            android.view.ViewGroup r0 = r7.f728
            if (r0 == 0) goto L_0x03da
            android.view.View r0 = r7.f750
            r0.clearAnimation()
            android.view.ViewGroup r0 = r7.f728
            android.view.View r1 = r7.f750
            r0.endViewTransition(r1)
            int r0 = r6.f829
            r1 = 0
            if (r0 <= 0) goto L_0x03cb
            boolean r0 = r6.f838
            if (r0 != 0) goto L_0x03cb
            android.view.View r0 = r7.f750
            int r0 = r0.getVisibility()
            if (r0 != 0) goto L_0x03cb
            float r0 = r7.f748
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x03cb
            r0 = r20
            r2 = r21
            android.support.v4.ʻ.י$ʽ r0 = r6.m1466(r7, r0, r15, r2)
            goto L_0x03cc
        L_0x03cb:
            r0 = r14
        L_0x03cc:
            r7.f748 = r1
            if (r0 == 0) goto L_0x03d3
            r6.m1435(r7, r0, r11)
        L_0x03d3:
            android.view.ViewGroup r0 = r7.f728
            android.view.View r1 = r7.f750
            r0.removeView(r1)
        L_0x03da:
            r7.f728 = r14
            r7.f750 = r14
            r7.f732 = r14
            r7.f747 = r15
        L_0x03e2:
            if (r11 >= r8) goto L_0x0456
            boolean r0 = r6.f838
            if (r0 == 0) goto L_0x0409
            android.view.View r0 = r18.m1291()
            if (r0 == 0) goto L_0x03f9
            android.view.View r0 = r18.m1291()
            r7.m1218(r14)
            r0.clearAnimation()
            goto L_0x0409
        L_0x03f9:
            android.animation.Animator r0 = r18.m1295()
            if (r0 == 0) goto L_0x0409
            android.animation.Animator r0 = r18.m1295()
            r7.m1203(r14)
            r0.cancel()
        L_0x0409:
            android.view.View r0 = r18.m1291()
            if (r0 != 0) goto L_0x0452
            android.animation.Animator r0 = r18.m1295()
            if (r0 == 0) goto L_0x0416
            goto L_0x0452
        L_0x0416:
            boolean r0 = android.support.v4.ʻ.C0246.f810
            if (r0 == 0) goto L_0x042e
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "movefrom CREATED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r13, r0)
        L_0x042e:
            boolean r0 = r7.f722
            if (r0 != 0) goto L_0x0439
            r18.m1270()
            r6.m1515(r7, r15)
            goto L_0x043b
        L_0x0439:
            r7.f723 = r15
        L_0x043b:
            r18.m1267()
            r6.m1519(r7, r15)
            if (r22 != 0) goto L_0x0456
            boolean r0 = r7.f722
            if (r0 != 0) goto L_0x044b
            r17.m1514(r18)
            goto L_0x0456
        L_0x044b:
            r7.f756 = r14
            r7.f762 = r14
            r7.f754 = r14
            goto L_0x0456
        L_0x0452:
            r7.m1238(r11)
            goto L_0x0457
        L_0x0456:
            r8 = r11
        L_0x0457:
            int r0 = r7.f723
            if (r0 == r8) goto L_0x0488
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "moveToState: Fragment state for "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r1 = " not updated inline; "
            r0.append(r1)
            java.lang.String r1 = "expected state "
            r0.append(r1)
            r0.append(r8)
            java.lang.String r1 = " found "
            r0.append(r1)
            int r1 = r7.f723
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.w(r13, r0)
            r7.f723 = r8
        L_0x0488:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.ʻ.C0246.m1474(android.support.v4.ʻ.ˉ, int, int, int, boolean):void");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m1435(final C0222 r4, C0249 r5, int i) {
        final View view = r4.f750;
        r4.m1238(i);
        if (r5.f859 != null) {
            Animation animation = r5.f859;
            r4.m1218(r4.f750);
            animation.setAnimationListener(new C0248(m1433(animation)) {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void
                 arg types: [android.support.v4.ʻ.ˉ, int, int, int, int]
                 candidates:
                  android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.ˈ.ʼ<android.support.v4.ʻ.ˉ>):int
                  android.support.v4.ʻ.י.ʻ(android.content.Context, float, float, float, float):android.support.v4.ʻ.י$ʽ
                  android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י, android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
                  android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
                  android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void */
                public void onAnimationEnd(Animation animation) {
                    super.onAnimationEnd(animation);
                    if (r4.m1291() != null) {
                        r4.m1218((View) null);
                        C0246 r1 = C0246.this;
                        C0222 r2 = r4;
                        r1.m1474(r2, r2.m1297(), 0, 0, false);
                    }
                }
            });
            m1450(view, r5);
            r4.f750.startAnimation(animation);
            return;
        }
        Animator animator = r5.f860;
        r4.m1203(r5.f860);
        final ViewGroup viewGroup = r4.f728;
        if (viewGroup != null) {
            viewGroup.startViewTransition(view);
        }
        animator.addListener(new AnimatorListenerAdapter() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void
             arg types: [android.support.v4.ʻ.ˉ, int, int, int, int]
             candidates:
              android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.ˈ.ʼ<android.support.v4.ʻ.ˉ>):int
              android.support.v4.ʻ.י.ʻ(android.content.Context, float, float, float, float):android.support.v4.ʻ.י$ʽ
              android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י, android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
              android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
              android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void */
            public void onAnimationEnd(Animator animator) {
                ViewGroup viewGroup = viewGroup;
                if (viewGroup != null) {
                    viewGroup.endViewTransition(view);
                }
                if (r4.m1295() != null) {
                    r4.m1203((Animator) null);
                    C0246 r1 = C0246.this;
                    C0222 r2 = r4;
                    r1.m1474(r2, r2.m1297(), 0, 0, false);
                }
            }
        });
        animator.setTarget(r4.f750);
        m1450(r4.f750, r5);
        animator.start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, int, int, int, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.ˈ.ʼ<android.support.v4.ʻ.ˉ>):int
      android.support.v4.ʻ.י.ʻ(android.content.Context, float, float, float, float):android.support.v4.ʻ.י$ʽ
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י, android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1491(C0222 r7) {
        m1474(r7, this.f829, 0, 0, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.view.View, android.os.Bundle, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, android.view.View, android.os.Bundle, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, boolean, int):android.support.v4.ʻ.י$ʽ
      android.support.v4.ʻ.י.ʻ(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.ʻ.ˑ.ʻ(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.view.View, android.os.Bundle, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1500(C0222 r4) {
        if (r4.f745 && !r4.f751) {
            r4.f750 = r4.m1227(r4.m1244(r4.f725), (ViewGroup) null, r4.f725);
            if (r4.f750 != null) {
                r4.f732 = r4.f750;
                r4.f750.setSaveFromParentEnabled(false);
                if (r4.f767) {
                    r4.f750.setVisibility(8);
                }
                r4.m1219(r4.f750, r4.f725);
                m1477(r4, r4.f750, r4.f725, false);
                return;
            }
            r4.f732 = null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m1505(final C0222 r8) {
        if (r8.f750 != null) {
            C0249 r0 = m1466(r8, r8.m1273(), !r8.f767, r8.m1279());
            if (r0 == null || r0.f860 == null) {
                if (r0 != null) {
                    m1450(r8.f750, r0);
                    r8.f750.startAnimation(r0.f859);
                    r0.f859.start();
                }
                r8.f750.setVisibility((!r8.f767 || r8.m1285()) ? 0 : 8);
                if (r8.m1285()) {
                    r8.m1262(false);
                }
            } else {
                r0.f860.setTarget(r8.f750);
                if (!r8.f767) {
                    r8.f750.setVisibility(0);
                } else if (r8.m1285()) {
                    r8.m1262(false);
                } else {
                    final ViewGroup viewGroup = r8.f728;
                    final View view = r8.f750;
                    viewGroup.startViewTransition(view);
                    r0.f860.addListener(new AnimatorListenerAdapter() {
                        public void onAnimationEnd(Animator animator) {
                            viewGroup.endViewTransition(view);
                            animator.removeListener(this);
                            if (r8.f750 != null) {
                                r8.f750.setVisibility(8);
                            }
                        }
                    });
                }
                m1450(r8.f750, r0);
                r0.f860.start();
            }
        }
        if (r8.f741 && r8.f726 && r8.f724) {
            this.f835 = true;
        }
        r8.f759 = false;
        r8.m1221(r8.f767);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, int, int, int, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.ˈ.ʼ<android.support.v4.ʻ.ˉ>):int
      android.support.v4.ʻ.י.ʻ(android.content.Context, float, float, float, float):android.support.v4.ʻ.י$ʽ
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י, android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, boolean, int):android.support.v4.ʻ.י$ʽ
     arg types: [android.support.v4.ʻ.ˉ, int, int, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.view.View, android.os.Bundle, boolean):void
      android.support.v4.ʻ.י.ʻ(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.ʻ.ˑ.ʻ(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, boolean, int):android.support.v4.ʻ.י$ʽ */
    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m1508(C0222 r11) {
        ViewGroup viewGroup;
        int indexOfChild;
        int indexOfChild2;
        if (r11 != null) {
            int i = this.f829;
            if (r11.f743) {
                if (r11.m1223()) {
                    i = Math.min(i, 1);
                } else {
                    i = Math.min(i, 0);
                }
            }
            m1474(r11, i, r11.m1273(), r11.m1279(), false);
            if (r11.f750 != null) {
                C0222 r0 = m1459(r11);
                if (r0 != null && (indexOfChild2 = viewGroup.indexOfChild(r11.f750)) < (indexOfChild = (viewGroup = r11.f728).indexOfChild(r0.f750))) {
                    viewGroup.removeViewAt(indexOfChild2);
                    viewGroup.addView(r11.f750, indexOfChild);
                }
                if (r11.f746 && r11.f728 != null) {
                    if (r11.f748 > 0.0f) {
                        r11.f750.setAlpha(r11.f748);
                    }
                    r11.f748 = 0.0f;
                    r11.f746 = false;
                    C0249 r02 = m1466(r11, r11.m1273(), true, r11.m1279());
                    if (r02 != null) {
                        m1450(r11.f750, r02);
                        if (r02.f859 != null) {
                            r11.f750.startAnimation(r02.f859);
                        } else {
                            r02.f860.setTarget(r11.f750);
                            r02.f860.start();
                        }
                    }
                }
            }
            if (r11.f759) {
                m1505(r11);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1469(int i, boolean z) {
        C0237 r5;
        if (this.f830 == null && i != 0) {
            throw new IllegalStateException("No activity");
        } else if (z || i != this.f829) {
            this.f829 = i;
            if (this.f822 != null) {
                int size = this.f821.size();
                boolean z2 = false;
                for (int i2 = 0; i2 < size; i2++) {
                    C0222 r2 = this.f821.get(i2);
                    m1508(r2);
                    if (r2.f740 != null) {
                        z2 |= r2.f740.m1637();
                    }
                }
                int size2 = this.f822.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    C0222 valueAt = this.f822.valueAt(i3);
                    if (valueAt != null && ((valueAt.f743 || valueAt.f753) && !valueAt.f746)) {
                        m1508(valueAt);
                        if (valueAt.f740 != null) {
                            z2 |= valueAt.f740.m1637();
                        }
                    }
                }
                if (!z2) {
                    m1511();
                }
                if (this.f835 && (r5 = this.f830) != null && this.f829 == 5) {
                    r5.m1390();
                    this.f835 = false;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m1511() {
        if (this.f822 != null) {
            for (int i = 0; i < this.f822.size(); i++) {
                C0222 valueAt = this.f822.valueAt(i);
                if (valueAt != null) {
                    m1473(valueAt);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m1512(C0222 r3) {
        if (r3.f729 < 0) {
            int i = this.f820;
            this.f820 = i + 1;
            r3.m1201(i, this.f832);
            if (this.f822 == null) {
                this.f822 = new SparseArray<>();
            }
            this.f822.put(r3.f729, r3);
            if (f810) {
                Log.v("FragmentManager", "Allocated fragment index " + r3);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m1514(C0222 r4) {
        if (r4.f729 >= 0) {
            if (f810) {
                Log.v("FragmentManager", "Freeing fragment index " + r4);
            }
            this.f822.put(r4.f729, null);
            this.f830.m1382(r4.f731);
            r4.m1286();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1478(C0222 r4, boolean z) {
        if (f810) {
            Log.v("FragmentManager", "add: " + r4);
        }
        m1512(r4);
        if (r4.f753) {
            return;
        }
        if (!this.f821.contains(r4)) {
            synchronized (this.f821) {
                this.f821.add(r4);
            }
            r4.f741 = true;
            r4.f743 = false;
            if (r4.f750 == null) {
                r4.f759 = false;
            }
            if (r4.f726 && r4.f724) {
                this.f835 = true;
            }
            if (z) {
                m1491(r4);
                return;
            }
            return;
        }
        throw new IllegalStateException("Fragment already added: " + r4);
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public void m1518(C0222 r4) {
        if (f810) {
            Log.v("FragmentManager", "remove: " + r4 + " nesting=" + r4.f752);
        }
        boolean z = !r4.m1223();
        if (!r4.f753 || z) {
            synchronized (this.f821) {
                this.f821.remove(r4);
            }
            if (r4.f726 && r4.f724) {
                this.f835 = true;
            }
            r4.f741 = false;
            r4.f743 = true;
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public void m1521(C0222 r3) {
        if (f810) {
            Log.v("FragmentManager", "hide: " + r3);
        }
        if (!r3.f767) {
            r3.f767 = true;
            r3.f759 = true ^ r3.f759;
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public void m1523(C0222 r3) {
        if (f810) {
            Log.v("FragmentManager", "show: " + r3);
        }
        if (r3.f767) {
            r3.f767 = false;
            r3.f759 = !r3.f759;
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public void m1525(C0222 r4) {
        if (f810) {
            Log.v("FragmentManager", "detach: " + r4);
        }
        if (!r4.f753) {
            r4.f753 = true;
            if (r4.f741) {
                if (f810) {
                    Log.v("FragmentManager", "remove from detach: " + r4);
                }
                synchronized (this.f821) {
                    this.f821.remove(r4);
                }
                if (r4.f726 && r4.f724) {
                    this.f835 = true;
                }
                r4.f741 = false;
            }
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public void m1527(C0222 r4) {
        if (f810) {
            Log.v("FragmentManager", "attach: " + r4);
        }
        if (r4.f753) {
            r4.f753 = false;
            if (r4.f741) {
                return;
            }
            if (!this.f821.contains(r4)) {
                if (f810) {
                    Log.v("FragmentManager", "add from attach: " + r4);
                }
                synchronized (this.f821) {
                    this.f821.add(r4);
                }
                r4.f741 = true;
                if (r4.f726 && r4.f724) {
                    this.f835 = true;
                    return;
                }
                return;
            }
            throw new IllegalStateException("Fragment already added: " + r4);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0222 m1488(int i) {
        for (int size = this.f821.size() - 1; size >= 0; size--) {
            C0222 r1 = this.f821.get(size);
            if (r1 != null && r1.f764 == i) {
                return r1;
            }
        }
        SparseArray<C0222> sparseArray = this.f822;
        if (sparseArray == null) {
            return null;
        }
        for (int size2 = sparseArray.size() - 1; size2 >= 0; size2--) {
            C0222 valueAt = this.f822.valueAt(size2);
            if (valueAt != null && valueAt.f764 == i) {
                return valueAt;
            }
        }
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0222 m1465(String str) {
        if (str != null) {
            for (int size = this.f821.size() - 1; size >= 0; size--) {
                C0222 r1 = this.f821.get(size);
                if (r1 != null && str.equals(r1.f766)) {
                    return r1;
                }
            }
        }
        SparseArray<C0222> sparseArray = this.f822;
        if (sparseArray == null || str == null) {
            return null;
        }
        for (int size2 = sparseArray.size() - 1; size2 >= 0; size2--) {
            C0222 valueAt = this.f822.valueAt(size2);
            if (valueAt != null && str.equals(valueAt.f766)) {
                return valueAt;
            }
        }
        return null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0222 m1489(String str) {
        C0222 r1;
        SparseArray<C0222> sparseArray = this.f822;
        if (sparseArray == null || str == null) {
            return null;
        }
        for (int size = sparseArray.size() - 1; size >= 0; size--) {
            C0222 valueAt = this.f822.valueAt(size);
            if (valueAt != null && (r1 = valueAt.m1195(str)) != null) {
                return r1;
            }
        }
        return null;
    }

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private void m1462() {
        if (this.f837) {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        } else if (this.f839 != null) {
            throw new IllegalStateException("Can not perform this action inside of " + this.f839);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m1510() {
        return this.f837;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1480(C0252 r2, boolean z) {
        if (!z) {
            m1462();
        }
        synchronized (this) {
            if (!this.f838) {
                if (this.f830 != null) {
                    if (this.f817 == null) {
                        this.f817 = new ArrayList<>();
                    }
                    this.f817.add(r2);
                    m1460();
                    return;
                }
            }
            if (!z) {
                throw new IllegalStateException("Activity has been destroyed");
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public void m1460() {
        synchronized (this) {
            boolean z = false;
            boolean z2 = this.f836 != null && !this.f836.isEmpty();
            if (this.f817 != null && this.f817.size() == 1) {
                z = true;
            }
            if (z2 || z) {
                this.f830.m1395().removeCallbacks(this.f819);
                this.f830.m1395().post(this.f819);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m1463(C0201 r5) {
        synchronized (this) {
            if (this.f827 != null) {
                if (this.f827.size() > 0) {
                    int intValue = this.f827.remove(this.f827.size() - 1).intValue();
                    if (f810) {
                        Log.v("FragmentManager", "Adding back stack index " + intValue + " with " + r5);
                    }
                    this.f826.set(intValue, r5);
                    return intValue;
                }
            }
            if (this.f826 == null) {
                this.f826 = new ArrayList<>();
            }
            int size = this.f826.size();
            if (f810) {
                Log.v("FragmentManager", "Setting back stack index " + size + " to " + r5);
            }
            this.f826.add(r5);
            return size;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1468(int i, C0201 r6) {
        synchronized (this) {
            if (this.f826 == null) {
                this.f826 = new ArrayList<>();
            }
            int size = this.f826.size();
            if (i < size) {
                if (f810) {
                    Log.v("FragmentManager", "Setting back stack index " + i + " to " + r6);
                }
                this.f826.set(i, r6);
            } else {
                while (size < i) {
                    this.f826.add(null);
                    if (this.f827 == null) {
                        this.f827 = new ArrayList<>();
                    }
                    if (f810) {
                        Log.v("FragmentManager", "Adding available back stack index " + size);
                    }
                    this.f827.add(Integer.valueOf(size));
                    size++;
                }
                if (f810) {
                    Log.v("FragmentManager", "Adding back stack index " + i + " with " + r6);
                }
                this.f826.add(r6);
            }
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1499(int i) {
        synchronized (this) {
            this.f826.set(i, null);
            if (this.f827 == null) {
                this.f827 = new ArrayList<>();
            }
            if (f810) {
                Log.v("FragmentManager", "Freeing back stack index " + i);
            }
            this.f827.add(Integer.valueOf(i));
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m1454(boolean z) {
        if (this.f818) {
            throw new IllegalStateException("FragmentManager is already executing transactions");
        } else if (Looper.myLooper() == this.f830.m1395().getLooper()) {
            if (!z) {
                m1462();
            }
            if (this.f841 == null) {
                this.f841 = new ArrayList<>();
                this.f842 = new ArrayList<>();
            }
            this.f818 = true;
            try {
                m1441((ArrayList<C0201>) null, (ArrayList<Boolean>) null);
            } finally {
                this.f818 = false;
            }
        } else {
            throw new IllegalStateException("Must be called from main thread of fragment host");
        }
    }

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private void m1461() {
        this.f818 = false;
        this.f842.clear();
        this.f841.clear();
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m1516() {
        m1454(true);
        boolean z = false;
        while (m1455(this.f841, this.f842)) {
            this.f818 = true;
            try {
                m1451(this.f841, this.f842);
                m1461();
                z = true;
            } catch (Throwable th) {
                m1461();
                throw th;
            }
        }
        m1517();
        m1453();
        return z;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m1441(ArrayList<C0201> arrayList, ArrayList<Boolean> arrayList2) {
        int indexOf;
        int indexOf2;
        ArrayList<C0253> arrayList3 = this.f836;
        int size = arrayList3 == null ? 0 : arrayList3.size();
        int i = 0;
        while (i < size) {
            C0253 r3 = this.f836.get(i);
            if (arrayList != null && !r3.f863 && (indexOf2 = arrayList.indexOf(r3.f864)) != -1 && arrayList2.get(indexOf2).booleanValue()) {
                r3.m1551();
            } else if (r3.m1549() || (arrayList != null && r3.f864.m1154(arrayList, 0, arrayList.size()))) {
                this.f836.remove(i);
                i--;
                size--;
                if (arrayList == null || r3.f863 || (indexOf = arrayList.indexOf(r3.f864)) == -1 || !arrayList2.get(indexOf).booleanValue()) {
                    r3.m1550();
                } else {
                    r3.m1551();
                }
            }
            i++;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m1451(ArrayList<C0201> arrayList, ArrayList<Boolean> arrayList2) {
        if (arrayList != null && !arrayList.isEmpty()) {
            if (arrayList2 == null || arrayList.size() != arrayList2.size()) {
                throw new IllegalStateException("Internal error with the back stack records");
            }
            m1441(arrayList, arrayList2);
            int size = arrayList.size();
            int i = 0;
            int i2 = 0;
            while (i < size) {
                if (!arrayList.get(i).f675) {
                    if (i2 != i) {
                        m1442(arrayList, arrayList2, i2, i);
                    }
                    i2 = i + 1;
                    if (arrayList2.get(i).booleanValue()) {
                        while (i2 < size && arrayList2.get(i2).booleanValue() && !arrayList.get(i2).f675) {
                            i2++;
                        }
                    }
                    m1442(arrayList, arrayList2, i, i2);
                    i = i2 - 1;
                }
                i++;
            }
            if (i2 != size) {
                m1442(arrayList, arrayList2, i2, size);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.ʻ.י.ʻ(android.view.View, android.support.v4.ʻ.י$ʽ):boolean
      android.support.v4.ʻ.י.ʻ(android.os.Bundle, java.lang.String):android.support.v4.ʻ.ˉ
      android.support.v4.ʻ.י.ʻ(int, android.support.v4.ʻ.ʽ):void
      android.support.v4.ʻ.י.ʻ(android.os.Parcelable, android.support.v4.ʻ.ـ):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, boolean):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י$ˆ, boolean):void
      android.support.v4.ʻ.י.ʻ(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.ʻ.י.ʻ(int, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m1442(ArrayList<C0201> arrayList, ArrayList<Boolean> arrayList2, int i, int i2) {
        int i3;
        ArrayList<C0201> arrayList3 = arrayList;
        ArrayList<Boolean> arrayList4 = arrayList2;
        int i4 = i;
        int i5 = i2;
        boolean z = arrayList3.get(i4).f675;
        ArrayList<C0222> arrayList5 = this.f843;
        if (arrayList5 == null) {
            this.f843 = new ArrayList<>();
        } else {
            arrayList5.clear();
        }
        this.f843.addAll(this.f821);
        C0222 r2 = m1542();
        boolean z2 = false;
        for (int i6 = i4; i6 < i5; i6++) {
            C0201 r3 = arrayList3.get(i6);
            if (!arrayList4.get(i6).booleanValue()) {
                r2 = r3.m1144(this.f843, r2);
            } else {
                r2 = r3.m1157(this.f843, r2);
            }
            z2 = z2 || r3.f664;
        }
        this.f843.clear();
        if (!z) {
            C0263.m1586(this, arrayList, arrayList2, i, i2, false);
        }
        m1452(arrayList, arrayList2, i, i2);
        if (z) {
            C0332 r14 = new C0332();
            m1449(r14);
            int r0 = m1430(arrayList, arrayList2, i, i2, r14);
            m1439(r14);
            i3 = r0;
        } else {
            i3 = i5;
        }
        if (i3 != i4 && z) {
            C0263.m1586(this, arrayList, arrayList2, i, i3, true);
            m1469(this.f829, true);
        }
        while (i4 < i5) {
            C0201 r02 = arrayList3.get(i4);
            if (arrayList4.get(i4).booleanValue() && r02.f668 >= 0) {
                m1499(r02.f668);
                r02.f668 = -1;
            }
            r02.m1148();
            i4++;
        }
        if (z2) {
            m1520();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m1439(C0332<C0222> r6) {
        int size = r6.size();
        for (int i = 0; i < size; i++) {
            C0222 r2 = r6.m1901(i);
            if (!r2.f741) {
                View r3 = r2.m1268();
                r2.f748 = r3.getAlpha();
                r3.setAlpha(0.0f);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m1430(ArrayList<C0201> arrayList, ArrayList<Boolean> arrayList2, int i, int i2, C0332<C0222> r12) {
        int i3 = i2;
        for (int i4 = i2 - 1; i4 >= i; i4--) {
            C0201 r2 = arrayList.get(i4);
            boolean booleanValue = arrayList2.get(i4).booleanValue();
            if (r2.m1162() && !r2.m1154(arrayList, i4 + 1, i2)) {
                if (this.f836 == null) {
                    this.f836 = new ArrayList<>();
                }
                C0253 r4 = new C0253(r2, booleanValue);
                this.f836.add(r4);
                r2.m1151(r4);
                if (booleanValue) {
                    r2.m1161();
                } else {
                    r2.m1159(false);
                }
                i3--;
                if (i4 != i3) {
                    arrayList.remove(i4);
                    arrayList.add(i3, r2);
                }
                m1449(r12);
            }
        }
        return i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.ʻ.י.ʻ(android.view.View, android.support.v4.ʻ.י$ʽ):boolean
      android.support.v4.ʻ.י.ʻ(android.os.Bundle, java.lang.String):android.support.v4.ʻ.ˉ
      android.support.v4.ʻ.י.ʻ(int, android.support.v4.ʻ.ʽ):void
      android.support.v4.ʻ.י.ʻ(android.os.Parcelable, android.support.v4.ʻ.ـ):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, boolean):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י$ˆ, boolean):void
      android.support.v4.ʻ.י.ʻ(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.ʻ.י.ʻ(int, boolean):void */
    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1434(C0201 r8, boolean z, boolean z2, boolean z3) {
        if (z) {
            r8.m1159(z3);
        } else {
            r8.m1161();
        }
        ArrayList arrayList = new ArrayList(1);
        ArrayList arrayList2 = new ArrayList(1);
        arrayList.add(r8);
        arrayList2.add(Boolean.valueOf(z));
        if (z2) {
            C0263.m1586(this, arrayList, arrayList2, 0, 1, true);
        }
        if (z3) {
            m1469(this.f829, true);
        }
        SparseArray<C0222> sparseArray = this.f822;
        if (sparseArray != null) {
            int size = sparseArray.size();
            for (int i = 0; i < size; i++) {
                C0222 valueAt = this.f822.valueAt(i);
                if (valueAt != null && valueAt.f750 != null && valueAt.f746 && r8.m1160(valueAt.f765)) {
                    if (valueAt.f748 > 0.0f) {
                        valueAt.f750.setAlpha(valueAt.f748);
                    }
                    if (z3) {
                        valueAt.f748 = 0.0f;
                    } else {
                        valueAt.f748 = -1.0f;
                        valueAt.f746 = false;
                    }
                }
            }
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private C0222 m1459(C0222 r5) {
        ViewGroup viewGroup = r5.f728;
        View view = r5.f750;
        if (!(viewGroup == null || view == null)) {
            for (int indexOf = this.f821.indexOf(r5) - 1; indexOf >= 0; indexOf--) {
                C0222 r1 = this.f821.get(indexOf);
                if (r1.f728 == viewGroup && r1.f750 != null) {
                    return r1;
                }
            }
        }
        return null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static void m1452(ArrayList<C0201> arrayList, ArrayList<Boolean> arrayList2, int i, int i2) {
        while (i < i2) {
            C0201 r0 = arrayList.get(i);
            boolean z = true;
            if (arrayList2.get(i).booleanValue()) {
                r0.m1149(-1);
                if (i != i2 - 1) {
                    z = false;
                }
                r0.m1159(z);
            } else {
                r0.m1149(1);
                r0.m1161();
            }
            i++;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, int, int, int, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.ˈ.ʼ<android.support.v4.ʻ.ˉ>):int
      android.support.v4.ʻ.י.ʻ(android.content.Context, float, float, float, float):android.support.v4.ʻ.י$ʽ
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י, android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void */
    /* renamed from: ʼ  reason: contains not printable characters */
    private void m1449(C0332<C0222> r11) {
        int i = this.f829;
        if (i >= 1) {
            int min = Math.min(i, 4);
            int size = this.f821.size();
            for (int i2 = 0; i2 < size; i2++) {
                C0222 r9 = this.f821.get(i2);
                if (r9.f723 < min) {
                    m1474(r9, min, r9.m1276(), r9.m1273(), false);
                    if (r9.f750 != null && !r9.f767 && r9.f746) {
                        r11.add(r9);
                    }
                }
            }
        }
    }

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private void m1447() {
        if (this.f836 != null) {
            while (!this.f836.isEmpty()) {
                this.f836.remove(0).m1550();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, int, int, int, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.ˈ.ʼ<android.support.v4.ʻ.ˉ>):int
      android.support.v4.ʻ.י.ʻ(android.content.Context, float, float, float, float):android.support.v4.ʻ.י$ʽ
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י, android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void */
    /* renamed from: ʽʽ  reason: contains not printable characters */
    private void m1456() {
        SparseArray<C0222> sparseArray = this.f822;
        int size = sparseArray == null ? 0 : sparseArray.size();
        for (int i = 0; i < size; i++) {
            C0222 valueAt = this.f822.valueAt(i);
            if (valueAt != null) {
                if (valueAt.m1291() != null) {
                    int r5 = valueAt.m1297();
                    View r2 = valueAt.m1291();
                    valueAt.m1218((View) null);
                    Animation animation = r2.getAnimation();
                    if (animation != null) {
                        animation.cancel();
                        r2.clearAnimation();
                    }
                    m1474(valueAt, r5, 0, 0, false);
                } else if (valueAt.m1295() != null) {
                    valueAt.m1295().end();
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003b, code lost:
        return false;
     */
    /* renamed from: ʽ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m1455(java.util.ArrayList<android.support.v4.ʻ.C0201> r5, java.util.ArrayList<java.lang.Boolean> r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            java.util.ArrayList<android.support.v4.ʻ.י$ˆ> r0 = r4.f817     // Catch:{ all -> 0x003c }
            r1 = 0
            if (r0 == 0) goto L_0x003a
            java.util.ArrayList<android.support.v4.ʻ.י$ˆ> r0 = r4.f817     // Catch:{ all -> 0x003c }
            int r0 = r0.size()     // Catch:{ all -> 0x003c }
            if (r0 != 0) goto L_0x000f
            goto L_0x003a
        L_0x000f:
            java.util.ArrayList<android.support.v4.ʻ.י$ˆ> r0 = r4.f817     // Catch:{ all -> 0x003c }
            int r0 = r0.size()     // Catch:{ all -> 0x003c }
            r2 = 0
        L_0x0016:
            if (r1 >= r0) goto L_0x0028
            java.util.ArrayList<android.support.v4.ʻ.י$ˆ> r3 = r4.f817     // Catch:{ all -> 0x003c }
            java.lang.Object r3 = r3.get(r1)     // Catch:{ all -> 0x003c }
            android.support.v4.ʻ.י$ˆ r3 = (android.support.v4.ʻ.C0246.C0252) r3     // Catch:{ all -> 0x003c }
            boolean r3 = r3.m1544(r5, r6)     // Catch:{ all -> 0x003c }
            r2 = r2 | r3
            int r1 = r1 + 1
            goto L_0x0016
        L_0x0028:
            java.util.ArrayList<android.support.v4.ʻ.י$ˆ> r5 = r4.f817     // Catch:{ all -> 0x003c }
            r5.clear()     // Catch:{ all -> 0x003c }
            android.support.v4.ʻ.ˏ r5 = r4.f830     // Catch:{ all -> 0x003c }
            android.os.Handler r5 = r5.m1395()     // Catch:{ all -> 0x003c }
            java.lang.Runnable r6 = r4.f819     // Catch:{ all -> 0x003c }
            r5.removeCallbacks(r6)     // Catch:{ all -> 0x003c }
            monitor-exit(r4)     // Catch:{ all -> 0x003c }
            return r2
        L_0x003a:
            monitor-exit(r4)     // Catch:{ all -> 0x003c }
            return r1
        L_0x003c:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x003c }
            goto L_0x0040
        L_0x003f:
            throw r5
        L_0x0040:
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.ʻ.C0246.m1455(java.util.ArrayList, java.util.ArrayList):boolean");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public void m1517() {
        if (this.f840) {
            boolean z = false;
            for (int i = 0; i < this.f822.size(); i++) {
                C0222 valueAt = this.f822.valueAt(i);
                if (!(valueAt == null || valueAt.f740 == null)) {
                    z |= valueAt.f740.m1637();
                }
            }
            if (!z) {
                this.f840 = false;
                m1511();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public void m1520() {
        if (this.f828 != null) {
            for (int i = 0; i < this.f828.size(); i++) {
                this.f828.get(i).m1423();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1490(C0201 r2) {
        if (this.f824 == null) {
            this.f824 = new ArrayList<>();
        }
        this.f824.add(r2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1487(ArrayList<C0201> arrayList, ArrayList<Boolean> arrayList2, String str, int i, int i2) {
        int i3;
        ArrayList<C0201> arrayList3 = this.f824;
        if (arrayList3 == null) {
            return false;
        }
        if (str == null && i < 0 && (i2 & 1) == 0) {
            int size = arrayList3.size() - 1;
            if (size < 0) {
                return false;
            }
            arrayList.add(this.f824.remove(size));
            arrayList2.add(true);
        } else {
            if (str != null || i >= 0) {
                i3 = this.f824.size() - 1;
                while (i3 >= 0) {
                    C0201 r3 = this.f824.get(i3);
                    if ((str != null && str.equals(r3.m1163())) || (i >= 0 && i == r3.f668)) {
                        break;
                    }
                    i3--;
                }
                if (i3 < 0) {
                    return false;
                }
                if ((i2 & 1) != 0) {
                    while (true) {
                        i3--;
                        if (i3 < 0) {
                            break;
                        }
                        C0201 r10 = this.f824.get(i3);
                        if ((str == null || !str.equals(r10.m1163())) && (i < 0 || i != r10.f668)) {
                            break;
                        }
                    }
                }
            } else {
                i3 = -1;
            }
            if (i3 == this.f824.size() - 1) {
                return false;
            }
            for (int size2 = this.f824.size() - 1; size2 > i3; size2--) {
                arrayList.add(this.f824.remove(size2));
                arrayList2.add(true);
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public C0254 m1522() {
        m1438(this.f816);
        return this.f816;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m1438(C0254 r3) {
        if (r3 != null) {
            List<C0222> r0 = r3.m1552();
            if (r0 != null) {
                for (C0222 r1 : r0) {
                    r1.f722 = true;
                }
            }
            List<C0254> r32 = r3.m1553();
            if (r32 != null) {
                for (C0254 r02 : r32) {
                    m1438(r02);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public void m1524() {
        ArrayList arrayList;
        ArrayList arrayList2;
        C0254 r5;
        if (this.f822 != null) {
            arrayList2 = null;
            arrayList = null;
            for (int i = 0; i < this.f822.size(); i++) {
                C0222 valueAt = this.f822.valueAt(i);
                if (valueAt != null) {
                    if (valueAt.f755) {
                        if (arrayList2 == null) {
                            arrayList2 = new ArrayList();
                        }
                        arrayList2.add(valueAt);
                        valueAt.f737 = valueAt.f735 != null ? valueAt.f735.f729 : -1;
                        if (f810) {
                            Log.v("FragmentManager", "retainNonConfig: keeping retained " + valueAt);
                        }
                    }
                    if (valueAt.f758 != null) {
                        valueAt.f758.m1524();
                        r5 = valueAt.f758.f816;
                    } else {
                        r5 = valueAt.f760;
                    }
                    if (arrayList == null && r5 != null) {
                        arrayList = new ArrayList(this.f822.size());
                        for (int i2 = 0; i2 < i; i2++) {
                            arrayList.add(null);
                        }
                    }
                    if (arrayList != null) {
                        arrayList.add(r5);
                    }
                }
            }
        } else {
            arrayList2 = null;
            arrayList = null;
        }
        if (arrayList2 == null && arrayList == null) {
            this.f816 = null;
        } else {
            this.f816 = new C0254(arrayList2, arrayList);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m1529(C0222 r3) {
        if (r3.f732 != null) {
            SparseArray<Parcelable> sparseArray = this.f834;
            if (sparseArray == null) {
                this.f834 = new SparseArray<>();
            } else {
                sparseArray.clear();
            }
            r3.f732.saveHierarchyState(this.f834);
            if (this.f834.size() > 0) {
                r3.f727 = this.f834;
                this.f834 = null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: י  reason: contains not printable characters */
    public Bundle m1530(C0222 r4) {
        Bundle bundle;
        if (this.f844 == null) {
            this.f844 = new Bundle();
        }
        r4.m1278(this.f844);
        m1506(r4, this.f844, false);
        if (!this.f844.isEmpty()) {
            bundle = this.f844;
            this.f844 = null;
        } else {
            bundle = null;
        }
        if (r4.f750 != null) {
            m1529(r4);
        }
        if (r4.f727 != null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putSparseParcelableArray("android:view_state", r4.f727);
        }
        if (!r4.f734) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean("android:user_visible_hint", r4.f734);
        }
        return bundle;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public Parcelable m1526() {
        int[] iArr;
        int size;
        m1447();
        m1456();
        m1516();
        this.f837 = true;
        C0205[] r1 = null;
        this.f816 = null;
        SparseArray<C0222> sparseArray = this.f822;
        if (sparseArray == null || sparseArray.size() <= 0) {
            return null;
        }
        int size2 = this.f822.size();
        C0257[] r3 = new C0257[size2];
        boolean z = false;
        for (int i = 0; i < size2; i++) {
            C0222 valueAt = this.f822.valueAt(i);
            if (valueAt != null) {
                if (valueAt.f729 < 0) {
                    m1440(new IllegalStateException("Failure saving state: active " + valueAt + " has cleared index: " + valueAt.f729));
                }
                C0257 r6 = new C0257(valueAt);
                r3[i] = r6;
                if (valueAt.f723 <= 0 || r6.f886 != null) {
                    r6.f886 = valueAt.f725;
                } else {
                    r6.f886 = m1530(valueAt);
                    if (valueAt.f735 != null) {
                        if (valueAt.f735.f729 < 0) {
                            m1440(new IllegalStateException("Failure saving state: " + valueAt + " has target not in fragment manager: " + valueAt.f735));
                        }
                        if (r6.f886 == null) {
                            r6.f886 = new Bundle();
                        }
                        m1471(r6.f886, "android:target_state", valueAt.f735);
                        if (valueAt.f739 != 0) {
                            r6.f886.putInt("android:target_req_state", valueAt.f739);
                        }
                    }
                }
                if (f810) {
                    Log.v("FragmentManager", "Saved state of " + valueAt + ": " + r6.f886);
                }
                z = true;
            }
        }
        if (!z) {
            if (f810) {
                Log.v("FragmentManager", "saveAllState: no fragments!");
            }
            return null;
        }
        int size3 = this.f821.size();
        if (size3 > 0) {
            iArr = new int[size3];
            for (int i2 = 0; i2 < size3; i2++) {
                iArr[i2] = this.f821.get(i2).f729;
                if (iArr[i2] < 0) {
                    m1440(new IllegalStateException("Failure saving state: active " + this.f821.get(i2) + " has cleared index: " + iArr[i2]));
                }
                if (f810) {
                    Log.v("FragmentManager", "saveAllState: adding fragment #" + i2 + ": " + this.f821.get(i2));
                }
            }
        } else {
            iArr = null;
        }
        ArrayList<C0201> arrayList = this.f824;
        if (arrayList != null && (size = arrayList.size()) > 0) {
            r1 = new C0205[size];
            for (int i3 = 0; i3 < size; i3++) {
                r1[i3] = new C0205(this.f824.get(i3));
                if (f810) {
                    Log.v("FragmentManager", "saveAllState: adding back stack #" + i3 + ": " + this.f824.get(i3));
                }
            }
        }
        C0256 r0 = new C0256();
        r0.f871 = r3;
        r0.f872 = iArr;
        r0.f873 = r1;
        C0222 r12 = this.f833;
        if (r12 != null) {
            r0.f874 = r12.f729;
        }
        r0.f875 = this.f820;
        m1524();
        return r0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.ʽ.ʻ(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      android.support.v4.ʻ.ʽ.ʻ(int, android.support.v4.ʻ.ˉ, java.lang.String):android.support.v4.ʻ.ᴵ
      android.support.v4.ʻ.ʽ.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, int, int):boolean
      android.support.v4.ʻ.ʽ.ʻ(java.lang.String, java.io.PrintWriter, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1472(Parcelable parcelable, C0254 r13) {
        List<C0254> list;
        if (parcelable != null) {
            C0256 r12 = (C0256) parcelable;
            if (r12.f871 != null) {
                if (r13 != null) {
                    List<C0222> r2 = r13.m1552();
                    list = r13.m1553();
                    int size = r2 != null ? r2.size() : 0;
                    for (int i = 0; i < size; i++) {
                        C0222 r6 = r2.get(i);
                        if (f810) {
                            Log.v("FragmentManager", "restoreAllState: re-attaching retained " + r6);
                        }
                        int i2 = 0;
                        while (i2 < r12.f871.length && r12.f871[i2].f877 != r6.f729) {
                            i2++;
                        }
                        if (i2 == r12.f871.length) {
                            m1440(new IllegalStateException("Could not find active fragment with index " + r6.f729));
                        }
                        C0257 r7 = r12.f871[i2];
                        r7.f887 = r6;
                        r6.f727 = null;
                        r6.f752 = 0;
                        r6.f747 = false;
                        r6.f741 = false;
                        r6.f735 = null;
                        if (r7.f886 != null) {
                            r7.f886.setClassLoader(this.f830.m1394().getClassLoader());
                            r6.f727 = r7.f886.getSparseParcelableArray("android:view_state");
                            r6.f725 = r7.f886;
                        }
                    }
                } else {
                    list = null;
                }
                this.f822 = new SparseArray<>(r12.f871.length);
                int i3 = 0;
                while (i3 < r12.f871.length) {
                    C0257 r4 = r12.f871[i3];
                    if (r4 != null) {
                        C0222 r5 = r4.m1558(this.f830, this.f831, this.f832, (list == null || i3 >= list.size()) ? null : (C0254) list.get(i3));
                        if (f810) {
                            Log.v("FragmentManager", "restoreAllState: active #" + i3 + ": " + r5);
                        }
                        this.f822.put(r5.f729, r5);
                        r4.f887 = null;
                    }
                    i3++;
                }
                if (r13 != null) {
                    List<C0222> r132 = r13.m1552();
                    int size2 = r132 != null ? r132.size() : 0;
                    for (int i4 = 0; i4 < size2; i4++) {
                        C0222 r42 = r132.get(i4);
                        if (r42.f737 >= 0) {
                            r42.f735 = this.f822.get(r42.f737);
                            if (r42.f735 == null) {
                                Log.w("FragmentManager", "Re-attaching retained fragment " + r42 + " target no longer exists: " + r42.f737);
                            }
                        }
                    }
                }
                this.f821.clear();
                if (r12.f872 != null) {
                    int i5 = 0;
                    while (i5 < r12.f872.length) {
                        C0222 r22 = this.f822.get(r12.f872[i5]);
                        if (r22 == null) {
                            m1440(new IllegalStateException("No instantiated fragment for index #" + r12.f872[i5]));
                        }
                        r22.f741 = true;
                        if (f810) {
                            Log.v("FragmentManager", "restoreAllState: added #" + i5 + ": " + r22);
                        }
                        if (!this.f821.contains(r22)) {
                            synchronized (this.f821) {
                                this.f821.add(r22);
                            }
                            i5++;
                        } else {
                            throw new IllegalStateException("Already added!");
                        }
                    }
                }
                if (r12.f873 != null) {
                    this.f824 = new ArrayList<>(r12.f873.length);
                    for (int i6 = 0; i6 < r12.f873.length; i6++) {
                        C0201 r0 = r12.f873[i6].m1166(this);
                        if (f810) {
                            Log.v("FragmentManager", "restoreAllState: back stack #" + i6 + " (index " + r0.f668 + "): " + r0);
                            PrintWriter printWriter = new PrintWriter(new C0335("FragmentManager"));
                            r0.m1153("  ", printWriter, false);
                            printWriter.close();
                        }
                        this.f824.add(r0);
                        if (r0.f668 >= 0) {
                            m1468(r0.f668, r0);
                        }
                    }
                } else {
                    this.f824 = null;
                }
                if (r12.f874 >= 0) {
                    this.f833 = this.f822.get(r12.f874);
                }
                this.f820 = r12.f875;
            }
        }
    }

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private void m1453() {
        SparseArray<C0222> sparseArray = this.f822;
        if (sparseArray != null) {
            for (int size = sparseArray.size() - 1; size >= 0; size--) {
                if (this.f822.valueAt(size) == null) {
                    SparseArray<C0222> sparseArray2 = this.f822;
                    sparseArray2.delete(sparseArray2.keyAt(size));
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1479(C0237 r2, C0232 r3, C0222 r4) {
        if (this.f830 == null) {
            this.f830 = r2;
            this.f831 = r3;
            this.f832 = r4;
            return;
        }
        throw new IllegalStateException("Already attached");
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m1528() {
        this.f816 = null;
        this.f837 = false;
        int size = this.f821.size();
        for (int i = 0; i < size; i++) {
            C0222 r2 = this.f821.get(i);
            if (r2 != null) {
                r2.m1252();
            }
        }
    }

    /* renamed from: י  reason: contains not printable characters */
    public void m1531() {
        this.f837 = false;
        m1458(1);
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public void m1532() {
        this.f837 = false;
        m1458(2);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m1534() {
        this.f837 = false;
        m1458(4);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m1535() {
        this.f837 = false;
        m1458(5);
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public void m1536() {
        m1458(4);
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public void m1537() {
        this.f837 = true;
        m1458(3);
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    public void m1538() {
        m1458(2);
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    public void m1539() {
        m1458(1);
    }

    /* renamed from: ⁱ  reason: contains not printable characters */
    public void m1540() {
        this.f838 = true;
        m1516();
        m1458(0);
        this.f830 = null;
        this.f831 = null;
        this.f832 = null;
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.ʻ.י.ʻ(android.view.View, android.support.v4.ʻ.י$ʽ):boolean
      android.support.v4.ʻ.י.ʻ(android.os.Bundle, java.lang.String):android.support.v4.ʻ.ˉ
      android.support.v4.ʻ.י.ʻ(int, android.support.v4.ʻ.ʽ):void
      android.support.v4.ʻ.י.ʻ(android.os.Parcelable, android.support.v4.ʻ.ـ):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, boolean):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י$ˆ, boolean):void
      android.support.v4.ʻ.י.ʻ(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.ʻ.י.ʻ(int, boolean):void */
    /* renamed from: ʿ  reason: contains not printable characters */
    private void m1458(int i) {
        try {
            this.f818 = true;
            m1469(i, false);
            this.f818 = false;
            m1516();
        } catch (Throwable th) {
            this.f818 = false;
            throw th;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1482(boolean z) {
        for (int size = this.f821.size() - 1; size >= 0; size--) {
            C0222 r1 = this.f821.get(size);
            if (r1 != null) {
                r1.m1255(z);
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1496(boolean z) {
        for (int size = this.f821.size() - 1; size >= 0; size--) {
            C0222 r1 = this.f821.get(size);
            if (r1 != null) {
                r1.m1259(z);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1470(Configuration configuration) {
        for (int i = 0; i < this.f821.size(); i++) {
            C0222 r1 = this.f821.get(i);
            if (r1 != null) {
                r1.m1212(configuration);
            }
        }
    }

    /* renamed from: ﹳ  reason: contains not printable characters */
    public void m1541() {
        for (int i = 0; i < this.f821.size(); i++) {
            C0222 r1 = this.f821.get(i);
            if (r1 != null) {
                r1.m1248();
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1485(Menu menu, MenuInflater menuInflater) {
        ArrayList<C0222> arrayList = null;
        boolean z = false;
        for (int i = 0; i < this.f821.size(); i++) {
            C0222 r4 = this.f821.get(i);
            if (r4 != null && r4.m1233(menu, menuInflater)) {
                if (arrayList == null) {
                    arrayList = new ArrayList<>();
                }
                arrayList.add(r4);
                z = true;
            }
        }
        if (this.f825 != null) {
            for (int i2 = 0; i2 < this.f825.size(); i2++) {
                C0222 r7 = this.f825.get(i2);
                if (arrayList == null || !arrayList.contains(r7)) {
                    r7.m1290();
                }
            }
        }
        this.f825 = arrayList;
        return z;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1484(Menu menu) {
        boolean z = false;
        for (int i = 0; i < this.f821.size(); i++) {
            C0222 r2 = this.f821.get(i);
            if (r2 != null && r2.m1240(menu)) {
                z = true;
            }
        }
        return z;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1486(MenuItem menuItem) {
        for (int i = 0; i < this.f821.size(); i++) {
            C0222 r2 = this.f821.get(i);
            if (r2 != null && r2.m1241(menuItem)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m1498(MenuItem menuItem) {
        for (int i = 0; i < this.f821.size(); i++) {
            C0222 r2 = this.f821.get(i);
            if (r2 != null && r2.m1247(menuItem)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1495(Menu menu) {
        for (int i = 0; i < this.f821.size(); i++) {
            C0222 r1 = this.f821.get(i);
            if (r1 != null) {
                r1.m1245(menu);
            }
        }
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public void m1533(C0222 r4) {
        if (r4 == null || (this.f822.get(r4.f729) == r4 && (r4.f756 == null || r4.m1249() == this))) {
            this.f833 = r4;
            return;
        }
        throw new IllegalArgumentException("Fragment " + r4 + " is not an active fragment of FragmentManager " + this);
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public C0222 m1542() {
        return this.f833;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.content.Context, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, android.content.Context, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(android.content.Context, float, float):android.support.v4.ʻ.י$ʽ
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.support.v4.ʻ.י$ʽ, int):void
      android.support.v4.ʻ.י.ʻ(java.lang.String, int, int):boolean
      android.support.v4.ʻ.י.ʻ(android.os.Bundle, java.lang.String, android.support.v4.ʻ.ˉ):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.os.Bundle, boolean):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˏ, android.support.v4.ʻ.ˋ, android.support.v4.ʻ.ˉ):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.content.Context, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1475(C0222 r4, Context context, boolean z) {
        C0222 r0 = this.f832;
        if (r0 != null) {
            C0239 r02 = r0.m1249();
            if (r02 instanceof C0246) {
                ((C0246) r02).m1475(r4, context, true);
            }
        }
        Iterator<C0347<C0239.C0240, Boolean>> it = this.f823.iterator();
        while (it.hasNext()) {
            C0347 next = it.next();
            if (!z || ((Boolean) next.f1163).booleanValue()) {
                ((C0239.C0240) next.f1162).m1410(this, r4, context);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʼ(android.support.v4.ʻ.ˉ, android.content.Context, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, android.content.Context, int]
     candidates:
      android.support.v4.ʻ.י.ʼ(android.support.v4.ʻ.ˉ, android.os.Bundle, boolean):void
      android.support.v4.ʻ.י.ʼ(android.support.v4.ʻ.ˉ, android.content.Context, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1492(C0222 r4, Context context, boolean z) {
        C0222 r0 = this.f832;
        if (r0 != null) {
            C0239 r02 = r0.m1249();
            if (r02 instanceof C0246) {
                ((C0246) r02).m1492(r4, context, true);
            }
        }
        Iterator<C0347<C0239.C0240, Boolean>> it = this.f823.iterator();
        while (it.hasNext()) {
            C0347 next = it.next();
            if (!z || ((Boolean) next.f1163).booleanValue()) {
                ((C0239.C0240) next.f1162).m1414(this, r4, context);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.os.Bundle, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, android.os.Bundle, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(android.content.Context, float, float):android.support.v4.ʻ.י$ʽ
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.support.v4.ʻ.י$ʽ, int):void
      android.support.v4.ʻ.י.ʻ(java.lang.String, int, int):boolean
      android.support.v4.ʻ.י.ʻ(android.os.Bundle, java.lang.String, android.support.v4.ʻ.ˉ):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.content.Context, boolean):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˏ, android.support.v4.ʻ.ˋ, android.support.v4.ʻ.ˉ):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.os.Bundle, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1476(C0222 r4, Bundle bundle, boolean z) {
        C0222 r0 = this.f832;
        if (r0 != null) {
            C0239 r02 = r0.m1249();
            if (r02 instanceof C0246) {
                ((C0246) r02).m1476(r4, bundle, true);
            }
        }
        Iterator<C0347<C0239.C0240, Boolean>> it = this.f823.iterator();
        while (it.hasNext()) {
            C0347 next = it.next();
            if (!z || ((Boolean) next.f1163).booleanValue()) {
                ((C0239.C0240) next.f1162).m1411(this, r4, bundle);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʼ(android.support.v4.ʻ.ˉ, android.os.Bundle, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, android.os.Bundle, int]
     candidates:
      android.support.v4.ʻ.י.ʼ(android.support.v4.ʻ.ˉ, android.content.Context, boolean):void
      android.support.v4.ʻ.י.ʼ(android.support.v4.ʻ.ˉ, android.os.Bundle, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1493(C0222 r4, Bundle bundle, boolean z) {
        C0222 r0 = this.f832;
        if (r0 != null) {
            C0239 r02 = r0.m1249();
            if (r02 instanceof C0246) {
                ((C0246) r02).m1493(r4, bundle, true);
            }
        }
        Iterator<C0347<C0239.C0240, Boolean>> it = this.f823.iterator();
        while (it.hasNext()) {
            C0347 next = it.next();
            if (!z || ((Boolean) next.f1163).booleanValue()) {
                ((C0239.C0240) next.f1162).m1415(this, r4, bundle);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1501(C0222 r4, Bundle bundle, boolean z) {
        C0222 r0 = this.f832;
        if (r0 != null) {
            C0239 r02 = r0.m1249();
            if (r02 instanceof C0246) {
                ((C0246) r02).m1501(r4, bundle, true);
            }
        }
        Iterator<C0347<C0239.C0240, Boolean>> it = this.f823.iterator();
        while (it.hasNext()) {
            C0347 next = it.next();
            if (!z || ((Boolean) next.f1163).booleanValue()) {
                ((C0239.C0240) next.f1162).m1417(this, r4, bundle);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.view.View, android.os.Bundle, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, android.view.View, android.os.Bundle, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, boolean, int):android.support.v4.ʻ.י$ʽ
      android.support.v4.ʻ.י.ʻ(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.ʻ.ˑ.ʻ(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, android.view.View, android.os.Bundle, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1477(C0222 r4, View view, Bundle bundle, boolean z) {
        C0222 r0 = this.f832;
        if (r0 != null) {
            C0239 r02 = r0.m1249();
            if (r02 instanceof C0246) {
                ((C0246) r02).m1477(r4, view, bundle, true);
            }
        }
        Iterator<C0347<C0239.C0240, Boolean>> it = this.f823.iterator();
        while (it.hasNext()) {
            C0347 next = it.next();
            if (!z || ((Boolean) next.f1163).booleanValue()) {
                ((C0239.C0240) next.f1162).m1412(this, r4, view, bundle);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʼ(android.support.v4.ʻ.ˉ, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, int]
     candidates:
      android.support.v4.ʻ.י.ʼ(int, boolean):int
      android.support.v4.ʻ.י.ʼ(android.view.View, android.support.v4.ʻ.י$ʽ):void
      android.support.v4.ʻ.י.ʼ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.ʻ.י.ʼ(android.support.v4.ʻ.ˉ, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1494(C0222 r4, boolean z) {
        C0222 r0 = this.f832;
        if (r0 != null) {
            C0239 r02 = r0.m1249();
            if (r02 instanceof C0246) {
                ((C0246) r02).m1494(r4, true);
            }
        }
        Iterator<C0347<C0239.C0240, Boolean>> it = this.f823.iterator();
        while (it.hasNext()) {
            C0347 next = it.next();
            if (!z || ((Boolean) next.f1163).booleanValue()) {
                ((C0239.C0240) next.f1162).m1409(this, r4);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʽ(android.support.v4.ʻ.ˉ, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, int]
     candidates:
      android.support.v4.ʻ.י.ʽ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>):boolean
      android.support.v4.ʻ.י.ʽ(android.support.v4.ʻ.ˉ, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1502(C0222 r4, boolean z) {
        C0222 r0 = this.f832;
        if (r0 != null) {
            C0239 r02 = r0.m1249();
            if (r02 instanceof C0246) {
                ((C0246) r02).m1502(r4, true);
            }
        }
        Iterator<C0347<C0239.C0240, Boolean>> it = this.f823.iterator();
        while (it.hasNext()) {
            C0347 next = it.next();
            if (!z || ((Boolean) next.f1163).booleanValue()) {
                ((C0239.C0240) next.f1162).m1413(this, r4);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m1507(C0222 r4, boolean z) {
        C0222 r0 = this.f832;
        if (r0 != null) {
            C0239 r02 = r0.m1249();
            if (r02 instanceof C0246) {
                ((C0246) r02).m1507(r4, true);
            }
        }
        Iterator<C0347<C0239.C0240, Boolean>> it = this.f823.iterator();
        while (it.hasNext()) {
            C0347 next = it.next();
            if (!z || ((Boolean) next.f1163).booleanValue()) {
                ((C0239.C0240) next.f1162).m1416(this, r4);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m1509(C0222 r4, boolean z) {
        C0222 r0 = this.f832;
        if (r0 != null) {
            C0239 r02 = r0.m1249();
            if (r02 instanceof C0246) {
                ((C0246) r02).m1509(r4, true);
            }
        }
        Iterator<C0347<C0239.C0240, Boolean>> it = this.f823.iterator();
        while (it.hasNext()) {
            C0347 next = it.next();
            if (!z || ((Boolean) next.f1163).booleanValue()) {
                ((C0239.C0240) next.f1162).m1418(this, r4);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m1506(C0222 r4, Bundle bundle, boolean z) {
        C0222 r0 = this.f832;
        if (r0 != null) {
            C0239 r02 = r0.m1249();
            if (r02 instanceof C0246) {
                ((C0246) r02).m1506(r4, bundle, true);
            }
        }
        Iterator<C0347<C0239.C0240, Boolean>> it = this.f823.iterator();
        while (it.hasNext()) {
            C0347 next = it.next();
            if (!z || ((Boolean) next.f1163).booleanValue()) {
                ((C0239.C0240) next.f1162).m1419(this, r4, bundle);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m1513(C0222 r4, boolean z) {
        C0222 r0 = this.f832;
        if (r0 != null) {
            C0239 r02 = r0.m1249();
            if (r02 instanceof C0246) {
                ((C0246) r02).m1513(r4, true);
            }
        }
        Iterator<C0347<C0239.C0240, Boolean>> it = this.f823.iterator();
        while (it.hasNext()) {
            C0347 next = it.next();
            if (!z || ((Boolean) next.f1163).booleanValue()) {
                ((C0239.C0240) next.f1162).m1420(this, r4);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m1515(C0222 r4, boolean z) {
        C0222 r0 = this.f832;
        if (r0 != null) {
            C0239 r02 = r0.m1249();
            if (r02 instanceof C0246) {
                ((C0246) r02).m1515(r4, true);
            }
        }
        Iterator<C0347<C0239.C0240, Boolean>> it = this.f823.iterator();
        while (it.hasNext()) {
            C0347 next = it.next();
            if (!z || ((Boolean) next.f1163).booleanValue()) {
                ((C0239.C0240) next.f1162).m1421(this, r4);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public void m1519(C0222 r4, boolean z) {
        C0222 r0 = this.f832;
        if (r0 != null) {
            C0239 r02 = r0.m1249();
            if (r02 instanceof C0246) {
                ((C0246) r02).m1519(r4, true);
            }
        }
        Iterator<C0347<C0239.C0240, Boolean>> it = this.f823.iterator();
        while (it.hasNext()) {
            C0347 next = it.next();
            if (!z || ((Boolean) next.f1163).booleanValue()) {
                ((C0239.C0240) next.f1162).m1422(this, r4);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.ʻ.י.ʻ(android.view.View, android.support.v4.ʻ.י$ʽ):boolean
      android.support.v4.ʻ.י.ʻ(android.os.Bundle, java.lang.String):android.support.v4.ʻ.ˉ
      android.support.v4.ʻ.י.ʻ(int, android.support.v4.ʻ.ʽ):void
      android.support.v4.ʻ.י.ʻ(int, boolean):void
      android.support.v4.ʻ.י.ʻ(android.os.Parcelable, android.support.v4.ʻ.ـ):void
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י$ˆ, boolean):void
      android.support.v4.ʻ.י.ʻ(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void
     arg types: [android.support.v4.ʻ.ˉ, int, int, int, int]
     candidates:
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.ˈ.ʼ<android.support.v4.ʻ.ˉ>):int
      android.support.v4.ʻ.י.ʻ(android.content.Context, float, float, float, float):android.support.v4.ʻ.י$ʽ
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י, android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
      android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void */
    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        C0222 r11;
        AttributeSet attributeSet2 = attributeSet;
        if (!"fragment".equals(str)) {
            return null;
        }
        String attributeValue = attributeSet2.getAttributeValue(null, "class");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet2, C0251.f862);
        int i = 0;
        if (attributeValue == null) {
            attributeValue = obtainStyledAttributes.getString(0);
        }
        String str2 = attributeValue;
        int resourceId = obtainStyledAttributes.getResourceId(1, -1);
        String string = obtainStyledAttributes.getString(2);
        obtainStyledAttributes.recycle();
        if (!C0222.m1191(this.f830.m1394(), str2)) {
            return null;
        }
        if (view != null) {
            i = view.getId();
        }
        if (i == -1 && resourceId == -1 && string == null) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Must specify unique android:id, android:tag, or have a parent with an id for " + str2);
        }
        C0222 r4 = resourceId != -1 ? m1488(resourceId) : null;
        if (r4 == null && string != null) {
            r4 = m1465(string);
        }
        if (r4 == null && i != -1) {
            r4 = m1488(i);
        }
        if (f810) {
            Log.v("FragmentManager", "onCreateView: id=0x" + Integer.toHexString(resourceId) + " fname=" + str2 + " existing=" + r4);
        }
        if (r4 == null) {
            C0222 r0 = this.f831.m1337(context, str2, null);
            r0.f745 = true;
            r0.f764 = resourceId != 0 ? resourceId : i;
            r0.f765 = i;
            r0.f766 = string;
            r0.f747 = true;
            r0.f754 = this;
            C0237 r2 = this.f830;
            r0.f756 = r2;
            r0.m1207(r2.m1394(), attributeSet2, r0.f725);
            m1478(r0, true);
            r11 = r0;
        } else if (!r4.f747) {
            r4.f747 = true;
            r4.f756 = this.f830;
            if (!r4.f722) {
                r4.m1207(this.f830.m1394(), attributeSet2, r4.f725);
            }
            r11 = r4;
        } else {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Duplicate id 0x" + Integer.toHexString(resourceId) + ", tag " + string + ", or parent id 0x" + Integer.toHexString(i) + " with another fragment for " + str2);
        }
        if (this.f829 >= 1 || !r11.f745) {
            m1491(r11);
        } else {
            m1474(r11, 1, 0, 0, false);
        }
        if (r11.f750 != null) {
            if (resourceId != 0) {
                r11.f750.setId(resourceId);
            }
            if (r11.f750.getTag() == null) {
                r11.f750.setTag(string);
            }
            return r11.f750;
        }
        throw new IllegalStateException("Fragment " + str2 + " did not create a view.");
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }

    /* renamed from: android.support.v4.ʻ.י$ˈ  reason: contains not printable characters */
    /* compiled from: FragmentManager */
    static class C0253 implements C0222.C0225 {
        /* access modifiers changed from: private */

        /* renamed from: ʻ  reason: contains not printable characters */
        public final boolean f863;
        /* access modifiers changed from: private */

        /* renamed from: ʼ  reason: contains not printable characters */
        public final C0201 f864;

        /* renamed from: ʽ  reason: contains not printable characters */
        private int f865;

        C0253(C0201 r1, boolean z) {
            this.f863 = z;
            this.f864 = r1;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1547() {
            this.f865--;
            if (this.f865 == 0) {
                this.f864.f656.m1460();
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m1548() {
            this.f865++;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m1549() {
            return this.f865 == 0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י, android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
         arg types: [android.support.v4.ʻ.י, android.support.v4.ʻ.ʽ, boolean, boolean, int]
         candidates:
          android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.ˈ.ʼ<android.support.v4.ʻ.ˉ>):int
          android.support.v4.ʻ.י.ʻ(android.content.Context, float, float, float, float):android.support.v4.ʻ.י$ʽ
          android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void
          android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
          android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י, android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void */
        /* renamed from: ʾ  reason: contains not printable characters */
        public void m1550() {
            boolean z = this.f865 > 0;
            C0246 r3 = this.f864.f656;
            int size = r3.f821.size();
            for (int i = 0; i < size; i++) {
                C0222 r5 = r3.f821.get(i);
                r5.m1214((C0222.C0225) null);
                if (z && r5.m1299()) {
                    r5.m1289();
                }
            }
            this.f864.f656.m1434(this.f864, this.f863, !z, true);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י, android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void
         arg types: [android.support.v4.ʻ.י, android.support.v4.ʻ.ʽ, boolean, int, int]
         candidates:
          android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.ˈ.ʼ<android.support.v4.ʻ.ˉ>):int
          android.support.v4.ʻ.י.ʻ(android.content.Context, float, float, float, float):android.support.v4.ʻ.י$ʽ
          android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.ˉ, int, int, int, boolean):void
          android.support.v4.ʻ.י.ʻ(java.util.ArrayList<android.support.v4.ʻ.ʽ>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
          android.support.v4.ʻ.י.ʻ(android.support.v4.ʻ.י, android.support.v4.ʻ.ʽ, boolean, boolean, boolean):void */
        /* renamed from: ʿ  reason: contains not printable characters */
        public void m1551() {
            this.f864.f656.m1434(this.f864, this.f863, false, false);
        }
    }

    /* renamed from: android.support.v4.ʻ.י$ʽ  reason: contains not printable characters */
    /* compiled from: FragmentManager */
    private static class C0249 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public final Animation f859;

        /* renamed from: ʼ  reason: contains not printable characters */
        public final Animator f860;

        private C0249(Animation animation) {
            this.f859 = animation;
            this.f860 = null;
            if (animation == null) {
                throw new IllegalStateException("Animation cannot be null");
            }
        }

        private C0249(Animator animator) {
            this.f859 = null;
            this.f860 = animator;
            if (animator == null) {
                throw new IllegalStateException("Animator cannot be null");
            }
        }
    }

    /* renamed from: android.support.v4.ʻ.י$ʼ  reason: contains not printable characters */
    /* compiled from: FragmentManager */
    private static class C0248 implements Animation.AnimationListener {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final Animation.AnimationListener f858;

        private C0248(Animation.AnimationListener animationListener) {
            this.f858 = animationListener;
        }

        public void onAnimationStart(Animation animation) {
            Animation.AnimationListener animationListener = this.f858;
            if (animationListener != null) {
                animationListener.onAnimationStart(animation);
            }
        }

        public void onAnimationEnd(Animation animation) {
            Animation.AnimationListener animationListener = this.f858;
            if (animationListener != null) {
                animationListener.onAnimationEnd(animation);
            }
        }

        public void onAnimationRepeat(Animation animation) {
            Animation.AnimationListener animationListener = this.f858;
            if (animationListener != null) {
                animationListener.onAnimationRepeat(animation);
            }
        }
    }

    /* renamed from: android.support.v4.ʻ.י$ʻ  reason: contains not printable characters */
    /* compiled from: FragmentManager */
    private static class C0247 extends C0248 {

        /* renamed from: ʻ  reason: contains not printable characters */
        View f856;

        C0247(View view, Animation.AnimationListener animationListener) {
            super(animationListener);
            this.f856 = view;
        }

        public void onAnimationEnd(Animation animation) {
            if (C0414.m2257(this.f856) || Build.VERSION.SDK_INT >= 24) {
                this.f856.post(new Runnable() {
                    public void run() {
                        C0247.this.f856.setLayerType(0, null);
                    }
                });
            } else {
                this.f856.setLayerType(0, null);
            }
            super.onAnimationEnd(animation);
        }
    }

    /* renamed from: android.support.v4.ʻ.י$ʾ  reason: contains not printable characters */
    /* compiled from: FragmentManager */
    private static class C0250 extends AnimatorListenerAdapter {

        /* renamed from: ʻ  reason: contains not printable characters */
        View f861;

        C0250(View view) {
            this.f861 = view;
        }

        public void onAnimationStart(Animator animator) {
            this.f861.setLayerType(2, null);
        }

        public void onAnimationEnd(Animator animator) {
            this.f861.setLayerType(0, null);
            animator.removeListener(this);
        }
    }
}
