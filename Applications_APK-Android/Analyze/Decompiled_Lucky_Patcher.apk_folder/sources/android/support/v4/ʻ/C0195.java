package android.support.v4.ʻ;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.RemoteInput;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.ʻ.C0198;
import android.widget.RemoteViews;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: android.support.v4.ʻ.ʻʻ  reason: contains not printable characters */
/* compiled from: NotificationCompatApi24 */
class C0195 {

    /* renamed from: android.support.v4.ʻ.ʻʻ$ʻ  reason: contains not printable characters */
    /* compiled from: NotificationCompatApi24 */
    public static class C0196 implements C0271, C0272 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private Notification.Builder f653;

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f654;

        public C0196(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z, boolean z2, boolean z3, int i4, CharSequence charSequence4, boolean z4, String str, ArrayList<String> arrayList, Bundle bundle, int i5, int i6, Notification notification2, String str2, boolean z5, String str3, CharSequence[] charSequenceArr, RemoteViews remoteViews2, RemoteViews remoteViews3, RemoteViews remoteViews4, int i7) {
            PendingIntent pendingIntent3;
            Notification notification3 = notification;
            RemoteViews remoteViews5 = remoteViews2;
            RemoteViews remoteViews6 = remoteViews3;
            RemoteViews remoteViews7 = remoteViews4;
            boolean z6 = true;
            Notification.Builder deleteIntent = new Notification.Builder(context).setWhen(notification3.when).setShowWhen(z2).setSmallIcon(notification3.icon, notification3.iconLevel).setContent(notification3.contentView).setTicker(notification3.tickerText, remoteViews).setSound(notification3.sound, notification3.audioStreamType).setVibrate(notification3.vibrate).setLights(notification3.ledARGB, notification3.ledOnMS, notification3.ledOffMS).setOngoing((notification3.flags & 2) != 0).setOnlyAlertOnce((notification3.flags & 8) != 0).setAutoCancel((notification3.flags & 16) != 0).setDefaults(notification3.defaults).setContentTitle(charSequence).setContentText(charSequence2).setSubText(charSequence4).setContentInfo(charSequence3).setContentIntent(pendingIntent).setDeleteIntent(notification3.deleteIntent);
            if ((notification3.flags & 128) != 0) {
                pendingIntent3 = pendingIntent2;
            } else {
                pendingIntent3 = pendingIntent2;
                z6 = false;
            }
            this.f653 = deleteIntent.setFullScreenIntent(pendingIntent3, z6).setLargeIcon(bitmap).setNumber(i).setUsesChronometer(z3).setPriority(i4).setProgress(i2, i3, z).setLocalOnly(z4).setExtras(bundle).setGroup(str2).setGroupSummary(z5).setSortKey(str3).setCategory(str).setColor(i5).setVisibility(i6).setPublicVersion(notification2).setRemoteInputHistory(charSequenceArr);
            if (remoteViews5 != null) {
                this.f653.setCustomContentView(remoteViews5);
            }
            if (remoteViews6 != null) {
                this.f653.setCustomBigContentView(remoteViews6);
            }
            if (remoteViews7 != null) {
                this.f653.setCustomHeadsUpContentView(remoteViews7);
            }
            Iterator<String> it = arrayList.iterator();
            while (it.hasNext()) {
                this.f653.addPerson(it.next());
            }
            this.f654 = i7;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1131(C0198.C0199 r2) {
            C0195.m1128(this.f653, r2);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Notification m1130() {
            Notification build = this.f653.build();
            if (this.f654 != 0) {
                if (!(build.getGroup() == null || (build.flags & 512) == 0 || this.f654 != 2)) {
                    m1129(build);
                }
                if (build.getGroup() != null && (build.flags & 512) == 0 && this.f654 == 1) {
                    m1129(build);
                }
            }
            return build;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m1129(Notification notification) {
            notification.sound = null;
            notification.vibrate = null;
            notification.defaults &= -2;
            notification.defaults &= -3;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1128(Notification.Builder builder, C0198.C0199 r6) {
        Bundle bundle;
        Notification.Action.Builder builder2 = new Notification.Action.Builder(r6.m1134(), r6.m1135(), r6.m1136());
        if (r6.m1140() != null) {
            for (RemoteInput addRemoteInput : C0226.m1316(r6.m1140())) {
                builder2.addRemoteInput(addRemoteInput);
            }
        }
        if (r6.m1137() != null) {
            bundle = new Bundle(r6.m1137());
        } else {
            bundle = new Bundle();
        }
        bundle.putBoolean("android.support.allowGeneratedReplies", r6.m1138());
        builder2.setAllowGeneratedReplies(r6.m1138());
        builder2.addExtras(bundle);
        builder.addAction(builder2.build());
    }
}
