package android.support.v4.ʻ;

import android.app.RemoteInput;
import android.support.v4.ʻ.C0219;

/* renamed from: android.support.v4.ʻ.ˉˉ  reason: contains not printable characters */
/* compiled from: RemoteInputCompatApi20 */
class C0226 {
    /* renamed from: ʻ  reason: contains not printable characters */
    static RemoteInput[] m1316(C0219.C0220[] r5) {
        if (r5 == null) {
            return null;
        }
        RemoteInput[] remoteInputArr = new RemoteInput[r5.length];
        for (int i = 0; i < r5.length; i++) {
            C0219.C0220 r2 = r5[i];
            remoteInputArr[i] = new RemoteInput.Builder(r2.m1184()).setLabel(r2.m1185()).setChoices(r2.m1186()).setAllowFreeFormInput(r2.m1188()).addExtras(r2.m1189()).build();
        }
        return remoteInputArr;
    }
}
