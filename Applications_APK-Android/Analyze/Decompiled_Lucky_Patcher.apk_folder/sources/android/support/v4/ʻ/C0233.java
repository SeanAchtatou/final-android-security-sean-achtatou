package android.support.v4.ʻ;

import android.os.Bundle;
import android.support.v4.ʻ.C0219;
import java.util.ArrayList;
import java.util.Set;

/* renamed from: android.support.v4.ʻ.ˋˋ  reason: contains not printable characters */
/* compiled from: RemoteInputCompatJellybean */
class C0233 {
    /* renamed from: ʻ  reason: contains not printable characters */
    static Bundle m1340(C0219.C0220 r3) {
        Bundle bundle = new Bundle();
        bundle.putString("resultKey", r3.m1184());
        bundle.putCharSequence("label", r3.m1185());
        bundle.putCharSequenceArray("choices", r3.m1186());
        bundle.putBoolean("allowFreeFormInput", r3.m1188());
        bundle.putBundle("extras", r3.m1189());
        Set<String> r32 = r3.m1187();
        if (r32 != null && !r32.isEmpty()) {
            ArrayList arrayList = new ArrayList(r32.size());
            for (String add : r32) {
                arrayList.add(add);
            }
            bundle.putStringArrayList("allowedDataTypes", arrayList);
        }
        return bundle;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static Bundle[] m1341(C0219.C0220[] r3) {
        if (r3 == null) {
            return null;
        }
        Bundle[] bundleArr = new Bundle[r3.length];
        for (int i = 0; i < r3.length; i++) {
            bundleArr[i] = m1340(r3[i]);
        }
        return bundleArr;
    }
}
