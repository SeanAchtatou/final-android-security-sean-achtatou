package android.support.v4.ʻ;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Parcelable;
import android.support.v4.ˈ.C0353;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* renamed from: android.support.v4.ʻ.ˎ  reason: contains not printable characters */
/* compiled from: FragmentController */
public class C0234 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C0237<?> f796;

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final C0234 m1342(C0237<?> r1) {
        return new C0234(r1);
    }

    private C0234(C0237<?> r1) {
        this.f796 = r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0239 m1344() {
        return this.f796.m1396();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0266 m1355() {
        return this.f796.m1397();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0222 m1343(String str) {
        return this.f796.f800.m1489(str);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1348(C0222 r3) {
        C0246 r0 = this.f796.f800;
        C0237<?> r1 = this.f796;
        r0.m1479(r1, r1, r3);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public View m1345(View view, String str, Context context, AttributeSet attributeSet) {
        return this.f796.f800.onCreateView(view, str, context, attributeSet);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1359() {
        this.f796.f800.m1528();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public Parcelable m1361() {
        return this.f796.f800.m1526();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1347(Parcelable parcelable, C0254 r3) {
        this.f796.f800.m1472(parcelable, r3);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public C0254 m1362() {
        return this.f796.f800.m1522();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m1363() {
        this.f796.f800.m1531();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public void m1364() {
        this.f796.f800.m1532();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public void m1365() {
        this.f796.f800.m1534();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public void m1366() {
        this.f796.f800.m1535();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public void m1367() {
        this.f796.f800.m1536();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public void m1368() {
        this.f796.f800.m1537();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public void m1369() {
        this.f796.f800.m1538();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m1370() {
        this.f796.f800.m1540();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1351(boolean z) {
        this.f796.f800.m1482(z);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1357(boolean z) {
        this.f796.f800.m1496(z);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1346(Configuration configuration) {
        this.f796.f800.m1470(configuration);
    }

    /* renamed from: י  reason: contains not printable characters */
    public void m1371() {
        this.f796.f800.m1541();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1353(Menu menu, MenuInflater menuInflater) {
        return this.f796.f800.m1485(menu, menuInflater);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1352(Menu menu) {
        return this.f796.f800.m1484(menu);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1354(MenuItem menuItem) {
        return this.f796.f800.m1486(menuItem);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m1358(MenuItem menuItem) {
        return this.f796.f800.m1498(menuItem);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1356(Menu menu) {
        this.f796.f800.m1495(menu);
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public boolean m1372() {
        return this.f796.f800.m1516();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m1373() {
        this.f796.m1399();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1360(boolean z) {
        this.f796.m1384(z);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m1374() {
        this.f796.m1400();
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public void m1375() {
        this.f796.m1401();
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public C0353<String, C0266> m1376() {
        return this.f796.m1402();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1349(C0353<String, C0266> r2) {
        this.f796.m1381(r2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1350(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        this.f796.m1389(str, fileDescriptor, printWriter, strArr);
    }
}
