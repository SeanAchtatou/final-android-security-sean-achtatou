package android.support.v4.ʻ;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.RemoteInput;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.ʻ.C0198;
import android.widget.RemoteViews;
import java.util.ArrayList;

/* renamed from: android.support.v4.ʻ.ᐧᐧ  reason: contains not printable characters */
/* compiled from: NotificationCompatApi20 */
class C0258 {

    /* renamed from: android.support.v4.ʻ.ᐧᐧ$ʻ  reason: contains not printable characters */
    /* compiled from: NotificationCompatApi20 */
    public static class C0259 implements C0271, C0272 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private Notification.Builder f888;

        /* renamed from: ʼ  reason: contains not printable characters */
        private Bundle f889;

        /* renamed from: ʽ  reason: contains not printable characters */
        private RemoteViews f890;

        /* renamed from: ʾ  reason: contains not printable characters */
        private RemoteViews f891;

        /* renamed from: ʿ  reason: contains not printable characters */
        private int f892;

        public C0259(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z, boolean z2, boolean z3, int i4, CharSequence charSequence4, boolean z4, ArrayList<String> arrayList, Bundle bundle, String str, boolean z5, String str2, RemoteViews remoteViews2, RemoteViews remoteViews3, int i5) {
            PendingIntent pendingIntent3;
            Notification notification2 = notification;
            ArrayList<String> arrayList2 = arrayList;
            Bundle bundle2 = bundle;
            boolean z6 = true;
            Notification.Builder deleteIntent = new Notification.Builder(context).setWhen(notification2.when).setShowWhen(z2).setSmallIcon(notification2.icon, notification2.iconLevel).setContent(notification2.contentView).setTicker(notification2.tickerText, remoteViews).setSound(notification2.sound, notification2.audioStreamType).setVibrate(notification2.vibrate).setLights(notification2.ledARGB, notification2.ledOnMS, notification2.ledOffMS).setOngoing((notification2.flags & 2) != 0).setOnlyAlertOnce((notification2.flags & 8) != 0).setAutoCancel((notification2.flags & 16) != 0).setDefaults(notification2.defaults).setContentTitle(charSequence).setContentText(charSequence2).setSubText(charSequence4).setContentInfo(charSequence3).setContentIntent(pendingIntent).setDeleteIntent(notification2.deleteIntent);
            if ((notification2.flags & 128) != 0) {
                pendingIntent3 = pendingIntent2;
            } else {
                pendingIntent3 = pendingIntent2;
                z6 = false;
            }
            this.f888 = deleteIntent.setFullScreenIntent(pendingIntent3, z6).setLargeIcon(bitmap).setNumber(i).setUsesChronometer(z3).setPriority(i4).setProgress(i2, i3, z).setLocalOnly(z4).setGroup(str).setGroupSummary(z5).setSortKey(str2);
            this.f889 = new Bundle();
            if (bundle2 != null) {
                this.f889.putAll(bundle2);
            }
            if (arrayList2 != null && !arrayList.isEmpty()) {
                this.f889.putStringArray("android.people", (String[]) arrayList2.toArray(new String[arrayList.size()]));
            }
            this.f890 = remoteViews2;
            this.f891 = remoteViews3;
            this.f892 = i5;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1564(C0198.C0199 r2) {
            C0258.m1561(this.f888, r2);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Notification m1563() {
            this.f888.setExtras(this.f889);
            Notification build = this.f888.build();
            RemoteViews remoteViews = this.f890;
            if (remoteViews != null) {
                build.contentView = remoteViews;
            }
            RemoteViews remoteViews2 = this.f891;
            if (remoteViews2 != null) {
                build.bigContentView = remoteViews2;
            }
            if (this.f892 != 0) {
                if (!(build.getGroup() == null || (build.flags & 512) == 0 || this.f892 != 2)) {
                    m1562(build);
                }
                if (build.getGroup() != null && (build.flags & 512) == 0 && this.f892 == 1) {
                    m1562(build);
                }
            }
            return build;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m1562(Notification notification) {
            notification.sound = null;
            notification.vibrate = null;
            notification.defaults &= -2;
            notification.defaults &= -3;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1561(Notification.Builder builder, C0198.C0199 r6) {
        Bundle bundle;
        Notification.Action.Builder builder2 = new Notification.Action.Builder(r6.m1134(), r6.m1135(), r6.m1136());
        if (r6.m1140() != null) {
            for (RemoteInput addRemoteInput : C0226.m1316(r6.m1140())) {
                builder2.addRemoteInput(addRemoteInput);
            }
        }
        if (r6.m1137() != null) {
            bundle = new Bundle(r6.m1137());
        } else {
            bundle = new Bundle();
        }
        bundle.putBoolean("android.support.allowGeneratedReplies", r6.m1138());
        builder2.addExtras(bundle);
        builder.addAction(builder2.build());
    }
}
