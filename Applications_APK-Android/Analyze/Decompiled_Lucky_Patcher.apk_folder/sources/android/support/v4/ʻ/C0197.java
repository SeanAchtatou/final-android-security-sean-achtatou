package android.support.v4.ʻ;

import android.app.AppOpsManager;
import android.content.Context;
import android.os.Build;

/* renamed from: android.support.v4.ʻ.ʼ  reason: contains not printable characters */
/* compiled from: AppOpsManagerCompat */
public final class C0197 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m1133(String str) {
        if (Build.VERSION.SDK_INT >= 23) {
            return AppOpsManager.permissionToOp(str);
        }
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m1132(Context context, String str, String str2) {
        if (Build.VERSION.SDK_INT >= 23) {
            return ((AppOpsManager) context.getSystemService(AppOpsManager.class)).noteProxyOp(str, str2);
        }
        return 1;
    }
}
