package android.support.v4.ʻ;

import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: android.support.v4.ʻ.ˈ  reason: contains not printable characters */
/* compiled from: BundleCompat */
public final class C0217 {

    /* renamed from: android.support.v4.ʻ.ˈ$ʻ  reason: contains not printable characters */
    /* compiled from: BundleCompat */
    static class C0218 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private static Method f718;

        /* renamed from: ʼ  reason: contains not printable characters */
        private static boolean f719;

        /* renamed from: ʻ  reason: contains not printable characters */
        public static IBinder m1183(Bundle bundle, String str) {
            if (!f719) {
                try {
                    f718 = Bundle.class.getMethod("getIBinder", String.class);
                    f718.setAccessible(true);
                } catch (NoSuchMethodException e) {
                    Log.i("BundleCompatBaseImpl", "Failed to retrieve getIBinder method", e);
                }
                f719 = true;
            }
            Method method = f718;
            if (method != null) {
                try {
                    return (IBinder) method.invoke(bundle, str);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
                    Log.i("BundleCompatBaseImpl", "Failed to invoke getIBinder via reflection", e2);
                    f718 = null;
                }
            }
            return null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static IBinder m1182(Bundle bundle, String str) {
        if (Build.VERSION.SDK_INT >= 18) {
            return bundle.getBinder(str);
        }
        return C0218.m1183(bundle, str);
    }
}
