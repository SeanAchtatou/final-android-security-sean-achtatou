package android.support.v4.ʻ;

import android.app.Activity;
import android.arch.lifecycle.C0003;
import android.arch.lifecycle.C0006;
import android.arch.lifecycle.C0007;
import android.arch.lifecycle.C0011;
import android.os.Bundle;
import android.support.v4.ˈ.C0353;

/* renamed from: android.support.v4.ʻ.ˎˎ  reason: contains not printable characters */
/* compiled from: SupportActivity */
public class C0235 extends Activity implements C0006 {
    private C0353<Class<? extends C0236>, C0236> mExtraDataMap = new C0353<>();
    private C0007 mLifecycleRegistry = new C0007(this);

    /* renamed from: android.support.v4.ʻ.ˎˎ$ʻ  reason: contains not printable characters */
    /* compiled from: SupportActivity */
    public static class C0236 {
    }

    public void putExtraData(C0236 r3) {
        this.mExtraDataMap.put(r3.getClass(), r3);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C0011.m23(this);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        this.mLifecycleRegistry.m19(C0003.C0005.CREATED);
        super.onSaveInstanceState(bundle);
    }

    public <T extends C0236> T getExtraData(Class<T> cls) {
        return (C0236) this.mExtraDataMap.get(cls);
    }

    public C0003 getLifecycle() {
        return this.mLifecycleRegistry;
    }
}
