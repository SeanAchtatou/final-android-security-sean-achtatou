package android.support.v4.ʻ;

import android.app.Activity;
import android.app.SharedElementCallback;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.v4.content.C0101;
import android.support.v4.ʻ.C0230;
import android.view.View;
import java.util.List;
import java.util.Map;

/* renamed from: android.support.v4.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: ActivityCompat */
public class C0190 extends C0101 {

    /* renamed from: android.support.v4.ʻ.ʻ$ʻ  reason: contains not printable characters */
    /* compiled from: ActivityCompat */
    public interface C0191 {
        void onRequestPermissionsResult(int i, String[] strArr, int[] iArr);
    }

    /* renamed from: android.support.v4.ʻ.ʻ$ʼ  reason: contains not printable characters */
    /* compiled from: ActivityCompat */
    public interface C0192 {
        void validateRequestPermissionsRequestCode(int i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1119(Activity activity, Intent intent, int i, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            activity.startActivityForResult(intent, i, bundle);
        } else {
            activity.startActivityForResult(intent, i);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1120(Activity activity, IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            activity.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4, bundle);
        } else {
            activity.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1118(Activity activity) {
        if (Build.VERSION.SDK_INT >= 16) {
            activity.finishAffinity();
        } else {
            activity.finish();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static void m1123(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.finishAfterTransition();
        } else {
            activity.finish();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1121(Activity activity, C0230 r4) {
        SharedElementCallback sharedElementCallback = null;
        if (Build.VERSION.SDK_INT >= 23) {
            if (r4 != null) {
                sharedElementCallback = new C0194(r4);
            }
            activity.setEnterSharedElementCallback(sharedElementCallback);
        } else if (Build.VERSION.SDK_INT >= 21) {
            if (r4 != null) {
                sharedElementCallback = new C0193(r4);
            }
            activity.setEnterSharedElementCallback(sharedElementCallback);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static void m1124(Activity activity, C0230 r4) {
        SharedElementCallback sharedElementCallback = null;
        if (Build.VERSION.SDK_INT >= 23) {
            if (r4 != null) {
                sharedElementCallback = new C0194(r4);
            }
            activity.setExitSharedElementCallback(sharedElementCallback);
        } else if (Build.VERSION.SDK_INT >= 21) {
            if (r4 != null) {
                sharedElementCallback = new C0193(r4);
            }
            activity.setExitSharedElementCallback(sharedElementCallback);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static void m1125(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.postponeEnterTransition();
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static void m1126(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.startPostponedEnterTransition();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1122(final Activity activity, final String[] strArr, final int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity instanceof C0192) {
                ((C0192) activity).validateRequestPermissionsRequestCode(i);
            }
            activity.requestPermissions(strArr, i);
        } else if (activity instanceof C0191) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    int[] iArr = new int[strArr.length];
                    PackageManager packageManager = activity.getPackageManager();
                    String packageName = activity.getPackageName();
                    int length = strArr.length;
                    for (int i = 0; i < length; i++) {
                        iArr[i] = packageManager.checkPermission(strArr[i], packageName);
                    }
                    ((C0191) activity).onRequestPermissionsResult(i, strArr, iArr);
                }
            });
        }
    }

    /* renamed from: android.support.v4.ʻ.ʻ$ʽ  reason: contains not printable characters */
    /* compiled from: ActivityCompat */
    private static class C0193 extends SharedElementCallback {

        /* renamed from: ʻ  reason: contains not printable characters */
        protected C0230 f650;

        public C0193(C0230 r1) {
            this.f650 = r1;
        }

        public void onSharedElementStart(List<String> list, List<View> list2, List<View> list3) {
            this.f650.m1333(list, list2, list3);
        }

        public void onSharedElementEnd(List<String> list, List<View> list2, List<View> list3) {
            this.f650.m1335(list, list2, list3);
        }

        public void onRejectSharedElements(List<View> list) {
            this.f650.m1331(list);
        }

        public void onMapSharedElements(List<String> list, Map<String, View> map) {
            this.f650.m1334(list, map);
        }

        public Parcelable onCaptureSharedElementSnapshot(View view, Matrix matrix, RectF rectF) {
            return this.f650.m1329(view, matrix, rectF);
        }

        public View onCreateSnapshotView(Context context, Parcelable parcelable) {
            return this.f650.m1330(context, parcelable);
        }
    }

    /* renamed from: android.support.v4.ʻ.ʻ$ʾ  reason: contains not printable characters */
    /* compiled from: ActivityCompat */
    private static class C0194 extends C0193 {
        public C0194(C0230 r1) {
            super(r1);
        }

        public void onSharedElementsArrived(List<String> list, List<View> list2, final SharedElementCallback.OnSharedElementsReadyListener onSharedElementsReadyListener) {
            this.f650.m1332(list, list2, new C0230.C0231() {
                /* renamed from: ʻ  reason: contains not printable characters */
                public void m1127() {
                    onSharedElementsReadyListener.onSharedElementsReady();
                }
            });
        }
    }
}
