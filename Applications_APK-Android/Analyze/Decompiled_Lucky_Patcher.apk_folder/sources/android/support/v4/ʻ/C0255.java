package android.support.v4.ʻ;

import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: android.support.v4.ʻ.ــ  reason: contains not printable characters */
/* compiled from: OneShotPreDrawListener */
class C0255 implements View.OnAttachStateChangeListener, ViewTreeObserver.OnPreDrawListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final View f868;

    /* renamed from: ʼ  reason: contains not printable characters */
    private ViewTreeObserver f869;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Runnable f870;

    private C0255(View view, Runnable runnable) {
        this.f868 = view;
        this.f869 = view.getViewTreeObserver();
        this.f870 = runnable;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0255 m1554(View view, Runnable runnable) {
        C0255 r0 = new C0255(view, runnable);
        view.getViewTreeObserver().addOnPreDrawListener(r0);
        view.addOnAttachStateChangeListener(r0);
        return r0;
    }

    public boolean onPreDraw() {
        m1555();
        this.f870.run();
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1555() {
        if (this.f869.isAlive()) {
            this.f869.removeOnPreDrawListener(this);
        } else {
            this.f868.getViewTreeObserver().removeOnPreDrawListener(this);
        }
        this.f868.removeOnAttachStateChangeListener(this);
    }

    public void onViewAttachedToWindow(View view) {
        this.f869 = view.getViewTreeObserver();
    }

    public void onViewDetachedFromWindow(View view) {
        m1555();
    }
}
