package android.support.v4.ʻ;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.ʻ.C0198;
import android.widget.RemoteViews;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: android.support.v4.ʻ.ʽʽ  reason: contains not printable characters */
/* compiled from: NotificationCompatApi26 */
class C0203 {

    /* renamed from: android.support.v4.ʻ.ʽʽ$ʻ  reason: contains not printable characters */
    /* compiled from: NotificationCompatApi26 */
    public static class C0204 implements C0271, C0272 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private Notification.Builder f683;

        C0204(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z, boolean z2, boolean z3, int i4, CharSequence charSequence4, boolean z4, String str, ArrayList<String> arrayList, Bundle bundle, int i5, int i6, Notification notification2, String str2, boolean z5, String str3, CharSequence[] charSequenceArr, RemoteViews remoteViews2, RemoteViews remoteViews3, RemoteViews remoteViews4, String str4, int i7, String str5, long j, boolean z6, boolean z7, int i8) {
            PendingIntent pendingIntent3;
            Notification notification3 = notification;
            RemoteViews remoteViews5 = remoteViews2;
            RemoteViews remoteViews6 = remoteViews3;
            RemoteViews remoteViews7 = remoteViews4;
            String str6 = str4;
            boolean z8 = true;
            Notification.Builder deleteIntent = new Notification.Builder(context, str6).setWhen(notification3.when).setShowWhen(z2).setSmallIcon(notification3.icon, notification3.iconLevel).setContent(notification3.contentView).setTicker(notification3.tickerText, remoteViews).setSound(notification3.sound, notification3.audioStreamType).setVibrate(notification3.vibrate).setLights(notification3.ledARGB, notification3.ledOnMS, notification3.ledOffMS).setOngoing((notification3.flags & 2) != 0).setOnlyAlertOnce((notification3.flags & 8) != 0).setAutoCancel((notification3.flags & 16) != 0).setDefaults(notification3.defaults).setContentTitle(charSequence).setContentText(charSequence2).setSubText(charSequence4).setContentInfo(charSequence3).setContentIntent(pendingIntent).setDeleteIntent(notification3.deleteIntent);
            if ((notification3.flags & 128) != 0) {
                pendingIntent3 = pendingIntent2;
            } else {
                pendingIntent3 = pendingIntent2;
                z8 = false;
            }
            this.f683 = deleteIntent.setFullScreenIntent(pendingIntent3, z8).setLargeIcon(bitmap).setNumber(i).setUsesChronometer(z3).setPriority(i4).setProgress(i2, i3, z).setLocalOnly(z4).setExtras(bundle).setGroup(str2).setGroupSummary(z5).setSortKey(str3).setCategory(str).setColor(i5).setVisibility(i6).setPublicVersion(notification2).setRemoteInputHistory(charSequenceArr).setChannelId(str6).setBadgeIconType(i7).setShortcutId(str5).setTimeoutAfter(j).setGroupAlertBehavior(i8);
            if (z7) {
                this.f683.setColorized(z6);
            }
            if (remoteViews5 != null) {
                this.f683.setCustomContentView(remoteViews5);
            }
            if (remoteViews6 != null) {
                this.f683.setCustomBigContentView(remoteViews6);
            }
            if (remoteViews7 != null) {
                this.f683.setCustomHeadsUpContentView(remoteViews7);
            }
            Iterator<String> it = arrayList.iterator();
            while (it.hasNext()) {
                this.f683.addPerson(it.next());
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1165(C0198.C0199 r2) {
            C0195.m1128(this.f683, r2);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Notification m1164() {
            return this.f683.build();
        }
    }
}
