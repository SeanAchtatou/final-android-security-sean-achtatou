package android.support.v4.ʻ;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: android.support.v4.ʻ.ٴ  reason: contains not printable characters */
/* compiled from: FragmentManager */
final class C0256 implements Parcelable {
    public static final Parcelable.Creator<C0256> CREATOR = new Parcelable.Creator<C0256>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public C0256 createFromParcel(Parcel parcel) {
            return new C0256(parcel);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0256[] newArray(int i) {
            return new C0256[i];
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    C0257[] f871;

    /* renamed from: ʼ  reason: contains not printable characters */
    int[] f872;

    /* renamed from: ʽ  reason: contains not printable characters */
    C0205[] f873;

    /* renamed from: ʾ  reason: contains not printable characters */
    int f874 = -1;

    /* renamed from: ʿ  reason: contains not printable characters */
    int f875;

    public int describeContents() {
        return 0;
    }

    public C0256() {
    }

    public C0256(Parcel parcel) {
        this.f871 = (C0257[]) parcel.createTypedArray(C0257.CREATOR);
        this.f872 = parcel.createIntArray();
        this.f873 = (C0205[]) parcel.createTypedArray(C0205.CREATOR);
        this.f874 = parcel.readInt();
        this.f875 = parcel.readInt();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedArray(this.f871, i);
        parcel.writeIntArray(this.f872);
        parcel.writeTypedArray(this.f873, i);
        parcel.writeInt(this.f874);
        parcel.writeInt(this.f875);
    }
}
