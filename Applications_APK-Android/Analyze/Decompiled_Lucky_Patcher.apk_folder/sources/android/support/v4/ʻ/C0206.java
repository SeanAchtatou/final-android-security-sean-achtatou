package android.support.v4.ʻ;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.ʻ.C0198;
import android.util.SparseArray;
import android.widget.RemoteViews;
import java.util.ArrayList;
import java.util.List;

/* renamed from: android.support.v4.ʻ.ʾʾ  reason: contains not printable characters */
/* compiled from: NotificationCompatKitKat */
class C0206 {

    /* renamed from: android.support.v4.ʻ.ʾʾ$ʻ  reason: contains not printable characters */
    /* compiled from: NotificationCompatKitKat */
    public static class C0207 implements C0271, C0272 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private Notification.Builder f696;

        /* renamed from: ʼ  reason: contains not printable characters */
        private Bundle f697;

        /* renamed from: ʽ  reason: contains not printable characters */
        private List<Bundle> f698 = new ArrayList();

        /* renamed from: ʾ  reason: contains not printable characters */
        private RemoteViews f699;

        /* renamed from: ʿ  reason: contains not printable characters */
        private RemoteViews f700;

        public C0207(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z, boolean z2, boolean z3, int i4, CharSequence charSequence4, boolean z4, ArrayList<String> arrayList, Bundle bundle, String str, boolean z5, String str2, RemoteViews remoteViews2, RemoteViews remoteViews3) {
            PendingIntent pendingIntent3;
            Notification notification2 = notification;
            ArrayList<String> arrayList2 = arrayList;
            Bundle bundle2 = bundle;
            String str3 = str;
            String str4 = str2;
            boolean z6 = false;
            Notification.Builder deleteIntent = new Notification.Builder(context).setWhen(notification2.when).setShowWhen(z2).setSmallIcon(notification2.icon, notification2.iconLevel).setContent(notification2.contentView).setTicker(notification2.tickerText, remoteViews).setSound(notification2.sound, notification2.audioStreamType).setVibrate(notification2.vibrate).setLights(notification2.ledARGB, notification2.ledOnMS, notification2.ledOffMS).setOngoing((notification2.flags & 2) != 0).setOnlyAlertOnce((notification2.flags & 8) != 0).setAutoCancel((notification2.flags & 16) != 0).setDefaults(notification2.defaults).setContentTitle(charSequence).setContentText(charSequence2).setSubText(charSequence4).setContentInfo(charSequence3).setContentIntent(pendingIntent).setDeleteIntent(notification2.deleteIntent);
            if ((notification2.flags & 128) != 0) {
                pendingIntent3 = pendingIntent2;
                z6 = true;
            } else {
                pendingIntent3 = pendingIntent2;
            }
            this.f696 = deleteIntent.setFullScreenIntent(pendingIntent3, z6).setLargeIcon(bitmap).setNumber(i).setUsesChronometer(z3).setPriority(i4).setProgress(i2, i3, z);
            this.f697 = new Bundle();
            if (bundle2 != null) {
                this.f697.putAll(bundle2);
            }
            if (arrayList2 != null && !arrayList.isEmpty()) {
                this.f697.putStringArray("android.people", (String[]) arrayList2.toArray(new String[arrayList.size()]));
            }
            if (z4) {
                this.f697.putBoolean("android.support.localOnly", true);
            }
            if (str3 != null) {
                this.f697.putString("android.support.groupKey", str3);
                if (z5) {
                    this.f697.putBoolean("android.support.isGroupSummary", true);
                } else {
                    this.f697.putBoolean("android.support.useSideChannel", true);
                }
            }
            if (str4 != null) {
                this.f697.putString("android.support.sortKey", str4);
            }
            this.f699 = remoteViews2;
            this.f700 = remoteViews3;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1170(C0198.C0199 r3) {
            this.f698.add(C0209.m1171(this.f696, r3));
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Notification m1169() {
            SparseArray<Bundle> r0 = C0209.m1173(this.f698);
            if (r0 != null) {
                this.f697.putSparseParcelableArray("android.support.actionExtras", r0);
            }
            this.f696.setExtras(this.f697);
            Notification build = this.f696.build();
            RemoteViews remoteViews = this.f699;
            if (remoteViews != null) {
                build.contentView = remoteViews;
            }
            RemoteViews remoteViews2 = this.f700;
            if (remoteViews2 != null) {
                build.bigContentView = remoteViews2;
            }
            return build;
        }
    }
}
