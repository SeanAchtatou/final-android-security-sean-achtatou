package android.support.v4.ʻ;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.ʻ.C0195;
import android.support.v4.ʻ.C0198;
import android.support.v4.ʻ.C0203;
import android.support.v4.ʻ.C0206;
import android.support.v4.ʻ.C0209;
import android.support.v4.ʻ.C0258;
import android.support.v4.ʻ.C0261;
import android.widget.RemoteViews;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: android.support.v4.ʻ.ﾞﾞ  reason: contains not printable characters */
/* compiled from: NotificationCompat */
public class C0273 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0285 f972;

    /* renamed from: android.support.v4.ʻ.ﾞﾞ$ˎ  reason: contains not printable characters */
    /* compiled from: NotificationCompat */
    interface C0285 {
        /* renamed from: ʻ  reason: contains not printable characters */
        Notification m1689(C0275 r1, C0276 r2);
    }

    /* renamed from: android.support.v4.ʻ.ﾞﾞ$ˏ  reason: contains not printable characters */
    /* compiled from: NotificationCompat */
    public static abstract class C0286 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1690(Bundle bundle) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1691(C0272 r1) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public RemoteViews m1692(C0272 r1) {
            return null;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public RemoteViews m1693(C0272 r1) {
            return null;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public RemoteViews m1694(C0272 r1) {
            return null;
        }
    }

    /* renamed from: android.support.v4.ʻ.ﾞﾞ$ʽ  reason: contains not printable characters */
    /* compiled from: NotificationCompat */
    protected static class C0276 {
        protected C0276() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Notification m1680(C0275 r4, C0272 r5) {
            RemoteViews r42;
            RemoteViews r0;
            RemoteViews r02 = r4.f1004 != null ? r4.f1004.m1692(r5) : null;
            Notification r1 = r5.m1660();
            if (r02 != null) {
                r1.contentView = r02;
            } else if (r4.f984 != null) {
                r1.contentView = r4.f984;
            }
            if (!(Build.VERSION.SDK_INT < 16 || r4.f1004 == null || (r0 = r4.f1004.m1693(r5)) == null)) {
                r1.bigContentView = r0;
            }
            if (!(Build.VERSION.SDK_INT < 21 || r4.f1004 == null || (r42 = r4.f1004.m1694(r5)) == null)) {
                r1.headsUpContentView = r42;
            }
            return r1;
        }
    }

    /* renamed from: android.support.v4.ʻ.ﾞﾞ$ˋ  reason: contains not printable characters */
    /* compiled from: NotificationCompat */
    static class C0283 implements C0285 {
        C0283() {
        }

        /* renamed from: android.support.v4.ʻ.ﾞﾞ$ˋ$ʻ  reason: contains not printable characters */
        /* compiled from: NotificationCompat */
        public static class C0284 implements C0272 {

            /* renamed from: ʻ  reason: contains not printable characters */
            private Notification.Builder f1021;

            C0284(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z) {
                Notification notification2 = notification;
                boolean z2 = true;
                Notification.Builder deleteIntent = new Notification.Builder(context).setWhen(notification2.when).setSmallIcon(notification2.icon, notification2.iconLevel).setContent(notification2.contentView).setTicker(notification2.tickerText, remoteViews).setSound(notification2.sound, notification2.audioStreamType).setVibrate(notification2.vibrate).setLights(notification2.ledARGB, notification2.ledOnMS, notification2.ledOffMS).setOngoing((notification2.flags & 2) != 0).setOnlyAlertOnce((notification2.flags & 8) != 0).setAutoCancel((notification2.flags & 16) != 0).setDefaults(notification2.defaults).setContentTitle(charSequence).setContentText(charSequence2).setContentInfo(charSequence3).setContentIntent(pendingIntent).setDeleteIntent(notification2.deleteIntent);
                if ((notification2.flags & 128) == 0) {
                    z2 = false;
                }
                this.f1021 = deleteIntent.setFullScreenIntent(pendingIntent2, z2).setLargeIcon(bitmap).setNumber(i).setProgress(i2, i3, z);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public Notification m1688() {
                return this.f1021.getNotification();
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Notification m1687(C0275 r17, C0276 r18) {
            C0275 r0 = r17;
            return r18.m1680(r0, new C0284(r0.f981, r0.f1000, r0.f983, r0.f985, r0.f995, r0.f991, r0.f997, r0.f987, r0.f989, r0.f993, r0.f1008, r0.f1009, r0.f1011));
        }
    }

    /* renamed from: android.support.v4.ʻ.ﾞﾞ$ʾ  reason: contains not printable characters */
    /* compiled from: NotificationCompat */
    static class C0277 extends C0283 {
        C0277() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Notification m1681(C0275 r28, C0276 r29) {
            Bundle r2;
            C0275 r0 = r28;
            C0209.C0210 r25 = r1;
            C0209.C0210 r1 = new C0209.C0210(r0.f981, r0.f1000, r0.f983, r0.f985, r0.f995, r0.f991, r0.f997, r0.f987, r0.f989, r0.f993, r0.f1008, r0.f1009, r0.f1011, r0.f1002, r0.f999, r0.f1005, r0.f1017, r0.f1010, r0.f1013, r0.f1014, r0.f1015, r0.f984, r0.f990);
            C0209.C0210 r22 = r25;
            C0273.m1662(r22, r0.f1016);
            if (r0.f1004 != null) {
                r0.f1004.m1691(r22);
            }
            Notification r12 = r29.m1680(r0, r22);
            if (!(r0.f1004 == null || (r2 = C0273.m1661(r12)) == null)) {
                r0.f1004.m1690(r2);
            }
            return r12;
        }
    }

    /* renamed from: android.support.v4.ʻ.ﾞﾞ$ʿ  reason: contains not printable characters */
    /* compiled from: NotificationCompat */
    static class C0278 extends C0277 {
        C0278() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Notification m1682(C0275 r30, C0276 r31) {
            C0275 r0 = r30;
            C0206.C0207 r27 = r1;
            C0206.C0207 r1 = new C0206.C0207(r0.f981, r0.f1000, r0.f983, r0.f985, r0.f995, r0.f991, r0.f997, r0.f987, r0.f989, r0.f993, r0.f1008, r0.f1009, r0.f1011, r0.f1001, r0.f1002, r0.f999, r0.f1005, r0.f1017, r0.f998, r0.f1010, r0.f1013, r0.f1014, r0.f1015, r0.f984, r0.f990);
            C0206.C0207 r2 = r27;
            C0273.m1662(r2, r0.f1016);
            if (r0.f1004 != null) {
                r0.f1004.m1691(r2);
            }
            return r31.m1680(r0, r2);
        }
    }

    /* renamed from: android.support.v4.ʻ.ﾞﾞ$ˆ  reason: contains not printable characters */
    /* compiled from: NotificationCompat */
    static class C0279 extends C0278 {
        C0279() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Notification m1683(C0275 r31, C0276 r32) {
            C0275 r0 = r31;
            C0258.C0259 r28 = r1;
            C0258.C0259 r1 = new C0258.C0259(r0.f981, r0.f1000, r0.f983, r0.f985, r0.f995, r0.f991, r0.f997, r0.f987, r0.f989, r0.f993, r0.f1008, r0.f1009, r0.f1011, r0.f1001, r0.f1002, r0.f999, r0.f1005, r0.f1017, r0.f998, r0.f1010, r0.f1013, r0.f1014, r0.f1015, r0.f984, r0.f990, r31.f1003);
            C0258.C0259 r2 = r28;
            C0273.m1662(r2, r0.f1016);
            if (r0.f1004 != null) {
                r0.f1004.m1691(r2);
            }
            Notification r12 = r32.m1680(r0, r2);
            if (r0.f1004 != null) {
                r0.f1004.m1690(C0273.m1661(r12));
            }
            return r12;
        }
    }

    /* renamed from: android.support.v4.ʻ.ﾞﾞ$ˈ  reason: contains not printable characters */
    /* compiled from: NotificationCompat */
    static class C0280 extends C0279 {
        C0280() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Notification m1684(C0275 r36, C0276 r37) {
            C0275 r0 = r36;
            C0261.C0262 r33 = r1;
            C0261.C0262 r1 = new C0261.C0262(r0.f981, r0.f1000, r0.f983, r0.f985, r0.f995, r0.f991, r0.f997, r0.f987, r0.f989, r0.f993, r0.f1008, r0.f1009, r0.f1011, r0.f1001, r0.f1002, r0.f999, r0.f1005, r0.f1017, r0.f1020, r0.f998, r0.f1010, r0.f1012, r0.f982, r0.f986, r0.f1013, r0.f1014, r0.f1015, r0.f984, r0.f990, r0.f988, r36.f1003);
            C0261.C0262 r2 = r33;
            C0273.m1662(r2, r0.f1016);
            if (r0.f1004 != null) {
                r0.f1004.m1691(r2);
            }
            Notification r12 = r37.m1680(r0, r2);
            if (r0.f1004 != null) {
                r0.f1004.m1690(C0273.m1661(r12));
            }
            return r12;
        }
    }

    /* renamed from: android.support.v4.ʻ.ﾞﾞ$ˉ  reason: contains not printable characters */
    /* compiled from: NotificationCompat */
    static class C0281 extends C0280 {
        C0281() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Notification m1685(C0275 r37, C0276 r38) {
            C0275 r0 = r37;
            C0195.C0196 r34 = r1;
            C0195.C0196 r1 = new C0195.C0196(r0.f981, r0.f1000, r0.f983, r0.f985, r0.f995, r0.f991, r0.f997, r0.f987, r0.f989, r0.f993, r0.f1008, r0.f1009, r0.f1011, r0.f1001, r0.f1002, r0.f999, r0.f1005, r0.f1017, r0.f1020, r0.f998, r0.f1010, r0.f1012, r0.f982, r0.f986, r0.f1013, r0.f1014, r0.f1015, r0.f1006, r0.f984, r0.f990, r0.f988, r37.f1003);
            C0195.C0196 r2 = r34;
            C0273.m1662(r2, r0.f1016);
            if (r0.f1004 != null) {
                r0.f1004.m1691(r2);
            }
            Notification r12 = r38.m1680(r0, r2);
            if (r0.f1004 != null) {
                r0.f1004.m1690(C0273.m1661(r12));
            }
            return r12;
        }
    }

    /* renamed from: android.support.v4.ʻ.ﾞﾞ$ˊ  reason: contains not printable characters */
    /* compiled from: NotificationCompat */
    static class C0282 extends C0281 {
        C0282() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Notification m1686(C0275 r45, C0276 r46) {
            C0275 r0 = r45;
            C0203.C0204 r41 = r1;
            C0203.C0204 r1 = new C0203.C0204(r0.f981, r0.f1000, r0.f983, r0.f985, r0.f995, r0.f991, r0.f997, r0.f987, r0.f989, r0.f993, r0.f1008, r0.f1009, r0.f1011, r0.f1001, r0.f1002, r0.f999, r0.f1005, r0.f1017, r0.f1020, r0.f998, r0.f1010, r0.f1012, r0.f982, r0.f986, r0.f1013, r0.f1014, r0.f1015, r0.f1006, r0.f984, r0.f990, r0.f988, r0.f1007, r0.f992, r0.f996, r0.f994, r0.f1018, r0.f1019, r45.f1003);
            C0203.C0204 r2 = r41;
            C0273.m1662(r2, r0.f1016);
            if (r0.f1004 != null) {
                r0.f1004.m1691(r2);
            }
            Notification r12 = r46.m1680(r0, r2);
            if (r0.f1004 != null) {
                r0.f1004.m1690(C0273.m1661(r12));
            }
            return r12;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m1662(C0271 r1, ArrayList<C0274> arrayList) {
        Iterator<C0274> it = arrayList.iterator();
        while (it.hasNext()) {
            r1.m1659(it.next());
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 26) {
            f972 = new C0282();
        } else if (Build.VERSION.SDK_INT >= 24) {
            f972 = new C0281();
        } else if (Build.VERSION.SDK_INT >= 21) {
            f972 = new C0280();
        } else if (Build.VERSION.SDK_INT >= 20) {
            f972 = new C0279();
        } else if (Build.VERSION.SDK_INT >= 19) {
            f972 = new C0278();
        } else if (Build.VERSION.SDK_INT >= 16) {
            f972 = new C0277();
        } else {
            f972 = new C0283();
        }
    }

    /* renamed from: android.support.v4.ʻ.ﾞﾞ$ʼ  reason: contains not printable characters */
    /* compiled from: NotificationCompat */
    public static class C0275 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public Context f981;

        /* renamed from: ʻʻ  reason: contains not printable characters */
        int f982;

        /* renamed from: ʼ  reason: contains not printable characters */
        public CharSequence f983;

        /* renamed from: ʼʼ  reason: contains not printable characters */
        RemoteViews f984;

        /* renamed from: ʽ  reason: contains not printable characters */
        public CharSequence f985;

        /* renamed from: ʽʽ  reason: contains not printable characters */
        Notification f986;

        /* renamed from: ʾ  reason: contains not printable characters */
        PendingIntent f987;

        /* renamed from: ʾʾ  reason: contains not printable characters */
        RemoteViews f988;

        /* renamed from: ʿ  reason: contains not printable characters */
        PendingIntent f989;

        /* renamed from: ʿʿ  reason: contains not printable characters */
        RemoteViews f990;

        /* renamed from: ˆ  reason: contains not printable characters */
        RemoteViews f991;

        /* renamed from: ˆˆ  reason: contains not printable characters */
        int f992;

        /* renamed from: ˈ  reason: contains not printable characters */
        public Bitmap f993;

        /* renamed from: ˈˈ  reason: contains not printable characters */
        long f994;

        /* renamed from: ˉ  reason: contains not printable characters */
        public CharSequence f995;

        /* renamed from: ˉˉ  reason: contains not printable characters */
        String f996;

        /* renamed from: ˊ  reason: contains not printable characters */
        public int f997;

        /* renamed from: ˊˊ  reason: contains not printable characters */
        public ArrayList<String> f998;

        /* renamed from: ˋ  reason: contains not printable characters */
        int f999;

        /* renamed from: ˋˋ  reason: contains not printable characters */
        public Notification f1000;

        /* renamed from: ˎ  reason: contains not printable characters */
        boolean f1001;

        /* renamed from: ˏ  reason: contains not printable characters */
        public boolean f1002;
        /* access modifiers changed from: private */

        /* renamed from: ˏˏ  reason: contains not printable characters */
        public int f1003;

        /* renamed from: ˑ  reason: contains not printable characters */
        public C0286 f1004;

        /* renamed from: י  reason: contains not printable characters */
        public CharSequence f1005;

        /* renamed from: ـ  reason: contains not printable characters */
        public CharSequence[] f1006;

        /* renamed from: ــ  reason: contains not printable characters */
        String f1007;

        /* renamed from: ٴ  reason: contains not printable characters */
        int f1008;

        /* renamed from: ᐧ  reason: contains not printable characters */
        int f1009;

        /* renamed from: ᐧᐧ  reason: contains not printable characters */
        Bundle f1010;

        /* renamed from: ᴵ  reason: contains not printable characters */
        boolean f1011;

        /* renamed from: ᴵᴵ  reason: contains not printable characters */
        int f1012;

        /* renamed from: ᵎ  reason: contains not printable characters */
        String f1013;

        /* renamed from: ᵔ  reason: contains not printable characters */
        boolean f1014;

        /* renamed from: ᵢ  reason: contains not printable characters */
        String f1015;

        /* renamed from: ⁱ  reason: contains not printable characters */
        public ArrayList<C0274> f1016;

        /* renamed from: ﹳ  reason: contains not printable characters */
        boolean f1017;

        /* renamed from: ﹶ  reason: contains not printable characters */
        boolean f1018;

        /* renamed from: ﾞ  reason: contains not printable characters */
        boolean f1019;

        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        String f1020;

        public C0275(Context context, String str) {
            this.f1001 = true;
            this.f1016 = new ArrayList<>();
            this.f1017 = false;
            this.f1012 = 0;
            this.f982 = 0;
            this.f992 = 0;
            this.f1003 = 0;
            this.f1000 = new Notification();
            this.f981 = context;
            this.f1007 = str;
            this.f1000.when = System.currentTimeMillis();
            this.f1000.audioStreamType = -1;
            this.f999 = 0;
            this.f998 = new ArrayList<>();
        }

        @Deprecated
        public C0275(Context context) {
            this(context, null);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0275 m1675(int i) {
            this.f1000.icon = i;
            return this;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0275 m1677(CharSequence charSequence) {
            this.f983 = m1673(charSequence);
            return this;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public C0275 m1678(CharSequence charSequence) {
            this.f985 = m1673(charSequence);
            return this;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0275 m1676(PendingIntent pendingIntent) {
            this.f987 = pendingIntent;
            return this;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Notification m1674() {
            return C0273.f972.m1689(this, m1679());
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʼ  reason: contains not printable characters */
        public C0276 m1679() {
            return new C0276();
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        protected static CharSequence m1673(CharSequence charSequence) {
            return (charSequence != null && charSequence.length() > 5120) ? charSequence.subSequence(0, 5120) : charSequence;
        }
    }

    /* renamed from: android.support.v4.ʻ.ﾞﾞ$ʻ  reason: contains not printable characters */
    /* compiled from: NotificationCompat */
    public static class C0274 extends C0198.C0199 {

        /* renamed from: ʿ  reason: contains not printable characters */
        public static final C0198.C0199.C0200 f973 = new C0198.C0199.C0200() {
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        final Bundle f974;

        /* renamed from: ʼ  reason: contains not printable characters */
        public int f975;

        /* renamed from: ʽ  reason: contains not printable characters */
        public CharSequence f976;

        /* renamed from: ʾ  reason: contains not printable characters */
        public PendingIntent f977;

        /* renamed from: ˆ  reason: contains not printable characters */
        private final C0212[] f978;

        /* renamed from: ˈ  reason: contains not printable characters */
        private final C0212[] f979;

        /* renamed from: ˉ  reason: contains not printable characters */
        private boolean f980;

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m1663() {
            return this.f975;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public CharSequence m1664() {
            return this.f976;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public PendingIntent m1665() {
            return this.f977;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public Bundle m1666() {
            return this.f974;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public boolean m1667() {
            return this.f980;
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public C0212[] m1671() {
            return this.f978;
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public C0212[] m1670() {
            return this.f979;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Bundle m1661(Notification notification) {
        if (Build.VERSION.SDK_INT >= 19) {
            return notification.extras;
        }
        if (Build.VERSION.SDK_INT >= 16) {
            return C0209.m1172(notification);
        }
        return null;
    }
}
