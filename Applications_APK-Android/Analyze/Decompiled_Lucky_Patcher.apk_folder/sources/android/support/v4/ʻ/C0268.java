package android.support.v4.ʻ;

import android.os.Bundle;
import android.support.v4.content.C0109;
import android.support.v4.ʻ.C0266;
import android.support.v4.ˈ.C0334;
import android.support.v4.ˈ.C0354;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;

/* renamed from: android.support.v4.ʻ.ⁱ  reason: contains not printable characters */
/* compiled from: LoaderManager */
class C0268 extends C0266 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static boolean f950 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    final C0354<C0269> f951 = new C0354<>();

    /* renamed from: ʽ  reason: contains not printable characters */
    final C0354<C0269> f952 = new C0354<>();

    /* renamed from: ʾ  reason: contains not printable characters */
    final String f953;

    /* renamed from: ʿ  reason: contains not printable characters */
    boolean f954;

    /* renamed from: ˆ  reason: contains not printable characters */
    boolean f955;

    /* renamed from: ˈ  reason: contains not printable characters */
    C0237 f956;

    /* renamed from: android.support.v4.ʻ.ⁱ$ʻ  reason: contains not printable characters */
    /* compiled from: LoaderManager */
    final class C0269 implements C0109.C0110<Object>, C0109.C0111<Object> {

        /* renamed from: ʻ  reason: contains not printable characters */
        final int f957;

        /* renamed from: ʼ  reason: contains not printable characters */
        final Bundle f958;

        /* renamed from: ʽ  reason: contains not printable characters */
        C0266.C0267<Object> f959;

        /* renamed from: ʾ  reason: contains not printable characters */
        C0109<Object> f960;

        /* renamed from: ʿ  reason: contains not printable characters */
        boolean f961;

        /* renamed from: ˆ  reason: contains not printable characters */
        boolean f962;

        /* renamed from: ˈ  reason: contains not printable characters */
        Object f963;

        /* renamed from: ˉ  reason: contains not printable characters */
        boolean f964;

        /* renamed from: ˊ  reason: contains not printable characters */
        boolean f965;

        /* renamed from: ˋ  reason: contains not printable characters */
        boolean f966;

        /* renamed from: ˎ  reason: contains not printable characters */
        boolean f967;

        /* renamed from: ˏ  reason: contains not printable characters */
        boolean f968;

        /* renamed from: ˑ  reason: contains not printable characters */
        boolean f969;

        /* renamed from: י  reason: contains not printable characters */
        C0269 f970;

        /* renamed from: ـ  reason: contains not printable characters */
        final /* synthetic */ C0268 f971;

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1645() {
            C0266.C0267<Object> r0;
            if (this.f965 && this.f966) {
                this.f964 = true;
            } else if (!this.f964) {
                this.f964 = true;
                if (C0268.f950) {
                    Log.v("LoaderManager", "  Starting: " + this);
                }
                if (this.f960 == null && (r0 = this.f959) != null) {
                    this.f960 = r0.m1632(this.f957, this.f958);
                }
                C0109<Object> r02 = this.f960;
                if (r02 == null) {
                    return;
                }
                if (!r02.getClass().isMemberClass() || Modifier.isStatic(this.f960.getClass().getModifiers())) {
                    if (!this.f969) {
                        this.f960.m575(this.f957, this);
                        this.f960.m576((C0109.C0110<Object>) this);
                        this.f969 = true;
                    }
                    this.f960.m574();
                    return;
                }
                throw new IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + this.f960);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m1648() {
            if (C0268.f950) {
                Log.v("LoaderManager", "  Retaining: " + this);
            }
            this.f965 = true;
            this.f966 = this.f964;
            this.f964 = false;
            this.f959 = null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public void m1649() {
            if (this.f965) {
                if (C0268.f950) {
                    Log.v("LoaderManager", "  Finished Retaining: " + this);
                }
                this.f965 = false;
                boolean z = this.f964;
                if (z != this.f966 && !z) {
                    m1651();
                }
            }
            if (this.f964 && this.f961 && !this.f967) {
                m1646(this.f960, this.f963);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʾ  reason: contains not printable characters */
        public void m1650() {
            if (this.f964 && this.f967) {
                this.f967 = false;
                if (this.f961 && !this.f965) {
                    m1646(this.f960, this.f963);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʿ  reason: contains not printable characters */
        public void m1651() {
            C0109<Object> r1;
            if (C0268.f950) {
                Log.v("LoaderManager", "  Stopping: " + this);
            }
            this.f964 = false;
            if (!this.f965 && (r1 = this.f960) != null && this.f969) {
                this.f969 = false;
                r1.m577((C0109.C0111<Object>) this);
                this.f960.m580(this);
                this.f960.m581();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˆ  reason: contains not printable characters */
        public void m1652() {
            String str;
            if (C0268.f950) {
                Log.v("LoaderManager", "  Destroying: " + this);
            }
            this.f968 = true;
            boolean z = this.f962;
            this.f962 = false;
            if (this.f959 != null && this.f960 != null && this.f961 && z) {
                if (C0268.f950) {
                    Log.v("LoaderManager", "  Resetting: " + this);
                }
                if (this.f971.f956 != null) {
                    str = this.f971.f956.f800.f839;
                    this.f971.f956.f800.f839 = "onLoaderReset";
                } else {
                    str = null;
                }
                try {
                    this.f959.m1633(this.f960);
                } finally {
                    if (this.f971.f956 != null) {
                        this.f971.f956.f800.f839 = str;
                    }
                }
            }
            this.f959 = null;
            this.f963 = null;
            this.f961 = false;
            C0109<Object> r0 = this.f960;
            if (r0 != null) {
                if (this.f969) {
                    this.f969 = false;
                    r0.m577((C0109.C0111<Object>) this);
                    this.f960.m580(this);
                }
                this.f960.m583();
            }
            C0269 r02 = this.f970;
            if (r02 != null) {
                r02.m1652();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1646(C0109<Object> r5, Object obj) {
            if (this.f959 != null) {
                String str = null;
                if (this.f971.f956 != null) {
                    str = this.f971.f956.f800.f839;
                    this.f971.f956.f800.f839 = "onLoadFinished";
                }
                try {
                    if (C0268.f950) {
                        Log.v("LoaderManager", "  onLoadFinished in " + r5 + ": " + r5.m573(obj));
                    }
                    this.f959.m1634(r5, obj);
                    this.f962 = true;
                } finally {
                    if (this.f971.f956 != null) {
                        this.f971.f956.f800.f839 = str;
                    }
                }
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(64);
            sb.append("LoaderInfo{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" #");
            sb.append(this.f957);
            sb.append(" : ");
            C0334.m1918(this.f960, sb);
            sb.append("}}");
            return sb.toString();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1647(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            printWriter.print(str);
            printWriter.print("mId=");
            printWriter.print(this.f957);
            printWriter.print(" mArgs=");
            printWriter.println(this.f958);
            printWriter.print(str);
            printWriter.print("mCallbacks=");
            printWriter.println(this.f959);
            printWriter.print(str);
            printWriter.print("mLoader=");
            printWriter.println(this.f960);
            C0109<Object> r0 = this.f960;
            if (r0 != null) {
                r0.m578(str + "  ", fileDescriptor, printWriter, strArr);
            }
            if (this.f961 || this.f962) {
                printWriter.print(str);
                printWriter.print("mHaveData=");
                printWriter.print(this.f961);
                printWriter.print("  mDeliveredData=");
                printWriter.println(this.f962);
                printWriter.print(str);
                printWriter.print("mData=");
                printWriter.println(this.f963);
            }
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.print(this.f964);
            printWriter.print(" mReportNextStart=");
            printWriter.print(this.f967);
            printWriter.print(" mDestroyed=");
            printWriter.println(this.f968);
            printWriter.print(str);
            printWriter.print("mRetaining=");
            printWriter.print(this.f965);
            printWriter.print(" mRetainingStarted=");
            printWriter.print(this.f966);
            printWriter.print(" mListenerRegistered=");
            printWriter.println(this.f969);
            if (this.f970 != null) {
                printWriter.print(str);
                printWriter.println("Pending Loader ");
                printWriter.print(this.f970);
                printWriter.println(":");
                C0269 r02 = this.f970;
                r02.m1647(str + "  ", fileDescriptor, printWriter, strArr);
            }
        }
    }

    C0268(String str, C0237 r3, boolean z) {
        this.f953 = str;
        this.f956 = r3;
        this.f954 = z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1635(C0237 r1) {
        this.f956 = r1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1638() {
        if (f950) {
            Log.v("LoaderManager", "Starting in " + this);
        }
        if (this.f954) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStart when already started: " + this, runtimeException);
            return;
        }
        this.f954 = true;
        for (int r1 = this.f951.m1986() - 1; r1 >= 0; r1--) {
            this.f951.m1993(r1).m1645();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1639() {
        if (f950) {
            Log.v("LoaderManager", "Stopping in " + this);
        }
        if (!this.f954) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStop when not started: " + this, runtimeException);
            return;
        }
        for (int r0 = this.f951.m1986() - 1; r0 >= 0; r0--) {
            this.f951.m1993(r0).m1651();
        }
        this.f954 = false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m1640() {
        if (f950) {
            Log.v("LoaderManager", "Retaining in " + this);
        }
        if (!this.f954) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doRetain when not started: " + this, runtimeException);
            return;
        }
        this.f955 = true;
        this.f954 = false;
        for (int r1 = this.f951.m1986() - 1; r1 >= 0; r1--) {
            this.f951.m1993(r1).m1648();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m1641() {
        if (this.f955) {
            if (f950) {
                Log.v("LoaderManager", "Finished Retaining in " + this);
            }
            this.f955 = false;
            for (int r0 = this.f951.m1986() - 1; r0 >= 0; r0--) {
                this.f951.m1993(r0).m1649();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m1642() {
        for (int r0 = this.f951.m1986() - 1; r0 >= 0; r0--) {
            this.f951.m1993(r0).f967 = true;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m1643() {
        for (int r0 = this.f951.m1986() - 1; r0 >= 0; r0--) {
            this.f951.m1993(r0).m1650();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public void m1644() {
        if (!this.f955) {
            if (f950) {
                Log.v("LoaderManager", "Destroying Active in " + this);
            }
            for (int r0 = this.f951.m1986() - 1; r0 >= 0; r0--) {
                this.f951.m1993(r0).m1652();
            }
            this.f951.m1989();
        }
        if (f950) {
            Log.v("LoaderManager", "Destroying Inactive in " + this);
        }
        for (int r02 = this.f952.m1986() - 1; r02 >= 0; r02--) {
            this.f952.m1993(r02).m1652();
        }
        this.f952.m1989();
        this.f956 = null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        C0334.m1918(this.f956, sb);
        sb.append("}}");
        return sb.toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1636(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (this.f951.m1986() > 0) {
            printWriter.print(str);
            printWriter.println("Active Loaders:");
            String str2 = str + "    ";
            for (int i = 0; i < this.f951.m1986(); i++) {
                C0269 r6 = this.f951.m1993(i);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.f951.m1992(i));
                printWriter.print(": ");
                printWriter.println(r6.toString());
                r6.m1647(str2, fileDescriptor, printWriter, strArr);
            }
        }
        if (this.f952.m1986() > 0) {
            printWriter.print(str);
            printWriter.println("Inactive Loaders:");
            String str3 = str + "    ";
            for (int i2 = 0; i2 < this.f952.m1986(); i2++) {
                C0269 r4 = this.f952.m1993(i2);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.f952.m1992(i2));
                printWriter.print(": ");
                printWriter.println(r4.toString());
                r4.m1647(str3, fileDescriptor, printWriter, strArr);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1637() {
        int r0 = this.f951.m1986();
        boolean z = false;
        for (int i = 0; i < r0; i++) {
            C0269 r4 = this.f951.m1993(i);
            z |= r4.f964 && !r4.f962;
        }
        return z;
    }
}
