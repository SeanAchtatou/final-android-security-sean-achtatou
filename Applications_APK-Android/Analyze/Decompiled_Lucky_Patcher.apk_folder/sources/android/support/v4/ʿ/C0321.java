package android.support.v4.ʿ;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.ʿ.C0318;

/* renamed from: android.support.v4.ʿ.ʼ  reason: contains not printable characters */
/* compiled from: ResultReceiver */
public class C0321 implements Parcelable {
    public static final Parcelable.Creator<C0321> CREATOR = new Parcelable.Creator<C0321>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public C0321 createFromParcel(Parcel parcel) {
            return new C0321(parcel);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0321[] newArray(int i) {
            return new C0321[i];
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    final boolean f1060 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    final Handler f1061 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    C0318 f1062;

    public int describeContents() {
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1837(int i, Bundle bundle) {
    }

    /* renamed from: android.support.v4.ʿ.ʼ$ʼ  reason: contains not printable characters */
    /* compiled from: ResultReceiver */
    class C0323 implements Runnable {

        /* renamed from: ʻ  reason: contains not printable characters */
        final int f1064;

        /* renamed from: ʼ  reason: contains not printable characters */
        final Bundle f1065;

        C0323(int i, Bundle bundle) {
            this.f1064 = i;
            this.f1065 = bundle;
        }

        public void run() {
            C0321.this.m1837(this.f1064, this.f1065);
        }
    }

    /* renamed from: android.support.v4.ʿ.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: ResultReceiver */
    class C0322 extends C0318.C0319 {
        C0322() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1840(int i, Bundle bundle) {
            if (C0321.this.f1061 != null) {
                C0321.this.f1061.post(new C0323(i, bundle));
            } else {
                C0321.this.m1837(i, bundle);
            }
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        synchronized (this) {
            if (this.f1062 == null) {
                this.f1062 = new C0322();
            }
            parcel.writeStrongBinder(this.f1062.asBinder());
        }
    }

    C0321(Parcel parcel) {
        this.f1062 = C0318.C0319.m1835(parcel.readStrongBinder());
    }
}
