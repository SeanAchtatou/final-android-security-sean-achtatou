package android.support.v4.ʿ;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: android.support.v4.ʿ.ʻ  reason: contains not printable characters */
/* compiled from: IResultReceiver */
public interface C0318 extends IInterface {
    /* renamed from: ʻ  reason: contains not printable characters */
    void m1834(int i, Bundle bundle);

    /* renamed from: android.support.v4.ʿ.ʻ$ʻ  reason: contains not printable characters */
    /* compiled from: IResultReceiver */
    public static abstract class C0319 extends Binder implements C0318 {
        public IBinder asBinder() {
            return this;
        }

        public C0319() {
            attachInterface(this, "android.support.v4.os.IResultReceiver");
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static C0318 m1835(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("android.support.v4.os.IResultReceiver");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof C0318)) {
                return new C0320(iBinder);
            }
            return (C0318) queryLocalInterface;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            if (i == 1) {
                parcel.enforceInterface("android.support.v4.os.IResultReceiver");
                m1834(parcel.readInt(), parcel.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(parcel) : null);
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("android.support.v4.os.IResultReceiver");
                return true;
            }
        }

        /* renamed from: android.support.v4.ʿ.ʻ$ʻ$ʻ  reason: contains not printable characters */
        /* compiled from: IResultReceiver */
        private static class C0320 implements C0318 {

            /* renamed from: ʻ  reason: contains not printable characters */
            private IBinder f1059;

            C0320(IBinder iBinder) {
                this.f1059 = iBinder;
            }

            public IBinder asBinder() {
                return this.f1059;
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m1836(int i, Bundle bundle) {
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.os.IResultReceiver");
                    obtain.writeInt(i);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f1059.transact(1, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }
        }
    }
}
