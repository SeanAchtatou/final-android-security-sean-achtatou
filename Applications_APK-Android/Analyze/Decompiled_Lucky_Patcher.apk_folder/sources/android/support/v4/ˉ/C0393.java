package android.support.v4.ˉ;

import android.content.Context;
import android.util.Log;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

/* renamed from: android.support.v4.ˉ.ʽ  reason: contains not printable characters */
/* compiled from: ActionProvider */
public abstract class C0393 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Context f1240;

    /* renamed from: ʼ  reason: contains not printable characters */
    private C0394 f1241;

    /* renamed from: ʽ  reason: contains not printable characters */
    private C0395 f1242;

    /* renamed from: android.support.v4.ˉ.ʽ$ʻ  reason: contains not printable characters */
    /* compiled from: ActionProvider */
    public interface C0394 {
        /* renamed from: ʼ  reason: contains not printable characters */
        void m2153(boolean z);
    }

    /* renamed from: android.support.v4.ˉ.ʽ$ʼ  reason: contains not printable characters */
    /* compiled from: ActionProvider */
    public interface C0395 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m2154(boolean z);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract View m2142();

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2146(SubMenu subMenu) {
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m2148() {
        return false;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m2149() {
        return true;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m2150() {
        return false;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m2151() {
        return false;
    }

    public C0393(Context context) {
        this.f1240 = context;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public View m2143(MenuItem menuItem) {
        return m2142();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2147(boolean z) {
        C0394 r0 = this.f1241;
        if (r0 != null) {
            r0.m2153(z);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2144(C0394 r1) {
        this.f1241 = r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2145(C0395 r3) {
        if (!(this.f1242 == null || r3 == null)) {
            Log.w("ActionProvider(support)", "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this " + getClass().getSimpleName() + " instance while it is still in use somewhere else?");
        }
        this.f1242 = r3;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m2152() {
        this.f1242 = null;
        this.f1241 = null;
    }
}
