package android.support.v4.ˉ;

import android.os.Build;
import android.view.ViewGroup;

/* renamed from: android.support.v4.ˉ.ᵎ  reason: contains not printable characters */
/* compiled from: ViewGroupCompat */
public final class C0426 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0429 f1267;

    /* renamed from: android.support.v4.ˉ.ᵎ$ʽ  reason: contains not printable characters */
    /* compiled from: ViewGroupCompat */
    static class C0429 {
        C0429() {
        }
    }

    /* renamed from: android.support.v4.ˉ.ᵎ$ʻ  reason: contains not printable characters */
    /* compiled from: ViewGroupCompat */
    static class C0427 extends C0429 {
        C0427() {
        }
    }

    /* renamed from: android.support.v4.ˉ.ᵎ$ʼ  reason: contains not printable characters */
    /* compiled from: ViewGroupCompat */
    static class C0428 extends C0427 {
        C0428() {
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 21) {
            f1267 = new C0428();
        } else if (Build.VERSION.SDK_INT >= 18) {
            f1267 = new C0427();
        } else {
            f1267 = new C0429();
        }
    }

    @Deprecated
    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2352(ViewGroup viewGroup, boolean z) {
        viewGroup.setMotionEventSplittingEnabled(z);
    }
}
