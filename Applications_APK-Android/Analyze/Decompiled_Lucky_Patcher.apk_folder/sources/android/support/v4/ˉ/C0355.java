package android.support.v4.ˉ;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: android.support.v4.ˉ.ʻ  reason: contains not printable characters */
/* compiled from: AbsSavedState */
public abstract class C0355 implements Parcelable {
    public static final Parcelable.Creator<C0355> CREATOR = new Parcelable.ClassLoaderCreator<C0355>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public C0355 createFromParcel(Parcel parcel, ClassLoader classLoader) {
            if (parcel.readParcelable(classLoader) == null) {
                return C0355.f1179;
            }
            throw new IllegalStateException("superState must be null");
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0355 createFromParcel(Parcel parcel) {
            return createFromParcel(parcel, null);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0355[] newArray(int i) {
            return new C0355[i];
        }
    };

    /* renamed from: ʾ  reason: contains not printable characters */
    public static final C0355 f1179 = new C0355() {
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Parcelable f1180;

    public int describeContents() {
        return 0;
    }

    private C0355() {
        this.f1180 = null;
    }

    protected C0355(Parcelable parcelable) {
        if (parcelable != null) {
            this.f1180 = parcelable == f1179 ? null : parcelable;
            return;
        }
        throw new IllegalArgumentException("superState must not be null");
    }

    protected C0355(Parcel parcel, ClassLoader classLoader) {
        Parcelable readParcelable = parcel.readParcelable(classLoader);
        this.f1180 = readParcelable == null ? f1179 : readParcelable;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final Parcelable m1995() {
        return this.f1180;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.f1180, i);
    }
}
