package android.support.v4.ˉ;

import android.graphics.Rect;
import android.os.Build;
import android.view.Gravity;

/* renamed from: android.support.v4.ˉ.ʾ  reason: contains not printable characters */
/* compiled from: GravityCompat */
public final class C0396 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2156(int i, int i2, int i3, Rect rect, Rect rect2, int i4) {
        if (Build.VERSION.SDK_INT >= 17) {
            Gravity.apply(i, i2, i3, rect, rect2, i4);
        } else {
            Gravity.apply(i, i2, i3, rect, rect2);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m2155(int i, int i2) {
        return Build.VERSION.SDK_INT >= 17 ? Gravity.getAbsoluteGravity(i, i2) : i & -8388609;
    }
}
