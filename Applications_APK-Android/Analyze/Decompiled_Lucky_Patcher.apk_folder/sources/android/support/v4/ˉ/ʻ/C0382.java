package android.support.v4.ˉ.ʻ;

import android.os.Build;
import android.view.accessibility.AccessibilityRecord;

/* renamed from: android.support.v4.ˉ.ʻ.ˆ  reason: contains not printable characters */
/* compiled from: AccessibilityRecordCompat */
public class C0382 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final C0385 f1226;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final AccessibilityRecord f1227;

    /* renamed from: android.support.v4.ˉ.ʻ.ˆ$ʽ  reason: contains not printable characters */
    /* compiled from: AccessibilityRecordCompat */
    static class C0385 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2124(AccessibilityRecord accessibilityRecord, int i) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2125(AccessibilityRecord accessibilityRecord, int i) {
        }

        C0385() {
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ˆ$ʻ  reason: contains not printable characters */
    /* compiled from: AccessibilityRecordCompat */
    static class C0383 extends C0385 {
        C0383() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2122(AccessibilityRecord accessibilityRecord, int i) {
            accessibilityRecord.setMaxScrollX(i);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2123(AccessibilityRecord accessibilityRecord, int i) {
            accessibilityRecord.setMaxScrollY(i);
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ˆ$ʼ  reason: contains not printable characters */
    /* compiled from: AccessibilityRecordCompat */
    static class C0384 extends C0383 {
        C0384() {
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f1226 = new C0384();
        } else if (Build.VERSION.SDK_INT >= 15) {
            f1226 = new C0383();
        } else {
            f1226 = new C0385();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2120(AccessibilityRecord accessibilityRecord, int i) {
        f1226.m2124(accessibilityRecord, i);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static void m2121(AccessibilityRecord accessibilityRecord, int i) {
        f1226.m2125(accessibilityRecord, i);
    }

    @Deprecated
    public int hashCode() {
        AccessibilityRecord accessibilityRecord = this.f1227;
        if (accessibilityRecord == null) {
            return 0;
        }
        return accessibilityRecord.hashCode();
    }

    @Deprecated
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        C0382 r5 = (C0382) obj;
        AccessibilityRecord accessibilityRecord = this.f1227;
        if (accessibilityRecord == null) {
            if (r5.f1227 != null) {
                return false;
            }
        } else if (!accessibilityRecord.equals(r5.f1227)) {
            return false;
        }
        return true;
    }
}
