package android.support.v4.ˉ;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.ˉ.ʻ.C0360;
import android.support.v4.ˉ.ʻ.C0373;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;

/* renamed from: android.support.v4.ˉ.ʼ  reason: contains not printable characters */
/* compiled from: AccessibilityDelegateCompat */
public class C0386 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final C0389 f1228;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final View.AccessibilityDelegate f1229 = new View.AccessibilityDelegate();

    /* renamed from: ʼ  reason: contains not printable characters */
    final View.AccessibilityDelegate f1230 = f1228.m2140(this);

    /* renamed from: android.support.v4.ˉ.ʼ$ʼ  reason: contains not printable characters */
    /* compiled from: AccessibilityDelegateCompat */
    static class C0389 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public C0373 m2139(View.AccessibilityDelegate accessibilityDelegate, View view) {
            return null;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2141(View.AccessibilityDelegate accessibilityDelegate, View view, int i, Bundle bundle) {
            return false;
        }

        C0389() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public View.AccessibilityDelegate m2140(final C0386 r2) {
            return new View.AccessibilityDelegate() {
                public boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
                    return r2.m2134(view, accessibilityEvent);
                }

                public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
                    r2.m2130(view, accessibilityEvent);
                }

                public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfo accessibilityNodeInfo) {
                    r2.m2129(view, C0360.m2006(accessibilityNodeInfo));
                }

                public void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
                    r2.m2135(view, accessibilityEvent);
                }

                public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
                    return r2.m2132(viewGroup, view, accessibilityEvent);
                }

                public void sendAccessibilityEvent(View view, int i) {
                    r2.m2128(view, i);
                }

                public void sendAccessibilityEventUnchecked(View view, AccessibilityEvent accessibilityEvent) {
                    r2.m2133(view, accessibilityEvent);
                }
            };
        }
    }

    /* renamed from: android.support.v4.ˉ.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: AccessibilityDelegateCompat */
    static class C0387 extends C0389 {
        C0387() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public View.AccessibilityDelegate m2137(final C0386 r2) {
            return new View.AccessibilityDelegate() {
                public boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
                    return r2.m2134(view, accessibilityEvent);
                }

                public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
                    r2.m2130(view, accessibilityEvent);
                }

                public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfo accessibilityNodeInfo) {
                    r2.m2129(view, C0360.m2006(accessibilityNodeInfo));
                }

                public void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
                    r2.m2135(view, accessibilityEvent);
                }

                public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
                    return r2.m2132(viewGroup, view, accessibilityEvent);
                }

                public void sendAccessibilityEvent(View view, int i) {
                    r2.m2128(view, i);
                }

                public void sendAccessibilityEventUnchecked(View view, AccessibilityEvent accessibilityEvent) {
                    r2.m2133(view, accessibilityEvent);
                }

                public AccessibilityNodeProvider getAccessibilityNodeProvider(View view) {
                    C0373 r2 = r2.m2126(view);
                    if (r2 != null) {
                        return (AccessibilityNodeProvider) r2.m2096();
                    }
                    return null;
                }

                public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
                    return r2.m2131(view, i, bundle);
                }
            };
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0373 m2136(View.AccessibilityDelegate accessibilityDelegate, View view) {
            AccessibilityNodeProvider accessibilityNodeProvider = accessibilityDelegate.getAccessibilityNodeProvider(view);
            if (accessibilityNodeProvider != null) {
                return new C0373(accessibilityNodeProvider);
            }
            return null;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2138(View.AccessibilityDelegate accessibilityDelegate, View view, int i, Bundle bundle) {
            return accessibilityDelegate.performAccessibilityAction(view, i, bundle);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f1228 = new C0387();
        } else {
            f1228 = new C0389();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public View.AccessibilityDelegate m2127() {
        return this.f1230;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2128(View view, int i) {
        f1229.sendAccessibilityEvent(view, i);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2133(View view, AccessibilityEvent accessibilityEvent) {
        f1229.sendAccessibilityEventUnchecked(view, accessibilityEvent);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m2134(View view, AccessibilityEvent accessibilityEvent) {
        return f1229.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m2135(View view, AccessibilityEvent accessibilityEvent) {
        f1229.onPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2130(View view, AccessibilityEvent accessibilityEvent) {
        f1229.onInitializeAccessibilityEvent(view, accessibilityEvent);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2129(View view, C0360 r3) {
        f1229.onInitializeAccessibilityNodeInfo(view, r3.m2008());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2132(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return f1229.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0373 m2126(View view) {
        return f1228.m2139(f1229, view);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2131(View view, int i, Bundle bundle) {
        return f1228.m2141(f1229, view, i, bundle);
    }
}
