package android.support.v4.ˉ.ʻ;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.ˉ.ʻ.C0378;
import android.support.v4.ˉ.ʻ.C0380;
import java.util.ArrayList;
import java.util.List;

/* renamed from: android.support.v4.ˉ.ʻ.ʽ  reason: contains not printable characters */
/* compiled from: AccessibilityNodeProviderCompat */
public class C0373 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final C0374 f1218;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Object f1219;

    /* renamed from: android.support.v4.ˉ.ʻ.ʽ$ʻ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeProviderCompat */
    interface C0374 {
        /* renamed from: ʻ  reason: contains not printable characters */
        Object m2100(C0373 r1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0360 m2095(int i) {
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public List<C0360> m2097(String str, int i) {
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2098(int i, int i2, Bundle bundle) {
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0360 m2099(int i) {
        return null;
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʽ$ʾ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeProviderCompat */
    static class C0377 implements C0374 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public Object m2110(C0373 r1) {
            return null;
        }

        C0377() {
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʽ$ʼ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeProviderCompat */
    private static class C0375 extends C0377 {
        C0375() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Object m2101(final C0373 r2) {
            return C0378.m2111(new C0378.C0379() {
                /* renamed from: ʻ  reason: contains not printable characters */
                public boolean m2104(int i, int i2, Bundle bundle) {
                    return r2.m2098(i, i2, bundle);
                }

                /* renamed from: ʻ  reason: contains not printable characters */
                public List<Object> m2103(String str, int i) {
                    List<C0360> r4 = r2.m2097(str, i);
                    if (r4 == null) {
                        return null;
                    }
                    ArrayList arrayList = new ArrayList();
                    int size = r4.size();
                    for (int i2 = 0; i2 < size; i2++) {
                        arrayList.add(r4.get(i2).m2008());
                    }
                    return arrayList;
                }

                /* renamed from: ʻ  reason: contains not printable characters */
                public Object m2102(int i) {
                    C0360 r2 = r2.m2095(i);
                    if (r2 == null) {
                        return null;
                    }
                    return r2.m2008();
                }
            });
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʽ$ʽ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeProviderCompat */
    private static class C0376 extends C0377 {
        C0376() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Object m2105(final C0373 r2) {
            return C0380.m2115(new C0380.C0381() {
                /* renamed from: ʻ  reason: contains not printable characters */
                public boolean m2108(int i, int i2, Bundle bundle) {
                    return r2.m2098(i, i2, bundle);
                }

                /* renamed from: ʻ  reason: contains not printable characters */
                public List<Object> m2107(String str, int i) {
                    List<C0360> r4 = r2.m2097(str, i);
                    if (r4 == null) {
                        return null;
                    }
                    ArrayList arrayList = new ArrayList();
                    int size = r4.size();
                    for (int i2 = 0; i2 < size; i2++) {
                        arrayList.add(r4.get(i2).m2008());
                    }
                    return arrayList;
                }

                /* renamed from: ʻ  reason: contains not printable characters */
                public Object m2106(int i) {
                    C0360 r2 = r2.m2095(i);
                    if (r2 == null) {
                        return null;
                    }
                    return r2.m2008();
                }

                /* renamed from: ʼ  reason: contains not printable characters */
                public Object m2109(int i) {
                    C0360 r2 = r2.m2099(i);
                    if (r2 == null) {
                        return null;
                    }
                    return r2.m2008();
                }
            });
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 19) {
            f1218 = new C0376();
        } else if (Build.VERSION.SDK_INT >= 16) {
            f1218 = new C0375();
        } else {
            f1218 = new C0377();
        }
    }

    public C0373() {
        this.f1219 = f1218.m2100(this);
    }

    public C0373(Object obj) {
        this.f1219 = obj;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Object m2096() {
        return this.f1219;
    }
}
