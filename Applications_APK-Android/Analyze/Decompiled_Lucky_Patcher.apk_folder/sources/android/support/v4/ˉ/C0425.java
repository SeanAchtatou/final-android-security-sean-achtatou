package android.support.v4.ˉ;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.util.TypedValue;
import android.view.ViewConfiguration;
import java.lang.reflect.Method;

@Deprecated
/* renamed from: android.support.v4.ˉ.ᴵ  reason: contains not printable characters */
/* compiled from: ViewConfigurationCompat */
public final class C0425 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Method f1266;

    static {
        if (Build.VERSION.SDK_INT == 25) {
            try {
                f1266 = ViewConfiguration.class.getDeclaredMethod("getScaledScrollFactor", new Class[0]);
            } catch (Exception unused) {
                Log.i("ViewConfigCompat", "Could not find method getScaledScrollFactor() on ViewConfiguration");
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static float m2349(ViewConfiguration viewConfiguration, Context context) {
        if (Build.VERSION.SDK_INT >= 26) {
            return viewConfiguration.getScaledHorizontalScrollFactor();
        }
        return m2351(viewConfiguration, context);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static float m2350(ViewConfiguration viewConfiguration, Context context) {
        if (Build.VERSION.SDK_INT >= 26) {
            return viewConfiguration.getScaledVerticalScrollFactor();
        }
        return m2351(viewConfiguration, context);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static float m2351(ViewConfiguration viewConfiguration, Context context) {
        Method method;
        if (Build.VERSION.SDK_INT >= 25 && (method = f1266) != null) {
            try {
                return (float) ((Integer) method.invoke(viewConfiguration, new Object[0])).intValue();
            } catch (Exception unused) {
                Log.i("ViewConfigCompat", "Could not find method getScaledScrollFactor() on ViewConfiguration");
            }
        }
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(16842829, typedValue, true)) {
            return typedValue.getDimension(context.getResources().getDisplayMetrics());
        }
        return 0.0f;
    }
}
