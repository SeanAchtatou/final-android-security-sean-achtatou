package android.support.v4.ˉ.ʻ;

import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import net.lingala.zip4j.util.InternalZipConstants;
import org.tukaani.xz.common.Util;

/* renamed from: android.support.v4.ˉ.ʻ.ʼ  reason: contains not printable characters */
/* compiled from: AccessibilityNodeInfoCompat */
public class C0360 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0370 f1182;

    /* renamed from: ʼ  reason: contains not printable characters */
    public int f1183 = -1;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final AccessibilityNodeInfo f1184;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static String m2007(int i) {
        if (i == 1) {
            return "ACTION_FOCUS";
        }
        if (i == 2) {
            return "ACTION_CLEAR_FOCUS";
        }
        switch (i) {
            case 4:
                return "ACTION_SELECT";
            case 8:
                return "ACTION_CLEAR_SELECTION";
            case 16:
                return "ACTION_CLICK";
            case 32:
                return "ACTION_LONG_CLICK";
            case 64:
                return "ACTION_ACCESSIBILITY_FOCUS";
            case 128:
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            case 256:
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            case 512:
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            case Util.BLOCK_HEADER_SIZE_MAX /*1024*/:
                return "ACTION_NEXT_HTML_ELEMENT";
            case InternalZipConstants.UFT8_NAMES_FLAG /*2048*/:
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            case 4096:
                return "ACTION_SCROLL_FORWARD";
            case 8192:
                return "ACTION_SCROLL_BACKWARD";
            case 16384:
                return "ACTION_COPY";
            case 32768:
                return "ACTION_PASTE";
            case InternalZipConstants.MIN_SPLIT_LENGTH /*65536*/:
                return "ACTION_CUT";
            case 131072:
                return "ACTION_SET_SELECTION";
            default:
                return "ACTION_UNKNOWN";
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeInfoCompat */
    public static class C0361 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public static final C0361 f1185 = new C0361(1, null);

        /* renamed from: ʻʻ  reason: contains not printable characters */
        public static final C0361 f1186 = new C0361(C0360.f1182.m2091());

        /* renamed from: ʼ  reason: contains not printable characters */
        public static final C0361 f1187 = new C0361(2, null);

        /* renamed from: ʽ  reason: contains not printable characters */
        public static final C0361 f1188 = new C0361(4, null);

        /* renamed from: ʽʽ  reason: contains not printable characters */
        public static final C0361 f1189 = new C0361(C0360.f1182.m2092());

        /* renamed from: ʾ  reason: contains not printable characters */
        public static final C0361 f1190 = new C0361(8, null);

        /* renamed from: ʿ  reason: contains not printable characters */
        public static final C0361 f1191 = new C0361(16, null);

        /* renamed from: ˆ  reason: contains not printable characters */
        public static final C0361 f1192 = new C0361(32, null);

        /* renamed from: ˈ  reason: contains not printable characters */
        public static final C0361 f1193 = new C0361(64, null);

        /* renamed from: ˉ  reason: contains not printable characters */
        public static final C0361 f1194 = new C0361(128, null);

        /* renamed from: ˊ  reason: contains not printable characters */
        public static final C0361 f1195 = new C0361(256, null);

        /* renamed from: ˋ  reason: contains not printable characters */
        public static final C0361 f1196 = new C0361(512, null);

        /* renamed from: ˎ  reason: contains not printable characters */
        public static final C0361 f1197 = new C0361(Util.BLOCK_HEADER_SIZE_MAX, null);

        /* renamed from: ˏ  reason: contains not printable characters */
        public static final C0361 f1198 = new C0361(InternalZipConstants.UFT8_NAMES_FLAG, null);

        /* renamed from: ˑ  reason: contains not printable characters */
        public static final C0361 f1199 = new C0361(4096, null);

        /* renamed from: י  reason: contains not printable characters */
        public static final C0361 f1200 = new C0361(8192, null);

        /* renamed from: ـ  reason: contains not printable characters */
        public static final C0361 f1201 = new C0361(16384, null);

        /* renamed from: ٴ  reason: contains not printable characters */
        public static final C0361 f1202 = new C0361(32768, null);

        /* renamed from: ᐧ  reason: contains not printable characters */
        public static final C0361 f1203 = new C0361(InternalZipConstants.MIN_SPLIT_LENGTH, null);

        /* renamed from: ᐧᐧ  reason: contains not printable characters */
        public static final C0361 f1204 = new C0361(C0360.f1182.m2088());

        /* renamed from: ᴵ  reason: contains not printable characters */
        public static final C0361 f1205 = new C0361(131072, null);

        /* renamed from: ᴵᴵ  reason: contains not printable characters */
        public static final C0361 f1206 = new C0361(C0360.f1182.m2090());

        /* renamed from: ᵎ  reason: contains not printable characters */
        public static final C0361 f1207 = new C0361(262144, null);

        /* renamed from: ᵔ  reason: contains not printable characters */
        public static final C0361 f1208 = new C0361(524288, null);

        /* renamed from: ᵢ  reason: contains not printable characters */
        public static final C0361 f1209 = new C0361(1048576, null);

        /* renamed from: ⁱ  reason: contains not printable characters */
        public static final C0361 f1210 = new C0361(2097152, null);

        /* renamed from: ﹳ  reason: contains not printable characters */
        public static final C0361 f1211 = new C0361(C0360.f1182.m2081());

        /* renamed from: ﹶ  reason: contains not printable characters */
        public static final C0361 f1212 = new C0361(C0360.f1182.m2074());

        /* renamed from: ﾞ  reason: contains not printable characters */
        public static final C0361 f1213 = new C0361(C0360.f1182.m2085());

        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        public static final C0361 f1214 = new C0361(C0360.f1182.m2089());

        /* renamed from: ʼʼ  reason: contains not printable characters */
        final Object f1215;

        public C0361(int i, CharSequence charSequence) {
            this(C0360.f1182.m2077(i, charSequence));
        }

        C0361(Object obj) {
            this.f1215 = obj;
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʼ$ˎ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeInfoCompat */
    public static class C0371 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final Object f1216;

        /* renamed from: ʻ  reason: contains not printable characters */
        public static C0371 m2093(int i, int i2, boolean z, int i3) {
            return new C0371(C0360.f1182.m2076(i, i2, z, i3));
        }

        C0371(Object obj) {
            this.f1216 = obj;
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʼ$ˏ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeInfoCompat */
    public static class C0372 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final Object f1217;

        /* renamed from: ʻ  reason: contains not printable characters */
        public static C0372 m2094(int i, int i2, int i3, int i4, boolean z, boolean z2) {
            return new C0372(C0360.f1182.m2075(i, i2, i3, i4, z, z2));
        }

        C0372(Object obj) {
            this.f1217 = obj;
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʼ$ˋ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeInfoCompat */
    static class C0370 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public Object m2074() {
            return null;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Object m2075(int i, int i2, int i3, int i4, boolean z, boolean z2) {
            return null;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Object m2076(int i, int i2, boolean z, int i3) {
            return null;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Object m2077(int i, CharSequence charSequence) {
            return null;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2078(AccessibilityNodeInfo accessibilityNodeInfo, Object obj) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2079(AccessibilityNodeInfo accessibilityNodeInfo, boolean z) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2080(AccessibilityNodeInfo accessibilityNodeInfo) {
            return false;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public Object m2081() {
            return null;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2082(AccessibilityNodeInfo accessibilityNodeInfo, Object obj) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2083(AccessibilityNodeInfo accessibilityNodeInfo, boolean z) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m2084(AccessibilityNodeInfo accessibilityNodeInfo) {
            return false;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public Object m2085() {
            return null;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public String m2086(AccessibilityNodeInfo accessibilityNodeInfo) {
            return null;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m2087(AccessibilityNodeInfo accessibilityNodeInfo, Object obj) {
            return false;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public Object m2088() {
            return null;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public Object m2089() {
            return null;
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public Object m2090() {
            return null;
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public Object m2091() {
            return null;
        }

        /* renamed from: ˉ  reason: contains not printable characters */
        public Object m2092() {
            return null;
        }

        C0370() {
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʼ$ʼ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeInfoCompat */
    static class C0362 extends C0370 {
        C0362() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2054(AccessibilityNodeInfo accessibilityNodeInfo) {
            return accessibilityNodeInfo.isVisibleToUser();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2053(AccessibilityNodeInfo accessibilityNodeInfo, boolean z) {
            accessibilityNodeInfo.setVisibleToUser(z);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m2056(AccessibilityNodeInfo accessibilityNodeInfo) {
            return accessibilityNodeInfo.isAccessibilityFocused();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2055(AccessibilityNodeInfo accessibilityNodeInfo, boolean z) {
            accessibilityNodeInfo.setAccessibilityFocused(z);
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʼ$ʽ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeInfoCompat */
    static class C0363 extends C0362 {
        C0363() {
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʼ$ʾ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeInfoCompat */
    static class C0364 extends C0363 {
        C0364() {
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public String m2057(AccessibilityNodeInfo accessibilityNodeInfo) {
            return accessibilityNodeInfo.getViewIdResourceName();
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʼ$ʿ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeInfoCompat */
    static class C0365 extends C0364 {
        C0365() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2060(AccessibilityNodeInfo accessibilityNodeInfo, Object obj) {
            accessibilityNodeInfo.setCollectionInfo((AccessibilityNodeInfo.CollectionInfo) obj);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Object m2059(int i, int i2, boolean z, int i3) {
            return AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Object m2058(int i, int i2, int i3, int i4, boolean z, boolean z2) {
            return AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2061(AccessibilityNodeInfo accessibilityNodeInfo, Object obj) {
            accessibilityNodeInfo.setCollectionItemInfo((AccessibilityNodeInfo.CollectionItemInfo) obj);
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʼ$ˆ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeInfoCompat */
    static class C0366 extends C0365 {
        C0366() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Object m2064(int i, CharSequence charSequence) {
            return new AccessibilityNodeInfo.AccessibilityAction(i, charSequence);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Object m2063(int i, int i2, boolean z, int i3) {
            return AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z, i3);
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m2065(AccessibilityNodeInfo accessibilityNodeInfo, Object obj) {
            return accessibilityNodeInfo.removeAction((AccessibilityNodeInfo.AccessibilityAction) obj);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Object m2062(int i, int i2, int i3, int i4, boolean z, boolean z2) {
            return AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z, z2);
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʼ$ˈ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeInfoCompat */
    static class C0367 extends C0366 {
        C0367() {
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʼ$ˉ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeInfoCompat */
    static class C0368 extends C0367 {
        C0368() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Object m2066() {
            return AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_TO_POSITION;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public Object m2067() {
            return AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_ON_SCREEN;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public Object m2068() {
            return AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_UP;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public Object m2069() {
            return AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_DOWN;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public Object m2070() {
            return AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_LEFT;
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public Object m2071() {
            return AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_RIGHT;
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public Object m2072() {
            return AccessibilityNodeInfo.AccessibilityAction.ACTION_CONTEXT_CLICK;
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʼ$ˊ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeInfoCompat */
    static class C0369 extends C0368 {
        C0369() {
        }

        /* renamed from: ˉ  reason: contains not printable characters */
        public Object m2073() {
            return AccessibilityNodeInfo.AccessibilityAction.ACTION_SET_PROGRESS;
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 24) {
            f1182 = new C0369();
        } else if (Build.VERSION.SDK_INT >= 23) {
            f1182 = new C0368();
        } else if (Build.VERSION.SDK_INT >= 22) {
            f1182 = new C0367();
        } else if (Build.VERSION.SDK_INT >= 21) {
            f1182 = new C0366();
        } else if (Build.VERSION.SDK_INT >= 19) {
            f1182 = new C0365();
        } else if (Build.VERSION.SDK_INT >= 18) {
            f1182 = new C0364();
        } else if (Build.VERSION.SDK_INT >= 17) {
            f1182 = new C0363();
        } else if (Build.VERSION.SDK_INT >= 16) {
            f1182 = new C0362();
        } else {
            f1182 = new C0370();
        }
    }

    private C0360(AccessibilityNodeInfo accessibilityNodeInfo) {
        this.f1184 = accessibilityNodeInfo;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0360 m2006(AccessibilityNodeInfo accessibilityNodeInfo) {
        return new C0360(accessibilityNodeInfo);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public AccessibilityNodeInfo m2008() {
        return this.f1184;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0360 m2005(C0360 r0) {
        return m2006(AccessibilityNodeInfo.obtain(r0.f1184));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2011(View view) {
        this.f1184.setSource(view);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2018(View view) {
        this.f1184.addChild(view);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m2016() {
        return this.f1184.getActions();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2009(int i) {
        this.f1184.addAction(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2015(C0361 r3) {
        return f1182.m2087(this.f1184, r3.f1215);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2023(View view) {
        this.f1184.setParent(view);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2010(Rect rect) {
        this.f1184.getBoundsInParent(rect);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2017(Rect rect) {
        this.f1184.setBoundsInParent(rect);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2022(Rect rect) {
        this.f1184.getBoundsInScreen(rect);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m2027(Rect rect) {
        this.f1184.setBoundsInScreen(rect);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m2026() {
        return this.f1184.isCheckable();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2014(boolean z) {
        this.f1184.setCheckable(z);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m2029() {
        return this.f1184.isChecked();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2021(boolean z) {
        this.f1184.setChecked(z);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m2031() {
        return this.f1184.isFocusable();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2025(boolean z) {
        this.f1184.setFocusable(z);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m2033() {
        return this.f1184.isFocused();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m2028(boolean z) {
        this.f1184.setFocused(z);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m2035() {
        return f1182.m2080(this.f1184);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m2030(boolean z) {
        f1182.m2079(this.f1184, z);
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m2037() {
        return f1182.m2084(this.f1184);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m2032(boolean z) {
        f1182.m2083(this.f1184, z);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean m2039() {
        return this.f1184.isSelected();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public void m2034(boolean z) {
        this.f1184.setSelected(z);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean m2041() {
        return this.f1184.isClickable();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public void m2036(boolean z) {
        this.f1184.setClickable(z);
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean m2043() {
        return this.f1184.isLongClickable();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public void m2038(boolean z) {
        this.f1184.setLongClickable(z);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public boolean m2044() {
        return this.f1184.isEnabled();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public void m2040(boolean z) {
        this.f1184.setEnabled(z);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m2045() {
        return this.f1184.isPassword();
    }

    /* renamed from: י  reason: contains not printable characters */
    public boolean m2046() {
        return this.f1184.isScrollable();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public void m2042(boolean z) {
        this.f1184.setScrollable(z);
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public CharSequence m2047() {
        return this.f1184.getPackageName();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2012(CharSequence charSequence) {
        this.f1184.setPackageName(charSequence);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public CharSequence m2048() {
        return this.f1184.getClassName();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2019(CharSequence charSequence) {
        this.f1184.setClassName(charSequence);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public CharSequence m2049() {
        return this.f1184.getText();
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public CharSequence m2050() {
        return this.f1184.getContentDescription();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2024(CharSequence charSequence) {
        this.f1184.setContentDescription(charSequence);
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public void m2051() {
        this.f1184.recycle();
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    public String m2052() {
        return f1182.m2086(this.f1184);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2013(Object obj) {
        f1182.m2078(this.f1184, ((C0371) obj).f1216);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2020(Object obj) {
        f1182.m2082(this.f1184, ((C0372) obj).f1217);
    }

    public int hashCode() {
        AccessibilityNodeInfo accessibilityNodeInfo = this.f1184;
        if (accessibilityNodeInfo == null) {
            return 0;
        }
        return accessibilityNodeInfo.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        C0360 r5 = (C0360) obj;
        AccessibilityNodeInfo accessibilityNodeInfo = this.f1184;
        if (accessibilityNodeInfo == null) {
            if (r5.f1184 != null) {
                return false;
            }
        } else if (!accessibilityNodeInfo.equals(r5.f1184)) {
            return false;
        }
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        Rect rect = new Rect();
        m2010(rect);
        sb.append("; boundsInParent: " + rect);
        m2022(rect);
        sb.append("; boundsInScreen: " + rect);
        sb.append("; packageName: ");
        sb.append(m2047());
        sb.append("; className: ");
        sb.append(m2048());
        sb.append("; text: ");
        sb.append(m2049());
        sb.append("; contentDescription: ");
        sb.append(m2050());
        sb.append("; viewId: ");
        sb.append(m2052());
        sb.append("; checkable: ");
        sb.append(m2026());
        sb.append("; checked: ");
        sb.append(m2029());
        sb.append("; focusable: ");
        sb.append(m2031());
        sb.append("; focused: ");
        sb.append(m2033());
        sb.append("; selected: ");
        sb.append(m2039());
        sb.append("; clickable: ");
        sb.append(m2041());
        sb.append("; longClickable: ");
        sb.append(m2043());
        sb.append("; enabled: ");
        sb.append(m2044());
        sb.append("; password: ");
        sb.append(m2045());
        sb.append("; scrollable: " + m2046());
        sb.append("; [");
        int r1 = m2016();
        while (r1 != 0) {
            int numberOfTrailingZeros = 1 << Integer.numberOfTrailingZeros(r1);
            r1 &= numberOfTrailingZeros ^ -1;
            sb.append(m2007(numberOfTrailingZeros));
            if (r1 != 0) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
