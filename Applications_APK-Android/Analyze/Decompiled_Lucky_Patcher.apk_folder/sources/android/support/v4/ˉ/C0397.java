package android.support.v4.ˉ;

import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import java.lang.reflect.Field;

/* renamed from: android.support.v4.ˉ.ʿ  reason: contains not printable characters */
/* compiled from: LayoutInflaterCompat */
public final class C0397 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0399 f1243;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static Field f1244;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static boolean f1245;

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m2157(LayoutInflater layoutInflater, LayoutInflater.Factory2 factory2) {
        if (!f1245) {
            try {
                f1244 = LayoutInflater.class.getDeclaredField("mFactory2");
                f1244.setAccessible(true);
            } catch (NoSuchFieldException e) {
                Log.e("LayoutInflaterCompatHC", "forceSetFactory2 Could not find field 'mFactory2' on class " + LayoutInflater.class.getName() + "; inflation may have unexpected results.", e);
            }
            f1245 = true;
        }
        Field field = f1244;
        if (field != null) {
            try {
                field.set(layoutInflater, factory2);
            } catch (IllegalAccessException e2) {
                Log.e("LayoutInflaterCompatHC", "forceSetFactory2 could not set the Factory2 on LayoutInflater " + layoutInflater + "; inflation may have unexpected results.", e2);
            }
        }
    }

    /* renamed from: android.support.v4.ˉ.ʿ$ʼ  reason: contains not printable characters */
    /* compiled from: LayoutInflaterCompat */
    static class C0399 {
        C0399() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2160(LayoutInflater layoutInflater, LayoutInflater.Factory2 factory2) {
            layoutInflater.setFactory2(factory2);
            LayoutInflater.Factory factory = layoutInflater.getFactory();
            if (factory instanceof LayoutInflater.Factory2) {
                C0397.m2157(layoutInflater, (LayoutInflater.Factory2) factory);
            } else {
                C0397.m2157(layoutInflater, factory2);
            }
        }
    }

    /* renamed from: android.support.v4.ˉ.ʿ$ʻ  reason: contains not printable characters */
    /* compiled from: LayoutInflaterCompat */
    static class C0398 extends C0399 {
        C0398() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2159(LayoutInflater layoutInflater, LayoutInflater.Factory2 factory2) {
            layoutInflater.setFactory2(factory2);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 21) {
            f1243 = new C0398();
        } else {
            f1243 = new C0399();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static void m2158(LayoutInflater layoutInflater, LayoutInflater.Factory2 factory2) {
        f1243.m2160(layoutInflater, factory2);
    }
}
