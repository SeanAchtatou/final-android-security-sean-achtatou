package android.support.v4.ˉ;

import android.os.Build;
import android.view.WindowInsets;

/* renamed from: android.support.v4.ˉ.ﾞ  reason: contains not printable characters */
/* compiled from: WindowInsetsCompat */
public class C0439 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Object f1281;

    private C0439(Object obj) {
        this.f1281 = obj;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m2397() {
        if (Build.VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.f1281).getSystemWindowInsetLeft();
        }
        return 0;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m2399() {
        if (Build.VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.f1281).getSystemWindowInsetTop();
        }
        return 0;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m2400() {
        if (Build.VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.f1281).getSystemWindowInsetRight();
        }
        return 0;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m2401() {
        if (Build.VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.f1281).getSystemWindowInsetBottom();
        }
        return 0;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m2402() {
        if (Build.VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.f1281).hasSystemWindowInsets();
        }
        return false;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m2403() {
        if (Build.VERSION.SDK_INT >= 21) {
            return ((WindowInsets) this.f1281).isConsumed();
        }
        return false;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public C0439 m2404() {
        if (Build.VERSION.SDK_INT >= 20) {
            return new C0439(((WindowInsets) this.f1281).consumeSystemWindowInsets());
        }
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0439 m2398(int i, int i2, int i3, int i4) {
        if (Build.VERSION.SDK_INT >= 20) {
            return new C0439(((WindowInsets) this.f1281).replaceSystemWindowInsets(i, i2, i3, i4));
        }
        return null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        C0439 r5 = (C0439) obj;
        Object obj2 = this.f1281;
        if (obj2 != null) {
            return obj2.equals(r5.f1281);
        }
        if (r5.f1281 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        Object obj = this.f1281;
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static C0439 m2395(Object obj) {
        if (obj == null) {
            return null;
        }
        return new C0439(obj);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static Object m2396(C0439 r0) {
        if (r0 == null) {
            return null;
        }
        return r0.f1281;
    }
}
