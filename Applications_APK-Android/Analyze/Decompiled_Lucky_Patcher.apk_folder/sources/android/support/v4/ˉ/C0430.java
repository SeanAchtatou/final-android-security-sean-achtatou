package android.support.v4.ˉ;

import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewParent;

/* renamed from: android.support.v4.ˉ.ᵔ  reason: contains not printable characters */
/* compiled from: ViewParentCompat */
public final class C0430 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0433 f1268;

    /* renamed from: android.support.v4.ˉ.ᵔ$ʽ  reason: contains not printable characters */
    /* compiled from: ViewParentCompat */
    static class C0433 {
        C0433() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2372(ViewParent viewParent, View view, View view2, int i) {
            if (viewParent instanceof C0409) {
                return ((C0409) viewParent).onStartNestedScroll(view, view2, i);
            }
            return false;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2373(ViewParent viewParent, View view, View view2, int i) {
            if (viewParent instanceof C0409) {
                ((C0409) viewParent).onNestedScrollAccepted(view, view2, i);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2367(ViewParent viewParent, View view) {
            if (viewParent instanceof C0409) {
                ((C0409) viewParent).onStopNestedScroll(view);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2368(ViewParent viewParent, View view, int i, int i2, int i3, int i4) {
            if (viewParent instanceof C0409) {
                ((C0409) viewParent).onNestedScroll(view, i, i2, i3, i4);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2369(ViewParent viewParent, View view, int i, int i2, int[] iArr) {
            if (viewParent instanceof C0409) {
                ((C0409) viewParent).onNestedPreScroll(view, i, i2, iArr);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2371(ViewParent viewParent, View view, float f, float f2, boolean z) {
            if (viewParent instanceof C0409) {
                return ((C0409) viewParent).onNestedFling(view, f, f2, z);
            }
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2370(ViewParent viewParent, View view, float f, float f2) {
            if (viewParent instanceof C0409) {
                return ((C0409) viewParent).onNestedPreFling(view, f, f2);
            }
            return false;
        }
    }

    /* renamed from: android.support.v4.ˉ.ᵔ$ʻ  reason: contains not printable characters */
    /* compiled from: ViewParentCompat */
    static class C0431 extends C0433 {
        C0431() {
        }
    }

    /* renamed from: android.support.v4.ˉ.ᵔ$ʼ  reason: contains not printable characters */
    /* compiled from: ViewParentCompat */
    static class C0432 extends C0431 {
        C0432() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2365(ViewParent viewParent, View view, View view2, int i) {
            try {
                return viewParent.onStartNestedScroll(view, view2, i);
            } catch (AbstractMethodError e) {
                Log.e("ViewParentCompat", "ViewParent " + viewParent + " does not implement interface " + "method onStartNestedScroll", e);
                return false;
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2366(ViewParent viewParent, View view, View view2, int i) {
            try {
                viewParent.onNestedScrollAccepted(view, view2, i);
            } catch (AbstractMethodError e) {
                Log.e("ViewParentCompat", "ViewParent " + viewParent + " does not implement interface " + "method onNestedScrollAccepted", e);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2360(ViewParent viewParent, View view) {
            try {
                viewParent.onStopNestedScroll(view);
            } catch (AbstractMethodError e) {
                Log.e("ViewParentCompat", "ViewParent " + viewParent + " does not implement interface " + "method onStopNestedScroll", e);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2361(ViewParent viewParent, View view, int i, int i2, int i3, int i4) {
            try {
                viewParent.onNestedScroll(view, i, i2, i3, i4);
            } catch (AbstractMethodError e) {
                Log.e("ViewParentCompat", "ViewParent " + viewParent + " does not implement interface " + "method onNestedScroll", e);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2362(ViewParent viewParent, View view, int i, int i2, int[] iArr) {
            try {
                viewParent.onNestedPreScroll(view, i, i2, iArr);
            } catch (AbstractMethodError e) {
                Log.e("ViewParentCompat", "ViewParent " + viewParent + " does not implement interface " + "method onNestedPreScroll", e);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2364(ViewParent viewParent, View view, float f, float f2, boolean z) {
            try {
                return viewParent.onNestedFling(view, f, f2, z);
            } catch (AbstractMethodError e) {
                Log.e("ViewParentCompat", "ViewParent " + viewParent + " does not implement interface " + "method onNestedFling", e);
                return false;
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2363(ViewParent viewParent, View view, float f, float f2) {
            try {
                return viewParent.onNestedPreFling(view, f, f2);
            } catch (AbstractMethodError e) {
                Log.e("ViewParentCompat", "ViewParent " + viewParent + " does not implement interface " + "method onNestedPreFling", e);
                return false;
            }
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 21) {
            f1268 = new C0432();
        } else if (Build.VERSION.SDK_INT >= 19) {
            f1268 = new C0431();
        } else {
            f1268 = new C0433();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m2358(ViewParent viewParent, View view, View view2, int i, int i2) {
        if (viewParent instanceof C0410) {
            return ((C0410) viewParent).m2207(view, view2, i, i2);
        }
        if (i2 == 0) {
            return f1268.m2372(viewParent, view, view2, i);
        }
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static void m2359(ViewParent viewParent, View view, View view2, int i, int i2) {
        if (viewParent instanceof C0410) {
            ((C0410) viewParent).m2208(view, view2, i, i2);
        } else if (i2 == 0) {
            f1268.m2373(viewParent, view, view2, i);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2353(ViewParent viewParent, View view, int i) {
        if (viewParent instanceof C0410) {
            ((C0410) viewParent).m2209(view, i);
        } else if (i == 0) {
            f1268.m2367(viewParent, view);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2354(ViewParent viewParent, View view, int i, int i2, int i3, int i4, int i5) {
        if (viewParent instanceof C0410) {
            ((C0410) viewParent).m2205(view, i, i2, i3, i4, i5);
        } else if (i5 == 0) {
            f1268.m2368(viewParent, view, i, i2, i3, i4);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2355(ViewParent viewParent, View view, int i, int i2, int[] iArr, int i3) {
        if (viewParent instanceof C0410) {
            ((C0410) viewParent).m2206(view, i, i2, iArr, i3);
        } else if (i3 == 0) {
            f1268.m2369(viewParent, view, i, i2, iArr);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m2357(ViewParent viewParent, View view, float f, float f2, boolean z) {
        return f1268.m2371(viewParent, view, f, f2, z);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m2356(ViewParent viewParent, View view, float f, float f2) {
        return f1268.m2370(viewParent, view, f, f2);
    }
}
