package android.support.v4.ˉ.ʻ;

import android.os.Build;
import android.view.accessibility.AccessibilityEvent;

/* renamed from: android.support.v4.ˉ.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: AccessibilityEventCompat */
public final class C0356 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final C0359 f1181;

    /* renamed from: android.support.v4.ˉ.ʻ.ʻ$ʽ  reason: contains not printable characters */
    /* compiled from: AccessibilityEventCompat */
    static class C0359 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public int m2003(AccessibilityEvent accessibilityEvent) {
            return 0;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2004(AccessibilityEvent accessibilityEvent, int i) {
        }

        C0359() {
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʻ$ʻ  reason: contains not printable characters */
    /* compiled from: AccessibilityEventCompat */
    static class C0357 extends C0359 {
        C0357() {
        }
    }

    /* renamed from: android.support.v4.ˉ.ʻ.ʻ$ʼ  reason: contains not printable characters */
    /* compiled from: AccessibilityEventCompat */
    static class C0358 extends C0357 {
        C0358() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2002(AccessibilityEvent accessibilityEvent, int i) {
            accessibilityEvent.setContentChangeTypes(i);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m2001(AccessibilityEvent accessibilityEvent) {
            return accessibilityEvent.getContentChangeTypes();
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 19) {
            f1181 = new C0358();
        } else if (Build.VERSION.SDK_INT >= 16) {
            f1181 = new C0357();
        } else {
            f1181 = new C0359();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2000(AccessibilityEvent accessibilityEvent, int i) {
        f1181.m2004(accessibilityEvent, i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m1999(AccessibilityEvent accessibilityEvent) {
        return f1181.m2003(accessibilityEvent);
    }
}
