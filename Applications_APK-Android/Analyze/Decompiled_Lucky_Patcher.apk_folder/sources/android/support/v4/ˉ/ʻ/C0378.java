package android.support.v4.ˉ.ʻ;

import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.List;

/* renamed from: android.support.v4.ˉ.ʻ.ʾ  reason: contains not printable characters */
/* compiled from: AccessibilityNodeProviderCompatJellyBean */
class C0378 {

    /* renamed from: android.support.v4.ˉ.ʻ.ʾ$ʻ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeProviderCompatJellyBean */
    interface C0379 {
        /* renamed from: ʻ  reason: contains not printable characters */
        Object m2112(int i);

        /* renamed from: ʻ  reason: contains not printable characters */
        List<Object> m2113(String str, int i);

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m2114(int i, int i2, Bundle bundle);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Object m2111(final C0379 r1) {
        return new AccessibilityNodeProvider() {
            public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
                return (AccessibilityNodeInfo) r1.m2112(i);
            }

            public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
                return r1.m2113(str, i);
            }

            public boolean performAction(int i, int i2, Bundle bundle) {
                return r1.m2114(i, i2, bundle);
            }
        };
    }
}
