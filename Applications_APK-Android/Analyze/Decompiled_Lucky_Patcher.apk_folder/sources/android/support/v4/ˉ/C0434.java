package android.support.v4.ˉ;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.os.Build;
import android.view.View;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;

/* renamed from: android.support.v4.ˉ.ᵢ  reason: contains not printable characters */
/* compiled from: ViewPropertyAnimatorCompat */
public final class C0434 {

    /* renamed from: ʻ  reason: contains not printable characters */
    Runnable f1269 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    Runnable f1270 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    int f1271 = -1;

    /* renamed from: ʾ  reason: contains not printable characters */
    private WeakReference<View> f1272;

    C0434(View view) {
        this.f1272 = new WeakReference<>(view);
    }

    /* renamed from: android.support.v4.ˉ.ᵢ$ʻ  reason: contains not printable characters */
    /* compiled from: ViewPropertyAnimatorCompat */
    static class C0435 implements C0436 {

        /* renamed from: ʻ  reason: contains not printable characters */
        C0434 f1279;

        /* renamed from: ʼ  reason: contains not printable characters */
        boolean f1280;

        C0435(C0434 r1) {
            this.f1279 = r1;
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: android.support.v4.ˉ.ⁱ} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* renamed from: ʻ  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m2385(android.view.View r4) {
            /*
                r3 = this;
                r0 = 0
                r3.f1280 = r0
                android.support.v4.ˉ.ᵢ r0 = r3.f1279
                int r0 = r0.f1271
                r1 = 0
                r2 = -1
                if (r0 <= r2) goto L_0x000f
                r0 = 2
                r4.setLayerType(r0, r1)
            L_0x000f:
                android.support.v4.ˉ.ᵢ r0 = r3.f1279
                java.lang.Runnable r0 = r0.f1269
                if (r0 == 0) goto L_0x0020
                android.support.v4.ˉ.ᵢ r0 = r3.f1279
                java.lang.Runnable r0 = r0.f1269
                android.support.v4.ˉ.ᵢ r2 = r3.f1279
                r2.f1269 = r1
                r0.run()
            L_0x0020:
                r0 = 2113929216(0x7e000000, float:4.2535296E37)
                java.lang.Object r0 = r4.getTag(r0)
                boolean r2 = r0 instanceof android.support.v4.ˉ.C0436
                if (r2 == 0) goto L_0x002d
                r1 = r0
                android.support.v4.ˉ.ⁱ r1 = (android.support.v4.ˉ.C0436) r1
            L_0x002d:
                if (r1 == 0) goto L_0x0032
                r1.m2388(r4)
            L_0x0032:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.ˉ.C0434.C0435.m2385(android.view.View):void");
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: android.support.v4.ˉ.ⁱ} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* renamed from: ʼ  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m2386(android.view.View r4) {
            /*
                r3 = this;
                android.support.v4.ˉ.ᵢ r0 = r3.f1279
                int r0 = r0.f1271
                r1 = -1
                r2 = 0
                if (r0 <= r1) goto L_0x0013
                android.support.v4.ˉ.ᵢ r0 = r3.f1279
                int r0 = r0.f1271
                r4.setLayerType(r0, r2)
                android.support.v4.ˉ.ᵢ r0 = r3.f1279
                r0.f1271 = r1
            L_0x0013:
                int r0 = android.os.Build.VERSION.SDK_INT
                r1 = 16
                if (r0 >= r1) goto L_0x001d
                boolean r0 = r3.f1280
                if (r0 != 0) goto L_0x0043
            L_0x001d:
                android.support.v4.ˉ.ᵢ r0 = r3.f1279
                java.lang.Runnable r0 = r0.f1270
                if (r0 == 0) goto L_0x002e
                android.support.v4.ˉ.ᵢ r0 = r3.f1279
                java.lang.Runnable r0 = r0.f1270
                android.support.v4.ˉ.ᵢ r1 = r3.f1279
                r1.f1270 = r2
                r0.run()
            L_0x002e:
                r0 = 2113929216(0x7e000000, float:4.2535296E37)
                java.lang.Object r0 = r4.getTag(r0)
                boolean r1 = r0 instanceof android.support.v4.ˉ.C0436
                if (r1 == 0) goto L_0x003b
                r2 = r0
                android.support.v4.ˉ.ⁱ r2 = (android.support.v4.ˉ.C0436) r2
            L_0x003b:
                if (r2 == 0) goto L_0x0040
                r2.m2389(r4)
            L_0x0040:
                r4 = 1
                r3.f1280 = r4
            L_0x0043:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.ˉ.C0434.C0435.m2386(android.view.View):void");
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m2387(View view) {
            Object tag = view.getTag(2113929216);
            C0436 r0 = tag instanceof C0436 ? (C0436) tag : null;
            if (r0 != null) {
                r0.m2390(view);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0434 m2377(long j) {
        View view = this.f1272.get();
        if (view != null) {
            view.animate().setDuration(j);
        }
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0434 m2376(float f) {
        View view = this.f1272.get();
        if (view != null) {
            view.animate().alpha(f);
        }
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0434 m2381(float f) {
        View view = this.f1272.get();
        if (view != null) {
            view.animate().translationY(f);
        }
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public long m2375() {
        View view = this.f1272.get();
        if (view != null) {
            return view.animate().getDuration();
        }
        return 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0434 m2380(Interpolator interpolator) {
        View view = this.f1272.get();
        if (view != null) {
            view.animate().setInterpolator(interpolator);
        }
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0434 m2382(long j) {
        View view = this.f1272.get();
        if (view != null) {
            view.animate().setStartDelay(j);
        }
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2383() {
        View view = this.f1272.get();
        if (view != null) {
            view.animate().cancel();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2384() {
        View view = this.f1272.get();
        if (view != null) {
            view.animate().start();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0434 m2378(C0436 r4) {
        View view = this.f1272.get();
        if (view != null) {
            if (Build.VERSION.SDK_INT >= 16) {
                m2374(view, r4);
            } else {
                view.setTag(2113929216, r4);
                m2374(view, new C0435(this));
            }
        }
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m2374(final View view, final C0436 r4) {
        if (r4 != null) {
            view.animate().setListener(new AnimatorListenerAdapter() {
                public void onAnimationCancel(Animator animator) {
                    r4.m2390(view);
                }

                public void onAnimationEnd(Animator animator) {
                    r4.m2389(view);
                }

                public void onAnimationStart(Animator animator) {
                    r4.m2388(view);
                }
            });
        } else {
            view.animate().setListener(null);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0434 m2379(final C0438 r4) {
        final View view = this.f1272.get();
        if (view != null && Build.VERSION.SDK_INT >= 19) {
            AnonymousClass2 r1 = null;
            if (r4 != null) {
                r1 = new ValueAnimator.AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        r4.m2394(view);
                    }
                };
            }
            view.animate().setUpdateListener(r1);
        }
        return this;
    }
}
