package android.support.v4.ˉ.ʼ;

import android.view.animation.Interpolator;

/* renamed from: android.support.v4.ˉ.ʼ.ʾ  reason: contains not printable characters */
/* compiled from: LookupTableInterpolator */
abstract class C0392 implements Interpolator {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final float[] f1238;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final float f1239 = (1.0f / ((float) (this.f1238.length - 1)));

    public C0392(float[] fArr) {
        this.f1238 = fArr;
    }

    public float getInterpolation(float f) {
        if (f >= 1.0f) {
            return 1.0f;
        }
        if (f <= 0.0f) {
            return 0.0f;
        }
        float[] fArr = this.f1238;
        int min = Math.min((int) (((float) (fArr.length - 1)) * f), fArr.length - 2);
        float f2 = this.f1239;
        float f3 = (f - (((float) min) * f2)) / f2;
        float[] fArr2 = this.f1238;
        return fArr2[min] + (f3 * (fArr2[min + 1] - fArr2[min]));
    }
}
