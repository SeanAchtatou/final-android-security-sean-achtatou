package android.support.v4.ˉ;

import android.view.View;
import android.view.ViewParent;

/* renamed from: android.support.v4.ˉ.ˎ  reason: contains not printable characters */
/* compiled from: NestedScrollingChildHelper */
public class C0408 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private ViewParent f1247;

    /* renamed from: ʼ  reason: contains not printable characters */
    private ViewParent f1248;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final View f1249;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f1250;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int[] f1251;

    public C0408(View view) {
        this.f1249 = view;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2191(boolean z) {
        if (this.f1250) {
            C0414.m2254(this.f1249);
        }
        this.f1250 = z;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2192() {
        return this.f1250;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m2201() {
        return m2195(0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2195(int i) {
        return m2190(i) != null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m2202(int i) {
        return m2196(i, 0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2196(int i, int i2) {
        if (m2195(i2)) {
            return true;
        }
        if (!m2192()) {
            return false;
        }
        View view = this.f1249;
        for (ViewParent parent = this.f1249.getParent(); parent != null; parent = parent.getParent()) {
            if (C0430.m2358(parent, view, this.f1249, i, i2)) {
                m2189(i2, parent);
                C0430.m2359(parent, view, this.f1249, i, i2);
                return true;
            }
            if (parent instanceof View) {
                view = (View) parent;
            }
        }
        return false;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2203() {
        m2204(0);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2204(int i) {
        ViewParent r0 = m2190(i);
        if (r0 != null) {
            C0430.m2353(r0, this.f1249, i);
            m2189(i, (ViewParent) null);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2197(int i, int i2, int i3, int i4, int[] iArr) {
        return m2198(i, i2, i3, i4, iArr, 0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2198(int i, int i2, int i3, int i4, int[] iArr, int i5) {
        ViewParent r4;
        int i6;
        int i7;
        int[] iArr2 = iArr;
        if (!m2192() || (r4 = m2190(i5)) == null) {
            return false;
        }
        if (i == 0 && i2 == 0 && i3 == 0 && i4 == 0) {
            if (iArr2 != null) {
                iArr2[0] = 0;
                iArr2[1] = 0;
            }
            return false;
        }
        if (iArr2 != null) {
            this.f1249.getLocationInWindow(iArr2);
            i7 = iArr2[0];
            i6 = iArr2[1];
        } else {
            i7 = 0;
            i6 = 0;
        }
        C0430.m2354(r4, this.f1249, i, i2, i3, i4, i5);
        if (iArr2 != null) {
            this.f1249.getLocationInWindow(iArr2);
            iArr2[0] = iArr2[0] - i7;
            iArr2[1] = iArr2[1] - i6;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2199(int i, int i2, int[] iArr, int[] iArr2) {
        return m2200(i, i2, iArr, iArr2, 0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2200(int i, int i2, int[] iArr, int[] iArr2, int i3) {
        ViewParent r2;
        int i4;
        int i5;
        if (!m2192() || (r2 = m2190(i3)) == null) {
            return false;
        }
        if (i == 0 && i2 == 0) {
            if (iArr2 != null) {
                iArr2[0] = 0;
                iArr2[1] = 0;
            }
            return false;
        }
        if (iArr2 != null) {
            this.f1249.getLocationInWindow(iArr2);
            i5 = iArr2[0];
            i4 = iArr2[1];
        } else {
            i5 = 0;
            i4 = 0;
        }
        if (iArr == null) {
            if (this.f1251 == null) {
                this.f1251 = new int[2];
            }
            iArr = this.f1251;
        }
        iArr[0] = 0;
        iArr[1] = 0;
        C0430.m2355(r2, this.f1249, i, i2, iArr, i3);
        if (iArr2 != null) {
            this.f1249.getLocationInWindow(iArr2);
            iArr2[0] = iArr2[0] - i5;
            iArr2[1] = iArr2[1] - i4;
        }
        if (iArr[0] == 0 && iArr[1] == 0) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2194(float f, float f2, boolean z) {
        ViewParent r0;
        if (!m2192() || (r0 = m2190(0)) == null) {
            return false;
        }
        return C0430.m2357(r0, this.f1249, f, f2, z);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2193(float f, float f2) {
        ViewParent r0;
        if (!m2192() || (r0 = m2190(0)) == null) {
            return false;
        }
        return C0430.m2356(r0, this.f1249, f, f2);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private ViewParent m2190(int i) {
        if (i == 0) {
            return this.f1247;
        }
        if (i != 1) {
            return null;
        }
        return this.f1248;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m2189(int i, ViewParent viewParent) {
        if (i == 0) {
            this.f1247 = viewParent;
        } else if (i == 1) {
            this.f1248 = viewParent;
        }
    }
}
