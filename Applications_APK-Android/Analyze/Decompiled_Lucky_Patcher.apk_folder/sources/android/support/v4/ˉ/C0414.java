package android.support.v4.ˉ;

import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.Display;
import android.view.View;
import android.view.ViewParent;
import android.view.WindowInsets;
import android.view.WindowManager;
import java.lang.reflect.Field;
import java.util.WeakHashMap;

/* renamed from: android.support.v4.ˉ.ᐧ  reason: contains not printable characters */
/* compiled from: ViewCompat */
public class C0414 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0424 f1254;

    /* renamed from: android.support.v4.ˉ.ᐧ$ˋ  reason: contains not printable characters */
    /* compiled from: ViewCompat */
    static class C0424 {

        /* renamed from: ʼ  reason: contains not printable characters */
        static Field f1258 = null;

        /* renamed from: ʽ  reason: contains not printable characters */
        static boolean f1259 = false;

        /* renamed from: ʾ  reason: contains not printable characters */
        private static Field f1260;

        /* renamed from: ʿ  reason: contains not printable characters */
        private static boolean f1261;

        /* renamed from: ˆ  reason: contains not printable characters */
        private static Field f1262;

        /* renamed from: ˈ  reason: contains not printable characters */
        private static boolean f1263;

        /* renamed from: ˉ  reason: contains not printable characters */
        private static WeakHashMap<View, String> f1264;

        /* renamed from: ʻ  reason: contains not printable characters */
        WeakHashMap<View, C0434> f1265 = null;

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0439 m2306(View view, C0439 r2) {
            return r2;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2307(View view, float f) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2308(View view, int i) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2309(View view, int i, int i2) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2315(View view, C0412 r2) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2318(View view) {
            return false;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public C0439 m2319(View view, C0439 r2) {
            return r2;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m2321(View view) {
            return false;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public int m2324(View view) {
            return 0;
        }

        /* renamed from: ˉ  reason: contains not printable characters */
        public void m2328(View view) {
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public boolean m2329(View view) {
            return false;
        }

        /* renamed from: ˋ  reason: contains not printable characters */
        public boolean m2330(View view) {
            return true;
        }

        /* renamed from: ˎ  reason: contains not printable characters */
        public int m2331(View view) {
            return 0;
        }

        /* renamed from: י  reason: contains not printable characters */
        public int m2334(View view) {
            return 0;
        }

        /* renamed from: ـ  reason: contains not printable characters */
        public boolean m2335(View view) {
            return false;
        }

        /* renamed from: ᵔ  reason: contains not printable characters */
        public float m2342(View view) {
            return 0.0f;
        }

        /* renamed from: ᵢ  reason: contains not printable characters */
        public float m2343(View view) {
            return 0.0f;
        }

        C0424() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2314(View view, C0386 r2) {
            view.setAccessibilityDelegate(r2 == null ? null : r2.m2127());
        }

        /* renamed from: ᐧᐧ  reason: contains not printable characters */
        public boolean m2338(View view) {
            if (f1259) {
                return false;
            }
            if (f1258 == null) {
                try {
                    f1258 = View.class.getDeclaredField("mAccessibilityDelegate");
                    f1258.setAccessible(true);
                } catch (Throwable unused) {
                    f1259 = true;
                    return false;
                }
            }
            try {
                if (f1258.get(view) != null) {
                    return true;
                }
                return false;
            } catch (Throwable unused2) {
                f1259 = true;
                return false;
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m2322(View view) {
            view.postInvalidate();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2316(View view, Runnable runnable) {
            view.postDelayed(runnable, m2305());
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2317(View view, Runnable runnable, long j) {
            view.postDelayed(runnable, m2305() + j);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public long m2305() {
            return ValueAnimator.getFrameDelay();
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public ViewParent m2325(View view) {
            return view.getParent();
        }

        /* renamed from: ˏ  reason: contains not printable characters */
        public int m2332(View view) {
            return view.getPaddingLeft();
        }

        /* renamed from: ˑ  reason: contains not printable characters */
        public int m2333(View view) {
            return view.getPaddingRight();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2310(View view, int i, int i2, int i3, int i4) {
            view.setPadding(i, i2, i3, i4);
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public int m2326(View view) {
            if (!f1261) {
                try {
                    f1260 = View.class.getDeclaredField("mMinWidth");
                    f1260.setAccessible(true);
                } catch (NoSuchFieldException unused) {
                }
                f1261 = true;
            }
            Field field = f1260;
            if (field == null) {
                return 0;
            }
            try {
                return ((Integer) field.get(view)).intValue();
            } catch (Exception unused2) {
                return 0;
            }
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public int m2327(View view) {
            if (!f1263) {
                try {
                    f1262 = View.class.getDeclaredField("mMinHeight");
                    f1262.setAccessible(true);
                } catch (NoSuchFieldException unused) {
                }
                f1263 = true;
            }
            Field field = f1262;
            if (field == null) {
                return 0;
            }
            try {
                return ((Integer) field.get(view)).intValue();
            } catch (Exception unused2) {
                return 0;
            }
        }

        /* renamed from: ᴵᴵ  reason: contains not printable characters */
        public C0434 m2340(View view) {
            if (this.f1265 == null) {
                this.f1265 = new WeakHashMap<>();
            }
            C0434 r0 = this.f1265.get(view);
            if (r0 != null) {
                return r0;
            }
            C0434 r02 = new C0434(view);
            this.f1265.put(view, r02);
            return r02;
        }

        /* renamed from: ᵎ  reason: contains not printable characters */
        public String m2341(View view) {
            WeakHashMap<View, String> weakHashMap = f1264;
            if (weakHashMap == null) {
                return null;
            }
            return weakHashMap.get(view);
        }

        /* renamed from: ⁱ  reason: contains not printable characters */
        public boolean m2344(View view) {
            if (view instanceof C0406) {
                return ((C0406) view).isNestedScrollingEnabled();
            }
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2313(View view, Drawable drawable) {
            view.setBackgroundDrawable(drawable);
        }

        /* renamed from: ﹶ  reason: contains not printable characters */
        public ColorStateList m2346(View view) {
            if (view instanceof C0413) {
                return ((C0413) view).getSupportBackgroundTintList();
            }
            return null;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2311(View view, ColorStateList colorStateList) {
            if (view instanceof C0413) {
                ((C0413) view).setSupportBackgroundTintList(colorStateList);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2312(View view, PorterDuff.Mode mode) {
            if (view instanceof C0413) {
                ((C0413) view).setSupportBackgroundTintMode(mode);
            }
        }

        /* renamed from: ﾞ  reason: contains not printable characters */
        public PorterDuff.Mode m2347(View view) {
            if (view instanceof C0413) {
                return ((C0413) view).getSupportBackgroundTintMode();
            }
            return null;
        }

        /* renamed from: ﹳ  reason: contains not printable characters */
        public void m2345(View view) {
            if (view instanceof C0406) {
                ((C0406) view).stopNestedScroll();
            }
        }

        /* renamed from: ᐧ  reason: contains not printable characters */
        public boolean m2337(View view) {
            return view.getWidth() > 0 && view.getHeight() > 0;
        }

        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        public float m2348(View view) {
            return m2343(view) + m2342(view);
        }

        /* renamed from: ᴵ  reason: contains not printable characters */
        public boolean m2339(View view) {
            return view.getWindowToken() != null;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2320(View view, int i) {
            view.offsetLeftAndRight(i);
            if (view.getVisibility() == 0) {
                m2304(view);
                ViewParent parent = view.getParent();
                if (parent instanceof View) {
                    m2304((View) parent);
                }
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m2323(View view, int i) {
            view.offsetTopAndBottom(i);
            if (view.getVisibility() == 0) {
                m2304(view);
                ViewParent parent = view.getParent();
                if (parent instanceof View) {
                    m2304((View) parent);
                }
            }
        }

        /* renamed from: ʻʻ  reason: contains not printable characters */
        private static void m2304(View view) {
            float translationY = view.getTranslationY();
            view.setTranslationY(1.0f + translationY);
            view.setTranslationY(translationY);
        }

        /* renamed from: ٴ  reason: contains not printable characters */
        public Display m2336(View view) {
            if (m2339(view)) {
                return ((WindowManager) view.getContext().getSystemService("window")).getDefaultDisplay();
            }
            return null;
        }
    }

    /* renamed from: android.support.v4.ˉ.ᐧ$ʻ  reason: contains not printable characters */
    /* compiled from: ViewCompat */
    static class C0415 extends C0424 {
        C0415() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2259(View view) {
            return view.hasOnClickListeners();
        }
    }

    /* renamed from: android.support.v4.ˉ.ᐧ$ʼ  reason: contains not printable characters */
    /* compiled from: ViewCompat */
    static class C0416 extends C0415 {
        C0416() {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m2264(View view) {
            return view.hasTransientState();
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m2265(View view) {
            view.postInvalidateOnAnimation();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2262(View view, Runnable runnable) {
            view.postOnAnimation(runnable);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2263(View view, Runnable runnable, long j) {
            view.postOnAnimationDelayed(runnable, j);
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public int m2266(View view) {
            return view.getImportantForAccessibility();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2260(View view, int i) {
            if (i == 4) {
                i = 2;
            }
            view.setImportantForAccessibility(i);
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public ViewParent m2267(View view) {
            return view.getParentForAccessibility();
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public int m2268(View view) {
            return view.getMinimumWidth();
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public int m2269(View view) {
            return view.getMinimumHeight();
        }

        /* renamed from: ˉ  reason: contains not printable characters */
        public void m2270(View view) {
            view.requestFitSystemWindows();
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public boolean m2271(View view) {
            return view.getFitsSystemWindows();
        }

        /* renamed from: ˋ  reason: contains not printable characters */
        public boolean m2272(View view) {
            return view.hasOverlappingRendering();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2261(View view, Drawable drawable) {
            view.setBackground(drawable);
        }
    }

    /* renamed from: android.support.v4.ˉ.ᐧ$ʽ  reason: contains not printable characters */
    /* compiled from: ViewCompat */
    static class C0417 extends C0416 {
        C0417() {
        }

        /* renamed from: ˎ  reason: contains not printable characters */
        public int m2274(View view) {
            return view.getLayoutDirection();
        }

        /* renamed from: ˏ  reason: contains not printable characters */
        public int m2275(View view) {
            return view.getPaddingStart();
        }

        /* renamed from: ˑ  reason: contains not printable characters */
        public int m2276(View view) {
            return view.getPaddingEnd();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2273(View view, int i, int i2, int i3, int i4) {
            view.setPaddingRelative(i, i2, i3, i4);
        }

        /* renamed from: י  reason: contains not printable characters */
        public int m2277(View view) {
            return view.getWindowSystemUiVisibility();
        }

        /* renamed from: ـ  reason: contains not printable characters */
        public boolean m2278(View view) {
            return view.isPaddingRelative();
        }

        /* renamed from: ٴ  reason: contains not printable characters */
        public Display m2279(View view) {
            return view.getDisplay();
        }
    }

    /* renamed from: android.support.v4.ˉ.ᐧ$ʾ  reason: contains not printable characters */
    /* compiled from: ViewCompat */
    static class C0418 extends C0417 {
        C0418() {
        }
    }

    /* renamed from: android.support.v4.ˉ.ᐧ$ʿ  reason: contains not printable characters */
    /* compiled from: ViewCompat */
    static class C0419 extends C0418 {
        C0419() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2280(View view, int i) {
            view.setImportantForAccessibility(i);
        }

        /* renamed from: ᐧ  reason: contains not printable characters */
        public boolean m2281(View view) {
            return view.isLaidOut();
        }

        /* renamed from: ᴵ  reason: contains not printable characters */
        public boolean m2282(View view) {
            return view.isAttachedToWindow();
        }
    }

    /* renamed from: android.support.v4.ˉ.ᐧ$ˆ  reason: contains not printable characters */
    /* compiled from: ViewCompat */
    static class C0420 extends C0419 {

        /* renamed from: ʾ  reason: contains not printable characters */
        private static ThreadLocal<Rect> f1255;

        C0420() {
        }

        /* renamed from: ᵎ  reason: contains not printable characters */
        public String m2293(View view) {
            return view.getTransitionName();
        }

        /* renamed from: ˉ  reason: contains not printable characters */
        public void m2292(View view) {
            view.requestApplyInsets();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2285(View view, float f) {
            view.setElevation(f);
        }

        /* renamed from: ᵔ  reason: contains not printable characters */
        public float m2294(View view) {
            return view.getElevation();
        }

        /* renamed from: ᵢ  reason: contains not printable characters */
        public float m2295(View view) {
            return view.getTranslationZ();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2288(View view, final C0412 r3) {
            if (r3 == null) {
                view.setOnApplyWindowInsetsListener(null);
            } else {
                view.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
                    public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
                        return (WindowInsets) C0439.m2396(r3.m2215(view, C0439.m2395(windowInsets)));
                    }
                });
            }
        }

        /* renamed from: ⁱ  reason: contains not printable characters */
        public boolean m2296(View view) {
            return view.isNestedScrollingEnabled();
        }

        /* renamed from: ﹳ  reason: contains not printable characters */
        public void m2297(View view) {
            view.stopNestedScroll();
        }

        /* renamed from: ﹶ  reason: contains not printable characters */
        public ColorStateList m2298(View view) {
            return view.getBackgroundTintList();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2286(View view, ColorStateList colorStateList) {
            view.setBackgroundTintList(colorStateList);
            if (Build.VERSION.SDK_INT == 21) {
                Drawable background = view.getBackground();
                boolean z = (view.getBackgroundTintList() == null || view.getBackgroundTintMode() == null) ? false : true;
                if (background != null && z) {
                    if (background.isStateful()) {
                        background.setState(view.getDrawableState());
                    }
                    view.setBackground(background);
                }
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2287(View view, PorterDuff.Mode mode) {
            view.setBackgroundTintMode(mode);
            if (Build.VERSION.SDK_INT == 21) {
                Drawable background = view.getBackground();
                boolean z = (view.getBackgroundTintList() == null || view.getBackgroundTintMode() == null) ? false : true;
                if (background != null && z) {
                    if (background.isStateful()) {
                        background.setState(view.getDrawableState());
                    }
                    view.setBackground(background);
                }
            }
        }

        /* renamed from: ﾞ  reason: contains not printable characters */
        public PorterDuff.Mode m2299(View view) {
            return view.getBackgroundTintMode();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0439 m2284(View view, C0439 r2) {
            WindowInsets windowInsets = (WindowInsets) C0439.m2396(r2);
            WindowInsets onApplyWindowInsets = view.onApplyWindowInsets(windowInsets);
            if (onApplyWindowInsets != windowInsets) {
                windowInsets = new WindowInsets(onApplyWindowInsets);
            }
            return C0439.m2395(windowInsets);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public C0439 m2289(View view, C0439 r2) {
            WindowInsets windowInsets = (WindowInsets) C0439.m2396(r2);
            WindowInsets dispatchApplyWindowInsets = view.dispatchApplyWindowInsets(windowInsets);
            if (dispatchApplyWindowInsets != windowInsets) {
                windowInsets = new WindowInsets(dispatchApplyWindowInsets);
            }
            return C0439.m2395(windowInsets);
        }

        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        public float m2300(View view) {
            return view.getZ();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2290(View view, int i) {
            boolean z;
            Rect r0 = m2283();
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                View view2 = (View) parent;
                r0.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                z = !r0.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            } else {
                z = false;
            }
            super.m2320(view, i);
            if (z && r0.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View) parent).invalidate(r0);
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m2291(View view, int i) {
            boolean z;
            Rect r0 = m2283();
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                View view2 = (View) parent;
                r0.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                z = !r0.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            } else {
                z = false;
            }
            super.m2323(view, i);
            if (z && r0.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View) parent).invalidate(r0);
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private static Rect m2283() {
            if (f1255 == null) {
                f1255 = new ThreadLocal<>();
            }
            Rect rect = f1255.get();
            if (rect == null) {
                rect = new Rect();
                f1255.set(rect);
            }
            rect.setEmpty();
            return rect;
        }
    }

    /* renamed from: android.support.v4.ˉ.ᐧ$ˈ  reason: contains not printable characters */
    /* compiled from: ViewCompat */
    static class C0421 extends C0420 {
        C0421() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2301(View view, int i, int i2) {
            view.setScrollIndicators(i, i2);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2302(View view, int i) {
            view.offsetLeftAndRight(i);
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m2303(View view, int i) {
            view.offsetTopAndBottom(i);
        }
    }

    /* renamed from: android.support.v4.ˉ.ᐧ$ˉ  reason: contains not printable characters */
    /* compiled from: ViewCompat */
    static class C0422 extends C0421 {
        C0422() {
        }
    }

    /* renamed from: android.support.v4.ˉ.ᐧ$ˊ  reason: contains not printable characters */
    /* compiled from: ViewCompat */
    static class C0423 extends C0422 {
        C0423() {
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 26) {
            f1254 = new C0423();
        } else if (Build.VERSION.SDK_INT >= 24) {
            f1254 = new C0422();
        } else if (Build.VERSION.SDK_INT >= 23) {
            f1254 = new C0421();
        } else if (Build.VERSION.SDK_INT >= 21) {
            f1254 = new C0420();
        } else if (Build.VERSION.SDK_INT >= 19) {
            f1254 = new C0419();
        } else if (Build.VERSION.SDK_INT >= 18) {
            f1254 = new C0418();
        } else if (Build.VERSION.SDK_INT >= 17) {
            f1254 = new C0417();
        } else if (Build.VERSION.SDK_INT >= 16) {
            f1254 = new C0416();
        } else if (Build.VERSION.SDK_INT >= 15) {
            f1254 = new C0415();
        } else {
            f1254 = new C0424();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2224(View view, C0386 r2) {
        f1254.m2314(view, r2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m2229(View view) {
        return f1254.m2338(view);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static boolean m2232(View view) {
        return f1254.m2321(view);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static void m2233(View view) {
        f1254.m2322(view);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2226(View view, Runnable runnable) {
        f1254.m2316(view, runnable);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2227(View view, Runnable runnable, long j) {
        f1254.m2317(view, runnable, j);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static int m2235(View view) {
        return f1254.m2324(view);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2218(View view, int i) {
        f1254.m2308(view, i);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public static int m2236(View view) {
        return f1254.m2331(view);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public static ViewParent m2237(View view) {
        return f1254.m2325(view);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public static int m2238(View view) {
        return f1254.m2332(view);
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public static int m2239(View view) {
        return f1254.m2333(view);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2220(View view, int i, int i2, int i3, int i4) {
        f1254.m2310(view, i, i2, i3, i4);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static int m2240(View view) {
        return f1254.m2326(view);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public static int m2241(View view) {
        return f1254.m2327(view);
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public static C0434 m2242(View view) {
        return f1254.m2340(view);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2217(View view, float f) {
        f1254.m2307(view, f);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public static float m2243(View view) {
        return f1254.m2342(view);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public static String m2244(View view) {
        return f1254.m2341(view);
    }

    /* renamed from: י  reason: contains not printable characters */
    public static int m2245(View view) {
        return f1254.m2334(view);
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public static void m2246(View view) {
        f1254.m2328(view);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public static boolean m2247(View view) {
        return f1254.m2329(view);
    }

    @Deprecated
    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2228(View view, boolean z) {
        view.setFitsSystemWindows(z);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2225(View view, C0412 r2) {
        f1254.m2315(view, r2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0439 m2216(View view, C0439 r2) {
        return f1254.m2306(view, r2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static C0439 m2230(View view, C0439 r2) {
        return f1254.m2319(view, r2);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static boolean m2248(View view) {
        return f1254.m2330(view);
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public static boolean m2250(View view) {
        return f1254.m2335(view);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2223(View view, Drawable drawable) {
        f1254.m2313(view, drawable);
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public static ColorStateList m2251(View view) {
        return f1254.m2346(view);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2221(View view, ColorStateList colorStateList) {
        f1254.m2311(view, colorStateList);
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    public static PorterDuff.Mode m2252(View view) {
        return f1254.m2347(view);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2222(View view, PorterDuff.Mode mode) {
        f1254.m2312(view, mode);
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    public static boolean m2253(View view) {
        return f1254.m2344(view);
    }

    /* renamed from: ⁱ  reason: contains not printable characters */
    public static void m2254(View view) {
        f1254.m2345(view);
    }

    /* renamed from: ﹳ  reason: contains not printable characters */
    public static boolean m2255(View view) {
        return f1254.m2337(view);
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public static float m2256(View view) {
        return f1254.m2348(view);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static void m2231(View view, int i) {
        f1254.m2323(view, i);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static void m2234(View view, int i) {
        f1254.m2320(view, i);
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public static boolean m2257(View view) {
        return f1254.m2339(view);
    }

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public static boolean m2258(View view) {
        return f1254.m2318(view);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m2219(View view, int i, int i2) {
        f1254.m2309(view, i, i2);
    }

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public static Display m2249(View view) {
        return f1254.m2336(view);
    }
}
