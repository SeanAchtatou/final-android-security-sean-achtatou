package android.support.v4.ˉ;

import android.os.Build;
import android.view.ViewGroup;

/* renamed from: android.support.v4.ˉ.ˆ  reason: contains not printable characters */
/* compiled from: MarginLayoutParamsCompat */
public final class C0400 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m2161(ViewGroup.MarginLayoutParams marginLayoutParams) {
        if (Build.VERSION.SDK_INT >= 17) {
            return marginLayoutParams.getMarginStart();
        }
        return marginLayoutParams.leftMargin;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int m2162(ViewGroup.MarginLayoutParams marginLayoutParams) {
        if (Build.VERSION.SDK_INT >= 17) {
            return marginLayoutParams.getMarginEnd();
        }
        return marginLayoutParams.rightMargin;
    }
}
