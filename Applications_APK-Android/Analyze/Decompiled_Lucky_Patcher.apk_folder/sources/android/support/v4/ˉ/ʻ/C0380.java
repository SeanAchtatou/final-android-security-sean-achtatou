package android.support.v4.ˉ.ʻ;

import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.List;

/* renamed from: android.support.v4.ˉ.ʻ.ʿ  reason: contains not printable characters */
/* compiled from: AccessibilityNodeProviderCompatKitKat */
class C0380 {

    /* renamed from: android.support.v4.ˉ.ʻ.ʿ$ʻ  reason: contains not printable characters */
    /* compiled from: AccessibilityNodeProviderCompatKitKat */
    interface C0381 {
        /* renamed from: ʻ  reason: contains not printable characters */
        Object m2116(int i);

        /* renamed from: ʻ  reason: contains not printable characters */
        List<Object> m2117(String str, int i);

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m2118(int i, int i2, Bundle bundle);

        /* renamed from: ʼ  reason: contains not printable characters */
        Object m2119(int i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Object m2115(final C0381 r1) {
        return new AccessibilityNodeProvider() {
            public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
                return (AccessibilityNodeInfo) r1.m2116(i);
            }

            public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
                return r1.m2117(str, i);
            }

            public boolean performAction(int i, int i2, Bundle bundle) {
                return r1.m2118(i, i2, bundle);
            }

            public AccessibilityNodeInfo findFocus(int i) {
                return (AccessibilityNodeInfo) r1.m2119(i);
            }
        };
    }
}
