package android.support.v4.content.ʻ;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: android.support.v4.content.ʻ.ʽ  reason: contains not printable characters */
/* compiled from: TypedArrayUtils */
public class C0108 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m569(XmlPullParser xmlPullParser, String str) {
        return xmlPullParser.getAttributeValue("http://schemas.android.com/apk/res/android", str) != null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static float m564(TypedArray typedArray, XmlPullParser xmlPullParser, String str, int i, float f) {
        if (!m569(xmlPullParser, str)) {
            return f;
        }
        return typedArray.getFloat(i, f);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m568(TypedArray typedArray, XmlPullParser xmlPullParser, String str, int i, boolean z) {
        if (!m569(xmlPullParser, str)) {
            return z;
        }
        return typedArray.getBoolean(i, z);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m565(TypedArray typedArray, XmlPullParser xmlPullParser, String str, int i, int i2) {
        if (!m569(xmlPullParser, str)) {
            return i2;
        }
        return typedArray.getInt(i, i2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int m570(TypedArray typedArray, XmlPullParser xmlPullParser, String str, int i, int i2) {
        if (!m569(xmlPullParser, str)) {
            return i2;
        }
        return typedArray.getColor(i, i2);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static int m572(TypedArray typedArray, XmlPullParser xmlPullParser, String str, int i, int i2) {
        if (!m569(xmlPullParser, str)) {
            return i2;
        }
        return typedArray.getResourceId(i, i2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m567(TypedArray typedArray, XmlPullParser xmlPullParser, String str, int i) {
        if (!m569(xmlPullParser, str)) {
            return null;
        }
        return typedArray.getString(i);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static TypedValue m571(TypedArray typedArray, XmlPullParser xmlPullParser, String str, int i) {
        if (!m569(xmlPullParser, str)) {
            return null;
        }
        return typedArray.peekValue(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static TypedArray m566(Resources resources, Resources.Theme theme, AttributeSet attributeSet, int[] iArr) {
        if (theme == null) {
            return resources.obtainAttributes(attributeSet, iArr);
        }
        return theme.obtainStyledAttributes(attributeSet, iArr, 0, 0);
    }
}
