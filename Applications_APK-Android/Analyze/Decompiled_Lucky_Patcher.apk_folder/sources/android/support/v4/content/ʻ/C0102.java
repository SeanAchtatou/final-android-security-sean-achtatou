package android.support.v4.content.ʻ;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.v4.ˆ.C0325;
import android.support.ʻ.C0747;
import android.util.Base64;
import android.util.Xml;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* renamed from: android.support.v4.content.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: FontResourcesParserCompat */
public class C0102 {

    /* renamed from: android.support.v4.content.ʻ.ʻ$ʻ  reason: contains not printable characters */
    /* compiled from: FontResourcesParserCompat */
    public interface C0103 {
    }

    /* renamed from: android.support.v4.content.ʻ.ʻ$ʾ  reason: contains not printable characters */
    /* compiled from: FontResourcesParserCompat */
    public static final class C0106 implements C0103 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final C0325 f388;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final int f389;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final int f390;

        public C0106(C0325 r1, int i, int i2) {
            this.f388 = r1;
            this.f390 = i;
            this.f389 = i2;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0325 m557() {
            return this.f388;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m558() {
            return this.f390;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public int m559() {
            return this.f389;
        }
    }

    /* renamed from: android.support.v4.content.ʻ.ʻ$ʽ  reason: contains not printable characters */
    /* compiled from: FontResourcesParserCompat */
    public static final class C0105 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final String f384;

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f385;

        /* renamed from: ʽ  reason: contains not printable characters */
        private boolean f386;

        /* renamed from: ʾ  reason: contains not printable characters */
        private int f387;

        public C0105(String str, int i, boolean z, int i2) {
            this.f384 = str;
            this.f385 = i;
            this.f386 = z;
            this.f387 = i2;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public String m553() {
            return this.f384;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m554() {
            return this.f385;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m555() {
            return this.f386;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public int m556() {
            return this.f387;
        }
    }

    /* renamed from: android.support.v4.content.ʻ.ʻ$ʼ  reason: contains not printable characters */
    /* compiled from: FontResourcesParserCompat */
    public static final class C0104 implements C0103 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final C0105[] f383;

        public C0104(C0105[] r1) {
            this.f383 = r1;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0105[] m552() {
            return this.f383;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0103 m545(XmlPullParser xmlPullParser, Resources resources) {
        int next;
        do {
            next = xmlPullParser.next();
            if (next == 2) {
                break;
            }
        } while (next != 1);
        if (next == 2) {
            return m549(xmlPullParser, resources);
        }
        throw new XmlPullParserException("No start tag found");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static C0103 m549(XmlPullParser xmlPullParser, Resources resources) {
        xmlPullParser.require(2, null, "font-family");
        if (xmlPullParser.getName().equals("font-family")) {
            return m550(xmlPullParser, resources);
        }
        m548(xmlPullParser);
        return null;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static C0103 m550(XmlPullParser xmlPullParser, Resources resources) {
        TypedArray obtainAttributes = resources.obtainAttributes(Xml.asAttributeSet(xmlPullParser), C0747.C0748.FontFamily);
        String string = obtainAttributes.getString(C0747.C0748.FontFamily_fontProviderAuthority);
        String string2 = obtainAttributes.getString(C0747.C0748.FontFamily_fontProviderPackage);
        String string3 = obtainAttributes.getString(C0747.C0748.FontFamily_fontProviderQuery);
        int resourceId = obtainAttributes.getResourceId(C0747.C0748.FontFamily_fontProviderCerts, 0);
        int integer = obtainAttributes.getInteger(C0747.C0748.FontFamily_fontProviderFetchStrategy, 1);
        int integer2 = obtainAttributes.getInteger(C0747.C0748.FontFamily_fontProviderFetchTimeout, 500);
        obtainAttributes.recycle();
        if (string == null || string2 == null || string3 == null) {
            ArrayList arrayList = new ArrayList();
            while (xmlPullParser.next() != 3) {
                if (xmlPullParser.getEventType() == 2) {
                    if (xmlPullParser.getName().equals("font")) {
                        arrayList.add(m551(xmlPullParser, resources));
                    } else {
                        m548(xmlPullParser);
                    }
                }
            }
            if (arrayList.isEmpty()) {
                return null;
            }
            return new C0104((C0105[]) arrayList.toArray(new C0105[arrayList.size()]));
        }
        while (xmlPullParser.next() != 3) {
            m548(xmlPullParser);
        }
        return new C0106(new C0325(string, string2, string3, m546(resources, resourceId)), integer, integer2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static List<List<byte[]>> m546(Resources resources, int i) {
        ArrayList arrayList = null;
        if (i != 0) {
            TypedArray obtainTypedArray = resources.obtainTypedArray(i);
            if (obtainTypedArray.length() > 0) {
                arrayList = new ArrayList();
                if (obtainTypedArray.getResourceId(0, 0) != 0) {
                    for (int i2 = 0; i2 < obtainTypedArray.length(); i2++) {
                        arrayList.add(m547(resources.getStringArray(obtainTypedArray.getResourceId(i2, 0))));
                    }
                } else {
                    arrayList.add(m547(resources.getStringArray(i)));
                }
            }
            obtainTypedArray.recycle();
        }
        return arrayList != null ? arrayList : Collections.emptyList();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static List<byte[]> m547(String[] strArr) {
        ArrayList arrayList = new ArrayList();
        for (String decode : strArr) {
            arrayList.add(Base64.decode(decode, 0));
        }
        return arrayList;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private static C0105 m551(XmlPullParser xmlPullParser, Resources resources) {
        TypedArray obtainAttributes = resources.obtainAttributes(Xml.asAttributeSet(xmlPullParser), C0747.C0748.FontFamilyFont);
        int i = obtainAttributes.getInt(C0747.C0748.FontFamilyFont_fontWeight, 400);
        boolean z = true;
        if (1 != obtainAttributes.getInt(C0747.C0748.FontFamilyFont_fontStyle, 0)) {
            z = false;
        }
        int resourceId = obtainAttributes.getResourceId(C0747.C0748.FontFamilyFont_font, 0);
        String string = obtainAttributes.getString(C0747.C0748.FontFamilyFont_font);
        obtainAttributes.recycle();
        while (xmlPullParser.next() != 3) {
            m548(xmlPullParser);
        }
        return new C0105(string, i, z, resourceId);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m548(XmlPullParser xmlPullParser) {
        int i = 1;
        while (i > 0) {
            int next = xmlPullParser.next();
            if (next == 2) {
                i++;
            } else if (next == 3) {
                i--;
            }
        }
    }
}
