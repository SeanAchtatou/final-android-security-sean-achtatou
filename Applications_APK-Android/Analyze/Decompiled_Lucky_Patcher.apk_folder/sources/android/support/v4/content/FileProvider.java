package android.support.v4.content;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ProviderInfo;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipConstants;
import org.xmlpull.v1.XmlPullParserException;

public class FileProvider extends ContentProvider {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final String[] f375 = {"_display_name", "_size"};

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final File f376 = new File(InternalZipConstants.ZIP_FILE_SEPARATOR);

    /* renamed from: ʽ  reason: contains not printable characters */
    private static HashMap<String, C0099> f377 = new HashMap<>();

    /* renamed from: ʾ  reason: contains not printable characters */
    private C0099 f378;

    /* renamed from: android.support.v4.content.FileProvider$ʻ  reason: contains not printable characters */
    interface C0099 {
        /* renamed from: ʻ  reason: contains not printable characters */
        Uri m534(File file);

        /* renamed from: ʻ  reason: contains not printable characters */
        File m535(Uri uri);
    }

    public boolean onCreate() {
        return true;
    }

    public void attachInfo(Context context, ProviderInfo providerInfo) {
        super.attachInfo(context, providerInfo);
        if (providerInfo.exported) {
            throw new SecurityException("Provider must not be exported");
        } else if (providerInfo.grantUriPermissions) {
            this.f378 = m529(context, providerInfo.authority);
        } else {
            throw new SecurityException("Provider must grant uri permissions");
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Uri m528(Context context, String str, File file) {
        return m529(context, str).m534(file);
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        int i;
        File r6 = this.f378.m535(uri);
        if (strArr == null) {
            strArr = f375;
        }
        String[] strArr3 = new String[strArr.length];
        Object[] objArr = new Object[strArr.length];
        int i2 = 0;
        for (String str3 : strArr) {
            if ("_display_name".equals(str3)) {
                strArr3[i2] = "_display_name";
                i = i2 + 1;
                objArr[i2] = r6.getName();
            } else if ("_size".equals(str3)) {
                strArr3[i2] = "_size";
                i = i2 + 1;
                objArr[i2] = Long.valueOf(r6.length());
            }
            i2 = i;
        }
        String[] r62 = m532(strArr3, i2);
        Object[] r7 = m531(objArr, i2);
        MatrixCursor matrixCursor = new MatrixCursor(r62, 1);
        matrixCursor.addRow(r7);
        return matrixCursor;
    }

    public String getType(Uri uri) {
        String mimeTypeFromExtension;
        File r3 = this.f378.m535(uri);
        int lastIndexOf = r3.getName().lastIndexOf(46);
        return (lastIndexOf < 0 || (mimeTypeFromExtension = MimeTypeMap.getSingleton().getMimeTypeFromExtension(r3.getName().substring(lastIndexOf + 1))) == null) ? "application/octet-stream" : mimeTypeFromExtension;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        throw new UnsupportedOperationException("No external inserts");
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        throw new UnsupportedOperationException("No external updates");
    }

    public int delete(Uri uri, String str, String[] strArr) {
        return this.f378.m535(uri).delete() ? 1 : 0;
    }

    public ParcelFileDescriptor openFile(Uri uri, String str) {
        return ParcelFileDescriptor.open(this.f378.m535(uri), m527(str));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static C0099 m529(Context context, String str) {
        C0099 r1;
        synchronized (f377) {
            r1 = f377.get(str);
            if (r1 == null) {
                try {
                    r1 = m533(context, str);
                    f377.put(str, r1);
                } catch (IOException e) {
                    throw new IllegalArgumentException("Failed to parse android.support.FILE_PROVIDER_PATHS meta-data", e);
                } catch (XmlPullParserException e2) {
                    throw new IllegalArgumentException("Failed to parse android.support.FILE_PROVIDER_PATHS meta-data", e2);
                }
            }
        }
        return r1;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static C0099 m533(Context context, String str) {
        C0100 r0 = new C0100(str);
        XmlResourceParser loadXmlMetaData = context.getPackageManager().resolveContentProvider(str, 128).loadXmlMetaData(context.getPackageManager(), "android.support.FILE_PROVIDER_PATHS");
        if (loadXmlMetaData != null) {
            while (true) {
                int next = loadXmlMetaData.next();
                if (next == 1) {
                    return r0;
                }
                if (next == 2) {
                    String name = loadXmlMetaData.getName();
                    File file = null;
                    String attributeValue = loadXmlMetaData.getAttributeValue(null, "name");
                    String attributeValue2 = loadXmlMetaData.getAttributeValue(null, "path");
                    if ("root-path".equals(name)) {
                        file = f376;
                    } else if ("files-path".equals(name)) {
                        file = context.getFilesDir();
                    } else if ("cache-path".equals(name)) {
                        file = context.getCacheDir();
                    } else if ("external-path".equals(name)) {
                        file = Environment.getExternalStorageDirectory();
                    } else if ("external-files-path".equals(name)) {
                        File[] r1 = C0101.m542(context, (String) null);
                        if (r1.length > 0) {
                            file = r1[0];
                        }
                    } else if ("external-cache-path".equals(name)) {
                        File[] r12 = C0101.m541(context);
                        if (r12.length > 0) {
                            file = r12[0];
                        }
                    }
                    if (file != null) {
                        r0.m538(attributeValue, m530(file, attributeValue2));
                    }
                }
            }
        } else {
            throw new IllegalArgumentException("Missing android.support.FILE_PROVIDER_PATHS meta-data");
        }
    }

    /* renamed from: android.support.v4.content.FileProvider$ʼ  reason: contains not printable characters */
    static class C0100 implements C0099 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final String f379;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final HashMap<String, File> f380 = new HashMap<>();

        public C0100(String str) {
            this.f379 = str;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m538(String str, File file) {
            if (!TextUtils.isEmpty(str)) {
                try {
                    this.f380.put(str, file.getCanonicalFile());
                } catch (IOException e) {
                    throw new IllegalArgumentException("Failed to resolve canonical path for " + file, e);
                }
            } else {
                throw new IllegalArgumentException("Name must not be empty");
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Uri m536(File file) {
            String str;
            try {
                String canonicalPath = file.getCanonicalPath();
                Map.Entry entry = null;
                for (Map.Entry next : this.f380.entrySet()) {
                    String path = ((File) next.getValue()).getPath();
                    if (canonicalPath.startsWith(path) && (entry == null || path.length() > ((File) entry.getValue()).getPath().length())) {
                        entry = next;
                    }
                }
                if (entry != null) {
                    String path2 = ((File) entry.getValue()).getPath();
                    if (path2.endsWith(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                        str = canonicalPath.substring(path2.length());
                    } else {
                        str = canonicalPath.substring(path2.length() + 1);
                    }
                    return new Uri.Builder().scheme("content").authority(this.f379).encodedPath(Uri.encode((String) entry.getKey()) + '/' + Uri.encode(str, InternalZipConstants.ZIP_FILE_SEPARATOR)).build();
                }
                throw new IllegalArgumentException("Failed to find configured root that contains " + canonicalPath);
            } catch (IOException unused) {
                throw new IllegalArgumentException("Failed to resolve canonical path for " + file);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public File m537(Uri uri) {
            String encodedPath = uri.getEncodedPath();
            int indexOf = encodedPath.indexOf(47, 1);
            String decode = Uri.decode(encodedPath.substring(1, indexOf));
            String decode2 = Uri.decode(encodedPath.substring(indexOf + 1));
            File file = this.f380.get(decode);
            if (file != null) {
                File file2 = new File(file, decode2);
                try {
                    File canonicalFile = file2.getCanonicalFile();
                    if (canonicalFile.getPath().startsWith(file.getPath())) {
                        return canonicalFile;
                    }
                    throw new SecurityException("Resolved path jumped beyond configured root");
                } catch (IOException unused) {
                    throw new IllegalArgumentException("Failed to resolve canonical path for " + file2);
                }
            } else {
                throw new IllegalArgumentException("Unable to find configured root for " + uri);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static int m527(String str) {
        if (InternalZipConstants.READ_MODE.equals(str)) {
            return 268435456;
        }
        if ("w".equals(str) || "wt".equals(str)) {
            return 738197504;
        }
        if ("wa".equals(str)) {
            return 704643072;
        }
        if (InternalZipConstants.WRITE_MODE.equals(str)) {
            return 939524096;
        }
        if ("rwt".equals(str)) {
            return 1006632960;
        }
        throw new IllegalArgumentException("Invalid mode: " + str);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static File m530(File file, String... strArr) {
        for (String str : strArr) {
            if (str != null) {
                file = new File(file, str);
            }
        }
        return file;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static String[] m532(String[] strArr, int i) {
        String[] strArr2 = new String[i];
        System.arraycopy(strArr, 0, strArr2, 0, i);
        return strArr2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Object[] m531(Object[] objArr, int i) {
        Object[] objArr2 = new Object[i];
        System.arraycopy(objArr, 0, objArr2, 0, i);
        return objArr2;
    }
}
