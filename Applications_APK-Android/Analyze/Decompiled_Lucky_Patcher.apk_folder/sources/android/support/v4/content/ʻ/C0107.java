package android.support.v4.content.ʻ;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ʻ.C0102;
import android.support.v4.ʼ.C0306;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

/* renamed from: android.support.v4.content.ʻ.ʼ  reason: contains not printable characters */
/* compiled from: ResourcesCompat */
public final class C0107 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static Drawable m562(Resources resources, int i, Resources.Theme theme) {
        if (Build.VERSION.SDK_INT >= 21) {
            return resources.getDrawable(i, theme);
        }
        return resources.getDrawable(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Typeface m560(Context context, int i, TypedValue typedValue, int i2, TextView textView) {
        if (context.isRestricted()) {
            return null;
        }
        return m563(context, i, typedValue, i2, textView);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* renamed from: ʼ  reason: contains not printable characters */
    private static Typeface m563(Context context, int i, TypedValue typedValue, int i2, TextView textView) {
        Resources resources = context.getResources();
        resources.getValue(i, typedValue, true);
        Typeface r6 = m561(context, resources, typedValue, i, i2, textView);
        if (r6 != null) {
            return r6;
        }
        throw new Resources.NotFoundException("Font resource ID #0x" + Integer.toHexString(i));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Typeface m561(Context context, Resources resources, TypedValue typedValue, int i, int i2, TextView textView) {
        if (typedValue.string != null) {
            String charSequence = typedValue.string.toString();
            if (!charSequence.startsWith("res/")) {
                return null;
            }
            Typeface r1 = C0306.m1782(resources, i, i2);
            if (r1 != null) {
                return r1;
            }
            try {
                if (!charSequence.toLowerCase().endsWith(".xml")) {
                    return C0306.m1779(context, resources, i, charSequence, i2);
                }
                C0102.C0103 r4 = C0102.m545(resources.getXml(i), resources);
                if (r4 != null) {
                    return C0306.m1781(context, r4, resources, i, i2, textView);
                }
                Log.e("ResourcesCompat", "Failed to find font-family tag");
                return null;
            } catch (XmlPullParserException e) {
                Log.e("ResourcesCompat", "Failed to parse xml resource " + charSequence, e);
                return null;
            } catch (IOException e2) {
                Log.e("ResourcesCompat", "Failed to read xml resource " + charSequence, e2);
                return null;
            }
        } else {
            throw new Resources.NotFoundException("Resource \"" + resources.getResourceName(i) + "\" (" + Integer.toHexString(i) + ") is not a Font: " + typedValue);
        }
    }
}
