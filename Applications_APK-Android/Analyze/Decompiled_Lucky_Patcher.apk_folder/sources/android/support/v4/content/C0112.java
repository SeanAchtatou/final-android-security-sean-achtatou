package android.support.v4.content;

import android.content.Context;
import android.os.Process;
import android.support.v4.ʻ.C0197;

/* renamed from: android.support.v4.content.ʽ  reason: contains not printable characters */
/* compiled from: PermissionChecker */
public final class C0112 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m586(Context context, String str, int i, int i2, String str2) {
        if (context.checkPermission(str, i, i2) == -1) {
            return -1;
        }
        String r2 = C0197.m1133(str);
        if (r2 == null) {
            return 0;
        }
        if (str2 == null) {
            String[] packagesForUid = context.getPackageManager().getPackagesForUid(i2);
            if (packagesForUid == null || packagesForUid.length <= 0) {
                return -1;
            }
            str2 = packagesForUid[0];
        }
        if (C0197.m1132(context, r2, str2) != 0) {
            return -2;
        }
        return 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m585(Context context, String str) {
        return m586(context, str, Process.myPid(), Process.myUid(), context.getPackageName());
    }
}
