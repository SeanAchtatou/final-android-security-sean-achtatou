package android.support.v4.content;

import android.support.v4.ˈ.C0334;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* renamed from: android.support.v4.content.ʼ  reason: contains not printable characters */
/* compiled from: Loader */
public class C0109<D> {

    /* renamed from: ʻ  reason: contains not printable characters */
    int f391;

    /* renamed from: ʼ  reason: contains not printable characters */
    C0111<D> f392;

    /* renamed from: ʽ  reason: contains not printable characters */
    C0110<D> f393;

    /* renamed from: ʾ  reason: contains not printable characters */
    boolean f394;

    /* renamed from: ʿ  reason: contains not printable characters */
    boolean f395;

    /* renamed from: ˆ  reason: contains not printable characters */
    boolean f396;

    /* renamed from: ˈ  reason: contains not printable characters */
    boolean f397;

    /* renamed from: ˉ  reason: contains not printable characters */
    boolean f398;

    /* renamed from: android.support.v4.content.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: Loader */
    public interface C0110<D> {
    }

    /* renamed from: android.support.v4.content.ʼ$ʼ  reason: contains not printable characters */
    /* compiled from: Loader */
    public interface C0111<D> {
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m579() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m582() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m584() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m575(int i, C0111<D> r3) {
        if (this.f392 == null) {
            this.f392 = r3;
            this.f391 = i;
            return;
        }
        throw new IllegalStateException("There is already a listener registered");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m577(C0111<D> r2) {
        C0111<D> r0 = this.f392;
        if (r0 == null) {
            throw new IllegalStateException("No listener register");
        } else if (r0 == r2) {
            this.f392 = null;
        } else {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m576(C0110<D> r2) {
        if (this.f393 == null) {
            this.f393 = r2;
            return;
        }
        throw new IllegalStateException("There is already a listener registered");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m580(C0110<D> r2) {
        C0110<D> r0 = this.f393;
        if (r0 == null) {
            throw new IllegalStateException("No listener register");
        } else if (r0 == r2) {
            this.f393 = null;
        } else {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m574() {
        this.f394 = true;
        this.f396 = false;
        this.f395 = false;
        m579();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m581() {
        this.f394 = false;
        m582();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m583() {
        m584();
        this.f396 = true;
        this.f394 = false;
        this.f395 = false;
        this.f397 = false;
        this.f398 = false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m573(D d) {
        StringBuilder sb = new StringBuilder(64);
        C0334.m1918(d, sb);
        sb.append("}");
        return sb.toString();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        C0334.m1918(this, sb);
        sb.append(" id=");
        sb.append(this.f391);
        sb.append("}");
        return sb.toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m578(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mId=");
        printWriter.print(this.f391);
        printWriter.print(" mListener=");
        printWriter.println(this.f392);
        if (this.f394 || this.f397 || this.f398) {
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.print(this.f394);
            printWriter.print(" mContentChanged=");
            printWriter.print(this.f397);
            printWriter.print(" mProcessingChange=");
            printWriter.println(this.f398);
        }
        if (this.f395 || this.f396) {
            printWriter.print(str);
            printWriter.print("mAbandoned=");
            printWriter.print(this.f395);
            printWriter.print(" mReset=");
            printWriter.println(this.f396);
        }
    }
}
