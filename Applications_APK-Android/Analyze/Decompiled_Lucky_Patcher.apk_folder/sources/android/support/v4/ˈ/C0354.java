package android.support.v4.ˈ;

/* renamed from: android.support.v4.ˈ.י  reason: contains not printable characters */
/* compiled from: SparseArrayCompat */
public class C0354<E> implements Cloneable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final Object f1174 = new Object();

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f1175;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int[] f1176;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Object[] f1177;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f1178;

    public C0354() {
        this(10);
    }

    public C0354(int i) {
        this.f1175 = false;
        if (i == 0) {
            this.f1176 = C0333.f1128;
            this.f1177 = C0333.f1130;
        } else {
            int r3 = C0333.m1912(i);
            this.f1176 = new int[r3];
            this.f1177 = new Object[r3];
        }
        this.f1178 = 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0354<E> clone() {
        try {
            C0354<E> r1 = (C0354) super.clone();
            try {
                r1.f1176 = (int[]) this.f1176.clone();
                r1.f1177 = (Object[]) this.f1177.clone();
                return r1;
            } catch (CloneNotSupportedException unused) {
                return r1;
            }
        } catch (CloneNotSupportedException unused2) {
            return null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public E m1984(int i) {
        return m1985(i, null);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public E m1985(int i, E e) {
        int r4 = C0333.m1913(this.f1176, this.f1178, i);
        if (r4 >= 0) {
            E[] eArr = this.f1177;
            if (eArr[r4] != f1174) {
                return eArr[r4];
            }
        }
        return e;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1987(int i) {
        Object[] objArr;
        Object obj;
        int r4 = C0333.m1913(this.f1176, this.f1178, i);
        if (r4 >= 0 && (objArr = this.f1177)[r4] != (obj = f1174)) {
            objArr[r4] = obj;
            this.f1175 = true;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1990(int i) {
        m1987(i);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m1982() {
        int i = this.f1178;
        int[] iArr = this.f1176;
        Object[] objArr = this.f1177;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != f1174) {
                if (i3 != i2) {
                    iArr[i2] = iArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.f1175 = false;
        this.f1178 = i2;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1988(int i, E e) {
        int r0 = C0333.m1913(this.f1176, this.f1178, i);
        if (r0 >= 0) {
            this.f1177[r0] = e;
            return;
        }
        int i2 = r0 ^ -1;
        if (i2 < this.f1178) {
            Object[] objArr = this.f1177;
            if (objArr[i2] == f1174) {
                this.f1176[i2] = i;
                objArr[i2] = e;
                return;
            }
        }
        if (this.f1175 && this.f1178 >= this.f1176.length) {
            m1982();
            i2 = C0333.m1913(this.f1176, this.f1178, i) ^ -1;
        }
        int i3 = this.f1178;
        if (i3 >= this.f1176.length) {
            int r1 = C0333.m1912(i3 + 1);
            int[] iArr = new int[r1];
            Object[] objArr2 = new Object[r1];
            int[] iArr2 = this.f1176;
            System.arraycopy(iArr2, 0, iArr, 0, iArr2.length);
            Object[] objArr3 = this.f1177;
            System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.f1176 = iArr;
            this.f1177 = objArr2;
        }
        int i4 = this.f1178;
        if (i4 - i2 != 0) {
            int[] iArr3 = this.f1176;
            int i5 = i2 + 1;
            System.arraycopy(iArr3, i2, iArr3, i5, i4 - i2);
            Object[] objArr4 = this.f1177;
            System.arraycopy(objArr4, i2, objArr4, i5, this.f1178 - i2);
        }
        this.f1176[i2] = i;
        this.f1177[i2] = e;
        this.f1178++;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m1986() {
        if (this.f1175) {
            m1982();
        }
        return this.f1178;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m1992(int i) {
        if (this.f1175) {
            m1982();
        }
        return this.f1176[i];
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public E m1993(int i) {
        if (this.f1175) {
            m1982();
        }
        return this.f1177[i];
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public int m1994(int i) {
        if (this.f1175) {
            m1982();
        }
        return C0333.m1913(this.f1176, this.f1178, i);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1989() {
        int i = this.f1178;
        Object[] objArr = this.f1177;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.f1178 = 0;
        this.f1175 = false;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1991(int i, E e) {
        int i2 = this.f1178;
        if (i2 == 0 || i > this.f1176[i2 - 1]) {
            if (this.f1175 && this.f1178 >= this.f1176.length) {
                m1982();
            }
            int i3 = this.f1178;
            if (i3 >= this.f1176.length) {
                int r1 = C0333.m1912(i3 + 1);
                int[] iArr = new int[r1];
                Object[] objArr = new Object[r1];
                int[] iArr2 = this.f1176;
                System.arraycopy(iArr2, 0, iArr, 0, iArr2.length);
                Object[] objArr2 = this.f1177;
                System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
                this.f1176 = iArr;
                this.f1177 = objArr;
            }
            this.f1176[i3] = i;
            this.f1177[i3] = e;
            this.f1178 = i3 + 1;
            return;
        }
        m1988(i, e);
    }

    public String toString() {
        if (m1986() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.f1178 * 28);
        sb.append('{');
        for (int i = 0; i < this.f1178; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(m1992(i));
            sb.append('=');
            Object r2 = m1993(i);
            if (r2 != this) {
                sb.append(r2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}
