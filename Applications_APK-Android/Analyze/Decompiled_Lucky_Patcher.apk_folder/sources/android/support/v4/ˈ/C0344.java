package android.support.v4.ˈ;

import android.os.Build;
import java.util.Objects;

/* renamed from: android.support.v4.ˈ.ˊ  reason: contains not printable characters */
/* compiled from: ObjectsCompat */
public class C0344 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final C0346 f1161;

    static {
        if (Build.VERSION.SDK_INT >= 19) {
            f1161 = new C0345();
        } else {
            f1161 = new C0346();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m1958(Object obj, Object obj2) {
        return f1161.m1960(obj, obj2);
    }

    /* renamed from: android.support.v4.ˈ.ˊ$ʼ  reason: contains not printable characters */
    /* compiled from: ObjectsCompat */
    private static class C0346 {
        private C0346() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m1960(Object obj, Object obj2) {
            return obj == obj2 || (obj != null && obj.equals(obj2));
        }
    }

    /* renamed from: android.support.v4.ˈ.ˊ$ʻ  reason: contains not printable characters */
    /* compiled from: ObjectsCompat */
    private static class C0345 extends C0346 {
        private C0345() {
            super();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m1959(Object obj, Object obj2) {
            return Objects.equals(obj, obj2);
        }
    }
}
