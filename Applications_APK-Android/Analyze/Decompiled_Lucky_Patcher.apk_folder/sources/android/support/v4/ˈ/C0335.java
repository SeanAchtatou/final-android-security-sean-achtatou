package android.support.v4.ˈ;

import android.util.Log;
import java.io.Writer;

/* renamed from: android.support.v4.ˈ.ʿ  reason: contains not printable characters */
/* compiled from: LogWriter */
public class C0335 extends Writer {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f1131;

    /* renamed from: ʼ  reason: contains not printable characters */
    private StringBuilder f1132 = new StringBuilder(128);

    public C0335(String str) {
        this.f1131 = str;
    }

    public void close() {
        m1919();
    }

    public void flush() {
        m1919();
    }

    public void write(char[] cArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            char c = cArr[i + i3];
            if (c == 10) {
                m1919();
            } else {
                this.f1132.append(c);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m1919() {
        if (this.f1132.length() > 0) {
            Log.d(this.f1131, this.f1132.toString());
            StringBuilder sb = this.f1132;
            sb.delete(0, sb.length());
        }
    }
}
