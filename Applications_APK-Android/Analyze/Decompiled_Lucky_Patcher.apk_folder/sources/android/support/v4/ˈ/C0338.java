package android.support.v4.ˈ;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/* renamed from: android.support.v4.ˈ.ˉ  reason: contains not printable characters */
/* compiled from: MapCollections */
abstract class C0338<K, V> {

    /* renamed from: ʼ  reason: contains not printable characters */
    C0338<K, V>.ʼ f1146;

    /* renamed from: ʽ  reason: contains not printable characters */
    C0338<K, V>.ʽ f1147;

    /* renamed from: ʾ  reason: contains not printable characters */
    C0338<K, V>.ʿ f1148;

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract int m1942();

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract int m1943(Object obj);

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract Object m1944(int i, int i2);

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract V m1945(int i, V v);

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m1946(int i);

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m1947(K k, V v);

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract int m1949(Object obj);

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract Map<K, V> m1950();

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract void m1952();

    C0338() {
    }

    /* renamed from: android.support.v4.ˈ.ˉ$ʻ  reason: contains not printable characters */
    /* compiled from: MapCollections */
    final class C0339<T> implements Iterator<T> {

        /* renamed from: ʻ  reason: contains not printable characters */
        final int f1149;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f1150;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f1151;

        /* renamed from: ʾ  reason: contains not printable characters */
        boolean f1152 = false;

        C0339(int i) {
            this.f1149 = i;
            this.f1150 = C0338.this.m1942();
        }

        public boolean hasNext() {
            return this.f1151 < this.f1150;
        }

        public T next() {
            if (hasNext()) {
                T r0 = C0338.this.m1944(this.f1151, this.f1149);
                this.f1151++;
                this.f1152 = true;
                return r0;
            }
            throw new NoSuchElementException();
        }

        public void remove() {
            if (this.f1152) {
                this.f1151--;
                this.f1150--;
                this.f1152 = false;
                C0338.this.m1946(this.f1151);
                return;
            }
            throw new IllegalStateException();
        }
    }

    /* renamed from: android.support.v4.ˈ.ˉ$ʾ  reason: contains not printable characters */
    /* compiled from: MapCollections */
    final class C0342 implements Iterator<Map.Entry<K, V>>, Map.Entry<K, V> {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f1156;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f1157;

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean f1158 = false;

        C0342() {
            this.f1156 = C0338.this.m1942() - 1;
            this.f1157 = -1;
        }

        public boolean hasNext() {
            return this.f1157 < this.f1156;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Map.Entry<K, V> next() {
            if (hasNext()) {
                this.f1157++;
                this.f1158 = true;
                return this;
            }
            throw new NoSuchElementException();
        }

        public void remove() {
            if (this.f1158) {
                C0338.this.m1946(this.f1157);
                this.f1157--;
                this.f1156--;
                this.f1158 = false;
                return;
            }
            throw new IllegalStateException();
        }

        public K getKey() {
            if (this.f1158) {
                return C0338.this.m1944(this.f1157, 0);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public V getValue() {
            if (this.f1158) {
                return C0338.this.m1944(this.f1157, 1);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public V setValue(V v) {
            if (this.f1158) {
                return C0338.this.m1945(this.f1157, v);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public final boolean equals(Object obj) {
            if (!this.f1158) {
                throw new IllegalStateException("This container does not support retaining Map.Entry objects");
            } else if (!(obj instanceof Map.Entry)) {
                return false;
            } else {
                Map.Entry entry = (Map.Entry) obj;
                if (!C0333.m1915(entry.getKey(), C0338.this.m1944(this.f1157, 0)) || !C0333.m1915(entry.getValue(), C0338.this.m1944(this.f1157, 1))) {
                    return false;
                }
                return true;
            }
        }

        public final int hashCode() {
            int i;
            if (this.f1158) {
                int i2 = 0;
                Object r0 = C0338.this.m1944(this.f1157, 0);
                Object r1 = C0338.this.m1944(this.f1157, 1);
                if (r0 == null) {
                    i = 0;
                } else {
                    i = r0.hashCode();
                }
                if (r1 != null) {
                    i2 = r1.hashCode();
                }
                return i ^ i2;
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public final String toString() {
            return getKey() + "=" + getValue();
        }
    }

    /* renamed from: android.support.v4.ˈ.ˉ$ʼ  reason: contains not printable characters */
    /* compiled from: MapCollections */
    final class C0340 implements Set<Map.Entry<K, V>> {
        C0340() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean add(Map.Entry<K, V> entry) {
            throw new UnsupportedOperationException();
        }

        public boolean addAll(Collection<? extends Map.Entry<K, V>> collection) {
            int r0 = C0338.this.m1942();
            for (Map.Entry entry : collection) {
                C0338.this.m1947(entry.getKey(), entry.getValue());
            }
            return r0 != C0338.this.m1942();
        }

        public void clear() {
            C0338.this.m1952();
        }

        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            int r0 = C0338.this.m1943(entry.getKey());
            if (r0 < 0) {
                return false;
            }
            return C0333.m1915(C0338.this.m1944(r0, 1), entry.getValue());
        }

        public boolean containsAll(Collection<?> collection) {
            for (Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        public boolean isEmpty() {
            return C0338.this.m1942() == 0;
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            return new C0342();
        }

        public boolean remove(Object obj) {
            throw new UnsupportedOperationException();
        }

        public boolean removeAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        public boolean retainAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        public int size() {
            return C0338.this.m1942();
        }

        public Object[] toArray() {
            throw new UnsupportedOperationException();
        }

        public <T> T[] toArray(T[] tArr) {
            throw new UnsupportedOperationException();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.ˈ.ˉ.ʻ(java.util.Set, java.lang.Object):boolean
         arg types: [android.support.v4.ˈ.ˉ$ʼ, java.lang.Object]
         candidates:
          android.support.v4.ˈ.ˉ.ʻ(java.util.Map, java.util.Collection<?>):boolean
          android.support.v4.ˈ.ˉ.ʻ(int, int):java.lang.Object
          android.support.v4.ˈ.ˉ.ʻ(int, java.lang.Object):V
          android.support.v4.ˈ.ˉ.ʻ(java.lang.Object, java.lang.Object):void
          android.support.v4.ˈ.ˉ.ʻ(java.lang.Object[], int):T[]
          android.support.v4.ˈ.ˉ.ʻ(java.util.Set, java.lang.Object):boolean */
        public boolean equals(Object obj) {
            return C0338.m1939((Set) this, obj);
        }

        public int hashCode() {
            int i;
            int i2;
            int i3 = 0;
            for (int r0 = C0338.this.m1942() - 1; r0 >= 0; r0--) {
                Object r4 = C0338.this.m1944(r0, 0);
                Object r5 = C0338.this.m1944(r0, 1);
                if (r4 == null) {
                    i = 0;
                } else {
                    i = r4.hashCode();
                }
                if (r5 == null) {
                    i2 = 0;
                } else {
                    i2 = r5.hashCode();
                }
                i3 += i ^ i2;
            }
            return i3;
        }
    }

    /* renamed from: android.support.v4.ˈ.ˉ$ʽ  reason: contains not printable characters */
    /* compiled from: MapCollections */
    final class C0341 implements Set<K> {
        C0341() {
        }

        public boolean add(K k) {
            throw new UnsupportedOperationException();
        }

        public boolean addAll(Collection<? extends K> collection) {
            throw new UnsupportedOperationException();
        }

        public void clear() {
            C0338.this.m1952();
        }

        public boolean contains(Object obj) {
            return C0338.this.m1943(obj) >= 0;
        }

        public boolean containsAll(Collection<?> collection) {
            return C0338.m1938(C0338.this.m1950(), collection);
        }

        public boolean isEmpty() {
            return C0338.this.m1942() == 0;
        }

        public Iterator<K> iterator() {
            return new C0339(0);
        }

        public boolean remove(Object obj) {
            int r2 = C0338.this.m1943(obj);
            if (r2 < 0) {
                return false;
            }
            C0338.this.m1946(r2);
            return true;
        }

        public boolean removeAll(Collection<?> collection) {
            return C0338.m1940(C0338.this.m1950(), collection);
        }

        public boolean retainAll(Collection<?> collection) {
            return C0338.m1941(C0338.this.m1950(), collection);
        }

        public int size() {
            return C0338.this.m1942();
        }

        public Object[] toArray() {
            return C0338.this.m1951(0);
        }

        public <T> T[] toArray(T[] tArr) {
            return C0338.this.m1948(tArr, 0);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.ˈ.ˉ.ʻ(java.util.Set, java.lang.Object):boolean
         arg types: [android.support.v4.ˈ.ˉ$ʽ, java.lang.Object]
         candidates:
          android.support.v4.ˈ.ˉ.ʻ(java.util.Map, java.util.Collection<?>):boolean
          android.support.v4.ˈ.ˉ.ʻ(int, int):java.lang.Object
          android.support.v4.ˈ.ˉ.ʻ(int, java.lang.Object):V
          android.support.v4.ˈ.ˉ.ʻ(java.lang.Object, java.lang.Object):void
          android.support.v4.ˈ.ˉ.ʻ(java.lang.Object[], int):T[]
          android.support.v4.ˈ.ˉ.ʻ(java.util.Set, java.lang.Object):boolean */
        public boolean equals(Object obj) {
            return C0338.m1939((Set) this, obj);
        }

        public int hashCode() {
            int i;
            int i2 = 0;
            for (int r0 = C0338.this.m1942() - 1; r0 >= 0; r0--) {
                Object r3 = C0338.this.m1944(r0, 0);
                if (r3 == null) {
                    i = 0;
                } else {
                    i = r3.hashCode();
                }
                i2 += i;
            }
            return i2;
        }
    }

    /* renamed from: android.support.v4.ˈ.ˉ$ʿ  reason: contains not printable characters */
    /* compiled from: MapCollections */
    final class C0343 implements Collection<V> {
        C0343() {
        }

        public boolean add(V v) {
            throw new UnsupportedOperationException();
        }

        public boolean addAll(Collection<? extends V> collection) {
            throw new UnsupportedOperationException();
        }

        public void clear() {
            C0338.this.m1952();
        }

        public boolean contains(Object obj) {
            return C0338.this.m1949(obj) >= 0;
        }

        public boolean containsAll(Collection<?> collection) {
            for (Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        public boolean isEmpty() {
            return C0338.this.m1942() == 0;
        }

        public Iterator<V> iterator() {
            return new C0339(1);
        }

        public boolean remove(Object obj) {
            int r2 = C0338.this.m1949(obj);
            if (r2 < 0) {
                return false;
            }
            C0338.this.m1946(r2);
            return true;
        }

        public boolean removeAll(Collection<?> collection) {
            int r0 = C0338.this.m1942();
            int i = 0;
            boolean z = false;
            while (i < r0) {
                if (collection.contains(C0338.this.m1944(i, 1))) {
                    C0338.this.m1946(i);
                    i--;
                    r0--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        public boolean retainAll(Collection<?> collection) {
            int r0 = C0338.this.m1942();
            int i = 0;
            boolean z = false;
            while (i < r0) {
                if (!collection.contains(C0338.this.m1944(i, 1))) {
                    C0338.this.m1946(i);
                    i--;
                    r0--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        public int size() {
            return C0338.this.m1942();
        }

        public Object[] toArray() {
            return C0338.this.m1951(1);
        }

        public <T> T[] toArray(T[] tArr) {
            return C0338.this.m1948(tArr, 1);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <K, V> boolean m1938(Map<K, V> map, Collection<?> collection) {
        for (Object containsKey : collection) {
            if (!map.containsKey(containsKey)) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static <K, V> boolean m1940(Map<K, V> map, Collection<?> collection) {
        int size = map.size();
        for (Object remove : collection) {
            map.remove(remove);
        }
        return size != map.size();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static <K, V> boolean m1941(Map<K, V> map, Collection<?> collection) {
        int size = map.size();
        Iterator<K> it = map.keySet().iterator();
        while (it.hasNext()) {
            if (!collection.contains(it.next())) {
                it.remove();
            }
        }
        return size != map.size();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Object[] m1951(int i) {
        int r0 = m1942();
        Object[] objArr = new Object[r0];
        for (int i2 = 0; i2 < r0; i2++) {
            objArr[i2] = m1944(i2, i);
        }
        return objArr;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public <T> T[] m1948(T[] tArr, int i) {
        int r0 = m1942();
        if (tArr.length < r0) {
            tArr = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), r0);
        }
        for (int i2 = 0; i2 < r0; i2++) {
            tArr[i2] = m1944(i2, i);
        }
        if (tArr.length > r0) {
            tArr[r0] = null;
        }
        return tArr;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> boolean m1939(Set<T> set, Object obj) {
        if (set == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set2 = (Set) obj;
            try {
                if (set.size() != set2.size() || !set.containsAll(set2)) {
                    return false;
                }
                return true;
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public Set<Map.Entry<K, V>> m1953() {
        if (this.f1146 == null) {
            this.f1146 = new C0340();
        }
        return this.f1146;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public Set<K> m1954() {
        if (this.f1147 == null) {
            this.f1147 = new C0341();
        }
        return this.f1147;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public Collection<V> m1955() {
        if (this.f1148 == null) {
            this.f1148 = new C0343();
        }
        return this.f1148;
    }
}
