package android.support.v4.ˈ;

/* renamed from: android.support.v4.ˈ.ʽ  reason: contains not printable characters */
/* compiled from: ContainerHelpers */
class C0333 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final int[] f1128 = new int[0];

    /* renamed from: ʼ  reason: contains not printable characters */
    static final long[] f1129 = new long[0];

    /* renamed from: ʽ  reason: contains not printable characters */
    static final Object[] f1130 = new Object[0];

    /* renamed from: ʽ  reason: contains not printable characters */
    public static int m1917(int i) {
        for (int i2 = 4; i2 < 32; i2++) {
            int i3 = (1 << i2) - 12;
            if (i <= i3) {
                return i3;
            }
        }
        return i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m1912(int i) {
        return m1917(i * 4) / 4;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int m1916(int i) {
        return m1917(i * 8) / 8;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m1915(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static int m1913(int[] iArr, int i, int i2) {
        int i3 = i - 1;
        int i4 = 0;
        while (i4 <= i3) {
            int i5 = (i4 + i3) >>> 1;
            int i6 = iArr[i5];
            if (i6 < i2) {
                i4 = i5 + 1;
            } else if (i6 <= i2) {
                return i5;
            } else {
                i3 = i5 - 1;
            }
        }
        return i4 ^ -1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static int m1914(long[] jArr, int i, long j) {
        int i2 = i - 1;
        int i3 = 0;
        while (i3 <= i2) {
            int i4 = (i3 + i2) >>> 1;
            long j2 = jArr[i4];
            if (j2 < j) {
                i3 = i4 + 1;
            } else if (j2 <= j) {
                return i4;
            } else {
                i2 = i4 - 1;
            }
        }
        return i3 ^ -1;
    }
}
