package android.support.v4.ˈ;

import java.util.ConcurrentModificationException;
import java.util.Map;

/* renamed from: android.support.v4.ˈ.ˑ  reason: contains not printable characters */
/* compiled from: SimpleArrayMap */
public class C0353<K, V> {

    /* renamed from: ʼ  reason: contains not printable characters */
    static Object[] f1167;

    /* renamed from: ʽ  reason: contains not printable characters */
    static int f1168;

    /* renamed from: ʾ  reason: contains not printable characters */
    static Object[] f1169;

    /* renamed from: ʿ  reason: contains not printable characters */
    static int f1170;

    /* renamed from: ˆ  reason: contains not printable characters */
    int[] f1171;

    /* renamed from: ˈ  reason: contains not printable characters */
    Object[] f1172;

    /* renamed from: ˉ  reason: contains not printable characters */
    int f1173;

    /* renamed from: ʻ  reason: contains not printable characters */
    private static int m1970(int[] iArr, int i, int i2) {
        try {
            return C0333.m1913(iArr, i, i2);
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new ConcurrentModificationException();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m1975(Object obj, int i) {
        int i2 = this.f1173;
        if (i2 == 0) {
            return -1;
        }
        int r2 = m1970(this.f1171, i2, i);
        if (r2 < 0 || obj.equals(this.f1172[r2 << 1])) {
            return r2;
        }
        int i3 = r2 + 1;
        while (i3 < i2 && this.f1171[i3] == i) {
            if (obj.equals(this.f1172[i3 << 1])) {
                return i3;
            }
            i3++;
        }
        int i4 = r2 - 1;
        while (i4 >= 0 && this.f1171[i4] == i) {
            if (obj.equals(this.f1172[i4 << 1])) {
                return i4;
            }
            i4--;
        }
        return i3 ^ -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m1973() {
        int i = this.f1173;
        if (i == 0) {
            return -1;
        }
        int r2 = m1970(this.f1171, i, 0);
        if (r2 < 0 || this.f1172[r2 << 1] == null) {
            return r2;
        }
        int i2 = r2 + 1;
        while (i2 < i && this.f1171[i2] == 0) {
            if (this.f1172[i2 << 1] == null) {
                return i2;
            }
            i2++;
        }
        int i3 = r2 - 1;
        while (i3 >= 0 && this.f1171[i3] == 0) {
            if (this.f1172[i3 << 1] == null) {
                return i3;
            }
            i3--;
        }
        return i2 ^ -1;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m1972(int i) {
        if (i == 8) {
            synchronized (C0331.class) {
                if (f1169 != null) {
                    Object[] objArr = f1169;
                    this.f1172 = objArr;
                    f1169 = (Object[]) objArr[0];
                    this.f1171 = (int[]) objArr[1];
                    objArr[1] = null;
                    objArr[0] = null;
                    f1170--;
                    return;
                }
            }
        } else if (i == 4) {
            synchronized (C0331.class) {
                if (f1167 != null) {
                    Object[] objArr2 = f1167;
                    this.f1172 = objArr2;
                    f1167 = (Object[]) objArr2[0];
                    this.f1171 = (int[]) objArr2[1];
                    objArr2[1] = null;
                    objArr2[0] = null;
                    f1168--;
                    return;
                }
            }
        }
        this.f1171 = new int[i];
        this.f1172 = new Object[(i << 1)];
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m1971(int[] iArr, Object[] objArr, int i) {
        if (iArr.length == 8) {
            synchronized (C0331.class) {
                if (f1170 < 10) {
                    objArr[0] = f1169;
                    objArr[1] = iArr;
                    for (int i2 = (i << 1) - 1; i2 >= 2; i2--) {
                        objArr[i2] = null;
                    }
                    f1169 = objArr;
                    f1170++;
                }
            }
        } else if (iArr.length == 4) {
            synchronized (C0331.class) {
                if (f1168 < 10) {
                    objArr[0] = f1167;
                    objArr[1] = iArr;
                    for (int i3 = (i << 1) - 1; i3 >= 2; i3--) {
                        objArr[i3] = null;
                    }
                    f1167 = objArr;
                    f1168++;
                }
            }
        }
    }

    public C0353() {
        this.f1171 = C0333.f1128;
        this.f1172 = C0333.f1130;
        this.f1173 = 0;
    }

    public C0353(int i) {
        if (i == 0) {
            this.f1171 = C0333.f1128;
            this.f1172 = C0333.f1130;
        } else {
            m1972(i);
        }
        this.f1173 = 0;
    }

    public void clear() {
        int i = this.f1173;
        if (i > 0) {
            int[] iArr = this.f1171;
            Object[] objArr = this.f1172;
            this.f1171 = C0333.f1128;
            this.f1172 = C0333.f1130;
            this.f1173 = 0;
            m1971(iArr, objArr, i);
        }
        if (this.f1173 > 0) {
            throw new ConcurrentModificationException();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1977(int i) {
        int i2 = this.f1173;
        int[] iArr = this.f1171;
        if (iArr.length < i) {
            Object[] objArr = this.f1172;
            m1972(i);
            if (this.f1173 > 0) {
                System.arraycopy(iArr, 0, this.f1171, 0, i2);
                System.arraycopy(objArr, 0, this.f1172, 0, i2 << 1);
            }
            m1971(iArr, objArr, i2);
        }
        if (this.f1173 != i2) {
            throw new ConcurrentModificationException();
        }
    }

    public boolean containsKey(Object obj) {
        return m1974(obj) >= 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m1974(Object obj) {
        return obj == null ? m1973() : m1975(obj, obj.hashCode());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m1978(Object obj) {
        int i = this.f1173 * 2;
        Object[] objArr = this.f1172;
        if (obj == null) {
            for (int i2 = 1; i2 < i; i2 += 2) {
                if (objArr[i2] == null) {
                    return i2 >> 1;
                }
            }
            return -1;
        }
        for (int i3 = 1; i3 < i; i3 += 2) {
            if (obj.equals(objArr[i3])) {
                return i3 >> 1;
            }
        }
        return -1;
    }

    public boolean containsValue(Object obj) {
        return m1978(obj) >= 0;
    }

    public V get(Object obj) {
        int r2 = m1974(obj);
        if (r2 >= 0) {
            return this.f1172[(r2 << 1) + 1];
        }
        return null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public K m1979(int i) {
        return this.f1172[i << 1];
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public V m1980(int i) {
        return this.f1172[(i << 1) + 1];
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public V m1976(int i, V v) {
        int i2 = (i << 1) + 1;
        V[] vArr = this.f1172;
        V v2 = vArr[i2];
        vArr[i2] = v;
        return v2;
    }

    public boolean isEmpty() {
        return this.f1173 <= 0;
    }

    public V put(K k, V v) {
        int i;
        int i2;
        int i3 = this.f1173;
        if (k == null) {
            i2 = m1973();
            i = 0;
        } else {
            int hashCode = k.hashCode();
            i = hashCode;
            i2 = m1975(k, hashCode);
        }
        if (i2 >= 0) {
            int i4 = (i2 << 1) + 1;
            V[] vArr = this.f1172;
            V v2 = vArr[i4];
            vArr[i4] = v;
            return v2;
        }
        int i5 = i2 ^ -1;
        if (i3 >= this.f1171.length) {
            int i6 = 4;
            if (i3 >= 8) {
                i6 = (i3 >> 1) + i3;
            } else if (i3 >= 4) {
                i6 = 8;
            }
            int[] iArr = this.f1171;
            Object[] objArr = this.f1172;
            m1972(i6);
            if (i3 == this.f1173) {
                int[] iArr2 = this.f1171;
                if (iArr2.length > 0) {
                    System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
                    System.arraycopy(objArr, 0, this.f1172, 0, objArr.length);
                }
                m1971(iArr, objArr, i3);
            } else {
                throw new ConcurrentModificationException();
            }
        }
        if (i5 < i3) {
            int[] iArr3 = this.f1171;
            int i7 = i5 + 1;
            System.arraycopy(iArr3, i5, iArr3, i7, i3 - i5);
            Object[] objArr2 = this.f1172;
            System.arraycopy(objArr2, i5 << 1, objArr2, i7 << 1, (this.f1173 - i5) << 1);
        }
        int i8 = this.f1173;
        if (i3 == i8) {
            int[] iArr4 = this.f1171;
            if (i5 < iArr4.length) {
                iArr4[i5] = i;
                Object[] objArr3 = this.f1172;
                int i9 = i5 << 1;
                objArr3[i9] = k;
                objArr3[i9 + 1] = v;
                this.f1173 = i8 + 1;
                return null;
            }
        }
        throw new ConcurrentModificationException();
    }

    public V remove(Object obj) {
        int r1 = m1974(obj);
        if (r1 >= 0) {
            return m1981(r1);
        }
        return null;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public V m1981(int i) {
        int i2;
        V[] vArr = this.f1172;
        int i3 = i << 1;
        V v = vArr[i3 + 1];
        int i4 = this.f1173;
        if (i4 <= 1) {
            m1971(this.f1171, vArr, i4);
            this.f1171 = C0333.f1128;
            this.f1172 = C0333.f1130;
            i2 = 0;
        } else {
            i2 = i4 - 1;
            int[] iArr = this.f1171;
            int i5 = 8;
            if (iArr.length <= 8 || i4 >= iArr.length / 3) {
                if (i < i2) {
                    int[] iArr2 = this.f1171;
                    int i6 = i + 1;
                    int i7 = i2 - i;
                    System.arraycopy(iArr2, i6, iArr2, i, i7);
                    Object[] objArr = this.f1172;
                    System.arraycopy(objArr, i6 << 1, objArr, i3, i7 << 1);
                }
                Object[] objArr2 = this.f1172;
                int i8 = i2 << 1;
                objArr2[i8] = null;
                objArr2[i8 + 1] = null;
            } else {
                if (i4 > 8) {
                    i5 = i4 + (i4 >> 1);
                }
                int[] iArr3 = this.f1171;
                Object[] objArr3 = this.f1172;
                m1972(i5);
                if (i4 == this.f1173) {
                    if (i > 0) {
                        System.arraycopy(iArr3, 0, this.f1171, 0, i);
                        System.arraycopy(objArr3, 0, this.f1172, 0, i3);
                    }
                    if (i < i2) {
                        int i9 = i + 1;
                        int i10 = i2 - i;
                        System.arraycopy(iArr3, i9, this.f1171, i, i10);
                        System.arraycopy(objArr3, i9 << 1, this.f1172, i3, i10 << 1);
                    }
                } else {
                    throw new ConcurrentModificationException();
                }
            }
        }
        if (i4 == this.f1173) {
            this.f1173 = i2;
            return v;
        }
        throw new ConcurrentModificationException();
    }

    public int size() {
        return this.f1173;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof C0353) {
            C0353 r7 = (C0353) obj;
            if (size() != r7.size()) {
                return false;
            }
            int i = 0;
            while (i < this.f1173) {
                try {
                    Object r3 = m1979(i);
                    Object r4 = m1980(i);
                    Object obj2 = r7.get(r3);
                    if (r4 == null) {
                        if (obj2 != null || !r7.containsKey(r3)) {
                            return false;
                        }
                    } else if (!r4.equals(obj2)) {
                        return false;
                    }
                    i++;
                } catch (ClassCastException | NullPointerException unused) {
                    return false;
                }
            }
            return true;
        }
        if (obj instanceof Map) {
            Map map = (Map) obj;
            if (size() != map.size()) {
                return false;
            }
            int i2 = 0;
            while (i2 < this.f1173) {
                try {
                    Object r32 = m1979(i2);
                    Object r42 = m1980(i2);
                    Object obj3 = map.get(r32);
                    if (r42 == null) {
                        if (obj3 != null || !map.containsKey(r32)) {
                            return false;
                        }
                    } else if (!r42.equals(obj3)) {
                        return false;
                    }
                    i2++;
                } catch (ClassCastException | NullPointerException unused2) {
                }
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        int[] iArr = this.f1171;
        Object[] objArr = this.f1172;
        int i = this.f1173;
        int i2 = 0;
        int i3 = 0;
        int i4 = 1;
        while (i2 < i) {
            Object obj = objArr[i4];
            i3 += (obj == null ? 0 : obj.hashCode()) ^ iArr[i2];
            i2++;
            i4 += 2;
        }
        return i3;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.f1173 * 28);
        sb.append('{');
        for (int i = 0; i < this.f1173; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            Object r2 = m1979(i);
            if (r2 != this) {
                sb.append(r2);
            } else {
                sb.append("(this Map)");
            }
            sb.append('=');
            Object r22 = m1980(i);
            if (r22 != this) {
                sb.append(r22);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}
