package android.support.v4.ˈ;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: android.support.v4.ˈ.ʼ  reason: contains not printable characters */
/* compiled from: ArraySet */
public final class C0332<E> implements Collection<E>, Set<E> {

    /* renamed from: ʻ  reason: contains not printable characters */
    static Object[] f1116;

    /* renamed from: ʼ  reason: contains not printable characters */
    static int f1117;

    /* renamed from: ʽ  reason: contains not printable characters */
    static Object[] f1118;

    /* renamed from: ʾ  reason: contains not printable characters */
    static int f1119;

    /* renamed from: ˋ  reason: contains not printable characters */
    private static final int[] f1120 = new int[0];

    /* renamed from: ˎ  reason: contains not printable characters */
    private static final Object[] f1121 = new Object[0];

    /* renamed from: ʿ  reason: contains not printable characters */
    final boolean f1122;

    /* renamed from: ˆ  reason: contains not printable characters */
    int[] f1123;

    /* renamed from: ˈ  reason: contains not printable characters */
    Object[] f1124;

    /* renamed from: ˉ  reason: contains not printable characters */
    int f1125;

    /* renamed from: ˊ  reason: contains not printable characters */
    C0338<E, E> f1126;

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m1895(Object obj, int i) {
        int i2 = this.f1125;
        if (i2 == 0) {
            return -1;
        }
        int r2 = C0333.m1913(this.f1123, i2, i);
        if (r2 < 0 || obj.equals(this.f1124[r2])) {
            return r2;
        }
        int i3 = r2 + 1;
        while (i3 < i2 && this.f1123[i3] == i) {
            if (obj.equals(this.f1124[i3])) {
                return i3;
            }
            i3++;
        }
        int i4 = r2 - 1;
        while (i4 >= 0 && this.f1123[i4] == i) {
            if (obj.equals(this.f1124[i4])) {
                return i4;
            }
            i4--;
        }
        return i3 ^ -1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m1894() {
        int i = this.f1125;
        if (i == 0) {
            return -1;
        }
        int r2 = C0333.m1913(this.f1123, i, 0);
        if (r2 < 0 || this.f1124[r2] == null) {
            return r2;
        }
        int i2 = r2 + 1;
        while (i2 < i && this.f1123[i2] == 0) {
            if (this.f1124[i2] == null) {
                return i2;
            }
            i2++;
        }
        int i3 = r2 - 1;
        while (i3 >= 0 && this.f1123[i3] == 0) {
            if (this.f1124[i3] == null) {
                return i3;
            }
            i3--;
        }
        return i2 ^ -1;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m1898(int i) {
        if (i == 8) {
            synchronized (C0332.class) {
                if (f1118 != null) {
                    Object[] objArr = f1118;
                    this.f1124 = objArr;
                    f1118 = (Object[]) objArr[0];
                    this.f1123 = (int[]) objArr[1];
                    objArr[1] = null;
                    objArr[0] = null;
                    f1119--;
                    return;
                }
            }
        } else if (i == 4) {
            synchronized (C0332.class) {
                if (f1116 != null) {
                    Object[] objArr2 = f1116;
                    this.f1124 = objArr2;
                    f1116 = (Object[]) objArr2[0];
                    this.f1123 = (int[]) objArr2[1];
                    objArr2[1] = null;
                    objArr2[0] = null;
                    f1117--;
                    return;
                }
            }
        }
        this.f1123 = new int[i];
        this.f1124 = new Object[i];
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m1896(int[] iArr, Object[] objArr, int i) {
        if (iArr.length == 8) {
            synchronized (C0332.class) {
                if (f1119 < 10) {
                    objArr[0] = f1118;
                    objArr[1] = iArr;
                    for (int i2 = i - 1; i2 >= 2; i2--) {
                        objArr[i2] = null;
                    }
                    f1118 = objArr;
                    f1119++;
                }
            }
        } else if (iArr.length == 4) {
            synchronized (C0332.class) {
                if (f1117 < 10) {
                    objArr[0] = f1116;
                    objArr[1] = iArr;
                    for (int i3 = i - 1; i3 >= 2; i3--) {
                        objArr[i3] = null;
                    }
                    f1116 = objArr;
                    f1117++;
                }
            }
        }
    }

    public C0332() {
        this(0, false);
    }

    public C0332(int i, boolean z) {
        this.f1122 = z;
        if (i == 0) {
            this.f1123 = f1120;
            this.f1124 = f1121;
        } else {
            m1898(i);
        }
        this.f1125 = 0;
    }

    public void clear() {
        int i = this.f1125;
        if (i != 0) {
            m1896(this.f1123, this.f1124, i);
            this.f1123 = f1120;
            this.f1124 = f1121;
            this.f1125 = 0;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1900(int i) {
        int[] iArr = this.f1123;
        if (iArr.length < i) {
            Object[] objArr = this.f1124;
            m1898(i);
            int i2 = this.f1125;
            if (i2 > 0) {
                System.arraycopy(iArr, 0, this.f1123, 0, i2);
                System.arraycopy(objArr, 0, this.f1124, 0, this.f1125);
            }
            m1896(iArr, objArr, this.f1125);
        }
    }

    public boolean contains(Object obj) {
        return m1899(obj) >= 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m1899(Object obj) {
        if (obj == null) {
            return m1894();
        }
        return m1895(obj, this.f1122 ? System.identityHashCode(obj) : obj.hashCode());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public E m1901(int i) {
        return this.f1124[i];
    }

    public boolean isEmpty() {
        return this.f1125 <= 0;
    }

    public boolean add(E e) {
        int i;
        int i2;
        if (e == null) {
            i2 = m1894();
            i = 0;
        } else {
            int identityHashCode = this.f1122 ? System.identityHashCode(e) : e.hashCode();
            i = identityHashCode;
            i2 = m1895(e, identityHashCode);
        }
        if (i2 >= 0) {
            return false;
        }
        int i3 = i2 ^ -1;
        int i4 = this.f1125;
        if (i4 >= this.f1123.length) {
            int i5 = 4;
            if (i4 >= 8) {
                i5 = (i4 >> 1) + i4;
            } else if (i4 >= 4) {
                i5 = 8;
            }
            int[] iArr = this.f1123;
            Object[] objArr = this.f1124;
            m1898(i5);
            int[] iArr2 = this.f1123;
            if (iArr2.length > 0) {
                System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
                System.arraycopy(objArr, 0, this.f1124, 0, objArr.length);
            }
            m1896(iArr, objArr, this.f1125);
        }
        int i6 = this.f1125;
        if (i3 < i6) {
            int[] iArr3 = this.f1123;
            int i7 = i3 + 1;
            System.arraycopy(iArr3, i3, iArr3, i7, i6 - i3);
            Object[] objArr2 = this.f1124;
            System.arraycopy(objArr2, i3, objArr2, i7, this.f1125 - i3);
        }
        this.f1123[i3] = i;
        this.f1124[i3] = e;
        this.f1125++;
        return true;
    }

    public boolean remove(Object obj) {
        int r1 = m1899(obj);
        if (r1 < 0) {
            return false;
        }
        m1902(r1);
        return true;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public E m1902(int i) {
        E[] eArr = this.f1124;
        E e = eArr[i];
        int i2 = this.f1125;
        if (i2 <= 1) {
            m1896(this.f1123, eArr, i2);
            this.f1123 = f1120;
            this.f1124 = f1121;
            this.f1125 = 0;
        } else {
            int[] iArr = this.f1123;
            int i3 = 8;
            if (iArr.length <= 8 || i2 >= iArr.length / 3) {
                this.f1125--;
                int i4 = this.f1125;
                if (i < i4) {
                    int[] iArr2 = this.f1123;
                    int i5 = i + 1;
                    System.arraycopy(iArr2, i5, iArr2, i, i4 - i);
                    Object[] objArr = this.f1124;
                    System.arraycopy(objArr, i5, objArr, i, this.f1125 - i);
                }
                this.f1124[this.f1125] = null;
            } else {
                if (i2 > 8) {
                    i3 = i2 + (i2 >> 1);
                }
                int[] iArr3 = this.f1123;
                Object[] objArr2 = this.f1124;
                m1898(i3);
                this.f1125--;
                if (i > 0) {
                    System.arraycopy(iArr3, 0, this.f1123, 0, i);
                    System.arraycopy(objArr2, 0, this.f1124, 0, i);
                }
                int i6 = this.f1125;
                if (i < i6) {
                    int i7 = i + 1;
                    System.arraycopy(iArr3, i7, this.f1123, i, i6 - i);
                    System.arraycopy(objArr2, i7, this.f1124, i, this.f1125 - i);
                }
            }
        }
        return e;
    }

    public int size() {
        return this.f1125;
    }

    public Object[] toArray() {
        int i = this.f1125;
        Object[] objArr = new Object[i];
        System.arraycopy(this.f1124, 0, objArr, 0, i);
        return objArr;
    }

    public <T> T[] toArray(T[] tArr) {
        if (tArr.length < this.f1125) {
            tArr = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), this.f1125);
        }
        System.arraycopy(this.f1124, 0, tArr, 0, this.f1125);
        int length = tArr.length;
        int i = this.f1125;
        if (length > i) {
            tArr[i] = null;
        }
        return tArr;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set = (Set) obj;
            if (size() != set.size()) {
                return false;
            }
            int i = 0;
            while (i < this.f1125) {
                try {
                    if (!set.contains(m1901(i))) {
                        return false;
                    }
                    i++;
                } catch (ClassCastException | NullPointerException unused) {
                }
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        int[] iArr = this.f1123;
        int i = this.f1125;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            i2 += iArr[i3];
        }
        return i2;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.f1125 * 14);
        sb.append('{');
        for (int i = 0; i < this.f1125; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            Object r2 = m1901(i);
            if (r2 != this) {
                sb.append(r2);
            } else {
                sb.append("(this Set)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private C0338<E, E> m1897() {
        if (this.f1126 == null) {
            this.f1126 = new C0338<E, E>() {
                /* access modifiers changed from: protected */
                /* renamed from: ʻ  reason: contains not printable characters */
                public int m1903() {
                    return C0332.this.f1125;
                }

                /* access modifiers changed from: protected */
                /* renamed from: ʻ  reason: contains not printable characters */
                public Object m1905(int i, int i2) {
                    return C0332.this.f1124[i];
                }

                /* access modifiers changed from: protected */
                /* renamed from: ʻ  reason: contains not printable characters */
                public int m1904(Object obj) {
                    return C0332.this.m1899(obj);
                }

                /* access modifiers changed from: protected */
                /* renamed from: ʼ  reason: contains not printable characters */
                public int m1909(Object obj) {
                    return C0332.this.m1899(obj);
                }

                /* access modifiers changed from: protected */
                /* renamed from: ʼ  reason: contains not printable characters */
                public Map<E, E> m1910() {
                    throw new UnsupportedOperationException("not a map");
                }

                /* access modifiers changed from: protected */
                /* renamed from: ʻ  reason: contains not printable characters */
                public void m1908(E e, E e2) {
                    C0332.this.add(e);
                }

                /* access modifiers changed from: protected */
                /* renamed from: ʻ  reason: contains not printable characters */
                public E m1906(int i, E e) {
                    throw new UnsupportedOperationException("not a map");
                }

                /* access modifiers changed from: protected */
                /* renamed from: ʻ  reason: contains not printable characters */
                public void m1907(int i) {
                    C0332.this.m1902(i);
                }

                /* access modifiers changed from: protected */
                /* renamed from: ʽ  reason: contains not printable characters */
                public void m1911() {
                    C0332.this.clear();
                }
            };
        }
        return this.f1126;
    }

    public Iterator<E> iterator() {
        return m1897().m1954().iterator();
    }

    public boolean containsAll(Collection<?> collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public boolean addAll(Collection<? extends E> collection) {
        m1900(this.f1125 + collection.size());
        boolean z = false;
        for (Object add : collection) {
            z |= add(add);
        }
        return z;
    }

    public boolean removeAll(Collection<?> collection) {
        boolean z = false;
        for (Object remove : collection) {
            z |= remove(remove);
        }
        return z;
    }

    public boolean retainAll(Collection<?> collection) {
        boolean z = false;
        for (int i = this.f1125 - 1; i >= 0; i--) {
            if (!collection.contains(this.f1124[i])) {
                m1902(i);
                z = true;
            }
        }
        return z;
    }
}
