package android.support.v4.ˈ;

/* renamed from: android.support.v4.ˈ.ʾ  reason: contains not printable characters */
/* compiled from: DebugUtils */
public class C0334 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1918(Object obj, StringBuilder sb) {
        int lastIndexOf;
        if (obj == null) {
            sb.append("null");
            return;
        }
        String simpleName = obj.getClass().getSimpleName();
        if ((simpleName == null || simpleName.length() <= 0) && (lastIndexOf = (simpleName = obj.getClass().getName()).lastIndexOf(46)) > 0) {
            simpleName = simpleName.substring(lastIndexOf + 1);
        }
        sb.append(simpleName);
        sb.append('{');
        sb.append(Integer.toHexString(System.identityHashCode(obj)));
    }
}
