package android.support.v4.ˈ;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/* renamed from: android.support.v4.ˈ.ʻ  reason: contains not printable characters */
/* compiled from: ArrayMap */
public class C0331<K, V> extends C0353<K, V> implements Map<K, V> {

    /* renamed from: ʻ  reason: contains not printable characters */
    C0338<K, V> f1114;

    public C0331() {
    }

    public C0331(int i) {
        super(i);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private C0338<K, V> m1883() {
        if (this.f1114 == null) {
            this.f1114 = new C0338<K, V>() {
                /* access modifiers changed from: protected */
                /* renamed from: ʻ  reason: contains not printable characters */
                public int m1885() {
                    return C0331.this.f1173;
                }

                /* access modifiers changed from: protected */
                /* renamed from: ʻ  reason: contains not printable characters */
                public Object m1887(int i, int i2) {
                    return C0331.this.f1172[(i << 1) + i2];
                }

                /* access modifiers changed from: protected */
                /* renamed from: ʻ  reason: contains not printable characters */
                public int m1886(Object obj) {
                    return C0331.this.m1974(obj);
                }

                /* access modifiers changed from: protected */
                /* renamed from: ʼ  reason: contains not printable characters */
                public int m1891(Object obj) {
                    return C0331.this.m1978(obj);
                }

                /* access modifiers changed from: protected */
                /* renamed from: ʼ  reason: contains not printable characters */
                public Map<K, V> m1892() {
                    return C0331.this;
                }

                /* access modifiers changed from: protected */
                /* renamed from: ʻ  reason: contains not printable characters */
                public void m1890(K k, V v) {
                    C0331.this.put(k, v);
                }

                /* access modifiers changed from: protected */
                /* renamed from: ʻ  reason: contains not printable characters */
                public V m1888(int i, V v) {
                    return C0331.this.m1976(i, v);
                }

                /* access modifiers changed from: protected */
                /* renamed from: ʻ  reason: contains not printable characters */
                public void m1889(int i) {
                    C0331.this.m1981(i);
                }

                /* access modifiers changed from: protected */
                /* renamed from: ʽ  reason: contains not printable characters */
                public void m1893() {
                    C0331.this.clear();
                }
            };
        }
        return this.f1114;
    }

    public void putAll(Map<? extends K, ? extends V> map) {
        m1977(this.f1173 + map.size());
        for (Map.Entry next : map.entrySet()) {
            put(next.getKey(), next.getValue());
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1884(Collection<?> collection) {
        return C0338.m1941(this, collection);
    }

    public Set<Map.Entry<K, V>> entrySet() {
        return m1883().m1953();
    }

    public Set<K> keySet() {
        return m1883().m1954();
    }

    public Collection<V> values() {
        return m1883().m1955();
    }
}
