package android.support.v4.ˈ;

/* renamed from: android.support.v4.ˈ.ˆ  reason: contains not printable characters */
/* compiled from: LongSparseArray */
public class C0336<E> implements Cloneable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final Object f1133 = new Object();

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f1134;

    /* renamed from: ʽ  reason: contains not printable characters */
    private long[] f1135;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Object[] f1136;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f1137;

    public C0336() {
        this(10);
    }

    public C0336(int i) {
        this.f1134 = false;
        if (i == 0) {
            this.f1135 = C0333.f1129;
            this.f1136 = C0333.f1130;
        } else {
            int r3 = C0333.m1916(i);
            this.f1135 = new long[r3];
            this.f1136 = new Object[r3];
        }
        this.f1137 = 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0336<E> clone() {
        try {
            C0336<E> r1 = (C0336) super.clone();
            try {
                r1.f1135 = (long[]) this.f1135.clone();
                r1.f1136 = (Object[]) this.f1136.clone();
                return r1;
            } catch (CloneNotSupportedException unused) {
                return r1;
            }
        } catch (CloneNotSupportedException unused2) {
            return null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public E m1922(long j) {
        return m1923(j, null);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public E m1923(long j, E e) {
        int r3 = C0333.m1914(this.f1135, this.f1137, j);
        if (r3 >= 0) {
            E[] eArr = this.f1136;
            if (eArr[r3] != f1133) {
                return eArr[r3];
            }
        }
        return e;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1927(long j) {
        Object[] objArr;
        Object obj;
        int r3 = C0333.m1914(this.f1135, this.f1137, j);
        if (r3 >= 0 && (objArr = this.f1136)[r3] != (obj = f1133)) {
            objArr[r3] = obj;
            this.f1134 = true;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1924(int i) {
        Object[] objArr = this.f1136;
        Object obj = objArr[i];
        Object obj2 = f1133;
        if (obj != obj2) {
            objArr[i] = obj2;
            this.f1134 = true;
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m1920() {
        int i = this.f1137;
        long[] jArr = this.f1135;
        Object[] objArr = this.f1136;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != f1133) {
                if (i3 != i2) {
                    jArr[i2] = jArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.f1134 = false;
        this.f1137 = i2;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1928(long j, E e) {
        int r0 = C0333.m1914(this.f1135, this.f1137, j);
        if (r0 >= 0) {
            this.f1136[r0] = e;
            return;
        }
        int i = r0 ^ -1;
        if (i < this.f1137) {
            Object[] objArr = this.f1136;
            if (objArr[i] == f1133) {
                this.f1135[i] = j;
                objArr[i] = e;
                return;
            }
        }
        if (this.f1134 && this.f1137 >= this.f1135.length) {
            m1920();
            i = C0333.m1914(this.f1135, this.f1137, j) ^ -1;
        }
        int i2 = this.f1137;
        if (i2 >= this.f1135.length) {
            int r1 = C0333.m1916(i2 + 1);
            long[] jArr = new long[r1];
            Object[] objArr2 = new Object[r1];
            long[] jArr2 = this.f1135;
            System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
            Object[] objArr3 = this.f1136;
            System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.f1135 = jArr;
            this.f1136 = objArr2;
        }
        int i3 = this.f1137;
        if (i3 - i != 0) {
            long[] jArr3 = this.f1135;
            int i4 = i + 1;
            System.arraycopy(jArr3, i, jArr3, i4, i3 - i);
            Object[] objArr4 = this.f1136;
            System.arraycopy(objArr4, i, objArr4, i4, this.f1137 - i);
        }
        this.f1135[i] = j;
        this.f1136[i] = e;
        this.f1137++;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m1925() {
        if (this.f1134) {
            m1920();
        }
        return this.f1137;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public long m1926(int i) {
        if (this.f1134) {
            m1920();
        }
        return this.f1135[i];
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public E m1929(int i) {
        if (this.f1134) {
            m1920();
        }
        return this.f1136[i];
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1930() {
        int i = this.f1137;
        Object[] objArr = this.f1136;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.f1137 = 0;
        this.f1134 = false;
    }

    public String toString() {
        if (m1925() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.f1137 * 28);
        sb.append('{');
        for (int i = 0; i < this.f1137; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(m1926(i));
            sb.append('=');
            Object r2 = m1929(i);
            if (r2 != this) {
                sb.append(r2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}
