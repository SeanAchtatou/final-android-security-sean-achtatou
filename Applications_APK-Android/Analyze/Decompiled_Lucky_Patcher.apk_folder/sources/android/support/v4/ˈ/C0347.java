package android.support.v4.ˈ;

/* renamed from: android.support.v4.ˈ.ˋ  reason: contains not printable characters */
/* compiled from: Pair */
public class C0347<F, S> {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final F f1162;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final S f1163;

    public boolean equals(Object obj) {
        if (!(obj instanceof C0347)) {
            return false;
        }
        C0347 r4 = (C0347) obj;
        if (!m1961(r4.f1162, this.f1162) || !m1961(r4.f1163, this.f1163)) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean m1961(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public int hashCode() {
        F f = this.f1162;
        int i = 0;
        int hashCode = f == null ? 0 : f.hashCode();
        S s = this.f1163;
        if (s != null) {
            i = s.hashCode();
        }
        return hashCode ^ i;
    }

    public String toString() {
        return "Pair{" + String.valueOf(this.f1162) + " " + String.valueOf(this.f1163) + "}";
    }
}
