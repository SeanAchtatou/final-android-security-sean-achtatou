package android.support.v4.ˈ;

/* renamed from: android.support.v4.ˈ.ˎ  reason: contains not printable characters */
/* compiled from: Pools */
public final class C0348 {

    /* renamed from: android.support.v4.ˈ.ˎ$ʻ  reason: contains not printable characters */
    /* compiled from: Pools */
    public interface C0349<T> {
        /* renamed from: ʻ  reason: contains not printable characters */
        T m1962();

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m1963(T t);
    }

    /* renamed from: android.support.v4.ˈ.ˎ$ʼ  reason: contains not printable characters */
    /* compiled from: Pools */
    public static class C0350<T> implements C0349<T> {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final Object[] f1164;

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f1165;

        public C0350(int i) {
            if (i > 0) {
                this.f1164 = new Object[i];
                return;
            }
            throw new IllegalArgumentException("The max pool size must be > 0");
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public T m1965() {
            int i = this.f1165;
            if (i <= 0) {
                return null;
            }
            int i2 = i - 1;
            T[] tArr = this.f1164;
            T t = tArr[i2];
            tArr[i2] = null;
            this.f1165 = i - 1;
            return t;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m1966(T t) {
            if (!m1964(t)) {
                int i = this.f1165;
                Object[] objArr = this.f1164;
                if (i >= objArr.length) {
                    return false;
                }
                objArr[i] = t;
                this.f1165 = i + 1;
                return true;
            }
            throw new IllegalStateException("Already in the pool!");
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean m1964(T t) {
            for (int i = 0; i < this.f1165; i++) {
                if (this.f1164[i] == t) {
                    return true;
                }
            }
            return false;
        }
    }

    /* renamed from: android.support.v4.ˈ.ˎ$ʽ  reason: contains not printable characters */
    /* compiled from: Pools */
    public static class C0351<T> extends C0350<T> {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final Object f1166 = new Object();

        public C0351(int i) {
            super(i);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public T m1967() {
            T r1;
            synchronized (this.f1166) {
                r1 = super.m1965();
            }
            return r1;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m1968(T t) {
            boolean r2;
            synchronized (this.f1166) {
                r2 = super.m1966(t);
            }
            return r2;
        }
    }
}
