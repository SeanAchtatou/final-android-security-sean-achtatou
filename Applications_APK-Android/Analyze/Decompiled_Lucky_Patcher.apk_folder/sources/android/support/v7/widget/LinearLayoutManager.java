package android.support.v7.widget;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.C0690;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import java.util.List;

public class LinearLayoutManager extends C0690.C0701 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private C0556 f2026;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f2027;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f2028;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f2029;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f2030;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f2031;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final C0555 f2032;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f2033;

    /* renamed from: ˊ  reason: contains not printable characters */
    int f2034;

    /* renamed from: ˋ  reason: contains not printable characters */
    C0688 f2035;

    /* renamed from: ˎ  reason: contains not printable characters */
    boolean f2036;

    /* renamed from: ˏ  reason: contains not printable characters */
    int f2037;

    /* renamed from: ˑ  reason: contains not printable characters */
    int f2038;

    /* renamed from: י  reason: contains not printable characters */
    C0557 f2039;

    /* renamed from: ـ  reason: contains not printable characters */
    final C0554 f2040;

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3330(C0690.C0711 r1, C0690.C0717 r2, C0554 r3, int i) {
    }

    public LinearLayoutManager(Context context) {
        this(context, 1, false);
    }

    public LinearLayoutManager(Context context, int i, boolean z) {
        this.f2028 = false;
        this.f2036 = false;
        this.f2029 = false;
        this.f2030 = true;
        this.f2037 = -1;
        this.f2038 = Integer.MIN_VALUE;
        this.f2039 = null;
        this.f2040 = new C0554();
        this.f2032 = new C0555();
        this.f2033 = 2;
        m3341(i);
        m3342(z);
        m4616(true);
    }

    public LinearLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        this.f2028 = false;
        this.f2036 = false;
        this.f2029 = false;
        this.f2030 = true;
        this.f2037 = -1;
        this.f2038 = Integer.MIN_VALUE;
        this.f2039 = null;
        this.f2040 = new C0554();
        this.f2032 = new C0555();
        this.f2033 = 2;
        C0690.C0701.C0703 r3 = m4536(context, attributeSet, i, i2);
        m3341(r3.f2850);
        m3342(r3.f2852);
        m3337(r3.f2853);
        m4616(true);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0690.C0704 m3323() {
        return new C0690.C0704(-2, -2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3334(C0690 r1, C0690.C0711 r2) {
        super.m4568(r1, r2);
        if (this.f2031) {
            m4610(r2);
            r2.m4715();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3335(AccessibilityEvent accessibilityEvent) {
        super.m4578(accessibilityEvent);
        if (m4667() > 0) {
            accessibilityEvent.setFromIndex(m3364());
            accessibilityEvent.setToIndex(m3365());
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Parcelable m3346() {
        C0557 r0 = this.f2039;
        if (r0 != null) {
            return new C0557(r0);
        }
        C0557 r02 = new C0557();
        if (m4667() > 0) {
            m3360();
            boolean z = this.f2027 ^ this.f2036;
            r02.f2064 = z;
            if (z) {
                View r1 = m3316();
                r02.f2063 = this.f2035.m4296() - this.f2035.m4293(r1);
                r02.f2062 = m4620(r1);
            } else {
                View r12 = m3309();
                r02.f2062 = m4620(r12);
                r02.f2063 = this.f2035.m4289(r12) - this.f2035.m4294();
            }
        } else {
            r02.m3379();
        }
        return r02;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3329(Parcelable parcelable) {
        if (parcelable instanceof C0557) {
            this.f2039 = (C0557) parcelable;
            m4656();
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m3351() {
        return this.f2034 == 0;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m3354() {
        return this.f2034 == 1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3337(boolean z) {
        m3336((String) null);
        if (this.f2029 != z) {
            this.f2029 = z;
            m4656();
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public int m3355() {
        return this.f2034;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3341(int i) {
        if (i == 0 || i == 1) {
            m3336((String) null);
            if (i != this.f2034) {
                this.f2034 = i;
                this.f2035 = null;
                m4656();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("invalid orientation:" + i);
    }

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private void m3311() {
        if (this.f2034 == 1 || !m3358()) {
            this.f2036 = this.f2028;
        } else {
            this.f2036 = !this.f2028;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3342(boolean z) {
        m3336((String) null);
        if (z != this.f2028) {
            this.f2028 = z;
            m4656();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public View m3347(int i) {
        int r0 = m4667();
        if (r0 == 0) {
            return null;
        }
        int r1 = i - m4620(m4645(0));
        if (r1 >= 0 && r1 < r0) {
            View r02 = m4645(r1);
            if (m4620(r02) == i) {
                return r02;
            }
        }
        return super.m4608(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m3339(C0690.C0717 r1) {
        if (r1.m4774()) {
            return this.f2035.m4300();
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
     arg types: [android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, android.support.v7.widget.ﹳﹳ$ᵔ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.LinearLayoutManager$ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.ﹳﹳ$ˉ$ʼ
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, java.lang.Object):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.view.View):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int, android.support.v7.widget.ﹳﹳ$ˊ):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
     arg types: [int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, android.support.v7.widget.ﹳﹳ$ᵔ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.LinearLayoutManager$ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.ﹳﹳ$ˉ$ʼ
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, java.lang.Object):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.view.View):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int, android.support.v7.widget.ﹳﹳ$ˊ):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʼ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
     arg types: [int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʼ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean):int[]
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(android.view.View, int, int, android.support.v7.widget.ﹳﹳ$ˊ):boolean
      android.support.v7.widget.LinearLayoutManager.ʼ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3348(C0690.C0711 r9, C0690.C0717 r10) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        View r5;
        int i8;
        int i9;
        int i10 = -1;
        if (!(this.f2039 == null && this.f2037 == -1) && r10.m4775() == 0) {
            m4610(r9);
            return;
        }
        C0557 r0 = this.f2039;
        if (r0 != null && r0.m3378()) {
            this.f2037 = this.f2039.f2062;
        }
        m3360();
        this.f2026.f2050 = false;
        m3311();
        View r02 = m4617();
        if (!this.f2040.f2044 || this.f2037 != -1 || this.f2039 != null) {
            this.f2040.m3366();
            C0554 r03 = this.f2040;
            r03.f2043 = this.f2036 ^ this.f2029;
            m3298(r9, r10, r03);
            this.f2040.f2044 = true;
        } else if (r02 != null && (this.f2035.m4289(r02) >= this.f2035.m4296() || this.f2035.m4293(r02) <= this.f2035.m4294())) {
            this.f2040.m3367(r02);
        }
        int r04 = m3339(r10);
        if (this.f2026.f2059 >= 0) {
            i = r04;
            r04 = 0;
        } else {
            i = 0;
        }
        int r05 = r04 + this.f2035.m4294();
        int r3 = i + this.f2035.m4302();
        if (!(!r10.m4771() || (i7 = this.f2037) == -1 || this.f2038 == Integer.MIN_VALUE || (r5 = m3347(i7)) == null)) {
            if (this.f2036) {
                i8 = this.f2035.m4296() - this.f2035.m4293(r5);
                i9 = this.f2038;
            } else {
                i9 = this.f2035.m4289(r5) - this.f2035.m4294();
                i8 = this.f2038;
            }
            int i11 = i8 - i9;
            if (i11 > 0) {
                r05 += i11;
            } else {
                r3 -= i11;
            }
        }
        if (!this.f2040.f2043 ? !this.f2036 : this.f2036) {
            i10 = 1;
        }
        m3330(r9, r10, this.f2040, i10);
        m4558(r9);
        this.f2026.f2061 = m3362();
        this.f2026.f2058 = r10.m4771();
        if (this.f2040.f2043) {
            m3302(this.f2040);
            C0556 r1 = this.f2026;
            r1.f2057 = r05;
            m3322(r9, r1, r10, false);
            i3 = this.f2026.f2051;
            int i12 = this.f2026.f2053;
            if (this.f2026.f2052 > 0) {
                r3 += this.f2026.f2052;
            }
            m3294(this.f2040);
            C0556 r52 = this.f2026;
            r52.f2057 = r3;
            r52.f2053 += this.f2026.f2054;
            m3322(r9, this.f2026, r10, false);
            i2 = this.f2026.f2051;
            if (this.f2026.f2052 > 0) {
                int i13 = this.f2026.f2052;
                m3308(i12, i3);
                C0556 r06 = this.f2026;
                r06.f2057 = i13;
                m3322(r9, r06, r10, false);
                i3 = this.f2026.f2051;
            }
        } else {
            m3294(this.f2040);
            C0556 r12 = this.f2026;
            r12.f2057 = r3;
            m3322(r9, r12, r10, false);
            i2 = this.f2026.f2051;
            int i14 = this.f2026.f2053;
            if (this.f2026.f2052 > 0) {
                r05 += this.f2026.f2052;
            }
            m3302(this.f2040);
            C0556 r53 = this.f2026;
            r53.f2057 = r05;
            r53.f2053 += this.f2026.f2054;
            m3322(r9, this.f2026, r10, false);
            i3 = this.f2026.f2051;
            if (this.f2026.f2052 > 0) {
                int i15 = this.f2026.f2052;
                m3292(i14, i2);
                C0556 r13 = this.f2026;
                r13.f2057 = i15;
                m3322(r9, r13, r10, false);
                i2 = this.f2026.f2051;
            }
        }
        if (m4667() > 0) {
            if (this.f2036 ^ this.f2029) {
                int r14 = m3290(i2, r9, r10, true);
                i6 = i3 + r14;
                i4 = i2 + r14;
                i5 = m3300(i6, r9, r10, false);
            } else {
                int r15 = m3300(i3, r9, r10, true);
                i6 = i3 + r15;
                i4 = i2 + r15;
                i5 = m3290(i4, r9, r10, false);
            }
            i3 = i6 + i5;
            i2 = i4 + i5;
        }
        m3304(r9, r10, i3, i2);
        if (!r10.m4771()) {
            this.f2035.m4290();
        } else {
            this.f2040.m3366();
        }
        this.f2027 = this.f2029;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3332(C0690.C0717 r1) {
        super.m4563(r1);
        this.f2039 = null;
        this.f2037 = -1;
        this.f2038 = Integer.MIN_VALUE;
        this.f2040.m3366();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
     arg types: [android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, android.support.v7.widget.ﹳﹳ$ᵔ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.LinearLayoutManager$ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.ﹳﹳ$ˉ$ʼ
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, java.lang.Object):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.view.View):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int, android.support.v7.widget.ﹳﹳ$ˊ):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int */
    /* renamed from: ʼ  reason: contains not printable characters */
    private void m3304(C0690.C0711 r16, C0690.C0717 r17, int i, int i2) {
        C0690.C0711 r1 = r16;
        C0690.C0717 r2 = r17;
        if (r17.m4772() && m4667() != 0 && !r17.m4771() && m3343()) {
            List<C0690.C0720> r3 = r16.m4732();
            int size = r3.size();
            int r6 = m4620(m4645(0));
            int i3 = 0;
            int i4 = 0;
            for (int i5 = 0; i5 < size; i5++) {
                C0690.C0720 r10 = r3.get(i5);
                if (!r10.m4827()) {
                    char c = 1;
                    if ((r10.m4814() < r6) != this.f2036) {
                        c = 65535;
                    }
                    if (c == 65535) {
                        i3 += this.f2035.m4299(r10.f2914);
                    } else {
                        i4 += this.f2035.m4299(r10.f2914);
                    }
                }
            }
            this.f2026.f2060 = r3;
            if (i3 > 0) {
                m3308(m4620(m3309()), i);
                C0556 r32 = this.f2026;
                r32.f2057 = i3;
                r32.f2052 = 0;
                r32.m3374();
                m3322(r1, this.f2026, r2, false);
            }
            if (i4 > 0) {
                m3292(m4620(m3316()), i2);
                C0556 r33 = this.f2026;
                r33.f2057 = i4;
                r33.f2052 = 0;
                r33.m3374();
                m3322(r1, this.f2026, r2, false);
            }
            this.f2026.f2060 = null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3298(C0690.C0711 r2, C0690.C0717 r3, C0554 r4) {
        if (!m3299(r3, r4) && !m3305(r2, r3, r4)) {
            r4.m3369();
            r4.f2041 = this.f2029 ? r3.m4775() - 1 : 0;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m3305(C0690.C0711 r5, C0690.C0717 r6, C0554 r7) {
        View view;
        int i;
        boolean z = false;
        if (m4667() == 0) {
            return false;
        }
        View r0 = m4617();
        if (r0 != null && r7.m3368(r0, r6)) {
            r7.m3367(r0);
            return true;
        } else if (this.f2027 != this.f2029) {
            return false;
        } else {
            if (r7.f2043) {
                view = m3306(r5, r6);
            } else {
                view = m3307(r5, r6);
            }
            if (view == null) {
                return false;
            }
            r7.m3370(view);
            if (!r6.m4771() && m3343()) {
                if (this.f2035.m4289(view) >= this.f2035.m4296() || this.f2035.m4293(view) < this.f2035.m4294()) {
                    z = true;
                }
                if (z) {
                    if (r7.f2043) {
                        i = this.f2035.m4296();
                    } else {
                        i = this.f2035.m4294();
                    }
                    r7.f2042 = i;
                }
            }
            return true;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m3299(C0690.C0717 r5, C0554 r6) {
        int i;
        int i2;
        boolean z = false;
        if (!r5.m4771() && (i = this.f2037) != -1) {
            if (i < 0 || i >= r5.m4775()) {
                this.f2037 = -1;
                this.f2038 = Integer.MIN_VALUE;
            } else {
                r6.f2041 = this.f2037;
                C0557 r52 = this.f2039;
                if (r52 != null && r52.m3378()) {
                    r6.f2043 = this.f2039.f2064;
                    if (r6.f2043) {
                        r6.f2042 = this.f2035.m4296() - this.f2039.f2063;
                    } else {
                        r6.f2042 = this.f2035.m4294() + this.f2039.f2063;
                    }
                    return true;
                } else if (this.f2038 == Integer.MIN_VALUE) {
                    View r53 = m3347(this.f2037);
                    if (r53 == null) {
                        if (m4667() > 0) {
                            if ((this.f2037 < m4620(m4645(0))) == this.f2036) {
                                z = true;
                            }
                            r6.f2043 = z;
                        }
                        r6.m3369();
                    } else if (this.f2035.m4299(r53) > this.f2035.m4300()) {
                        r6.m3369();
                        return true;
                    } else if (this.f2035.m4289(r53) - this.f2035.m4294() < 0) {
                        r6.f2042 = this.f2035.m4294();
                        r6.f2043 = false;
                        return true;
                    } else if (this.f2035.m4296() - this.f2035.m4293(r53) < 0) {
                        r6.f2042 = this.f2035.m4296();
                        r6.f2043 = true;
                        return true;
                    } else {
                        if (r6.f2043) {
                            i2 = this.f2035.m4293(r53) + this.f2035.m4292();
                        } else {
                            i2 = this.f2035.m4289(r53);
                        }
                        r6.f2042 = i2;
                    }
                    return true;
                } else {
                    boolean z2 = this.f2036;
                    r6.f2043 = z2;
                    if (z2) {
                        r6.f2042 = this.f2035.m4296() - this.f2038;
                    } else {
                        r6.f2042 = this.f2035.m4294() + this.f2038;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m3290(int i, C0690.C0711 r3, C0690.C0717 r4, boolean z) {
        int r42;
        int r0 = this.f2035.m4296() - i;
        if (r0 <= 0) {
            return 0;
        }
        int i2 = -m3344(-r0, r3, r4);
        int i3 = i + i2;
        if (!z || (r42 = this.f2035.m4296() - i3) <= 0) {
            return i2;
        }
        this.f2035.m4291(r42);
        return r42 + i2;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int m3300(int i, C0690.C0711 r3, C0690.C0717 r4, boolean z) {
        int r2;
        int r0 = i - this.f2035.m4294();
        if (r0 <= 0) {
            return 0;
        }
        int i2 = -m3344(r0, r3, r4);
        int i3 = i + i2;
        if (!z || (r2 = i3 - this.f2035.m4294()) <= 0) {
            return i2;
        }
        this.f2035.m4291(-r2);
        return i2 - r2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3294(C0554 r2) {
        m3292(r2.f2041, r2.f2042);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3292(int i, int i2) {
        this.f2026.f2052 = this.f2035.m4296() - i2;
        this.f2026.f2054 = this.f2036 ? -1 : 1;
        C0556 r0 = this.f2026;
        r0.f2053 = i;
        r0.f2055 = 1;
        r0.f2051 = i2;
        r0.f2056 = Integer.MIN_VALUE;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m3302(C0554 r2) {
        m3308(r2.f2041, r2.f2042);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private void m3308(int i, int i2) {
        this.f2026.f2052 = i2 - this.f2035.m4294();
        C0556 r0 = this.f2026;
        r0.f2053 = i;
        r0.f2054 = this.f2036 ? 1 : -1;
        C0556 r3 = this.f2026;
        r3.f2055 = -1;
        r3.f2051 = i2;
        r3.f2056 = Integer.MIN_VALUE;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m3358() {
        return m4665() == 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public void m3360() {
        if (this.f2026 == null) {
            this.f2026 = m3361();
        }
        if (this.f2035 == null) {
            this.f2035 = C0688.m4287(this, this.f2034);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public C0556 m3361() {
        return new C0556();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m3350(int i) {
        this.f2037 = i;
        this.f2038 = Integer.MIN_VALUE;
        C0557 r1 = this.f2039;
        if (r1 != null) {
            r1.m3379();
        }
        m4656();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m3321(int i, C0690.C0711 r4, C0690.C0717 r5) {
        if (this.f2034 == 1) {
            return 0;
        }
        return m3344(i, r4, r5);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m3338(int i, C0690.C0711 r3, C0690.C0717 r4) {
        if (this.f2034 == 0) {
            return 0;
        }
        return m3344(i, r3, r4);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m3345(C0690.C0717 r1) {
        return m3312(r1);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m3349(C0690.C0717 r1) {
        return m3312(r1);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m3353(C0690.C0717 r1) {
        return m3314(r1);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public int m3356(C0690.C0717 r1) {
        return m3314(r1);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public int m3357(C0690.C0717 r1) {
        return m3317(r1);
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public int m3359(C0690.C0717 r1) {
        return m3317(r1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʻ(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.view.View):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ˉ, android.support.v7.widget.ﹳﹳ$ᵎ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.content.Context, android.util.AttributeSet):android.support.v7.widget.ﹳﹳ$ˊ
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ʻ, android.support.v7.widget.ﹳﹳ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.os.Bundle):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(boolean, boolean):android.view.View */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʼ(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʼ(android.support.v7.widget.ﹳﹳ$ـ, int):void
      android.support.v7.widget.LinearLayoutManager.ʼ(int, int):android.view.View
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(android.view.View, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.LinearLayoutManager.ʼ(boolean, boolean):android.view.View */
    /* renamed from: ˊ  reason: contains not printable characters */
    private int m3312(C0690.C0717 r8) {
        if (m4667() == 0) {
            return 0;
        }
        m3360();
        C0688 r1 = this.f2035;
        View r3 = m3291(!this.f2030, true);
        return C0579.m3669(r8, r1, r3, m3301(!this.f2030, true), this, this.f2030, this.f2036);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʻ(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.view.View):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ˉ, android.support.v7.widget.ﹳﹳ$ᵎ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.content.Context, android.util.AttributeSet):android.support.v7.widget.ﹳﹳ$ˊ
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ʻ, android.support.v7.widget.ﹳﹳ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.os.Bundle):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(boolean, boolean):android.view.View */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʼ(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʼ(android.support.v7.widget.ﹳﹳ$ـ, int):void
      android.support.v7.widget.LinearLayoutManager.ʼ(int, int):android.view.View
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(android.view.View, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.LinearLayoutManager.ʼ(boolean, boolean):android.view.View */
    /* renamed from: ˋ  reason: contains not printable characters */
    private int m3314(C0690.C0717 r7) {
        if (m4667() == 0) {
            return 0;
        }
        m3360();
        C0688 r1 = this.f2035;
        View r3 = m3291(!this.f2030, true);
        return C0579.m3668(r7, r1, r3, m3301(!this.f2030, true), this, this.f2030);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʻ(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.view.View):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ˉ, android.support.v7.widget.ﹳﹳ$ᵎ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.content.Context, android.util.AttributeSet):android.support.v7.widget.ﹳﹳ$ˊ
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ʻ, android.support.v7.widget.ﹳﹳ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.os.Bundle):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(boolean, boolean):android.view.View */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʼ(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʼ(android.support.v7.widget.ﹳﹳ$ـ, int):void
      android.support.v7.widget.LinearLayoutManager.ʼ(int, int):android.view.View
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(android.view.View, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.LinearLayoutManager.ʼ(boolean, boolean):android.view.View */
    /* renamed from: ˎ  reason: contains not printable characters */
    private int m3317(C0690.C0717 r7) {
        if (m4667() == 0) {
            return 0;
        }
        m3360();
        C0688 r1 = this.f2035;
        View r3 = m3291(!this.f2030, true);
        return C0579.m3670(r7, r1, r3, m3301(!this.f2030, true), this, this.f2030);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3293(int i, int i2, boolean z, C0690.C0717 r8) {
        int i3;
        this.f2026.f2061 = m3362();
        this.f2026.f2057 = m3339(r8);
        C0556 r82 = this.f2026;
        r82.f2055 = i;
        int i4 = -1;
        if (i == 1) {
            r82.f2057 += this.f2035.m4302();
            View r5 = m3316();
            C0556 r83 = this.f2026;
            if (!this.f2036) {
                i4 = 1;
            }
            r83.f2054 = i4;
            this.f2026.f2053 = m4620(r5) + this.f2026.f2054;
            this.f2026.f2051 = this.f2035.m4293(r5);
            i3 = this.f2035.m4293(r5) - this.f2035.m4296();
        } else {
            View r52 = m3309();
            this.f2026.f2057 += this.f2035.m4294();
            C0556 r84 = this.f2026;
            if (this.f2036) {
                i4 = 1;
            }
            r84.f2054 = i4;
            this.f2026.f2053 = m4620(r52) + this.f2026.f2054;
            this.f2026.f2051 = this.f2035.m4289(r52);
            i3 = (-this.f2035.m4289(r52)) + this.f2035.m4294();
        }
        C0556 r85 = this.f2026;
        r85.f2052 = i2;
        if (z) {
            r85.f2052 -= i3;
        }
        this.f2026.f2056 = i3;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean m3362() {
        return this.f2035.m4303() == 0 && this.f2035.m4298() == 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3333(C0690.C0717 r2, C0556 r3, C0690.C0701.C0702 r4) {
        int i = r3.f2053;
        if (i >= 0 && i < r2.m4775()) {
            r4.m4683(i, Math.max(0, r3.f2056));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3328(int i, C0690.C0701.C0702 r7) {
        int i2;
        boolean z;
        C0557 r0 = this.f2039;
        int i3 = -1;
        if (r0 == null || !r0.m3378()) {
            m3311();
            z = this.f2036;
            i2 = this.f2037;
            if (i2 == -1) {
                i2 = z ? i - 1 : 0;
            }
        } else {
            z = this.f2039.f2064;
            i2 = this.f2039.f2062;
        }
        if (!z) {
            i3 = 1;
        }
        for (int i4 = 0; i4 < this.f2033 && i2 >= 0 && i2 < i; i4++) {
            r7.m4683(i2, 0);
            i2 += i3;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, android.support.v7.widget.ﹳﹳ$ᵔ):void
     arg types: [int, int, int, android.support.v7.widget.ﹳﹳ$ᵔ]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.LinearLayoutManager$ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.ﹳﹳ$ˉ$ʼ
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, java.lang.Object):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.view.View):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int, android.support.v7.widget.ﹳﹳ$ˊ):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, android.support.v7.widget.ﹳﹳ$ᵔ):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3327(int i, int i2, C0690.C0717 r4, C0690.C0701.C0702 r5) {
        if (this.f2034 != 0) {
            i = i2;
        }
        if (m4667() != 0 && i != 0) {
            m3360();
            m3293(i > 0 ? 1 : -1, Math.abs(i), true, r4);
            m3333(r4, this.f2026, r5);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, android.support.v7.widget.ﹳﹳ$ᵔ):void
     arg types: [int, int, int, android.support.v7.widget.ﹳﹳ$ᵔ]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.LinearLayoutManager$ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.ﹳﹳ$ˉ$ʼ
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, java.lang.Object):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.view.View):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int, android.support.v7.widget.ﹳﹳ$ˊ):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, android.support.v7.widget.ﹳﹳ$ᵔ):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
     arg types: [android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, android.support.v7.widget.ﹳﹳ$ᵔ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.LinearLayoutManager$ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.ﹳﹳ$ˉ$ʼ
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, java.lang.Object):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.view.View):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int, android.support.v7.widget.ﹳﹳ$ˊ):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int */
    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public int m3344(int i, C0690.C0711 r7, C0690.C0717 r8) {
        if (m4667() == 0 || i == 0) {
            return 0;
        }
        this.f2026.f2050 = true;
        m3360();
        int i2 = i > 0 ? 1 : -1;
        int abs = Math.abs(i);
        m3293(i2, abs, true, r8);
        int r2 = this.f2026.f2056 + m3322(r7, this.f2026, r8, false);
        if (r2 < 0) {
            return 0;
        }
        if (abs > r2) {
            i = i2 * r2;
        }
        this.f2035.m4291(-i);
        this.f2026.f2059 = i;
        return i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3336(String str) {
        if (this.f2039 == null) {
            super.m4579(str);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3296(C0690.C0711 r1, int i, int i2) {
        if (i != i2) {
            if (i2 > i) {
                for (int i3 = i2 - 1; i3 >= i; i3--) {
                    m4553(i3, r1);
                }
                return;
            }
            while (i > i2) {
                m4553(i, r1);
                i--;
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3295(C0690.C0711 r6, int i) {
        if (i >= 0) {
            int r0 = m4667();
            if (this.f2036) {
                int i2 = r0 - 1;
                for (int i3 = i2; i3 >= 0; i3--) {
                    View r2 = m4645(i3);
                    if (this.f2035.m4293(r2) > i || this.f2035.m4295(r2) > i) {
                        m3296(r6, i2, i3);
                        return;
                    }
                }
                return;
            }
            for (int i4 = 0; i4 < r0; i4++) {
                View r3 = m4645(i4);
                if (this.f2035.m4293(r3) > i || this.f2035.m4295(r3) > i) {
                    m3296(r6, 0, i4);
                    return;
                }
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m3303(C0690.C0711 r6, int i) {
        int r0 = m4667();
        if (i >= 0) {
            int r1 = this.f2035.m4298() - i;
            if (this.f2036) {
                for (int i2 = 0; i2 < r0; i2++) {
                    View r3 = m4645(i2);
                    if (this.f2035.m4289(r3) < r1 || this.f2035.m4297(r3) < r1) {
                        m3296(r6, 0, i2);
                        return;
                    }
                }
                return;
            }
            int i3 = r0 - 1;
            for (int i4 = i3; i4 >= 0; i4--) {
                View r2 = m4645(i4);
                if (this.f2035.m4289(r2) < r1 || this.f2035.m4297(r2) < r1) {
                    m3296(r6, i3, i4);
                    return;
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3297(C0690.C0711 r3, C0556 r4) {
        if (r4.f2050 && !r4.f2061) {
            if (r4.f2055 == -1) {
                m3303(r3, r4.f2056);
            } else {
                m3295(r3, r4.f2056);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m3322(C0690.C0711 r8, C0556 r9, C0690.C0717 r10, boolean z) {
        int i = r9.f2052;
        if (r9.f2056 != Integer.MIN_VALUE) {
            if (r9.f2052 < 0) {
                r9.f2056 += r9.f2052;
            }
            m3297(r8, r9);
        }
        int i2 = r9.f2052 + r9.f2057;
        C0555 r3 = this.f2032;
        while (true) {
            if ((!r9.f2061 && i2 <= 0) || !r9.m3376(r10)) {
                break;
            }
            r3.m3371();
            m3331(r8, r10, r9, r3);
            if (!r3.f2047) {
                r9.f2051 += r3.f2046 * r9.f2055;
                if (!r3.f2048 || this.f2026.f2060 != null || !r10.m4771()) {
                    r9.f2052 -= r3.f2046;
                    i2 -= r3.f2046;
                }
                if (r9.f2056 != Integer.MIN_VALUE) {
                    r9.f2056 += r3.f2046;
                    if (r9.f2052 < 0) {
                        r9.f2056 += r9.f2052;
                    }
                    m3297(r8, r9);
                }
                if (z && r3.f2049) {
                    break;
                }
            } else {
                break;
            }
        }
        return i - r9.f2052;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3331(C0690.C0711 r8, C0690.C0717 r9, C0556 r10, C0555 r11) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        View r82 = r10.m3373(r8);
        if (r82 == null) {
            r11.f2047 = true;
            return;
        }
        C0690.C0704 r6 = (C0690.C0704) r82.getLayoutParams();
        if (r10.f2060 == null) {
            if (this.f2036 == (r10.f2055 == -1)) {
                m4600(r82);
            } else {
                m4601(r82, 0);
            }
        } else {
            if (this.f2036 == (r10.f2055 == -1)) {
                m4569(r82);
            } else {
                m4570(r82, 0);
            }
        }
        m4571(r82, 0, 0);
        r11.f2046 = this.f2035.m4299(r82);
        if (this.f2034 == 1) {
            if (m3358()) {
                i5 = m4670() - m4664();
                i4 = i5 - this.f2035.m4301(r82);
            } else {
                i4 = m4672();
                i5 = this.f2035.m4301(r82) + i4;
            }
            if (r10.f2055 == -1) {
                int i6 = r10.f2051;
                i3 = r10.f2051 - r11.f2046;
                i2 = i5;
                i = i6;
            } else {
                int i7 = r10.f2051;
                i = r10.f2051 + r11.f2046;
                i2 = i5;
                i3 = i7;
            }
        } else {
            int r0 = m4662();
            int r2 = this.f2035.m4301(r82) + r0;
            if (r10.f2055 == -1) {
                i3 = r0;
                i2 = r10.f2051;
                i = r2;
                i4 = r10.f2051 - r11.f2046;
            } else {
                int i8 = r10.f2051;
                i2 = r10.f2051 + r11.f2046;
                i3 = r0;
                i = r2;
                i4 = i8;
            }
        }
        m4572(r82, i4, i3, i2, i);
        if (r6.m4685() || r6.m4686()) {
            r11.f2048 = true;
        }
        r11.f2049 = r82.hasFocusable();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean m3363() {
        return (m4669() == 1073741824 || m4668() == 1073741824 || !m4639()) ? false : true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public int m3352(int i) {
        if (i == 1) {
            return (this.f2034 != 1 && m3358()) ? 1 : -1;
        }
        if (i == 2) {
            return (this.f2034 != 1 && m3358()) ? -1 : 1;
        }
        if (i != 17) {
            if (i != 33) {
                if (i != 66) {
                    return (i == 130 && this.f2034 == 1) ? 1 : Integer.MIN_VALUE;
                }
                if (this.f2034 == 0) {
                    return 1;
                }
                return Integer.MIN_VALUE;
            } else if (this.f2034 == 1) {
                return -1;
            } else {
                return Integer.MIN_VALUE;
            }
        } else if (this.f2034 == 0) {
            return -1;
        } else {
            return Integer.MIN_VALUE;
        }
    }

    /* renamed from: ˈˈ  reason: contains not printable characters */
    private View m3309() {
        return m4645(this.f2036 ? m4667() - 1 : 0);
    }

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private View m3316() {
        return m4645(this.f2036 ? 0 : m4667() - 1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private View m3291(boolean z, boolean z2) {
        if (this.f2036) {
            return m3324(m4667() - 1, -1, z, z2);
        }
        return m3324(0, m4667(), z, z2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private View m3301(boolean z, boolean z2) {
        if (this.f2036) {
            return m3324(0, m4667(), z, z2);
        }
        return m3324(m4667() - 1, -1, z, z2);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private View m3306(C0690.C0711 r2, C0690.C0717 r3) {
        if (this.f2036) {
            return m3310(r2, r3);
        }
        return m3313(r2, r3);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private View m3307(C0690.C0711 r2, C0690.C0717 r3) {
        if (this.f2036) {
            return m3313(r2, r3);
        }
        return m3310(r2, r3);
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private View m3310(C0690.C0711 r7, C0690.C0717 r8) {
        return m3325(r7, r8, 0, m4667(), r8.m4775());
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private View m3313(C0690.C0711 r8, C0690.C0717 r9) {
        return m3325(r8, r9, m4667() - 1, -1, r9.m4775());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public View m3325(C0690.C0711 r6, C0690.C0717 r7, int i, int i2, int i3) {
        m3360();
        int r62 = this.f2035.m4294();
        int r72 = this.f2035.m4296();
        int i4 = i2 > i ? 1 : -1;
        View view = null;
        View view2 = null;
        while (i != i2) {
            View r3 = m4645(i);
            int r4 = m4620(r3);
            if (r4 >= 0 && r4 < i3) {
                if (((C0690.C0704) r3.getLayoutParams()).m4685()) {
                    if (view2 == null) {
                        view2 = r3;
                    }
                } else if (this.f2035.m4289(r3) < r72 && this.f2035.m4293(r3) >= r62) {
                    return r3;
                } else {
                    if (view == null) {
                        view = r3;
                    }
                }
            }
            i += i4;
        }
        return view != null ? view : view2;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private View m3315(C0690.C0711 r2, C0690.C0717 r3) {
        if (this.f2036) {
            return m3319(r2, r3);
        }
        return m3320(r2, r3);
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private View m3318(C0690.C0711 r2, C0690.C0717 r3) {
        if (this.f2036) {
            return m3320(r2, r3);
        }
        return m3319(r2, r3);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private View m3319(C0690.C0711 r1, C0690.C0717 r2) {
        return m3340(0, m4667());
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private View m3320(C0690.C0711 r1, C0690.C0717 r2) {
        return m3340(m4667() - 1, -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, boolean):android.view.View
     arg types: [int, int, int, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, android.support.v7.widget.ﹳﹳ$ᵔ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.LinearLayoutManager$ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.ﹳﹳ$ˉ$ʼ
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, java.lang.Object):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.view.View):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int, android.support.v7.widget.ﹳﹳ$ˊ):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, boolean):android.view.View */
    /* renamed from: ˏ  reason: contains not printable characters */
    public int m3364() {
        View r0 = m3324(0, m4667(), false, true);
        if (r0 == null) {
            return -1;
        }
        return m4620(r0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, boolean):android.view.View
     arg types: [int, int, int, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, android.support.v7.widget.ﹳﹳ$ᵔ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.LinearLayoutManager$ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.ﹳﹳ$ˉ$ʼ
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, java.lang.Object):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.view.View):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int, android.support.v7.widget.ﹳﹳ$ˊ):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, boolean):android.view.View */
    /* renamed from: ˑ  reason: contains not printable characters */
    public int m3365() {
        View r0 = m3324(m4667() - 1, -1, false, true);
        if (r0 == null) {
            return -1;
        }
        return m4620(r0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public View m3324(int i, int i2, boolean z, boolean z2) {
        m3360();
        int i3 = 320;
        int i4 = z ? 24579 : 320;
        if (!z2) {
            i3 = 0;
        }
        if (this.f2034 == 0) {
            return this.f2840.m3821(i, i2, i4, i3);
        }
        return this.f2841.m3821(i, i2, i4, i3);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public View m3340(int i, int i2) {
        int i3;
        int i4;
        m3360();
        if ((i2 > i ? 1 : i2 < i ? (char) 65535 : 0) == 0) {
            return m4645(i);
        }
        if (this.f2035.m4289(m4645(i)) < this.f2035.m4294()) {
            i4 = 16644;
            i3 = 16388;
        } else {
            i4 = 4161;
            i3 = 4097;
        }
        if (this.f2034 == 0) {
            return this.f2840.m3821(i, i2, i4, i3);
        }
        return this.f2841.m3821(i, i2, i4, i3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, android.support.v7.widget.ﹳﹳ$ᵔ):void
     arg types: [int, int, int, android.support.v7.widget.ﹳﹳ$ᵔ]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.LinearLayoutManager$ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.ﹳﹳ$ˉ$ʼ
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, java.lang.Object):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.view.View):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int, android.support.v7.widget.ﹳﹳ$ˊ):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, android.support.v7.widget.ﹳﹳ$ᵔ):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
     arg types: [android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, android.support.v7.widget.ﹳﹳ$ᵔ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.LinearLayoutManager$ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.ﹳﹳ$ˉ$ʼ
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, java.lang.Object):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.view.View):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int, android.support.v7.widget.ﹳﹳ$ˊ):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int */
    /* renamed from: ʻ  reason: contains not printable characters */
    public View m3326(View view, int i, C0690.C0711 r6, C0690.C0717 r7) {
        int r4;
        View view2;
        View view3;
        m3311();
        if (m4667() == 0 || (r4 = m3352(i)) == Integer.MIN_VALUE) {
            return null;
        }
        m3360();
        m3360();
        m3293(r4, (int) (((float) this.f2035.m4300()) * 0.33333334f), false, r7);
        C0556 r1 = this.f2026;
        r1.f2056 = Integer.MIN_VALUE;
        r1.f2050 = false;
        m3322(r6, r1, r7, true);
        if (r4 == -1) {
            view2 = m3318(r6, r7);
        } else {
            view2 = m3315(r6, r7);
        }
        if (r4 == -1) {
            view3 = m3309();
        } else {
            view3 = m3316();
        }
        if (!view3.hasFocusable()) {
            return view2;
        }
        if (view2 == null) {
            return null;
        }
        return view3;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m3343() {
        return this.f2039 == null && this.f2027 == this.f2029;
    }

    /* renamed from: android.support.v7.widget.LinearLayoutManager$ʽ  reason: contains not printable characters */
    static class C0556 {

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean f2050 = true;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f2051;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f2052;

        /* renamed from: ʾ  reason: contains not printable characters */
        int f2053;

        /* renamed from: ʿ  reason: contains not printable characters */
        int f2054;

        /* renamed from: ˆ  reason: contains not printable characters */
        int f2055;

        /* renamed from: ˈ  reason: contains not printable characters */
        int f2056;

        /* renamed from: ˉ  reason: contains not printable characters */
        int f2057 = 0;

        /* renamed from: ˊ  reason: contains not printable characters */
        boolean f2058 = false;

        /* renamed from: ˋ  reason: contains not printable characters */
        int f2059;

        /* renamed from: ˎ  reason: contains not printable characters */
        List<C0690.C0720> f2060 = null;

        /* renamed from: ˏ  reason: contains not printable characters */
        boolean f2061;

        C0556() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m3376(C0690.C0717 r2) {
            int i = this.f2053;
            return i >= 0 && i < r2.m4775();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public View m3373(C0690.C0711 r3) {
            if (this.f2060 != null) {
                return m3372();
            }
            View r32 = r3.m4731(this.f2053);
            this.f2053 += this.f2054;
            return r32;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private View m3372() {
            int size = this.f2060.size();
            for (int i = 0; i < size; i++) {
                View view = this.f2060.get(i).f2914;
                C0690.C0704 r3 = (C0690.C0704) view.getLayoutParams();
                if (!r3.m4685() && this.f2053 == r3.m4687()) {
                    m3375(view);
                    return view;
                }
            }
            return null;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3374() {
            m3375((View) null);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3375(View view) {
            View r1 = m3377(view);
            if (r1 == null) {
                this.f2053 = -1;
            } else {
                this.f2053 = ((C0690.C0704) r1.getLayoutParams()).m4687();
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public View m3377(View view) {
            int r5;
            int size = this.f2060.size();
            View view2 = null;
            int i = Integer.MAX_VALUE;
            for (int i2 = 0; i2 < size; i2++) {
                View view3 = this.f2060.get(i2).f2914;
                C0690.C0704 r52 = (C0690.C0704) view3.getLayoutParams();
                if (view3 != view && !r52.m4685() && (r5 = (r52.m4687() - this.f2053) * this.f2054) >= 0 && r5 < i) {
                    if (r5 == 0) {
                        return view3;
                    }
                    view2 = view3;
                    i = r5;
                }
            }
            return view2;
        }
    }

    /* renamed from: android.support.v7.widget.LinearLayoutManager$ʾ  reason: contains not printable characters */
    public static class C0557 implements Parcelable {
        public static final Parcelable.Creator<C0557> CREATOR = new Parcelable.Creator<C0557>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C0557 createFromParcel(Parcel parcel) {
                return new C0557(parcel);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0557[] newArray(int i) {
                return new C0557[i];
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        int f2062;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f2063;

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean f2064;

        public int describeContents() {
            return 0;
        }

        public C0557() {
        }

        C0557(Parcel parcel) {
            this.f2062 = parcel.readInt();
            this.f2063 = parcel.readInt();
            this.f2064 = parcel.readInt() != 1 ? false : true;
        }

        public C0557(C0557 r2) {
            this.f2062 = r2.f2062;
            this.f2063 = r2.f2063;
            this.f2064 = r2.f2064;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m3378() {
            return this.f2062 >= 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m3379() {
            this.f2062 = -1;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f2062);
            parcel.writeInt(this.f2063);
            parcel.writeInt(this.f2064 ? 1 : 0);
        }
    }

    /* renamed from: android.support.v7.widget.LinearLayoutManager$ʻ  reason: contains not printable characters */
    class C0554 {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f2041;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f2042;

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean f2043;

        /* renamed from: ʾ  reason: contains not printable characters */
        boolean f2044;

        C0554() {
            m3366();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3366() {
            this.f2041 = -1;
            this.f2042 = Integer.MIN_VALUE;
            this.f2043 = false;
            this.f2044 = false;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m3369() {
            int i;
            if (this.f2043) {
                i = LinearLayoutManager.this.f2035.m4296();
            } else {
                i = LinearLayoutManager.this.f2035.m4294();
            }
            this.f2042 = i;
        }

        public String toString() {
            return "AnchorInfo{mPosition=" + this.f2041 + ", mCoordinate=" + this.f2042 + ", mLayoutFromEnd=" + this.f2043 + ", mValid=" + this.f2044 + '}';
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m3368(View view, C0690.C0717 r3) {
            C0690.C0704 r2 = (C0690.C0704) view.getLayoutParams();
            return !r2.m4685() && r2.m4687() >= 0 && r2.m4687() < r3.m4775();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3367(View view) {
            int r0 = LinearLayoutManager.this.f2035.m4292();
            if (r0 >= 0) {
                m3370(view);
                return;
            }
            this.f2041 = LinearLayoutManager.this.m4620(view);
            if (this.f2043) {
                int r1 = (LinearLayoutManager.this.f2035.m4296() - r0) - LinearLayoutManager.this.f2035.m4293(view);
                this.f2042 = LinearLayoutManager.this.f2035.m4296() - r1;
                if (r1 > 0) {
                    int r3 = this.f2042 - LinearLayoutManager.this.f2035.m4299(view);
                    int r02 = LinearLayoutManager.this.f2035.m4294();
                    int min = r3 - (r02 + Math.min(LinearLayoutManager.this.f2035.m4289(view) - r02, 0));
                    if (min < 0) {
                        this.f2042 += Math.min(r1, -min);
                        return;
                    }
                    return;
                }
                return;
            }
            int r12 = LinearLayoutManager.this.f2035.m4289(view);
            int r32 = r12 - LinearLayoutManager.this.f2035.m4294();
            this.f2042 = r12;
            if (r32 > 0) {
                int r6 = (LinearLayoutManager.this.f2035.m4296() - Math.min(0, (LinearLayoutManager.this.f2035.m4296() - r0) - LinearLayoutManager.this.f2035.m4293(view))) - (r12 + LinearLayoutManager.this.f2035.m4299(view));
                if (r6 < 0) {
                    this.f2042 -= Math.min(r32, -r6);
                }
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m3370(View view) {
            if (this.f2043) {
                this.f2042 = LinearLayoutManager.this.f2035.m4293(view) + LinearLayoutManager.this.f2035.m4292();
            } else {
                this.f2042 = LinearLayoutManager.this.f2035.m4289(view);
            }
            this.f2041 = LinearLayoutManager.this.m4620(view);
        }
    }

    /* renamed from: android.support.v7.widget.LinearLayoutManager$ʼ  reason: contains not printable characters */
    protected static class C0555 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public int f2046;

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean f2047;

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean f2048;

        /* renamed from: ʾ  reason: contains not printable characters */
        public boolean f2049;

        protected C0555() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3371() {
            this.f2046 = 0;
            this.f2047 = false;
            this.f2048 = false;
            this.f2049 = false;
        }
    }
}
