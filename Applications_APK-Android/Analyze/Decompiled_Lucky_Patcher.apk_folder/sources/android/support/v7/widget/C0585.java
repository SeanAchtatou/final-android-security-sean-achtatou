package android.support.v7.widget;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.C0101;
import android.support.v4.widget.C0178;
import android.support.v7.ʻ.C0727;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.WeakHashMap;
import net.lingala.zip4j.util.InternalZipConstants;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

/* renamed from: android.support.v7.widget.ʻˆ  reason: contains not printable characters */
/* compiled from: SuggestionsAdapter */
class C0585 extends C0178 implements View.OnClickListener {

    /* renamed from: ˋ  reason: contains not printable characters */
    private final SearchManager f2285 = ((SearchManager) this.f603.getSystemService("search"));

    /* renamed from: ˎ  reason: contains not printable characters */
    private final SearchView f2286;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final SearchableInfo f2287;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Context f2288;

    /* renamed from: י  reason: contains not printable characters */
    private final WeakHashMap<String, Drawable.ConstantState> f2289;

    /* renamed from: ـ  reason: contains not printable characters */
    private final int f2290;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f2291 = false;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f2292 = 1;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private ColorStateList f2293;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private int f2294 = -1;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private int f2295 = -1;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private int f2296 = -1;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private int f2297 = -1;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private int f2298 = -1;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int f2299 = -1;

    public boolean hasStableIds() {
        return false;
    }

    public C0585(Context context, SearchView searchView, SearchableInfo searchableInfo, WeakHashMap<String, Drawable.ConstantState> weakHashMap) {
        super(context, searchView.getSuggestionRowLayout(), null, true);
        this.f2286 = searchView;
        this.f2287 = searchableInfo;
        this.f2290 = searchView.getSuggestionCommitIconResId();
        this.f2288 = context;
        this.f2289 = weakHashMap;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3724(int i) {
        this.f2292 = i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Cursor m3721(CharSequence charSequence) {
        String charSequence2 = charSequence == null ? BuildConfig.FLAVOR : charSequence.toString();
        if (this.f2286.getVisibility() == 0 && this.f2286.getWindowVisibility() == 0) {
            try {
                Cursor r4 = m3720(this.f2287, charSequence2, 50);
                if (r4 != null) {
                    r4.getCount();
                    return r4;
                }
            } catch (RuntimeException e) {
                Log.w("SuggestionsAdapter", "Search suggestions query threw an exception.", e);
            }
        }
        return null;
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        m3716(m1007());
    }

    public void notifyDataSetInvalidated() {
        super.notifyDataSetInvalidated();
        m3716(m1007());
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m3716(Cursor cursor) {
        Bundle extras = cursor != null ? cursor.getExtras() : null;
        if (extras == null || extras.getBoolean("in_progress")) {
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3725(Cursor cursor) {
        if (this.f2291) {
            Log.w("SuggestionsAdapter", "Tried to change cursor after adapter was closed.");
            if (cursor != null) {
                cursor.close();
                return;
            }
            return;
        }
        try {
            super.m1011(cursor);
            if (cursor != null) {
                this.f2294 = cursor.getColumnIndex("suggest_text_1");
                this.f2295 = cursor.getColumnIndex("suggest_text_2");
                this.f2296 = cursor.getColumnIndex("suggest_text_2_url");
                this.f2297 = cursor.getColumnIndex("suggest_icon_1");
                this.f2298 = cursor.getColumnIndex("suggest_icon_2");
                this.f2299 = cursor.getColumnIndex("suggest_flags");
            }
        } catch (Exception e) {
            Log.e("SuggestionsAdapter", "error changing cursor and caching columns", e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public View m3723(Context context, Cursor cursor, ViewGroup viewGroup) {
        View r1 = super.m1054(context, cursor, viewGroup);
        r1.setTag(new C0586(r1));
        ((ImageView) r1.findViewById(C0727.C0733.edit_query)).setImageResource(this.f2290);
        return r1;
    }

    /* renamed from: android.support.v7.widget.ʻˆ$ʻ  reason: contains not printable characters */
    /* compiled from: SuggestionsAdapter */
    private static final class C0586 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public final TextView f2300;

        /* renamed from: ʼ  reason: contains not printable characters */
        public final TextView f2301;

        /* renamed from: ʽ  reason: contains not printable characters */
        public final ImageView f2302;

        /* renamed from: ʾ  reason: contains not printable characters */
        public final ImageView f2303;

        /* renamed from: ʿ  reason: contains not printable characters */
        public final ImageView f2304;

        public C0586(View view) {
            this.f2300 = (TextView) view.findViewById(16908308);
            this.f2301 = (TextView) view.findViewById(16908309);
            this.f2302 = (ImageView) view.findViewById(16908295);
            this.f2303 = (ImageView) view.findViewById(16908296);
            this.f2304 = (ImageView) view.findViewById(C0727.C0733.edit_query);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3726(View view, Context context, Cursor cursor) {
        CharSequence charSequence;
        C0586 r7 = (C0586) view.getTag();
        int i = this.f2299;
        int i2 = i != -1 ? cursor.getInt(i) : 0;
        if (r7.f2300 != null) {
            m3710(r7.f2300, m3707(cursor, this.f2294));
        }
        if (r7.f2301 != null) {
            String r1 = m3707(cursor, this.f2296);
            if (r1 != null) {
                charSequence = m3715((CharSequence) r1);
            } else {
                charSequence = m3707(cursor, this.f2295);
            }
            if (TextUtils.isEmpty(charSequence)) {
                if (r7.f2300 != null) {
                    r7.f2300.setSingleLine(false);
                    r7.f2300.setMaxLines(2);
                }
            } else if (r7.f2300 != null) {
                r7.f2300.setSingleLine(true);
                r7.f2300.setMaxLines(1);
            }
            m3710(r7.f2301, charSequence);
        }
        if (r7.f2302 != null) {
            m3709(r7.f2302, m3717(cursor), 4);
        }
        if (r7.f2303 != null) {
            m3709(r7.f2303, m3718(cursor), 8);
        }
        int i3 = this.f2292;
        if (i3 == 2 || (i3 == 1 && (i2 & 1) != 0)) {
            r7.f2304.setVisibility(0);
            r7.f2304.setTag(r7.f2300.getText());
            r7.f2304.setOnClickListener(this);
            return;
        }
        r7.f2304.setVisibility(8);
    }

    public void onClick(View view) {
        Object tag = view.getTag();
        if (tag instanceof CharSequence) {
            this.f2286.m3399((CharSequence) tag);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private CharSequence m3715(CharSequence charSequence) {
        if (this.f2293 == null) {
            TypedValue typedValue = new TypedValue();
            this.f603.getTheme().resolveAttribute(C0727.C0728.textColorSearchUrl, typedValue, true);
            this.f2293 = this.f603.getResources().getColorStateList(typedValue.resourceId);
        }
        SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan(new TextAppearanceSpan(null, 0, 0, this.f2293, null), 0, charSequence.length(), 33);
        return spannableString;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3710(TextView textView, CharSequence charSequence) {
        textView.setText(charSequence);
        if (TextUtils.isEmpty(charSequence)) {
            textView.setVisibility(8);
        } else {
            textView.setVisibility(0);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private Drawable m3717(Cursor cursor) {
        int i = this.f2297;
        if (i == -1) {
            return null;
        }
        Drawable r0 = m3706(cursor.getString(i));
        if (r0 != null) {
            return r0;
        }
        return m3719(cursor);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private Drawable m3718(Cursor cursor) {
        int i = this.f2298;
        if (i == -1) {
            return null;
        }
        return m3706(cursor.getString(i));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3709(ImageView imageView, Drawable drawable, int i) {
        imageView.setImageDrawable(drawable);
        if (drawable == null) {
            imageView.setVisibility(i);
            return;
        }
        imageView.setVisibility(0);
        drawable.setVisible(false, false);
        drawable.setVisible(true, false);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public CharSequence m3727(Cursor cursor) {
        String r3;
        String r1;
        if (cursor == null) {
            return null;
        }
        String r12 = m3708(cursor, "suggest_intent_query");
        if (r12 != null) {
            return r12;
        }
        if (this.f2287.shouldRewriteQueryFromData() && (r1 = m3708(cursor, "suggest_intent_data")) != null) {
            return r1;
        }
        if (!this.f2287.shouldRewriteQueryFromText() || (r3 = m3708(cursor, "suggest_text_1")) == null) {
            return null;
        }
        return r3;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        try {
            return super.getView(i, view, viewGroup);
        } catch (RuntimeException e) {
            Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", e);
            View r3 = m3723(this.f603, this.f602, viewGroup);
            if (r3 != null) {
                ((C0586) r3.getTag()).f2300.setText(e.toString());
            }
            return r3;
        }
    }

    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        try {
            return super.getDropDownView(i, view, viewGroup);
        } catch (RuntimeException e) {
            Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", e);
            View r3 = m1055(this.f603, this.f602, viewGroup);
            if (r3 != null) {
                ((C0586) r3.getTag()).f2300.setText(e.toString());
            }
            return r3;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private Drawable m3706(String str) {
        if (str == null || str.isEmpty() || "0".equals(str)) {
            return null;
        }
        try {
            int parseInt = Integer.parseInt(str);
            String str2 = "android.resource://" + this.f2288.getPackageName() + InternalZipConstants.ZIP_FILE_SEPARATOR + parseInt;
            Drawable r3 = m3714(str2);
            if (r3 != null) {
                return r3;
            }
            Drawable r1 = C0101.m539(this.f2288, parseInt);
            m3711(str2, r1);
            return r1;
        } catch (NumberFormatException unused) {
            Drawable r0 = m3714(str);
            if (r0 != null) {
                return r0;
            }
            Drawable r02 = m3713(Uri.parse(str));
            m3711(str, r02);
            return r02;
        } catch (Resources.NotFoundException unused2) {
            Log.w("SuggestionsAdapter", "Icon resource not found: " + str);
            return null;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:7|8|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002c, code lost:
        throw new java.io.FileNotFoundException("Resource does not exist: " + r7);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0016 */
    /* renamed from: ʼ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.drawable.Drawable m3713(android.net.Uri r7) {
        /*
            r6 = this;
            java.lang.String r0 = "Error closing icon stream for "
            java.lang.String r1 = "SuggestionsAdapter"
            r2 = 0
            java.lang.String r3 = r7.getScheme()     // Catch:{ FileNotFoundException -> 0x0085 }
            java.lang.String r4 = "android.resource"
            boolean r3 = r4.equals(r3)     // Catch:{ FileNotFoundException -> 0x0085 }
            if (r3 == 0) goto L_0x002d
            android.graphics.drawable.Drawable r7 = r6.m3722(r7)     // Catch:{ NotFoundException -> 0x0016 }
            return r7
        L_0x0016:
            java.io.FileNotFoundException r0 = new java.io.FileNotFoundException     // Catch:{ FileNotFoundException -> 0x0085 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0085 }
            r3.<init>()     // Catch:{ FileNotFoundException -> 0x0085 }
            java.lang.String r4 = "Resource does not exist: "
            r3.append(r4)     // Catch:{ FileNotFoundException -> 0x0085 }
            r3.append(r7)     // Catch:{ FileNotFoundException -> 0x0085 }
            java.lang.String r3 = r3.toString()     // Catch:{ FileNotFoundException -> 0x0085 }
            r0.<init>(r3)     // Catch:{ FileNotFoundException -> 0x0085 }
            throw r0     // Catch:{ FileNotFoundException -> 0x0085 }
        L_0x002d:
            android.content.Context r3 = r6.f2288     // Catch:{ FileNotFoundException -> 0x0085 }
            android.content.ContentResolver r3 = r3.getContentResolver()     // Catch:{ FileNotFoundException -> 0x0085 }
            java.io.InputStream r3 = r3.openInputStream(r7)     // Catch:{ FileNotFoundException -> 0x0085 }
            if (r3 == 0) goto L_0x006e
            android.graphics.drawable.Drawable r4 = android.graphics.drawable.Drawable.createFromStream(r3, r2)     // Catch:{ all -> 0x0055 }
            r3.close()     // Catch:{ IOException -> 0x0041 }
            goto L_0x0054
        L_0x0041:
            r3 = move-exception
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0085 }
            r5.<init>()     // Catch:{ FileNotFoundException -> 0x0085 }
            r5.append(r0)     // Catch:{ FileNotFoundException -> 0x0085 }
            r5.append(r7)     // Catch:{ FileNotFoundException -> 0x0085 }
            java.lang.String r0 = r5.toString()     // Catch:{ FileNotFoundException -> 0x0085 }
            android.util.Log.e(r1, r0, r3)     // Catch:{ FileNotFoundException -> 0x0085 }
        L_0x0054:
            return r4
        L_0x0055:
            r4 = move-exception
            r3.close()     // Catch:{ IOException -> 0x005a }
            goto L_0x006d
        L_0x005a:
            r3 = move-exception
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0085 }
            r5.<init>()     // Catch:{ FileNotFoundException -> 0x0085 }
            r5.append(r0)     // Catch:{ FileNotFoundException -> 0x0085 }
            r5.append(r7)     // Catch:{ FileNotFoundException -> 0x0085 }
            java.lang.String r0 = r5.toString()     // Catch:{ FileNotFoundException -> 0x0085 }
            android.util.Log.e(r1, r0, r3)     // Catch:{ FileNotFoundException -> 0x0085 }
        L_0x006d:
            throw r4     // Catch:{ FileNotFoundException -> 0x0085 }
        L_0x006e:
            java.io.FileNotFoundException r0 = new java.io.FileNotFoundException     // Catch:{ FileNotFoundException -> 0x0085 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0085 }
            r3.<init>()     // Catch:{ FileNotFoundException -> 0x0085 }
            java.lang.String r4 = "Failed to open "
            r3.append(r4)     // Catch:{ FileNotFoundException -> 0x0085 }
            r3.append(r7)     // Catch:{ FileNotFoundException -> 0x0085 }
            java.lang.String r3 = r3.toString()     // Catch:{ FileNotFoundException -> 0x0085 }
            r0.<init>(r3)     // Catch:{ FileNotFoundException -> 0x0085 }
            throw r0     // Catch:{ FileNotFoundException -> 0x0085 }
        L_0x0085:
            r0 = move-exception
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Icon not found: "
            r3.append(r4)
            r3.append(r7)
            java.lang.String r7 = ", "
            r3.append(r7)
            java.lang.String r7 = r0.getMessage()
            r3.append(r7)
            java.lang.String r7 = r3.toString()
            android.util.Log.w(r1, r7)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0585.m3713(android.net.Uri):android.graphics.drawable.Drawable");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private Drawable m3714(String str) {
        Drawable.ConstantState constantState = this.f2289.get(str);
        if (constantState == null) {
            return null;
        }
        return constantState.newDrawable();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3711(String str, Drawable drawable) {
        if (drawable != null) {
            this.f2289.put(str, drawable.getConstantState());
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private Drawable m3719(Cursor cursor) {
        Drawable r1 = m3705(this.f2287.getSearchActivity());
        if (r1 != null) {
            return r1;
        }
        return this.f603.getPackageManager().getDefaultActivityIcon();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private Drawable m3705(ComponentName componentName) {
        String flattenToShortString = componentName.flattenToShortString();
        Drawable.ConstantState constantState = null;
        if (this.f2289.containsKey(flattenToShortString)) {
            Drawable.ConstantState constantState2 = this.f2289.get(flattenToShortString);
            if (constantState2 == null) {
                return null;
            }
            return constantState2.newDrawable(this.f2288.getResources());
        }
        Drawable r4 = m3712(componentName);
        if (r4 != null) {
            constantState = r4.getConstantState();
        }
        this.f2289.put(flattenToShortString, constantState);
        return r4;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private Drawable m3712(ComponentName componentName) {
        PackageManager packageManager = this.f603.getPackageManager();
        try {
            ActivityInfo activityInfo = packageManager.getActivityInfo(componentName, 128);
            int iconResource = activityInfo.getIconResource();
            if (iconResource == 0) {
                return null;
            }
            Drawable drawable = packageManager.getDrawable(componentName.getPackageName(), iconResource, activityInfo.applicationInfo);
            if (drawable != null) {
                return drawable;
            }
            Log.w("SuggestionsAdapter", "Invalid icon resource " + iconResource + " for " + componentName.flattenToShortString());
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            Log.w("SuggestionsAdapter", e.toString());
            return null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m3708(Cursor cursor, String str) {
        return m3707(cursor, cursor.getColumnIndex(str));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static String m3707(Cursor cursor, int i) {
        if (i == -1) {
            return null;
        }
        try {
            return cursor.getString(i);
        } catch (Exception e) {
            Log.e("SuggestionsAdapter", "unexpected error retrieving valid column from cursor, did the remote process die?", e);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Drawable m3722(Uri uri) {
        int i;
        String authority = uri.getAuthority();
        if (!TextUtils.isEmpty(authority)) {
            try {
                Resources resourcesForApplication = this.f603.getPackageManager().getResourcesForApplication(authority);
                List<String> pathSegments = uri.getPathSegments();
                if (pathSegments != null) {
                    int size = pathSegments.size();
                    if (size == 1) {
                        try {
                            i = Integer.parseInt(pathSegments.get(0));
                        } catch (NumberFormatException unused) {
                            throw new FileNotFoundException("Single path segment is not a resource ID: " + uri);
                        }
                    } else if (size == 2) {
                        i = resourcesForApplication.getIdentifier(pathSegments.get(1), pathSegments.get(0), authority);
                    } else {
                        throw new FileNotFoundException("More than two path segments: " + uri);
                    }
                    if (i != 0) {
                        return resourcesForApplication.getDrawable(i);
                    }
                    throw new FileNotFoundException("No resource found for: " + uri);
                }
                throw new FileNotFoundException("No path: " + uri);
            } catch (PackageManager.NameNotFoundException unused2) {
                throw new FileNotFoundException("No package found for authority: " + uri);
            }
        } else {
            throw new FileNotFoundException("No authority: " + uri);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Cursor m3720(SearchableInfo searchableInfo, String str, int i) {
        String suggestAuthority;
        String[] strArr = null;
        if (searchableInfo == null || (suggestAuthority = searchableInfo.getSuggestAuthority()) == null) {
            return null;
        }
        Uri.Builder fragment = new Uri.Builder().scheme("content").authority(suggestAuthority).query(BuildConfig.FLAVOR).fragment(BuildConfig.FLAVOR);
        String suggestPath = searchableInfo.getSuggestPath();
        if (suggestPath != null) {
            fragment.appendEncodedPath(suggestPath);
        }
        fragment.appendPath("search_suggest_query");
        String suggestSelection = searchableInfo.getSuggestSelection();
        if (suggestSelection != null) {
            strArr = new String[]{str};
        } else {
            fragment.appendPath(str);
        }
        String[] strArr2 = strArr;
        if (i > 0) {
            fragment.appendQueryParameter("limit", String.valueOf(i));
        }
        return this.f603.getContentResolver().query(fragment.build(), null, suggestSelection, strArr2, null);
    }
}
