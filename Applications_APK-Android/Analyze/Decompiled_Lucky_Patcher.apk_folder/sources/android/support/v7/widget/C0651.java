package android.support.v7.widget;

import android.content.Context;
import android.support.v7.ʼ.ʻ.C0739;
import android.util.AttributeSet;
import android.widget.CheckedTextView;

/* renamed from: android.support.v7.widget.ˎ  reason: contains not printable characters */
/* compiled from: AppCompatCheckedTextView */
public class C0651 extends CheckedTextView {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final int[] f2585 = {16843016};

    /* renamed from: ʼ  reason: contains not printable characters */
    private final C0726 f2586;

    public C0651(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16843720);
    }

    public C0651(Context context, AttributeSet attributeSet, int i) {
        super(C0589.m3735(context), attributeSet, i);
        this.f2586 = C0726.m4855(this);
        this.f2586.m4864(attributeSet, i);
        this.f2586.m4858();
        C0592 r3 = C0592.m3740(getContext(), attributeSet, f2585, i, 0);
        setCheckMarkDrawable(r3.m3744(0));
        r3.m3745();
    }

    public void setCheckMarkDrawable(int i) {
        setCheckMarkDrawable(C0739.m4883(getContext(), i));
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        C0726 r0 = this.f2586;
        if (r0 != null) {
            r0.m4862(context, i);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        C0726 r0 = this.f2586;
        if (r0 != null) {
            r0.m4858();
        }
    }
}
