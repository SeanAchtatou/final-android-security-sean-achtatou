package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ʻ.C0107;
import android.support.v7.ʼ.ʻ.C0739;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

/* renamed from: android.support.v7.widget.ʻˏ  reason: contains not printable characters */
/* compiled from: TintTypedArray */
public class C0592 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Context f2324;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final TypedArray f2325;

    /* renamed from: ʽ  reason: contains not printable characters */
    private TypedValue f2326;

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0592 m3739(Context context, AttributeSet attributeSet, int[] iArr) {
        return new C0592(context, context.obtainStyledAttributes(attributeSet, iArr));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0592 m3740(Context context, AttributeSet attributeSet, int[] iArr, int i, int i2) {
        return new C0592(context, context.obtainStyledAttributes(attributeSet, iArr, i, i2));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0592 m3738(Context context, int i, int[] iArr) {
        return new C0592(context, context.obtainStyledAttributes(i, iArr));
    }

    private C0592(Context context, TypedArray typedArray) {
        this.f2324 = context;
        this.f2325 = typedArray;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Drawable m3744(int i) {
        int resourceId;
        if (!this.f2325.hasValue(i) || (resourceId = this.f2325.getResourceId(i, 0)) == 0) {
            return this.f2325.getDrawable(i);
        }
        return C0739.m4883(this.f2324, resourceId);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ˑ.ʻ(android.content.Context, int, boolean):android.graphics.drawable.Drawable
     arg types: [android.content.Context, int, int]
     candidates:
      android.support.v7.widget.ˑ.ʻ(android.content.res.ColorStateList, android.graphics.PorterDuff$Mode, int[]):android.graphics.PorterDuffColorFilter
      android.support.v7.widget.ˑ.ʻ(android.content.Context, int, android.content.res.ColorStateList):void
      android.support.v7.widget.ˑ.ʻ(android.graphics.drawable.Drawable, int, android.graphics.PorterDuff$Mode):void
      android.support.v7.widget.ˑ.ʻ(android.graphics.drawable.Drawable, android.support.v7.widget.ʻˋ, int[]):void
      android.support.v7.widget.ˑ.ʻ(android.content.Context, int, android.graphics.drawable.Drawable):boolean
      android.support.v7.widget.ˑ.ʻ(android.content.Context, long, android.graphics.drawable.Drawable):boolean
      android.support.v7.widget.ˑ.ʻ(android.content.Context, android.support.v7.widget.ʻᐧ, int):android.graphics.drawable.Drawable
      android.support.v7.widget.ˑ.ʻ(android.content.Context, int, boolean):android.graphics.drawable.Drawable */
    /* renamed from: ʼ  reason: contains not printable characters */
    public Drawable m3748(int i) {
        int resourceId;
        if (!this.f2325.hasValue(i) || (resourceId = this.f2325.getResourceId(i, 0)) == 0) {
            return null;
        }
        return C0656.m4164().m4184(this.f2324, resourceId, true);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Typeface m3743(int i, int i2, TextView textView) {
        int resourceId = this.f2325.getResourceId(i, 0);
        if (resourceId == 0) {
            return null;
        }
        if (this.f2326 == null) {
            this.f2326 = new TypedValue();
        }
        return C0107.m560(this.f2324, resourceId, this.f2326, i2, textView);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public CharSequence m3750(int i) {
        return this.f2325.getText(i);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public String m3752(int i) {
        return this.f2325.getString(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3746(int i, boolean z) {
        return this.f2325.getBoolean(i, z);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m3742(int i, int i2) {
        return this.f2325.getInt(i, i2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public float m3741(int i, float f) {
        return this.f2325.getFloat(i, f);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m3747(int i, int i2) {
        return this.f2325.getColor(i, i2);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public ColorStateList m3754(int i) {
        int resourceId;
        ColorStateList r0;
        if (!this.f2325.hasValue(i) || (resourceId = this.f2325.getResourceId(i, 0)) == 0 || (r0 = C0739.m4880(this.f2324, resourceId)) == null) {
            return this.f2325.getColorStateList(i);
        }
        return r0;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m3749(int i, int i2) {
        return this.f2325.getInteger(i, i2);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m3751(int i, int i2) {
        return this.f2325.getDimensionPixelOffset(i, i2);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m3753(int i, int i2) {
        return this.f2325.getDimensionPixelSize(i, i2);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public int m3755(int i, int i2) {
        return this.f2325.getLayoutDimension(i, i2);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public int m3757(int i, int i2) {
        return this.f2325.getResourceId(i, i2);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public CharSequence[] m3756(int i) {
        return this.f2325.getTextArray(i);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m3758(int i) {
        return this.f2325.hasValue(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3745() {
        this.f2325.recycle();
    }
}
