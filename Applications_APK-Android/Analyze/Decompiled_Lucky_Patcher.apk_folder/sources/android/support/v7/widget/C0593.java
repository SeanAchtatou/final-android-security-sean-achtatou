package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.ˉ.C0414;
import android.support.v4.ˉ.C0434;
import android.support.v4.ˉ.C0437;
import android.support.v7.view.menu.C0495;
import android.support.v7.view.menu.C0504;
import android.support.v7.view.menu.C0518;
import android.support.v7.widget.Toolbar;
import android.support.v7.ʻ.C0727;
import android.support.v7.ʼ.ʻ.C0739;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

/* renamed from: android.support.v7.widget.ʻˑ  reason: contains not printable characters */
/* compiled from: ToolbarWidgetWrapper */
public class C0593 implements C0631 {

    /* renamed from: ʻ  reason: contains not printable characters */
    Toolbar f2327;

    /* renamed from: ʼ  reason: contains not printable characters */
    CharSequence f2328;

    /* renamed from: ʽ  reason: contains not printable characters */
    Window.Callback f2329;

    /* renamed from: ʾ  reason: contains not printable characters */
    boolean f2330;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f2331;

    /* renamed from: ˆ  reason: contains not printable characters */
    private View f2332;

    /* renamed from: ˈ  reason: contains not printable characters */
    private View f2333;

    /* renamed from: ˉ  reason: contains not printable characters */
    private Drawable f2334;

    /* renamed from: ˊ  reason: contains not printable characters */
    private Drawable f2335;

    /* renamed from: ˋ  reason: contains not printable characters */
    private Drawable f2336;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f2337;

    /* renamed from: ˏ  reason: contains not printable characters */
    private CharSequence f2338;

    /* renamed from: ˑ  reason: contains not printable characters */
    private CharSequence f2339;

    /* renamed from: י  reason: contains not printable characters */
    private C0614 f2340;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f2341;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f2342;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Drawable f2343;

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3779(boolean z) {
    }

    public C0593(Toolbar toolbar, boolean z) {
        this(toolbar, z, C0727.C0735.abc_action_bar_up_description, C0727.C0732.abc_ic_ab_back_material);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.widget.Toolbar, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public C0593(Toolbar toolbar, boolean z, int i, int i2) {
        Drawable drawable;
        this.f2341 = 0;
        this.f2342 = 0;
        this.f2327 = toolbar;
        this.f2328 = toolbar.getTitle();
        this.f2338 = toolbar.getSubtitle();
        this.f2337 = this.f2328 != null;
        this.f2336 = toolbar.getNavigationIcon();
        C0592 r4 = C0592.m3740(toolbar.getContext(), null, C0727.C0737.ActionBar, C0727.C0728.actionBarStyle, 0);
        this.f2343 = r4.m3744(C0727.C0737.ActionBar_homeAsUpIndicator);
        if (z) {
            CharSequence r5 = r4.m3750(C0727.C0737.ActionBar_title);
            if (!TextUtils.isEmpty(r5)) {
                m3778(r5);
            }
            CharSequence r52 = r4.m3750(C0727.C0737.ActionBar_subtitle);
            if (!TextUtils.isEmpty(r52)) {
                m3782(r52);
            }
            Drawable r53 = r4.m3744(C0727.C0737.ActionBar_logo);
            if (r53 != null) {
                m3781(r53);
            }
            Drawable r54 = r4.m3744(C0727.C0737.ActionBar_icon);
            if (r54 != null) {
                m3767(r54);
            }
            if (this.f2336 == null && (drawable = this.f2343) != null) {
                m3777(drawable);
            }
            m3780(r4.m3742(C0727.C0737.ActionBar_displayOptions, 0));
            int r55 = r4.m3757(C0727.C0737.ActionBar_customNavigationLayout, 0);
            if (r55 != 0) {
                m3771(LayoutInflater.from(this.f2327.getContext()).inflate(r55, (ViewGroup) this.f2327, false));
                m3780(this.f2331 | 16);
            }
            int r56 = r4.m3755(C0727.C0737.ActionBar_height, 0);
            if (r56 > 0) {
                ViewGroup.LayoutParams layoutParams = this.f2327.getLayoutParams();
                layoutParams.height = r56;
                this.f2327.setLayoutParams(layoutParams);
            }
            int r57 = r4.m3751(C0727.C0737.ActionBar_contentInsetStart, -1);
            int r0 = r4.m3751(C0727.C0737.ActionBar_contentInsetEnd, -1);
            if (r57 >= 0 || r0 >= 0) {
                this.f2327.m3588(Math.max(r57, 0), Math.max(r0, 0));
            }
            int r58 = r4.m3757(C0727.C0737.ActionBar_titleTextStyle, 0);
            if (r58 != 0) {
                Toolbar toolbar2 = this.f2327;
                toolbar2.m3589(toolbar2.getContext(), r58);
            }
            int r59 = r4.m3757(C0727.C0737.ActionBar_subtitleTextStyle, 0);
            if (r59 != 0) {
                Toolbar toolbar3 = this.f2327;
                toolbar3.m3593(toolbar3.getContext(), r59);
            }
            int r510 = r4.m3757(C0727.C0737.ActionBar_popupTheme, 0);
            if (r510 != 0) {
                this.f2327.setPopupTheme(r510);
            }
        } else {
            this.f2331 = m3760();
        }
        r4.m3745();
        m3790(i);
        this.f2339 = this.f2327.getNavigationContentDescription();
        this.f2327.setNavigationOnClickListener(new View.OnClickListener() {

            /* renamed from: ʻ  reason: contains not printable characters */
            final C0495 f2344 = new C0495(C0593.this.f2327.getContext(), 0, 16908332, 0, 0, C0593.this.f2328);

            public void onClick(View view) {
                if (C0593.this.f2329 != null && C0593.this.f2330) {
                    C0593.this.f2329.onMenuItemSelected(0, this.f2344);
                }
            }
        });
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m3790(int i) {
        if (i != this.f2342) {
            this.f2342 = i;
            if (TextUtils.isEmpty(this.f2327.getNavigationContentDescription())) {
                m3785(this.f2342);
            }
        }
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    private int m3760() {
        if (this.f2327.getNavigationIcon() == null) {
            return 11;
        }
        this.f2343 = this.f2327.getNavigationIcon();
        return 15;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public ViewGroup m3765() {
        return this.f2327;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Context m3775() {
        return this.f2327.getContext();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m3783() {
        return this.f2327.m3599();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m3784() {
        this.f2327.m3600();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3772(Window.Callback callback) {
        this.f2329 = callback;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3773(CharSequence charSequence) {
        if (!this.f2337) {
            m3759(charSequence);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public CharSequence m3787() {
        return this.f2327.getTitle();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3778(CharSequence charSequence) {
        this.f2337 = true;
        m3759(charSequence);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m3759(CharSequence charSequence) {
        this.f2328 = charSequence;
        if ((this.f2331 & 8) != 0) {
            this.f2327.setTitle(charSequence);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3782(CharSequence charSequence) {
        this.f2338 = charSequence;
        if ((this.f2331 & 8) != 0) {
            this.f2327.setSubtitle(charSequence);
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m3789() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public void m3791() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3766(int i) {
        m3767(i != 0 ? C0739.m4883(m3775(), i) : null);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3767(Drawable drawable) {
        this.f2334 = drawable;
        m3761();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3776(int i) {
        m3781(i != 0 ? C0739.m4883(m3775(), i) : null);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3781(Drawable drawable) {
        this.f2335 = drawable;
        m3761();
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    private void m3761() {
        Drawable drawable;
        int i = this.f2331;
        if ((i & 2) == 0) {
            drawable = null;
        } else if ((i & 1) != 0) {
            drawable = this.f2335;
            if (drawable == null) {
                drawable = this.f2334;
            }
        } else {
            drawable = this.f2334;
        }
        this.f2327.setLogo(drawable);
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m3792() {
        return this.f2327.m3592();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean m3793() {
        return this.f2327.m3594();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean m3794() {
        return this.f2327.m3595();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean m3795() {
        return this.f2327.m3596();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public boolean m3796() {
        return this.f2327.m3597();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m3797() {
        this.f2330 = true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3770(Menu menu, C0518.C0519 r4) {
        if (this.f2340 == null) {
            this.f2340 = new C0614(this.f2327.getContext());
            this.f2340.m2799(C0727.C0733.action_menu_presenter);
        }
        this.f2340.m2803(r4);
        this.f2327.m3590((C0504) menu, this.f2340);
    }

    /* renamed from: י  reason: contains not printable characters */
    public void m3798() {
        this.f2327.m3598();
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public int m3799() {
        return this.f2331;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3780(int i) {
        View view;
        int i2 = this.f2331 ^ i;
        this.f2331 = i;
        if (i2 != 0) {
            if ((i2 & 4) != 0) {
                if ((i & 4) != 0) {
                    m3763();
                }
                m3762();
            }
            if ((i2 & 3) != 0) {
                m3761();
            }
            if ((i2 & 8) != 0) {
                if ((i & 8) != 0) {
                    this.f2327.setTitle(this.f2328);
                    this.f2327.setSubtitle(this.f2338);
                } else {
                    this.f2327.setTitle((CharSequence) null);
                    this.f2327.setSubtitle((CharSequence) null);
                }
            }
            if ((i2 & 16) != 0 && (view = this.f2333) != null) {
                if ((i & 16) != 0) {
                    this.f2327.addView(view);
                } else {
                    this.f2327.removeView(view);
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3769(C0580 r4) {
        Toolbar toolbar;
        View view = this.f2332;
        if (view != null && view.getParent() == (toolbar = this.f2327)) {
            toolbar.removeView(this.f2332);
        }
        this.f2332 = r4;
        if (r4 != null && this.f2341 == 2) {
            this.f2327.addView(this.f2332, 0);
            Toolbar.C0571 r0 = (Toolbar.C0571) this.f2332.getLayoutParams();
            r0.width = -2;
            r0.height = -2;
            r0.f1390 = 8388691;
            r4.setAllowCollapse(true);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3774(boolean z) {
        this.f2327.setCollapsible(z);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public int m3800() {
        return this.f2341;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3771(View view) {
        View view2 = this.f2333;
        if (!(view2 == null || (this.f2331 & 16) == 0)) {
            this.f2327.removeView(view2);
        }
        this.f2333 = view;
        if (view != null && (this.f2331 & 16) != 0) {
            this.f2327.addView(this.f2333);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0434 m3764(final int i, long j) {
        return C0414.m2242(this.f2327).m2376(i == 0 ? 1.0f : 0.0f).m2377(j).m2378(new C0437() {

            /* renamed from: ʽ  reason: contains not printable characters */
            private boolean f2348 = false;

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m3802(View view) {
                C0593.this.f2327.setVisibility(0);
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public void m3803(View view) {
                if (!this.f2348) {
                    C0593.this.f2327.setVisibility(i);
                }
            }

            /* renamed from: ʽ  reason: contains not printable characters */
            public void m3804(View view) {
                this.f2348 = true;
            }
        });
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3777(Drawable drawable) {
        this.f2336 = drawable;
        m3762();
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    private void m3762() {
        if ((this.f2331 & 4) != 0) {
            Toolbar toolbar = this.f2327;
            Drawable drawable = this.f2336;
            if (drawable == null) {
                drawable = this.f2343;
            }
            toolbar.setNavigationIcon(drawable);
            return;
        }
        this.f2327.setNavigationIcon((Drawable) null);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m3786(CharSequence charSequence) {
        this.f2339 = charSequence;
        m3763();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m3785(int i) {
        m3786(i == 0 ? null : m3775().getString(i));
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    private void m3763() {
        if ((this.f2331 & 4) == 0) {
            return;
        }
        if (TextUtils.isEmpty(this.f2339)) {
            this.f2327.setNavigationContentDescription(this.f2342);
        } else {
            this.f2327.setNavigationContentDescription(this.f2339);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m3788(int i) {
        this.f2327.setVisibility(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3768(C0518.C0519 r2, C0504.C0505 r3) {
        this.f2327.m3591(r2, r3);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public Menu m3801() {
        return this.f2327.getMenu();
    }
}
