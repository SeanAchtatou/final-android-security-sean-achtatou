package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Observable;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.ʿ.C0324;
import android.support.v4.ˉ.C0355;
import android.support.v4.ˉ.C0386;
import android.support.v4.ˉ.C0405;
import android.support.v4.ˉ.C0407;
import android.support.v4.ˉ.C0408;
import android.support.v4.ˉ.C0414;
import android.support.v4.ˉ.C0425;
import android.support.v4.ˉ.ʻ.C0356;
import android.support.v4.ˉ.ʻ.C0360;
import android.support.v7.widget.C0601;
import android.support.v7.widget.C0604;
import android.support.v7.widget.C0611;
import android.support.v7.widget.C0632;
import android.support.v7.widget.C0644;
import android.support.v7.ʾ.C0744;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.FocusFinder;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.OverScroller;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.lingala.zip4j.util.InternalZipConstants;
import org.tukaani.xz.common.Util;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

/* renamed from: android.support.v7.widget.ﹳﹳ  reason: contains not printable characters */
/* compiled from: RecyclerView */
public class C0690 extends ViewGroup implements C0407 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final boolean f2731 = (Build.VERSION.SDK_INT == 18 || Build.VERSION.SDK_INT == 19 || Build.VERSION.SDK_INT == 20);

    /* renamed from: ʼ  reason: contains not printable characters */
    static final boolean f2732 = (Build.VERSION.SDK_INT >= 23);

    /* renamed from: ʽ  reason: contains not printable characters */
    static final boolean f2733 = (Build.VERSION.SDK_INT >= 16);

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private static final int[] f2734 = {16843830};
    /* access modifiers changed from: private */

    /* renamed from: ˈˈ  reason: contains not printable characters */
    public static final boolean f2735 = (Build.VERSION.SDK_INT >= 21);

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private static final int[] f2736 = {16842987};

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private static final boolean f2737 = (Build.VERSION.SDK_INT <= 15);

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private static final boolean f2738 = (Build.VERSION.SDK_INT <= 15);

    /* renamed from: ˏˏ  reason: contains not printable characters */
    private static final Class<?>[] f2739 = {Context.class, AttributeSet.class, Integer.TYPE, Integer.TYPE};

    /* renamed from: ــ  reason: contains not printable characters */
    static final Interpolator f2740 = new Interpolator() {
        public float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (f2 * f2 * f2 * f2 * f2) + 1.0f;
        }
    };

    /* renamed from: ʻʻ  reason: contains not printable characters */
    boolean f2741;

    /* renamed from: ʻʼ  reason: contains not printable characters */
    private int f2742;

    /* renamed from: ʻʽ  reason: contains not printable characters */
    private int f2743;

    /* renamed from: ʻʾ  reason: contains not printable characters */
    private EdgeEffect f2744;

    /* renamed from: ʻʿ  reason: contains not printable characters */
    private EdgeEffect f2745;

    /* renamed from: ʻˆ  reason: contains not printable characters */
    private EdgeEffect f2746;

    /* renamed from: ʻˈ  reason: contains not printable characters */
    private EdgeEffect f2747;

    /* renamed from: ʻˉ  reason: contains not printable characters */
    private int f2748;

    /* renamed from: ʻˊ  reason: contains not printable characters */
    private int f2749;

    /* renamed from: ʻˋ  reason: contains not printable characters */
    private VelocityTracker f2750;

    /* renamed from: ʻˎ  reason: contains not printable characters */
    private int f2751;

    /* renamed from: ʻˏ  reason: contains not printable characters */
    private int f2752;

    /* renamed from: ʻˑ  reason: contains not printable characters */
    private int f2753;

    /* renamed from: ʻי  reason: contains not printable characters */
    private int f2754;

    /* renamed from: ʻـ  reason: contains not printable characters */
    private int f2755;

    /* renamed from: ʻٴ  reason: contains not printable characters */
    private C0706 f2756;

    /* renamed from: ʻᐧ  reason: contains not printable characters */
    private final int f2757;

    /* renamed from: ʻᴵ  reason: contains not printable characters */
    private final int f2758;

    /* renamed from: ʻᵎ  reason: contains not printable characters */
    private float f2759;

    /* renamed from: ʻᵔ  reason: contains not printable characters */
    private float f2760;

    /* renamed from: ʻᵢ  reason: contains not printable characters */
    private boolean f2761;

    /* renamed from: ʻⁱ  reason: contains not printable characters */
    private C0708 f2762;

    /* renamed from: ʻﹳ  reason: contains not printable characters */
    private List<C0708> f2763;

    /* renamed from: ʻﹶ  reason: contains not printable characters */
    private C0695.C0697 f2764;

    /* renamed from: ʻﾞ  reason: contains not printable characters */
    private C0694 f2765;

    /* renamed from: ʼʻ  reason: contains not printable characters */
    private final int[] f2766;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    boolean f2767;

    /* renamed from: ʼʽ  reason: contains not printable characters */
    private C0408 f2768;

    /* renamed from: ʼʾ  reason: contains not printable characters */
    private final int[] f2769;
    /* access modifiers changed from: private */

    /* renamed from: ʼʿ  reason: contains not printable characters */
    public final int[] f2770;

    /* renamed from: ʼˆ  reason: contains not printable characters */
    private final int[] f2771;

    /* renamed from: ʼˈ  reason: contains not printable characters */
    private Runnable f2772;

    /* renamed from: ʼˉ  reason: contains not printable characters */
    private final C0604.C0606 f2773;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    boolean f2774;

    /* renamed from: ʾ  reason: contains not printable characters */
    final C0711 f2775;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    final List<C0720> f2776;

    /* renamed from: ʿ  reason: contains not printable characters */
    C0632 f2777;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    C0672 f2778;

    /* renamed from: ˆ  reason: contains not printable characters */
    C0611 f2779;

    /* renamed from: ˈ  reason: contains not printable characters */
    final C0604 f2780;

    /* renamed from: ˉ  reason: contains not printable characters */
    boolean f2781;

    /* renamed from: ˊ  reason: contains not printable characters */
    final Runnable f2782;

    /* renamed from: ˋ  reason: contains not printable characters */
    final Rect f2783;

    /* renamed from: ˎ  reason: contains not printable characters */
    final RectF f2784;

    /* renamed from: ˎˎ  reason: contains not printable characters */
    private final C0713 f2785;

    /* renamed from: ˏ  reason: contains not printable characters */
    C0691 f2786;

    /* renamed from: ˑ  reason: contains not printable characters */
    C0701 f2787;

    /* renamed from: ˑˑ  reason: contains not printable characters */
    private C0714 f2788;

    /* renamed from: י  reason: contains not printable characters */
    C0712 f2789;

    /* renamed from: יי  reason: contains not printable characters */
    private final ArrayList<C0707> f2790;

    /* renamed from: ـ  reason: contains not printable characters */
    final ArrayList<C0700> f2791;

    /* renamed from: ٴ  reason: contains not printable characters */
    boolean f2792;

    /* renamed from: ٴٴ  reason: contains not printable characters */
    private final AccessibilityManager f2793;

    /* renamed from: ᐧ  reason: contains not printable characters */
    boolean f2794;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    C0644.C0645 f2795;

    /* renamed from: ᴵ  reason: contains not printable characters */
    boolean f2796;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    final C0717 f2797;

    /* renamed from: ᵎ  reason: contains not printable characters */
    boolean f2798;

    /* renamed from: ᵎᵎ  reason: contains not printable characters */
    private C0707 f2799;

    /* renamed from: ᵔ  reason: contains not printable characters */
    boolean f2800;

    /* renamed from: ᵔᵔ  reason: contains not printable characters */
    private final Rect f2801;

    /* renamed from: ᵢ  reason: contains not printable characters */
    boolean f2802;

    /* renamed from: ᵢᵢ  reason: contains not printable characters */
    private int f2803;

    /* renamed from: ⁱ  reason: contains not printable characters */
    boolean f2804;

    /* renamed from: ⁱⁱ  reason: contains not printable characters */
    private boolean f2805;

    /* renamed from: ﹳ  reason: contains not printable characters */
    boolean f2806;

    /* renamed from: ﹳﹳ  reason: contains not printable characters */
    private int f2807;

    /* renamed from: ﹶ  reason: contains not printable characters */
    C0695 f2808;

    /* renamed from: ﹶﹶ  reason: contains not printable characters */
    private List<C0705> f2809;

    /* renamed from: ﾞ  reason: contains not printable characters */
    final C0719 f2810;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    C0644 f2811;

    /* renamed from: android.support.v7.widget.ﹳﹳ$ʽ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public static abstract class C0693 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4499() {
        }
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ʾ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public interface C0694 {
        /* renamed from: ʻ  reason: contains not printable characters */
        int m4500(int i, int i2);
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ˋ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public interface C0705 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m4688(View view);

        /* renamed from: ʼ  reason: contains not printable characters */
        void m4689(View view);
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ˎ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public static abstract class C0706 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract boolean m4690(int i, int i2);
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ˏ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public interface C0707 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m4691(boolean z);

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m4692(C0690 r1, MotionEvent motionEvent);

        /* renamed from: ʼ  reason: contains not printable characters */
        void m4693(C0690 r1, MotionEvent motionEvent);
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ˑ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public static abstract class C0708 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4694(C0690 r1, int i) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4695(C0690 r1, int i, int i2) {
        }
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ٴ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public interface C0712 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m4747(C0720 r1);
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ᵢ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public static abstract class C0718 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract View m4776(C0711 r1, int i, int i2);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m4420(int i) {
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public void m4429(View view) {
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public void m4431(int i, int i2) {
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public void m4432(View view) {
    }

    public C0690(Context context) {
        this(context, null);
    }

    public C0690(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ˉ.ᐧ.ʻ(android.view.View, int):void
     arg types: [android.support.v7.widget.ﹳﹳ, int]
     candidates:
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.support.v4.ˉ.ﾞ):android.support.v4.ˉ.ﾞ
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, float):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.content.res.ColorStateList):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.graphics.PorterDuff$Mode):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.graphics.drawable.Drawable):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.support.v4.ˉ.ʼ):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.support.v4.ˉ.ـ):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, java.lang.Runnable):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, boolean):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, int):void */
    public C0690(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2785 = new C0713();
        this.f2775 = new C0711();
        this.f2780 = new C0604();
        this.f2782 = new Runnable() {
            public void run() {
                if (C0690.this.f2798 && !C0690.this.isLayoutRequested()) {
                    if (!C0690.this.f2792) {
                        C0690.this.requestLayout();
                    } else if (C0690.this.f2802) {
                        C0690.this.f2800 = true;
                    } else {
                        C0690.this.m4416();
                    }
                }
            }
        };
        this.f2783 = new Rect();
        this.f2801 = new Rect();
        this.f2784 = new RectF();
        this.f2791 = new ArrayList<>();
        this.f2790 = new ArrayList<>();
        this.f2803 = 0;
        this.f2806 = false;
        this.f2742 = 0;
        this.f2743 = 0;
        this.f2808 = new C0622();
        this.f2748 = 0;
        this.f2749 = -1;
        this.f2759 = Float.MIN_VALUE;
        this.f2760 = Float.MIN_VALUE;
        boolean z = true;
        this.f2761 = true;
        this.f2810 = new C0719();
        this.f2795 = f2735 ? new C0644.C0645() : null;
        this.f2797 = new C0717();
        this.f2741 = false;
        this.f2774 = false;
        this.f2764 = new C0699();
        this.f2767 = false;
        this.f2766 = new int[2];
        this.f2769 = new int[2];
        this.f2770 = new int[2];
        this.f2771 = new int[2];
        this.f2776 = new ArrayList();
        this.f2772 = new Runnable() {
            public void run() {
                if (C0690.this.f2808 != null) {
                    C0690.this.f2808.m4504();
                }
                C0690.this.f2767 = false;
            }
        };
        this.f2773 = new C0604.C0606() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public void m4457(C0720 r2, C0695.C0698 r3, C0695.C0698 r4) {
                C0690.this.f2775.m4734(r2);
                C0690.this.m4406(r2, r3, r4);
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public void m4458(C0720 r2, C0695.C0698 r3, C0695.C0698 r4) {
                C0690.this.m4391(r2, r3, r4);
            }

            /* renamed from: ʽ  reason: contains not printable characters */
            public void m4459(C0720 r2, C0695.C0698 r3, C0695.C0698 r4) {
                r2.m4809(false);
                if (C0690.this.f2806) {
                    if (C0690.this.f2808.m4507(r2, r2, r3, r4)) {
                        C0690.this.m4446();
                    }
                } else if (C0690.this.f2808.m4511(r2, r3, r4)) {
                    C0690.this.m4446();
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m4456(C0720 r3) {
                C0690.this.f2787.m4576(r3.f2914, C0690.this.f2775);
            }
        };
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, f2736, i, 0);
            this.f2781 = obtainStyledAttributes.getBoolean(0, true);
            obtainStyledAttributes.recycle();
        } else {
            this.f2781 = true;
        }
        setScrollContainer(true);
        setFocusableInTouchMode(true);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.f2755 = viewConfiguration.getScaledTouchSlop();
        this.f2759 = C0425.m2349(viewConfiguration, context);
        this.f2760 = C0425.m2350(viewConfiguration, context);
        this.f2757 = viewConfiguration.getScaledMinimumFlingVelocity();
        this.f2758 = viewConfiguration.getScaledMaximumFlingVelocity();
        setWillNotDraw(getOverScrollMode() == 2);
        this.f2808.m4505(this.f2764);
        m4402();
        m4372();
        if (C0414.m2235(this) == 0) {
            C0414.m2218((View) this, 1);
        }
        this.f2793 = (AccessibilityManager) getContext().getSystemService("accessibility");
        setAccessibilityDelegateCompat(new C0672(this));
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, C0744.C0746.RecyclerView, i, 0);
            String string = obtainStyledAttributes2.getString(C0744.C0746.RecyclerView_layoutManager);
            if (obtainStyledAttributes2.getInt(C0744.C0746.RecyclerView_android_descendantFocusability, -1) == -1) {
                setDescendantFocusability(262144);
            }
            this.f2796 = obtainStyledAttributes2.getBoolean(C0744.C0746.RecyclerView_fastScrollEnabled, false);
            if (this.f2796) {
                m4384((StateListDrawable) obtainStyledAttributes2.getDrawable(C0744.C0746.RecyclerView_fastScrollVerticalThumbDrawable), obtainStyledAttributes2.getDrawable(C0744.C0746.RecyclerView_fastScrollVerticalTrackDrawable), (StateListDrawable) obtainStyledAttributes2.getDrawable(C0744.C0746.RecyclerView_fastScrollHorizontalThumbDrawable), obtainStyledAttributes2.getDrawable(C0744.C0746.RecyclerView_fastScrollHorizontalTrackDrawable));
            }
            obtainStyledAttributes2.recycle();
            m4336(context, string, attributeSet, i, 0);
            if (Build.VERSION.SDK_INT >= 21) {
                TypedArray obtainStyledAttributes3 = context.obtainStyledAttributes(attributeSet, f2734, i, 0);
                boolean z2 = obtainStyledAttributes3.getBoolean(0, true);
                obtainStyledAttributes3.recycle();
                z = z2;
            }
        } else {
            setDescendantFocusability(262144);
        }
        setNestedScrollingEnabled(z);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public String m4378() {
        return " " + super.toString() + ", adapter:" + this.f2786 + ", layout:" + this.f2787 + ", context:" + getContext();
    }

    public C0672 getCompatAccessibilityDelegate() {
        return this.f2778;
    }

    public void setAccessibilityDelegateCompat(C0672 r1) {
        this.f2778 = r1;
        C0414.m2224(this, this.f2778);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4336(Context context, String str, AttributeSet attributeSet, int i, int i2) {
        ClassLoader classLoader;
        Constructor<? extends U> constructor;
        if (str != null) {
            String trim = str.trim();
            if (!trim.isEmpty()) {
                String r9 = m4333(context, trim);
                try {
                    if (isInEditMode()) {
                        classLoader = getClass().getClassLoader();
                    } else {
                        classLoader = context.getClassLoader();
                    }
                    Class<? extends U> asSubclass = classLoader.loadClass(r9).asSubclass(C0701.class);
                    Object[] objArr = null;
                    try {
                        constructor = asSubclass.getConstructor(f2739);
                        objArr = new Object[]{context, attributeSet, Integer.valueOf(i), Integer.valueOf(i2)};
                    } catch (NoSuchMethodException e) {
                        constructor = asSubclass.getConstructor(new Class[0]);
                    }
                    constructor.setAccessible(true);
                    setLayoutManager((C0701) constructor.newInstance(objArr));
                } catch (NoSuchMethodException e2) {
                    e2.initCause(e);
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Error creating LayoutManager " + r9, e2);
                } catch (ClassNotFoundException e3) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Unable to find LayoutManager " + r9, e3);
                } catch (InvocationTargetException e4) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Could not instantiate the LayoutManager: " + r9, e4);
                } catch (InstantiationException e5) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Could not instantiate the LayoutManager: " + r9, e5);
                } catch (IllegalAccessException e6) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Cannot access non-public constructor " + r9, e6);
                } catch (ClassCastException e7) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Class is not a LayoutManager " + r9, e7);
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private String m4333(Context context, String str) {
        if (str.charAt(0) == '.') {
            return context.getPackageName() + str;
        } else if (str.contains(".")) {
            return str;
        } else {
            return C0690.class.getPackage().getName() + '.' + str;
        }
    }

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private void m4372() {
        this.f2779 = new C0611(new C0611.C0613() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public int m4460() {
                return C0690.this.getChildCount();
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m4463(View view, int i) {
                C0690.this.addView(view, i);
                C0690.this.m4442(view);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public int m4461(View view) {
                return C0690.this.indexOfChild(view);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m4462(int i) {
                View childAt = C0690.this.getChildAt(i);
                if (childAt != null) {
                    C0690.this.m4440(childAt);
                    childAt.clearAnimation();
                }
                C0690.this.removeViewAt(i);
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public View m4466(int i) {
                return C0690.this.getChildAt(i);
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public void m4467() {
                int r0 = m4460();
                for (int i = 0; i < r0; i++) {
                    View r2 = m4466(i);
                    C0690.this.m4440(r2);
                    r2.clearAnimation();
                }
                C0690.this.removeAllViews();
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public C0720 m4465(View view) {
                return C0690.m4357(view);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m4464(View view, int i, ViewGroup.LayoutParams layoutParams) {
                C0720 r0 = C0690.m4357(view);
                if (r0 != null) {
                    if (r0.m4828() || r0.m4813()) {
                        r0.m4823();
                    } else {
                        throw new IllegalArgumentException("Called attach on a child which is not detached: " + r0 + C0690.this.m4378());
                    }
                }
                C0690.this.attachViewToParent(view, i, layoutParams);
            }

            /* renamed from: ʽ  reason: contains not printable characters */
            public void m4468(int i) {
                C0720 r0;
                View r02 = m4466(i);
                if (!(r02 == null || (r0 = C0690.m4357(r02)) == null)) {
                    if (!r0.m4828() || r0.m4813()) {
                        r0.m4812(256);
                    } else {
                        throw new IllegalArgumentException("called detach on an already detached child " + r0 + C0690.this.m4378());
                    }
                }
                C0690.this.detachViewFromParent(i);
            }

            /* renamed from: ʽ  reason: contains not printable characters */
            public void m4469(View view) {
                C0720 r2 = C0690.m4357(view);
                if (r2 != null) {
                    r2.m4792(C0690.this);
                }
            }

            /* renamed from: ʾ  reason: contains not printable characters */
            public void m4470(View view) {
                C0720 r2 = C0690.m4357(view);
                if (r2 != null) {
                    r2.m4796(C0690.this);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4402() {
        this.f2777 = new C0632(new C0632.C0633() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v7.widget.ﹳﹳ.ʻ(int, boolean):android.support.v7.widget.ﹳﹳ$ﹳ
             arg types: [int, int]
             candidates:
              android.support.v7.widget.ﹳﹳ.ʻ(android.content.Context, java.lang.String):java.lang.String
              android.support.v7.widget.ﹳﹳ.ʻ(android.support.v7.widget.ﹳﹳ, int):void
              android.support.v7.widget.ﹳﹳ.ʻ(android.view.View, android.graphics.Rect):void
              android.support.v7.widget.ﹳﹳ.ʻ(android.view.View, android.view.View):void
              android.support.v7.widget.ﹳﹳ.ʻ(int, int):void
              android.support.v7.widget.ﹳﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ˈ, int):void
              android.support.v7.widget.ﹳﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ$ʿ$ʽ):void
              android.support.v7.widget.ﹳﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, int):boolean
              android.support.v7.widget.ﹳﹳ.ʻ(int, boolean):android.support.v7.widget.ﹳﹳ$ﹳ */
            /* renamed from: ʻ  reason: contains not printable characters */
            public C0720 m4471(int i) {
                C0720 r4 = C0690.this.m4376(i, true);
                if (r4 != null && !C0690.this.f2779.m3883(r4.f2914)) {
                    return r4;
                }
                return null;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v7.widget.ﹳﹳ.ʻ(int, int, boolean):void
             arg types: [int, int, int]
             candidates:
              android.support.v7.widget.ﹳﹳ.ʻ(long, android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ$ﹳ):void
              android.support.v7.widget.ﹳﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ʻ, boolean, boolean):void
              android.support.v7.widget.ﹳﹳ.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
              android.support.v7.widget.ﹳﹳ.ʻ(android.view.View, android.view.View, int):boolean
              android.support.v7.widget.ﹳﹳ.ʻ(int, int, android.view.animation.Interpolator):void
              android.support.v7.widget.ﹳﹳ.ʻ(int, int, java.lang.Object):void
              android.support.v7.widget.ﹳﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ$ʿ$ʽ, android.support.v7.widget.ﹳﹳ$ʿ$ʽ):void
              android.support.v7.widget.ﹳﹳ.ʻ(int, int, android.view.MotionEvent):boolean
              android.support.v7.widget.ﹳﹳ.ʻ(int, int, boolean):void */
            /* renamed from: ʻ  reason: contains not printable characters */
            public void m4472(int i, int i2) {
                C0690.this.m4383(i, i2, true);
                C0690 r3 = C0690.this;
                r3.f2741 = true;
                r3.f2797.f2890 += i2;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v7.widget.ﹳﹳ.ʻ(int, int, boolean):void
             arg types: [int, int, int]
             candidates:
              android.support.v7.widget.ﹳﹳ.ʻ(long, android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ$ﹳ):void
              android.support.v7.widget.ﹳﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ʻ, boolean, boolean):void
              android.support.v7.widget.ﹳﹳ.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
              android.support.v7.widget.ﹳﹳ.ʻ(android.view.View, android.view.View, int):boolean
              android.support.v7.widget.ﹳﹳ.ʻ(int, int, android.view.animation.Interpolator):void
              android.support.v7.widget.ﹳﹳ.ʻ(int, int, java.lang.Object):void
              android.support.v7.widget.ﹳﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ$ʿ$ʽ, android.support.v7.widget.ﹳﹳ$ʿ$ʽ):void
              android.support.v7.widget.ﹳﹳ.ʻ(int, int, android.view.MotionEvent):boolean
              android.support.v7.widget.ﹳﹳ.ʻ(int, int, boolean):void */
            /* renamed from: ʼ  reason: contains not printable characters */
            public void m4475(int i, int i2) {
                C0690.this.m4383(i, i2, false);
                C0690.this.f2741 = true;
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m4473(int i, int i2, Object obj) {
                C0690.this.m4382(i, i2, obj);
                C0690.this.f2774 = true;
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m4474(C0632.C0634 r1) {
                m4478(r1);
            }

            /* access modifiers changed from: package-private */
            /* renamed from: ʽ  reason: contains not printable characters */
            public void m4478(C0632.C0634 r5) {
                int i = r5.f2502;
                if (i == 1) {
                    C0690.this.f2787.m4565(C0690.this, r5.f2503, r5.f2505);
                } else if (i == 2) {
                    C0690.this.f2787.m4598(C0690.this, r5.f2503, r5.f2505);
                } else if (i == 4) {
                    C0690.this.f2787.m4567(C0690.this, r5.f2503, r5.f2505, r5.f2504);
                } else if (i == 8) {
                    C0690.this.f2787.m4566(C0690.this, r5.f2503, r5.f2505, 1);
                }
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public void m4476(C0632.C0634 r1) {
                m4478(r1);
            }

            /* renamed from: ʽ  reason: contains not printable characters */
            public void m4477(int i, int i2) {
                C0690.this.m4428(i, i2);
                C0690.this.f2741 = true;
            }

            /* renamed from: ʾ  reason: contains not printable characters */
            public void m4479(int i, int i2) {
                C0690.this.m4425(i, i2);
                C0690.this.f2741 = true;
            }
        });
    }

    public void setHasFixedSize(boolean z) {
        this.f2794 = z;
    }

    public void setClipToPadding(boolean z) {
        if (z != this.f2781) {
            m4439();
        }
        this.f2781 = z;
        super.setClipToPadding(z);
        if (this.f2798) {
            requestLayout();
        }
    }

    public boolean getClipToPadding() {
        return this.f2781;
    }

    public void setScrollingTouchSlop(int i) {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        if (i != 0) {
            if (i != 1) {
                Log.w("RecyclerView", "setScrollingTouchSlop(): bad argument constant " + i + "; using default value");
            } else {
                this.f2755 = viewConfiguration.getScaledPagingTouchSlop();
                return;
            }
        }
        this.f2755 = viewConfiguration.getScaledTouchSlop();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ﹳﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ʻ, boolean, boolean):void
     arg types: [android.support.v7.widget.ﹳﹳ$ʻ, int, int]
     candidates:
      android.support.v7.widget.ﹳﹳ.ʻ(long, android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ$ﹳ):void
      android.support.v7.widget.ﹳﹳ.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
      android.support.v7.widget.ﹳﹳ.ʻ(android.view.View, android.view.View, int):boolean
      android.support.v7.widget.ﹳﹳ.ʻ(int, int, android.view.animation.Interpolator):void
      android.support.v7.widget.ﹳﹳ.ʻ(int, int, java.lang.Object):void
      android.support.v7.widget.ﹳﹳ.ʻ(int, int, boolean):void
      android.support.v7.widget.ﹳﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ$ʿ$ʽ, android.support.v7.widget.ﹳﹳ$ʿ$ʽ):void
      android.support.v7.widget.ﹳﹳ.ʻ(int, int, android.view.MotionEvent):boolean
      android.support.v7.widget.ﹳﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ʻ, boolean, boolean):void */
    public void setAdapter(C0691 r3) {
        setLayoutFrozen(false);
        m4337(r3, false, true);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m4411() {
        C0695 r0 = this.f2808;
        if (r0 != null) {
            r0.m4512();
        }
        C0701 r02 = this.f2787;
        if (r02 != null) {
            r02.m4610(this.f2775);
            this.f2787.m4596(this.f2775);
        }
        this.f2775.m4715();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4337(C0691 r3, boolean z, boolean z2) {
        C0691 r0 = this.f2786;
        if (r0 != null) {
            r0.m4489(this.f2785);
            this.f2786.m4491(this);
        }
        if (!z || z2) {
            m4411();
        }
        this.f2777.m4031();
        C0691 r5 = this.f2786;
        this.f2786 = r3;
        if (r3 != null) {
            r3.m4482(this.f2785);
            r3.m4486(this);
        }
        C0701 r32 = this.f2787;
        if (r32 != null) {
            r32.m4557(r5, this.f2786);
        }
        this.f2775.m4719(r5, this.f2786, z);
        this.f2797.f2893 = true;
        m4451();
    }

    public C0691 getAdapter() {
        return this.f2786;
    }

    public void setRecyclerListener(C0712 r1) {
        this.f2789 = r1;
    }

    public int getBaseline() {
        C0701 r0 = this.f2787;
        if (r0 != null) {
            return r0.m4666();
        }
        return super.getBaseline();
    }

    public void setLayoutManager(C0701 r4) {
        if (r4 != this.f2787) {
            m4423();
            if (this.f2787 != null) {
                C0695 r0 = this.f2808;
                if (r0 != null) {
                    r0.m4512();
                }
                this.f2787.m4610(this.f2775);
                this.f2787.m4596(this.f2775);
                this.f2775.m4715();
                if (this.f2792) {
                    this.f2787.m4599(this, this.f2775);
                }
                this.f2787.m4597((C0690) null);
                this.f2787 = null;
            } else {
                this.f2775.m4715();
            }
            this.f2779.m3872();
            this.f2787 = r4;
            if (r4 != null) {
                if (r4.f2839 == null) {
                    this.f2787.m4597(this);
                    if (this.f2792) {
                        this.f2787.m4612(this);
                    }
                } else {
                    throw new IllegalArgumentException("LayoutManager " + r4 + " is already attached to a RecyclerView:" + r4.f2839.m4378());
                }
            }
            this.f2775.m4727();
            requestLayout();
        }
    }

    public void setOnFlingListener(C0706 r1) {
        this.f2756 = r1;
    }

    public C0706 getOnFlingListener() {
        return this.f2756;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        C0714 r0 = new C0714(super.onSaveInstanceState());
        C0714 r1 = this.f2788;
        if (r1 != null) {
            r0.m4749(r1);
        } else {
            C0701 r12 = this.f2787;
            if (r12 != null) {
                r0.f2874 = r12.m4607();
            } else {
                r0.f2874 = null;
            }
        }
        return r0;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof C0714)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        this.f2788 = (C0714) parcelable;
        super.onRestoreInstanceState(this.f2788.m1995());
        if (this.f2787 != null && this.f2788.f2874 != null) {
            this.f2787.m4555(this.f2788.f2874);
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchSaveInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }

    /* access modifiers changed from: protected */
    public void dispatchRestoreInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m4358(C0720 r6) {
        View view = r6.f2914;
        boolean z = view.getParent() == this;
        this.f2775.m4734(m4401(view));
        if (r6.m4828()) {
            this.f2779.m3875(view, -1, view.getLayoutParams(), true);
        } else if (!z) {
            this.f2779.m3877(view, true);
        } else {
            this.f2779.m3885(view);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m4398(View view) {
        m4419();
        boolean r0 = this.f2779.m3888(view);
        if (r0) {
            C0720 r3 = m4357(view);
            this.f2775.m4734(r3);
            this.f2775.m4729(r3);
        }
        m4393(!r0);
        return r0;
    }

    public C0701 getLayoutManager() {
        return this.f2787;
    }

    public C0709 getRecycledViewPool() {
        return this.f2775.m4743();
    }

    public void setRecycledViewPool(C0709 r2) {
        this.f2775.m4720(r2);
    }

    public void setViewCacheExtension(C0718 r2) {
        this.f2775.m4721(r2);
    }

    public void setItemViewCacheSize(int i) {
        this.f2775.m4716(i);
    }

    public int getScrollState() {
        return this.f2748;
    }

    /* access modifiers changed from: package-private */
    public void setScrollState(int i) {
        if (i != this.f2748) {
            this.f2748 = i;
            if (i != 2) {
                m4348();
            }
            m4424(i);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4386(C0700 r3, int i) {
        C0701 r0 = this.f2787;
        if (r0 != null) {
            r0.m4579("Cannot add item decoration during a scroll  or layout");
        }
        if (this.f2791.isEmpty()) {
            setWillNotDraw(false);
        }
        if (i < 0) {
            this.f2791.add(r3);
        } else {
            this.f2791.add(i, r3);
        }
        m4448();
        requestLayout();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4385(C0700 r2) {
        m4386(r2, -1);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4403(C0700 r3) {
        C0701 r0 = this.f2787;
        if (r0 != null) {
            r0.m4579("Cannot remove item decoration during a scroll  or layout");
        }
        this.f2791.remove(r3);
        if (this.f2791.isEmpty()) {
            setWillNotDraw(getOverScrollMode() == 2);
        }
        m4448();
        requestLayout();
    }

    public void setChildDrawingOrderCallback(C0694 r2) {
        if (r2 != this.f2765) {
            this.f2765 = r2;
            setChildrenDrawingOrderEnabled(this.f2765 != null);
        }
    }

    @Deprecated
    public void setOnScrollListener(C0708 r1) {
        this.f2762 = r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4388(C0708 r2) {
        if (this.f2763 == null) {
            this.f2763 = new ArrayList();
        }
        this.f2763.add(r2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4405(C0708 r2) {
        List<C0708> list = this.f2763;
        if (list != null) {
            list.remove(r2);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4379(int i) {
        C0701 r0 = this.f2787;
        if (r0 != null) {
            r0.m4622(i);
            awakenScrollBars();
        }
    }

    public void scrollTo(int i, int i2) {
        Log.w("RecyclerView", "RecyclerView does not support scrolling to an absolute position. Use scrollToPosition instead");
    }

    public void scrollBy(int i, int i2) {
        C0701 r0 = this.f2787;
        if (r0 == null) {
            Log.e("RecyclerView", "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.f2802) {
            boolean r02 = r0.m4625();
            boolean r1 = this.f2787.m4631();
            if (r02 || r1) {
                if (!r02) {
                    i = 0;
                }
                if (!r1) {
                    i2 = 0;
                }
                m4395(i, i2, (MotionEvent) null);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m4416() {
        if (!this.f2798 || this.f2806) {
            C0324.m1842("RV FullInvalidate");
            m4447();
            C0324.m1841();
        } else if (this.f2777.m4040()) {
            if (this.f2777.m4035(4) && !this.f2777.m4035(11)) {
                C0324.m1842("RV PartialInvalidate");
                m4419();
                m4441();
                this.f2777.m4037();
                if (!this.f2800) {
                    if (m4373()) {
                        m4447();
                    } else {
                        this.f2777.m4039();
                    }
                }
                m4393(true);
                m4443();
                C0324.m1841();
            } else if (this.f2777.m4040()) {
                C0324.m1842("RV FullInvalidate");
                m4447();
                C0324.m1841();
            }
        }
    }

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private boolean m4373() {
        int r0 = this.f2779.m3878();
        for (int i = 0; i < r0; i++) {
            C0720 r3 = m4357(this.f2779.m3880(i));
            if (r3 != null && !r3.m4813() && r3.m4834()) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m4395(int i, int i2, MotionEvent motionEvent) {
        int i3;
        int i4;
        int i5;
        int i6;
        m4416();
        if (this.f2786 != null) {
            m4419();
            m4441();
            C0324.m1842("RV Scroll");
            m4389(this.f2797);
            if (i != 0) {
                i6 = this.f2787.m4545(i, this.f2775, this.f2797);
                i5 = i - i6;
            } else {
                i6 = 0;
                i5 = 0;
            }
            if (i2 != 0) {
                i4 = this.f2787.m4594(i2, this.f2775, this.f2797);
                i3 = i2 - i4;
            } else {
                i4 = 0;
                i3 = 0;
            }
            C0324.m1841();
            m4454();
            m4443();
            m4393(false);
        } else {
            i6 = 0;
            i5 = 0;
            i4 = 0;
            i3 = 0;
        }
        if (!this.f2791.isEmpty()) {
            invalidate();
        }
        if (m4394(i6, i4, i5, i3, this.f2769, 0)) {
            int i7 = this.f2753;
            int[] iArr = this.f2769;
            this.f2753 = i7 - iArr[0];
            this.f2754 -= iArr[1];
            if (motionEvent != null) {
                motionEvent.offsetLocation((float) iArr[0], (float) iArr[1]);
            }
            int[] iArr2 = this.f2771;
            int i8 = iArr2[0];
            int[] iArr3 = this.f2769;
            iArr2[0] = i8 + iArr3[0];
            iArr2[1] = iArr2[1] + iArr3[1];
        } else if (getOverScrollMode() != 2) {
            if (motionEvent != null && !C0405.m2188(motionEvent, 8194)) {
                m4334(motionEvent.getX(), (float) i5, motionEvent.getY(), (float) i3);
            }
            m4413(i, i2);
        }
        if (!(i6 == 0 && i4 == 0)) {
            m4436(i6, i4);
        }
        if (!awakenScrollBars()) {
            invalidate();
        }
        if (i6 == 0 && i4 == 0) {
            return false;
        }
        return true;
    }

    public int computeHorizontalScrollOffset() {
        C0701 r0 = this.f2787;
        if (r0 != null && r0.m4625()) {
            return this.f2787.m4606(this.f2797);
        }
        return 0;
    }

    public int computeHorizontalScrollExtent() {
        C0701 r0 = this.f2787;
        if (r0 != null && r0.m4625()) {
            return this.f2787.m4627(this.f2797);
        }
        return 0;
    }

    public int computeHorizontalScrollRange() {
        C0701 r0 = this.f2787;
        if (r0 != null && r0.m4625()) {
            return this.f2787.m4640(this.f2797);
        }
        return 0;
    }

    public int computeVerticalScrollOffset() {
        C0701 r0 = this.f2787;
        if (r0 != null && r0.m4631()) {
            return this.f2787.m4619(this.f2797);
        }
        return 0;
    }

    public int computeVerticalScrollExtent() {
        C0701 r0 = this.f2787;
        if (r0 != null && r0.m4631()) {
            return this.f2787.m4634(this.f2797);
        }
        return 0;
    }

    public int computeVerticalScrollRange() {
        C0701 r0 = this.f2787;
        if (r0 != null && r0.m4631()) {
            return this.f2787.m4643(this.f2797);
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m4419() {
        this.f2803++;
        if (this.f2803 == 1 && !this.f2802) {
            this.f2800 = false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4393(boolean z) {
        if (this.f2803 < 1) {
            this.f2803 = 1;
        }
        if (!z) {
            this.f2800 = false;
        }
        if (this.f2803 == 1) {
            if (z && this.f2800 && !this.f2802 && this.f2787 != null && this.f2786 != null) {
                m4447();
            }
            if (!this.f2802) {
                this.f2800 = false;
            }
        }
        this.f2803--;
    }

    public void setLayoutFrozen(boolean z) {
        if (z != this.f2802) {
            m4392("Do not setLayoutFrozen in layout or scroll");
            if (!z) {
                this.f2802 = false;
                if (!(!this.f2800 || this.f2787 == null || this.f2786 == null)) {
                    requestLayout();
                }
                this.f2800 = false;
                return;
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            onTouchEvent(MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0));
            this.f2802 = true;
            this.f2805 = true;
            m4423();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4380(int i, int i2) {
        m4381(i, i2, (Interpolator) null);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4381(int i, int i2, Interpolator interpolator) {
        C0701 r0 = this.f2787;
        if (r0 == null) {
            Log.e("RecyclerView", "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.f2802) {
            if (!r0.m4625()) {
                i = 0;
            }
            if (!this.f2787.m4631()) {
                i2 = 0;
            }
            if (i != 0 || i2 != 0) {
                this.f2810.m4787(i, i2, interpolator);
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m4408(int i, int i2) {
        C0701 r0 = this.f2787;
        int i3 = 0;
        if (r0 == null) {
            Log.e("RecyclerView", "Cannot fling without a LayoutManager set. Call setLayoutManager with a non-null argument.");
            return false;
        } else if (this.f2802) {
            return false;
        } else {
            boolean r02 = r0.m4625();
            boolean r2 = this.f2787.m4631();
            if (!r02 || Math.abs(i) < this.f2757) {
                i = 0;
            }
            if (!r2 || Math.abs(i2) < this.f2757) {
                i2 = 0;
            }
            if (i == 0 && i2 == 0) {
                return false;
            }
            float f = (float) i;
            float f2 = (float) i2;
            if (!dispatchNestedPreFling(f, f2)) {
                boolean z = r02 || r2;
                dispatchNestedFling(f, f2, z);
                C0706 r3 = this.f2756;
                if (r3 != null && r3.m4690(i, i2)) {
                    return true;
                }
                if (z) {
                    if (r02) {
                        i3 = 1;
                    }
                    if (r2) {
                        i3 |= 2;
                    }
                    m4438(i3, 1);
                    int i4 = this.f2758;
                    int max = Math.max(-i4, Math.min(i, i4));
                    int i5 = this.f2758;
                    this.f2810.m4783(max, Math.max(-i5, Math.min(i2, i5)));
                    return true;
                }
            }
            return false;
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m4423() {
        setScrollState(0);
        m4348();
    }

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private void m4348() {
        this.f2810.m4788();
        C0701 r0 = this.f2787;
        if (r0 != null) {
            r0.m4626();
        }
    }

    public int getMinFlingVelocity() {
        return this.f2757;
    }

    public int getMaxFlingVelocity() {
        return this.f2758;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0056  */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m4334(float r7, float r8, float r9, float r10) {
        /*
            r6 = this;
            r0 = 1065353216(0x3f800000, float:1.0)
            r1 = 1
            r2 = 0
            int r3 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r3 >= 0) goto L_0x0021
            r6.m4426()
            android.widget.EdgeEffect r3 = r6.f2744
            float r4 = -r8
            int r5 = r6.getWidth()
            float r5 = (float) r5
            float r4 = r4 / r5
            int r5 = r6.getHeight()
            float r5 = (float) r5
            float r9 = r9 / r5
            float r9 = r0 - r9
            android.support.v4.widget.C0164.m1021(r3, r4, r9)
        L_0x001f:
            r9 = 1
            goto L_0x003c
        L_0x0021:
            int r3 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r3 <= 0) goto L_0x003b
            r6.m4430()
            android.widget.EdgeEffect r3 = r6.f2746
            int r4 = r6.getWidth()
            float r4 = (float) r4
            float r4 = r8 / r4
            int r5 = r6.getHeight()
            float r5 = (float) r5
            float r9 = r9 / r5
            android.support.v4.widget.C0164.m1021(r3, r4, r9)
            goto L_0x001f
        L_0x003b:
            r9 = 0
        L_0x003c:
            int r3 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            if (r3 >= 0) goto L_0x0056
            r6.m4435()
            android.widget.EdgeEffect r9 = r6.f2745
            float r0 = -r10
            int r3 = r6.getHeight()
            float r3 = (float) r3
            float r0 = r0 / r3
            int r3 = r6.getWidth()
            float r3 = (float) r3
            float r7 = r7 / r3
            android.support.v4.widget.C0164.m1021(r9, r0, r7)
            goto L_0x0072
        L_0x0056:
            int r3 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            if (r3 <= 0) goto L_0x0071
            r6.m4437()
            android.widget.EdgeEffect r9 = r6.f2747
            int r3 = r6.getHeight()
            float r3 = (float) r3
            float r3 = r10 / r3
            int r4 = r6.getWidth()
            float r4 = (float) r4
            float r7 = r7 / r4
            float r0 = r0 - r7
            android.support.v4.widget.C0164.m1021(r9, r3, r0)
            goto L_0x0072
        L_0x0071:
            r1 = r9
        L_0x0072:
            if (r1 != 0) goto L_0x007c
            int r7 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r7 != 0) goto L_0x007c
            int r7 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            if (r7 == 0) goto L_0x007f
        L_0x007c:
            android.support.v4.ˉ.C0414.m2233(r6)
        L_0x007f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0690.m4334(float, float, float, float):void");
    }

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private void m4355() {
        boolean z;
        EdgeEffect edgeEffect = this.f2744;
        if (edgeEffect != null) {
            edgeEffect.onRelease();
            z = this.f2744.isFinished();
        } else {
            z = false;
        }
        EdgeEffect edgeEffect2 = this.f2745;
        if (edgeEffect2 != null) {
            edgeEffect2.onRelease();
            z |= this.f2745.isFinished();
        }
        EdgeEffect edgeEffect3 = this.f2746;
        if (edgeEffect3 != null) {
            edgeEffect3.onRelease();
            z |= this.f2746.isFinished();
        }
        EdgeEffect edgeEffect4 = this.f2747;
        if (edgeEffect4 != null) {
            edgeEffect4.onRelease();
            z |= this.f2747.isFinished();
        }
        if (z) {
            C0414.m2233(this);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m4413(int i, int i2) {
        boolean z;
        EdgeEffect edgeEffect = this.f2744;
        if (edgeEffect == null || edgeEffect.isFinished() || i <= 0) {
            z = false;
        } else {
            this.f2744.onRelease();
            z = this.f2744.isFinished();
        }
        EdgeEffect edgeEffect2 = this.f2746;
        if (edgeEffect2 != null && !edgeEffect2.isFinished() && i < 0) {
            this.f2746.onRelease();
            z |= this.f2746.isFinished();
        }
        EdgeEffect edgeEffect3 = this.f2745;
        if (edgeEffect3 != null && !edgeEffect3.isFinished() && i2 > 0) {
            this.f2745.onRelease();
            z |= this.f2745.isFinished();
        }
        EdgeEffect edgeEffect4 = this.f2747;
        if (edgeEffect4 != null && !edgeEffect4.isFinished() && i2 < 0) {
            this.f2747.onRelease();
            z |= this.f2747.isFinished();
        }
        if (z) {
            C0414.m2233(this);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m4418(int i, int i2) {
        if (i < 0) {
            m4426();
            this.f2744.onAbsorb(-i);
        } else if (i > 0) {
            m4430();
            this.f2746.onAbsorb(i);
        }
        if (i2 < 0) {
            m4435();
            this.f2745.onAbsorb(-i2);
        } else if (i2 > 0) {
            m4437();
            this.f2747.onAbsorb(i2);
        }
        if (i != 0 || i2 != 0) {
            C0414.m2233(this);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m4426() {
        if (this.f2744 == null) {
            this.f2744 = new EdgeEffect(getContext());
            if (this.f2781) {
                this.f2744.setSize((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
            } else {
                this.f2744.setSize(getMeasuredHeight(), getMeasuredWidth());
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public void m4430() {
        if (this.f2746 == null) {
            this.f2746 = new EdgeEffect(getContext());
            if (this.f2781) {
                this.f2746.setSize((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
            } else {
                this.f2746.setSize(getMeasuredHeight(), getMeasuredWidth());
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public void m4435() {
        if (this.f2745 == null) {
            this.f2745 = new EdgeEffect(getContext());
            if (this.f2781) {
                this.f2745.setSize((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
            } else {
                this.f2745.setSize(getMeasuredWidth(), getMeasuredHeight());
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public void m4437() {
        if (this.f2747 == null) {
            this.f2747 = new EdgeEffect(getContext());
            if (this.f2781) {
                this.f2747.setSize((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
            } else {
                this.f2747.setSize(getMeasuredWidth(), getMeasuredHeight());
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public void m4439() {
        this.f2747 = null;
        this.f2745 = null;
        this.f2746 = null;
        this.f2744 = null;
    }

    public View focusSearch(View view, int i) {
        View view2;
        boolean z;
        View r0 = this.f2787.m4621(view, i);
        if (r0 != null) {
            return r0;
        }
        boolean z2 = this.f2786 != null && this.f2787 != null && !m4445() && !this.f2802;
        FocusFinder instance = FocusFinder.getInstance();
        if (!z2 || !(i == 2 || i == 1)) {
            View findNextFocus = instance.findNextFocus(this, view, i);
            if (findNextFocus != null || !z2) {
                view2 = findNextFocus;
            } else {
                m4416();
                if (m4410(view) == null) {
                    return null;
                }
                m4419();
                view2 = this.f2787.m4550(view, i, this.f2775, this.f2797);
                m4393(false);
            }
        } else {
            if (this.f2787.m4631()) {
                int i2 = i == 2 ? 130 : 33;
                z = instance.findNextFocus(this, view, i2) == null;
                if (f2738) {
                    i = i2;
                }
            } else {
                z = false;
            }
            if (!z && this.f2787.m4625()) {
                int i3 = (this.f2787.m4665() == 1) ^ (i == 2) ? 66 : 17;
                z = instance.findNextFocus(this, view, i3) == null;
                if (f2738) {
                    i = i3;
                }
            }
            if (z) {
                m4416();
                if (m4410(view) == null) {
                    return null;
                }
                m4419();
                this.f2787.m4550(view, i, this.f2775, this.f2797);
                m4393(false);
            }
            view2 = instance.findNextFocus(this, view, i);
        }
        if (view2 == null || view2.hasFocusable()) {
            return m4346(view, view2, i) ? view2 : super.focusSearch(view, i);
        }
        if (getFocusedChild() == null) {
            return super.focusSearch(view, i);
        }
        m4343(view2, (View) null);
        return view;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m4346(View view, View view2, int i) {
        boolean z = false;
        if (view2 == null || view2 == this) {
            return false;
        }
        if (view == null) {
            return true;
        }
        if (i != 2 && i != 1) {
            return m4351(view, view2, i);
        }
        boolean z2 = this.f2787.m4665() == 1;
        if (i == 2) {
            z = true;
        }
        if (m4351(view, view2, z ^ z2 ? 66 : 17)) {
            return true;
        }
        if (i == 2) {
            return m4351(view, view2, 130);
        }
        return m4351(view, view2, 33);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m4351(View view, View view2, int i) {
        this.f2783.set(0, 0, view.getWidth(), view.getHeight());
        this.f2801.set(0, 0, view2.getWidth(), view2.getHeight());
        offsetDescendantRectToMyCoords(view, this.f2783);
        offsetDescendantRectToMyCoords(view2, this.f2801);
        if (i != 17) {
            if (i != 33) {
                if (i != 66) {
                    if (i == 130) {
                        return (this.f2783.top < this.f2801.top || this.f2783.bottom <= this.f2801.top) && this.f2783.bottom < this.f2801.bottom;
                    }
                    throw new IllegalArgumentException("direction must be absolute. received:" + i + m4378());
                } else if ((this.f2783.left < this.f2801.left || this.f2783.right <= this.f2801.left) && this.f2783.right < this.f2801.right) {
                    return true;
                } else {
                    return false;
                }
            } else if ((this.f2783.bottom > this.f2801.bottom || this.f2783.top >= this.f2801.bottom) && this.f2783.top > this.f2801.top) {
                return true;
            } else {
                return false;
            }
        } else if ((this.f2783.right > this.f2801.right || this.f2783.left >= this.f2801.right) && this.f2783.left > this.f2801.left) {
            return true;
        } else {
            return false;
        }
    }

    public void requestChildFocus(View view, View view2) {
        if (!this.f2787.m4584(this, this.f2797, view, view2) && view2 != null) {
            m4343(view, view2);
        }
        super.requestChildFocus(view, view2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4343(View view, View view2) {
        View view3 = view2 != null ? view2 : view;
        this.f2783.set(0, 0, view3.getWidth(), view3.getHeight());
        ViewGroup.LayoutParams layoutParams = view3.getLayoutParams();
        if (layoutParams instanceof C0704) {
            C0704 r0 = (C0704) layoutParams;
            if (!r0.f2856) {
                Rect rect = r0.f2855;
                this.f2783.left -= rect.left;
                this.f2783.right += rect.right;
                this.f2783.top -= rect.top;
                this.f2783.bottom += rect.bottom;
            }
        }
        if (view2 != null) {
            offsetDescendantRectToMyCoords(view2, this.f2783);
            offsetRectIntoDescendantCoords(view, this.f2783);
        }
        this.f2787.m4586(this, view, this.f2783, !this.f2798, view2 == null);
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        return this.f2787.m4585(this, view, rect, z);
    }

    public void addFocusables(ArrayList<View> arrayList, int i, int i2) {
        C0701 r0 = this.f2787;
        if (r0 == null || !r0.m4588(this, arrayList, i, i2)) {
            super.addFocusables(arrayList, i, i2);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i, Rect rect) {
        if (m4445()) {
            return false;
        }
        return super.onRequestFocusInDescendants(i, rect);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004f, code lost:
        if (r0 >= 30.0f) goto L_0x0054;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onAttachedToWindow() {
        /*
            r4 = this;
            super.onAttachedToWindow()
            r0 = 0
            r4.f2742 = r0
            r1 = 1
            r4.f2792 = r1
            boolean r2 = r4.f2798
            if (r2 == 0) goto L_0x0014
            boolean r2 = r4.isLayoutRequested()
            if (r2 != 0) goto L_0x0014
            goto L_0x0015
        L_0x0014:
            r1 = 0
        L_0x0015:
            r4.f2798 = r1
            android.support.v7.widget.ﹳﹳ$ˉ r1 = r4.f2787
            if (r1 == 0) goto L_0x001e
            r1.m4612(r4)
        L_0x001e:
            r4.f2767 = r0
            boolean r0 = android.support.v7.widget.C0690.f2735
            if (r0 == 0) goto L_0x0069
            java.lang.ThreadLocal<android.support.v7.widget.ˊˊ> r0 = android.support.v7.widget.C0644.f2558
            java.lang.Object r0 = r0.get()
            android.support.v7.widget.ˊˊ r0 = (android.support.v7.widget.C0644) r0
            r4.f2811 = r0
            android.support.v7.widget.ˊˊ r0 = r4.f2811
            if (r0 != 0) goto L_0x0064
            android.support.v7.widget.ˊˊ r0 = new android.support.v7.widget.ˊˊ
            r0.<init>()
            r4.f2811 = r0
            android.view.Display r0 = android.support.v4.ˉ.C0414.m2249(r4)
            r1 = 1114636288(0x42700000, float:60.0)
            boolean r2 = r4.isInEditMode()
            if (r2 != 0) goto L_0x0052
            if (r0 == 0) goto L_0x0052
            float r0 = r0.getRefreshRate()
            r2 = 1106247680(0x41f00000, float:30.0)
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 < 0) goto L_0x0052
            goto L_0x0054
        L_0x0052:
            r0 = 1114636288(0x42700000, float:60.0)
        L_0x0054:
            android.support.v7.widget.ˊˊ r1 = r4.f2811
            r2 = 1315859240(0x4e6e6b28, float:1.0E9)
            float r2 = r2 / r0
            long r2 = (long) r2
            r1.f2562 = r2
            java.lang.ThreadLocal<android.support.v7.widget.ˊˊ> r0 = android.support.v7.widget.C0644.f2558
            android.support.v7.widget.ˊˊ r1 = r4.f2811
            r0.set(r1)
        L_0x0064:
            android.support.v7.widget.ˊˊ r0 = r4.f2811
            r0.m4107(r4)
        L_0x0069:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0690.onAttachedToWindow():void");
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        C0695 r0 = this.f2808;
        if (r0 != null) {
            r0.m4512();
        }
        m4423();
        this.f2792 = false;
        C0701 r02 = this.f2787;
        if (r02 != null) {
            r02.m4599(this, this.f2775);
        }
        this.f2776.clear();
        removeCallbacks(this.f2772);
        this.f2780.m3841();
        if (f2735) {
            this.f2811.m4109(this);
            this.f2811 = null;
        }
    }

    public boolean isAttachedToWindow() {
        return this.f2792;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4392(String str) {
        if (m4445()) {
            if (str == null) {
                throw new IllegalStateException("Cannot call this method while RecyclerView is computing a layout or scrolling" + m4378());
            }
            throw new IllegalStateException(str);
        } else if (this.f2743 > 0) {
            Log.w("RecyclerView", "Cannot call this method in a scroll callback. Scroll callbacks mightbe run during a measure & layout pass where you cannot change theRecyclerView data. Any method call that might change the structureof the RecyclerView or the adapter contents should be postponed tothe next frame.", new IllegalStateException(BuildConfig.FLAVOR + m4378()));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4387(C0707 r2) {
        this.f2790.add(r2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4404(C0707 r2) {
        this.f2790.remove(r2);
        if (this.f2799 == r2) {
            this.f2799 = null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m4345(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 3 || action == 0) {
            this.f2799 = null;
        }
        int size = this.f2790.size();
        int i = 0;
        while (i < size) {
            C0707 r5 = this.f2790.get(i);
            if (!r5.m4692(this, motionEvent) || action == 3) {
                i++;
            } else {
                this.f2799 = r5;
                return true;
            }
        }
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m4350(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        C0707 r1 = this.f2799;
        if (r1 != null) {
            if (action == 0) {
                this.f2799 = null;
            } else {
                r1.m4693(this, motionEvent);
                if (action == 3 || action == 1) {
                    this.f2799 = null;
                }
                return true;
            }
        }
        if (action != 0) {
            int size = this.f2790.size();
            for (int i = 0; i < size; i++) {
                C0707 r4 = this.f2790.get(i);
                if (r4.m4692(this, motionEvent)) {
                    this.f2799 = r4;
                    return true;
                }
            }
        }
        return false;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        if (this.f2802) {
            return false;
        }
        if (m4345(motionEvent)) {
            m4359();
            return true;
        }
        C0701 r0 = this.f2787;
        if (r0 == null) {
            return false;
        }
        boolean r02 = r0.m4625();
        boolean r3 = this.f2787.m4631();
        if (this.f2750 == null) {
            this.f2750 = VelocityTracker.obtain();
        }
        this.f2750.addMovement(motionEvent);
        int actionMasked = motionEvent.getActionMasked();
        int actionIndex = motionEvent.getActionIndex();
        if (actionMasked == 0) {
            if (this.f2805) {
                this.f2805 = false;
            }
            this.f2749 = motionEvent.getPointerId(0);
            int x = (int) (motionEvent.getX() + 0.5f);
            this.f2753 = x;
            this.f2751 = x;
            int y = (int) (motionEvent.getY() + 0.5f);
            this.f2754 = y;
            this.f2752 = y;
            if (this.f2748 == 2) {
                getParent().requestDisallowInterceptTouchEvent(true);
                setScrollState(1);
            }
            int[] iArr = this.f2771;
            iArr[1] = 0;
            iArr[0] = 0;
            int i = r02 ? 1 : 0;
            if (r3) {
                i |= 2;
            }
            m4438(i, 0);
        } else if (actionMasked == 1) {
            this.f2750.clear();
            m4427(0);
        } else if (actionMasked == 2) {
            int findPointerIndex = motionEvent.findPointerIndex(this.f2749);
            if (findPointerIndex < 0) {
                Log.e("RecyclerView", "Error processing scroll; pointer index for id " + this.f2749 + " not found. Did any MotionEvents get skipped?");
                return false;
            }
            int x2 = (int) (motionEvent.getX(findPointerIndex) + 0.5f);
            int y2 = (int) (motionEvent.getY(findPointerIndex) + 0.5f);
            if (this.f2748 != 1) {
                int i2 = x2 - this.f2751;
                int i3 = y2 - this.f2752;
                if (!r02 || Math.abs(i2) <= this.f2755) {
                    z = false;
                } else {
                    this.f2753 = x2;
                    z = true;
                }
                if (r3 && Math.abs(i3) > this.f2755) {
                    this.f2754 = y2;
                    z = true;
                }
                if (z) {
                    setScrollState(1);
                }
            }
        } else if (actionMasked == 3) {
            m4359();
        } else if (actionMasked == 5) {
            this.f2749 = motionEvent.getPointerId(actionIndex);
            int x3 = (int) (motionEvent.getX(actionIndex) + 0.5f);
            this.f2753 = x3;
            this.f2751 = x3;
            int y3 = (int) (motionEvent.getY(actionIndex) + 0.5f);
            this.f2754 = y3;
            this.f2752 = y3;
        } else if (actionMasked == 6) {
            m4354(motionEvent);
        }
        if (this.f2748 == 1) {
            return true;
        }
        return false;
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        int size = this.f2790.size();
        for (int i = 0; i < size; i++) {
            this.f2790.get(i).m4691(z);
        }
        super.requestDisallowInterceptTouchEvent(z);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i;
        int i2;
        boolean z;
        int i3;
        int i4;
        boolean z2 = false;
        if (this.f2802 || this.f2805) {
            return false;
        }
        if (m4350(motionEvent)) {
            m4359();
            return true;
        }
        C0701 r0 = this.f2787;
        if (r0 == null) {
            return false;
        }
        boolean r02 = r0.m4625();
        boolean r3 = this.f2787.m4631();
        if (this.f2750 == null) {
            this.f2750 = VelocityTracker.obtain();
        }
        MotionEvent obtain = MotionEvent.obtain(motionEvent);
        int actionMasked = motionEvent.getActionMasked();
        int actionIndex = motionEvent.getActionIndex();
        if (actionMasked == 0) {
            int[] iArr = this.f2771;
            iArr[1] = 0;
            iArr[0] = 0;
        }
        int[] iArr2 = this.f2771;
        obtain.offsetLocation((float) iArr2[0], (float) iArr2[1]);
        if (actionMasked == 0) {
            this.f2749 = motionEvent.getPointerId(0);
            int x = (int) (motionEvent.getX() + 0.5f);
            this.f2753 = x;
            this.f2751 = x;
            int y = (int) (motionEvent.getY() + 0.5f);
            this.f2754 = y;
            this.f2752 = y;
            int i5 = r02 ? 1 : 0;
            if (r3) {
                i5 |= 2;
            }
            m4438(i5, 0);
        } else if (actionMasked == 1) {
            this.f2750.addMovement(obtain);
            this.f2750.computeCurrentVelocity(1000, (float) this.f2758);
            float f = r02 ? -this.f2750.getXVelocity(this.f2749) : 0.0f;
            float f2 = r3 ? -this.f2750.getYVelocity(this.f2749) : 0.0f;
            if ((f == 0.0f && f2 == 0.0f) || !m4408((int) f, (int) f2)) {
                setScrollState(0);
            }
            m4352();
            z2 = true;
        } else if (actionMasked == 2) {
            int findPointerIndex = motionEvent.findPointerIndex(this.f2749);
            if (findPointerIndex < 0) {
                Log.e("RecyclerView", "Error processing scroll; pointer index for id " + this.f2749 + " not found. Did any MotionEvents get skipped?");
                return false;
            }
            int x2 = (int) (motionEvent.getX(findPointerIndex) + 0.5f);
            int y2 = (int) (motionEvent.getY(findPointerIndex) + 0.5f);
            int i6 = this.f2753 - x2;
            int i7 = this.f2754 - y2;
            if (m4396(i6, i7, this.f2770, this.f2769, 0)) {
                int[] iArr3 = this.f2770;
                i6 -= iArr3[0];
                i7 -= iArr3[1];
                int[] iArr4 = this.f2769;
                obtain.offsetLocation((float) iArr4[0], (float) iArr4[1]);
                int[] iArr5 = this.f2771;
                int i8 = iArr5[0];
                int[] iArr6 = this.f2769;
                iArr5[0] = i8 + iArr6[0];
                iArr5[1] = iArr5[1] + iArr6[1];
            }
            if (this.f2748 != 1) {
                if (!r02 || Math.abs(i2) <= (i4 = this.f2755)) {
                    z = false;
                } else {
                    i2 = i2 > 0 ? i2 - i4 : i2 + i4;
                    z = true;
                }
                if (r3 && Math.abs(i) > (i3 = this.f2755)) {
                    i = i > 0 ? i - i3 : i + i3;
                    z = true;
                }
                if (z) {
                    setScrollState(1);
                }
            }
            if (this.f2748 == 1) {
                int[] iArr7 = this.f2769;
                this.f2753 = x2 - iArr7[0];
                this.f2754 = y2 - iArr7[1];
                if (m4395(r02 ? i2 : 0, r3 ? i : 0, obtain)) {
                    getParent().requestDisallowInterceptTouchEvent(true);
                }
                if (!(this.f2811 == null || (i2 == 0 && i == 0))) {
                    this.f2811.m4108(this, i2, i);
                }
            }
        } else if (actionMasked == 3) {
            m4359();
        } else if (actionMasked == 5) {
            this.f2749 = motionEvent.getPointerId(actionIndex);
            int x3 = (int) (motionEvent.getX(actionIndex) + 0.5f);
            this.f2753 = x3;
            this.f2751 = x3;
            int y3 = (int) (motionEvent.getY(actionIndex) + 0.5f);
            this.f2754 = y3;
            this.f2752 = y3;
        } else if (actionMasked == 6) {
            m4354(motionEvent);
        }
        if (!z2) {
            this.f2750.addMovement(obtain);
        }
        obtain.recycle();
        return true;
    }

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private void m4352() {
        VelocityTracker velocityTracker = this.f2750;
        if (velocityTracker != null) {
            velocityTracker.clear();
        }
        m4427(0);
        m4355();
    }

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private void m4359() {
        m4352();
        setScrollState(0);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m4354(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.f2749) {
            int i = actionIndex == 0 ? 1 : 0;
            this.f2749 = motionEvent.getPointerId(i);
            int x = (int) (motionEvent.getX(i) + 0.5f);
            this.f2753 = x;
            this.f2751 = x;
            int y = (int) (motionEvent.getY(i) + 0.5f);
            this.f2754 = y;
            this.f2752 = y;
        }
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        float f;
        float f2;
        if (this.f2787 != null && !this.f2802 && motionEvent.getAction() == 8) {
            if ((motionEvent.getSource() & 2) != 0) {
                f2 = this.f2787.m4631() ? -motionEvent.getAxisValue(9) : 0.0f;
                if (this.f2787.m4625()) {
                    f = motionEvent.getAxisValue(10);
                    if (!(f2 == 0.0f && f == 0.0f)) {
                        m4395((int) (f * this.f2759), (int) (f2 * this.f2760), motionEvent);
                    }
                }
            } else {
                if ((motionEvent.getSource() & 4194304) != 0) {
                    float axisValue = motionEvent.getAxisValue(26);
                    if (this.f2787.m4631()) {
                        f2 = -axisValue;
                    } else if (this.f2787.m4625()) {
                        f = axisValue;
                        f2 = 0.0f;
                        m4395((int) (f * this.f2759), (int) (f2 * this.f2760), motionEvent);
                    }
                }
                f2 = 0.0f;
            }
            f = 0.0f;
            m4395((int) (f * this.f2759), (int) (f2 * this.f2760), motionEvent);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        C0701 r0 = this.f2787;
        if (r0 == null) {
            m4421(i, i2);
            return;
        }
        boolean z = false;
        if (r0.f2845) {
            int mode = View.MeasureSpec.getMode(i);
            int mode2 = View.MeasureSpec.getMode(i2);
            if (mode == 1073741824 && mode2 == 1073741824) {
                z = true;
            }
            this.f2787.m4559(this.f2775, this.f2797, i, i2);
            if (!z && this.f2786 != null) {
                if (this.f2797.f2891 == 1) {
                    m4368();
                }
                this.f2787.m4609(i, i2);
                this.f2797.f2896 = true;
                m4367();
                this.f2787.m4623(i, i2);
                if (this.f2787.m4652()) {
                    this.f2787.m4609(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824));
                    this.f2797.f2896 = true;
                    m4367();
                    this.f2787.m4623(i, i2);
                }
            }
        } else if (this.f2794) {
            this.f2787.m4559(this.f2775, this.f2797, i, i2);
        } else {
            if (this.f2804) {
                m4419();
                m4441();
                m4360();
                m4443();
                if (this.f2797.f2898) {
                    this.f2797.f2894 = true;
                } else {
                    this.f2777.m4041();
                    this.f2797.f2894 = false;
                }
                this.f2804 = false;
                m4393(false);
            } else if (this.f2797.f2898) {
                setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
                return;
            }
            C0691 r02 = this.f2786;
            if (r02 != null) {
                this.f2797.f2892 = r02.m4480();
            } else {
                this.f2797.f2892 = 0;
            }
            m4419();
            this.f2787.m4559(this.f2775, this.f2797, i, i2);
            m4393(false);
            this.f2797.f2894 = false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m4421(int i, int i2) {
        setMeasuredDimension(C0701.m4534(i, getPaddingLeft() + getPaddingRight(), C0414.m2240(this)), C0701.m4534(i2, getPaddingTop() + getPaddingBottom(), C0414.m2241(this)));
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i != i3 || i2 != i4) {
            m4439();
        }
    }

    public void setItemAnimator(C0695 r3) {
        C0695 r0 = this.f2808;
        if (r0 != null) {
            r0.m4512();
            this.f2808.m4505(null);
        }
        this.f2808 = r3;
        C0695 r32 = this.f2808;
        if (r32 != null) {
            r32.m4505(this.f2764);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m4441() {
        this.f2742++;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m4443() {
        m4407(true);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4407(boolean z) {
        this.f2742--;
        if (this.f2742 < 1) {
            this.f2742 = 0;
            if (z) {
                m4356();
                m4455();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: י  reason: contains not printable characters */
    public boolean m4444() {
        AccessibilityManager accessibilityManager = this.f2793;
        return accessibilityManager != null && accessibilityManager.isEnabled();
    }

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private void m4356() {
        int i = this.f2807;
        this.f2807 = 0;
        if (i != 0 && m4444()) {
            AccessibilityEvent obtain = AccessibilityEvent.obtain();
            obtain.setEventType(InternalZipConstants.UFT8_NAMES_FLAG);
            C0356.m2000(obtain, i);
            sendAccessibilityEventUnchecked(obtain);
        }
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public boolean m4445() {
        return this.f2742 > 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m4399(AccessibilityEvent accessibilityEvent) {
        if (!m4445()) {
            return false;
        }
        int r3 = accessibilityEvent != null ? C0356.m1999(accessibilityEvent) : 0;
        if (r3 == 0) {
            r3 = 0;
        }
        this.f2807 = r3 | this.f2807;
        return true;
    }

    public void sendAccessibilityEventUnchecked(AccessibilityEvent accessibilityEvent) {
        if (!m4399(accessibilityEvent)) {
            super.sendAccessibilityEventUnchecked(accessibilityEvent);
        }
    }

    public C0695 getItemAnimator() {
        return this.f2808;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m4446() {
        if (!this.f2767 && this.f2792) {
            C0414.m2226(this, this.f2772);
            this.f2767 = true;
        }
    }

    /* renamed from: ــ  reason: contains not printable characters */
    private boolean m4371() {
        return this.f2808 != null && this.f2787.m4603();
    }

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private void m4360() {
        if (this.f2806) {
            this.f2777.m4031();
            this.f2787.m4564(this);
        }
        if (m4371()) {
            this.f2777.m4037();
        } else {
            this.f2777.m4041();
        }
        boolean z = false;
        boolean z2 = this.f2741 || this.f2774;
        this.f2797.f2897 = this.f2798 && this.f2808 != null && (this.f2806 || z2 || this.f2787.f2843) && (!this.f2806 || this.f2786.m4496());
        C0717 r3 = this.f2797;
        if (r3.f2897 && z2 && !this.f2806 && m4371()) {
            z = true;
        }
        r3.f2898 = z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m4447() {
        if (this.f2786 == null) {
            Log.e("RecyclerView", "No adapter attached; skipping layout");
        } else if (this.f2787 == null) {
            Log.e("RecyclerView", "No layout manager attached; skipping layout");
        } else {
            C0717 r0 = this.f2797;
            r0.f2896 = false;
            if (r0.f2891 == 1) {
                m4368();
                this.f2787.m4638(this);
                m4367();
            } else if (!this.f2777.m4042() && this.f2787.m4670() == getWidth() && this.f2787.m4671() == getHeight()) {
                this.f2787.m4638(this);
            } else {
                this.f2787.m4638(this);
                m4367();
            }
            m4370();
        }
    }

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private void m4362() {
        int i;
        C0720 r1 = null;
        View focusedChild = (!this.f2761 || !hasFocus() || this.f2786 == null) ? null : getFocusedChild();
        if (focusedChild != null) {
            r1 = m4415(focusedChild);
        }
        if (r1 == null) {
            m4361();
            return;
        }
        this.f2797.f2900 = this.f2786.m4496() ? r1.m4817() : -1;
        C0717 r0 = this.f2797;
        if (this.f2806) {
            i = -1;
        } else if (r1.m4827()) {
            i = r1.f2917;
        } else {
            i = r1.m4815();
        }
        r0.f2899 = i;
        this.f2797.f2901 = m4369(r1.f2914);
    }

    /* renamed from: ˈˈ  reason: contains not printable characters */
    private void m4361() {
        C0717 r0 = this.f2797;
        r0.f2900 = -1;
        r0.f2899 = -1;
        r0.f2901 = -1;
    }

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private View m4365() {
        C0720 r2;
        int i = this.f2797.f2899 != -1 ? this.f2797.f2899 : 0;
        int r1 = this.f2797.m4775();
        int i2 = i;
        while (i2 < r1) {
            C0720 r3 = m4400(i2);
            if (r3 == null) {
                break;
            } else if (r3.f2914.hasFocusable()) {
                return r3.f2914;
            } else {
                i2++;
            }
        }
        int min = Math.min(r1, i);
        while (true) {
            min--;
            if (min < 0 || (r2 = m4400(min)) == null) {
                return null;
            }
            if (r2.f2914.hasFocusable()) {
                return r2.f2914;
            }
        }
    }

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private void m4363() {
        View view;
        if (this.f2761 && this.f2786 != null && hasFocus() && getDescendantFocusability() != 393216) {
            if (getDescendantFocusability() != 131072 || !isFocused()) {
                if (!isFocused()) {
                    View focusedChild = getFocusedChild();
                    if (!f2737 || (focusedChild.getParent() != null && focusedChild.hasFocus())) {
                        if (!this.f2779.m3883(focusedChild)) {
                            return;
                        }
                    } else if (this.f2779.m3878() == 0) {
                        requestFocus();
                        return;
                    }
                }
                View view2 = null;
                C0720 r0 = (this.f2797.f2900 == -1 || !this.f2786.m4496()) ? null : m4377(this.f2797.f2900);
                if (r0 != null && !this.f2779.m3883(r0.f2914) && r0.f2914.hasFocusable()) {
                    view2 = r0.f2914;
                } else if (this.f2779.m3878() > 0) {
                    view2 = m4365();
                }
                if (view2 != null) {
                    if (((long) this.f2797.f2901) == -1 || (view = view2.findViewById(this.f2797.f2901)) == null || !view.isFocusable()) {
                        view = view2;
                    }
                    view.requestFocus();
                }
            }
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private int m4369(View view) {
        int id = view.getId();
        while (!view.isFocused() && (view instanceof ViewGroup) && view.hasFocus()) {
            view = ((ViewGroup) view).getFocusedChild();
            if (view.getId() != -1) {
                id = view.getId();
            }
        }
        return id;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m4389(C0717 r4) {
        if (getScrollState() == 2) {
            OverScroller r0 = this.f2810.f2910;
            r4.f2902 = r0.getFinalX() - r0.getCurrX();
            r4.f2903 = r0.getFinalY() - r0.getCurrY();
            return;
        }
        r4.f2902 = 0;
        r4.f2903 = 0;
    }

    /* renamed from: ˏˏ  reason: contains not printable characters */
    private void m4368() {
        boolean z = true;
        this.f2797.m4769(1);
        m4389(this.f2797);
        this.f2797.f2896 = false;
        m4419();
        this.f2780.m3835();
        m4441();
        m4360();
        m4362();
        C0717 r0 = this.f2797;
        if (!r0.f2897 || !this.f2774) {
            z = false;
        }
        r0.f2895 = z;
        this.f2774 = false;
        this.f2741 = false;
        C0717 r02 = this.f2797;
        r02.f2894 = r02.f2898;
        this.f2797.f2892 = this.f2786.m4480();
        m4344(this.f2766);
        if (this.f2797.f2897) {
            int r03 = this.f2779.m3878();
            for (int i = 0; i < r03; i++) {
                C0720 r3 = m4357(this.f2779.m3880(i));
                if (!r3.m4813() && (!r3.m4824() || this.f2786.m4496())) {
                    this.f2780.m3838(r3, this.f2808.m4503(this.f2797, r3, C0695.m4501(r3), r3.m4831()));
                    if (this.f2797.f2895 && r3.m4834() && !r3.m4827() && !r3.m4813() && !r3.m4824()) {
                        this.f2780.m3836(m4375(r3), r3);
                    }
                }
            }
        }
        if (this.f2797.f2898) {
            m4449();
            boolean z2 = this.f2797.f2893;
            C0717 r1 = this.f2797;
            r1.f2893 = false;
            this.f2787.m4611(this.f2775, r1);
            this.f2797.f2893 = z2;
            for (int i2 = 0; i2 < this.f2779.m3878(); i2++) {
                C0720 r12 = m4357(this.f2779.m3880(i2));
                if (!r12.m4813() && !this.f2780.m3845(r12)) {
                    int r32 = C0695.m4501(r12);
                    boolean r4 = r12.m4810(8192);
                    if (!r4) {
                        r32 |= 4096;
                    }
                    C0695.C0698 r33 = this.f2808.m4503(this.f2797, r12, r32, r12.m4831());
                    if (r4) {
                        m4390(r12, r33);
                    } else {
                        this.f2780.m3842(r12, r33);
                    }
                }
            }
            m4450();
        } else {
            m4450();
        }
        m4443();
        m4393(false);
        this.f2797.f2891 = 2;
    }

    /* renamed from: ˎˎ  reason: contains not printable characters */
    private void m4367() {
        m4419();
        m4441();
        this.f2797.m4769(6);
        this.f2777.m4041();
        this.f2797.f2892 = this.f2786.m4480();
        C0717 r0 = this.f2797;
        r0.f2890 = 0;
        r0.f2894 = false;
        this.f2787.m4611(this.f2775, r0);
        C0717 r02 = this.f2797;
        r02.f2893 = false;
        this.f2788 = null;
        r02.f2897 = r02.f2897 && this.f2808 != null;
        this.f2797.f2891 = 4;
        m4443();
        m4393(false);
    }

    /* renamed from: ˑˑ  reason: contains not printable characters */
    private void m4370() {
        this.f2797.m4769(4);
        m4419();
        m4441();
        C0717 r0 = this.f2797;
        r0.f2891 = 1;
        if (r0.f2897) {
            for (int r02 = this.f2779.m3878() - 1; r02 >= 0; r02--) {
                C0720 r5 = m4357(this.f2779.m3880(r02));
                if (!r5.m4813()) {
                    long r2 = m4375(r5);
                    C0695.C0698 r4 = this.f2808.m4502(this.f2797, r5);
                    C0720 r6 = this.f2780.m3834(r2);
                    if (r6 == null || r6.m4813()) {
                        this.f2780.m3844(r5, r4);
                    } else {
                        boolean r8 = this.f2780.m3839(r6);
                        boolean r9 = this.f2780.m3839(r5);
                        if (!r8 || r6 != r5) {
                            C0695.C0698 r7 = this.f2780.m3840(r6);
                            this.f2780.m3844(r5, r4);
                            C0695.C0698 r10 = this.f2780.m3843(r5);
                            if (r7 == null) {
                                m4335(r2, r5, r6);
                            } else {
                                m4338(r6, r5, r7, r10, r8, r9);
                            }
                        } else {
                            this.f2780.m3844(r5, r4);
                        }
                    }
                }
            }
            this.f2780.m3837(this.f2773);
        }
        this.f2787.m4596(this.f2775);
        C0717 r03 = this.f2797;
        r03.f2889 = r03.f2892;
        this.f2806 = false;
        C0717 r22 = this.f2797;
        r22.f2897 = false;
        r22.f2898 = false;
        this.f2787.f2843 = false;
        if (this.f2775.f2865 != null) {
            this.f2775.f2865.clear();
        }
        if (this.f2787.f2847) {
            C0701 r23 = this.f2787;
            r23.f2846 = 0;
            r23.f2847 = false;
            this.f2775.m4727();
        }
        this.f2787.m4563(this.f2797);
        m4443();
        m4393(false);
        this.f2780.m3835();
        int[] iArr = this.f2766;
        if (m4366(iArr[0], iArr[1])) {
            m4436(0, 0);
        }
        m4363();
        m4361();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4335(long j, C0720 r9, C0720 r10) {
        int r0 = this.f2779.m3878();
        for (int i = 0; i < r0; i++) {
            C0720 r2 = m4357(this.f2779.m3880(i));
            if (r2 != r9 && m4375(r2) == j) {
                C0691 r7 = this.f2786;
                if (r7 == null || !r7.m4496()) {
                    throw new IllegalStateException("Two different ViewHolders have the same change ID. This might happen due to inconsistent Adapter update events or if the LayoutManager lays out the same View multiple times.\n ViewHolder 1:" + r2 + " \n View Holder 2:" + r9 + m4378());
                }
                throw new IllegalStateException("Two different ViewHolders have the same stable ID. Stable IDs in your adapter MUST BE unique and SHOULD NOT change.\n ViewHolder 1:" + r2 + " \n View Holder 2:" + r9 + m4378());
            }
        }
        Log.e("RecyclerView", "Problem while matching changed view holders with the newones. The pre-layout information for the change holder " + r10 + " cannot be found but it is necessary for " + r9 + m4378());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4390(C0720 r4, C0695.C0698 r5) {
        r4.m4804(0, 8192);
        if (this.f2797.f2895 && r4.m4834() && !r4.m4827() && !r4.m4813()) {
            this.f2780.m3836(m4375(r4), r4);
        }
        this.f2780.m3838(r4, r5);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4344(int[] iArr) {
        int r0 = this.f2779.m3878();
        if (r0 == 0) {
            iArr[0] = -1;
            iArr[1] = -1;
            return;
        }
        int i = Integer.MAX_VALUE;
        int i2 = Integer.MIN_VALUE;
        for (int i3 = 0; i3 < r0; i3++) {
            C0720 r6 = m4357(this.f2779.m3880(i3));
            if (!r6.m4813()) {
                int r62 = r6.m4814();
                if (r62 < i) {
                    i = r62;
                }
                if (r62 > i2) {
                    i2 = r62;
                }
            }
        }
        iArr[0] = i;
        iArr[1] = i2;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean m4366(int i, int i2) {
        m4344(this.f2766);
        int[] iArr = this.f2766;
        return (iArr[0] == i && iArr[1] == i2) ? false : true;
    }

    /* access modifiers changed from: protected */
    public void removeDetachedView(View view, boolean z) {
        C0720 r0 = m4357(view);
        if (r0 != null) {
            if (r0.m4828()) {
                r0.m4823();
            } else if (!r0.m4813()) {
                throw new IllegalArgumentException("Called removeDetachedView with a view which is not flagged as tmp detached." + r0 + m4378());
            }
        }
        view.clearAnimation();
        m4440(view);
        super.removeDetachedView(view, z);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public long m4375(C0720 r3) {
        return this.f2786.m4496() ? r3.m4817() : (long) r3.f2916;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4391(C0720 r2, C0695.C0698 r3, C0695.C0698 r4) {
        r2.m4809(false);
        if (this.f2808.m4510(r2, r3, r4)) {
            m4446();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4406(C0720 r2, C0695.C0698 r3, C0695.C0698 r4) {
        m4358(r2);
        r2.m4809(false);
        if (this.f2808.m4506(r2, r3, r4)) {
            m4446();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4338(C0720 r2, C0720 r3, C0695.C0698 r4, C0695.C0698 r5, boolean z, boolean z2) {
        r2.m4809(false);
        if (z) {
            m4358(r2);
        }
        if (r2 != r3) {
            if (z2) {
                m4358(r3);
            }
            r2.f2921 = r3;
            m4358(r2);
            this.f2775.m4734(r2);
            r3.m4809(false);
            r3.f2922 = r2;
        }
        if (this.f2808.m4507(r2, r3, r4, r5)) {
            m4446();
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        C0324.m1842("RV OnLayout");
        m4447();
        C0324.m1841();
        this.f2798 = true;
    }

    public void requestLayout() {
        if (this.f2803 != 0 || this.f2802) {
            this.f2800 = true;
        } else {
            super.requestLayout();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᴵ  reason: contains not printable characters */
    public void m4448() {
        int r0 = this.f2779.m3881();
        for (int i = 0; i < r0; i++) {
            ((C0704) this.f2779.m3884(i).getLayoutParams()).f2856 = true;
        }
        this.f2775.m4746();
    }

    public void draw(Canvas canvas) {
        boolean z;
        boolean z2;
        super.draw(canvas);
        int size = this.f2791.size();
        boolean z3 = false;
        for (int i = 0; i < size; i++) {
            this.f2791.get(i).m4529(canvas, this, this.f2797);
        }
        EdgeEffect edgeEffect = this.f2744;
        if (edgeEffect == null || edgeEffect.isFinished()) {
            z = false;
        } else {
            int save = canvas.save();
            int paddingBottom = this.f2781 ? getPaddingBottom() : 0;
            canvas.rotate(270.0f);
            canvas.translate((float) ((-getHeight()) + paddingBottom), 0.0f);
            EdgeEffect edgeEffect2 = this.f2744;
            z = edgeEffect2 != null && edgeEffect2.draw(canvas);
            canvas.restoreToCount(save);
        }
        EdgeEffect edgeEffect3 = this.f2745;
        if (edgeEffect3 != null && !edgeEffect3.isFinished()) {
            int save2 = canvas.save();
            if (this.f2781) {
                canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
            }
            EdgeEffect edgeEffect4 = this.f2745;
            z |= edgeEffect4 != null && edgeEffect4.draw(canvas);
            canvas.restoreToCount(save2);
        }
        EdgeEffect edgeEffect5 = this.f2746;
        if (edgeEffect5 != null && !edgeEffect5.isFinished()) {
            int save3 = canvas.save();
            int width = getWidth();
            int paddingTop = this.f2781 ? getPaddingTop() : 0;
            canvas.rotate(90.0f);
            canvas.translate((float) (-paddingTop), (float) (-width));
            EdgeEffect edgeEffect6 = this.f2746;
            z |= edgeEffect6 != null && edgeEffect6.draw(canvas);
            canvas.restoreToCount(save3);
        }
        EdgeEffect edgeEffect7 = this.f2747;
        if (edgeEffect7 == null || edgeEffect7.isFinished()) {
            z2 = z;
        } else {
            int save4 = canvas.save();
            canvas.rotate(180.0f);
            if (this.f2781) {
                canvas.translate((float) ((-getWidth()) + getPaddingRight()), (float) ((-getHeight()) + getPaddingBottom()));
            } else {
                canvas.translate((float) (-getWidth()), (float) (-getHeight()));
            }
            EdgeEffect edgeEffect8 = this.f2747;
            if (edgeEffect8 != null && edgeEffect8.draw(canvas)) {
                z3 = true;
            }
            z2 = z3 | z;
            canvas.restoreToCount(save4);
        }
        if (!z2 && this.f2808 != null && this.f2791.size() > 0 && this.f2808.m4509()) {
            z2 = true;
        }
        if (z2) {
            C0414.m2233(this);
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int size = this.f2791.size();
        for (int i = 0; i < size; i++) {
            this.f2791.get(i).m4533(canvas, this, this.f2797);
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof C0704) && this.f2787.m4581((C0704) layoutParams);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        C0701 r0 = this.f2787;
        if (r0 != null) {
            return r0.m4547();
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager" + m4378());
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        C0701 r0 = this.f2787;
        if (r0 != null) {
            return r0.m4548(getContext(), attributeSet);
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager" + m4378());
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        C0701 r0 = this.f2787;
        if (r0 != null) {
            return r0.m4549(layoutParams);
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager" + m4378());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵎ  reason: contains not printable characters */
    public void m4449() {
        int r0 = this.f2779.m3881();
        for (int i = 0; i < r0; i++) {
            C0720 r2 = m4357(this.f2779.m3884(i));
            if (!r2.m4813()) {
                r2.m4811();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵔ  reason: contains not printable characters */
    public void m4450() {
        int r0 = this.f2779.m3881();
        for (int i = 0; i < r0; i++) {
            C0720 r2 = m4357(this.f2779.m3884(i));
            if (!r2.m4813()) {
                r2.m4803();
            }
        }
        this.f2775.m4745();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ$ـ):android.support.v7.widget.ﹳﹳ$ـ
      android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ):void
      android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):boolean
      android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, int):void
      android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, boolean):void
      android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m4425(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int r0 = this.f2779.m3881();
        if (i < i2) {
            i5 = i;
            i4 = i2;
            i3 = -1;
        } else {
            i4 = i;
            i5 = i2;
            i3 = 1;
        }
        for (int i6 = 0; i6 < r0; i6++) {
            C0720 r7 = m4357(this.f2779.m3884(i6));
            if (r7 != null && r7.f2916 >= i5 && r7.f2916 <= i4) {
                if (r7.f2916 == i) {
                    r7.m4806(i2 - i, false);
                } else {
                    r7.m4806(i3, false);
                }
                this.f2797.f2893 = true;
            }
        }
        this.f2775.m4717(i, i2);
        requestLayout();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ$ـ):android.support.v7.widget.ﹳﹳ$ـ
      android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ):void
      android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):boolean
      android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, int):void
      android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, boolean):void
      android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m4428(int i, int i2) {
        int r0 = this.f2779.m3881();
        for (int i3 = 0; i3 < r0; i3++) {
            C0720 r3 = m4357(this.f2779.m3884(i3));
            if (r3 != null && !r3.m4813() && r3.f2916 >= i) {
                r3.m4806(i2, false);
                this.f2797.f2893 = true;
            }
        }
        this.f2775.m4728(i, i2);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4383(int i, int i2, boolean z) {
        int i3 = i + i2;
        int r1 = this.f2779.m3881();
        for (int i4 = 0; i4 < r1; i4++) {
            C0720 r3 = m4357(this.f2779.m3884(i4));
            if (r3 != null && !r3.m4813()) {
                if (r3.f2916 >= i3) {
                    r3.m4806(-i2, z);
                    this.f2797.f2893 = true;
                } else if (r3.f2916 >= i) {
                    r3.m4805(i - 1, -i2, z);
                    this.f2797.f2893 = true;
                }
            }
        }
        this.f2775.m4718(i, i2, z);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4382(int i, int i2, Object obj) {
        int r0 = this.f2779.m3881();
        int i3 = i + i2;
        for (int i4 = 0; i4 < r0; i4++) {
            View r3 = this.f2779.m3884(i4);
            C0720 r4 = m4357(r3);
            if (r4 != null && !r4.m4813() && r4.f2916 >= i && r4.f2916 < i3) {
                r4.m4812(2);
                r4.m4808(obj);
                ((C0704) r3.getLayoutParams()).f2856 = true;
            }
        }
        this.f2775.m4733(i, i2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m4409(C0720 r3) {
        C0695 r0 = this.f2808;
        return r0 == null || r0.m4508(r3, r3.m4831());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵢ  reason: contains not printable characters */
    public void m4451() {
        this.f2806 = true;
        m4452();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ⁱ  reason: contains not printable characters */
    public void m4452() {
        int r0 = this.f2779.m3881();
        for (int i = 0; i < r0; i++) {
            C0720 r2 = m4357(this.f2779.m3884(i));
            if (r2 != null && !r2.m4813()) {
                r2.m4812(6);
            }
        }
        m4448();
        this.f2775.m4744();
    }

    public boolean getPreserveFocusAfterLayout() {
        return this.f2761;
    }

    public void setPreserveFocusAfterLayout(boolean z) {
        this.f2761 = z;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0720 m4401(View view) {
        ViewParent parent = view.getParent();
        if (parent == null || parent == this) {
            return m4357(view);
        }
        throw new IllegalArgumentException("View " + view + " is not a direct child of " + this);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public View m4410(View view) {
        ViewParent parent = view.getParent();
        while (parent != null && parent != this && (parent instanceof View)) {
            view = (View) parent;
            parent = view.getParent();
        }
        if (parent == this) {
            return view;
        }
        return null;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public C0720 m4415(View view) {
        View r1 = m4410(view);
        if (r1 == null) {
            return null;
        }
        return m4401(r1);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    static C0720 m4357(View view) {
        if (view == null) {
            return null;
        }
        return ((C0704) view.getLayoutParams()).f2854;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public int m4422(View view) {
        C0720 r1 = m4357(view);
        if (r1 != null) {
            return r1.m4814();
        }
        return -1;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0720 m4400(int i) {
        C0720 r1 = null;
        if (this.f2806) {
            return null;
        }
        int r0 = this.f2779.m3881();
        for (int i2 = 0; i2 < r0; i2++) {
            C0720 r3 = m4357(this.f2779.m3884(i2));
            if (r3 != null && !r3.m4827() && m4414(r3) == i) {
                if (!this.f2779.m3883(r3.f2914)) {
                    return r3;
                }
                r1 = r3;
            }
        }
        return r1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0720 m4376(int i, boolean z) {
        int r0 = this.f2779.m3881();
        C0720 r1 = null;
        for (int i2 = 0; i2 < r0; i2++) {
            C0720 r3 = m4357(this.f2779.m3884(i2));
            if (r3 != null && !r3.m4827()) {
                if (z) {
                    if (r3.f2916 != i) {
                        continue;
                    }
                } else if (r3.m4814() != i) {
                    continue;
                }
                if (!this.f2779.m3883(r3.f2914)) {
                    return r3;
                }
                r1 = r3;
            }
        }
        return r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0720 m4377(long j) {
        C0691 r0 = this.f2786;
        C0720 r1 = null;
        if (r0 != null && r0.m4496()) {
            int r02 = this.f2779.m3881();
            for (int i = 0; i < r02; i++) {
                C0720 r3 = m4357(this.f2779.m3884(i));
                if (r3 != null && !r3.m4827() && r3.m4817() == j) {
                    if (!this.f2779.m3883(r3.f2914)) {
                        return r3;
                    }
                    r1 = r3;
                }
            }
        }
        return r1;
    }

    public boolean drawChild(Canvas canvas, View view, long j) {
        return super.drawChild(canvas, view, j);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m4412(int i) {
        int r0 = this.f2779.m3878();
        for (int i2 = 0; i2 < r0; i2++) {
            this.f2779.m3880(i2).offsetTopAndBottom(i);
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m4417(int i) {
        int r0 = this.f2779.m3878();
        for (int i2 = 0; i2 < r0; i2++) {
            this.f2779.m3880(i2).offsetLeftAndRight(i);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m4342(View view, Rect rect) {
        C0704 r0 = (C0704) view.getLayoutParams();
        Rect rect2 = r0.f2855;
        rect.set((view.getLeft() - rect2.left) - r0.leftMargin, (view.getTop() - rect2.top) - r0.topMargin, view.getRight() + rect2.right + r0.rightMargin, view.getBottom() + rect2.bottom + r0.bottomMargin);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public Rect m4434(View view) {
        C0704 r0 = (C0704) view.getLayoutParams();
        if (!r0.f2856) {
            return r0.f2855;
        }
        if (this.f2797.m4771() && (r0.m4686() || r0.m4684())) {
            return r0.f2855;
        }
        Rect rect = r0.f2855;
        rect.set(0, 0, 0, 0);
        int size = this.f2791.size();
        for (int i = 0; i < size; i++) {
            this.f2783.set(0, 0, 0, 0);
            this.f2791.get(i).m4531(this.f2783, view, this, this.f2797);
            rect.left += this.f2783.left;
            rect.top += this.f2783.top;
            rect.right += this.f2783.right;
            rect.bottom += this.f2783.bottom;
        }
        r0.f2856 = false;
        return rect;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public void m4436(int i, int i2) {
        this.f2743++;
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        onScrollChanged(scrollX, scrollY, scrollX, scrollY);
        m4431(i, i2);
        C0708 r0 = this.f2762;
        if (r0 != null) {
            r0.m4695(this, i, i2);
        }
        List<C0708> list = this.f2763;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.f2763.get(size).m4695(this, i, i2);
            }
        }
        this.f2743--;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m4424(int i) {
        C0701 r0 = this.f2787;
        if (r0 != null) {
            r0.m4651(i);
        }
        m4420(i);
        C0708 r02 = this.f2762;
        if (r02 != null) {
            r02.m4694(this, i);
        }
        List<C0708> list = this.f2763;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.f2763.get(size).m4694(this, i);
            }
        }
    }

    /* renamed from: ﹳ  reason: contains not printable characters */
    public boolean m4453() {
        return !this.f2798 || this.f2806 || this.f2777.m4040();
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ⁱ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    class C0719 implements Runnable {

        /* renamed from: ʻ  reason: contains not printable characters */
        Interpolator f2906 = C0690.f2740;

        /* renamed from: ʽ  reason: contains not printable characters */
        private int f2908;

        /* renamed from: ʾ  reason: contains not printable characters */
        private int f2909;
        /* access modifiers changed from: private */

        /* renamed from: ʿ  reason: contains not printable characters */
        public OverScroller f2910;

        /* renamed from: ˆ  reason: contains not printable characters */
        private boolean f2911 = false;

        /* renamed from: ˈ  reason: contains not printable characters */
        private boolean f2912 = false;

        C0719() {
            this.f2910 = new OverScroller(C0690.this.getContext(), C0690.f2740);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:52:0x0132, code lost:
            if (r8 > 0) goto L_0x0136;
         */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x012e  */
        /* JADX WARNING: Removed duplicated region for block: B:56:0x013e  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r22 = this;
                r0 = r22
                android.support.v7.widget.ﹳﹳ r1 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ˉ r1 = r1.f2787
                if (r1 != 0) goto L_0x000c
                r22.m4788()
                return
            L_0x000c:
                r22.m4780()
                android.support.v7.widget.ﹳﹳ r1 = android.support.v7.widget.C0690.this
                r1.m4416()
                android.widget.OverScroller r1 = r0.f2910
                android.support.v7.widget.ﹳﹳ r2 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ˉ r2 = r2.f2787
                android.support.v7.widget.ﹳﹳ$ᵎ r2 = r2.f2842
                boolean r3 = r1.computeScrollOffset()
                r4 = 0
                if (r3 == 0) goto L_0x01da
                android.support.v7.widget.ﹳﹳ r3 = android.support.v7.widget.C0690.this
                int[] r3 = r3.f2770
                int r11 = r1.getCurrX()
                int r12 = r1.getCurrY()
                int r5 = r0.f2908
                int r13 = r11 - r5
                int r5 = r0.f2909
                int r14 = r12 - r5
                r0.f2908 = r11
                r0.f2909 = r12
                android.support.v7.widget.ﹳﹳ r5 = android.support.v7.widget.C0690.this
                r9 = 0
                r10 = 1
                r6 = r13
                r7 = r14
                r8 = r3
                boolean r5 = r5.m4396(r6, r7, r8, r9, r10)
                r6 = 1
                if (r5 == 0) goto L_0x0051
                r5 = r3[r4]
                int r13 = r13 - r5
                r3 = r3[r6]
                int r14 = r14 - r3
            L_0x0051:
                android.support.v7.widget.ﹳﹳ r3 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ʻ r3 = r3.f2786
                if (r3 == 0) goto L_0x00e3
                android.support.v7.widget.ﹳﹳ r3 = android.support.v7.widget.C0690.this
                r3.m4419()
                android.support.v7.widget.ﹳﹳ r3 = android.support.v7.widget.C0690.this
                r3.m4441()
                java.lang.String r3 = "RV Scroll"
                android.support.v4.ʿ.C0324.m1842(r3)
                android.support.v7.widget.ﹳﹳ r3 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ᵔ r5 = r3.f2797
                r3.m4389(r5)
                if (r13 == 0) goto L_0x0082
                android.support.v7.widget.ﹳﹳ r3 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ˉ r3 = r3.f2787
                android.support.v7.widget.ﹳﹳ r5 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ـ r5 = r5.f2775
                android.support.v7.widget.ﹳﹳ r7 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ᵔ r7 = r7.f2797
                int r3 = r3.m4545(r13, r5, r7)
                int r5 = r13 - r3
                goto L_0x0084
            L_0x0082:
                r3 = 0
                r5 = 0
            L_0x0084:
                if (r14 == 0) goto L_0x0099
                android.support.v7.widget.ﹳﹳ r7 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ˉ r7 = r7.f2787
                android.support.v7.widget.ﹳﹳ r8 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ـ r8 = r8.f2775
                android.support.v7.widget.ﹳﹳ r9 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ᵔ r9 = r9.f2797
                int r7 = r7.m4594(r14, r8, r9)
                int r8 = r14 - r7
                goto L_0x009b
            L_0x0099:
                r7 = 0
                r8 = 0
            L_0x009b:
                android.support.v4.ʿ.C0324.m1841()
                android.support.v7.widget.ﹳﹳ r9 = android.support.v7.widget.C0690.this
                r9.m4454()
                android.support.v7.widget.ﹳﹳ r9 = android.support.v7.widget.C0690.this
                r9.m4443()
                android.support.v7.widget.ﹳﹳ r9 = android.support.v7.widget.C0690.this
                r9.m4393(r4)
                if (r2 == 0) goto L_0x00e7
                boolean r9 = r2.m4761()
                if (r9 != 0) goto L_0x00e7
                boolean r9 = r2.m4762()
                if (r9 == 0) goto L_0x00e7
                android.support.v7.widget.ﹳﹳ r9 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ᵔ r9 = r9.f2797
                int r9 = r9.m4775()
                if (r9 != 0) goto L_0x00c9
                r2.m4756()
                goto L_0x00e7
            L_0x00c9:
                int r10 = r2.m4763()
                if (r10 < r9) goto L_0x00db
                int r9 = r9 - r6
                r2.m4757(r9)
                int r9 = r13 - r5
                int r10 = r14 - r8
                r2.m4753(r9, r10)
                goto L_0x00e7
            L_0x00db:
                int r9 = r13 - r5
                int r10 = r14 - r8
                r2.m4753(r9, r10)
                goto L_0x00e7
            L_0x00e3:
                r3 = 0
                r5 = 0
                r7 = 0
                r8 = 0
            L_0x00e7:
                android.support.v7.widget.ﹳﹳ r9 = android.support.v7.widget.C0690.this
                java.util.ArrayList<android.support.v7.widget.ﹳﹳ$ˈ> r9 = r9.f2791
                boolean r9 = r9.isEmpty()
                if (r9 != 0) goto L_0x00f6
                android.support.v7.widget.ﹳﹳ r9 = android.support.v7.widget.C0690.this
                r9.invalidate()
            L_0x00f6:
                android.support.v7.widget.ﹳﹳ r9 = android.support.v7.widget.C0690.this
                int r9 = r9.getOverScrollMode()
                r10 = 2
                if (r9 == r10) goto L_0x0104
                android.support.v7.widget.ﹳﹳ r9 = android.support.v7.widget.C0690.this
                r9.m4413(r13, r14)
            L_0x0104:
                android.support.v7.widget.ﹳﹳ r15 = android.support.v7.widget.C0690.this
                r20 = 0
                r21 = 1
                r16 = r3
                r17 = r7
                r18 = r5
                r19 = r8
                boolean r9 = r15.m4394(r16, r17, r18, r19, r20, r21)
                if (r9 != 0) goto L_0x015a
                if (r5 != 0) goto L_0x011c
                if (r8 == 0) goto L_0x015a
            L_0x011c:
                float r9 = r1.getCurrVelocity()
                int r9 = (int) r9
                if (r5 == r11) goto L_0x012b
                if (r5 >= 0) goto L_0x0127
                int r15 = -r9
                goto L_0x012c
            L_0x0127:
                if (r5 <= 0) goto L_0x012b
                r15 = r9
                goto L_0x012c
            L_0x012b:
                r15 = 0
            L_0x012c:
                if (r8 == r12) goto L_0x0135
                if (r8 >= 0) goto L_0x0132
                int r9 = -r9
                goto L_0x0136
            L_0x0132:
                if (r8 <= 0) goto L_0x0135
                goto L_0x0136
            L_0x0135:
                r9 = 0
            L_0x0136:
                android.support.v7.widget.ﹳﹳ r4 = android.support.v7.widget.C0690.this
                int r4 = r4.getOverScrollMode()
                if (r4 == r10) goto L_0x0143
                android.support.v7.widget.ﹳﹳ r4 = android.support.v7.widget.C0690.this
                r4.m4418(r15, r9)
            L_0x0143:
                if (r15 != 0) goto L_0x014d
                if (r5 == r11) goto L_0x014d
                int r4 = r1.getFinalX()
                if (r4 != 0) goto L_0x015a
            L_0x014d:
                if (r9 != 0) goto L_0x0157
                if (r8 == r12) goto L_0x0157
                int r4 = r1.getFinalY()
                if (r4 != 0) goto L_0x015a
            L_0x0157:
                r1.abortAnimation()
            L_0x015a:
                if (r3 != 0) goto L_0x015e
                if (r7 == 0) goto L_0x0163
            L_0x015e:
                android.support.v7.widget.ﹳﹳ r4 = android.support.v7.widget.C0690.this
                r4.m4436(r3, r7)
            L_0x0163:
                android.support.v7.widget.ﹳﹳ r4 = android.support.v7.widget.C0690.this
                boolean r4 = r4.awakenScrollBars()
                if (r4 != 0) goto L_0x0170
                android.support.v7.widget.ﹳﹳ r4 = android.support.v7.widget.C0690.this
                r4.invalidate()
            L_0x0170:
                if (r14 == 0) goto L_0x0180
                android.support.v7.widget.ﹳﹳ r4 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ˉ r4 = r4.f2787
                boolean r4 = r4.m4631()
                if (r4 == 0) goto L_0x0180
                if (r7 != r14) goto L_0x0180
                r4 = 1
                goto L_0x0181
            L_0x0180:
                r4 = 0
            L_0x0181:
                if (r13 == 0) goto L_0x0191
                android.support.v7.widget.ﹳﹳ r5 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ˉ r5 = r5.f2787
                boolean r5 = r5.m4625()
                if (r5 == 0) goto L_0x0191
                if (r3 != r13) goto L_0x0191
                r3 = 1
                goto L_0x0192
            L_0x0191:
                r3 = 0
            L_0x0192:
                if (r13 != 0) goto L_0x0196
                if (r14 == 0) goto L_0x019d
            L_0x0196:
                if (r3 != 0) goto L_0x019d
                if (r4 == 0) goto L_0x019b
                goto L_0x019d
            L_0x019b:
                r3 = 0
                goto L_0x019e
            L_0x019d:
                r3 = 1
            L_0x019e:
                boolean r1 = r1.isFinished()
                if (r1 != 0) goto L_0x01c2
                if (r3 != 0) goto L_0x01af
                android.support.v7.widget.ﹳﹳ r1 = android.support.v7.widget.C0690.this
                boolean r1 = r1.m4433(r6)
                if (r1 != 0) goto L_0x01af
                goto L_0x01c2
            L_0x01af:
                r22.m4782()
                android.support.v7.widget.ﹳﹳ r1 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ˊˊ r1 = r1.f2811
                if (r1 == 0) goto L_0x01da
                android.support.v7.widget.ﹳﹳ r1 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ˊˊ r1 = r1.f2811
                android.support.v7.widget.ﹳﹳ r3 = android.support.v7.widget.C0690.this
                r1.m4108(r3, r13, r14)
                goto L_0x01da
            L_0x01c2:
                android.support.v7.widget.ﹳﹳ r1 = android.support.v7.widget.C0690.this
                r3 = 0
                r1.setScrollState(r3)
                boolean r1 = android.support.v7.widget.C0690.f2735
                if (r1 == 0) goto L_0x01d5
                android.support.v7.widget.ﹳﹳ r1 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ˊˊ$ʻ r1 = r1.f2795
                r1.m4111()
            L_0x01d5:
                android.support.v7.widget.ﹳﹳ r1 = android.support.v7.widget.C0690.this
                r1.m4427(r6)
            L_0x01da:
                if (r2 == 0) goto L_0x01ed
                boolean r1 = r2.m4761()
                if (r1 == 0) goto L_0x01e6
                r1 = 0
                r2.m4753(r1, r1)
            L_0x01e6:
                boolean r1 = r0.f2912
                if (r1 != 0) goto L_0x01ed
                r2.m4756()
            L_0x01ed:
                r22.m4781()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0690.C0719.run():void");
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        private void m4780() {
            this.f2912 = false;
            this.f2911 = true;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        private void m4781() {
            this.f2911 = false;
            if (this.f2912) {
                m4782();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4782() {
            if (this.f2911) {
                this.f2912 = true;
                return;
            }
            C0690.this.removeCallbacks(this);
            C0414.m2226(C0690.this, this);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4783(int i, int i2) {
            C0690.this.setScrollState(2);
            this.f2909 = 0;
            this.f2908 = 0;
            this.f2910.fling(0, 0, i, i2, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
            m4782();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4789(int i, int i2) {
            m4785(i, i2, 0, 0);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4785(int i, int i2, int i3, int i4) {
            m4784(i, i2, m4779(i, i2, i3, i4));
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private float m4777(float f) {
            return (float) Math.sin((double) ((f - 0.5f) * 0.47123894f));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(long, long):long}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(float, float):float} */
        /* renamed from: ʼ  reason: contains not printable characters */
        private int m4779(int i, int i2, int i3, int i4) {
            int i5;
            int abs = Math.abs(i);
            int abs2 = Math.abs(i2);
            boolean z = abs > abs2;
            int sqrt = (int) Math.sqrt((double) ((i3 * i3) + (i4 * i4)));
            int sqrt2 = (int) Math.sqrt((double) ((i * i) + (i2 * i2)));
            int width = z ? C0690.this.getWidth() : C0690.this.getHeight();
            int i6 = width / 2;
            float f = (float) width;
            float f2 = (float) i6;
            float r8 = f2 + (m4777(Math.min(1.0f, (((float) sqrt2) * 1.0f) / f)) * f2);
            if (sqrt > 0) {
                i5 = Math.round(Math.abs(r8 / ((float) sqrt)) * 1000.0f) * 4;
            } else {
                if (!z) {
                    abs = abs2;
                }
                i5 = (int) (((((float) abs) / f) + 1.0f) * 300.0f);
            }
            return Math.min(i5, 2000);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4784(int i, int i2, int i3) {
            m4786(i, i2, i3, C0690.f2740);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4787(int i, int i2, Interpolator interpolator) {
            int r0 = m4779(i, i2, 0, 0);
            if (interpolator == null) {
                interpolator = C0690.f2740;
            }
            m4786(i, i2, r0, interpolator);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4786(int i, int i2, int i3, Interpolator interpolator) {
            if (this.f2906 != interpolator) {
                this.f2906 = interpolator;
                this.f2910 = new OverScroller(C0690.this.getContext(), interpolator);
            }
            C0690.this.setScrollState(2);
            this.f2909 = 0;
            this.f2908 = 0;
            this.f2910.startScroll(0, 0, i, i2, i3);
            if (Build.VERSION.SDK_INT < 23) {
                this.f2910.computeScrollOffset();
            }
            m4782();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4788() {
            C0690.this.removeCallbacks(this);
            this.f2910.abortAnimation();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﹶ  reason: contains not printable characters */
    public void m4454() {
        int r0 = this.f2779.m3878();
        for (int i = 0; i < r0; i++) {
            View r2 = this.f2779.m3880(i);
            C0720 r3 = m4401(r2);
            if (!(r3 == null || r3.f2922 == null)) {
                View view = r3.f2922.f2914;
                int left = r2.getLeft();
                int top = r2.getTop();
                if (left != view.getLeft() || top != view.getTop()) {
                    view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
                }
            }
        }
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ᐧ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    private class C0713 extends C0693 {
        C0713() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4748() {
            C0690.this.m4392((String) null);
            C0690.this.f2797.f2893 = true;
            C0690.this.m4451();
            if (!C0690.this.f2777.m4040()) {
                C0690.this.requestLayout();
            }
        }
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$י  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public static class C0709 {

        /* renamed from: ʻ  reason: contains not printable characters */
        SparseArray<C0710> f2858 = new SparseArray<>();

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f2859 = 0;

        /* renamed from: android.support.v7.widget.ﹳﹳ$י$ʻ  reason: contains not printable characters */
        /* compiled from: RecyclerView */
        static class C0710 {

            /* renamed from: ʻ  reason: contains not printable characters */
            ArrayList<C0720> f2860 = new ArrayList<>();

            /* renamed from: ʼ  reason: contains not printable characters */
            int f2861 = 5;

            /* renamed from: ʽ  reason: contains not printable characters */
            long f2862 = 0;

            /* renamed from: ʾ  reason: contains not printable characters */
            long f2863 = 0;

            C0710() {
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4699() {
            for (int i = 0; i < this.f2858.size(); i++) {
                this.f2858.valueAt(i).f2860.clear();
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0720 m4698(int i) {
            C0710 r2 = this.f2858.get(i);
            if (r2 == null || r2.f2860.isEmpty()) {
                return null;
            }
            ArrayList<C0720> arrayList = r2.f2860;
            return arrayList.remove(arrayList.size() - 1);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4703(C0720 r4) {
            int r0 = r4.m4818();
            ArrayList<C0720> arrayList = m4696(r0).f2860;
            if (this.f2858.get(r0).f2861 > arrayList.size()) {
                r4.m4832();
                arrayList.add(r4);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public long m4697(long j, long j2) {
            return j == 0 ? j2 : ((j / 4) * 3) + (j2 / 4);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4700(int i, long j) {
            C0710 r3 = m4696(i);
            r3.f2862 = m4697(r3.f2862, j);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4706(int i, long j) {
            C0710 r3 = m4696(i);
            r3.f2863 = m4697(r3.f2863, j);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4704(int i, long j, long j2) {
            long j3 = m4696(i).f2862;
            return j3 == 0 || j + j3 < j2;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m4707(int i, long j, long j2) {
            long j3 = m4696(i).f2863;
            return j3 == 0 || j + j3 < j2;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4701(C0691 r1) {
            this.f2859++;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4705() {
            this.f2859--;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4702(C0691 r1, C0691 r2, boolean z) {
            if (r1 != null) {
                m4705();
            }
            if (!z && this.f2859 == 0) {
                m4699();
            }
            if (r2 != null) {
                m4701(r2);
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private C0710 m4696(int i) {
            C0710 r0 = this.f2858.get(i);
            if (r0 != null) {
                return r0;
            }
            C0710 r02 = new C0710();
            this.f2858.put(i, r02);
            return r02;
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static C0690 m4364(View view) {
        if (!(view instanceof ViewGroup)) {
            return null;
        }
        if (view instanceof C0690) {
            return (C0690) view;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            C0690 r3 = m4364(viewGroup.getChildAt(i));
            if (r3 != null) {
                return r3;
            }
        }
        return null;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    static void m4353(C0720 r3) {
        if (r3.f2915 != null) {
            View view = r3.f2915.get();
            while (view != null) {
                if (view != r3.f2914) {
                    ViewParent parent = view.getParent();
                    view = parent instanceof View ? (View) parent : null;
                } else {
                    return;
                }
            }
            r3.f2915 = null;
        }
    }

    /* access modifiers changed from: package-private */
    public long getNanoTime() {
        if (f2735) {
            return System.nanoTime();
        }
        return 0;
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ـ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public final class C0711 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final ArrayList<C0720> f2864 = new ArrayList<>();

        /* renamed from: ʼ  reason: contains not printable characters */
        ArrayList<C0720> f2865 = null;

        /* renamed from: ʽ  reason: contains not printable characters */
        final ArrayList<C0720> f2866 = new ArrayList<>();

        /* renamed from: ʾ  reason: contains not printable characters */
        int f2867 = 2;

        /* renamed from: ʿ  reason: contains not printable characters */
        C0709 f2868;

        /* renamed from: ˈ  reason: contains not printable characters */
        private final List<C0720> f2870 = Collections.unmodifiableList(this.f2864);

        /* renamed from: ˉ  reason: contains not printable characters */
        private int f2871 = 2;

        /* renamed from: ˊ  reason: contains not printable characters */
        private C0718 f2872;

        public C0711() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4715() {
            this.f2864.clear();
            m4736();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4716(int i) {
            this.f2871 = i;
            m4727();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4727() {
            this.f2867 = this.f2871 + (C0690.this.f2787 != null ? C0690.this.f2787.f2846 : 0);
            for (int size = this.f2866.size() - 1; size >= 0 && this.f2866.size() > this.f2867; size--) {
                m4737(size);
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public List<C0720> m4732() {
            return this.f2870;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4724(C0720 r8) {
            if (r8.m4827()) {
                return C0690.this.f2797.m4771();
            }
            if (r8.f2916 < 0 || r8.f2916 >= C0690.this.f2786.m4480()) {
                throw new IndexOutOfBoundsException("Inconsistency detected. Invalid view holder adapter position" + r8 + C0690.this.m4378());
            } else if (!C0690.this.f2797.m4771() && C0690.this.f2786.m4487(r8.f2916) != r8.m4818()) {
                return false;
            } else {
                if (!C0690.this.f2786.m4496() || r8.m4817() == C0690.this.f2786.m4481(r8.f2916)) {
                    return true;
                }
                return false;
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean m4709(C0720 r10, int i, int i2, long j) {
            r10.f2926 = C0690.this;
            int r2 = r10.m4818();
            long nanoTime = C0690.this.getNanoTime();
            if (j != Util.VLI_MAX && !this.f2868.m4707(r2, nanoTime, j)) {
                return false;
            }
            C0690.this.f2786.m4490(r10, i);
            this.f2868.m4706(r10.m4818(), C0690.this.getNanoTime() - nanoTime);
            m4710(r10);
            if (!C0690.this.f2797.m4771()) {
                return true;
            }
            r10.f2920 = i2;
            return true;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m4725(int i) {
            if (i < 0 || i >= C0690.this.f2797.m4775()) {
                throw new IndexOutOfBoundsException("invalid position " + i + ". State " + "item count is " + C0690.this.f2797.m4775() + C0690.this.m4378());
            } else if (!C0690.this.f2797.m4771()) {
                return i;
            } else {
                return C0690.this.f2777.m4036(i);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.ﹳﹳ.ـ.ʻ(int, boolean):android.view.View
         arg types: [int, int]
         candidates:
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.view.ViewGroup, boolean):void
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(int, int):void
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):void
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(int, boolean):android.view.View */
        /* renamed from: ʽ  reason: contains not printable characters */
        public View m4731(int i) {
            return m4714(i, false);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public View m4714(int i, boolean z) {
            return m4712(i, z, (long) Util.VLI_MAX).f2914;
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0037  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x005c  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x005f  */
        /* JADX WARNING: Removed duplicated region for block: B:78:0x01aa  */
        /* JADX WARNING: Removed duplicated region for block: B:81:0x01cf  */
        /* JADX WARNING: Removed duplicated region for block: B:94:0x0208  */
        /* JADX WARNING: Removed duplicated region for block: B:95:0x0216  */
        /* renamed from: ʻ  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.support.v7.widget.C0690.C0720 m4712(int r17, boolean r18, long r19) {
            /*
                r16 = this;
                r6 = r16
                r3 = r17
                r0 = r18
                if (r3 < 0) goto L_0x0239
                android.support.v7.widget.ﹳﹳ r1 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ᵔ r1 = r1.f2797
                int r1 = r1.m4775()
                if (r3 >= r1) goto L_0x0239
                android.support.v7.widget.ﹳﹳ r1 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ᵔ r1 = r1.f2797
                boolean r1 = r1.m4771()
                r2 = 0
                r7 = 1
                r8 = 0
                if (r1 == 0) goto L_0x0027
                android.support.v7.widget.ﹳﹳ$ﹳ r1 = r16.m4741(r17)
                if (r1 == 0) goto L_0x0028
                r4 = 1
                goto L_0x0029
            L_0x0027:
                r1 = r2
            L_0x0028:
                r4 = 0
            L_0x0029:
                if (r1 != 0) goto L_0x005d
                android.support.v7.widget.ﹳﹳ$ﹳ r1 = r16.m4726(r17, r18)
                if (r1 == 0) goto L_0x005d
                boolean r5 = r6.m4724(r1)
                if (r5 != 0) goto L_0x005c
                if (r0 != 0) goto L_0x005a
                r5 = 4
                r1.m4812(r5)
                boolean r5 = r1.m4819()
                if (r5 == 0) goto L_0x004e
                android.support.v7.widget.ﹳﹳ r5 = android.support.v7.widget.C0690.this
                android.view.View r9 = r1.f2914
                r5.removeDetachedView(r9, r8)
                r1.m4820()
                goto L_0x0057
            L_0x004e:
                boolean r5 = r1.m4821()
                if (r5 == 0) goto L_0x0057
                r1.m4822()
            L_0x0057:
                r6.m4729(r1)
            L_0x005a:
                r1 = r2
                goto L_0x005d
            L_0x005c:
                r4 = 1
            L_0x005d:
                if (r1 != 0) goto L_0x0189
                android.support.v7.widget.ﹳﹳ r5 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ˆ r5 = r5.f2777
                int r5 = r5.m4036(r3)
                if (r5 < 0) goto L_0x014c
                android.support.v7.widget.ﹳﹳ r9 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ʻ r9 = r9.f2786
                int r9 = r9.m4480()
                if (r5 >= r9) goto L_0x014c
                android.support.v7.widget.ﹳﹳ r9 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ʻ r9 = r9.f2786
                int r9 = r9.m4487(r5)
                android.support.v7.widget.ﹳﹳ r10 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ʻ r10 = r10.f2786
                boolean r10 = r10.m4496()
                if (r10 == 0) goto L_0x0096
                android.support.v7.widget.ﹳﹳ r1 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ʻ r1 = r1.f2786
                long r10 = r1.m4481(r5)
                android.support.v7.widget.ﹳﹳ$ﹳ r1 = r6.m4713(r10, r9, r0)
                if (r1 == 0) goto L_0x0096
                r1.f2916 = r5
                r4 = 1
            L_0x0096:
                if (r1 != 0) goto L_0x00eb
                android.support.v7.widget.ﹳﹳ$ᵢ r0 = r6.f2872
                if (r0 == 0) goto L_0x00eb
                android.view.View r0 = r0.m4776(r6, r3, r9)
                if (r0 == 0) goto L_0x00eb
                android.support.v7.widget.ﹳﹳ r1 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ﹳ r1 = r1.m4401(r0)
                if (r1 == 0) goto L_0x00ce
                boolean r0 = r1.m4813()
                if (r0 != 0) goto L_0x00b1
                goto L_0x00eb
            L_0x00b1:
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "getViewForPositionAndType returned a view that is ignored. You must call stopIgnoring before returning this view."
                r1.append(r2)
                android.support.v7.widget.ﹳﹳ r2 = android.support.v7.widget.C0690.this
                java.lang.String r2 = r2.m4378()
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                r0.<init>(r1)
                throw r0
            L_0x00ce:
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "getViewForPositionAndType returned a view which does not have a ViewHolder"
                r1.append(r2)
                android.support.v7.widget.ﹳﹳ r2 = android.support.v7.widget.C0690.this
                java.lang.String r2 = r2.m4378()
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                r0.<init>(r1)
                throw r0
            L_0x00eb:
                if (r1 != 0) goto L_0x0101
                android.support.v7.widget.ﹳﹳ$י r0 = r16.m4743()
                android.support.v7.widget.ﹳﹳ$ﹳ r1 = r0.m4698(r9)
                if (r1 == 0) goto L_0x0101
                r1.m4832()
                boolean r0 = android.support.v7.widget.C0690.f2731
                if (r0 == 0) goto L_0x0101
                r6.m4711(r1)
            L_0x0101:
                if (r1 != 0) goto L_0x0189
                android.support.v7.widget.ﹳﹳ r0 = android.support.v7.widget.C0690.this
                long r0 = r0.getNanoTime()
                r10 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                int r5 = (r19 > r10 ? 1 : (r19 == r10 ? 0 : -1))
                if (r5 == 0) goto L_0x011f
                android.support.v7.widget.ﹳﹳ$י r10 = r6.f2868
                r11 = r9
                r12 = r0
                r14 = r19
                boolean r5 = r10.m4704(r11, r12, r14)
                if (r5 != 0) goto L_0x011f
                return r2
            L_0x011f:
                android.support.v7.widget.ﹳﹳ r2 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ʻ r2 = r2.f2786
                android.support.v7.widget.ﹳﹳ r5 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ﹳ r2 = r2.m4493(r5, r9)
                boolean r5 = android.support.v7.widget.C0690.f2735
                if (r5 == 0) goto L_0x013e
                android.view.View r5 = r2.f2914
                android.support.v7.widget.ﹳﹳ r5 = android.support.v7.widget.C0690.m4364(r5)
                if (r5 == 0) goto L_0x013e
                java.lang.ref.WeakReference r10 = new java.lang.ref.WeakReference
                r10.<init>(r5)
                r2.f2915 = r10
            L_0x013e:
                android.support.v7.widget.ﹳﹳ r5 = android.support.v7.widget.C0690.this
                long r10 = r5.getNanoTime()
                android.support.v7.widget.ﹳﹳ$י r5 = r6.f2868
                long r10 = r10 - r0
                r5.m4700(r9, r10)
                r10 = r2
                goto L_0x018a
            L_0x014c:
                java.lang.IndexOutOfBoundsException r0 = new java.lang.IndexOutOfBoundsException
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "Inconsistency detected. Invalid item position "
                r1.append(r2)
                r1.append(r3)
                java.lang.String r2 = "(offset:"
                r1.append(r2)
                r1.append(r5)
                java.lang.String r2 = ")."
                r1.append(r2)
                java.lang.String r2 = "state:"
                r1.append(r2)
                android.support.v7.widget.ﹳﹳ r2 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ᵔ r2 = r2.f2797
                int r2 = r2.m4775()
                r1.append(r2)
                android.support.v7.widget.ﹳﹳ r2 = android.support.v7.widget.C0690.this
                java.lang.String r2 = r2.m4378()
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                r0.<init>(r1)
                throw r0
            L_0x0189:
                r10 = r1
            L_0x018a:
                r9 = r4
                if (r9 == 0) goto L_0x01c5
                android.support.v7.widget.ﹳﹳ r0 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ᵔ r0 = r0.f2797
                boolean r0 = r0.m4771()
                if (r0 != 0) goto L_0x01c5
                r0 = 8192(0x2000, float:1.14794E-41)
                boolean r1 = r10.m4810(r0)
                if (r1 == 0) goto L_0x01c5
                r10.m4804(r8, r0)
                android.support.v7.widget.ﹳﹳ r0 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ᵔ r0 = r0.f2797
                boolean r0 = r0.f2897
                if (r0 == 0) goto L_0x01c5
                int r0 = android.support.v7.widget.C0690.C0695.m4501(r10)
                r0 = r0 | 4096(0x1000, float:5.74E-42)
                android.support.v7.widget.ﹳﹳ r1 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ʿ r1 = r1.f2808
                android.support.v7.widget.ﹳﹳ r2 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ᵔ r2 = r2.f2797
                java.util.List r4 = r10.m4831()
                android.support.v7.widget.ﹳﹳ$ʿ$ʽ r0 = r1.m4503(r2, r10, r0, r4)
                android.support.v7.widget.ﹳﹳ r1 = android.support.v7.widget.C0690.this
                r1.m4390(r10, r0)
            L_0x01c5:
                android.support.v7.widget.ﹳﹳ r0 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ᵔ r0 = r0.f2797
                boolean r0 = r0.m4771()
                if (r0 == 0) goto L_0x01d8
                boolean r0 = r10.m4826()
                if (r0 == 0) goto L_0x01d8
                r10.f2920 = r3
                goto L_0x01eb
            L_0x01d8:
                boolean r0 = r10.m4826()
                if (r0 == 0) goto L_0x01ed
                boolean r0 = r10.m4825()
                if (r0 != 0) goto L_0x01ed
                boolean r0 = r10.m4824()
                if (r0 == 0) goto L_0x01eb
                goto L_0x01ed
            L_0x01eb:
                r0 = 0
                goto L_0x0200
            L_0x01ed:
                android.support.v7.widget.ﹳﹳ r0 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ˆ r0 = r0.f2777
                int r2 = r0.m4036(r3)
                r0 = r16
                r1 = r10
                r3 = r17
                r4 = r19
                boolean r0 = r0.m4709(r1, r2, r3, r4)
            L_0x0200:
                android.view.View r1 = r10.f2914
                android.view.ViewGroup$LayoutParams r1 = r1.getLayoutParams()
                if (r1 != 0) goto L_0x0216
                android.support.v7.widget.ﹳﹳ r1 = android.support.v7.widget.C0690.this
                android.view.ViewGroup$LayoutParams r1 = r1.generateDefaultLayoutParams()
                android.support.v7.widget.ﹳﹳ$ˊ r1 = (android.support.v7.widget.C0690.C0704) r1
                android.view.View r2 = r10.f2914
                r2.setLayoutParams(r1)
                goto L_0x022e
            L_0x0216:
                android.support.v7.widget.ﹳﹳ r2 = android.support.v7.widget.C0690.this
                boolean r2 = r2.checkLayoutParams(r1)
                if (r2 != 0) goto L_0x022c
                android.support.v7.widget.ﹳﹳ r2 = android.support.v7.widget.C0690.this
                android.view.ViewGroup$LayoutParams r1 = r2.generateLayoutParams(r1)
                android.support.v7.widget.ﹳﹳ$ˊ r1 = (android.support.v7.widget.C0690.C0704) r1
                android.view.View r2 = r10.f2914
                r2.setLayoutParams(r1)
                goto L_0x022e
            L_0x022c:
                android.support.v7.widget.ﹳﹳ$ˊ r1 = (android.support.v7.widget.C0690.C0704) r1
            L_0x022e:
                r1.f2854 = r10
                if (r9 == 0) goto L_0x0235
                if (r0 == 0) goto L_0x0235
                goto L_0x0236
            L_0x0235:
                r7 = 0
            L_0x0236:
                r1.f2857 = r7
                return r10
            L_0x0239:
                java.lang.IndexOutOfBoundsException r0 = new java.lang.IndexOutOfBoundsException
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "Invalid item position "
                r1.append(r2)
                r1.append(r3)
                java.lang.String r2 = "("
                r1.append(r2)
                r1.append(r3)
                java.lang.String r2 = "). Item count:"
                r1.append(r2)
                android.support.v7.widget.ﹳﹳ r2 = android.support.v7.widget.C0690.this
                android.support.v7.widget.ﹳﹳ$ᵔ r2 = r2.f2797
                int r2 = r2.m4775()
                r1.append(r2)
                android.support.v7.widget.ﹳﹳ r2 = android.support.v7.widget.C0690.this
                java.lang.String r2 = r2.m4378()
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                r0.<init>(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0690.C0711.m4712(int, boolean, long):android.support.v7.widget.ﹳﹳ$ﹳ");
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        private void m4710(C0720 r3) {
            if (C0690.this.m4444()) {
                View view = r3.f2914;
                if (C0414.m2235(view) == 0) {
                    C0414.m2218(view, 1);
                }
                if (!C0414.m2229(view)) {
                    r3.m4812(16384);
                    C0414.m2224(view, C0690.this.f2778.m4248());
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.view.ViewGroup, boolean):void
         arg types: [android.view.ViewGroup, int]
         candidates:
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(int, boolean):android.view.View
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(int, int):void
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):void
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.view.ViewGroup, boolean):void */
        /* renamed from: ˆ  reason: contains not printable characters */
        private void m4711(C0720 r2) {
            if (r2.f2914 instanceof ViewGroup) {
                m4708((ViewGroup) r2.f2914, false);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.view.ViewGroup, boolean):void
         arg types: [android.view.ViewGroup, int]
         candidates:
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(int, boolean):android.view.View
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(int, int):void
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):void
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.view.ViewGroup, boolean):void */
        /* renamed from: ʻ  reason: contains not printable characters */
        private void m4708(ViewGroup viewGroup, boolean z) {
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (childAt instanceof ViewGroup) {
                    m4708((ViewGroup) childAt, true);
                }
            }
            if (z) {
                if (viewGroup.getVisibility() == 4) {
                    viewGroup.setVisibility(0);
                    viewGroup.setVisibility(4);
                    return;
                }
                int visibility = viewGroup.getVisibility();
                viewGroup.setVisibility(4);
                viewGroup.setVisibility(visibility);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4723(View view) {
            C0720 r0 = C0690.m4357(view);
            if (r0.m4828()) {
                C0690.this.removeDetachedView(view, false);
            }
            if (r0.m4819()) {
                r0.m4820();
            } else if (r0.m4821()) {
                r0.m4822();
            }
            m4729(r0);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʾ  reason: contains not printable characters */
        public void m4736() {
            for (int size = this.f2866.size() - 1; size >= 0; size--) {
                m4737(size);
            }
            this.f2866.clear();
            if (C0690.f2735) {
                C0690.this.f2795.m4111();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):void
         arg types: [android.support.v7.widget.ﹳﹳ$ﹳ, int]
         candidates:
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.view.ViewGroup, boolean):void
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(int, boolean):android.view.View
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(int, int):void
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):void */
        /* access modifiers changed from: package-private */
        /* renamed from: ʾ  reason: contains not printable characters */
        public void m4737(int i) {
            m4722(this.f2866.get(i), true);
            this.f2866.remove(i);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):void
         arg types: [android.support.v7.widget.ﹳﹳ$ﹳ, int]
         candidates:
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.view.ViewGroup, boolean):void
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(int, boolean):android.view.View
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(int, int):void
          android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):void */
        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4729(C0720 r7) {
            boolean z;
            boolean z2 = false;
            if (r7.m4819() || r7.f2914.getParent() != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("Scrapped or attached views may not be recycled. isScrap:");
                sb.append(r7.m4819());
                sb.append(" isAttached:");
                if (r7.f2914.getParent() != null) {
                    z2 = true;
                }
                sb.append(z2);
                sb.append(C0690.this.m4378());
                throw new IllegalArgumentException(sb.toString());
            } else if (r7.m4828()) {
                throw new IllegalArgumentException("Tmp detached view should be removed from RecyclerView before it can be recycled: " + r7 + C0690.this.m4378());
            } else if (!r7.m4813()) {
                boolean r0 = r7.m4800();
                if ((C0690.this.f2786 != null && r0 && C0690.this.f2786.m4492(r7)) || r7.m4833()) {
                    if (this.f2867 <= 0 || r7.m4810(526)) {
                        z = false;
                    } else {
                        int size = this.f2866.size();
                        if (size >= this.f2867 && size > 0) {
                            m4737(0);
                            size--;
                        }
                        if (C0690.f2735 && size > 0 && !C0690.this.f2795.m4114(r7.f2916)) {
                            int i = size - 1;
                            while (i >= 0) {
                                if (!C0690.this.f2795.m4114(this.f2866.get(i).f2916)) {
                                    break;
                                }
                                i--;
                            }
                            size = i + 1;
                        }
                        this.f2866.add(size, r7);
                        z = true;
                    }
                    if (!z) {
                        m4722(r7, true);
                        z2 = true;
                    }
                } else {
                    z = false;
                }
                C0690.this.f2780.m3848(r7);
                if (!z && !z2 && r0) {
                    r7.f2926 = null;
                }
            } else {
                throw new IllegalArgumentException("Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle." + C0690.this.m4378());
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4722(C0720 r4, boolean z) {
            C0690.m4353(r4);
            if (r4.m4810(16384)) {
                r4.m4804(0, 16384);
                C0414.m2224(r4.f2914, (C0386) null);
            }
            if (z) {
                m4738(r4);
            }
            r4.f2926 = null;
            m4743().m4703(r4);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):boolean
         arg types: [android.support.v7.widget.ﹳﹳ$ﹳ, int]
         candidates:
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ$ـ):android.support.v7.widget.ﹳﹳ$ـ
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, int):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, boolean):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, boolean):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):boolean */
        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4730(View view) {
            C0720 r2 = C0690.m4357(view);
            C0711 unused = r2.f2929 = (C0711) null;
            boolean unused2 = r2.f2930 = false;
            r2.m4822();
            m4729(r2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, boolean):void
         arg types: [android.support.v7.widget.ﹳﹳ$ـ, int]
         candidates:
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ$ـ):android.support.v7.widget.ﹳﹳ$ـ
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):boolean
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, int):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, boolean):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, boolean):void */
        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public void m4735(View view) {
            C0720 r3 = C0690.m4357(view);
            if (!r3.m4810(12) && r3.m4834() && !C0690.this.m4409(r3)) {
                if (this.f2865 == null) {
                    this.f2865 = new ArrayList<>();
                }
                r3.m4807(this, true);
                this.f2865.add(r3);
            } else if (!r3.m4824() || r3.m4827() || C0690.this.f2786.m4496()) {
                r3.m4807(this, false);
                this.f2864.add(r3);
            } else {
                throw new IllegalArgumentException("Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool." + C0690.this.m4378());
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):boolean
         arg types: [android.support.v7.widget.ﹳﹳ$ﹳ, int]
         candidates:
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ$ـ):android.support.v7.widget.ﹳﹳ$ـ
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, int):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, boolean):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, boolean):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):boolean */
        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public void m4734(C0720 r2) {
            if (r2.f2930) {
                this.f2865.remove(r2);
            } else {
                this.f2864.remove(r2);
            }
            C0711 unused = r2.f2929 = (C0711) null;
            boolean unused2 = r2.f2930 = false;
            r2.m4822();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʿ  reason: contains not printable characters */
        public int m4739() {
            return this.f2864.size();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʿ  reason: contains not printable characters */
        public View m4740(int i) {
            return this.f2864.get(i).f2914;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˆ  reason: contains not printable characters */
        public void m4742() {
            this.f2864.clear();
            ArrayList<C0720> arrayList = this.f2865;
            if (arrayList != null) {
                arrayList.clear();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˆ  reason: contains not printable characters */
        public C0720 m4741(int i) {
            int size;
            int r10;
            ArrayList<C0720> arrayList = this.f2865;
            if (!(arrayList == null || (size = arrayList.size()) == 0)) {
                int i2 = 0;
                int i3 = 0;
                while (i3 < size) {
                    C0720 r5 = this.f2865.get(i3);
                    if (r5.m4821() || r5.m4814() != i) {
                        i3++;
                    } else {
                        r5.m4812(32);
                        return r5;
                    }
                }
                if (C0690.this.f2786.m4496() && (r10 = C0690.this.f2777.m4036(i)) > 0 && r10 < C0690.this.f2786.m4480()) {
                    long r52 = C0690.this.f2786.m4481(r10);
                    while (i2 < size) {
                        C0720 r102 = this.f2865.get(i2);
                        if (r102.m4821() || r102.m4817() != r52) {
                            i2++;
                        } else {
                            r102.m4812(32);
                            return r102;
                        }
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public C0720 m4726(int i, boolean z) {
            View r0;
            int size = this.f2864.size();
            int i2 = 0;
            int i3 = 0;
            while (i3 < size) {
                C0720 r3 = this.f2864.get(i3);
                if (r3.m4821() || r3.m4814() != i || r3.m4824() || (!C0690.this.f2797.f2894 && r3.m4827())) {
                    i3++;
                } else {
                    r3.m4812(32);
                    return r3;
                }
            }
            if (z || (r0 = C0690.this.f2779.m3882(i)) == null) {
                int size2 = this.f2866.size();
                while (i2 < size2) {
                    C0720 r2 = this.f2866.get(i2);
                    if (r2.m4824() || r2.m4814() != i) {
                        i2++;
                    } else {
                        if (!z) {
                            this.f2866.remove(i2);
                        }
                        return r2;
                    }
                }
                return null;
            }
            C0720 r6 = C0690.m4357(r0);
            C0690.this.f2779.m3887(r0);
            int r7 = C0690.this.f2779.m3879(r0);
            if (r7 != -1) {
                C0690.this.f2779.m3886(r7);
                m4735(r0);
                r6.m4812(8224);
                return r6;
            }
            throw new IllegalStateException("layout index should not be -1 after unhiding a view:" + r6 + C0690.this.m4378());
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public C0720 m4713(long j, int i, boolean z) {
            for (int size = this.f2864.size() - 1; size >= 0; size--) {
                C0720 r1 = this.f2864.get(size);
                if (r1.m4817() == j && !r1.m4821()) {
                    if (i == r1.m4818()) {
                        r1.m4812(32);
                        if (r1.m4827() && !C0690.this.f2797.m4771()) {
                            r1.m4804(2, 14);
                        }
                        return r1;
                    } else if (!z) {
                        this.f2864.remove(size);
                        C0690.this.removeDetachedView(r1.f2914, false);
                        m4730(r1.f2914);
                    }
                }
            }
            int size2 = this.f2866.size();
            while (true) {
                size2--;
                if (size2 < 0) {
                    return null;
                }
                C0720 r2 = this.f2866.get(size2);
                if (r2.m4817() == j) {
                    if (i == r2.m4818()) {
                        if (!z) {
                            this.f2866.remove(size2);
                        }
                        return r2;
                    } else if (!z) {
                        m4737(size2);
                        return null;
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʾ  reason: contains not printable characters */
        public void m4738(C0720 r2) {
            if (C0690.this.f2789 != null) {
                C0690.this.f2789.m4747(r2);
            }
            if (C0690.this.f2786 != null) {
                C0690.this.f2786.m4483(r2);
            }
            if (C0690.this.f2797 != null) {
                C0690.this.f2780.m3848(r2);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4719(C0691 r2, C0691 r3, boolean z) {
            m4715();
            m4743().m4702(r2, r3, z);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, boolean):void
         arg types: [int, int]
         candidates:
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ$ـ):android.support.v7.widget.ﹳﹳ$ـ
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):boolean
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, int):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, boolean):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, boolean):void */
        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4717(int i, int i2) {
            int i3;
            int i4;
            int i5;
            if (i < i2) {
                i5 = i;
                i4 = i2;
                i3 = -1;
            } else {
                i4 = i;
                i5 = i2;
                i3 = 1;
            }
            int size = this.f2866.size();
            for (int i6 = 0; i6 < size; i6++) {
                C0720 r6 = this.f2866.get(i6);
                if (r6 != null && r6.f2916 >= i5 && r6.f2916 <= i4) {
                    if (r6.f2916 == i) {
                        r6.m4806(i2 - i, false);
                    } else {
                        r6.m4806(i3, false);
                    }
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, boolean):void
         arg types: [int, int]
         candidates:
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ$ـ):android.support.v7.widget.ﹳﹳ$ـ
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, android.support.v7.widget.ﹳﹳ):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):boolean
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, int):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, boolean):void
          android.support.v7.widget.ﹳﹳ.ﹳ.ʻ(int, boolean):void */
        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4728(int i, int i2) {
            int size = this.f2866.size();
            for (int i3 = 0; i3 < size; i3++) {
                C0720 r2 = this.f2866.get(i3);
                if (r2 != null && r2.f2916 >= i) {
                    r2.m4806(i2, true);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4718(int i, int i2, boolean z) {
            int i3 = i + i2;
            for (int size = this.f2866.size() - 1; size >= 0; size--) {
                C0720 r2 = this.f2866.get(size);
                if (r2 != null) {
                    if (r2.f2916 >= i3) {
                        r2.m4806(-i2, z);
                    } else if (r2.f2916 >= i) {
                        r2.m4812(8);
                        m4737(size);
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4721(C0718 r1) {
            this.f2872 = r1;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4720(C0709 r2) {
            C0709 r0 = this.f2868;
            if (r0 != null) {
                r0.m4705();
            }
            this.f2868 = r2;
            if (r2 != null) {
                this.f2868.m4701(C0690.this.getAdapter());
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˈ  reason: contains not printable characters */
        public C0709 m4743() {
            if (this.f2868 == null) {
                this.f2868 = new C0709();
            }
            return this.f2868;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public void m4733(int i, int i2) {
            int i3;
            int i4 = i2 + i;
            for (int size = this.f2866.size() - 1; size >= 0; size--) {
                C0720 r1 = this.f2866.get(size);
                if (r1 != null && (i3 = r1.f2916) >= i && i3 < i4) {
                    r1.m4812(2);
                    m4737(size);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˉ  reason: contains not printable characters */
        public void m4744() {
            if (C0690.this.f2786 == null || !C0690.this.f2786.m4496()) {
                m4736();
                return;
            }
            int size = this.f2866.size();
            for (int i = 0; i < size; i++) {
                C0720 r2 = this.f2866.get(i);
                if (r2 != null) {
                    r2.m4812(6);
                    r2.m4808((Object) null);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˊ  reason: contains not printable characters */
        public void m4745() {
            int size = this.f2866.size();
            for (int i = 0; i < size; i++) {
                this.f2866.get(i).m4803();
            }
            int size2 = this.f2864.size();
            for (int i2 = 0; i2 < size2; i2++) {
                this.f2864.get(i2).m4803();
            }
            ArrayList<C0720> arrayList = this.f2865;
            if (arrayList != null) {
                int size3 = arrayList.size();
                for (int i3 = 0; i3 < size3; i3++) {
                    this.f2865.get(i3).m4803();
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˋ  reason: contains not printable characters */
        public void m4746() {
            int size = this.f2866.size();
            for (int i = 0; i < size; i++) {
                C0704 r2 = (C0704) this.f2866.get(i).f2914.getLayoutParams();
                if (r2 != null) {
                    r2.f2856 = true;
                }
            }
        }
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ʻ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public static abstract class C0691<VH extends C0720> {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final C0692 f2817 = new C0692();

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean f2818 = false;

        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract int m4480();

        /* renamed from: ʻ  reason: contains not printable characters */
        public long m4481(int i) {
            return -1;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4483(VH vh) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract void m4484(VH vh, int i);

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4486(C0690 r1) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m4487(int i) {
            return 0;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public abstract VH m4488(ViewGroup viewGroup, int i);

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4491(C0690 r1) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m4492(VH vh) {
            return false;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m4494(VH vh) {
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public void m4495(VH vh) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4485(VH vh, int i, List<Object> list) {
            m4484(vh, i);
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public final VH m4493(ViewGroup viewGroup, int i) {
            C0324.m1842("RV CreateView");
            VH r2 = m4488(viewGroup, i);
            r2.f2919 = i;
            C0324.m1841();
            return r2;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: VH
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        /* renamed from: ʼ  reason: contains not printable characters */
        public final void m4490(VH r3, int r4) {
            /*
                r2 = this;
                r3.f2916 = r4
                boolean r0 = r2.m4496()
                if (r0 == 0) goto L_0x000e
                long r0 = r2.m4481(r4)
                r3.f2918 = r0
            L_0x000e:
                r0 = 519(0x207, float:7.27E-43)
                r1 = 1
                r3.m4804(r1, r0)
                java.lang.String r0 = "RV OnBindView"
                android.support.v4.ʿ.C0324.m1842(r0)
                java.util.List r0 = r3.m4831()
                r2.m4485(r3, r4, r0)
                r3.m4830()
                android.view.View r3 = r3.f2914
                android.view.ViewGroup$LayoutParams r3 = r3.getLayoutParams()
                boolean r4 = r3 instanceof android.support.v7.widget.C0690.C0704
                if (r4 == 0) goto L_0x0031
                android.support.v7.widget.ﹳﹳ$ˊ r3 = (android.support.v7.widget.C0690.C0704) r3
                r3.f2856 = r1
            L_0x0031:
                android.support.v4.ʿ.C0324.m1841()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0690.C0691.m4490(android.support.v7.widget.ﹳﹳ$ﹳ, int):void");
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public final boolean m4496() {
            return this.f2818;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4482(C0693 r2) {
            this.f2817.registerObserver(r2);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4489(C0693 r2) {
            this.f2817.unregisterObserver(r2);
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public final void m4497() {
            this.f2817.m4498();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public void m4440(View view) {
        C0720 r0 = m4357(view);
        m4432(view);
        C0691 r1 = this.f2786;
        if (!(r1 == null || r0 == null)) {
            r1.m4495(r0);
        }
        List<C0705> list = this.f2809;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.f2809.get(size).m4689(view);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m4442(View view) {
        C0720 r0 = m4357(view);
        m4429(view);
        C0691 r1 = this.f2786;
        if (!(r1 == null || r0 == null)) {
            r1.m4494(r0);
        }
        List<C0705> list = this.f2809;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.f2809.get(size).m4688(view);
            }
        }
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ˉ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public static abstract class C0701 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final C0601.C0603 f2830 = new C0601.C0603() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public View m4675(int i) {
                return C0701.this.m4645(i);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public int m4673() {
                return C0701.this.m4672();
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public int m4676() {
                return C0701.this.m4670() - C0701.this.m4664();
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public int m4674(View view) {
                return C0701.this.m4644(view) - ((C0704) view.getLayoutParams()).leftMargin;
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public int m4677(View view) {
                return C0701.this.m4648(view) + ((C0704) view.getLayoutParams()).rightMargin;
            }
        };

        /* renamed from: ʼ  reason: contains not printable characters */
        private final C0601.C0603 f2831 = new C0601.C0603() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public View m4680(int i) {
                return C0701.this.m4645(i);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public int m4678() {
                return C0701.this.m4662();
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public int m4681() {
                return C0701.this.m4671() - C0701.this.m4593();
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public int m4679(View view) {
                return C0701.this.m4646(view) - ((C0704) view.getLayoutParams()).topMargin;
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public int m4682(View view) {
                return C0701.this.m4650(view) + ((C0704) view.getLayoutParams()).bottomMargin;
            }
        };

        /* renamed from: ʽ  reason: contains not printable characters */
        private boolean f2832 = true;

        /* renamed from: ʾ  reason: contains not printable characters */
        private boolean f2833 = true;

        /* renamed from: ʿ  reason: contains not printable characters */
        private int f2834;

        /* renamed from: ˆ  reason: contains not printable characters */
        private int f2835;

        /* renamed from: ˈ  reason: contains not printable characters */
        private int f2836;

        /* renamed from: ˉ  reason: contains not printable characters */
        private int f2837;

        /* renamed from: ٴ  reason: contains not printable characters */
        C0611 f2838;

        /* renamed from: ᐧ  reason: contains not printable characters */
        C0690 f2839;

        /* renamed from: ᴵ  reason: contains not printable characters */
        C0601 f2840 = new C0601(this.f2830);

        /* renamed from: ᵎ  reason: contains not printable characters */
        C0601 f2841 = new C0601(this.f2831);

        /* renamed from: ᵔ  reason: contains not printable characters */
        C0715 f2842;

        /* renamed from: ᵢ  reason: contains not printable characters */
        boolean f2843 = false;

        /* renamed from: ⁱ  reason: contains not printable characters */
        boolean f2844 = false;

        /* renamed from: ﹳ  reason: contains not printable characters */
        boolean f2845 = false;

        /* renamed from: ﹶ  reason: contains not printable characters */
        int f2846;

        /* renamed from: ﾞ  reason: contains not printable characters */
        boolean f2847;

        /* renamed from: android.support.v7.widget.ﹳﹳ$ˉ$ʻ  reason: contains not printable characters */
        /* compiled from: RecyclerView */
        public interface C0702 {
            /* renamed from: ʼ  reason: contains not printable characters */
            void m4683(int i, int i2);
        }

        /* renamed from: android.support.v7.widget.ﹳﹳ$ˉ$ʼ  reason: contains not printable characters */
        /* compiled from: RecyclerView */
        public static class C0703 {

            /* renamed from: ʻ  reason: contains not printable characters */
            public int f2850;

            /* renamed from: ʼ  reason: contains not printable characters */
            public int f2851;

            /* renamed from: ʽ  reason: contains not printable characters */
            public boolean f2852;

            /* renamed from: ʾ  reason: contains not printable characters */
            public boolean f2853;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m4545(int i, C0711 r2, C0717 r3) {
            return 0;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract C0704 m4547();

        /* renamed from: ʻ  reason: contains not printable characters */
        public View m4550(View view, int i, C0711 r3, C0717 r4) {
            return null;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4551(int i, int i2, C0717 r3, C0702 r4) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4552(int i, C0702 r2) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4555(Parcelable parcelable) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4557(C0691 r1, C0691 r2) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4563(C0717 r1) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4564(C0690 r1) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4565(C0690 r1, int i, int i2) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4566(C0690 r1, int i, int i2, int i3) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4581(C0704 r1) {
            return r1 != null;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4583(C0711 r1, C0717 r2, View view, int i, Bundle bundle) {
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4588(C0690 r1, ArrayList<View> arrayList, int i, int i2) {
            return false;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m4594(int i, C0711 r2, C0717 r3) {
            return 0;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4598(C0690 r1, int i, int i2) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m4603() {
            return false;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public int m4606(C0717 r1) {
            return 0;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public Parcelable m4607() {
            return null;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m4613(C0690 r1, int i, int i2) {
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public int m4618(C0711 r1, C0717 r2) {
            return 0;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public int m4619(C0717 r1) {
            return 0;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public View m4621(View view, int i) {
            return null;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public void m4622(int i) {
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public void m4624(C0690 r1) {
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public boolean m4625() {
            return false;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public int m4627(C0717 r1) {
            return 0;
        }

        @Deprecated
        /* renamed from: ʿ  reason: contains not printable characters */
        public void m4630(C0690 r1) {
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public boolean m4631() {
            return false;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public boolean m4632(C0711 r1, C0717 r2) {
            return false;
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public int m4634(C0717 r1) {
            return 0;
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public int m4640(C0717 r1) {
            return 0;
        }

        /* renamed from: ˉ  reason: contains not printable characters */
        public int m4643(C0717 r1) {
            return 0;
        }

        /* renamed from: ˎ  reason: contains not printable characters */
        public void m4651(int i) {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˎ  reason: contains not printable characters */
        public boolean m4652() {
            return false;
        }

        /* renamed from: ᵔ  reason: contains not printable characters */
        public int m4666() {
            return -1;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4597(C0690 r2) {
            if (r2 == null) {
                this.f2839 = null;
                this.f2838 = null;
                this.f2836 = 0;
                this.f2837 = 0;
            } else {
                this.f2839 = r2;
                this.f2838 = r2.f2779;
                this.f2836 = r2.getWidth();
                this.f2837 = r2.getHeight();
            }
            this.f2834 = 1073741824;
            this.f2835 = 1073741824;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public void m4609(int i, int i2) {
            this.f2836 = View.MeasureSpec.getSize(i);
            this.f2834 = View.MeasureSpec.getMode(i);
            if (this.f2834 == 0 && !C0690.f2732) {
                this.f2836 = 0;
            }
            this.f2837 = View.MeasureSpec.getSize(i2);
            this.f2835 = View.MeasureSpec.getMode(i2);
            if (this.f2835 == 0 && !C0690.f2732) {
                this.f2837 = 0;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʾ  reason: contains not printable characters */
        public void m4623(int i, int i2) {
            int r0 = m4667();
            if (r0 == 0) {
                this.f2839.m4421(i, i2);
                return;
            }
            int i3 = Integer.MAX_VALUE;
            int i4 = Integer.MAX_VALUE;
            int i5 = Integer.MIN_VALUE;
            int i6 = Integer.MIN_VALUE;
            for (int i7 = 0; i7 < r0; i7++) {
                View r6 = m4645(i7);
                Rect rect = this.f2839.f2783;
                m4574(r6, rect);
                if (rect.left < i3) {
                    i3 = rect.left;
                }
                if (rect.right > i5) {
                    i5 = rect.right;
                }
                if (rect.top < i4) {
                    i4 = rect.top;
                }
                if (rect.bottom > i6) {
                    i6 = rect.bottom;
                }
            }
            this.f2839.f2783.set(i3, i4, i5, i6);
            m4554(this.f2839.f2783, i, i2);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4554(Rect rect, int i, int i2) {
            m4637(m4534(i, rect.width() + m4672() + m4664(), m4605()), m4534(i2, rect.height() + m4662() + m4593(), m4633()));
        }

        /* renamed from: י  reason: contains not printable characters */
        public void m4656() {
            C0690 r0 = this.f2839;
            if (r0 != null) {
                r0.requestLayout();
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static int m4534(int i, int i2, int i3) {
            int mode = View.MeasureSpec.getMode(i);
            int size = View.MeasureSpec.getSize(i);
            if (mode != Integer.MIN_VALUE) {
                return mode != 1073741824 ? Math.max(i2, i3) : size;
            }
            return Math.min(size, Math.max(i2, i3));
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4579(String str) {
            C0690 r0 = this.f2839;
            if (r0 != null) {
                r0.m4392(str);
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m4616(boolean z) {
            this.f2845 = z;
        }

        /* renamed from: ـ  reason: contains not printable characters */
        public final boolean m4658() {
            return this.f2833;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public void m4612(C0690 r2) {
            this.f2844 = true;
            m4624(r2);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4599(C0690 r2, C0711 r3) {
            this.f2844 = false;
            m4568(r2, r3);
        }

        /* renamed from: ٴ  reason: contains not printable characters */
        public boolean m4660() {
            return this.f2844;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4592(Runnable runnable) {
            C0690 r0 = this.f2839;
            if (r0 != null) {
                return r0.removeCallbacks(runnable);
            }
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4568(C0690 r1, C0711 r2) {
            m4630(r1);
        }

        /* renamed from: ᐧ  reason: contains not printable characters */
        public boolean m4661() {
            C0690 r0 = this.f2839;
            return r0 != null && r0.f2781;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m4611(C0711 r1, C0717 r2) {
            Log.e("RecyclerView", "You must override onLayoutChildren(Recycler recycler, State state) ");
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0704 m4549(ViewGroup.LayoutParams layoutParams) {
            if (layoutParams instanceof C0704) {
                return new C0704((C0704) layoutParams);
            }
            if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                return new C0704((ViewGroup.MarginLayoutParams) layoutParams);
            }
            return new C0704(layoutParams);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0704 m4548(Context context, AttributeSet attributeSet) {
            return new C0704(context, attributeSet);
        }

        /* renamed from: ᴵ  reason: contains not printable characters */
        public boolean m4663() {
            C0715 r0 = this.f2842;
            return r0 != null && r0.m4762();
        }

        /* renamed from: ᵎ  reason: contains not printable characters */
        public int m4665() {
            return C0414.m2236(this.f2839);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4569(View view) {
            m4570(view, -1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, boolean):void
         arg types: [android.view.View, int, int]
         candidates:
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int):int
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int, android.view.View):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.graphics.Rect, int, int):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v4.ˉ.ʻ.ʼ):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.accessibility.AccessibilityEvent):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ˊ):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, android.graphics.Rect):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.view.View):boolean
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.os.Bundle):boolean
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, boolean):boolean
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, boolean):void */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4570(View view, int i) {
            m4541(view, i, true);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4600(View view) {
            m4601(view, -1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, boolean):void
         arg types: [android.view.View, int, int]
         candidates:
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int):int
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int, android.view.View):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.graphics.Rect, int, int):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v4.ˉ.ʻ.ʼ):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.accessibility.AccessibilityEvent):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ˊ):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, android.graphics.Rect):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.view.View):boolean
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.os.Bundle):boolean
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, boolean):boolean
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, boolean):void */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4601(View view, int i) {
            m4541(view, i, false);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m4541(View view, int i, boolean z) {
            C0720 r0 = C0690.m4357(view);
            if (z || r0.m4827()) {
                this.f2839.f2780.m3846(r0);
            } else {
                this.f2839.f2780.m3847(r0);
            }
            C0704 r7 = (C0704) view.getLayoutParams();
            if (r0.m4821() || r0.m4819()) {
                if (r0.m4819()) {
                    r0.m4820();
                } else {
                    r0.m4822();
                }
                this.f2838.m3875(view, i, view.getLayoutParams(), false);
            } else if (view.getParent() == this.f2839) {
                int r1 = this.f2838.m3879(view);
                if (i == -1) {
                    i = this.f2838.m3878();
                }
                if (r1 == -1) {
                    throw new IllegalStateException("Added View has RecyclerView as parent but view is not a real child. Unfiltered index:" + this.f2839.indexOfChild(view) + this.f2839.m4378());
                } else if (r1 != i) {
                    this.f2839.f2787.m4629(r1, i);
                }
            } else {
                this.f2838.m3876(view, i, false);
                r7.f2856 = true;
                C0715 r6 = this.f2842;
                if (r6 != null && r6.m4762()) {
                    this.f2842.m4760(view);
                }
            }
            if (r7.f2857) {
                r0.f2914.invalidate();
                r7.f2857 = false;
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m4614(View view) {
            this.f2838.m3874(view);
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public void m4636(int i) {
            if (m4645(i) != null) {
                this.f2838.m3873(i);
            }
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public int m4620(View view) {
            return ((C0704) view.getLayoutParams()).m4687();
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public View m4628(View view) {
            View r3;
            C0690 r0 = this.f2839;
            if (r0 == null || (r3 = r0.m4410(view)) == null || this.f2838.m3883(r3)) {
                return null;
            }
            return r3;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public View m4608(int i) {
            int r0 = m4667();
            for (int i2 = 0; i2 < r0; i2++) {
                View r2 = m4645(i2);
                C0720 r3 = C0690.m4357(r2);
                if (r3 != null && r3.m4814() == i && !r3.m4813() && (this.f2839.f2797.m4771() || !r3.m4827())) {
                    return r2;
                }
            }
            return null;
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public void m4642(int i) {
            m4537(i, m4645(i));
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m4537(int i, View view) {
            this.f2838.m3886(i);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4573(View view, int i, C0704 r5) {
            C0720 r0 = C0690.m4357(view);
            if (r0.m4827()) {
                this.f2839.f2780.m3846(r0);
            } else {
                this.f2839.f2780.m3847(r0);
            }
            this.f2838.m3875(view, i, r5, r0.m4827());
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m4615(View view, int i) {
            m4573(view, i, (C0704) view.getLayoutParams());
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public void m4629(int i, int i2) {
            View r0 = m4645(i);
            if (r0 != null) {
                m4642(i);
                m4615(r0, i2);
                return;
            }
            throw new IllegalArgumentException("Cannot move a child from non-existing index:" + i + this.f2839.toString());
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4576(View view, C0711 r2) {
            m4614(view);
            r2.m4723(view);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4553(int i, C0711 r3) {
            View r0 = m4645(i);
            m4636(i);
            r3.m4723(r0);
        }

        /* renamed from: ᵢ  reason: contains not printable characters */
        public int m4667() {
            C0611 r0 = this.f2838;
            if (r0 != null) {
                return r0.m3878();
            }
            return 0;
        }

        /* renamed from: ˉ  reason: contains not printable characters */
        public View m4645(int i) {
            C0611 r0 = this.f2838;
            if (r0 != null) {
                return r0.m3880(i);
            }
            return null;
        }

        /* renamed from: ⁱ  reason: contains not printable characters */
        public int m4668() {
            return this.f2834;
        }

        /* renamed from: ﹳ  reason: contains not printable characters */
        public int m4669() {
            return this.f2835;
        }

        /* renamed from: ﹶ  reason: contains not printable characters */
        public int m4670() {
            return this.f2836;
        }

        /* renamed from: ﾞ  reason: contains not printable characters */
        public int m4671() {
            return this.f2837;
        }

        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        public int m4672() {
            C0690 r0 = this.f2839;
            if (r0 != null) {
                return r0.getPaddingLeft();
            }
            return 0;
        }

        /* renamed from: ᐧᐧ  reason: contains not printable characters */
        public int m4662() {
            C0690 r0 = this.f2839;
            if (r0 != null) {
                return r0.getPaddingTop();
            }
            return 0;
        }

        /* renamed from: ᴵᴵ  reason: contains not printable characters */
        public int m4664() {
            C0690 r0 = this.f2839;
            if (r0 != null) {
                return r0.getPaddingRight();
            }
            return 0;
        }

        /* renamed from: ʻʻ  reason: contains not printable characters */
        public int m4593() {
            C0690 r0 = this.f2839;
            if (r0 != null) {
                return r0.getPaddingBottom();
            }
            return 0;
        }

        /* renamed from: ʽʽ  reason: contains not printable characters */
        public View m4617() {
            View focusedChild;
            C0690 r0 = this.f2839;
            if (r0 == null || (focusedChild = r0.getFocusedChild()) == null || this.f2838.m3883(focusedChild)) {
                return null;
            }
            return focusedChild;
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public void m4647(int i) {
            C0690 r0 = this.f2839;
            if (r0 != null) {
                r0.m4417(i);
            }
        }

        /* renamed from: ˋ  reason: contains not printable characters */
        public void m4649(int i) {
            C0690 r0 = this.f2839;
            if (r0 != null) {
                r0.m4412(i);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4558(C0711 r3) {
            for (int r0 = m4667() - 1; r0 >= 0; r0--) {
                m4539(r3, r0, m4645(r0));
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m4539(C0711 r3, int i, View view) {
            C0720 r0 = C0690.m4357(view);
            if (!r0.m4813()) {
                if (!r0.m4824() || r0.m4827() || this.f2839.f2786.m4496()) {
                    m4642(i);
                    r3.m4735(view);
                    this.f2839.f2780.m3849(r0);
                    return;
                }
                m4636(i);
                r3.m4729(r0);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4596(C0711 r7) {
            int r0 = r7.m4739();
            for (int i = r0 - 1; i >= 0; i--) {
                View r2 = r7.m4740(i);
                C0720 r3 = C0690.m4357(r2);
                if (!r3.m4813()) {
                    r3.m4809(false);
                    if (r3.m4828()) {
                        this.f2839.removeDetachedView(r2, false);
                    }
                    if (this.f2839.f2808 != null) {
                        this.f2839.f2808.m4513(r3);
                    }
                    r3.m4809(true);
                    r7.m4730(r2);
                }
            }
            r7.m4742();
            if (r0 > 0) {
                this.f2839.invalidate();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4589(View view, int i, int i2, C0704 r6) {
            return !this.f2832 || !m4542(view.getMeasuredWidth(), i, r6.width) || !m4542(view.getMeasuredHeight(), i2, r6.height);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m4604(View view, int i, int i2, C0704 r6) {
            return view.isLayoutRequested() || !this.f2832 || !m4542(view.getWidth(), i, r6.width) || !m4542(view.getHeight(), i2, r6.height);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private static boolean m4542(int i, int i2, int i3) {
            int mode = View.MeasureSpec.getMode(i2);
            int size = View.MeasureSpec.getSize(i2);
            if (i3 > 0 && i != i3) {
                return false;
            }
            if (mode == Integer.MIN_VALUE) {
                return size >= i;
            }
            if (mode != 0) {
                return mode == 1073741824 && size == i;
            }
            return true;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4571(View view, int i, int i2) {
            C0704 r0 = (C0704) view.getLayoutParams();
            Rect r1 = this.f2839.m4434(view);
            int i3 = i + r1.left + r1.right;
            int i4 = i2 + r1.top + r1.bottom;
            int r7 = m4535(m4670(), m4668(), m4672() + m4664() + r0.leftMargin + r0.rightMargin + i3, r0.width, m4625());
            int r8 = m4535(m4671(), m4669(), m4662() + m4593() + r0.topMargin + r0.bottomMargin + i4, r0.height, m4631());
            if (m4604(view, r7, r8, r0)) {
                view.measure(r7, r8);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static int m4535(int i, int i2, int i3, int i4, boolean z) {
            int i5;
            int i6 = i - i3;
            int i7 = 0;
            int max = Math.max(0, i6);
            if (z) {
                if (i4 < 0) {
                    if (i4 == -1) {
                        if (i2 == Integer.MIN_VALUE || (i2 != 0 && i2 == 1073741824)) {
                            i5 = max;
                        } else {
                            i2 = 0;
                            i5 = 0;
                        }
                        i7 = i2;
                        max = i5;
                        return View.MeasureSpec.makeMeasureSpec(max, i7);
                    }
                    max = 0;
                    return View.MeasureSpec.makeMeasureSpec(max, i7);
                }
            } else if (i4 < 0) {
                if (i4 == -1) {
                    i7 = i2;
                } else {
                    if (i4 == -2) {
                        if (i2 == Integer.MIN_VALUE || i2 == 1073741824) {
                            i7 = Integer.MIN_VALUE;
                        }
                    }
                    max = 0;
                }
                return View.MeasureSpec.makeMeasureSpec(max, i7);
            }
            max = i4;
            i7 = 1073741824;
            return View.MeasureSpec.makeMeasureSpec(max, i7);
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public int m4635(View view) {
            Rect rect = ((C0704) view.getLayoutParams()).f2855;
            return view.getMeasuredWidth() + rect.left + rect.right;
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public int m4641(View view) {
            Rect rect = ((C0704) view.getLayoutParams()).f2855;
            return view.getMeasuredHeight() + rect.top + rect.bottom;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4572(View view, int i, int i2, int i3, int i4) {
            C0704 r0 = (C0704) view.getLayoutParams();
            Rect rect = r0.f2855;
            view.layout(i + rect.left + r0.leftMargin, i2 + rect.top + r0.topMargin, (i3 - rect.right) - r0.rightMargin, (i4 - rect.bottom) - r0.bottomMargin);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4577(View view, boolean z, Rect rect) {
            Matrix matrix;
            if (z) {
                Rect rect2 = ((C0704) view.getLayoutParams()).f2855;
                rect.set(-rect2.left, -rect2.top, view.getWidth() + rect2.right, view.getHeight() + rect2.bottom);
            } else {
                rect.set(0, 0, view.getWidth(), view.getHeight());
            }
            if (!(this.f2839 == null || (matrix = view.getMatrix()) == null || matrix.isIdentity())) {
                RectF rectF = this.f2839.f2784;
                rectF.set(rect);
                matrix.mapRect(rectF);
                rect.set((int) Math.floor((double) rectF.left), (int) Math.floor((double) rectF.top), (int) Math.ceil((double) rectF.right), (int) Math.ceil((double) rectF.bottom));
            }
            rect.offset(view.getLeft(), view.getTop());
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4574(View view, Rect rect) {
            C0690.m4342(view, rect);
        }

        /* renamed from: ˉ  reason: contains not printable characters */
        public int m4644(View view) {
            return view.getLeft() - m4655(view);
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public int m4646(View view) {
            return view.getTop() - m4653(view);
        }

        /* renamed from: ˋ  reason: contains not printable characters */
        public int m4648(View view) {
            return view.getRight() + m4657(view);
        }

        /* renamed from: ˎ  reason: contains not printable characters */
        public int m4650(View view) {
            return view.getBottom() + m4654(view);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4602(View view, Rect rect) {
            C0690 r0 = this.f2839;
            if (r0 == null) {
                rect.set(0, 0, 0, 0);
            } else {
                rect.set(r0.m4434(view));
            }
        }

        /* renamed from: ˏ  reason: contains not printable characters */
        public int m4653(View view) {
            return ((C0704) view.getLayoutParams()).f2855.top;
        }

        /* renamed from: ˑ  reason: contains not printable characters */
        public int m4654(View view) {
            return ((C0704) view.getLayoutParams()).f2855.bottom;
        }

        /* renamed from: י  reason: contains not printable characters */
        public int m4655(View view) {
            return ((C0704) view.getLayoutParams()).f2855.left;
        }

        /* renamed from: ـ  reason: contains not printable characters */
        public int m4657(View view) {
            return ((C0704) view.getLayoutParams()).f2855.right;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private int[] m4543(C0690 r8, View view, Rect rect, boolean z) {
            int[] iArr = new int[2];
            int r11 = m4672();
            int r0 = m4662();
            int r1 = m4670() - m4664();
            int r2 = m4671() - m4593();
            int left = (view.getLeft() + rect.left) - view.getScrollX();
            int top = (view.getTop() + rect.top) - view.getScrollY();
            int width = rect.width() + left;
            int height = rect.height() + top;
            int i = left - r11;
            int min = Math.min(0, i);
            int i2 = top - r0;
            int min2 = Math.min(0, i2);
            int i3 = width - r1;
            int max = Math.max(0, i3);
            int max2 = Math.max(0, height - r2);
            if (m4665() != 1) {
                if (min == 0) {
                    min = Math.min(i, max);
                }
                max = min;
            } else if (max == 0) {
                max = Math.max(min, i3);
            }
            if (min2 == 0) {
                min2 = Math.min(i2, max2);
            }
            iArr[0] = max;
            iArr[1] = min2;
            return iArr;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean, boolean):boolean
         arg types: [android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean, int]
         candidates:
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int, int, boolean):int
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int, int, int):void
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, int, android.os.Bundle):boolean
          android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean, boolean):boolean */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4585(C0690 r7, View view, Rect rect, boolean z) {
            return m4586(r7, view, rect, z, false);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4586(C0690 r3, View view, Rect rect, boolean z, boolean z2) {
            int[] r4 = m4543(r3, view, rect, z);
            int i = r4[0];
            int i2 = r4[1];
            if ((z2 && !m4544(r3, i, i2)) || (i == 0 && i2 == 0)) {
                return false;
            }
            if (z) {
                r3.scrollBy(i, i2);
            } else {
                r3.m4380(i, i2);
            }
            return true;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4591(View view, boolean z, boolean z2) {
            boolean z3 = this.f2840.m3822(view, 24579) && this.f2841.m3822(view, 24579);
            return z ? z3 : !z3;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        private boolean m4544(C0690 r7, int i, int i2) {
            View focusedChild = r7.getFocusedChild();
            if (focusedChild == null) {
                return false;
            }
            int r1 = m4672();
            int r2 = m4662();
            int r3 = m4670() - m4664();
            int r4 = m4671() - m4593();
            Rect rect = this.f2839.f2783;
            m4574(focusedChild, rect);
            if (rect.left - i >= r3 || rect.right - i <= r1 || rect.top - i2 >= r4 || rect.bottom - i2 <= r2) {
                return false;
            }
            return true;
        }

        @Deprecated
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4587(C0690 r1, View view, View view2) {
            return m4663() || r1.m4445();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4584(C0690 r1, C0717 r2, View view, View view2) {
            return m4587(r1, view, view2);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4567(C0690 r1, int i, int i2, Object obj) {
            m4613(r1, i, i2);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4559(C0711 r1, C0717 r2, int i, int i2) {
            this.f2839.m4421(i, i2);
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public void m4637(int i, int i2) {
            this.f2839.setMeasuredDimension(i, i2);
        }

        /* renamed from: ʼʼ  reason: contains not printable characters */
        public int m4605() {
            return C0414.m2240(this.f2839);
        }

        /* renamed from: ʿʿ  reason: contains not printable characters */
        public int m4633() {
            return C0414.m2241(this.f2839);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʾʾ  reason: contains not printable characters */
        public void m4626() {
            C0715 r0 = this.f2842;
            if (r0 != null) {
                r0.m4756();
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4540(C0715 r2) {
            if (this.f2842 == r2) {
                this.f2842 = null;
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m4610(C0711 r3) {
            for (int r0 = m4667() - 1; r0 >= 0; r0--) {
                if (!C0690.m4357(m4645(r0)).m4813()) {
                    m4553(r0, r3);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4556(C0360 r3) {
            m4560(this.f2839.f2775, this.f2839.f2797, r3);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4560(C0711 r4, C0717 r5, C0360 r6) {
            if (this.f2839.canScrollVertically(-1) || this.f2839.canScrollHorizontally(-1)) {
                r6.m2009(8192);
                r6.m2042(true);
            }
            if (this.f2839.canScrollVertically(1) || this.f2839.canScrollHorizontally(1)) {
                r6.m2009(4096);
                r6.m2042(true);
            }
            r6.m2013(C0360.C0371.m2093(m4546(r4, r5), m4595(r4, r5), m4632(r4, r5), m4618(r4, r5)));
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4578(AccessibilityEvent accessibilityEvent) {
            m4562(this.f2839.f2775, this.f2839.f2797, accessibilityEvent);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4562(C0711 r2, C0717 r3, AccessibilityEvent accessibilityEvent) {
            C0690 r22 = this.f2839;
            if (r22 != null && accessibilityEvent != null) {
                boolean z = true;
                if (!r22.canScrollVertically(1) && !this.f2839.canScrollVertically(-1) && !this.f2839.canScrollHorizontally(-1) && !this.f2839.canScrollHorizontally(1)) {
                    z = false;
                }
                accessibilityEvent.setScrollable(z);
                if (this.f2839.f2786 != null) {
                    accessibilityEvent.setItemCount(this.f2839.f2786.m4480());
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4575(View view, C0360 r4) {
            C0720 r0 = C0690.m4357(view);
            if (r0 != null && !r0.m4827() && !this.f2838.m3883(r0.f2914)) {
                m4561(this.f2839.f2775, this.f2839.f2797, view, r4);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4561(C0711 r7, C0717 r8, View view, C0360 r10) {
            r10.m2020(C0360.C0372.m2094(m4631() ? m4620(view) : 0, 1, m4625() ? m4620(view) : 0, 1, false, false));
        }

        /* renamed from: ــ  reason: contains not printable characters */
        public void m4659() {
            this.f2843 = true;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m4546(C0711 r1, C0717 r2) {
            C0690 r12 = this.f2839;
            if (r12 == null || r12.f2786 == null || !m4631()) {
                return 1;
            }
            return this.f2839.f2786.m4480();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m4595(C0711 r1, C0717 r2) {
            C0690 r12 = this.f2839;
            if (r12 == null || r12.f2786 == null || !m4625()) {
                return 1;
            }
            return this.f2839.f2786.m4480();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4580(int i, Bundle bundle) {
            return m4582(this.f2839.f2775, this.f2839.f2797, i, bundle);
        }

        /* JADX WARNING: Removed duplicated region for block: B:24:0x0070 A[ADDED_TO_REGION] */
        /* renamed from: ʻ  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean m4582(android.support.v7.widget.C0690.C0711 r2, android.support.v7.widget.C0690.C0717 r3, int r4, android.os.Bundle r5) {
            /*
                r1 = this;
                android.support.v7.widget.ﹳﹳ r2 = r1.f2839
                r3 = 0
                if (r2 != 0) goto L_0x0006
                return r3
            L_0x0006:
                r5 = 4096(0x1000, float:5.74E-42)
                r0 = 1
                if (r4 == r5) goto L_0x0042
                r5 = 8192(0x2000, float:1.14794E-41)
                if (r4 == r5) goto L_0x0012
                r2 = 0
            L_0x0010:
                r4 = 0
                goto L_0x006e
            L_0x0012:
                r4 = -1
                boolean r2 = r2.canScrollVertically(r4)
                if (r2 == 0) goto L_0x0029
                int r2 = r1.m4671()
                int r5 = r1.m4662()
                int r2 = r2 - r5
                int r5 = r1.m4593()
                int r2 = r2 - r5
                int r2 = -r2
                goto L_0x002a
            L_0x0029:
                r2 = 0
            L_0x002a:
                android.support.v7.widget.ﹳﹳ r5 = r1.f2839
                boolean r4 = r5.canScrollHorizontally(r4)
                if (r4 == 0) goto L_0x0010
                int r4 = r1.m4670()
                int r5 = r1.m4672()
                int r4 = r4 - r5
                int r5 = r1.m4664()
                int r4 = r4 - r5
                int r4 = -r4
                goto L_0x006e
            L_0x0042:
                boolean r2 = r2.canScrollVertically(r0)
                if (r2 == 0) goto L_0x0057
                int r2 = r1.m4671()
                int r4 = r1.m4662()
                int r2 = r2 - r4
                int r4 = r1.m4593()
                int r2 = r2 - r4
                goto L_0x0058
            L_0x0057:
                r2 = 0
            L_0x0058:
                android.support.v7.widget.ﹳﹳ r4 = r1.f2839
                boolean r4 = r4.canScrollHorizontally(r0)
                if (r4 == 0) goto L_0x0010
                int r4 = r1.m4670()
                int r5 = r1.m4672()
                int r4 = r4 - r5
                int r5 = r1.m4664()
                int r4 = r4 - r5
            L_0x006e:
                if (r2 != 0) goto L_0x0073
                if (r4 != 0) goto L_0x0073
                return r3
            L_0x0073:
                android.support.v7.widget.ﹳﹳ r3 = r1.f2839
                r3.scrollBy(r4, r2)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0690.C0701.m4582(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, android.os.Bundle):boolean");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4590(View view, int i, Bundle bundle) {
            return m4583(this.f2839.f2775, this.f2839.f2797, view, i, bundle);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static C0703 m4536(Context context, AttributeSet attributeSet, int i, int i2) {
            C0703 r0 = new C0703();
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0744.C0746.RecyclerView, i, i2);
            r0.f2850 = obtainStyledAttributes.getInt(C0744.C0746.RecyclerView_android_orientation, 1);
            r0.f2851 = obtainStyledAttributes.getInt(C0744.C0746.RecyclerView_spanCount, 1);
            r0.f2852 = obtainStyledAttributes.getBoolean(C0744.C0746.RecyclerView_reverseLayout, false);
            r0.f2853 = obtainStyledAttributes.getBoolean(C0744.C0746.RecyclerView_stackFromEnd, false);
            obtainStyledAttributes.recycle();
            return r0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˆ  reason: contains not printable characters */
        public void m4638(C0690 r3) {
            m4609(View.MeasureSpec.makeMeasureSpec(r3.getWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(r3.getHeight(), 1073741824));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˆˆ  reason: contains not printable characters */
        public boolean m4639() {
            int r0 = m4667();
            for (int i = 0; i < r0; i++) {
                ViewGroup.LayoutParams layoutParams = m4645(i).getLayoutParams();
                if (layoutParams.width < 0 && layoutParams.height < 0) {
                    return true;
                }
            }
            return false;
        }
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ˈ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public static abstract class C0700 {
        @Deprecated
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4528(Canvas canvas, C0690 r2) {
        }

        @Deprecated
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4532(Canvas canvas, C0690 r2) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4533(Canvas canvas, C0690 r2, C0717 r3) {
            m4528(canvas, r2);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4529(Canvas canvas, C0690 r2, C0717 r3) {
            m4532(canvas, r2);
        }

        @Deprecated
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4530(Rect rect, int i, C0690 r3) {
            rect.set(0, 0, 0, 0);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4531(Rect rect, View view, C0690 r3, C0717 r4) {
            m4530(rect, ((C0704) view.getLayoutParams()).m4687(), r3);
        }
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ﹳ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public static abstract class C0720 {

        /* renamed from: ـ  reason: contains not printable characters */
        private static final List<Object> f2913 = Collections.EMPTY_LIST;

        /* renamed from: ʻ  reason: contains not printable characters */
        public final View f2914;

        /* renamed from: ʼ  reason: contains not printable characters */
        WeakReference<C0690> f2915;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f2916 = -1;

        /* renamed from: ʾ  reason: contains not printable characters */
        int f2917 = -1;

        /* renamed from: ʿ  reason: contains not printable characters */
        long f2918 = -1;

        /* renamed from: ˆ  reason: contains not printable characters */
        int f2919 = -1;

        /* renamed from: ˈ  reason: contains not printable characters */
        int f2920 = -1;

        /* renamed from: ˉ  reason: contains not printable characters */
        C0720 f2921 = null;

        /* renamed from: ˊ  reason: contains not printable characters */
        C0720 f2922 = null;

        /* renamed from: ˋ  reason: contains not printable characters */
        List<Object> f2923 = null;

        /* renamed from: ˎ  reason: contains not printable characters */
        List<Object> f2924 = null;

        /* renamed from: ˏ  reason: contains not printable characters */
        int f2925 = -1;

        /* renamed from: ˑ  reason: contains not printable characters */
        C0690 f2926;
        /* access modifiers changed from: private */

        /* renamed from: י  reason: contains not printable characters */
        public int f2927;

        /* renamed from: ٴ  reason: contains not printable characters */
        private int f2928 = 0;
        /* access modifiers changed from: private */

        /* renamed from: ᐧ  reason: contains not printable characters */
        public C0711 f2929 = null;
        /* access modifiers changed from: private */

        /* renamed from: ᴵ  reason: contains not printable characters */
        public boolean f2930 = false;

        /* renamed from: ᵎ  reason: contains not printable characters */
        private int f2931 = 0;

        public C0720(View view) {
            if (view != null) {
                this.f2914 = view;
                return;
            }
            throw new IllegalArgumentException("itemView may not be null");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4805(int i, int i2, boolean z) {
            m4812(8);
            m4806(i2, z);
            this.f2916 = i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4806(int i, boolean z) {
            if (this.f2917 == -1) {
                this.f2917 = this.f2916;
            }
            if (this.f2920 == -1) {
                this.f2920 = this.f2916;
            }
            if (z) {
                this.f2920 += i;
            }
            this.f2916 += i;
            if (this.f2914.getLayoutParams() != null) {
                ((C0704) this.f2914.getLayoutParams()).f2856 = true;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4803() {
            this.f2917 = -1;
            this.f2920 = -1;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4811() {
            if (this.f2917 == -1) {
                this.f2917 = this.f2916;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m4813() {
            return (this.f2927 & 128) != 0;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public final int m4814() {
            int i = this.f2920;
            return i == -1 ? this.f2916 : i;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public final int m4815() {
            C0690 r0 = this.f2926;
            if (r0 == null) {
                return -1;
            }
            return r0.m4414(this);
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public final int m4816() {
            return this.f2917;
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public final long m4817() {
            return this.f2918;
        }

        /* renamed from: ˉ  reason: contains not printable characters */
        public final int m4818() {
            return this.f2919;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˊ  reason: contains not printable characters */
        public boolean m4819() {
            return this.f2929 != null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˋ  reason: contains not printable characters */
        public void m4820() {
            this.f2929.m4734(this);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˎ  reason: contains not printable characters */
        public boolean m4821() {
            return (this.f2927 & 32) != 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˏ  reason: contains not printable characters */
        public void m4822() {
            this.f2927 &= -33;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˑ  reason: contains not printable characters */
        public void m4823() {
            this.f2927 &= -257;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4807(C0711 r1, boolean z) {
            this.f2929 = r1;
            this.f2930 = z;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: י  reason: contains not printable characters */
        public boolean m4824() {
            return (this.f2927 & 4) != 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ـ  reason: contains not printable characters */
        public boolean m4825() {
            return (this.f2927 & 2) != 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ٴ  reason: contains not printable characters */
        public boolean m4826() {
            return (this.f2927 & 1) != 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ᐧ  reason: contains not printable characters */
        public boolean m4827() {
            return (this.f2927 & 8) != 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4810(int i) {
            return (i & this.f2927) != 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ᴵ  reason: contains not printable characters */
        public boolean m4828() {
            return (this.f2927 & 256) != 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ᵎ  reason: contains not printable characters */
        public boolean m4829() {
            return (this.f2927 & 512) != 0 || m4824();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4804(int i, int i2) {
            this.f2927 = (i & i2) | (this.f2927 & (i2 ^ -1));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4812(int i) {
            this.f2927 = i | this.f2927;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4808(Object obj) {
            if (obj == null) {
                m4812((int) Util.BLOCK_HEADER_SIZE_MAX);
            } else if ((1024 & this.f2927) == 0) {
                m4801();
                this.f2923.add(obj);
            }
        }

        /* renamed from: ﾞ  reason: contains not printable characters */
        private void m4801() {
            if (this.f2923 == null) {
                this.f2923 = new ArrayList();
                this.f2924 = Collections.unmodifiableList(this.f2923);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ᵔ  reason: contains not printable characters */
        public void m4830() {
            List<Object> list = this.f2923;
            if (list != null) {
                list.clear();
            }
            this.f2927 &= -1025;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ᵢ  reason: contains not printable characters */
        public List<Object> m4831() {
            if ((this.f2927 & Util.BLOCK_HEADER_SIZE_MAX) != 0) {
                return f2913;
            }
            List<Object> list = this.f2923;
            if (list == null || list.size() == 0) {
                return f2913;
            }
            return this.f2924;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ⁱ  reason: contains not printable characters */
        public void m4832() {
            this.f2927 = 0;
            this.f2916 = -1;
            this.f2917 = -1;
            this.f2918 = -1;
            this.f2920 = -1;
            this.f2928 = 0;
            this.f2921 = null;
            this.f2922 = null;
            m4830();
            this.f2931 = 0;
            this.f2925 = -1;
            C0690.m4353(this);
        }

        /* access modifiers changed from: private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4792(C0690 r2) {
            this.f2931 = C0414.m2235(this.f2914);
            r2.m4397(this, 4);
        }

        /* access modifiers changed from: private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4796(C0690 r2) {
            r2.m4397(this, this.f2931);
            this.f2931 = 0;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("ViewHolder{" + Integer.toHexString(hashCode()) + " position=" + this.f2916 + " id=" + this.f2918 + ", oldPos=" + this.f2917 + ", pLpos:" + this.f2920);
            if (m4819()) {
                sb.append(" scrap ");
                sb.append(this.f2930 ? "[changeScrap]" : "[attachedScrap]");
            }
            if (m4824()) {
                sb.append(" invalid");
            }
            if (!m4826()) {
                sb.append(" unbound");
            }
            if (m4825()) {
                sb.append(" update");
            }
            if (m4827()) {
                sb.append(" removed");
            }
            if (m4813()) {
                sb.append(" ignored");
            }
            if (m4828()) {
                sb.append(" tmpDetached");
            }
            if (!m4833()) {
                sb.append(" not recyclable(" + this.f2928 + ")");
            }
            if (m4829()) {
                sb.append(" undefined adapter position");
            }
            if (this.f2914.getParent() == null) {
                sb.append(" no parent");
            }
            sb.append("}");
            return sb.toString();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public final void m4809(boolean z) {
            this.f2928 = z ? this.f2928 - 1 : this.f2928 + 1;
            int i = this.f2928;
            if (i < 0) {
                this.f2928 = 0;
                Log.e("View", "isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for " + this);
            } else if (!z && i == 1) {
                this.f2927 |= 16;
            } else if (z && this.f2928 == 0) {
                this.f2927 &= -17;
            }
        }

        /* renamed from: ﹳ  reason: contains not printable characters */
        public final boolean m4833() {
            return (this.f2927 & 16) == 0 && !C0414.m2232(this.f2914);
        }

        /* access modifiers changed from: private */
        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        public boolean m4802() {
            return (this.f2927 & 16) != 0;
        }

        /* access modifiers changed from: private */
        /* renamed from: ᐧᐧ  reason: contains not printable characters */
        public boolean m4800() {
            return (this.f2927 & 16) == 0 && C0414.m2232(this.f2914);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ﹶ  reason: contains not printable characters */
        public boolean m4834() {
            return (this.f2927 & 2) != 0;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m4397(C0720 r2, int i) {
        if (m4445()) {
            r2.f2925 = i;
            this.f2776.add(r2);
            return false;
        }
        C0414.m2218(r2.f2914, i);
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﾞ  reason: contains not printable characters */
    public void m4455() {
        int i;
        for (int size = this.f2776.size() - 1; size >= 0; size--) {
            C0720 r1 = this.f2776.get(size);
            if (r1.f2914.getParent() == this && !r1.m4813() && (i = r1.f2925) != -1) {
                C0414.m2218(r1.f2914, i);
                r1.f2925 = -1;
            }
        }
        this.f2776.clear();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public int m4414(C0720 r2) {
        if (r2.m4810(524) || !r2.m4826()) {
            return -1;
        }
        return this.f2777.m4038(r2.f2916);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4384(StateListDrawable stateListDrawable, Drawable drawable, StateListDrawable stateListDrawable2, Drawable drawable2) {
        if (stateListDrawable == null || drawable == null || stateListDrawable2 == null || drawable2 == null) {
            throw new IllegalArgumentException("Trying to set fast scroller without both required drawables." + m4378());
        }
        Resources resources = getContext().getResources();
        new C0640(this, stateListDrawable, drawable, stateListDrawable2, drawable2, resources.getDimensionPixelSize(C0744.C0745.fastscroll_default_thickness), resources.getDimensionPixelSize(C0744.C0745.fastscroll_minimum_range), resources.getDimensionPixelOffset(C0744.C0745.fastscroll_margin));
    }

    public void setNestedScrollingEnabled(boolean z) {
        getScrollingChildHelper().m2191(z);
    }

    public boolean isNestedScrollingEnabled() {
        return getScrollingChildHelper().m2192();
    }

    public boolean startNestedScroll(int i) {
        return getScrollingChildHelper().m2202(i);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean m4438(int i, int i2) {
        return getScrollingChildHelper().m2196(i, i2);
    }

    public void stopNestedScroll() {
        getScrollingChildHelper().m2203();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public void m4427(int i) {
        getScrollingChildHelper().m2204(i);
    }

    public boolean hasNestedScrollingParent() {
        return getScrollingChildHelper().m2201();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m4433(int i) {
        return getScrollingChildHelper().m2195(i);
    }

    public boolean dispatchNestedScroll(int i, int i2, int i3, int i4, int[] iArr) {
        return getScrollingChildHelper().m2197(i, i2, i3, i4, iArr);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m4394(int i, int i2, int i3, int i4, int[] iArr, int i5) {
        return getScrollingChildHelper().m2198(i, i2, i3, i4, iArr, i5);
    }

    public boolean dispatchNestedPreScroll(int i, int i2, int[] iArr, int[] iArr2) {
        return getScrollingChildHelper().m2199(i, i2, iArr, iArr2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m4396(int i, int i2, int[] iArr, int[] iArr2, int i3) {
        return getScrollingChildHelper().m2200(i, i2, iArr, iArr2, i3);
    }

    public boolean dispatchNestedFling(float f, float f2, boolean z) {
        return getScrollingChildHelper().m2194(f, f2, z);
    }

    public boolean dispatchNestedPreFling(float f, float f2) {
        return getScrollingChildHelper().m2193(f, f2);
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ˊ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public static class C0704 extends ViewGroup.MarginLayoutParams {

        /* renamed from: ʽ  reason: contains not printable characters */
        C0720 f2854;

        /* renamed from: ʾ  reason: contains not printable characters */
        final Rect f2855 = new Rect();

        /* renamed from: ʿ  reason: contains not printable characters */
        boolean f2856 = true;

        /* renamed from: ˆ  reason: contains not printable characters */
        boolean f2857 = false;

        public C0704(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public C0704(int i, int i2) {
            super(i, i2);
        }

        public C0704(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public C0704(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public C0704(C0704 r1) {
            super((ViewGroup.LayoutParams) r1);
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m4684() {
            return this.f2854.m4824();
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public boolean m4685() {
            return this.f2854.m4827();
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public boolean m4686() {
            return this.f2854.m4834();
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public int m4687() {
            return this.f2854.m4814();
        }
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ᵎ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public static abstract class C0715 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private int f2875;

        /* renamed from: ʼ  reason: contains not printable characters */
        private C0690 f2876;

        /* renamed from: ʽ  reason: contains not printable characters */
        private C0701 f2877;

        /* renamed from: ʾ  reason: contains not printable characters */
        private boolean f2878;

        /* renamed from: ʿ  reason: contains not printable characters */
        private boolean f2879;

        /* renamed from: ˆ  reason: contains not printable characters */
        private View f2880;

        /* renamed from: ˈ  reason: contains not printable characters */
        private final C0716 f2881;

        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract void m4758(int i, int i2, C0717 r3, C0716 r4);

        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract void m4759(View view, C0717 r2, C0716 r3);

        /* access modifiers changed from: protected */
        /* renamed from: ʿ  reason: contains not printable characters */
        public abstract void m4764();

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4757(int i) {
            this.f2875 = i;
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public final void m4756() {
            if (this.f2879) {
                m4764();
                int unused = this.f2876.f2797.f2904 = -1;
                this.f2880 = null;
                this.f2875 = -1;
                this.f2878 = false;
                this.f2879 = false;
                this.f2877.m4540(this);
                this.f2877 = null;
                this.f2876 = null;
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m4761() {
            return this.f2878;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m4762() {
            return this.f2879;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public int m4763() {
            return this.f2875;
        }

        /* access modifiers changed from: private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4753(int i, int i2) {
            C0690 r0 = this.f2876;
            if (!this.f2879 || this.f2875 == -1 || r0 == null) {
                m4756();
            }
            this.f2878 = false;
            View view = this.f2880;
            if (view != null) {
                if (m4755(view) == this.f2875) {
                    m4759(this.f2880, r0.f2797, this.f2881);
                    this.f2881.m4766(r0);
                    m4756();
                } else {
                    Log.e("RecyclerView", "Passed over target position while smooth scrolling.");
                    this.f2880 = null;
                }
            }
            if (this.f2879) {
                m4758(i, i2, r0.f2797, this.f2881);
                boolean r5 = this.f2881.m4767();
                this.f2881.m4766(r0);
                if (!r5) {
                    return;
                }
                if (this.f2879) {
                    this.f2878 = true;
                    r0.f2810.m4782();
                    return;
                }
                m4756();
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m4755(View view) {
            return this.f2876.m4422(view);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4760(View view) {
            if (m4755(view) == m4763()) {
                this.f2880 = view;
            }
        }

        /* renamed from: android.support.v7.widget.ﹳﹳ$ᵎ$ʻ  reason: contains not printable characters */
        /* compiled from: RecyclerView */
        public static class C0716 {

            /* renamed from: ʻ  reason: contains not printable characters */
            private int f2882;

            /* renamed from: ʼ  reason: contains not printable characters */
            private int f2883;

            /* renamed from: ʽ  reason: contains not printable characters */
            private int f2884;

            /* renamed from: ʾ  reason: contains not printable characters */
            private int f2885;

            /* renamed from: ʿ  reason: contains not printable characters */
            private Interpolator f2886;

            /* renamed from: ˆ  reason: contains not printable characters */
            private boolean f2887;

            /* renamed from: ˈ  reason: contains not printable characters */
            private int f2888;

            /* access modifiers changed from: package-private */
            /* renamed from: ʻ  reason: contains not printable characters */
            public boolean m4767() {
                return this.f2885 >= 0;
            }

            /* access modifiers changed from: package-private */
            /* renamed from: ʻ  reason: contains not printable characters */
            public void m4766(C0690 r6) {
                int i = this.f2885;
                if (i >= 0) {
                    this.f2885 = -1;
                    r6.m4379(i);
                    this.f2887 = false;
                } else if (this.f2887) {
                    m4765();
                    if (this.f2886 != null) {
                        r6.f2810.m4786(this.f2882, this.f2883, this.f2884, this.f2886);
                    } else if (this.f2884 == Integer.MIN_VALUE) {
                        r6.f2810.m4789(this.f2882, this.f2883);
                    } else {
                        r6.f2810.m4784(this.f2882, this.f2883, this.f2884);
                    }
                    this.f2888++;
                    if (this.f2888 > 10) {
                        Log.e("RecyclerView", "Smooth Scroll action is being updated too frequently. Make sure you are not changing it unless necessary");
                    }
                    this.f2887 = false;
                } else {
                    this.f2888 = 0;
                }
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            private void m4765() {
                if (this.f2886 != null && this.f2884 < 1) {
                    throw new IllegalStateException("If you provide an interpolator, you must set a positive duration");
                } else if (this.f2884 < 1) {
                    throw new IllegalStateException("Scroll duration must be a positive number");
                }
            }
        }
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ʼ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    static class C0692 extends Observable<C0693> {
        C0692() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4498() {
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((C0693) this.mObservers.get(size)).m4499();
            }
        }
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ᴵ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public static class C0714 extends C0355 {
        public static final Parcelable.Creator<C0714> CREATOR = new Parcelable.ClassLoaderCreator<C0714>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C0714 createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new C0714(parcel, classLoader);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0714 createFromParcel(Parcel parcel) {
                return new C0714(parcel, null);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0714[] newArray(int i) {
                return new C0714[i];
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        Parcelable f2874;

        C0714(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f2874 = parcel.readParcelable(classLoader == null ? C0701.class.getClassLoader() : classLoader);
        }

        C0714(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeParcelable(this.f2874, 0);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4749(C0714 r1) {
            this.f2874 = r1.f2874;
        }
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ᵔ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public static class C0717 {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f2889 = 0;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f2890 = 0;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f2891 = 1;

        /* renamed from: ʾ  reason: contains not printable characters */
        int f2892 = 0;

        /* renamed from: ʿ  reason: contains not printable characters */
        boolean f2893 = false;

        /* renamed from: ˆ  reason: contains not printable characters */
        boolean f2894 = false;

        /* renamed from: ˈ  reason: contains not printable characters */
        boolean f2895 = false;

        /* renamed from: ˉ  reason: contains not printable characters */
        boolean f2896 = false;

        /* renamed from: ˊ  reason: contains not printable characters */
        boolean f2897 = false;

        /* renamed from: ˋ  reason: contains not printable characters */
        boolean f2898 = false;

        /* renamed from: ˎ  reason: contains not printable characters */
        int f2899;

        /* renamed from: ˏ  reason: contains not printable characters */
        long f2900;

        /* renamed from: ˑ  reason: contains not printable characters */
        int f2901;

        /* renamed from: י  reason: contains not printable characters */
        int f2902;

        /* renamed from: ـ  reason: contains not printable characters */
        int f2903;
        /* access modifiers changed from: private */

        /* renamed from: ٴ  reason: contains not printable characters */
        public int f2904 = -1;

        /* renamed from: ᐧ  reason: contains not printable characters */
        private SparseArray<Object> f2905;

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4769(int i) {
            if ((this.f2891 & i) == 0) {
                throw new IllegalStateException("Layout state should be one of " + Integer.toBinaryString(i) + " but it is " + Integer.toBinaryString(this.f2891));
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4770(C0691 r2) {
            this.f2891 = 1;
            this.f2892 = r2.m4480();
            this.f2894 = false;
            this.f2895 = false;
            this.f2896 = false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4771() {
            return this.f2894;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m4772() {
            return this.f2898;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public int m4773() {
            return this.f2904;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public boolean m4774() {
            return this.f2904 != -1;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public int m4775() {
            return this.f2894 ? this.f2889 - this.f2890 : this.f2892;
        }

        public String toString() {
            return "State{mTargetPosition=" + this.f2904 + ", mData=" + this.f2905 + ", mItemCount=" + this.f2892 + ", mPreviousLayoutItemCount=" + this.f2889 + ", mDeletedInvisibleItemCountSincePreviousLayout=" + this.f2890 + ", mStructureChanged=" + this.f2893 + ", mInPreLayout=" + this.f2894 + ", mRunSimpleAnimations=" + this.f2897 + ", mRunPredictiveAnimations=" + this.f2898 + '}';
        }
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ˆ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    private class C0699 implements C0695.C0697 {
        C0699() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4527(C0720 r3) {
            r3.m4809(true);
            if (r3.f2921 != null && r3.f2922 == null) {
                r3.f2921 = null;
            }
            r3.f2922 = null;
            if (!r3.m4802() && !C0690.this.m4398(r3.f2914) && r3.m4828()) {
                C0690.this.removeDetachedView(r3.f2914, false);
            }
        }
    }

    /* renamed from: android.support.v7.widget.ﹳﹳ$ʿ  reason: contains not printable characters */
    /* compiled from: RecyclerView */
    public static abstract class C0695 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private C0697 f2819 = null;

        /* renamed from: ʼ  reason: contains not printable characters */
        private ArrayList<C0696> f2820 = new ArrayList<>();

        /* renamed from: ʽ  reason: contains not printable characters */
        private long f2821 = 120;

        /* renamed from: ʾ  reason: contains not printable characters */
        private long f2822 = 120;

        /* renamed from: ʿ  reason: contains not printable characters */
        private long f2823 = 250;

        /* renamed from: ˆ  reason: contains not printable characters */
        private long f2824 = 250;

        /* renamed from: android.support.v7.widget.ﹳﹳ$ʿ$ʻ  reason: contains not printable characters */
        /* compiled from: RecyclerView */
        public interface C0696 {
            /* renamed from: ʻ  reason: contains not printable characters */
            void m4523();
        }

        /* renamed from: android.support.v7.widget.ﹳﹳ$ʿ$ʼ  reason: contains not printable characters */
        /* compiled from: RecyclerView */
        interface C0697 {
            /* renamed from: ʻ  reason: contains not printable characters */
            void m4524(C0720 r1);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract void m4504();

        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract boolean m4506(C0720 r1, C0698 r2, C0698 r3);

        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract boolean m4507(C0720 r1, C0720 r2, C0698 r3, C0698 r4);

        /* renamed from: ʼ  reason: contains not printable characters */
        public abstract boolean m4509();

        /* renamed from: ʼ  reason: contains not printable characters */
        public abstract boolean m4510(C0720 r1, C0698 r2, C0698 r3);

        /* renamed from: ʽ  reason: contains not printable characters */
        public abstract boolean m4511(C0720 r1, C0698 r2, C0698 r3);

        /* renamed from: ʾ  reason: contains not printable characters */
        public abstract void m4512();

        /* renamed from: ʾ  reason: contains not printable characters */
        public abstract void m4513(C0720 r1);

        /* renamed from: ˈ  reason: contains not printable characters */
        public void m4518(C0720 r1) {
        }

        /* renamed from: ˉ  reason: contains not printable characters */
        public boolean m4520(C0720 r1) {
            return true;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public long m4514() {
            return this.f2823;
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public long m4515() {
            return this.f2821;
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public long m4517() {
            return this.f2822;
        }

        /* renamed from: ˉ  reason: contains not printable characters */
        public long m4519() {
            return this.f2824;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4505(C0697 r1) {
            this.f2819 = r1;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0698 m4503(C0717 r1, C0720 r2, int i, List<Object> list) {
            return m4522().m4525(r2);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0698 m4502(C0717 r1, C0720 r2) {
            return m4522().m4525(r2);
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        static int m4501(C0720 r3) {
            int r0 = r3.f2927 & 14;
            if (r3.m4824()) {
                return 4;
            }
            if ((r0 & 4) != 0) {
                return r0;
            }
            int r1 = r3.m4816();
            int r32 = r3.m4815();
            return (r1 == -1 || r32 == -1 || r1 == r32) ? r0 : r0 | InternalZipConstants.UFT8_NAMES_FLAG;
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public final void m4516(C0720 r2) {
            m4518(r2);
            C0697 r0 = this.f2819;
            if (r0 != null) {
                r0.m4524(r2);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4508(C0720 r1, List<Object> list) {
            return m4520(r1);
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m4521() {
            int size = this.f2820.size();
            for (int i = 0; i < size; i++) {
                this.f2820.get(i).m4523();
            }
            this.f2820.clear();
        }

        /* renamed from: ˋ  reason: contains not printable characters */
        public C0698 m4522() {
            return new C0698();
        }

        /* renamed from: android.support.v7.widget.ﹳﹳ$ʿ$ʽ  reason: contains not printable characters */
        /* compiled from: RecyclerView */
        public static class C0698 {

            /* renamed from: ʻ  reason: contains not printable characters */
            public int f2825;

            /* renamed from: ʼ  reason: contains not printable characters */
            public int f2826;

            /* renamed from: ʽ  reason: contains not printable characters */
            public int f2827;

            /* renamed from: ʾ  reason: contains not printable characters */
            public int f2828;

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0698 m4525(C0720 r2) {
                return m4526(r2, 0);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0698 m4526(C0720 r1, int i) {
                View view = r1.f2914;
                this.f2825 = view.getLeft();
                this.f2826 = view.getTop();
                this.f2827 = view.getRight();
                this.f2828 = view.getBottom();
                return this;
            }
        }
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i, int i2) {
        C0694 r0 = this.f2765;
        if (r0 == null) {
            return super.getChildDrawingOrder(i, i2);
        }
        return r0.m4500(i, i2);
    }

    private C0408 getScrollingChildHelper() {
        if (this.f2768 == null) {
            this.f2768 = new C0408(this);
        }
        return this.f2768;
    }
}
