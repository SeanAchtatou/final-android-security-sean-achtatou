package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.support.v4.ˉ.C0414;
import android.support.v4.ˉ.C0434;
import android.support.v4.ˉ.C0436;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: android.support.v7.widget.ʻ  reason: contains not printable characters */
/* compiled from: AbsActionBarView */
abstract class C0575 extends ViewGroup {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final C0576 f2233;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected final Context f2234;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected ActionMenuView f2235;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected C0614 f2236;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected int f2237;

    /* renamed from: ˆ  reason: contains not printable characters */
    protected C0434 f2238;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f2239;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f2240;

    /* renamed from: ʻ  reason: contains not printable characters */
    protected static int m3624(int i, int i2, boolean z) {
        return z ? i - i2 : i + i2;
    }

    C0575(Context context) {
        this(context, null);
    }

    C0575(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    C0575(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2233 = new C0576();
        TypedValue typedValue = new TypedValue();
        if (!context.getTheme().resolveAttribute(C0727.C0728.actionBarPopupTheme, typedValue, true) || typedValue.resourceId == 0) {
            this.f2234 = context;
        } else {
            this.f2234 = new ContextThemeWrapper(context, typedValue.resourceId);
        }
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(null, C0727.C0737.ActionBar, C0727.C0728.actionBarStyle, 0);
        setContentHeight(obtainStyledAttributes.getLayoutDimension(C0727.C0737.ActionBar_height, 0));
        obtainStyledAttributes.recycle();
        C0614 r0 = this.f2236;
        if (r0 != null) {
            r0.m3918(configuration);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.f2239 = false;
        }
        if (!this.f2239) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.f2239 = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.f2239 = false;
        }
        return true;
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.f2240 = false;
        }
        if (!this.f2240) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.f2240 = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.f2240 = false;
        }
        return true;
    }

    public void setContentHeight(int i) {
        this.f2237 = i;
        requestLayout();
    }

    public int getContentHeight() {
        return this.f2237;
    }

    public int getAnimatedVisibility() {
        if (this.f2238 != null) {
            return this.f2233.f2241;
        }
        return getVisibility();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0434 m3629(int i, long j) {
        C0434 r0 = this.f2238;
        if (r0 != null) {
            r0.m2383();
        }
        if (i == 0) {
            if (getVisibility() != 0) {
                setAlpha(0.0f);
            }
            C0434 r02 = C0414.m2242(this).m2376(1.0f);
            r02.m2377(j);
            r02.m2378(this.f2233.m3631(r02, i));
            return r02;
        }
        C0434 r03 = C0414.m2242(this).m2376(0.0f);
        r03.m2377(j);
        r03.m2378(this.f2233.m3631(r03, i));
        return r03;
    }

    public void setVisibility(int i) {
        if (i != getVisibility()) {
            C0434 r0 = this.f2238;
            if (r0 != null) {
                r0.m2383();
            }
            super.setVisibility(i);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3630() {
        C0614 r0 = this.f2236;
        if (r0 != null) {
            return r0.m3934();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m3627(View view, int i, int i2, int i3) {
        view.measure(View.MeasureSpec.makeMeasureSpec(i, Integer.MIN_VALUE), i2);
        return Math.max(0, (i - view.getMeasuredWidth()) - i3);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m3628(View view, int i, int i2, int i3, boolean z) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i4 = i2 + ((i3 - measuredHeight) / 2);
        if (z) {
            view.layout(i - measuredWidth, i4, i, measuredHeight + i4);
        } else {
            view.layout(i, i4, i + measuredWidth, measuredHeight + i4);
        }
        return z ? -measuredWidth : measuredWidth;
    }

    /* renamed from: android.support.v7.widget.ʻ$ʻ  reason: contains not printable characters */
    /* compiled from: AbsActionBarView */
    protected class C0576 implements C0436 {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f2241;

        /* renamed from: ʽ  reason: contains not printable characters */
        private boolean f2243 = false;

        protected C0576() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0576 m3631(C0434 r2, int i) {
            C0575.this.f2238 = r2;
            this.f2241 = i;
            return this;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3632(View view) {
            C0575.super.setVisibility(0);
            this.f2243 = false;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m3633(View view) {
            if (!this.f2243) {
                C0575 r2 = C0575.this;
                r2.f2238 = null;
                C0575.super.setVisibility(this.f2241);
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m3634(View view) {
            this.f2243 = true;
        }
    }
}
