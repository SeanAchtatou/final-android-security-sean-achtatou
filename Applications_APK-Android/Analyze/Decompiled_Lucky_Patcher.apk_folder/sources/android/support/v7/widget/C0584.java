package android.support.v7.widget;

import android.support.v7.widget.C0690;
import android.view.View;

/* renamed from: android.support.v7.widget.ʻʿ  reason: contains not printable characters */
/* compiled from: SimpleItemAnimator */
public abstract class C0584 extends C0690.C0695 {

    /* renamed from: ˉ  reason: contains not printable characters */
    boolean f2284 = true;

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract boolean m3681(C0690.C0720 r1);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract boolean m3682(C0690.C0720 r1, int i, int i2, int i3, int i4);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract boolean m3684(C0690.C0720 r1, C0690.C0720 r2, int i, int i2, int i3, int i4);

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract boolean m3687(C0690.C0720 r1);

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3689(C0690.C0720 r1, boolean z) {
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m3691(C0690.C0720 r1, boolean z) {
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public void m3699(C0690.C0720 r1) {
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m3700(C0690.C0720 r1) {
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m3701(C0690.C0720 r1) {
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public void m3702(C0690.C0720 r1) {
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public void m3703(C0690.C0720 r1) {
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    public void m3704(C0690.C0720 r1) {
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m3692(C0690.C0720 r2) {
        return !this.f2284 || r2.m4824();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3683(C0690.C0720 r7, C0690.C0695.C0698 r8, C0690.C0695.C0698 r9) {
        int i = r8.f2825;
        int i2 = r8.f2826;
        View view = r7.f2914;
        int left = r9 == null ? view.getLeft() : r9.f2825;
        int top = r9 == null ? view.getTop() : r9.f2826;
        if (r7.m4827() || (i == left && i2 == top)) {
            return m3681(r7);
        }
        view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
        return m3682(r7, i, i2, left, top);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m3688(C0690.C0720 r9, C0690.C0695.C0698 r10, C0690.C0695.C0698 r11) {
        if (r10 == null || (r10.f2825 == r11.f2825 && r10.f2826 == r11.f2826)) {
            return m3687(r9);
        }
        return m3682(r9, r10.f2825, r10.f2826, r11.f2825, r11.f2826);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m3690(C0690.C0720 r7, C0690.C0695.C0698 r8, C0690.C0695.C0698 r9) {
        if (r8.f2825 == r9.f2825 && r8.f2826 == r9.f2826) {
            m3694(r7);
            return false;
        }
        return m3682(r7, r8.f2825, r8.f2826, r9.f2825, r9.f2826);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3685(C0690.C0720 r8, C0690.C0720 r9, C0690.C0695.C0698 r10, C0690.C0695.C0698 r11) {
        int i;
        int i2;
        int i3 = r10.f2825;
        int i4 = r10.f2826;
        if (r9.m4813()) {
            int i5 = r10.f2825;
            i = r10.f2826;
            i2 = i5;
        } else {
            i2 = r11.f2825;
            i = r11.f2826;
        }
        return m3684(r8, r9, i3, i4, i2, i);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m3693(C0690.C0720 r1) {
        m3700(r1);
        m4516(r1);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m3694(C0690.C0720 r1) {
        m3704(r1);
        m4516(r1);
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m3695(C0690.C0720 r1) {
        m3702(r1);
        m4516(r1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m3680(C0690.C0720 r1, boolean z) {
        m3691(r1, z);
        m4516(r1);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final void m3696(C0690.C0720 r1) {
        m3699(r1);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m3697(C0690.C0720 r1) {
        m3703(r1);
    }

    /* renamed from: י  reason: contains not printable characters */
    public final void m3698(C0690.C0720 r1) {
        m3701(r1);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m3686(C0690.C0720 r1, boolean z) {
        m3689(r1, z);
    }
}
