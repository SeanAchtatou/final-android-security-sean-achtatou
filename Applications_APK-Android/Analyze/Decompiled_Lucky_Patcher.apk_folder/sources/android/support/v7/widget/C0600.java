package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.app.C0462;
import java.lang.ref.WeakReference;

/* renamed from: android.support.v7.widget.ʻᐧ  reason: contains not printable characters */
/* compiled from: VectorEnabledTintResources */
public class C0600 extends Resources {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final WeakReference<Context> f2368;

    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m3819() {
        return C0462.m2512() && Build.VERSION.SDK_INT <= 20;
    }

    public C0600(Context context, Resources resources) {
        super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        this.f2368 = new WeakReference<>(context);
    }

    public Drawable getDrawable(int i) {
        Context context = this.f2368.get();
        if (context != null) {
            return C0656.m4164().m4185(context, this, i);
        }
        return super.getDrawable(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final Drawable m3820(int i) {
        return super.getDrawable(i);
    }
}
