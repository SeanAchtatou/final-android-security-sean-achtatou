package android.support.v7.widget;

import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.widget.C0159;
import android.support.v4.ˉ.C0355;
import android.support.v7.view.C0531;
import android.support.v7.ʻ.C0727;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import java.lang.reflect.Method;
import java.util.WeakHashMap;
import net.lingala.zip4j.util.InternalZipConstants;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

public class SearchView extends C0652 implements C0531 {

    /* renamed from: ˊ  reason: contains not printable characters */
    static final C0558 f2065 = new C0558();

    /* renamed from: ʻ  reason: contains not printable characters */
    final SearchAutoComplete f2066;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private boolean f2067;

    /* renamed from: ʼ  reason: contains not printable characters */
    final ImageView f2068;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private CharSequence f2069;

    /* renamed from: ʽ  reason: contains not printable characters */
    final ImageView f2070;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private boolean f2071;

    /* renamed from: ʾ  reason: contains not printable characters */
    final ImageView f2072;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private boolean f2073;

    /* renamed from: ʿ  reason: contains not printable characters */
    final ImageView f2074;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private boolean f2075;

    /* renamed from: ˆ  reason: contains not printable characters */
    View.OnFocusChangeListener f2076;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private boolean f2077;

    /* renamed from: ˈ  reason: contains not printable characters */
    C0159 f2078;

    /* renamed from: ˈˈ  reason: contains not printable characters */
    private boolean f2079;

    /* renamed from: ˉ  reason: contains not printable characters */
    SearchableInfo f2080;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private CharSequence f2081;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private Bundle f2082;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final View f2083;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private int f2084;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final View f2085;

    /* renamed from: ˎˎ  reason: contains not printable characters */
    private Runnable f2086;

    /* renamed from: ˏ  reason: contains not printable characters */
    private C0563 f2087;

    /* renamed from: ˏˏ  reason: contains not printable characters */
    private final Runnable f2088;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Rect f2089;

    /* renamed from: ˑˑ  reason: contains not printable characters */
    private final WeakHashMap<String, Drawable.ConstantState> f2090;

    /* renamed from: י  reason: contains not printable characters */
    private Rect f2091;

    /* renamed from: ـ  reason: contains not printable characters */
    private int[] f2092;

    /* renamed from: ــ  reason: contains not printable characters */
    private int f2093;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int[] f2094;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final ImageView f2095;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private View.OnClickListener f2096;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final Drawable f2097;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private boolean f2098;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private final int f2099;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private final int f2100;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private final Intent f2101;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private final Intent f2102;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private final CharSequence f2103;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private C0560 f2104;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private C0559 f2105;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private C0561 f2106;

    /* renamed from: android.support.v7.widget.SearchView$ʼ  reason: contains not printable characters */
    public interface C0559 {
        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m3414();
    }

    /* renamed from: android.support.v7.widget.SearchView$ʽ  reason: contains not printable characters */
    public interface C0560 {
        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m3415(String str);
    }

    /* renamed from: android.support.v7.widget.SearchView$ʾ  reason: contains not printable characters */
    public interface C0561 {
    }

    /* access modifiers changed from: package-private */
    public int getSuggestionRowLayout() {
        return this.f2099;
    }

    /* access modifiers changed from: package-private */
    public int getSuggestionCommitIconResId() {
        return this.f2100;
    }

    public void setSearchableInfo(SearchableInfo searchableInfo) {
        this.f2080 = searchableInfo;
        if (this.f2080 != null) {
            m3395();
            m3394();
        }
        this.f2077 = m3389();
        if (this.f2077) {
            this.f2066.setPrivateImeOptions("nm");
        }
        m3384(m3402());
    }

    public void setAppSearchData(Bundle bundle) {
        this.f2082 = bundle;
    }

    public void setImeOptions(int i) {
        this.f2066.setImeOptions(i);
    }

    public int getImeOptions() {
        return this.f2066.getImeOptions();
    }

    public void setInputType(int i) {
        this.f2066.setInputType(i);
    }

    public int getInputType() {
        return this.f2066.getInputType();
    }

    public boolean requestFocus(int i, Rect rect) {
        if (this.f2073 || !isFocusable()) {
            return false;
        }
        if (m3402()) {
            return super.requestFocus(i, rect);
        }
        boolean requestFocus = this.f2066.requestFocus(i, rect);
        if (requestFocus) {
            m3384(false);
        }
        return requestFocus;
    }

    public void clearFocus() {
        this.f2073 = true;
        super.clearFocus();
        this.f2066.clearFocus();
        this.f2066.setImeVisibility(false);
        this.f2073 = false;
    }

    public void setOnQueryTextListener(C0560 r1) {
        this.f2104 = r1;
    }

    public void setOnCloseListener(C0559 r1) {
        this.f2105 = r1;
    }

    public void setOnQueryTextFocusChangeListener(View.OnFocusChangeListener onFocusChangeListener) {
        this.f2076 = onFocusChangeListener;
    }

    public void setOnSuggestionListener(C0561 r1) {
        this.f2106 = r1;
    }

    public void setOnSearchClickListener(View.OnClickListener onClickListener) {
        this.f2096 = onClickListener;
    }

    public CharSequence getQuery() {
        return this.f2066.getText();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3400(CharSequence charSequence, boolean z) {
        this.f2066.setText(charSequence);
        if (charSequence != null) {
            SearchAutoComplete searchAutoComplete = this.f2066;
            searchAutoComplete.setSelection(searchAutoComplete.length());
            this.f2081 = charSequence;
        }
        if (z && !TextUtils.isEmpty(charSequence)) {
            m3403();
        }
    }

    public void setQueryHint(CharSequence charSequence) {
        this.f2069 = charSequence;
        m3394();
    }

    public CharSequence getQueryHint() {
        CharSequence charSequence = this.f2069;
        if (charSequence != null) {
            return charSequence;
        }
        SearchableInfo searchableInfo = this.f2080;
        if (searchableInfo == null || searchableInfo.getHintId() == 0) {
            return this.f2103;
        }
        return getContext().getText(this.f2080.getHintId());
    }

    public void setIconifiedByDefault(boolean z) {
        if (this.f2098 != z) {
            this.f2098 = z;
            m3384(z);
            m3394();
        }
    }

    public void setIconified(boolean z) {
        if (z) {
            m3404();
        } else {
            m3405();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m3402() {
        return this.f2067;
    }

    public void setSubmitButtonEnabled(boolean z) {
        this.f2071 = z;
        m3384(m3402());
    }

    public void setQueryRefinementEnabled(boolean z) {
        this.f2075 = z;
        C0159 r0 = this.f2078;
        if (r0 instanceof C0585) {
            ((C0585) r0).m3724(z ? 2 : 1);
        }
    }

    public void setSuggestionsAdapter(C0159 r2) {
        this.f2078 = r2;
        this.f2066.setAdapter(this.f2078);
    }

    public C0159 getSuggestionsAdapter() {
        return this.f2078;
    }

    public void setMaxWidth(int i) {
        this.f2093 = i;
        requestLayout();
    }

    public int getMaxWidth() {
        return this.f2093;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        if (m3402()) {
            super.onMeasure(i, i2);
            return;
        }
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        if (mode == Integer.MIN_VALUE) {
            int i4 = this.f2093;
            size = i4 > 0 ? Math.min(i4, size) : Math.min(getPreferredWidth(), size);
        } else if (mode == 0) {
            size = this.f2093;
            if (size <= 0) {
                size = getPreferredWidth();
            }
        } else if (mode == 1073741824 && (i3 = this.f2093) > 0) {
            size = Math.min(i3, size);
        }
        int mode2 = View.MeasureSpec.getMode(i2);
        int size2 = View.MeasureSpec.getSize(i2);
        if (mode2 == Integer.MIN_VALUE) {
            size2 = Math.min(getPreferredHeight(), size2);
        } else if (mode2 == 0) {
            size2 = getPreferredHeight();
        }
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), View.MeasureSpec.makeMeasureSpec(size2, 1073741824));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (z) {
            m3383(this.f2066, this.f2089);
            this.f2091.set(this.f2089.left, 0, this.f2089.right, i4 - i2);
            C0563 r2 = this.f2087;
            if (r2 == null) {
                this.f2087 = new C0563(this.f2091, this.f2089, this.f2066);
                setTouchDelegate(this.f2087);
                return;
            }
            r2.m3419(this.f2091, this.f2089);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3383(View view, Rect rect) {
        view.getLocationInWindow(this.f2092);
        getLocationInWindow(this.f2094);
        int[] iArr = this.f2092;
        int i = iArr[1];
        int[] iArr2 = this.f2094;
        int i2 = i - iArr2[1];
        int i3 = iArr[0] - iArr2[0];
        rect.set(i3, i2, view.getWidth() + i3, view.getHeight() + i2);
    }

    private int getPreferredWidth() {
        return getContext().getResources().getDimensionPixelSize(C0727.C0731.abc_search_view_preferred_width);
    }

    private int getPreferredHeight() {
        return getContext().getResources().getDimensionPixelSize(C0727.C0731.abc_search_view_preferred_height);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3384(boolean z) {
        this.f2067 = z;
        int i = 8;
        boolean z2 = false;
        int i2 = z ? 0 : 8;
        boolean z3 = !TextUtils.isEmpty(this.f2066.getText());
        this.f2068.setVisibility(i2);
        m3387(z3);
        this.f2083.setVisibility(z ? 8 : 0);
        if (this.f2095.getDrawable() != null && !this.f2098) {
            i = 0;
        }
        this.f2095.setVisibility(i);
        m3392();
        if (!z3) {
            z2 = true;
        }
        m3388(z2);
        m3391();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean m3389() {
        SearchableInfo searchableInfo = this.f2080;
        if (searchableInfo == null || !searchableInfo.getVoiceSearchEnabled()) {
            return false;
        }
        Intent intent = null;
        if (this.f2080.getVoiceSearchLaunchWebSearch()) {
            intent = this.f2101;
        } else if (this.f2080.getVoiceSearchLaunchRecognizer()) {
            intent = this.f2102;
        }
        if (intent == null || getContext().getPackageManager().resolveActivity(intent, InternalZipConstants.MIN_SPLIT_LENGTH) == null) {
            return false;
        }
        return true;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean m3390() {
        return (this.f2071 || this.f2077) && !m3402();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m3387(boolean z) {
        this.f2070.setVisibility((!this.f2071 || !m3390() || !hasFocus() || (!z && this.f2077)) ? 8 : 0);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private void m3391() {
        this.f2085.setVisibility((!m3390() || !(this.f2070.getVisibility() == 0 || this.f2074.getVisibility() == 0)) ? 8 : 0);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m3392() {
        boolean z = true;
        boolean z2 = !TextUtils.isEmpty(this.f2066.getText());
        int i = 0;
        if (!z2 && (!this.f2098 || this.f2079)) {
            z = false;
        }
        ImageView imageView = this.f2072;
        if (!z) {
            i = 8;
        }
        imageView.setVisibility(i);
        Drawable drawable = this.f2072.getDrawable();
        if (drawable != null) {
            drawable.setState(z2 ? ENABLED_STATE_SET : EMPTY_STATE_SET);
        }
    }

    /* renamed from: י  reason: contains not printable characters */
    private void m3393() {
        post(this.f2088);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.f2088);
        post(this.f2086);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3399(CharSequence charSequence) {
        setQuery(charSequence);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private CharSequence m3386(CharSequence charSequence) {
        if (!this.f2098 || this.f2097 == null) {
            return charSequence;
        }
        double textSize = (double) this.f2066.getTextSize();
        Double.isNaN(textSize);
        int i = (int) (textSize * 1.25d);
        this.f2097.setBounds(0, 0, i, i);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("   ");
        spannableStringBuilder.setSpan(new ImageSpan(this.f2097), 1, 2, 33);
        spannableStringBuilder.append(charSequence);
        return spannableStringBuilder;
    }

    /* renamed from: ـ  reason: contains not printable characters */
    private void m3394() {
        CharSequence queryHint = getQueryHint();
        SearchAutoComplete searchAutoComplete = this.f2066;
        if (queryHint == null) {
            queryHint = BuildConfig.FLAVOR;
        }
        searchAutoComplete.setHint(m3386(queryHint));
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m3395() {
        this.f2066.setThreshold(this.f2080.getSuggestThreshold());
        this.f2066.setImeOptions(this.f2080.getImeOptions());
        int inputType = this.f2080.getInputType();
        int i = 1;
        if ((inputType & 15) == 1) {
            inputType &= -65537;
            if (this.f2080.getSuggestAuthority() != null) {
                inputType = inputType | InternalZipConstants.MIN_SPLIT_LENGTH | 524288;
            }
        }
        this.f2066.setInputType(inputType);
        C0159 r0 = this.f2078;
        if (r0 != null) {
            r0.m1011((Cursor) null);
        }
        if (this.f2080.getSuggestAuthority() != null) {
            this.f2078 = new C0585(getContext(), this, this.f2080, this.f2090);
            this.f2066.setAdapter(this.f2078);
            C0585 r02 = (C0585) this.f2078;
            if (this.f2075) {
                i = 2;
            }
            r02.m3724(i);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m3388(boolean z) {
        int i;
        if (!this.f2077 || m3402() || !z) {
            i = 8;
        } else {
            i = 0;
            this.f2070.setVisibility(8);
        }
        this.f2074.setVisibility(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m3403() {
        Editable text = this.f2066.getText();
        if (text != null && TextUtils.getTrimmedLength(text) > 0) {
            C0560 r1 = this.f2104;
            if (r1 == null || !r1.m3415(text.toString())) {
                if (this.f2080 != null) {
                    m3398(0, null, text.toString());
                }
                this.f2066.setImeVisibility(false);
                m3396();
            }
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m3396() {
        this.f2066.dismissDropDown();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m3404() {
        if (!TextUtils.isEmpty(this.f2066.getText())) {
            this.f2066.setText(BuildConfig.FLAVOR);
            this.f2066.requestFocus();
            this.f2066.setImeVisibility(true);
        } else if (this.f2098) {
            C0559 r0 = this.f2105;
            if (r0 == null || !r0.m3414()) {
                clearFocus();
                m3384(true);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m3405() {
        m3384(false);
        this.f2066.requestFocus();
        this.f2066.setImeVisibility(true);
        View.OnClickListener onClickListener = this.f2096;
        if (onClickListener != null) {
            onClickListener.onClick(this);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m3406() {
        m3384(m3402());
        m3393();
        if (this.f2066.hasFocus()) {
            m3407();
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        m3393();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.SearchView.ʻ(java.lang.CharSequence, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      android.support.v7.widget.SearchView.ʻ(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.ˎˎ.ʻ(android.view.View, int):int
      android.support.v7.widget.ˎˎ.ʻ(int, int):void
      android.support.v7.widget.ˎˎ.ʻ(android.graphics.Canvas, int):void
      android.support.v7.widget.SearchView.ʻ(java.lang.CharSequence, boolean):void */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3401() {
        m3400((CharSequence) BuildConfig.FLAVOR, false);
        clearFocus();
        m3384(true);
        this.f2066.setImeOptions(this.f2084);
        this.f2079 = false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3397() {
        if (!this.f2079) {
            this.f2079 = true;
            this.f2084 = this.f2066.getImeOptions();
            this.f2066.setImeOptions(this.f2084 | 33554432);
            this.f2066.setText(BuildConfig.FLAVOR);
            setIconified(false);
        }
    }

    /* renamed from: android.support.v7.widget.SearchView$ʿ  reason: contains not printable characters */
    static class C0562 extends C0355 {
        public static final Parcelable.Creator<C0562> CREATOR = new Parcelable.ClassLoaderCreator<C0562>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C0562 createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new C0562(parcel, classLoader);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0562 createFromParcel(Parcel parcel) {
                return new C0562(parcel, null);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0562[] newArray(int i) {
                return new C0562[i];
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean f2115;

        C0562(Parcelable parcelable) {
            super(parcelable);
        }

        public C0562(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f2115 = ((Boolean) parcel.readValue(null)).booleanValue();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeValue(Boolean.valueOf(this.f2115));
        }

        public String toString() {
            return "SearchView.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " isIconified=" + this.f2115 + "}";
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        C0562 r1 = new C0562(super.onSaveInstanceState());
        r1.f2115 = m3402();
        return r1;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof C0562)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        C0562 r2 = (C0562) parcelable;
        super.onRestoreInstanceState(r2.m1995());
        m3384(r2.f2115);
        requestLayout();
    }

    private void setQuery(CharSequence charSequence) {
        this.f2066.setText(charSequence);
        this.f2066.setSelection(TextUtils.isEmpty(charSequence) ? 0 : charSequence.length());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3398(int i, String str, String str2) {
        getContext().startActivity(m3382("android.intent.action.SEARCH", (Uri) null, (String) null, str2, i, str));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private Intent m3382(String str, Uri uri, String str2, String str3, int i, String str4) {
        Intent intent = new Intent(str);
        intent.addFlags(268435456);
        if (uri != null) {
            intent.setData(uri);
        }
        intent.putExtra("user_query", this.f2081);
        if (str3 != null) {
            intent.putExtra("query", str3);
        }
        if (str2 != null) {
            intent.putExtra("intent_extra_data_key", str2);
        }
        Bundle bundle = this.f2082;
        if (bundle != null) {
            intent.putExtra("app_data", bundle);
        }
        if (i != 0) {
            intent.putExtra("action_key", i);
            intent.putExtra("action_msg", str4);
        }
        intent.setComponent(this.f2080.getSearchActivity());
        return intent;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public void m3407() {
        f2065.m3411(this.f2066);
        f2065.m3413(this.f2066);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static boolean m3385(Context context) {
        return context.getResources().getConfiguration().orientation == 2;
    }

    /* renamed from: android.support.v7.widget.SearchView$ˆ  reason: contains not printable characters */
    private static class C0563 extends TouchDelegate {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final View f2116;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final Rect f2117 = new Rect();

        /* renamed from: ʽ  reason: contains not printable characters */
        private final Rect f2118 = new Rect();

        /* renamed from: ʾ  reason: contains not printable characters */
        private final Rect f2119 = new Rect();

        /* renamed from: ʿ  reason: contains not printable characters */
        private final int f2120;

        /* renamed from: ˆ  reason: contains not printable characters */
        private boolean f2121;

        public C0563(Rect rect, Rect rect2, View view) {
            super(rect, view);
            this.f2120 = ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
            m3419(rect, rect2);
            this.f2116 = view;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3419(Rect rect, Rect rect2) {
            this.f2117.set(rect);
            this.f2119.set(rect);
            Rect rect3 = this.f2119;
            int i = this.f2120;
            rect3.inset(-i, -i);
            this.f2118.set(rect2);
        }

        /* JADX WARNING: Removed duplicated region for block: B:17:0x003d  */
        /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onTouchEvent(android.view.MotionEvent r8) {
            /*
                r7 = this;
                float r0 = r8.getX()
                int r0 = (int) r0
                float r1 = r8.getY()
                int r1 = (int) r1
                int r2 = r8.getAction()
                r3 = 2
                r4 = 1
                r5 = 0
                if (r2 == 0) goto L_0x002e
                if (r2 == r4) goto L_0x0020
                if (r2 == r3) goto L_0x0020
                r6 = 3
                if (r2 == r6) goto L_0x001b
                goto L_0x003a
            L_0x001b:
                boolean r2 = r7.f2121
                r7.f2121 = r5
                goto L_0x003b
            L_0x0020:
                boolean r2 = r7.f2121
                if (r2 == 0) goto L_0x003b
                android.graphics.Rect r6 = r7.f2119
                boolean r6 = r6.contains(r0, r1)
                if (r6 != 0) goto L_0x003b
                r4 = 0
                goto L_0x003b
            L_0x002e:
                android.graphics.Rect r2 = r7.f2117
                boolean r2 = r2.contains(r0, r1)
                if (r2 == 0) goto L_0x003a
                r7.f2121 = r4
                r2 = 1
                goto L_0x003b
            L_0x003a:
                r2 = 0
            L_0x003b:
                if (r2 == 0) goto L_0x0070
                if (r4 == 0) goto L_0x005b
                android.graphics.Rect r2 = r7.f2118
                boolean r2 = r2.contains(r0, r1)
                if (r2 != 0) goto L_0x005b
                android.view.View r0 = r7.f2116
                int r0 = r0.getWidth()
                int r0 = r0 / r3
                float r0 = (float) r0
                android.view.View r1 = r7.f2116
                int r1 = r1.getHeight()
                int r1 = r1 / r3
                float r1 = (float) r1
                r8.setLocation(r0, r1)
                goto L_0x006a
            L_0x005b:
                android.graphics.Rect r2 = r7.f2118
                int r2 = r2.left
                int r0 = r0 - r2
                float r0 = (float) r0
                android.graphics.Rect r2 = r7.f2118
                int r2 = r2.top
                int r1 = r1 - r2
                float r1 = (float) r1
                r8.setLocation(r0, r1)
            L_0x006a:
                android.view.View r0 = r7.f2116
                boolean r5 = r0.dispatchTouchEvent(r8)
            L_0x0070:
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.SearchView.C0563.onTouchEvent(android.view.MotionEvent):boolean");
        }
    }

    public static class SearchAutoComplete extends C0636 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final Runnable f2107;

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f2108;

        /* renamed from: ʽ  reason: contains not printable characters */
        private SearchView f2109;

        /* renamed from: ʾ  reason: contains not printable characters */
        private boolean f2110;

        public void performCompletion() {
        }

        /* access modifiers changed from: protected */
        public void replaceText(CharSequence charSequence) {
        }

        public SearchAutoComplete(Context context) {
            this(context, null);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet) {
            this(context, attributeSet, C0727.C0728.autoCompleteTextViewStyle);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet, i);
            this.f2107 = new Runnable() {
                public void run() {
                    SearchAutoComplete.this.m3408();
                }
            };
            this.f2108 = getThreshold();
        }

        /* access modifiers changed from: protected */
        public void onFinishInflate() {
            super.onFinishInflate();
            setMinWidth((int) TypedValue.applyDimension(1, (float) getSearchViewTextMinWidthDp(), getResources().getDisplayMetrics()));
        }

        /* access modifiers changed from: package-private */
        public void setSearchView(SearchView searchView) {
            this.f2109 = searchView;
        }

        public void setThreshold(int i) {
            super.setThreshold(i);
            this.f2108 = i;
        }

        public void onWindowFocusChanged(boolean z) {
            super.onWindowFocusChanged(z);
            if (z && this.f2109.hasFocus() && getVisibility() == 0) {
                this.f2110 = true;
                if (SearchView.m3385(getContext())) {
                    SearchView.f2065.m3412(this, true);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onFocusChanged(boolean z, int i, Rect rect) {
            super.onFocusChanged(z, i, rect);
            this.f2109.m3406();
        }

        public boolean enoughToFilter() {
            return this.f2108 <= 0 || super.enoughToFilter();
        }

        public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
            if (i == 4) {
                if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                    KeyEvent.DispatcherState keyDispatcherState = getKeyDispatcherState();
                    if (keyDispatcherState != null) {
                        keyDispatcherState.startTracking(keyEvent, this);
                    }
                    return true;
                } else if (keyEvent.getAction() == 1) {
                    KeyEvent.DispatcherState keyDispatcherState2 = getKeyDispatcherState();
                    if (keyDispatcherState2 != null) {
                        keyDispatcherState2.handleUpEvent(keyEvent);
                    }
                    if (keyEvent.isTracking() && !keyEvent.isCanceled()) {
                        this.f2109.clearFocus();
                        setImeVisibility(false);
                        return true;
                    }
                }
            }
            return super.onKeyPreIme(i, keyEvent);
        }

        private int getSearchViewTextMinWidthDp() {
            Configuration configuration = getResources().getConfiguration();
            int i = configuration.screenWidthDp;
            int i2 = configuration.screenHeightDp;
            if (i >= 960 && i2 >= 720 && configuration.orientation == 2) {
                return 256;
            }
            if (i < 600) {
                return (i < 640 || i2 < 480) ? 160 : 192;
            }
            return 192;
        }

        public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
            InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
            if (this.f2110) {
                removeCallbacks(this.f2107);
                post(this.f2107);
            }
            return onCreateInputConnection;
        }

        /* access modifiers changed from: private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3408() {
            if (this.f2110) {
                ((InputMethodManager) getContext().getSystemService("input_method")).showSoftInput(this, 0);
                this.f2110 = false;
            }
        }

        /* access modifiers changed from: private */
        public void setImeVisibility(boolean z) {
            InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
            if (!z) {
                this.f2110 = false;
                removeCallbacks(this.f2107);
                inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            } else if (inputMethodManager.isActive(this)) {
                this.f2110 = false;
                removeCallbacks(this.f2107);
                inputMethodManager.showSoftInput(this, 0);
            } else {
                this.f2110 = true;
            }
        }
    }

    /* renamed from: android.support.v7.widget.SearchView$ʻ  reason: contains not printable characters */
    private static class C0558 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private Method f2112;

        /* renamed from: ʼ  reason: contains not printable characters */
        private Method f2113;

        /* renamed from: ʽ  reason: contains not printable characters */
        private Method f2114;

        C0558() {
            try {
                this.f2112 = AutoCompleteTextView.class.getDeclaredMethod("doBeforeTextChanged", new Class[0]);
                this.f2112.setAccessible(true);
            } catch (NoSuchMethodException unused) {
            }
            try {
                this.f2113 = AutoCompleteTextView.class.getDeclaredMethod("doAfterTextChanged", new Class[0]);
                this.f2113.setAccessible(true);
            } catch (NoSuchMethodException unused2) {
            }
            Class<AutoCompleteTextView> cls = AutoCompleteTextView.class;
            try {
                this.f2114 = cls.getMethod("ensureImeVisible", Boolean.TYPE);
                this.f2114.setAccessible(true);
            } catch (NoSuchMethodException unused3) {
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3411(AutoCompleteTextView autoCompleteTextView) {
            Method method = this.f2112;
            if (method != null) {
                try {
                    method.invoke(autoCompleteTextView, new Object[0]);
                } catch (Exception unused) {
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m3413(AutoCompleteTextView autoCompleteTextView) {
            Method method = this.f2113;
            if (method != null) {
                try {
                    method.invoke(autoCompleteTextView, new Object[0]);
                } catch (Exception unused) {
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3412(AutoCompleteTextView autoCompleteTextView, boolean z) {
            Method method = this.f2114;
            if (method != null) {
                try {
                    method.invoke(autoCompleteTextView, Boolean.valueOf(z));
                } catch (Exception unused) {
                }
            }
        }
    }
}
