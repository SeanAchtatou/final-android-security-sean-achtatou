package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.support.v4.widget.C0187;
import android.support.v4.ˉ.C0413;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.widget.ImageButton;

/* renamed from: android.support.v7.widget.ـ  reason: contains not printable characters */
/* compiled from: AppCompatImageButton */
public class C0669 extends ImageButton implements C0187, C0413 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C0639 f2680;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final C0671 f2681;

    public C0669(Context context) {
        this(context, null);
    }

    public C0669(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0727.C0728.imageButtonStyle);
    }

    public C0669(Context context, AttributeSet attributeSet, int i) {
        super(C0589.m3735(context), attributeSet, i);
        this.f2680 = new C0639(this);
        this.f2680.m4065(attributeSet, i);
        this.f2681 = new C0671(this);
        this.f2681.m4239(attributeSet, i);
    }

    public void setImageResource(int i) {
        this.f2681.m4236(i);
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        C0671 r1 = this.f2681;
        if (r1 != null) {
            r1.m4243();
        }
    }

    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        C0671 r1 = this.f2681;
        if (r1 != null) {
            r1.m4243();
        }
    }

    public void setImageIcon(Icon icon) {
        super.setImageIcon(icon);
        C0671 r1 = this.f2681;
        if (r1 != null) {
            r1.m4243();
        }
    }

    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        C0671 r1 = this.f2681;
        if (r1 != null) {
            r1.m4243();
        }
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        C0639 r0 = this.f2680;
        if (r0 != null) {
            r0.m4061(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        C0639 r0 = this.f2680;
        if (r0 != null) {
            r0.m4064(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        C0639 r0 = this.f2680;
        if (r0 != null) {
            r0.m4062(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        C0639 r0 = this.f2680;
        if (r0 != null) {
            return r0.m4060();
        }
        return null;
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        C0639 r0 = this.f2680;
        if (r0 != null) {
            r0.m4063(mode);
        }
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        C0639 r0 = this.f2680;
        if (r0 != null) {
            return r0.m4066();
        }
        return null;
    }

    public void setSupportImageTintList(ColorStateList colorStateList) {
        C0671 r0 = this.f2681;
        if (r0 != null) {
            r0.m4237(colorStateList);
        }
    }

    public ColorStateList getSupportImageTintList() {
        C0671 r0 = this.f2681;
        if (r0 != null) {
            return r0.m4241();
        }
        return null;
    }

    public void setSupportImageTintMode(PorterDuff.Mode mode) {
        C0671 r0 = this.f2681;
        if (r0 != null) {
            r0.m4238(mode);
        }
    }

    public PorterDuff.Mode getSupportImageTintMode() {
        C0671 r0 = this.f2681;
        if (r0 != null) {
            return r0.m4242();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        C0639 r0 = this.f2680;
        if (r0 != null) {
            r0.m4068();
        }
        C0671 r02 = this.f2681;
        if (r02 != null) {
            r02.m4243();
        }
    }

    public boolean hasOverlappingRendering() {
        return this.f2681.m4240() && super.hasOverlappingRendering();
    }
}
