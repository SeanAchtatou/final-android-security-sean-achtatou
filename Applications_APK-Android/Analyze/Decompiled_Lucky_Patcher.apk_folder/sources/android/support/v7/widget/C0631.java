package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.ˉ.C0434;
import android.support.v7.view.menu.C0504;
import android.support.v7.view.menu.C0518;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;

/* renamed from: android.support.v7.widget.ʿʿ  reason: contains not printable characters */
/* compiled from: DecorToolbar */
public interface C0631 {
    /* renamed from: ʻ  reason: contains not printable characters */
    C0434 m3989(int i, long j);

    /* renamed from: ʻ  reason: contains not printable characters */
    ViewGroup m3990();

    /* renamed from: ʻ  reason: contains not printable characters */
    void m3991(int i);

    /* renamed from: ʻ  reason: contains not printable characters */
    void m3992(Drawable drawable);

    /* renamed from: ʻ  reason: contains not printable characters */
    void m3993(C0518.C0519 r1, C0504.C0505 r2);

    /* renamed from: ʻ  reason: contains not printable characters */
    void m3994(C0580 r1);

    /* renamed from: ʻ  reason: contains not printable characters */
    void m3995(Menu menu, C0518.C0519 r2);

    /* renamed from: ʻ  reason: contains not printable characters */
    void m3996(Window.Callback callback);

    /* renamed from: ʻ  reason: contains not printable characters */
    void m3997(CharSequence charSequence);

    /* renamed from: ʻ  reason: contains not printable characters */
    void m3998(boolean z);

    /* renamed from: ʼ  reason: contains not printable characters */
    Context m3999();

    /* renamed from: ʼ  reason: contains not printable characters */
    void m4000(int i);

    /* renamed from: ʼ  reason: contains not printable characters */
    void m4001(Drawable drawable);

    /* renamed from: ʼ  reason: contains not printable characters */
    void m4002(boolean z);

    /* renamed from: ʽ  reason: contains not printable characters */
    void m4003(int i);

    /* renamed from: ʽ  reason: contains not printable characters */
    boolean m4004();

    /* renamed from: ʾ  reason: contains not printable characters */
    void m4005();

    /* renamed from: ʾ  reason: contains not printable characters */
    void m4006(int i);

    /* renamed from: ʿ  reason: contains not printable characters */
    CharSequence m4007();

    /* renamed from: ʿ  reason: contains not printable characters */
    void m4008(int i);

    /* renamed from: ˆ  reason: contains not printable characters */
    void m4009();

    /* renamed from: ˈ  reason: contains not printable characters */
    void m4010();

    /* renamed from: ˉ  reason: contains not printable characters */
    boolean m4011();

    /* renamed from: ˊ  reason: contains not printable characters */
    boolean m4012();

    /* renamed from: ˋ  reason: contains not printable characters */
    boolean m4013();

    /* renamed from: ˎ  reason: contains not printable characters */
    boolean m4014();

    /* renamed from: ˏ  reason: contains not printable characters */
    boolean m4015();

    /* renamed from: ˑ  reason: contains not printable characters */
    void m4016();

    /* renamed from: י  reason: contains not printable characters */
    void m4017();

    /* renamed from: ـ  reason: contains not printable characters */
    int m4018();

    /* renamed from: ٴ  reason: contains not printable characters */
    int m4019();

    /* renamed from: ᐧ  reason: contains not printable characters */
    Menu m4020();
}
