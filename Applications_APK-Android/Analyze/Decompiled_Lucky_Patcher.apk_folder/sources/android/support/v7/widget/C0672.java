package android.support.v7.widget;

import android.os.Bundle;
import android.support.v4.ˉ.C0386;
import android.support.v4.ˉ.ʻ.C0360;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

/* renamed from: android.support.v7.widget.ٴٴ  reason: contains not printable characters */
/* compiled from: RecyclerViewAccessibilityDelegate */
public class C0672 extends C0386 {

    /* renamed from: ʻ  reason: contains not printable characters */
    final C0690 f2688;

    /* renamed from: ʽ  reason: contains not printable characters */
    final C0386 f2689 = new C0673(this);

    public C0672(C0690 r1) {
        this.f2688 = r1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m4247() {
        return this.f2688.m4453();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m4246(View view, int i, Bundle bundle) {
        if (super.m2131(view, i, bundle)) {
            return true;
        }
        if (m4247() || this.f2688.getLayoutManager() == null) {
            return false;
        }
        return this.f2688.getLayoutManager().m4580(i, bundle);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4244(View view, C0360 r2) {
        super.m2129(view, r2);
        r2.m2019((CharSequence) C0690.class.getName());
        if (!m4247() && this.f2688.getLayoutManager() != null) {
            this.f2688.getLayoutManager().m4556(r2);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4245(View view, AccessibilityEvent accessibilityEvent) {
        super.m2130(view, accessibilityEvent);
        accessibilityEvent.setClassName(C0690.class.getName());
        if ((view instanceof C0690) && !m4247()) {
            C0690 r2 = (C0690) view;
            if (r2.getLayoutManager() != null) {
                r2.getLayoutManager().m4578(accessibilityEvent);
            }
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public C0386 m4248() {
        return this.f2689;
    }

    /* renamed from: android.support.v7.widget.ٴٴ$ʻ  reason: contains not printable characters */
    /* compiled from: RecyclerViewAccessibilityDelegate */
    public static class C0673 extends C0386 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final C0672 f2690;

        public C0673(C0672 r1) {
            this.f2690 = r1;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4249(View view, C0360 r3) {
            super.m2129(view, r3);
            if (!this.f2690.m4247() && this.f2690.f2688.getLayoutManager() != null) {
                this.f2690.f2688.getLayoutManager().m4575(view, r3);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4250(View view, int i, Bundle bundle) {
            if (super.m2131(view, i, bundle)) {
                return true;
            }
            if (this.f2690.m4247() || this.f2690.f2688.getLayoutManager() == null) {
                return false;
            }
            return this.f2690.f2688.getLayoutManager().m4590(view, i, bundle);
        }
    }
}
