package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.widget.C0173;
import android.support.v4.ˉ.C0414;
import android.support.v7.view.menu.C0524;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import java.lang.reflect.Method;

/* renamed from: android.support.v7.widget.ˑˑ  reason: contains not printable characters */
/* compiled from: ListPopupWindow */
public class C0661 implements C0524 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Method f2635;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static Method f2636;

    /* renamed from: ˉ  reason: contains not printable characters */
    private static Method f2637;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private AdapterView.OnItemSelectedListener f2638;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private final C0664 f2639;

    /* renamed from: ʽ  reason: contains not printable characters */
    C0635 f2640;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private final C0665 f2641;

    /* renamed from: ʾ  reason: contains not printable characters */
    int f2642;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private Runnable f2643;

    /* renamed from: ʿ  reason: contains not printable characters */
    final C0666 f2644;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private final C0662 f2645;

    /* renamed from: ˆ  reason: contains not printable characters */
    final Handler f2646;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private Rect f2647;

    /* renamed from: ˈ  reason: contains not printable characters */
    PopupWindow f2648;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private boolean f2649;

    /* renamed from: ˊ  reason: contains not printable characters */
    private Context f2650;

    /* renamed from: ˋ  reason: contains not printable characters */
    private ListAdapter f2651;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f2652;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f2653;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f2654;

    /* renamed from: י  reason: contains not printable characters */
    private int f2655;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f2656;

    /* renamed from: ــ  reason: contains not printable characters */
    private final Rect f2657;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f2658;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f2659;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private Drawable f2660;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private boolean f2661;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private AdapterView.OnItemClickListener f2662;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f2663;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private int f2664;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private boolean f2665;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private boolean f2666;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private View f2667;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int f2668;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private DataSetObserver f2669;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private View f2670;

    static {
        Class<PopupWindow> cls = PopupWindow.class;
        try {
            f2635 = cls.getDeclaredMethod("setClipToScreenEnabled", Boolean.TYPE);
        } catch (NoSuchMethodException unused) {
            Log.i("ListPopupWindow", "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well.");
        }
        Class<PopupWindow> cls2 = PopupWindow.class;
        try {
            f2636 = cls2.getDeclaredMethod("getMaxAvailableHeight", View.class, Integer.TYPE, Boolean.TYPE);
        } catch (NoSuchMethodException unused2) {
            Log.i("ListPopupWindow", "Could not find method getMaxAvailableHeight(View, int, boolean) on PopupWindow. Oh well.");
        }
        try {
            f2637 = PopupWindow.class.getDeclaredMethod("setEpicenterBounds", Rect.class);
        } catch (NoSuchMethodException unused3) {
            Log.i("ListPopupWindow", "Could not find method setEpicenterBounds(Rect) on PopupWindow. Oh well.");
        }
    }

    public C0661(Context context) {
        this(context, null, C0727.C0728.listPopupWindowStyle);
    }

    public C0661(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    public C0661(Context context, AttributeSet attributeSet, int i, int i2) {
        this.f2652 = -2;
        this.f2653 = -2;
        this.f2656 = 1002;
        this.f2659 = true;
        this.f2664 = 0;
        this.f2665 = false;
        this.f2666 = false;
        this.f2642 = Integer.MAX_VALUE;
        this.f2668 = 0;
        this.f2644 = new C0666();
        this.f2641 = new C0665();
        this.f2639 = new C0664();
        this.f2645 = new C0662();
        this.f2657 = new Rect();
        this.f2650 = context;
        this.f2646 = new Handler(context.getMainLooper());
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0727.C0737.ListPopupWindow, i, i2);
        this.f2654 = obtainStyledAttributes.getDimensionPixelOffset(C0727.C0737.ListPopupWindow_android_dropDownHorizontalOffset, 0);
        this.f2655 = obtainStyledAttributes.getDimensionPixelOffset(C0727.C0737.ListPopupWindow_android_dropDownVerticalOffset, 0);
        if (this.f2655 != 0) {
            this.f2658 = true;
        }
        obtainStyledAttributes.recycle();
        this.f2648 = new C0678(context, attributeSet, i, i2);
        this.f2648.setInputMethodMode(1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4203(ListAdapter listAdapter) {
        DataSetObserver dataSetObserver = this.f2669;
        if (dataSetObserver == null) {
            this.f2669 = new C0663();
        } else {
            ListAdapter listAdapter2 = this.f2651;
            if (listAdapter2 != null) {
                listAdapter2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.f2651 = listAdapter;
        if (this.f2651 != null) {
            listAdapter.registerDataSetObserver(this.f2669);
        }
        C0635 r3 = this.f2640;
        if (r3 != null) {
            r3.setAdapter(this.f2651);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4199(int i) {
        this.f2668 = i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4205(boolean z) {
        this.f2649 = z;
        this.f2648.setFocusable(z);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m4210() {
        return this.f2649;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public Drawable m4219() {
        return this.f2648.getBackground();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4201(Drawable drawable) {
        this.f2648.setBackgroundDrawable(drawable);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4206(int i) {
        this.f2648.setAnimationStyle(i);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public View m4221() {
        return this.f2670;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4207(View view) {
        this.f2670 = view;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public int m4223() {
        return this.f2654;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m4209(int i) {
        this.f2654 = i;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public int m4224() {
        if (!this.f2658) {
            return 0;
        }
        return this.f2655;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m4212(int i) {
        this.f2655 = i;
        this.f2658 = true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4200(Rect rect) {
        this.f2647 = rect;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m4214(int i) {
        this.f2664 = i;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public int m4225() {
        return this.f2653;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m4215(int i) {
        this.f2653 = i;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public void m4218(int i) {
        Drawable background = this.f2648.getBackground();
        if (background != null) {
            background.getPadding(this.f2657);
            this.f2653 = this.f2657.left + this.f2657.right + i;
            return;
        }
        m4215(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4202(AdapterView.OnItemClickListener onItemClickListener) {
        this.f2662 = onItemClickListener;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m4211() {
        int r0 = m4196();
        boolean r1 = m4227();
        C0173.m1044(this.f2648, this.f2656);
        boolean z = true;
        if (!this.f2648.isShowing()) {
            int i = this.f2653;
            if (i == -1) {
                i = -1;
            } else if (i == -2) {
                i = m4221().getWidth();
            }
            int i2 = this.f2652;
            if (i2 == -1) {
                r0 = -1;
            } else if (i2 != -2) {
                r0 = i2;
            }
            this.f2648.setWidth(i);
            this.f2648.setHeight(r0);
            m4197(true);
            this.f2648.setOutsideTouchable(!this.f2666 && !this.f2665);
            this.f2648.setTouchInterceptor(this.f2641);
            if (this.f2663) {
                C0173.m1046(this.f2648, this.f2661);
            }
            Method method = f2637;
            if (method != null) {
                try {
                    method.invoke(this.f2648, this.f2647);
                } catch (Exception e) {
                    Log.e("ListPopupWindow", "Could not invoke setEpicenterBounds on PopupWindow", e);
                }
            }
            C0173.m1045(this.f2648, m4221(), this.f2654, this.f2655, this.f2664);
            this.f2640.setSelection(-1);
            if (!this.f2649 || this.f2640.isInTouchMode()) {
                m4226();
            }
            if (!this.f2649) {
                this.f2646.post(this.f2645);
            }
        } else if (C0414.m2257(m4221())) {
            int i3 = this.f2653;
            if (i3 == -1) {
                i3 = -1;
            } else if (i3 == -2) {
                i3 = m4221().getWidth();
            }
            int i4 = this.f2652;
            if (i4 == -1) {
                if (!r1) {
                    r0 = -1;
                }
                if (r1) {
                    this.f2648.setWidth(this.f2653 == -1 ? -1 : 0);
                    this.f2648.setHeight(0);
                } else {
                    this.f2648.setWidth(this.f2653 == -1 ? -1 : 0);
                    this.f2648.setHeight(-1);
                }
            } else if (i4 != -2) {
                r0 = i4;
            }
            PopupWindow popupWindow = this.f2648;
            if (this.f2666 || this.f2665) {
                z = false;
            }
            popupWindow.setOutsideTouchable(z);
            this.f2648.update(m4221(), this.f2654, this.f2655, i3 < 0 ? -1 : i3, r0 < 0 ? -1 : r0);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m4213() {
        this.f2648.dismiss();
        m4195();
        this.f2648.setContentView(null);
        this.f2640 = null;
        this.f2646.removeCallbacks(this.f2644);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4204(PopupWindow.OnDismissListener onDismissListener) {
        this.f2648.setOnDismissListener(onDismissListener);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4195() {
        View view = this.f2667;
        if (view != null) {
            ViewParent parent = view.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.f2667);
            }
        }
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public void m4220(int i) {
        this.f2648.setInputMethodMode(i);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public void m4222(int i) {
        C0635 r0 = this.f2640;
        if (m4216() && r0 != null) {
            r0.setListSelectionHidden(false);
            r0.setSelection(i);
            if (r0.getChoiceMode() != 0) {
                r0.setItemChecked(i, true);
            }
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m4226() {
        C0635 r0 = this.f2640;
        if (r0 != null) {
            r0.setListSelectionHidden(true);
            r0.requestLayout();
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m4216() {
        return this.f2648.isShowing();
    }

    /* renamed from: י  reason: contains not printable characters */
    public boolean m4227() {
        return this.f2648.getInputMethodMode() == 2;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public ListView m4217() {
        return this.f2640;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0635 m4198(Context context, boolean z) {
        return new C0635(context, z);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v24, resolved type: android.support.v7.widget.ˆˆ} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v25, resolved type: android.support.v7.widget.ˆˆ} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v2, resolved type: android.widget.LinearLayout} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v30, resolved type: android.support.v7.widget.ˆˆ} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: ʼ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int m4196() {
        /*
            r12 = this;
            android.support.v7.widget.ˆˆ r0 = r12.f2640
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = -1
            r3 = 1
            r4 = 0
            if (r0 != 0) goto L_0x00c1
            android.content.Context r0 = r12.f2650
            android.support.v7.widget.ˑˑ$1 r5 = new android.support.v7.widget.ˑˑ$1
            r5.<init>()
            r12.f2643 = r5
            boolean r5 = r12.f2649
            r5 = r5 ^ r3
            android.support.v7.widget.ˆˆ r5 = r12.m4198(r0, r5)
            r12.f2640 = r5
            android.graphics.drawable.Drawable r5 = r12.f2660
            if (r5 == 0) goto L_0x0024
            android.support.v7.widget.ˆˆ r6 = r12.f2640
            r6.setSelector(r5)
        L_0x0024:
            android.support.v7.widget.ˆˆ r5 = r12.f2640
            android.widget.ListAdapter r6 = r12.f2651
            r5.setAdapter(r6)
            android.support.v7.widget.ˆˆ r5 = r12.f2640
            android.widget.AdapterView$OnItemClickListener r6 = r12.f2662
            r5.setOnItemClickListener(r6)
            android.support.v7.widget.ˆˆ r5 = r12.f2640
            r5.setFocusable(r3)
            android.support.v7.widget.ˆˆ r5 = r12.f2640
            r5.setFocusableInTouchMode(r3)
            android.support.v7.widget.ˆˆ r5 = r12.f2640
            android.support.v7.widget.ˑˑ$2 r6 = new android.support.v7.widget.ˑˑ$2
            r6.<init>()
            r5.setOnItemSelectedListener(r6)
            android.support.v7.widget.ˆˆ r5 = r12.f2640
            android.support.v7.widget.ˑˑ$ʽ r6 = r12.f2639
            r5.setOnScrollListener(r6)
            android.widget.AdapterView$OnItemSelectedListener r5 = r12.f2638
            if (r5 == 0) goto L_0x0056
            android.support.v7.widget.ˆˆ r6 = r12.f2640
            r6.setOnItemSelectedListener(r5)
        L_0x0056:
            android.support.v7.widget.ˆˆ r5 = r12.f2640
            android.view.View r6 = r12.f2667
            if (r6 == 0) goto L_0x00ba
            android.widget.LinearLayout r7 = new android.widget.LinearLayout
            r7.<init>(r0)
            r7.setOrientation(r3)
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            r8 = 1065353216(0x3f800000, float:1.0)
            r0.<init>(r2, r4, r8)
            int r8 = r12.f2668
            if (r8 == 0) goto L_0x0091
            if (r8 == r3) goto L_0x008a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r5 = "Invalid hint position "
            r0.append(r5)
            int r5 = r12.f2668
            r0.append(r5)
            java.lang.String r0 = r0.toString()
            java.lang.String r5 = "ListPopupWindow"
            android.util.Log.e(r5, r0)
            goto L_0x0097
        L_0x008a:
            r7.addView(r5, r0)
            r7.addView(r6)
            goto L_0x0097
        L_0x0091:
            r7.addView(r6)
            r7.addView(r5, r0)
        L_0x0097:
            int r0 = r12.f2653
            if (r0 < 0) goto L_0x009e
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            goto L_0x00a0
        L_0x009e:
            r0 = 0
            r5 = 0
        L_0x00a0:
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r5)
            r6.measure(r0, r4)
            android.view.ViewGroup$LayoutParams r0 = r6.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r0 = (android.widget.LinearLayout.LayoutParams) r0
            int r5 = r6.getMeasuredHeight()
            int r6 = r0.topMargin
            int r5 = r5 + r6
            int r0 = r0.bottomMargin
            int r5 = r5 + r0
            r0 = r5
            r5 = r7
            goto L_0x00bb
        L_0x00ba:
            r0 = 0
        L_0x00bb:
            android.widget.PopupWindow r6 = r12.f2648
            r6.setContentView(r5)
            goto L_0x00df
        L_0x00c1:
            android.widget.PopupWindow r0 = r12.f2648
            android.view.View r0 = r0.getContentView()
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            android.view.View r0 = r12.f2667
            if (r0 == 0) goto L_0x00de
            android.view.ViewGroup$LayoutParams r5 = r0.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r5 = (android.widget.LinearLayout.LayoutParams) r5
            int r0 = r0.getMeasuredHeight()
            int r6 = r5.topMargin
            int r0 = r0 + r6
            int r5 = r5.bottomMargin
            int r0 = r0 + r5
            goto L_0x00df
        L_0x00de:
            r0 = 0
        L_0x00df:
            android.widget.PopupWindow r5 = r12.f2648
            android.graphics.drawable.Drawable r5 = r5.getBackground()
            if (r5 == 0) goto L_0x0101
            android.graphics.Rect r6 = r12.f2657
            r5.getPadding(r6)
            android.graphics.Rect r5 = r12.f2657
            int r5 = r5.top
            android.graphics.Rect r6 = r12.f2657
            int r6 = r6.bottom
            int r5 = r5 + r6
            boolean r6 = r12.f2658
            if (r6 != 0) goto L_0x0107
            android.graphics.Rect r6 = r12.f2657
            int r6 = r6.top
            int r6 = -r6
            r12.f2655 = r6
            goto L_0x0107
        L_0x0101:
            android.graphics.Rect r5 = r12.f2657
            r5.setEmpty()
            r5 = 0
        L_0x0107:
            android.widget.PopupWindow r6 = r12.f2648
            int r6 = r6.getInputMethodMode()
            r7 = 2
            if (r6 != r7) goto L_0x0111
            goto L_0x0112
        L_0x0111:
            r3 = 0
        L_0x0112:
            android.view.View r4 = r12.m4221()
            int r6 = r12.f2655
            int r3 = r12.m4194(r4, r6, r3)
            boolean r4 = r12.f2665
            if (r4 != 0) goto L_0x0188
            int r4 = r12.f2652
            if (r4 != r2) goto L_0x0125
            goto L_0x0188
        L_0x0125:
            int r4 = r12.f2653
            r6 = -2
            if (r4 == r6) goto L_0x014f
            r1 = 1073741824(0x40000000, float:2.0)
            if (r4 == r2) goto L_0x0134
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r4, r1)
        L_0x0132:
            r7 = r1
            goto L_0x016a
        L_0x0134:
            android.content.Context r2 = r12.f2650
            android.content.res.Resources r2 = r2.getResources()
            android.util.DisplayMetrics r2 = r2.getDisplayMetrics()
            int r2 = r2.widthPixels
            android.graphics.Rect r4 = r12.f2657
            int r4 = r4.left
            android.graphics.Rect r6 = r12.f2657
            int r6 = r6.right
            int r4 = r4 + r6
            int r2 = r2 - r4
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r1)
            goto L_0x0132
        L_0x014f:
            android.content.Context r2 = r12.f2650
            android.content.res.Resources r2 = r2.getResources()
            android.util.DisplayMetrics r2 = r2.getDisplayMetrics()
            int r2 = r2.widthPixels
            android.graphics.Rect r4 = r12.f2657
            int r4 = r4.left
            android.graphics.Rect r6 = r12.f2657
            int r6 = r6.right
            int r4 = r4 + r6
            int r2 = r2 - r4
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r1)
            goto L_0x0132
        L_0x016a:
            android.support.v7.widget.ˆˆ r6 = r12.f2640
            r8 = 0
            r9 = -1
            int r10 = r3 - r0
            r11 = -1
            int r1 = r6.m4269(r7, r8, r9, r10, r11)
            if (r1 <= 0) goto L_0x0186
            android.support.v7.widget.ˆˆ r2 = r12.f2640
            int r2 = r2.getPaddingTop()
            android.support.v7.widget.ˆˆ r3 = r12.f2640
            int r3 = r3.getPaddingBottom()
            int r2 = r2 + r3
            int r5 = r5 + r2
            int r0 = r0 + r5
        L_0x0186:
            int r1 = r1 + r0
            return r1
        L_0x0188:
            int r3 = r3 + r5
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0661.m4196():int");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4208(boolean z) {
        this.f2663 = true;
        this.f2661 = z;
    }

    /* renamed from: android.support.v7.widget.ˑˑ$ʼ  reason: contains not printable characters */
    /* compiled from: ListPopupWindow */
    private class C0663 extends DataSetObserver {
        C0663() {
        }

        public void onChanged() {
            if (C0661.this.m4216()) {
                C0661.this.m4211();
            }
        }

        public void onInvalidated() {
            C0661.this.m4213();
        }
    }

    /* renamed from: android.support.v7.widget.ˑˑ$ʻ  reason: contains not printable characters */
    /* compiled from: ListPopupWindow */
    private class C0662 implements Runnable {
        C0662() {
        }

        public void run() {
            C0661.this.m4226();
        }
    }

    /* renamed from: android.support.v7.widget.ˑˑ$ʿ  reason: contains not printable characters */
    /* compiled from: ListPopupWindow */
    private class C0666 implements Runnable {
        C0666() {
        }

        public void run() {
            if (C0661.this.f2640 != null && C0414.m2257(C0661.this.f2640) && C0661.this.f2640.getCount() > C0661.this.f2640.getChildCount() && C0661.this.f2640.getChildCount() <= C0661.this.f2642) {
                C0661.this.f2648.setInputMethodMode(2);
                C0661.this.m4211();
            }
        }
    }

    /* renamed from: android.support.v7.widget.ˑˑ$ʾ  reason: contains not printable characters */
    /* compiled from: ListPopupWindow */
    private class C0665 implements View.OnTouchListener {
        C0665() {
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            int action = motionEvent.getAction();
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();
            if (action == 0 && C0661.this.f2648 != null && C0661.this.f2648.isShowing() && x >= 0 && x < C0661.this.f2648.getWidth() && y >= 0 && y < C0661.this.f2648.getHeight()) {
                C0661.this.f2646.postDelayed(C0661.this.f2644, 250);
                return false;
            } else if (action != 1) {
                return false;
            } else {
                C0661.this.f2646.removeCallbacks(C0661.this.f2644);
                return false;
            }
        }
    }

    /* renamed from: android.support.v7.widget.ˑˑ$ʽ  reason: contains not printable characters */
    /* compiled from: ListPopupWindow */
    private class C0664 implements AbsListView.OnScrollListener {
        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        }

        C0664() {
        }

        public void onScrollStateChanged(AbsListView absListView, int i) {
            if (i == 1 && !C0661.this.m4227() && C0661.this.f2648.getContentView() != null) {
                C0661.this.f2646.removeCallbacks(C0661.this.f2644);
                C0661.this.f2644.run();
            }
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m4197(boolean z) {
        Method method = f2635;
        if (method != null) {
            try {
                method.invoke(this.f2648, Boolean.valueOf(z));
            } catch (Exception unused) {
                Log.i("ListPopupWindow", "Could not call setClipToScreenEnabled() on PopupWindow. Oh well.");
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m4194(View view, int i, boolean z) {
        Method method = f2636;
        if (method != null) {
            try {
                return ((Integer) method.invoke(this.f2648, view, Integer.valueOf(i), Boolean.valueOf(z))).intValue();
            } catch (Exception unused) {
                Log.i("ListPopupWindow", "Could not call getMaxAvailableHeightMethod(View, int, boolean) on PopupWindow. Using the public version.");
            }
        }
        return this.f2648.getMaxAvailableHeight(view, i);
    }
}
