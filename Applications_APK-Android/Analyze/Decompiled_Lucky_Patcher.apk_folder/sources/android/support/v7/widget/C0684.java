package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.C0186;
import android.support.v7.ʻ.C0727;
import android.support.v7.ʼ.ʻ.C0739;
import android.util.AttributeSet;
import android.widget.RadioButton;

/* renamed from: android.support.v7.widget.ᵢ  reason: contains not printable characters */
/* compiled from: AppCompatRadioButton */
public class C0684 extends RadioButton implements C0186 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C0654 f2724;

    public C0684(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0727.C0728.radioButtonStyle);
    }

    public C0684(Context context, AttributeSet attributeSet, int i) {
        super(C0589.m3735(context), attributeSet, i);
        this.f2724 = new C0654(this);
        this.f2724.m4152(attributeSet, i);
    }

    public void setButtonDrawable(Drawable drawable) {
        super.setButtonDrawable(drawable);
        C0654 r1 = this.f2724;
        if (r1 != null) {
            r1.m4154();
        }
    }

    public void setButtonDrawable(int i) {
        setButtonDrawable(C0739.m4883(getContext(), i));
    }

    public int getCompoundPaddingLeft() {
        int compoundPaddingLeft = super.getCompoundPaddingLeft();
        C0654 r1 = this.f2724;
        return r1 != null ? r1.m4148(compoundPaddingLeft) : compoundPaddingLeft;
    }

    public void setSupportButtonTintList(ColorStateList colorStateList) {
        C0654 r0 = this.f2724;
        if (r0 != null) {
            r0.m4150(colorStateList);
        }
    }

    public ColorStateList getSupportButtonTintList() {
        C0654 r0 = this.f2724;
        if (r0 != null) {
            return r0.m4149();
        }
        return null;
    }

    public void setSupportButtonTintMode(PorterDuff.Mode mode) {
        C0654 r0 = this.f2724;
        if (r0 != null) {
            r0.m4151(mode);
        }
    }

    public PorterDuff.Mode getSupportButtonTintMode() {
        C0654 r0 = this.f2724;
        if (r0 != null) {
            return r0.m4153();
        }
        return null;
    }
}
