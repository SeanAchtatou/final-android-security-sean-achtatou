package android.support.v7.widget;

import android.content.Context;
import android.os.Build;
import android.support.v4.widget.C0171;
import android.support.v4.ˉ.C0434;
import android.support.v7.ʻ.C0727;
import android.view.View;

/* renamed from: android.support.v7.widget.ˆˆ  reason: contains not printable characters */
/* compiled from: DropDownListView */
class C0635 extends C0682 {

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f2506;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f2507;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f2508;

    /* renamed from: ˋ  reason: contains not printable characters */
    private C0434 f2509;

    /* renamed from: ˎ  reason: contains not printable characters */
    private C0171 f2510;

    public C0635(Context context, boolean z) {
        super(context, null, C0727.C0728.dropDownListViewStyle);
        this.f2507 = z;
        setCacheColorHint(0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (r0 != 3) goto L_0x000e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0065  */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean m4056(android.view.MotionEvent r8, int r9) {
        /*
            r7 = this;
            int r0 = r8.getActionMasked()
            r1 = 0
            r2 = 1
            if (r0 == r2) goto L_0x0016
            r3 = 2
            if (r0 == r3) goto L_0x0014
            r9 = 3
            if (r0 == r9) goto L_0x0011
        L_0x000e:
            r9 = 0
            r3 = 1
            goto L_0x0046
        L_0x0011:
            r9 = 0
            r3 = 0
            goto L_0x0046
        L_0x0014:
            r3 = 1
            goto L_0x0017
        L_0x0016:
            r3 = 0
        L_0x0017:
            int r9 = r8.findPointerIndex(r9)
            if (r9 >= 0) goto L_0x001e
            goto L_0x0011
        L_0x001e:
            float r4 = r8.getX(r9)
            int r4 = (int) r4
            float r9 = r8.getY(r9)
            int r9 = (int) r9
            int r5 = r7.pointToPosition(r4, r9)
            r6 = -1
            if (r5 != r6) goto L_0x0031
            r9 = 1
            goto L_0x0046
        L_0x0031:
            int r3 = r7.getFirstVisiblePosition()
            int r3 = r5 - r3
            android.view.View r3 = r7.getChildAt(r3)
            float r4 = (float) r4
            float r9 = (float) r9
            r7.m4053(r3, r5, r4, r9)
            if (r0 != r2) goto L_0x000e
            r7.m4052(r3, r5)
            goto L_0x000e
        L_0x0046:
            if (r3 == 0) goto L_0x004a
            if (r9 == 0) goto L_0x004d
        L_0x004a:
            r7.m4054()
        L_0x004d:
            if (r3 == 0) goto L_0x0065
            android.support.v4.widget.ˈ r9 = r7.f2510
            if (r9 != 0) goto L_0x005a
            android.support.v4.widget.ˈ r9 = new android.support.v4.widget.ˈ
            r9.<init>(r7)
            r7.f2510 = r9
        L_0x005a:
            android.support.v4.widget.ˈ r9 = r7.f2510
            r9.m972(r2)
            android.support.v4.widget.ˈ r9 = r7.f2510
            r9.onTouch(r7, r8)
            goto L_0x006c
        L_0x0065:
            android.support.v4.widget.ˈ r8 = r7.f2510
            if (r8 == 0) goto L_0x006c
            r8.m972(r1)
        L_0x006c:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0635.m4056(android.view.MotionEvent, int):boolean");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4052(View view, int i) {
        performItemClick(view, i, getItemIdAtPosition(i));
    }

    /* access modifiers changed from: package-private */
    public void setListSelectionHidden(boolean z) {
        this.f2506 = z;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m4054() {
        this.f2508 = false;
        setPressed(false);
        drawableStateChanged();
        View childAt = getChildAt(this.f2720 - getFirstVisiblePosition());
        if (childAt != null) {
            childAt.setPressed(false);
        }
        C0434 r0 = this.f2509;
        if (r0 != null) {
            r0.m2383();
            this.f2509 = null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4053(View view, int i, float f, float f2) {
        View childAt;
        this.f2508 = true;
        if (Build.VERSION.SDK_INT >= 21) {
            drawableHotspotChanged(f, f2);
        }
        if (!isPressed()) {
            setPressed(true);
        }
        layoutChildren();
        if (!(this.f2720 == -1 || (childAt = getChildAt(this.f2720 - getFirstVisiblePosition())) == null || childAt == view || !childAt.isPressed())) {
            childAt.setPressed(false);
        }
        this.f2720 = i;
        float left = f - ((float) view.getLeft());
        float top = f2 - ((float) view.getTop());
        if (Build.VERSION.SDK_INT >= 21) {
            view.drawableHotspotChanged(left, top);
        }
        if (!view.isPressed()) {
            view.setPressed(true);
        }
        m4271(i, view, f, f2);
        setSelectorEnabled(false);
        refreshDrawableState();
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m4055() {
        return this.f2508 || super.m4273();
    }

    public boolean isInTouchMode() {
        return (this.f2507 && this.f2506) || super.isInTouchMode();
    }

    public boolean hasWindowFocus() {
        return this.f2507 || super.hasWindowFocus();
    }

    public boolean isFocused() {
        return this.f2507 || super.isFocused();
    }

    public boolean hasFocus() {
        return this.f2507 || super.hasFocus();
    }
}
