package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.support.v4.content.C0101;
import android.support.v4.ʼ.C0287;
import android.support.v4.ʼ.ʻ.C0288;
import android.support.v4.ˈ.C0331;
import android.support.v4.ˈ.C0336;
import android.support.v4.ˈ.C0337;
import android.support.v4.ˈ.C0354;
import android.support.v7.ʻ.C0727;
import android.support.v7.ʼ.ʻ.C0739;
import android.support.ʼ.ʻ.C0751;
import android.support.ʼ.ʻ.C0760;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: android.support.v7.widget.ˑ  reason: contains not printable characters */
/* compiled from: AppCompatDrawableManager */
public final class C0656 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final PorterDuff.Mode f2619 = PorterDuff.Mode.SRC_IN;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static C0656 f2620;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final C0658 f2621 = new C0658(6);

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final int[] f2622 = {C0727.C0732.abc_textfield_search_default_mtrl_alpha, C0727.C0732.abc_textfield_default_mtrl_alpha, C0727.C0732.abc_ab_share_pack_mtrl_alpha};

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final int[] f2623 = {C0727.C0732.abc_ic_commit_search_api_mtrl_alpha, C0727.C0732.abc_seekbar_tick_mark_material, C0727.C0732.abc_ic_menu_share_mtrl_alpha, C0727.C0732.abc_ic_menu_copy_mtrl_am_alpha, C0727.C0732.abc_ic_menu_cut_mtrl_alpha, C0727.C0732.abc_ic_menu_selectall_mtrl_alpha, C0727.C0732.abc_ic_menu_paste_mtrl_am_alpha};

    /* renamed from: ˆ  reason: contains not printable characters */
    private static final int[] f2624 = {C0727.C0732.abc_textfield_activated_mtrl_alpha, C0727.C0732.abc_textfield_search_activated_mtrl_alpha, C0727.C0732.abc_cab_background_top_mtrl_alpha, C0727.C0732.abc_text_cursor_material, C0727.C0732.abc_text_select_handle_left_mtrl_dark, C0727.C0732.abc_text_select_handle_middle_mtrl_dark, C0727.C0732.abc_text_select_handle_right_mtrl_dark, C0727.C0732.abc_text_select_handle_left_mtrl_light, C0727.C0732.abc_text_select_handle_middle_mtrl_light, C0727.C0732.abc_text_select_handle_right_mtrl_light};

    /* renamed from: ˈ  reason: contains not printable characters */
    private static final int[] f2625 = {C0727.C0732.abc_popup_background_mtrl_mult, C0727.C0732.abc_cab_background_internal_bg, C0727.C0732.abc_menu_hardkey_panel_mtrl_mult};

    /* renamed from: ˉ  reason: contains not printable characters */
    private static final int[] f2626 = {C0727.C0732.abc_tab_indicator_material, C0727.C0732.abc_textfield_search_material};

    /* renamed from: ˊ  reason: contains not printable characters */
    private static final int[] f2627 = {C0727.C0732.abc_btn_check_material, C0727.C0732.abc_btn_radio_material};

    /* renamed from: ˋ  reason: contains not printable characters */
    private WeakHashMap<Context, C0354<ColorStateList>> f2628;

    /* renamed from: ˎ  reason: contains not printable characters */
    private C0331<String, C0659> f2629;

    /* renamed from: ˏ  reason: contains not printable characters */
    private C0354<String> f2630;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Object f2631 = new Object();

    /* renamed from: י  reason: contains not printable characters */
    private final WeakHashMap<Context, C0336<WeakReference<Drawable.ConstantState>>> f2632 = new WeakHashMap<>(0);

    /* renamed from: ـ  reason: contains not printable characters */
    private TypedValue f2633;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f2634;

    /* renamed from: android.support.v7.widget.ˑ$ʽ  reason: contains not printable characters */
    /* compiled from: AppCompatDrawableManager */
    private interface C0659 {
        /* renamed from: ʻ  reason: contains not printable characters */
        Drawable m4192(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0656 m4164() {
        if (f2620 == null) {
            f2620 = new C0656();
            m4168(f2620);
        }
        return f2620;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m4168(C0656 r2) {
        if (Build.VERSION.SDK_INT < 24) {
            r2.m4169("vector", new C0660());
            if (Build.VERSION.SDK_INT >= 11) {
                r2.m4169("animated-vector", new C0657());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ˑ.ʻ(android.content.Context, int, boolean):android.graphics.drawable.Drawable
     arg types: [android.content.Context, int, int]
     candidates:
      android.support.v7.widget.ˑ.ʻ(android.content.res.ColorStateList, android.graphics.PorterDuff$Mode, int[]):android.graphics.PorterDuffColorFilter
      android.support.v7.widget.ˑ.ʻ(android.content.Context, int, android.content.res.ColorStateList):void
      android.support.v7.widget.ˑ.ʻ(android.graphics.drawable.Drawable, int, android.graphics.PorterDuff$Mode):void
      android.support.v7.widget.ˑ.ʻ(android.graphics.drawable.Drawable, android.support.v7.widget.ʻˋ, int[]):void
      android.support.v7.widget.ˑ.ʻ(android.content.Context, int, android.graphics.drawable.Drawable):boolean
      android.support.v7.widget.ˑ.ʻ(android.content.Context, long, android.graphics.drawable.Drawable):boolean
      android.support.v7.widget.ˑ.ʻ(android.content.Context, android.support.v7.widget.ʻᐧ, int):android.graphics.drawable.Drawable
      android.support.v7.widget.ˑ.ʻ(android.content.Context, int, boolean):android.graphics.drawable.Drawable */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Drawable m4183(Context context, int i) {
        return m4184(context, i, false);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Drawable m4184(Context context, int i, boolean z) {
        m4182(context);
        Drawable r0 = m4178(context, i);
        if (r0 == null) {
            r0 = m4176(context, i);
        }
        if (r0 == null) {
            r0 = C0101.m539(context, i);
        }
        if (r0 != null) {
            r0 = m4162(context, i, z, r0);
        }
        if (r0 != null) {
            C0670.m4231(r0);
        }
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4186(Context context) {
        synchronized (this.f2631) {
            C0336 r3 = this.f2632.get(context);
            if (r3 != null) {
                r3.m1930();
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static long m4158(TypedValue typedValue) {
        return (((long) typedValue.assetCookie) << 32) | ((long) typedValue.data);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* renamed from: ʽ  reason: contains not printable characters */
    private Drawable m4176(Context context, int i) {
        if (this.f2633 == null) {
            this.f2633 = new TypedValue();
        }
        TypedValue typedValue = this.f2633;
        context.getResources().getValue(i, typedValue, true);
        long r3 = m4158(typedValue);
        Drawable r1 = m4163(context, r3);
        if (r1 != null) {
            return r1;
        }
        if (i == C0727.C0732.abc_cab_background_top_material) {
            r1 = new LayerDrawable(new Drawable[]{m4183(context, C0727.C0732.abc_cab_background_internal_bg), m4183(context, C0727.C0732.abc_cab_background_top_mtrl_alpha)});
        }
        if (r1 != null) {
            r1.setChangingConfigurations(typedValue.changingConfigurations);
            m4171(context, r3, r1);
        }
        return r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private Drawable m4162(Context context, int i, boolean z, Drawable drawable) {
        ColorStateList r0 = m4187(context, i);
        if (r0 != null) {
            if (C0670.m4232(drawable)) {
                drawable = drawable.mutate();
            }
            Drawable r8 = C0288.m1713(drawable);
            C0288.m1703(r8, r0);
            PorterDuff.Mode r5 = m4159(i);
            if (r5 == null) {
                return r8;
            }
            C0288.m1706(r8, r5);
            return r8;
        } else if (i == C0727.C0732.abc_seekbar_track_material) {
            LayerDrawable layerDrawable = (LayerDrawable) drawable;
            m4166(layerDrawable.findDrawableByLayerId(16908288), C0587.m3728(context, C0727.C0728.colorControlNormal), f2619);
            m4166(layerDrawable.findDrawableByLayerId(16908303), C0587.m3728(context, C0727.C0728.colorControlNormal), f2619);
            m4166(layerDrawable.findDrawableByLayerId(16908301), C0587.m3728(context, C0727.C0728.colorControlActivated), f2619);
            return drawable;
        } else if (i == C0727.C0732.abc_ratingbar_material || i == C0727.C0732.abc_ratingbar_indicator_material || i == C0727.C0732.abc_ratingbar_small_material) {
            LayerDrawable layerDrawable2 = (LayerDrawable) drawable;
            m4166(layerDrawable2.findDrawableByLayerId(16908288), C0587.m3732(context, C0727.C0728.colorControlNormal), f2619);
            m4166(layerDrawable2.findDrawableByLayerId(16908303), C0587.m3728(context, C0727.C0728.colorControlActivated), f2619);
            m4166(layerDrawable2.findDrawableByLayerId(16908301), C0587.m3728(context, C0727.C0728.colorControlActivated), f2619);
            return drawable;
        } else if (m4170(context, i, drawable) || !z) {
            return drawable;
        } else {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0075 A[Catch:{ Exception -> 0x00a4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009c A[Catch:{ Exception -> 0x00a4 }] */
    /* renamed from: ʾ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.drawable.Drawable m4178(android.content.Context r11, int r12) {
        /*
            r10 = this;
            android.support.v4.ˈ.ʻ<java.lang.String, android.support.v7.widget.ˑ$ʽ> r0 = r10.f2629
            r1 = 0
            if (r0 == 0) goto L_0x00b4
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x00b4
            android.support.v4.ˈ.י<java.lang.String> r0 = r10.f2630
            java.lang.String r2 = "appcompat_skip_skip"
            if (r0 == 0) goto L_0x0028
            java.lang.Object r0 = r0.m1984(r12)
            java.lang.String r0 = (java.lang.String) r0
            boolean r3 = r2.equals(r0)
            if (r3 != 0) goto L_0x0027
            if (r0 == 0) goto L_0x002f
            android.support.v4.ˈ.ʻ<java.lang.String, android.support.v7.widget.ˑ$ʽ> r3 = r10.f2629
            java.lang.Object r0 = r3.get(r0)
            if (r0 != 0) goto L_0x002f
        L_0x0027:
            return r1
        L_0x0028:
            android.support.v4.ˈ.י r0 = new android.support.v4.ˈ.י
            r0.<init>()
            r10.f2630 = r0
        L_0x002f:
            android.util.TypedValue r0 = r10.f2633
            if (r0 != 0) goto L_0x003a
            android.util.TypedValue r0 = new android.util.TypedValue
            r0.<init>()
            r10.f2633 = r0
        L_0x003a:
            android.util.TypedValue r0 = r10.f2633
            android.content.res.Resources r1 = r11.getResources()
            r3 = 1
            r1.getValue(r12, r0, r3)
            long r4 = m4158(r0)
            android.graphics.drawable.Drawable r6 = r10.m4163(r11, r4)
            if (r6 == 0) goto L_0x004f
            return r6
        L_0x004f:
            java.lang.CharSequence r7 = r0.string
            if (r7 == 0) goto L_0x00ac
            java.lang.CharSequence r7 = r0.string
            java.lang.String r7 = r7.toString()
            java.lang.String r8 = ".xml"
            boolean r7 = r7.endsWith(r8)
            if (r7 == 0) goto L_0x00ac
            android.content.res.XmlResourceParser r1 = r1.getXml(r12)     // Catch:{ Exception -> 0x00a4 }
            android.util.AttributeSet r7 = android.util.Xml.asAttributeSet(r1)     // Catch:{ Exception -> 0x00a4 }
        L_0x0069:
            int r8 = r1.next()     // Catch:{ Exception -> 0x00a4 }
            r9 = 2
            if (r8 == r9) goto L_0x0073
            if (r8 == r3) goto L_0x0073
            goto L_0x0069
        L_0x0073:
            if (r8 != r9) goto L_0x009c
            java.lang.String r3 = r1.getName()     // Catch:{ Exception -> 0x00a4 }
            android.support.v4.ˈ.י<java.lang.String> r8 = r10.f2630     // Catch:{ Exception -> 0x00a4 }
            r8.m1991(r12, r3)     // Catch:{ Exception -> 0x00a4 }
            android.support.v4.ˈ.ʻ<java.lang.String, android.support.v7.widget.ˑ$ʽ> r8 = r10.f2629     // Catch:{ Exception -> 0x00a4 }
            java.lang.Object r3 = r8.get(r3)     // Catch:{ Exception -> 0x00a4 }
            android.support.v7.widget.ˑ$ʽ r3 = (android.support.v7.widget.C0656.C0659) r3     // Catch:{ Exception -> 0x00a4 }
            if (r3 == 0) goto L_0x0091
            android.content.res.Resources$Theme r8 = r11.getTheme()     // Catch:{ Exception -> 0x00a4 }
            android.graphics.drawable.Drawable r1 = r3.m4192(r11, r1, r7, r8)     // Catch:{ Exception -> 0x00a4 }
            r6 = r1
        L_0x0091:
            if (r6 == 0) goto L_0x00ac
            int r0 = r0.changingConfigurations     // Catch:{ Exception -> 0x00a4 }
            r6.setChangingConfigurations(r0)     // Catch:{ Exception -> 0x00a4 }
            r10.m4171(r11, r4, r6)     // Catch:{ Exception -> 0x00a4 }
            goto L_0x00ac
        L_0x009c:
            org.xmlpull.v1.XmlPullParserException r11 = new org.xmlpull.v1.XmlPullParserException     // Catch:{ Exception -> 0x00a4 }
            java.lang.String r0 = "No start tag found"
            r11.<init>(r0)     // Catch:{ Exception -> 0x00a4 }
            throw r11     // Catch:{ Exception -> 0x00a4 }
        L_0x00a4:
            r11 = move-exception
            java.lang.String r0 = "AppCompatDrawableManager"
            java.lang.String r1 = "Exception while inflating drawable"
            android.util.Log.e(r0, r1, r11)
        L_0x00ac:
            if (r6 != 0) goto L_0x00b3
            android.support.v4.ˈ.י<java.lang.String> r11 = r10.f2630
            r11.m1991(r12, r2)
        L_0x00b3:
            return r6
        L_0x00b4:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0656.m4178(android.content.Context, int):android.graphics.drawable.Drawable");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002e, code lost:
        return null;
     */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.drawable.Drawable m4163(android.content.Context r5, long r6) {
        /*
            r4 = this;
            java.lang.Object r0 = r4.f2631
            monitor-enter(r0)
            java.util.WeakHashMap<android.content.Context, android.support.v4.ˈ.ˆ<java.lang.ref.WeakReference<android.graphics.drawable.Drawable$ConstantState>>> r1 = r4.f2632     // Catch:{ all -> 0x002f }
            java.lang.Object r1 = r1.get(r5)     // Catch:{ all -> 0x002f }
            android.support.v4.ˈ.ˆ r1 = (android.support.v4.ˈ.C0336) r1     // Catch:{ all -> 0x002f }
            r2 = 0
            if (r1 != 0) goto L_0x0010
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            return r2
        L_0x0010:
            java.lang.Object r3 = r1.m1922(r6)     // Catch:{ all -> 0x002f }
            java.lang.ref.WeakReference r3 = (java.lang.ref.WeakReference) r3     // Catch:{ all -> 0x002f }
            if (r3 == 0) goto L_0x002d
            java.lang.Object r3 = r3.get()     // Catch:{ all -> 0x002f }
            android.graphics.drawable.Drawable$ConstantState r3 = (android.graphics.drawable.Drawable.ConstantState) r3     // Catch:{ all -> 0x002f }
            if (r3 == 0) goto L_0x002a
            android.content.res.Resources r5 = r5.getResources()     // Catch:{ all -> 0x002f }
            android.graphics.drawable.Drawable r5 = r3.newDrawable(r5)     // Catch:{ all -> 0x002f }
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            return r5
        L_0x002a:
            r1.m1927(r6)     // Catch:{ all -> 0x002f }
        L_0x002d:
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            return r2
        L_0x002f:
            r5 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0656.m4163(android.content.Context, long):android.graphics.drawable.Drawable");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m4171(Context context, long j, Drawable drawable) {
        Drawable.ConstantState constantState = drawable.getConstantState();
        if (constantState == null) {
            return false;
        }
        synchronized (this.f2631) {
            C0336 r1 = this.f2632.get(context);
            if (r1 == null) {
                r1 = new C0336();
                this.f2632.put(context, r1);
            }
            r1.m1928(j, new WeakReference(constantState));
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Drawable m4185(Context context, C0600 r3, int i) {
        Drawable r0 = m4178(context, i);
        if (r0 == null) {
            r0 = r3.m3820(i);
        }
        if (r0 != null) {
            return m4162(context, i, false, r0);
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0061 A[RETURN] */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static boolean m4170(android.content.Context r6, int r7, android.graphics.drawable.Drawable r8) {
        /*
            android.graphics.PorterDuff$Mode r0 = android.support.v7.widget.C0656.f2619
            int[] r1 = android.support.v7.widget.C0656.f2622
            boolean r1 = m4173(r1, r7)
            r2 = 16842801(0x1010031, float:2.3693695E-38)
            r3 = -1
            r4 = 0
            r5 = 1
            if (r1 == 0) goto L_0x0015
            int r2 = android.support.v7.ʻ.C0727.C0728.colorControlNormal
        L_0x0012:
            r7 = 1
            r1 = -1
            goto L_0x0044
        L_0x0015:
            int[] r1 = android.support.v7.widget.C0656.f2624
            boolean r1 = m4173(r1, r7)
            if (r1 == 0) goto L_0x0020
            int r2 = android.support.v7.ʻ.C0727.C0728.colorControlActivated
            goto L_0x0012
        L_0x0020:
            int[] r1 = android.support.v7.widget.C0656.f2625
            boolean r1 = m4173(r1, r7)
            if (r1 == 0) goto L_0x002b
            android.graphics.PorterDuff$Mode r0 = android.graphics.PorterDuff.Mode.MULTIPLY
            goto L_0x0012
        L_0x002b:
            int r1 = android.support.v7.ʻ.C0727.C0732.abc_list_divider_mtrl_alpha
            if (r7 != r1) goto L_0x003c
            r2 = 16842800(0x1010030, float:2.3693693E-38)
            r7 = 1109603123(0x42233333, float:40.8)
            int r7 = java.lang.Math.round(r7)
            r1 = r7
            r7 = 1
            goto L_0x0044
        L_0x003c:
            int r1 = android.support.v7.ʻ.C0727.C0732.abc_dialog_material_background
            if (r7 != r1) goto L_0x0041
            goto L_0x0012
        L_0x0041:
            r7 = 0
            r1 = -1
            r2 = 0
        L_0x0044:
            if (r7 == 0) goto L_0x0061
            boolean r7 = android.support.v7.widget.C0670.m4232(r8)
            if (r7 == 0) goto L_0x0050
            android.graphics.drawable.Drawable r8 = r8.mutate()
        L_0x0050:
            int r6 = android.support.v7.widget.C0587.m3728(r6, r2)
            android.graphics.PorterDuffColorFilter r6 = m4160(r6, r0)
            r8.setColorFilter(r6)
            if (r1 == r3) goto L_0x0060
            r8.setAlpha(r1)
        L_0x0060:
            return r5
        L_0x0061:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0656.m4170(android.content.Context, int, android.graphics.drawable.Drawable):boolean");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4169(String str, C0659 r3) {
        if (this.f2629 == null) {
            this.f2629 = new C0331<>();
        }
        this.f2629.put(str, r3);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean m4173(int[] iArr, int i) {
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static PorterDuff.Mode m4159(int i) {
        if (i == C0727.C0732.abc_switch_thumb_material) {
            return PorterDuff.Mode.MULTIPLY;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public ColorStateList m4187(Context context, int i) {
        ColorStateList r0 = m4180(context, i);
        if (r0 == null) {
            if (i == C0727.C0732.abc_edit_text_material) {
                r0 = C0739.m4880(context, C0727.C0730.abc_tint_edittext);
            } else if (i == C0727.C0732.abc_switch_track_mtrl_alpha) {
                r0 = C0739.m4880(context, C0727.C0730.abc_tint_switch_track);
            } else if (i == C0727.C0732.abc_switch_thumb_material) {
                r0 = m4179(context);
            } else if (i == C0727.C0732.abc_btn_default_mtrl_shape) {
                r0 = m4174(context);
            } else if (i == C0727.C0732.abc_btn_borderless_material) {
                r0 = m4175(context);
            } else if (i == C0727.C0732.abc_btn_colored_material) {
                r0 = m4177(context);
            } else if (i == C0727.C0732.abc_spinner_mtrl_am_alpha || i == C0727.C0732.abc_spinner_textfield_background_material) {
                r0 = C0739.m4880(context, C0727.C0730.abc_tint_spinner);
            } else if (m4173(f2623, i)) {
                r0 = C0587.m3731(context, C0727.C0728.colorControlNormal);
            } else if (m4173(f2626, i)) {
                r0 = C0739.m4880(context, C0727.C0730.abc_tint_default);
            } else if (m4173(f2627, i)) {
                r0 = C0739.m4880(context, C0727.C0730.abc_tint_btn_checkable);
            } else if (i == C0727.C0732.abc_seekbar_thumb_material) {
                r0 = C0739.m4880(context, C0727.C0730.abc_tint_seek_thumb);
            }
            if (r0 != null) {
                m4165(context, i, r0);
            }
        }
        return r0;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private ColorStateList m4180(Context context, int i) {
        C0354 r3;
        WeakHashMap<Context, C0354<ColorStateList>> weakHashMap = this.f2628;
        if (weakHashMap == null || (r3 = weakHashMap.get(context)) == null) {
            return null;
        }
        return (ColorStateList) r3.m1984(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4165(Context context, int i, ColorStateList colorStateList) {
        if (this.f2628 == null) {
            this.f2628 = new WeakHashMap<>();
        }
        C0354 r0 = this.f2628.get(context);
        if (r0 == null) {
            r0 = new C0354();
            this.f2628.put(context, r0);
        }
        r0.m1991(i, colorStateList);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private ColorStateList m4174(Context context) {
        return m4181(context, C0587.m3728(context, C0727.C0728.colorButtonNormal));
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private ColorStateList m4175(Context context) {
        return m4181(context, 0);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private ColorStateList m4177(Context context) {
        return m4181(context, C0587.m3728(context, C0727.C0728.colorAccent));
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private ColorStateList m4181(Context context, int i) {
        int r2 = C0587.m3728(context, C0727.C0728.colorControlHighlight);
        int r6 = C0587.m3732(context, C0727.C0728.colorButtonNormal);
        return new ColorStateList(new int[][]{C0587.f2305, C0587.f2308, C0587.f2306, C0587.f2312}, new int[]{r6, C0287.m1695(r2, i), C0287.m1695(r2, i), i});
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private ColorStateList m4179(Context context) {
        int[][] iArr = new int[3][];
        int[] iArr2 = new int[3];
        ColorStateList r2 = C0587.m3731(context, C0727.C0728.colorSwitchThumbNormal);
        if (r2 == null || !r2.isStateful()) {
            iArr[0] = C0587.f2305;
            iArr2[0] = C0587.m3732(context, C0727.C0728.colorSwitchThumbNormal);
            iArr[1] = C0587.f2309;
            iArr2[1] = C0587.m3728(context, C0727.C0728.colorControlActivated);
            iArr[2] = C0587.f2312;
            iArr2[2] = C0587.m3728(context, C0727.C0728.colorSwitchThumbNormal);
        } else {
            iArr[0] = C0587.f2305;
            iArr2[0] = r2.getColorForState(iArr[0], 0);
            iArr[1] = C0587.f2309;
            iArr2[1] = C0587.m3728(context, C0727.C0728.colorControlActivated);
            iArr[2] = C0587.f2312;
            iArr2[2] = r2.getDefaultColor();
        }
        return new ColorStateList(iArr, iArr2);
    }

    /* renamed from: android.support.v7.widget.ˑ$ʼ  reason: contains not printable characters */
    /* compiled from: AppCompatDrawableManager */
    private static class C0658 extends C0337<Integer, PorterDuffColorFilter> {
        public C0658(int i) {
            super(i);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public PorterDuffColorFilter m4190(int i, PorterDuff.Mode mode) {
            return (PorterDuffColorFilter) m1932((String) Integer.valueOf(m4189(i, mode)));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public PorterDuffColorFilter m4191(int i, PorterDuff.Mode mode, PorterDuffColorFilter porterDuffColorFilter) {
            return (PorterDuffColorFilter) m1933(Integer.valueOf(m4189(i, mode)), porterDuffColorFilter);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private static int m4189(int i, PorterDuff.Mode mode) {
            return ((i + 31) * 31) + mode.hashCode();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m4167(Drawable drawable, C0590 r3, int[] iArr) {
        if (!C0670.m4232(drawable) || drawable.mutate() == drawable) {
            if (r3.f2322 || r3.f2321) {
                drawable.setColorFilter(m4161(r3.f2322 ? r3.f2319 : null, r3.f2321 ? r3.f2320 : f2619, iArr));
            } else {
                drawable.clearColorFilter();
            }
            if (Build.VERSION.SDK_INT <= 23) {
                drawable.invalidateSelf();
                return;
            }
            return;
        }
        Log.d("AppCompatDrawableManager", "Mutated drawable is not the same instance as the input.");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static PorterDuffColorFilter m4161(ColorStateList colorStateList, PorterDuff.Mode mode, int[] iArr) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return m4160(colorStateList.getColorForState(iArr, 0), mode);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static PorterDuffColorFilter m4160(int i, PorterDuff.Mode mode) {
        PorterDuffColorFilter r0 = f2621.m4190(i, mode);
        if (r0 != null) {
            return r0;
        }
        PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(i, mode);
        f2621.m4191(i, mode, porterDuffColorFilter);
        return porterDuffColorFilter;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m4166(Drawable drawable, int i, PorterDuff.Mode mode) {
        if (C0670.m4232(drawable)) {
            drawable = drawable.mutate();
        }
        if (mode == null) {
            mode = f2619;
        }
        drawable.setColorFilter(m4160(i, mode));
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private void m4182(Context context) {
        if (!this.f2634) {
            this.f2634 = true;
            Drawable r2 = m4183(context, C0727.C0732.abc_vector_test);
            if (r2 == null || !m4172(r2)) {
                this.f2634 = false;
                throw new IllegalStateException("This app has been built with an incorrect configuration. Please configure your build for VectorDrawableCompat.");
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean m4172(Drawable drawable) {
        return (drawable instanceof C0760) || "android.graphics.drawable.VectorDrawable".equals(drawable.getClass().getName());
    }

    /* renamed from: android.support.v7.widget.ˑ$ʾ  reason: contains not printable characters */
    /* compiled from: AppCompatDrawableManager */
    private static class C0660 implements C0659 {
        C0660() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Drawable m4193(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return C0760.m4935(context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("VdcInflateDelegate", "Exception while inflating <vector>", e);
                return null;
            }
        }
    }

    /* renamed from: android.support.v7.widget.ˑ$ʻ  reason: contains not printable characters */
    /* compiled from: AppCompatDrawableManager */
    private static class C0657 implements C0659 {
        C0657() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Drawable m4188(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return C0751.m4899(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("AvdcInflateDelegate", "Exception while inflating <animated-vector>", e);
                return null;
            }
        }
    }
}
