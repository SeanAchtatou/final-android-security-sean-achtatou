package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.support.v7.view.menu.C0503;
import android.support.v7.view.menu.C0504;
import android.support.v7.view.menu.C0508;
import android.support.v7.view.menu.ListMenuItemView;
import android.transition.Transition;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import java.lang.reflect.Method;

/* renamed from: android.support.v7.widget.ᵎᵎ  reason: contains not printable characters */
/* compiled from: MenuPopupWindow */
public class C0679 extends C0661 implements C0668 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Method f2705;

    /* renamed from: ʼ  reason: contains not printable characters */
    private C0668 f2706;

    static {
        Class<PopupWindow> cls = PopupWindow.class;
        try {
            f2705 = cls.getDeclaredMethod("setTouchModal", Boolean.TYPE);
        } catch (NoSuchMethodException unused) {
            Log.i("MenuPopupWindow", "Could not find method setTouchModal() on PopupWindow. Oh well.");
        }
    }

    public C0679(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0635 m4256(Context context, boolean z) {
        C0680 r0 = new C0680(context, z);
        r0.setHoverListener(this);
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4259(Object obj) {
        if (Build.VERSION.SDK_INT >= 23) {
            this.f2648.setEnterTransition((Transition) obj);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4261(Object obj) {
        if (Build.VERSION.SDK_INT >= 23) {
            this.f2648.setExitTransition((Transition) obj);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4258(C0668 r1) {
        this.f2706 = r1;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m4262(boolean z) {
        Method method = f2705;
        if (method != null) {
            try {
                method.invoke(this.f2648, Boolean.valueOf(z));
            } catch (Exception unused) {
                Log.i("MenuPopupWindow", "Could not invoke setTouchModal() on PopupWindow. Oh well.");
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4260(C0504 r2, MenuItem menuItem) {
        C0668 r0 = this.f2706;
        if (r0 != null) {
            r0.m4229(r2, menuItem);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4257(C0504 r2, MenuItem menuItem) {
        C0668 r0 = this.f2706;
        if (r0 != null) {
            r0.m4228(r2, menuItem);
        }
    }

    /* renamed from: android.support.v7.widget.ᵎᵎ$ʻ  reason: contains not printable characters */
    /* compiled from: MenuPopupWindow */
    public static class C0680 extends C0635 {

        /* renamed from: ˈ  reason: contains not printable characters */
        final int f2707;

        /* renamed from: ˉ  reason: contains not printable characters */
        final int f2708;

        /* renamed from: ˊ  reason: contains not printable characters */
        private C0668 f2709;

        /* renamed from: ˋ  reason: contains not printable characters */
        private MenuItem f2710;

        public /* bridge */ /* synthetic */ boolean hasFocus() {
            return super.hasFocus();
        }

        public /* bridge */ /* synthetic */ boolean hasWindowFocus() {
            return super.hasWindowFocus();
        }

        public /* bridge */ /* synthetic */ boolean isFocused() {
            return super.isFocused();
        }

        public /* bridge */ /* synthetic */ boolean isInTouchMode() {
            return super.isInTouchMode();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public /* bridge */ /* synthetic */ boolean m4263(MotionEvent motionEvent, int i) {
            return super.m4056(motionEvent, i);
        }

        public C0680(Context context, boolean z) {
            super(context, z);
            Configuration configuration = context.getResources().getConfiguration();
            if (Build.VERSION.SDK_INT < 17 || 1 != configuration.getLayoutDirection()) {
                this.f2707 = 22;
                this.f2708 = 21;
                return;
            }
            this.f2707 = 21;
            this.f2708 = 22;
        }

        public void setHoverListener(C0668 r1) {
            this.f2709 = r1;
        }

        public boolean onKeyDown(int i, KeyEvent keyEvent) {
            ListMenuItemView listMenuItemView = (ListMenuItemView) getSelectedView();
            if (listMenuItemView != null && i == this.f2707) {
                if (listMenuItemView.isEnabled() && listMenuItemView.getItemData().hasSubMenu()) {
                    performItemClick(listMenuItemView, getSelectedItemPosition(), getSelectedItemId());
                }
                return true;
            } else if (listMenuItemView == null || i != this.f2708) {
                return super.onKeyDown(i, keyEvent);
            } else {
                setSelection(-1);
                ((C0503) getAdapter()).m2870().m2907(false);
                return true;
            }
        }

        public boolean onHoverEvent(MotionEvent motionEvent) {
            int i;
            C0503 r0;
            int pointToPosition;
            int i2;
            if (this.f2709 != null) {
                ListAdapter adapter = getAdapter();
                if (adapter instanceof HeaderViewListAdapter) {
                    HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
                    i = headerViewListAdapter.getHeadersCount();
                    r0 = (C0503) headerViewListAdapter.getWrappedAdapter();
                } else {
                    i = 0;
                    r0 = (C0503) adapter;
                }
                C0508 r2 = null;
                if (motionEvent.getAction() != 10 && (pointToPosition = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY())) != -1 && (i2 = pointToPosition - i) >= 0 && i2 < r0.getCount()) {
                    r2 = r0.getItem(i2);
                }
                MenuItem menuItem = this.f2710;
                if (menuItem != r2) {
                    C0504 r02 = r0.m2870();
                    if (menuItem != null) {
                        this.f2709.m4228(r02, menuItem);
                    }
                    this.f2710 = r2;
                    if (r2 != null) {
                        this.f2709.m4229(r02, r2);
                    }
                }
            }
            return super.onHoverEvent(motionEvent);
        }
    }
}
