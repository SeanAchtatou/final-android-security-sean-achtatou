package android.support.v7.widget;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Shader;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.support.v4.ʼ.ʻ.C0294;
import android.util.AttributeSet;
import android.widget.ProgressBar;

/* renamed from: android.support.v7.widget.ᵔ  reason: contains not printable characters */
/* compiled from: AppCompatProgressBarHelper */
class C0681 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final int[] f2711 = {16843067, 16843068};

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ProgressBar f2712;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Bitmap f2713;

    C0681(ProgressBar progressBar) {
        this.f2712 = progressBar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ᵔ.ʻ(android.graphics.drawable.Drawable, boolean):android.graphics.drawable.Drawable
     arg types: [android.graphics.drawable.Drawable, int]
     candidates:
      android.support.v7.widget.ᵔ.ʻ(android.util.AttributeSet, int):void
      android.support.v7.widget.ᵔ.ʻ(android.graphics.drawable.Drawable, boolean):android.graphics.drawable.Drawable */
    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4268(AttributeSet attributeSet, int i) {
        C0592 r4 = C0592.m3740(this.f2712.getContext(), attributeSet, f2711, i, 0);
        Drawable r5 = r4.m3748(0);
        if (r5 != null) {
            this.f2712.setIndeterminateDrawable(m4264(r5));
        }
        Drawable r52 = r4.m3748(1);
        if (r52 != null) {
            this.f2712.setProgressDrawable(m4265(r52, false));
        }
        r4.m3745();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private Drawable m4265(Drawable drawable, boolean z) {
        if (drawable instanceof C0294) {
            C0294 r0 = (C0294) drawable;
            Drawable r1 = r0.m1750();
            if (r1 == null) {
                return drawable;
            }
            r0.m1751(m4265(r1, z));
            return drawable;
        } else if (drawable instanceof LayerDrawable) {
            LayerDrawable layerDrawable = (LayerDrawable) drawable;
            int numberOfLayers = layerDrawable.getNumberOfLayers();
            Drawable[] drawableArr = new Drawable[numberOfLayers];
            for (int i = 0; i < numberOfLayers; i++) {
                int id = layerDrawable.getId(i);
                drawableArr[i] = m4265(layerDrawable.getDrawable(i), id == 16908301 || id == 16908303);
            }
            LayerDrawable layerDrawable2 = new LayerDrawable(drawableArr);
            for (int i2 = 0; i2 < numberOfLayers; i2++) {
                layerDrawable2.setId(i2, layerDrawable.getId(i2));
            }
            return layerDrawable2;
        } else if (!(drawable instanceof BitmapDrawable)) {
            return drawable;
        } else {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            if (this.f2713 == null) {
                this.f2713 = bitmap;
            }
            ShapeDrawable shapeDrawable = new ShapeDrawable(m4266());
            shapeDrawable.getPaint().setShader(new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.CLAMP));
            shapeDrawable.getPaint().setColorFilter(bitmapDrawable.getPaint().getColorFilter());
            return z ? new ClipDrawable(shapeDrawable, 3, 1) : shapeDrawable;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ᵔ.ʻ(android.graphics.drawable.Drawable, boolean):android.graphics.drawable.Drawable
     arg types: [android.graphics.drawable.Drawable, int]
     candidates:
      android.support.v7.widget.ᵔ.ʻ(android.util.AttributeSet, int):void
      android.support.v7.widget.ᵔ.ʻ(android.graphics.drawable.Drawable, boolean):android.graphics.drawable.Drawable */
    /* renamed from: ʻ  reason: contains not printable characters */
    private Drawable m4264(Drawable drawable) {
        if (!(drawable instanceof AnimationDrawable)) {
            return drawable;
        }
        AnimationDrawable animationDrawable = (AnimationDrawable) drawable;
        int numberOfFrames = animationDrawable.getNumberOfFrames();
        AnimationDrawable animationDrawable2 = new AnimationDrawable();
        animationDrawable2.setOneShot(animationDrawable.isOneShot());
        for (int i = 0; i < numberOfFrames; i++) {
            Drawable r4 = m4265(animationDrawable.getFrame(i), true);
            r4.setLevel(10000);
            animationDrawable2.addFrame(r4, animationDrawable.getDuration(i));
        }
        animationDrawable2.setLevel(10000);
        return animationDrawable2;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private Shape m4266() {
        return new RoundRectShape(new float[]{5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f}, null, null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Bitmap m4267() {
        return this.f2713;
    }
}
