package android.support.v7.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.ʼ.ʻ.C0288;
import android.support.v7.ʽ.ʻ.C0742;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.lang.reflect.Field;

/* renamed from: android.support.v7.widget.ᵔᵔ  reason: contains not printable characters */
/* compiled from: ListViewCompat */
public class C0682 extends ListView {

    /* renamed from: ˈ  reason: contains not printable characters */
    private static final int[] f2714 = {0};

    /* renamed from: ʻ  reason: contains not printable characters */
    final Rect f2715 = new Rect();

    /* renamed from: ʼ  reason: contains not printable characters */
    int f2716 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    int f2717 = 0;

    /* renamed from: ʾ  reason: contains not printable characters */
    int f2718 = 0;

    /* renamed from: ʿ  reason: contains not printable characters */
    int f2719 = 0;

    /* renamed from: ˆ  reason: contains not printable characters */
    protected int f2720;

    /* renamed from: ˉ  reason: contains not printable characters */
    private Field f2721;

    /* renamed from: ˊ  reason: contains not printable characters */
    private C0683 f2722;

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m4273() {
        return false;
    }

    public C0682(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        try {
            this.f2721 = AbsListView.class.getDeclaredField("mIsChildViewEnabled");
            this.f2721.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    public void setSelector(Drawable drawable) {
        this.f2722 = drawable != null ? new C0683(drawable) : null;
        super.setSelector(this.f2722);
        Rect rect = new Rect();
        if (drawable != null) {
            drawable.getPadding(rect);
        }
        this.f2716 = rect.left;
        this.f2717 = rect.top;
        this.f2718 = rect.right;
        this.f2719 = rect.bottom;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        setSelectorEnabled(true);
        m4274();
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        m4272(canvas);
        super.dispatchDraw(canvas);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.f2720 = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
        }
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4274() {
        Drawable selector = getSelector();
        if (selector != null && m4276()) {
            selector.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m4276() {
        return m4273() && isPressed();
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4272(Canvas canvas) {
        Drawable selector;
        if (!this.f2715.isEmpty() && (selector = getSelector()) != null) {
            selector.setBounds(this.f2715);
            selector.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4271(int i, View view, float f, float f2) {
        m4270(i, view);
        Drawable selector = getSelector();
        if (selector != null && i != -1) {
            C0288.m1700(selector, f, f2);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4270(int i, View view) {
        Drawable selector = getSelector();
        boolean z = true;
        boolean z2 = (selector == null || i == -1) ? false : true;
        if (z2) {
            selector.setVisible(false, false);
        }
        m4275(i, view);
        if (z2) {
            Rect rect = this.f2715;
            float exactCenterX = rect.exactCenterX();
            float exactCenterY = rect.exactCenterY();
            if (getVisibility() != 0) {
                z = false;
            }
            selector.setVisible(z, false);
            C0288.m1700(selector, exactCenterX, exactCenterY);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4275(int i, View view) {
        Rect rect = this.f2715;
        rect.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        rect.left -= this.f2716;
        rect.top -= this.f2717;
        rect.right += this.f2718;
        rect.bottom += this.f2719;
        try {
            boolean z = this.f2721.getBoolean(this);
            if (view.isEnabled() != z) {
                this.f2721.set(this, Boolean.valueOf(!z));
                if (i != -1) {
                    refreshDrawableState();
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m4269(int i, int i2, int i3, int i4, int i5) {
        int i6;
        int listPaddingTop = getListPaddingTop();
        int listPaddingBottom = getListPaddingBottom();
        getListPaddingLeft();
        getListPaddingRight();
        int dividerHeight = getDividerHeight();
        Drawable divider = getDivider();
        ListAdapter adapter = getAdapter();
        if (adapter == null) {
            return listPaddingTop + listPaddingBottom;
        }
        int i7 = listPaddingTop + listPaddingBottom;
        if (dividerHeight <= 0 || divider == null) {
            dividerHeight = 0;
        }
        int count = adapter.getCount();
        int i8 = i7;
        View view = null;
        int i9 = 0;
        int i10 = 0;
        int i11 = 0;
        while (i9 < count) {
            int itemViewType = adapter.getItemViewType(i9);
            if (itemViewType != i10) {
                view = null;
                i10 = itemViewType;
            }
            view = adapter.getView(i9, view, this);
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = generateDefaultLayoutParams();
                view.setLayoutParams(layoutParams);
            }
            if (layoutParams.height > 0) {
                i6 = View.MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824);
            } else {
                i6 = View.MeasureSpec.makeMeasureSpec(0, 0);
            }
            view.measure(i, i6);
            view.forceLayout();
            if (i9 > 0) {
                i8 += dividerHeight;
            }
            i8 += view.getMeasuredHeight();
            if (i8 >= i4) {
                return (i5 < 0 || i9 <= i5 || i11 <= 0 || i8 == i4) ? i4 : i11;
            }
            if (i5 >= 0 && i9 >= i5) {
                i11 = i8;
            }
            i9++;
        }
        return i8;
    }

    /* access modifiers changed from: protected */
    public void setSelectorEnabled(boolean z) {
        C0683 r0 = this.f2722;
        if (r0 != null) {
            r0.m4277(z);
        }
    }

    /* renamed from: android.support.v7.widget.ᵔᵔ$ʻ  reason: contains not printable characters */
    /* compiled from: ListViewCompat */
    private static class C0683 extends C0742 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f2723 = true;

        public C0683(Drawable drawable) {
            super(drawable);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4277(boolean z) {
            this.f2723 = z;
        }

        public boolean setState(int[] iArr) {
            if (this.f2723) {
                return super.setState(iArr);
            }
            return false;
        }

        public void draw(Canvas canvas) {
            if (this.f2723) {
                super.draw(canvas);
            }
        }

        public void setHotspot(float f, float f2) {
            if (this.f2723) {
                super.setHotspot(f, f2);
            }
        }

        public void setHotspotBounds(int i, int i2, int i3, int i4) {
            if (this.f2723) {
                super.setHotspotBounds(i, i2, i3, i4);
            }
        }

        public boolean setVisible(boolean z, boolean z2) {
            if (this.f2723) {
                return super.setVisible(z, z2);
            }
            return false;
        }
    }
}
