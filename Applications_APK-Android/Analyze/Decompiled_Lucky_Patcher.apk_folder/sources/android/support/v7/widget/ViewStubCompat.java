package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import java.lang.ref.WeakReference;

public final class ViewStubCompat extends View {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f2228;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f2229;

    /* renamed from: ʽ  reason: contains not printable characters */
    private WeakReference<View> f2230;

    /* renamed from: ʾ  reason: contains not printable characters */
    private LayoutInflater f2231;

    /* renamed from: ʿ  reason: contains not printable characters */
    private C0574 f2232;

    /* renamed from: android.support.v7.widget.ViewStubCompat$ʻ  reason: contains not printable characters */
    public interface C0574 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m3623(ViewStubCompat viewStubCompat, View view);
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
    }

    public void draw(Canvas canvas) {
    }

    public ViewStubCompat(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ViewStubCompat(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2228 = 0;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0727.C0737.ViewStubCompat, i, 0);
        this.f2229 = obtainStyledAttributes.getResourceId(C0727.C0737.ViewStubCompat_android_inflatedId, -1);
        this.f2228 = obtainStyledAttributes.getResourceId(C0727.C0737.ViewStubCompat_android_layout, 0);
        setId(obtainStyledAttributes.getResourceId(C0727.C0737.ViewStubCompat_android_id, -1));
        obtainStyledAttributes.recycle();
        setVisibility(8);
        setWillNotDraw(true);
    }

    public int getInflatedId() {
        return this.f2229;
    }

    public void setInflatedId(int i) {
        this.f2229 = i;
    }

    public int getLayoutResource() {
        return this.f2228;
    }

    public void setLayoutResource(int i) {
        this.f2228 = i;
    }

    public void setLayoutInflater(LayoutInflater layoutInflater) {
        this.f2231 = layoutInflater;
    }

    public LayoutInflater getLayoutInflater() {
        return this.f2231;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(0, 0);
    }

    public void setVisibility(int i) {
        WeakReference<View> weakReference = this.f2230;
        if (weakReference != null) {
            View view = weakReference.get();
            if (view != null) {
                view.setVisibility(i);
                return;
            }
            throw new IllegalStateException("setVisibility called on un-referenced view");
        }
        super.setVisibility(i);
        if (i == 0 || i == 4) {
            m3622();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ʻ  reason: contains not printable characters */
    public View m3622() {
        ViewParent parent = getParent();
        if (parent == null || !(parent instanceof ViewGroup)) {
            throw new IllegalStateException("ViewStub must have a non-null ViewGroup viewParent");
        } else if (this.f2228 != 0) {
            ViewGroup viewGroup = (ViewGroup) parent;
            LayoutInflater layoutInflater = this.f2231;
            if (layoutInflater == null) {
                layoutInflater = LayoutInflater.from(getContext());
            }
            View inflate = layoutInflater.inflate(this.f2228, viewGroup, false);
            int i = this.f2229;
            if (i != -1) {
                inflate.setId(i);
            }
            int indexOfChild = viewGroup.indexOfChild(this);
            viewGroup.removeViewInLayout(this);
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            if (layoutParams != null) {
                viewGroup.addView(inflate, indexOfChild, layoutParams);
            } else {
                viewGroup.addView(inflate, indexOfChild);
            }
            this.f2230 = new WeakReference<>(inflate);
            C0574 r0 = this.f2232;
            if (r0 != null) {
                r0.m3623(this, inflate);
            }
            return inflate;
        } else {
            throw new IllegalArgumentException("ViewStub must have a valid layoutResource");
        }
    }

    public void setOnInflateListener(C0574 r1) {
        this.f2232 = r1;
    }
}
