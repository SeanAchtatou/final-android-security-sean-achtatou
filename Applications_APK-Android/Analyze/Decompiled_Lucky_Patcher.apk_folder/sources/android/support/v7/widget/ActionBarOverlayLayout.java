package android.support.v7.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.ˉ.C0409;
import android.support.v4.ˉ.C0411;
import android.support.v4.ˉ.C0414;
import android.support.v7.view.menu.C0518;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.widget.OverScroller;

public class ActionBarOverlayLayout extends ViewGroup implements C0409, C0609 {

    /* renamed from: ʿ  reason: contains not printable characters */
    static final int[] f1926 = {C0727.C0728.actionBarSize, 16842841};

    /* renamed from: ʻ  reason: contains not printable characters */
    ActionBarContainer f1927;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private final C0411 f1928;

    /* renamed from: ʼ  reason: contains not printable characters */
    boolean f1929;

    /* renamed from: ʽ  reason: contains not printable characters */
    ViewPropertyAnimator f1930;

    /* renamed from: ʾ  reason: contains not printable characters */
    final AnimatorListenerAdapter f1931;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f1932;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f1933;

    /* renamed from: ˉ  reason: contains not printable characters */
    private ContentFrameLayout f1934;

    /* renamed from: ˊ  reason: contains not printable characters */
    private C0631 f1935;

    /* renamed from: ˋ  reason: contains not printable characters */
    private Drawable f1936;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f1937;

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f1938;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f1939;

    /* renamed from: י  reason: contains not printable characters */
    private boolean f1940;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f1941;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f1942;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final Rect f1943;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private final Runnable f1944;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final Rect f1945;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private final Runnable f1946;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private final Rect f1947;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private final Rect f1948;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private final Rect f1949;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private final Rect f1950;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private final Rect f1951;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private C0541 f1952;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final int f1953;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private OverScroller f1954;

    /* renamed from: android.support.v7.widget.ActionBarOverlayLayout$ʻ  reason: contains not printable characters */
    public interface C0541 {
        /* renamed from: ʼ  reason: contains not printable characters */
        void m3183(int i);

        /* renamed from: ˈ  reason: contains not printable characters */
        void m3184(boolean z);

        /* renamed from: ˋ  reason: contains not printable characters */
        void m3185();

        /* renamed from: ˎ  reason: contains not printable characters */
        void m3186();

        /* renamed from: ˏ  reason: contains not printable characters */
        void m3187();

        /* renamed from: ˑ  reason: contains not printable characters */
        void m3188();
    }

    public boolean onNestedPreFling(View view, float f, float f2) {
        return false;
    }

    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
    }

    public void setShowingForActionMode(boolean z) {
    }

    public void setUiOptions(int i) {
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public ActionBarOverlayLayout(Context context) {
        this(context, null);
    }

    public ActionBarOverlayLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f1933 = 0;
        this.f1943 = new Rect();
        this.f1945 = new Rect();
        this.f1947 = new Rect();
        this.f1948 = new Rect();
        this.f1949 = new Rect();
        this.f1950 = new Rect();
        this.f1951 = new Rect();
        this.f1953 = 600;
        this.f1931 = new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animator) {
                ActionBarOverlayLayout actionBarOverlayLayout = ActionBarOverlayLayout.this;
                actionBarOverlayLayout.f1930 = null;
                actionBarOverlayLayout.f1929 = false;
            }

            public void onAnimationCancel(Animator animator) {
                ActionBarOverlayLayout actionBarOverlayLayout = ActionBarOverlayLayout.this;
                actionBarOverlayLayout.f1930 = null;
                actionBarOverlayLayout.f1929 = false;
            }
        };
        this.f1944 = new Runnable() {
            public void run() {
                ActionBarOverlayLayout.this.m3175();
                ActionBarOverlayLayout actionBarOverlayLayout = ActionBarOverlayLayout.this;
                actionBarOverlayLayout.f1930 = actionBarOverlayLayout.f1927.animate().translationY(0.0f).setListener(ActionBarOverlayLayout.this.f1931);
            }
        };
        this.f1946 = new Runnable() {
            public void run() {
                ActionBarOverlayLayout.this.m3175();
                ActionBarOverlayLayout actionBarOverlayLayout = ActionBarOverlayLayout.this;
                actionBarOverlayLayout.f1930 = actionBarOverlayLayout.f1927.animate().translationY((float) (-ActionBarOverlayLayout.this.f1927.getHeight())).setListener(ActionBarOverlayLayout.this.f1931);
            }
        };
        m3162(context);
        this.f1928 = new C0411(this);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3162(Context context) {
        TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(f1926);
        boolean z = false;
        this.f1932 = obtainStyledAttributes.getDimensionPixelSize(0, 0);
        this.f1936 = obtainStyledAttributes.getDrawable(1);
        setWillNotDraw(this.f1936 == null);
        obtainStyledAttributes.recycle();
        if (context.getApplicationInfo().targetSdkVersion < 19) {
            z = true;
        }
        this.f1937 = z;
        this.f1954 = new OverScroller(context);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        m3175();
    }

    public void setActionBarVisibilityCallback(C0541 r2) {
        this.f1952 = r2;
        if (getWindowToken() != null) {
            this.f1952.m3183(this.f1933);
            int i = this.f1942;
            if (i != 0) {
                onWindowSystemUiVisibilityChanged(i);
                C0414.m2246(this);
            }
        }
    }

    public void setOverlayMode(boolean z) {
        this.f1938 = z;
        this.f1937 = z && getContext().getApplicationInfo().targetSdkVersion < 19;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3172() {
        return this.f1938;
    }

    public void setHasNonEmbeddedTabs(boolean z) {
        this.f1939 = z;
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        m3162(getContext());
        C0414.m2246(this);
    }

    public void onWindowSystemUiVisibilityChanged(int i) {
        if (Build.VERSION.SDK_INT >= 16) {
            super.onWindowSystemUiVisibilityChanged(i);
        }
        m3174();
        int i2 = this.f1942 ^ i;
        this.f1942 = i;
        boolean z = false;
        boolean z2 = (i & 4) == 0;
        if ((i & 256) != 0) {
            z = true;
        }
        C0541 r5 = this.f1952;
        if (r5 != null) {
            r5.m3184(!z);
            if (z2 || !z) {
                this.f1952.m3185();
            } else {
                this.f1952.m3186();
            }
        }
        if ((i2 & 256) != 0 && this.f1952 != null) {
            C0414.m2246(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        this.f1933 = i;
        C0541 r0 = this.f1952;
        if (r0 != null) {
            r0.m3183(i);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m3164(View view, Rect rect, boolean z, boolean z2, boolean z3, boolean z4) {
        boolean z5;
        C0542 r3 = (C0542) view.getLayoutParams();
        if (!z || r3.leftMargin == rect.left) {
            z5 = false;
        } else {
            r3.leftMargin = rect.left;
            z5 = true;
        }
        if (z2 && r3.topMargin != rect.top) {
            r3.topMargin = rect.top;
            z5 = true;
        }
        if (z4 && r3.rightMargin != rect.right) {
            r3.rightMargin = rect.right;
            z5 = true;
        }
        if (!z3 || r3.bottomMargin == rect.bottom) {
            return z5;
        }
        r3.bottomMargin = rect.bottom;
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean fitSystemWindows(Rect rect) {
        m3174();
        int r0 = C0414.m2245(this) & 256;
        boolean r02 = m3164(this.f1927, rect, true, true, false, true);
        this.f1948.set(rect);
        C0607.m3857(this, this.f1948, this.f1943);
        if (!this.f1949.equals(this.f1948)) {
            this.f1949.set(this.f1948);
            r02 = true;
        }
        if (!this.f1945.equals(this.f1943)) {
            this.f1945.set(this.f1943);
            r02 = true;
        }
        if (r02) {
            requestLayout();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public C0542 generateDefaultLayoutParams() {
        return new C0542(-1, -1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0542 generateLayoutParams(AttributeSet attributeSet) {
        return new C0542(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new C0542(layoutParams);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof C0542;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        m3174();
        measureChildWithMargins(this.f1927, i, 0, i2, 0);
        C0542 r0 = (C0542) this.f1927.getLayoutParams();
        int max = Math.max(0, this.f1927.getMeasuredWidth() + r0.leftMargin + r0.rightMargin);
        int max2 = Math.max(0, this.f1927.getMeasuredHeight() + r0.topMargin + r0.bottomMargin);
        int combineMeasuredStates = View.combineMeasuredStates(0, this.f1927.getMeasuredState());
        boolean z = (C0414.m2245(this) & 256) != 0;
        if (z) {
            i3 = this.f1932;
            if (this.f1939 && this.f1927.getTabContainer() != null) {
                i3 += this.f1932;
            }
        } else {
            i3 = this.f1927.getVisibility() != 8 ? this.f1927.getMeasuredHeight() : 0;
        }
        this.f1947.set(this.f1943);
        this.f1950.set(this.f1948);
        if (this.f1938 || z) {
            this.f1950.top += i3;
            this.f1950.bottom += 0;
        } else {
            this.f1947.top += i3;
            this.f1947.bottom += 0;
        }
        m3164(this.f1934, this.f1947, true, true, true, true);
        if (!this.f1951.equals(this.f1950)) {
            this.f1951.set(this.f1950);
            this.f1934.m3237(this.f1950);
        }
        measureChildWithMargins(this.f1934, i, 0, i2, 0);
        C0542 r2 = (C0542) this.f1934.getLayoutParams();
        int max3 = Math.max(max, this.f1934.getMeasuredWidth() + r2.leftMargin + r2.rightMargin);
        int max4 = Math.max(max2, this.f1934.getMeasuredHeight() + r2.topMargin + r2.bottomMargin);
        int combineMeasuredStates2 = View.combineMeasuredStates(combineMeasuredStates, this.f1934.getMeasuredState());
        setMeasuredDimension(View.resolveSizeAndState(Math.max(max3 + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth()), i, combineMeasuredStates2), View.resolveSizeAndState(Math.max(max4 + getPaddingTop() + getPaddingBottom(), getSuggestedMinimumHeight()), i2, combineMeasuredStates2 << 16));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int childCount = getChildCount();
        int paddingLeft = getPaddingLeft();
        getPaddingRight();
        int paddingTop = getPaddingTop();
        getPaddingBottom();
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            if (childAt.getVisibility() != 8) {
                C0542 r0 = (C0542) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                int i6 = r0.leftMargin + paddingLeft;
                int i7 = r0.topMargin + paddingTop;
                childAt.layout(i6, i7, measuredWidth + i6, measuredHeight + i7);
            }
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.f1936 != null && !this.f1937) {
            int bottom = this.f1927.getVisibility() == 0 ? (int) (((float) this.f1927.getBottom()) + this.f1927.getTranslationY() + 0.5f) : 0;
            this.f1936.setBounds(0, bottom, getWidth(), this.f1936.getIntrinsicHeight() + bottom);
            this.f1936.draw(canvas);
        }
    }

    public boolean onStartNestedScroll(View view, View view2, int i) {
        if ((i & 2) == 0 || this.f1927.getVisibility() != 0) {
            return false;
        }
        return this.f1940;
    }

    public void onNestedScrollAccepted(View view, View view2, int i) {
        this.f1928.m2213(view, view2, i);
        this.f1941 = getActionBarHideOffset();
        m3175();
        C0541 r2 = this.f1952;
        if (r2 != null) {
            r2.m3187();
        }
    }

    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        this.f1941 += i2;
        setActionBarHideOffset(this.f1941);
    }

    public void onStopNestedScroll(View view) {
        if (this.f1940 && !this.f1929) {
            if (this.f1941 <= this.f1927.getHeight()) {
                m3165();
            } else {
                m3166();
            }
        }
        C0541 r2 = this.f1952;
        if (r2 != null) {
            r2.m3188();
        }
    }

    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        if (!this.f1940 || !z) {
            return false;
        }
        if (m3163(f, f2)) {
            m3168();
        } else {
            m3167();
        }
        this.f1929 = true;
        return true;
    }

    public int getNestedScrollAxes() {
        return this.f1928.m2210();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3174() {
        if (this.f1934 == null) {
            this.f1934 = (ContentFrameLayout) findViewById(C0727.C0733.action_bar_activity_content);
            this.f1927 = (ActionBarContainer) findViewById(C0727.C0733.action_bar_container);
            this.f1935 = m3161(findViewById(C0727.C0733.action_bar));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private C0631 m3161(View view) {
        if (view instanceof C0631) {
            return (C0631) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        throw new IllegalStateException("Can't make a decor toolbar out of " + view.getClass().getSimpleName());
    }

    public void setHideOnContentScrollEnabled(boolean z) {
        if (z != this.f1940) {
            this.f1940 = z;
            if (!z) {
                m3175();
                setActionBarHideOffset(0);
            }
        }
    }

    public int getActionBarHideOffset() {
        ActionBarContainer actionBarContainer = this.f1927;
        if (actionBarContainer != null) {
            return -((int) actionBarContainer.getTranslationY());
        }
        return 0;
    }

    public void setActionBarHideOffset(int i) {
        m3175();
        this.f1927.setTranslationY((float) (-Math.max(0, Math.min(i, this.f1927.getHeight()))));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m3175() {
        removeCallbacks(this.f1944);
        removeCallbacks(this.f1946);
        ViewPropertyAnimator viewPropertyAnimator = this.f1930;
        if (viewPropertyAnimator != null) {
            viewPropertyAnimator.cancel();
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private void m3165() {
        m3175();
        postDelayed(this.f1944, 600);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m3166() {
        m3175();
        postDelayed(this.f1946, 600);
    }

    /* renamed from: י  reason: contains not printable characters */
    private void m3167() {
        m3175();
        this.f1944.run();
    }

    /* renamed from: ـ  reason: contains not printable characters */
    private void m3168() {
        m3175();
        this.f1946.run();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m3163(float f, float f2) {
        this.f1954.fling(0, 0, 0, (int) f2, 0, 0, Integer.MIN_VALUE, Integer.MAX_VALUE);
        return this.f1954.getFinalY() > this.f1927.getHeight();
    }

    public void setWindowCallback(Window.Callback callback) {
        m3174();
        this.f1935.m3996(callback);
    }

    public void setWindowTitle(CharSequence charSequence) {
        m3174();
        this.f1935.m3997(charSequence);
    }

    public CharSequence getTitle() {
        m3174();
        return this.f1935.m4007();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3170(int i) {
        m3174();
        if (i == 2) {
            this.f1935.m4009();
        } else if (i == 5) {
            this.f1935.m4010();
        } else if (i == 109) {
            setOverlayMode(true);
        }
    }

    public void setIcon(int i) {
        m3174();
        this.f1935.m3991(i);
    }

    public void setIcon(Drawable drawable) {
        m3174();
        this.f1935.m3992(drawable);
    }

    public void setLogo(int i) {
        m3174();
        this.f1935.m4000(i);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m3176() {
        m3174();
        return this.f1935.m4011();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m3177() {
        m3174();
        return this.f1935.m4012();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m3178() {
        m3174();
        return this.f1935.m4013();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m3179() {
        m3174();
        return this.f1935.m4014();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean m3180() {
        m3174();
        return this.f1935.m4015();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public void m3181() {
        m3174();
        this.f1935.m4016();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3171(Menu menu, C0518.C0519 r3) {
        m3174();
        this.f1935.m3995(menu, r3);
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public void m3182() {
        m3174();
        this.f1935.m4017();
    }

    /* renamed from: android.support.v7.widget.ActionBarOverlayLayout$ʼ  reason: contains not printable characters */
    public static class C0542 extends ViewGroup.MarginLayoutParams {
        public C0542(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public C0542(int i, int i2) {
            super(i, i2);
        }

        public C0542(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }
}
