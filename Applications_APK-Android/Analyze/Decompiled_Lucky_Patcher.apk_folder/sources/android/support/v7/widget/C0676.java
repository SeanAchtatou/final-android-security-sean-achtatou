package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.ˉ.C0413;
import android.support.v7.ʻ.C0727;
import android.support.v7.ʼ.ʻ.C0739;
import android.util.AttributeSet;
import android.widget.MultiAutoCompleteTextView;

/* renamed from: android.support.v7.widget.ᴵ  reason: contains not printable characters */
/* compiled from: AppCompatMultiAutoCompleteTextView */
public class C0676 extends MultiAutoCompleteTextView implements C0413 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final int[] f2695 = {16843126};

    /* renamed from: ʼ  reason: contains not printable characters */
    private final C0639 f2696;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final C0726 f2697;

    public C0676(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0727.C0728.autoCompleteTextViewStyle);
    }

    public C0676(Context context, AttributeSet attributeSet, int i) {
        super(C0589.m3735(context), attributeSet, i);
        C0592 r3 = C0592.m3740(getContext(), attributeSet, f2695, i, 0);
        if (r3.m3758(0)) {
            setDropDownBackgroundDrawable(r3.m3744(0));
        }
        r3.m3745();
        this.f2696 = new C0639(this);
        this.f2696.m4065(attributeSet, i);
        this.f2697 = C0726.m4855(this);
        this.f2697.m4864(attributeSet, i);
        this.f2697.m4858();
    }

    public void setDropDownBackgroundResource(int i) {
        setDropDownBackgroundDrawable(C0739.m4883(getContext(), i));
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        C0639 r0 = this.f2696;
        if (r0 != null) {
            r0.m4061(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        C0639 r0 = this.f2696;
        if (r0 != null) {
            r0.m4064(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        C0639 r0 = this.f2696;
        if (r0 != null) {
            r0.m4062(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        C0639 r0 = this.f2696;
        if (r0 != null) {
            return r0.m4060();
        }
        return null;
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        C0639 r0 = this.f2696;
        if (r0 != null) {
            r0.m4063(mode);
        }
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        C0639 r0 = this.f2696;
        if (r0 != null) {
            return r0.m4066();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        C0639 r0 = this.f2696;
        if (r0 != null) {
            r0.m4068();
        }
        C0726 r02 = this.f2697;
        if (r02 != null) {
            r02.m4858();
        }
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        C0726 r0 = this.f2697;
        if (r0 != null) {
            r0.m4862(context, i);
        }
    }
}
