package android.support.v7.widget;

import android.support.v4.ʿ.C0324;
import android.support.v7.widget.C0690;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;
import org.tukaani.xz.common.Util;

/* renamed from: android.support.v7.widget.ˊˊ  reason: contains not printable characters */
/* compiled from: GapWorker */
final class C0644 implements Runnable {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final ThreadLocal<C0644> f2558 = new ThreadLocal<>();

    /* renamed from: ʿ  reason: contains not printable characters */
    static Comparator<C0646> f2559 = new Comparator<C0646>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public int compare(C0646 r6, C0646 r7) {
            if ((r6.f2571 == null) != (r7.f2571 == null)) {
                if (r6.f2571 == null) {
                    return 1;
                }
                return -1;
            } else if (r6.f2568 == r7.f2568) {
                int i = r7.f2569 - r6.f2569;
                if (i != 0) {
                    return i;
                }
                int i2 = r6.f2570 - r7.f2570;
                if (i2 != 0) {
                    return i2;
                }
                return 0;
            } else if (r6.f2568) {
                return -1;
            } else {
                return 1;
            }
        }
    };

    /* renamed from: ʼ  reason: contains not printable characters */
    ArrayList<C0690> f2560 = new ArrayList<>();

    /* renamed from: ʽ  reason: contains not printable characters */
    long f2561;

    /* renamed from: ʾ  reason: contains not printable characters */
    long f2562;

    /* renamed from: ˆ  reason: contains not printable characters */
    private ArrayList<C0646> f2563 = new ArrayList<>();

    C0644() {
    }

    /* renamed from: android.support.v7.widget.ˊˊ$ʼ  reason: contains not printable characters */
    /* compiled from: GapWorker */
    static class C0646 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean f2568;

        /* renamed from: ʼ  reason: contains not printable characters */
        public int f2569;

        /* renamed from: ʽ  reason: contains not printable characters */
        public int f2570;

        /* renamed from: ʾ  reason: contains not printable characters */
        public C0690 f2571;

        /* renamed from: ʿ  reason: contains not printable characters */
        public int f2572;

        C0646() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4116() {
            this.f2568 = false;
            this.f2569 = 0;
            this.f2570 = 0;
            this.f2571 = null;
            this.f2572 = 0;
        }
    }

    /* renamed from: android.support.v7.widget.ˊˊ$ʻ  reason: contains not printable characters */
    /* compiled from: GapWorker */
    static class C0645 implements C0690.C0701.C0702 {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f2564;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f2565;

        /* renamed from: ʽ  reason: contains not printable characters */
        int[] f2566;

        /* renamed from: ʾ  reason: contains not printable characters */
        int f2567;

        C0645() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4112(int i, int i2) {
            this.f2564 = i;
            this.f2565 = i2;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4113(C0690 r5, boolean z) {
            this.f2567 = 0;
            int[] iArr = this.f2566;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            C0690.C0701 r0 = r5.f2787;
            if (r5.f2786 != null && r0 != null && r0.m4658()) {
                if (z) {
                    if (!r5.f2777.m4040()) {
                        r0.m4552(r5.f2786.m4480(), this);
                    }
                } else if (!r5.m4453()) {
                    r0.m4551(this.f2564, this.f2565, r5.f2797, this);
                }
                if (this.f2567 > r0.f2846) {
                    r0.f2846 = this.f2567;
                    r0.f2847 = z;
                    r5.f2775.m4727();
                }
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4115(int i, int i2) {
            if (i < 0) {
                throw new IllegalArgumentException("Layout positions must be non-negative");
            } else if (i2 >= 0) {
                int i3 = this.f2567 * 2;
                int[] iArr = this.f2566;
                if (iArr == null) {
                    this.f2566 = new int[4];
                    Arrays.fill(this.f2566, -1);
                } else if (i3 >= iArr.length) {
                    this.f2566 = new int[(i3 * 2)];
                    System.arraycopy(iArr, 0, this.f2566, 0, iArr.length);
                }
                int[] iArr2 = this.f2566;
                iArr2[i3] = i;
                iArr2[i3 + 1] = i2;
                this.f2567++;
            } else {
                throw new IllegalArgumentException("Pixel distance must be non-negative");
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4114(int i) {
            if (this.f2566 != null) {
                int i2 = this.f2567 * 2;
                for (int i3 = 0; i3 < i2; i3 += 2) {
                    if (this.f2566[i3] == i) {
                        return true;
                    }
                }
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4111() {
            int[] iArr = this.f2566;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            this.f2567 = 0;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4107(C0690 r2) {
        this.f2560.add(r2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4109(C0690 r2) {
        this.f2560.remove(r2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4108(C0690 r6, int i, int i2) {
        if (r6.isAttachedToWindow() && this.f2561 == 0) {
            this.f2561 = r6.getNanoTime();
            r6.post(this);
        }
        r6.f2795.m4112(i, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ˊˊ.ʻ.ʻ(android.support.v7.widget.ﹳﹳ, boolean):void
     arg types: [android.support.v7.widget.ﹳﹳ, int]
     candidates:
      android.support.v7.widget.ˊˊ.ʻ.ʻ(int, int):void
      android.support.v7.widget.ˊˊ.ʻ.ʻ(android.support.v7.widget.ﹳﹳ, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4101() {
        C0646 r8;
        int size = this.f2560.size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            C0690 r4 = this.f2560.get(i2);
            if (r4.getWindowVisibility() == 0) {
                r4.f2795.m4113(r4, false);
                i += r4.f2795.f2567;
            }
        }
        this.f2563.ensureCapacity(i);
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            C0690 r42 = this.f2560.get(i4);
            if (r42.getWindowVisibility() == 0) {
                C0645 r5 = r42.f2795;
                int abs = Math.abs(r5.f2564) + Math.abs(r5.f2565);
                int i5 = i3;
                for (int i6 = 0; i6 < r5.f2567 * 2; i6 += 2) {
                    if (i5 >= this.f2563.size()) {
                        r8 = new C0646();
                        this.f2563.add(r8);
                    } else {
                        r8 = this.f2563.get(i5);
                    }
                    int i7 = r5.f2566[i6 + 1];
                    r8.f2568 = i7 <= abs;
                    r8.f2569 = abs;
                    r8.f2570 = i7;
                    r8.f2571 = r42;
                    r8.f2572 = r5.f2566[i6];
                    i5++;
                }
                i3 = i5;
            }
        }
        Collections.sort(this.f2563, f2559);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static boolean m4104(C0690 r5, int i) {
        int r0 = r5.f2779.m3881();
        for (int i2 = 0; i2 < r0; i2++) {
            C0690.C0720 r3 = C0690.m4357(r5.f2779.m3884(i2));
            if (r3.f2916 == i && !r3.m4824()) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ﹳﹳ.ـ.ʻ(int, boolean, long):android.support.v7.widget.ﹳﹳ$ﹳ
     arg types: [int, int, long]
     candidates:
      android.support.v7.widget.ﹳﹳ.ـ.ʻ(long, int, boolean):android.support.v7.widget.ﹳﹳ$ﹳ
      android.support.v7.widget.ﹳﹳ.ـ.ʻ(int, int, boolean):void
      android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.support.v7.widget.ﹳﹳ$ʻ, android.support.v7.widget.ﹳﹳ$ʻ, boolean):void
      android.support.v7.widget.ﹳﹳ.ـ.ʻ(int, boolean, long):android.support.v7.widget.ﹳﹳ$ﹳ */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):void
     arg types: [android.support.v7.widget.ﹳﹳ$ﹳ, int]
     candidates:
      android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.view.ViewGroup, boolean):void
      android.support.v7.widget.ﹳﹳ.ـ.ʻ(int, boolean):android.view.View
      android.support.v7.widget.ﹳﹳ.ـ.ʻ(int, int):void
      android.support.v7.widget.ﹳﹳ.ـ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    private C0690.C0720 m4100(C0690 r3, int i, long j) {
        if (m4104(r3, i)) {
            return null;
        }
        C0690.C0711 r0 = r3.f2775;
        try {
            r3.m4441();
            C0690.C0720 r4 = r0.m4712(i, false, j);
            if (r4 != null) {
                if (!r4.m4826() || r4.m4824()) {
                    r0.m4722(r4, false);
                } else {
                    r0.m4723(r4.f2914);
                }
            }
            return r4;
        } finally {
            r3.m4407(false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ˊˊ.ʻ.ʻ(android.support.v7.widget.ﹳﹳ, boolean):void
     arg types: [android.support.v7.widget.ﹳﹳ, int]
     candidates:
      android.support.v7.widget.ˊˊ.ʻ.ʻ(int, int):void
      android.support.v7.widget.ˊˊ.ʻ.ʻ(android.support.v7.widget.ﹳﹳ, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4103(C0690 r4, long j) {
        if (r4 != null) {
            if (r4.f2806 && r4.f2779.m3881() != 0) {
                r4.m4411();
            }
            C0645 r0 = r4.f2795;
            r0.m4113(r4, true);
            if (r0.f2567 != 0) {
                try {
                    C0324.m1842("RV Nested Prefetch");
                    r4.f2797.m4770(r4.f2786);
                    for (int i = 0; i < r0.f2567 * 2; i += 2) {
                        m4100(r4, r0.f2566[i], j);
                    }
                } finally {
                    C0324.m1841();
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4102(C0646 r4, long j) {
        C0690.C0720 r42 = m4100(r4.f2571, r4.f2572, r4.f2568 ? Util.VLI_MAX : j);
        if (r42 != null && r42.f2915 != null && r42.m4826() && !r42.m4824()) {
            m4103(r42.f2915.get(), j);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m4105(long j) {
        int i = 0;
        while (i < this.f2563.size()) {
            C0646 r1 = this.f2563.get(i);
            if (r1.f2571 != null) {
                m4102(r1, j);
                r1.m4116();
                i++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4106(long j) {
        m4101();
        m4105(j);
    }

    public void run() {
        try {
            C0324.m1842("RV Prefetch");
            if (!this.f2560.isEmpty()) {
                int size = this.f2560.size();
                long j = 0;
                for (int i = 0; i < size; i++) {
                    C0690 r6 = this.f2560.get(i);
                    if (r6.getWindowVisibility() == 0) {
                        j = Math.max(r6.getDrawingTime(), j);
                    }
                }
                if (j != 0) {
                    m4106(TimeUnit.MILLISECONDS.toNanos(j) + this.f2562);
                    this.f2561 = 0;
                    C0324.m1841();
                }
            }
        } finally {
            this.f2561 = 0;
            C0324.m1841();
        }
    }
}
