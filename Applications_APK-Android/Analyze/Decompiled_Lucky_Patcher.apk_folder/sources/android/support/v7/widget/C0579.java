package android.support.v7.widget;

import android.support.v7.widget.C0690;
import android.view.View;

/* renamed from: android.support.v7.widget.ʻʽ  reason: contains not printable characters */
/* compiled from: ScrollbarHelper */
class C0579 {
    /* renamed from: ʻ  reason: contains not printable characters */
    static int m3669(C0690.C0717 r4, C0688 r5, View view, View view2, C0690.C0701 r8, boolean z, boolean z2) {
        int i;
        if (r8.m4667() == 0 || r4.m4775() == 0 || view == null || view2 == null) {
            return 0;
        }
        int min = Math.min(r8.m4620(view), r8.m4620(view2));
        int max = Math.max(r8.m4620(view), r8.m4620(view2));
        if (z2) {
            i = Math.max(0, (r4.m4775() - max) - 1);
        } else {
            i = Math.max(0, min);
        }
        if (!z) {
            return i;
        }
        return Math.round((((float) i) * (((float) Math.abs(r5.m4293(view2) - r5.m4289(view))) / ((float) (Math.abs(r8.m4620(view) - r8.m4620(view2)) + 1)))) + ((float) (r5.m4294() - r5.m4289(view))));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static int m3668(C0690.C0717 r1, C0688 r2, View view, View view2, C0690.C0701 r5, boolean z) {
        if (r5.m4667() == 0 || r1.m4775() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return Math.abs(r5.m4620(view) - r5.m4620(view2)) + 1;
        }
        return Math.min(r2.m4300(), r2.m4293(view2) - r2.m4289(view));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static int m3670(C0690.C0717 r1, C0688 r2, View view, View view2, C0690.C0701 r5, boolean z) {
        if (r5.m4667() == 0 || r1.m4775() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return r1.m4775();
        }
        return (int) ((((float) (r2.m4293(view2) - r2.m4289(view))) / ((float) (Math.abs(r5.m4620(view) - r5.m4620(view2)) + 1))) * ((float) r1.m4775()));
    }
}
