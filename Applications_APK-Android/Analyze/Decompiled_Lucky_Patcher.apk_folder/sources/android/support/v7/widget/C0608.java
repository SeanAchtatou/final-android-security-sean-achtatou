package android.support.v7.widget;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

/* renamed from: android.support.v7.widget.ʼ  reason: contains not printable characters */
/* compiled from: ActionBarBackgroundDrawable */
class C0608 extends Drawable {

    /* renamed from: ʻ  reason: contains not printable characters */
    final ActionBarContainer f2383;

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public C0608(ActionBarContainer actionBarContainer) {
        this.f2383 = actionBarContainer;
    }

    public void draw(Canvas canvas) {
        if (!this.f2383.f1906) {
            if (this.f2383.f1903 != null) {
                this.f2383.f1903.draw(canvas);
            }
            if (this.f2383.f1904 != null && this.f2383.f1907) {
                this.f2383.f1904.draw(canvas);
            }
        } else if (this.f2383.f1905 != null) {
            this.f2383.f1905.draw(canvas);
        }
    }
}
