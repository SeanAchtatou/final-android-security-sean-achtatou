package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.ˉ.C0414;
import android.support.v4.ˉ.C0434;
import android.support.v7.view.C0529;
import android.support.v7.view.menu.C0504;
import android.support.v7.ʻ.C0727;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ActionBarContextView extends C0575 {

    /* renamed from: ˈ  reason: contains not printable characters */
    private CharSequence f1913;

    /* renamed from: ˉ  reason: contains not printable characters */
    private CharSequence f1914;

    /* renamed from: ˊ  reason: contains not printable characters */
    private View f1915;

    /* renamed from: ˋ  reason: contains not printable characters */
    private View f1916;

    /* renamed from: ˎ  reason: contains not printable characters */
    private LinearLayout f1917;

    /* renamed from: ˏ  reason: contains not printable characters */
    private TextView f1918;

    /* renamed from: ˑ  reason: contains not printable characters */
    private TextView f1919;

    /* renamed from: י  reason: contains not printable characters */
    private int f1920;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f1921;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f1922;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f1923;

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public /* bridge */ /* synthetic */ int getAnimatedVisibility() {
        return super.getAnimatedVisibility();
    }

    public /* bridge */ /* synthetic */ int getContentHeight() {
        return super.getContentHeight();
    }

    public /* bridge */ /* synthetic */ boolean onHoverEvent(MotionEvent motionEvent) {
        return super.onHoverEvent(motionEvent);
    }

    public /* bridge */ /* synthetic */ boolean onTouchEvent(MotionEvent motionEvent) {
        return super.onTouchEvent(motionEvent);
    }

    public /* bridge */ /* synthetic */ void setVisibility(int i) {
        super.setVisibility(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ C0434 m3155(int i, long j) {
        return super.m3629(i, j);
    }

    public ActionBarContextView(Context context) {
        this(context, null);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0727.C0728.actionModeStyle);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        C0592 r3 = C0592.m3740(context, attributeSet, C0727.C0737.ActionMode, i, 0);
        C0414.m2223(this, r3.m3744(C0727.C0737.ActionMode_background));
        this.f1920 = r3.m3757(C0727.C0737.ActionMode_titleTextStyle, 0);
        this.f1921 = r3.m3757(C0727.C0737.ActionMode_subtitleTextStyle, 0);
        this.f2237 = r3.m3755(C0727.C0737.ActionMode_height, 0);
        this.f1923 = r3.m3757(C0727.C0737.ActionMode_closeItemLayout, C0727.C0734.abc_action_mode_close_item_material);
        r3.m3745();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f2236 != null) {
            this.f2236.m3935();
            this.f2236.m3937();
        }
    }

    public void setContentHeight(int i) {
        this.f2237 = i;
    }

    public void setCustomView(View view) {
        LinearLayout linearLayout;
        View view2 = this.f1916;
        if (view2 != null) {
            removeView(view2);
        }
        this.f1916 = view;
        if (!(view == null || (linearLayout = this.f1917) == null)) {
            removeView(linearLayout);
            this.f1917 = null;
        }
        if (view != null) {
            addView(view);
        }
        requestLayout();
    }

    public void setTitle(CharSequence charSequence) {
        this.f1913 = charSequence;
        m3154();
    }

    public void setSubtitle(CharSequence charSequence) {
        this.f1914 = charSequence;
        m3154();
    }

    public CharSequence getTitle() {
        return this.f1913;
    }

    public CharSequence getSubtitle() {
        return this.f1914;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m3154() {
        if (this.f1917 == null) {
            LayoutInflater.from(getContext()).inflate(C0727.C0734.abc_action_bar_title_item, this);
            this.f1917 = (LinearLayout) getChildAt(getChildCount() - 1);
            this.f1918 = (TextView) this.f1917.findViewById(C0727.C0733.action_bar_title);
            this.f1919 = (TextView) this.f1917.findViewById(C0727.C0733.action_bar_subtitle);
            if (this.f1920 != 0) {
                this.f1918.setTextAppearance(getContext(), this.f1920);
            }
            if (this.f1921 != 0) {
                this.f1919.setTextAppearance(getContext(), this.f1921);
            }
        }
        this.f1918.setText(this.f1913);
        this.f1919.setText(this.f1914);
        boolean z = !TextUtils.isEmpty(this.f1913);
        boolean z2 = !TextUtils.isEmpty(this.f1914);
        int i = 0;
        this.f1919.setVisibility(z2 ? 0 : 8);
        LinearLayout linearLayout = this.f1917;
        if (!z && !z2) {
            i = 8;
        }
        linearLayout.setVisibility(i);
        if (this.f1917.getParent() == null) {
            addView(this.f1917);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.widget.ActionBarContextView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3156(final C0529 r4) {
        View view = this.f1915;
        if (view == null) {
            this.f1915 = LayoutInflater.from(getContext()).inflate(this.f1923, (ViewGroup) this, false);
            addView(this.f1915);
        } else if (view.getParent() == null) {
            addView(this.f1915);
        }
        this.f1915.findViewById(C0727.C0733.action_mode_close_button).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                r4.m3089();
            }
        });
        C0504 r42 = (C0504) r4.m3086();
        if (this.f2236 != null) {
            this.f2236.m3936();
        }
        this.f2236 = new C0614(getContext());
        this.f2236.m3931(true);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -1);
        r42.m2896(this.f2236, this.f2234);
        this.f2235 = (ActionMenuView) this.f2236.m3915(this);
        C0414.m2223(this.f2235, (Drawable) null);
        addView(this.f2235, layoutParams);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3158() {
        if (this.f1915 == null) {
            m3159();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3159() {
        removeAllViews();
        this.f1916 = null;
        this.f2235 = null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3157() {
        if (this.f2236 != null) {
            return this.f2236.m3934();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.MarginLayoutParams(-1, -2);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4 = 1073741824;
        if (View.MeasureSpec.getMode(i) != 1073741824) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with android:layout_width=\"match_parent\" (or fill_parent)");
        } else if (View.MeasureSpec.getMode(i2) != 0) {
            int size = View.MeasureSpec.getSize(i);
            if (this.f2237 > 0) {
                i3 = this.f2237;
            } else {
                i3 = View.MeasureSpec.getSize(i2);
            }
            int paddingTop = getPaddingTop() + getPaddingBottom();
            int paddingLeft = (size - getPaddingLeft()) - getPaddingRight();
            int i5 = i3 - paddingTop;
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i5, Integer.MIN_VALUE);
            View view = this.f1915;
            if (view != null) {
                int r1 = m3627(view, paddingLeft, makeMeasureSpec, 0);
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.f1915.getLayoutParams();
                paddingLeft = r1 - (marginLayoutParams.leftMargin + marginLayoutParams.rightMargin);
            }
            if (this.f2235 != null && this.f2235.getParent() == this) {
                paddingLeft = m3627(this.f2235, paddingLeft, makeMeasureSpec, 0);
            }
            LinearLayout linearLayout = this.f1917;
            if (linearLayout != null && this.f1916 == null) {
                if (this.f1922) {
                    this.f1917.measure(View.MeasureSpec.makeMeasureSpec(0, 0), makeMeasureSpec);
                    int measuredWidth = this.f1917.getMeasuredWidth();
                    boolean z = measuredWidth <= paddingLeft;
                    if (z) {
                        paddingLeft -= measuredWidth;
                    }
                    this.f1917.setVisibility(z ? 0 : 8);
                } else {
                    paddingLeft = m3627(linearLayout, paddingLeft, makeMeasureSpec, 0);
                }
            }
            View view2 = this.f1916;
            if (view2 != null) {
                ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
                int i6 = layoutParams.width != -2 ? 1073741824 : Integer.MIN_VALUE;
                if (layoutParams.width >= 0) {
                    paddingLeft = Math.min(layoutParams.width, paddingLeft);
                }
                if (layoutParams.height == -2) {
                    i4 = Integer.MIN_VALUE;
                }
                if (layoutParams.height >= 0) {
                    i5 = Math.min(layoutParams.height, i5);
                }
                this.f1916.measure(View.MeasureSpec.makeMeasureSpec(paddingLeft, i6), View.MeasureSpec.makeMeasureSpec(i5, i4));
            }
            if (this.f2237 <= 0) {
                int childCount = getChildCount();
                int i7 = 0;
                for (int i8 = 0; i8 < childCount; i8++) {
                    int measuredHeight = getChildAt(i8).getMeasuredHeight() + paddingTop;
                    if (measuredHeight > i7) {
                        i7 = measuredHeight;
                    }
                }
                setMeasuredDimension(size, i7);
                return;
            }
            setMeasuredDimension(size, i3);
        } else {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with android:layout_height=\"wrap_content\"");
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        boolean r10 = C0607.m3858(this);
        int paddingRight = r10 ? (i3 - i) - getPaddingRight() : getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingTop2 = ((i4 - i2) - getPaddingTop()) - getPaddingBottom();
        View view = this.f1915;
        if (view == null || view.getVisibility() == 8) {
            i5 = paddingRight;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.f1915.getLayoutParams();
            int i6 = r10 ? marginLayoutParams.rightMargin : marginLayoutParams.leftMargin;
            int i7 = r10 ? marginLayoutParams.leftMargin : marginLayoutParams.rightMargin;
            int r8 = m3624(paddingRight, i6, r10);
            i5 = m3624(r8 + m3628(this.f1915, r8, paddingTop, paddingTop2, r10), i7, r10);
        }
        LinearLayout linearLayout = this.f1917;
        if (!(linearLayout == null || this.f1916 != null || linearLayout.getVisibility() == 8)) {
            i5 += m3628(this.f1917, i5, paddingTop, paddingTop2, r10);
        }
        int i8 = i5;
        View view2 = this.f1916;
        if (view2 != null) {
            m3628(view2, i8, paddingTop, paddingTop2, r10);
        }
        int paddingLeft = r10 ? getPaddingLeft() : (i3 - i) - getPaddingRight();
        if (this.f2235 != null) {
            m3628(this.f2235, paddingLeft, paddingTop, paddingTop2, !r10);
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        if (accessibilityEvent.getEventType() == 32) {
            accessibilityEvent.setSource(this);
            accessibilityEvent.setClassName(getClass().getName());
            accessibilityEvent.setPackageName(getContext().getPackageName());
            accessibilityEvent.setContentDescription(this.f1913);
            return;
        }
        super.onInitializeAccessibilityEvent(accessibilityEvent);
    }

    public void setTitleOptional(boolean z) {
        if (z != this.f1922) {
            requestLayout();
        }
        this.f1922 = z;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m3160() {
        return this.f1922;
    }
}
