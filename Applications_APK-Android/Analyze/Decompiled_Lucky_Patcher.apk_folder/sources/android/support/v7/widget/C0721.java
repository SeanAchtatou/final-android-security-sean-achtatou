package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.ʼ.ʻ.C0288;
import android.support.v4.ˉ.C0414;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.widget.SeekBar;

/* renamed from: android.support.v7.widget.ﹶ  reason: contains not printable characters */
/* compiled from: AppCompatSeekBarHelper */
class C0721 extends C0681 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final SeekBar f2932;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Drawable f2933;

    /* renamed from: ʽ  reason: contains not printable characters */
    private ColorStateList f2934 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    private PorterDuff.Mode f2935 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f2936 = false;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f2937 = false;

    C0721(SeekBar seekBar) {
        super(seekBar);
        this.f2932 = seekBar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4838(AttributeSet attributeSet, int i) {
        super.m4268(attributeSet, i);
        C0592 r4 = C0592.m3740(this.f2932.getContext(), attributeSet, C0727.C0737.AppCompatSeekBar, i, 0);
        Drawable r5 = r4.m3748(C0727.C0737.AppCompatSeekBar_android_thumb);
        if (r5 != null) {
            this.f2932.setThumb(r5);
        }
        m4837(r4.m3744(C0727.C0737.AppCompatSeekBar_tickMark));
        if (r4.m3758(C0727.C0737.AppCompatSeekBar_tickMarkTintMode)) {
            this.f2935 = C0670.m4230(r4.m3742(C0727.C0737.AppCompatSeekBar_tickMarkTintMode, -1), this.f2935);
            this.f2937 = true;
        }
        if (r4.m3758(C0727.C0737.AppCompatSeekBar_tickMarkTint)) {
            this.f2934 = r4.m3754(C0727.C0737.AppCompatSeekBar_tickMarkTint);
            this.f2936 = true;
        }
        r4.m3745();
        m4835();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4837(Drawable drawable) {
        Drawable drawable2 = this.f2933;
        if (drawable2 != null) {
            drawable2.setCallback(null);
        }
        this.f2933 = drawable;
        if (drawable != null) {
            drawable.setCallback(this.f2932);
            C0288.m1709(drawable, C0414.m2236(this.f2932));
            if (drawable.isStateful()) {
                drawable.setState(this.f2932.getDrawableState());
            }
            m4835();
        }
        this.f2932.invalidate();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m4835() {
        if (this.f2933 == null) {
            return;
        }
        if (this.f2936 || this.f2937) {
            this.f2933 = C0288.m1713(this.f2933.mutate());
            if (this.f2936) {
                C0288.m1703(this.f2933, this.f2934);
            }
            if (this.f2937) {
                C0288.m1706(this.f2933, this.f2935);
            }
            if (this.f2933.isStateful()) {
                this.f2933.setState(this.f2932.getDrawableState());
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4839() {
        Drawable drawable = this.f2933;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m4840() {
        Drawable drawable = this.f2933;
        if (drawable != null && drawable.isStateful() && drawable.setState(this.f2932.getDrawableState())) {
            this.f2932.invalidateDrawable(drawable);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4836(Canvas canvas) {
        if (this.f2933 != null) {
            int max = this.f2932.getMax();
            int i = 1;
            if (max > 1) {
                int intrinsicWidth = this.f2933.getIntrinsicWidth();
                int intrinsicHeight = this.f2933.getIntrinsicHeight();
                int i2 = intrinsicWidth >= 0 ? intrinsicWidth / 2 : 1;
                if (intrinsicHeight >= 0) {
                    i = intrinsicHeight / 2;
                }
                this.f2933.setBounds(-i2, -i, i2, i);
                float width = ((float) ((this.f2932.getWidth() - this.f2932.getPaddingLeft()) - this.f2932.getPaddingRight())) / ((float) max);
                int save = canvas.save();
                canvas.translate((float) this.f2932.getPaddingLeft(), (float) (this.f2932.getHeight() / 2));
                for (int i3 = 0; i3 <= max; i3++) {
                    this.f2933.draw(canvas);
                    canvas.translate(width, 0.0f);
                }
                canvas.restoreToCount(save);
            }
        }
    }
}
