package android.support.v7.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.support.v4.ˉ.C0414;
import android.support.v7.widget.C0690;
import android.view.MotionEvent;

/* renamed from: android.support.v7.widget.ˉˉ  reason: contains not printable characters */
/* compiled from: FastScroller */
class C0640 extends C0690.C0700 implements C0690.C0707 {

    /* renamed from: ˈ  reason: contains not printable characters */
    private static final int[] f2520 = {16842919};

    /* renamed from: ˉ  reason: contains not printable characters */
    private static final int[] f2521 = new int[0];

    /* renamed from: ʻ  reason: contains not printable characters */
    int f2522;
    /* access modifiers changed from: private */

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public int f2523 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    int f2524;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private final C0690.C0708 f2525 = new C0690.C0708() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4099(C0690 r1, int i, int i2) {
            C0640.this.m4091(r1.computeHorizontalScrollOffset(), r1.computeVerticalScrollOffset());
        }
    };

    /* renamed from: ʽ  reason: contains not printable characters */
    float f2526;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private final Runnable f2527 = new Runnable() {
        public void run() {
            C0640.this.m4090(500);
        }
    };

    /* renamed from: ʾ  reason: contains not printable characters */
    int f2528;

    /* renamed from: ʿ  reason: contains not printable characters */
    int f2529;

    /* renamed from: ˆ  reason: contains not printable characters */
    float f2530;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final int f2531;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final int f2532;
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public final StateListDrawable f2533;
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public final Drawable f2534;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final int f2535;

    /* renamed from: י  reason: contains not printable characters */
    private final int f2536;

    /* renamed from: ـ  reason: contains not printable characters */
    private final StateListDrawable f2537;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final Drawable f2538;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final int f2539;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private final int[] f2540 = new int[2];

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final int f2541;
    /* access modifiers changed from: private */

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public final ValueAnimator f2542 = ValueAnimator.ofFloat(0.0f, 1.0f);

    /* renamed from: ᵎ  reason: contains not printable characters */
    private int f2543 = 0;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private int f2544 = 0;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private C0690 f2545;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private boolean f2546 = false;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private boolean f2547 = false;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int f2548 = 0;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f2549 = 0;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private final int[] f2550 = new int[2];

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4094(boolean z) {
    }

    C0640(C0690 r4, StateListDrawable stateListDrawable, Drawable drawable, StateListDrawable stateListDrawable2, Drawable drawable2, int i, int i2, int i3) {
        this.f2533 = stateListDrawable;
        this.f2534 = drawable;
        this.f2537 = stateListDrawable2;
        this.f2538 = drawable2;
        this.f2535 = Math.max(i, stateListDrawable.getIntrinsicWidth());
        this.f2536 = Math.max(i, drawable.getIntrinsicWidth());
        this.f2539 = Math.max(i, stateListDrawable2.getIntrinsicWidth());
        this.f2541 = Math.max(i, drawable2.getIntrinsicWidth());
        this.f2531 = i2;
        this.f2532 = i3;
        this.f2533.setAlpha(255);
        this.f2534.setAlpha(255);
        this.f2542.addListener(new C0641());
        this.f2542.addUpdateListener(new C0642());
        m4093(r4);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4093(C0690 r2) {
        C0690 r0 = this.f2545;
        if (r0 != r2) {
            if (r0 != null) {
                m4081();
            }
            this.f2545 = r2;
            if (this.f2545 != null) {
                m4074();
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m4074() {
        this.f2545.m4385((C0690.C0700) this);
        this.f2545.m4387((C0690.C0707) this);
        this.f2545.m4388(this.f2525);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m4081() {
        this.f2545.m4403((C0690.C0700) this);
        this.f2545.m4404((C0690.C0707) this);
        this.f2545.m4405(this.f2525);
        m4086();
    }

    /* access modifiers changed from: private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m4084() {
        this.f2545.invalidate();
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4076(int i) {
        if (i == 2 && this.f2548 != 2) {
            this.f2533.setState(f2520);
            m4086();
        }
        if (i == 0) {
            m4084();
        } else {
            m4089();
        }
        if (this.f2548 == 2 && i != 2) {
            this.f2533.setState(f2521);
            m4082(1200);
        } else if (i == 1) {
            m4082(1500);
        }
        this.f2548 = i;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean m4085() {
        return C0414.m2236(this.f2545) == 1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4089() {
        int i = this.f2523;
        if (i != 0) {
            if (i == 3) {
                this.f2542.cancel();
            } else {
                return;
            }
        }
        this.f2523 = 1;
        ValueAnimator valueAnimator = this.f2542;
        valueAnimator.setFloatValues(((Float) valueAnimator.getAnimatedValue()).floatValue(), 1.0f);
        this.f2542.setDuration(500L);
        this.f2542.setStartDelay(0);
        this.f2542.start();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4090(int i) {
        int i2 = this.f2523;
        if (i2 == 1) {
            this.f2542.cancel();
        } else if (i2 != 2) {
            return;
        }
        this.f2523 = 3;
        ValueAnimator valueAnimator = this.f2542;
        valueAnimator.setFloatValues(((Float) valueAnimator.getAnimatedValue()).floatValue(), 0.0f);
        this.f2542.setDuration((long) i);
        this.f2542.start();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private void m4086() {
        this.f2545.removeCallbacks(this.f2527);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m4082(int i) {
        m4086();
        this.f2545.postDelayed(this.f2527, (long) i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4092(Canvas canvas, C0690 r2, C0690.C0717 r3) {
        if (this.f2543 != this.f2545.getWidth() || this.f2544 != this.f2545.getHeight()) {
            this.f2543 = this.f2545.getWidth();
            this.f2544 = this.f2545.getHeight();
            m4076(0);
        } else if (this.f2523 != 0) {
            if (this.f2546) {
                m4073(canvas);
            }
            if (this.f2547) {
                m4077(canvas);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4073(Canvas canvas) {
        int i = this.f2543;
        int i2 = this.f2535;
        int i3 = i - i2;
        int i4 = this.f2524;
        int i5 = this.f2522;
        int i6 = i4 - (i5 / 2);
        this.f2533.setBounds(0, 0, i2, i5);
        this.f2534.setBounds(0, 0, this.f2536, this.f2544);
        if (m4085()) {
            this.f2534.draw(canvas);
            canvas.translate((float) this.f2535, (float) i6);
            canvas.scale(-1.0f, 1.0f);
            this.f2533.draw(canvas);
            canvas.scale(1.0f, 1.0f);
            canvas.translate((float) (-this.f2535), (float) (-i6));
            return;
        }
        canvas.translate((float) i3, 0.0f);
        this.f2534.draw(canvas);
        canvas.translate(0.0f, (float) i6);
        this.f2533.draw(canvas);
        canvas.translate((float) (-i3), (float) (-i6));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m4077(Canvas canvas) {
        int i = this.f2544;
        int i2 = this.f2539;
        int i3 = i - i2;
        int i4 = this.f2529;
        int i5 = this.f2528;
        int i6 = i4 - (i5 / 2);
        this.f2537.setBounds(0, 0, i5, i2);
        this.f2538.setBounds(0, 0, this.f2543, this.f2541);
        canvas.translate(0.0f, (float) i3);
        this.f2538.draw(canvas);
        canvas.translate((float) i6, 0.0f);
        this.f2537.draw(canvas);
        canvas.translate((float) (-i6), (float) (-i3));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4091(int i, int i2) {
        int computeVerticalScrollRange = this.f2545.computeVerticalScrollRange();
        int i3 = this.f2544;
        this.f2546 = computeVerticalScrollRange - i3 > 0 && i3 >= this.f2531;
        int computeHorizontalScrollRange = this.f2545.computeHorizontalScrollRange();
        int i4 = this.f2543;
        this.f2547 = computeHorizontalScrollRange - i4 > 0 && i4 >= this.f2531;
        if (this.f2546 || this.f2547) {
            if (this.f2546) {
                float f = (float) i3;
                this.f2524 = (int) ((f * (((float) i2) + (f / 2.0f))) / ((float) computeVerticalScrollRange));
                this.f2522 = Math.min(i3, (i3 * i3) / computeVerticalScrollRange);
            }
            if (this.f2547) {
                float f2 = (float) i4;
                this.f2529 = (int) ((f2 * (((float) i) + (f2 / 2.0f))) / ((float) computeHorizontalScrollRange));
                this.f2528 = Math.min(i4, (i4 * i4) / computeHorizontalScrollRange);
            }
            int i5 = this.f2548;
            if (i5 == 0 || i5 == 1) {
                m4076(1);
            }
        } else if (this.f2548 != 0) {
            m4076(0);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m4096(C0690 r6, MotionEvent motionEvent) {
        int i = this.f2548;
        if (i == 1) {
            boolean r62 = m4095(motionEvent.getX(), motionEvent.getY());
            boolean r3 = m4098(motionEvent.getX(), motionEvent.getY());
            if (motionEvent.getAction() != 0) {
                return false;
            }
            if (!r62 && !r3) {
                return false;
            }
            if (r3) {
                this.f2549 = 1;
                this.f2530 = (float) ((int) motionEvent.getX());
            } else if (r62) {
                this.f2549 = 2;
                this.f2526 = (float) ((int) motionEvent.getY());
            }
            m4076(2);
        } else if (i == 2) {
            return true;
        } else {
            return false;
        }
        return true;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4097(C0690 r5, MotionEvent motionEvent) {
        if (this.f2548 != 0) {
            if (motionEvent.getAction() == 0) {
                boolean r52 = m4095(motionEvent.getX(), motionEvent.getY());
                boolean r2 = m4098(motionEvent.getX(), motionEvent.getY());
                if (r52 || r2) {
                    if (r2) {
                        this.f2549 = 1;
                        this.f2530 = (float) ((int) motionEvent.getX());
                    } else if (r52) {
                        this.f2549 = 2;
                        this.f2526 = (float) ((int) motionEvent.getY());
                    }
                    m4076(2);
                }
            } else if (motionEvent.getAction() == 1 && this.f2548 == 2) {
                this.f2526 = 0.0f;
                this.f2530 = 0.0f;
                m4076(1);
                this.f2549 = 0;
            } else if (motionEvent.getAction() == 2 && this.f2548 == 2) {
                m4089();
                if (this.f2549 == 1) {
                    m4075(motionEvent.getX());
                }
                if (this.f2549 == 2) {
                    m4072(motionEvent.getY());
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4072(float f) {
        int[] r3 = m4087();
        float max = Math.max((float) r3[0], Math.min((float) r3[1], f));
        if (Math.abs(((float) this.f2524) - max) >= 2.0f) {
            int r0 = m4069(this.f2526, max, r3, this.f2545.computeVerticalScrollRange(), this.f2545.computeVerticalScrollOffset(), this.f2544);
            if (r0 != 0) {
                this.f2545.scrollBy(0, r0);
            }
            this.f2526 = max;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m4075(float f) {
        int[] r3 = m4088();
        float max = Math.max((float) r3[0], Math.min((float) r3[1], f));
        if (Math.abs(((float) this.f2529) - max) >= 2.0f) {
            int r0 = m4069(this.f2530, max, r3, this.f2545.computeHorizontalScrollRange(), this.f2545.computeHorizontalScrollOffset(), this.f2543);
            if (r0 != 0) {
                this.f2545.scrollBy(r0, 0);
            }
            this.f2530 = max;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m4069(float f, float f2, int[] iArr, int i, int i2, int i3) {
        int i4 = iArr[1] - iArr[0];
        if (i4 == 0) {
            return 0;
        }
        int i5 = i - i3;
        int i6 = (int) (((f2 - f) / ((float) i4)) * ((float) i5));
        int i7 = i2 + i6;
        if (i7 >= i5 || i7 < 0) {
            return 0;
        }
        return i6;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m4095(float f, float f2) {
        if (!m4085() ? f >= ((float) (this.f2543 - this.f2535)) : f <= ((float) (this.f2535 / 2))) {
            int i = this.f2524;
            int i2 = this.f2522;
            return f2 >= ((float) (i - (i2 / 2))) && f2 <= ((float) (i + (i2 / 2)));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m4098(float f, float f2) {
        if (f2 >= ((float) (this.f2544 - this.f2539))) {
            int i = this.f2529;
            int i2 = this.f2528;
            return f >= ((float) (i - (i2 / 2))) && f <= ((float) (i + (i2 / 2)));
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private int[] m4087() {
        int[] iArr = this.f2550;
        int i = this.f2532;
        iArr[0] = i;
        iArr[1] = this.f2544 - i;
        return iArr;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private int[] m4088() {
        int[] iArr = this.f2540;
        int i = this.f2532;
        iArr[0] = i;
        iArr[1] = this.f2543 - i;
        return iArr;
    }

    /* renamed from: android.support.v7.widget.ˉˉ$ʻ  reason: contains not printable characters */
    /* compiled from: FastScroller */
    private class C0641 extends AnimatorListenerAdapter {

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean f2554;

        private C0641() {
            this.f2554 = false;
        }

        public void onAnimationEnd(Animator animator) {
            if (this.f2554) {
                this.f2554 = false;
            } else if (((Float) C0640.this.f2542.getAnimatedValue()).floatValue() == 0.0f) {
                int unused = C0640.this.f2523 = 0;
                C0640.this.m4076(0);
            } else {
                int unused2 = C0640.this.f2523 = 2;
                C0640.this.m4084();
            }
        }

        public void onAnimationCancel(Animator animator) {
            this.f2554 = true;
        }
    }

    /* renamed from: android.support.v7.widget.ˉˉ$ʼ  reason: contains not printable characters */
    /* compiled from: FastScroller */
    private class C0642 implements ValueAnimator.AnimatorUpdateListener {
        private C0642() {
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            int floatValue = (int) (((Float) valueAnimator.getAnimatedValue()).floatValue() * 255.0f);
            C0640.this.f2533.setAlpha(floatValue);
            C0640.this.f2534.setAlpha(floatValue);
            C0640.this.m4084();
        }
    }
}
