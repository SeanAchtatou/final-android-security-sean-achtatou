package android.support.v7.widget;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Build;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* renamed from: android.support.v7.widget.ʻˊ  reason: contains not printable characters */
/* compiled from: TintContextWrapper */
public class C0589 extends ContextWrapper {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final Object f2315 = new Object();

    /* renamed from: ʼ  reason: contains not printable characters */
    private static ArrayList<WeakReference<C0589>> f2316;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Resources f2317;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final Resources.Theme f2318;

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Context m3735(Context context) {
        if (!m3736(context)) {
            return context;
        }
        synchronized (f2315) {
            if (f2316 == null) {
                f2316 = new ArrayList<>();
            } else {
                for (int size = f2316.size() - 1; size >= 0; size--) {
                    WeakReference weakReference = f2316.get(size);
                    if (weakReference == null || weakReference.get() == null) {
                        f2316.remove(size);
                    }
                }
                for (int size2 = f2316.size() - 1; size2 >= 0; size2--) {
                    WeakReference weakReference2 = f2316.get(size2);
                    C0589 r2 = weakReference2 != null ? (C0589) weakReference2.get() : null;
                    if (r2 != null && r2.getBaseContext() == context) {
                        return r2;
                    }
                }
            }
            C0589 r1 = new C0589(context);
            f2316.add(new WeakReference(r1));
            return r1;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static boolean m3736(Context context) {
        if ((context instanceof C0589) || (context.getResources() instanceof C0591) || (context.getResources() instanceof C0600)) {
            return false;
        }
        if (Build.VERSION.SDK_INT < 21 || C0600.m3819()) {
            return true;
        }
        return false;
    }

    private C0589(Context context) {
        super(context);
        if (C0600.m3819()) {
            this.f2317 = new C0600(this, context.getResources());
            this.f2318 = this.f2317.newTheme();
            this.f2318.setTo(context.getTheme());
            return;
        }
        this.f2317 = new C0591(this, context.getResources());
        this.f2318 = null;
    }

    public Resources.Theme getTheme() {
        Resources.Theme theme = this.f2318;
        return theme == null ? super.getTheme() : theme;
    }

    public void setTheme(int i) {
        Resources.Theme theme = this.f2318;
        if (theme == null) {
            super.setTheme(i);
        } else {
            theme.applyStyle(i, true);
        }
    }

    public Resources getResources() {
        return this.f2317;
    }

    public AssetManager getAssets() {
        return this.f2317.getAssets();
    }
}
