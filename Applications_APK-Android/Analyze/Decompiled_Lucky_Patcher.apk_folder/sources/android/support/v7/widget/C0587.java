package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v4.ʼ.C0287;
import android.util.AttributeSet;
import android.util.TypedValue;

/* renamed from: android.support.v7.widget.ʻˈ  reason: contains not printable characters */
/* compiled from: ThemeUtils */
class C0587 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final int[] f2305 = {-16842910};

    /* renamed from: ʼ  reason: contains not printable characters */
    static final int[] f2306 = {16842908};

    /* renamed from: ʽ  reason: contains not printable characters */
    static final int[] f2307 = {16843518};

    /* renamed from: ʾ  reason: contains not printable characters */
    static final int[] f2308 = {16842919};

    /* renamed from: ʿ  reason: contains not printable characters */
    static final int[] f2309 = {16842912};

    /* renamed from: ˆ  reason: contains not printable characters */
    static final int[] f2310 = {16842913};

    /* renamed from: ˈ  reason: contains not printable characters */
    static final int[] f2311 = {-16842919, -16842908};

    /* renamed from: ˉ  reason: contains not printable characters */
    static final int[] f2312 = new int[0];

    /* renamed from: ˊ  reason: contains not printable characters */
    private static final ThreadLocal<TypedValue> f2313 = new ThreadLocal<>();

    /* renamed from: ˋ  reason: contains not printable characters */
    private static final int[] f2314 = new int[1];

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m3728(Context context, int i) {
        int[] iArr = f2314;
        iArr[0] = i;
        C0592 r2 = C0592.m3739(context, (AttributeSet) null, iArr);
        try {
            return r2.m3747(0, 0);
        } finally {
            r2.m3745();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static ColorStateList m3731(Context context, int i) {
        int[] iArr = f2314;
        iArr[0] = i;
        C0592 r2 = C0592.m3739(context, (AttributeSet) null, iArr);
        try {
            return r2.m3754(0);
        } finally {
            r2.m3745();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static int m3732(Context context, int i) {
        ColorStateList r0 = m3731(context, i);
        if (r0 != null && r0.isStateful()) {
            return r0.getColorForState(f2305, r0.getDefaultColor());
        }
        TypedValue r02 = m3730();
        context.getTheme().resolveAttribute(16842803, r02, true);
        return m3729(context, i, r02.getFloat());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static TypedValue m3730() {
        TypedValue typedValue = f2313.get();
        if (typedValue != null) {
            return typedValue;
        }
        TypedValue typedValue2 = new TypedValue();
        f2313.set(typedValue2);
        return typedValue2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static int m3729(Context context, int i, float f) {
        int r0 = m3728(context, i);
        return C0287.m1697(r0, Math.round(((float) Color.alpha(r0)) * f));
    }
}
