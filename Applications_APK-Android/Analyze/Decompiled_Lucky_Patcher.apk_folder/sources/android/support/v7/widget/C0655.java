package android.support.v7.widget;

import android.support.v7.widget.C0690;
import android.view.View;

/* renamed from: android.support.v7.widget.ˏˏ  reason: contains not printable characters */
/* compiled from: LayoutState */
class C0655 {

    /* renamed from: ʻ  reason: contains not printable characters */
    boolean f2610 = true;

    /* renamed from: ʼ  reason: contains not printable characters */
    int f2611;

    /* renamed from: ʽ  reason: contains not printable characters */
    int f2612;

    /* renamed from: ʾ  reason: contains not printable characters */
    int f2613;

    /* renamed from: ʿ  reason: contains not printable characters */
    int f2614;

    /* renamed from: ˆ  reason: contains not printable characters */
    int f2615 = 0;

    /* renamed from: ˈ  reason: contains not printable characters */
    int f2616 = 0;

    /* renamed from: ˉ  reason: contains not printable characters */
    boolean f2617;

    /* renamed from: ˊ  reason: contains not printable characters */
    boolean f2618;

    C0655() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m4157(C0690.C0717 r2) {
        int i = this.f2612;
        return i >= 0 && i < r2.m4775();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public View m4156(C0690.C0711 r3) {
        View r32 = r3.m4731(this.f2612);
        this.f2612 += this.f2613;
        return r32;
    }

    public String toString() {
        return "LayoutState{mAvailable=" + this.f2611 + ", mCurrentPosition=" + this.f2612 + ", mItemDirection=" + this.f2613 + ", mLayoutDirection=" + this.f2614 + ", mStartLine=" + this.f2615 + ", mEndLine=" + this.f2616 + '}';
    }
}
