package android.support.v7.widget;

import android.graphics.Rect;
import android.support.v7.widget.C0690;
import android.view.View;

/* renamed from: android.support.v7.widget.ⁱⁱ  reason: contains not printable characters */
/* compiled from: OrientationHelper */
public abstract class C0688 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final C0690.C0701 f2727;

    /* renamed from: ʼ  reason: contains not printable characters */
    final Rect f2728;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f2729;

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract int m4289(View view);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m4291(int i);

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract int m4293(View view);

    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract int m4294();

    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract int m4295(View view);

    /* renamed from: ʾ  reason: contains not printable characters */
    public abstract int m4296();

    /* renamed from: ʾ  reason: contains not printable characters */
    public abstract int m4297(View view);

    /* renamed from: ʿ  reason: contains not printable characters */
    public abstract int m4298();

    /* renamed from: ʿ  reason: contains not printable characters */
    public abstract int m4299(View view);

    /* renamed from: ˆ  reason: contains not printable characters */
    public abstract int m4300();

    /* renamed from: ˆ  reason: contains not printable characters */
    public abstract int m4301(View view);

    /* renamed from: ˈ  reason: contains not printable characters */
    public abstract int m4302();

    /* renamed from: ˉ  reason: contains not printable characters */
    public abstract int m4303();

    /* renamed from: ˊ  reason: contains not printable characters */
    public abstract int m4304();

    private C0688(C0690.C0701 r2) {
        this.f2729 = Integer.MIN_VALUE;
        this.f2728 = new Rect();
        this.f2727 = r2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4290() {
        this.f2729 = m4300();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m4292() {
        if (Integer.MIN_VALUE == this.f2729) {
            return 0;
        }
        return m4300() - this.f2729;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0688 m4287(C0690.C0701 r1, int i) {
        if (i == 0) {
            return m4286(r1);
        }
        if (i == 1) {
            return m4288(r1);
        }
        throw new IllegalArgumentException("invalid orientation");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0688 m4286(C0690.C0701 r1) {
        return new C0688(r1) {
            /* renamed from: ʾ  reason: contains not printable characters */
            public int m4310() {
                return this.f2727.m4670() - this.f2727.m4664();
            }

            /* renamed from: ʿ  reason: contains not printable characters */
            public int m4312() {
                return this.f2727.m4670();
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m4306(int i) {
                this.f2727.m4647(i);
            }

            /* renamed from: ʽ  reason: contains not printable characters */
            public int m4308() {
                return this.f2727.m4672();
            }

            /* renamed from: ʿ  reason: contains not printable characters */
            public int m4313(View view) {
                C0690.C0704 r0 = (C0690.C0704) view.getLayoutParams();
                return this.f2727.m4635(view) + r0.leftMargin + r0.rightMargin;
            }

            /* renamed from: ˆ  reason: contains not printable characters */
            public int m4315(View view) {
                C0690.C0704 r0 = (C0690.C0704) view.getLayoutParams();
                return this.f2727.m4641(view) + r0.topMargin + r0.bottomMargin;
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public int m4307(View view) {
                return this.f2727.m4648(view) + ((C0690.C0704) view.getLayoutParams()).rightMargin;
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public int m4305(View view) {
                return this.f2727.m4644(view) - ((C0690.C0704) view.getLayoutParams()).leftMargin;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, android.graphics.Rect):void
             arg types: [android.view.View, int, android.graphics.Rect]
             candidates:
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int):int
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int, android.view.View):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, boolean):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.graphics.Rect, int, int):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v4.ˉ.ʻ.ʼ):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.accessibility.AccessibilityEvent):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ˊ):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.view.View):boolean
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.os.Bundle):boolean
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, boolean):boolean
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, android.graphics.Rect):void */
            /* renamed from: ʽ  reason: contains not printable characters */
            public int m4309(View view) {
                this.f2727.m4577(view, true, this.f2728);
                return this.f2728.right;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, android.graphics.Rect):void
             arg types: [android.view.View, int, android.graphics.Rect]
             candidates:
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int):int
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int, android.view.View):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, boolean):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.graphics.Rect, int, int):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v4.ˉ.ʻ.ʼ):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.accessibility.AccessibilityEvent):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ˊ):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.view.View):boolean
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.os.Bundle):boolean
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, boolean):boolean
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, android.graphics.Rect):void */
            /* renamed from: ʾ  reason: contains not printable characters */
            public int m4311(View view) {
                this.f2727.m4577(view, true, this.f2728);
                return this.f2728.left;
            }

            /* renamed from: ˆ  reason: contains not printable characters */
            public int m4314() {
                return (this.f2727.m4670() - this.f2727.m4672()) - this.f2727.m4664();
            }

            /* renamed from: ˈ  reason: contains not printable characters */
            public int m4316() {
                return this.f2727.m4664();
            }

            /* renamed from: ˉ  reason: contains not printable characters */
            public int m4317() {
                return this.f2727.m4668();
            }

            /* renamed from: ˊ  reason: contains not printable characters */
            public int m4318() {
                return this.f2727.m4669();
            }
        };
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static C0688 m4288(C0690.C0701 r1) {
        return new C0688(r1) {
            /* renamed from: ʾ  reason: contains not printable characters */
            public int m4324() {
                return this.f2727.m4671() - this.f2727.m4593();
            }

            /* renamed from: ʿ  reason: contains not printable characters */
            public int m4326() {
                return this.f2727.m4671();
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m4320(int i) {
                this.f2727.m4649(i);
            }

            /* renamed from: ʽ  reason: contains not printable characters */
            public int m4322() {
                return this.f2727.m4662();
            }

            /* renamed from: ʿ  reason: contains not printable characters */
            public int m4327(View view) {
                C0690.C0704 r0 = (C0690.C0704) view.getLayoutParams();
                return this.f2727.m4641(view) + r0.topMargin + r0.bottomMargin;
            }

            /* renamed from: ˆ  reason: contains not printable characters */
            public int m4329(View view) {
                C0690.C0704 r0 = (C0690.C0704) view.getLayoutParams();
                return this.f2727.m4635(view) + r0.leftMargin + r0.rightMargin;
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public int m4321(View view) {
                return this.f2727.m4650(view) + ((C0690.C0704) view.getLayoutParams()).bottomMargin;
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public int m4319(View view) {
                return this.f2727.m4646(view) - ((C0690.C0704) view.getLayoutParams()).topMargin;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, android.graphics.Rect):void
             arg types: [android.view.View, int, android.graphics.Rect]
             candidates:
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int):int
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int, android.view.View):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, boolean):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.graphics.Rect, int, int):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v4.ˉ.ʻ.ʼ):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.accessibility.AccessibilityEvent):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ˊ):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.view.View):boolean
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.os.Bundle):boolean
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, boolean):boolean
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, android.graphics.Rect):void */
            /* renamed from: ʽ  reason: contains not printable characters */
            public int m4323(View view) {
                this.f2727.m4577(view, true, this.f2728);
                return this.f2728.bottom;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, android.graphics.Rect):void
             arg types: [android.view.View, int, android.graphics.Rect]
             candidates:
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int):int
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int, android.view.View):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, boolean):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.graphics.Rect, int, int):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v4.ˉ.ʻ.ʼ):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.accessibility.AccessibilityEvent):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ˊ):void
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.view.View):boolean
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.os.Bundle):boolean
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, boolean):boolean
              android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, android.graphics.Rect):void */
            /* renamed from: ʾ  reason: contains not printable characters */
            public int m4325(View view) {
                this.f2727.m4577(view, true, this.f2728);
                return this.f2728.top;
            }

            /* renamed from: ˆ  reason: contains not printable characters */
            public int m4328() {
                return (this.f2727.m4671() - this.f2727.m4662()) - this.f2727.m4593();
            }

            /* renamed from: ˈ  reason: contains not printable characters */
            public int m4330() {
                return this.f2727.m4593();
            }

            /* renamed from: ˉ  reason: contains not printable characters */
            public int m4331() {
                return this.f2727.m4669();
            }

            /* renamed from: ˊ  reason: contains not printable characters */
            public int m4332() {
                return this.f2727.m4668();
            }
        };
    }
}
