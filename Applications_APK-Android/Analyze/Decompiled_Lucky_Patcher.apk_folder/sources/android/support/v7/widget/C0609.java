package android.support.v7.widget;

import android.support.v7.view.menu.C0518;
import android.view.Menu;
import android.view.Window;

/* renamed from: android.support.v7.widget.ʼʼ  reason: contains not printable characters */
/* compiled from: DecorContentParent */
public interface C0609 {
    void setWindowCallback(Window.Callback callback);

    void setWindowTitle(CharSequence charSequence);

    /* renamed from: ʻ  reason: contains not printable characters */
    void m3860(int i);

    /* renamed from: ʻ  reason: contains not printable characters */
    void m3861(Menu menu, C0518.C0519 r2);

    /* renamed from: ʿ  reason: contains not printable characters */
    boolean m3862();

    /* renamed from: ˆ  reason: contains not printable characters */
    boolean m3863();

    /* renamed from: ˈ  reason: contains not printable characters */
    boolean m3864();

    /* renamed from: ˉ  reason: contains not printable characters */
    boolean m3865();

    /* renamed from: ˊ  reason: contains not printable characters */
    boolean m3866();

    /* renamed from: ˋ  reason: contains not printable characters */
    void m3867();

    /* renamed from: ˎ  reason: contains not printable characters */
    void m3868();
}
