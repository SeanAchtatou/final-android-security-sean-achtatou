package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.support.v4.widget.C0167;
import android.support.v7.ʻ.C0727;
import android.support.v7.ʼ.ʻ.C0739;
import android.util.AttributeSet;
import android.widget.ImageView;

/* renamed from: android.support.v7.widget.ٴ  reason: contains not printable characters */
/* compiled from: AppCompatImageHelper */
public class C0671 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final ImageView f2684;

    /* renamed from: ʼ  reason: contains not printable characters */
    private C0590 f2685;

    /* renamed from: ʽ  reason: contains not printable characters */
    private C0590 f2686;

    /* renamed from: ʾ  reason: contains not printable characters */
    private C0590 f2687;

    public C0671(ImageView imageView) {
        this.f2684 = imageView;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4239(AttributeSet attributeSet, int i) {
        int r1;
        C0592 r4 = C0592.m3740(this.f2684.getContext(), attributeSet, C0727.C0737.AppCompatImageView, i, 0);
        try {
            Drawable drawable = this.f2684.getDrawable();
            if (!(drawable != null || (r1 = r4.m3757(C0727.C0737.AppCompatImageView_srcCompat, -1)) == -1 || (drawable = C0739.m4883(this.f2684.getContext(), r1)) == null)) {
                this.f2684.setImageDrawable(drawable);
            }
            if (drawable != null) {
                C0670.m4231(drawable);
            }
            if (r4.m3758(C0727.C0737.AppCompatImageView_tint)) {
                C0167.m1025(this.f2684, r4.m3754(C0727.C0737.AppCompatImageView_tint));
            }
            if (r4.m3758(C0727.C0737.AppCompatImageView_tintMode)) {
                C0167.m1026(this.f2684, C0670.m4230(r4.m3742(C0727.C0737.AppCompatImageView_tintMode, -1), null));
            }
        } finally {
            r4.m3745();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4236(int i) {
        if (i != 0) {
            Drawable r2 = C0739.m4883(this.f2684.getContext(), i);
            if (r2 != null) {
                C0670.m4231(r2);
            }
            this.f2684.setImageDrawable(r2);
        } else {
            this.f2684.setImageDrawable(null);
        }
        m4243();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m4240() {
        return Build.VERSION.SDK_INT < 21 || !(this.f2684.getBackground() instanceof RippleDrawable);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4237(ColorStateList colorStateList) {
        if (this.f2686 == null) {
            this.f2686 = new C0590();
        }
        C0590 r0 = this.f2686;
        r0.f2319 = colorStateList;
        r0.f2322 = true;
        m4243();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public ColorStateList m4241() {
        C0590 r0 = this.f2686;
        if (r0 != null) {
            return r0.f2319;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4238(PorterDuff.Mode mode) {
        if (this.f2686 == null) {
            this.f2686 = new C0590();
        }
        C0590 r0 = this.f2686;
        r0.f2320 = mode;
        r0.f2321 = true;
        m4243();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public PorterDuff.Mode m4242() {
        C0590 r0 = this.f2686;
        if (r0 != null) {
            return r0.f2320;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m4243() {
        Drawable drawable = this.f2684.getDrawable();
        if (drawable != null) {
            C0670.m4231(drawable);
        }
        if (drawable == null) {
            return;
        }
        if (!m4235() || !m4234(drawable)) {
            C0590 r1 = this.f2686;
            if (r1 != null) {
                C0656.m4167(drawable, r1, this.f2684.getDrawableState());
                return;
            }
            C0590 r12 = this.f2685;
            if (r12 != null) {
                C0656.m4167(drawable, r12, this.f2684.getDrawableState());
            }
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean m4235() {
        int i = Build.VERSION.SDK_INT;
        if (i <= 21) {
            return i == 21;
        }
        if (this.f2685 != null) {
            return true;
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m4234(Drawable drawable) {
        if (this.f2687 == null) {
            this.f2687 = new C0590();
        }
        C0590 r0 = this.f2687;
        r0.m3737();
        ColorStateList r1 = C0167.m1024(this.f2684);
        if (r1 != null) {
            r0.f2322 = true;
            r0.f2319 = r1;
        }
        PorterDuff.Mode r12 = C0167.m1027(this.f2684);
        if (r12 != null) {
            r0.f2321 = true;
            r0.f2320 = r12;
        }
        if (!r0.f2322 && !r0.f2321) {
            return false;
        }
        C0656.m4167(drawable, r0, this.f2684.getDrawableState());
        return true;
    }
}
