package android.support.v7.widget;

import android.graphics.Rect;
import android.os.Build;
import android.support.v4.ˉ.C0414;
import android.util.Log;
import android.view.View;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: android.support.v7.widget.ʻᵔ  reason: contains not printable characters */
/* compiled from: ViewUtils */
public class C0607 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Method f2382;

    static {
        if (Build.VERSION.SDK_INT >= 18) {
            try {
                f2382 = View.class.getDeclaredMethod("computeFitSystemWindows", Rect.class, Rect.class);
                if (!f2382.isAccessible()) {
                    f2382.setAccessible(true);
                }
            } catch (NoSuchMethodException unused) {
                Log.d("ViewUtils", "Could not find method computeFitSystemWindows. Oh well.");
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m3858(View view) {
        return C0414.m2236(view) == 1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m3857(View view, Rect rect, Rect rect2) {
        Method method = f2382;
        if (method != null) {
            try {
                method.invoke(view, rect, rect2);
            } catch (Exception e) {
                Log.d("ViewUtils", "Could not invoke computeFitSystemWindows", e);
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static void m3859(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            try {
                Method method = view.getClass().getMethod("makeOptionalFitsSystemWindows", new Class[0]);
                if (!method.isAccessible()) {
                    method.setAccessible(true);
                }
                method.invoke(view, new Object[0]);
            } catch (NoSuchMethodException unused) {
                Log.d("ViewUtils", "Could not find method makeOptionalFitsSystemWindows. Oh well...");
            } catch (InvocationTargetException e) {
                Log.d("ViewUtils", "Could not invoke makeOptionalFitsSystemWindows", e);
            } catch (IllegalAccessException e2) {
                Log.d("ViewUtils", "Could not invoke makeOptionalFitsSystemWindows", e2);
            }
        }
    }
}
