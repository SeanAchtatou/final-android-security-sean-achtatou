package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import java.lang.ref.WeakReference;

/* renamed from: android.support.v7.widget.ʻˎ  reason: contains not printable characters */
/* compiled from: TintResources */
class C0591 extends C0722 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final WeakReference<Context> f2323;

    public C0591(Context context, Resources resources) {
        super(resources);
        this.f2323 = new WeakReference<>(context);
    }

    public Drawable getDrawable(int i) {
        Drawable drawable = super.getDrawable(i);
        Context context = this.f2323.get();
        if (!(drawable == null || context == null)) {
            C0656.m4164();
            C0656.m4170(context, i, drawable);
        }
        return drawable;
    }
}
