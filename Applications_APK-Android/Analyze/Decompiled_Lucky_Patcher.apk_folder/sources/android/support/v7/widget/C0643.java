package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.ˉ.C0413;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;

/* renamed from: android.support.v7.widget.ˊ  reason: contains not printable characters */
/* compiled from: AppCompatButton */
public class C0643 extends Button implements C0413 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C0639 f2556;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final C0726 f2557;

    public C0643(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0727.C0728.buttonStyle);
    }

    public C0643(Context context, AttributeSet attributeSet, int i) {
        super(C0589.m3735(context), attributeSet, i);
        this.f2556 = new C0639(this);
        this.f2556.m4065(attributeSet, i);
        this.f2557 = C0726.m4855(this);
        this.f2557.m4864(attributeSet, i);
        this.f2557.m4858();
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        C0639 r0 = this.f2556;
        if (r0 != null) {
            r0.m4061(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        C0639 r0 = this.f2556;
        if (r0 != null) {
            r0.m4064(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        C0639 r0 = this.f2556;
        if (r0 != null) {
            r0.m4062(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        C0639 r0 = this.f2556;
        if (r0 != null) {
            return r0.m4060();
        }
        return null;
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        C0639 r0 = this.f2556;
        if (r0 != null) {
            r0.m4063(mode);
        }
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        C0639 r0 = this.f2556;
        if (r0 != null) {
            return r0.m4066();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        C0639 r0 = this.f2556;
        if (r0 != null) {
            r0.m4068();
        }
        C0726 r02 = this.f2557;
        if (r02 != null) {
            r02.m4858();
        }
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        C0726 r0 = this.f2557;
        if (r0 != null) {
            r0.m4862(context, i);
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(Button.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(Button.class.getName());
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        C0726 r0 = this.f2557;
        if (r0 != null) {
            r0.m4866(z, i, i2, i3, i4);
        }
    }

    public void setTextSize(int i, float f) {
        if (Build.VERSION.SDK_INT >= 26) {
            super.setTextSize(i, f);
            return;
        }
        C0726 r0 = this.f2557;
        if (r0 != null) {
            r0.m4860(i, f);
        }
    }

    /* access modifiers changed from: protected */
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        super.onTextChanged(charSequence, i, i2, i3);
        if (this.f2557 != null && Build.VERSION.SDK_INT < 26 && this.f2557.m4869()) {
            this.f2557.m4868();
        }
    }

    public void setAutoSizeTextTypeWithDefaults(int i) {
        if (Build.VERSION.SDK_INT >= 26) {
            super.setAutoSizeTextTypeWithDefaults(i);
            return;
        }
        C0726 r0 = this.f2557;
        if (r0 != null) {
            r0.m4859(i);
        }
    }

    public void setAutoSizeTextTypeUniformWithConfiguration(int i, int i2, int i3, int i4) {
        if (Build.VERSION.SDK_INT >= 26) {
            super.setAutoSizeTextTypeUniformWithConfiguration(i, i2, i3, i4);
            return;
        }
        C0726 r0 = this.f2557;
        if (r0 != null) {
            r0.m4861(i, i2, i3, i4);
        }
    }

    public void setAutoSizeTextTypeUniformWithPresetSizes(int[] iArr, int i) {
        if (Build.VERSION.SDK_INT >= 26) {
            super.setAutoSizeTextTypeUniformWithPresetSizes(iArr, i);
            return;
        }
        C0726 r0 = this.f2557;
        if (r0 != null) {
            r0.m4867(iArr, i);
        }
    }

    public int getAutoSizeTextType() {
        if (Build.VERSION.SDK_INT < 26) {
            C0726 r0 = this.f2557;
            if (r0 != null) {
                return r0.m4870();
            }
            return 0;
        } else if (super.getAutoSizeTextType() == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    public int getAutoSizeStepGranularity() {
        if (Build.VERSION.SDK_INT >= 26) {
            return super.getAutoSizeStepGranularity();
        }
        C0726 r0 = this.f2557;
        if (r0 != null) {
            return r0.m4871();
        }
        return -1;
    }

    public int getAutoSizeMinTextSize() {
        if (Build.VERSION.SDK_INT >= 26) {
            return super.getAutoSizeMinTextSize();
        }
        C0726 r0 = this.f2557;
        if (r0 != null) {
            return r0.m4872();
        }
        return -1;
    }

    public int getAutoSizeMaxTextSize() {
        if (Build.VERSION.SDK_INT >= 26) {
            return super.getAutoSizeMaxTextSize();
        }
        C0726 r0 = this.f2557;
        if (r0 != null) {
            return r0.m4873();
        }
        return -1;
    }

    public int[] getAutoSizeTextAvailableSizes() {
        if (Build.VERSION.SDK_INT >= 26) {
            return super.getAutoSizeTextAvailableSizes();
        }
        C0726 r0 = this.f2557;
        if (r0 != null) {
            return r0.m4874();
        }
        return new int[0];
    }

    public void setSupportAllCaps(boolean z) {
        C0726 r0 = this.f2557;
        if (r0 != null) {
            r0.m4865(z);
        }
    }
}
