package android.support.v7.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RatingBar;

/* renamed from: android.support.v7.widget.ⁱ  reason: contains not printable characters */
/* compiled from: AppCompatRatingBar */
public class C0687 extends RatingBar {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C0681 f2726;

    public C0687(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0727.C0728.ratingBarStyle);
    }

    public C0687(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2726 = new C0681(this);
        this.f2726.m4268(attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public synchronized void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        Bitmap r3 = this.f2726.m4267();
        if (r3 != null) {
            setMeasuredDimension(View.resolveSizeAndState(r3.getWidth() * getNumStars(), i, 0), getMeasuredHeight());
        }
    }
}
