package android.support.v7.widget;

import android.support.v7.widget.C0690;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

/* renamed from: android.support.v7.widget.ʽʽ  reason: contains not printable characters */
/* compiled from: ChildHelper */
class C0611 {

    /* renamed from: ʻ  reason: contains not printable characters */
    final C0613 f2384;

    /* renamed from: ʼ  reason: contains not printable characters */
    final C0612 f2385 = new C0612();

    /* renamed from: ʽ  reason: contains not printable characters */
    final List<View> f2386 = new ArrayList();

    /* renamed from: android.support.v7.widget.ʽʽ$ʼ  reason: contains not printable characters */
    /* compiled from: ChildHelper */
    interface C0613 {
        /* renamed from: ʻ  reason: contains not printable characters */
        int m3897();

        /* renamed from: ʻ  reason: contains not printable characters */
        int m3898(View view);

        /* renamed from: ʻ  reason: contains not printable characters */
        void m3899(int i);

        /* renamed from: ʻ  reason: contains not printable characters */
        void m3900(View view, int i);

        /* renamed from: ʻ  reason: contains not printable characters */
        void m3901(View view, int i, ViewGroup.LayoutParams layoutParams);

        /* renamed from: ʼ  reason: contains not printable characters */
        C0690.C0720 m3902(View view);

        /* renamed from: ʼ  reason: contains not printable characters */
        View m3903(int i);

        /* renamed from: ʼ  reason: contains not printable characters */
        void m3904();

        /* renamed from: ʽ  reason: contains not printable characters */
        void m3905(int i);

        /* renamed from: ʽ  reason: contains not printable characters */
        void m3906(View view);

        /* renamed from: ʾ  reason: contains not printable characters */
        void m3907(View view);
    }

    C0611(C0613 r1) {
        this.f2384 = r1;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private void m3870(View view) {
        this.f2386.add(view);
        this.f2384.m3906(view);
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean m3871(View view) {
        if (!this.f2386.remove(view)) {
            return false;
        }
        this.f2384.m3907(view);
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3877(View view, boolean z) {
        m3876(view, -1, z);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3876(View view, int i, boolean z) {
        int i2;
        if (i < 0) {
            i2 = this.f2384.m3897();
        } else {
            i2 = m3869(i);
        }
        this.f2385.m3892(i2, z);
        if (z) {
            m3870(view);
        }
        this.f2384.m3900(view, i2);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private int m3869(int i) {
        if (i < 0) {
            return -1;
        }
        int r1 = this.f2384.m3897();
        int i2 = i;
        while (i2 < r1) {
            int r3 = i - (i2 - this.f2385.m3896(i2));
            if (r3 == 0) {
                while (this.f2385.m3894(i2)) {
                    i2++;
                }
                return i2;
            }
            i2 += r3;
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3874(View view) {
        int r0 = this.f2384.m3898(view);
        if (r0 >= 0) {
            if (this.f2385.m3895(r0)) {
                m3871(view);
            }
            this.f2384.m3899(r0);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3873(int i) {
        int r3 = m3869(i);
        View r0 = this.f2384.m3903(r3);
        if (r0 != null) {
            if (this.f2385.m3895(r3)) {
                m3871(r0);
            }
            this.f2384.m3899(r3);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public View m3880(int i) {
        return this.f2384.m3903(m3869(i));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3872() {
        this.f2385.m3890();
        for (int size = this.f2386.size() - 1; size >= 0; size--) {
            this.f2384.m3907(this.f2386.get(size));
            this.f2386.remove(size);
        }
        this.f2384.m3904();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public View m3882(int i) {
        int size = this.f2386.size();
        for (int i2 = 0; i2 < size; i2++) {
            View view = this.f2386.get(i2);
            C0690.C0720 r3 = this.f2384.m3902(view);
            if (r3.m4814() == i && !r3.m4824() && !r3.m4827()) {
                return view;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3875(View view, int i, ViewGroup.LayoutParams layoutParams, boolean z) {
        int i2;
        if (i < 0) {
            i2 = this.f2384.m3897();
        } else {
            i2 = m3869(i);
        }
        this.f2385.m3892(i2, z);
        if (z) {
            m3870(view);
        }
        this.f2384.m3901(view, i2, layoutParams);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m3878() {
        return this.f2384.m3897() - this.f2386.size();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public int m3881() {
        return this.f2384.m3897();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public View m3884(int i) {
        return this.f2384.m3903(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m3886(int i) {
        int r2 = m3869(i);
        this.f2385.m3895(r2);
        this.f2384.m3905(r2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m3879(View view) {
        int r3 = this.f2384.m3898(view);
        if (r3 != -1 && !this.f2385.m3894(r3)) {
            return r3 - this.f2385.m3896(r3);
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m3883(View view) {
        return this.f2386.contains(view);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m3885(View view) {
        int r0 = this.f2384.m3898(view);
        if (r0 >= 0) {
            this.f2385.m3891(r0);
            m3870(view);
            return;
        }
        throw new IllegalArgumentException("view is not a child, cannot hide " + view);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m3887(View view) {
        int r0 = this.f2384.m3898(view);
        if (r0 < 0) {
            throw new IllegalArgumentException("view is not a child, cannot hide " + view);
        } else if (this.f2385.m3894(r0)) {
            this.f2385.m3893(r0);
            m3871(view);
        } else {
            throw new RuntimeException("trying to unhide a view that was not hidden" + view);
        }
    }

    public String toString() {
        return this.f2385.toString() + ", hidden list:" + this.f2386.size();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m3888(View view) {
        int r0 = this.f2384.m3898(view);
        if (r0 == -1) {
            m3871(view);
            return true;
        } else if (!this.f2385.m3894(r0)) {
            return false;
        } else {
            this.f2385.m3895(r0);
            m3871(view);
            this.f2384.m3899(r0);
            return true;
        }
    }

    /* renamed from: android.support.v7.widget.ʽʽ$ʻ  reason: contains not printable characters */
    /* compiled from: ChildHelper */
    static class C0612 {

        /* renamed from: ʻ  reason: contains not printable characters */
        long f2387 = 0;

        /* renamed from: ʼ  reason: contains not printable characters */
        C0612 f2388;

        C0612() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3891(int i) {
            if (i >= 64) {
                m3889();
                this.f2388.m3891(i - 64);
                return;
            }
            this.f2387 |= 1 << i;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private void m3889() {
            if (this.f2388 == null) {
                this.f2388 = new C0612();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m3893(int i) {
            if (i >= 64) {
                C0612 r1 = this.f2388;
                if (r1 != null) {
                    r1.m3893(i - 64);
                    return;
                }
                return;
            }
            this.f2387 &= (1 << i) ^ -1;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m3894(int i) {
            if (i < 64) {
                return (this.f2387 & (1 << i)) != 0;
            }
            m3889();
            return this.f2388.m3894(i - 64);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3890() {
            this.f2387 = 0;
            C0612 r0 = this.f2388;
            if (r0 != null) {
                r0.m3890();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3892(int i, boolean z) {
            if (i >= 64) {
                m3889();
                this.f2388.m3892(i - 64, z);
                return;
            }
            boolean z2 = (this.f2387 & Long.MIN_VALUE) != 0;
            long j = (1 << i) - 1;
            long j2 = this.f2387;
            this.f2387 = ((j2 & (j ^ -1)) << 1) | (j2 & j);
            if (z) {
                m3891(i);
            } else {
                m3893(i);
            }
            if (z2 || this.f2388 != null) {
                m3889();
                this.f2388.m3892(0, z2);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʾ  reason: contains not printable characters */
        public boolean m3895(int i) {
            if (i >= 64) {
                m3889();
                return this.f2388.m3895(i - 64);
            }
            long j = 1 << i;
            boolean z = (this.f2387 & j) != 0;
            this.f2387 &= j ^ -1;
            long j2 = j - 1;
            long j3 = this.f2387;
            this.f2387 = Long.rotateRight(j3 & (j2 ^ -1), 1) | (j3 & j2);
            C0612 r14 = this.f2388;
            if (r14 != null) {
                if (r14.m3894(0)) {
                    m3891(63);
                }
                this.f2388.m3895(0);
            }
            return z;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʿ  reason: contains not printable characters */
        public int m3896(int i) {
            C0612 r0 = this.f2388;
            if (r0 == null) {
                if (i >= 64) {
                    return Long.bitCount(this.f2387);
                }
                return Long.bitCount(this.f2387 & ((1 << i) - 1));
            } else if (i < 64) {
                return Long.bitCount(this.f2387 & ((1 << i) - 1));
            } else {
                return r0.m3896(i - 64) + Long.bitCount(this.f2387);
            }
        }

        public String toString() {
            if (this.f2388 == null) {
                return Long.toBinaryString(this.f2387);
            }
            return this.f2388.toString() + "xx" + Long.toBinaryString(this.f2387);
        }
    }
}
