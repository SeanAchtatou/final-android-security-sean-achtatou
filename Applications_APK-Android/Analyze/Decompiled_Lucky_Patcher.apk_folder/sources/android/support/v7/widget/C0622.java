package android.support.v7.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.support.v4.ˉ.C0414;
import android.support.v7.widget.C0690;
import android.view.View;
import android.view.ViewPropertyAnimator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: android.support.v7.widget.ʾʾ  reason: contains not printable characters */
/* compiled from: DefaultItemAnimator */
public class C0622 extends C0584 {

    /* renamed from: ˊ  reason: contains not printable characters */
    private static TimeInterpolator f2421;

    /* renamed from: ʻ  reason: contains not printable characters */
    ArrayList<ArrayList<C0690.C0720>> f2422 = new ArrayList<>();

    /* renamed from: ʼ  reason: contains not printable characters */
    ArrayList<ArrayList<C0624>> f2423 = new ArrayList<>();

    /* renamed from: ʽ  reason: contains not printable characters */
    ArrayList<ArrayList<C0623>> f2424 = new ArrayList<>();

    /* renamed from: ʾ  reason: contains not printable characters */
    ArrayList<C0690.C0720> f2425 = new ArrayList<>();

    /* renamed from: ʿ  reason: contains not printable characters */
    ArrayList<C0690.C0720> f2426 = new ArrayList<>();

    /* renamed from: ˆ  reason: contains not printable characters */
    ArrayList<C0690.C0720> f2427 = new ArrayList<>();

    /* renamed from: ˈ  reason: contains not printable characters */
    ArrayList<C0690.C0720> f2428 = new ArrayList<>();

    /* renamed from: ˋ  reason: contains not printable characters */
    private ArrayList<C0690.C0720> f2429 = new ArrayList<>();

    /* renamed from: ˎ  reason: contains not printable characters */
    private ArrayList<C0690.C0720> f2430 = new ArrayList<>();

    /* renamed from: ˏ  reason: contains not printable characters */
    private ArrayList<C0624> f2431 = new ArrayList<>();

    /* renamed from: ˑ  reason: contains not printable characters */
    private ArrayList<C0623> f2432 = new ArrayList<>();

    /* renamed from: android.support.v7.widget.ʾʾ$ʼ  reason: contains not printable characters */
    /* compiled from: DefaultItemAnimator */
    private static class C0624 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0690.C0720 f2467;

        /* renamed from: ʼ  reason: contains not printable characters */
        public int f2468;

        /* renamed from: ʽ  reason: contains not printable characters */
        public int f2469;

        /* renamed from: ʾ  reason: contains not printable characters */
        public int f2470;

        /* renamed from: ʿ  reason: contains not printable characters */
        public int f2471;

        C0624(C0690.C0720 r1, int i, int i2, int i3, int i4) {
            this.f2467 = r1;
            this.f2468 = i;
            this.f2469 = i2;
            this.f2470 = i3;
            this.f2471 = i4;
        }
    }

    /* renamed from: android.support.v7.widget.ʾʾ$ʻ  reason: contains not printable characters */
    /* compiled from: DefaultItemAnimator */
    private static class C0623 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0690.C0720 f2461;

        /* renamed from: ʼ  reason: contains not printable characters */
        public C0690.C0720 f2462;

        /* renamed from: ʽ  reason: contains not printable characters */
        public int f2463;

        /* renamed from: ʾ  reason: contains not printable characters */
        public int f2464;

        /* renamed from: ʿ  reason: contains not printable characters */
        public int f2465;

        /* renamed from: ˆ  reason: contains not printable characters */
        public int f2466;

        private C0623(C0690.C0720 r1, C0690.C0720 r2) {
            this.f2461 = r1;
            this.f2462 = r2;
        }

        C0623(C0690.C0720 r1, C0690.C0720 r2, int i, int i2, int i3, int i4) {
            this(r1, r2);
            this.f2463 = i;
            this.f2464 = i2;
            this.f2465 = i3;
            this.f2466 = i4;
        }

        public String toString() {
            return "ChangeInfo{oldHolder=" + this.f2461 + ", newHolder=" + this.f2462 + ", fromX=" + this.f2463 + ", fromY=" + this.f2464 + ", toX=" + this.f2465 + ", toY=" + this.f2466 + '}';
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3957() {
        boolean z = !this.f2429.isEmpty();
        boolean z2 = !this.f2431.isEmpty();
        boolean z3 = !this.f2432.isEmpty();
        boolean z4 = !this.f2430.isEmpty();
        if (z || z2 || z4 || z3) {
            Iterator<C0690.C0720> it = this.f2429.iterator();
            while (it.hasNext()) {
                m3955(it.next());
            }
            this.f2429.clear();
            if (z2) {
                final ArrayList arrayList = new ArrayList();
                arrayList.addAll(this.f2431);
                this.f2423.add(arrayList);
                this.f2431.clear();
                AnonymousClass1 r6 = new Runnable() {
                    public void run() {
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            C0624 r1 = (C0624) it.next();
                            C0622.this.m3964(r1.f2467, r1.f2468, r1.f2469, r1.f2470, r1.f2471);
                        }
                        arrayList.clear();
                        C0622.this.f2423.remove(arrayList);
                    }
                };
                if (z) {
                    C0414.m2227(((C0624) arrayList.get(0)).f2467.f2914, r6, m4517());
                } else {
                    r6.run();
                }
            }
            if (z3) {
                final ArrayList arrayList2 = new ArrayList();
                arrayList2.addAll(this.f2432);
                this.f2424.add(arrayList2);
                this.f2432.clear();
                AnonymousClass2 r62 = new Runnable() {
                    public void run() {
                        Iterator it = arrayList2.iterator();
                        while (it.hasNext()) {
                            C0622.this.m3958((C0623) it.next());
                        }
                        arrayList2.clear();
                        C0622.this.f2424.remove(arrayList2);
                    }
                };
                if (z) {
                    C0414.m2227(((C0623) arrayList2.get(0)).f2461.f2914, r62, m4517());
                } else {
                    r62.run();
                }
            }
            if (z4) {
                final ArrayList arrayList3 = new ArrayList();
                arrayList3.addAll(this.f2430);
                this.f2422.add(arrayList3);
                this.f2430.clear();
                AnonymousClass3 r5 = new Runnable() {
                    public void run() {
                        Iterator it = arrayList3.iterator();
                        while (it.hasNext()) {
                            C0622.this.m3968((C0690.C0720) it.next());
                        }
                        arrayList3.clear();
                        C0622.this.f2422.remove(arrayList3);
                    }
                };
                if (z || z2 || z3) {
                    long j = 0;
                    long r8 = z ? m4517() : 0;
                    long r0 = z2 ? m4514() : 0;
                    if (z3) {
                        j = m4519();
                    }
                    C0414.m2227(((C0690.C0720) arrayList3.get(0)).f2914, r5, r8 + Math.max(r0, j));
                    return;
                }
                r5.run();
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3960(C0690.C0720 r2) {
        m3956(r2);
        this.f2429.add(r2);
        return true;
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    private void m3955(final C0690.C0720 r5) {
        final View view = r5.f2914;
        final ViewPropertyAnimator animate = view.animate();
        this.f2427.add(r5);
        animate.setDuration(m4517()).alpha(0.0f).setListener(new AnimatorListenerAdapter() {
            public void onAnimationStart(Animator animator) {
                C0622.this.m3696(r5);
            }

            public void onAnimationEnd(Animator animator) {
                animate.setListener(null);
                view.setAlpha(1.0f);
                C0622.this.m3693(r5);
                C0622.this.f2427.remove(r5);
                C0622.this.m3967();
            }
        }).start();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m3966(C0690.C0720 r3) {
        m3956(r3);
        r3.f2914.setAlpha(0.0f);
        this.f2430.add(r3);
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3968(final C0690.C0720 r6) {
        final View view = r6.f2914;
        final ViewPropertyAnimator animate = view.animate();
        this.f2425.add(r6);
        animate.alpha(1.0f).setDuration(m4515()).setListener(new AnimatorListenerAdapter() {
            public void onAnimationStart(Animator animator) {
                C0622.this.m3698(r6);
            }

            public void onAnimationCancel(Animator animator) {
                view.setAlpha(1.0f);
            }

            public void onAnimationEnd(Animator animator) {
                animate.setListener(null);
                C0622.this.m3695(r6);
                C0622.this.f2425.remove(r6);
                C0622.this.m3967();
            }
        }).start();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3961(C0690.C0720 r9, int i, int i2, int i3, int i4) {
        View view = r9.f2914;
        int translationX = i + ((int) r9.f2914.getTranslationX());
        int translationY = i2 + ((int) r9.f2914.getTranslationY());
        m3956(r9);
        int i5 = i3 - translationX;
        int i6 = i4 - translationY;
        if (i5 == 0 && i6 == 0) {
            m3694(r9);
            return false;
        }
        if (i5 != 0) {
            view.setTranslationX((float) (-i5));
        }
        if (i6 != 0) {
            view.setTranslationY((float) (-i6));
        }
        this.f2431.add(new C0624(r9, translationX, translationY, i3, i4));
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3964(C0690.C0720 r8, int i, int i2, int i3, int i4) {
        final View view = r8.f2914;
        final int i5 = i3 - i;
        final int i6 = i4 - i2;
        if (i5 != 0) {
            view.animate().translationX(0.0f);
        }
        if (i6 != 0) {
            view.animate().translationY(0.0f);
        }
        final ViewPropertyAnimator animate = view.animate();
        this.f2426.add(r8);
        final C0690.C0720 r2 = r8;
        animate.setDuration(m4514()).setListener(new AnimatorListenerAdapter() {
            public void onAnimationStart(Animator animator) {
                C0622.this.m3697(r2);
            }

            public void onAnimationCancel(Animator animator) {
                if (i5 != 0) {
                    view.setTranslationX(0.0f);
                }
                if (i6 != 0) {
                    view.setTranslationY(0.0f);
                }
            }

            public void onAnimationEnd(Animator animator) {
                animate.setListener(null);
                C0622.this.m3694(r2);
                C0622.this.f2426.remove(r2);
                C0622.this.m3967();
            }
        }).start();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3962(C0690.C0720 r10, C0690.C0720 r11, int i, int i2, int i3, int i4) {
        if (r10 == r11) {
            return m3961(r10, i, i2, i3, i4);
        }
        float translationX = r10.f2914.getTranslationX();
        float translationY = r10.f2914.getTranslationY();
        float alpha = r10.f2914.getAlpha();
        m3956(r10);
        int i5 = (int) (((float) (i3 - i)) - translationX);
        int i6 = (int) (((float) (i4 - i2)) - translationY);
        r10.f2914.setTranslationX(translationX);
        r10.f2914.setTranslationY(translationY);
        r10.f2914.setAlpha(alpha);
        if (r11 != null) {
            m3956(r11);
            r11.f2914.setTranslationX((float) (-i5));
            r11.f2914.setTranslationY((float) (-i6));
            r11.f2914.setAlpha(0.0f);
        }
        this.f2432.add(new C0623(r10, r11, i, i2, i3, i4));
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3958(final C0623 r7) {
        final View view;
        C0690.C0720 r0 = r7.f2461;
        final View view2 = null;
        if (r0 == null) {
            view = null;
        } else {
            view = r0.f2914;
        }
        C0690.C0720 r2 = r7.f2462;
        if (r2 != null) {
            view2 = r2.f2914;
        }
        if (view != null) {
            final ViewPropertyAnimator duration = view.animate().setDuration(m4519());
            this.f2428.add(r7.f2461);
            duration.translationX((float) (r7.f2465 - r7.f2463));
            duration.translationY((float) (r7.f2466 - r7.f2464));
            duration.alpha(0.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationStart(Animator animator) {
                    C0622.this.m3686(r7.f2461, true);
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: android.support.v7.widget.ʻʿ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):void
                 arg types: [android.support.v7.widget.ﹳﹳ$ﹳ, int]
                 candidates:
                  android.support.v7.widget.ʾʾ.ʻ(java.util.List<android.support.v7.widget.ʾʾ$ʻ>, android.support.v7.widget.ﹳﹳ$ﹳ):void
                  android.support.v7.widget.ʾʾ.ʻ(android.support.v7.widget.ʾʾ$ʻ, android.support.v7.widget.ﹳﹳ$ﹳ):boolean
                  android.support.v7.widget.ʾʾ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, java.util.List<java.lang.Object>):boolean
                  android.support.v7.widget.ﹳﹳ.ʿ.ʻ(android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ﹳ):android.support.v7.widget.ﹳﹳ$ʿ$ʽ
                  android.support.v7.widget.ﹳﹳ.ʿ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, java.util.List<java.lang.Object>):boolean
                  android.support.v7.widget.ʻʿ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):void */
                public void onAnimationEnd(Animator animator) {
                    duration.setListener(null);
                    view.setAlpha(1.0f);
                    view.setTranslationX(0.0f);
                    view.setTranslationY(0.0f);
                    C0622.this.m3680(r7.f2461, true);
                    C0622.this.f2428.remove(r7.f2461);
                    C0622.this.m3967();
                }
            }).start();
        }
        if (view2 != null) {
            final ViewPropertyAnimator animate = view2.animate();
            this.f2428.add(r7.f2462);
            animate.translationX(0.0f).translationY(0.0f).setDuration(m4519()).alpha(1.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationStart(Animator animator) {
                    C0622.this.m3686(r7.f2462, false);
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: android.support.v7.widget.ʻʿ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):void
                 arg types: [android.support.v7.widget.ﹳﹳ$ﹳ, int]
                 candidates:
                  android.support.v7.widget.ʾʾ.ʻ(java.util.List<android.support.v7.widget.ʾʾ$ʻ>, android.support.v7.widget.ﹳﹳ$ﹳ):void
                  android.support.v7.widget.ʾʾ.ʻ(android.support.v7.widget.ʾʾ$ʻ, android.support.v7.widget.ﹳﹳ$ﹳ):boolean
                  android.support.v7.widget.ʾʾ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, java.util.List<java.lang.Object>):boolean
                  android.support.v7.widget.ﹳﹳ.ʿ.ʻ(android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ﹳ):android.support.v7.widget.ﹳﹳ$ʿ$ʽ
                  android.support.v7.widget.ﹳﹳ.ʿ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, java.util.List<java.lang.Object>):boolean
                  android.support.v7.widget.ʻʿ.ʻ(android.support.v7.widget.ﹳﹳ$ﹳ, boolean):void */
                public void onAnimationEnd(Animator animator) {
                    animate.setListener(null);
                    view2.setAlpha(1.0f);
                    view2.setTranslationX(0.0f);
                    view2.setTranslationY(0.0f);
                    C0622.this.m3680(r7.f2462, false);
                    C0622.this.f2428.remove(r7.f2462);
                    C0622.this.m3967();
                }
            }).start();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3952(List<C0623> list, C0690.C0720 r5) {
        for (int size = list.size() - 1; size >= 0; size--) {
            C0623 r1 = list.get(size);
            if (m3953(r1, r5) && r1.f2461 == null && r1.f2462 == null) {
                list.remove(r1);
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m3954(C0623 r2) {
        if (r2.f2461 != null) {
            m3953(r2, r2.f2461);
        }
        if (r2.f2462 != null) {
            m3953(r2, r2.f2462);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m3953(C0623 r5, C0690.C0720 r6) {
        boolean z = false;
        if (r5.f2462 == r6) {
            r5.f2462 = null;
        } else if (r5.f2461 != r6) {
            return false;
        } else {
            r5.f2461 = null;
            z = true;
        }
        r6.f2914.setAlpha(1.0f);
        r6.f2914.setTranslationX(0.0f);
        r6.f2914.setTranslationY(0.0f);
        m3680(r6, z);
        return true;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m3970(C0690.C0720 r8) {
        View view = r8.f2914;
        view.animate().cancel();
        int size = this.f2431.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            } else if (this.f2431.get(size).f2467 == r8) {
                view.setTranslationY(0.0f);
                view.setTranslationX(0.0f);
                m3694(r8);
                this.f2431.remove(size);
            }
        }
        m3952(this.f2432, r8);
        if (this.f2429.remove(r8)) {
            view.setAlpha(1.0f);
            m3693(r8);
        }
        if (this.f2430.remove(r8)) {
            view.setAlpha(1.0f);
            m3695(r8);
        }
        for (int size2 = this.f2424.size() - 1; size2 >= 0; size2--) {
            ArrayList arrayList = this.f2424.get(size2);
            m3952(arrayList, r8);
            if (arrayList.isEmpty()) {
                this.f2424.remove(size2);
            }
        }
        for (int size3 = this.f2423.size() - 1; size3 >= 0; size3--) {
            ArrayList arrayList2 = this.f2423.get(size3);
            int size4 = arrayList2.size() - 1;
            while (true) {
                if (size4 < 0) {
                    break;
                } else if (((C0624) arrayList2.get(size4)).f2467 == r8) {
                    view.setTranslationY(0.0f);
                    view.setTranslationX(0.0f);
                    m3694(r8);
                    arrayList2.remove(size4);
                    if (arrayList2.isEmpty()) {
                        this.f2423.remove(size3);
                    }
                } else {
                    size4--;
                }
            }
        }
        for (int size5 = this.f2422.size() - 1; size5 >= 0; size5--) {
            ArrayList arrayList3 = this.f2422.get(size5);
            if (arrayList3.remove(r8)) {
                view.setAlpha(1.0f);
                m3695(r8);
                if (arrayList3.isEmpty()) {
                    this.f2422.remove(size5);
                }
            }
        }
        this.f2427.remove(r8);
        this.f2425.remove(r8);
        this.f2428.remove(r8);
        this.f2426.remove(r8);
        m3967();
    }

    /* renamed from: ⁱ  reason: contains not printable characters */
    private void m3956(C0690.C0720 r3) {
        if (f2421 == null) {
            f2421 = new ValueAnimator().getInterpolator();
        }
        r3.f2914.animate().setInterpolator(f2421);
        m3970(r3);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m3965() {
        return !this.f2430.isEmpty() || !this.f2432.isEmpty() || !this.f2431.isEmpty() || !this.f2429.isEmpty() || !this.f2426.isEmpty() || !this.f2427.isEmpty() || !this.f2425.isEmpty() || !this.f2428.isEmpty() || !this.f2423.isEmpty() || !this.f2422.isEmpty() || !this.f2424.isEmpty();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3967() {
        if (!m3965()) {
            m4521();
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m3969() {
        int size = this.f2431.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            }
            C0624 r2 = this.f2431.get(size);
            View view = r2.f2467.f2914;
            view.setTranslationY(0.0f);
            view.setTranslationX(0.0f);
            m3694(r2.f2467);
            this.f2431.remove(size);
        }
        for (int size2 = this.f2429.size() - 1; size2 >= 0; size2--) {
            m3693(this.f2429.get(size2));
            this.f2429.remove(size2);
        }
        int size3 = this.f2430.size();
        while (true) {
            size3--;
            if (size3 < 0) {
                break;
            }
            C0690.C0720 r3 = this.f2430.get(size3);
            r3.f2914.setAlpha(1.0f);
            m3695(r3);
            this.f2430.remove(size3);
        }
        for (int size4 = this.f2432.size() - 1; size4 >= 0; size4--) {
            m3954(this.f2432.get(size4));
        }
        this.f2432.clear();
        if (m3965()) {
            for (int size5 = this.f2423.size() - 1; size5 >= 0; size5--) {
                ArrayList arrayList = this.f2423.get(size5);
                for (int size6 = arrayList.size() - 1; size6 >= 0; size6--) {
                    C0624 r5 = (C0624) arrayList.get(size6);
                    View view2 = r5.f2467.f2914;
                    view2.setTranslationY(0.0f);
                    view2.setTranslationX(0.0f);
                    m3694(r5.f2467);
                    arrayList.remove(size6);
                    if (arrayList.isEmpty()) {
                        this.f2423.remove(arrayList);
                    }
                }
            }
            for (int size7 = this.f2422.size() - 1; size7 >= 0; size7--) {
                ArrayList arrayList2 = this.f2422.get(size7);
                for (int size8 = arrayList2.size() - 1; size8 >= 0; size8--) {
                    C0690.C0720 r4 = (C0690.C0720) arrayList2.get(size8);
                    r4.f2914.setAlpha(1.0f);
                    m3695(r4);
                    arrayList2.remove(size8);
                    if (arrayList2.isEmpty()) {
                        this.f2422.remove(arrayList2);
                    }
                }
            }
            for (int size9 = this.f2424.size() - 1; size9 >= 0; size9--) {
                ArrayList arrayList3 = this.f2424.get(size9);
                for (int size10 = arrayList3.size() - 1; size10 >= 0; size10--) {
                    m3954((C0623) arrayList3.get(size10));
                    if (arrayList3.isEmpty()) {
                        this.f2424.remove(arrayList3);
                    }
                }
            }
            m3959(this.f2427);
            m3959(this.f2426);
            m3959(this.f2425);
            m3959(this.f2428);
            m4521();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3959(List<C0690.C0720> list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            list.get(size).f2914.animate().cancel();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3963(C0690.C0720 r2, List<Object> list) {
        return !list.isEmpty() || super.m4508(r2, list);
    }
}
