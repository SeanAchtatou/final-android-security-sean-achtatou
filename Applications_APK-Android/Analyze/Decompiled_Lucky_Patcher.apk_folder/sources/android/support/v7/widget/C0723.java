package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.ˉ.C0413;
import android.support.v4.ˉ.C0414;
import android.support.v7.ʻ.C0727;
import android.support.v7.ʼ.ʻ.C0739;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.ThemedSpinnerAdapter;

/* renamed from: android.support.v7.widget.ﾞ  reason: contains not printable characters */
/* compiled from: AppCompatSpinner */
public class C0723 extends Spinner implements C0413 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final int[] f2939 = {16843505};

    /* renamed from: ʼ  reason: contains not printable characters */
    private final C0639 f2940;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Context f2941;

    /* renamed from: ʾ  reason: contains not printable characters */
    private C0648 f2942;

    /* renamed from: ʿ  reason: contains not printable characters */
    private SpinnerAdapter f2943;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final boolean f2944;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public C0725 f2945;
    /* access modifiers changed from: private */

    /* renamed from: ˉ  reason: contains not printable characters */
    public int f2946;
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public final Rect f2947;

    public C0723(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0727.C0728.spinnerStyle);
    }

    public C0723(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, -1);
    }

    public C0723(Context context, AttributeSet attributeSet, int i, int i2) {
        this(context, attributeSet, i, i2, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005c, code lost:
        if (r12 != null) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005e, code lost:
        r12.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0070, code lost:
        if (r12 != null) goto L_0x005e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0076  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C0723(android.content.Context r8, android.util.AttributeSet r9, int r10, int r11, android.content.res.Resources.Theme r12) {
        /*
            r7 = this;
            r7.<init>(r8, r9, r10)
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>()
            r7.f2947 = r0
            int[] r0 = android.support.v7.ʻ.C0727.C0737.Spinner
            r1 = 0
            android.support.v7.widget.ʻˏ r0 = android.support.v7.widget.C0592.m3740(r8, r9, r0, r10, r1)
            android.support.v7.widget.ˉ r2 = new android.support.v7.widget.ˉ
            r2.<init>(r7)
            r7.f2940 = r2
            r2 = 0
            if (r12 == 0) goto L_0x0023
            android.support.v7.view.ʾ r3 = new android.support.v7.view.ʾ
            r3.<init>(r8, r12)
            r7.f2941 = r3
            goto L_0x003e
        L_0x0023:
            int r12 = android.support.v7.ʻ.C0727.C0737.Spinner_popupTheme
            int r12 = r0.m3757(r12, r1)
            if (r12 == 0) goto L_0x0033
            android.support.v7.view.ʾ r3 = new android.support.v7.view.ʾ
            r3.<init>(r8, r12)
            r7.f2941 = r3
            goto L_0x003e
        L_0x0033:
            int r12 = android.os.Build.VERSION.SDK_INT
            r3 = 23
            if (r12 >= r3) goto L_0x003b
            r12 = r8
            goto L_0x003c
        L_0x003b:
            r12 = r2
        L_0x003c:
            r7.f2941 = r12
        L_0x003e:
            android.content.Context r12 = r7.f2941
            r3 = 1
            if (r12 == 0) goto L_0x00b3
            r12 = -1
            if (r11 != r12) goto L_0x007b
            int r12 = android.os.Build.VERSION.SDK_INT
            r4 = 11
            if (r12 < r4) goto L_0x007a
            int[] r12 = android.support.v7.widget.C0723.f2939     // Catch:{ Exception -> 0x0067, all -> 0x0064 }
            android.content.res.TypedArray r12 = r8.obtainStyledAttributes(r9, r12, r10, r1)     // Catch:{ Exception -> 0x0067, all -> 0x0064 }
            boolean r4 = r12.hasValue(r1)     // Catch:{ Exception -> 0x0062 }
            if (r4 == 0) goto L_0x005c
            int r11 = r12.getInt(r1, r1)     // Catch:{ Exception -> 0x0062 }
        L_0x005c:
            if (r12 == 0) goto L_0x007b
        L_0x005e:
            r12.recycle()
            goto L_0x007b
        L_0x0062:
            r4 = move-exception
            goto L_0x0069
        L_0x0064:
            r8 = move-exception
            r12 = r2
            goto L_0x0074
        L_0x0067:
            r4 = move-exception
            r12 = r2
        L_0x0069:
            java.lang.String r5 = "AppCompatSpinner"
            java.lang.String r6 = "Could not read android:spinnerMode"
            android.util.Log.i(r5, r6, r4)     // Catch:{ all -> 0x0073 }
            if (r12 == 0) goto L_0x007b
            goto L_0x005e
        L_0x0073:
            r8 = move-exception
        L_0x0074:
            if (r12 == 0) goto L_0x0079
            r12.recycle()
        L_0x0079:
            throw r8
        L_0x007a:
            r11 = 1
        L_0x007b:
            if (r11 != r3) goto L_0x00b3
            android.support.v7.widget.ﾞ$ʼ r11 = new android.support.v7.widget.ﾞ$ʼ
            android.content.Context r12 = r7.f2941
            r11.<init>(r12, r9, r10)
            android.content.Context r12 = r7.f2941
            int[] r4 = android.support.v7.ʻ.C0727.C0737.Spinner
            android.support.v7.widget.ʻˏ r12 = android.support.v7.widget.C0592.m3740(r12, r9, r4, r10, r1)
            int r1 = android.support.v7.ʻ.C0727.C0737.Spinner_android_dropDownWidth
            r4 = -2
            int r1 = r12.m3755(r1, r4)
            r7.f2946 = r1
            int r1 = android.support.v7.ʻ.C0727.C0737.Spinner_android_popupBackground
            android.graphics.drawable.Drawable r1 = r12.m3744(r1)
            r11.m4201(r1)
            int r1 = android.support.v7.ʻ.C0727.C0737.Spinner_android_prompt
            java.lang.String r1 = r0.m3752(r1)
            r11.m4850(r1)
            r12.m3745()
            r7.f2945 = r11
            android.support.v7.widget.ﾞ$1 r12 = new android.support.v7.widget.ﾞ$1
            r12.<init>(r7, r11)
            r7.f2942 = r12
        L_0x00b3:
            int r11 = android.support.v7.ʻ.C0727.C0737.Spinner_android_entries
            java.lang.CharSequence[] r11 = r0.m3756(r11)
            if (r11 == 0) goto L_0x00cb
            android.widget.ArrayAdapter r12 = new android.widget.ArrayAdapter
            r1 = 17367048(0x1090008, float:2.5162948E-38)
            r12.<init>(r8, r1, r11)
            int r8 = android.support.v7.ʻ.C0727.C0734.support_simple_spinner_dropdown_item
            r12.setDropDownViewResource(r8)
            r7.setAdapter(r12)
        L_0x00cb:
            r0.m3745()
            r7.f2944 = r3
            android.widget.SpinnerAdapter r8 = r7.f2943
            if (r8 == 0) goto L_0x00d9
            r7.setAdapter(r8)
            r7.f2943 = r2
        L_0x00d9:
            android.support.v7.widget.ˉ r8 = r7.f2940
            r8.m4065(r9, r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0723.<init>(android.content.Context, android.util.AttributeSet, int, int, android.content.res.Resources$Theme):void");
    }

    public Context getPopupContext() {
        if (this.f2945 != null) {
            return this.f2941;
        }
        if (Build.VERSION.SDK_INT >= 23) {
            return super.getPopupContext();
        }
        return null;
    }

    public void setPopupBackgroundDrawable(Drawable drawable) {
        C0725 r0 = this.f2945;
        if (r0 != null) {
            r0.m4201(drawable);
        } else if (Build.VERSION.SDK_INT >= 16) {
            super.setPopupBackgroundDrawable(drawable);
        }
    }

    public void setPopupBackgroundResource(int i) {
        setPopupBackgroundDrawable(C0739.m4883(getPopupContext(), i));
    }

    public Drawable getPopupBackground() {
        C0725 r0 = this.f2945;
        if (r0 != null) {
            return r0.m4219();
        }
        if (Build.VERSION.SDK_INT >= 16) {
            return super.getPopupBackground();
        }
        return null;
    }

    public void setDropDownVerticalOffset(int i) {
        C0725 r0 = this.f2945;
        if (r0 != null) {
            r0.m4212(i);
        } else if (Build.VERSION.SDK_INT >= 16) {
            super.setDropDownVerticalOffset(i);
        }
    }

    public int getDropDownVerticalOffset() {
        C0725 r0 = this.f2945;
        if (r0 != null) {
            return r0.m4224();
        }
        if (Build.VERSION.SDK_INT >= 16) {
            return super.getDropDownVerticalOffset();
        }
        return 0;
    }

    public void setDropDownHorizontalOffset(int i) {
        C0725 r0 = this.f2945;
        if (r0 != null) {
            r0.m4209(i);
        } else if (Build.VERSION.SDK_INT >= 16) {
            super.setDropDownHorizontalOffset(i);
        }
    }

    public int getDropDownHorizontalOffset() {
        C0725 r0 = this.f2945;
        if (r0 != null) {
            return r0.m4223();
        }
        if (Build.VERSION.SDK_INT >= 16) {
            return super.getDropDownHorizontalOffset();
        }
        return 0;
    }

    public void setDropDownWidth(int i) {
        if (this.f2945 != null) {
            this.f2946 = i;
        } else if (Build.VERSION.SDK_INT >= 16) {
            super.setDropDownWidth(i);
        }
    }

    public int getDropDownWidth() {
        if (this.f2945 != null) {
            return this.f2946;
        }
        if (Build.VERSION.SDK_INT >= 16) {
            return super.getDropDownWidth();
        }
        return 0;
    }

    public void setAdapter(SpinnerAdapter spinnerAdapter) {
        if (!this.f2944) {
            this.f2943 = spinnerAdapter;
            return;
        }
        super.setAdapter(spinnerAdapter);
        if (this.f2945 != null) {
            Context context = this.f2941;
            if (context == null) {
                context = getContext();
            }
            this.f2945.m4849(new C0724(spinnerAdapter, context.getTheme()));
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        C0725 r0 = this.f2945;
        if (r0 != null && r0.m4216()) {
            this.f2945.m4213();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        C0648 r0 = this.f2942;
        if (r0 == null || !r0.onTouch(this, motionEvent)) {
            return super.onTouchEvent(motionEvent);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.f2945 != null && View.MeasureSpec.getMode(i) == Integer.MIN_VALUE) {
            setMeasuredDimension(Math.min(Math.max(getMeasuredWidth(), m4844(getAdapter(), getBackground())), View.MeasureSpec.getSize(i)), getMeasuredHeight());
        }
    }

    public boolean performClick() {
        C0725 r0 = this.f2945;
        if (r0 == null) {
            return super.performClick();
        }
        if (r0.m4216()) {
            return true;
        }
        this.f2945.m4853();
        return true;
    }

    public void setPrompt(CharSequence charSequence) {
        C0725 r0 = this.f2945;
        if (r0 != null) {
            r0.m4850(charSequence);
        } else {
            super.setPrompt(charSequence);
        }
    }

    public CharSequence getPrompt() {
        C0725 r0 = this.f2945;
        return r0 != null ? r0.m4848() : super.getPrompt();
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        C0639 r0 = this.f2940;
        if (r0 != null) {
            r0.m4061(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        C0639 r0 = this.f2940;
        if (r0 != null) {
            r0.m4064(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        C0639 r0 = this.f2940;
        if (r0 != null) {
            r0.m4062(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        C0639 r0 = this.f2940;
        if (r0 != null) {
            return r0.m4060();
        }
        return null;
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        C0639 r0 = this.f2940;
        if (r0 != null) {
            r0.m4063(mode);
        }
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        C0639 r0 = this.f2940;
        if (r0 != null) {
            return r0.m4066();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        C0639 r0 = this.f2940;
        if (r0 != null) {
            r0.m4068();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m4844(SpinnerAdapter spinnerAdapter, Drawable drawable) {
        int i = 0;
        if (spinnerAdapter == null) {
            return 0;
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 0);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 0);
        int max = Math.max(0, getSelectedItemPosition());
        int min = Math.min(spinnerAdapter.getCount(), max + 15);
        View view = null;
        int i2 = 0;
        for (int max2 = Math.max(0, max - (15 - (min - max))); max2 < min; max2++) {
            int itemViewType = spinnerAdapter.getItemViewType(max2);
            if (itemViewType != i) {
                view = null;
                i = itemViewType;
            }
            view = spinnerAdapter.getView(max2, view, this);
            if (view.getLayoutParams() == null) {
                view.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
            }
            view.measure(makeMeasureSpec, makeMeasureSpec2);
            i2 = Math.max(i2, view.getMeasuredWidth());
        }
        if (drawable == null) {
            return i2;
        }
        drawable.getPadding(this.f2947);
        return i2 + this.f2947.left + this.f2947.right;
    }

    /* renamed from: android.support.v7.widget.ﾞ$ʻ  reason: contains not printable characters */
    /* compiled from: AppCompatSpinner */
    private static class C0724 implements ListAdapter, SpinnerAdapter {

        /* renamed from: ʻ  reason: contains not printable characters */
        private SpinnerAdapter f2950;

        /* renamed from: ʼ  reason: contains not printable characters */
        private ListAdapter f2951;

        public int getItemViewType(int i) {
            return 0;
        }

        public int getViewTypeCount() {
            return 1;
        }

        public C0724(SpinnerAdapter spinnerAdapter, Resources.Theme theme) {
            this.f2950 = spinnerAdapter;
            if (spinnerAdapter instanceof ListAdapter) {
                this.f2951 = (ListAdapter) spinnerAdapter;
            }
            if (theme == null) {
                return;
            }
            if (Build.VERSION.SDK_INT >= 23 && (spinnerAdapter instanceof ThemedSpinnerAdapter)) {
                ThemedSpinnerAdapter themedSpinnerAdapter = (ThemedSpinnerAdapter) spinnerAdapter;
                if (themedSpinnerAdapter.getDropDownViewTheme() != theme) {
                    themedSpinnerAdapter.setDropDownViewTheme(theme);
                }
            } else if (spinnerAdapter instanceof C0588) {
                C0588 r3 = (C0588) spinnerAdapter;
                if (r3.m3733() == null) {
                    r3.m3734(theme);
                }
            }
        }

        public int getCount() {
            SpinnerAdapter spinnerAdapter = this.f2950;
            if (spinnerAdapter == null) {
                return 0;
            }
            return spinnerAdapter.getCount();
        }

        public Object getItem(int i) {
            SpinnerAdapter spinnerAdapter = this.f2950;
            if (spinnerAdapter == null) {
                return null;
            }
            return spinnerAdapter.getItem(i);
        }

        public long getItemId(int i) {
            SpinnerAdapter spinnerAdapter = this.f2950;
            if (spinnerAdapter == null) {
                return -1;
            }
            return spinnerAdapter.getItemId(i);
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            return getDropDownView(i, view, viewGroup);
        }

        public View getDropDownView(int i, View view, ViewGroup viewGroup) {
            SpinnerAdapter spinnerAdapter = this.f2950;
            if (spinnerAdapter == null) {
                return null;
            }
            return spinnerAdapter.getDropDownView(i, view, viewGroup);
        }

        public boolean hasStableIds() {
            SpinnerAdapter spinnerAdapter = this.f2950;
            return spinnerAdapter != null && spinnerAdapter.hasStableIds();
        }

        public void registerDataSetObserver(DataSetObserver dataSetObserver) {
            SpinnerAdapter spinnerAdapter = this.f2950;
            if (spinnerAdapter != null) {
                spinnerAdapter.registerDataSetObserver(dataSetObserver);
            }
        }

        public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
            SpinnerAdapter spinnerAdapter = this.f2950;
            if (spinnerAdapter != null) {
                spinnerAdapter.unregisterDataSetObserver(dataSetObserver);
            }
        }

        public boolean areAllItemsEnabled() {
            ListAdapter listAdapter = this.f2951;
            if (listAdapter != null) {
                return listAdapter.areAllItemsEnabled();
            }
            return true;
        }

        public boolean isEnabled(int i) {
            ListAdapter listAdapter = this.f2951;
            if (listAdapter != null) {
                return listAdapter.isEnabled(i);
            }
            return true;
        }

        public boolean isEmpty() {
            return getCount() == 0;
        }
    }

    /* renamed from: android.support.v7.widget.ﾞ$ʼ  reason: contains not printable characters */
    /* compiled from: AppCompatSpinner */
    private class C0725 extends C0661 {

        /* renamed from: ʻ  reason: contains not printable characters */
        ListAdapter f2952;

        /* renamed from: ˉ  reason: contains not printable characters */
        private CharSequence f2954;

        /* renamed from: ˊ  reason: contains not printable characters */
        private final Rect f2955 = new Rect();

        public C0725(Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet, i);
            m4207(C0723.this);
            m4205(true);
            m4199(0);
            m4202(new AdapterView.OnItemClickListener(C0723.this) {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    C0723.this.setSelection(i);
                    if (C0723.this.getOnItemClickListener() != null) {
                        C0723.this.performItemClick(view, i, C0725.this.f2952.getItemId(i));
                    }
                    C0725.this.m4213();
                }
            });
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4849(ListAdapter listAdapter) {
            super.m4203(listAdapter);
            this.f2952 = listAdapter;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public CharSequence m4848() {
            return this.f2954;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4850(CharSequence charSequence) {
            this.f2954 = charSequence;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4852() {
            int i;
            Drawable r0 = m4219();
            int i2 = 0;
            if (r0 != null) {
                r0.getPadding(C0723.this.f2947);
                if (C0607.m3858(C0723.this)) {
                    i = C0723.this.f2947.right;
                } else {
                    i = -C0723.this.f2947.left;
                }
                i2 = i;
            } else {
                Rect r02 = C0723.this.f2947;
                C0723.this.f2947.right = 0;
                r02.left = 0;
            }
            int paddingLeft = C0723.this.getPaddingLeft();
            int paddingRight = C0723.this.getPaddingRight();
            int width = C0723.this.getWidth();
            if (C0723.this.f2946 == -2) {
                int r4 = C0723.this.m4844((SpinnerAdapter) this.f2952, m4219());
                int i3 = (C0723.this.getContext().getResources().getDisplayMetrics().widthPixels - C0723.this.f2947.left) - C0723.this.f2947.right;
                if (r4 > i3) {
                    r4 = i3;
                }
                m4218(Math.max(r4, (width - paddingLeft) - paddingRight));
            } else if (C0723.this.f2946 == -1) {
                m4218((width - paddingLeft) - paddingRight);
            } else {
                m4218(C0723.this.f2946);
            }
            m4209(C0607.m3858(C0723.this) ? i2 + ((width - paddingRight) - m4225()) : i2 + paddingLeft);
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public void m4853() {
            ViewTreeObserver viewTreeObserver;
            boolean r0 = m4216();
            m4852();
            m4220(2);
            super.m4211();
            m4217().setChoiceMode(1);
            m4222(C0723.this.getSelectedItemPosition());
            if (!r0 && (viewTreeObserver = C0723.this.getViewTreeObserver()) != null) {
                final AnonymousClass2 r1 = new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        C0725 r0 = C0725.this;
                        if (!r0.m4851(C0723.this)) {
                            C0725.this.m4213();
                            return;
                        }
                        C0725.this.m4852();
                        C0725.super.m4211();
                    }
                };
                viewTreeObserver.addOnGlobalLayoutListener(r1);
                m4204(new PopupWindow.OnDismissListener() {
                    public void onDismiss() {
                        ViewTreeObserver viewTreeObserver = C0723.this.getViewTreeObserver();
                        if (viewTreeObserver != null) {
                            viewTreeObserver.removeGlobalOnLayoutListener(r1);
                        }
                    }
                });
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4851(View view) {
            return C0414.m2257(view) && view.getGlobalVisibleRect(this.f2955);
        }
    }
}
