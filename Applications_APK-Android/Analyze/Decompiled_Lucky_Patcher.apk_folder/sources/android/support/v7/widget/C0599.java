package android.support.v7.widget;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.ʻ.C0727;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

/* renamed from: android.support.v7.widget.ʻٴ  reason: contains not printable characters */
/* compiled from: TooltipPopup */
class C0599 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Context f2361;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final View f2362;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final TextView f2363;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final WindowManager.LayoutParams f2364 = new WindowManager.LayoutParams();

    /* renamed from: ʿ  reason: contains not printable characters */
    private final Rect f2365 = new Rect();

    /* renamed from: ˆ  reason: contains not printable characters */
    private final int[] f2366 = new int[2];

    /* renamed from: ˈ  reason: contains not printable characters */
    private final int[] f2367 = new int[2];

    C0599(Context context) {
        this.f2361 = context;
        this.f2362 = LayoutInflater.from(this.f2361).inflate(C0727.C0734.tooltip, (ViewGroup) null);
        this.f2363 = (TextView) this.f2362.findViewById(C0727.C0733.message);
        this.f2364.setTitle(getClass().getSimpleName());
        this.f2364.packageName = this.f2361.getPackageName();
        WindowManager.LayoutParams layoutParams = this.f2364;
        layoutParams.type = 1002;
        layoutParams.width = -2;
        layoutParams.height = -2;
        layoutParams.format = -3;
        layoutParams.windowAnimations = C0727.C0736.Animation_AppCompat_Tooltip;
        this.f2364.flags = 24;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3817(View view, int i, int i2, boolean z, CharSequence charSequence) {
        if (m3818()) {
            m3816();
        }
        this.f2363.setText(charSequence);
        m3815(view, i, i2, z, this.f2364);
        ((WindowManager) this.f2361.getSystemService("window")).addView(this.f2362, this.f2364);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3816() {
        if (m3818()) {
            ((WindowManager) this.f2361.getSystemService("window")).removeView(this.f2362);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m3818() {
        return this.f2362.getParent() != null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3815(View view, int i, int i2, boolean z, WindowManager.LayoutParams layoutParams) {
        int i3;
        int i4;
        int dimensionPixelOffset = this.f2361.getResources().getDimensionPixelOffset(C0727.C0731.tooltip_precise_anchor_threshold);
        if (view.getWidth() < dimensionPixelOffset) {
            i = view.getWidth() / 2;
        }
        if (view.getHeight() >= dimensionPixelOffset) {
            int dimensionPixelOffset2 = this.f2361.getResources().getDimensionPixelOffset(C0727.C0731.tooltip_precise_anchor_extra_offset);
            i4 = i2 + dimensionPixelOffset2;
            i3 = i2 - dimensionPixelOffset2;
        } else {
            i4 = view.getHeight();
            i3 = 0;
        }
        layoutParams.gravity = 49;
        int dimensionPixelOffset3 = this.f2361.getResources().getDimensionPixelOffset(z ? C0727.C0731.tooltip_y_offset_touch : C0727.C0731.tooltip_y_offset_non_touch);
        View r3 = m3814(view);
        if (r3 == null) {
            Log.e("TooltipPopup", "Cannot find app view");
            return;
        }
        r3.getWindowVisibleDisplayFrame(this.f2365);
        if (this.f2365.left < 0 && this.f2365.top < 0) {
            Resources resources = this.f2361.getResources();
            int identifier = resources.getIdentifier("status_bar_height", "dimen", "android");
            int dimensionPixelSize = identifier != 0 ? resources.getDimensionPixelSize(identifier) : 0;
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            this.f2365.set(0, dimensionPixelSize, displayMetrics.widthPixels, displayMetrics.heightPixels);
        }
        r3.getLocationOnScreen(this.f2367);
        view.getLocationOnScreen(this.f2366);
        int[] iArr = this.f2366;
        int i5 = iArr[0];
        int[] iArr2 = this.f2367;
        iArr[0] = i5 - iArr2[0];
        iArr[1] = iArr[1] - iArr2[1];
        layoutParams.x = (iArr[0] + i) - (this.f2365.width() / 2);
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        this.f2362.measure(makeMeasureSpec, makeMeasureSpec);
        int measuredHeight = this.f2362.getMeasuredHeight();
        int[] iArr3 = this.f2366;
        int i6 = ((iArr3[1] + i3) - dimensionPixelOffset3) - measuredHeight;
        int i7 = iArr3[1] + i4 + dimensionPixelOffset3;
        if (z) {
            if (i6 >= 0) {
                layoutParams.y = i6;
            } else {
                layoutParams.y = i7;
            }
        } else if (measuredHeight + i7 <= this.f2365.height()) {
            layoutParams.y = i7;
        } else {
            layoutParams.y = i6;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static View m3814(View view) {
        for (Context context = view.getContext(); context instanceof ContextWrapper; context = ((ContextWrapper) context).getBaseContext()) {
            if (context instanceof Activity) {
                return ((Activity) context).getWindow().getDecorView();
            }
        }
        return view.getRootView();
    }
}
