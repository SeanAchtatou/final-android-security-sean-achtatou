package android.support.v7.widget;

import android.os.SystemClock;
import android.support.v7.view.menu.C0524;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;

/* renamed from: android.support.v7.widget.ˋˋ  reason: contains not printable characters */
/* compiled from: ForwardingListener */
public abstract class C0648 implements View.OnAttachStateChangeListener, View.OnTouchListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final float f2574;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f2575;

    /* renamed from: ʽ  reason: contains not printable characters */
    final View f2576;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final int f2577;

    /* renamed from: ʿ  reason: contains not printable characters */
    private Runnable f2578;

    /* renamed from: ˆ  reason: contains not printable characters */
    private Runnable f2579;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f2580;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f2581;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final int[] f2582 = new int[2];

    public void onViewAttachedToWindow(View view) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract C0524 m4123();

    public C0648(View view) {
        this.f2576 = view;
        view.setLongClickable(true);
        view.addOnAttachStateChangeListener(this);
        this.f2574 = (float) ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
        this.f2575 = ViewConfiguration.getTapTimeout();
        this.f2577 = (this.f2575 + ViewConfiguration.getLongPressTimeout()) / 2;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean z;
        boolean z2 = this.f2580;
        if (z2) {
            z = m4120(motionEvent) || !m4125();
        } else {
            z = m4117(motionEvent) && m4124();
            if (z) {
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                this.f2576.onTouchEvent(obtain);
                obtain.recycle();
            }
        }
        this.f2580 = z;
        if (z || z2) {
            return true;
        }
        return false;
    }

    public void onViewDetachedFromWindow(View view) {
        this.f2580 = false;
        this.f2581 = -1;
        Runnable runnable = this.f2578;
        if (runnable != null) {
            this.f2576.removeCallbacks(runnable);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m4124() {
        C0524 r0 = m4123();
        if (r0 == null || r0.m3037()) {
            return true;
        }
        r0.m3035();
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m4125() {
        C0524 r0 = m4123();
        if (r0 == null || !r0.m3037()) {
            return true;
        }
        r0.m3036();
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0017, code lost:
        if (r1 != 3) goto L_0x006d;
     */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m4117(android.view.MotionEvent r6) {
        /*
            r5 = this;
            android.view.View r0 = r5.f2576
            boolean r1 = r0.isEnabled()
            r2 = 0
            if (r1 != 0) goto L_0x000a
            return r2
        L_0x000a:
            int r1 = r6.getActionMasked()
            if (r1 == 0) goto L_0x0041
            r3 = 1
            if (r1 == r3) goto L_0x003d
            r4 = 2
            if (r1 == r4) goto L_0x001a
            r6 = 3
            if (r1 == r6) goto L_0x003d
            goto L_0x006d
        L_0x001a:
            int r1 = r5.f2581
            int r1 = r6.findPointerIndex(r1)
            if (r1 < 0) goto L_0x006d
            float r4 = r6.getX(r1)
            float r6 = r6.getY(r1)
            float r1 = r5.f2574
            boolean r6 = m4118(r0, r4, r6, r1)
            if (r6 != 0) goto L_0x006d
            r5.m4122()
            android.view.ViewParent r6 = r0.getParent()
            r6.requestDisallowInterceptTouchEvent(r3)
            return r3
        L_0x003d:
            r5.m4122()
            goto L_0x006d
        L_0x0041:
            int r6 = r6.getPointerId(r2)
            r5.f2581 = r6
            java.lang.Runnable r6 = r5.f2578
            if (r6 != 0) goto L_0x0052
            android.support.v7.widget.ˋˋ$ʻ r6 = new android.support.v7.widget.ˋˋ$ʻ
            r6.<init>()
            r5.f2578 = r6
        L_0x0052:
            java.lang.Runnable r6 = r5.f2578
            int r1 = r5.f2575
            long r3 = (long) r1
            r0.postDelayed(r6, r3)
            java.lang.Runnable r6 = r5.f2579
            if (r6 != 0) goto L_0x0065
            android.support.v7.widget.ˋˋ$ʼ r6 = new android.support.v7.widget.ˋˋ$ʼ
            r6.<init>()
            r5.f2579 = r6
        L_0x0065:
            java.lang.Runnable r6 = r5.f2579
            int r1 = r5.f2577
            long r3 = (long) r1
            r0.postDelayed(r6, r3)
        L_0x006d:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0648.m4117(android.view.MotionEvent):boolean");
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m4122() {
        Runnable runnable = this.f2579;
        if (runnable != null) {
            this.f2576.removeCallbacks(runnable);
        }
        Runnable runnable2 = this.f2578;
        if (runnable2 != null) {
            this.f2576.removeCallbacks(runnable2);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m4126() {
        m4122();
        View view = this.f2576;
        if (view.isEnabled() && !view.isLongClickable() && m4124()) {
            view.getParent().requestDisallowInterceptTouchEvent(true);
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
            view.onTouchEvent(obtain);
            obtain.recycle();
            this.f2580 = true;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m4120(MotionEvent motionEvent) {
        C0635 r1;
        View view = this.f2576;
        C0524 r12 = m4123();
        if (r12 == null || !r12.m3037() || (r1 = (C0635) r12.m3038()) == null || !r1.isShown()) {
            return false;
        }
        MotionEvent obtainNoHistory = MotionEvent.obtainNoHistory(motionEvent);
        m4121(view, obtainNoHistory);
        m4119(r1, obtainNoHistory);
        boolean r0 = r1.m4056(obtainNoHistory, this.f2581);
        obtainNoHistory.recycle();
        int actionMasked = motionEvent.getActionMasked();
        return r0 && (actionMasked != 1 && actionMasked != 3);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean m4118(View view, float f, float f2, float f3) {
        float f4 = -f3;
        return f >= f4 && f2 >= f4 && f < ((float) (view.getRight() - view.getLeft())) + f3 && f2 < ((float) (view.getBottom() - view.getTop())) + f3;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m4119(View view, MotionEvent motionEvent) {
        int[] iArr = this.f2582;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) (-iArr[0]), (float) (-iArr[1]));
        return true;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m4121(View view, MotionEvent motionEvent) {
        int[] iArr = this.f2582;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) iArr[0], (float) iArr[1]);
        return true;
    }

    /* renamed from: android.support.v7.widget.ˋˋ$ʻ  reason: contains not printable characters */
    /* compiled from: ForwardingListener */
    private class C0649 implements Runnable {
        C0649() {
        }

        public void run() {
            ViewParent parent = C0648.this.f2576.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }
    }

    /* renamed from: android.support.v7.widget.ˋˋ$ʼ  reason: contains not printable characters */
    /* compiled from: ForwardingListener */
    private class C0650 implements Runnable {
        C0650() {
        }

        public void run() {
            C0648.this.m4126();
        }
    }
}
