package android.support.v7.widget;

/* renamed from: android.support.v7.widget.ʻʼ  reason: contains not printable characters */
/* compiled from: RtlSpacingHelper */
class C0578 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f2256 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f2257 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f2258 = Integer.MIN_VALUE;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f2259 = Integer.MIN_VALUE;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f2260 = 0;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f2261 = 0;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f2262 = false;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f2263 = false;

    C0578() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m3661() {
        return this.f2256;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m3664() {
        return this.f2257;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m3666() {
        return this.f2262 ? this.f2257 : this.f2256;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m3667() {
        return this.f2262 ? this.f2256 : this.f2257;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3662(int i, int i2) {
        this.f2258 = i;
        this.f2259 = i2;
        this.f2263 = true;
        if (this.f2262) {
            if (i2 != Integer.MIN_VALUE) {
                this.f2256 = i2;
            }
            if (i != Integer.MIN_VALUE) {
                this.f2257 = i;
                return;
            }
            return;
        }
        if (i != Integer.MIN_VALUE) {
            this.f2256 = i;
        }
        if (i2 != Integer.MIN_VALUE) {
            this.f2257 = i2;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3665(int i, int i2) {
        this.f2263 = false;
        if (i != Integer.MIN_VALUE) {
            this.f2260 = i;
            this.f2256 = i;
        }
        if (i2 != Integer.MIN_VALUE) {
            this.f2261 = i2;
            this.f2257 = i2;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3663(boolean z) {
        if (z != this.f2262) {
            this.f2262 = z;
            if (!this.f2263) {
                this.f2256 = this.f2260;
                this.f2257 = this.f2261;
            } else if (z) {
                int i = this.f2259;
                if (i == Integer.MIN_VALUE) {
                    i = this.f2260;
                }
                this.f2256 = i;
                int i2 = this.f2258;
                if (i2 == Integer.MIN_VALUE) {
                    i2 = this.f2261;
                }
                this.f2257 = i2;
            } else {
                int i3 = this.f2258;
                if (i3 == Integer.MIN_VALUE) {
                    i3 = this.f2260;
                }
                this.f2256 = i3;
                int i4 = this.f2259;
                if (i4 == Integer.MIN_VALUE) {
                    i4 = this.f2261;
                }
                this.f2257 = i4;
            }
        }
    }
}
