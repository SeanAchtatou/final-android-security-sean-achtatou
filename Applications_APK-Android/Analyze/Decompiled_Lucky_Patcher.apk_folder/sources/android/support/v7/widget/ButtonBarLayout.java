package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.ˉ.C0414;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

public class ButtonBarLayout extends LinearLayout {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f2001;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f2002 = -1;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f2003;

    public ButtonBarLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        boolean z = false;
        this.f2003 = 0;
        z = getResources().getConfiguration().screenHeightDp >= 320 ? true : z;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0727.C0737.ButtonBarLayout);
        this.f2001 = obtainStyledAttributes.getBoolean(C0727.C0737.ButtonBarLayout_allowStacking, z);
        obtainStyledAttributes.recycle();
    }

    public void setAllowStacking(boolean z) {
        if (this.f2001 != z) {
            this.f2001 = z;
            if (!this.f2001 && getOrientation() == 1) {
                setStacked(false);
            }
            requestLayout();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        boolean z;
        int i3;
        int size = View.MeasureSpec.getSize(i);
        int i4 = 0;
        if (this.f2001) {
            if (size > this.f2002 && m3235()) {
                setStacked(false);
            }
            this.f2002 = size;
        }
        if (m3235() || View.MeasureSpec.getMode(i) != 1073741824) {
            i3 = i;
            z = false;
        } else {
            i3 = View.MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE);
            z = true;
        }
        super.onMeasure(i3, i2);
        if (this.f2001 && !m3235()) {
            if ((getMeasuredWidthAndState() & -16777216) == 16777216) {
                setStacked(true);
                z = true;
            }
        }
        if (z) {
            super.onMeasure(i, i2);
        }
        int r6 = m3234(0);
        if (r6 >= 0) {
            View childAt = getChildAt(r6);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) childAt.getLayoutParams();
            int paddingTop = getPaddingTop() + childAt.getMeasuredHeight() + layoutParams.topMargin + layoutParams.bottomMargin + 0;
            if (m3235()) {
                int r62 = m3234(r6 + 1);
                if (r62 >= 0) {
                    paddingTop += getChildAt(r62).getPaddingTop() + ((int) (getResources().getDisplayMetrics().density * 16.0f));
                }
                i4 = paddingTop;
            } else {
                i4 = paddingTop + getPaddingBottom();
            }
        }
        if (C0414.m2241(this) != i4) {
            setMinimumHeight(i4);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m3234(int i) {
        int childCount = getChildCount();
        while (i < childCount) {
            if (getChildAt(i).getVisibility() == 0) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public int getMinimumHeight() {
        return Math.max(this.f2003, super.getMinimumHeight());
    }

    private void setStacked(boolean z) {
        setOrientation(z ? 1 : 0);
        setGravity(z ? 5 : 80);
        View findViewById = findViewById(C0727.C0733.spacer);
        if (findViewById != null) {
            findViewById.setVisibility(z ? 8 : 4);
        }
        for (int childCount = getChildCount() - 2; childCount >= 0; childCount--) {
            bringChildToFront(getChildAt(childCount));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m3235() {
        return getOrientation() == 1;
    }
}
