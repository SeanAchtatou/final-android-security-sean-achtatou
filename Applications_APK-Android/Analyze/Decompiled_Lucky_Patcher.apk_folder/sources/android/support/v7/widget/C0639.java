package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.ˉ.C0414;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.view.View;

/* renamed from: android.support.v7.widget.ˉ  reason: contains not printable characters */
/* compiled from: AppCompatBackgroundHelper */
class C0639 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final View f2514;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final C0656 f2515;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f2516 = -1;

    /* renamed from: ʾ  reason: contains not printable characters */
    private C0590 f2517;

    /* renamed from: ʿ  reason: contains not printable characters */
    private C0590 f2518;

    /* renamed from: ˆ  reason: contains not printable characters */
    private C0590 f2519;

    C0639(View view) {
        this.f2514 = view;
        this.f2515 = C0656.m4164();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4065(AttributeSet attributeSet, int i) {
        C0592 r4 = C0592.m3740(this.f2514.getContext(), attributeSet, C0727.C0737.ViewBackgroundHelper, i, 0);
        try {
            if (r4.m3758(C0727.C0737.ViewBackgroundHelper_android_background)) {
                this.f2516 = r4.m3757(C0727.C0737.ViewBackgroundHelper_android_background, -1);
                ColorStateList r5 = this.f2515.m4187(this.f2514.getContext(), this.f2516);
                if (r5 != null) {
                    m4067(r5);
                }
            }
            if (r4.m3758(C0727.C0737.ViewBackgroundHelper_backgroundTint)) {
                C0414.m2221(this.f2514, r4.m3754(C0727.C0737.ViewBackgroundHelper_backgroundTint));
            }
            if (r4.m3758(C0727.C0737.ViewBackgroundHelper_backgroundTintMode)) {
                C0414.m2222(this.f2514, C0670.m4230(r4.m3742(C0727.C0737.ViewBackgroundHelper_backgroundTintMode, -1), null));
            }
        } finally {
            r4.m3745();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4061(int i) {
        this.f2516 = i;
        C0656 r0 = this.f2515;
        m4067(r0 != null ? r0.m4187(this.f2514.getContext(), i) : null);
        m4068();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4064(Drawable drawable) {
        this.f2516 = -1;
        m4067((ColorStateList) null);
        m4068();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4062(ColorStateList colorStateList) {
        if (this.f2518 == null) {
            this.f2518 = new C0590();
        }
        C0590 r0 = this.f2518;
        r0.f2319 = colorStateList;
        r0.f2322 = true;
        m4068();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public ColorStateList m4060() {
        C0590 r0 = this.f2518;
        if (r0 != null) {
            return r0.f2319;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4063(PorterDuff.Mode mode) {
        if (this.f2518 == null) {
            this.f2518 = new C0590();
        }
        C0590 r0 = this.f2518;
        r0.f2320 = mode;
        r0.f2321 = true;
        m4068();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public PorterDuff.Mode m4066() {
        C0590 r0 = this.f2518;
        if (r0 != null) {
            return r0.f2320;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m4068() {
        Drawable background = this.f2514.getBackground();
        if (background == null) {
            return;
        }
        if (!m4059() || !m4058(background)) {
            C0590 r1 = this.f2518;
            if (r1 != null) {
                C0656.m4167(background, r1, this.f2514.getDrawableState());
                return;
            }
            C0590 r12 = this.f2517;
            if (r12 != null) {
                C0656.m4167(background, r12, this.f2514.getDrawableState());
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4067(ColorStateList colorStateList) {
        if (colorStateList != null) {
            if (this.f2517 == null) {
                this.f2517 = new C0590();
            }
            C0590 r0 = this.f2517;
            r0.f2319 = colorStateList;
            r0.f2322 = true;
        } else {
            this.f2517 = null;
        }
        m4068();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean m4059() {
        int i = Build.VERSION.SDK_INT;
        if (i <= 21) {
            return i == 21;
        }
        if (this.f2517 != null) {
            return true;
        }
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m4058(Drawable drawable) {
        if (this.f2519 == null) {
            this.f2519 = new C0590();
        }
        C0590 r0 = this.f2519;
        r0.m3737();
        ColorStateList r1 = C0414.m2251(this.f2514);
        if (r1 != null) {
            r0.f2322 = true;
            r0.f2319 = r1;
        }
        PorterDuff.Mode r12 = C0414.m2252(this.f2514);
        if (r12 != null) {
            r0.f2321 = true;
            r0.f2320 = r12;
        }
        if (!r0.f2322 && !r0.f2321) {
            return false;
        }
        C0656.m4167(drawable, r0, this.f2514.getDrawableState());
        return true;
    }
}
