package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.support.v7.app.C0444;
import android.support.v7.view.C0528;
import android.support.v7.widget.C0652;
import android.support.v7.ʻ.C0727;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

/* renamed from: android.support.v7.widget.ʻʾ  reason: contains not printable characters */
/* compiled from: ScrollingTabContainerView */
public class C0580 extends HorizontalScrollView implements AdapterView.OnItemSelectedListener {

    /* renamed from: ˋ  reason: contains not printable characters */
    private static final Interpolator f2264 = new DecelerateInterpolator();

    /* renamed from: ʻ  reason: contains not printable characters */
    Runnable f2265;

    /* renamed from: ʼ  reason: contains not printable characters */
    C0652 f2266;

    /* renamed from: ʽ  reason: contains not printable characters */
    int f2267;

    /* renamed from: ʾ  reason: contains not printable characters */
    int f2268;

    /* renamed from: ʿ  reason: contains not printable characters */
    private C0582 f2269;

    /* renamed from: ˆ  reason: contains not printable characters */
    private Spinner f2270;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f2271;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f2272;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f2273;

    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        boolean z = true;
        boolean z2 = mode == 1073741824;
        setFillViewport(z2);
        int childCount = this.f2266.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.f2267 = -1;
        } else {
            if (childCount > 2) {
                this.f2267 = (int) (((float) View.MeasureSpec.getSize(i)) * 0.4f);
            } else {
                this.f2267 = View.MeasureSpec.getSize(i) / 2;
            }
            this.f2267 = Math.min(this.f2267, this.f2268);
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.f2272, 1073741824);
        if (z2 || !this.f2271) {
            z = false;
        }
        if (z) {
            this.f2266.measure(0, makeMeasureSpec);
            if (this.f2266.getMeasuredWidth() > View.MeasureSpec.getSize(i)) {
                m3672();
            } else {
                m3673();
            }
        } else {
            m3673();
        }
        int measuredWidth = getMeasuredWidth();
        super.onMeasure(i, makeMeasureSpec);
        int measuredWidth2 = getMeasuredWidth();
        if (z2 && measuredWidth != measuredWidth2) {
            setTabSelected(this.f2273);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m3671() {
        Spinner spinner = this.f2270;
        return spinner != null && spinner.getParent() == this;
    }

    public void setAllowCollapse(boolean z) {
        this.f2271 = z;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m3672() {
        if (!m3671()) {
            if (this.f2270 == null) {
                this.f2270 = m3674();
            }
            removeView(this.f2266);
            addView(this.f2270, new ViewGroup.LayoutParams(-2, -1));
            if (this.f2270.getAdapter() == null) {
                this.f2270.setAdapter((SpinnerAdapter) new C0581());
            }
            Runnable runnable = this.f2265;
            if (runnable != null) {
                removeCallbacks(runnable);
                this.f2265 = null;
            }
            this.f2270.setSelection(this.f2273);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean m3673() {
        if (!m3671()) {
            return false;
        }
        removeView(this.f2270);
        addView(this.f2266, new ViewGroup.LayoutParams(-2, -1));
        setTabSelected(this.f2270.getSelectedItemPosition());
        return false;
    }

    public void setTabSelected(int i) {
        this.f2273 = i;
        int childCount = this.f2266.getChildCount();
        int i2 = 0;
        while (i2 < childCount) {
            View childAt = this.f2266.getChildAt(i2);
            boolean z = i2 == i;
            childAt.setSelected(z);
            if (z) {
                m3676(i);
            }
            i2++;
        }
        Spinner spinner = this.f2270;
        if (spinner != null && i >= 0) {
            spinner.setSelection(i);
        }
    }

    public void setContentHeight(int i) {
        this.f2272 = i;
        requestLayout();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private Spinner m3674() {
        C0723 r0 = new C0723(getContext(), null, C0727.C0728.actionDropDownStyle);
        r0.setLayoutParams(new C0652.C0653(-2, -1));
        r0.setOnItemSelectedListener(this);
        return r0;
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        C0528 r2 = C0528.m3072(getContext());
        setContentHeight(r2.m3077());
        this.f2268 = r2.m3079();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3676(int i) {
        final View childAt = this.f2266.getChildAt(i);
        Runnable runnable = this.f2265;
        if (runnable != null) {
            removeCallbacks(runnable);
        }
        this.f2265 = new Runnable() {
            public void run() {
                C0580.this.smoothScrollTo(childAt.getLeft() - ((C0580.this.getWidth() - childAt.getWidth()) / 2), 0);
                C0580.this.f2265 = null;
            }
        };
        post(this.f2265);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Runnable runnable = this.f2265;
        if (runnable != null) {
            post(runnable);
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Runnable runnable = this.f2265;
        if (runnable != null) {
            removeCallbacks(runnable);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0583 m3675(C0444.C0447 r3, boolean z) {
        C0583 r0 = new C0583(getContext(), r3, z);
        if (z) {
            r0.setBackgroundDrawable(null);
            r0.setLayoutParams(new AbsListView.LayoutParams(-1, this.f2272));
        } else {
            r0.setFocusable(true);
            if (this.f2269 == null) {
                this.f2269 = new C0582();
            }
            r0.setOnClickListener(this.f2269);
        }
        return r0;
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
        ((C0583) view).m3679().m2459();
    }

    /* renamed from: android.support.v7.widget.ʻʾ$ʽ  reason: contains not printable characters */
    /* compiled from: ScrollingTabContainerView */
    private class C0583 extends C0652 {

        /* renamed from: ʼ  reason: contains not printable characters */
        private final int[] f2279 = {16842964};

        /* renamed from: ʽ  reason: contains not printable characters */
        private C0444.C0447 f2280;

        /* renamed from: ʾ  reason: contains not printable characters */
        private TextView f2281;

        /* renamed from: ʿ  reason: contains not printable characters */
        private ImageView f2282;

        /* renamed from: ˆ  reason: contains not printable characters */
        private View f2283;

        public C0583(Context context, C0444.C0447 r6, boolean z) {
            super(context, null, C0727.C0728.actionBarTabStyle);
            this.f2280 = r6;
            C0592 r4 = C0592.m3740(context, null, this.f2279, C0727.C0728.actionBarTabStyle, 0);
            if (r4.m3758(0)) {
                setBackgroundDrawable(r4.m3744(0));
            }
            r4.m3745();
            if (z) {
                setGravity(8388627);
            }
            m3677();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3678(C0444.C0447 r1) {
            this.f2280 = r1;
            m3677();
        }

        public void setSelected(boolean z) {
            boolean z2 = isSelected() != z;
            super.setSelected(z);
            if (z2 && z) {
                sendAccessibilityEvent(4);
            }
        }

        public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setClassName(C0444.C0447.class.getName());
        }

        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            accessibilityNodeInfo.setClassName(C0444.C0447.class.getName());
        }

        public void onMeasure(int i, int i2) {
            super.onMeasure(i, i2);
            if (C0580.this.f2267 > 0 && getMeasuredWidth() > C0580.this.f2267) {
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(C0580.this.f2267, 1073741824), i2);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3677() {
            C0444.C0447 r0 = this.f2280;
            View r1 = r0.m2458();
            CharSequence charSequence = null;
            if (r1 != null) {
                ViewParent parent = r1.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(r1);
                    }
                    addView(r1);
                }
                this.f2283 = r1;
                TextView textView = this.f2281;
                if (textView != null) {
                    textView.setVisibility(8);
                }
                ImageView imageView = this.f2282;
                if (imageView != null) {
                    imageView.setVisibility(8);
                    this.f2282.setImageDrawable(null);
                    return;
                }
                return;
            }
            View view = this.f2283;
            if (view != null) {
                removeView(view);
                this.f2283 = null;
            }
            Drawable r12 = r0.m2456();
            CharSequence r4 = r0.m2457();
            if (r12 != null) {
                if (this.f2282 == null) {
                    C0674 r8 = new C0674(getContext());
                    C0652.C0653 r9 = new C0652.C0653(-2, -2);
                    r9.f2603 = 16;
                    r8.setLayoutParams(r9);
                    addView(r8, 0);
                    this.f2282 = r8;
                }
                this.f2282.setImageDrawable(r12);
                this.f2282.setVisibility(0);
            } else {
                ImageView imageView2 = this.f2282;
                if (imageView2 != null) {
                    imageView2.setVisibility(8);
                    this.f2282.setImageDrawable(null);
                }
            }
            boolean z = !TextUtils.isEmpty(r4);
            if (z) {
                if (this.f2281 == null) {
                    C0677 r2 = new C0677(getContext(), null, C0727.C0728.actionBarTabTextStyle);
                    r2.setEllipsize(TextUtils.TruncateAt.END);
                    C0652.C0653 r82 = new C0652.C0653(-2, -2);
                    r82.f2603 = 16;
                    r2.setLayoutParams(r82);
                    addView(r2);
                    this.f2281 = r2;
                }
                this.f2281.setText(r4);
                this.f2281.setVisibility(0);
            } else {
                TextView textView2 = this.f2281;
                if (textView2 != null) {
                    textView2.setVisibility(8);
                    this.f2281.setText((CharSequence) null);
                }
            }
            ImageView imageView3 = this.f2282;
            if (imageView3 != null) {
                imageView3.setContentDescription(r0.m2460());
            }
            if (!z) {
                charSequence = r0.m2460();
            }
            C0594.m3805(this, charSequence);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public C0444.C0447 m3679() {
            return this.f2280;
        }
    }

    /* renamed from: android.support.v7.widget.ʻʾ$ʻ  reason: contains not printable characters */
    /* compiled from: ScrollingTabContainerView */
    private class C0581 extends BaseAdapter {
        public long getItemId(int i) {
            return (long) i;
        }

        C0581() {
        }

        public int getCount() {
            return C0580.this.f2266.getChildCount();
        }

        public Object getItem(int i) {
            return ((C0583) C0580.this.f2266.getChildAt(i)).m3679();
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                return C0580.this.m3675((C0444.C0447) getItem(i), true);
            }
            ((C0583) view).m3678((C0444.C0447) getItem(i));
            return view;
        }
    }

    /* renamed from: android.support.v7.widget.ʻʾ$ʼ  reason: contains not printable characters */
    /* compiled from: ScrollingTabContainerView */
    private class C0582 implements View.OnClickListener {
        C0582() {
        }

        public void onClick(View view) {
            ((C0583) view).m3679().m2459();
            int childCount = C0580.this.f2266.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = C0580.this.f2266.getChildAt(i);
                childAt.setSelected(childAt == view);
            }
        }
    }
}
