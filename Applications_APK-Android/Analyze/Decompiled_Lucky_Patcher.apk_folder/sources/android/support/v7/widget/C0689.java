package android.support.v7.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.widget.SeekBar;

/* renamed from: android.support.v7.widget.ﹳ  reason: contains not printable characters */
/* compiled from: AppCompatSeekBar */
public class C0689 extends SeekBar {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C0721 f2730;

    public C0689(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0727.C0728.seekBarStyle);
    }

    public C0689(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2730 = new C0721(this);
        this.f2730.m4838(attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.f2730.m4836(canvas);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        this.f2730.m4840();
    }

    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        this.f2730.m4839();
    }
}
