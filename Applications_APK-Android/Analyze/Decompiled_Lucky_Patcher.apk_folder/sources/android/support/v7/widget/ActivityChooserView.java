package android.support.v7.widget;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.DataSetObserver;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.ˉ.C0393;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

public class ActivityChooserView extends ViewGroup {

    /* renamed from: ʻ  reason: contains not printable characters */
    final C0548 f1977;

    /* renamed from: ʼ  reason: contains not printable characters */
    final FrameLayout f1978;

    /* renamed from: ʽ  reason: contains not printable characters */
    final FrameLayout f1979;

    /* renamed from: ʾ  reason: contains not printable characters */
    C0393 f1980;

    /* renamed from: ʿ  reason: contains not printable characters */
    final DataSetObserver f1981;

    /* renamed from: ˆ  reason: contains not printable characters */
    PopupWindow.OnDismissListener f1982;

    /* renamed from: ˈ  reason: contains not printable characters */
    boolean f1983;

    /* renamed from: ˉ  reason: contains not printable characters */
    int f1984;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final C0549 f1985;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final C0652 f1986;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final ImageView f1987;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final int f1988;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final ViewTreeObserver.OnGlobalLayoutListener f1989;

    /* renamed from: י  reason: contains not printable characters */
    private C0661 f1990;

    /* renamed from: ـ  reason: contains not printable characters */
    private boolean f1991;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f1992;

    public void setActivityChooserModel(C0625 r2) {
        this.f1977.m3222(r2);
        if (m3219()) {
            m3218();
            m3217();
        }
    }

    public void setExpandActivityOverflowButtonDrawable(Drawable drawable) {
        this.f1987.setImageDrawable(drawable);
    }

    public void setExpandActivityOverflowButtonContentDescription(int i) {
        this.f1987.setContentDescription(getContext().getString(i));
    }

    public void setProvider(C0393 r1) {
        this.f1980 = r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3217() {
        if (m3219() || !this.f1991) {
            return false;
        }
        this.f1983 = false;
        m3216(this.f1984);
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3216(int i) {
        if (this.f1977.m3227() != null) {
            getViewTreeObserver().addOnGlobalLayoutListener(this.f1989);
            boolean z = this.f1979.getVisibility() == 0;
            int r3 = this.f1977.m3226();
            if (i == Integer.MAX_VALUE || r3 <= i + (z ? 1 : 0)) {
                this.f1977.m3223(false);
                this.f1977.m3221(i);
            } else {
                this.f1977.m3223(true);
                this.f1977.m3221(i - 1);
            }
            C0661 listPopupWindow = getListPopupWindow();
            if (!listPopupWindow.m4216()) {
                if (this.f1983 || !z) {
                    this.f1977.m3224(true, z);
                } else {
                    this.f1977.m3224(false, false);
                }
                listPopupWindow.m4218(Math.min(this.f1977.m3220(), this.f1988));
                listPopupWindow.m4211();
                C0393 r0 = this.f1980;
                if (r0 != null) {
                    r0.m2147(true);
                }
                listPopupWindow.m4217().setContentDescription(getContext().getString(C0727.C0735.abc_activitychooserview_choose_application));
                listPopupWindow.m4217().setSelector(new ColorDrawable(0));
                return;
            }
            return;
        }
        throw new IllegalStateException("No data model. Did you call #setDataModel?");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m3218() {
        if (!m3219()) {
            return true;
        }
        getListPopupWindow().m4213();
        ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        if (!viewTreeObserver.isAlive()) {
            return true;
        }
        viewTreeObserver.removeGlobalOnLayoutListener(this.f1989);
        return true;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m3219() {
        return getListPopupWindow().m4216();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        C0625 r0 = this.f1977.m3227();
        if (r0 != null) {
            r0.registerObserver(this.f1981);
        }
        this.f1991 = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        C0625 r0 = this.f1977.m3227();
        if (r0 != null) {
            r0.unregisterObserver(this.f1981);
        }
        ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.removeGlobalOnLayoutListener(this.f1989);
        }
        if (m3219()) {
            m3218();
        }
        this.f1991 = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        C0652 r0 = this.f1986;
        if (this.f1979.getVisibility() != 0) {
            i2 = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i2), 1073741824);
        }
        measureChild(r0, i, i2);
        setMeasuredDimension(r0.getMeasuredWidth(), r0.getMeasuredHeight());
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        this.f1986.layout(0, 0, i3 - i, i4 - i2);
        if (!m3219()) {
            m3218();
        }
    }

    public C0625 getDataModel() {
        return this.f1977.m3227();
    }

    public void setOnDismissListener(PopupWindow.OnDismissListener onDismissListener) {
        this.f1982 = onDismissListener;
    }

    public void setInitialActivityCount(int i) {
        this.f1984 = i;
    }

    public void setDefaultActionButtonContentDescription(int i) {
        this.f1992 = i;
    }

    /* access modifiers changed from: package-private */
    public C0661 getListPopupWindow() {
        if (this.f1990 == null) {
            this.f1990 = new C0661(getContext());
            this.f1990.m4203(this.f1977);
            this.f1990.m4207(this);
            this.f1990.m4205(true);
            this.f1990.m4202((AdapterView.OnItemClickListener) this.f1985);
            this.f1990.m4204((PopupWindow.OnDismissListener) this.f1985);
        }
        return this.f1990;
    }

    /* renamed from: android.support.v7.widget.ActivityChooserView$ʼ  reason: contains not printable characters */
    private class C0549 implements View.OnClickListener, View.OnLongClickListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener {

        /* renamed from: ʻ  reason: contains not printable characters */
        final /* synthetic */ ActivityChooserView f2000;

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            int itemViewType = ((C0548) adapterView.getAdapter()).getItemViewType(i);
            if (itemViewType == 0) {
                this.f2000.m3218();
                if (!this.f2000.f1983) {
                    if (!this.f2000.f1977.m3228()) {
                        i++;
                    }
                    Intent r1 = this.f2000.f1977.m3227().m3982(i);
                    if (r1 != null) {
                        r1.addFlags(524288);
                        this.f2000.getContext().startActivity(r1);
                    }
                } else if (i > 0) {
                    this.f2000.f1977.m3227().m3984(i);
                }
            } else if (itemViewType == 1) {
                this.f2000.m3216(Integer.MAX_VALUE);
            } else {
                throw new IllegalArgumentException();
            }
        }

        public void onClick(View view) {
            if (view == this.f2000.f1979) {
                this.f2000.m3218();
                Intent r2 = this.f2000.f1977.m3227().m3982(this.f2000.f1977.m3227().m3980(this.f2000.f1977.m3225()));
                if (r2 != null) {
                    r2.addFlags(524288);
                    this.f2000.getContext().startActivity(r2);
                }
            } else if (view == this.f2000.f1978) {
                ActivityChooserView activityChooserView = this.f2000;
                activityChooserView.f1983 = false;
                activityChooserView.m3216(activityChooserView.f1984);
            } else {
                throw new IllegalArgumentException();
            }
        }

        public boolean onLongClick(View view) {
            if (view == this.f2000.f1979) {
                if (this.f2000.f1977.getCount() > 0) {
                    ActivityChooserView activityChooserView = this.f2000;
                    activityChooserView.f1983 = true;
                    activityChooserView.m3216(activityChooserView.f1984);
                }
                return true;
            }
            throw new IllegalArgumentException();
        }

        public void onDismiss() {
            m3229();
            if (this.f2000.f1980 != null) {
                this.f2000.f1980.m2147(false);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m3229() {
            if (this.f2000.f1982 != null) {
                this.f2000.f1982.onDismiss();
            }
        }
    }

    /* renamed from: android.support.v7.widget.ActivityChooserView$ʻ  reason: contains not printable characters */
    private class C0548 extends BaseAdapter {

        /* renamed from: ʻ  reason: contains not printable characters */
        final /* synthetic */ ActivityChooserView f1994;

        /* renamed from: ʼ  reason: contains not printable characters */
        private C0625 f1995;

        /* renamed from: ʽ  reason: contains not printable characters */
        private int f1996;

        /* renamed from: ʾ  reason: contains not printable characters */
        private boolean f1997;

        /* renamed from: ʿ  reason: contains not printable characters */
        private boolean f1998;

        /* renamed from: ˆ  reason: contains not printable characters */
        private boolean f1999;

        public long getItemId(int i) {
            return (long) i;
        }

        public int getViewTypeCount() {
            return 3;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3222(C0625 r3) {
            C0625 r0 = this.f1994.f1977.m3227();
            if (r0 != null && this.f1994.isShown()) {
                r0.unregisterObserver(this.f1994.f1981);
            }
            this.f1995 = r3;
            if (r3 != null && this.f1994.isShown()) {
                r3.registerObserver(this.f1994.f1981);
            }
            notifyDataSetChanged();
        }

        public int getItemViewType(int i) {
            return (!this.f1999 || i != getCount() - 1) ? 0 : 1;
        }

        public int getCount() {
            int r0 = this.f1995.m3979();
            if (!this.f1997 && this.f1995.m3983() != null) {
                r0--;
            }
            int min = Math.min(r0, this.f1996);
            return this.f1999 ? min + 1 : min;
        }

        public Object getItem(int i) {
            int itemViewType = getItemViewType(i);
            if (itemViewType == 0) {
                if (!this.f1997 && this.f1995.m3983() != null) {
                    i++;
                }
                return this.f1995.m3981(i);
            } else if (itemViewType == 1) {
                return null;
            } else {
                throw new IllegalArgumentException();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            int itemViewType = getItemViewType(i);
            if (itemViewType == 0) {
                if (view == null || view.getId() != C0727.C0733.list_item) {
                    view = LayoutInflater.from(this.f1994.getContext()).inflate(C0727.C0734.abc_activity_chooser_view_list_item, viewGroup, false);
                }
                PackageManager packageManager = this.f1994.getContext().getPackageManager();
                ResolveInfo resolveInfo = (ResolveInfo) getItem(i);
                ((ImageView) view.findViewById(C0727.C0733.icon)).setImageDrawable(resolveInfo.loadIcon(packageManager));
                ((TextView) view.findViewById(C0727.C0733.title)).setText(resolveInfo.loadLabel(packageManager));
                if (!this.f1997 || i != 0 || !this.f1998) {
                    view.setActivated(false);
                } else {
                    view.setActivated(true);
                }
                return view;
            } else if (itemViewType != 1) {
                throw new IllegalArgumentException();
            } else if (view != null && view.getId() == 1) {
                return view;
            } else {
                View inflate = LayoutInflater.from(this.f1994.getContext()).inflate(C0727.C0734.abc_activity_chooser_view_list_item, viewGroup, false);
                inflate.setId(1);
                ((TextView) inflate.findViewById(C0727.C0733.title)).setText(this.f1994.getContext().getString(C0727.C0735.abc_activity_chooser_view_see_all));
                return inflate;
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m3220() {
            int i = this.f1996;
            this.f1996 = Integer.MAX_VALUE;
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
            int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
            int count = getCount();
            View view = null;
            int i2 = 0;
            for (int i3 = 0; i3 < count; i3++) {
                view = getView(i3, view, null);
                view.measure(makeMeasureSpec, makeMeasureSpec2);
                i2 = Math.max(i2, view.getMeasuredWidth());
            }
            this.f1996 = i;
            return i2;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3221(int i) {
            if (this.f1996 != i) {
                this.f1996 = i;
                notifyDataSetChanged();
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public ResolveInfo m3225() {
            return this.f1995.m3983();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3223(boolean z) {
            if (this.f1999 != z) {
                this.f1999 = z;
                notifyDataSetChanged();
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public int m3226() {
            return this.f1995.m3979();
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public C0625 m3227() {
            return this.f1995;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3224(boolean z, boolean z2) {
            if (this.f1997 != z || this.f1998 != z2) {
                this.f1997 = z;
                this.f1998 = z2;
                notifyDataSetChanged();
            }
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public boolean m3228() {
            return this.f1997;
        }
    }

    public static class InnerLayout extends C0652 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private static final int[] f1993 = {16842964};

        public InnerLayout(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            C0592 r2 = C0592.m3739(context, attributeSet, f1993);
            setBackgroundDrawable(r2.m3744(0));
            r2.m3745();
        }
    }
}
