package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.ˉ.ʻ.C0360;
import android.support.v7.widget.C0690;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

public class StaggeredGridLayoutManager extends C0690.C0701 {

    /* renamed from: ʻ  reason: contains not printable characters */
    C0569[] f2122;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private final Rect f2123 = new Rect();

    /* renamed from: ʼ  reason: contains not printable characters */
    C0688 f2124;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private boolean f2125 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    C0688 f2126;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private final C0564 f2127 = new C0564();

    /* renamed from: ʾ  reason: contains not printable characters */
    boolean f2128 = false;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private int[] f2129;

    /* renamed from: ʿ  reason: contains not printable characters */
    boolean f2130 = false;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private boolean f2131;

    /* renamed from: ˆ  reason: contains not printable characters */
    int f2132 = -1;

    /* renamed from: ˈ  reason: contains not printable characters */
    int f2133 = Integer.MIN_VALUE;

    /* renamed from: ˉ  reason: contains not printable characters */
    C0566 f2134 = new C0566();

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f2135 = -1;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f2136;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f2137;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final C0655 f2138;

    /* renamed from: ˑ  reason: contains not printable characters */
    private BitSet f2139;

    /* renamed from: י  reason: contains not printable characters */
    private int f2140 = 2;

    /* renamed from: ـ  reason: contains not printable characters */
    private boolean f2141;

    /* renamed from: ــ  reason: contains not printable characters */
    private final Runnable f2142;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private C0568 f2143;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private int f2144;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private boolean f2145;

    public StaggeredGridLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        boolean z = true;
        this.f2131 = true;
        this.f2142 = new Runnable() {
            public void run() {
                StaggeredGridLayoutManager.this.m3501();
            }
        };
        C0690.C0701.C0703 r4 = m4536(context, attributeSet, i, i2);
        m3485(r4.f2850);
        m3465(r4.f2851);
        m3480(r4.f2852);
        m4616(this.f2140 == 0 ? false : z);
        this.f2138 = new C0655();
        m3443();
    }

    public StaggeredGridLayoutManager(int i, int i2) {
        boolean z = true;
        this.f2131 = true;
        this.f2142 = new Runnable() {
            public void run() {
                StaggeredGridLayoutManager.this.m3501();
            }
        };
        this.f2136 = i2;
        m3465(i);
        m4616(this.f2140 == 0 ? false : z);
        this.f2138 = new C0655();
        m3443();
    }

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private void m3443() {
        this.f2124 = C0688.m4287(this, this.f2136);
        this.f2126 = C0688.m4287(this, 1 - this.f2136);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m3501() {
        int i;
        int i2;
        if (m4667() == 0 || this.f2140 == 0 || !m4660()) {
            return false;
        }
        if (this.f2130) {
            i2 = m3507();
            i = m3504();
        } else {
            i2 = m3504();
            i = m3507();
        }
        if (i2 == 0 && m3503() != null) {
            this.f2134.m3526();
            m4659();
            m4656();
            return true;
        } else if (!this.f2125) {
            return false;
        } else {
            int i3 = this.f2130 ? -1 : 1;
            int i4 = i + 1;
            C0566.C0567 r6 = this.f2134.m3525(i2, i4, i3, true);
            if (r6 == null) {
                this.f2125 = false;
                this.f2134.m3524(i4);
                return false;
            }
            C0566.C0567 r0 = this.f2134.m3525(i2, r6.f2158, i3 * -1, true);
            if (r0 == null) {
                this.f2134.m3524(r6.f2158);
            } else {
                this.f2134.m3524(r0.f2158 + 1);
            }
            m4659();
            m4656();
            return true;
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public void m3512(int i) {
        if (i == 0) {
            m3501();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3477(C0690 r2, C0690.C0711 r3) {
        m4592(this.f2142);
        for (int i = 0; i < this.f2135; i++) {
            this.f2122[i].m3558();
        }
        r2.requestLayout();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0074, code lost:
        if (r10 == r11) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0086, code lost:
        if (r10 == r11) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008a, code lost:
        r10 = false;
     */
    /* renamed from: ˈ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View m3503() {
        /*
            r12 = this;
            int r0 = r12.m4667()
            r1 = 1
            int r0 = r0 - r1
            java.util.BitSet r2 = new java.util.BitSet
            int r3 = r12.f2135
            r2.<init>(r3)
            int r3 = r12.f2135
            r4 = 0
            r2.set(r4, r3, r1)
            int r3 = r12.f2136
            r5 = -1
            if (r3 != r1) goto L_0x0020
            boolean r3 = r12.m3509()
            if (r3 == 0) goto L_0x0020
            r3 = 1
            goto L_0x0021
        L_0x0020:
            r3 = -1
        L_0x0021:
            boolean r6 = r12.f2130
            if (r6 == 0) goto L_0x0027
            r6 = -1
            goto L_0x002b
        L_0x0027:
            int r0 = r0 + 1
            r6 = r0
            r0 = 0
        L_0x002b:
            if (r0 >= r6) goto L_0x002e
            r5 = 1
        L_0x002e:
            if (r0 == r6) goto L_0x00ab
            android.view.View r7 = r12.m4645(r0)
            android.view.ViewGroup$LayoutParams r8 = r7.getLayoutParams()
            android.support.v7.widget.StaggeredGridLayoutManager$ʼ r8 = (android.support.v7.widget.StaggeredGridLayoutManager.C0565) r8
            android.support.v7.widget.StaggeredGridLayoutManager$ʿ r9 = r8.f2154
            int r9 = r9.f2176
            boolean r9 = r2.get(r9)
            if (r9 == 0) goto L_0x0054
            android.support.v7.widget.StaggeredGridLayoutManager$ʿ r9 = r8.f2154
            boolean r9 = r12.m3431(r9)
            if (r9 == 0) goto L_0x004d
            return r7
        L_0x004d:
            android.support.v7.widget.StaggeredGridLayoutManager$ʿ r9 = r8.f2154
            int r9 = r9.f2176
            r2.clear(r9)
        L_0x0054:
            boolean r9 = r8.f2155
            if (r9 == 0) goto L_0x0059
            goto L_0x00a9
        L_0x0059:
            int r9 = r0 + r5
            if (r9 == r6) goto L_0x00a9
            android.view.View r9 = r12.m4645(r9)
            boolean r10 = r12.f2130
            if (r10 == 0) goto L_0x0077
            android.support.v7.widget.ⁱⁱ r10 = r12.f2124
            int r10 = r10.m4293(r7)
            android.support.v7.widget.ⁱⁱ r11 = r12.f2124
            int r11 = r11.m4293(r9)
            if (r10 >= r11) goto L_0x0074
            return r7
        L_0x0074:
            if (r10 != r11) goto L_0x008a
            goto L_0x0088
        L_0x0077:
            android.support.v7.widget.ⁱⁱ r10 = r12.f2124
            int r10 = r10.m4289(r7)
            android.support.v7.widget.ⁱⁱ r11 = r12.f2124
            int r11 = r11.m4289(r9)
            if (r10 <= r11) goto L_0x0086
            return r7
        L_0x0086:
            if (r10 != r11) goto L_0x008a
        L_0x0088:
            r10 = 1
            goto L_0x008b
        L_0x008a:
            r10 = 0
        L_0x008b:
            if (r10 == 0) goto L_0x00a9
            android.view.ViewGroup$LayoutParams r9 = r9.getLayoutParams()
            android.support.v7.widget.StaggeredGridLayoutManager$ʼ r9 = (android.support.v7.widget.StaggeredGridLayoutManager.C0565) r9
            android.support.v7.widget.StaggeredGridLayoutManager$ʿ r8 = r8.f2154
            int r8 = r8.f2176
            android.support.v7.widget.StaggeredGridLayoutManager$ʿ r9 = r9.f2154
            int r9 = r9.f2176
            int r8 = r8 - r9
            if (r8 >= 0) goto L_0x00a0
            r8 = 1
            goto L_0x00a1
        L_0x00a0:
            r8 = 0
        L_0x00a1:
            if (r3 >= 0) goto L_0x00a5
            r9 = 1
            goto L_0x00a6
        L_0x00a5:
            r9 = 0
        L_0x00a6:
            if (r8 == r9) goto L_0x00a9
            return r7
        L_0x00a9:
            int r0 = r0 + r5
            goto L_0x002e
        L_0x00ab:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.StaggeredGridLayoutManager.m3503():android.view.View");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m3431(C0569 r4) {
        if (this.f2130) {
            if (r4.m3556() < this.f2124.m4296()) {
                return !r4.m3553(r4.f2172.get(r4.f2172.size() - 1)).f2155;
            }
        } else if (r4.m3550() > this.f2124.m4294()) {
            return !r4.m3553(r4.f2172.get(0)).f2155;
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3465(int i) {
        m3479((String) null);
        if (i != this.f2135) {
            m3506();
            this.f2135 = i;
            this.f2139 = new BitSet(this.f2135);
            this.f2122 = new C0569[this.f2135];
            for (int i2 = 0; i2 < this.f2135; i2++) {
                this.f2122[i2] = new C0569(i2);
            }
            m4656();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3485(int i) {
        if (i == 0 || i == 1) {
            m3479((String) null);
            if (i != this.f2136) {
                this.f2136 = i;
                C0688 r2 = this.f2124;
                this.f2124 = this.f2126;
                this.f2126 = r2;
                m4656();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("invalid orientation.");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3480(boolean z) {
        m3479((String) null);
        C0568 r0 = this.f2143;
        if (!(r0 == null || r0.f2169 == z)) {
            this.f2143.f2169 = z;
        }
        this.f2128 = z;
        m4656();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3479(String str) {
        if (this.f2143 == null) {
            super.m4579(str);
        }
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public void m3506() {
        this.f2134.m3526();
        m4656();
    }

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private void m3441() {
        if (this.f2136 == 1 || !m3509()) {
            this.f2130 = this.f2128;
        } else {
            this.f2130 = !this.f2128;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean m3509() {
        return m4665() == 1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3468(Rect rect, int i, int i2) {
        int i3;
        int i4;
        int r0 = m4672() + m4664();
        int r1 = m4662() + m4593();
        if (this.f2136 == 1) {
            i4 = m4534(i2, rect.height() + r1, m4633());
            i3 = m4534(i, (this.f2137 * this.f2135) + r0, m4605());
        } else {
            i3 = m4534(i, rect.width() + r0, m4605());
            i4 = m4534(i2, (this.f2137 * this.f2135) + r1, m4633());
        }
        m4637(i3, i4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.StaggeredGridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):void
     arg types: [android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int]
     candidates:
      android.support.v7.widget.StaggeredGridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ˏˏ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.StaggeredGridLayoutManager.ʻ(android.support.v7.widget.StaggeredGridLayoutManager$ʿ, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.ʻ(android.view.View, android.support.v7.widget.StaggeredGridLayoutManager$ʼ, android.support.v7.widget.ˏˏ):void
      android.support.v7.widget.StaggeredGridLayoutManager.ʻ(android.view.View, android.support.v7.widget.StaggeredGridLayoutManager$ʼ, boolean):void
      android.support.v7.widget.StaggeredGridLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.StaggeredGridLayoutManager.ʻ(android.graphics.Rect, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int, android.view.View):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, boolean):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.graphics.Rect, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.accessibility.AccessibilityEvent):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ˊ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, android.graphics.Rect):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.view.View):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, boolean):boolean
      android.support.v7.widget.StaggeredGridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):void */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3492(C0690.C0711 r2, C0690.C0717 r3) {
        m3427(r2, r3, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.StaggeredGridLayoutManager.ʼ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):void
     arg types: [android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int]
     candidates:
      android.support.v7.widget.StaggeredGridLayoutManager.ʼ(int, int, int):int
      android.support.v7.widget.StaggeredGridLayoutManager.ʼ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.StaggeredGridLayoutManager.ʼ(android.support.v7.widget.ﹳﹳ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(int, int, int):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(android.support.v7.widget.ﹳﹳ, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.ʼ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.StaggeredGridLayoutManager.ʽ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):void
     arg types: [android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int]
     candidates:
      android.support.v7.widget.StaggeredGridLayoutManager.ʽ(int, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.ʽ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʽ(android.support.v7.widget.ﹳﹳ, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.ʽ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.StaggeredGridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):void
     arg types: [android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int]
     candidates:
      android.support.v7.widget.StaggeredGridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ˏˏ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.StaggeredGridLayoutManager.ʻ(android.support.v7.widget.StaggeredGridLayoutManager$ʿ, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.ʻ(android.view.View, android.support.v7.widget.StaggeredGridLayoutManager$ʼ, android.support.v7.widget.ˏˏ):void
      android.support.v7.widget.StaggeredGridLayoutManager.ʻ(android.view.View, android.support.v7.widget.StaggeredGridLayoutManager$ʼ, boolean):void
      android.support.v7.widget.StaggeredGridLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.StaggeredGridLayoutManager.ʻ(android.graphics.Rect, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int, android.view.View):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, boolean):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.graphics.Rect, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.accessibility.AccessibilityEvent):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ˊ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, android.graphics.Rect):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.view.View):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, boolean):boolean
      android.support.v7.widget.StaggeredGridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0161, code lost:
        if (m3501() != false) goto L_0x0165;
     */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m3427(android.support.v7.widget.C0690.C0711 r9, android.support.v7.widget.C0690.C0717 r10, boolean r11) {
        /*
            r8 = this;
            android.support.v7.widget.StaggeredGridLayoutManager$ʻ r0 = r8.f2127
            android.support.v7.widget.StaggeredGridLayoutManager$ʾ r1 = r8.f2143
            r2 = -1
            if (r1 != 0) goto L_0x000b
            int r1 = r8.f2132
            if (r1 == r2) goto L_0x0018
        L_0x000b:
            int r1 = r10.m4775()
            if (r1 != 0) goto L_0x0018
            r8.m4610(r9)
            r0.m3515()
            return
        L_0x0018:
            boolean r1 = r0.f2151
            r3 = 0
            r4 = 1
            if (r1 == 0) goto L_0x0029
            int r1 = r8.f2132
            if (r1 != r2) goto L_0x0029
            android.support.v7.widget.StaggeredGridLayoutManager$ʾ r1 = r8.f2143
            if (r1 == 0) goto L_0x0027
            goto L_0x0029
        L_0x0027:
            r1 = 0
            goto L_0x002a
        L_0x0029:
            r1 = 1
        L_0x002a:
            if (r1 == 0) goto L_0x0043
            r0.m3515()
            android.support.v7.widget.StaggeredGridLayoutManager$ʾ r5 = r8.f2143
            if (r5 == 0) goto L_0x0037
            r8.m3423(r0)
            goto L_0x003e
        L_0x0037:
            r8.m3441()
            boolean r5 = r8.f2130
            r0.f2149 = r5
        L_0x003e:
            r8.m3472(r10, r0)
            r0.f2151 = r4
        L_0x0043:
            android.support.v7.widget.StaggeredGridLayoutManager$ʾ r5 = r8.f2143
            if (r5 != 0) goto L_0x0060
            int r5 = r8.f2132
            if (r5 != r2) goto L_0x0060
            boolean r5 = r0.f2149
            boolean r6 = r8.f2141
            if (r5 != r6) goto L_0x0059
            boolean r5 = r8.m3509()
            boolean r6 = r8.f2145
            if (r5 == r6) goto L_0x0060
        L_0x0059:
            android.support.v7.widget.StaggeredGridLayoutManager$ʽ r5 = r8.f2134
            r5.m3526()
            r0.f2150 = r4
        L_0x0060:
            int r5 = r8.m4667()
            if (r5 <= 0) goto L_0x00cb
            android.support.v7.widget.StaggeredGridLayoutManager$ʾ r5 = r8.f2143
            if (r5 == 0) goto L_0x006e
            int r5 = r5.f2164
            if (r5 >= r4) goto L_0x00cb
        L_0x006e:
            boolean r5 = r0.f2150
            if (r5 == 0) goto L_0x0090
            r1 = 0
        L_0x0073:
            int r5 = r8.f2135
            if (r1 >= r5) goto L_0x00cb
            android.support.v7.widget.StaggeredGridLayoutManager$ʿ[] r5 = r8.f2122
            r5 = r5[r1]
            r5.m3558()
            int r5 = r0.f2148
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r5 == r6) goto L_0x008d
            android.support.v7.widget.StaggeredGridLayoutManager$ʿ[] r5 = r8.f2122
            r5 = r5[r1]
            int r6 = r0.f2148
            r5.m3555(r6)
        L_0x008d:
            int r1 = r1 + 1
            goto L_0x0073
        L_0x0090:
            if (r1 != 0) goto L_0x00b1
            android.support.v7.widget.StaggeredGridLayoutManager$ʻ r1 = r8.f2127
            int[] r1 = r1.f2152
            if (r1 != 0) goto L_0x0099
            goto L_0x00b1
        L_0x0099:
            r1 = 0
        L_0x009a:
            int r5 = r8.f2135
            if (r1 >= r5) goto L_0x00cb
            android.support.v7.widget.StaggeredGridLayoutManager$ʿ[] r5 = r8.f2122
            r5 = r5[r1]
            r5.m3558()
            android.support.v7.widget.StaggeredGridLayoutManager$ʻ r6 = r8.f2127
            int[] r6 = r6.f2152
            r6 = r6[r1]
            r5.m3555(r6)
            int r1 = r1 + 1
            goto L_0x009a
        L_0x00b1:
            r1 = 0
        L_0x00b2:
            int r5 = r8.f2135
            if (r1 >= r5) goto L_0x00c4
            android.support.v7.widget.StaggeredGridLayoutManager$ʿ[] r5 = r8.f2122
            r5 = r5[r1]
            boolean r6 = r8.f2130
            int r7 = r0.f2148
            r5.m3549(r6, r7)
            int r1 = r1 + 1
            goto L_0x00b2
        L_0x00c4:
            android.support.v7.widget.StaggeredGridLayoutManager$ʻ r1 = r8.f2127
            android.support.v7.widget.StaggeredGridLayoutManager$ʿ[] r5 = r8.f2122
            r1.m3517(r5)
        L_0x00cb:
            r8.m4558(r9)
            android.support.v7.widget.ˏˏ r1 = r8.f2138
            r1.f2610 = r3
            r8.f2125 = r3
            android.support.v7.widget.ⁱⁱ r1 = r8.f2126
            int r1 = r1.m4300()
            r8.m3498(r1)
            int r1 = r0.f2147
            r8.m3434(r1, r10)
            boolean r1 = r0.f2149
            if (r1 == 0) goto L_0x0102
            r8.m3444(r2)
            android.support.v7.widget.ˏˏ r1 = r8.f2138
            r8.m3420(r9, r1, r10)
            r8.m3444(r4)
            android.support.v7.widget.ˏˏ r1 = r8.f2138
            int r2 = r0.f2147
            android.support.v7.widget.ˏˏ r5 = r8.f2138
            int r5 = r5.f2613
            int r2 = r2 + r5
            r1.f2612 = r2
            android.support.v7.widget.ˏˏ r1 = r8.f2138
            r8.m3420(r9, r1, r10)
            goto L_0x011d
        L_0x0102:
            r8.m3444(r4)
            android.support.v7.widget.ˏˏ r1 = r8.f2138
            r8.m3420(r9, r1, r10)
            r8.m3444(r2)
            android.support.v7.widget.ˏˏ r1 = r8.f2138
            int r2 = r0.f2147
            android.support.v7.widget.ˏˏ r5 = r8.f2138
            int r5 = r5.f2613
            int r2 = r2 + r5
            r1.f2612 = r2
            android.support.v7.widget.ˏˏ r1 = r8.f2138
            r8.m3420(r9, r1, r10)
        L_0x011d:
            r8.m3445()
            int r1 = r8.m4667()
            if (r1 <= 0) goto L_0x0137
            boolean r1 = r8.f2130
            if (r1 == 0) goto L_0x0131
            r8.m3436(r9, r10, r4)
            r8.m3438(r9, r10, r3)
            goto L_0x0137
        L_0x0131:
            r8.m3438(r9, r10, r4)
            r8.m3436(r9, r10, r3)
        L_0x0137:
            if (r11 == 0) goto L_0x0164
            boolean r11 = r10.m4771()
            if (r11 != 0) goto L_0x0164
            int r11 = r8.f2140
            if (r11 == 0) goto L_0x0155
            int r11 = r8.m4667()
            if (r11 <= 0) goto L_0x0155
            boolean r11 = r8.f2125
            if (r11 != 0) goto L_0x0153
            android.view.View r11 = r8.m3503()
            if (r11 == 0) goto L_0x0155
        L_0x0153:
            r11 = 1
            goto L_0x0156
        L_0x0155:
            r11 = 0
        L_0x0156:
            if (r11 == 0) goto L_0x0164
            java.lang.Runnable r11 = r8.f2142
            r8.m4592(r11)
            boolean r11 = r8.m3501()
            if (r11 == 0) goto L_0x0164
            goto L_0x0165
        L_0x0164:
            r4 = 0
        L_0x0165:
            boolean r11 = r10.m4771()
            if (r11 == 0) goto L_0x0170
            android.support.v7.widget.StaggeredGridLayoutManager$ʻ r11 = r8.f2127
            r11.m3515()
        L_0x0170:
            boolean r11 = r0.f2149
            r8.f2141 = r11
            boolean r11 = r8.m3509()
            r8.f2145 = r11
            if (r4 == 0) goto L_0x0184
            android.support.v7.widget.StaggeredGridLayoutManager$ʻ r11 = r8.f2127
            r11.m3515()
            r8.m3427(r9, r10, r3)
        L_0x0184:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.StaggeredGridLayoutManager.m3427(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):void");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3471(C0690.C0717 r1) {
        super.m4563(r1);
        this.f2132 = -1;
        this.f2133 = Integer.MIN_VALUE;
        this.f2143 = null;
        this.f2127.m3515();
    }

    /* renamed from: ˏˏ  reason: contains not printable characters */
    private void m3445() {
        if (this.f2126.m4303() != 1073741824) {
            int r1 = m4667();
            float f = 0.0f;
            for (int i = 0; i < r1; i++) {
                View r4 = m4645(i);
                float r5 = (float) this.f2126.m4299(r4);
                if (r5 >= f) {
                    if (((C0565) r4.getLayoutParams()).m3519()) {
                        r5 = (r5 * 1.0f) / ((float) this.f2135);
                    }
                    f = Math.max(f, r5);
                }
            }
            int i2 = this.f2137;
            int round = Math.round(f * ((float) this.f2135));
            if (this.f2126.m4303() == Integer.MIN_VALUE) {
                round = Math.min(round, this.f2126.m4300());
            }
            m3498(round);
            if (this.f2137 != i2) {
                for (int i3 = 0; i3 < r1; i3++) {
                    View r3 = m4645(i3);
                    C0565 r42 = (C0565) r3.getLayoutParams();
                    if (!r42.f2155) {
                        if (!m3509() || this.f2136 != 1) {
                            int i4 = r42.f2154.f2176 * this.f2137;
                            int i5 = r42.f2154.f2176 * i2;
                            if (this.f2136 == 1) {
                                r3.offsetLeftAndRight(i4 - i5);
                            } else {
                                r3.offsetTopAndBottom(i4 - i5);
                            }
                        } else {
                            r3.offsetLeftAndRight(((-((this.f2135 - 1) - r42.f2154.f2176)) * this.f2137) - ((-((this.f2135 - 1) - r42.f2154.f2176)) * i2));
                        }
                    }
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3423(C0564 r4) {
        int i;
        if (this.f2143.f2164 > 0) {
            if (this.f2143.f2164 == this.f2135) {
                for (int i2 = 0; i2 < this.f2135; i2++) {
                    this.f2122[i2].m3558();
                    int i3 = this.f2143.f2165[i2];
                    if (i3 != Integer.MIN_VALUE) {
                        if (this.f2143.f2170) {
                            i = this.f2124.m4296();
                        } else {
                            i = this.f2124.m4294();
                        }
                        i3 += i;
                    }
                    this.f2122[i2].m3555(i3);
                }
            } else {
                this.f2143.m3539();
                C0568 r0 = this.f2143;
                r0.f2162 = r0.f2163;
            }
        }
        this.f2145 = this.f2143.f2171;
        m3480(this.f2143.f2169);
        m3441();
        if (this.f2143.f2162 != -1) {
            this.f2132 = this.f2143.f2162;
            r4.f2149 = this.f2143.f2170;
        } else {
            r4.f2149 = this.f2130;
        }
        if (this.f2143.f2166 > 1) {
            this.f2134.f2156 = this.f2143.f2167;
            this.f2134.f2157 = this.f2143.f2168;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3472(C0690.C0717 r2, C0564 r3) {
        if (!m3488(r2, r3) && !m3439(r2, r3)) {
            r3.m3518();
            r3.f2147 = 0;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean m3439(C0690.C0717 r2, C0564 r3) {
        int i;
        if (this.f2141) {
            i = m3457(r2.m4775());
        } else {
            i = m3456(r2.m4775());
        }
        r3.f2147 = i;
        r3.f2148 = Integer.MIN_VALUE;
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m3488(C0690.C0717 r5, C0564 r6) {
        int i;
        int i2;
        int i3;
        boolean z = false;
        if (!r5.m4771() && (i = this.f2132) != -1) {
            if (i < 0 || i >= r5.m4775()) {
                this.f2132 = -1;
                this.f2133 = Integer.MIN_VALUE;
            } else {
                C0568 r52 = this.f2143;
                if (r52 == null || r52.f2162 == -1 || this.f2143.f2164 < 1) {
                    View r53 = m4608(this.f2132);
                    if (r53 != null) {
                        if (this.f2130) {
                            i2 = m3507();
                        } else {
                            i2 = m3504();
                        }
                        r6.f2147 = i2;
                        if (this.f2133 != Integer.MIN_VALUE) {
                            if (r6.f2149) {
                                r6.f2148 = (this.f2124.m4296() - this.f2133) - this.f2124.m4293(r53);
                            } else {
                                r6.f2148 = (this.f2124.m4294() + this.f2133) - this.f2124.m4289(r53);
                            }
                            return true;
                        } else if (this.f2124.m4299(r53) > this.f2124.m4300()) {
                            if (r6.f2149) {
                                i3 = this.f2124.m4296();
                            } else {
                                i3 = this.f2124.m4294();
                            }
                            r6.f2148 = i3;
                            return true;
                        } else {
                            int r1 = this.f2124.m4289(r53) - this.f2124.m4294();
                            if (r1 < 0) {
                                r6.f2148 = -r1;
                                return true;
                            }
                            int r12 = this.f2124.m4296() - this.f2124.m4293(r53);
                            if (r12 < 0) {
                                r6.f2148 = r12;
                                return true;
                            }
                            r6.f2148 = Integer.MIN_VALUE;
                        }
                    } else {
                        r6.f2147 = this.f2132;
                        int i4 = this.f2133;
                        if (i4 == Integer.MIN_VALUE) {
                            if (m3455(r6.f2147) == 1) {
                                z = true;
                            }
                            r6.f2149 = z;
                            r6.m3518();
                        } else {
                            r6.m3516(i4);
                        }
                        r6.f2150 = true;
                    }
                } else {
                    r6.f2148 = Integer.MIN_VALUE;
                    r6.f2147 = this.f2132;
                }
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m3498(int i) {
        this.f2137 = i / this.f2135;
        this.f2144 = View.MeasureSpec.makeMeasureSpec(i, this.f2126.m4303());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m3487() {
        return this.f2143 == null;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m3490(C0690.C0717 r1) {
        return m3433(r1);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int m3433(C0690.C0717 r8) {
        if (m4667() == 0) {
            return 0;
        }
        return C0579.m3669(r8, this.f2124, m3484(!this.f2131), m3494(!this.f2131), this, this.f2131, this.f2130);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m3493(C0690.C0717 r1) {
        return m3433(r1);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m3497(C0690.C0717 r1) {
        return m3440(r1);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private int m3440(C0690.C0717 r7) {
        if (m4667() == 0) {
            return 0;
        }
        return C0579.m3668(r7, this.f2124, m3484(!this.f2131), m3494(!this.f2131), this, this.f2131);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public int m3500(C0690.C0717 r1) {
        return m3440(r1);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public int m3502(C0690.C0717 r1) {
        return m3442(r1);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private int m3442(C0690.C0717 r7) {
        if (m4667() == 0) {
            return 0;
        }
        return C0579.m3670(r7, this.f2124, m3484(!this.f2131), m3494(!this.f2131), this, this.f2131);
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public int m3505(C0690.C0717 r1) {
        return m3442(r1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int, int, boolean):int
     arg types: [int, int, int, int, int]
     candidates:
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean, boolean):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int, int, boolean):int */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3430(View view, C0565 r7, boolean z) {
        if (r7.f2155) {
            if (this.f2136 == 1) {
                m3428(view, this.f2144, m4535(m4671(), m4669(), 0, r7.height, true), z);
            } else {
                m3428(view, m4535(m4670(), m4668(), 0, r7.width, true), this.f2144, z);
            }
        } else if (this.f2136 == 1) {
            m3428(view, m4535(this.f2137, m4668(), 0, r7.width, false), m4535(m4671(), m4669(), 0, r7.height, true), z);
        } else {
            m3428(view, m4535(m4670(), m4668(), 0, r7.width, true), m4535(this.f2137, m4669(), 0, r7.height, false), z);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.StaggeredGridLayoutManager.ʼ(int, int, int):int
     arg types: [int, int, int]
     candidates:
      android.support.v7.widget.StaggeredGridLayoutManager.ʼ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):void
      android.support.v7.widget.StaggeredGridLayoutManager.ʼ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.StaggeredGridLayoutManager.ʼ(android.support.v7.widget.ﹳﹳ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(int, int, int):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʼ(android.support.v7.widget.ﹳﹳ, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.ʼ(int, int, int):int */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3428(View view, int i, int i2, boolean z) {
        boolean z2;
        m4602(view, this.f2123);
        C0565 r0 = (C0565) view.getLayoutParams();
        int r6 = m3432(i, r0.leftMargin + this.f2123.left, r0.rightMargin + this.f2123.right);
        int r7 = m3432(i2, r0.topMargin + this.f2123.top, r0.bottomMargin + this.f2123.bottom);
        if (z) {
            z2 = m4589(view, r6, r7, r0);
        } else {
            z2 = m4604(view, r6, r7, r0);
        }
        if (z2) {
            view.measure(r6, r7);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int m3432(int i, int i2, int i3) {
        if (i2 == 0 && i3 == 0) {
            return i;
        }
        int mode = View.MeasureSpec.getMode(i);
        if (mode == Integer.MIN_VALUE || mode == 1073741824) {
            return View.MeasureSpec.makeMeasureSpec(Math.max(0, (View.MeasureSpec.getSize(i) - i2) - i3), mode);
        }
        return i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3469(Parcelable parcelable) {
        if (parcelable instanceof C0568) {
            this.f2143 = (C0568) parcelable;
            m4656();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Parcelable m3491() {
        int i;
        int i2;
        int i3;
        C0568 r0 = this.f2143;
        if (r0 != null) {
            return new C0568(r0);
        }
        C0568 r02 = new C0568();
        r02.f2169 = this.f2128;
        r02.f2170 = this.f2141;
        r02.f2171 = this.f2145;
        C0566 r1 = this.f2134;
        if (r1 == null || r1.f2156 == null) {
            r02.f2166 = 0;
        } else {
            r02.f2167 = this.f2134.f2156;
            r02.f2166 = r02.f2167.length;
            r02.f2168 = this.f2134.f2157;
        }
        if (m4667() > 0) {
            if (this.f2141) {
                i = m3507();
            } else {
                i = m3504();
            }
            r02.f2162 = i;
            r02.f2163 = m3510();
            int i4 = this.f2135;
            r02.f2164 = i4;
            r02.f2165 = new int[i4];
            for (int i5 = 0; i5 < this.f2135; i5++) {
                if (this.f2141) {
                    i2 = this.f2122[i5].m3551(Integer.MIN_VALUE);
                    if (i2 != Integer.MIN_VALUE) {
                        i3 = this.f2124.m4296();
                    } else {
                        r02.f2165[i5] = i2;
                    }
                } else {
                    i2 = this.f2122[i5].m3543(Integer.MIN_VALUE);
                    if (i2 != Integer.MIN_VALUE) {
                        i3 = this.f2124.m4294();
                    } else {
                        r02.f2165[i5] = i2;
                    }
                }
                i2 -= i3;
                r02.f2165[i5] = i2;
            }
        } else {
            r02.f2162 = -1;
            r02.f2163 = -1;
            r02.f2164 = 0;
        }
        return r02;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3470(C0690.C0711 r7, C0690.C0717 r8, View view, C0360 r10) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof C0565)) {
            super.m4575(view, r10);
            return;
        }
        C0565 r72 = (C0565) layoutParams;
        if (this.f2136 == 0) {
            r10.m2020(C0360.C0372.m2094(r72.m3520(), r72.f2155 ? this.f2135 : 1, -1, -1, r72.f2155, false));
        } else {
            r10.m2020(C0360.C0372.m2094(-1, -1, r72.m3520(), r72.f2155 ? this.f2135 : 1, r72.f2155, false));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3478(AccessibilityEvent accessibilityEvent) {
        super.m4578(accessibilityEvent);
        if (m4667() > 0) {
            View r1 = m3484(false);
            View r0 = m3494(false);
            if (r1 != null && r0 != null) {
                int r12 = m4620(r1);
                int r02 = m4620(r0);
                if (r12 < r02) {
                    accessibilityEvent.setFromIndex(r12);
                    accessibilityEvent.setToIndex(r02);
                    return;
                }
                accessibilityEvent.setFromIndex(r02);
                accessibilityEvent.setToIndex(r12);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public int m3510() {
        View view;
        if (this.f2130) {
            view = m3494(true);
        } else {
            view = m3484(true);
        }
        if (view == null) {
            return -1;
        }
        return m4620(view);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m3460(C0690.C0711 r2, C0690.C0717 r3) {
        if (this.f2136 == 0) {
            return this.f2135;
        }
        return super.m4546(r2, r3);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m3483(C0690.C0711 r3, C0690.C0717 r4) {
        if (this.f2136 == 1) {
            return this.f2135;
        }
        return super.m4595(r3, r4);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public View m3484(boolean z) {
        int r0 = this.f2124.m4294();
        int r1 = this.f2124.m4296();
        int r2 = m4667();
        View view = null;
        for (int i = 0; i < r2; i++) {
            View r5 = m4645(i);
            int r6 = this.f2124.m4289(r5);
            if (this.f2124.m4293(r5) > r0 && r6 < r1) {
                if (r6 >= r0 || !z) {
                    return r5;
                }
                if (view == null) {
                    view = r5;
                }
            }
        }
        return view;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public View m3494(boolean z) {
        int r0 = this.f2124.m4294();
        int r1 = this.f2124.m4296();
        View view = null;
        for (int r2 = m4667() - 1; r2 >= 0; r2--) {
            View r4 = m4645(r2);
            int r5 = this.f2124.m4289(r4);
            int r6 = this.f2124.m4293(r4);
            if (r6 > r0 && r5 < r1) {
                if (r6 <= r1 || !z) {
                    return r4;
                }
                if (view == null) {
                    view = r4;
                }
            }
        }
        return view;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m3436(C0690.C0711 r3, C0690.C0717 r4, boolean z) {
        int r0;
        int r1 = m3451(Integer.MIN_VALUE);
        if (r1 != Integer.MIN_VALUE && (r0 = this.f2124.m4296() - r1) > 0) {
            int i = r0 - (-m3489(-r0, r3, r4));
            if (z && i > 0) {
                this.f2124.m4291(i);
            }
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m3438(C0690.C0711 r3, C0690.C0717 r4, boolean z) {
        int r1;
        int r12 = m3449(Integer.MAX_VALUE);
        if (r12 != Integer.MAX_VALUE && (r1 = r12 - this.f2124.m4294()) > 0) {
            int r13 = r1 - m3489(r1, r3, r4);
            if (z && r13 > 0) {
                this.f2124.m4291(-r13);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004d  */
    /* renamed from: ʼ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m3434(int r5, android.support.v7.widget.C0690.C0717 r6) {
        /*
            r4 = this;
            android.support.v7.widget.ˏˏ r0 = r4.f2138
            r1 = 0
            r0.f2611 = r1
            r0.f2612 = r5
            boolean r0 = r4.m4663()
            r2 = 1
            if (r0 == 0) goto L_0x002e
            int r6 = r6.m4773()
            r0 = -1
            if (r6 == r0) goto L_0x002e
            boolean r0 = r4.f2130
            if (r6 >= r5) goto L_0x001b
            r5 = 1
            goto L_0x001c
        L_0x001b:
            r5 = 0
        L_0x001c:
            if (r0 != r5) goto L_0x0025
            android.support.v7.widget.ⁱⁱ r5 = r4.f2124
            int r5 = r5.m4300()
            goto L_0x002f
        L_0x0025:
            android.support.v7.widget.ⁱⁱ r5 = r4.f2124
            int r5 = r5.m4300()
            r6 = r5
            r5 = 0
            goto L_0x0030
        L_0x002e:
            r5 = 0
        L_0x002f:
            r6 = 0
        L_0x0030:
            boolean r0 = r4.m4661()
            if (r0 == 0) goto L_0x004d
            android.support.v7.widget.ˏˏ r0 = r4.f2138
            android.support.v7.widget.ⁱⁱ r3 = r4.f2124
            int r3 = r3.m4294()
            int r3 = r3 - r6
            r0.f2615 = r3
            android.support.v7.widget.ˏˏ r6 = r4.f2138
            android.support.v7.widget.ⁱⁱ r0 = r4.f2124
            int r0 = r0.m4296()
            int r0 = r0 + r5
            r6.f2616 = r0
            goto L_0x005d
        L_0x004d:
            android.support.v7.widget.ˏˏ r0 = r4.f2138
            android.support.v7.widget.ⁱⁱ r3 = r4.f2124
            int r3 = r3.m4298()
            int r3 = r3 + r5
            r0.f2616 = r3
            android.support.v7.widget.ˏˏ r5 = r4.f2138
            int r6 = -r6
            r5.f2615 = r6
        L_0x005d:
            android.support.v7.widget.ˏˏ r5 = r4.f2138
            r5.f2617 = r1
            r5.f2610 = r2
            android.support.v7.widget.ⁱⁱ r6 = r4.f2124
            int r6 = r6.m4303()
            if (r6 != 0) goto L_0x0074
            android.support.v7.widget.ⁱⁱ r6 = r4.f2124
            int r6 = r6.m4298()
            if (r6 != 0) goto L_0x0074
            r1 = 1
        L_0x0074:
            r5.f2618 = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.StaggeredGridLayoutManager.m3434(int, android.support.v7.widget.ﹳﹳ$ᵔ):void");
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private void m3444(int i) {
        C0655 r0 = this.f2138;
        r0.f2614 = i;
        int i2 = 1;
        if (this.f2130 != (i == -1)) {
            i2 = -1;
        }
        r0.f2613 = i2;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public void m3508(int i) {
        super.m4647(i);
        for (int i2 = 0; i2 < this.f2135; i2++) {
            this.f2122[i2].m3557(i);
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public void m3511(int i) {
        super.m4649(i);
        for (int i2 = 0; i2 < this.f2135; i2++) {
            this.f2122[i2].m3557(i);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3486(C0690 r1, int i, int i2) {
        m3437(i, i2, 2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3474(C0690 r1, int i, int i2) {
        m3437(i, i2, 1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3473(C0690 r1) {
        this.f2134.m3526();
        m4656();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3475(C0690 r1, int i, int i2, int i3) {
        m3437(i, i2, 8);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3476(C0690 r1, int i, int i2, Object obj) {
        m3437(i, i2, 4);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0045 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0046  */
    /* renamed from: ʽ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m3437(int r7, int r8, int r9) {
        /*
            r6 = this;
            boolean r0 = r6.f2130
            if (r0 == 0) goto L_0x0009
            int r0 = r6.m3507()
            goto L_0x000d
        L_0x0009:
            int r0 = r6.m3504()
        L_0x000d:
            r1 = 8
            if (r9 != r1) goto L_0x001b
            if (r7 >= r8) goto L_0x0016
            int r2 = r8 + 1
            goto L_0x001d
        L_0x0016:
            int r2 = r7 + 1
            r3 = r2
            r2 = r8
            goto L_0x001f
        L_0x001b:
            int r2 = r7 + r8
        L_0x001d:
            r3 = r2
            r2 = r7
        L_0x001f:
            android.support.v7.widget.StaggeredGridLayoutManager$ʽ r4 = r6.f2134
            r4.m3530(r2)
            r4 = 1
            if (r9 == r4) goto L_0x003e
            r5 = 2
            if (r9 == r5) goto L_0x0038
            if (r9 == r1) goto L_0x002d
            goto L_0x0043
        L_0x002d:
            android.support.v7.widget.StaggeredGridLayoutManager$ʽ r9 = r6.f2134
            r9.m3527(r7, r4)
            android.support.v7.widget.StaggeredGridLayoutManager$ʽ r7 = r6.f2134
            r7.m3531(r8, r4)
            goto L_0x0043
        L_0x0038:
            android.support.v7.widget.StaggeredGridLayoutManager$ʽ r9 = r6.f2134
            r9.m3527(r7, r8)
            goto L_0x0043
        L_0x003e:
            android.support.v7.widget.StaggeredGridLayoutManager$ʽ r9 = r6.f2134
            r9.m3531(r7, r8)
        L_0x0043:
            if (r3 > r0) goto L_0x0046
            return
        L_0x0046:
            boolean r7 = r6.f2130
            if (r7 == 0) goto L_0x004f
            int r7 = r6.m3504()
            goto L_0x0053
        L_0x004f:
            int r7 = r6.m3507()
        L_0x0053:
            if (r2 > r7) goto L_0x0058
            r6.m4656()
        L_0x0058:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.StaggeredGridLayoutManager.m3437(int, int, int):void");
    }

    /* JADX WARN: Type inference failed for: r9v0 */
    /* JADX WARN: Type inference failed for: r9v1, types: [boolean, int] */
    /* JADX WARN: Type inference failed for: r9v4 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.BitSet.set(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.BitSet.set(int, int):void}
      ClspMth{java.util.BitSet.set(int, boolean):void} */
    /* renamed from: ʻ  reason: contains not printable characters */
    private int m3420(C0690.C0711 r17, C0655 r18, C0690.C0717 r19) {
        int i;
        int i2;
        int i3;
        C0569 r1;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        boolean z;
        int i10;
        int i11;
        int i12;
        C0690.C0711 r7 = r17;
        C0655 r8 = r18;
        ? r9 = 0;
        this.f2139.set(0, this.f2135, true);
        if (this.f2138.f2618) {
            i = r8.f2614 == 1 ? Integer.MAX_VALUE : Integer.MIN_VALUE;
        } else {
            if (r8.f2614 == 1) {
                i12 = r8.f2616 + r8.f2611;
            } else {
                i12 = r8.f2615 - r8.f2611;
            }
            i = i12;
        }
        m3422(r8.f2614, i);
        if (this.f2130) {
            i2 = this.f2124.m4296();
        } else {
            i2 = this.f2124.m4294();
        }
        int i13 = i2;
        boolean z2 = false;
        while (r18.m4157(r19) && (this.f2138.f2618 || !this.f2139.isEmpty())) {
            View r13 = r8.m4156(r7);
            C0565 r14 = (C0565) r13.getLayoutParams();
            int r0 = r14.m4687();
            int r12 = this.f2134.m3532(r0);
            boolean z3 = r12 == -1;
            if (z3) {
                r1 = r14.f2155 ? this.f2122[r9] : m3421(r8);
                this.f2134.m3528(r0, r1);
            } else {
                r1 = this.f2122[r12];
            }
            C0569 r15 = r1;
            r14.f2154 = r15;
            if (r8.f2614 == 1) {
                m4600(r13);
            } else {
                m4601(r13, (int) r9);
            }
            m3430(r13, r14, (boolean) r9);
            if (r8.f2614 == 1) {
                if (r14.f2155) {
                    i11 = m3451(i13);
                } else {
                    i11 = r15.m3551(i13);
                }
                int r4 = this.f2124.m4299(r13) + i11;
                if (z3 && r14.f2155) {
                    C0566.C0567 r5 = m3446(i11);
                    r5.f2159 = -1;
                    r5.f2158 = r0;
                    this.f2134.m3529(r5);
                }
                i4 = r4;
                i5 = i11;
            } else {
                if (r14.f2155) {
                    i10 = m3449(i13);
                } else {
                    i10 = r15.m3543(i13);
                }
                i5 = i10 - this.f2124.m4299(r13);
                if (z3 && r14.f2155) {
                    C0566.C0567 r52 = m3447(i10);
                    r52.f2159 = 1;
                    r52.f2158 = r0;
                    this.f2134.m3529(r52);
                }
                i4 = i10;
            }
            if (r14.f2155 && r8.f2613 == -1) {
                if (z3) {
                    this.f2125 = true;
                } else {
                    if (r8.f2614 == 1) {
                        z = m3513();
                    } else {
                        z = m3514();
                    }
                    if (!z) {
                        C0566.C0567 r02 = this.f2134.m3535(r0);
                        if (r02 != null) {
                            r02.f2161 = true;
                        }
                        this.f2125 = true;
                    }
                }
            }
            m3429(r13, r14, r8);
            if (!m3509() || this.f2136 != 1) {
                if (r14.f2155) {
                    i8 = this.f2126.m4294();
                } else {
                    i8 = (r15.f2176 * this.f2137) + this.f2126.m4294();
                }
                i7 = i8;
                i6 = this.f2126.m4299(r13) + i8;
            } else {
                if (r14.f2155) {
                    i9 = this.f2126.m4296();
                } else {
                    i9 = this.f2126.m4296() - (((this.f2135 - 1) - r15.f2176) * this.f2137);
                }
                i6 = i9;
                i7 = i9 - this.f2126.m4299(r13);
            }
            if (this.f2136 == 1) {
                m4572(r13, i7, i5, i6, i4);
            } else {
                m4572(r13, i5, i7, i4, i6);
            }
            if (r14.f2155) {
                m3422(this.f2138.f2614, i);
            } else {
                m3424(r15, this.f2138.f2614, i);
            }
            m3426(r7, this.f2138);
            if (this.f2138.f2617 && r13.hasFocusable()) {
                if (r14.f2155) {
                    this.f2139.clear();
                } else {
                    this.f2139.set(r15.f2176, false);
                    z2 = true;
                    r9 = 0;
                }
            }
            z2 = true;
            r9 = 0;
        }
        if (!z2) {
            m3426(r7, this.f2138);
        }
        if (this.f2138.f2614 == -1) {
            i3 = this.f2124.m4294() - m3449(this.f2124.m4294());
        } else {
            i3 = m3451(this.f2124.m4296()) - this.f2124.m4296();
        }
        if (i3 > 0) {
            return Math.min(r8.f2611, i3);
        }
        return 0;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private C0566.C0567 m3446(int i) {
        C0566.C0567 r0 = new C0566.C0567();
        r0.f2160 = new int[this.f2135];
        for (int i2 = 0; i2 < this.f2135; i2++) {
            r0.f2160[i2] = i - this.f2122[i2].m3551(i);
        }
        return r0;
    }

    /* renamed from: י  reason: contains not printable characters */
    private C0566.C0567 m3447(int i) {
        C0566.C0567 r0 = new C0566.C0567();
        r0.f2160 = new int[this.f2135];
        for (int i2 = 0; i2 < this.f2135; i2++) {
            r0.f2160[i2] = this.f2122[i2].m3543(i) - i;
        }
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3429(View view, C0565 r3, C0655 r4) {
        if (r4.f2614 == 1) {
            if (r3.f2155) {
                m3450(view);
            } else {
                r3.f2154.m3552(view);
            }
        } else if (r3.f2155) {
            m3452(view);
        } else {
            r3.f2154.m3548(view);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3426(C0690.C0711 r3, C0655 r4) {
        int i;
        int i2;
        if (r4.f2610 && !r4.f2618) {
            if (r4.f2611 == 0) {
                if (r4.f2614 == -1) {
                    m3435(r3, r4.f2616);
                } else {
                    m3425(r3, r4.f2615);
                }
            } else if (r4.f2614 == -1) {
                int r0 = r4.f2615 - m3448(r4.f2615);
                if (r0 < 0) {
                    i2 = r4.f2616;
                } else {
                    i2 = r4.f2616 - Math.min(r0, r4.f2611);
                }
                m3435(r3, i2);
            } else {
                int r02 = m3453(r4.f2616) - r4.f2616;
                if (r02 < 0) {
                    i = r4.f2615;
                } else {
                    i = Math.min(r02, r4.f2611) + r4.f2615;
                }
                m3425(r3, i);
            }
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m3450(View view) {
        for (int i = this.f2135 - 1; i >= 0; i--) {
            this.f2122[i].m3552(view);
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m3452(View view) {
        for (int i = this.f2135 - 1; i >= 0; i--) {
            this.f2122[i].m3548(view);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3422(int i, int i2) {
        for (int i3 = 0; i3 < this.f2135; i3++) {
            if (!this.f2122[i3].f2172.isEmpty()) {
                m3424(this.f2122[i3], i, i2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.BitSet.set(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.BitSet.set(int, int):void}
      ClspMth{java.util.BitSet.set(int, boolean):void} */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3424(C0569 r4, int i, int i2) {
        int r0 = r4.m3562();
        if (i == -1) {
            if (r4.m3550() + r0 <= i2) {
                this.f2139.set(r4.f2176, false);
            }
        } else if (r4.m3556() - r0 >= i2) {
            this.f2139.set(r4.f2176, false);
        }
    }

    /* renamed from: ـ  reason: contains not printable characters */
    private int m3448(int i) {
        int r0 = this.f2122[0].m3543(i);
        for (int i2 = 1; i2 < this.f2135; i2++) {
            int r2 = this.f2122[i2].m3543(i);
            if (r2 > r0) {
                r0 = r2;
            }
        }
        return r0;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private int m3449(int i) {
        int r0 = this.f2122[0].m3543(i);
        for (int i2 = 1; i2 < this.f2135; i2++) {
            int r2 = this.f2122[i2].m3543(i);
            if (r2 < r0) {
                r0 = r2;
            }
        }
        return r0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public boolean m3513() {
        int r0 = this.f2122[0].m3551(Integer.MIN_VALUE);
        for (int i = 1; i < this.f2135; i++) {
            if (this.f2122[i].m3551(Integer.MIN_VALUE) != r0) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m3514() {
        int r0 = this.f2122[0].m3543(Integer.MIN_VALUE);
        for (int i = 1; i < this.f2135; i++) {
            if (this.f2122[i].m3543(Integer.MIN_VALUE) != r0) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int m3451(int i) {
        int r0 = this.f2122[0].m3551(i);
        for (int i2 = 1; i2 < this.f2135; i2++) {
            int r2 = this.f2122[i2].m3551(i);
            if (r2 > r0) {
                r0 = r2;
            }
        }
        return r0;
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    private int m3453(int i) {
        int r0 = this.f2122[0].m3551(i);
        for (int i2 = 1; i2 < this.f2135; i2++) {
            int r2 = this.f2122[i2].m3551(i);
            if (r2 < r0) {
                r0 = r2;
            }
        }
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3425(C0690.C0711 r6, int i) {
        while (m4667() > 0) {
            View r1 = m4645(0);
            if (this.f2124.m4293(r1) <= i && this.f2124.m4295(r1) <= i) {
                C0565 r2 = (C0565) r1.getLayoutParams();
                if (r2.f2155) {
                    int i2 = 0;
                    while (i2 < this.f2135) {
                        if (this.f2122[i2].f2172.size() != 1) {
                            i2++;
                        } else {
                            return;
                        }
                    }
                    for (int i3 = 0; i3 < this.f2135; i3++) {
                        this.f2122[i3].m3561();
                    }
                } else if (r2.f2154.f2172.size() != 1) {
                    r2.f2154.m3561();
                } else {
                    return;
                }
                m4576(r1, r6);
            } else {
                return;
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m3435(C0690.C0711 r7, int i) {
        int r0 = m4667() - 1;
        while (r0 >= 0) {
            View r2 = m4645(r0);
            if (this.f2124.m4289(r2) >= i && this.f2124.m4297(r2) >= i) {
                C0565 r3 = (C0565) r2.getLayoutParams();
                if (r3.f2155) {
                    int i2 = 0;
                    while (i2 < this.f2135) {
                        if (this.f2122[i2].f2172.size() != 1) {
                            i2++;
                        } else {
                            return;
                        }
                    }
                    for (int i3 = 0; i3 < this.f2135; i3++) {
                        this.f2122[i3].m3560();
                    }
                } else if (r3.f2154.f2172.size() != 1) {
                    r3.f2154.m3560();
                } else {
                    return;
                }
                m4576(r2, r7);
                r0--;
            } else {
                return;
            }
        }
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean m3454(int i) {
        if (this.f2136 == 0) {
            if ((i == -1) != this.f2130) {
                return true;
            }
            return false;
        }
        if (((i == -1) == this.f2130) == m3509()) {
            return true;
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private C0569 m3421(C0655 r8) {
        int i;
        int i2;
        int i3 = -1;
        if (m3454(r8.f2614)) {
            i2 = this.f2135 - 1;
            i = -1;
        } else {
            i2 = 0;
            i3 = this.f2135;
            i = 1;
        }
        C0569 r4 = null;
        if (r8.f2614 == 1) {
            int i4 = Integer.MAX_VALUE;
            int r2 = this.f2124.m4294();
            while (i2 != i3) {
                C0569 r5 = this.f2122[i2];
                int r6 = r5.m3551(r2);
                if (r6 < i4) {
                    r4 = r5;
                    i4 = r6;
                }
                i2 += i;
            }
            return r4;
        }
        int i5 = Integer.MIN_VALUE;
        int r22 = this.f2124.m4296();
        while (i2 != i3) {
            C0569 r52 = this.f2122[i2];
            int r62 = r52.m3543(r22);
            if (r62 > i5) {
                r4 = r52;
                i5 = r62;
            }
            i2 += i;
        }
        return r4;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m3499() {
        return this.f2136 == 1;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m3496() {
        return this.f2136 == 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m3459(int i, C0690.C0711 r2, C0690.C0717 r3) {
        return m3489(i, r2, r3);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m3482(int i, C0690.C0711 r2, C0690.C0717 r3) {
        return m3489(i, r2, r3);
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    private int m3455(int i) {
        if (m4667() != 0) {
            if ((i < m3504()) != this.f2130) {
                return -1;
            }
            return 1;
        } else if (this.f2130) {
            return 1;
        } else {
            return -1;
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m3495(int i) {
        C0568 r0 = this.f2143;
        if (!(r0 == null || r0.f2162 == i)) {
            this.f2143.m3540();
        }
        this.f2132 = i;
        this.f2133 = Integer.MIN_VALUE;
        m4656();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3466(int i, int i2, C0690.C0717 r7, C0690.C0701.C0702 r8) {
        int i3;
        int i4;
        if (this.f2136 != 0) {
            i = i2;
        }
        if (m4667() != 0 && i != 0) {
            m3467(i, r7);
            int[] iArr = this.f2129;
            if (iArr == null || iArr.length < this.f2135) {
                this.f2129 = new int[this.f2135];
            }
            int i5 = 0;
            for (int i6 = 0; i6 < this.f2135; i6++) {
                if (this.f2138.f2613 == -1) {
                    i4 = this.f2138.f2615;
                    i3 = this.f2122[i6].m3543(this.f2138.f2615);
                } else {
                    i4 = this.f2122[i6].m3551(this.f2138.f2616);
                    i3 = this.f2138.f2616;
                }
                int i7 = i4 - i3;
                if (i7 >= 0) {
                    this.f2129[i5] = i7;
                    i5++;
                }
            }
            Arrays.sort(this.f2129, 0, i5);
            for (int i8 = 0; i8 < i5 && this.f2138.m4157(r7); i8++) {
                r8.m4683(this.f2138.f2612, this.f2129[i8]);
                this.f2138.f2612 += this.f2138.f2613;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3467(int i, C0690.C0717 r6) {
        int i2;
        int i3;
        if (i > 0) {
            i3 = m3507();
            i2 = 1;
        } else {
            i3 = m3504();
            i2 = -1;
        }
        this.f2138.f2610 = true;
        m3434(i3, r6);
        m3444(i2);
        C0655 r62 = this.f2138;
        r62.f2612 = i3 + r62.f2613;
        this.f2138.f2611 = Math.abs(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public int m3489(int i, C0690.C0711 r4, C0690.C0717 r5) {
        if (m4667() == 0 || i == 0) {
            return 0;
        }
        m3467(i, r5);
        int r52 = m3420(r4, this.f2138, r5);
        if (this.f2138.f2611 >= r52) {
            i = i < 0 ? -r52 : r52;
        }
        this.f2124.m4291(-i);
        this.f2141 = this.f2130;
        C0655 r53 = this.f2138;
        r53.f2611 = 0;
        m3426(r4, r53);
        return i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉˉ  reason: contains not printable characters */
    public int m3507() {
        int r0 = m4667();
        if (r0 == 0) {
            return 0;
        }
        return m4620(m4645(r0 - 1));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈˈ  reason: contains not printable characters */
    public int m3504() {
        if (m4667() == 0) {
            return 0;
        }
        return m4620(m4645(0));
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    private int m3456(int i) {
        int r0 = m4667();
        for (int i2 = 0; i2 < r0; i2++) {
            int r3 = m4620(m4645(i2));
            if (r3 >= 0 && r3 < i) {
                return r3;
            }
        }
        return 0;
    }

    /* renamed from: ⁱ  reason: contains not printable characters */
    private int m3457(int i) {
        for (int r0 = m4667() - 1; r0 >= 0; r0--) {
            int r1 = m4620(m4645(r0));
            if (r1 >= 0 && r1 < i) {
                return r1;
            }
        }
        return 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0690.C0704 m3461() {
        if (this.f2136 == 0) {
            return new C0565(-2, -1);
        }
        return new C0565(-1, -2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0690.C0704 m3462(Context context, AttributeSet attributeSet) {
        return new C0565(context, attributeSet);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0690.C0704 m3463(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new C0565((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new C0565(layoutParams);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3481(C0690.C0704 r1) {
        return r1 instanceof C0565;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public View m3464(View view, int i, C0690.C0711 r11, C0690.C0717 r12) {
        View r9;
        int i2;
        int i3;
        int i4;
        int i5;
        View r112;
        if (m4667() == 0 || (r9 = m4628(view)) == null) {
            return null;
        }
        m3441();
        int r10 = m3458(i);
        if (r10 == Integer.MIN_VALUE) {
            return null;
        }
        C0565 r0 = (C0565) r9.getLayoutParams();
        boolean z = r0.f2155;
        C0569 r02 = r0.f2154;
        if (r10 == 1) {
            i2 = m3507();
        } else {
            i2 = m3504();
        }
        m3434(i2, r12);
        m3444(r10);
        C0655 r5 = this.f2138;
        r5.f2612 = r5.f2613 + i2;
        this.f2138.f2611 = (int) (((float) this.f2124.m4300()) * 0.33333334f);
        C0655 r52 = this.f2138;
        r52.f2617 = true;
        r52.f2610 = false;
        m3420(r11, r52, r12);
        this.f2141 = this.f2130;
        if (!z && (r112 = r02.m3546(i2, r10)) != null && r112 != r9) {
            return r112;
        }
        if (m3454(r10)) {
            for (int i6 = this.f2135 - 1; i6 >= 0; i6--) {
                View r122 = this.f2122[i6].m3546(i2, r10);
                if (r122 != null && r122 != r9) {
                    return r122;
                }
            }
        } else {
            for (int i7 = 0; i7 < this.f2135; i7++) {
                View r123 = this.f2122[i7].m3546(i2, r10);
                if (r123 != null && r123 != r9) {
                    return r123;
                }
            }
        }
        boolean z2 = (this.f2128 ^ true) == (r10 == -1);
        if (!z) {
            if (z2) {
                i5 = r02.m3563();
            } else {
                i5 = r02.m3564();
            }
            View r124 = m4608(i5);
            if (!(r124 == null || r124 == r9)) {
                return r124;
            }
        }
        if (m3454(r10)) {
            for (int i8 = this.f2135 - 1; i8 >= 0; i8--) {
                if (i8 != r02.f2176) {
                    if (z2) {
                        i4 = this.f2122[i8].m3563();
                    } else {
                        i4 = this.f2122[i8].m3564();
                    }
                    View r125 = m4608(i4);
                    if (!(r125 == null || r125 == r9)) {
                        return r125;
                    }
                }
            }
        } else {
            for (int i9 = 0; i9 < this.f2135; i9++) {
                if (z2) {
                    i3 = this.f2122[i9].m3563();
                } else {
                    i3 = this.f2122[i9].m3564();
                }
                View r102 = m4608(i3);
                if (r102 != null && r102 != r9) {
                    return r102;
                }
            }
        }
        return null;
    }

    /* renamed from: ﹳ  reason: contains not printable characters */
    private int m3458(int i) {
        if (i == 1) {
            return (this.f2136 != 1 && m3509()) ? 1 : -1;
        }
        if (i == 2) {
            return (this.f2136 != 1 && m3509()) ? -1 : 1;
        }
        if (i != 17) {
            if (i != 33) {
                if (i != 66) {
                    return (i == 130 && this.f2136 == 1) ? 1 : Integer.MIN_VALUE;
                }
                if (this.f2136 == 0) {
                    return 1;
                }
                return Integer.MIN_VALUE;
            } else if (this.f2136 == 1) {
                return -1;
            } else {
                return Integer.MIN_VALUE;
            }
        } else if (this.f2136 == 0) {
            return -1;
        } else {
            return Integer.MIN_VALUE;
        }
    }

    /* renamed from: android.support.v7.widget.StaggeredGridLayoutManager$ʼ  reason: contains not printable characters */
    public static class C0565 extends C0690.C0704 {

        /* renamed from: ʻ  reason: contains not printable characters */
        C0569 f2154;

        /* renamed from: ʼ  reason: contains not printable characters */
        boolean f2155;

        public C0565(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public C0565(int i, int i2) {
            super(i, i2);
        }

        public C0565(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public C0565(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m3519() {
            return this.f2155;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public final int m3520() {
            C0569 r0 = this.f2154;
            if (r0 == null) {
                return -1;
            }
            return r0.f2176;
        }
    }

    /* renamed from: android.support.v7.widget.StaggeredGridLayoutManager$ʿ  reason: contains not printable characters */
    class C0569 {

        /* renamed from: ʻ  reason: contains not printable characters */
        ArrayList<View> f2172 = new ArrayList<>();

        /* renamed from: ʼ  reason: contains not printable characters */
        int f2173 = Integer.MIN_VALUE;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f2174 = Integer.MIN_VALUE;

        /* renamed from: ʾ  reason: contains not printable characters */
        int f2175 = 0;

        /* renamed from: ʿ  reason: contains not printable characters */
        final int f2176;

        C0569(int i) {
            this.f2176 = i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public int m3543(int i) {
            int i2 = this.f2173;
            if (i2 != Integer.MIN_VALUE) {
                return i2;
            }
            if (this.f2172.size() == 0) {
                return i;
            }
            m3547();
            return this.f2173;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3547() {
            C0566.C0567 r0;
            View view = this.f2172.get(0);
            C0565 r1 = m3553(view);
            this.f2173 = StaggeredGridLayoutManager.this.f2124.m4289(view);
            if (r1.f2155 && (r0 = StaggeredGridLayoutManager.this.f2134.m3535(r1.m4687())) != null && r0.f2159 == -1) {
                this.f2173 -= r0.m3536(this.f2176);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public int m3550() {
            int i = this.f2173;
            if (i != Integer.MIN_VALUE) {
                return i;
            }
            m3547();
            return this.f2173;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public int m3551(int i) {
            int i2 = this.f2174;
            if (i2 != Integer.MIN_VALUE) {
                return i2;
            }
            if (this.f2172.size() == 0) {
                return i;
            }
            m3554();
            return this.f2174;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public void m3554() {
            C0566.C0567 r0;
            ArrayList<View> arrayList = this.f2172;
            View view = arrayList.get(arrayList.size() - 1);
            C0565 r1 = m3553(view);
            this.f2174 = StaggeredGridLayoutManager.this.f2124.m4293(view);
            if (r1.f2155 && (r0 = StaggeredGridLayoutManager.this.f2134.m3535(r1.m4687())) != null && r0.f2159 == 1) {
                this.f2174 += r0.m3536(this.f2176);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʾ  reason: contains not printable characters */
        public int m3556() {
            int i = this.f2174;
            if (i != Integer.MIN_VALUE) {
                return i;
            }
            m3554();
            return this.f2174;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3548(View view) {
            C0565 r0 = m3553(view);
            r0.f2154 = this;
            this.f2172.add(0, view);
            this.f2173 = Integer.MIN_VALUE;
            if (this.f2172.size() == 1) {
                this.f2174 = Integer.MIN_VALUE;
            }
            if (r0.m4685() || r0.m4686()) {
                this.f2175 += StaggeredGridLayoutManager.this.f2124.m4299(view);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m3552(View view) {
            C0565 r0 = m3553(view);
            r0.f2154 = this;
            this.f2172.add(view);
            this.f2174 = Integer.MIN_VALUE;
            if (this.f2172.size() == 1) {
                this.f2173 = Integer.MIN_VALUE;
            }
            if (r0.m4685() || r0.m4686()) {
                this.f2175 += StaggeredGridLayoutManager.this.f2124.m4299(view);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3549(boolean z, int i) {
            int i2;
            if (z) {
                i2 = m3551(Integer.MIN_VALUE);
            } else {
                i2 = m3543(Integer.MIN_VALUE);
            }
            m3558();
            if (i2 != Integer.MIN_VALUE) {
                if (z && i2 < StaggeredGridLayoutManager.this.f2124.m4296()) {
                    return;
                }
                if (z || i2 <= StaggeredGridLayoutManager.this.f2124.m4294()) {
                    if (i != Integer.MIN_VALUE) {
                        i2 += i;
                    }
                    this.f2174 = i2;
                    this.f2173 = i2;
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʿ  reason: contains not printable characters */
        public void m3558() {
            this.f2172.clear();
            m3559();
            this.f2175 = 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˆ  reason: contains not printable characters */
        public void m3559() {
            this.f2173 = Integer.MIN_VALUE;
            this.f2174 = Integer.MIN_VALUE;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public void m3555(int i) {
            this.f2173 = i;
            this.f2174 = i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˈ  reason: contains not printable characters */
        public void m3560() {
            int size = this.f2172.size();
            View remove = this.f2172.remove(size - 1);
            C0565 r2 = m3553(remove);
            r2.f2154 = null;
            if (r2.m4685() || r2.m4686()) {
                this.f2175 -= StaggeredGridLayoutManager.this.f2124.m4299(remove);
            }
            if (size == 1) {
                this.f2173 = Integer.MIN_VALUE;
            }
            this.f2174 = Integer.MIN_VALUE;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˉ  reason: contains not printable characters */
        public void m3561() {
            View remove = this.f2172.remove(0);
            C0565 r1 = m3553(remove);
            r1.f2154 = null;
            if (this.f2172.size() == 0) {
                this.f2174 = Integer.MIN_VALUE;
            }
            if (r1.m4685() || r1.m4686()) {
                this.f2175 -= StaggeredGridLayoutManager.this.f2124.m4299(remove);
            }
            this.f2173 = Integer.MIN_VALUE;
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public int m3562() {
            return this.f2175;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public C0565 m3553(View view) {
            return (C0565) view.getLayoutParams();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʾ  reason: contains not printable characters */
        public void m3557(int i) {
            int i2 = this.f2173;
            if (i2 != Integer.MIN_VALUE) {
                this.f2173 = i2 + i;
            }
            int i3 = this.f2174;
            if (i3 != Integer.MIN_VALUE) {
                this.f2174 = i3 + i;
            }
        }

        /* renamed from: ˋ  reason: contains not printable characters */
        public int m3563() {
            if (StaggeredGridLayoutManager.this.f2128) {
                return m3544(this.f2172.size() - 1, -1, true);
            }
            return m3544(0, this.f2172.size(), true);
        }

        /* renamed from: ˎ  reason: contains not printable characters */
        public int m3564() {
            if (StaggeredGridLayoutManager.this.f2128) {
                return m3544(0, this.f2172.size(), true);
            }
            return m3544(this.f2172.size() - 1, -1, true);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public int m3545(int i, int i2, boolean z, boolean z2, boolean z3) {
            int r0 = StaggeredGridLayoutManager.this.f2124.m4294();
            int r1 = StaggeredGridLayoutManager.this.f2124.m4296();
            int i3 = i2 > i ? 1 : -1;
            while (i != i2) {
                View view = this.f2172.get(i);
                int r6 = StaggeredGridLayoutManager.this.f2124.m4289(view);
                int r7 = StaggeredGridLayoutManager.this.f2124.m4293(view);
                boolean z4 = false;
                boolean z5 = !z3 ? r6 < r1 : r6 <= r1;
                if (!z3 ? r7 > r0 : r7 >= r0) {
                    z4 = true;
                }
                if (z5 && z4) {
                    if (!z || !z2) {
                        if (z2) {
                            return StaggeredGridLayoutManager.this.m4620(view);
                        }
                        if (r6 < r0 || r7 > r1) {
                            return StaggeredGridLayoutManager.this.m4620(view);
                        }
                    } else if (r6 >= r0 && r7 <= r1) {
                        return StaggeredGridLayoutManager.this.m4620(view);
                    }
                }
                i += i3;
            }
            return -1;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public int m3544(int i, int i2, boolean z) {
            return m3545(i, i2, false, false, z);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public View m3546(int i, int i2) {
            View view = null;
            if (i2 != -1) {
                int size = this.f2172.size() - 1;
                while (size >= 0) {
                    View view2 = this.f2172.get(size);
                    if ((StaggeredGridLayoutManager.this.f2128 && StaggeredGridLayoutManager.this.m4620(view2) >= i) || ((!StaggeredGridLayoutManager.this.f2128 && StaggeredGridLayoutManager.this.m4620(view2) <= i) || !view2.hasFocusable())) {
                        break;
                    }
                    size--;
                    view = view2;
                }
            } else {
                int size2 = this.f2172.size();
                int i3 = 0;
                while (i3 < size2) {
                    View view3 = this.f2172.get(i3);
                    if ((StaggeredGridLayoutManager.this.f2128 && StaggeredGridLayoutManager.this.m4620(view3) <= i) || ((!StaggeredGridLayoutManager.this.f2128 && StaggeredGridLayoutManager.this.m4620(view3) >= i) || !view3.hasFocusable())) {
                        break;
                    }
                    i3++;
                    view = view3;
                }
            }
            return view;
        }
    }

    /* renamed from: android.support.v7.widget.StaggeredGridLayoutManager$ʽ  reason: contains not printable characters */
    static class C0566 {

        /* renamed from: ʻ  reason: contains not printable characters */
        int[] f2156;

        /* renamed from: ʼ  reason: contains not printable characters */
        List<C0567> f2157;

        C0566() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public int m3524(int i) {
            List<C0567> list = this.f2157;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    if (this.f2157.get(size).f2158 >= i) {
                        this.f2157.remove(size);
                    }
                }
            }
            return m3530(i);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public int m3530(int i) {
            int[] iArr = this.f2156;
            if (iArr == null || i >= iArr.length) {
                return -1;
            }
            int r0 = m3523(i);
            if (r0 == -1) {
                int[] iArr2 = this.f2156;
                Arrays.fill(iArr2, i, iArr2.length, -1);
                return this.f2156.length;
            }
            int i2 = r0 + 1;
            Arrays.fill(this.f2156, i, i2, -1);
            return i2;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public int m3532(int i) {
            int[] iArr = this.f2156;
            if (iArr == null || i >= iArr.length) {
                return -1;
            }
            return iArr[i];
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3528(int i, C0569 r3) {
            m3534(i);
            this.f2156[i] = r3.f2176;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʾ  reason: contains not printable characters */
        public int m3533(int i) {
            int length = this.f2156.length;
            while (length <= i) {
                length *= 2;
            }
            return length;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʿ  reason: contains not printable characters */
        public void m3534(int i) {
            int[] iArr = this.f2156;
            if (iArr == null) {
                this.f2156 = new int[(Math.max(i, 10) + 1)];
                Arrays.fill(this.f2156, -1);
            } else if (i >= iArr.length) {
                this.f2156 = new int[m3533(i)];
                System.arraycopy(iArr, 0, this.f2156, 0, iArr.length);
                int[] iArr2 = this.f2156;
                Arrays.fill(iArr2, iArr.length, iArr2.length, -1);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3526() {
            int[] iArr = this.f2156;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            this.f2157 = null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3527(int i, int i2) {
            int[] iArr = this.f2156;
            if (iArr != null && i < iArr.length) {
                int i3 = i + i2;
                m3534(i3);
                int[] iArr2 = this.f2156;
                System.arraycopy(iArr2, i3, iArr2, i, (iArr2.length - i) - i2);
                int[] iArr3 = this.f2156;
                Arrays.fill(iArr3, iArr3.length - i2, iArr3.length, -1);
                m3521(i, i2);
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        private void m3521(int i, int i2) {
            List<C0567> list = this.f2157;
            if (list != null) {
                int i3 = i + i2;
                for (int size = list.size() - 1; size >= 0; size--) {
                    C0567 r2 = this.f2157.get(size);
                    if (r2.f2158 >= i) {
                        if (r2.f2158 < i3) {
                            this.f2157.remove(size);
                        } else {
                            r2.f2158 -= i2;
                        }
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m3531(int i, int i2) {
            int[] iArr = this.f2156;
            if (iArr != null && i < iArr.length) {
                int i3 = i + i2;
                m3534(i3);
                int[] iArr2 = this.f2156;
                System.arraycopy(iArr2, i, iArr2, i3, (iArr2.length - i) - i2);
                Arrays.fill(this.f2156, i, i3, -1);
                m3522(i, i2);
            }
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        private void m3522(int i, int i2) {
            List<C0567> list = this.f2157;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    C0567 r1 = this.f2157.get(size);
                    if (r1.f2158 >= i) {
                        r1.f2158 += i2;
                    }
                }
            }
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        private int m3523(int i) {
            if (this.f2157 == null) {
                return -1;
            }
            C0567 r0 = m3535(i);
            if (r0 != null) {
                this.f2157.remove(r0);
            }
            int size = this.f2157.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    i2 = -1;
                    break;
                } else if (this.f2157.get(i2).f2158 >= i) {
                    break;
                } else {
                    i2++;
                }
            }
            if (i2 == -1) {
                return -1;
            }
            this.f2157.remove(i2);
            return this.f2157.get(i2).f2158;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3529(C0567 r6) {
            if (this.f2157 == null) {
                this.f2157 = new ArrayList();
            }
            int size = this.f2157.size();
            for (int i = 0; i < size; i++) {
                C0567 r2 = this.f2157.get(i);
                if (r2.f2158 == r6.f2158) {
                    this.f2157.remove(i);
                }
                if (r2.f2158 >= r6.f2158) {
                    this.f2157.add(i, r6);
                    return;
                }
            }
            this.f2157.add(r6);
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public C0567 m3535(int i) {
            List<C0567> list = this.f2157;
            if (list == null) {
                return null;
            }
            for (int size = list.size() - 1; size >= 0; size--) {
                C0567 r2 = this.f2157.get(size);
                if (r2.f2158 == i) {
                    return r2;
                }
            }
            return null;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0567 m3525(int i, int i2, int i3, boolean z) {
            List<C0567> list = this.f2157;
            if (list == null) {
                return null;
            }
            int size = list.size();
            for (int i4 = 0; i4 < size; i4++) {
                C0567 r3 = this.f2157.get(i4);
                if (r3.f2158 >= i2) {
                    return null;
                }
                if (r3.f2158 >= i && (i3 == 0 || r3.f2159 == i3 || (z && r3.f2161))) {
                    return r3;
                }
            }
            return null;
        }

        /* renamed from: android.support.v7.widget.StaggeredGridLayoutManager$ʽ$ʻ  reason: contains not printable characters */
        static class C0567 implements Parcelable {
            public static final Parcelable.Creator<C0567> CREATOR = new Parcelable.Creator<C0567>() {
                /* renamed from: ʻ  reason: contains not printable characters */
                public C0567 createFromParcel(Parcel parcel) {
                    return new C0567(parcel);
                }

                /* renamed from: ʻ  reason: contains not printable characters */
                public C0567[] newArray(int i) {
                    return new C0567[i];
                }
            };

            /* renamed from: ʻ  reason: contains not printable characters */
            int f2158;

            /* renamed from: ʼ  reason: contains not printable characters */
            int f2159;

            /* renamed from: ʽ  reason: contains not printable characters */
            int[] f2160;

            /* renamed from: ʾ  reason: contains not printable characters */
            boolean f2161;

            public int describeContents() {
                return 0;
            }

            C0567(Parcel parcel) {
                this.f2158 = parcel.readInt();
                this.f2159 = parcel.readInt();
                this.f2161 = parcel.readInt() != 1 ? false : true;
                int readInt = parcel.readInt();
                if (readInt > 0) {
                    this.f2160 = new int[readInt];
                    parcel.readIntArray(this.f2160);
                }
            }

            C0567() {
            }

            /* access modifiers changed from: package-private */
            /* renamed from: ʻ  reason: contains not printable characters */
            public int m3536(int i) {
                int[] iArr = this.f2160;
                if (iArr == null) {
                    return 0;
                }
                return iArr[i];
            }

            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(this.f2158);
                parcel.writeInt(this.f2159);
                parcel.writeInt(this.f2161 ? 1 : 0);
                int[] iArr = this.f2160;
                if (iArr == null || iArr.length <= 0) {
                    parcel.writeInt(0);
                    return;
                }
                parcel.writeInt(iArr.length);
                parcel.writeIntArray(this.f2160);
            }

            public String toString() {
                return "FullSpanItem{mPosition=" + this.f2158 + ", mGapDir=" + this.f2159 + ", mHasUnwantedGapAfter=" + this.f2161 + ", mGapPerSpan=" + Arrays.toString(this.f2160) + '}';
            }
        }
    }

    /* renamed from: android.support.v7.widget.StaggeredGridLayoutManager$ʾ  reason: contains not printable characters */
    public static class C0568 implements Parcelable {
        public static final Parcelable.Creator<C0568> CREATOR = new Parcelable.Creator<C0568>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C0568 createFromParcel(Parcel parcel) {
                return new C0568(parcel);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0568[] newArray(int i) {
                return new C0568[i];
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        int f2162;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f2163;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f2164;

        /* renamed from: ʾ  reason: contains not printable characters */
        int[] f2165;

        /* renamed from: ʿ  reason: contains not printable characters */
        int f2166;

        /* renamed from: ˆ  reason: contains not printable characters */
        int[] f2167;

        /* renamed from: ˈ  reason: contains not printable characters */
        List<C0566.C0567> f2168;

        /* renamed from: ˉ  reason: contains not printable characters */
        boolean f2169;

        /* renamed from: ˊ  reason: contains not printable characters */
        boolean f2170;

        /* renamed from: ˋ  reason: contains not printable characters */
        boolean f2171;

        public int describeContents() {
            return 0;
        }

        public C0568() {
        }

        C0568(Parcel parcel) {
            this.f2162 = parcel.readInt();
            this.f2163 = parcel.readInt();
            this.f2164 = parcel.readInt();
            int i = this.f2164;
            if (i > 0) {
                this.f2165 = new int[i];
                parcel.readIntArray(this.f2165);
            }
            this.f2166 = parcel.readInt();
            int i2 = this.f2166;
            if (i2 > 0) {
                this.f2167 = new int[i2];
                parcel.readIntArray(this.f2167);
            }
            boolean z = false;
            this.f2169 = parcel.readInt() == 1;
            this.f2170 = parcel.readInt() == 1;
            this.f2171 = parcel.readInt() == 1 ? true : z;
            this.f2168 = parcel.readArrayList(C0566.C0567.class.getClassLoader());
        }

        public C0568(C0568 r2) {
            this.f2164 = r2.f2164;
            this.f2162 = r2.f2162;
            this.f2163 = r2.f2163;
            this.f2165 = r2.f2165;
            this.f2166 = r2.f2166;
            this.f2167 = r2.f2167;
            this.f2169 = r2.f2169;
            this.f2170 = r2.f2170;
            this.f2171 = r2.f2171;
            this.f2168 = r2.f2168;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3539() {
            this.f2165 = null;
            this.f2164 = 0;
            this.f2166 = 0;
            this.f2167 = null;
            this.f2168 = null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m3540() {
            this.f2165 = null;
            this.f2164 = 0;
            this.f2162 = -1;
            this.f2163 = -1;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f2162);
            parcel.writeInt(this.f2163);
            parcel.writeInt(this.f2164);
            if (this.f2164 > 0) {
                parcel.writeIntArray(this.f2165);
            }
            parcel.writeInt(this.f2166);
            if (this.f2166 > 0) {
                parcel.writeIntArray(this.f2167);
            }
            parcel.writeInt(this.f2169 ? 1 : 0);
            parcel.writeInt(this.f2170 ? 1 : 0);
            parcel.writeInt(this.f2171 ? 1 : 0);
            parcel.writeList(this.f2168);
        }
    }

    /* renamed from: android.support.v7.widget.StaggeredGridLayoutManager$ʻ  reason: contains not printable characters */
    class C0564 {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f2147;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f2148;

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean f2149;

        /* renamed from: ʾ  reason: contains not printable characters */
        boolean f2150;

        /* renamed from: ʿ  reason: contains not printable characters */
        boolean f2151;

        /* renamed from: ˆ  reason: contains not printable characters */
        int[] f2152;

        C0564() {
            m3515();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3515() {
            this.f2147 = -1;
            this.f2148 = Integer.MIN_VALUE;
            this.f2149 = false;
            this.f2150 = false;
            this.f2151 = false;
            int[] iArr = this.f2152;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3517(C0569[] r6) {
            int length = r6.length;
            int[] iArr = this.f2152;
            if (iArr == null || iArr.length < length) {
                this.f2152 = new int[StaggeredGridLayoutManager.this.f2122.length];
            }
            for (int i = 0; i < length; i++) {
                this.f2152[i] = r6[i].m3543(Integer.MIN_VALUE);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m3518() {
            int i;
            if (this.f2149) {
                i = StaggeredGridLayoutManager.this.f2124.m4296();
            } else {
                i = StaggeredGridLayoutManager.this.f2124.m4294();
            }
            this.f2148 = i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3516(int i) {
            if (this.f2149) {
                this.f2148 = StaggeredGridLayoutManager.this.f2124.m4296() - i;
            } else {
                this.f2148 = StaggeredGridLayoutManager.this.f2124.m4294() + i;
            }
        }
    }
}
