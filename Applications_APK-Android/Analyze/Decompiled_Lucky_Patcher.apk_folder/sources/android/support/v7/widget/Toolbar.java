package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.ˉ.C0355;
import android.support.v4.ˉ.C0396;
import android.support.v4.ˉ.C0400;
import android.support.v4.ˉ.C0414;
import android.support.v7.app.C0444;
import android.support.v7.view.C0531;
import android.support.v7.view.C0536;
import android.support.v7.view.menu.C0504;
import android.support.v7.view.menu.C0508;
import android.support.v7.view.menu.C0518;
import android.support.v7.view.menu.C0526;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.ʻ.C0727;
import android.support.v7.ʼ.ʻ.C0739;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class Toolbar extends ViewGroup {

    /* renamed from: ʻ  reason: contains not printable characters */
    ImageButton f2178;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private boolean f2179;

    /* renamed from: ʼ  reason: contains not printable characters */
    View f2180;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private final ArrayList<View> f2181;

    /* renamed from: ʽ  reason: contains not printable characters */
    int f2182;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private boolean f2183;

    /* renamed from: ʾ  reason: contains not printable characters */
    C0572 f2184;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private final int[] f2185;

    /* renamed from: ʿ  reason: contains not printable characters */
    private ActionMenuView f2186;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private final ArrayList<View> f2187;

    /* renamed from: ˆ  reason: contains not printable characters */
    private TextView f2188;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private C0593 f2189;

    /* renamed from: ˈ  reason: contains not printable characters */
    private TextView f2190;

    /* renamed from: ˈˈ  reason: contains not printable characters */
    private C0570 f2191;

    /* renamed from: ˉ  reason: contains not printable characters */
    private ImageButton f2192;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private C0614 f2193;

    /* renamed from: ˊ  reason: contains not printable characters */
    private ImageView f2194;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private C0504.C0505 f2195;

    /* renamed from: ˋ  reason: contains not printable characters */
    private Drawable f2196;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private C0518.C0519 f2197;

    /* renamed from: ˎ  reason: contains not printable characters */
    private CharSequence f2198;

    /* renamed from: ˎˎ  reason: contains not printable characters */
    private final Runnable f2199;

    /* renamed from: ˏ  reason: contains not printable characters */
    private Context f2200;

    /* renamed from: ˏˏ  reason: contains not printable characters */
    private boolean f2201;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f2202;

    /* renamed from: י  reason: contains not printable characters */
    private int f2203;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f2204;

    /* renamed from: ــ  reason: contains not printable characters */
    private final ActionMenuView.C0547 f2205;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f2206;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f2207;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private int f2208;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private int f2209;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private int f2210;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private int f2211;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private int f2212;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private C0578 f2213;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private int f2214;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private int f2215;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int f2216;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private CharSequence f2217;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private CharSequence f2218;

    /* renamed from: android.support.v7.widget.Toolbar$ʽ  reason: contains not printable characters */
    public interface C0572 {
        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m3618(MenuItem menuItem);
    }

    public Toolbar(Context context) {
        this(context, null);
    }

    public Toolbar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0727.C0728.toolbarStyle);
    }

    public Toolbar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2216 = 8388627;
        this.f2181 = new ArrayList<>();
        this.f2187 = new ArrayList<>();
        this.f2185 = new int[2];
        this.f2205 = new ActionMenuView.C0547() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public boolean m3605(MenuItem menuItem) {
                if (Toolbar.this.f2184 != null) {
                    return Toolbar.this.f2184.m3618(menuItem);
                }
                return false;
            }
        };
        this.f2199 = new Runnable() {
            public void run() {
                Toolbar.this.m3596();
            }
        };
        C0592 r7 = C0592.m3740(getContext(), attributeSet, C0727.C0737.Toolbar, i, 0);
        this.f2203 = r7.m3757(C0727.C0737.Toolbar_titleTextAppearance, 0);
        this.f2204 = r7.m3757(C0727.C0737.Toolbar_subtitleTextAppearance, 0);
        this.f2216 = r7.m3749(C0727.C0737.Toolbar_android_gravity, this.f2216);
        this.f2182 = r7.m3749(C0727.C0737.Toolbar_buttonGravity, 48);
        int r8 = r7.m3751(C0727.C0737.Toolbar_titleMargin, 0);
        r8 = r7.m3758(C0727.C0737.Toolbar_titleMargins) ? r7.m3751(C0727.C0737.Toolbar_titleMargins, r8) : r8;
        this.f2212 = r8;
        this.f2211 = r8;
        this.f2209 = r8;
        this.f2207 = r8;
        int r82 = r7.m3751(C0727.C0737.Toolbar_titleMarginStart, -1);
        if (r82 >= 0) {
            this.f2207 = r82;
        }
        int r83 = r7.m3751(C0727.C0737.Toolbar_titleMarginEnd, -1);
        if (r83 >= 0) {
            this.f2209 = r83;
        }
        int r84 = r7.m3751(C0727.C0737.Toolbar_titleMarginTop, -1);
        if (r84 >= 0) {
            this.f2211 = r84;
        }
        int r85 = r7.m3751(C0727.C0737.Toolbar_titleMarginBottom, -1);
        if (r85 >= 0) {
            this.f2212 = r85;
        }
        this.f2206 = r7.m3753(C0727.C0737.Toolbar_maxButtonHeight, -1);
        int r86 = r7.m3751(C0727.C0737.Toolbar_contentInsetStart, Integer.MIN_VALUE);
        int r2 = r7.m3751(C0727.C0737.Toolbar_contentInsetEnd, Integer.MIN_VALUE);
        int r3 = r7.m3753(C0727.C0737.Toolbar_contentInsetLeft, 0);
        int r4 = r7.m3753(C0727.C0737.Toolbar_contentInsetRight, 0);
        m3585();
        this.f2213.m3665(r3, r4);
        if (!(r86 == Integer.MIN_VALUE && r2 == Integer.MIN_VALUE)) {
            this.f2213.m3662(r86, r2);
        }
        this.f2214 = r7.m3751(C0727.C0737.Toolbar_contentInsetStartWithNavigation, Integer.MIN_VALUE);
        this.f2215 = r7.m3751(C0727.C0737.Toolbar_contentInsetEndWithActions, Integer.MIN_VALUE);
        this.f2196 = r7.m3744(C0727.C0737.Toolbar_collapseIcon);
        this.f2198 = r7.m3750(C0727.C0737.Toolbar_collapseContentDescription);
        CharSequence r87 = r7.m3750(C0727.C0737.Toolbar_title);
        if (!TextUtils.isEmpty(r87)) {
            setTitle(r87);
        }
        CharSequence r88 = r7.m3750(C0727.C0737.Toolbar_subtitle);
        if (!TextUtils.isEmpty(r88)) {
            setSubtitle(r88);
        }
        this.f2200 = getContext();
        setPopupTheme(r7.m3757(C0727.C0737.Toolbar_popupTheme, 0));
        Drawable r89 = r7.m3744(C0727.C0737.Toolbar_navigationIcon);
        if (r89 != null) {
            setNavigationIcon(r89);
        }
        CharSequence r810 = r7.m3750(C0727.C0737.Toolbar_navigationContentDescription);
        if (!TextUtils.isEmpty(r810)) {
            setNavigationContentDescription(r810);
        }
        Drawable r811 = r7.m3744(C0727.C0737.Toolbar_logo);
        if (r811 != null) {
            setLogo(r811);
        }
        CharSequence r812 = r7.m3750(C0727.C0737.Toolbar_logoDescription);
        if (!TextUtils.isEmpty(r812)) {
            setLogoDescription(r812);
        }
        if (r7.m3758(C0727.C0737.Toolbar_titleTextColor)) {
            setTitleTextColor(r7.m3747(C0727.C0737.Toolbar_titleTextColor, -1));
        }
        if (r7.m3758(C0727.C0737.Toolbar_subtitleTextColor)) {
            setSubtitleTextColor(r7.m3747(C0727.C0737.Toolbar_subtitleTextColor, -1));
        }
        r7.m3745();
    }

    public void setPopupTheme(int i) {
        if (this.f2202 != i) {
            this.f2202 = i;
            if (i == 0) {
                this.f2200 = getContext();
            } else {
                this.f2200 = new ContextThemeWrapper(getContext(), i);
            }
        }
    }

    public int getPopupTheme() {
        return this.f2202;
    }

    public int getTitleMarginStart() {
        return this.f2207;
    }

    public void setTitleMarginStart(int i) {
        this.f2207 = i;
        requestLayout();
    }

    public int getTitleMarginTop() {
        return this.f2211;
    }

    public void setTitleMarginTop(int i) {
        this.f2211 = i;
        requestLayout();
    }

    public int getTitleMarginEnd() {
        return this.f2209;
    }

    public void setTitleMarginEnd(int i) {
        this.f2209 = i;
        requestLayout();
    }

    public int getTitleMarginBottom() {
        return this.f2212;
    }

    public void setTitleMarginBottom(int i) {
        this.f2212 = i;
        requestLayout();
    }

    public void onRtlPropertiesChanged(int i) {
        if (Build.VERSION.SDK_INT >= 17) {
            super.onRtlPropertiesChanged(i);
        }
        m3585();
        C0578 r0 = this.f2213;
        boolean z = true;
        if (i != 1) {
            z = false;
        }
        r0.m3663(z);
    }

    public void setLogo(int i) {
        setLogo(C0739.m4883(getContext(), i));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3592() {
        ActionMenuView actionMenuView;
        return getVisibility() == 0 && (actionMenuView = this.f2186) != null && actionMenuView.m3195();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m3594() {
        ActionMenuView actionMenuView = this.f2186;
        return actionMenuView != null && actionMenuView.m3205();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m3595() {
        ActionMenuView actionMenuView = this.f2186;
        return actionMenuView != null && actionMenuView.m3206();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m3596() {
        ActionMenuView actionMenuView = this.f2186;
        return actionMenuView != null && actionMenuView.m3203();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m3597() {
        ActionMenuView actionMenuView = this.f2186;
        return actionMenuView != null && actionMenuView.m3204();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3590(C0504 r4, C0614 r5) {
        if (r4 != null || this.f2186 != null) {
            m3581();
            C0504 r0 = this.f2186.m3202();
            if (r0 != r4) {
                if (r0 != null) {
                    r0.m2906(this.f2193);
                    r0.m2906(this.f2191);
                }
                if (this.f2191 == null) {
                    this.f2191 = new C0570();
                }
                r5.m3932(true);
                if (r4 != null) {
                    r4.m2896(r5, this.f2200);
                    r4.m2896(this.f2191, this.f2200);
                } else {
                    r5.m3917(this.f2200, (C0504) null);
                    this.f2191.m3606(this.f2200, (C0504) null);
                    r5.m3924(true);
                    this.f2191.m3610(true);
                }
                this.f2186.setPopupTheme(this.f2202);
                this.f2186.setPresenter(r5);
                this.f2193 = r5;
            }
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m3598() {
        ActionMenuView actionMenuView = this.f2186;
        if (actionMenuView != null) {
            actionMenuView.m3207();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.Toolbar.ʻ(android.view.View, boolean):void
     arg types: [android.widget.ImageView, int]
     candidates:
      android.support.v7.widget.Toolbar.ʻ(android.view.View, int):int
      android.support.v7.widget.Toolbar.ʻ(java.util.List<android.view.View>, int[]):int
      android.support.v7.widget.Toolbar.ʻ(java.util.List<android.view.View>, int):void
      android.support.v7.widget.Toolbar.ʻ(int, int):void
      android.support.v7.widget.Toolbar.ʻ(android.content.Context, int):void
      android.support.v7.widget.Toolbar.ʻ(android.support.v7.view.menu.ˉ, android.support.v7.widget.ʾ):void
      android.support.v7.widget.Toolbar.ʻ(android.support.v7.view.menu.ـ$ʻ, android.support.v7.view.menu.ˉ$ʻ):void
      android.support.v7.widget.Toolbar.ʻ(android.view.View, boolean):void */
    public void setLogo(Drawable drawable) {
        if (drawable != null) {
            m3579();
            if (!m3578(this.f2194)) {
                m3571((View) this.f2194, true);
            }
        } else {
            ImageView imageView = this.f2194;
            if (imageView != null && m3578(imageView)) {
                removeView(this.f2194);
                this.f2187.remove(this.f2194);
            }
        }
        ImageView imageView2 = this.f2194;
        if (imageView2 != null) {
            imageView2.setImageDrawable(drawable);
        }
    }

    public Drawable getLogo() {
        ImageView imageView = this.f2194;
        if (imageView != null) {
            return imageView.getDrawable();
        }
        return null;
    }

    public void setLogoDescription(int i) {
        setLogoDescription(getContext().getText(i));
    }

    public void setLogoDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            m3579();
        }
        ImageView imageView = this.f2194;
        if (imageView != null) {
            imageView.setContentDescription(charSequence);
        }
    }

    public CharSequence getLogoDescription() {
        ImageView imageView = this.f2194;
        if (imageView != null) {
            return imageView.getContentDescription();
        }
        return null;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m3579() {
        if (this.f2194 == null) {
            this.f2194 = new C0674(getContext());
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m3599() {
        C0570 r0 = this.f2191;
        return (r0 == null || r0.f2223 == null) ? false : true;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public void m3600() {
        C0570 r0 = this.f2191;
        C0508 r02 = r0 == null ? null : r0.f2223;
        if (r02 != null) {
            r02.collapseActionView();
        }
    }

    public CharSequence getTitle() {
        return this.f2217;
    }

    public void setTitle(int i) {
        setTitle(getContext().getText(i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.Toolbar.ʻ(android.view.View, boolean):void
     arg types: [android.widget.TextView, int]
     candidates:
      android.support.v7.widget.Toolbar.ʻ(android.view.View, int):int
      android.support.v7.widget.Toolbar.ʻ(java.util.List<android.view.View>, int[]):int
      android.support.v7.widget.Toolbar.ʻ(java.util.List<android.view.View>, int):void
      android.support.v7.widget.Toolbar.ʻ(int, int):void
      android.support.v7.widget.Toolbar.ʻ(android.content.Context, int):void
      android.support.v7.widget.Toolbar.ʻ(android.support.v7.view.menu.ˉ, android.support.v7.widget.ʾ):void
      android.support.v7.widget.Toolbar.ʻ(android.support.v7.view.menu.ـ$ʻ, android.support.v7.view.menu.ˉ$ʻ):void
      android.support.v7.widget.Toolbar.ʻ(android.view.View, boolean):void */
    public void setTitle(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.f2188 == null) {
                Context context = getContext();
                this.f2188 = new C0677(context);
                this.f2188.setSingleLine();
                this.f2188.setEllipsize(TextUtils.TruncateAt.END);
                int i = this.f2203;
                if (i != 0) {
                    this.f2188.setTextAppearance(context, i);
                }
                int i2 = this.f2208;
                if (i2 != 0) {
                    this.f2188.setTextColor(i2);
                }
            }
            if (!m3578(this.f2188)) {
                m3571((View) this.f2188, true);
            }
        } else {
            TextView textView = this.f2188;
            if (textView != null && m3578(textView)) {
                removeView(this.f2188);
                this.f2187.remove(this.f2188);
            }
        }
        TextView textView2 = this.f2188;
        if (textView2 != null) {
            textView2.setText(charSequence);
        }
        this.f2217 = charSequence;
    }

    public CharSequence getSubtitle() {
        return this.f2218;
    }

    public void setSubtitle(int i) {
        setSubtitle(getContext().getText(i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.Toolbar.ʻ(android.view.View, boolean):void
     arg types: [android.widget.TextView, int]
     candidates:
      android.support.v7.widget.Toolbar.ʻ(android.view.View, int):int
      android.support.v7.widget.Toolbar.ʻ(java.util.List<android.view.View>, int[]):int
      android.support.v7.widget.Toolbar.ʻ(java.util.List<android.view.View>, int):void
      android.support.v7.widget.Toolbar.ʻ(int, int):void
      android.support.v7.widget.Toolbar.ʻ(android.content.Context, int):void
      android.support.v7.widget.Toolbar.ʻ(android.support.v7.view.menu.ˉ, android.support.v7.widget.ʾ):void
      android.support.v7.widget.Toolbar.ʻ(android.support.v7.view.menu.ـ$ʻ, android.support.v7.view.menu.ˉ$ʻ):void
      android.support.v7.widget.Toolbar.ʻ(android.view.View, boolean):void */
    public void setSubtitle(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.f2190 == null) {
                Context context = getContext();
                this.f2190 = new C0677(context);
                this.f2190.setSingleLine();
                this.f2190.setEllipsize(TextUtils.TruncateAt.END);
                int i = this.f2204;
                if (i != 0) {
                    this.f2190.setTextAppearance(context, i);
                }
                int i2 = this.f2210;
                if (i2 != 0) {
                    this.f2190.setTextColor(i2);
                }
            }
            if (!m3578(this.f2190)) {
                m3571((View) this.f2190, true);
            }
        } else {
            TextView textView = this.f2190;
            if (textView != null && m3578(textView)) {
                removeView(this.f2190);
                this.f2187.remove(this.f2190);
            }
        }
        TextView textView2 = this.f2190;
        if (textView2 != null) {
            textView2.setText(charSequence);
        }
        this.f2218 = charSequence;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3589(Context context, int i) {
        this.f2203 = i;
        TextView textView = this.f2188;
        if (textView != null) {
            textView.setTextAppearance(context, i);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3593(Context context, int i) {
        this.f2204 = i;
        TextView textView = this.f2190;
        if (textView != null) {
            textView.setTextAppearance(context, i);
        }
    }

    public void setTitleTextColor(int i) {
        this.f2208 = i;
        TextView textView = this.f2188;
        if (textView != null) {
            textView.setTextColor(i);
        }
    }

    public void setSubtitleTextColor(int i) {
        this.f2210 = i;
        TextView textView = this.f2190;
        if (textView != null) {
            textView.setTextColor(i);
        }
    }

    public CharSequence getNavigationContentDescription() {
        ImageButton imageButton = this.f2192;
        if (imageButton != null) {
            return imageButton.getContentDescription();
        }
        return null;
    }

    public void setNavigationContentDescription(int i) {
        setNavigationContentDescription(i != 0 ? getContext().getText(i) : null);
    }

    public void setNavigationContentDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            m3582();
        }
        ImageButton imageButton = this.f2192;
        if (imageButton != null) {
            imageButton.setContentDescription(charSequence);
        }
    }

    public void setNavigationIcon(int i) {
        setNavigationIcon(C0739.m4883(getContext(), i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.Toolbar.ʻ(android.view.View, boolean):void
     arg types: [android.widget.ImageButton, int]
     candidates:
      android.support.v7.widget.Toolbar.ʻ(android.view.View, int):int
      android.support.v7.widget.Toolbar.ʻ(java.util.List<android.view.View>, int[]):int
      android.support.v7.widget.Toolbar.ʻ(java.util.List<android.view.View>, int):void
      android.support.v7.widget.Toolbar.ʻ(int, int):void
      android.support.v7.widget.Toolbar.ʻ(android.content.Context, int):void
      android.support.v7.widget.Toolbar.ʻ(android.support.v7.view.menu.ˉ, android.support.v7.widget.ʾ):void
      android.support.v7.widget.Toolbar.ʻ(android.support.v7.view.menu.ـ$ʻ, android.support.v7.view.menu.ˉ$ʻ):void
      android.support.v7.widget.Toolbar.ʻ(android.view.View, boolean):void */
    public void setNavigationIcon(Drawable drawable) {
        if (drawable != null) {
            m3582();
            if (!m3578(this.f2192)) {
                m3571((View) this.f2192, true);
            }
        } else {
            ImageButton imageButton = this.f2192;
            if (imageButton != null && m3578(imageButton)) {
                removeView(this.f2192);
                this.f2187.remove(this.f2192);
            }
        }
        ImageButton imageButton2 = this.f2192;
        if (imageButton2 != null) {
            imageButton2.setImageDrawable(drawable);
        }
    }

    public Drawable getNavigationIcon() {
        ImageButton imageButton = this.f2192;
        if (imageButton != null) {
            return imageButton.getDrawable();
        }
        return null;
    }

    public void setNavigationOnClickListener(View.OnClickListener onClickListener) {
        m3582();
        this.f2192.setOnClickListener(onClickListener);
    }

    public Menu getMenu() {
        m3580();
        return this.f2186.getMenu();
    }

    public void setOverflowIcon(Drawable drawable) {
        m3580();
        this.f2186.setOverflowIcon(drawable);
    }

    public Drawable getOverflowIcon() {
        m3580();
        return this.f2186.getOverflowIcon();
    }

    /* renamed from: י  reason: contains not printable characters */
    private void m3580() {
        m3581();
        if (this.f2186.m3202() == null) {
            C0504 r0 = (C0504) this.f2186.getMenu();
            if (this.f2191 == null) {
                this.f2191 = new C0570();
            }
            this.f2186.setExpandedActionViewsExclusive(true);
            r0.m2896(this.f2191, this.f2200);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.Toolbar.ʻ(android.view.View, boolean):void
     arg types: [android.support.v7.widget.ActionMenuView, int]
     candidates:
      android.support.v7.widget.Toolbar.ʻ(android.view.View, int):int
      android.support.v7.widget.Toolbar.ʻ(java.util.List<android.view.View>, int[]):int
      android.support.v7.widget.Toolbar.ʻ(java.util.List<android.view.View>, int):void
      android.support.v7.widget.Toolbar.ʻ(int, int):void
      android.support.v7.widget.Toolbar.ʻ(android.content.Context, int):void
      android.support.v7.widget.Toolbar.ʻ(android.support.v7.view.menu.ˉ, android.support.v7.widget.ʾ):void
      android.support.v7.widget.Toolbar.ʻ(android.support.v7.view.menu.ـ$ʻ, android.support.v7.view.menu.ˉ$ʻ):void
      android.support.v7.widget.Toolbar.ʻ(android.view.View, boolean):void */
    /* renamed from: ـ  reason: contains not printable characters */
    private void m3581() {
        if (this.f2186 == null) {
            this.f2186 = new ActionMenuView(getContext());
            this.f2186.setPopupTheme(this.f2202);
            this.f2186.setOnMenuItemClickListener(this.f2205);
            this.f2186.m3194(this.f2197, this.f2195);
            C0571 r0 = generateDefaultLayoutParams();
            r0.f1390 = 8388613 | (this.f2182 & 112);
            this.f2186.setLayoutParams(r0);
            m3571((View) this.f2186, false);
        }
    }

    private MenuInflater getMenuInflater() {
        return new C0536(getContext());
    }

    public void setOnMenuItemClickListener(C0572 r1) {
        this.f2184 = r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3588(int i, int i2) {
        m3585();
        this.f2213.m3662(i, i2);
    }

    public int getContentInsetStart() {
        C0578 r0 = this.f2213;
        if (r0 != null) {
            return r0.m3666();
        }
        return 0;
    }

    public int getContentInsetEnd() {
        C0578 r0 = this.f2213;
        if (r0 != null) {
            return r0.m3667();
        }
        return 0;
    }

    public int getContentInsetLeft() {
        C0578 r0 = this.f2213;
        if (r0 != null) {
            return r0.m3661();
        }
        return 0;
    }

    public int getContentInsetRight() {
        C0578 r0 = this.f2213;
        if (r0 != null) {
            return r0.m3664();
        }
        return 0;
    }

    public int getContentInsetStartWithNavigation() {
        int i = this.f2214;
        return i != Integer.MIN_VALUE ? i : getContentInsetStart();
    }

    public void setContentInsetStartWithNavigation(int i) {
        if (i < 0) {
            i = Integer.MIN_VALUE;
        }
        if (i != this.f2214) {
            this.f2214 = i;
            if (getNavigationIcon() != null) {
                requestLayout();
            }
        }
    }

    public int getContentInsetEndWithActions() {
        int i = this.f2215;
        return i != Integer.MIN_VALUE ? i : getContentInsetEnd();
    }

    public void setContentInsetEndWithActions(int i) {
        if (i < 0) {
            i = Integer.MIN_VALUE;
        }
        if (i != this.f2215) {
            this.f2215 = i;
            if (getNavigationIcon() != null) {
                requestLayout();
            }
        }
    }

    public int getCurrentContentInsetStart() {
        if (getNavigationIcon() != null) {
            return Math.max(getContentInsetStart(), Math.max(this.f2214, 0));
        }
        return getContentInsetStart();
    }

    public int getCurrentContentInsetEnd() {
        C0504 r0;
        ActionMenuView actionMenuView = this.f2186;
        if ((actionMenuView == null || (r0 = actionMenuView.m3202()) == null || !r0.hasVisibleItems()) ? false : true) {
            return Math.max(getContentInsetEnd(), Math.max(this.f2215, 0));
        }
        return getContentInsetEnd();
    }

    public int getCurrentContentInsetLeft() {
        if (C0414.m2236(this) == 1) {
            return getCurrentContentInsetEnd();
        }
        return getCurrentContentInsetStart();
    }

    public int getCurrentContentInsetRight() {
        if (C0414.m2236(this) == 1) {
            return getCurrentContentInsetStart();
        }
        return getCurrentContentInsetEnd();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m3582() {
        if (this.f2192 == null) {
            this.f2192 = new C0669(getContext(), null, C0727.C0728.toolbarNavigationButtonStyle);
            C0571 r0 = generateDefaultLayoutParams();
            r0.f1390 = 8388611 | (this.f2182 & 112);
            this.f2192.setLayoutParams(r0);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public void m3601() {
        if (this.f2178 == null) {
            this.f2178 = new C0669(getContext(), null, C0727.C0728.toolbarNavigationButtonStyle);
            this.f2178.setImageDrawable(this.f2196);
            this.f2178.setContentDescription(this.f2198);
            C0571 r0 = generateDefaultLayoutParams();
            r0.f1390 = 8388611 | (this.f2182 & 112);
            r0.f2225 = 2;
            this.f2178.setLayoutParams(r0);
            this.f2178.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Toolbar.this.m3600();
                }
            });
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3571(View view, boolean z) {
        C0571 r0;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            r0 = generateDefaultLayoutParams();
        } else if (!checkLayoutParams(layoutParams)) {
            r0 = generateLayoutParams(layoutParams);
        } else {
            r0 = (C0571) layoutParams;
        }
        r0.f2225 = 1;
        if (!z || this.f2180 == null) {
            addView(view, r0);
            return;
        }
        view.setLayoutParams(r0);
        this.f2187.add(view);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        C0573 r0 = new C0573(super.onSaveInstanceState());
        C0570 r1 = this.f2191;
        if (!(r1 == null || r1.f2223 == null)) {
            r0.f2226 = this.f2191.f2223.getItemId();
        }
        r0.f2227 = m3594();
        return r0;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        MenuItem findItem;
        if (!(parcelable instanceof C0573)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        C0573 r3 = (C0573) parcelable;
        super.onRestoreInstanceState(r3.m1995());
        ActionMenuView actionMenuView = this.f2186;
        C0504 r0 = actionMenuView != null ? actionMenuView.m3202() : null;
        if (!(r3.f2226 == 0 || this.f2191 == null || r0 == null || (findItem = r0.findItem(r3.f2226)) == null)) {
            findItem.expandActionView();
        }
        if (r3.f2227) {
            m3583();
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m3583() {
        removeCallbacks(this.f2199);
        post(this.f2199);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.f2199);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.f2179 = false;
        }
        if (!this.f2179) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.f2179 = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.f2179 = false;
        }
        return true;
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.f2183 = false;
        }
        if (!this.f2183) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.f2183 = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.f2183 = false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3570(View view, int i, int i2, int i3, int i4, int i5) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int childMeasureSpec = getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + i2, marginLayoutParams.width);
        int childMeasureSpec2 = getChildMeasureSpec(i3, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i4, marginLayoutParams.height);
        int mode = View.MeasureSpec.getMode(childMeasureSpec2);
        if (mode != 1073741824 && i5 >= 0) {
            if (mode != 0) {
                i5 = Math.min(View.MeasureSpec.getSize(childMeasureSpec2), i5);
            }
            childMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i5, 1073741824);
        }
        view.measure(childMeasureSpec, childMeasureSpec2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m3567(View view, int i, int i2, int i3, int i4, int[] iArr) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int i5 = marginLayoutParams.leftMargin - iArr[0];
        int i6 = marginLayoutParams.rightMargin - iArr[1];
        int max = Math.max(0, i5) + Math.max(0, i6);
        iArr[0] = Math.max(0, -i5);
        iArr[1] = Math.max(0, -i6);
        view.measure(getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight() + max + i2, marginLayoutParams.width), getChildMeasureSpec(i3, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i4, marginLayoutParams.height));
        return view.getMeasuredWidth() + max;
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    private boolean m3584() {
        if (!this.f2201) {
            return false;
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (m3573(childAt) && childAt.getMeasuredWidth() > 0 && childAt.getMeasuredHeight() > 0) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        char c;
        char c2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int[] iArr = this.f2185;
        if (C0607.m3858(this)) {
            c2 = 1;
            c = 0;
        } else {
            c2 = 0;
            c = 1;
        }
        if (m3573(this.f2192)) {
            m3570(this.f2192, i, 0, i2, 0, this.f2206);
            i5 = this.f2192.getMeasuredWidth() + m3575(this.f2192);
            i4 = Math.max(0, this.f2192.getMeasuredHeight() + m3577(this.f2192));
            i3 = View.combineMeasuredStates(0, this.f2192.getMeasuredState());
        } else {
            i5 = 0;
            i4 = 0;
            i3 = 0;
        }
        if (m3573(this.f2178)) {
            m3570(this.f2178, i, 0, i2, 0, this.f2206);
            i5 = this.f2178.getMeasuredWidth() + m3575(this.f2178);
            i4 = Math.max(i4, this.f2178.getMeasuredHeight() + m3577(this.f2178));
            i3 = View.combineMeasuredStates(i3, this.f2178.getMeasuredState());
        }
        int currentContentInsetStart = getCurrentContentInsetStart();
        int max = 0 + Math.max(currentContentInsetStart, i5);
        iArr[c2] = Math.max(0, currentContentInsetStart - i5);
        if (m3573(this.f2186)) {
            m3570(this.f2186, i, max, i2, 0, this.f2206);
            i6 = this.f2186.getMeasuredWidth() + m3575(this.f2186);
            i4 = Math.max(i4, this.f2186.getMeasuredHeight() + m3577(this.f2186));
            i3 = View.combineMeasuredStates(i3, this.f2186.getMeasuredState());
        } else {
            i6 = 0;
        }
        int currentContentInsetEnd = getCurrentContentInsetEnd();
        int max2 = max + Math.max(currentContentInsetEnd, i6);
        iArr[c] = Math.max(0, currentContentInsetEnd - i6);
        if (m3573(this.f2180)) {
            max2 += m3567(this.f2180, i, max2, i2, 0, iArr);
            i4 = Math.max(i4, this.f2180.getMeasuredHeight() + m3577(this.f2180));
            i3 = View.combineMeasuredStates(i3, this.f2180.getMeasuredState());
        }
        if (m3573(this.f2194)) {
            max2 += m3567(this.f2194, i, max2, i2, 0, iArr);
            i4 = Math.max(i4, this.f2194.getMeasuredHeight() + m3577(this.f2194));
            i3 = View.combineMeasuredStates(i3, this.f2194.getMeasuredState());
        }
        int childCount = getChildCount();
        int i10 = i4;
        int i11 = max2;
        for (int i12 = 0; i12 < childCount; i12++) {
            View childAt = getChildAt(i12);
            if (((C0571) childAt.getLayoutParams()).f2225 == 0 && m3573(childAt)) {
                i11 += m3567(childAt, i, i11, i2, 0, iArr);
                i10 = Math.max(i10, childAt.getMeasuredHeight() + m3577(childAt));
                i3 = View.combineMeasuredStates(i3, childAt.getMeasuredState());
            }
        }
        int i13 = this.f2211 + this.f2212;
        int i14 = this.f2207 + this.f2209;
        if (m3573(this.f2188)) {
            m3567(this.f2188, i, i11 + i14, i2, i13, iArr);
            int measuredWidth = this.f2188.getMeasuredWidth() + m3575(this.f2188);
            i7 = this.f2188.getMeasuredHeight() + m3577(this.f2188);
            i9 = View.combineMeasuredStates(i3, this.f2188.getMeasuredState());
            i8 = measuredWidth;
        } else {
            i9 = i3;
            i8 = 0;
            i7 = 0;
        }
        if (m3573(this.f2190)) {
            int i15 = i7 + i13;
            i8 = Math.max(i8, m3567(this.f2190, i, i11 + i14, i2, i15, iArr));
            i7 += this.f2190.getMeasuredHeight() + m3577(this.f2190);
            i9 = View.combineMeasuredStates(i9, this.f2190.getMeasuredState());
        }
        int max3 = Math.max(i10, i7);
        int paddingLeft = i11 + i8 + getPaddingLeft() + getPaddingRight();
        int paddingTop = max3 + getPaddingTop() + getPaddingBottom();
        int resolveSizeAndState = View.resolveSizeAndState(Math.max(paddingLeft, getSuggestedMinimumWidth()), i, -16777216 & i9);
        int resolveSizeAndState2 = View.resolveSizeAndState(Math.max(paddingTop, getSuggestedMinimumHeight()), i2, i9 << 16);
        if (m3584()) {
            resolveSizeAndState2 = 0;
        }
        setMeasuredDimension(resolveSizeAndState, resolveSizeAndState2);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x02a5 A[LOOP:0: B:101:0x02a3->B:102:0x02a5, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x02c7 A[LOOP:1: B:104:0x02c5->B:105:0x02c7, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x02f2  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x0301 A[LOOP:2: B:112:0x02ff->B:113:0x0301, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0130  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0137  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x016a  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x01a9  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x01b8  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x022b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLayout(boolean r19, int r20, int r21, int r22, int r23) {
        /*
            r18 = this;
            r0 = r18
            int r1 = android.support.v4.ˉ.C0414.m2236(r18)
            r2 = 1
            r3 = 0
            if (r1 != r2) goto L_0x000c
            r1 = 1
            goto L_0x000d
        L_0x000c:
            r1 = 0
        L_0x000d:
            int r4 = r18.getWidth()
            int r5 = r18.getHeight()
            int r6 = r18.getPaddingLeft()
            int r7 = r18.getPaddingRight()
            int r8 = r18.getPaddingTop()
            int r9 = r18.getPaddingBottom()
            int r10 = r4 - r7
            int[] r11 = r0.f2185
            r11[r2] = r3
            r11[r3] = r3
            int r12 = android.support.v4.ˉ.C0414.m2241(r18)
            if (r12 < 0) goto L_0x003a
            int r13 = r23 - r21
            int r12 = java.lang.Math.min(r12, r13)
            goto L_0x003b
        L_0x003a:
            r12 = 0
        L_0x003b:
            android.widget.ImageButton r13 = r0.f2192
            boolean r13 = r0.m3573(r13)
            if (r13 == 0) goto L_0x0055
            if (r1 == 0) goto L_0x004e
            android.widget.ImageButton r13 = r0.f2192
            int r13 = r0.m3576(r13, r10, r11, r12)
            r14 = r13
            r13 = r6
            goto L_0x0057
        L_0x004e:
            android.widget.ImageButton r13 = r0.f2192
            int r13 = r0.m3568(r13, r6, r11, r12)
            goto L_0x0056
        L_0x0055:
            r13 = r6
        L_0x0056:
            r14 = r10
        L_0x0057:
            android.widget.ImageButton r15 = r0.f2178
            boolean r15 = r0.m3573(r15)
            if (r15 == 0) goto L_0x006e
            if (r1 == 0) goto L_0x0068
            android.widget.ImageButton r15 = r0.f2178
            int r14 = r0.m3576(r15, r14, r11, r12)
            goto L_0x006e
        L_0x0068:
            android.widget.ImageButton r15 = r0.f2178
            int r13 = r0.m3568(r15, r13, r11, r12)
        L_0x006e:
            android.support.v7.widget.ActionMenuView r15 = r0.f2186
            boolean r15 = r0.m3573(r15)
            if (r15 == 0) goto L_0x0085
            if (r1 == 0) goto L_0x007f
            android.support.v7.widget.ActionMenuView r15 = r0.f2186
            int r13 = r0.m3568(r15, r13, r11, r12)
            goto L_0x0085
        L_0x007f:
            android.support.v7.widget.ActionMenuView r15 = r0.f2186
            int r14 = r0.m3576(r15, r14, r11, r12)
        L_0x0085:
            int r15 = r18.getCurrentContentInsetLeft()
            int r16 = r18.getCurrentContentInsetRight()
            int r2 = r15 - r13
            int r2 = java.lang.Math.max(r3, r2)
            r11[r3] = r2
            int r2 = r10 - r14
            int r2 = r16 - r2
            int r2 = java.lang.Math.max(r3, r2)
            r17 = 1
            r11[r17] = r2
            int r2 = java.lang.Math.max(r13, r15)
            int r10 = r10 - r16
            int r10 = java.lang.Math.min(r14, r10)
            android.view.View r13 = r0.f2180
            boolean r13 = r0.m3573(r13)
            if (r13 == 0) goto L_0x00c2
            if (r1 == 0) goto L_0x00bc
            android.view.View r13 = r0.f2180
            int r10 = r0.m3576(r13, r10, r11, r12)
            goto L_0x00c2
        L_0x00bc:
            android.view.View r13 = r0.f2180
            int r2 = r0.m3568(r13, r2, r11, r12)
        L_0x00c2:
            android.widget.ImageView r13 = r0.f2194
            boolean r13 = r0.m3573(r13)
            if (r13 == 0) goto L_0x00d9
            if (r1 == 0) goto L_0x00d3
            android.widget.ImageView r13 = r0.f2194
            int r10 = r0.m3576(r13, r10, r11, r12)
            goto L_0x00d9
        L_0x00d3:
            android.widget.ImageView r13 = r0.f2194
            int r2 = r0.m3568(r13, r2, r11, r12)
        L_0x00d9:
            android.widget.TextView r13 = r0.f2188
            boolean r13 = r0.m3573(r13)
            android.widget.TextView r14 = r0.f2190
            boolean r14 = r0.m3573(r14)
            if (r13 == 0) goto L_0x0100
            android.widget.TextView r15 = r0.f2188
            android.view.ViewGroup$LayoutParams r15 = r15.getLayoutParams()
            android.support.v7.widget.Toolbar$ʼ r15 = (android.support.v7.widget.Toolbar.C0571) r15
            int r3 = r15.topMargin
            r22 = r7
            android.widget.TextView r7 = r0.f2188
            int r7 = r7.getMeasuredHeight()
            int r3 = r3 + r7
            int r7 = r15.bottomMargin
            int r3 = r3 + r7
            r7 = 0
            int r3 = r3 + r7
            goto L_0x0103
        L_0x0100:
            r22 = r7
            r3 = 0
        L_0x0103:
            if (r14 == 0) goto L_0x011d
            android.widget.TextView r7 = r0.f2190
            android.view.ViewGroup$LayoutParams r7 = r7.getLayoutParams()
            android.support.v7.widget.Toolbar$ʼ r7 = (android.support.v7.widget.Toolbar.C0571) r7
            int r15 = r7.topMargin
            r16 = r4
            android.widget.TextView r4 = r0.f2190
            int r4 = r4.getMeasuredHeight()
            int r15 = r15 + r4
            int r4 = r7.bottomMargin
            int r15 = r15 + r4
            int r3 = r3 + r15
            goto L_0x011f
        L_0x011d:
            r16 = r4
        L_0x011f:
            if (r13 != 0) goto L_0x012b
            if (r14 == 0) goto L_0x0124
            goto L_0x012b
        L_0x0124:
            r17 = r6
            r21 = r12
        L_0x0128:
            r7 = 0
            goto L_0x0295
        L_0x012b:
            if (r13 == 0) goto L_0x0130
            android.widget.TextView r4 = r0.f2188
            goto L_0x0132
        L_0x0130:
            android.widget.TextView r4 = r0.f2190
        L_0x0132:
            if (r14 == 0) goto L_0x0137
            android.widget.TextView r7 = r0.f2190
            goto L_0x0139
        L_0x0137:
            android.widget.TextView r7 = r0.f2188
        L_0x0139:
            android.view.ViewGroup$LayoutParams r4 = r4.getLayoutParams()
            android.support.v7.widget.Toolbar$ʼ r4 = (android.support.v7.widget.Toolbar.C0571) r4
            android.view.ViewGroup$LayoutParams r7 = r7.getLayoutParams()
            android.support.v7.widget.Toolbar$ʼ r7 = (android.support.v7.widget.Toolbar.C0571) r7
            if (r13 == 0) goto L_0x014f
            android.widget.TextView r15 = r0.f2188
            int r15 = r15.getMeasuredWidth()
            if (r15 > 0) goto L_0x0159
        L_0x014f:
            if (r14 == 0) goto L_0x015d
            android.widget.TextView r15 = r0.f2190
            int r15 = r15.getMeasuredWidth()
            if (r15 <= 0) goto L_0x015d
        L_0x0159:
            r17 = r6
            r15 = 1
            goto L_0x0160
        L_0x015d:
            r17 = r6
            r15 = 0
        L_0x0160:
            int r6 = r0.f2216
            r6 = r6 & 112(0x70, float:1.57E-43)
            r21 = r12
            r12 = 48
            if (r6 == r12) goto L_0x01a9
            r12 = 80
            if (r6 == r12) goto L_0x019d
            int r6 = r5 - r8
            int r6 = r6 - r9
            int r6 = r6 - r3
            int r6 = r6 / 2
            int r12 = r4.topMargin
            r23 = r2
            int r2 = r0.f2211
            int r12 = r12 + r2
            if (r6 >= r12) goto L_0x0184
            int r2 = r4.topMargin
            int r3 = r0.f2211
            int r6 = r2 + r3
            goto L_0x019b
        L_0x0184:
            int r5 = r5 - r9
            int r5 = r5 - r3
            int r5 = r5 - r6
            int r5 = r5 - r8
            int r2 = r4.bottomMargin
            int r3 = r0.f2212
            int r2 = r2 + r3
            if (r5 >= r2) goto L_0x019b
            int r2 = r7.bottomMargin
            int r3 = r0.f2212
            int r2 = r2 + r3
            int r2 = r2 - r5
            int r6 = r6 - r2
            r2 = 0
            int r6 = java.lang.Math.max(r2, r6)
        L_0x019b:
            int r8 = r8 + r6
            goto L_0x01b6
        L_0x019d:
            r23 = r2
            int r5 = r5 - r9
            int r2 = r7.bottomMargin
            int r5 = r5 - r2
            int r2 = r0.f2212
            int r5 = r5 - r2
            int r8 = r5 - r3
            goto L_0x01b6
        L_0x01a9:
            r23 = r2
            int r2 = r18.getPaddingTop()
            int r3 = r4.topMargin
            int r2 = r2 + r3
            int r3 = r0.f2211
            int r8 = r2 + r3
        L_0x01b6:
            if (r1 == 0) goto L_0x022b
            if (r15 == 0) goto L_0x01be
            int r3 = r0.f2207
            r1 = 1
            goto L_0x01c0
        L_0x01be:
            r1 = 1
            r3 = 0
        L_0x01c0:
            r2 = r11[r1]
            int r3 = r3 - r2
            r2 = 0
            int r4 = java.lang.Math.max(r2, r3)
            int r10 = r10 - r4
            int r3 = -r3
            int r3 = java.lang.Math.max(r2, r3)
            r11[r1] = r3
            if (r13 == 0) goto L_0x01f6
            android.widget.TextView r1 = r0.f2188
            android.view.ViewGroup$LayoutParams r1 = r1.getLayoutParams()
            android.support.v7.widget.Toolbar$ʼ r1 = (android.support.v7.widget.Toolbar.C0571) r1
            android.widget.TextView r2 = r0.f2188
            int r2 = r2.getMeasuredWidth()
            int r2 = r10 - r2
            android.widget.TextView r3 = r0.f2188
            int r3 = r3.getMeasuredHeight()
            int r3 = r3 + r8
            android.widget.TextView r4 = r0.f2188
            r4.layout(r2, r8, r10, r3)
            int r4 = r0.f2209
            int r2 = r2 - r4
            int r1 = r1.bottomMargin
            int r8 = r3 + r1
            goto L_0x01f7
        L_0x01f6:
            r2 = r10
        L_0x01f7:
            if (r14 == 0) goto L_0x021f
            android.widget.TextView r1 = r0.f2190
            android.view.ViewGroup$LayoutParams r1 = r1.getLayoutParams()
            android.support.v7.widget.Toolbar$ʼ r1 = (android.support.v7.widget.Toolbar.C0571) r1
            int r3 = r1.topMargin
            int r8 = r8 + r3
            android.widget.TextView r3 = r0.f2190
            int r3 = r3.getMeasuredWidth()
            int r3 = r10 - r3
            android.widget.TextView r4 = r0.f2190
            int r4 = r4.getMeasuredHeight()
            int r4 = r4 + r8
            android.widget.TextView r5 = r0.f2190
            r5.layout(r3, r8, r10, r4)
            int r3 = r0.f2209
            int r3 = r10 - r3
            int r1 = r1.bottomMargin
            goto L_0x0220
        L_0x021f:
            r3 = r10
        L_0x0220:
            if (r15 == 0) goto L_0x0227
            int r1 = java.lang.Math.min(r2, r3)
            r10 = r1
        L_0x0227:
            r2 = r23
            goto L_0x0128
        L_0x022b:
            if (r15 == 0) goto L_0x0230
            int r3 = r0.f2207
            goto L_0x0231
        L_0x0230:
            r3 = 0
        L_0x0231:
            r7 = 0
            r1 = r11[r7]
            int r3 = r3 - r1
            int r1 = java.lang.Math.max(r7, r3)
            int r2 = r23 + r1
            int r1 = -r3
            int r1 = java.lang.Math.max(r7, r1)
            r11[r7] = r1
            if (r13 == 0) goto L_0x0267
            android.widget.TextView r1 = r0.f2188
            android.view.ViewGroup$LayoutParams r1 = r1.getLayoutParams()
            android.support.v7.widget.Toolbar$ʼ r1 = (android.support.v7.widget.Toolbar.C0571) r1
            android.widget.TextView r3 = r0.f2188
            int r3 = r3.getMeasuredWidth()
            int r3 = r3 + r2
            android.widget.TextView r4 = r0.f2188
            int r4 = r4.getMeasuredHeight()
            int r4 = r4 + r8
            android.widget.TextView r5 = r0.f2188
            r5.layout(r2, r8, r3, r4)
            int r5 = r0.f2209
            int r3 = r3 + r5
            int r1 = r1.bottomMargin
            int r8 = r4 + r1
            goto L_0x0268
        L_0x0267:
            r3 = r2
        L_0x0268:
            if (r14 == 0) goto L_0x028e
            android.widget.TextView r1 = r0.f2190
            android.view.ViewGroup$LayoutParams r1 = r1.getLayoutParams()
            android.support.v7.widget.Toolbar$ʼ r1 = (android.support.v7.widget.Toolbar.C0571) r1
            int r4 = r1.topMargin
            int r8 = r8 + r4
            android.widget.TextView r4 = r0.f2190
            int r4 = r4.getMeasuredWidth()
            int r4 = r4 + r2
            android.widget.TextView r5 = r0.f2190
            int r5 = r5.getMeasuredHeight()
            int r5 = r5 + r8
            android.widget.TextView r6 = r0.f2190
            r6.layout(r2, r8, r4, r5)
            int r5 = r0.f2209
            int r4 = r4 + r5
            int r1 = r1.bottomMargin
            goto L_0x028f
        L_0x028e:
            r4 = r2
        L_0x028f:
            if (r15 == 0) goto L_0x0295
            int r2 = java.lang.Math.max(r3, r4)
        L_0x0295:
            java.util.ArrayList<android.view.View> r1 = r0.f2181
            r3 = 3
            r0.m3572(r1, r3)
            java.util.ArrayList<android.view.View> r1 = r0.f2181
            int r1 = r1.size()
            r3 = r2
            r2 = 0
        L_0x02a3:
            if (r2 >= r1) goto L_0x02b6
            java.util.ArrayList<android.view.View> r4 = r0.f2181
            java.lang.Object r4 = r4.get(r2)
            android.view.View r4 = (android.view.View) r4
            r12 = r21
            int r3 = r0.m3568(r4, r3, r11, r12)
            int r2 = r2 + 1
            goto L_0x02a3
        L_0x02b6:
            r12 = r21
            java.util.ArrayList<android.view.View> r1 = r0.f2181
            r2 = 5
            r0.m3572(r1, r2)
            java.util.ArrayList<android.view.View> r1 = r0.f2181
            int r1 = r1.size()
            r2 = 0
        L_0x02c5:
            if (r2 >= r1) goto L_0x02d6
            java.util.ArrayList<android.view.View> r4 = r0.f2181
            java.lang.Object r4 = r4.get(r2)
            android.view.View r4 = (android.view.View) r4
            int r10 = r0.m3576(r4, r10, r11, r12)
            int r2 = r2 + 1
            goto L_0x02c5
        L_0x02d6:
            java.util.ArrayList<android.view.View> r1 = r0.f2181
            r2 = 1
            r0.m3572(r1, r2)
            java.util.ArrayList<android.view.View> r1 = r0.f2181
            int r1 = r0.m3569(r1, r11)
            int r4 = r16 - r17
            int r4 = r4 - r22
            int r4 = r4 / 2
            int r6 = r17 + r4
            int r2 = r1 / 2
            int r2 = r6 - r2
            int r1 = r1 + r2
            if (r2 >= r3) goto L_0x02f2
            goto L_0x02f9
        L_0x02f2:
            if (r1 <= r10) goto L_0x02f8
            int r1 = r1 - r10
            int r3 = r2 - r1
            goto L_0x02f9
        L_0x02f8:
            r3 = r2
        L_0x02f9:
            java.util.ArrayList<android.view.View> r1 = r0.f2181
            int r1 = r1.size()
        L_0x02ff:
            if (r7 >= r1) goto L_0x0310
            java.util.ArrayList<android.view.View> r2 = r0.f2181
            java.lang.Object r2 = r2.get(r7)
            android.view.View r2 = (android.view.View) r2
            int r3 = r0.m3568(r2, r3, r11, r12)
            int r7 = r7 + 1
            goto L_0x02ff
        L_0x0310:
            java.util.ArrayList<android.view.View> r1 = r0.f2181
            r1.clear()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.Toolbar.onLayout(boolean, int, int, int, int):void");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m3569(List<View> list, int[] iArr) {
        int i = iArr[0];
        int i2 = iArr[1];
        int size = list.size();
        int i3 = i2;
        int i4 = i;
        int i5 = 0;
        int i6 = 0;
        while (i5 < size) {
            View view = list.get(i5);
            C0571 r6 = (C0571) view.getLayoutParams();
            int i7 = r6.leftMargin - i4;
            int i8 = r6.rightMargin - i3;
            int max = Math.max(0, i7);
            int max2 = Math.max(0, i8);
            int max3 = Math.max(0, -i7);
            int max4 = Math.max(0, -i8);
            i6 += max + view.getMeasuredWidth() + max2;
            i5++;
            i3 = max4;
            i4 = max3;
        }
        return i6;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m3568(View view, int i, int[] iArr, int i2) {
        C0571 r0 = (C0571) view.getLayoutParams();
        int i3 = r0.leftMargin - iArr[0];
        int max = i + Math.max(0, i3);
        iArr[0] = Math.max(0, -i3);
        int r7 = m3566(view, i2);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max, r7, max + measuredWidth, view.getMeasuredHeight() + r7);
        return max + measuredWidth + r0.rightMargin;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int m3576(View view, int i, int[] iArr, int i2) {
        C0571 r0 = (C0571) view.getLayoutParams();
        int i3 = r0.rightMargin - iArr[1];
        int max = i - Math.max(0, i3);
        iArr[1] = Math.max(0, -i3);
        int r8 = m3566(view, i2);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max - measuredWidth, r8, max, view.getMeasuredHeight() + r8);
        return max - (measuredWidth + r0.leftMargin);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m3566(View view, int i) {
        C0571 r0 = (C0571) view.getLayoutParams();
        int measuredHeight = view.getMeasuredHeight();
        int i2 = i > 0 ? (measuredHeight - i) / 2 : 0;
        int r2 = m3565(r0.f1390);
        if (r2 == 48) {
            return getPaddingTop() - i2;
        }
        if (r2 == 80) {
            return (((getHeight() - getPaddingBottom()) - measuredHeight) - r0.bottomMargin) - i2;
        }
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int height = getHeight();
        int i3 = (((height - paddingTop) - paddingBottom) - measuredHeight) / 2;
        if (i3 < r0.topMargin) {
            i3 = r0.topMargin;
        } else {
            int i4 = (((height - paddingBottom) - measuredHeight) - i3) - paddingTop;
            if (i4 < r0.bottomMargin) {
                i3 = Math.max(0, i3 - (r0.bottomMargin - i4));
            }
        }
        return paddingTop + i3;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m3565(int i) {
        int i2 = i & 112;
        return (i2 == 16 || i2 == 48 || i2 == 80) ? i2 : this.f2216 & 112;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3572(List<View> list, int i) {
        boolean z = C0414.m2236(this) == 1;
        int childCount = getChildCount();
        int r7 = C0396.m2155(i, C0414.m2236(this));
        list.clear();
        if (z) {
            for (int i2 = childCount - 1; i2 >= 0; i2--) {
                View childAt = getChildAt(i2);
                C0571 r1 = (C0571) childAt.getLayoutParams();
                if (r1.f2225 == 0 && m3573(childAt) && m3574(r1.f1390) == r7) {
                    list.add(childAt);
                }
            }
            return;
        }
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt2 = getChildAt(i3);
            C0571 r2 = (C0571) childAt2.getLayoutParams();
            if (r2.f2225 == 0 && m3573(childAt2) && m3574(r2.f1390) == r7) {
                list.add(childAt2);
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int m3574(int i) {
        int r0 = C0414.m2236(this);
        int r5 = C0396.m2155(i, r0) & 7;
        if (r5 == 1 || r5 == 3 || r5 == 5) {
            return r5;
        }
        return r0 == 1 ? 5 : 3;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m3573(View view) {
        return (view == null || view.getParent() != this || view.getVisibility() == 8) ? false : true;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int m3575(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return C0400.m2161(marginLayoutParams) + C0400.m2162(marginLayoutParams);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private int m3577(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return marginLayoutParams.topMargin + marginLayoutParams.bottomMargin;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0571 generateLayoutParams(AttributeSet attributeSet) {
        return new C0571(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0571 generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof C0571) {
            return new C0571((C0571) layoutParams);
        }
        if (layoutParams instanceof C0444.C0445) {
            return new C0571((C0444.C0445) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new C0571((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new C0571(layoutParams);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˋ  reason: contains not printable characters */
    public C0571 generateDefaultLayoutParams() {
        return new C0571(-2, -2);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return super.checkLayoutParams(layoutParams) && (layoutParams instanceof C0571);
    }

    public C0631 getWrapper() {
        if (this.f2189 == null) {
            this.f2189 = new C0593(this, true);
        }
        return this.f2189;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public void m3603() {
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            if (!(((C0571) childAt.getLayoutParams()).f2225 == 2 || childAt == this.f2186)) {
                removeViewAt(childCount);
                this.f2187.add(childAt);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m3604() {
        for (int size = this.f2187.size() - 1; size >= 0; size--) {
            addView(this.f2187.get(size));
        }
        this.f2187.clear();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean m3578(View view) {
        return view.getParent() == this || this.f2187.contains(view);
    }

    public void setCollapsible(boolean z) {
        this.f2201 = z;
        requestLayout();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3591(C0518.C0519 r2, C0504.C0505 r3) {
        this.f2197 = r2;
        this.f2195 = r3;
        ActionMenuView actionMenuView = this.f2186;
        if (actionMenuView != null) {
            actionMenuView.m3194(r2, r3);
        }
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    private void m3585() {
        if (this.f2213 == null) {
            this.f2213 = new C0578();
        }
    }

    /* access modifiers changed from: package-private */
    public C0614 getOuterActionMenuPresenter() {
        return this.f2193;
    }

    /* access modifiers changed from: package-private */
    public Context getPopupContext() {
        return this.f2200;
    }

    /* renamed from: android.support.v7.widget.Toolbar$ʼ  reason: contains not printable characters */
    public static class C0571 extends C0444.C0445 {

        /* renamed from: ʼ  reason: contains not printable characters */
        int f2225 = 0;

        public C0571(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public C0571(int i, int i2) {
            super(i, i2);
            this.f1390 = 8388627;
        }

        public C0571(C0571 r2) {
            super((C0444.C0445) r2);
            this.f2225 = r2.f2225;
        }

        public C0571(C0444.C0445 r1) {
            super(r1);
        }

        public C0571(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            m3617(marginLayoutParams);
        }

        public C0571(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3617(ViewGroup.MarginLayoutParams marginLayoutParams) {
            this.leftMargin = marginLayoutParams.leftMargin;
            this.topMargin = marginLayoutParams.topMargin;
            this.rightMargin = marginLayoutParams.rightMargin;
            this.bottomMargin = marginLayoutParams.bottomMargin;
        }
    }

    /* renamed from: android.support.v7.widget.Toolbar$ʾ  reason: contains not printable characters */
    public static class C0573 extends C0355 {
        public static final Parcelable.Creator<C0573> CREATOR = new Parcelable.ClassLoaderCreator<C0573>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C0573 createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new C0573(parcel, classLoader);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0573 createFromParcel(Parcel parcel) {
                return new C0573(parcel, null);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0573[] newArray(int i) {
                return new C0573[i];
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        int f2226;

        /* renamed from: ʼ  reason: contains not printable characters */
        boolean f2227;

        public C0573(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f2226 = parcel.readInt();
            this.f2227 = parcel.readInt() != 0;
        }

        public C0573(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f2226);
            parcel.writeInt(this.f2227 ? 1 : 0);
        }
    }

    /* renamed from: android.support.v7.widget.Toolbar$ʻ  reason: contains not printable characters */
    private class C0570 implements C0518 {

        /* renamed from: ʻ  reason: contains not printable characters */
        C0504 f2222;

        /* renamed from: ʼ  reason: contains not printable characters */
        C0508 f2223;

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3607(Parcelable parcelable) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3608(C0504 r1, boolean z) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3609(C0518.C0519 r1) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m3611() {
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m3613(C0526 r1) {
            return false;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m3614() {
            return 0;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public Parcelable m3616() {
            return null;
        }

        C0570() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3606(Context context, C0504 r3) {
            C0508 r0;
            C0504 r2 = this.f2222;
            if (!(r2 == null || (r0 = this.f2223) == null)) {
                r2.m2917(r0);
            }
            this.f2222 = r3;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3610(boolean z) {
            if (this.f2223 != null) {
                C0504 r5 = this.f2222;
                boolean z2 = false;
                if (r5 != null) {
                    int size = r5.size();
                    int i = 0;
                    while (true) {
                        if (i >= size) {
                            break;
                        } else if (this.f2222.getItem(i) == this.f2223) {
                            z2 = true;
                            break;
                        } else {
                            i++;
                        }
                    }
                }
                if (!z2) {
                    m3615(this.f2222, this.f2223);
                }
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m3612(C0504 r3, C0508 r4) {
            Toolbar.this.m3601();
            ViewParent parent = Toolbar.this.f2178.getParent();
            Toolbar toolbar = Toolbar.this;
            if (parent != toolbar) {
                toolbar.addView(toolbar.f2178);
            }
            Toolbar.this.f2180 = r4.getActionView();
            this.f2223 = r4;
            ViewParent parent2 = Toolbar.this.f2180.getParent();
            Toolbar toolbar2 = Toolbar.this;
            if (parent2 != toolbar2) {
                C0571 r32 = toolbar2.generateDefaultLayoutParams();
                r32.f1390 = 8388611 | (Toolbar.this.f2182 & 112);
                r32.f2225 = 2;
                Toolbar.this.f2180.setLayoutParams(r32);
                Toolbar toolbar3 = Toolbar.this;
                toolbar3.addView(toolbar3.f2180);
            }
            Toolbar.this.m3603();
            Toolbar.this.requestLayout();
            r4.m2959(true);
            if (Toolbar.this.f2180 instanceof C0531) {
                ((C0531) Toolbar.this.f2180).m3101();
            }
            return true;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m3615(C0504 r2, C0508 r3) {
            if (Toolbar.this.f2180 instanceof C0531) {
                ((C0531) Toolbar.this.f2180).m3102();
            }
            Toolbar toolbar = Toolbar.this;
            toolbar.removeView(toolbar.f2180);
            Toolbar toolbar2 = Toolbar.this;
            toolbar2.removeView(toolbar2.f2178);
            Toolbar toolbar3 = Toolbar.this;
            toolbar3.f2180 = null;
            toolbar3.m3604();
            this.f2223 = null;
            Toolbar.this.requestLayout();
            r3.m2959(false);
            return true;
        }
    }
}
