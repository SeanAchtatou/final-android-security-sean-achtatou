package android.support.v7.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* renamed from: android.support.v7.widget.ʿ  reason: contains not printable characters */
/* compiled from: ActivityChooserModel */
class C0625 extends DataSetObservable {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final String f2472 = "ʿ";

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final Object f2473 = new Object();

    /* renamed from: ˆ  reason: contains not printable characters */
    private static final Map<String, C0625> f2474 = new HashMap();

    /* renamed from: ʼ  reason: contains not printable characters */
    final Context f2475;

    /* renamed from: ʽ  reason: contains not printable characters */
    final String f2476;

    /* renamed from: ʾ  reason: contains not printable characters */
    boolean f2477;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final Object f2478;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final List<C0626> f2479;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final List<C0628> f2480;

    /* renamed from: ˋ  reason: contains not printable characters */
    private Intent f2481;

    /* renamed from: ˎ  reason: contains not printable characters */
    private C0627 f2482;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f2483;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f2484;

    /* renamed from: י  reason: contains not printable characters */
    private boolean f2485;

    /* renamed from: ـ  reason: contains not printable characters */
    private boolean f2486;

    /* renamed from: ٴ  reason: contains not printable characters */
    private C0629 f2487;

    /* renamed from: android.support.v7.widget.ʿ$ʼ  reason: contains not printable characters */
    /* compiled from: ActivityChooserModel */
    public interface C0627 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m3986(Intent intent, List<C0626> list, List<C0628> list2);
    }

    /* renamed from: android.support.v7.widget.ʿ$ʾ  reason: contains not printable characters */
    /* compiled from: ActivityChooserModel */
    public interface C0629 {
        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m3987(C0625 r1, Intent intent);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m3979() {
        int size;
        synchronized (this.f2478) {
            m3973();
            size = this.f2479.size();
        }
        return size;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public ResolveInfo m3981(int i) {
        ResolveInfo resolveInfo;
        synchronized (this.f2478) {
            m3973();
            resolveInfo = this.f2479.get(i).f2488;
        }
        return resolveInfo;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m3980(ResolveInfo resolveInfo) {
        synchronized (this.f2478) {
            m3973();
            List<C0626> list = this.f2479;
            int size = list.size();
            for (int i = 0; i < size; i++) {
                if (list.get(i).f2488 == resolveInfo) {
                    return i;
                }
            }
            return -1;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Intent m3982(int i) {
        synchronized (this.f2478) {
            if (this.f2481 == null) {
                return null;
            }
            m3973();
            C0626 r7 = this.f2479.get(i);
            ComponentName componentName = new ComponentName(r7.f2488.activityInfo.packageName, r7.f2488.activityInfo.name);
            Intent intent = new Intent(this.f2481);
            intent.setComponent(componentName);
            if (this.f2487 != null) {
                if (this.f2487.m3987(this, new Intent(intent))) {
                    return null;
                }
            }
            m3971(new C0628(componentName, System.currentTimeMillis(), 1.0f));
            return intent;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public ResolveInfo m3983() {
        synchronized (this.f2478) {
            m3973();
            if (this.f2479.isEmpty()) {
                return null;
            }
            ResolveInfo resolveInfo = this.f2479.get(0).f2488;
            return resolveInfo;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3984(int i) {
        synchronized (this.f2478) {
            m3973();
            C0626 r6 = this.f2479.get(i);
            C0626 r1 = this.f2479.get(0);
            m3971(new C0628(new ComponentName(r6.f2488.activityInfo.packageName, r6.f2488.activityInfo.name), System.currentTimeMillis(), r1 != null ? (r1.f2489 - r6.f2489) + 5.0f : 1.0f));
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m3972() {
        if (!this.f2484) {
            throw new IllegalStateException("No preceding call to #readHistoricalData");
        } else if (this.f2485) {
            this.f2485 = false;
            if (!TextUtils.isEmpty(this.f2476)) {
                new C0630().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new ArrayList(this.f2480), this.f2476);
            }
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m3973() {
        boolean r0 = m3975() | m3976();
        m3977();
        if (r0) {
            m3974();
            notifyChanged();
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean m3974() {
        if (this.f2482 == null || this.f2481 == null || this.f2479.isEmpty() || this.f2480.isEmpty()) {
            return false;
        }
        this.f2482.m3986(this.f2481, this.f2479, Collections.unmodifiableList(this.f2480));
        return true;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean m3975() {
        if (!this.f2486 || this.f2481 == null) {
            return false;
        }
        this.f2486 = false;
        this.f2479.clear();
        List<ResolveInfo> queryIntentActivities = this.f2475.getPackageManager().queryIntentActivities(this.f2481, 0);
        int size = queryIntentActivities.size();
        for (int i = 0; i < size; i++) {
            this.f2479.add(new C0626(queryIntentActivities.get(i)));
        }
        return true;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean m3976() {
        if (!this.f2477 || !this.f2485 || TextUtils.isEmpty(this.f2476)) {
            return false;
        }
        this.f2477 = false;
        this.f2484 = true;
        m3978();
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m3971(C0628 r2) {
        boolean add = this.f2480.add(r2);
        if (add) {
            this.f2485 = true;
            m3977();
            m3972();
            m3974();
            notifyChanged();
        }
        return add;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private void m3977() {
        int size = this.f2480.size() - this.f2483;
        if (size > 0) {
            this.f2485 = true;
            for (int i = 0; i < size; i++) {
                C0628 remove = this.f2480.remove(0);
            }
        }
    }

    /* renamed from: android.support.v7.widget.ʿ$ʽ  reason: contains not printable characters */
    /* compiled from: ActivityChooserModel */
    public static final class C0628 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public final ComponentName f2490;

        /* renamed from: ʼ  reason: contains not printable characters */
        public final long f2491;

        /* renamed from: ʽ  reason: contains not printable characters */
        public final float f2492;

        public C0628(String str, long j, float f) {
            this(ComponentName.unflattenFromString(str), j, f);
        }

        public C0628(ComponentName componentName, long j, float f) {
            this.f2490 = componentName;
            this.f2491 = j;
            this.f2492 = f;
        }

        public int hashCode() {
            ComponentName componentName = this.f2490;
            int hashCode = componentName == null ? 0 : componentName.hashCode();
            long j = this.f2491;
            return ((((hashCode + 31) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + Float.floatToIntBits(this.f2492);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C0628 r8 = (C0628) obj;
            ComponentName componentName = this.f2490;
            if (componentName == null) {
                if (r8.f2490 != null) {
                    return false;
                }
            } else if (!componentName.equals(r8.f2490)) {
                return false;
            }
            return this.f2491 == r8.f2491 && Float.floatToIntBits(this.f2492) == Float.floatToIntBits(r8.f2492);
        }

        public String toString() {
            return "[" + "; activity:" + this.f2490 + "; time:" + this.f2491 + "; weight:" + new BigDecimal((double) this.f2492) + "]";
        }
    }

    /* renamed from: android.support.v7.widget.ʿ$ʻ  reason: contains not printable characters */
    /* compiled from: ActivityChooserModel */
    public static final class C0626 implements Comparable<C0626> {

        /* renamed from: ʻ  reason: contains not printable characters */
        public final ResolveInfo f2488;

        /* renamed from: ʼ  reason: contains not printable characters */
        public float f2489;

        public C0626(ResolveInfo resolveInfo) {
            this.f2488 = resolveInfo;
        }

        public int hashCode() {
            return Float.floatToIntBits(this.f2489) + 31;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return obj != null && getClass() == obj.getClass() && Float.floatToIntBits(this.f2489) == Float.floatToIntBits(((C0626) obj).f2489);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int compareTo(C0626 r2) {
            return Float.floatToIntBits(r2.f2489) - Float.floatToIntBits(this.f2489);
        }

        public String toString() {
            return "[" + "resolveInfo:" + this.f2488.toString() + "; weight:" + new BigDecimal((double) this.f2489) + "]";
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private void m3978() {
        try {
            FileInputStream openFileInput = this.f2475.openFileInput(this.f2476);
            try {
                XmlPullParser newPullParser = Xml.newPullParser();
                newPullParser.setInput(openFileInput, "UTF-8");
                int i = 0;
                while (i != 1 && i != 2) {
                    i = newPullParser.next();
                }
                if ("historical-records".equals(newPullParser.getName())) {
                    List<C0628> list = this.f2480;
                    list.clear();
                    while (true) {
                        int next = newPullParser.next();
                        if (next == 1) {
                            if (openFileInput == null) {
                                return;
                            }
                        } else if (!(next == 3 || next == 4)) {
                            if ("historical-record".equals(newPullParser.getName())) {
                                list.add(new C0628(newPullParser.getAttributeValue(null, "activity"), Long.parseLong(newPullParser.getAttributeValue(null, "time")), Float.parseFloat(newPullParser.getAttributeValue(null, "weight"))));
                            } else {
                                throw new XmlPullParserException("Share records file not well-formed.");
                            }
                        }
                    }
                    try {
                        openFileInput.close();
                    } catch (IOException unused) {
                    }
                } else {
                    throw new XmlPullParserException("Share records file does not start with historical-records tag.");
                }
            } catch (XmlPullParserException e) {
                String str = f2472;
                Log.e(str, "Error reading historical recrod file: " + this.f2476, e);
                if (openFileInput == null) {
                }
            } catch (IOException e2) {
                String str2 = f2472;
                Log.e(str2, "Error reading historical recrod file: " + this.f2476, e2);
                if (openFileInput == null) {
                }
            } catch (Throwable th) {
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException unused2) {
                    }
                }
                throw th;
            }
        } catch (FileNotFoundException unused3) {
        }
    }

    /* renamed from: android.support.v7.widget.ʿ$ʿ  reason: contains not printable characters */
    /* compiled from: ActivityChooserModel */
    private final class C0630 extends AsyncTask<Object, Void, Void> {
        C0630() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x006d, code lost:
            if (r15 != null) goto L_0x006f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
            r15.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0092, code lost:
            if (r15 == null) goto L_0x00d5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00b2, code lost:
            if (r15 == null) goto L_0x00d5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x00d2, code lost:
            if (r15 == null) goto L_0x00d5;
         */
        /* renamed from: ʻ  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Void doInBackground(java.lang.Object... r15) {
            /*
                r14 = this;
                java.lang.String r0 = "historical-record"
                java.lang.String r1 = "historical-records"
                java.lang.String r2 = "Error writing historical record file: "
                r3 = 0
                r4 = r15[r3]
                java.util.List r4 = (java.util.List) r4
                r5 = 1
                r15 = r15[r5]
                java.lang.String r15 = (java.lang.String) r15
                r6 = 0
                android.support.v7.widget.ʿ r7 = android.support.v7.widget.C0625.this     // Catch:{ FileNotFoundException -> 0x00e0 }
                android.content.Context r7 = r7.f2475     // Catch:{ FileNotFoundException -> 0x00e0 }
                java.io.FileOutputStream r15 = r7.openFileOutput(r15, r3)     // Catch:{ FileNotFoundException -> 0x00e0 }
                org.xmlpull.v1.XmlSerializer r7 = android.util.Xml.newSerializer()
                r7.setOutput(r15, r6)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                java.lang.String r8 = "UTF-8"
                java.lang.Boolean r9 = java.lang.Boolean.valueOf(r5)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r7.startDocument(r8, r9)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r7.startTag(r6, r1)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                int r8 = r4.size()     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r9 = 0
            L_0x0031:
                if (r9 >= r8) goto L_0x0063
                java.lang.Object r10 = r4.remove(r3)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                android.support.v7.widget.ʿ$ʽ r10 = (android.support.v7.widget.C0625.C0628) r10     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r7.startTag(r6, r0)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                java.lang.String r11 = "activity"
                android.content.ComponentName r12 = r10.f2490     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                java.lang.String r12 = r12.flattenToString()     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r7.attribute(r6, r11, r12)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                java.lang.String r11 = "time"
                long r12 = r10.f2491     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                java.lang.String r12 = java.lang.String.valueOf(r12)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r7.attribute(r6, r11, r12)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                java.lang.String r11 = "weight"
                float r10 = r10.f2492     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r7.attribute(r6, r11, r10)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r7.endTag(r6, r0)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                int r9 = r9 + 1
                goto L_0x0031
            L_0x0063:
                r7.endTag(r6, r1)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r7.endDocument()     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                android.support.v7.widget.ʿ r0 = android.support.v7.widget.C0625.this
                r0.f2477 = r5
                if (r15 == 0) goto L_0x00d5
            L_0x006f:
                r15.close()     // Catch:{ IOException -> 0x00d5 }
                goto L_0x00d5
            L_0x0073:
                r0 = move-exception
                goto L_0x00d6
            L_0x0075:
                r0 = move-exception
                java.lang.String r1 = android.support.v7.widget.C0625.f2472     // Catch:{ all -> 0x0073 }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0073 }
                r3.<init>()     // Catch:{ all -> 0x0073 }
                r3.append(r2)     // Catch:{ all -> 0x0073 }
                android.support.v7.widget.ʿ r2 = android.support.v7.widget.C0625.this     // Catch:{ all -> 0x0073 }
                java.lang.String r2 = r2.f2476     // Catch:{ all -> 0x0073 }
                r3.append(r2)     // Catch:{ all -> 0x0073 }
                java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x0073 }
                android.util.Log.e(r1, r2, r0)     // Catch:{ all -> 0x0073 }
                android.support.v7.widget.ʿ r0 = android.support.v7.widget.C0625.this
                r0.f2477 = r5
                if (r15 == 0) goto L_0x00d5
                goto L_0x006f
            L_0x0095:
                r0 = move-exception
                java.lang.String r1 = android.support.v7.widget.C0625.f2472     // Catch:{ all -> 0x0073 }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0073 }
                r3.<init>()     // Catch:{ all -> 0x0073 }
                r3.append(r2)     // Catch:{ all -> 0x0073 }
                android.support.v7.widget.ʿ r2 = android.support.v7.widget.C0625.this     // Catch:{ all -> 0x0073 }
                java.lang.String r2 = r2.f2476     // Catch:{ all -> 0x0073 }
                r3.append(r2)     // Catch:{ all -> 0x0073 }
                java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x0073 }
                android.util.Log.e(r1, r2, r0)     // Catch:{ all -> 0x0073 }
                android.support.v7.widget.ʿ r0 = android.support.v7.widget.C0625.this
                r0.f2477 = r5
                if (r15 == 0) goto L_0x00d5
                goto L_0x006f
            L_0x00b5:
                r0 = move-exception
                java.lang.String r1 = android.support.v7.widget.C0625.f2472     // Catch:{ all -> 0x0073 }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0073 }
                r3.<init>()     // Catch:{ all -> 0x0073 }
                r3.append(r2)     // Catch:{ all -> 0x0073 }
                android.support.v7.widget.ʿ r2 = android.support.v7.widget.C0625.this     // Catch:{ all -> 0x0073 }
                java.lang.String r2 = r2.f2476     // Catch:{ all -> 0x0073 }
                r3.append(r2)     // Catch:{ all -> 0x0073 }
                java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x0073 }
                android.util.Log.e(r1, r2, r0)     // Catch:{ all -> 0x0073 }
                android.support.v7.widget.ʿ r0 = android.support.v7.widget.C0625.this
                r0.f2477 = r5
                if (r15 == 0) goto L_0x00d5
                goto L_0x006f
            L_0x00d5:
                return r6
            L_0x00d6:
                android.support.v7.widget.ʿ r1 = android.support.v7.widget.C0625.this
                r1.f2477 = r5
                if (r15 == 0) goto L_0x00df
                r15.close()     // Catch:{ IOException -> 0x00df }
            L_0x00df:
                throw r0
            L_0x00e0:
                r0 = move-exception
                java.lang.String r1 = android.support.v7.widget.C0625.f2472
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                r3.append(r2)
                r3.append(r15)
                java.lang.String r15 = r3.toString()
                android.util.Log.e(r1, r15, r0)
                return r6
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0625.C0630.doInBackground(java.lang.Object[]):java.lang.Void");
        }
    }
}
