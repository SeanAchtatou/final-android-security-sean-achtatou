package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.widget.TextView;

/* renamed from: android.support.v7.widget.ᐧᐧ  reason: contains not printable characters */
/* compiled from: AppCompatTextHelperV17 */
class C0675 extends C0726 {

    /* renamed from: ʼ  reason: contains not printable characters */
    private C0590 f2693;

    /* renamed from: ʽ  reason: contains not printable characters */
    private C0590 f2694;

    C0675(TextView textView) {
        super(textView);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4252(AttributeSet attributeSet, int i) {
        super.m4864(attributeSet, i);
        Context context = this.f2961.getContext();
        C0656 r1 = C0656.m4164();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0727.C0737.AppCompatTextHelper, i, 0);
        if (obtainStyledAttributes.hasValue(C0727.C0737.AppCompatTextHelper_android_drawableStart)) {
            this.f2693 = m4854(context, r1, obtainStyledAttributes.getResourceId(C0727.C0737.AppCompatTextHelper_android_drawableStart, 0));
        }
        if (obtainStyledAttributes.hasValue(C0727.C0737.AppCompatTextHelper_android_drawableEnd)) {
            this.f2694 = m4854(context, r1, obtainStyledAttributes.getResourceId(C0727.C0737.AppCompatTextHelper_android_drawableEnd, 0));
        }
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4251() {
        super.m4858();
        if (this.f2693 != null || this.f2694 != null) {
            Drawable[] compoundDrawablesRelative = this.f2961.getCompoundDrawablesRelative();
            m4863(compoundDrawablesRelative[0], this.f2693);
            m4863(compoundDrawablesRelative[2], this.f2694);
        }
    }
}
