package android.support.v7.widget;

import android.support.v4.ˈ.C0348;
import android.support.v7.widget.C0685;
import android.support.v7.widget.C0690;
import java.util.ArrayList;
import java.util.List;

/* renamed from: android.support.v7.widget.ˆ  reason: contains not printable characters */
/* compiled from: AdapterHelper */
class C0632 implements C0685.C0686 {

    /* renamed from: ʻ  reason: contains not printable characters */
    final ArrayList<C0634> f2494;

    /* renamed from: ʼ  reason: contains not printable characters */
    final ArrayList<C0634> f2495;

    /* renamed from: ʽ  reason: contains not printable characters */
    final C0633 f2496;

    /* renamed from: ʾ  reason: contains not printable characters */
    Runnable f2497;

    /* renamed from: ʿ  reason: contains not printable characters */
    final boolean f2498;

    /* renamed from: ˆ  reason: contains not printable characters */
    final C0685 f2499;

    /* renamed from: ˈ  reason: contains not printable characters */
    private C0348.C0349<C0634> f2500;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f2501;

    /* renamed from: android.support.v7.widget.ˆ$ʻ  reason: contains not printable characters */
    /* compiled from: AdapterHelper */
    interface C0633 {
        /* renamed from: ʻ  reason: contains not printable characters */
        C0690.C0720 m4043(int i);

        /* renamed from: ʻ  reason: contains not printable characters */
        void m4044(int i, int i2);

        /* renamed from: ʻ  reason: contains not printable characters */
        void m4045(int i, int i2, Object obj);

        /* renamed from: ʻ  reason: contains not printable characters */
        void m4046(C0634 r1);

        /* renamed from: ʼ  reason: contains not printable characters */
        void m4047(int i, int i2);

        /* renamed from: ʼ  reason: contains not printable characters */
        void m4048(C0634 r1);

        /* renamed from: ʽ  reason: contains not printable characters */
        void m4049(int i, int i2);

        /* renamed from: ʾ  reason: contains not printable characters */
        void m4050(int i, int i2);
    }

    C0632(C0633 r2) {
        this(r2, false);
    }

    C0632(C0633 r3, boolean z) {
        this.f2500 = new C0348.C0350(30);
        this.f2494 = new ArrayList<>();
        this.f2495 = new ArrayList<>();
        this.f2501 = 0;
        this.f2496 = r3;
        this.f2498 = z;
        this.f2499 = new C0685(this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4031() {
        m4034(this.f2494);
        m4034(this.f2495);
        this.f2501 = 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4037() {
        this.f2499.m4281(this.f2494);
        int size = this.f2494.size();
        for (int i = 0; i < size; i++) {
            C0634 r2 = this.f2494.get(i);
            int i2 = r2.f2502;
            if (i2 == 1) {
                m4027(r2);
            } else if (i2 == 2) {
                m4023(r2);
            } else if (i2 == 4) {
                m4024(r2);
            } else if (i2 == 8) {
                m4022(r2);
            }
            Runnable runnable = this.f2497;
            if (runnable != null) {
                runnable.run();
            }
        }
        this.f2494.clear();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m4039() {
        int size = this.f2495.size();
        for (int i = 0; i < size; i++) {
            this.f2496.m4048(this.f2495.get(i));
        }
        m4034(this.f2495);
        this.f2501 = 0;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m4022(C0634 r1) {
        m4028(r1);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m4023(C0634 r11) {
        char c;
        boolean z;
        boolean z2;
        int i = r11.f2503;
        int i2 = r11.f2503 + r11.f2505;
        int i3 = r11.f2503;
        int i4 = 0;
        char c2 = 65535;
        while (i3 < i2) {
            if (this.f2496.m4043(i3) != null || m4025(i3)) {
                if (c2 == 0) {
                    m4026(m4030(2, i, i4, null));
                    z2 = true;
                } else {
                    z2 = false;
                }
                c = 1;
            } else {
                if (c2 == 1) {
                    m4028(m4030(2, i, i4, null));
                    z = true;
                } else {
                    z = false;
                }
                c = 0;
            }
            if (z) {
                i3 -= i4;
                i2 -= i4;
                i4 = 1;
            } else {
                i4++;
            }
            i3++;
            c2 = c;
        }
        if (i4 != r11.f2505) {
            m4032(r11);
            r11 = m4030(2, i, i4, null);
        }
        if (c2 == 0) {
            m4026(r11);
        } else {
            m4028(r11);
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m4024(C0634 r10) {
        int i = r10.f2503;
        int i2 = r10.f2503 + r10.f2505;
        int i3 = i;
        int i4 = 0;
        char c = 65535;
        for (int i5 = r10.f2503; i5 < i2; i5++) {
            if (this.f2496.m4043(i5) != null || m4025(i5)) {
                if (c == 0) {
                    m4026(m4030(4, i3, i4, r10.f2504));
                    i3 = i5;
                    i4 = 0;
                }
                c = 1;
            } else {
                if (c == 1) {
                    m4028(m4030(4, i3, i4, r10.f2504));
                    i3 = i5;
                    i4 = 0;
                }
                c = 0;
            }
            i4++;
        }
        if (i4 != r10.f2505) {
            Object obj = r10.f2504;
            m4032(r10);
            r10 = m4030(4, i3, i4, obj);
        }
        if (c == 0) {
            m4026(r10);
        } else {
            m4028(r10);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m4026(C0634 r13) {
        int i;
        if (r13.f2502 == 1 || r13.f2502 == 8) {
            throw new IllegalArgumentException("should not dispatch add or move for pre layout");
        }
        int r0 = m4021(r13.f2503, r13.f2502);
        int i2 = r13.f2503;
        int i3 = r13.f2502;
        if (i3 == 2) {
            i = 0;
        } else if (i3 == 4) {
            i = 1;
        } else {
            throw new IllegalArgumentException("op should be remove or update." + r13);
        }
        int i4 = r0;
        int i5 = i2;
        int i6 = 1;
        for (int i7 = 1; i7 < r13.f2505; i7++) {
            int r9 = m4021(r13.f2503 + (i * i7), r13.f2502);
            int i8 = r13.f2502;
            if (i8 == 2 ? r9 == i4 : i8 == 4 && r9 == i4 + 1) {
                i6++;
            } else {
                C0634 r7 = m4030(r13.f2502, i4, i6, r13.f2504);
                m4033(r7, i5);
                m4032(r7);
                if (r13.f2502 == 4) {
                    i5 += i6;
                }
                i4 = r9;
                i6 = 1;
            }
        }
        Object obj = r13.f2504;
        m4032(r13);
        if (i6 > 0) {
            C0634 r132 = m4030(r13.f2502, i4, i6, obj);
            m4033(r132, i5);
            m4032(r132);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4033(C0634 r3, int i) {
        this.f2496.m4046(r3);
        int i2 = r3.f2502;
        if (i2 == 2) {
            this.f2496.m4044(i, r3.f2505);
        } else if (i2 == 4) {
            this.f2496.m4045(i, r3.f2505, r3.f2504);
        } else {
            throw new IllegalArgumentException("only remove and update ops can be dispatched in first pass");
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int m4021(int i, int i2) {
        int i3;
        int i4;
        for (int size = this.f2495.size() - 1; size >= 0; size--) {
            C0634 r3 = this.f2495.get(size);
            if (r3.f2502 == 8) {
                if (r3.f2503 < r3.f2505) {
                    i4 = r3.f2503;
                    i3 = r3.f2505;
                } else {
                    i4 = r3.f2505;
                    i3 = r3.f2503;
                }
                if (i < i4 || i > i3) {
                    if (i < r3.f2503) {
                        if (i2 == 1) {
                            r3.f2503++;
                            r3.f2505++;
                        } else if (i2 == 2) {
                            r3.f2503--;
                            r3.f2505--;
                        }
                    }
                } else if (i4 == r3.f2503) {
                    if (i2 == 1) {
                        r3.f2505++;
                    } else if (i2 == 2) {
                        r3.f2505--;
                    }
                    i++;
                } else {
                    if (i2 == 1) {
                        r3.f2503++;
                    } else if (i2 == 2) {
                        r3.f2503--;
                    }
                    i--;
                }
            } else if (r3.f2503 <= i) {
                if (r3.f2502 == 1) {
                    i -= r3.f2505;
                } else if (r3.f2502 == 2) {
                    i += r3.f2505;
                }
            } else if (i2 == 1) {
                r3.f2503++;
            } else if (i2 == 2) {
                r3.f2503--;
            }
        }
        for (int size2 = this.f2495.size() - 1; size2 >= 0; size2--) {
            C0634 r0 = this.f2495.get(size2);
            if (r0.f2502 == 8) {
                if (r0.f2505 == r0.f2503 || r0.f2505 < 0) {
                    this.f2495.remove(size2);
                    m4032(r0);
                }
            } else if (r0.f2505 <= 0) {
                this.f2495.remove(size2);
                m4032(r0);
            }
        }
        return i;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean m4025(int i) {
        int size = this.f2495.size();
        for (int i2 = 0; i2 < size; i2++) {
            C0634 r3 = this.f2495.get(i2);
            if (r3.f2502 == 8) {
                if (m4029(r3.f2505, i2 + 1) == i) {
                    return true;
                }
            } else if (r3.f2502 == 1) {
                int i3 = r3.f2503 + r3.f2505;
                for (int i4 = r3.f2503; i4 < i3; i4++) {
                    if (m4029(i4, i2 + 1) == i) {
                        return true;
                    }
                }
                continue;
            } else {
                continue;
            }
        }
        return false;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private void m4027(C0634 r1) {
        m4028(r1);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private void m4028(C0634 r4) {
        this.f2495.add(r4);
        int i = r4.f2502;
        if (i == 1) {
            this.f2496.m4049(r4.f2503, r4.f2505);
        } else if (i == 2) {
            this.f2496.m4047(r4.f2503, r4.f2505);
        } else if (i == 4) {
            this.f2496.m4045(r4.f2503, r4.f2505, r4.f2504);
        } else if (i == 8) {
            this.f2496.m4050(r4.f2503, r4.f2505);
        } else {
            throw new IllegalArgumentException("Unknown update op type for " + r4);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m4040() {
        return this.f2494.size() > 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m4035(int i) {
        return (i & this.f2501) != 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m4036(int i) {
        return m4029(i, 0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m4029(int i, int i2) {
        int size = this.f2495.size();
        while (i2 < size) {
            C0634 r1 = this.f2495.get(i2);
            if (r1.f2502 == 8) {
                if (r1.f2503 == i) {
                    i = r1.f2505;
                } else {
                    if (r1.f2503 < i) {
                        i--;
                    }
                    if (r1.f2505 <= i) {
                        i++;
                    }
                }
            } else if (r1.f2503 > i) {
                continue;
            } else if (r1.f2502 == 2) {
                if (i < r1.f2503 + r1.f2505) {
                    return -1;
                }
                i -= r1.f2505;
            } else if (r1.f2502 == 1) {
                i += r1.f2505;
            }
            i2++;
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m4041() {
        m4039();
        int size = this.f2494.size();
        for (int i = 0; i < size; i++) {
            C0634 r3 = this.f2494.get(i);
            int i2 = r3.f2502;
            if (i2 == 1) {
                this.f2496.m4048(r3);
                this.f2496.m4049(r3.f2503, r3.f2505);
            } else if (i2 == 2) {
                this.f2496.m4048(r3);
                this.f2496.m4044(r3.f2503, r3.f2505);
            } else if (i2 == 4) {
                this.f2496.m4048(r3);
                this.f2496.m4045(r3.f2503, r3.f2505, r3.f2504);
            } else if (i2 == 8) {
                this.f2496.m4048(r3);
                this.f2496.m4050(r3.f2503, r3.f2505);
            }
            Runnable runnable = this.f2497;
            if (runnable != null) {
                runnable.run();
            }
        }
        m4034(this.f2494);
        this.f2501 = 0;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m4038(int i) {
        int size = this.f2494.size();
        for (int i2 = 0; i2 < size; i2++) {
            C0634 r2 = this.f2494.get(i2);
            int i3 = r2.f2502;
            if (i3 != 1) {
                if (i3 != 2) {
                    if (i3 == 8) {
                        if (r2.f2503 == i) {
                            i = r2.f2505;
                        } else {
                            if (r2.f2503 < i) {
                                i--;
                            }
                            if (r2.f2505 <= i) {
                                i++;
                            }
                        }
                    }
                } else if (r2.f2503 > i) {
                    continue;
                } else if (r2.f2503 + r2.f2505 > i) {
                    return -1;
                } else {
                    i -= r2.f2505;
                }
            } else if (r2.f2503 <= i) {
                i += r2.f2505;
            }
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m4042() {
        return !this.f2495.isEmpty() && !this.f2494.isEmpty();
    }

    /* renamed from: android.support.v7.widget.ˆ$ʼ  reason: contains not printable characters */
    /* compiled from: AdapterHelper */
    static class C0634 {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f2502;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f2503;

        /* renamed from: ʽ  reason: contains not printable characters */
        Object f2504;

        /* renamed from: ʾ  reason: contains not printable characters */
        int f2505;

        C0634(int i, int i2, int i3, Object obj) {
            this.f2502 = i;
            this.f2503 = i2;
            this.f2505 = i3;
            this.f2504 = obj;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public String m4051() {
            int i = this.f2502;
            if (i == 1) {
                return "add";
            }
            if (i == 2) {
                return "rm";
            }
            if (i != 4) {
                return i != 8 ? "??" : "mv";
            }
            return "up";
        }

        public String toString() {
            return Integer.toHexString(System.identityHashCode(this)) + "[" + m4051() + ",s:" + this.f2503 + "c:" + this.f2505 + ",p:" + this.f2504 + "]";
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C0634 r5 = (C0634) obj;
            int i = this.f2502;
            if (i != r5.f2502) {
                return false;
            }
            if (i == 8 && Math.abs(this.f2505 - this.f2503) == 1 && this.f2505 == r5.f2503 && this.f2503 == r5.f2505) {
                return true;
            }
            if (this.f2505 != r5.f2505 || this.f2503 != r5.f2503) {
                return false;
            }
            Object obj2 = this.f2504;
            if (obj2 != null) {
                if (!obj2.equals(r5.f2504)) {
                    return false;
                }
            } else if (r5.f2504 != null) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return (((this.f2502 * 31) + this.f2503) * 31) + this.f2505;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0634 m4030(int i, int i2, int i3, Object obj) {
        C0634 r0 = this.f2500.m1962();
        if (r0 == null) {
            return new C0634(i, i2, i3, obj);
        }
        r0.f2502 = i;
        r0.f2503 = i2;
        r0.f2505 = i3;
        r0.f2504 = obj;
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4032(C0634 r2) {
        if (!this.f2498) {
            r2.f2504 = null;
            this.f2500.m1963(r2);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4034(List<C0634> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            m4032(list.get(i));
        }
        list.clear();
    }
}
