package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.ʻ.C0727;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.widget.TextView;

/* renamed from: android.support.v7.widget.ﾞﾞ  reason: contains not printable characters */
/* compiled from: AppCompatTextHelper */
class C0726 {

    /* renamed from: ʻ  reason: contains not printable characters */
    final TextView f2961;

    /* renamed from: ʼ  reason: contains not printable characters */
    private C0590 f2962;

    /* renamed from: ʽ  reason: contains not printable characters */
    private C0590 f2963;

    /* renamed from: ʾ  reason: contains not printable characters */
    private C0590 f2964;

    /* renamed from: ʿ  reason: contains not printable characters */
    private C0590 f2965;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final C0577 f2966;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f2967 = 0;

    /* renamed from: ˉ  reason: contains not printable characters */
    private Typeface f2968;

    /* renamed from: ʻ  reason: contains not printable characters */
    static C0726 m4855(TextView textView) {
        if (Build.VERSION.SDK_INT >= 17) {
            return new C0675(textView);
        }
        return new C0726(textView);
    }

    C0726(TextView textView) {
        this.f2961 = textView;
        this.f2966 = new C0577(this.f2961);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ʻˏ.ʻ(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.widget.ʻˏ.ʻ(int, float):float
      android.support.v7.widget.ʻˏ.ʻ(int, int):int
      android.support.v7.widget.ʻˏ.ʻ(int, boolean):boolean */
    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4864(AttributeSet attributeSet, int i) {
        ColorStateList colorStateList;
        ColorStateList colorStateList2;
        boolean z;
        boolean z2;
        Context context = this.f2961.getContext();
        C0656 r1 = C0656.m4164();
        C0592 r2 = C0592.m3740(context, attributeSet, C0727.C0737.AppCompatTextHelper, i, 0);
        int r4 = r2.m3757(C0727.C0737.AppCompatTextHelper_android_textAppearance, -1);
        if (r2.m3758(C0727.C0737.AppCompatTextHelper_android_drawableLeft)) {
            this.f2962 = m4854(context, r1, r2.m3757(C0727.C0737.AppCompatTextHelper_android_drawableLeft, 0));
        }
        if (r2.m3758(C0727.C0737.AppCompatTextHelper_android_drawableTop)) {
            this.f2963 = m4854(context, r1, r2.m3757(C0727.C0737.AppCompatTextHelper_android_drawableTop, 0));
        }
        if (r2.m3758(C0727.C0737.AppCompatTextHelper_android_drawableRight)) {
            this.f2964 = m4854(context, r1, r2.m3757(C0727.C0737.AppCompatTextHelper_android_drawableRight, 0));
        }
        if (r2.m3758(C0727.C0737.AppCompatTextHelper_android_drawableBottom)) {
            this.f2965 = m4854(context, r1, r2.m3757(C0727.C0737.AppCompatTextHelper_android_drawableBottom, 0));
        }
        r2.m3745();
        boolean z3 = this.f2961.getTransformationMethod() instanceof PasswordTransformationMethod;
        boolean z4 = true;
        ColorStateList colorStateList3 = null;
        if (r4 != -1) {
            C0592 r42 = C0592.m3738(context, r4, C0727.C0737.TextAppearance);
            if (z3 || !r42.m3758(C0727.C0737.TextAppearance_textAllCaps)) {
                z2 = false;
                z = false;
            } else {
                z = r42.m3746(C0727.C0737.TextAppearance_textAllCaps, false);
                z2 = true;
            }
            m4856(context, r42);
            if (Build.VERSION.SDK_INT < 23) {
                ColorStateList r9 = r42.m3758(C0727.C0737.TextAppearance_android_textColor) ? r42.m3754(C0727.C0737.TextAppearance_android_textColor) : null;
                colorStateList = r42.m3758(C0727.C0737.TextAppearance_android_textColorHint) ? r42.m3754(C0727.C0737.TextAppearance_android_textColorHint) : null;
                if (r42.m3758(C0727.C0737.TextAppearance_android_textColorLink)) {
                    colorStateList3 = r42.m3754(C0727.C0737.TextAppearance_android_textColorLink);
                }
                ColorStateList colorStateList4 = r9;
                colorStateList2 = colorStateList3;
                colorStateList3 = colorStateList4;
            } else {
                colorStateList2 = null;
                colorStateList = null;
            }
            r42.m3745();
        } else {
            colorStateList2 = null;
            colorStateList = null;
            z2 = false;
            z = false;
        }
        C0592 r43 = C0592.m3740(context, attributeSet, C0727.C0737.TextAppearance, i, 0);
        if (z3 || !r43.m3758(C0727.C0737.TextAppearance_textAllCaps)) {
            z4 = z2;
        } else {
            z = r43.m3746(C0727.C0737.TextAppearance_textAllCaps, false);
        }
        if (Build.VERSION.SDK_INT < 23) {
            if (r43.m3758(C0727.C0737.TextAppearance_android_textColor)) {
                colorStateList3 = r43.m3754(C0727.C0737.TextAppearance_android_textColor);
            }
            if (r43.m3758(C0727.C0737.TextAppearance_android_textColorHint)) {
                colorStateList = r43.m3754(C0727.C0737.TextAppearance_android_textColorHint);
            }
            if (r43.m3758(C0727.C0737.TextAppearance_android_textColorLink)) {
                colorStateList2 = r43.m3754(C0727.C0737.TextAppearance_android_textColorLink);
            }
        }
        m4856(context, r43);
        r43.m3745();
        if (colorStateList3 != null) {
            this.f2961.setTextColor(colorStateList3);
        }
        if (colorStateList != null) {
            this.f2961.setHintTextColor(colorStateList);
        }
        if (colorStateList2 != null) {
            this.f2961.setLinkTextColor(colorStateList2);
        }
        if (!z3 && z4) {
            m4865(z);
        }
        Typeface typeface = this.f2968;
        if (typeface != null) {
            this.f2961.setTypeface(typeface, this.f2967);
        }
        this.f2966.m3653(attributeSet, i);
        if (Build.VERSION.SDK_INT >= 26 && this.f2966.m3649() != 0) {
            int[] r14 = this.f2966.m3658();
            if (r14.length <= 0) {
                return;
            }
            if (((float) this.f2961.getAutoSizeStepGranularity()) != -1.0f) {
                this.f2961.setAutoSizeTextTypeUniformWithConfiguration(this.f2966.m3656(), this.f2966.m3657(), this.f2966.m3655(), 0);
            } else {
                this.f2961.setAutoSizeTextTypeUniformWithPresetSizes(r14, 0);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4856(Context context, C0592 r4) {
        this.f2967 = r4.m3742(C0727.C0737.TextAppearance_android_textStyle, this.f2967);
        if (r4.m3758(C0727.C0737.TextAppearance_android_fontFamily) || r4.m3758(C0727.C0737.TextAppearance_fontFamily)) {
            this.f2968 = null;
            int i = r4.m3758(C0727.C0737.TextAppearance_android_fontFamily) ? C0727.C0737.TextAppearance_android_fontFamily : C0727.C0737.TextAppearance_fontFamily;
            if (!context.isRestricted()) {
                try {
                    this.f2968 = r4.m3743(i, this.f2967, this.f2961);
                } catch (Resources.NotFoundException | UnsupportedOperationException unused) {
                }
            }
            if (this.f2968 == null) {
                this.f2968 = Typeface.create(r4.m3752(i), this.f2967);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ʻˏ.ʻ(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.widget.ʻˏ.ʻ(int, float):float
      android.support.v7.widget.ʻˏ.ʻ(int, int):int
      android.support.v7.widget.ʻˏ.ʻ(int, boolean):boolean */
    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4862(Context context, int i) {
        ColorStateList r0;
        C0592 r4 = C0592.m3738(context, i, C0727.C0737.TextAppearance);
        if (r4.m3758(C0727.C0737.TextAppearance_textAllCaps)) {
            m4865(r4.m3746(C0727.C0737.TextAppearance_textAllCaps, false));
        }
        if (Build.VERSION.SDK_INT < 23 && r4.m3758(C0727.C0737.TextAppearance_android_textColor) && (r0 = r4.m3754(C0727.C0737.TextAppearance_android_textColor)) != null) {
            this.f2961.setTextColor(r0);
        }
        m4856(context, r4);
        r4.m3745();
        Typeface typeface = this.f2968;
        if (typeface != null) {
            this.f2961.setTypeface(typeface, this.f2967);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4865(boolean z) {
        this.f2961.setAllCaps(z);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4858() {
        if (this.f2962 != null || this.f2963 != null || this.f2964 != null || this.f2965 != null) {
            Drawable[] compoundDrawables = this.f2961.getCompoundDrawables();
            m4863(compoundDrawables[0], this.f2962);
            m4863(compoundDrawables[1], this.f2963);
            m4863(compoundDrawables[2], this.f2964);
            m4863(compoundDrawables[3], this.f2965);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m4863(Drawable drawable, C0590 r3) {
        if (drawable != null && r3 != null) {
            C0656.m4167(drawable, r3, this.f2961.getDrawableState());
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    protected static C0590 m4854(Context context, C0656 r1, int i) {
        ColorStateList r0 = r1.m4187(context, i);
        if (r0 == null) {
            return null;
        }
        C0590 r12 = new C0590();
        r12.f2322 = true;
        r12.f2319 = r0;
        return r12;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4866(boolean z, int i, int i2, int i3, int i4) {
        if (Build.VERSION.SDK_INT < 26) {
            m4868();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4860(int i, float f) {
        if (Build.VERSION.SDK_INT < 26 && !m4869()) {
            m4857(i, f);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4868() {
        this.f2966.m3659();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m4869() {
        return this.f2966.m3660();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m4857(int i, float f) {
        this.f2966.m3651(i, f);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4859(int i) {
        this.f2966.m3650(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4861(int i, int i2, int i3, int i4) {
        this.f2966.m3652(i, i2, i3, i4);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4867(int[] iArr, int i) {
        this.f2966.m3654(iArr, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public int m4870() {
        return this.f2966.m3649();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public int m4871() {
        return this.f2966.m3655();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public int m4872() {
        return this.f2966.m3656();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public int m4873() {
        return this.f2966.m3657();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public int[] m4874() {
        return this.f2966.m3658();
    }
}
