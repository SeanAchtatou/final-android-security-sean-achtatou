package android.support.v7.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.os.Build;
import android.support.v7.ʻ.C0727;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;

/* renamed from: android.support.v7.widget.ʻʻ  reason: contains not printable characters */
/* compiled from: AppCompatTextViewAutoSizeHelper */
class C0577 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final RectF f2244 = new RectF();

    /* renamed from: ʼ  reason: contains not printable characters */
    private static Hashtable<String, Method> f2245 = new Hashtable<>();

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f2246 = 0;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f2247 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    private float f2248 = -1.0f;

    /* renamed from: ˆ  reason: contains not printable characters */
    private float f2249 = -1.0f;

    /* renamed from: ˈ  reason: contains not printable characters */
    private float f2250 = -1.0f;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int[] f2251 = new int[0];

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f2252 = false;

    /* renamed from: ˋ  reason: contains not printable characters */
    private TextPaint f2253;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final TextView f2254;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final Context f2255;

    C0577(TextView textView) {
        this.f2254 = textView;
        this.f2255 = this.f2254.getContext();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3653(AttributeSet attributeSet, int i) {
        int resourceId;
        TypedArray obtainStyledAttributes = this.f2255.obtainStyledAttributes(attributeSet, C0727.C0737.AppCompatTextView, i, 0);
        if (obtainStyledAttributes.hasValue(C0727.C0737.AppCompatTextView_autoSizeTextType)) {
            this.f2246 = obtainStyledAttributes.getInt(C0727.C0737.AppCompatTextView_autoSizeTextType, 0);
        }
        float dimension = obtainStyledAttributes.hasValue(C0727.C0737.AppCompatTextView_autoSizeStepGranularity) ? obtainStyledAttributes.getDimension(C0727.C0737.AppCompatTextView_autoSizeStepGranularity, -1.0f) : -1.0f;
        float dimension2 = obtainStyledAttributes.hasValue(C0727.C0737.AppCompatTextView_autoSizeMinTextSize) ? obtainStyledAttributes.getDimension(C0727.C0737.AppCompatTextView_autoSizeMinTextSize, -1.0f) : -1.0f;
        float dimension3 = obtainStyledAttributes.hasValue(C0727.C0737.AppCompatTextView_autoSizeMaxTextSize) ? obtainStyledAttributes.getDimension(C0727.C0737.AppCompatTextView_autoSizeMaxTextSize, -1.0f) : -1.0f;
        if (obtainStyledAttributes.hasValue(C0727.C0737.AppCompatTextView_autoSizePresetSizes) && (resourceId = obtainStyledAttributes.getResourceId(C0727.C0737.AppCompatTextView_autoSizePresetSizes, 0)) > 0) {
            TypedArray obtainTypedArray = obtainStyledAttributes.getResources().obtainTypedArray(resourceId);
            m3642(obtainTypedArray);
            obtainTypedArray.recycle();
        }
        obtainStyledAttributes.recycle();
        if (!m3648()) {
            this.f2246 = 0;
        } else if (this.f2246 == 1) {
            if (!this.f2252) {
                DisplayMetrics displayMetrics = this.f2255.getResources().getDisplayMetrics();
                if (dimension2 == -1.0f) {
                    dimension2 = TypedValue.applyDimension(2, 12.0f, displayMetrics);
                }
                if (dimension3 == -1.0f) {
                    dimension3 = TypedValue.applyDimension(2, 112.0f, displayMetrics);
                }
                if (dimension == -1.0f) {
                    dimension = 1.0f;
                }
                m3641(dimension2, dimension3, dimension);
            }
            m3646();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3650(int i) {
        if (!m3648()) {
            return;
        }
        if (i == 0) {
            m3647();
        } else if (i == 1) {
            DisplayMetrics displayMetrics = this.f2255.getResources().getDisplayMetrics();
            m3641(TypedValue.applyDimension(2, 12.0f, displayMetrics), TypedValue.applyDimension(2, 112.0f, displayMetrics), 1.0f);
            if (m3646()) {
                m3659();
            }
        } else {
            throw new IllegalArgumentException("Unknown auto-size text type: " + i);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3652(int i, int i2, int i3, int i4) {
        if (m3648()) {
            DisplayMetrics displayMetrics = this.f2255.getResources().getDisplayMetrics();
            m3641(TypedValue.applyDimension(i4, (float) i, displayMetrics), TypedValue.applyDimension(i4, (float) i2, displayMetrics), TypedValue.applyDimension(i4, (float) i3, displayMetrics));
            if (m3646()) {
                m3659();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3654(int[] iArr, int i) {
        if (m3648()) {
            int length = iArr.length;
            if (length > 0) {
                int[] iArr2 = new int[length];
                if (i == 0) {
                    iArr2 = Arrays.copyOf(iArr, length);
                } else {
                    DisplayMetrics displayMetrics = this.f2255.getResources().getDisplayMetrics();
                    for (int i2 = 0; i2 < length; i2++) {
                        iArr2[i2] = Math.round(TypedValue.applyDimension(i, (float) iArr[i2], displayMetrics));
                    }
                }
                this.f2251 = m3644(iArr2);
                if (!m3645()) {
                    throw new IllegalArgumentException("None of the preset sizes is valid: " + Arrays.toString(iArr));
                }
            } else {
                this.f2252 = false;
            }
            if (m3646()) {
                m3659();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m3649() {
        return this.f2246;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m3655() {
        return Math.round(this.f2248);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public int m3656() {
        return Math.round(this.f2249);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public int m3657() {
        return Math.round(this.f2250);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public int[] m3658() {
        return this.f2251;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3642(TypedArray typedArray) {
        int length = typedArray.length();
        int[] iArr = new int[length];
        if (length > 0) {
            for (int i = 0; i < length; i++) {
                iArr[i] = typedArray.getDimensionPixelSize(i, -1);
            }
            this.f2251 = m3644(iArr);
            m3645();
        }
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean m3645() {
        int length = this.f2251.length;
        this.f2252 = length > 0;
        if (this.f2252) {
            this.f2246 = 1;
            int[] iArr = this.f2251;
            this.f2249 = (float) iArr[0];
            this.f2250 = (float) iArr[length - 1];
            this.f2248 = -1.0f;
        }
        return this.f2252;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int[] m3644(int[] iArr) {
        if (r0 == 0) {
            return iArr;
        }
        Arrays.sort(iArr);
        ArrayList arrayList = new ArrayList();
        for (int i : iArr) {
            if (i > 0 && Collections.binarySearch(arrayList, Integer.valueOf(i)) < 0) {
                arrayList.add(Integer.valueOf(i));
            }
        }
        if (r0 == arrayList.size()) {
            return iArr;
        }
        int size = arrayList.size();
        int[] iArr2 = new int[size];
        for (int i2 = 0; i2 < size; i2++) {
            iArr2[i2] = ((Integer) arrayList.get(i2)).intValue();
        }
        return iArr2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3641(float f, float f2, float f3) {
        if (f <= 0.0f) {
            throw new IllegalArgumentException("Minimum auto-size text size (" + f + "px) is less or equal to (0px)");
        } else if (f2 <= f) {
            throw new IllegalArgumentException("Maximum auto-size text size (" + f2 + "px) is less or equal to minimum auto-size " + "text size (" + f + "px)");
        } else if (f3 > 0.0f) {
            this.f2246 = 1;
            this.f2249 = f;
            this.f2250 = f2;
            this.f2248 = f3;
            this.f2252 = false;
        } else {
            throw new IllegalArgumentException("The auto-size step granularity (" + f3 + "px) is less or equal to (0px)");
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean m3646() {
        if (!m3648() || this.f2246 != 1) {
            this.f2247 = false;
        } else {
            if (!this.f2252 || this.f2251.length == 0) {
                float round = (float) Math.round(this.f2249);
                int i = 1;
                while (Math.round(this.f2248 + round) <= Math.round(this.f2250)) {
                    i++;
                    round += this.f2248;
                }
                int[] iArr = new int[i];
                float f = this.f2249;
                for (int i2 = 0; i2 < i; i2++) {
                    iArr[i2] = Math.round(f);
                    f += this.f2248;
                }
                this.f2251 = m3644(iArr);
            }
            this.f2247 = true;
        }
        return this.f2247;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ʻʻ.ʻ(java.lang.Object, java.lang.String, java.lang.Object):T
     arg types: [android.widget.TextView, java.lang.String, boolean]
     candidates:
      android.support.v7.widget.ʻʻ.ʻ(java.lang.CharSequence, android.text.Layout$Alignment, int):android.text.StaticLayout
      android.support.v7.widget.ʻʻ.ʻ(float, float, float):void
      android.support.v7.widget.ʻʻ.ʻ(java.lang.Object, java.lang.String, java.lang.Object):T */
    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m3659() {
        int i;
        if (m3660()) {
            if (this.f2247) {
                if (this.f2254.getMeasuredHeight() > 0 && this.f2254.getMeasuredWidth() > 0) {
                    if (((Boolean) m3638((Object) this.f2254, "getHorizontallyScrolling", (Object) false)).booleanValue()) {
                        i = 1048576;
                    } else {
                        i = (this.f2254.getMeasuredWidth() - this.f2254.getTotalPaddingLeft()) - this.f2254.getTotalPaddingRight();
                    }
                    int height = (this.f2254.getHeight() - this.f2254.getCompoundPaddingBottom()) - this.f2254.getCompoundPaddingTop();
                    if (i > 0 && height > 0) {
                        synchronized (f2244) {
                            f2244.setEmpty();
                            f2244.right = (float) i;
                            f2244.bottom = (float) height;
                            float r0 = (float) m3635(f2244);
                            if (r0 != this.f2254.getTextSize()) {
                                m3651(0, r0);
                            }
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
            this.f2247 = true;
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private void m3647() {
        this.f2246 = 0;
        this.f2249 = -1.0f;
        this.f2250 = -1.0f;
        this.f2248 = -1.0f;
        this.f2251 = new int[0];
        this.f2247 = false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3651(int i, float f) {
        Resources resources;
        Context context = this.f2255;
        if (context == null) {
            resources = Resources.getSystem();
        } else {
            resources = context.getResources();
        }
        m3640(TypedValue.applyDimension(i, f, resources.getDisplayMetrics()));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3640(float f) {
        if (f != this.f2254.getPaint().getTextSize()) {
            this.f2254.getPaint().setTextSize(f);
            boolean isInLayout = Build.VERSION.SDK_INT >= 18 ? this.f2254.isInLayout() : false;
            if (this.f2254.getLayout() != null) {
                this.f2247 = false;
                try {
                    Method r0 = m3639("nullLayouts");
                    if (r0 != null) {
                        r0.invoke(this.f2254, new Object[0]);
                    }
                } catch (Exception e) {
                    Log.w("ACTVAutoSizeHelper", "Failed to invoke TextView#nullLayouts() method", e);
                }
                if (!isInLayout) {
                    this.f2254.requestLayout();
                } else {
                    this.f2254.forceLayout();
                }
                this.f2254.invalidate();
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m3635(RectF rectF) {
        int length = this.f2251.length;
        if (length != 0) {
            int i = length - 1;
            int i2 = 1;
            int i3 = 0;
            while (i2 <= i) {
                int i4 = (i2 + i) / 2;
                if (m3643(this.f2251[i4], rectF)) {
                    int i5 = i4 + 1;
                    i3 = i2;
                    i2 = i5;
                } else {
                    i3 = i4 - 1;
                    i = i3;
                }
            }
            return this.f2251[i3];
        }
        throw new IllegalStateException("No available text sizes to choose from.");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m3643(int i, RectF rectF) {
        StaticLayout staticLayout;
        CharSequence text = this.f2254.getText();
        int maxLines = Build.VERSION.SDK_INT >= 16 ? this.f2254.getMaxLines() : -1;
        TextPaint textPaint = this.f2253;
        if (textPaint == null) {
            this.f2253 = new TextPaint();
        } else {
            textPaint.reset();
        }
        this.f2253.set(this.f2254.getPaint());
        this.f2253.setTextSize((float) i);
        Layout.Alignment alignment = (Layout.Alignment) m3638(this.f2254, "getLayoutAlignment", Layout.Alignment.ALIGN_NORMAL);
        if (Build.VERSION.SDK_INT >= 23) {
            staticLayout = m3637(text, alignment, Math.round(rectF.right), maxLines);
        } else {
            staticLayout = m3636(text, alignment, Math.round(rectF.right));
        }
        return (maxLines == -1 || (staticLayout.getLineCount() <= maxLines && staticLayout.getLineEnd(staticLayout.getLineCount() - 1) == text.length())) && ((float) staticLayout.getHeight()) <= rectF.bottom;
    }

    @TargetApi(23)
    /* renamed from: ʻ  reason: contains not printable characters */
    private StaticLayout m3637(CharSequence charSequence, Layout.Alignment alignment, int i, int i2) {
        TextDirectionHeuristic textDirectionHeuristic = (TextDirectionHeuristic) m3638(this.f2254, "getTextDirectionHeuristic", TextDirectionHeuristics.FIRSTSTRONG_LTR);
        StaticLayout.Builder hyphenationFrequency = StaticLayout.Builder.obtain(charSequence, 0, charSequence.length(), this.f2253, i).setAlignment(alignment).setLineSpacing(this.f2254.getLineSpacingExtra(), this.f2254.getLineSpacingMultiplier()).setIncludePad(this.f2254.getIncludeFontPadding()).setBreakStrategy(this.f2254.getBreakStrategy()).setHyphenationFrequency(this.f2254.getHyphenationFrequency());
        if (i2 == -1) {
            i2 = Integer.MAX_VALUE;
        }
        return hyphenationFrequency.setMaxLines(i2).setTextDirection(textDirectionHeuristic).build();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ʻʻ.ʻ(java.lang.Object, java.lang.String, java.lang.Object):T
     arg types: [android.widget.TextView, java.lang.String, boolean]
     candidates:
      android.support.v7.widget.ʻʻ.ʻ(java.lang.CharSequence, android.text.Layout$Alignment, int):android.text.StaticLayout
      android.support.v7.widget.ʻʻ.ʻ(float, float, float):void
      android.support.v7.widget.ʻʻ.ʻ(java.lang.Object, java.lang.String, java.lang.Object):T */
    @TargetApi(14)
    /* renamed from: ʻ  reason: contains not printable characters */
    private StaticLayout m3636(CharSequence charSequence, Layout.Alignment alignment, int i) {
        boolean z;
        float f;
        float f2;
        if (Build.VERSION.SDK_INT >= 16) {
            f2 = this.f2254.getLineSpacingMultiplier();
            f = this.f2254.getLineSpacingExtra();
            z = this.f2254.getIncludeFontPadding();
        } else {
            f2 = ((Float) m3638(this.f2254, "getLineSpacingMultiplier", Float.valueOf(1.0f))).floatValue();
            f = ((Float) m3638(this.f2254, "getLineSpacingExtra", Float.valueOf(0.0f))).floatValue();
            z = ((Boolean) m3638((Object) this.f2254, "getIncludeFontPadding", (Object) true)).booleanValue();
        }
        return new StaticLayout(charSequence, this.f2253, i, alignment, f2, f, z);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private <T> T m3638(Object obj, String str, T t) {
        try {
            return m3639(str).invoke(obj, new Object[0]);
        } catch (Exception e) {
            Log.w("ACTVAutoSizeHelper", "Failed to invoke TextView#" + str + "() method", e);
            return t;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private Method m3639(String str) {
        try {
            Method method = f2245.get(str);
            if (method == null && (method = TextView.class.getDeclaredMethod(str, new Class[0])) != null) {
                method.setAccessible(true);
                f2245.put(str, method);
            }
            return method;
        } catch (Exception e) {
            Log.w("ACTVAutoSizeHelper", "Failed to retrieve TextView#" + str + "() method", e);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m3660() {
        return m3648() && this.f2246 != 0;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean m3648() {
        return !(this.f2254 instanceof C0667);
    }
}
