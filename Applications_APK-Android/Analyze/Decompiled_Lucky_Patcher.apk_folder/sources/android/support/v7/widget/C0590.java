package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

/* renamed from: android.support.v7.widget.ʻˋ  reason: contains not printable characters */
/* compiled from: TintInfo */
class C0590 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public ColorStateList f2319;

    /* renamed from: ʼ  reason: contains not printable characters */
    public PorterDuff.Mode f2320;

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean f2321;

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean f2322;

    C0590() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3737() {
        this.f2319 = null;
        this.f2322 = false;
        this.f2320 = null;
        this.f2321 = false;
    }
}
