package android.support.v7.widget;

import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.Build;
import android.support.v4.ʼ.ʻ.C0294;
import android.support.v7.ʽ.ʻ.C0742;

/* renamed from: android.support.v7.widget.ــ  reason: contains not printable characters */
/* compiled from: DrawableUtils */
public class C0670 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final Rect f2682 = new Rect();

    /* renamed from: ʼ  reason: contains not printable characters */
    private static Class<?> f2683;

    static {
        if (Build.VERSION.SDK_INT >= 18) {
            try {
                f2683 = Class.forName("android.graphics.Insets");
            } catch (ClassNotFoundException unused) {
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m4231(Drawable drawable) {
        if (Build.VERSION.SDK_INT == 21 && "android.graphics.drawable.VectorDrawable".equals(drawable.getClass().getName())) {
            m4233(drawable);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static boolean m4232(Drawable drawable) {
        if (Build.VERSION.SDK_INT < 15 && (drawable instanceof InsetDrawable)) {
            return false;
        }
        if (Build.VERSION.SDK_INT < 15 && (drawable instanceof GradientDrawable)) {
            return false;
        }
        if (Build.VERSION.SDK_INT < 17 && (drawable instanceof LayerDrawable)) {
            return false;
        }
        if (drawable instanceof DrawableContainer) {
            Drawable.ConstantState constantState = drawable.getConstantState();
            if (!(constantState instanceof DrawableContainer.DrawableContainerState)) {
                return true;
            }
            for (Drawable r3 : ((DrawableContainer.DrawableContainerState) constantState).getChildren()) {
                if (!m4232(r3)) {
                    return false;
                }
            }
            return true;
        } else if (drawable instanceof C0294) {
            return m4232(((C0294) drawable).m1750());
        } else {
            if (drawable instanceof C0742) {
                return m4232(((C0742) drawable).m4891());
            }
            if (drawable instanceof ScaleDrawable) {
                return m4232(((ScaleDrawable) drawable).getDrawable());
            }
            return true;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static void m4233(Drawable drawable) {
        int[] state = drawable.getState();
        if (state == null || state.length == 0) {
            drawable.setState(C0587.f2309);
        } else {
            drawable.setState(C0587.f2312);
        }
        drawable.setState(state);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static PorterDuff.Mode m4230(int i, PorterDuff.Mode mode) {
        if (i == 3) {
            return PorterDuff.Mode.SRC_OVER;
        }
        if (i == 5) {
            return PorterDuff.Mode.SRC_IN;
        }
        if (i == 9) {
            return PorterDuff.Mode.SRC_ATOP;
        }
        switch (i) {
            case 14:
                return PorterDuff.Mode.MULTIPLY;
            case 15:
                return PorterDuff.Mode.SCREEN;
            case 16:
                return Build.VERSION.SDK_INT >= 11 ? PorterDuff.Mode.valueOf("ADD") : mode;
            default:
                return mode;
        }
    }
}
