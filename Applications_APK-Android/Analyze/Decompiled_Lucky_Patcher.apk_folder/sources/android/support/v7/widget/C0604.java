package android.support.v7.widget;

import android.support.v4.ˈ.C0331;
import android.support.v4.ˈ.C0336;
import android.support.v4.ˈ.C0348;
import android.support.v7.widget.C0690;

/* renamed from: android.support.v7.widget.ʻᵎ  reason: contains not printable characters */
/* compiled from: ViewInfoStore */
class C0604 {

    /* renamed from: ʻ  reason: contains not printable characters */
    final C0331<C0690.C0720, C0605> f2376 = new C0331<>();

    /* renamed from: ʼ  reason: contains not printable characters */
    final C0336<C0690.C0720> f2377 = new C0336<>();

    /* renamed from: android.support.v7.widget.ʻᵎ$ʼ  reason: contains not printable characters */
    /* compiled from: ViewInfoStore */
    interface C0606 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m3853(C0690.C0720 r1);

        /* renamed from: ʻ  reason: contains not printable characters */
        void m3854(C0690.C0720 r1, C0690.C0695.C0698 r2, C0690.C0695.C0698 r3);

        /* renamed from: ʼ  reason: contains not printable characters */
        void m3855(C0690.C0720 r1, C0690.C0695.C0698 r2, C0690.C0695.C0698 r3);

        /* renamed from: ʽ  reason: contains not printable characters */
        void m3856(C0690.C0720 r1, C0690.C0695.C0698 r2, C0690.C0695.C0698 r3);
    }

    C0604() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3835() {
        this.f2376.clear();
        this.f2377.m1930();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3838(C0690.C0720 r3, C0690.C0695.C0698 r4) {
        C0605 r0 = this.f2376.get(r3);
        if (r0 == null) {
            r0 = C0605.m3850();
            this.f2376.put(r3, r0);
        }
        r0.f2380 = r4;
        r0.f2379 |= 4;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3839(C0690.C0720 r2) {
        C0605 r22 = this.f2376.get(r2);
        if (r22 == null || (r22.f2379 & 1) == 0) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public C0690.C0695.C0698 m3840(C0690.C0720 r2) {
        return m3833(r2, 4);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public C0690.C0695.C0698 m3843(C0690.C0720 r2) {
        return m3833(r2, 8);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private C0690.C0695.C0698 m3833(C0690.C0720 r4, int i) {
        C0605 r1;
        C0690.C0695.C0698 r5;
        int r42 = this.f2376.m1974(r4);
        if (r42 < 0 || (r1 = this.f2376.m1980(r42)) == null || (r1.f2379 & i) == 0) {
            return null;
        }
        r1.f2379 &= i ^ -1;
        if (i == 4) {
            r5 = r1.f2380;
        } else if (i == 8) {
            r5 = r1.f2381;
        } else {
            throw new IllegalArgumentException("Must provide flag PRE or POST");
        }
        if ((r1.f2379 & 12) == 0) {
            this.f2376.m1981(r42);
            C0605.m3851(r1);
        }
        return r5;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3836(long j, C0690.C0720 r4) {
        this.f2377.m1928(j, r4);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3842(C0690.C0720 r3, C0690.C0695.C0698 r4) {
        C0605 r0 = this.f2376.get(r3);
        if (r0 == null) {
            r0 = C0605.m3850();
            this.f2376.put(r3, r0);
        }
        r0.f2379 |= 2;
        r0.f2380 = r4;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m3845(C0690.C0720 r2) {
        C0605 r22 = this.f2376.get(r2);
        return (r22 == null || (r22.f2379 & 4) == 0) ? false : true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0690.C0720 m3834(long j) {
        return this.f2377.m1922(j);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3844(C0690.C0720 r3, C0690.C0695.C0698 r4) {
        C0605 r0 = this.f2376.get(r3);
        if (r0 == null) {
            r0 = C0605.m3850();
            this.f2376.put(r3, r0);
        }
        r0.f2381 = r4;
        r0.f2379 |= 8;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m3846(C0690.C0720 r3) {
        C0605 r0 = this.f2376.get(r3);
        if (r0 == null) {
            r0 = C0605.m3850();
            this.f2376.put(r3, r0);
        }
        r0.f2379 |= 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m3847(C0690.C0720 r2) {
        C0605 r22 = this.f2376.get(r2);
        if (r22 != null) {
            r22.f2379 &= -2;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3837(C0606 r6) {
        for (int size = this.f2376.size() - 1; size >= 0; size--) {
            C0690.C0720 r1 = this.f2376.m1979(size);
            C0605 r2 = this.f2376.m1981(size);
            if ((r2.f2379 & 3) == 3) {
                r6.m3853(r1);
            } else if ((r2.f2379 & 1) != 0) {
                if (r2.f2380 == null) {
                    r6.m3853(r1);
                } else {
                    r6.m3854(r1, r2.f2380, r2.f2381);
                }
            } else if ((r2.f2379 & 14) == 14) {
                r6.m3855(r1, r2.f2380, r2.f2381);
            } else if ((r2.f2379 & 12) == 12) {
                r6.m3856(r1, r2.f2380, r2.f2381);
            } else if ((r2.f2379 & 4) != 0) {
                r6.m3854(r1, r2.f2380, null);
            } else if ((r2.f2379 & 8) != 0) {
                r6.m3855(r1, r2.f2380, r2.f2381);
            } else {
                int i = r2.f2379;
            }
            C0605.m3851(r2);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m3848(C0690.C0720 r3) {
        int r0 = this.f2377.m1925() - 1;
        while (true) {
            if (r0 < 0) {
                break;
            } else if (r3 == this.f2377.m1929(r0)) {
                this.f2377.m1924(r0);
                break;
            } else {
                r0--;
            }
        }
        C0605 remove = this.f2376.remove(r3);
        if (remove != null) {
            C0605.m3851(remove);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3841() {
        C0605.m3852();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public void m3849(C0690.C0720 r1) {
        m3847(r1);
    }

    /* renamed from: android.support.v7.widget.ʻᵎ$ʻ  reason: contains not printable characters */
    /* compiled from: ViewInfoStore */
    static class C0605 {

        /* renamed from: ʾ  reason: contains not printable characters */
        static C0348.C0349<C0605> f2378 = new C0348.C0350(20);

        /* renamed from: ʻ  reason: contains not printable characters */
        int f2379;

        /* renamed from: ʼ  reason: contains not printable characters */
        C0690.C0695.C0698 f2380;

        /* renamed from: ʽ  reason: contains not printable characters */
        C0690.C0695.C0698 f2381;

        private C0605() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        static C0605 m3850() {
            C0605 r0 = f2378.m1962();
            return r0 == null ? new C0605() : r0;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        static void m3851(C0605 r1) {
            r1.f2379 = 0;
            r1.f2380 = null;
            r1.f2381 = null;
            f2378.m1963(r1);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        static void m3852() {
            do {
            } while (f2378.m1962() != null);
        }
    }
}
