package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.ˉ.ʻ.C0360;
import android.support.v7.widget.C0690;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import java.util.Arrays;

public class GridLayoutManager extends LinearLayoutManager {

    /* renamed from: ʻ  reason: contains not printable characters */
    boolean f2014 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    int f2015 = -1;

    /* renamed from: ʽ  reason: contains not printable characters */
    int[] f2016;

    /* renamed from: ʾ  reason: contains not printable characters */
    View[] f2017;

    /* renamed from: ʿ  reason: contains not printable characters */
    final SparseIntArray f2018 = new SparseIntArray();

    /* renamed from: ˆ  reason: contains not printable characters */
    final SparseIntArray f2019 = new SparseIntArray();

    /* renamed from: ˈ  reason: contains not printable characters */
    C0553 f2020 = new C0551();

    /* renamed from: ˉ  reason: contains not printable characters */
    final Rect f2021 = new Rect();

    public GridLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        m3262(m4536(context, attributeSet, i, i2).f2851);
    }

    public GridLayoutManager(Context context, int i) {
        super(context);
        m3262(i);
    }

    public GridLayoutManager(Context context, int i, int i2, boolean z) {
        super(context, i2, z);
        m3262(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3273(boolean z) {
        if (!z) {
            super.m3337(false);
            return;
        }
        throw new UnsupportedOperationException("GridLayoutManager does not support stack from end. Consider using reverse layout");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m3256(C0690.C0711 r3, C0690.C0717 r4) {
        if (this.f2034 == 0) {
            return this.f2015;
        }
        if (r4.m4775() < 1) {
            return 0;
        }
        return m3240(r3, r4, r4.m4775() - 1) + 1;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m3276(C0690.C0711 r3, C0690.C0717 r4) {
        if (this.f2034 == 1) {
            return this.f2015;
        }
        if (r4.m4775() < 1) {
            return 0;
        }
        return m3240(r3, r4, r4.m4775() - 1) + 1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3266(C0690.C0711 r8, C0690.C0717 r9, View view, C0360 r11) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof C0552)) {
            super.m4575(view, r11);
            return;
        }
        C0552 r0 = (C0552) layoutParams;
        int r82 = m3240(r8, r9, r0.m4687());
        if (this.f2034 == 0) {
            r11.m2020(C0360.C0372.m2094(r0.m3282(), r0.m3283(), r82, 1, this.f2015 > 1 && r0.m3283() == this.f2015, false));
        } else {
            r11.m2020(C0360.C0372.m2094(r82, 1, r0.m3282(), r0.m3283(), this.f2015 > 1 && r0.m3283() == this.f2015, false));
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3279(C0690.C0711 r2, C0690.C0717 r3) {
        if (r3.m4771()) {
            m3249();
        }
        super.m3348(r2, r3);
        m3250();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3267(C0690.C0717 r1) {
        super.m3332(r1);
        this.f2014 = false;
    }

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private void m3250() {
        this.f2018.clear();
        this.f2019.clear();
    }

    /* renamed from: ˈˈ  reason: contains not printable characters */
    private void m3249() {
        int r0 = m4667();
        for (int i = 0; i < r0; i++) {
            C0552 r2 = (C0552) m4645(i).getLayoutParams();
            int r3 = r2.m4687();
            this.f2018.put(r3, r2.m3283());
            this.f2019.put(r3, r2.m3282());
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3270(C0690 r1, int i, int i2) {
        this.f2020.m3286();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3269(C0690 r1) {
        this.f2020.m3286();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3277(C0690 r1, int i, int i2) {
        this.f2020.m3286();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3272(C0690 r1, int i, int i2, Object obj) {
        this.f2020.m3286();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3271(C0690 r1, int i, int i2, int i3) {
        this.f2020.m3286();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0690.C0704 m3257() {
        if (this.f2034 == 0) {
            return new C0552(-2, -1);
        }
        return new C0552(-1, -2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0690.C0704 m3258(Context context, AttributeSet attributeSet) {
        return new C0552(context, attributeSet);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0690.C0704 m3259(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new C0552((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new C0552(layoutParams);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3274(C0690.C0704 r1) {
        return r1 instanceof C0552;
    }

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private void m3252() {
        int i;
        int i2;
        if (m3355() == 1) {
            i2 = m4670() - m4664();
            i = m4672();
        } else {
            i2 = m4671() - m4593();
            i = m4662();
        }
        m3253(i2 - i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3263(Rect rect, int i, int i2) {
        int i3;
        int i4;
        if (this.f2016 == null) {
            super.m4554(rect, i, i2);
        }
        int r0 = m4672() + m4664();
        int r1 = m4662() + m4593();
        if (this.f2034 == 1) {
            i4 = m4534(i2, rect.height() + r1, m4633());
            int[] iArr = this.f2016;
            i3 = m4534(i, iArr[iArr.length - 1] + r0, m4605());
        } else {
            i3 = m4534(i, rect.width() + r0, m4605());
            int[] iArr2 = this.f2016;
            i4 = m4534(i2, iArr2[iArr2.length - 1] + r1, m4633());
        }
        m4637(i3, i4);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private void m3253(int i) {
        this.f2016 = m3245(this.f2016, this.f2015, i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static int[] m3245(int[] iArr, int i, int i2) {
        int i3;
        if (!(iArr != null && iArr.length == i + 1 && iArr[iArr.length - 1] == i2)) {
            iArr = new int[(i + 1)];
        }
        int i4 = 0;
        iArr[0] = 0;
        int i5 = i2 / i;
        int i6 = i2 % i;
        int i7 = 0;
        for (int i8 = 1; i8 <= i; i8++) {
            i4 += i6;
            if (i4 <= 0 || i - i4 >= i6) {
                i3 = i5;
            } else {
                i3 = i5 + 1;
                i4 -= i;
            }
            i7 += i3;
            iArr[i8] = i7;
        }
        return iArr;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m3254(int i, int i2) {
        if (this.f2034 != 1 || !m3358()) {
            int[] iArr = this.f2016;
            return iArr[i2 + i] - iArr[i];
        }
        int[] iArr2 = this.f2016;
        int i3 = this.f2015;
        return iArr2[i3 - i] - iArr2[(i3 - i) - i2];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3264(C0690.C0711 r2, C0690.C0717 r3, LinearLayoutManager.C0554 r4, int i) {
        super.m3330(r2, r3, r4, i);
        m3252();
        if (r3.m4775() > 0 && !r3.m4771()) {
            m3247(r2, r3, r4, i);
        }
        m3251();
    }

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private void m3251() {
        View[] viewArr = this.f2017;
        if (viewArr == null || viewArr.length != this.f2015) {
            this.f2017 = new View[this.f2015];
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m3255(int i, C0690.C0711 r2, C0690.C0717 r3) {
        m3252();
        m3251();
        return super.m3321(i, r2, r3);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m3275(int i, C0690.C0711 r2, C0690.C0717 r3) {
        m3252();
        m3251();
        return super.m3338(i, r2, r3);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m3247(C0690.C0711 r5, C0690.C0717 r6, LinearLayoutManager.C0554 r7, int i) {
        boolean z = i == 1;
        int r1 = m3246(r5, r6, r7.f2041);
        if (z) {
            while (r1 > 0 && r7.f2041 > 0) {
                r7.f2041--;
                r1 = m3246(r5, r6, r7.f2041);
            }
            return;
        }
        int r8 = r6.m4775() - 1;
        int i2 = r7.f2041;
        while (i2 < r8) {
            int i3 = i2 + 1;
            int r3 = m3246(r5, r6, i3);
            if (r3 <= r1) {
                break;
            }
            i2 = i3;
            r1 = r3;
        }
        r7.f2041 = i2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public View m3260(C0690.C0711 r8, C0690.C0717 r9, int i, int i2, int i3) {
        m3360();
        int r0 = this.f2035.m4294();
        int r1 = this.f2035.m4296();
        int i4 = i2 > i ? 1 : -1;
        View view = null;
        View view2 = null;
        while (i != i2) {
            View r5 = m4645(i);
            int r6 = m4620(r5);
            if (r6 >= 0 && r6 < i3 && m3246(r8, r9, r6) == 0) {
                if (((C0690.C0704) r5.getLayoutParams()).m4685()) {
                    if (view2 == null) {
                        view2 = r5;
                    }
                } else if (this.f2035.m4289(r5) < r1 && this.f2035.m4293(r5) >= r0) {
                    return r5;
                } else {
                    if (view == null) {
                        view = r5;
                    }
                }
            }
            i += i4;
        }
        return view != null ? view : view2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m3240(C0690.C0711 r1, C0690.C0717 r2, int i) {
        if (!r2.m4771()) {
            return this.f2020.m3289(i, this.f2015);
        }
        int r12 = r1.m4725(i);
        if (r12 != -1) {
            return this.f2020.m3289(r12, this.f2015);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. " + i);
        return 0;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int m3246(C0690.C0711 r2, C0690.C0717 r3, int i) {
        if (!r3.m4771()) {
            return this.f2020.m3288(i, this.f2015);
        }
        int i2 = this.f2019.get(i, -1);
        if (i2 != -1) {
            return i2;
        }
        int r22 = r2.m4725(i);
        if (r22 != -1) {
            return this.f2020.m3288(r22, this.f2015);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + i);
        return 0;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private int m3248(C0690.C0711 r2, C0690.C0717 r3, int i) {
        if (!r3.m4771()) {
            return this.f2020.m3284(i);
        }
        int i2 = this.f2018.get(i, -1);
        if (i2 != -1) {
            return i2;
        }
        int r22 = r2.m4725(i);
        if (r22 != -1) {
            return this.f2020.m3284(r22);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + i);
        return 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3268(C0690.C0717 r6, LinearLayoutManager.C0556 r7, C0690.C0701.C0702 r8) {
        int i = this.f2015;
        for (int i2 = 0; i2 < this.f2015 && r7.m3376(r6) && i > 0; i2++) {
            int i3 = r7.f2053;
            r8.m4683(i3, Math.max(0, r7.f2056));
            i -= this.f2020.m3284(i3);
            r7.f2053 += r7.f2054;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.GridLayoutManager.ʻ(android.view.View, int, boolean):void
     arg types: [android.view.View, int, int]
     candidates:
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int):int
      android.support.v7.widget.GridLayoutManager.ʻ(int[], int, int):int[]
      android.support.v7.widget.GridLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.GridLayoutManager.ʻ(android.graphics.Rect, int, int):void
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int, android.view.View):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.graphics.Rect, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.accessibility.AccessibilityEvent):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ˊ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, android.graphics.Rect):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.view.View):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, boolean):boolean
      android.support.v7.widget.GridLayoutManager.ʻ(android.view.View, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.GridLayoutManager.ʻ(int, int):int
     arg types: [int, int]
     candidates:
      android.support.v7.widget.GridLayoutManager.ʻ(float, int):void
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.GridLayoutManager.ʻ(android.content.Context, android.util.AttributeSet):android.support.v7.widget.ﹳﹳ$ˊ
      android.support.v7.widget.LinearLayoutManager.ʻ(boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.view.View):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ˉ, android.support.v7.widget.ﹳﹳ$ᵎ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.content.Context, android.util.AttributeSet):android.support.v7.widget.ﹳﹳ$ˊ
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ʻ, android.support.v7.widget.ﹳﹳ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.os.Bundle):boolean
      android.support.v7.widget.GridLayoutManager.ʻ(int, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int, int, boolean):int
     arg types: [int, int, int, int, int]
     candidates:
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int, boolean):void
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int, int):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int, int):android.view.View
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean, boolean):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.GridLayoutManager.ʻ(android.view.View, int, int, boolean):void
     arg types: [android.view.View, int, int, int]
     candidates:
      android.support.v7.widget.GridLayoutManager.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ, int):void
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.LinearLayoutManager$ʼ):void
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ, int, int, int):void
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ, int, int, java.lang.Object):void
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, android.support.v7.widget.ﹳﹳ$ᵔ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ᵔ, boolean):int
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.LinearLayoutManager$ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.ﹳﹳ$ˉ$ʼ
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int, java.lang.Object):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, android.view.View):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int, android.support.v7.widget.ﹳﹳ$ˊ):boolean
      android.support.v7.widget.GridLayoutManager.ʻ(android.view.View, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3265(C0690.C0711 r19, C0690.C0717 r20, LinearLayoutManager.C0556 r21, LinearLayoutManager.C0555 r22) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        boolean z;
        View r3;
        C0690.C0711 r1 = r19;
        C0690.C0717 r2 = r20;
        LinearLayoutManager.C0556 r7 = r21;
        LinearLayoutManager.C0555 r8 = r22;
        int r9 = this.f2035.m4304();
        boolean z2 = r9 != 1073741824;
        int i9 = m4667() > 0 ? this.f2016[this.f2015] : 0;
        if (z2) {
            m3252();
        }
        boolean z3 = r7.f2054 == 1;
        int i10 = this.f2015;
        if (!z3) {
            i10 = m3246(r1, r2, r7.f2053) + m3248(r1, r2, r7.f2053);
        }
        int i11 = 0;
        int i12 = 0;
        while (i12 < this.f2015 && r7.m3376(r2) && i10 > 0) {
            int i13 = r7.f2053;
            int r10 = m3248(r1, r2, i13);
            if (r10 <= this.f2015) {
                i10 -= r10;
                if (i10 < 0 || (r3 = r7.m3373(r1)) == null) {
                    break;
                }
                i11 += r10;
                this.f2017[i12] = r3;
                i12++;
            } else {
                throw new IllegalArgumentException("Item at position " + i13 + " requires " + r10 + " spans but GridLayoutManager has only " + this.f2015 + " spans.");
            }
        }
        if (i12 == 0) {
            r8.f2047 = true;
            return;
        }
        float f = 0.0f;
        int i14 = i12;
        m3242(r19, r20, i12, i11, z3);
        int i15 = 0;
        for (int i16 = 0; i16 < i14; i16++) {
            View view = this.f2017[i16];
            if (r7.f2060 != null) {
                z = false;
                if (z3) {
                    m4569(view);
                } else {
                    m4570(view, 0);
                }
            } else if (z3) {
                m4600(view);
                z = false;
            } else {
                z = false;
                m4601(view, 0);
            }
            m4602(view, this.f2021);
            m3244(view, r9, z);
            int r32 = this.f2035.m4299(view);
            if (r32 > i15) {
                i15 = r32;
            }
            float r23 = (((float) this.f2035.m4301(view)) * 1.0f) / ((float) ((C0552) view.getLayoutParams()).f2023);
            if (r23 > f) {
                f = r23;
            }
        }
        if (z2) {
            m3241(f, i9);
            i15 = 0;
            for (int i17 = 0; i17 < i14; i17++) {
                View view2 = this.f2017[i17];
                m3244(view2, 1073741824, true);
                int r24 = this.f2035.m4299(view2);
                if (r24 > i15) {
                    i15 = r24;
                }
            }
        }
        for (int i18 = 0; i18 < i14; i18++) {
            View view3 = this.f2017[i18];
            if (this.f2035.m4299(view3) != i15) {
                C0552 r33 = (C0552) view3.getLayoutParams();
                Rect rect = r33.f2855;
                int i19 = rect.top + rect.bottom + r33.topMargin + r33.bottomMargin;
                int i20 = rect.left + rect.right + r33.leftMargin + r33.rightMargin;
                int r4 = m3254(r33.f2022, r33.f2023);
                if (this.f2034 == 1) {
                    i8 = m4535(r4, 1073741824, i20, r33.width, false);
                    i7 = View.MeasureSpec.makeMeasureSpec(i15 - i19, 1073741824);
                } else {
                    int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i15 - i20, 1073741824);
                    i7 = m4535(r4, 1073741824, i19, r33.height, false);
                    i8 = makeMeasureSpec;
                }
                m3243(view3, i8, i7, true);
            }
        }
        int i21 = 0;
        r8.f2046 = i15;
        if (this.f2034 == 1) {
            if (r7.f2055 == -1) {
                int i22 = r7.f2051;
                i = i22;
                i2 = i22 - i15;
            } else {
                int i23 = r7.f2051;
                i2 = i23;
                i = i15 + i23;
            }
            i4 = 0;
            i3 = 0;
        } else if (r7.f2055 == -1) {
            int i24 = r7.f2051;
            i2 = 0;
            i = 0;
            int i25 = i24 - i15;
            i3 = i24;
            i4 = i25;
        } else {
            i4 = r7.f2051;
            i3 = i15 + i4;
            i2 = 0;
            i = 0;
        }
        while (i21 < i14) {
            View view4 = this.f2017[i21];
            C0552 r92 = (C0552) view4.getLayoutParams();
            if (this.f2034 != 1) {
                i2 = m4662() + this.f2016[r92.f2022];
                i = this.f2035.m4301(view4) + i2;
            } else if (m3358()) {
                int r0 = m4672() + this.f2016[this.f2015 - r92.f2022];
                i5 = r0;
                i6 = r0 - this.f2035.m4301(view4);
                int i26 = i2;
                int i27 = i;
                m4572(view4, i6, i26, i5, i27);
                if (!r92.m4685() || r92.m4686()) {
                    r8.f2048 = true;
                }
                r8.f2049 |= view4.hasFocusable();
                i21++;
                i4 = i6;
                i2 = i26;
                i3 = i5;
                i = i27;
            } else {
                i4 = m4672() + this.f2016[r92.f2022];
                i3 = this.f2035.m4301(view4) + i4;
            }
            i6 = i4;
            i5 = i3;
            int i262 = i2;
            int i272 = i;
            m4572(view4, i6, i262, i5, i272);
            if (!r92.m4685()) {
            }
            r8.f2048 = true;
            r8.f2049 |= view4.hasFocusable();
            i21++;
            i4 = i6;
            i2 = i262;
            i3 = i5;
            i = i272;
        }
        Arrays.fill(this.f2017, (Object) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.GridLayoutManager.ʻ(int, int):int
     arg types: [int, int]
     candidates:
      android.support.v7.widget.GridLayoutManager.ʻ(float, int):void
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.GridLayoutManager.ʻ(android.content.Context, android.util.AttributeSet):android.support.v7.widget.ﹳﹳ$ˊ
      android.support.v7.widget.LinearLayoutManager.ʻ(boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(int, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.LinearLayoutManager$ʽ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ):boolean
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.view.View):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ˉ, android.support.v7.widget.ﹳﹳ$ᵎ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.content.Context, android.util.AttributeSet):android.support.v7.widget.ﹳﹳ$ˊ
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ʻ, android.support.v7.widget.ﹳﹳ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, android.support.v7.widget.ﹳﹳ$ـ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.os.Bundle):boolean
      android.support.v7.widget.GridLayoutManager.ʻ(int, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int, int, boolean):int
     arg types: [int, int, int, int, int]
     candidates:
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int, boolean):void
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int, int):android.view.View
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int, int, int):android.view.View
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.View, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.graphics.Rect, boolean, boolean):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int, int, boolean):int */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3244(View view, int i, boolean z) {
        int i2;
        int i3;
        C0552 r0 = (C0552) view.getLayoutParams();
        Rect rect = r0.f2855;
        int i4 = rect.top + rect.bottom + r0.topMargin + r0.bottomMargin;
        int i5 = rect.left + rect.right + r0.leftMargin + r0.rightMargin;
        int r1 = m3254(r0.f2022, r0.f2023);
        if (this.f2034 == 1) {
            i2 = m4535(r1, i, i5, r0.width, false);
            i3 = m4535(this.f2035.m4300(), m4669(), i4, r0.height, true);
        } else {
            int r10 = m4535(r1, i, i4, r0.height, false);
            int r7 = m4535(this.f2035.m4300(), m4668(), i5, r0.width, true);
            i3 = r10;
            i2 = r7;
        }
        m3243(view, i2, i3, z);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3241(float f, int i) {
        m3253(Math.max(Math.round(f * ((float) this.f2015)), i));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3243(View view, int i, int i2, boolean z) {
        boolean z2;
        C0690.C0704 r0 = (C0690.C0704) view.getLayoutParams();
        if (z) {
            z2 = m4589(view, i, i2, r0);
        } else {
            z2 = m4604(view, i, i2, r0);
        }
        if (z2) {
            view.measure(i, i2);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3242(C0690.C0711 r4, C0690.C0717 r5, int i, int i2, boolean z) {
        int i3;
        int i4;
        int i5 = 0;
        int i6 = -1;
        if (z) {
            i6 = i;
            i4 = 0;
            i3 = 1;
        } else {
            i4 = i - 1;
            i3 = -1;
        }
        while (i4 != i6) {
            View view = this.f2017[i4];
            C0552 r2 = (C0552) view.getLayoutParams();
            r2.f2023 = m3248(r4, r5, m4620(view));
            r2.f2022 = i5;
            i5 += r2.f2023;
            i4 += i3;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3262(int i) {
        if (i != this.f2015) {
            this.f2014 = true;
            if (i >= 1) {
                this.f2015 = i;
                this.f2020.m3286();
                m4656();
                return;
            }
            throw new IllegalArgumentException("Span count should be at least 1. Provided " + i);
        }
    }

    /* renamed from: android.support.v7.widget.GridLayoutManager$ʽ  reason: contains not printable characters */
    public static abstract class C0553 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final SparseIntArray f2024 = new SparseIntArray();

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean f2025 = false;

        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract int m3284(int i);

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3286() {
            this.f2024.clear();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public int m3288(int i, int i2) {
            if (!this.f2025) {
                return m3285(i, i2);
            }
            int i3 = this.f2024.get(i, -1);
            if (i3 != -1) {
                return i3;
            }
            int r4 = m3285(i, i2);
            this.f2024.put(i, r4);
            return r4;
        }

        /* JADX WARNING: Removed duplicated region for block: B:12:0x002a  */
        /* renamed from: ʻ  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int m3285(int r6, int r7) {
            /*
                r5 = this;
                int r0 = r5.m3284(r6)
                r1 = 0
                if (r0 != r7) goto L_0x0008
                return r1
            L_0x0008:
                boolean r2 = r5.f2025
                if (r2 == 0) goto L_0x0026
                android.util.SparseIntArray r2 = r5.f2024
                int r2 = r2.size()
                if (r2 <= 0) goto L_0x0026
                int r2 = r5.m3287(r6)
                if (r2 < 0) goto L_0x0026
                android.util.SparseIntArray r3 = r5.f2024
                int r3 = r3.get(r2)
                int r4 = r5.m3284(r2)
                int r3 = r3 + r4
                goto L_0x0036
            L_0x0026:
                r2 = 0
                r3 = 0
            L_0x0028:
                if (r2 >= r6) goto L_0x0039
                int r4 = r5.m3284(r2)
                int r3 = r3 + r4
                if (r3 != r7) goto L_0x0033
                r3 = 0
                goto L_0x0036
            L_0x0033:
                if (r3 <= r7) goto L_0x0036
                r3 = r4
            L_0x0036:
                int r2 = r2 + 1
                goto L_0x0028
            L_0x0039:
                int r0 = r0 + r3
                if (r0 > r7) goto L_0x003d
                return r3
            L_0x003d:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.GridLayoutManager.C0553.m3285(int, int):int");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public int m3287(int i) {
            int size = this.f2024.size() - 1;
            int i2 = 0;
            while (i2 <= size) {
                int i3 = (i2 + size) >>> 1;
                if (this.f2024.keyAt(i3) < i) {
                    i2 = i3 + 1;
                } else {
                    size = i3 - 1;
                }
            }
            int i4 = i2 - 1;
            if (i4 < 0 || i4 >= this.f2024.size()) {
                return -1;
            }
            return this.f2024.keyAt(i4);
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public int m3289(int i, int i2) {
            int r0 = m3284(i);
            int i3 = 0;
            int i4 = 0;
            for (int i5 = 0; i5 < i; i5++) {
                int r5 = m3284(i5);
                i3 += r5;
                if (i3 == i2) {
                    i4++;
                    i3 = 0;
                } else if (i3 > i2) {
                    i4++;
                    i3 = r5;
                }
            }
            return i3 + r0 > i2 ? i4 + 1 : i4;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, boolean):boolean
     arg types: [android.view.View, int, int]
     candidates:
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, int):int
      android.support.v7.widget.GridLayoutManager.ʻ(android.view.View, int, boolean):void
      android.support.v7.widget.GridLayoutManager.ʻ(int[], int, int):int[]
      android.support.v7.widget.GridLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.GridLayoutManager.ʻ(android.graphics.Rect, int, int):void
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.GridLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int, int):void
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʻ):void
      android.support.v7.widget.LinearLayoutManager.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.LinearLayoutManager.ʻ(android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v7.widget.LinearLayoutManager$ʽ, android.support.v7.widget.ﹳﹳ$ˉ$ʻ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, int, int):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, int, android.view.View):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, boolean):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):int
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.graphics.Rect, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.support.v4.ˉ.ʻ.ʼ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ, android.view.accessibility.AccessibilityEvent):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, int):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.support.v7.widget.ﹳﹳ$ˊ):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, android.graphics.Rect):void
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.support.v7.widget.ﹳﹳ, android.view.View, android.view.View):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, int, android.os.Bundle):boolean
      android.support.v7.widget.ﹳﹳ.ˉ.ʻ(android.view.View, boolean, boolean):boolean */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00d7, code lost:
        if (r13 == (r2 > r8)) goto L_0x00b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00f7, code lost:
        if (r13 == r10) goto L_0x00bb;
     */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0105  */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View m3261(android.view.View r23, int r24, android.support.v7.widget.C0690.C0711 r25, android.support.v7.widget.C0690.C0717 r26) {
        /*
            r22 = this;
            r0 = r22
            r1 = r25
            r2 = r26
            android.view.View r3 = r22.m4628(r23)
            r4 = 0
            if (r3 != 0) goto L_0x000e
            return r4
        L_0x000e:
            android.view.ViewGroup$LayoutParams r5 = r3.getLayoutParams()
            android.support.v7.widget.GridLayoutManager$ʼ r5 = (android.support.v7.widget.GridLayoutManager.C0552) r5
            int r6 = r5.f2022
            int r7 = r5.f2022
            int r5 = r5.f2023
            int r7 = r7 + r5
            android.view.View r5 = super.m3326(r23, r24, r25, r26)
            if (r5 != 0) goto L_0x0022
            return r4
        L_0x0022:
            r5 = r24
            int r5 = r0.m3352(r5)
            r9 = 1
            if (r5 != r9) goto L_0x002d
            r5 = 1
            goto L_0x002e
        L_0x002d:
            r5 = 0
        L_0x002e:
            boolean r10 = r0.f2036
            if (r5 == r10) goto L_0x0034
            r5 = 1
            goto L_0x0035
        L_0x0034:
            r5 = 0
        L_0x0035:
            r10 = -1
            if (r5 == 0) goto L_0x0040
            int r5 = r22.m4667()
            int r5 = r5 - r9
            r11 = -1
            r12 = -1
            goto L_0x0047
        L_0x0040:
            int r5 = r22.m4667()
            r11 = r5
            r5 = 0
            r12 = 1
        L_0x0047:
            int r13 = r0.f2034
            if (r13 != r9) goto L_0x0053
            boolean r13 = r22.m3358()
            if (r13 == 0) goto L_0x0053
            r13 = 1
            goto L_0x0054
        L_0x0053:
            r13 = 0
        L_0x0054:
            int r14 = r0.m3240(r1, r2, r5)
            r10 = r4
            r8 = -1
            r15 = 0
            r16 = 0
            r17 = -1
        L_0x005f:
            if (r5 == r11) goto L_0x0145
            int r9 = r0.m3240(r1, r2, r5)
            android.view.View r1 = r0.m4645(r5)
            if (r1 != r3) goto L_0x006d
            goto L_0x0145
        L_0x006d:
            boolean r18 = r1.hasFocusable()
            if (r18 == 0) goto L_0x0087
            if (r9 == r14) goto L_0x0087
            if (r4 == 0) goto L_0x0079
            goto L_0x0145
        L_0x0079:
            r18 = r3
            r20 = r8
            r23 = r10
            r19 = r11
            r8 = r16
            r11 = r17
            goto L_0x0131
        L_0x0087:
            android.view.ViewGroup$LayoutParams r9 = r1.getLayoutParams()
            android.support.v7.widget.GridLayoutManager$ʼ r9 = (android.support.v7.widget.GridLayoutManager.C0552) r9
            int r2 = r9.f2022
            r18 = r3
            int r3 = r9.f2022
            r19 = r11
            int r11 = r9.f2023
            int r3 = r3 + r11
            boolean r11 = r1.hasFocusable()
            if (r11 == 0) goto L_0x00a3
            if (r2 != r6) goto L_0x00a3
            if (r3 != r7) goto L_0x00a3
            return r1
        L_0x00a3:
            boolean r11 = r1.hasFocusable()
            if (r11 == 0) goto L_0x00ab
            if (r4 == 0) goto L_0x00b3
        L_0x00ab:
            boolean r11 = r1.hasFocusable()
            if (r11 != 0) goto L_0x00bd
            if (r10 != 0) goto L_0x00bd
        L_0x00b3:
            r20 = r8
            r23 = r10
            r8 = r16
            r11 = r17
        L_0x00bb:
            r10 = 1
            goto L_0x0103
        L_0x00bd:
            int r11 = java.lang.Math.max(r2, r6)
            int r20 = java.lang.Math.min(r3, r7)
            int r11 = r20 - r11
            boolean r20 = r1.hasFocusable()
            if (r20 == 0) goto L_0x00da
            if (r11 <= r15) goto L_0x00d0
            goto L_0x00b3
        L_0x00d0:
            if (r11 != r15) goto L_0x00fa
            if (r2 <= r8) goto L_0x00d6
            r11 = 1
            goto L_0x00d7
        L_0x00d6:
            r11 = 0
        L_0x00d7:
            if (r13 != r11) goto L_0x00fa
            goto L_0x00b3
        L_0x00da:
            if (r4 != 0) goto L_0x00fa
            r20 = r8
            r23 = r10
            r8 = 0
            r10 = 1
            boolean r21 = r0.m4591(r1, r8, r10)
            if (r21 == 0) goto L_0x00fe
            r8 = r16
            if (r11 <= r8) goto L_0x00ef
            r11 = r17
            goto L_0x0103
        L_0x00ef:
            if (r11 != r8) goto L_0x0100
            r11 = r17
            if (r2 <= r11) goto L_0x00f6
            goto L_0x00f7
        L_0x00f6:
            r10 = 0
        L_0x00f7:
            if (r13 != r10) goto L_0x0102
            goto L_0x00bb
        L_0x00fa:
            r20 = r8
            r23 = r10
        L_0x00fe:
            r8 = r16
        L_0x0100:
            r11 = r17
        L_0x0102:
            r10 = 0
        L_0x0103:
            if (r10 == 0) goto L_0x0131
            boolean r10 = r1.hasFocusable()
            if (r10 == 0) goto L_0x0120
            int r4 = r9.f2022
            int r3 = java.lang.Math.min(r3, r7)
            int r2 = java.lang.Math.max(r2, r6)
            int r3 = r3 - r2
            r10 = r23
            r15 = r3
            r16 = r8
            r17 = r11
            r8 = r4
            r4 = r1
            goto L_0x0139
        L_0x0120:
            int r8 = r9.f2022
            int r3 = java.lang.Math.min(r3, r7)
            int r2 = java.lang.Math.max(r2, r6)
            int r3 = r3 - r2
            r10 = r1
            r16 = r3
            r17 = r8
            goto L_0x0137
        L_0x0131:
            r10 = r23
            r16 = r8
            r17 = r11
        L_0x0137:
            r8 = r20
        L_0x0139:
            int r5 = r5 + r12
            r1 = r25
            r2 = r26
            r3 = r18
            r11 = r19
            r9 = 1
            goto L_0x005f
        L_0x0145:
            r23 = r10
            if (r4 == 0) goto L_0x014a
            goto L_0x014c
        L_0x014a:
            r4 = r23
        L_0x014c:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.GridLayoutManager.m3261(android.view.View, int, android.support.v7.widget.ﹳﹳ$ـ, android.support.v7.widget.ﹳﹳ$ᵔ):android.view.View");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m3278() {
        return this.f2039 == null && !this.f2014;
    }

    /* renamed from: android.support.v7.widget.GridLayoutManager$ʻ  reason: contains not printable characters */
    public static final class C0551 extends C0553 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public int m3280(int i) {
            return 1;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m3281(int i, int i2) {
            return i % i2;
        }
    }

    /* renamed from: android.support.v7.widget.GridLayoutManager$ʼ  reason: contains not printable characters */
    public static class C0552 extends C0690.C0704 {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f2022 = -1;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f2023 = 0;

        public C0552(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public C0552(int i, int i2) {
            super(i, i2);
        }

        public C0552(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public C0552(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m3282() {
            return this.f2022;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m3283() {
            return this.f2023;
        }
    }
}
