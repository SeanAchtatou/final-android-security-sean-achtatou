package android.support.v7.widget;

import android.view.View;

/* renamed from: android.support.v7.widget.ʻᴵ  reason: contains not printable characters */
/* compiled from: ViewBoundsCheck */
class C0601 {

    /* renamed from: ʻ  reason: contains not printable characters */
    final C0603 f2369;

    /* renamed from: ʼ  reason: contains not printable characters */
    C0602 f2370 = new C0602();

    /* renamed from: android.support.v7.widget.ʻᴵ$ʼ  reason: contains not printable characters */
    /* compiled from: ViewBoundsCheck */
    interface C0603 {
        /* renamed from: ʻ  reason: contains not printable characters */
        int m3828();

        /* renamed from: ʻ  reason: contains not printable characters */
        int m3829(View view);

        /* renamed from: ʻ  reason: contains not printable characters */
        View m3830(int i);

        /* renamed from: ʼ  reason: contains not printable characters */
        int m3831();

        /* renamed from: ʼ  reason: contains not printable characters */
        int m3832(View view);
    }

    C0601(C0603 r1) {
        this.f2369 = r1;
    }

    /* renamed from: android.support.v7.widget.ʻᴵ$ʻ  reason: contains not printable characters */
    /* compiled from: ViewBoundsCheck */
    static class C0602 {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f2371 = 0;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f2372;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f2373;

        /* renamed from: ʾ  reason: contains not printable characters */
        int f2374;

        /* renamed from: ʿ  reason: contains not printable characters */
        int f2375;

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public int m3823(int i, int i2) {
            if (i > i2) {
                return 1;
            }
            return i == i2 ? 2 : 4;
        }

        C0602() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3826(int i, int i2, int i3, int i4) {
            this.f2372 = i;
            this.f2373 = i2;
            this.f2374 = i3;
            this.f2375 = i4;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3825(int i) {
            this.f2371 = i | this.f2371;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3824() {
            this.f2371 = 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m3827() {
            int i = this.f2371;
            if ((i & 7) != 0 && (i & (m3823(this.f2374, this.f2372) << 0)) == 0) {
                return false;
            }
            int i2 = this.f2371;
            if ((i2 & 112) != 0 && (i2 & (m3823(this.f2374, this.f2373) << 4)) == 0) {
                return false;
            }
            int i3 = this.f2371;
            if ((i3 & 1792) != 0 && (i3 & (m3823(this.f2375, this.f2372) << 8)) == 0) {
                return false;
            }
            int i4 = this.f2371;
            if ((i4 & 28672) == 0 || (i4 & (m3823(this.f2375, this.f2373) << 12)) != 0) {
                return true;
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public View m3821(int i, int i2, int i3, int i4) {
        int r0 = this.f2369.m3828();
        int r1 = this.f2369.m3831();
        int i5 = i2 > i ? 1 : -1;
        View view = null;
        while (i != i2) {
            View r4 = this.f2369.m3830(i);
            this.f2370.m3826(r0, r1, this.f2369.m3829(r4), this.f2369.m3832(r4));
            if (i3 != 0) {
                this.f2370.m3824();
                this.f2370.m3825(i3);
                if (this.f2370.m3827()) {
                    return r4;
                }
            }
            if (i4 != 0) {
                this.f2370.m3824();
                this.f2370.m3825(i4);
                if (this.f2370.m3827()) {
                    view = r4;
                }
            }
            i += i5;
        }
        return view;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3822(View view, int i) {
        this.f2370.m3826(this.f2369.m3828(), this.f2369.m3831(), this.f2369.m3829(view), this.f2369.m3832(view));
        if (i == 0) {
            return false;
        }
        this.f2370.m3824();
        this.f2370.m3825(i);
        return this.f2370.m3827();
    }
}
