package android.support.v7.widget;

import android.support.v7.widget.C0632;
import java.util.List;

/* renamed from: android.support.v7.widget.ᵢᵢ  reason: contains not printable characters */
/* compiled from: OpReorderer */
class C0685 {

    /* renamed from: ʻ  reason: contains not printable characters */
    final C0686 f2725;

    /* renamed from: android.support.v7.widget.ᵢᵢ$ʻ  reason: contains not printable characters */
    /* compiled from: OpReorderer */
    interface C0686 {
        /* renamed from: ʻ  reason: contains not printable characters */
        C0632.C0634 m4284(int i, int i2, int i3, Object obj);

        /* renamed from: ʻ  reason: contains not printable characters */
        void m4285(C0632.C0634 r1);
    }

    C0685(C0686 r1) {
        this.f2725 = r1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4281(List<C0632.C0634> list) {
        while (true) {
            int r0 = m4279(list);
            if (r0 != -1) {
                m4278(list, r0, r0 + 1);
            } else {
                return;
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4278(List<C0632.C0634> list, int i, int i2) {
        C0632.C0634 r4 = list.get(i);
        C0632.C0634 r6 = list.get(i2);
        int i3 = r6.f2502;
        if (i3 == 1) {
            m4280(list, i, r4, i2, r6);
        } else if (i3 == 2) {
            m4282(list, i, r4, i2, r6);
        } else if (i3 == 4) {
            m4283(list, i, r4, i2, r6);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0097  */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m4282(java.util.List<android.support.v7.widget.C0632.C0634> r9, int r10, android.support.v7.widget.C0632.C0634 r11, int r12, android.support.v7.widget.C0632.C0634 r13) {
        /*
            r8 = this;
            int r0 = r11.f2503
            int r1 = r11.f2505
            r2 = 0
            r3 = 1
            if (r0 >= r1) goto L_0x001b
            int r0 = r13.f2503
            int r1 = r11.f2503
            if (r0 != r1) goto L_0x0019
            int r0 = r13.f2505
            int r1 = r11.f2505
            int r4 = r11.f2503
            int r1 = r1 - r4
            if (r0 != r1) goto L_0x0019
            r0 = 0
            goto L_0x002c
        L_0x0019:
            r0 = 0
            goto L_0x002f
        L_0x001b:
            int r0 = r13.f2503
            int r1 = r11.f2505
            int r1 = r1 + r3
            if (r0 != r1) goto L_0x002e
            int r0 = r13.f2505
            int r1 = r11.f2503
            int r4 = r11.f2505
            int r1 = r1 - r4
            if (r0 != r1) goto L_0x002e
            r0 = 1
        L_0x002c:
            r2 = 1
            goto L_0x002f
        L_0x002e:
            r0 = 1
        L_0x002f:
            int r1 = r11.f2505
            int r4 = r13.f2503
            r5 = 2
            if (r1 >= r4) goto L_0x003c
            int r1 = r13.f2503
            int r1 = r1 - r3
            r13.f2503 = r1
            goto L_0x005b
        L_0x003c:
            int r1 = r11.f2505
            int r4 = r13.f2503
            int r6 = r13.f2505
            int r4 = r4 + r6
            if (r1 >= r4) goto L_0x005b
            int r10 = r13.f2505
            int r10 = r10 - r3
            r13.f2505 = r10
            r11.f2502 = r5
            r11.f2505 = r3
            int r10 = r13.f2505
            if (r10 != 0) goto L_0x005a
            r9.remove(r12)
            android.support.v7.widget.ᵢᵢ$ʻ r9 = r8.f2725
            r9.m4285(r13)
        L_0x005a:
            return
        L_0x005b:
            int r1 = r11.f2503
            int r4 = r13.f2503
            r6 = 0
            if (r1 > r4) goto L_0x0068
            int r1 = r13.f2503
            int r1 = r1 + r3
            r13.f2503 = r1
            goto L_0x0089
        L_0x0068:
            int r1 = r11.f2503
            int r4 = r13.f2503
            int r7 = r13.f2505
            int r4 = r4 + r7
            if (r1 >= r4) goto L_0x0089
            int r1 = r13.f2503
            int r4 = r13.f2505
            int r1 = r1 + r4
            int r4 = r11.f2503
            int r1 = r1 - r4
            android.support.v7.widget.ᵢᵢ$ʻ r4 = r8.f2725
            int r7 = r11.f2503
            int r7 = r7 + r3
            android.support.v7.widget.ˆ$ʼ r6 = r4.m4284(r5, r7, r1, r6)
            int r1 = r11.f2503
            int r3 = r13.f2503
            int r1 = r1 - r3
            r13.f2505 = r1
        L_0x0089:
            if (r2 == 0) goto L_0x0097
            r9.set(r10, r13)
            r9.remove(r12)
            android.support.v7.widget.ᵢᵢ$ʻ r9 = r8.f2725
            r9.m4285(r11)
            return
        L_0x0097:
            if (r0 == 0) goto L_0x00d0
            if (r6 == 0) goto L_0x00b5
            int r0 = r11.f2503
            int r1 = r6.f2503
            if (r0 <= r1) goto L_0x00a8
            int r0 = r11.f2503
            int r1 = r6.f2505
            int r0 = r0 - r1
            r11.f2503 = r0
        L_0x00a8:
            int r0 = r11.f2505
            int r1 = r6.f2503
            if (r0 <= r1) goto L_0x00b5
            int r0 = r11.f2505
            int r1 = r6.f2505
            int r0 = r0 - r1
            r11.f2505 = r0
        L_0x00b5:
            int r0 = r11.f2503
            int r1 = r13.f2503
            if (r0 <= r1) goto L_0x00c2
            int r0 = r11.f2503
            int r1 = r13.f2505
            int r0 = r0 - r1
            r11.f2503 = r0
        L_0x00c2:
            int r0 = r11.f2505
            int r1 = r13.f2503
            if (r0 <= r1) goto L_0x0106
            int r0 = r11.f2505
            int r1 = r13.f2505
            int r0 = r0 - r1
            r11.f2505 = r0
            goto L_0x0106
        L_0x00d0:
            if (r6 == 0) goto L_0x00ec
            int r0 = r11.f2503
            int r1 = r6.f2503
            if (r0 < r1) goto L_0x00df
            int r0 = r11.f2503
            int r1 = r6.f2505
            int r0 = r0 - r1
            r11.f2503 = r0
        L_0x00df:
            int r0 = r11.f2505
            int r1 = r6.f2503
            if (r0 < r1) goto L_0x00ec
            int r0 = r11.f2505
            int r1 = r6.f2505
            int r0 = r0 - r1
            r11.f2505 = r0
        L_0x00ec:
            int r0 = r11.f2503
            int r1 = r13.f2503
            if (r0 < r1) goto L_0x00f9
            int r0 = r11.f2503
            int r1 = r13.f2505
            int r0 = r0 - r1
            r11.f2503 = r0
        L_0x00f9:
            int r0 = r11.f2505
            int r1 = r13.f2503
            if (r0 < r1) goto L_0x0106
            int r0 = r11.f2505
            int r1 = r13.f2505
            int r0 = r0 - r1
            r11.f2505 = r0
        L_0x0106:
            r9.set(r10, r13)
            int r13 = r11.f2503
            int r0 = r11.f2505
            if (r13 == r0) goto L_0x0113
            r9.set(r12, r11)
            goto L_0x0116
        L_0x0113:
            r9.remove(r12)
        L_0x0116:
            if (r6 == 0) goto L_0x011b
            r9.add(r10, r6)
        L_0x011b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0685.m4282(java.util.List, int, android.support.v7.widget.ˆ$ʼ, int, android.support.v7.widget.ˆ$ʼ):void");
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m4280(List<C0632.C0634> list, int i, C0632.C0634 r6, int i2, C0632.C0634 r8) {
        int i3 = r6.f2505 < r8.f2503 ? -1 : 0;
        if (r6.f2503 < r8.f2503) {
            i3++;
        }
        if (r8.f2503 <= r6.f2503) {
            r6.f2503 += r8.f2505;
        }
        if (r8.f2503 <= r6.f2505) {
            r6.f2505 += r8.f2505;
        }
        r8.f2503 += i3;
        list.set(i, r8);
        list.set(i2, r6);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002f  */
    /* renamed from: ʼ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m4283(java.util.List<android.support.v7.widget.C0632.C0634> r8, int r9, android.support.v7.widget.C0632.C0634 r10, int r11, android.support.v7.widget.C0632.C0634 r12) {
        /*
            r7 = this;
            int r0 = r10.f2505
            int r1 = r12.f2503
            r2 = 4
            r3 = 0
            r4 = 1
            if (r0 >= r1) goto L_0x000f
            int r0 = r12.f2503
            int r0 = r0 - r4
            r12.f2503 = r0
            goto L_0x0028
        L_0x000f:
            int r0 = r10.f2505
            int r1 = r12.f2503
            int r5 = r12.f2505
            int r1 = r1 + r5
            if (r0 >= r1) goto L_0x0028
            int r0 = r12.f2505
            int r0 = r0 - r4
            r12.f2505 = r0
            android.support.v7.widget.ᵢᵢ$ʻ r0 = r7.f2725
            int r1 = r10.f2503
            java.lang.Object r5 = r12.f2504
            android.support.v7.widget.ˆ$ʼ r0 = r0.m4284(r2, r1, r4, r5)
            goto L_0x0029
        L_0x0028:
            r0 = r3
        L_0x0029:
            int r1 = r10.f2503
            int r5 = r12.f2503
            if (r1 > r5) goto L_0x0035
            int r1 = r12.f2503
            int r1 = r1 + r4
            r12.f2503 = r1
            goto L_0x0056
        L_0x0035:
            int r1 = r10.f2503
            int r5 = r12.f2503
            int r6 = r12.f2505
            int r5 = r5 + r6
            if (r1 >= r5) goto L_0x0056
            int r1 = r12.f2503
            int r3 = r12.f2505
            int r1 = r1 + r3
            int r3 = r10.f2503
            int r1 = r1 - r3
            android.support.v7.widget.ᵢᵢ$ʻ r3 = r7.f2725
            int r5 = r10.f2503
            int r5 = r5 + r4
            java.lang.Object r4 = r12.f2504
            android.support.v7.widget.ˆ$ʼ r3 = r3.m4284(r2, r5, r1, r4)
            int r2 = r12.f2505
            int r2 = r2 - r1
            r12.f2505 = r2
        L_0x0056:
            r8.set(r11, r10)
            int r10 = r12.f2505
            if (r10 <= 0) goto L_0x0061
            r8.set(r9, r12)
            goto L_0x0069
        L_0x0061:
            r8.remove(r9)
            android.support.v7.widget.ᵢᵢ$ʻ r10 = r7.f2725
            r10.m4285(r12)
        L_0x0069:
            if (r0 == 0) goto L_0x006e
            r8.add(r9, r0)
        L_0x006e:
            if (r3 == 0) goto L_0x0073
            r8.add(r9, r3)
        L_0x0073:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.C0685.m4283(java.util.List, int, android.support.v7.widget.ˆ$ʼ, int, android.support.v7.widget.ˆ$ʼ):void");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int m4279(List<C0632.C0634> list) {
        boolean z = false;
        for (int size = list.size() - 1; size >= 0; size--) {
            if (list.get(size).f2502 != 8) {
                z = true;
            } else if (z) {
                return size;
            }
        }
        return -1;
    }
}
