package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.ʼ.ʻ.C0288;
import android.support.v4.ˉ.C0393;
import android.support.v7.view.C0528;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.C0496;
import android.support.v7.view.menu.C0504;
import android.support.v7.view.menu.C0508;
import android.support.v7.view.menu.C0517;
import android.support.v7.view.menu.C0518;
import android.support.v7.view.menu.C0520;
import android.support.v7.view.menu.C0524;
import android.support.v7.view.menu.C0526;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.ʻ.C0727;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

/* renamed from: android.support.v7.widget.ʾ  reason: contains not printable characters */
/* compiled from: ActionMenuPresenter */
class C0614 extends C0496 implements C0393.C0394 {

    /* renamed from: ˈ  reason: contains not printable characters */
    C0618 f2389;

    /* renamed from: ˉ  reason: contains not printable characters */
    C0619 f2390;

    /* renamed from: ˊ  reason: contains not printable characters */
    C0615 f2391;

    /* renamed from: ˋ  reason: contains not printable characters */
    C0617 f2392;

    /* renamed from: ˎ  reason: contains not printable characters */
    final C0620 f2393 = new C0620();

    /* renamed from: ˏ  reason: contains not printable characters */
    int f2394;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Drawable f2395;

    /* renamed from: י  reason: contains not printable characters */
    private boolean f2396;

    /* renamed from: ـ  reason: contains not printable characters */
    private boolean f2397;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f2398;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f2399;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private C0616 f2400;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private int f2401;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private int f2402;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f2403;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private boolean f2404;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private boolean f2405;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private boolean f2406;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int f2407;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final SparseBooleanArray f2408 = new SparseBooleanArray();

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private View f2409;

    public C0614(Context context) {
        super(context, C0727.C0734.abc_action_menu_layout, C0727.C0734.abc_action_menu_item_layout);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3917(Context context, C0504 r6) {
        super.m2800(context, r6);
        Resources resources = context.getResources();
        C0528 r5 = C0528.m3072(context);
        if (!this.f2398) {
            this.f2397 = r5.m3074();
        }
        if (!this.f2405) {
            this.f2399 = r5.m3075();
        }
        if (!this.f2403) {
            this.f2402 = r5.m3073();
        }
        int i = this.f2399;
        if (this.f2397) {
            if (this.f2389 == null) {
                this.f2389 = new C0618(this.f1651);
                if (this.f2396) {
                    this.f2389.setImageDrawable(this.f2395);
                    this.f2395 = null;
                    this.f2396 = false;
                }
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.f2389.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i -= this.f2389.getMeasuredWidth();
        } else {
            this.f2389 = null;
        }
        this.f2401 = i;
        this.f2407 = (int) (resources.getDisplayMetrics().density * 56.0f);
        this.f2409 = null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3918(Configuration configuration) {
        if (!this.f2403) {
            this.f2402 = C0528.m3072(this.f1652).m3073();
        }
        if (this.f1653 != null) {
            this.f1653.m2899(true);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3931(boolean z) {
        this.f2397 = z;
        this.f2398 = true;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m3932(boolean z) {
        this.f2406 = z;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3919(Drawable drawable) {
        C0618 r0 = this.f2389;
        if (r0 != null) {
            r0.setImageDrawable(drawable);
            return;
        }
        this.f2396 = true;
        this.f2395 = drawable;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public Drawable m3933() {
        C0618 r0 = this.f2389;
        if (r0 != null) {
            return r0.getDrawable();
        }
        if (this.f2396) {
            return this.f2395;
        }
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0520 m3915(ViewGroup viewGroup) {
        C0520 r0 = this.f1656;
        C0520 r2 = super.m2797(viewGroup);
        if (r0 != r2) {
            ((ActionMenuView) r2).setPresenter(this);
        }
        return r2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public View m3916(C0508 r3, View view, ViewGroup viewGroup) {
        View actionView = r3.getActionView();
        if (actionView == null || r3.m2968()) {
            actionView = super.m2798(r3, view, viewGroup);
        }
        actionView.setVisibility(r3.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.m3200(layoutParams));
        }
        return actionView;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3922(C0508 r2, C0520.C0521 r3) {
        r3.m3030(r2, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) r3;
        actionMenuItemView.setItemInvoker((ActionMenuView) this.f1656);
        if (this.f2400 == null) {
            this.f2400 = new C0616();
        }
        actionMenuItemView.setPopupCallback(this.f2400);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3926(int i, C0508 r2) {
        return r2.m2964();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3924(boolean z) {
        super.m2805(z);
        ((View) this.f1656).requestLayout();
        boolean z2 = false;
        if (this.f1653 != null) {
            ArrayList<C0508> r5 = this.f1653.m2925();
            int size = r5.size();
            for (int i = 0; i < size; i++) {
                C0393 r3 = r5.get(i).m2945();
                if (r3 != null) {
                    r3.m2144(this);
                }
            }
        }
        ArrayList<C0508> r52 = this.f1653 != null ? this.f1653.m2926() : null;
        if (this.f2397 && r52 != null) {
            int size2 = r52.size();
            if (size2 == 1) {
                z2 = !((C0508) r52.get(0)).isActionViewExpanded();
            } else if (size2 > 0) {
                z2 = true;
            }
        }
        if (z2) {
            if (this.f2389 == null) {
                this.f2389 = new C0618(this.f1651);
            }
            ViewGroup viewGroup = (ViewGroup) this.f2389.getParent();
            if (viewGroup != this.f1656) {
                if (viewGroup != null) {
                    viewGroup.removeView(this.f2389);
                }
                ActionMenuView actionMenuView = (ActionMenuView) this.f1656;
                actionMenuView.addView(this.f2389, actionMenuView.m3201());
            }
        } else {
            C0618 r53 = this.f2389;
            if (r53 != null && r53.getParent() == this.f1656) {
                ((ViewGroup) this.f1656).removeView(this.f2389);
            }
        }
        ((ActionMenuView) this.f1656).setOverflowReserved(this.f2397);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3928(ViewGroup viewGroup, int i) {
        if (viewGroup.getChildAt(i) == this.f2389) {
            return false;
        }
        return super.m2810(viewGroup, i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3927(C0526 r8) {
        boolean z = false;
        if (!r8.hasVisibleItems()) {
            return false;
        }
        C0526 r0 = r8;
        while (r0.m3070() != this.f1653) {
            r0 = (C0526) r0.m3070();
        }
        View r02 = m3909(r0.getItem());
        if (r02 == null) {
            return false;
        }
        this.f2394 = r8.getItem().getItemId();
        int size = r8.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                break;
            }
            MenuItem item = r8.getItem(i);
            if (item.isVisible() && item.getIcon() != null) {
                z = true;
                break;
            }
            i++;
        }
        this.f2391 = new C0615(this.f1652, r8, r02);
        this.f2391.m3009(z);
        this.f2391.m3004();
        super.m2809(r8);
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private View m3909(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) this.f1656;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if ((childAt instanceof C0520.C0521) && ((C0520.C0521) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m3934() {
        if (!this.f2397 || m3938() || this.f1653 == null || this.f1656 == null || this.f2392 != null || this.f1653.m2926().isEmpty()) {
            return false;
        }
        this.f2392 = new C0617(new C0619(this.f1652, this.f1653, this.f2389, true));
        ((View) this.f1656).post(this.f2392);
        super.m2809((C0526) null);
        return true;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m3935() {
        if (this.f2392 == null || this.f1656 == null) {
            C0619 r0 = this.f2390;
            if (r0 == null) {
                return false;
            }
            r0.m3013();
            return true;
        }
        ((View) this.f1656).removeCallbacks(this.f2392);
        this.f2392 = null;
        return true;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m3936() {
        return m3935() | m3937();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean m3937() {
        C0615 r0 = this.f2391;
        if (r0 == null) {
            return false;
        }
        r0.m3013();
        return true;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean m3938() {
        C0619 r0 = this.f2390;
        return r0 != null && r0.m3015();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean m3939() {
        return this.f2392 != null || m3938();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ActionMenuView.ʻ(android.view.View, int, int, int, int):int
     arg types: [android.view.View, int, int, int, int]
     candidates:
      android.support.v7.widget.ˎˎ.ʻ(android.view.View, int, int, int, int):void
      android.support.v7.widget.ActionMenuView.ʻ(android.view.View, int, int, int, int):int */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3925() {
        int i;
        ArrayList<C0508> arrayList;
        int i2;
        int i3;
        int i4;
        boolean z;
        C0614 r0 = this;
        int i5 = 0;
        if (r0.f1653 != null) {
            arrayList = r0.f1653.m2923();
            i = arrayList.size();
        } else {
            arrayList = null;
            i = 0;
        }
        int i6 = r0.f2402;
        int i7 = r0.f2401;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) r0.f1656;
        int i8 = i6;
        boolean z2 = false;
        int i9 = 0;
        int i10 = 0;
        for (int i11 = 0; i11 < i; i11++) {
            C0508 r13 = (C0508) arrayList.get(i11);
            if (r13.m2966()) {
                i9++;
            } else if (r13.m2965()) {
                i10++;
            } else {
                z2 = true;
            }
            if (r0.f2406 && r13.isActionViewExpanded()) {
                i8 = 0;
            }
        }
        if (r0.f2397 && (z2 || i10 + i9 > i8)) {
            i8--;
        }
        int i12 = i8 - i9;
        SparseBooleanArray sparseBooleanArray = r0.f2408;
        sparseBooleanArray.clear();
        if (r0.f2404) {
            int i13 = r0.f2407;
            i2 = i7 / i13;
            i3 = i13 + ((i7 % i13) / i2);
        } else {
            i3 = 0;
            i2 = 0;
        }
        int i14 = i7;
        int i15 = 0;
        int i16 = 0;
        while (i15 < i) {
            C0508 r14 = (C0508) arrayList.get(i15);
            if (r14.m2966()) {
                View r15 = r0.m3916(r14, r0.f2409, viewGroup);
                if (r0.f2409 == null) {
                    r0.f2409 = r15;
                }
                if (r0.f2404) {
                    i2 -= ActionMenuView.m3189(r15, i3, i2, makeMeasureSpec, i5);
                } else {
                    r15.measure(makeMeasureSpec, makeMeasureSpec);
                }
                int measuredWidth = r15.getMeasuredWidth();
                i14 -= measuredWidth;
                if (i16 != 0) {
                    measuredWidth = i16;
                }
                int groupId = r14.getGroupId();
                if (groupId != 0) {
                    z = true;
                    sparseBooleanArray.put(groupId, true);
                } else {
                    z = true;
                }
                r14.m2957(z);
                i4 = i;
                i16 = measuredWidth;
            } else if (r14.m2965()) {
                int groupId2 = r14.getGroupId();
                boolean z3 = sparseBooleanArray.get(groupId2);
                boolean z4 = (i12 > 0 || z3) && i14 > 0 && (!r0.f2404 || i2 > 0);
                if (z4) {
                    boolean z5 = z4;
                    View r2 = r0.m3916(r14, r0.f2409, viewGroup);
                    i4 = i;
                    if (r0.f2409 == null) {
                        r0.f2409 = r2;
                    }
                    if (r0.f2404) {
                        int r18 = ActionMenuView.m3189(r2, i3, i2, makeMeasureSpec, 0);
                        i2 -= r18;
                        if (r18 == 0) {
                            z5 = false;
                        }
                    } else {
                        r2.measure(makeMeasureSpec, makeMeasureSpec);
                    }
                    int measuredWidth2 = r2.getMeasuredWidth();
                    i14 -= measuredWidth2;
                    if (i16 == 0) {
                        i16 = measuredWidth2;
                    }
                    z4 = z5 & (!r0.f2404 ? i14 + i16 > 0 : i14 >= 0);
                } else {
                    i4 = i;
                }
                if (z4 && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                } else if (z3) {
                    sparseBooleanArray.put(groupId2, false);
                    for (int i17 = 0; i17 < i15; i17++) {
                        C0508 r152 = (C0508) arrayList.get(i17);
                        if (r152.getGroupId() == groupId2) {
                            if (r152.m2964()) {
                                i12++;
                            }
                            r152.m2957(false);
                        }
                    }
                }
                if (z4) {
                    i12--;
                }
                r14.m2957(z4);
            } else {
                i4 = i;
                r14.m2957(false);
                i15++;
                i5 = 0;
                r0 = this;
                i = i4;
            }
            i15++;
            i5 = 0;
            r0 = this;
            i = i4;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3921(C0504 r1, boolean z) {
        m3936();
        super.m2801(r1, z);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Parcelable m3930() {
        C0621 r0 = new C0621();
        r0.f2420 = this.f2394;
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3920(Parcelable parcelable) {
        MenuItem findItem;
        if (parcelable instanceof C0621) {
            C0621 r2 = (C0621) parcelable;
            if (r2.f2420 > 0 && (findItem = this.f1653.findItem(r2.f2420)) != null) {
                m3927((C0526) findItem.getSubMenu());
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3929(boolean z) {
        if (z) {
            super.m2809((C0526) null);
        } else if (this.f1653 != null) {
            this.f1653.m2907(false);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3923(ActionMenuView actionMenuView) {
        this.f1656 = actionMenuView;
        actionMenuView.m3193(this.f1653);
    }

    /* renamed from: android.support.v7.widget.ʾ$ˈ  reason: contains not printable characters */
    /* compiled from: ActionMenuPresenter */
    private static class C0621 implements Parcelable {
        public static final Parcelable.Creator<C0621> CREATOR = new Parcelable.Creator<C0621>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C0621 createFromParcel(Parcel parcel) {
                return new C0621(parcel);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0621[] newArray(int i) {
                return new C0621[i];
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        public int f2420;

        public int describeContents() {
            return 0;
        }

        C0621() {
        }

        C0621(Parcel parcel) {
            this.f2420 = parcel.readInt();
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f2420);
        }
    }

    /* renamed from: android.support.v7.widget.ʾ$ʾ  reason: contains not printable characters */
    /* compiled from: ActionMenuPresenter */
    private class C0618 extends C0674 implements ActionMenuView.C0543 {

        /* renamed from: ʼ  reason: contains not printable characters */
        private final float[] f2415 = new float[2];

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m3942() {
            return false;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public boolean m3943() {
            return false;
        }

        public C0618(Context context) {
            super(context, null, C0727.C0728.actionOverflowButtonStyle);
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            C0594.m3805(this, getContentDescription());
            setOnTouchListener(new C0648(this, C0614.this) {
                /* renamed from: ʻ  reason: contains not printable characters */
                public C0524 m3944() {
                    if (C0614.this.f2390 == null) {
                        return null;
                    }
                    return C0614.this.f2390.m3011();
                }

                /* renamed from: ʼ  reason: contains not printable characters */
                public boolean m3945() {
                    C0614.this.m3934();
                    return true;
                }

                /* renamed from: ʽ  reason: contains not printable characters */
                public boolean m3946() {
                    if (C0614.this.f2392 != null) {
                        return false;
                    }
                    C0614.this.m3935();
                    return true;
                }
            });
        }

        public boolean performClick() {
            if (super.performClick()) {
                return true;
            }
            playSoundEffect(0);
            C0614.this.m3934();
            return true;
        }

        /* access modifiers changed from: protected */
        public boolean setFrame(int i, int i2, int i3, int i4) {
            boolean frame = super.setFrame(i, i2, i3, i4);
            Drawable drawable = getDrawable();
            Drawable background = getBackground();
            if (!(drawable == null || background == null)) {
                int width = getWidth();
                int height = getHeight();
                int max = Math.max(width, height) / 2;
                int paddingLeft = (width + (getPaddingLeft() - getPaddingRight())) / 2;
                int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                C0288.m1702(background, paddingLeft - max, paddingTop - max, paddingLeft + max, paddingTop + max);
            }
            return frame;
        }
    }

    /* renamed from: android.support.v7.widget.ʾ$ʿ  reason: contains not printable characters */
    /* compiled from: ActionMenuPresenter */
    private class C0619 extends C0517 {
        public C0619(Context context, C0504 r9, View view, boolean z) {
            super(context, r9, view, z, C0727.C0728.actionOverflowMenuStyle);
            m3005(8388613);
            m3006(C0614.this.f2393);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʿ  reason: contains not printable characters */
        public void m3947() {
            if (C0614.this.f1653 != null) {
                C0614.this.f1653.close();
            }
            C0614.this.f2390 = null;
            super.m3014();
        }
    }

    /* renamed from: android.support.v7.widget.ʾ$ʻ  reason: contains not printable characters */
    /* compiled from: ActionMenuPresenter */
    private class C0615 extends C0517 {
        public C0615(Context context, C0526 r9, View view) {
            super(context, r9, view, false, C0727.C0728.actionOverflowMenuStyle);
            if (!((C0508) r9.getItem()).m2964()) {
                m3007(C0614.this.f2389 == null ? (View) C0614.this.f1656 : C0614.this.f2389);
            }
            m3006(C0614.this.f2393);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʿ  reason: contains not printable characters */
        public void m3940() {
            C0614 r0 = C0614.this;
            r0.f2391 = null;
            r0.f2394 = 0;
            super.m3014();
        }
    }

    /* renamed from: android.support.v7.widget.ʾ$ˆ  reason: contains not printable characters */
    /* compiled from: ActionMenuPresenter */
    private class C0620 implements C0518.C0519 {
        C0620() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m3949(C0504 r4) {
            if (r4 == null) {
                return false;
            }
            C0614.this.f2394 = ((C0526) r4).getItem().getItemId();
            C0518.C0519 r1 = C0614.this.m2814();
            if (r1 != null) {
                return r1.m3028(r4);
            }
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3948(C0504 r3, boolean z) {
            if (r3 instanceof C0526) {
                r3.m2930().m2907(false);
            }
            C0518.C0519 r0 = C0614.this.m2814();
            if (r0 != null) {
                r0.m3027(r3, z);
            }
        }
    }

    /* renamed from: android.support.v7.widget.ʾ$ʽ  reason: contains not printable characters */
    /* compiled from: ActionMenuPresenter */
    private class C0617 implements Runnable {

        /* renamed from: ʼ  reason: contains not printable characters */
        private C0619 f2413;

        public C0617(C0619 r2) {
            this.f2413 = r2;
        }

        public void run() {
            if (C0614.this.f1653 != null) {
                C0614.this.f1653.m2920();
            }
            View view = (View) C0614.this.f1656;
            if (!(view == null || view.getWindowToken() == null || !this.f2413.m3012())) {
                C0614.this.f2390 = this.f2413;
            }
            C0614.this.f2392 = null;
        }
    }

    /* renamed from: android.support.v7.widget.ʾ$ʼ  reason: contains not printable characters */
    /* compiled from: ActionMenuPresenter */
    private class C0616 extends ActionMenuItemView.C0494 {
        C0616() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0524 m3941() {
            if (C0614.this.f2391 != null) {
                return C0614.this.f2391.m3011();
            }
            return null;
        }
    }
}
