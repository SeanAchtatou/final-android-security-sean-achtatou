package android.support.v7.widget;

import android.graphics.Outline;

/* renamed from: android.support.v7.widget.ʽ  reason: contains not printable characters */
/* compiled from: ActionBarBackgroundDrawableV21 */
class C0610 extends C0608 {
    public C0610(ActionBarContainer actionBarContainer) {
        super(actionBarContainer);
    }

    public void getOutline(Outline outline) {
        if (this.f2383.f1906) {
            if (this.f2383.f1905 != null) {
                this.f2383.f1905.getOutline(outline);
            }
        } else if (this.f2383.f1903 != null) {
            this.f2383.f1903.getOutline(outline);
        }
    }
}
