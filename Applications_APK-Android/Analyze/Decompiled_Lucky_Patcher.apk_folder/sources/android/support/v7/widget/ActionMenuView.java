package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.C0504;
import android.support.v7.view.menu.C0508;
import android.support.v7.view.menu.C0518;
import android.support.v7.view.menu.C0520;
import android.support.v7.widget.C0652;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

public class ActionMenuView extends C0652 implements C0504.C0506, C0520 {

    /* renamed from: ʻ  reason: contains not printable characters */
    C0504.C0505 f1958;

    /* renamed from: ʼ  reason: contains not printable characters */
    C0547 f1959;

    /* renamed from: ʽ  reason: contains not printable characters */
    private C0504 f1960;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Context f1961;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f1962;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f1963;

    /* renamed from: ˈ  reason: contains not printable characters */
    private C0614 f1964;

    /* renamed from: ˉ  reason: contains not printable characters */
    private C0518.C0519 f1965;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f1966;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f1967;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f1968;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f1969;

    /* renamed from: android.support.v7.widget.ActionMenuView$ʻ  reason: contains not printable characters */
    public interface C0543 {
        /* renamed from: ʽ  reason: contains not printable characters */
        boolean m3209();

        /* renamed from: ʾ  reason: contains not printable characters */
        boolean m3210();
    }

    /* renamed from: android.support.v7.widget.ActionMenuView$ʿ  reason: contains not printable characters */
    public interface C0547 {
        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m3215(MenuItem menuItem);
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return false;
    }

    public int getWindowAnimations() {
        return 0;
    }

    public ActionMenuView(Context context) {
        this(context, null);
    }

    public ActionMenuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setBaselineAligned(false);
        float f = context.getResources().getDisplayMetrics().density;
        this.f1968 = (int) (56.0f * f);
        this.f1969 = (int) (f * 4.0f);
        this.f1961 = context;
        this.f1962 = 0;
    }

    public void setPopupTheme(int i) {
        if (this.f1962 != i) {
            this.f1962 = i;
            if (i == 0) {
                this.f1961 = getContext();
            } else {
                this.f1961 = new ContextThemeWrapper(getContext(), i);
            }
        }
    }

    public int getPopupTheme() {
        return this.f1962;
    }

    public void setPresenter(C0614 r1) {
        this.f1964 = r1;
        this.f1964.m3923(this);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        C0614 r2 = this.f1964;
        if (r2 != null) {
            r2.m3924(false);
            if (this.f1964.m3938()) {
                this.f1964.m3935();
                this.f1964.m3934();
            }
        }
    }

    public void setOnMenuItemClickListener(C0547 r1) {
        this.f1959 = r1;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        C0504 r1;
        boolean z = this.f1966;
        this.f1966 = View.MeasureSpec.getMode(i) == 1073741824;
        if (z != this.f1966) {
            this.f1967 = 0;
        }
        int size = View.MeasureSpec.getSize(i);
        if (!(!this.f1966 || (r1 = this.f1960) == null || size == this.f1967)) {
            this.f1967 = size;
            r1.m2899(true);
        }
        int childCount = getChildCount();
        if (!this.f1966 || childCount <= 0) {
            for (int i3 = 0; i3 < childCount; i3++) {
                C0545 r2 = (C0545) getChildAt(i3).getLayoutParams();
                r2.rightMargin = 0;
                r2.leftMargin = 0;
            }
            super.onMeasure(i, i2);
            return;
        }
        m3190(i, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ActionMenuView.ʻ(android.view.View, int, int, int, int):int
     arg types: [android.view.View, int, int, int, int]
     candidates:
      android.support.v7.widget.ˎˎ.ʻ(android.view.View, int, int, int, int):void
      android.support.v7.widget.ActionMenuView.ʻ(android.view.View, int, int, int, int):int */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x0251 A[LOOP:5: B:135:0x0251->B:139:0x0270, LOOP_START, PHI: r13 
      PHI: (r13v3 int) = (r13v2 int), (r13v4 int) binds: [B:134:0x024f, B:139:0x0270] A[DONT_GENERATE, DONT_INLINE]] */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x0275  */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x0278  */
    /* renamed from: ʽ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m3190(int r30, int r31) {
        /*
            r29 = this;
            r0 = r29
            int r1 = android.view.View.MeasureSpec.getMode(r31)
            int r2 = android.view.View.MeasureSpec.getSize(r30)
            int r3 = android.view.View.MeasureSpec.getSize(r31)
            int r4 = r29.getPaddingLeft()
            int r5 = r29.getPaddingRight()
            int r4 = r4 + r5
            int r5 = r29.getPaddingTop()
            int r6 = r29.getPaddingBottom()
            int r5 = r5 + r6
            r6 = -2
            r7 = r31
            int r6 = getChildMeasureSpec(r7, r5, r6)
            int r2 = r2 - r4
            int r4 = r0.f1968
            int r7 = r2 / r4
            int r8 = r2 % r4
            r9 = 0
            if (r7 != 0) goto L_0x0035
            r0.setMeasuredDimension(r2, r9)
            return
        L_0x0035:
            int r8 = r8 / r7
            int r4 = r4 + r8
            int r8 = r29.getChildCount()
            r14 = r7
            r7 = 0
            r10 = 0
            r12 = 0
            r13 = 0
            r15 = 0
            r16 = 0
            r17 = 0
        L_0x0045:
            if (r7 >= r8) goto L_0x00c5
            android.view.View r11 = r0.getChildAt(r7)
            int r9 = r11.getVisibility()
            r19 = r3
            r3 = 8
            if (r9 != r3) goto L_0x0057
            goto L_0x00bf
        L_0x0057:
            boolean r3 = r11 instanceof android.support.v7.view.menu.ActionMenuItemView
            int r13 = r13 + 1
            if (r3 == 0) goto L_0x0066
            int r9 = r0.f1969
            r20 = r13
            r13 = 0
            r11.setPadding(r9, r13, r9, r13)
            goto L_0x0069
        L_0x0066:
            r20 = r13
            r13 = 0
        L_0x0069:
            android.view.ViewGroup$LayoutParams r9 = r11.getLayoutParams()
            android.support.v7.widget.ActionMenuView$ʽ r9 = (android.support.v7.widget.ActionMenuView.C0545) r9
            r9.f1975 = r13
            r9.f1972 = r13
            r9.f1971 = r13
            r9.f1973 = r13
            r9.leftMargin = r13
            r9.rightMargin = r13
            if (r3 == 0) goto L_0x0088
            r3 = r11
            android.support.v7.view.menu.ActionMenuItemView r3 = (android.support.v7.view.menu.ActionMenuItemView) r3
            boolean r3 = r3.m2774()
            if (r3 == 0) goto L_0x0088
            r3 = 1
            goto L_0x0089
        L_0x0088:
            r3 = 0
        L_0x0089:
            r9.f1974 = r3
            boolean r3 = r9.f1970
            if (r3 == 0) goto L_0x0091
            r3 = 1
            goto L_0x0092
        L_0x0091:
            r3 = r14
        L_0x0092:
            int r3 = m3189(r11, r4, r3, r6, r5)
            int r13 = java.lang.Math.max(r15, r3)
            boolean r15 = r9.f1973
            if (r15 == 0) goto L_0x00a0
            int r16 = r16 + 1
        L_0x00a0:
            boolean r9 = r9.f1970
            if (r9 == 0) goto L_0x00a5
            r12 = 1
        L_0x00a5:
            int r14 = r14 - r3
            int r9 = r11.getMeasuredHeight()
            int r10 = java.lang.Math.max(r10, r9)
            r9 = 1
            if (r3 != r9) goto L_0x00bb
            int r3 = r9 << r7
            r11 = r10
            long r9 = (long) r3
            long r9 = r17 | r9
            r17 = r9
            r10 = r11
            goto L_0x00bc
        L_0x00bb:
            r11 = r10
        L_0x00bc:
            r15 = r13
            r13 = r20
        L_0x00bf:
            int r7 = r7 + 1
            r3 = r19
            r9 = 0
            goto L_0x0045
        L_0x00c5:
            r19 = r3
            r3 = 2
            if (r12 == 0) goto L_0x00ce
            if (r13 != r3) goto L_0x00ce
            r5 = 1
            goto L_0x00cf
        L_0x00ce:
            r5 = 0
        L_0x00cf:
            r7 = 0
        L_0x00d0:
            if (r16 <= 0) goto L_0x0182
            if (r14 <= 0) goto L_0x0182
            r9 = 2147483647(0x7fffffff, float:NaN)
            r3 = 2147483647(0x7fffffff, float:NaN)
            r9 = 0
            r11 = 0
            r20 = 0
        L_0x00de:
            if (r9 >= r8) goto L_0x0124
            android.view.View r22 = r0.getChildAt(r9)
            android.view.ViewGroup$LayoutParams r22 = r22.getLayoutParams()
            r23 = r7
            r7 = r22
            android.support.v7.widget.ActionMenuView$ʽ r7 = (android.support.v7.widget.ActionMenuView.C0545) r7
            r22 = r10
            boolean r10 = r7.f1973
            if (r10 != 0) goto L_0x00f8
        L_0x00f4:
            r7 = r1
            r24 = r2
            goto L_0x011a
        L_0x00f8:
            int r10 = r7.f1971
            if (r10 >= r3) goto L_0x0109
            int r3 = r7.f1971
            r10 = 1
            int r7 = r10 << r9
            long r10 = (long) r7
            r7 = r1
            r24 = r2
            r20 = r10
            r11 = 1
            goto L_0x011a
        L_0x0109:
            int r7 = r7.f1971
            if (r7 != r3) goto L_0x00f4
            r7 = 1
            int r10 = r7 << r9
            r7 = r1
            r24 = r2
            long r1 = (long) r10
            long r1 = r20 | r1
            int r11 = r11 + 1
            r20 = r1
        L_0x011a:
            int r9 = r9 + 1
            r1 = r7
            r10 = r22
            r7 = r23
            r2 = r24
            goto L_0x00de
        L_0x0124:
            r24 = r2
            r23 = r7
            r22 = r10
            r7 = r1
            long r17 = r17 | r20
            if (r11 <= r14) goto L_0x0130
            goto L_0x0189
        L_0x0130:
            int r3 = r3 + 1
            r1 = 0
        L_0x0133:
            if (r1 >= r8) goto L_0x0179
            android.view.View r2 = r0.getChildAt(r1)
            android.view.ViewGroup$LayoutParams r9 = r2.getLayoutParams()
            android.support.v7.widget.ActionMenuView$ʽ r9 = (android.support.v7.widget.ActionMenuView.C0545) r9
            r10 = 1
            int r11 = r10 << r1
            long r10 = (long) r11
            long r25 = r20 & r10
            r27 = 0
            int r23 = (r25 > r27 ? 1 : (r25 == r27 ? 0 : -1))
            if (r23 != 0) goto L_0x0154
            int r2 = r9.f1971
            if (r2 != r3) goto L_0x0151
            long r17 = r17 | r10
        L_0x0151:
            r23 = r3
            goto L_0x0174
        L_0x0154:
            if (r5 == 0) goto L_0x0168
            boolean r10 = r9.f1974
            if (r10 == 0) goto L_0x0168
            r10 = 1
            if (r14 != r10) goto L_0x0168
            int r11 = r0.f1969
            int r10 = r11 + r4
            r23 = r3
            r3 = 0
            r2.setPadding(r10, r3, r11, r3)
            goto L_0x016a
        L_0x0168:
            r23 = r3
        L_0x016a:
            int r2 = r9.f1971
            r3 = 1
            int r2 = r2 + r3
            r9.f1971 = r2
            r9.f1975 = r3
            int r14 = r14 + -1
        L_0x0174:
            int r1 = r1 + 1
            r3 = r23
            goto L_0x0133
        L_0x0179:
            r1 = r7
            r10 = r22
            r2 = r24
            r3 = 2
            r7 = 1
            goto L_0x00d0
        L_0x0182:
            r24 = r2
            r23 = r7
            r22 = r10
            r7 = r1
        L_0x0189:
            if (r12 != 0) goto L_0x0190
            r1 = 1
            if (r13 != r1) goto L_0x0191
            r2 = 1
            goto L_0x0192
        L_0x0190:
            r1 = 1
        L_0x0191:
            r2 = 0
        L_0x0192:
            if (r14 <= 0) goto L_0x024c
            r9 = 0
            int r3 = (r17 > r9 ? 1 : (r17 == r9 ? 0 : -1))
            if (r3 == 0) goto L_0x024c
            int r13 = r13 - r1
            if (r14 < r13) goto L_0x01a1
            if (r2 != 0) goto L_0x01a1
            if (r15 <= r1) goto L_0x024c
        L_0x01a1:
            int r1 = java.lang.Long.bitCount(r17)
            float r1 = (float) r1
            if (r2 != 0) goto L_0x01e4
            r2 = 1
            long r2 = r17 & r2
            r5 = 1056964608(0x3f000000, float:0.5)
            r9 = 0
            int r11 = (r2 > r9 ? 1 : (r2 == r9 ? 0 : -1))
            if (r11 == 0) goto L_0x01c5
            r13 = 0
            android.view.View r2 = r0.getChildAt(r13)
            android.view.ViewGroup$LayoutParams r2 = r2.getLayoutParams()
            android.support.v7.widget.ActionMenuView$ʽ r2 = (android.support.v7.widget.ActionMenuView.C0545) r2
            boolean r2 = r2.f1974
            if (r2 != 0) goto L_0x01c6
            float r1 = r1 - r5
            goto L_0x01c6
        L_0x01c5:
            r13 = 0
        L_0x01c6:
            int r2 = r8 + -1
            r3 = 1
            int r9 = r3 << r2
            long r9 = (long) r9
            long r9 = r17 & r9
            r11 = 0
            int r3 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r3 == 0) goto L_0x01e5
            android.view.View r2 = r0.getChildAt(r2)
            android.view.ViewGroup$LayoutParams r2 = r2.getLayoutParams()
            android.support.v7.widget.ActionMenuView$ʽ r2 = (android.support.v7.widget.ActionMenuView.C0545) r2
            boolean r2 = r2.f1974
            if (r2 != 0) goto L_0x01e5
            float r1 = r1 - r5
            goto L_0x01e5
        L_0x01e4:
            r13 = 0
        L_0x01e5:
            r2 = 0
            int r2 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r2 <= 0) goto L_0x01f0
            int r14 = r14 * r4
            float r2 = (float) r14
            float r2 = r2 / r1
            int r9 = (int) r2
            goto L_0x01f1
        L_0x01f0:
            r9 = 0
        L_0x01f1:
            r11 = r23
            r1 = 0
        L_0x01f4:
            if (r1 >= r8) goto L_0x0249
            r2 = 1
            int r3 = r2 << r1
            long r2 = (long) r3
            long r2 = r17 & r2
            r14 = 0
            int r5 = (r2 > r14 ? 1 : (r2 == r14 ? 0 : -1))
            if (r5 != 0) goto L_0x0205
            r2 = 1
            r5 = 2
            goto L_0x0246
        L_0x0205:
            android.view.View r2 = r0.getChildAt(r1)
            android.view.ViewGroup$LayoutParams r3 = r2.getLayoutParams()
            android.support.v7.widget.ActionMenuView$ʽ r3 = (android.support.v7.widget.ActionMenuView.C0545) r3
            boolean r2 = r2 instanceof android.support.v7.view.menu.ActionMenuItemView
            if (r2 == 0) goto L_0x0227
            r3.f1972 = r9
            r2 = 1
            r3.f1975 = r2
            if (r1 != 0) goto L_0x0224
            boolean r2 = r3.f1974
            if (r2 != 0) goto L_0x0224
            int r2 = -r9
            r5 = 2
            int r2 = r2 / r5
            r3.leftMargin = r2
            goto L_0x0225
        L_0x0224:
            r5 = 2
        L_0x0225:
            r2 = 1
            goto L_0x0235
        L_0x0227:
            r5 = 2
            boolean r2 = r3.f1970
            if (r2 == 0) goto L_0x0237
            r3.f1972 = r9
            r2 = 1
            r3.f1975 = r2
            int r10 = -r9
            int r10 = r10 / r5
            r3.rightMargin = r10
        L_0x0235:
            r11 = 1
            goto L_0x0246
        L_0x0237:
            r2 = 1
            if (r1 == 0) goto L_0x023e
            int r10 = r9 / 2
            r3.leftMargin = r10
        L_0x023e:
            int r10 = r8 + -1
            if (r1 == r10) goto L_0x0246
            int r10 = r9 / 2
            r3.rightMargin = r10
        L_0x0246:
            int r1 = r1 + 1
            goto L_0x01f4
        L_0x0249:
            r23 = r11
            goto L_0x024d
        L_0x024c:
            r13 = 0
        L_0x024d:
            r1 = 1073741824(0x40000000, float:2.0)
            if (r23 == 0) goto L_0x0273
        L_0x0251:
            if (r13 >= r8) goto L_0x0273
            android.view.View r2 = r0.getChildAt(r13)
            android.view.ViewGroup$LayoutParams r3 = r2.getLayoutParams()
            android.support.v7.widget.ActionMenuView$ʽ r3 = (android.support.v7.widget.ActionMenuView.C0545) r3
            boolean r5 = r3.f1975
            if (r5 != 0) goto L_0x0262
            goto L_0x0270
        L_0x0262:
            int r5 = r3.f1971
            int r5 = r5 * r4
            int r3 = r3.f1972
            int r5 = r5 + r3
            int r3 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r1)
            r2.measure(r3, r6)
        L_0x0270:
            int r13 = r13 + 1
            goto L_0x0251
        L_0x0273:
            if (r7 == r1) goto L_0x0278
            r1 = r22
            goto L_0x027a
        L_0x0278:
            r1 = r19
        L_0x027a:
            r2 = r24
            r0.setMeasuredDimension(r2, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ActionMenuView.m3190(int, int):void");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static int m3189(View view, int i, int i2, int i3, int i4) {
        C0545 r0 = (C0545) view.getLayoutParams();
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i3) - i4, View.MeasureSpec.getMode(i3));
        ActionMenuItemView actionMenuItemView = view instanceof ActionMenuItemView ? (ActionMenuItemView) view : null;
        boolean z = true;
        boolean z2 = actionMenuItemView != null && actionMenuItemView.m2774();
        int i5 = 2;
        if (i2 <= 0 || (z2 && i2 < 2)) {
            i5 = 0;
        } else {
            view.measure(View.MeasureSpec.makeMeasureSpec(i2 * i, Integer.MIN_VALUE), makeMeasureSpec);
            int measuredWidth = view.getMeasuredWidth();
            int i6 = measuredWidth / i;
            if (measuredWidth % i != 0) {
                i6++;
            }
            if (!z2 || i6 >= 2) {
                i5 = i6;
            }
        }
        if (r0.f1970 || !z2) {
            z = false;
        }
        r0.f1973 = z;
        r0.f1971 = i5;
        view.measure(View.MeasureSpec.makeMeasureSpec(i * i5, 1073741824), makeMeasureSpec);
        return i5;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        if (!this.f1966) {
            super.onLayout(z, i, i2, i3, i4);
            return;
        }
        int childCount = getChildCount();
        int i9 = (i4 - i2) / 2;
        int dividerWidth = getDividerWidth();
        int i10 = i3 - i;
        int paddingRight = (i10 - getPaddingRight()) - getPaddingLeft();
        boolean r6 = C0607.m3858(this);
        int i11 = paddingRight;
        int i12 = 0;
        int i13 = 0;
        for (int i14 = 0; i14 < childCount; i14++) {
            View childAt = getChildAt(i14);
            if (childAt.getVisibility() != 8) {
                C0545 r11 = (C0545) childAt.getLayoutParams();
                if (r11.f1970) {
                    int measuredWidth = childAt.getMeasuredWidth();
                    if (m3196(i14)) {
                        measuredWidth += dividerWidth;
                    }
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (r6) {
                        i7 = getPaddingLeft() + r11.leftMargin;
                        i8 = i7 + measuredWidth;
                    } else {
                        i8 = (getWidth() - getPaddingRight()) - r11.rightMargin;
                        i7 = i8 - measuredWidth;
                    }
                    int i15 = i9 - (measuredHeight / 2);
                    childAt.layout(i7, i15, i8, measuredHeight + i15);
                    i11 -= measuredWidth;
                    i12 = 1;
                } else {
                    i11 -= (childAt.getMeasuredWidth() + r11.leftMargin) + r11.rightMargin;
                    boolean r7 = m3196(i14);
                    i13++;
                }
            }
        }
        if (childCount == 1 && i12 == 0) {
            View childAt2 = getChildAt(0);
            int measuredWidth2 = childAt2.getMeasuredWidth();
            int measuredHeight2 = childAt2.getMeasuredHeight();
            int i16 = (i10 / 2) - (measuredWidth2 / 2);
            int i17 = i9 - (measuredHeight2 / 2);
            childAt2.layout(i16, i17, measuredWidth2 + i16, measuredHeight2 + i17);
            return;
        }
        int i18 = i13 - (i12 ^ 1);
        if (i18 > 0) {
            i5 = i11 / i18;
            i6 = 0;
        } else {
            i6 = 0;
            i5 = 0;
        }
        int max = Math.max(i6, i5);
        if (r6) {
            int width = getWidth() - getPaddingRight();
            while (i6 < childCount) {
                View childAt3 = getChildAt(i6);
                C0545 r72 = (C0545) childAt3.getLayoutParams();
                if (childAt3.getVisibility() != 8 && !r72.f1970) {
                    int i19 = width - r72.rightMargin;
                    int measuredWidth3 = childAt3.getMeasuredWidth();
                    int measuredHeight3 = childAt3.getMeasuredHeight();
                    int i20 = i9 - (measuredHeight3 / 2);
                    childAt3.layout(i19 - measuredWidth3, i20, i19, measuredHeight3 + i20);
                    width = i19 - ((measuredWidth3 + r72.leftMargin) + max);
                }
                i6++;
            }
            return;
        }
        int paddingLeft = getPaddingLeft();
        while (i6 < childCount) {
            View childAt4 = getChildAt(i6);
            C0545 r73 = (C0545) childAt4.getLayoutParams();
            if (childAt4.getVisibility() != 8 && !r73.f1970) {
                int i21 = paddingLeft + r73.leftMargin;
                int measuredWidth4 = childAt4.getMeasuredWidth();
                int measuredHeight4 = childAt4.getMeasuredHeight();
                int i22 = i9 - (measuredHeight4 / 2);
                childAt4.layout(i21, i22, i21 + measuredWidth4, measuredHeight4 + i22);
                paddingLeft = i21 + measuredWidth4 + r73.rightMargin + max;
            }
            i6++;
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        m3207();
    }

    public void setOverflowIcon(Drawable drawable) {
        getMenu();
        this.f1964.m3919(drawable);
    }

    public Drawable getOverflowIcon() {
        getMenu();
        return this.f1964.m3933();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3195() {
        return this.f1963;
    }

    public void setOverflowReserved(boolean z) {
        this.f1963 = z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public C0545 m3208() {
        C0545 r0 = new C0545(-2, -2);
        r0.f2603 = 16;
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0545 m3199(AttributeSet attributeSet) {
        return new C0545(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0545 m3200(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams == null) {
            return m3208();
        }
        C0545 r0 = layoutParams instanceof C0545 ? new C0545((C0545) layoutParams) : new C0545(layoutParams);
        if (r0.f2603 <= 0) {
            r0.f2603 = 16;
        }
        return r0;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams != null && (layoutParams instanceof C0545);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public C0545 m3201() {
        C0545 r0 = m3208();
        r0.f1970 = true;
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3197(C0508 r3) {
        return this.f1960.m2901(r3, 0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3193(C0504 r1) {
        this.f1960 = r1;
    }

    public Menu getMenu() {
        if (this.f1960 == null) {
            Context context = getContext();
            this.f1960 = new C0504(context);
            this.f1960.m2893(new C0546());
            this.f1964 = new C0614(context);
            this.f1964.m3931(true);
            C0614 r0 = this.f1964;
            C0518.C0519 r1 = this.f1965;
            if (r1 == null) {
                r1 = new C0544();
            }
            r0.m2803(r1);
            this.f1960.m2896(this.f1964, this.f1961);
            this.f1964.m3923(this);
        }
        return this.f1960;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3194(C0518.C0519 r1, C0504.C0505 r2) {
        this.f1965 = r1;
        this.f1958 = r2;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public C0504 m3202() {
        return this.f1960;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m3203() {
        C0614 r0 = this.f1964;
        return r0 != null && r0.m3934();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m3204() {
        C0614 r0 = this.f1964;
        return r0 != null && r0.m3935();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m3205() {
        C0614 r0 = this.f1964;
        return r0 != null && r0.m3938();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m3206() {
        C0614 r0 = this.f1964;
        return r0 != null && r0.m3939();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public void m3207() {
        C0614 r0 = this.f1964;
        if (r0 != null) {
            r0.m3936();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3196(int i) {
        boolean z = false;
        if (i == 0) {
            return false;
        }
        View childAt = getChildAt(i - 1);
        View childAt2 = getChildAt(i);
        if (i < getChildCount() && (childAt instanceof C0543)) {
            z = false | ((C0543) childAt).m3210();
        }
        return (i <= 0 || !(childAt2 instanceof C0543)) ? z : z | ((C0543) childAt2).m3209();
    }

    public void setExpandedActionViewsExclusive(boolean z) {
        this.f1964.m3932(z);
    }

    /* renamed from: android.support.v7.widget.ActionMenuView$ʾ  reason: contains not printable characters */
    private class C0546 implements C0504.C0505 {
        C0546() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m3214(C0504 r1, MenuItem menuItem) {
            return ActionMenuView.this.f1959 != null && ActionMenuView.this.f1959.m3215(menuItem);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3213(C0504 r2) {
            if (ActionMenuView.this.f1958 != null) {
                ActionMenuView.this.f1958.m2933(r2);
            }
        }
    }

    /* renamed from: android.support.v7.widget.ActionMenuView$ʼ  reason: contains not printable characters */
    private static class C0544 implements C0518.C0519 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3211(C0504 r1, boolean z) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m3212(C0504 r1) {
            return false;
        }

        C0544() {
        }
    }

    /* renamed from: android.support.v7.widget.ActionMenuView$ʽ  reason: contains not printable characters */
    public static class C0545 extends C0652.C0653 {
        @ViewDebug.ExportedProperty

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean f1970;
        @ViewDebug.ExportedProperty

        /* renamed from: ʼ  reason: contains not printable characters */
        public int f1971;
        @ViewDebug.ExportedProperty

        /* renamed from: ʽ  reason: contains not printable characters */
        public int f1972;
        @ViewDebug.ExportedProperty

        /* renamed from: ʾ  reason: contains not printable characters */
        public boolean f1973;
        @ViewDebug.ExportedProperty

        /* renamed from: ʿ  reason: contains not printable characters */
        public boolean f1974;

        /* renamed from: ˆ  reason: contains not printable characters */
        boolean f1975;

        public C0545(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public C0545(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public C0545(C0545 r1) {
            super(r1);
            this.f1970 = r1.f1970;
        }

        public C0545(int i, int i2) {
            super(i, i2);
            this.f1970 = false;
        }
    }
}
