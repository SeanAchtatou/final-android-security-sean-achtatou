package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.C0637;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class FitWindowsLinearLayout extends LinearLayout implements C0637 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private C0637.C0638 f2013;

    public FitWindowsLinearLayout(Context context) {
        super(context);
    }

    public FitWindowsLinearLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setOnFitSystemWindowsListener(C0637.C0638 r1) {
        this.f2013 = r1;
    }

    /* access modifiers changed from: protected */
    public boolean fitSystemWindows(Rect rect) {
        C0637.C0638 r0 = this.f2013;
        if (r0 != null) {
            r0.m4057(rect);
        }
        return super.fitSystemWindows(rect);
    }
}
