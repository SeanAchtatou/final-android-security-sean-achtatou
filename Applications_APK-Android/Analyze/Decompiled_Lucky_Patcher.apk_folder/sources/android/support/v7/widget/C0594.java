package android.support.v7.widget;

import android.annotation.TargetApi;
import android.os.Build;
import android.view.View;

/* renamed from: android.support.v7.widget.ʻי  reason: contains not printable characters */
/* compiled from: TooltipCompat */
public class C0594 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final C0597 f2349;

    /* renamed from: android.support.v7.widget.ʻי$ʽ  reason: contains not printable characters */
    /* compiled from: TooltipCompat */
    private interface C0597 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m3808(View view, CharSequence charSequence);
    }

    /* renamed from: android.support.v7.widget.ʻי$ʼ  reason: contains not printable characters */
    /* compiled from: TooltipCompat */
    private static class C0596 implements C0597 {
        private C0596() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3807(View view, CharSequence charSequence) {
            C0598.m3812(view, charSequence);
        }
    }

    @TargetApi(26)
    /* renamed from: android.support.v7.widget.ʻי$ʻ  reason: contains not printable characters */
    /* compiled from: TooltipCompat */
    private static class C0595 implements C0597 {
        private C0595() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3806(View view, CharSequence charSequence) {
            view.setTooltipText(charSequence);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 26) {
            f2349 = new C0595();
        } else {
            f2349 = new C0596();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m3805(View view, CharSequence charSequence) {
        f2349.m3808(view, charSequence);
    }
}
