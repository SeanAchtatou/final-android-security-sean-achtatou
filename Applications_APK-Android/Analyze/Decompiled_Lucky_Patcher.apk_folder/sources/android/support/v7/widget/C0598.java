package android.support.v7.widget;

import android.support.v4.ˉ.C0414;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityManager;

/* renamed from: android.support.v7.widget.ʻـ  reason: contains not printable characters */
/* compiled from: TooltipCompatHandler */
class C0598 implements View.OnAttachStateChangeListener, View.OnHoverListener, View.OnLongClickListener {

    /* renamed from: ˊ  reason: contains not printable characters */
    private static C0598 f2350;

    /* renamed from: ʻ  reason: contains not printable characters */
    private final View f2351;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final CharSequence f2352;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Runnable f2353 = new Runnable() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.ʻـ.ʻ(android.support.v7.widget.ʻـ, boolean):void
         arg types: [android.support.v7.widget.ʻـ, int]
         candidates:
          android.support.v7.widget.ʻـ.ʻ(android.view.View, java.lang.CharSequence):void
          android.support.v7.widget.ʻـ.ʻ(android.support.v7.widget.ʻـ, boolean):void */
        public void run() {
            C0598.this.m3813(false);
        }
    };

    /* renamed from: ʾ  reason: contains not printable characters */
    private final Runnable f2354 = new Runnable() {
        public void run() {
            C0598.this.m3809();
        }
    };

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f2355;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f2356;

    /* renamed from: ˈ  reason: contains not printable characters */
    private C0599 f2357;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f2358;

    public void onViewAttachedToWindow(View view) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m3812(View view, CharSequence charSequence) {
        if (TextUtils.isEmpty(charSequence)) {
            C0598 r2 = f2350;
            if (r2 != null && r2.f2351 == view) {
                r2.m3809();
            }
            view.setOnLongClickListener(null);
            view.setLongClickable(false);
            view.setOnHoverListener(null);
            return;
        }
        new C0598(view, charSequence);
    }

    private C0598(View view, CharSequence charSequence) {
        this.f2351 = view;
        this.f2352 = charSequence;
        this.f2351.setOnLongClickListener(this);
        this.f2351.setOnHoverListener(this);
    }

    public boolean onLongClick(View view) {
        this.f2355 = view.getWidth() / 2;
        this.f2356 = view.getHeight() / 2;
        m3813(true);
        return true;
    }

    public boolean onHover(View view, MotionEvent motionEvent) {
        if (this.f2357 != null && this.f2358) {
            return false;
        }
        AccessibilityManager accessibilityManager = (AccessibilityManager) this.f2351.getContext().getSystemService("accessibility");
        if (accessibilityManager.isEnabled() && accessibilityManager.isTouchExplorationEnabled()) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action != 7) {
            if (action == 10) {
                m3809();
            }
        } else if (this.f2351.isEnabled() && this.f2357 == null) {
            this.f2355 = (int) motionEvent.getX();
            this.f2356 = (int) motionEvent.getY();
            this.f2351.removeCallbacks(this.f2353);
            this.f2351.postDelayed(this.f2353, (long) ViewConfiguration.getLongPressTimeout());
        }
        return false;
    }

    public void onViewDetachedFromWindow(View view) {
        m3809();
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3813(boolean z) {
        long j;
        int i;
        long j2;
        if (C0414.m2257(this.f2351)) {
            C0598 r0 = f2350;
            if (r0 != null) {
                r0.m3809();
            }
            f2350 = this;
            this.f2358 = z;
            this.f2357 = new C0599(this.f2351.getContext());
            this.f2357.m3817(this.f2351, this.f2355, this.f2356, this.f2358, this.f2352);
            this.f2351.addOnAttachStateChangeListener(this);
            if (this.f2358) {
                j = 2500;
            } else {
                if ((C0414.m2245(this.f2351) & 1) == 1) {
                    j2 = 3000;
                    i = ViewConfiguration.getLongPressTimeout();
                } else {
                    j2 = 15000;
                    i = ViewConfiguration.getLongPressTimeout();
                }
                j = j2 - ((long) i);
            }
            this.f2351.removeCallbacks(this.f2354);
            this.f2351.postDelayed(this.f2354, j);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3809() {
        if (f2350 == this) {
            f2350 = null;
            C0599 r1 = this.f2357;
            if (r1 != null) {
                r1.m3816();
                this.f2357 = null;
                this.f2351.removeOnAttachStateChangeListener(this);
            } else {
                Log.e("TooltipCompatHandler", "sActiveHandler.mPopup == null");
            }
        }
        this.f2351.removeCallbacks(this.f2353);
        this.f2351.removeCallbacks(this.f2354);
    }
}
