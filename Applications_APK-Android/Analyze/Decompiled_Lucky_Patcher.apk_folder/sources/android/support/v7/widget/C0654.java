package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.widget.C0155;
import android.support.v4.ʼ.ʻ.C0288;
import android.support.v7.ʻ.C0727;
import android.support.v7.ʼ.ʻ.C0739;
import android.util.AttributeSet;
import android.widget.CompoundButton;

/* renamed from: android.support.v7.widget.ˏ  reason: contains not printable characters */
/* compiled from: AppCompatCompoundButtonHelper */
class C0654 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final CompoundButton f2604;

    /* renamed from: ʼ  reason: contains not printable characters */
    private ColorStateList f2605 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    private PorterDuff.Mode f2606 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f2607 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f2608 = false;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f2609;

    C0654(CompoundButton compoundButton) {
        this.f2604 = compoundButton;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4152(AttributeSet attributeSet, int i) {
        int resourceId;
        TypedArray obtainStyledAttributes = this.f2604.getContext().obtainStyledAttributes(attributeSet, C0727.C0737.CompoundButton, i, 0);
        try {
            if (obtainStyledAttributes.hasValue(C0727.C0737.CompoundButton_android_button) && (resourceId = obtainStyledAttributes.getResourceId(C0727.C0737.CompoundButton_android_button, 0)) != 0) {
                this.f2604.setButtonDrawable(C0739.m4883(this.f2604.getContext(), resourceId));
            }
            if (obtainStyledAttributes.hasValue(C0727.C0737.CompoundButton_buttonTint)) {
                C0155.m999(this.f2604, obtainStyledAttributes.getColorStateList(C0727.C0737.CompoundButton_buttonTint));
            }
            if (obtainStyledAttributes.hasValue(C0727.C0737.CompoundButton_buttonTintMode)) {
                C0155.m1000(this.f2604, C0670.m4230(obtainStyledAttributes.getInt(C0727.C0737.CompoundButton_buttonTintMode, -1), null));
            }
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4150(ColorStateList colorStateList) {
        this.f2605 = colorStateList;
        this.f2607 = true;
        m4155();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public ColorStateList m4149() {
        return this.f2605;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4151(PorterDuff.Mode mode) {
        this.f2606 = mode;
        this.f2608 = true;
        m4155();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public PorterDuff.Mode m4153() {
        return this.f2606;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m4154() {
        if (this.f2609) {
            this.f2609 = false;
            return;
        }
        this.f2609 = true;
        m4155();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m4155() {
        Drawable r0 = C0155.m998(this.f2604);
        if (r0 == null) {
            return;
        }
        if (this.f2607 || this.f2608) {
            Drawable mutate = C0288.m1713(r0).mutate();
            if (this.f2607) {
                C0288.m1703(mutate, this.f2605);
            }
            if (this.f2608) {
                C0288.m1706(mutate, this.f2606);
            }
            if (mutate.isStateful()) {
                mutate.setState(this.f2604.getDrawableState());
            }
            this.f2604.setButtonDrawable(mutate);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m4148(int i) {
        Drawable r0;
        return (Build.VERSION.SDK_INT >= 17 || (r0 = C0155.m998(this.f2604)) == null) ? i : i + r0.getIntrinsicWidth();
    }
}
