package android.support.v7.widget;

import android.content.Context;
import android.os.Build;
import android.support.v4.widget.C0173;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.PopupWindow;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;

/* renamed from: android.support.v7.widget.ᵎ  reason: contains not printable characters */
/* compiled from: AppCompatPopupWindow */
class C0678 extends PopupWindow {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final boolean f2700 = (Build.VERSION.SDK_INT < 21);

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f2701;

    public C0678(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        m4253(context, attributeSet, i, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ʻˏ.ʻ(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.widget.ʻˏ.ʻ(int, float):float
      android.support.v7.widget.ʻˏ.ʻ(int, int):int
      android.support.v7.widget.ʻˏ.ʻ(int, boolean):boolean */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4253(Context context, AttributeSet attributeSet, int i, int i2) {
        C0592 r2 = C0592.m3740(context, attributeSet, C0727.C0737.PopupWindow, i, i2);
        if (r2.m3758(C0727.C0737.PopupWindow_overlapAnchor)) {
            m4255(r2.m3746(C0727.C0737.PopupWindow_overlapAnchor, false));
        }
        setBackgroundDrawable(r2.m3744(C0727.C0737.PopupWindow_android_popupBackground));
        int i3 = Build.VERSION.SDK_INT;
        if (i2 != 0 && i3 < 11 && r2.m3758(C0727.C0737.PopupWindow_android_popupAnimationStyle)) {
            setAnimationStyle(r2.m3757(C0727.C0737.PopupWindow_android_popupAnimationStyle, -1));
        }
        r2.m3745();
        if (Build.VERSION.SDK_INT < 14) {
            m4254(this);
        }
    }

    public void showAsDropDown(View view, int i, int i2) {
        if (f2700 && this.f2701) {
            i2 -= view.getHeight();
        }
        super.showAsDropDown(view, i, i2);
    }

    public void showAsDropDown(View view, int i, int i2, int i3) {
        if (f2700 && this.f2701) {
            i2 -= view.getHeight();
        }
        super.showAsDropDown(view, i, i2, i3);
    }

    public void update(View view, int i, int i2, int i3, int i4) {
        if (f2700 && this.f2701) {
            i2 -= view.getHeight();
        }
        super.update(view, i, i2, i3, i4);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m4254(final PopupWindow popupWindow) {
        try {
            final Field declaredField = PopupWindow.class.getDeclaredField("mAnchor");
            declaredField.setAccessible(true);
            Field declaredField2 = PopupWindow.class.getDeclaredField("mOnScrollChangedListener");
            declaredField2.setAccessible(true);
            final ViewTreeObserver.OnScrollChangedListener onScrollChangedListener = (ViewTreeObserver.OnScrollChangedListener) declaredField2.get(popupWindow);
            declaredField2.set(popupWindow, new ViewTreeObserver.OnScrollChangedListener() {
                public void onScrollChanged() {
                    try {
                        WeakReference weakReference = (WeakReference) declaredField.get(popupWindow);
                        if (weakReference == null) {
                            return;
                        }
                        if (weakReference.get() != null) {
                            onScrollChangedListener.onScrollChanged();
                        }
                    } catch (IllegalAccessException unused) {
                    }
                }
            });
        } catch (Exception e) {
            Log.d("AppCompatPopupWindow", "Exception while installing workaround OnScrollChangedListener", e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.ˊ.ʻ(android.widget.PopupWindow, boolean):void
     arg types: [android.support.v7.widget.ᵎ, boolean]
     candidates:
      android.support.v4.widget.ˊ.ʻ(android.widget.PopupWindow, int):void
      android.support.v4.widget.ˊ.ʻ(android.widget.PopupWindow, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4255(boolean z) {
        if (f2700) {
            this.f2701 = z;
        } else {
            C0173.m1046((PopupWindow) this, z);
        }
    }
}
