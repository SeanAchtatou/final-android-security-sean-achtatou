package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.ˉ.C0414;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class ActionBarContainer extends FrameLayout {

    /* renamed from: ʻ  reason: contains not printable characters */
    Drawable f1903;

    /* renamed from: ʼ  reason: contains not printable characters */
    Drawable f1904;

    /* renamed from: ʽ  reason: contains not printable characters */
    Drawable f1905;

    /* renamed from: ʾ  reason: contains not printable characters */
    boolean f1906;

    /* renamed from: ʿ  reason: contains not printable characters */
    boolean f1907;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f1908;

    /* renamed from: ˈ  reason: contains not printable characters */
    private View f1909;

    /* renamed from: ˉ  reason: contains not printable characters */
    private View f1910;

    /* renamed from: ˊ  reason: contains not printable characters */
    private View f1911;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f1912;

    public ActionMode startActionModeForChild(View view, ActionMode.Callback callback) {
        return null;
    }

    public ActionBarContainer(Context context) {
        this(context, null);
    }

    public ActionBarContainer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        C0414.m2223(this, Build.VERSION.SDK_INT >= 21 ? new C0610(this) : new C0608(this));
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0727.C0737.ActionBar);
        this.f1903 = obtainStyledAttributes.getDrawable(C0727.C0737.ActionBar_background);
        this.f1904 = obtainStyledAttributes.getDrawable(C0727.C0737.ActionBar_backgroundStacked);
        this.f1912 = obtainStyledAttributes.getDimensionPixelSize(C0727.C0737.ActionBar_height, -1);
        if (getId() == C0727.C0733.split_action_bar) {
            this.f1906 = true;
            this.f1905 = obtainStyledAttributes.getDrawable(C0727.C0737.ActionBar_backgroundSplit);
        }
        obtainStyledAttributes.recycle();
        boolean z = false;
        if (!this.f1906 ? this.f1903 == null && this.f1904 == null : this.f1905 == null) {
            z = true;
        }
        setWillNotDraw(z);
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        this.f1910 = findViewById(C0727.C0733.action_bar);
        this.f1911 = findViewById(C0727.C0733.action_context_bar);
    }

    public void setPrimaryBackground(Drawable drawable) {
        Drawable drawable2 = this.f1903;
        if (drawable2 != null) {
            drawable2.setCallback(null);
            unscheduleDrawable(this.f1903);
        }
        this.f1903 = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            View view = this.f1910;
            if (view != null) {
                this.f1903.setBounds(view.getLeft(), this.f1910.getTop(), this.f1910.getRight(), this.f1910.getBottom());
            }
        }
        boolean z = true;
        if (!this.f1906 ? !(this.f1903 == null && this.f1904 == null) : this.f1905 != null) {
            z = false;
        }
        setWillNotDraw(z);
        invalidate();
    }

    public void setStackedBackground(Drawable drawable) {
        Drawable drawable2;
        Drawable drawable3 = this.f1904;
        if (drawable3 != null) {
            drawable3.setCallback(null);
            unscheduleDrawable(this.f1904);
        }
        this.f1904 = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            if (this.f1907 && (drawable2 = this.f1904) != null) {
                drawable2.setBounds(this.f1909.getLeft(), this.f1909.getTop(), this.f1909.getRight(), this.f1909.getBottom());
            }
        }
        boolean z = true;
        if (!this.f1906 ? !(this.f1903 == null && this.f1904 == null) : this.f1905 != null) {
            z = false;
        }
        setWillNotDraw(z);
        invalidate();
    }

    public void setSplitBackground(Drawable drawable) {
        Drawable drawable2;
        Drawable drawable3 = this.f1905;
        if (drawable3 != null) {
            drawable3.setCallback(null);
            unscheduleDrawable(this.f1905);
        }
        this.f1905 = drawable;
        boolean z = false;
        if (drawable != null) {
            drawable.setCallback(this);
            if (this.f1906 && (drawable2 = this.f1905) != null) {
                drawable2.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight());
            }
        }
        if (!this.f1906 ? this.f1903 == null && this.f1904 == null : this.f1905 == null) {
            z = true;
        }
        setWillNotDraw(z);
        invalidate();
    }

    public void setVisibility(int i) {
        super.setVisibility(i);
        boolean z = i == 0;
        Drawable drawable = this.f1903;
        if (drawable != null) {
            drawable.setVisible(z, false);
        }
        Drawable drawable2 = this.f1904;
        if (drawable2 != null) {
            drawable2.setVisible(z, false);
        }
        Drawable drawable3 = this.f1905;
        if (drawable3 != null) {
            drawable3.setVisible(z, false);
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return (drawable == this.f1903 && !this.f1906) || (drawable == this.f1904 && this.f1907) || ((drawable == this.f1905 && this.f1906) || super.verifyDrawable(drawable));
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.f1903;
        if (drawable != null && drawable.isStateful()) {
            this.f1903.setState(getDrawableState());
        }
        Drawable drawable2 = this.f1904;
        if (drawable2 != null && drawable2.isStateful()) {
            this.f1904.setState(getDrawableState());
        }
        Drawable drawable3 = this.f1905;
        if (drawable3 != null && drawable3.isStateful()) {
            this.f1905.setState(getDrawableState());
        }
    }

    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        Drawable drawable = this.f1903;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
        Drawable drawable2 = this.f1904;
        if (drawable2 != null) {
            drawable2.jumpToCurrentState();
        }
        Drawable drawable3 = this.f1905;
        if (drawable3 != null) {
            drawable3.jumpToCurrentState();
        }
    }

    public void setTransitioning(boolean z) {
        this.f1908 = z;
        setDescendantFocusability(z ? 393216 : 262144);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.f1908 || super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        return true;
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        super.onHoverEvent(motionEvent);
        return true;
    }

    public void setTabContainer(C0580 r3) {
        View view = this.f1909;
        if (view != null) {
            removeView(view);
        }
        this.f1909 = r3;
        if (r3 != null) {
            addView(r3);
            ViewGroup.LayoutParams layoutParams = r3.getLayoutParams();
            layoutParams.width = -1;
            layoutParams.height = -2;
            r3.setAllowCollapse(false);
        }
    }

    public View getTabContainer() {
        return this.f1909;
    }

    public ActionMode startActionModeForChild(View view, ActionMode.Callback callback, int i) {
        if (i != 0) {
            return super.startActionModeForChild(view, callback, i);
        }
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m3152(View view) {
        return view == null || view.getVisibility() == 8 || view.getMeasuredHeight() == 0;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int m3153(View view) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
        return view.getMeasuredHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
    }

    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        if (this.f1910 == null && View.MeasureSpec.getMode(i2) == Integer.MIN_VALUE && (i4 = this.f1912) >= 0) {
            i2 = View.MeasureSpec.makeMeasureSpec(Math.min(i4, View.MeasureSpec.getSize(i2)), Integer.MIN_VALUE);
        }
        super.onMeasure(i, i2);
        if (this.f1910 != null) {
            int mode = View.MeasureSpec.getMode(i2);
            View view = this.f1909;
            if (view != null && view.getVisibility() != 8 && mode != 1073741824) {
                if (!m3152(this.f1910)) {
                    i3 = m3153(this.f1910);
                } else {
                    i3 = !m3152(this.f1911) ? m3153(this.f1911) : 0;
                }
                setMeasuredDimension(getMeasuredWidth(), Math.min(i3 + m3153(this.f1909), mode == Integer.MIN_VALUE ? View.MeasureSpec.getSize(i2) : Integer.MAX_VALUE));
            }
        }
    }

    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        Drawable drawable;
        super.onLayout(z, i, i2, i3, i4);
        View view = this.f1909;
        boolean z2 = true;
        boolean z3 = false;
        boolean z4 = (view == null || view.getVisibility() == 8) ? false : true;
        if (!(view == null || view.getVisibility() == 8)) {
            int measuredHeight = getMeasuredHeight();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
            view.layout(i, (measuredHeight - view.getMeasuredHeight()) - layoutParams.bottomMargin, i3, measuredHeight - layoutParams.bottomMargin);
        }
        if (this.f1906) {
            Drawable drawable2 = this.f1905;
            if (drawable2 != null) {
                drawable2.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight());
            } else {
                z2 = false;
            }
        } else {
            if (this.f1903 != null) {
                if (this.f1910.getVisibility() == 0) {
                    this.f1903.setBounds(this.f1910.getLeft(), this.f1910.getTop(), this.f1910.getRight(), this.f1910.getBottom());
                } else {
                    View view2 = this.f1911;
                    if (view2 == null || view2.getVisibility() != 0) {
                        this.f1903.setBounds(0, 0, 0, 0);
                    } else {
                        this.f1903.setBounds(this.f1911.getLeft(), this.f1911.getTop(), this.f1911.getRight(), this.f1911.getBottom());
                    }
                }
                z3 = true;
            }
            this.f1907 = z4;
            if (!z4 || (drawable = this.f1904) == null) {
                z2 = z3;
            } else {
                drawable.setBounds(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
        }
        if (z2) {
            invalidate();
        }
    }
}
