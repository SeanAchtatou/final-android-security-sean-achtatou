package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.ˉ.C0413;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.widget.EditText;

/* renamed from: android.support.v7.widget.י  reason: contains not printable characters */
/* compiled from: AppCompatEditText */
public class C0667 extends EditText implements C0413 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C0639 f2678;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final C0726 f2679;

    public C0667(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0727.C0728.editTextStyle);
    }

    public C0667(Context context, AttributeSet attributeSet, int i) {
        super(C0589.m3735(context), attributeSet, i);
        this.f2678 = new C0639(this);
        this.f2678.m4065(attributeSet, i);
        this.f2679 = C0726.m4855(this);
        this.f2679.m4864(attributeSet, i);
        this.f2679.m4858();
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        C0639 r0 = this.f2678;
        if (r0 != null) {
            r0.m4061(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        C0639 r0 = this.f2678;
        if (r0 != null) {
            r0.m4064(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        C0639 r0 = this.f2678;
        if (r0 != null) {
            r0.m4062(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        C0639 r0 = this.f2678;
        if (r0 != null) {
            return r0.m4060();
        }
        return null;
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        C0639 r0 = this.f2678;
        if (r0 != null) {
            r0.m4063(mode);
        }
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        C0639 r0 = this.f2678;
        if (r0 != null) {
            return r0.m4066();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        C0639 r0 = this.f2678;
        if (r0 != null) {
            r0.m4068();
        }
        C0726 r02 = this.f2679;
        if (r02 != null) {
            r02.m4858();
        }
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        C0726 r0 = this.f2679;
        if (r0 != null) {
            r0.m4862(context, i);
        }
    }
}
