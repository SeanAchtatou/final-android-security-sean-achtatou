package android.support.v7.ʼ.ʻ;

import java.lang.reflect.Array;

/* renamed from: android.support.v7.ʼ.ʻ.ʽ  reason: contains not printable characters */
/* compiled from: GrowingArrayUtils */
final class C0741 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final /* synthetic */ boolean f2974 = (!C0741.class.desiredAssertionStatus());

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m4887(int i) {
        if (i <= 4) {
            return 8;
        }
        return i * 2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> T[] m4889(Object[] objArr, int i, Object obj) {
        if (f2974 || i <= objArr.length) {
            if (i + 1 > objArr.length) {
                Object[] objArr2 = (Object[]) Array.newInstance(objArr.getClass().getComponentType(), m4887(i));
                System.arraycopy(objArr, 0, objArr2, 0, i);
                objArr = objArr2;
            }
            objArr[i] = obj;
            return objArr;
        }
        throw new AssertionError();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int[] m4888(int[] iArr, int i, int i2) {
        if (f2974 || i <= iArr.length) {
            if (i + 1 > iArr.length) {
                int[] iArr2 = new int[m4887(i)];
                System.arraycopy(iArr, 0, iArr2, 0, i);
                iArr = iArr2;
            }
            iArr[i] = i2;
            return iArr;
        }
        throw new AssertionError();
    }

    private C0741() {
    }
}
