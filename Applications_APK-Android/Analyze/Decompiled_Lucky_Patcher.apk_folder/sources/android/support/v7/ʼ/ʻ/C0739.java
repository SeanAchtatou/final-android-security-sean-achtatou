package android.support.v7.ʼ.ʻ;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.C0101;
import android.support.v7.widget.C0656;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import java.util.WeakHashMap;

/* renamed from: android.support.v7.ʼ.ʻ.ʼ  reason: contains not printable characters */
/* compiled from: AppCompatResources */
public final class C0739 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final ThreadLocal<TypedValue> f2969 = new ThreadLocal<>();

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final WeakHashMap<Context, SparseArray<C0740>> f2970 = new WeakHashMap<>(0);

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final Object f2971 = new Object();

    /* renamed from: ʻ  reason: contains not printable characters */
    public static ColorStateList m4880(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            return context.getColorStateList(i);
        }
        ColorStateList r0 = m4885(context, i);
        if (r0 != null) {
            return r0;
        }
        ColorStateList r02 = m4884(context, i);
        if (r02 == null) {
            return C0101.m544(context, i);
        }
        m4882(context, i, r02);
        return r02;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static Drawable m4883(Context context, int i) {
        return C0656.m4164().m4183(context, i);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static ColorStateList m4884(Context context, int i) {
        if (m4886(context, i)) {
            return null;
        }
        Resources resources = context.getResources();
        try {
            return C0738.m4876(resources, resources.getXml(i), context.getTheme());
        } catch (Exception e) {
            Log.e("AppCompatResources", "Failed to inflate ColorStateList, leaving it to the framework", e);
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0033, code lost:
        return null;
     */
    /* renamed from: ʾ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.content.res.ColorStateList m4885(android.content.Context r4, int r5) {
        /*
            java.lang.Object r0 = android.support.v7.ʼ.ʻ.C0739.f2971
            monitor-enter(r0)
            java.util.WeakHashMap<android.content.Context, android.util.SparseArray<android.support.v7.ʼ.ʻ.ʼ$ʻ>> r1 = android.support.v7.ʼ.ʻ.C0739.f2970     // Catch:{ all -> 0x0035 }
            java.lang.Object r1 = r1.get(r4)     // Catch:{ all -> 0x0035 }
            android.util.SparseArray r1 = (android.util.SparseArray) r1     // Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0032
            int r2 = r1.size()     // Catch:{ all -> 0x0035 }
            if (r2 <= 0) goto L_0x0032
            java.lang.Object r2 = r1.get(r5)     // Catch:{ all -> 0x0035 }
            android.support.v7.ʼ.ʻ.ʼ$ʻ r2 = (android.support.v7.ʼ.ʻ.C0739.C0740) r2     // Catch:{ all -> 0x0035 }
            if (r2 == 0) goto L_0x0032
            android.content.res.Configuration r3 = r2.f2973     // Catch:{ all -> 0x0035 }
            android.content.res.Resources r4 = r4.getResources()     // Catch:{ all -> 0x0035 }
            android.content.res.Configuration r4 = r4.getConfiguration()     // Catch:{ all -> 0x0035 }
            boolean r4 = r3.equals(r4)     // Catch:{ all -> 0x0035 }
            if (r4 == 0) goto L_0x002f
            android.content.res.ColorStateList r4 = r2.f2972     // Catch:{ all -> 0x0035 }
            monitor-exit(r0)     // Catch:{ all -> 0x0035 }
            return r4
        L_0x002f:
            r1.remove(r5)     // Catch:{ all -> 0x0035 }
        L_0x0032:
            monitor-exit(r0)     // Catch:{ all -> 0x0035 }
            r4 = 0
            return r4
        L_0x0035:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0035 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.ʼ.ʻ.C0739.m4885(android.content.Context, int):android.content.res.ColorStateList");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m4882(Context context, int i, ColorStateList colorStateList) {
        synchronized (f2971) {
            SparseArray sparseArray = f2970.get(context);
            if (sparseArray == null) {
                sparseArray = new SparseArray();
                f2970.put(context, sparseArray);
            }
            sparseArray.append(i, new C0740(colorStateList, context.getResources().getConfiguration()));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* renamed from: ʿ  reason: contains not printable characters */
    private static boolean m4886(Context context, int i) {
        Resources resources = context.getResources();
        TypedValue r0 = m4881();
        resources.getValue(i, r0, true);
        if (r0.type < 28 || r0.type > 31) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static TypedValue m4881() {
        TypedValue typedValue = f2969.get();
        if (typedValue != null) {
            return typedValue;
        }
        TypedValue typedValue2 = new TypedValue();
        f2969.set(typedValue2);
        return typedValue2;
    }

    /* renamed from: android.support.v7.ʼ.ʻ.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: AppCompatResources */
    private static class C0740 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final ColorStateList f2972;

        /* renamed from: ʼ  reason: contains not printable characters */
        final Configuration f2973;

        C0740(ColorStateList colorStateList, Configuration configuration) {
            this.f2972 = colorStateList;
            this.f2973 = configuration;
        }
    }
}
