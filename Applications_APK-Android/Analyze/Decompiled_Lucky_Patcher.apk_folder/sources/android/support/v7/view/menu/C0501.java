package android.support.v7.view.menu;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.v7.view.menu.C0518;
import android.support.v7.view.menu.C0520;
import android.support.v7.ʻ.C0727;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import java.util.ArrayList;

/* renamed from: android.support.v7.view.menu.ˆ  reason: contains not printable characters */
/* compiled from: ListMenuPresenter */
public class C0501 implements C0518, AdapterView.OnItemClickListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    Context f1701;

    /* renamed from: ʼ  reason: contains not printable characters */
    LayoutInflater f1702;

    /* renamed from: ʽ  reason: contains not printable characters */
    C0504 f1703;

    /* renamed from: ʾ  reason: contains not printable characters */
    ExpandedMenuView f1704;

    /* renamed from: ʿ  reason: contains not printable characters */
    int f1705;

    /* renamed from: ˆ  reason: contains not printable characters */
    int f1706;

    /* renamed from: ˈ  reason: contains not printable characters */
    int f1707;

    /* renamed from: ˉ  reason: contains not printable characters */
    C0502 f1708;

    /* renamed from: ˊ  reason: contains not printable characters */
    private C0518.C0519 f1709;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f1710;

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2860() {
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2861(C0504 r1, C0508 r2) {
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m2865(C0504 r1, C0508 r2) {
        return false;
    }

    public C0501(Context context, int i) {
        this(i, 0);
        this.f1701 = context;
        this.f1702 = LayoutInflater.from(this.f1701);
    }

    public C0501(int i, int i2) {
        this.f1707 = i;
        this.f1706 = i2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2854(Context context, C0504 r4) {
        int i = this.f1706;
        if (i != 0) {
            this.f1701 = new ContextThemeWrapper(context, i);
            this.f1702 = LayoutInflater.from(this.f1701);
        } else if (this.f1701 != null) {
            this.f1701 = context;
            if (this.f1702 == null) {
                this.f1702 = LayoutInflater.from(this.f1701);
            }
        }
        this.f1703 = r4;
        C0502 r3 = this.f1708;
        if (r3 != null) {
            r3.notifyDataSetChanged();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0520 m2853(ViewGroup viewGroup) {
        if (this.f1704 == null) {
            this.f1704 = (ExpandedMenuView) this.f1702.inflate(C0727.C0734.abc_expanded_menu_layout, viewGroup, false);
            if (this.f1708 == null) {
                this.f1708 = new C0502();
            }
            this.f1704.setAdapter((ListAdapter) this.f1708);
            this.f1704.setOnItemClickListener(this);
        }
        return this.f1704;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public ListAdapter m2867() {
        if (this.f1708 == null) {
            this.f1708 = new C0502();
        }
        return this.f1708;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2859(boolean z) {
        C0502 r1 = this.f1708;
        if (r1 != null) {
            r1.notifyDataSetChanged();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2858(C0518.C0519 r1) {
        this.f1709 = r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2862(C0526 r3) {
        if (!r3.hasVisibleItems()) {
            return false;
        }
        new C0507(r3).m2937((IBinder) null);
        C0518.C0519 r0 = this.f1709;
        if (r0 == null) {
            return true;
        }
        r0.m3028(r3);
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2857(C0504 r2, boolean z) {
        C0518.C0519 r0 = this.f1709;
        if (r0 != null) {
            r0.m3027(r2, z);
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        this.f1703.m2902(this.f1708.getItem(i), this, 0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2855(Bundle bundle) {
        SparseArray sparseArray = new SparseArray();
        ExpandedMenuView expandedMenuView = this.f1704;
        if (expandedMenuView != null) {
            expandedMenuView.saveHierarchyState(sparseArray);
        }
        bundle.putSparseParcelableArray("android:menu:list", sparseArray);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2864(Bundle bundle) {
        SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
        if (sparseParcelableArray != null) {
            this.f1704.restoreHierarchyState(sparseParcelableArray);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m2863() {
        return this.f1710;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Parcelable m2866() {
        if (this.f1704 == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        m2855(bundle);
        return bundle;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2856(Parcelable parcelable) {
        m2864((Bundle) parcelable);
    }

    /* renamed from: android.support.v7.view.menu.ˆ$ʻ  reason: contains not printable characters */
    /* compiled from: ListMenuPresenter */
    private class C0502 extends BaseAdapter {

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f1712 = -1;

        public long getItemId(int i) {
            return (long) i;
        }

        public C0502() {
            m2869();
        }

        public int getCount() {
            int size = C0501.this.f1703.m2926().size() - C0501.this.f1705;
            return this.f1712 < 0 ? size : size - 1;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0508 getItem(int i) {
            ArrayList<C0508> r0 = C0501.this.f1703.m2926();
            int i2 = i + C0501.this.f1705;
            int i3 = this.f1712;
            if (i3 >= 0 && i2 >= i3) {
                i2++;
            }
            return r0.get(i2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = C0501.this.f1702.inflate(C0501.this.f1707, viewGroup, false);
            }
            ((C0520.C0521) view).m3030(getItem(i), 0);
            return view;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2869() {
            C0508 r0 = C0501.this.f1703.m2932();
            if (r0 != null) {
                ArrayList<C0508> r1 = C0501.this.f1703.m2926();
                int size = r1.size();
                for (int i = 0; i < size; i++) {
                    if (r1.get(i) == r0) {
                        this.f1712 = i;
                        return;
                    }
                }
            }
            this.f1712 = -1;
        }

        public void notifyDataSetChanged() {
            m2869();
            super.notifyDataSetChanged();
        }
    }
}
