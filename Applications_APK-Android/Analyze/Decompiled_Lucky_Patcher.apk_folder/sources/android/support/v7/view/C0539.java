package android.support.v7.view;

import android.support.v4.ˉ.C0434;
import android.support.v4.ˉ.C0436;
import android.support.v4.ˉ.C0437;
import android.view.View;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: android.support.v7.view.ˉ  reason: contains not printable characters */
/* compiled from: ViewPropertyAnimatorCompatSet */
public class C0539 {

    /* renamed from: ʻ  reason: contains not printable characters */
    final ArrayList<C0434> f1893 = new ArrayList<>();

    /* renamed from: ʼ  reason: contains not printable characters */
    C0436 f1894;

    /* renamed from: ʽ  reason: contains not printable characters */
    private long f1895 = -1;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Interpolator f1896;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f1897;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final C0437 f1898 = new C0437() {

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean f1900 = false;

        /* renamed from: ʽ  reason: contains not printable characters */
        private int f1901 = 0;

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3150(View view) {
            if (!this.f1900) {
                this.f1900 = true;
                if (C0539.this.f1894 != null) {
                    C0539.this.f1894.m2388(null);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3149() {
            this.f1901 = 0;
            this.f1900 = false;
            C0539.this.m3147();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m3151(View view) {
            int i = this.f1901 + 1;
            this.f1901 = i;
            if (i == C0539.this.f1893.size()) {
                if (C0539.this.f1894 != null) {
                    C0539.this.f1894.m2389(null);
                }
                m3149();
            }
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0539 m3142(C0434 r2) {
        if (!this.f1897) {
            this.f1893.add(r2);
        }
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0539 m3143(C0434 r3, C0434 r4) {
        this.f1893.add(r3);
        r4.m2382(r3.m2375());
        this.f1893.add(r4);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3146() {
        if (!this.f1897) {
            Iterator<C0434> it = this.f1893.iterator();
            while (it.hasNext()) {
                C0434 next = it.next();
                long j = this.f1895;
                if (j >= 0) {
                    next.m2377(j);
                }
                Interpolator interpolator = this.f1896;
                if (interpolator != null) {
                    next.m2380(interpolator);
                }
                if (this.f1894 != null) {
                    next.m2378(this.f1898);
                }
                next.m2384();
            }
            this.f1897 = true;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3147() {
        this.f1897 = false;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3148() {
        if (this.f1897) {
            Iterator<C0434> it = this.f1893.iterator();
            while (it.hasNext()) {
                it.next().m2383();
            }
            this.f1897 = false;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0539 m3141(long j) {
        if (!this.f1897) {
            this.f1895 = j;
        }
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0539 m3145(Interpolator interpolator) {
        if (!this.f1897) {
            this.f1896 = interpolator;
        }
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0539 m3144(C0436 r2) {
        if (!this.f1897) {
            this.f1894 = r2;
        }
        return this;
    }
}
