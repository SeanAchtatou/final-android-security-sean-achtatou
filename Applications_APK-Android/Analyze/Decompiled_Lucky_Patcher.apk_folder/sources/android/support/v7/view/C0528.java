package android.support.v7.view;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v7.ʻ.C0727;
import android.view.ViewConfiguration;

/* renamed from: android.support.v7.view.ʻ  reason: contains not printable characters */
/* compiled from: ActionBarPolicy */
public class C0528 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Context f1831;

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0528 m3072(Context context) {
        return new C0528(context);
    }

    private C0528(Context context) {
        this.f1831 = context;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m3073() {
        Configuration configuration = this.f1831.getResources().getConfiguration();
        int i = configuration.screenWidthDp;
        int i2 = configuration.screenHeightDp;
        if (configuration.smallestScreenWidthDp > 600 || i > 600) {
            return 5;
        }
        if (i > 960 && i2 > 720) {
            return 5;
        }
        if (i > 720 && i2 > 960) {
            return 5;
        }
        if (i >= 500) {
            return 4;
        }
        if (i > 640 && i2 > 480) {
            return 4;
        }
        if (i <= 480 || i2 <= 640) {
            return i >= 360 ? 3 : 2;
        }
        return 4;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m3074() {
        if (Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return !ViewConfiguration.get(this.f1831).hasPermanentMenuKey();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m3075() {
        return this.f1831.getResources().getDisplayMetrics().widthPixels / 2;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m3076() {
        return this.f1831.getResources().getBoolean(C0727.C0729.abc_action_bar_embed_tabs);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m3077() {
        TypedArray obtainStyledAttributes = this.f1831.obtainStyledAttributes(null, C0727.C0737.ActionBar, C0727.C0728.actionBarStyle, 0);
        int layoutDimension = obtainStyledAttributes.getLayoutDimension(C0727.C0737.ActionBar_height, 0);
        Resources resources = this.f1831.getResources();
        if (!m3076()) {
            layoutDimension = Math.min(layoutDimension, resources.getDimensionPixelSize(C0727.C0731.abc_action_bar_stacked_max_height));
        }
        obtainStyledAttributes.recycle();
        return layoutDimension;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m3078() {
        return this.f1831.getApplicationInfo().targetSdkVersion < 14;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public int m3079() {
        return this.f1831.getResources().getDimensionPixelSize(C0727.C0731.abc_action_bar_stacked_tab_max_width);
    }
}
