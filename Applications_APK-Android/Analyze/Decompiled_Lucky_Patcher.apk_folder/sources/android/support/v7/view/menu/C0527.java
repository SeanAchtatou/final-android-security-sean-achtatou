package android.support.v7.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.ʽ.ʻ.C0316;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

/* renamed from: android.support.v7.view.menu.ⁱ  reason: contains not printable characters */
/* compiled from: SubMenuWrapperICS */
class C0527 extends C0523 implements SubMenu {
    C0527(Context context, C0316 r2) {
        super(context, r2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0316 m3071() {
        return (C0316) this.f1664;
    }

    public SubMenu setHeaderTitle(int i) {
        m3071().setHeaderTitle(i);
        return this;
    }

    public SubMenu setHeaderTitle(CharSequence charSequence) {
        m3071().setHeaderTitle(charSequence);
        return this;
    }

    public SubMenu setHeaderIcon(int i) {
        m3071().setHeaderIcon(i);
        return this;
    }

    public SubMenu setHeaderIcon(Drawable drawable) {
        m3071().setHeaderIcon(drawable);
        return this;
    }

    public SubMenu setHeaderView(View view) {
        m3071().setHeaderView(view);
        return this;
    }

    public void clearHeader() {
        m3071().clearHeader();
    }

    public SubMenu setIcon(int i) {
        m3071().setIcon(i);
        return this;
    }

    public SubMenu setIcon(Drawable drawable) {
        m3071().setIcon(drawable);
        return this;
    }

    public MenuItem getItem() {
        return m2815(m3071().getItem());
    }
}
