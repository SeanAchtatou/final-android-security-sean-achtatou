package android.support.v7.view;

import android.content.Context;
import android.support.v4.ʽ.ʻ.C0314;
import android.support.v4.ʽ.ʻ.C0315;
import android.support.v4.ˈ.C0353;
import android.support.v7.view.C0529;
import android.support.v7.view.menu.C0522;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.util.ArrayList;

/* renamed from: android.support.v7.view.ˆ  reason: contains not printable characters */
/* compiled from: SupportActionModeWrapper */
public class C0534 extends ActionMode {

    /* renamed from: ʻ  reason: contains not printable characters */
    final Context f1846;

    /* renamed from: ʼ  reason: contains not printable characters */
    final C0529 f1847;

    public C0534(Context context, C0529 r2) {
        this.f1846 = context;
        this.f1847 = r2;
    }

    public Object getTag() {
        return this.f1847.m3095();
    }

    public void setTag(Object obj) {
        this.f1847.m3084(obj);
    }

    public void setTitle(CharSequence charSequence) {
        this.f1847.m3088(charSequence);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.f1847.m3083(charSequence);
    }

    public void invalidate() {
        this.f1847.m3090();
    }

    public void finish() {
        this.f1847.m3089();
    }

    public Menu getMenu() {
        return C0522.m3032(this.f1846, (C0314) this.f1847.m3086());
    }

    public CharSequence getTitle() {
        return this.f1847.m3091();
    }

    public void setTitle(int i) {
        this.f1847.m3081(i);
    }

    public CharSequence getSubtitle() {
        return this.f1847.m3092();
    }

    public void setSubtitle(int i) {
        this.f1847.m3087(i);
    }

    public View getCustomView() {
        return this.f1847.m3094();
    }

    public void setCustomView(View view) {
        this.f1847.m3082(view);
    }

    public MenuInflater getMenuInflater() {
        return this.f1847.m3080();
    }

    public boolean getTitleOptionalHint() {
        return this.f1847.m3096();
    }

    public void setTitleOptionalHint(boolean z) {
        this.f1847.m3085(z);
    }

    public boolean isTitleOptional() {
        return this.f1847.m3093();
    }

    /* renamed from: android.support.v7.view.ˆ$ʻ  reason: contains not printable characters */
    /* compiled from: SupportActionModeWrapper */
    public static class C0535 implements C0529.C0530 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final ActionMode.Callback f1848;

        /* renamed from: ʼ  reason: contains not printable characters */
        final Context f1849;

        /* renamed from: ʽ  reason: contains not printable characters */
        final ArrayList<C0534> f1850 = new ArrayList<>();

        /* renamed from: ʾ  reason: contains not printable characters */
        final C0353<Menu, Menu> f1851 = new C0353<>();

        public C0535(Context context, ActionMode.Callback callback) {
            this.f1849 = context;
            this.f1848 = callback;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m3125(C0529 r2, Menu menu) {
            return this.f1848.onCreateActionMode(m3127(r2), m3123(menu));
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m3128(C0529 r2, Menu menu) {
            return this.f1848.onPrepareActionMode(m3127(r2), m3123(menu));
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m3126(C0529 r3, MenuItem menuItem) {
            return this.f1848.onActionItemClicked(m3127(r3), C0522.m3033(this.f1849, (C0315) menuItem));
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3124(C0529 r2) {
            this.f1848.onDestroyActionMode(m3127(r2));
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private Menu m3123(Menu menu) {
            Menu menu2 = this.f1851.get(menu);
            if (menu2 != null) {
                return menu2;
            }
            Menu r0 = C0522.m3032(this.f1849, (C0314) menu);
            this.f1851.put(menu, r0);
            return r0;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public ActionMode m3127(C0529 r5) {
            int size = this.f1850.size();
            for (int i = 0; i < size; i++) {
                C0534 r2 = this.f1850.get(i);
                if (r2 != null && r2.f1847 == r5) {
                    return r2;
                }
            }
            C0534 r0 = new C0534(this.f1849, r5);
            this.f1850.add(r0);
            return r0;
        }
    }
}
