package android.support.v7.view;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.PorterDuff;
import android.support.v4.ʽ.ʻ.C0314;
import android.support.v4.ˉ.C0393;
import android.support.v4.ˉ.C0401;
import android.support.v7.view.menu.C0508;
import android.support.v7.view.menu.C0509;
import android.support.v7.widget.C0670;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.InflateException;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import net.lingala.zip4j.util.InternalZipConstants;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* renamed from: android.support.v7.view.ˈ  reason: contains not printable characters */
/* compiled from: SupportMenuInflater */
public class C0536 extends MenuInflater {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final Class<?>[] f1852 = {Context.class};

    /* renamed from: ʼ  reason: contains not printable characters */
    static final Class<?>[] f1853 = f1852;

    /* renamed from: ʽ  reason: contains not printable characters */
    final Object[] f1854;

    /* renamed from: ʾ  reason: contains not printable characters */
    final Object[] f1855 = this.f1854;

    /* renamed from: ʿ  reason: contains not printable characters */
    Context f1856;

    /* renamed from: ˆ  reason: contains not printable characters */
    private Object f1857;

    public C0536(Context context) {
        super(context);
        this.f1856 = context;
        this.f1854 = new Object[]{context};
    }

    public void inflate(int i, Menu menu) {
        if (!(menu instanceof C0314)) {
            super.inflate(i, menu);
            return;
        }
        XmlResourceParser xmlResourceParser = null;
        try {
            xmlResourceParser = this.f1856.getResources().getLayout(i);
            m3130(xmlResourceParser, Xml.asAttributeSet(xmlResourceParser), menu);
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
        } catch (XmlPullParserException e) {
            throw new InflateException("Error inflating menu XML", e);
        } catch (IOException e2) {
            throw new InflateException("Error inflating menu XML", e2);
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3130(XmlPullParser xmlPullParser, AttributeSet attributeSet, Menu menu) {
        C0538 r0 = new C0538(menu);
        int eventType = xmlPullParser.getEventType();
        while (true) {
            if (eventType != 2) {
                eventType = xmlPullParser.next();
                if (eventType == 1) {
                    break;
                }
            } else {
                String name = xmlPullParser.getName();
                if (name.equals("menu")) {
                    eventType = xmlPullParser.next();
                } else {
                    throw new RuntimeException("Expecting menu, got " + name);
                }
            }
        }
        int i = eventType;
        String str = null;
        boolean z = false;
        boolean z2 = false;
        while (!z) {
            if (i != 1) {
                if (i != 2) {
                    if (i == 3) {
                        String name2 = xmlPullParser.getName();
                        if (z2 && name2.equals(str)) {
                            str = null;
                            z2 = false;
                        } else if (name2.equals("group")) {
                            r0.m3135();
                        } else if (name2.equals("item")) {
                            if (!r0.m3140()) {
                                if (r0.f1861 == null || !r0.f1861.m2151()) {
                                    r0.m3137();
                                } else {
                                    r0.m3139();
                                }
                            }
                        } else if (name2.equals("menu")) {
                            z = true;
                        }
                    }
                } else if (!z2) {
                    String name3 = xmlPullParser.getName();
                    if (name3.equals("group")) {
                        r0.m3136(attributeSet);
                    } else if (name3.equals("item")) {
                        r0.m3138(attributeSet);
                    } else if (name3.equals("menu")) {
                        m3130(xmlPullParser, attributeSet, r0.m3139());
                    } else {
                        str = name3;
                        z2 = true;
                    }
                }
                i = xmlPullParser.next();
            } else {
                throw new RuntimeException("Unexpected end of document");
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Object m3131() {
        if (this.f1857 == null) {
            this.f1857 = m3129(this.f1856);
        }
        return this.f1857;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private Object m3129(Object obj) {
        return (!(obj instanceof Activity) && (obj instanceof ContextWrapper)) ? m3129(((ContextWrapper) obj).getBaseContext()) : obj;
    }

    /* renamed from: android.support.v7.view.ˈ$ʻ  reason: contains not printable characters */
    /* compiled from: SupportMenuInflater */
    private static class C0537 implements MenuItem.OnMenuItemClickListener {

        /* renamed from: ʻ  reason: contains not printable characters */
        private static final Class<?>[] f1858 = {MenuItem.class};

        /* renamed from: ʼ  reason: contains not printable characters */
        private Object f1859;

        /* renamed from: ʽ  reason: contains not printable characters */
        private Method f1860;

        public C0537(Object obj, String str) {
            this.f1859 = obj;
            Class<?> cls = obj.getClass();
            try {
                this.f1860 = cls.getMethod(str, f1858);
            } catch (Exception e) {
                InflateException inflateException = new InflateException("Couldn't resolve menu item onClick handler " + str + " in class " + cls.getName());
                inflateException.initCause(e);
                throw inflateException;
            }
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            try {
                if (this.f1860.getReturnType() == Boolean.TYPE) {
                    return ((Boolean) this.f1860.invoke(this.f1859, menuItem)).booleanValue();
                }
                this.f1860.invoke(this.f1859, menuItem);
                return true;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    /* renamed from: android.support.v7.view.ˈ$ʼ  reason: contains not printable characters */
    /* compiled from: SupportMenuInflater */
    private class C0538 {

        /* renamed from: ʻ  reason: contains not printable characters */
        C0393 f1861;

        /* renamed from: ʻʻ  reason: contains not printable characters */
        private CharSequence f1862;

        /* renamed from: ʼʼ  reason: contains not printable characters */
        private ColorStateList f1864 = null;

        /* renamed from: ʽ  reason: contains not printable characters */
        private Menu f1865;

        /* renamed from: ʽʽ  reason: contains not printable characters */
        private CharSequence f1866;

        /* renamed from: ʾ  reason: contains not printable characters */
        private int f1867;

        /* renamed from: ʿ  reason: contains not printable characters */
        private int f1868;

        /* renamed from: ʿʿ  reason: contains not printable characters */
        private PorterDuff.Mode f1869 = null;

        /* renamed from: ˆ  reason: contains not printable characters */
        private int f1870;

        /* renamed from: ˈ  reason: contains not printable characters */
        private int f1871;

        /* renamed from: ˉ  reason: contains not printable characters */
        private boolean f1872;

        /* renamed from: ˊ  reason: contains not printable characters */
        private boolean f1873;

        /* renamed from: ˋ  reason: contains not printable characters */
        private boolean f1874;

        /* renamed from: ˎ  reason: contains not printable characters */
        private int f1875;

        /* renamed from: ˏ  reason: contains not printable characters */
        private int f1876;

        /* renamed from: ˑ  reason: contains not printable characters */
        private CharSequence f1877;

        /* renamed from: י  reason: contains not printable characters */
        private CharSequence f1878;

        /* renamed from: ـ  reason: contains not printable characters */
        private int f1879;

        /* renamed from: ٴ  reason: contains not printable characters */
        private char f1880;

        /* renamed from: ᐧ  reason: contains not printable characters */
        private int f1881;

        /* renamed from: ᐧᐧ  reason: contains not printable characters */
        private String f1882;

        /* renamed from: ᴵ  reason: contains not printable characters */
        private char f1883;

        /* renamed from: ᴵᴵ  reason: contains not printable characters */
        private String f1884;

        /* renamed from: ᵎ  reason: contains not printable characters */
        private int f1885;

        /* renamed from: ᵔ  reason: contains not printable characters */
        private int f1886;

        /* renamed from: ᵢ  reason: contains not printable characters */
        private boolean f1887;

        /* renamed from: ⁱ  reason: contains not printable characters */
        private boolean f1888;

        /* renamed from: ﹳ  reason: contains not printable characters */
        private boolean f1889;

        /* renamed from: ﹶ  reason: contains not printable characters */
        private int f1890;

        /* renamed from: ﾞ  reason: contains not printable characters */
        private int f1891;

        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        private String f1892;

        public C0538(Menu menu) {
            this.f1865 = menu;
            m3135();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3135() {
            this.f1867 = 0;
            this.f1868 = 0;
            this.f1870 = 0;
            this.f1871 = 0;
            this.f1872 = true;
            this.f1873 = true;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m3136(AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = C0536.this.f1856.obtainStyledAttributes(attributeSet, C0727.C0737.MenuGroup);
            this.f1867 = obtainStyledAttributes.getResourceId(C0727.C0737.MenuGroup_android_id, 0);
            this.f1868 = obtainStyledAttributes.getInt(C0727.C0737.MenuGroup_android_menuCategory, 0);
            this.f1870 = obtainStyledAttributes.getInt(C0727.C0737.MenuGroup_android_orderInCategory, 0);
            this.f1871 = obtainStyledAttributes.getInt(C0727.C0737.MenuGroup_android_checkableBehavior, 0);
            this.f1872 = obtainStyledAttributes.getBoolean(C0727.C0737.MenuGroup_android_visible, true);
            this.f1873 = obtainStyledAttributes.getBoolean(C0727.C0737.MenuGroup_android_enabled, true);
            obtainStyledAttributes.recycle();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m3138(AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = C0536.this.f1856.obtainStyledAttributes(attributeSet, C0727.C0737.MenuItem);
            this.f1875 = obtainStyledAttributes.getResourceId(C0727.C0737.MenuItem_android_id, 0);
            this.f1876 = (obtainStyledAttributes.getInt(C0727.C0737.MenuItem_android_menuCategory, this.f1868) & -65536) | (obtainStyledAttributes.getInt(C0727.C0737.MenuItem_android_orderInCategory, this.f1870) & InternalZipConstants.MAX_ALLOWED_ZIP_COMMENT_LENGTH);
            this.f1877 = obtainStyledAttributes.getText(C0727.C0737.MenuItem_android_title);
            this.f1878 = obtainStyledAttributes.getText(C0727.C0737.MenuItem_android_titleCondensed);
            this.f1879 = obtainStyledAttributes.getResourceId(C0727.C0737.MenuItem_android_icon, 0);
            this.f1880 = m3132(obtainStyledAttributes.getString(C0727.C0737.MenuItem_android_alphabeticShortcut));
            this.f1881 = obtainStyledAttributes.getInt(C0727.C0737.MenuItem_alphabeticModifiers, 4096);
            this.f1883 = m3132(obtainStyledAttributes.getString(C0727.C0737.MenuItem_android_numericShortcut));
            this.f1885 = obtainStyledAttributes.getInt(C0727.C0737.MenuItem_numericModifiers, 4096);
            if (obtainStyledAttributes.hasValue(C0727.C0737.MenuItem_android_checkable)) {
                this.f1886 = obtainStyledAttributes.getBoolean(C0727.C0737.MenuItem_android_checkable, false) ? 1 : 0;
            } else {
                this.f1886 = this.f1871;
            }
            this.f1887 = obtainStyledAttributes.getBoolean(C0727.C0737.MenuItem_android_checked, false);
            this.f1888 = obtainStyledAttributes.getBoolean(C0727.C0737.MenuItem_android_visible, this.f1872);
            this.f1889 = obtainStyledAttributes.getBoolean(C0727.C0737.MenuItem_android_enabled, this.f1873);
            this.f1890 = obtainStyledAttributes.getInt(C0727.C0737.MenuItem_showAsAction, -1);
            this.f1884 = obtainStyledAttributes.getString(C0727.C0737.MenuItem_android_onClick);
            this.f1891 = obtainStyledAttributes.getResourceId(C0727.C0737.MenuItem_actionLayout, 0);
            this.f1892 = obtainStyledAttributes.getString(C0727.C0737.MenuItem_actionViewClass);
            this.f1882 = obtainStyledAttributes.getString(C0727.C0737.MenuItem_actionProviderClass);
            boolean z = this.f1882 != null;
            if (z && this.f1891 == 0 && this.f1892 == null) {
                this.f1861 = (C0393) m3133(this.f1882, C0536.f1853, C0536.this.f1855);
            } else {
                if (z) {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
                }
                this.f1861 = null;
            }
            this.f1862 = obtainStyledAttributes.getText(C0727.C0737.MenuItem_contentDescription);
            this.f1866 = obtainStyledAttributes.getText(C0727.C0737.MenuItem_tooltipText);
            if (obtainStyledAttributes.hasValue(C0727.C0737.MenuItem_iconTintMode)) {
                this.f1869 = C0670.m4230(obtainStyledAttributes.getInt(C0727.C0737.MenuItem_iconTintMode, -1), this.f1869);
            } else {
                this.f1869 = null;
            }
            if (obtainStyledAttributes.hasValue(C0727.C0737.MenuItem_iconTint)) {
                this.f1864 = obtainStyledAttributes.getColorStateList(C0727.C0737.MenuItem_iconTint);
            } else {
                this.f1864 = null;
            }
            obtainStyledAttributes.recycle();
            this.f1874 = false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private char m3132(String str) {
            if (str == null) {
                return 0;
            }
            return str.charAt(0);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m3134(MenuItem menuItem) {
            boolean z = false;
            menuItem.setChecked(this.f1887).setVisible(this.f1888).setEnabled(this.f1889).setCheckable(this.f1886 >= 1).setTitleCondensed(this.f1878).setIcon(this.f1879);
            int i = this.f1890;
            if (i >= 0) {
                menuItem.setShowAsAction(i);
            }
            if (this.f1884 != null) {
                if (!C0536.this.f1856.isRestricted()) {
                    menuItem.setOnMenuItemClickListener(new C0537(C0536.this.m3131(), this.f1884));
                } else {
                    throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
                }
            }
            boolean z2 = menuItem instanceof C0508;
            if (z2) {
                C0508 r1 = (C0508) menuItem;
            }
            if (this.f1886 >= 2) {
                if (z2) {
                    ((C0508) menuItem).m2949(true);
                } else if (menuItem instanceof C0509) {
                    ((C0509) menuItem).m2971(true);
                }
            }
            String str = this.f1892;
            if (str != null) {
                menuItem.setActionView((View) m3133(str, C0536.f1852, C0536.this.f1854));
                z = true;
            }
            int i2 = this.f1891;
            if (i2 > 0) {
                if (!z) {
                    menuItem.setActionView(i2);
                } else {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
                }
            }
            C0393 r0 = this.f1861;
            if (r0 != null) {
                C0401.m2163(menuItem, r0);
            }
            C0401.m2167(menuItem, this.f1862);
            C0401.m2169(menuItem, this.f1866);
            C0401.m2168(menuItem, this.f1880, this.f1881);
            C0401.m2164(menuItem, this.f1883, this.f1885);
            PorterDuff.Mode mode = this.f1869;
            if (mode != null) {
                C0401.m2166(menuItem, mode);
            }
            ColorStateList colorStateList = this.f1864;
            if (colorStateList != null) {
                C0401.m2165(menuItem, colorStateList);
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m3137() {
            this.f1874 = true;
            m3134(this.f1865.add(this.f1867, this.f1875, this.f1876, this.f1877));
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public SubMenu m3139() {
            this.f1874 = true;
            SubMenu addSubMenu = this.f1865.addSubMenu(this.f1867, this.f1875, this.f1876, this.f1877);
            m3134(addSubMenu.getItem());
            return addSubMenu;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public boolean m3140() {
            return this.f1874;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private <T> T m3133(String str, Class<?>[] clsArr, Object[] objArr) {
            try {
                Constructor<?> constructor = C0536.this.f1856.getClassLoader().loadClass(str).getConstructor(clsArr);
                constructor.setAccessible(true);
                return constructor.newInstance(objArr);
            } catch (Exception e) {
                Log.w("SupportMenuInflater", "Cannot instantiate class: " + str, e);
                return null;
            }
        }
    }
}
