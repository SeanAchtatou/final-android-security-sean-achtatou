package android.support.v7.view.menu;

import android.content.Context;
import android.graphics.Rect;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow;

/* renamed from: android.support.v7.view.menu.ˑ  reason: contains not printable characters */
/* compiled from: MenuPopup */
abstract class C0516 implements C0518, C0524, AdapterView.OnItemClickListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Rect f1793;

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m2987(int i);

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2988(Context context, C0504 r2) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m2990(C0504 r1);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m2991(View view);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m2992(PopupWindow.OnDismissListener onDismissListener);

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2993(C0504 r1, C0508 r2) {
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m2994() {
        return 0;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract void m2995(int i);

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract void m2996(boolean z);

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m2997(C0504 r1, C0508 r2) {
        return false;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract void m2998(int i);

    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract void m2999(boolean z);

    /* access modifiers changed from: protected */
    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m3000() {
        return true;
    }

    C0516() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2989(Rect rect) {
        this.f1793 = rect;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public Rect m3001() {
        return this.f1793;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        ListAdapter listAdapter = (ListAdapter) adapterView.getAdapter();
        m2985(listAdapter).f1714.m2902((MenuItem) listAdapter.getItem(i), this, m3000() ? 0 : 4);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    protected static int m2984(ListAdapter listAdapter, ViewGroup viewGroup, Context context, int i) {
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
        int count = listAdapter.getCount();
        ViewGroup viewGroup2 = viewGroup;
        View view = null;
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < count; i4++) {
            int itemViewType = listAdapter.getItemViewType(i4);
            if (itemViewType != i3) {
                view = null;
                i3 = itemViewType;
            }
            if (viewGroup2 == null) {
                viewGroup2 = new FrameLayout(context);
            }
            view = listAdapter.getView(i4, view, viewGroup2);
            view.measure(makeMeasureSpec, makeMeasureSpec2);
            int measuredWidth = view.getMeasuredWidth();
            if (measuredWidth >= i) {
                return i;
            }
            if (measuredWidth > i2) {
                i2 = measuredWidth;
            }
        }
        return i2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    protected static C0503 m2985(ListAdapter listAdapter) {
        if (listAdapter instanceof HeaderViewListAdapter) {
            return (C0503) ((HeaderViewListAdapter) listAdapter).getWrappedAdapter();
        }
        return (C0503) listAdapter;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    protected static boolean m2986(C0504 r5) {
        int size = r5.size();
        for (int i = 0; i < size; i++) {
            MenuItem item = r5.getItem(i);
            if (item.isVisible() && item.getIcon() != null) {
                return true;
            }
        }
        return false;
    }
}
