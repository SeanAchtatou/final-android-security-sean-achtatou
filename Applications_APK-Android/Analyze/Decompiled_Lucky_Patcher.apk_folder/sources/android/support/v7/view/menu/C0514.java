package android.support.v7.view.menu;

import android.content.Context;
import android.support.v4.ʽ.ʻ.C0315;
import android.support.v4.ˉ.C0393;
import android.support.v7.view.menu.C0509;
import android.view.ActionProvider;
import android.view.MenuItem;
import android.view.View;

/* renamed from: android.support.v7.view.menu.ˏ  reason: contains not printable characters */
/* compiled from: MenuItemWrapperJB */
class C0514 extends C0509 {
    C0514(Context context, C0315 r2) {
        super(context, r2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0509.C0510 m2979(ActionProvider actionProvider) {
        return new C0515(this.f1661, actionProvider);
    }

    /* renamed from: android.support.v7.view.menu.ˏ$ʻ  reason: contains not printable characters */
    /* compiled from: MenuItemWrapperJB */
    class C0515 extends C0509.C0510 implements ActionProvider.VisibilityListener {

        /* renamed from: ʽ  reason: contains not printable characters */
        C0393.C0395 f1791;

        public C0515(Context context, ActionProvider actionProvider) {
            super(context, actionProvider);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public View m2980(MenuItem menuItem) {
            return this.f1786.onCreateActionView(menuItem);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m2982() {
            return this.f1786.overridesItemVisibility();
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m2983() {
            return this.f1786.isVisible();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2981(C0393.C0395 r2) {
            this.f1791 = r2;
            this.f1786.setVisibilityListener(r2 != null ? this : null);
        }

        public void onActionProviderVisibilityChanged(boolean z) {
            C0393.C0395 r0 = this.f1791;
            if (r0 != null) {
                r0.m2154(z);
            }
        }
    }
}
