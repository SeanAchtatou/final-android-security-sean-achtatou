package android.support.v7.view.menu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.ʽ.ʻ.C0314;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

/* renamed from: android.support.v7.view.menu.ᴵ  reason: contains not printable characters */
/* compiled from: MenuWrapperICS */
class C0523 extends C0497<C0314> implements Menu {
    C0523(Context context, C0314 r2) {
        super(context, r2);
    }

    public MenuItem add(CharSequence charSequence) {
        return m2815(((C0314) this.f1664).add(charSequence));
    }

    public MenuItem add(int i) {
        return m2815(((C0314) this.f1664).add(i));
    }

    public MenuItem add(int i, int i2, int i3, CharSequence charSequence) {
        return m2815(((C0314) this.f1664).add(i, i2, i3, charSequence));
    }

    public MenuItem add(int i, int i2, int i3, int i4) {
        return m2815(((C0314) this.f1664).add(i, i2, i3, i4));
    }

    public SubMenu addSubMenu(CharSequence charSequence) {
        return m2816(((C0314) this.f1664).addSubMenu(charSequence));
    }

    public SubMenu addSubMenu(int i) {
        return m2816(((C0314) this.f1664).addSubMenu(i));
    }

    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        return m2816(((C0314) this.f1664).addSubMenu(i, i2, i3, charSequence));
    }

    public SubMenu addSubMenu(int i, int i2, int i3, int i4) {
        return m2816(((C0314) this.f1664).addSubMenu(i, i2, i3, i4));
    }

    public int addIntentOptions(int i, int i2, int i3, ComponentName componentName, Intent[] intentArr, Intent intent, int i4, MenuItem[] menuItemArr) {
        MenuItem[] menuItemArr2 = menuItemArr;
        MenuItem[] menuItemArr3 = menuItemArr2 != null ? new MenuItem[menuItemArr2.length] : null;
        int addIntentOptions = ((C0314) this.f1664).addIntentOptions(i, i2, i3, componentName, intentArr, intent, i4, menuItemArr3);
        if (menuItemArr3 != null) {
            int length = menuItemArr3.length;
            for (int i5 = 0; i5 < length; i5++) {
                menuItemArr2[i5] = m2815(menuItemArr3[i5]);
            }
        }
        return addIntentOptions;
    }

    public void removeItem(int i) {
        m2819(i);
        ((C0314) this.f1664).removeItem(i);
    }

    public void removeGroup(int i) {
        m2818(i);
        ((C0314) this.f1664).removeGroup(i);
    }

    public void clear() {
        m2817();
        ((C0314) this.f1664).clear();
    }

    public void setGroupCheckable(int i, boolean z, boolean z2) {
        ((C0314) this.f1664).setGroupCheckable(i, z, z2);
    }

    public void setGroupVisible(int i, boolean z) {
        ((C0314) this.f1664).setGroupVisible(i, z);
    }

    public void setGroupEnabled(int i, boolean z) {
        ((C0314) this.f1664).setGroupEnabled(i, z);
    }

    public boolean hasVisibleItems() {
        return ((C0314) this.f1664).hasVisibleItems();
    }

    public MenuItem findItem(int i) {
        return m2815(((C0314) this.f1664).findItem(i));
    }

    public int size() {
        return ((C0314) this.f1664).size();
    }

    public MenuItem getItem(int i) {
        return m2815(((C0314) this.f1664).getItem(i));
    }

    public void close() {
        ((C0314) this.f1664).close();
    }

    public boolean performShortcut(int i, KeyEvent keyEvent, int i2) {
        return ((C0314) this.f1664).performShortcut(i, keyEvent, i2);
    }

    public boolean isShortcutKey(int i, KeyEvent keyEvent) {
        return ((C0314) this.f1664).isShortcutKey(i, keyEvent);
    }

    public boolean performIdentifierAction(int i, int i2) {
        return ((C0314) this.f1664).performIdentifierAction(i, i2);
    }

    public void setQwertyMode(boolean z) {
        ((C0314) this.f1664).setQwertyMode(z);
    }
}
