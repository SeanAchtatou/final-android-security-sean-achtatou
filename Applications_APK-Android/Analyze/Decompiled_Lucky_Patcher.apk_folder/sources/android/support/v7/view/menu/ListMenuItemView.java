package android.support.v7.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.ˉ.C0414;
import android.support.v7.view.menu.C0520;
import android.support.v7.widget.C0592;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

public class ListMenuItemView extends LinearLayout implements C0520.C0521 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private C0508 f1614;

    /* renamed from: ʼ  reason: contains not printable characters */
    private ImageView f1615;

    /* renamed from: ʽ  reason: contains not printable characters */
    private RadioButton f1616;

    /* renamed from: ʾ  reason: contains not printable characters */
    private TextView f1617;

    /* renamed from: ʿ  reason: contains not printable characters */
    private CheckBox f1618;

    /* renamed from: ˆ  reason: contains not printable characters */
    private TextView f1619;

    /* renamed from: ˈ  reason: contains not printable characters */
    private ImageView f1620;

    /* renamed from: ˉ  reason: contains not printable characters */
    private Drawable f1621;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f1622;

    /* renamed from: ˋ  reason: contains not printable characters */
    private Context f1623;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f1624;

    /* renamed from: ˏ  reason: contains not printable characters */
    private Drawable f1625;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f1626;

    /* renamed from: י  reason: contains not printable characters */
    private LayoutInflater f1627;

    /* renamed from: ـ  reason: contains not printable characters */
    private boolean f1628;

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m2788() {
        return false;
    }

    public ListMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0727.C0728.listMenuViewStyle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ʻˏ.ʻ(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.widget.ʻˏ.ʻ(int, float):float
      android.support.v7.widget.ʻˏ.ʻ(int, int):int
      android.support.v7.widget.ʻˏ.ʻ(int, boolean):boolean */
    public ListMenuItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        C0592 r5 = C0592.m3740(getContext(), attributeSet, C0727.C0737.MenuView, i, 0);
        this.f1621 = r5.m3744(C0727.C0737.MenuView_android_itemBackground);
        this.f1622 = r5.m3757(C0727.C0737.MenuView_android_itemTextAppearance, -1);
        this.f1624 = r5.m3746(C0727.C0737.MenuView_preserveIconSpacing, false);
        this.f1623 = context;
        this.f1625 = r5.m3744(C0727.C0737.MenuView_subMenuArrow);
        r5.m3745();
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        C0414.m2223(this, this.f1621);
        this.f1617 = (TextView) findViewById(C0727.C0733.title);
        int i = this.f1622;
        if (i != -1) {
            this.f1617.setTextAppearance(this.f1623, i);
        }
        this.f1619 = (TextView) findViewById(C0727.C0733.shortcut);
        this.f1620 = (ImageView) findViewById(C0727.C0733.submenuarrow);
        ImageView imageView = this.f1620;
        if (imageView != null) {
            imageView.setImageDrawable(this.f1625);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2786(C0508 r2, int i) {
        this.f1614 = r2;
        this.f1626 = i;
        setVisibility(r2.isVisible() ? 0 : 8);
        setTitle(r2.m2946((C0520.C0521) this));
        setCheckable(r2.isCheckable());
        m2787(r2.m2960(), r2.m2956());
        setIcon(r2.getIcon());
        setEnabled(r2.isEnabled());
        setSubMenuArrowVisible(r2.hasSubMenu());
        setContentDescription(r2.getContentDescription());
    }

    public void setForceShowIcon(boolean z) {
        this.f1628 = z;
        this.f1624 = z;
    }

    public void setTitle(CharSequence charSequence) {
        if (charSequence != null) {
            this.f1617.setText(charSequence);
            if (this.f1617.getVisibility() != 0) {
                this.f1617.setVisibility(0);
            }
        } else if (this.f1617.getVisibility() != 8) {
            this.f1617.setVisibility(8);
        }
    }

    public C0508 getItemData() {
        return this.f1614;
    }

    public void setCheckable(boolean z) {
        CompoundButton compoundButton;
        CompoundButton compoundButton2;
        if (z || this.f1616 != null || this.f1618 != null) {
            if (this.f1614.m2961()) {
                if (this.f1616 == null) {
                    m2784();
                }
                compoundButton2 = this.f1616;
                compoundButton = this.f1618;
            } else {
                if (this.f1618 == null) {
                    m2785();
                }
                compoundButton2 = this.f1618;
                compoundButton = this.f1616;
            }
            if (z) {
                compoundButton2.setChecked(this.f1614.isChecked());
                int i = z ? 0 : 8;
                if (compoundButton2.getVisibility() != i) {
                    compoundButton2.setVisibility(i);
                }
                if (compoundButton != null && compoundButton.getVisibility() != 8) {
                    compoundButton.setVisibility(8);
                    return;
                }
                return;
            }
            CheckBox checkBox = this.f1618;
            if (checkBox != null) {
                checkBox.setVisibility(8);
            }
            RadioButton radioButton = this.f1616;
            if (radioButton != null) {
                radioButton.setVisibility(8);
            }
        }
    }

    public void setChecked(boolean z) {
        CompoundButton compoundButton;
        if (this.f1614.m2961()) {
            if (this.f1616 == null) {
                m2784();
            }
            compoundButton = this.f1616;
        } else {
            if (this.f1618 == null) {
                m2785();
            }
            compoundButton = this.f1618;
        }
        compoundButton.setChecked(z);
    }

    private void setSubMenuArrowVisible(boolean z) {
        ImageView imageView = this.f1620;
        if (imageView != null) {
            imageView.setVisibility(z ? 0 : 8);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2787(boolean z, char c) {
        int i = (!z || !this.f1614.m2960()) ? 8 : 0;
        if (i == 0) {
            this.f1619.setText(this.f1614.m2958());
        }
        if (this.f1619.getVisibility() != i) {
            this.f1619.setVisibility(i);
        }
    }

    public void setIcon(Drawable drawable) {
        boolean z = this.f1614.m2963() || this.f1628;
        if (!z && !this.f1624) {
            return;
        }
        if (this.f1615 != null || drawable != null || this.f1624) {
            if (this.f1615 == null) {
                m2783();
            }
            if (drawable != null || this.f1624) {
                ImageView imageView = this.f1615;
                if (!z) {
                    drawable = null;
                }
                imageView.setImageDrawable(drawable);
                if (this.f1615.getVisibility() != 0) {
                    this.f1615.setVisibility(0);
                    return;
                }
                return;
            }
            this.f1615.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (this.f1615 != null && this.f1624) {
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) this.f1615.getLayoutParams();
            if (layoutParams.height > 0 && layoutParams2.width <= 0) {
                layoutParams2.width = layoutParams.height;
            }
        }
        super.onMeasure(i, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.view.menu.ListMenuItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m2783() {
        this.f1615 = (ImageView) getInflater().inflate(C0727.C0734.abc_list_menu_item_icon, (ViewGroup) this, false);
        addView(this.f1615, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.view.menu.ListMenuItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ʽ  reason: contains not printable characters */
    private void m2784() {
        this.f1616 = (RadioButton) getInflater().inflate(C0727.C0734.abc_list_menu_item_radio, (ViewGroup) this, false);
        addView(this.f1616);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.view.menu.ListMenuItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ʾ  reason: contains not printable characters */
    private void m2785() {
        this.f1618 = (CheckBox) getInflater().inflate(C0727.C0734.abc_list_menu_item_checkbox, (ViewGroup) this, false);
        addView(this.f1618);
    }

    private LayoutInflater getInflater() {
        if (this.f1627 == null) {
            this.f1627 = LayoutInflater.from(getContext());
        }
        return this.f1627;
    }
}
