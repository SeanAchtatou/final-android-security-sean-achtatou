package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.support.v7.view.menu.C0518;
import android.support.v7.widget.C0679;
import android.support.v7.ʻ.C0727;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

/* renamed from: android.support.v7.view.menu.ᵔ  reason: contains not printable characters */
/* compiled from: StandardMenuPopup */
final class C0525 extends C0516 implements C0518, View.OnKeyListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    final C0679 f1807;

    /* renamed from: ʼ  reason: contains not printable characters */
    View f1808;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Context f1809;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final C0504 f1810;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final C0503 f1811;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final boolean f1812;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final int f1813;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final int f1814;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final int f1815;
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public final ViewTreeObserver.OnGlobalLayoutListener f1816 = new ViewTreeObserver.OnGlobalLayoutListener() {
        public void onGlobalLayout() {
            if (C0525.this.m3060() && !C0525.this.f1807.m4210()) {
                View view = C0525.this.f1808;
                if (view == null || !view.isShown()) {
                    C0525.this.m3059();
                } else {
                    C0525.this.f1807.m4211();
                }
            }
        }
    };

    /* renamed from: ˎ  reason: contains not printable characters */
    private final View.OnAttachStateChangeListener f1817 = new View.OnAttachStateChangeListener() {
        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            if (C0525.this.f1821 != null) {
                if (!C0525.this.f1821.isAlive()) {
                    ViewTreeObserver unused = C0525.this.f1821 = view.getViewTreeObserver();
                }
                C0525.this.f1821.removeGlobalOnLayoutListener(C0525.this.f1816);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    };

    /* renamed from: ˏ  reason: contains not printable characters */
    private PopupWindow.OnDismissListener f1818;

    /* renamed from: ˑ  reason: contains not printable characters */
    private View f1819;

    /* renamed from: י  reason: contains not printable characters */
    private C0518.C0519 f1820;
    /* access modifiers changed from: private */

    /* renamed from: ـ  reason: contains not printable characters */
    public ViewTreeObserver f1821;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f1822;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f1823;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private int f1824;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private int f1825 = 0;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f1826;

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3044(Parcelable parcelable) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3045(C0504 r1) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3051() {
        return false;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Parcelable m3055() {
        return null;
    }

    public C0525(Context context, C0504 r4, View view, int i, int i2, boolean z) {
        this.f1809 = context;
        this.f1810 = r4;
        this.f1812 = z;
        this.f1811 = new C0503(r4, LayoutInflater.from(context), this.f1812);
        this.f1814 = i;
        this.f1815 = i2;
        Resources resources = context.getResources();
        this.f1813 = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(C0727.C0731.abc_config_prefDialogWidth));
        this.f1819 = view;
        this.f1807 = new C0679(this.f1809, null, this.f1814, this.f1815);
        r4.m2896(this, context);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3054(boolean z) {
        this.f1811.m2872(z);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3043(int i) {
        this.f1825 = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.ListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean m3042() {
        View view;
        if (m3060()) {
            return true;
        }
        if (this.f1822 || (view = this.f1819) == null) {
            return false;
        }
        this.f1808 = view;
        this.f1807.m4204((PopupWindow.OnDismissListener) this);
        this.f1807.m4202((AdapterView.OnItemClickListener) this);
        this.f1807.m4205(true);
        View view2 = this.f1808;
        boolean z = this.f1821 == null;
        this.f1821 = view2.getViewTreeObserver();
        if (z) {
            this.f1821.addOnGlobalLayoutListener(this.f1816);
        }
        view2.addOnAttachStateChangeListener(this.f1817);
        this.f1807.m4207(view2);
        this.f1807.m4214(this.f1825);
        if (!this.f1823) {
            this.f1824 = m2984(this.f1811, null, this.f1809, this.f1813);
            this.f1823 = true;
        }
        this.f1807.m4218(this.f1824);
        this.f1807.m4220(2);
        this.f1807.m4200(m3001());
        this.f1807.m4211();
        ListView r0 = this.f1807.m4217();
        r0.setOnKeyListener(this);
        if (this.f1826 && this.f1810.m2927() != null) {
            FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(this.f1809).inflate(C0727.C0734.abc_popup_menu_header_item_layout, (ViewGroup) r0, false);
            TextView textView = (TextView) frameLayout.findViewById(16908310);
            if (textView != null) {
                textView.setText(this.f1810.m2927());
            }
            frameLayout.setEnabled(false);
            r0.addHeaderView(frameLayout, null, false);
        }
        this.f1807.m4203((ListAdapter) this.f1811);
        this.f1807.m4211();
        return true;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m3058() {
        if (!m3042()) {
            throw new IllegalStateException("StandardMenuPopup cannot be used without an anchor");
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m3059() {
        if (m3060()) {
            this.f1807.m4213();
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m3060() {
        return !this.f1822 && this.f1807.m4216();
    }

    public void onDismiss() {
        this.f1822 = true;
        this.f1810.close();
        ViewTreeObserver viewTreeObserver = this.f1821;
        if (viewTreeObserver != null) {
            if (!viewTreeObserver.isAlive()) {
                this.f1821 = this.f1808.getViewTreeObserver();
            }
            this.f1821.removeGlobalOnLayoutListener(this.f1816);
            this.f1821 = null;
        }
        this.f1808.removeOnAttachStateChangeListener(this.f1817);
        PopupWindow.OnDismissListener onDismissListener = this.f1818;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3050(boolean z) {
        this.f1823 = false;
        C0503 r1 = this.f1811;
        if (r1 != null) {
            r1.notifyDataSetChanged();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3047(C0518.C0519 r1) {
        this.f1820 = r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3052(C0526 r10) {
        if (r10.hasVisibleItems()) {
            C0517 r2 = new C0517(this.f1809, r10, this.f1808, this.f1812, this.f1814, this.f1815);
            r2.m3006(this.f1820);
            r2.m3009(C0516.m2986(r10));
            r2.m3005(this.f1825);
            r2.m3008(this.f1818);
            this.f1818 = null;
            this.f1810.m2907(false);
            if (r2.m3010(this.f1807.m4223(), this.f1807.m4224())) {
                C0518.C0519 r0 = this.f1820;
                if (r0 == null) {
                    return true;
                }
                r0.m3028(r10);
                return true;
            }
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3046(C0504 r2, boolean z) {
        if (r2 == this.f1810) {
            m3059();
            C0518.C0519 r0 = this.f1820;
            if (r0 != null) {
                r0.m3027(r2, z);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3048(View view) {
        this.f1819 = view;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i != 82) {
            return false;
        }
        m3059();
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3049(PopupWindow.OnDismissListener onDismissListener) {
        this.f1818 = onDismissListener;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public ListView m3061() {
        return this.f1807.m4217();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3053(int i) {
        this.f1807.m4209(i);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3056(int i) {
        this.f1807.m4212(i);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3057(boolean z) {
        this.f1826 = z;
    }
}
