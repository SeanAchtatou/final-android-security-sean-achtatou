package android.support.v7.view;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.support.v7.ʻ.C0727;
import android.view.LayoutInflater;

/* renamed from: android.support.v7.view.ʾ  reason: contains not printable characters */
/* compiled from: ContextThemeWrapper */
public class C0532 extends ContextWrapper {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f1834;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Resources.Theme f1835;

    /* renamed from: ʽ  reason: contains not printable characters */
    private LayoutInflater f1836;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Configuration f1837;

    /* renamed from: ʿ  reason: contains not printable characters */
    private Resources f1838;

    public C0532() {
        super(null);
    }

    public C0532(Context context, int i) {
        super(context);
        this.f1834 = i;
    }

    public C0532(Context context, Resources.Theme theme) {
        super(context);
        this.f1835 = theme;
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }

    public Resources getResources() {
        return m3103();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private Resources m3103() {
        if (this.f1838 == null) {
            if (this.f1837 == null) {
                this.f1838 = super.getResources();
            } else if (Build.VERSION.SDK_INT >= 17) {
                this.f1838 = createConfigurationContext(this.f1837).getResources();
            }
        }
        return this.f1838;
    }

    public void setTheme(int i) {
        if (this.f1834 != i) {
            this.f1834 = i;
            m3104();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m3105() {
        return this.f1834;
    }

    public Resources.Theme getTheme() {
        Resources.Theme theme = this.f1835;
        if (theme != null) {
            return theme;
        }
        if (this.f1834 == 0) {
            this.f1834 = C0727.C0736.Theme_AppCompat_Light;
        }
        m3104();
        return this.f1835;
    }

    public Object getSystemService(String str) {
        if (!"layout_inflater".equals(str)) {
            return getBaseContext().getSystemService(str);
        }
        if (this.f1836 == null) {
            this.f1836 = LayoutInflater.from(getBaseContext()).cloneInContext(this);
        }
        return this.f1836;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3106(Resources.Theme theme, int i, boolean z) {
        theme.applyStyle(i, true);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m3104() {
        boolean z = this.f1835 == null;
        if (z) {
            this.f1835 = getResources().newTheme();
            Resources.Theme theme = getBaseContext().getTheme();
            if (theme != null) {
                this.f1835.setTo(theme);
            }
        }
        m3106(this.f1835, this.f1834, z);
    }

    public AssetManager getAssets() {
        return getResources().getAssets();
    }
}
