package android.support.v7.view.menu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.C0101;
import android.support.v4.ʽ.ʻ.C0314;
import android.support.v4.ˉ.C0393;
import android.support.v7.ʻ.C0727;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import net.lingala.zip4j.util.InternalZipConstants;

/* renamed from: android.support.v7.view.menu.ˉ  reason: contains not printable characters */
/* compiled from: MenuBuilder */
public class C0504 implements C0314 {

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final int[] f1719 = {1, 4, 5, 3, 2, 0};

    /* renamed from: ʻ  reason: contains not printable characters */
    CharSequence f1720;

    /* renamed from: ʼ  reason: contains not printable characters */
    Drawable f1721;

    /* renamed from: ʽ  reason: contains not printable characters */
    View f1722;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final Context f1723;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final Resources f1724;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f1725;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f1726;

    /* renamed from: ˊ  reason: contains not printable characters */
    private C0505 f1727;

    /* renamed from: ˋ  reason: contains not printable characters */
    private ArrayList<C0508> f1728;

    /* renamed from: ˎ  reason: contains not printable characters */
    private ArrayList<C0508> f1729;

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f1730;

    /* renamed from: ˑ  reason: contains not printable characters */
    private ArrayList<C0508> f1731;

    /* renamed from: י  reason: contains not printable characters */
    private ArrayList<C0508> f1732;

    /* renamed from: ـ  reason: contains not printable characters */
    private boolean f1733;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f1734 = 0;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private ContextMenu.ContextMenuInfo f1735;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private boolean f1736 = false;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f1737 = false;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f1738 = false;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private boolean f1739 = false;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private boolean f1740 = false;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private ArrayList<C0508> f1741 = new ArrayList<>();

    /* renamed from: ﹶ  reason: contains not printable characters */
    private CopyOnWriteArrayList<WeakReference<C0518>> f1742 = new CopyOnWriteArrayList<>();

    /* renamed from: ﾞ  reason: contains not printable characters */
    private C0508 f1743;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private boolean f1744;

    /* renamed from: android.support.v7.view.menu.ˉ$ʻ  reason: contains not printable characters */
    /* compiled from: MenuBuilder */
    public interface C0505 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m2933(C0504 r1);

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m2934(C0504 r1, MenuItem menuItem);
    }

    /* renamed from: android.support.v7.view.menu.ˉ$ʼ  reason: contains not printable characters */
    /* compiled from: MenuBuilder */
    public interface C0506 {
        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m2935(C0508 r1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public String m2891() {
        return "android:menu:actionviewstates";
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public C0504 m2930() {
        return this;
    }

    public C0504(Context context) {
        this.f1723 = context;
        this.f1724 = context.getResources();
        this.f1728 = new ArrayList<>();
        this.f1729 = new ArrayList<>();
        this.f1730 = true;
        this.f1731 = new ArrayList<>();
        this.f1732 = new ArrayList<>();
        this.f1733 = true;
        m2881(true);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0504 m2885(int i) {
        this.f1734 = i;
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2895(C0518 r2) {
        m2896(r2, this.f1723);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2896(C0518 r3, Context context) {
        this.f1742.add(new WeakReference(r3));
        r3.m3016(context, this);
        this.f1733 = true;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2906(C0518 r4) {
        Iterator<WeakReference<C0518>> it = this.f1742.iterator();
        while (it.hasNext()) {
            WeakReference next = it.next();
            C0518 r2 = (C0518) next.get();
            if (r2 == null || r2 == r4) {
                this.f1742.remove(next);
            }
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m2879(boolean z) {
        if (!this.f1742.isEmpty()) {
            m2921();
            Iterator<WeakReference<C0518>> it = this.f1742.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                C0518 r2 = (C0518) next.get();
                if (r2 == null) {
                    this.f1742.remove(next);
                } else {
                    r2.m3020(z);
                }
            }
            m2922();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m2878(C0526 r4, C0518 r5) {
        boolean z = false;
        if (this.f1742.isEmpty()) {
            return false;
        }
        if (r5 != null) {
            z = r5.m3023(r4);
        }
        Iterator<WeakReference<C0518>> it = this.f1742.iterator();
        while (it.hasNext()) {
            WeakReference next = it.next();
            C0518 r2 = (C0518) next.get();
            if (r2 == null) {
                this.f1742.remove(next);
            } else if (!z) {
                z = r2.m3023(r4);
            }
        }
        return z;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m2880(Bundle bundle) {
        Parcelable r3;
        if (!this.f1742.isEmpty()) {
            SparseArray sparseArray = new SparseArray();
            Iterator<WeakReference<C0518>> it = this.f1742.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                C0518 r32 = (C0518) next.get();
                if (r32 == null) {
                    this.f1742.remove(next);
                } else {
                    int r2 = r32.m3024();
                    if (r2 > 0 && (r3 = r32.m3026()) != null) {
                        sparseArray.put(r2, r3);
                    }
                }
            }
            bundle.putSparseParcelableArray("android:menu:presenters", sparseArray);
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private void m2883(Bundle bundle) {
        Parcelable parcelable;
        SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:presenters");
        if (sparseParcelableArray != null && !this.f1742.isEmpty()) {
            Iterator<WeakReference<C0518>> it = this.f1742.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                C0518 r2 = (C0518) next.get();
                if (r2 == null) {
                    this.f1742.remove(next);
                } else {
                    int r1 = r2.m3024();
                    if (r1 > 0 && (parcelable = (Parcelable) sparseParcelableArray.get(r1)) != null) {
                        r2.m3017(parcelable);
                    }
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2892(Bundle bundle) {
        m2880(bundle);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2904(Bundle bundle) {
        m2883(bundle);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2910(Bundle bundle) {
        int size = size();
        SparseArray sparseArray = null;
        for (int i = 0; i < size; i++) {
            MenuItem item = getItem(i);
            View actionView = item.getActionView();
            if (!(actionView == null || actionView.getId() == -1)) {
                if (sparseArray == null) {
                    sparseArray = new SparseArray();
                }
                actionView.saveHierarchyState(sparseArray);
                if (item.isActionViewExpanded()) {
                    bundle.putInt("android:menu:expandedactionview", item.getItemId());
                }
            }
            if (item.hasSubMenu()) {
                ((C0526) item.getSubMenu()).m2910(bundle);
            }
        }
        if (sparseArray != null) {
            bundle.putSparseParcelableArray(m2891(), sparseArray);
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m2916(Bundle bundle) {
        MenuItem findItem;
        if (bundle != null) {
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray(m2891());
            int size = size();
            for (int i = 0; i < size; i++) {
                MenuItem item = getItem(i);
                View actionView = item.getActionView();
                if (!(actionView == null || actionView.getId() == -1)) {
                    actionView.restoreHierarchyState(sparseParcelableArray);
                }
                if (item.hasSubMenu()) {
                    ((C0526) item.getSubMenu()).m2916(bundle);
                }
            }
            int i2 = bundle.getInt("android:menu:expandedactionview");
            if (i2 > 0 && (findItem = findItem(i2)) != null) {
                findItem.expandActionView();
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2893(C0505 r1) {
        this.f1727 = r1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public MenuItem m2890(int i, int i2, int i3, CharSequence charSequence) {
        int r7 = m2882(i3);
        C0508 r9 = m2875(i, i2, i3, r7, charSequence, this.f1734);
        ContextMenu.ContextMenuInfo contextMenuInfo = this.f1735;
        if (contextMenuInfo != null) {
            r9.m2948(contextMenuInfo);
        }
        ArrayList<C0508> arrayList = this.f1728;
        arrayList.add(m2874(arrayList, r7), r9);
        m2899(true);
        return r9;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private C0508 m2875(int i, int i2, int i3, int i4, CharSequence charSequence, int i5) {
        return new C0508(this, i, i2, i3, i4, charSequence, i5);
    }

    public MenuItem add(CharSequence charSequence) {
        return m2890(0, 0, 0, charSequence);
    }

    public MenuItem add(int i) {
        return m2890(0, 0, 0, this.f1724.getString(i));
    }

    public MenuItem add(int i, int i2, int i3, CharSequence charSequence) {
        return m2890(i, i2, i3, charSequence);
    }

    public MenuItem add(int i, int i2, int i3, int i4) {
        return m2890(i, i2, i3, this.f1724.getString(i4));
    }

    public SubMenu addSubMenu(CharSequence charSequence) {
        return addSubMenu(0, 0, 0, charSequence);
    }

    public SubMenu addSubMenu(int i) {
        return addSubMenu(0, 0, 0, this.f1724.getString(i));
    }

    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        C0508 r1 = (C0508) m2890(i, i2, i3, charSequence);
        C0526 r2 = new C0526(this.f1723, this, r1);
        r1.m2947(r2);
        return r2;
    }

    public SubMenu addSubMenu(int i, int i2, int i3, int i4) {
        return addSubMenu(i, i2, i3, this.f1724.getString(i4));
    }

    public int addIntentOptions(int i, int i2, int i3, ComponentName componentName, Intent[] intentArr, Intent intent, int i4, MenuItem[] menuItemArr) {
        PackageManager packageManager = this.f1723.getPackageManager();
        List<ResolveInfo> queryIntentActivityOptions = packageManager.queryIntentActivityOptions(componentName, intentArr, intent, 0);
        int size = queryIntentActivityOptions != null ? queryIntentActivityOptions.size() : 0;
        if ((i4 & 1) == 0) {
            removeGroup(i);
        }
        for (int i5 = 0; i5 < size; i5++) {
            ResolveInfo resolveInfo = queryIntentActivityOptions.get(i5);
            Intent intent2 = new Intent(resolveInfo.specificIndex < 0 ? intent : intentArr[resolveInfo.specificIndex]);
            intent2.setComponent(new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name));
            MenuItem intent3 = add(i, i2, i3, resolveInfo.loadLabel(packageManager)).setIcon(resolveInfo.loadIcon(packageManager)).setIntent(intent2);
            if (menuItemArr != null && resolveInfo.specificIndex >= 0) {
                menuItemArr[resolveInfo.specificIndex] = intent3;
            }
        }
        return size;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.view.menu.ˉ.ʻ(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.view.menu.ˉ.ʻ(java.util.ArrayList<android.support.v7.view.menu.ˋ>, int):int
      android.support.v7.view.menu.ˉ.ʻ(android.support.v7.view.menu.ᵢ, android.support.v7.view.menu.ـ):boolean
      android.support.v7.view.menu.ˉ.ʻ(int, int):int
      android.support.v7.view.menu.ˉ.ʻ(int, android.view.KeyEvent):android.support.v7.view.menu.ˋ
      android.support.v7.view.menu.ˉ.ʻ(android.support.v7.view.menu.ـ, android.content.Context):void
      android.support.v7.view.menu.ˉ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.view.menu.ˉ.ʻ(android.view.MenuItem, int):boolean
      android.support.v7.view.menu.ˉ.ʻ(int, boolean):void */
    public void removeItem(int i) {
        m2877(m2903(i), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.view.menu.ˉ.ʻ(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.view.menu.ˉ.ʻ(java.util.ArrayList<android.support.v7.view.menu.ˋ>, int):int
      android.support.v7.view.menu.ˉ.ʻ(android.support.v7.view.menu.ᵢ, android.support.v7.view.menu.ـ):boolean
      android.support.v7.view.menu.ˉ.ʻ(int, int):int
      android.support.v7.view.menu.ˉ.ʻ(int, android.view.KeyEvent):android.support.v7.view.menu.ˋ
      android.support.v7.view.menu.ˉ.ʻ(android.support.v7.view.menu.ـ, android.content.Context):void
      android.support.v7.view.menu.ˉ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.view.menu.ˉ.ʻ(android.view.MenuItem, int):boolean
      android.support.v7.view.menu.ˉ.ʻ(int, boolean):void */
    public void removeGroup(int i) {
        int r0 = m2909(i);
        if (r0 >= 0) {
            int size = this.f1728.size() - r0;
            int i2 = 0;
            while (true) {
                int i3 = i2 + 1;
                if (i2 >= size || this.f1728.get(r0).getGroupId() != i) {
                    m2899(true);
                } else {
                    m2877(r0, false);
                    i2 = i3;
                }
            }
            m2899(true);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m2877(int i, boolean z) {
        if (i >= 0 && i < this.f1728.size()) {
            this.f1728.remove(i);
            if (z) {
                m2899(true);
            }
        }
    }

    public void clear() {
        C0508 r0 = this.f1743;
        if (r0 != null) {
            m2917(r0);
        }
        this.f1728.clear();
        m2899(true);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2897(MenuItem menuItem) {
        int groupId = menuItem.getGroupId();
        int size = this.f1728.size();
        m2921();
        for (int i = 0; i < size; i++) {
            C0508 r4 = this.f1728.get(i);
            if (r4.getGroupId() == groupId && r4.m2961() && r4.isCheckable()) {
                r4.m2952(r4 == menuItem);
            }
        }
        m2922();
    }

    public void setGroupCheckable(int i, boolean z, boolean z2) {
        int size = this.f1728.size();
        for (int i2 = 0; i2 < size; i2++) {
            C0508 r2 = this.f1728.get(i2);
            if (r2.getGroupId() == i) {
                r2.m2949(z2);
                r2.setCheckable(z);
            }
        }
    }

    public void setGroupVisible(int i, boolean z) {
        int size = this.f1728.size();
        boolean z2 = false;
        for (int i2 = 0; i2 < size; i2++) {
            C0508 r4 = this.f1728.get(i2);
            if (r4.getGroupId() == i && r4.m2955(z)) {
                z2 = true;
            }
        }
        if (z2) {
            m2899(true);
        }
    }

    public void setGroupEnabled(int i, boolean z) {
        int size = this.f1728.size();
        for (int i2 = 0; i2 < size; i2++) {
            C0508 r2 = this.f1728.get(i2);
            if (r2.getGroupId() == i) {
                r2.setEnabled(z);
            }
        }
    }

    public boolean hasVisibleItems() {
        if (this.f1744) {
            return true;
        }
        int size = size();
        for (int i = 0; i < size; i++) {
            if (this.f1728.get(i).isVisible()) {
                return true;
            }
        }
        return false;
    }

    public MenuItem findItem(int i) {
        MenuItem findItem;
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            C0508 r2 = this.f1728.get(i2);
            if (r2.getItemId() == i) {
                return r2;
            }
            if (r2.hasSubMenu() && (findItem = r2.getSubMenu().findItem(i)) != null) {
                return findItem;
            }
        }
        return null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m2903(int i) {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            if (this.f1728.get(i2).getItemId() == i) {
                return i2;
            }
        }
        return -1;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m2909(int i) {
        return m2884(i, 0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m2884(int i, int i2) {
        int size = size();
        if (i2 < 0) {
            i2 = 0;
        }
        while (i2 < size) {
            if (this.f1728.get(i2).getGroupId() == i) {
                return i2;
            }
            i2++;
        }
        return -1;
    }

    public int size() {
        return this.f1728.size();
    }

    public MenuItem getItem(int i) {
        return this.f1728.get(i);
    }

    public boolean isShortcutKey(int i, KeyEvent keyEvent) {
        return m2889(i, keyEvent) != null;
    }

    public void setQwertyMode(boolean z) {
        this.f1725 = z;
        m2899(false);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private static int m2882(int i) {
        int i2 = (-65536 & i) >> 16;
        if (i2 >= 0) {
            int[] iArr = f1719;
            if (i2 < iArr.length) {
                return (i & InternalZipConstants.MAX_ALLOWED_ZIP_COMMENT_LENGTH) | (iArr[i2] << 16);
            }
        }
        throw new IllegalArgumentException("order does not contain a valid category.");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m2908() {
        return this.f1725;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m2881(boolean z) {
        boolean z2 = true;
        if (!z || this.f1724.getConfiguration().keyboard == 1 || !this.f1724.getBoolean(C0727.C0729.abc_config_showMenuShortcutsWhenKeyboardPresent)) {
            z2 = false;
        }
        this.f1726 = z2;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m2912() {
        return this.f1726;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public Resources m2914() {
        return this.f1724;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public Context m2918() {
        return this.f1723;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2900(C0504 r2, MenuItem menuItem) {
        C0505 r0 = this.f1727;
        return r0 != null && r0.m2934(r2, menuItem);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m2920() {
        C0505 r0 = this.f1727;
        if (r0 != null) {
            r0.m2933(this);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static int m2874(ArrayList<C0508> arrayList, int i) {
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            if (arrayList.get(size).m2954() <= i) {
                return size + 1;
            }
        }
        return 0;
    }

    public boolean performShortcut(int i, KeyEvent keyEvent, int i2) {
        C0508 r1 = m2889(i, keyEvent);
        boolean r12 = r1 != null ? m2901(r1, i2) : false;
        if ((i2 & 2) != 0) {
            m2907(true);
        }
        return r12;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2898(List<C0508> list, int i, KeyEvent keyEvent) {
        boolean r0 = m2908();
        int modifiers = keyEvent.getModifiers();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        if (keyEvent.getKeyData(keyData) || i == 67) {
            int size = this.f1728.size();
            for (int i2 = 0; i2 < size; i2++) {
                C0508 r7 = this.f1728.get(i2);
                if (r7.hasSubMenu()) {
                    ((C0504) r7.getSubMenu()).m2898(list, i, keyEvent);
                }
                char alphabeticShortcut = r0 ? r7.getAlphabeticShortcut() : r7.getNumericShortcut();
                if (((modifiers & 69647) == ((r0 ? r7.getAlphabeticModifiers() : r7.getNumericModifiers()) & 69647)) && alphabeticShortcut != 0 && ((alphabeticShortcut == keyData.meta[0] || alphabeticShortcut == keyData.meta[2] || (r0 && alphabeticShortcut == 8 && i == 67)) && r7.isEnabled())) {
                    list.add(r7);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0508 m2889(int i, KeyEvent keyEvent) {
        char c;
        ArrayList<C0508> arrayList = this.f1741;
        arrayList.clear();
        m2898(arrayList, i, keyEvent);
        if (arrayList.isEmpty()) {
            return null;
        }
        int metaState = keyEvent.getMetaState();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        keyEvent.getKeyData(keyData);
        int size = arrayList.size();
        if (size == 1) {
            return arrayList.get(0);
        }
        boolean r4 = m2908();
        for (int i2 = 0; i2 < size; i2++) {
            C0508 r7 = arrayList.get(i2);
            if (r4) {
                c = r7.getAlphabeticShortcut();
            } else {
                c = r7.getNumericShortcut();
            }
            if ((c == keyData.meta[0] && (metaState & 2) == 0) || ((c == keyData.meta[2] && (metaState & 2) != 0) || (r4 && c == 8 && i == 67))) {
                return r7;
            }
        }
        return null;
    }

    public boolean performIdentifierAction(int i, int i2) {
        return m2901(findItem(i), i2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2901(MenuItem menuItem, int i) {
        return m2902(menuItem, (C0518) null, i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2902(MenuItem menuItem, C0518 r8, int i) {
        C0508 r7 = (C0508) menuItem;
        if (r7 == null || !r7.isEnabled()) {
            return false;
        }
        boolean r1 = r7.m2953();
        C0393 r2 = r7.m2945();
        boolean z = r2 != null && r2.m2151();
        if (r7.m2968()) {
            r1 |= r7.expandActionView();
            if (r1) {
                m2907(true);
            }
        } else if (r7.hasSubMenu() || z) {
            if ((i & 4) == 0) {
                m2907(false);
            }
            if (!r7.hasSubMenu()) {
                r7.m2947(new C0526(m2918(), this, r7));
            }
            C0526 r72 = (C0526) r7.getSubMenu();
            if (z) {
                r2.m2146(r72);
            }
            r1 |= m2878(r72, r8);
            if (!r1) {
                m2907(true);
            }
        } else if ((i & 1) == 0) {
            m2907(true);
        }
        return r1;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m2907(boolean z) {
        if (!this.f1740) {
            this.f1740 = true;
            Iterator<WeakReference<C0518>> it = this.f1742.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                C0518 r2 = (C0518) next.get();
                if (r2 == null) {
                    this.f1742.remove(next);
                } else {
                    r2.m3018(this, z);
                }
            }
            this.f1740 = false;
        }
    }

    public void close() {
        m2907(true);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2899(boolean z) {
        if (!this.f1736) {
            if (z) {
                this.f1730 = true;
                this.f1733 = true;
            }
            m2879(z);
            return;
        }
        this.f1737 = true;
        if (z) {
            this.f1738 = true;
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public void m2921() {
        if (!this.f1736) {
            this.f1736 = true;
            this.f1737 = false;
            this.f1738 = false;
        }
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public void m2922() {
        this.f1736 = false;
        if (this.f1737) {
            this.f1737 = false;
            m2899(this.f1738);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2894(C0508 r1) {
        this.f1730 = true;
        m2899(true);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2905(C0508 r1) {
        this.f1733 = true;
        m2899(true);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public ArrayList<C0508> m2923() {
        if (!this.f1730) {
            return this.f1729;
        }
        this.f1729.clear();
        int size = this.f1728.size();
        for (int i = 0; i < size; i++) {
            C0508 r3 = this.f1728.get(i);
            if (r3.isVisible()) {
                this.f1729.add(r3);
            }
        }
        this.f1730 = false;
        this.f1733 = true;
        return this.f1729;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public void m2924() {
        ArrayList<C0508> r0 = m2923();
        if (this.f1733) {
            Iterator<WeakReference<C0518>> it = this.f1742.iterator();
            boolean z = false;
            while (it.hasNext()) {
                WeakReference next = it.next();
                C0518 r5 = (C0518) next.get();
                if (r5 == null) {
                    this.f1742.remove(next);
                } else {
                    z |= r5.m3021();
                }
            }
            if (z) {
                this.f1731.clear();
                this.f1732.clear();
                int size = r0.size();
                for (int i = 0; i < size; i++) {
                    C0508 r4 = r0.get(i);
                    if (r4.m2964()) {
                        this.f1731.add(r4);
                    } else {
                        this.f1732.add(r4);
                    }
                }
            } else {
                this.f1731.clear();
                this.f1732.clear();
                this.f1732.addAll(m2923());
            }
            this.f1733 = false;
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public ArrayList<C0508> m2925() {
        m2924();
        return this.f1731;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public ArrayList<C0508> m2926() {
        m2924();
        return this.f1732;
    }

    public void clearHeader() {
        this.f1721 = null;
        this.f1720 = null;
        this.f1722 = null;
        m2899(false);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m2876(int i, CharSequence charSequence, int i2, Drawable drawable, View view) {
        Resources r0 = m2914();
        if (view != null) {
            this.f1722 = view;
            this.f1720 = null;
            this.f1721 = null;
        } else {
            if (i > 0) {
                this.f1720 = r0.getText(i);
            } else if (charSequence != null) {
                this.f1720 = charSequence;
            }
            if (i2 > 0) {
                this.f1721 = C0101.m539(m2918(), i2);
            } else if (drawable != null) {
                this.f1721 = drawable;
            }
            this.f1722 = null;
        }
        m2899(false);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0504 m2888(CharSequence charSequence) {
        m2876(0, charSequence, 0, null, null);
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʾ  reason: contains not printable characters */
    public C0504 m2915(int i) {
        m2876(i, null, 0, null, null);
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0504 m2886(Drawable drawable) {
        m2876(0, null, 0, drawable, null);
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʿ  reason: contains not printable characters */
    public C0504 m2919(int i) {
        m2876(0, null, i, null, null);
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0504 m2887(View view) {
        m2876(0, null, 0, null, view);
        return this;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public CharSequence m2927() {
        return this.f1720;
    }

    /* renamed from: י  reason: contains not printable characters */
    public Drawable m2928() {
        return this.f1721;
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public View m2929() {
        return this.f1722;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean m2931() {
        return this.f1739;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m2913(C0508 r5) {
        boolean z = false;
        if (this.f1742.isEmpty()) {
            return false;
        }
        m2921();
        Iterator<WeakReference<C0518>> it = this.f1742.iterator();
        while (it.hasNext()) {
            WeakReference next = it.next();
            C0518 r3 = (C0518) next.get();
            if (r3 == null) {
                this.f1742.remove(next);
            } else {
                z = r3.m3022(this, r5);
                if (z) {
                    break;
                }
            }
        }
        m2922();
        if (z) {
            this.f1743 = r5;
        }
        return z;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m2917(C0508 r5) {
        boolean z = false;
        if (!this.f1742.isEmpty() && this.f1743 == r5) {
            m2921();
            Iterator<WeakReference<C0518>> it = this.f1742.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                C0518 r3 = (C0518) next.get();
                if (r3 == null) {
                    this.f1742.remove(next);
                } else {
                    z = r3.m3025(this, r5);
                    if (z) {
                        break;
                    }
                }
            }
            m2922();
            if (z) {
                this.f1743 = null;
            }
        }
        return z;
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public C0508 m2932() {
        return this.f1743;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2911(boolean z) {
        this.f1744 = z;
    }
}
