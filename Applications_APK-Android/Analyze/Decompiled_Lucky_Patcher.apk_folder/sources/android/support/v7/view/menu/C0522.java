package android.support.v7.view.menu;

import android.content.Context;
import android.os.Build;
import android.support.v4.ʽ.ʻ.C0314;
import android.support.v4.ʽ.ʻ.C0315;
import android.support.v4.ʽ.ʻ.C0316;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

/* renamed from: android.support.v7.view.menu.ᐧ  reason: contains not printable characters */
/* compiled from: MenuWrapperFactory */
public final class C0522 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static Menu m3032(Context context, C0314 r2) {
        return new C0523(context, r2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static MenuItem m3033(Context context, C0315 r3) {
        if (Build.VERSION.SDK_INT >= 16) {
            return new C0514(context, r3);
        }
        return new C0509(context, r3);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static SubMenu m3034(Context context, C0316 r2) {
        return new C0527(context, r2);
    }
}
