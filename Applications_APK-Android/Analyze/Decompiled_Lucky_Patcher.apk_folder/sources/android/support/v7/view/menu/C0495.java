package android.support.v7.view.menu;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.C0101;
import android.support.v4.ʼ.ʻ.C0288;
import android.support.v4.ʽ.ʻ.C0315;
import android.support.v4.ˉ.C0393;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

/* renamed from: android.support.v7.view.menu.ʻ  reason: contains not printable characters */
/* compiled from: ActionMenuItem */
public class C0495 implements C0315 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int f1629;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f1630;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f1631;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final int f1632;

    /* renamed from: ʿ  reason: contains not printable characters */
    private CharSequence f1633;

    /* renamed from: ˆ  reason: contains not printable characters */
    private CharSequence f1634;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Intent f1635;

    /* renamed from: ˉ  reason: contains not printable characters */
    private char f1636;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f1637 = 4096;

    /* renamed from: ˋ  reason: contains not printable characters */
    private char f1638;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f1639 = 4096;

    /* renamed from: ˏ  reason: contains not printable characters */
    private Drawable f1640;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f1641 = 0;

    /* renamed from: י  reason: contains not printable characters */
    private Context f1642;

    /* renamed from: ـ  reason: contains not printable characters */
    private MenuItem.OnMenuItemClickListener f1643;

    /* renamed from: ٴ  reason: contains not printable characters */
    private CharSequence f1644;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private CharSequence f1645;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private ColorStateList f1646 = null;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private PorterDuff.Mode f1647 = null;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f1648 = false;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private boolean f1649 = false;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private int f1650 = 16;

    public boolean collapseActionView() {
        return false;
    }

    public boolean expandActionView() {
        return false;
    }

    public View getActionView() {
        return null;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return null;
    }

    public SubMenu getSubMenu() {
        return null;
    }

    public boolean hasSubMenu() {
        return false;
    }

    public boolean isActionViewExpanded() {
        return false;
    }

    public void setShowAsAction(int i) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0393 m2794() {
        return null;
    }

    public C0495(Context context, int i, int i2, int i3, int i4, CharSequence charSequence) {
        this.f1642 = context;
        this.f1629 = i2;
        this.f1630 = i;
        this.f1631 = i3;
        this.f1632 = i4;
        this.f1633 = charSequence;
    }

    public char getAlphabeticShortcut() {
        return this.f1638;
    }

    public int getAlphabeticModifiers() {
        return this.f1639;
    }

    public int getGroupId() {
        return this.f1630;
    }

    public Drawable getIcon() {
        return this.f1640;
    }

    public Intent getIntent() {
        return this.f1635;
    }

    public int getItemId() {
        return this.f1629;
    }

    public char getNumericShortcut() {
        return this.f1636;
    }

    public int getNumericModifiers() {
        return this.f1637;
    }

    public int getOrder() {
        return this.f1632;
    }

    public CharSequence getTitle() {
        return this.f1633;
    }

    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.f1634;
        return charSequence != null ? charSequence : this.f1633;
    }

    public boolean isCheckable() {
        return (this.f1650 & 1) != 0;
    }

    public boolean isChecked() {
        return (this.f1650 & 2) != 0;
    }

    public boolean isEnabled() {
        return (this.f1650 & 16) != 0;
    }

    public boolean isVisible() {
        return (this.f1650 & 8) == 0;
    }

    public MenuItem setAlphabeticShortcut(char c) {
        this.f1638 = Character.toLowerCase(c);
        return this;
    }

    public MenuItem setAlphabeticShortcut(char c, int i) {
        this.f1638 = Character.toLowerCase(c);
        this.f1639 = KeyEvent.normalizeMetaState(i);
        return this;
    }

    public MenuItem setCheckable(boolean z) {
        this.f1650 = z | (this.f1650 & true) ? 1 : 0;
        return this;
    }

    public MenuItem setChecked(boolean z) {
        this.f1650 = (z ? 2 : 0) | (this.f1650 & -3);
        return this;
    }

    public MenuItem setEnabled(boolean z) {
        this.f1650 = (z ? 16 : 0) | (this.f1650 & -17);
        return this;
    }

    public MenuItem setIcon(Drawable drawable) {
        this.f1640 = drawable;
        this.f1641 = 0;
        m2789();
        return this;
    }

    public MenuItem setIcon(int i) {
        this.f1641 = i;
        this.f1640 = C0101.m539(this.f1642, i);
        m2789();
        return this;
    }

    public MenuItem setIntent(Intent intent) {
        this.f1635 = intent;
        return this;
    }

    public MenuItem setNumericShortcut(char c) {
        this.f1636 = c;
        return this;
    }

    public MenuItem setNumericShortcut(char c, int i) {
        this.f1636 = c;
        this.f1637 = KeyEvent.normalizeMetaState(i);
        return this;
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.f1643 = onMenuItemClickListener;
        return this;
    }

    public MenuItem setShortcut(char c, char c2) {
        this.f1636 = c;
        this.f1638 = Character.toLowerCase(c2);
        return this;
    }

    public MenuItem setShortcut(char c, char c2, int i, int i2) {
        this.f1636 = c;
        this.f1637 = KeyEvent.normalizeMetaState(i);
        this.f1638 = Character.toLowerCase(c2);
        this.f1639 = KeyEvent.normalizeMetaState(i2);
        return this;
    }

    public MenuItem setTitle(CharSequence charSequence) {
        this.f1633 = charSequence;
        return this;
    }

    public MenuItem setTitle(int i) {
        this.f1633 = this.f1642.getResources().getString(i);
        return this;
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f1634 = charSequence;
        return this;
    }

    public MenuItem setVisible(boolean z) {
        int i = 8;
        int i2 = this.f1650 & 8;
        if (z) {
            i = 0;
        }
        this.f1650 = i2 | i;
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0315 setActionView(View view) {
        throw new UnsupportedOperationException();
    }

    public MenuItem setActionProvider(ActionProvider actionProvider) {
        throw new UnsupportedOperationException();
    }

    public ActionProvider getActionProvider() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0315 setActionView(int i) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0315 m2791(C0393 r1) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0315 setShowAsActionFlags(int i) {
        setShowAsAction(i);
        return this;
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0315 setContentDescription(CharSequence charSequence) {
        this.f1644 = charSequence;
        return this;
    }

    public CharSequence getContentDescription() {
        return this.f1644;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0315 setTooltipText(CharSequence charSequence) {
        this.f1645 = charSequence;
        return this;
    }

    public CharSequence getTooltipText() {
        return this.f1645;
    }

    public MenuItem setIconTintList(ColorStateList colorStateList) {
        this.f1646 = colorStateList;
        this.f1648 = true;
        m2789();
        return this;
    }

    public ColorStateList getIconTintList() {
        return this.f1646;
    }

    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        this.f1647 = mode;
        this.f1649 = true;
        m2789();
        return this;
    }

    public PorterDuff.Mode getIconTintMode() {
        return this.f1647;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m2789() {
        if (this.f1640 == null) {
            return;
        }
        if (this.f1648 || this.f1649) {
            this.f1640 = C0288.m1713(this.f1640);
            this.f1640 = this.f1640.mutate();
            if (this.f1648) {
                C0288.m1703(this.f1640, this.f1646);
            }
            if (this.f1649) {
                C0288.m1706(this.f1640, this.f1647);
            }
        }
    }
}
