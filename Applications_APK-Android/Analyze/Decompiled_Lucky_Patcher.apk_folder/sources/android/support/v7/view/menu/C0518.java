package android.support.v7.view.menu;

import android.content.Context;
import android.os.Parcelable;

/* renamed from: android.support.v7.view.menu.ـ  reason: contains not printable characters */
/* compiled from: MenuPresenter */
public interface C0518 {

    /* renamed from: android.support.v7.view.menu.ـ$ʻ  reason: contains not printable characters */
    /* compiled from: MenuPresenter */
    public interface C0519 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m3027(C0504 r1, boolean z);

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m3028(C0504 r1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    void m3016(Context context, C0504 r2);

    /* renamed from: ʻ  reason: contains not printable characters */
    void m3017(Parcelable parcelable);

    /* renamed from: ʻ  reason: contains not printable characters */
    void m3018(C0504 r1, boolean z);

    /* renamed from: ʻ  reason: contains not printable characters */
    void m3019(C0519 r1);

    /* renamed from: ʻ  reason: contains not printable characters */
    void m3020(boolean z);

    /* renamed from: ʻ  reason: contains not printable characters */
    boolean m3021();

    /* renamed from: ʻ  reason: contains not printable characters */
    boolean m3022(C0504 r1, C0508 r2);

    /* renamed from: ʻ  reason: contains not printable characters */
    boolean m3023(C0526 r1);

    /* renamed from: ʼ  reason: contains not printable characters */
    int m3024();

    /* renamed from: ʼ  reason: contains not printable characters */
    boolean m3025(C0504 r1, C0508 r2);

    /* renamed from: ʽ  reason: contains not printable characters */
    Parcelable m3026();
}
