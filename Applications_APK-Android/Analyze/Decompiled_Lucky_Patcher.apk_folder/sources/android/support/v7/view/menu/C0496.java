package android.support.v7.view.menu;

import android.content.Context;
import android.support.v7.view.menu.C0518;
import android.support.v7.view.menu.C0520;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

/* renamed from: android.support.v7.view.menu.ʼ  reason: contains not printable characters */
/* compiled from: BaseMenuPresenter */
public abstract class C0496 implements C0518 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected Context f1651;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected Context f1652;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected C0504 f1653;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected LayoutInflater f1654;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected LayoutInflater f1655;

    /* renamed from: ˆ  reason: contains not printable characters */
    protected C0520 f1656;

    /* renamed from: ˈ  reason: contains not printable characters */
    private C0518.C0519 f1657;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f1658;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f1659;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f1660;

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m2802(C0508 r1, C0520.C0521 r2);

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2806() {
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2807(int i, C0508 r2) {
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2808(C0504 r1, C0508 r2) {
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m2813(C0504 r1, C0508 r2) {
        return false;
    }

    public C0496(Context context, int i, int i2) {
        this.f1651 = context;
        this.f1654 = LayoutInflater.from(context);
        this.f1658 = i;
        this.f1659 = i2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2800(Context context, C0504 r2) {
        this.f1652 = context;
        this.f1655 = LayoutInflater.from(this.f1652);
        this.f1653 = r2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0520 m2797(ViewGroup viewGroup) {
        if (this.f1656 == null) {
            this.f1656 = (C0520) this.f1654.inflate(this.f1658, viewGroup, false);
            this.f1656.m3029(this.f1653);
            m2805(true);
        }
        return this.f1656;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2805(boolean z) {
        ViewGroup viewGroup = (ViewGroup) this.f1656;
        if (viewGroup != null) {
            C0504 r0 = this.f1653;
            int i = 0;
            if (r0 != null) {
                r0.m2924();
                ArrayList<C0508> r02 = this.f1653.m2923();
                int size = r02.size();
                int i2 = 0;
                for (int i3 = 0; i3 < size; i3++) {
                    C0508 r5 = r02.get(i3);
                    if (m2807(i2, r5)) {
                        View childAt = viewGroup.getChildAt(i2);
                        C0508 itemData = childAt instanceof C0520.C0521 ? ((C0520.C0521) childAt).getItemData() : null;
                        View r8 = m2798(r5, childAt, viewGroup);
                        if (r5 != itemData) {
                            r8.setPressed(false);
                            r8.jumpDrawablesToCurrentState();
                        }
                        if (r8 != childAt) {
                            m2804(r8, i2);
                        }
                        i2++;
                    }
                }
                i = i2;
            }
            while (i < viewGroup.getChildCount()) {
                if (!m2810(viewGroup, i)) {
                    i++;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2804(View view, int i) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.f1656).addView(view, i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2810(ViewGroup viewGroup, int i) {
        viewGroup.removeViewAt(i);
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2803(C0518.C0519 r1) {
        this.f1657 = r1;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public C0518.C0519 m2814() {
        return this.f1657;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ʼ  reason: contains not printable characters */
    public C0520.C0521 m2812(ViewGroup viewGroup) {
        return (C0520.C0521) this.f1654.inflate(this.f1659, viewGroup, false);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public View m2798(C0508 r2, View view, ViewGroup viewGroup) {
        C0520.C0521 r3;
        if (view instanceof C0520.C0521) {
            r3 = (C0520.C0521) view;
        } else {
            r3 = m2812(viewGroup);
        }
        m2802(r2, r3);
        return (View) r3;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2801(C0504 r2, boolean z) {
        C0518.C0519 r0 = this.f1657;
        if (r0 != null) {
            r0.m3027(r2, z);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2809(C0526 r2) {
        C0518.C0519 r0 = this.f1657;
        if (r0 != null) {
            return r0.m3028(r2);
        }
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m2811() {
        return this.f1660;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2799(int i) {
        this.f1660 = i;
    }
}
