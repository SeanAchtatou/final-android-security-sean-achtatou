package android.support.v7.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.view.menu.C0504;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

/* renamed from: android.support.v7.view.menu.ᵢ  reason: contains not printable characters */
/* compiled from: SubMenuBuilder */
public class C0526 extends C0504 implements SubMenu {

    /* renamed from: ʾ  reason: contains not printable characters */
    private C0504 f1829;

    /* renamed from: ʿ  reason: contains not printable characters */
    private C0508 f1830;

    public C0526(Context context, C0504 r2, C0508 r3) {
        super(context);
        this.f1829 = r2;
        this.f1830 = r3;
    }

    public void setQwertyMode(boolean z) {
        this.f1829.setQwertyMode(z);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m3065() {
        return this.f1829.m2908();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m3066() {
        return this.f1829.m2912();
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public Menu m3070() {
        return this.f1829;
    }

    public MenuItem getItem() {
        return this.f1830;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3063(C0504.C0505 r2) {
        this.f1829.m2893(r2);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public C0504 m3069() {
        return this.f1829.m2930();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3064(C0504 r2, MenuItem menuItem) {
        return super.m2900(r2, menuItem) || this.f1829.m2900(r2, menuItem);
    }

    public SubMenu setIcon(Drawable drawable) {
        this.f1830.setIcon(drawable);
        return this;
    }

    public SubMenu setIcon(int i) {
        this.f1830.setIcon(i);
        return this;
    }

    public SubMenu setHeaderIcon(Drawable drawable) {
        return (SubMenu) super.m2886(drawable);
    }

    public SubMenu setHeaderIcon(int i) {
        return (SubMenu) super.m2919(i);
    }

    public SubMenu setHeaderTitle(CharSequence charSequence) {
        return (SubMenu) super.m2888(charSequence);
    }

    public SubMenu setHeaderTitle(int i) {
        return (SubMenu) super.m2915(i);
    }

    public SubMenu setHeaderView(View view) {
        return (SubMenu) super.m2887(view);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m3067(C0508 r2) {
        return this.f1829.m2913(r2);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m3068(C0508 r2) {
        return this.f1829.m2917(r2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m3062() {
        C0508 r0 = this.f1830;
        int itemId = r0 != null ? r0.getItemId() : 0;
        if (itemId == 0) {
            return null;
        }
        return super.m2891() + ":" + itemId;
    }
}
