package android.support.v7.view.menu;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.ˉ.C0396;
import android.support.v4.ˉ.C0414;
import android.support.v7.view.menu.C0518;
import android.view.View;
import android.widget.PopupWindow;

/* renamed from: android.support.v7.view.menu.י  reason: contains not printable characters */
/* compiled from: MenuPopupHelper */
public class C0517 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Context f1794;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final C0504 f1795;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final boolean f1796;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final int f1797;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final int f1798;

    /* renamed from: ˆ  reason: contains not printable characters */
    private View f1799;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f1800;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f1801;

    /* renamed from: ˊ  reason: contains not printable characters */
    private C0518.C0519 f1802;

    /* renamed from: ˋ  reason: contains not printable characters */
    private C0516 f1803;

    /* renamed from: ˎ  reason: contains not printable characters */
    private PopupWindow.OnDismissListener f1804;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final PopupWindow.OnDismissListener f1805;

    public C0517(Context context, C0504 r9, View view, boolean z, int i) {
        this(context, r9, view, z, i, 0);
    }

    public C0517(Context context, C0504 r3, View view, boolean z, int i, int i2) {
        this.f1800 = 8388611;
        this.f1805 = new PopupWindow.OnDismissListener() {
            public void onDismiss() {
                C0517.this.m3014();
            }
        };
        this.f1794 = context;
        this.f1795 = r3;
        this.f1799 = view;
        this.f1796 = z;
        this.f1797 = i;
        this.f1798 = i2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3008(PopupWindow.OnDismissListener onDismissListener) {
        this.f1804 = onDismissListener;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3007(View view) {
        this.f1799 = view;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3009(boolean z) {
        this.f1801 = z;
        C0516 r0 = this.f1803;
        if (r0 != null) {
            r0.m2996(z);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3005(int i) {
        this.f1800 = i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3004() {
        if (!m3012()) {
            throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0516 m3011() {
        if (this.f1803 == null) {
            this.f1803 = m3003();
        }
        return this.f1803;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m3012() {
        if (m3015()) {
            return true;
        }
        if (this.f1799 == null) {
            return false;
        }
        m3002(0, 0, false, false);
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3010(int i, int i2) {
        if (m3015()) {
            return true;
        }
        if (this.f1799 == null) {
            return false;
        }
        m3002(i, i2, true, true);
        return true;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v7, types: [android.support.v7.view.menu.ˑ] */
    /* JADX WARN: Type inference failed for: r7v1, types: [android.support.v7.view.menu.ᵔ] */
    /* JADX WARN: Type inference failed for: r1v13, types: [android.support.v7.view.menu.ʿ] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: ˈ  reason: contains not printable characters */
    private android.support.v7.view.menu.C0516 m3003() {
        /*
            r14 = this;
            android.content.Context r0 = r14.f1794
            java.lang.String r1 = "window"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.view.WindowManager r0 = (android.view.WindowManager) r0
            android.view.Display r0 = r0.getDefaultDisplay()
            android.graphics.Point r1 = new android.graphics.Point
            r1.<init>()
            int r2 = android.os.Build.VERSION.SDK_INT
            r3 = 17
            if (r2 < r3) goto L_0x001d
            r0.getRealSize(r1)
            goto L_0x0020
        L_0x001d:
            r0.getSize(r1)
        L_0x0020:
            int r0 = r1.x
            int r1 = r1.y
            int r0 = java.lang.Math.min(r0, r1)
            android.content.Context r1 = r14.f1794
            android.content.res.Resources r1 = r1.getResources()
            int r2 = android.support.v7.ʻ.C0727.C0731.abc_cascading_menus_min_smallest_width
            int r1 = r1.getDimensionPixelSize(r2)
            if (r0 < r1) goto L_0x0038
            r0 = 1
            goto L_0x0039
        L_0x0038:
            r0 = 0
        L_0x0039:
            if (r0 == 0) goto L_0x004c
            android.support.v7.view.menu.ʿ r0 = new android.support.v7.view.menu.ʿ
            android.content.Context r2 = r14.f1794
            android.view.View r3 = r14.f1799
            int r4 = r14.f1797
            int r5 = r14.f1798
            boolean r6 = r14.f1796
            r1 = r0
            r1.<init>(r2, r3, r4, r5, r6)
            goto L_0x005e
        L_0x004c:
            android.support.v7.view.menu.ᵔ r0 = new android.support.v7.view.menu.ᵔ
            android.content.Context r8 = r14.f1794
            android.support.v7.view.menu.ˉ r9 = r14.f1795
            android.view.View r10 = r14.f1799
            int r11 = r14.f1797
            int r12 = r14.f1798
            boolean r13 = r14.f1796
            r7 = r0
            r7.<init>(r8, r9, r10, r11, r12, r13)
        L_0x005e:
            android.support.v7.view.menu.ˉ r1 = r14.f1795
            r0.m2990(r1)
            android.widget.PopupWindow$OnDismissListener r1 = r14.f1805
            r0.m2992(r1)
            android.view.View r1 = r14.f1799
            r0.m2991(r1)
            android.support.v7.view.menu.ـ$ʻ r1 = r14.f1802
            r0.m3019(r1)
            boolean r1 = r14.f1801
            r0.m2996(r1)
            int r1 = r14.f1800
            r0.m2987(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.view.menu.C0517.m3003():android.support.v7.view.menu.ˑ");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3002(int i, int i2, boolean z, boolean z2) {
        C0516 r0 = m3011();
        r0.m2999(z2);
        if (z) {
            if ((C0396.m2155(this.f1800, C0414.m2236(this.f1799)) & 7) == 5) {
                i += this.f1799.getWidth();
            }
            r0.m2995(i);
            r0.m2998(i2);
            int i3 = (int) ((this.f1794.getResources().getDisplayMetrics().density * 48.0f) / 2.0f);
            r0.m2989(new Rect(i - i3, i2 - i3, i + i3, i2 + i3));
        }
        r0.m3035();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m3013() {
        if (m3015()) {
            this.f1803.m3036();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m3014() {
        this.f1803 = null;
        PopupWindow.OnDismissListener onDismissListener = this.f1804;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m3015() {
        C0516 r0 = this.f1803;
        return r0 != null && r0.m3037();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3006(C0518.C0519 r2) {
        this.f1802 = r2;
        C0516 r0 = this.f1803;
        if (r0 != null) {
            r0.m3019(r2);
        }
    }
}
