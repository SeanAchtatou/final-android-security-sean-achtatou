package android.support.v7.view.menu;

import android.support.v7.view.menu.C0520;
import android.support.v7.ʻ.C0727;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

/* renamed from: android.support.v7.view.menu.ˈ  reason: contains not printable characters */
/* compiled from: MenuAdapter */
public class C0503 extends BaseAdapter {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final int f1713 = C0727.C0734.abc_popup_menu_item_layout;

    /* renamed from: ʼ  reason: contains not printable characters */
    C0504 f1714;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f1715 = -1;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f1716;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final boolean f1717;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final LayoutInflater f1718;

    public long getItemId(int i) {
        return (long) i;
    }

    public C0503(C0504 r2, LayoutInflater layoutInflater, boolean z) {
        this.f1717 = z;
        this.f1718 = layoutInflater;
        this.f1714 = r2;
        m2873();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2872(boolean z) {
        this.f1716 = z;
    }

    public int getCount() {
        ArrayList<C0508> r0 = this.f1717 ? this.f1714.m2926() : this.f1714.m2923();
        if (this.f1715 < 0) {
            return r0.size();
        }
        return r0.size() - 1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0504 m2870() {
        return this.f1714;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0508 getItem(int i) {
        ArrayList<C0508> r0 = this.f1717 ? this.f1714.m2926() : this.f1714.m2923();
        int i2 = this.f1715;
        if (i2 >= 0 && i >= i2) {
            i++;
        }
        return (C0508) r0.get(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.f1718.inflate(f1713, viewGroup, false);
        }
        C0520.C0521 r6 = (C0520.C0521) view;
        if (this.f1716) {
            ((ListMenuItemView) view).setForceShowIcon(true);
        }
        r6.m3030(getItem(i), 0);
        return view;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2873() {
        C0508 r0 = this.f1714.m2932();
        if (r0 != null) {
            ArrayList<C0508> r1 = this.f1714.m2926();
            int size = r1.size();
            for (int i = 0; i < size; i++) {
                if (r1.get(i) == r0) {
                    this.f1715 = i;
                    return;
                }
            }
        }
        this.f1715 = -1;
    }

    public void notifyDataSetChanged() {
        m2873();
        super.notifyDataSetChanged();
    }
}
