package android.support.v7.view.menu;

/* renamed from: android.support.v7.view.menu.ʾ  reason: contains not printable characters */
/* compiled from: BaseWrapper */
class C0498<T> {

    /* renamed from: ʼ  reason: contains not printable characters */
    final T f1664;

    C0498(T t) {
        if (t != null) {
            this.f1664 = t;
            return;
        }
        throw new IllegalArgumentException("Wrapped Object can not be null.");
    }
}
