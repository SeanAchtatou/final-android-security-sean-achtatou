package android.support.v7.view;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

/* renamed from: android.support.v7.view.ʼ  reason: contains not printable characters */
/* compiled from: ActionMode */
public abstract class C0529 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Object f1832;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f1833;

    /* renamed from: android.support.v7.view.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: ActionMode */
    public interface C0530 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m3097(C0529 r1);

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m3098(C0529 r1, Menu menu);

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m3099(C0529 r1, MenuItem menuItem);

        /* renamed from: ʼ  reason: contains not printable characters */
        boolean m3100(C0529 r1, Menu menu);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract MenuInflater m3080();

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m3081(int i);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m3082(View view);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m3083(CharSequence charSequence);

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract Menu m3086();

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract void m3087(int i);

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract void m3088(CharSequence charSequence);

    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract void m3089();

    /* renamed from: ʾ  reason: contains not printable characters */
    public abstract void m3090();

    /* renamed from: ˆ  reason: contains not printable characters */
    public abstract CharSequence m3091();

    /* renamed from: ˈ  reason: contains not printable characters */
    public abstract CharSequence m3092();

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m3093() {
        return false;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public abstract View m3094();

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3084(Object obj) {
        this.f1832 = obj;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public Object m3095() {
        return this.f1832;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3085(boolean z) {
        this.f1833 = z;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean m3096() {
        return this.f1833;
    }
}
