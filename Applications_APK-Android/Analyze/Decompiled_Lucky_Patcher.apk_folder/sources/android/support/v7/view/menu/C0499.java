package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.ˉ.C0396;
import android.support.v4.ˉ.C0414;
import android.support.v7.view.menu.C0518;
import android.support.v7.widget.C0668;
import android.support.v7.widget.C0679;
import android.support.v7.ʻ.C0727;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* renamed from: android.support.v7.view.menu.ʿ  reason: contains not printable characters */
/* compiled from: CascadingMenuPopup */
final class C0499 extends C0516 implements C0518, View.OnKeyListener, PopupWindow.OnDismissListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    final Handler f1665;

    /* renamed from: ʼ  reason: contains not printable characters */
    final List<C0500> f1666 = new ArrayList();

    /* renamed from: ʽ  reason: contains not printable characters */
    View f1667;

    /* renamed from: ʾ  reason: contains not printable characters */
    boolean f1668;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final Context f1669;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final int f1670;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final int f1671;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final int f1672;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final boolean f1673;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final List<C0504> f1674 = new LinkedList();
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public final ViewTreeObserver.OnGlobalLayoutListener f1675 = new ViewTreeObserver.OnGlobalLayoutListener() {
        public void onGlobalLayout() {
            if (C0499.this.m2847() && C0499.this.f1666.size() > 0 && !C0499.this.f1666.get(0).f1698.m4210()) {
                View view = C0499.this.f1667;
                if (view == null || !view.isShown()) {
                    C0499.this.m2846();
                    return;
                }
                for (C0500 r1 : C0499.this.f1666) {
                    r1.f1698.m4211();
                }
            }
        }
    };

    /* renamed from: ˏ  reason: contains not printable characters */
    private final View.OnAttachStateChangeListener f1676 = new View.OnAttachStateChangeListener() {
        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            if (C0499.this.f1689 != null) {
                if (!C0499.this.f1689.isAlive()) {
                    ViewTreeObserver unused = C0499.this.f1689 = view.getViewTreeObserver();
                }
                C0499.this.f1689.removeGlobalOnLayoutListener(C0499.this.f1675);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    };

    /* renamed from: ˑ  reason: contains not printable characters */
    private final C0668 f1677 = new C0668() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2850(C0504 r1, MenuItem menuItem) {
            C0499.this.f1665.removeCallbacksAndMessages(r1);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2851(final C0504 r6, final MenuItem menuItem) {
            final C0500 r1 = null;
            C0499.this.f1665.removeCallbacksAndMessages(null);
            int size = C0499.this.f1666.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    i = -1;
                    break;
                } else if (r6 == C0499.this.f1666.get(i).f1699) {
                    break;
                } else {
                    i++;
                }
            }
            if (i != -1) {
                int i2 = i + 1;
                if (i2 < C0499.this.f1666.size()) {
                    r1 = C0499.this.f1666.get(i2);
                }
                C0499.this.f1665.postAtTime(new Runnable() {
                    public void run() {
                        if (r1 != null) {
                            C0499.this.f1668 = true;
                            r1.f1699.m2907(false);
                            C0499.this.f1668 = false;
                        }
                        if (menuItem.isEnabled() && menuItem.hasSubMenu()) {
                            r6.m2901(menuItem, 4);
                        }
                    }
                }, r6, SystemClock.uptimeMillis() + 200);
            }
        }
    };

    /* renamed from: י  reason: contains not printable characters */
    private int f1678 = 0;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f1679 = 0;

    /* renamed from: ٴ  reason: contains not printable characters */
    private View f1680;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f1681;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private boolean f1682;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f1683;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private int f1684;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private int f1685;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private boolean f1686;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private boolean f1687;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private C0518.C0519 f1688;
    /* access modifiers changed from: private */

    /* renamed from: ﾞ  reason: contains not printable characters */
    public ViewTreeObserver f1689;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private PopupWindow.OnDismissListener f1690;

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2831(Parcelable parcelable) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2838() {
        return false;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Parcelable m2842() {
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m2849() {
        return false;
    }

    public C0499(Context context, View view, int i, int i2, boolean z) {
        this.f1669 = context;
        this.f1680 = view;
        this.f1671 = i;
        this.f1672 = i2;
        this.f1673 = z;
        this.f1686 = false;
        this.f1681 = m2829();
        Resources resources = context.getResources();
        this.f1670 = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(C0727.C0731.abc_config_prefDialogWidth));
        this.f1665 = new Handler();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2841(boolean z) {
        this.f1686 = z;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private C0679 m2828() {
        C0679 r0 = new C0679(this.f1669, null, this.f1671, this.f1672);
        r0.m4258(this.f1677);
        r0.m4202((AdapterView.OnItemClickListener) this);
        r0.m4204((PopupWindow.OnDismissListener) this);
        r0.m4207(this.f1680);
        r0.m4214(this.f1679);
        r0.m4205(true);
        r0.m4220(2);
        return r0;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m2845() {
        if (!m2847()) {
            for (C0504 r1 : this.f1674) {
                m2825(r1);
            }
            this.f1674.clear();
            this.f1667 = this.f1680;
            if (this.f1667 != null) {
                boolean z = this.f1689 == null;
                this.f1689 = this.f1667.getViewTreeObserver();
                if (z) {
                    this.f1689.addOnGlobalLayoutListener(this.f1675);
                }
                this.f1667.addOnAttachStateChangeListener(this.f1676);
            }
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m2846() {
        int size = this.f1666.size();
        if (size > 0) {
            C0500[] r1 = (C0500[]) this.f1666.toArray(new C0500[size]);
            for (int i = size - 1; i >= 0; i--) {
                C0500 r2 = r1[i];
                if (r2.f1698.m4216()) {
                    r2.f1698.m4213();
                }
            }
        }
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i != 82) {
            return false;
        }
        m2846();
        return true;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private int m2829() {
        return C0414.m2236(this.f1680) == 1 ? 0 : 1;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private int m2826(int i) {
        List<C0500> list = this.f1666;
        ListView r0 = list.get(list.size() - 1).m2852();
        int[] iArr = new int[2];
        r0.getLocationOnScreen(iArr);
        Rect rect = new Rect();
        this.f1667.getWindowVisibleDisplayFrame(rect);
        if (this.f1681 == 1) {
            if (iArr[0] + r0.getWidth() + i > rect.right) {
                return 0;
            }
            return 1;
        } else if (iArr[0] - i < 0) {
            return 1;
        } else {
            return 0;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2832(C0504 r2) {
        r2.m2896(this, this.f1669);
        if (m2847()) {
            m2825(r2);
        } else {
            this.f1674.add(r2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.ListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ʽ  reason: contains not printable characters */
    private void m2825(C0504 r14) {
        View view;
        C0500 r1;
        int i;
        int i2;
        int i3;
        LayoutInflater from = LayoutInflater.from(this.f1669);
        C0503 r12 = new C0503(r14, from, this.f1673);
        if (!m2847() && this.f1686) {
            r12.m2872(true);
        } else if (m2847()) {
            r12.m2872(C0516.m2986(r14));
        }
        int r2 = m2984(r12, null, this.f1669, this.f1670);
        C0679 r4 = m2828();
        r4.m4203((ListAdapter) r12);
        r4.m4218(r2);
        r4.m4214(this.f1679);
        if (this.f1666.size() > 0) {
            List<C0500> list = this.f1666;
            r1 = list.get(list.size() - 1);
            view = m2821(r1, r14);
        } else {
            r1 = null;
            view = null;
        }
        if (view != null) {
            r4.m4262(false);
            r4.m4259((Object) null);
            int r8 = m2826(r2);
            boolean z = r8 == 1;
            this.f1681 = r8;
            if (Build.VERSION.SDK_INT >= 26) {
                r4.m4207(view);
                i2 = 0;
                i = 0;
            } else {
                int[] iArr = new int[2];
                this.f1680.getLocationOnScreen(iArr);
                int[] iArr2 = new int[2];
                view.getLocationOnScreen(iArr2);
                i = iArr2[0] - iArr[0];
                i2 = iArr2[1] - iArr[1];
            }
            if ((this.f1679 & 5) != 5) {
                if (z) {
                    r2 = view.getWidth();
                }
                i3 = i - r2;
                r4.m4209(i3);
                r4.m4208(true);
                r4.m4212(i2);
            } else if (!z) {
                r2 = view.getWidth();
                i3 = i - r2;
                r4.m4209(i3);
                r4.m4208(true);
                r4.m4212(i2);
            }
            i3 = i + r2;
            r4.m4209(i3);
            r4.m4208(true);
            r4.m4212(i2);
        } else {
            if (this.f1682) {
                r4.m4209(this.f1684);
            }
            if (this.f1683) {
                r4.m4212(this.f1685);
            }
            r4.m4200(m3001());
        }
        this.f1666.add(new C0500(r4, r14, this.f1681));
        r4.m4211();
        ListView r22 = r4.m4217();
        r22.setOnKeyListener(this);
        if (r1 == null && this.f1687 && r14.m2927() != null) {
            FrameLayout frameLayout = (FrameLayout) from.inflate(C0727.C0734.abc_popup_menu_header_item_layout, (ViewGroup) r22, false);
            frameLayout.setEnabled(false);
            ((TextView) frameLayout.findViewById(16908310)).setText(r14.m2927());
            r22.addHeaderView(frameLayout, null, false);
            r4.m4211();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private MenuItem m2820(C0504 r5, C0504 r6) {
        int size = r5.size();
        for (int i = 0; i < size; i++) {
            MenuItem item = r5.getItem(i);
            if (item.hasSubMenu() && r6 == item.getSubMenu()) {
                return item;
            }
        }
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private View m2821(C0500 r8, C0504 r9) {
        int i;
        C0503 r1;
        int firstVisiblePosition;
        MenuItem r92 = m2820(r8.f1699, r9);
        if (r92 == null) {
            return null;
        }
        ListView r82 = r8.m2852();
        ListAdapter adapter = r82.getAdapter();
        int i2 = 0;
        if (adapter instanceof HeaderViewListAdapter) {
            HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
            i = headerViewListAdapter.getHeadersCount();
            r1 = (C0503) headerViewListAdapter.getWrappedAdapter();
        } else {
            r1 = (C0503) adapter;
            i = 0;
        }
        int count = r1.getCount();
        while (true) {
            if (i2 >= count) {
                i2 = -1;
                break;
            } else if (r92 == r1.getItem(i2)) {
                break;
            } else {
                i2++;
            }
        }
        if (i2 != -1 && (firstVisiblePosition = (i2 + i) - r82.getFirstVisiblePosition()) >= 0 && firstVisiblePosition < r82.getChildCount()) {
            return r82.getChildAt(firstVisiblePosition);
        }
        return null;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m2847() {
        return this.f1666.size() > 0 && this.f1666.get(0).f1698.m4216();
    }

    public void onDismiss() {
        C0500 r3;
        int size = this.f1666.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                r3 = null;
                break;
            }
            r3 = this.f1666.get(i);
            if (!r3.f1698.m4216()) {
                break;
            }
            i++;
        }
        if (r3 != null) {
            r3.f1699.m2907(false);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2837(boolean z) {
        for (C0500 r0 : this.f1666) {
            m2985(r0.m2852().getAdapter()).notifyDataSetChanged();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2834(C0518.C0519 r1) {
        this.f1688 = r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2839(C0526 r5) {
        for (C0500 next : this.f1666) {
            if (r5 == next.f1699) {
                next.m2852().requestFocus();
                return true;
            }
        }
        if (!r5.hasVisibleItems()) {
            return false;
        }
        m2832((C0504) r5);
        C0518.C0519 r0 = this.f1688;
        if (r0 != null) {
            r0.m3028(r5);
        }
        return true;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private int m2827(C0504 r4) {
        int size = this.f1666.size();
        for (int i = 0; i < size; i++) {
            if (r4 == this.f1666.get(i).f1699) {
                return i;
            }
        }
        return -1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2833(C0504 r6, boolean z) {
        int r0 = m2827(r6);
        if (r0 >= 0) {
            int i = r0 + 1;
            if (i < this.f1666.size()) {
                this.f1666.get(i).f1699.m2907(false);
            }
            C0500 remove = this.f1666.remove(r0);
            remove.f1699.m2906(this);
            if (this.f1668) {
                remove.f1698.m4261((Object) null);
                remove.f1698.m4206(0);
            }
            remove.f1698.m4213();
            int size = this.f1666.size();
            if (size > 0) {
                this.f1681 = this.f1666.get(size - 1).f1700;
            } else {
                this.f1681 = m2829();
            }
            if (size == 0) {
                m2846();
                C0518.C0519 r7 = this.f1688;
                if (r7 != null) {
                    r7.m3027(r6, true);
                }
                ViewTreeObserver viewTreeObserver = this.f1689;
                if (viewTreeObserver != null) {
                    if (viewTreeObserver.isAlive()) {
                        this.f1689.removeGlobalOnLayoutListener(this.f1675);
                    }
                    this.f1689 = null;
                }
                this.f1667.removeOnAttachStateChangeListener(this.f1676);
                this.f1690.onDismiss();
            } else if (z) {
                this.f1666.get(0).f1699.m2907(false);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2830(int i) {
        if (this.f1678 != i) {
            this.f1678 = i;
            this.f1679 = C0396.m2155(i, C0414.m2236(this.f1680));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2835(View view) {
        if (this.f1680 != view) {
            this.f1680 = view;
            this.f1679 = C0396.m2155(this.f1678, C0414.m2236(this.f1680));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2836(PopupWindow.OnDismissListener onDismissListener) {
        this.f1690 = onDismissListener;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public ListView m2848() {
        if (this.f1666.isEmpty()) {
            return null;
        }
        List<C0500> list = this.f1666;
        return list.get(list.size() - 1).m2852();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2840(int i) {
        this.f1682 = true;
        this.f1684 = i;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2843(int i) {
        this.f1683 = true;
        this.f1685 = i;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2844(boolean z) {
        this.f1687 = z;
    }

    /* renamed from: android.support.v7.view.menu.ʿ$ʻ  reason: contains not printable characters */
    /* compiled from: CascadingMenuPopup */
    private static class C0500 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public final C0679 f1698;

        /* renamed from: ʼ  reason: contains not printable characters */
        public final C0504 f1699;

        /* renamed from: ʽ  reason: contains not printable characters */
        public final int f1700;

        public C0500(C0679 r1, C0504 r2, int i) {
            this.f1698 = r1;
            this.f1699 = r2;
            this.f1700 = i;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public ListView m2852() {
            return this.f1698.m4217();
        }
    }
}
