package android.support.v7.view.menu;

import android.content.Context;
import android.support.v7.view.menu.C0504;
import android.support.v7.widget.C0592;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public final class ExpandedMenuView extends ListView implements C0504.C0506, C0520, AdapterView.OnItemClickListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final int[] f1611 = {16842964, 16843049};

    /* renamed from: ʼ  reason: contains not printable characters */
    private C0504 f1612;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f1613;

    public ExpandedMenuView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842868);
    }

    public ExpandedMenuView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        setOnItemClickListener(this);
        C0592 r3 = C0592.m3740(context, attributeSet, f1611, i, 0);
        if (r3.m3758(0)) {
            setBackgroundDrawable(r3.m3744(0));
        }
        if (r3.m3758(1)) {
            setDivider(r3.m3744(1));
        }
        r3.m3745();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2781(C0504 r1) {
        this.f1612 = r1;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        setChildrenDrawingCacheEnabled(false);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2782(C0508 r3) {
        return this.f1612.m2901(r3, 0);
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        m2782((C0508) getAdapter().getItem(i));
    }

    public int getWindowAnimations() {
        return this.f1613;
    }
}
