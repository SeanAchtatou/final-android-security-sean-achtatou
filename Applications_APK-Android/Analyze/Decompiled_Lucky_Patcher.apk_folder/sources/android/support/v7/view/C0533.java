package android.support.v7.view;

import android.content.Context;
import android.support.v7.view.C0529;
import android.support.v7.view.menu.C0504;
import android.support.v7.widget.ActionBarContextView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.lang.ref.WeakReference;

/* renamed from: android.support.v7.view.ʿ  reason: contains not printable characters */
/* compiled from: StandaloneActionMode */
public class C0533 extends C0529 implements C0504.C0505 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Context f1839;

    /* renamed from: ʼ  reason: contains not printable characters */
    private ActionBarContextView f1840;

    /* renamed from: ʽ  reason: contains not printable characters */
    private C0529.C0530 f1841;

    /* renamed from: ʾ  reason: contains not printable characters */
    private WeakReference<View> f1842;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f1843;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f1844;

    /* renamed from: ˈ  reason: contains not printable characters */
    private C0504 f1845;

    public C0533(Context context, ActionBarContextView actionBarContextView, C0529.C0530 r3, boolean z) {
        this.f1839 = context;
        this.f1840 = actionBarContextView;
        this.f1841 = r3;
        this.f1845 = new C0504(actionBarContextView.getContext()).m2885(1);
        this.f1845.m2893(this);
        this.f1844 = z;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3116(CharSequence charSequence) {
        this.f1840.setTitle(charSequence);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3111(CharSequence charSequence) {
        this.f1840.setSubtitle(charSequence);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3108(int i) {
        m3116(this.f1839.getString(i));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3115(int i) {
        m3111((CharSequence) this.f1839.getString(i));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3112(boolean z) {
        super.m3085(z);
        this.f1840.setTitleOptional(z);
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m3121() {
        return this.f1840.m3160();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3110(View view) {
        this.f1840.setCustomView(view);
        this.f1842 = view != null ? new WeakReference<>(view) : null;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m3118() {
        this.f1841.m3100(this, this.f1845);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m3117() {
        if (!this.f1843) {
            this.f1843 = true;
            this.f1840.sendAccessibilityEvent(32);
            this.f1841.m3097(this);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Menu m3114() {
        return this.f1845;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public CharSequence m3119() {
        return this.f1840.getTitle();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public CharSequence m3120() {
        return this.f1840.getSubtitle();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public View m3122() {
        WeakReference<View> weakReference = this.f1842;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public MenuInflater m3107() {
        return new C0536(this.f1840.getContext());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3113(C0504 r1, MenuItem menuItem) {
        return this.f1841.m3099(this, menuItem);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3109(C0504 r1) {
        m3118();
        this.f1840.m3157();
    }
}
