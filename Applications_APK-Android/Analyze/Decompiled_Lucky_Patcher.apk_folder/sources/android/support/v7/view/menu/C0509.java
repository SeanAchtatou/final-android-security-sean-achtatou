package android.support.v7.view.menu;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.ʽ.ʻ.C0315;
import android.support.v4.ˉ.C0393;
import android.support.v7.view.C0531;
import android.util.Log;
import android.view.ActionProvider;
import android.view.CollapsibleActionView;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import java.lang.reflect.Method;

/* renamed from: android.support.v7.view.menu.ˎ  reason: contains not printable characters */
/* compiled from: MenuItemWrapperICS */
public class C0509 extends C0497<C0315> implements MenuItem {

    /* renamed from: ʽ  reason: contains not printable characters */
    private Method f1785;

    C0509(Context context, C0315 r2) {
        super(context, r2);
    }

    public int getItemId() {
        return ((C0315) this.f1664).getItemId();
    }

    public int getGroupId() {
        return ((C0315) this.f1664).getGroupId();
    }

    public int getOrder() {
        return ((C0315) this.f1664).getOrder();
    }

    public MenuItem setTitle(CharSequence charSequence) {
        ((C0315) this.f1664).setTitle(charSequence);
        return this;
    }

    public MenuItem setTitle(int i) {
        ((C0315) this.f1664).setTitle(i);
        return this;
    }

    public CharSequence getTitle() {
        return ((C0315) this.f1664).getTitle();
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        ((C0315) this.f1664).setTitleCondensed(charSequence);
        return this;
    }

    public CharSequence getTitleCondensed() {
        return ((C0315) this.f1664).getTitleCondensed();
    }

    public MenuItem setIcon(Drawable drawable) {
        ((C0315) this.f1664).setIcon(drawable);
        return this;
    }

    public MenuItem setIcon(int i) {
        ((C0315) this.f1664).setIcon(i);
        return this;
    }

    public Drawable getIcon() {
        return ((C0315) this.f1664).getIcon();
    }

    public MenuItem setIntent(Intent intent) {
        ((C0315) this.f1664).setIntent(intent);
        return this;
    }

    public Intent getIntent() {
        return ((C0315) this.f1664).getIntent();
    }

    public MenuItem setShortcut(char c, char c2) {
        ((C0315) this.f1664).setShortcut(c, c2);
        return this;
    }

    public MenuItem setShortcut(char c, char c2, int i, int i2) {
        ((C0315) this.f1664).setShortcut(c, c2, i, i2);
        return this;
    }

    public MenuItem setNumericShortcut(char c) {
        ((C0315) this.f1664).setNumericShortcut(c);
        return this;
    }

    public MenuItem setNumericShortcut(char c, int i) {
        ((C0315) this.f1664).setNumericShortcut(c, i);
        return this;
    }

    public char getNumericShortcut() {
        return ((C0315) this.f1664).getNumericShortcut();
    }

    public int getNumericModifiers() {
        return ((C0315) this.f1664).getNumericModifiers();
    }

    public MenuItem setAlphabeticShortcut(char c) {
        ((C0315) this.f1664).setAlphabeticShortcut(c);
        return this;
    }

    public MenuItem setAlphabeticShortcut(char c, int i) {
        ((C0315) this.f1664).setAlphabeticShortcut(c, i);
        return this;
    }

    public char getAlphabeticShortcut() {
        return ((C0315) this.f1664).getAlphabeticShortcut();
    }

    public int getAlphabeticModifiers() {
        return ((C0315) this.f1664).getAlphabeticModifiers();
    }

    public MenuItem setCheckable(boolean z) {
        ((C0315) this.f1664).setCheckable(z);
        return this;
    }

    public boolean isCheckable() {
        return ((C0315) this.f1664).isCheckable();
    }

    public MenuItem setChecked(boolean z) {
        ((C0315) this.f1664).setChecked(z);
        return this;
    }

    public boolean isChecked() {
        return ((C0315) this.f1664).isChecked();
    }

    public MenuItem setVisible(boolean z) {
        return ((C0315) this.f1664).setVisible(z);
    }

    public boolean isVisible() {
        return ((C0315) this.f1664).isVisible();
    }

    public MenuItem setEnabled(boolean z) {
        ((C0315) this.f1664).setEnabled(z);
        return this;
    }

    public boolean isEnabled() {
        return ((C0315) this.f1664).isEnabled();
    }

    public boolean hasSubMenu() {
        return ((C0315) this.f1664).hasSubMenu();
    }

    public SubMenu getSubMenu() {
        return m2816(((C0315) this.f1664).getSubMenu());
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        ((C0315) this.f1664).setOnMenuItemClickListener(onMenuItemClickListener != null ? new C0513(onMenuItemClickListener) : null);
        return this;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return ((C0315) this.f1664).getMenuInfo();
    }

    public void setShowAsAction(int i) {
        ((C0315) this.f1664).setShowAsAction(i);
    }

    public MenuItem setShowAsActionFlags(int i) {
        ((C0315) this.f1664).setShowAsActionFlags(i);
        return this;
    }

    public MenuItem setActionView(View view) {
        if (view instanceof CollapsibleActionView) {
            view = new C0511(view);
        }
        ((C0315) this.f1664).setActionView(view);
        return this;
    }

    public MenuItem setActionView(int i) {
        ((C0315) this.f1664).setActionView(i);
        View actionView = ((C0315) this.f1664).getActionView();
        if (actionView instanceof CollapsibleActionView) {
            ((C0315) this.f1664).setActionView(new C0511(actionView));
        }
        return this;
    }

    public View getActionView() {
        View actionView = ((C0315) this.f1664).getActionView();
        return actionView instanceof C0511 ? ((C0511) actionView).m2978() : actionView;
    }

    public MenuItem setActionProvider(ActionProvider actionProvider) {
        ((C0315) this.f1664).m1829(actionProvider != null ? m2970(actionProvider) : null);
        return this;
    }

    public ActionProvider getActionProvider() {
        C0393 r0 = ((C0315) this.f1664).m1831();
        if (r0 instanceof C0510) {
            return ((C0510) r0).f1786;
        }
        return null;
    }

    public boolean expandActionView() {
        return ((C0315) this.f1664).expandActionView();
    }

    public boolean collapseActionView() {
        return ((C0315) this.f1664).collapseActionView();
    }

    public boolean isActionViewExpanded() {
        return ((C0315) this.f1664).isActionViewExpanded();
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        ((C0315) this.f1664).setOnActionExpandListener(onActionExpandListener != null ? new C0512(onActionExpandListener) : null);
        return this;
    }

    public MenuItem setContentDescription(CharSequence charSequence) {
        ((C0315) this.f1664).m1830(charSequence);
        return this;
    }

    public CharSequence getContentDescription() {
        return ((C0315) this.f1664).getContentDescription();
    }

    public MenuItem setTooltipText(CharSequence charSequence) {
        ((C0315) this.f1664).m1832(charSequence);
        return this;
    }

    public CharSequence getTooltipText() {
        return ((C0315) this.f1664).getTooltipText();
    }

    public MenuItem setIconTintList(ColorStateList colorStateList) {
        ((C0315) this.f1664).setIconTintList(colorStateList);
        return this;
    }

    public ColorStateList getIconTintList() {
        return ((C0315) this.f1664).getIconTintList();
    }

    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        ((C0315) this.f1664).setIconTintMode(mode);
        return this;
    }

    public PorterDuff.Mode getIconTintMode() {
        return ((C0315) this.f1664).getIconTintMode();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2971(boolean z) {
        try {
            if (this.f1785 == null) {
                this.f1785 = ((C0315) this.f1664).getClass().getDeclaredMethod("setExclusiveCheckable", Boolean.TYPE);
            }
            this.f1785.invoke(this.f1664, Boolean.valueOf(z));
        } catch (Exception e) {
            Log.w("MenuItemWrapper", "Error while calling setExclusiveCheckable", e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0510 m2970(ActionProvider actionProvider) {
        return new C0510(this.f1661, actionProvider);
    }

    /* renamed from: android.support.v7.view.menu.ˎ$ʾ  reason: contains not printable characters */
    /* compiled from: MenuItemWrapperICS */
    private class C0513 extends C0498<MenuItem.OnMenuItemClickListener> implements MenuItem.OnMenuItemClickListener {
        C0513(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
            super(onMenuItemClickListener);
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            return ((MenuItem.OnMenuItemClickListener) this.f1664).onMenuItemClick(C0509.this.m2815(menuItem));
        }
    }

    /* renamed from: android.support.v7.view.menu.ˎ$ʽ  reason: contains not printable characters */
    /* compiled from: MenuItemWrapperICS */
    private class C0512 extends C0498<MenuItem.OnActionExpandListener> implements MenuItem.OnActionExpandListener {
        C0512(MenuItem.OnActionExpandListener onActionExpandListener) {
            super(onActionExpandListener);
        }

        public boolean onMenuItemActionExpand(MenuItem menuItem) {
            return ((MenuItem.OnActionExpandListener) this.f1664).onMenuItemActionExpand(C0509.this.m2815(menuItem));
        }

        public boolean onMenuItemActionCollapse(MenuItem menuItem) {
            return ((MenuItem.OnActionExpandListener) this.f1664).onMenuItemActionCollapse(C0509.this.m2815(menuItem));
        }
    }

    /* renamed from: android.support.v7.view.menu.ˎ$ʻ  reason: contains not printable characters */
    /* compiled from: MenuItemWrapperICS */
    class C0510 extends C0393 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final ActionProvider f1786;

        public C0510(Context context, ActionProvider actionProvider) {
            super(context);
            this.f1786 = actionProvider;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public View m2972() {
            return this.f1786.onCreateActionView();
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public boolean m2974() {
            return this.f1786.onPerformDefaultAction();
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public boolean m2975() {
            return this.f1786.hasSubMenu();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2973(SubMenu subMenu) {
            this.f1786.onPrepareSubMenu(C0509.this.m2816(subMenu));
        }
    }

    /* renamed from: android.support.v7.view.menu.ˎ$ʼ  reason: contains not printable characters */
    /* compiled from: MenuItemWrapperICS */
    static class C0511 extends FrameLayout implements C0531 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final CollapsibleActionView f1788;

        C0511(View view) {
            super(view.getContext());
            this.f1788 = (CollapsibleActionView) view;
            addView(view);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2976() {
            this.f1788.onActionViewExpanded();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2977() {
            this.f1788.onActionViewCollapsed();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public View m2978() {
            return (View) this.f1788;
        }
    }
}
