package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.support.v7.view.menu.C0504;
import android.support.v7.view.menu.C0520;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.C0594;
import android.support.v7.widget.C0648;
import android.support.v7.widget.C0677;
import android.support.v7.ʻ.C0727;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class ActionMenuItemView extends C0677 implements C0520.C0521, ActionMenuView.C0543, View.OnClickListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    C0508 f1599;

    /* renamed from: ʼ  reason: contains not printable characters */
    C0504.C0506 f1600;

    /* renamed from: ʽ  reason: contains not printable characters */
    C0494 f1601;

    /* renamed from: ʾ  reason: contains not printable characters */
    private CharSequence f1602;

    /* renamed from: ʿ  reason: contains not printable characters */
    private Drawable f1603;

    /* renamed from: ˆ  reason: contains not printable characters */
    private C0648 f1604;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f1605;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f1606;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f1607;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f1608;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f1609;

    /* renamed from: android.support.v7.view.menu.ActionMenuItemView$ʼ  reason: contains not printable characters */
    public static abstract class C0494 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract C0524 m2780();
    }

    public void setCheckable(boolean z) {
    }

    public void setChecked(boolean z) {
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m2775() {
        return true;
    }

    public ActionMenuItemView(Context context) {
        this(context, null);
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Resources resources = context.getResources();
        this.f1605 = m2771();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0727.C0737.ActionMenuItemView, i, 0);
        this.f1607 = obtainStyledAttributes.getDimensionPixelSize(C0727.C0737.ActionMenuItemView_android_minWidth, 0);
        obtainStyledAttributes.recycle();
        this.f1609 = (int) ((resources.getDisplayMetrics().density * 32.0f) + 0.5f);
        setOnClickListener(this);
        this.f1608 = -1;
        setSaveEnabled(false);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.f1605 = m2771();
        m2772();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean m2771() {
        Configuration configuration = getContext().getResources().getConfiguration();
        int i = configuration.screenWidthDp;
        return i >= 480 || (i >= 640 && configuration.screenHeightDp >= 480) || configuration.orientation == 2;
    }

    public void setPadding(int i, int i2, int i3, int i4) {
        this.f1608 = i;
        super.setPadding(i, i2, i3, i4);
    }

    public C0508 getItemData() {
        return this.f1599;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2773(C0508 r1, int i) {
        this.f1599 = r1;
        setIcon(r1.getIcon());
        setTitle(r1.m2946((C0520.C0521) this));
        setId(r1.getItemId());
        setVisibility(r1.isVisible() ? 0 : 8);
        setEnabled(r1.isEnabled());
        if (r1.hasSubMenu() && this.f1604 == null) {
            this.f1604 = new C0493();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        C0648 r0;
        if (!this.f1599.hasSubMenu() || (r0 = this.f1604) == null || !r0.onTouch(this, motionEvent)) {
            return super.onTouchEvent(motionEvent);
        }
        return true;
    }

    public void onClick(View view) {
        C0504.C0506 r2 = this.f1600;
        if (r2 != null) {
            r2.m2935(this.f1599);
        }
    }

    public void setItemInvoker(C0504.C0506 r1) {
        this.f1600 = r1;
    }

    public void setPopupCallback(C0494 r1) {
        this.f1601 = r1;
    }

    public void setExpandedFormat(boolean z) {
        if (this.f1606 != z) {
            this.f1606 = z;
            C0508 r2 = this.f1599;
            if (r2 != null) {
                r2.m2962();
            }
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private void m2772() {
        CharSequence charSequence;
        boolean z = true;
        boolean z2 = !TextUtils.isEmpty(this.f1602);
        if (this.f1603 != null && (!this.f1599.m2967() || (!this.f1605 && !this.f1606))) {
            z = false;
        }
        boolean z3 = z2 & z;
        CharSequence charSequence2 = null;
        setText(z3 ? this.f1602 : null);
        CharSequence contentDescription = this.f1599.getContentDescription();
        if (TextUtils.isEmpty(contentDescription)) {
            if (z3) {
                charSequence = null;
            } else {
                charSequence = this.f1599.getTitle();
            }
            setContentDescription(charSequence);
        } else {
            setContentDescription(contentDescription);
        }
        CharSequence tooltipText = this.f1599.getTooltipText();
        if (TextUtils.isEmpty(tooltipText)) {
            if (!z3) {
                charSequence2 = this.f1599.getTitle();
            }
            C0594.m3805(this, charSequence2);
            return;
        }
        C0594.m3805(this, tooltipText);
    }

    public void setIcon(Drawable drawable) {
        this.f1603 = drawable;
        if (drawable != null) {
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            int i = this.f1609;
            if (intrinsicWidth > i) {
                intrinsicHeight = (int) (((float) intrinsicHeight) * (((float) i) / ((float) intrinsicWidth)));
                intrinsicWidth = i;
            }
            int i2 = this.f1609;
            if (intrinsicHeight > i2) {
                intrinsicWidth = (int) (((float) intrinsicWidth) * (((float) i2) / ((float) intrinsicHeight)));
                intrinsicHeight = i2;
            }
            drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        }
        setCompoundDrawables(drawable, null, null, null);
        m2772();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2774() {
        return !TextUtils.isEmpty(getText());
    }

    public void setTitle(CharSequence charSequence) {
        this.f1602 = charSequence;
        m2772();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m2776() {
        return m2774() && this.f1599.getIcon() == null;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m2777() {
        return m2774();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        boolean r0 = m2774();
        if (r0 && (i3 = this.f1608) >= 0) {
            super.setPadding(i3, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
        super.onMeasure(i, i2);
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        int measuredWidth = getMeasuredWidth();
        int min = mode == Integer.MIN_VALUE ? Math.min(size, this.f1607) : this.f1607;
        if (mode != 1073741824 && this.f1607 > 0 && measuredWidth < min) {
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(min, 1073741824), i2);
        }
        if (!r0 && this.f1603 != null) {
            super.setPadding((getMeasuredWidth() - this.f1603.getBounds().width()) / 2, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
    }

    /* renamed from: android.support.v7.view.menu.ActionMenuItemView$ʻ  reason: contains not printable characters */
    private class C0493 extends C0648 {
        public C0493() {
            super(ActionMenuItemView.this);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0524 m2778() {
            if (ActionMenuItemView.this.f1601 != null) {
                return ActionMenuItemView.this.f1601.m2780();
            }
            return null;
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m2779() {
            C0524 r0;
            if (ActionMenuItemView.this.f1600 == null || !ActionMenuItemView.this.f1600.m2935(ActionMenuItemView.this.f1599) || (r0 = m2778()) == null || !r0.m3037()) {
                return false;
            }
            return true;
        }
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        super.onRestoreInstanceState(null);
    }
}
