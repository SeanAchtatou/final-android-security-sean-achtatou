package android.support.v7.view.menu;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.ʼ.ʻ.C0288;
import android.support.v4.ʽ.ʻ.C0315;
import android.support.v4.ˉ.C0393;
import android.support.v7.view.menu.C0520;
import android.support.v7.ʼ.ʻ.C0739;
import android.util.Log;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

/* renamed from: android.support.v7.view.menu.ˋ  reason: contains not printable characters */
/* compiled from: MenuItemImpl */
public final class C0508 implements C0315 {

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private static String f1749;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private static String f1750;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private static String f1751;

    /* renamed from: ــ  reason: contains not printable characters */
    private static String f1752;

    /* renamed from: ʻ  reason: contains not printable characters */
    C0504 f1753;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private MenuItem.OnActionExpandListener f1754;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f1755;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private ContextMenu.ContextMenuInfo f1756;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f1757;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private boolean f1758 = false;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final int f1759;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final int f1760;

    /* renamed from: ˆ  reason: contains not printable characters */
    private CharSequence f1761;

    /* renamed from: ˈ  reason: contains not printable characters */
    private CharSequence f1762;

    /* renamed from: ˉ  reason: contains not printable characters */
    private Intent f1763;

    /* renamed from: ˊ  reason: contains not printable characters */
    private char f1764;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f1765 = 4096;

    /* renamed from: ˎ  reason: contains not printable characters */
    private char f1766;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f1767 = 4096;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Drawable f1768;

    /* renamed from: י  reason: contains not printable characters */
    private int f1769 = 0;

    /* renamed from: ـ  reason: contains not printable characters */
    private C0526 f1770;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Runnable f1771;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private MenuItem.OnMenuItemClickListener f1772;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private View f1773;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private CharSequence f1774;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private C0393 f1775;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private CharSequence f1776;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private ColorStateList f1777 = null;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private PorterDuff.Mode f1778 = null;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private boolean f1779 = false;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private boolean f1780 = false;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f1781 = false;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f1782 = 16;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private int f1783 = 0;

    C0508(C0504 r3, int i, int i2, int i3, int i4, CharSequence charSequence, int i5) {
        this.f1753 = r3;
        this.f1755 = i2;
        this.f1757 = i;
        this.f1759 = i3;
        this.f1760 = i4;
        this.f1761 = charSequence;
        this.f1783 = i5;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m2953() {
        MenuItem.OnMenuItemClickListener onMenuItemClickListener = this.f1772;
        if (onMenuItemClickListener != null && onMenuItemClickListener.onMenuItemClick(this)) {
            return true;
        }
        C0504 r0 = this.f1753;
        if (r0.m2900(r0, this)) {
            return true;
        }
        Runnable runnable = this.f1771;
        if (runnable != null) {
            runnable.run();
            return true;
        }
        if (this.f1763 != null) {
            try {
                this.f1753.m2918().startActivity(this.f1763);
                return true;
            } catch (ActivityNotFoundException e) {
                Log.e("MenuItemImpl", "Can't find activity to handle intent; ignoring", e);
            }
        }
        C0393 r02 = this.f1775;
        if (r02 == null || !r02.m2150()) {
            return false;
        }
        return true;
    }

    public boolean isEnabled() {
        return (this.f1782 & 16) != 0;
    }

    public MenuItem setEnabled(boolean z) {
        if (z) {
            this.f1782 |= 16;
        } else {
            this.f1782 &= -17;
        }
        this.f1753.m2899(false);
        return this;
    }

    public int getGroupId() {
        return this.f1757;
    }

    @ViewDebug.CapturedViewProperty
    public int getItemId() {
        return this.f1755;
    }

    public int getOrder() {
        return this.f1759;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m2954() {
        return this.f1760;
    }

    public Intent getIntent() {
        return this.f1763;
    }

    public MenuItem setIntent(Intent intent) {
        this.f1763 = intent;
        return this;
    }

    public char getAlphabeticShortcut() {
        return this.f1766;
    }

    public MenuItem setAlphabeticShortcut(char c) {
        if (this.f1766 == c) {
            return this;
        }
        this.f1766 = Character.toLowerCase(c);
        this.f1753.m2899(false);
        return this;
    }

    public MenuItem setAlphabeticShortcut(char c, int i) {
        if (this.f1766 == c && this.f1767 == i) {
            return this;
        }
        this.f1766 = Character.toLowerCase(c);
        this.f1767 = KeyEvent.normalizeMetaState(i);
        this.f1753.m2899(false);
        return this;
    }

    public int getAlphabeticModifiers() {
        return this.f1767;
    }

    public char getNumericShortcut() {
        return this.f1764;
    }

    public int getNumericModifiers() {
        return this.f1765;
    }

    public MenuItem setNumericShortcut(char c) {
        if (this.f1764 == c) {
            return this;
        }
        this.f1764 = c;
        this.f1753.m2899(false);
        return this;
    }

    public MenuItem setNumericShortcut(char c, int i) {
        if (this.f1764 == c && this.f1765 == i) {
            return this;
        }
        this.f1764 = c;
        this.f1765 = KeyEvent.normalizeMetaState(i);
        this.f1753.m2899(false);
        return this;
    }

    public MenuItem setShortcut(char c, char c2) {
        this.f1764 = c;
        this.f1766 = Character.toLowerCase(c2);
        this.f1753.m2899(false);
        return this;
    }

    public MenuItem setShortcut(char c, char c2, int i, int i2) {
        this.f1764 = c;
        this.f1765 = KeyEvent.normalizeMetaState(i);
        this.f1766 = Character.toLowerCase(c2);
        this.f1767 = KeyEvent.normalizeMetaState(i2);
        this.f1753.m2899(false);
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public char m2956() {
        return this.f1753.m2908() ? this.f1766 : this.f1764;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public String m2958() {
        char r0 = m2956();
        if (r0 == 0) {
            return BuildConfig.FLAVOR;
        }
        StringBuilder sb = new StringBuilder(f1750);
        if (r0 == 8) {
            sb.append(f1752);
        } else if (r0 == 10) {
            sb.append(f1749);
        } else if (r0 != ' ') {
            sb.append(r0);
        } else {
            sb.append(f1751);
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m2960() {
        return this.f1753.m2912() && m2956() != 0;
    }

    public SubMenu getSubMenu() {
        return this.f1770;
    }

    public boolean hasSubMenu() {
        return this.f1770 != null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2947(C0526 r2) {
        this.f1770 = r2;
        r2.setHeaderTitle(getTitle());
    }

    @ViewDebug.CapturedViewProperty
    public CharSequence getTitle() {
        return this.f1761;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public CharSequence m2946(C0520.C0521 r1) {
        if (r1 == null || !r1.m3031()) {
            return getTitle();
        }
        return getTitleCondensed();
    }

    public MenuItem setTitle(CharSequence charSequence) {
        this.f1761 = charSequence;
        this.f1753.m2899(false);
        C0526 r0 = this.f1770;
        if (r0 != null) {
            r0.setHeaderTitle(charSequence);
        }
        return this;
    }

    public MenuItem setTitle(int i) {
        return setTitle(this.f1753.m2918().getString(i));
    }

    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.f1762;
        if (charSequence == null) {
            charSequence = this.f1761;
        }
        return (Build.VERSION.SDK_INT >= 18 || charSequence == null || (charSequence instanceof String)) ? charSequence : charSequence.toString();
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f1762 = charSequence;
        if (charSequence == null) {
            CharSequence charSequence2 = this.f1761;
        }
        this.f1753.m2899(false);
        return this;
    }

    public Drawable getIcon() {
        Drawable drawable = this.f1768;
        if (drawable != null) {
            return m2940(drawable);
        }
        if (this.f1769 == 0) {
            return null;
        }
        Drawable r0 = C0739.m4883(this.f1753.m2918(), this.f1769);
        this.f1769 = 0;
        this.f1768 = r0;
        return m2940(r0);
    }

    public MenuItem setIcon(Drawable drawable) {
        this.f1769 = 0;
        this.f1768 = drawable;
        this.f1781 = true;
        this.f1753.m2899(false);
        return this;
    }

    public MenuItem setIcon(int i) {
        this.f1768 = null;
        this.f1769 = i;
        this.f1781 = true;
        this.f1753.m2899(false);
        return this;
    }

    public MenuItem setIconTintList(ColorStateList colorStateList) {
        this.f1777 = colorStateList;
        this.f1779 = true;
        this.f1781 = true;
        this.f1753.m2899(false);
        return this;
    }

    public ColorStateList getIconTintList() {
        return this.f1777;
    }

    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        this.f1778 = mode;
        this.f1780 = true;
        this.f1781 = true;
        this.f1753.m2899(false);
        return this;
    }

    public PorterDuff.Mode getIconTintMode() {
        return this.f1778;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private Drawable m2940(Drawable drawable) {
        if (drawable != null && this.f1781 && (this.f1779 || this.f1780)) {
            drawable = C0288.m1713(drawable).mutate();
            if (this.f1779) {
                C0288.m1703(drawable, this.f1777);
            }
            if (this.f1780) {
                C0288.m1706(drawable, this.f1778);
            }
            this.f1781 = false;
        }
        return drawable;
    }

    public boolean isCheckable() {
        return (this.f1782 & 1) == 1;
    }

    public MenuItem setCheckable(boolean z) {
        int i = this.f1782;
        this.f1782 = z | (i & true) ? 1 : 0;
        if (i != this.f1782) {
            this.f1753.m2899(false);
        }
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2949(boolean z) {
        this.f1782 = (z ? 4 : 0) | (this.f1782 & -5);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m2961() {
        return (this.f1782 & 4) != 0;
    }

    public boolean isChecked() {
        return (this.f1782 & 2) == 2;
    }

    public MenuItem setChecked(boolean z) {
        if ((this.f1782 & 4) != 0) {
            this.f1753.m2897((MenuItem) this);
        } else {
            m2952(z);
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2952(boolean z) {
        int i = this.f1782;
        this.f1782 = (z ? 2 : 0) | (i & -3);
        if (i != this.f1782) {
            this.f1753.m2899(false);
        }
    }

    public boolean isVisible() {
        C0393 r0 = this.f1775;
        if (r0 == null || !r0.m2148()) {
            if ((this.f1782 & 8) == 0) {
                return true;
            }
            return false;
        } else if ((this.f1782 & 8) != 0 || !this.f1775.m2149()) {
            return false;
        } else {
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m2955(boolean z) {
        int i = this.f1782;
        this.f1782 = (z ? 0 : 8) | (i & -9);
        if (i != this.f1782) {
            return true;
        }
        return false;
    }

    public MenuItem setVisible(boolean z) {
        if (m2955(z)) {
            this.f1753.m2894(this);
        }
        return this;
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.f1772 = onMenuItemClickListener;
        return this;
    }

    public String toString() {
        CharSequence charSequence = this.f1761;
        if (charSequence != null) {
            return charSequence.toString();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2948(ContextMenu.ContextMenuInfo contextMenuInfo) {
        this.f1756 = contextMenuInfo;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return this.f1756;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public void m2962() {
        this.f1753.m2905(this);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean m2963() {
        return this.f1753.m2931();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean m2964() {
        return (this.f1782 & 32) == 32;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean m2965() {
        return (this.f1783 & 1) == 1;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public boolean m2966() {
        return (this.f1783 & 2) == 2;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m2957(boolean z) {
        if (z) {
            this.f1782 |= 32;
        } else {
            this.f1782 &= -33;
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m2967() {
        return (this.f1783 & 4) == 4;
    }

    public void setShowAsAction(int i) {
        int i2 = i & 3;
        if (i2 == 0 || i2 == 1 || i2 == 2) {
            this.f1783 = i;
            this.f1753.m2905(this);
            return;
        }
        throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0315 setActionView(View view) {
        int i;
        this.f1773 = view;
        this.f1775 = null;
        if (view != null && view.getId() == -1 && (i = this.f1755) > 0) {
            view.setId(i);
        }
        this.f1753.m2905(this);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0315 setActionView(int i) {
        Context r0 = this.f1753.m2918();
        setActionView(LayoutInflater.from(r0).inflate(i, (ViewGroup) new LinearLayout(r0), false));
        return this;
    }

    public View getActionView() {
        View view = this.f1773;
        if (view != null) {
            return view;
        }
        C0393 r0 = this.f1775;
        if (r0 == null) {
            return null;
        }
        this.f1773 = r0.m2143(this);
        return this.f1773;
    }

    public MenuItem setActionProvider(ActionProvider actionProvider) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setActionProvider()");
    }

    public ActionProvider getActionProvider() {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.getActionProvider()");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0393 m2945() {
        return this.f1775;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0315 m2942(C0393 r2) {
        C0393 r0 = this.f1775;
        if (r0 != null) {
            r0.m2152();
        }
        this.f1773 = null;
        this.f1775 = r2;
        this.f1753.m2899(true);
        C0393 r22 = this.f1775;
        if (r22 != null) {
            r22.m2145(new C0393.C0395() {
                /* renamed from: ʻ  reason: contains not printable characters */
                public void m2969(boolean z) {
                    C0508.this.f1753.m2894(C0508.this);
                }
            });
        }
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0315 setShowAsActionFlags(int i) {
        setShowAsAction(i);
        return this;
    }

    public boolean expandActionView() {
        if (!m2968()) {
            return false;
        }
        MenuItem.OnActionExpandListener onActionExpandListener = this.f1754;
        if (onActionExpandListener == null || onActionExpandListener.onMenuItemActionExpand(this)) {
            return this.f1753.m2913(this);
        }
        return false;
    }

    public boolean collapseActionView() {
        if ((this.f1783 & 8) == 0) {
            return false;
        }
        if (this.f1773 == null) {
            return true;
        }
        MenuItem.OnActionExpandListener onActionExpandListener = this.f1754;
        if (onActionExpandListener == null || onActionExpandListener.onMenuItemActionCollapse(this)) {
            return this.f1753.m2917(this);
        }
        return false;
    }

    /* renamed from: י  reason: contains not printable characters */
    public boolean m2968() {
        C0393 r0;
        if ((this.f1783 & 8) == 0) {
            return false;
        }
        if (this.f1773 == null && (r0 = this.f1775) != null) {
            this.f1773 = r0.m2143(this);
        }
        if (this.f1773 != null) {
            return true;
        }
        return false;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m2959(boolean z) {
        this.f1758 = z;
        this.f1753.m2899(false);
    }

    public boolean isActionViewExpanded() {
        return this.f1758;
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        this.f1754 = onActionExpandListener;
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0315 setContentDescription(CharSequence charSequence) {
        this.f1774 = charSequence;
        this.f1753.m2899(false);
        return this;
    }

    public CharSequence getContentDescription() {
        return this.f1774;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0315 setTooltipText(CharSequence charSequence) {
        this.f1776 = charSequence;
        this.f1753.m2899(false);
        return this;
    }

    public CharSequence getTooltipText() {
        return this.f1776;
    }
}
