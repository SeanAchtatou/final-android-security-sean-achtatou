package android.support.v7.view.menu;

import android.content.DialogInterface;
import android.os.IBinder;
import android.support.v7.app.C0458;
import android.support.v7.view.menu.C0518;
import android.support.v7.ʻ.C0727;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/* renamed from: android.support.v7.view.menu.ˊ  reason: contains not printable characters */
/* compiled from: MenuDialogHelper */
class C0507 implements DialogInterface.OnClickListener, DialogInterface.OnDismissListener, DialogInterface.OnKeyListener, C0518.C0519 {

    /* renamed from: ʻ  reason: contains not printable characters */
    C0501 f1745;

    /* renamed from: ʼ  reason: contains not printable characters */
    private C0504 f1746;

    /* renamed from: ʽ  reason: contains not printable characters */
    private C0458 f1747;

    /* renamed from: ʾ  reason: contains not printable characters */
    private C0518.C0519 f1748;

    public C0507(C0504 r1) {
        this.f1746 = r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2937(IBinder iBinder) {
        C0504 r0 = this.f1746;
        C0458.C0459 r1 = new C0458.C0459(r0.m2918());
        this.f1745 = new C0501(r1.m2501(), C0727.C0734.abc_list_menu_item_layout);
        this.f1745.m2858(this);
        this.f1746.m2895(this.f1745);
        r1.m2505(this.f1745.m2867(), this);
        View r2 = r0.m2929();
        if (r2 != null) {
            r1.m2504(r2);
        } else {
            r1.m2503(r0.m2928()).m2506(r0.m2927());
        }
        r1.m2502(this);
        this.f1747 = r1.m2507();
        this.f1747.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.f1747.getWindow().getAttributes();
        attributes.type = 1003;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.f1747.show();
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.f1747.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.f1747.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.f1746.m2907(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.f1746.performShortcut(i, keyEvent, 0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2936() {
        C0458 r0 = this.f1747;
        if (r0 != null) {
            r0.dismiss();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.view.menu.ˆ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
     arg types: [android.support.v7.view.menu.ˉ, int]
     candidates:
      android.support.v7.view.menu.ˆ.ʻ(android.content.Context, android.support.v7.view.menu.ˉ):void
      android.support.v7.view.menu.ˆ.ʻ(android.support.v7.view.menu.ˉ, android.support.v7.view.menu.ˋ):boolean
      android.support.v7.view.menu.ـ.ʻ(android.content.Context, android.support.v7.view.menu.ˉ):void
      android.support.v7.view.menu.ـ.ʻ(android.support.v7.view.menu.ˉ, android.support.v7.view.menu.ˋ):boolean
      android.support.v7.view.menu.ˆ.ʻ(android.support.v7.view.menu.ˉ, boolean):void */
    public void onDismiss(DialogInterface dialogInterface) {
        this.f1745.m2857(this.f1746, true);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2938(C0504 r2, boolean z) {
        if (z || r2 == this.f1746) {
            m2936();
        }
        C0518.C0519 r0 = this.f1748;
        if (r0 != null) {
            r0.m3027(r2, z);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2939(C0504 r2) {
        C0518.C0519 r0 = this.f1748;
        if (r0 != null) {
            return r0.m3028(r2);
        }
        return false;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f1746.m2901((C0508) this.f1745.m2867().getItem(i), 0);
    }
}
