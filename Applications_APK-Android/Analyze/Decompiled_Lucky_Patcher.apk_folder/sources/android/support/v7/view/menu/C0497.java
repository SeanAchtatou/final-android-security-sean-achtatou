package android.support.v7.view.menu;

import android.content.Context;
import android.support.v4.ʽ.ʻ.C0315;
import android.support.v4.ʽ.ʻ.C0316;
import android.support.v4.ˈ.C0331;
import android.view.MenuItem;
import android.view.SubMenu;
import java.util.Iterator;
import java.util.Map;

/* renamed from: android.support.v7.view.menu.ʽ  reason: contains not printable characters */
/* compiled from: BaseMenuWrapper */
abstract class C0497<T> extends C0498<T> {

    /* renamed from: ʻ  reason: contains not printable characters */
    final Context f1661;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Map<C0315, MenuItem> f1662;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Map<C0316, SubMenu> f1663;

    C0497(Context context, T t) {
        super(t);
        this.f1661 = context;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final MenuItem m2815(MenuItem menuItem) {
        if (!(menuItem instanceof C0315)) {
            return menuItem;
        }
        C0315 r0 = (C0315) menuItem;
        if (this.f1662 == null) {
            this.f1662 = new C0331();
        }
        MenuItem menuItem2 = this.f1662.get(menuItem);
        if (menuItem2 != null) {
            return menuItem2;
        }
        MenuItem r3 = C0522.m3033(this.f1661, r0);
        this.f1662.put(r0, r3);
        return r3;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final SubMenu m2816(SubMenu subMenu) {
        if (!(subMenu instanceof C0316)) {
            return subMenu;
        }
        C0316 r3 = (C0316) subMenu;
        if (this.f1663 == null) {
            this.f1663 = new C0331();
        }
        SubMenu subMenu2 = this.f1663.get(r3);
        if (subMenu2 != null) {
            return subMenu2;
        }
        SubMenu r0 = C0522.m3034(this.f1661, r3);
        this.f1663.put(r3, r0);
        return r0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m2817() {
        Map<C0315, MenuItem> map = this.f1662;
        if (map != null) {
            map.clear();
        }
        Map<C0316, SubMenu> map2 = this.f1663;
        if (map2 != null) {
            map2.clear();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m2818(int i) {
        Map<C0315, MenuItem> map = this.f1662;
        if (map != null) {
            Iterator<C0315> it = map.keySet().iterator();
            while (it.hasNext()) {
                if (i == it.next().getGroupId()) {
                    it.remove();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m2819(int i) {
        Map<C0315, MenuItem> map = this.f1662;
        if (map != null) {
            Iterator<C0315> it = map.keySet().iterator();
            while (it.hasNext()) {
                if (i == it.next().getItemId()) {
                    it.remove();
                    return;
                }
            }
        }
    }
}
