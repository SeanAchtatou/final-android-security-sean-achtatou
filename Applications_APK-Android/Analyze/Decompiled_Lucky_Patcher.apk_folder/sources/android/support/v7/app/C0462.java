package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.C0448;
import android.support.v7.view.C0529;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

/* renamed from: android.support.v7.app.ˈ  reason: contains not printable characters */
/* compiled from: AppCompatDelegate */
public abstract class C0462 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static int f1416 = -1;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static boolean f1417 = false;

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract C0444 m2513();

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract C0529 m2514(C0529.C0530 r1);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract <T extends View> T m2515(int i);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m2516(Configuration configuration);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m2517(Bundle bundle);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m2518(Toolbar toolbar);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m2519(View view);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m2520(View view, ViewGroup.LayoutParams layoutParams);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m2521(CharSequence charSequence);

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract MenuInflater m2522();

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract void m2523(int i);

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract void m2524(Bundle bundle);

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract void m2525(View view, ViewGroup.LayoutParams layoutParams);

    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract void m2526();

    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract void m2527(Bundle bundle);

    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract boolean m2528(int i);

    /* renamed from: ʾ  reason: contains not printable characters */
    public abstract void m2529();

    /* renamed from: ʿ  reason: contains not printable characters */
    public abstract void m2530();

    /* renamed from: ˆ  reason: contains not printable characters */
    public abstract void m2531();

    /* renamed from: ˈ  reason: contains not printable characters */
    public abstract void m2532();

    /* renamed from: ˉ  reason: contains not printable characters */
    public abstract C0448.C0449 m2533();

    /* renamed from: ˊ  reason: contains not printable characters */
    public abstract void m2534();

    /* renamed from: ˋ  reason: contains not printable characters */
    public abstract boolean m2535();

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0462 m2508(Activity activity, C0461 r2) {
        return m2510(activity, activity.getWindow(), r2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0462 m2509(Dialog dialog, C0461 r2) {
        return m2510(dialog.getContext(), dialog.getWindow(), r2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static C0462 m2510(Context context, Window window, C0461 r4) {
        if (Build.VERSION.SDK_INT >= 24) {
            return new C0466(context, window, r4);
        }
        if (Build.VERSION.SDK_INT >= 23) {
            return new C0472(context, window, r4);
        }
        if (Build.VERSION.SDK_INT >= 14) {
            return new C0469(context, window, r4);
        }
        if (Build.VERSION.SDK_INT >= 11) {
            return new C0468(context, window, r4);
        }
        return new C0474(context, window, r4);
    }

    C0462() {
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public static int m2511() {
        return f1416;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public static boolean m2512() {
        return f1417;
    }
}
