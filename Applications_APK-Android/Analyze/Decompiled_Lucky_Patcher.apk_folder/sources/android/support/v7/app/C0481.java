package android.support.v7.app;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.ˈ.C0331;
import android.support.v4.ˉ.C0414;
import android.support.v7.view.C0532;
import android.support.v7.widget.C0589;
import android.support.v7.widget.C0636;
import android.support.v7.widget.C0643;
import android.support.v7.widget.C0647;
import android.support.v7.widget.C0651;
import android.support.v7.widget.C0667;
import android.support.v7.widget.C0669;
import android.support.v7.widget.C0674;
import android.support.v7.widget.C0676;
import android.support.v7.widget.C0677;
import android.support.v7.widget.C0684;
import android.support.v7.widget.C0687;
import android.support.v7.widget.C0689;
import android.support.v7.widget.C0723;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

/* renamed from: android.support.v7.app.ـ  reason: contains not printable characters */
/* compiled from: AppCompatViewInflater */
class C0481 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final Class<?>[] f1512 = {Context.class, AttributeSet.class};

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final int[] f1513 = {16843375};

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final String[] f1514 = {"android.widget.", "android.view.", "android.webkit."};

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final Map<String, Constructor<? extends View>> f1515 = new C0331();

    /* renamed from: ʿ  reason: contains not printable characters */
    private final Object[] f1516 = new Object[2];

    C0481() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final View m2674(View view, String str, Context context, AttributeSet attributeSet, boolean z, boolean z2, boolean z3, boolean z4) {
        Context context2 = (!z || view == null) ? context : view.getContext();
        if (z2 || z3) {
            context2 = m2670(context2, attributeSet, z2, z3);
        }
        if (z4) {
            context2 = C0589.m3735(context2);
        }
        View view2 = null;
        char c = 65535;
        switch (str.hashCode()) {
            case -1946472170:
                if (str.equals("RatingBar")) {
                    c = 11;
                    break;
                }
                break;
            case -1455429095:
                if (str.equals("CheckedTextView")) {
                    c = 8;
                    break;
                }
                break;
            case -1346021293:
                if (str.equals("MultiAutoCompleteTextView")) {
                    c = 10;
                    break;
                }
                break;
            case -938935918:
                if (str.equals("TextView")) {
                    c = 0;
                    break;
                }
                break;
            case -937446323:
                if (str.equals("ImageButton")) {
                    c = 5;
                    break;
                }
                break;
            case -658531749:
                if (str.equals("SeekBar")) {
                    c = 12;
                    break;
                }
                break;
            case -339785223:
                if (str.equals("Spinner")) {
                    c = 4;
                    break;
                }
                break;
            case 776382189:
                if (str.equals("RadioButton")) {
                    c = 7;
                    break;
                }
                break;
            case 1125864064:
                if (str.equals("ImageView")) {
                    c = 1;
                    break;
                }
                break;
            case 1413872058:
                if (str.equals("AutoCompleteTextView")) {
                    c = 9;
                    break;
                }
                break;
            case 1601505219:
                if (str.equals("CheckBox")) {
                    c = 6;
                    break;
                }
                break;
            case 1666676343:
                if (str.equals("EditText")) {
                    c = 3;
                    break;
                }
                break;
            case 2001146706:
                if (str.equals("Button")) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                view2 = new C0677(context2, attributeSet);
                break;
            case 1:
                view2 = new C0674(context2, attributeSet);
                break;
            case 2:
                view2 = new C0643(context2, attributeSet);
                break;
            case 3:
                view2 = new C0667(context2, attributeSet);
                break;
            case 4:
                view2 = new C0723(context2, attributeSet);
                break;
            case 5:
                view2 = new C0669(context2, attributeSet);
                break;
            case 6:
                view2 = new C0647(context2, attributeSet);
                break;
            case 7:
                view2 = new C0684(context2, attributeSet);
                break;
            case 8:
                view2 = new C0651(context2, attributeSet);
                break;
            case 9:
                view2 = new C0636(context2, attributeSet);
                break;
            case 10:
                view2 = new C0676(context2, attributeSet);
                break;
            case 11:
                view2 = new C0687(context2, attributeSet);
                break;
            case 12:
                view2 = new C0689(context2, attributeSet);
                break;
        }
        if (view2 == null && context != context2) {
            view2 = m2671(context2, str, attributeSet);
        }
        if (view2 != null) {
            m2673(view2, attributeSet);
        }
        return view2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private View m2671(Context context, String str, AttributeSet attributeSet) {
        if (str.equals("view")) {
            str = attributeSet.getAttributeValue(null, "class");
        }
        try {
            this.f1516[0] = context;
            this.f1516[1] = attributeSet;
            if (-1 == str.indexOf(46)) {
                for (String r3 : f1514) {
                    View r32 = m2672(context, str, r3);
                    if (r32 != null) {
                        return r32;
                    }
                }
                Object[] objArr = this.f1516;
                objArr[0] = null;
                objArr[1] = null;
                return null;
            }
            View r5 = m2672(context, str, (String) null);
            Object[] objArr2 = this.f1516;
            objArr2[0] = null;
            objArr2[1] = null;
            return r5;
        } catch (Exception unused) {
            return null;
        } finally {
            Object[] objArr3 = this.f1516;
            objArr3[0] = null;
            objArr3[1] = null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m2673(View view, AttributeSet attributeSet) {
        Context context = view.getContext();
        if (!(context instanceof ContextWrapper)) {
            return;
        }
        if (Build.VERSION.SDK_INT < 15 || C0414.m2258(view)) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, f1513);
            String string = obtainStyledAttributes.getString(0);
            if (string != null) {
                view.setOnClickListener(new C0482(view, string));
            }
            obtainStyledAttributes.recycle();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private View m2672(Context context, String str, String str2) {
        String str3;
        Constructor<? extends U> constructor = f1515.get(str);
        if (constructor == null) {
            try {
                ClassLoader classLoader = context.getClassLoader();
                if (str2 != null) {
                    str3 = str2 + str;
                } else {
                    str3 = str;
                }
                constructor = classLoader.loadClass(str3).asSubclass(View.class).getConstructor(f1512);
                f1515.put(str, constructor);
            } catch (Exception unused) {
                return null;
            }
        }
        constructor.setAccessible(true);
        return (View) constructor.newInstance(this.f1516);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Context m2670(Context context, AttributeSet attributeSet, boolean z, boolean z2) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0727.C0737.View, 0, 0);
        int resourceId = z ? obtainStyledAttributes.getResourceId(C0727.C0737.View_android_theme, 0) : 0;
        if (z2 && resourceId == 0 && (resourceId = obtainStyledAttributes.getResourceId(C0727.C0737.View_theme, 0)) != 0) {
            Log.i("AppCompatViewInflater", "app:theme is now deprecated. Please move to using android:theme instead.");
        }
        obtainStyledAttributes.recycle();
        if (resourceId != 0) {
            return (!(context instanceof C0532) || ((C0532) context).m3105() != resourceId) ? new C0532(context, resourceId) : context;
        }
        return context;
    }

    /* renamed from: android.support.v7.app.ـ$ʻ  reason: contains not printable characters */
    /* compiled from: AppCompatViewInflater */
    private static class C0482 implements View.OnClickListener {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final View f1517;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final String f1518;

        /* renamed from: ʽ  reason: contains not printable characters */
        private Method f1519;

        /* renamed from: ʾ  reason: contains not printable characters */
        private Context f1520;

        public C0482(View view, String str) {
            this.f1517 = view;
            this.f1518 = str;
        }

        public void onClick(View view) {
            if (this.f1519 == null) {
                m2675(this.f1517.getContext(), this.f1518);
            }
            try {
                this.f1519.invoke(this.f1520, view);
            } catch (IllegalAccessException e) {
                throw new IllegalStateException("Could not execute non-public method for android:onClick", e);
            } catch (InvocationTargetException e2) {
                throw new IllegalStateException("Could not execute method for android:onClick", e2);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m2675(Context context, String str) {
            String str2;
            Method method;
            while (context != null) {
                try {
                    if (!context.isRestricted() && (method = context.getClass().getMethod(this.f1518, View.class)) != null) {
                        this.f1519 = method;
                        this.f1520 = context;
                        return;
                    }
                } catch (NoSuchMethodException unused) {
                }
                context = context instanceof ContextWrapper ? ((ContextWrapper) context).getBaseContext() : null;
            }
            int id = this.f1517.getId();
            if (id == -1) {
                str2 = BuildConfig.FLAVOR;
            } else {
                str2 = " with id '" + this.f1517.getContext().getResources().getResourceEntryName(id) + "'";
            }
            throw new IllegalStateException("Could not find method " + this.f1518 + "(View) in a parent or ancestor Context for android:onClick " + "attribute defined on view " + this.f1517.getClass() + str2);
        }
    }
}
