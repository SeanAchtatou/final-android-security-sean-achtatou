package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.ʻ.C0270;
import android.support.v4.ˉ.C0397;
import android.support.v4.ˉ.C0412;
import android.support.v4.ˉ.C0414;
import android.support.v4.ˉ.C0434;
import android.support.v4.ˉ.C0436;
import android.support.v4.ˉ.C0437;
import android.support.v4.ˉ.C0439;
import android.support.v7.view.C0529;
import android.support.v7.view.C0532;
import android.support.v7.view.menu.C0501;
import android.support.v7.view.menu.C0504;
import android.support.v7.view.menu.C0518;
import android.support.v7.view.menu.C0520;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.C0600;
import android.support.v7.widget.C0607;
import android.support.v7.widget.C0609;
import android.support.v7.widget.C0637;
import android.support.v7.widget.C0656;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.Toolbar;
import android.support.v7.ʻ.C0727;
import android.support.v7.ʼ.ʻ.C0739;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: android.support.v7.app.ˑ  reason: contains not printable characters */
/* compiled from: AppCompatDelegateImplV9 */
class C0474 extends C0463 implements C0504.C0505, LayoutInflater.Factory2 {

    /* renamed from: ᵔ  reason: contains not printable characters */
    private static final boolean f1453 = (Build.VERSION.SDK_INT < 21);

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private boolean f1454;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private C0478[] f1455;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private boolean f1456;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private boolean f1457;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private C0478 f1458;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private boolean f1459;

    /* renamed from: ˈˈ  reason: contains not printable characters */
    private Rect f1460;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private Rect f1461;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private C0481 f1462;

    /* renamed from: ˑ  reason: contains not printable characters */
    C0529 f1463;

    /* renamed from: י  reason: contains not printable characters */
    ActionBarContextView f1464;

    /* renamed from: ـ  reason: contains not printable characters */
    PopupWindow f1465;

    /* renamed from: ــ  reason: contains not printable characters */
    private final Runnable f1466 = new Runnable() {
        public void run() {
            if ((C0474.this.f1472 & 1) != 0) {
                C0474.this.m2636(0);
            }
            if ((C0474.this.f1472 & 4096) != 0) {
                C0474.this.m2636(108);
            }
            C0474 r0 = C0474.this;
            r0.f1470 = false;
            r0.f1472 = 0;
        }
    };

    /* renamed from: ٴ  reason: contains not printable characters */
    Runnable f1467;

    /* renamed from: ᐧ  reason: contains not printable characters */
    C0434 f1468 = null;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private View f1469;

    /* renamed from: ᴵ  reason: contains not printable characters */
    boolean f1470;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private boolean f1471;

    /* renamed from: ᵎ  reason: contains not printable characters */
    int f1472;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private C0609 f1473;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private C0475 f1474;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private C0479 f1475;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f1476;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private ViewGroup f1477;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private TextView f1478;

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2617(ViewGroup viewGroup) {
    }

    C0474(Context context, Window window, C0461 r3) {
        super(context, window, r3);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2611(Bundle bundle) {
        if ((this.f1423 instanceof Activity) && C0270.m1656((Activity) this.f1423) != null) {
            C0444 r2 = m2553();
            if (r2 == null) {
                this.f1459 = true;
            } else {
                r2.m2447(true);
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2624(Bundle bundle) {
        m2600();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m2640() {
        m2600();
        if (this.f1428 && this.f1426 == null) {
            if (this.f1423 instanceof Activity) {
                this.f1426 = new C0491((Activity) this.f1423, this.f1429);
            } else if (this.f1423 instanceof Dialog) {
                this.f1426 = new C0491((Dialog) this.f1423);
            }
            if (this.f1426 != null) {
                this.f1426.m2447(this.f1459);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2614(Toolbar toolbar) {
        if (this.f1423 instanceof Activity) {
            C0444 r0 = m2536();
            if (!(r0 instanceof C0491)) {
                this.f1427 = null;
                if (r0 != null) {
                    r0.m2454();
                }
                if (toolbar != null) {
                    C0484 r02 = new C0484(toolbar, ((Activity) this.f1423).getTitle(), this.f1424);
                    this.f1426 = r02;
                    this.f1422.setCallback(r02.m2700());
                } else {
                    this.f1426 = null;
                    this.f1422.setCallback(this.f1424);
                }
                m2635();
                return;
            }
            throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public <T extends View> T m2606(int i) {
        m2600();
        return this.f1422.findViewById(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2610(Configuration configuration) {
        C0444 r0;
        if (this.f1428 && this.f1476 && (r0 = m2536()) != null) {
            r0.m2439(configuration);
        }
        C0656.m4164().m4186(this.f1421);
        m2551();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m2632() {
        C0444 r0 = m2536();
        if (r0 != null) {
            r0.m2449(false);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m2633() {
        C0444 r0 = m2536();
        if (r0 != null) {
            r0.m2449(true);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2615(View view) {
        m2600();
        ViewGroup viewGroup = (ViewGroup) this.f1477.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.f1423.onContentChanged();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2623(int i) {
        m2600();
        ViewGroup viewGroup = (ViewGroup) this.f1477.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.f1421).inflate(i, viewGroup);
        this.f1423.onContentChanged();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2616(View view, ViewGroup.LayoutParams layoutParams) {
        m2600();
        ViewGroup viewGroup = (ViewGroup) this.f1477.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.f1423.onContentChanged();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2626(View view, ViewGroup.LayoutParams layoutParams) {
        m2600();
        ((ViewGroup) this.f1477.findViewById(16908290)).addView(view, layoutParams);
        this.f1423.onContentChanged();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public void m2638() {
        if (this.f1470) {
            this.f1422.getDecorView().removeCallbacks(this.f1466);
        }
        super.m2549();
        if (this.f1426 != null) {
            this.f1426.m2454();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
     arg types: [int, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ */
    /* renamed from: ﹶ  reason: contains not printable characters */
    private void m2600() {
        if (!this.f1476) {
            this.f1477 = m2601();
            CharSequence r0 = m2558();
            if (!TextUtils.isEmpty(r0)) {
                m2627(r0);
            }
            m2602();
            m2617(this.f1477);
            this.f1476 = true;
            C0478 r02 = m2603(0, false);
            if (m2556()) {
                return;
            }
            if (r02 == null || r02.f1500 == null) {
                m2595(108);
            }
        }
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private ViewGroup m2601() {
        ViewGroup viewGroup;
        Context context;
        TypedArray obtainStyledAttributes = this.f1421.obtainStyledAttributes(C0727.C0737.AppCompatTheme);
        if (obtainStyledAttributes.hasValue(C0727.C0737.AppCompatTheme_windowActionBar)) {
            if (obtainStyledAttributes.getBoolean(C0727.C0737.AppCompatTheme_windowNoTitle, false)) {
                m2630(1);
            } else if (obtainStyledAttributes.getBoolean(C0727.C0737.AppCompatTheme_windowActionBar, false)) {
                m2630(108);
            }
            if (obtainStyledAttributes.getBoolean(C0727.C0737.AppCompatTheme_windowActionBarOverlay, false)) {
                m2630(109);
            }
            if (obtainStyledAttributes.getBoolean(C0727.C0737.AppCompatTheme_windowActionModeOverlay, false)) {
                m2630(10);
            }
            this.f1431 = obtainStyledAttributes.getBoolean(C0727.C0737.AppCompatTheme_android_windowIsFloating, false);
            obtainStyledAttributes.recycle();
            this.f1422.getDecorView();
            LayoutInflater from = LayoutInflater.from(this.f1421);
            if (this.f1432) {
                if (this.f1430) {
                    viewGroup = (ViewGroup) from.inflate(C0727.C0734.abc_screen_simple_overlay_action_mode, (ViewGroup) null);
                } else {
                    viewGroup = (ViewGroup) from.inflate(C0727.C0734.abc_screen_simple, (ViewGroup) null);
                }
                if (Build.VERSION.SDK_INT >= 21) {
                    C0414.m2225(viewGroup, new C0412() {
                        /* renamed from: ʻ  reason: contains not printable characters */
                        public C0439 m2645(View view, C0439 r6) {
                            int r0 = r6.m2399();
                            int r1 = C0474.this.m2637(r0);
                            if (r0 != r1) {
                                r6 = r6.m2398(r6.m2397(), r1, r6.m2400(), r6.m2401());
                            }
                            return C0414.m2216(view, r6);
                        }
                    });
                } else {
                    ((C0637) viewGroup).setOnFitSystemWindowsListener(new C0637.C0638() {
                        /* renamed from: ʻ  reason: contains not printable characters */
                        public void m2646(Rect rect) {
                            rect.top = C0474.this.m2637(rect.top);
                        }
                    });
                }
            } else if (this.f1431) {
                viewGroup = (ViewGroup) from.inflate(C0727.C0734.abc_dialog_title_material, (ViewGroup) null);
                this.f1429 = false;
                this.f1428 = false;
            } else if (this.f1428) {
                TypedValue typedValue = new TypedValue();
                this.f1421.getTheme().resolveAttribute(C0727.C0728.actionBarTheme, typedValue, true);
                if (typedValue.resourceId != 0) {
                    context = new C0532(this.f1421, typedValue.resourceId);
                } else {
                    context = this.f1421;
                }
                viewGroup = (ViewGroup) LayoutInflater.from(context).inflate(C0727.C0734.abc_screen_toolbar, (ViewGroup) null);
                this.f1473 = (C0609) viewGroup.findViewById(C0727.C0733.decor_content_parent);
                this.f1473.setWindowCallback(m2557());
                if (this.f1429) {
                    this.f1473.m3860(109);
                }
                if (this.f1471) {
                    this.f1473.m3860(2);
                }
                if (this.f1454) {
                    this.f1473.m3860(5);
                }
            } else {
                viewGroup = null;
            }
            if (viewGroup != null) {
                if (this.f1473 == null) {
                    this.f1478 = (TextView) viewGroup.findViewById(C0727.C0733.title);
                }
                C0607.m3859(viewGroup);
                ContentFrameLayout contentFrameLayout = (ContentFrameLayout) viewGroup.findViewById(C0727.C0733.action_bar_activity_content);
                ViewGroup viewGroup2 = (ViewGroup) this.f1422.findViewById(16908290);
                if (viewGroup2 != null) {
                    while (viewGroup2.getChildCount() > 0) {
                        View childAt = viewGroup2.getChildAt(0);
                        viewGroup2.removeViewAt(0);
                        contentFrameLayout.addView(childAt);
                    }
                    viewGroup2.setId(-1);
                    contentFrameLayout.setId(16908290);
                    if (viewGroup2 instanceof FrameLayout) {
                        ((FrameLayout) viewGroup2).setForeground(null);
                    }
                }
                this.f1422.setContentView(viewGroup);
                contentFrameLayout.setAttachListener(new ContentFrameLayout.C0550() {
                    /* renamed from: ʻ  reason: contains not printable characters */
                    public void m2647() {
                    }

                    /* renamed from: ʼ  reason: contains not printable characters */
                    public void m2648() {
                        C0474.this.m2644();
                    }
                });
                return viewGroup;
            }
            throw new IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + this.f1428 + ", windowActionBarOverlay: " + this.f1429 + ", android:windowIsFloating: " + this.f1431 + ", windowActionModeOverlay: " + this.f1430 + ", windowNoTitle: " + this.f1432 + " }");
        }
        obtainStyledAttributes.recycle();
        throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
    }

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private void m2602() {
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) this.f1477.findViewById(16908290);
        View decorView = this.f1422.getDecorView();
        contentFrameLayout.m3236(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
        TypedArray obtainStyledAttributes = this.f1421.obtainStyledAttributes(C0727.C0737.AppCompatTheme);
        obtainStyledAttributes.getValue(C0727.C0737.AppCompatTheme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        obtainStyledAttributes.getValue(C0727.C0737.AppCompatTheme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        if (obtainStyledAttributes.hasValue(C0727.C0737.AppCompatTheme_windowFixedWidthMajor)) {
            obtainStyledAttributes.getValue(C0727.C0737.AppCompatTheme_windowFixedWidthMajor, contentFrameLayout.getFixedWidthMajor());
        }
        if (obtainStyledAttributes.hasValue(C0727.C0737.AppCompatTheme_windowFixedWidthMinor)) {
            obtainStyledAttributes.getValue(C0727.C0737.AppCompatTheme_windowFixedWidthMinor, contentFrameLayout.getFixedWidthMinor());
        }
        if (obtainStyledAttributes.hasValue(C0727.C0737.AppCompatTheme_windowFixedHeightMajor)) {
            obtainStyledAttributes.getValue(C0727.C0737.AppCompatTheme_windowFixedHeightMajor, contentFrameLayout.getFixedHeightMajor());
        }
        if (obtainStyledAttributes.hasValue(C0727.C0737.AppCompatTheme_windowFixedHeightMinor)) {
            obtainStyledAttributes.getValue(C0727.C0737.AppCompatTheme_windowFixedHeightMinor, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        contentFrameLayout.requestLayout();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m2630(int i) {
        int r5 = m2598(i);
        if (this.f1432 && r5 == 108) {
            return false;
        }
        if (this.f1428 && r5 == 1) {
            this.f1428 = false;
        }
        if (r5 == 1) {
            m2599();
            this.f1432 = true;
            return true;
        } else if (r5 == 2) {
            m2599();
            this.f1471 = true;
            return true;
        } else if (r5 == 5) {
            m2599();
            this.f1454 = true;
            return true;
        } else if (r5 == 10) {
            m2599();
            this.f1430 = true;
            return true;
        } else if (r5 == 108) {
            m2599();
            this.f1428 = true;
            return true;
        } else if (r5 != 109) {
            return this.f1422.requestFeature(r5);
        } else {
            m2599();
            this.f1429 = true;
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2627(CharSequence charSequence) {
        C0609 r0 = this.f1473;
        if (r0 != null) {
            r0.setWindowTitle(charSequence);
        } else if (m2553() != null) {
            m2553().m2441(charSequence);
        } else {
            TextView textView = this.f1478;
            if (textView != null) {
                textView.setText(charSequence);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
     arg types: [int, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
     arg types: [android.support.v7.app.ˑ$ʾ, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2609(int i, Menu menu) {
        if (i == 108) {
            C0444 r2 = m2536();
            if (r2 != null) {
                r2.m2451(false);
            }
        } else if (i == 0) {
            C0478 r22 = m2603(i, true);
            if (r22.f1505) {
                m2612(r22, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m2629(int i, Menu menu) {
        if (i != 108) {
            return false;
        }
        C0444 r1 = m2536();
        if (r1 != null) {
            r1.m2451(true);
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2619(C0504 r3, MenuItem menuItem) {
        C0478 r32;
        Window.Callback r0 = m2557();
        if (r0 == null || m2556() || (r32 = m2604((Menu) r3.m2930())) == null) {
            return false;
        }
        return r0.onMenuItemSelected(r32.f1491, menuItem);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
     arg types: [android.support.v7.view.menu.ˉ, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2613(C0504 r2) {
        m2588(r2, true);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0529 m2605(C0529.C0530 r3) {
        if (r3 != null) {
            C0529 r0 = this.f1463;
            if (r0 != null) {
                r0.m3089();
            }
            C0476 r02 = new C0476(r3);
            C0444 r32 = m2536();
            if (r32 != null) {
                this.f1463 = r32.m2436(r02);
                if (!(this.f1463 == null || this.f1425 == null)) {
                    this.f1425.onSupportActionModeStarted(this.f1463);
                }
            }
            if (this.f1463 == null) {
                this.f1463 = m2621(r02);
            }
            return this.f1463;
        }
        throw new IllegalArgumentException("ActionMode callback can not be null.");
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m2635() {
        C0444 r0 = m2536();
        if (r0 == null || !r0.m2452()) {
            m2595(0);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002d  */
    /* renamed from: ʼ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.support.v7.view.C0529 m2621(android.support.v7.view.C0529.C0530 r8) {
        /*
            r7 = this;
            r7.m2642()
            android.support.v7.view.ʼ r0 = r7.f1463
            if (r0 == 0) goto L_0x000a
            r0.m3089()
        L_0x000a:
            boolean r0 = r8 instanceof android.support.v7.app.C0474.C0476
            if (r0 != 0) goto L_0x0014
            android.support.v7.app.ˑ$ʼ r0 = new android.support.v7.app.ˑ$ʼ
            r0.<init>(r8)
            r8 = r0
        L_0x0014:
            android.support.v7.app.ˆ r0 = r7.f1425
            r1 = 0
            if (r0 == 0) goto L_0x0026
            boolean r0 = r7.m2556()
            if (r0 != 0) goto L_0x0026
            android.support.v7.app.ˆ r0 = r7.f1425     // Catch:{ AbstractMethodError -> 0x0026 }
            android.support.v7.view.ʼ r0 = r0.onWindowStartingSupportActionMode(r8)     // Catch:{ AbstractMethodError -> 0x0026 }
            goto L_0x0027
        L_0x0026:
            r0 = r1
        L_0x0027:
            if (r0 == 0) goto L_0x002d
            r7.f1463 = r0
            goto L_0x016a
        L_0x002d:
            android.support.v7.widget.ActionBarContextView r0 = r7.f1464
            r2 = 0
            r3 = 1
            if (r0 != 0) goto L_0x00da
            boolean r0 = r7.f1431
            if (r0 == 0) goto L_0x00bb
            android.util.TypedValue r0 = new android.util.TypedValue
            r0.<init>()
            android.content.Context r4 = r7.f1421
            android.content.res.Resources$Theme r4 = r4.getTheme()
            int r5 = android.support.v7.ʻ.C0727.C0728.actionBarTheme
            r4.resolveAttribute(r5, r0, r3)
            int r5 = r0.resourceId
            if (r5 == 0) goto L_0x006c
            android.content.Context r5 = r7.f1421
            android.content.res.Resources r5 = r5.getResources()
            android.content.res.Resources$Theme r5 = r5.newTheme()
            r5.setTo(r4)
            int r4 = r0.resourceId
            r5.applyStyle(r4, r3)
            android.support.v7.view.ʾ r4 = new android.support.v7.view.ʾ
            android.content.Context r6 = r7.f1421
            r4.<init>(r6, r2)
            android.content.res.Resources$Theme r6 = r4.getTheme()
            r6.setTo(r5)
            goto L_0x006e
        L_0x006c:
            android.content.Context r4 = r7.f1421
        L_0x006e:
            android.support.v7.widget.ActionBarContextView r5 = new android.support.v7.widget.ActionBarContextView
            r5.<init>(r4)
            r7.f1464 = r5
            android.widget.PopupWindow r5 = new android.widget.PopupWindow
            int r6 = android.support.v7.ʻ.C0727.C0728.actionModePopupWindowStyle
            r5.<init>(r4, r1, r6)
            r7.f1465 = r5
            android.widget.PopupWindow r5 = r7.f1465
            r6 = 2
            android.support.v4.widget.C0173.m1044(r5, r6)
            android.widget.PopupWindow r5 = r7.f1465
            android.support.v7.widget.ActionBarContextView r6 = r7.f1464
            r5.setContentView(r6)
            android.widget.PopupWindow r5 = r7.f1465
            r6 = -1
            r5.setWidth(r6)
            android.content.res.Resources$Theme r5 = r4.getTheme()
            int r6 = android.support.v7.ʻ.C0727.C0728.actionBarSize
            r5.resolveAttribute(r6, r0, r3)
            int r0 = r0.data
            android.content.res.Resources r4 = r4.getResources()
            android.util.DisplayMetrics r4 = r4.getDisplayMetrics()
            int r0 = android.util.TypedValue.complexToDimensionPixelSize(r0, r4)
            android.support.v7.widget.ActionBarContextView r4 = r7.f1464
            r4.setContentHeight(r0)
            android.widget.PopupWindow r0 = r7.f1465
            r4 = -2
            r0.setHeight(r4)
            android.support.v7.app.ˑ$5 r0 = new android.support.v7.app.ˑ$5
            r0.<init>()
            r7.f1467 = r0
            goto L_0x00da
        L_0x00bb:
            android.view.ViewGroup r0 = r7.f1477
            int r4 = android.support.v7.ʻ.C0727.C0733.action_mode_bar_stub
            android.view.View r0 = r0.findViewById(r4)
            android.support.v7.widget.ViewStubCompat r0 = (android.support.v7.widget.ViewStubCompat) r0
            if (r0 == 0) goto L_0x00da
            android.content.Context r4 = r7.m2554()
            android.view.LayoutInflater r4 = android.view.LayoutInflater.from(r4)
            r0.setLayoutInflater(r4)
            android.view.View r0 = r0.m3622()
            android.support.v7.widget.ActionBarContextView r0 = (android.support.v7.widget.ActionBarContextView) r0
            r7.f1464 = r0
        L_0x00da:
            android.support.v7.widget.ActionBarContextView r0 = r7.f1464
            if (r0 == 0) goto L_0x016a
            r7.m2642()
            android.support.v7.widget.ActionBarContextView r0 = r7.f1464
            r0.m3159()
            android.support.v7.view.ʿ r0 = new android.support.v7.view.ʿ
            android.support.v7.widget.ActionBarContextView r4 = r7.f1464
            android.content.Context r4 = r4.getContext()
            android.support.v7.widget.ActionBarContextView r5 = r7.f1464
            android.widget.PopupWindow r6 = r7.f1465
            if (r6 != 0) goto L_0x00f5
            goto L_0x00f6
        L_0x00f5:
            r3 = 0
        L_0x00f6:
            r0.<init>(r4, r5, r8, r3)
            android.view.Menu r3 = r0.m3086()
            boolean r8 = r8.m3098(r0, r3)
            if (r8 == 0) goto L_0x0168
            r0.m3090()
            android.support.v7.widget.ActionBarContextView r8 = r7.f1464
            r8.m3156(r0)
            r7.f1463 = r0
            boolean r8 = r7.m2641()
            r0 = 1065353216(0x3f800000, float:1.0)
            if (r8 == 0) goto L_0x0132
            android.support.v7.widget.ActionBarContextView r8 = r7.f1464
            r1 = 0
            r8.setAlpha(r1)
            android.support.v7.widget.ActionBarContextView r8 = r7.f1464
            android.support.v4.ˉ.ᵢ r8 = android.support.v4.ˉ.C0414.m2242(r8)
            android.support.v4.ˉ.ᵢ r8 = r8.m2376(r0)
            r7.f1468 = r8
            android.support.v4.ˉ.ᵢ r8 = r7.f1468
            android.support.v7.app.ˑ$6 r0 = new android.support.v7.app.ˑ$6
            r0.<init>()
            r8.m2378(r0)
            goto L_0x0158
        L_0x0132:
            android.support.v7.widget.ActionBarContextView r8 = r7.f1464
            r8.setAlpha(r0)
            android.support.v7.widget.ActionBarContextView r8 = r7.f1464
            r8.setVisibility(r2)
            android.support.v7.widget.ActionBarContextView r8 = r7.f1464
            r0 = 32
            r8.sendAccessibilityEvent(r0)
            android.support.v7.widget.ActionBarContextView r8 = r7.f1464
            android.view.ViewParent r8 = r8.getParent()
            boolean r8 = r8 instanceof android.view.View
            if (r8 == 0) goto L_0x0158
            android.support.v7.widget.ActionBarContextView r8 = r7.f1464
            android.view.ViewParent r8 = r8.getParent()
            android.view.View r8 = (android.view.View) r8
            android.support.v4.ˉ.C0414.m2246(r8)
        L_0x0158:
            android.widget.PopupWindow r8 = r7.f1465
            if (r8 == 0) goto L_0x016a
            android.view.Window r8 = r7.f1422
            android.view.View r8 = r8.getDecorView()
            java.lang.Runnable r0 = r7.f1467
            r8.post(r0)
            goto L_0x016a
        L_0x0168:
            r7.f1463 = r1
        L_0x016a:
            android.support.v7.view.ʼ r8 = r7.f1463
            if (r8 == 0) goto L_0x0179
            android.support.v7.app.ˆ r8 = r7.f1425
            if (r8 == 0) goto L_0x0179
            android.support.v7.app.ˆ r8 = r7.f1425
            android.support.v7.view.ʼ r0 = r7.f1463
            r8.onSupportActionModeStarted(r0)
        L_0x0179:
            android.support.v7.view.ʼ r8 = r7.f1463
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.C0474.m2621(android.support.v7.view.ʼ$ʻ):android.support.v7.view.ʼ");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵔ  reason: contains not printable characters */
    public final boolean m2641() {
        ViewGroup viewGroup;
        return this.f1476 && (viewGroup = this.f1477) != null && C0414.m2255(viewGroup);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵢ  reason: contains not printable characters */
    public void m2642() {
        C0434 r0 = this.f1468;
        if (r0 != null) {
            r0.m2383();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ⁱ  reason: contains not printable characters */
    public boolean m2643() {
        C0529 r0 = this.f1463;
        if (r0 != null) {
            r0.m3089();
            return true;
        }
        C0444 r02 = m2536();
        if (r02 == null || !r02.m2453()) {
            return false;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
     arg types: [int, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ */
    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2618(int i, KeyEvent keyEvent) {
        C0444 r0 = m2536();
        if (r0 != null && r0.m2443(i, keyEvent)) {
            return true;
        }
        C0478 r4 = this.f1458;
        if (r4 == null || !m2590(r4, keyEvent.getKeyCode(), keyEvent, 1)) {
            if (this.f1458 == null) {
                C0478 r42 = m2603(0, true);
                m2593(r42, keyEvent);
                boolean r5 = m2590(r42, keyEvent.getKeyCode(), keyEvent, 1);
                r42.f1503 = false;
                if (r5) {
                    return true;
                }
            }
            return false;
        }
        C0478 r43 = this.f1458;
        if (r43 != null) {
            r43.f1504 = true;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2620(KeyEvent keyEvent) {
        boolean z = true;
        if (keyEvent.getKeyCode() == 82 && this.f1423.dispatchKeyEvent(keyEvent)) {
            return true;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() != 0) {
            z = false;
        }
        return z ? m2631(keyCode, keyEvent) : m2628(keyCode, keyEvent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
     arg types: [int, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
     arg types: [android.support.v7.app.ˑ$ʾ, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m2628(int i, KeyEvent keyEvent) {
        if (i == 4) {
            boolean z = this.f1457;
            this.f1457 = false;
            C0478 r5 = m2603(0, false);
            if (r5 != null && r5.f1505) {
                if (!z) {
                    m2612(r5, true);
                }
                return true;
            } else if (m2643()) {
                return true;
            }
        } else if (i == 82) {
            m2597(0, keyEvent);
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m2631(int i, KeyEvent keyEvent) {
        boolean z = true;
        if (i == 4) {
            if ((keyEvent.getFlags() & 128) == 0) {
                z = false;
            }
            this.f1457 = z;
        } else if (i == 82) {
            m2596(0, keyEvent);
            return true;
        }
        if (Build.VERSION.SDK_INT < 11) {
            m2618(i, keyEvent);
        }
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public View m2622(View view, String str, Context context, AttributeSet attributeSet) {
        boolean z;
        if (this.f1462 == null) {
            this.f1462 = new C0481();
        }
        boolean z2 = false;
        if (f1453) {
            if (!(attributeSet instanceof XmlPullParser)) {
                z2 = m2591((ViewParent) view);
            } else if (((XmlPullParser) attributeSet).getDepth() > 1) {
                z2 = true;
            }
            z = z2;
        } else {
            z = false;
        }
        return this.f1462.m2674(view, str, context, attributeSet, z, f1453, true, C0600.m3819());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m2591(ViewParent viewParent) {
        if (viewParent == null) {
            return false;
        }
        View decorView = this.f1422.getDecorView();
        while (viewParent != null) {
            if (viewParent == decorView || !(viewParent instanceof View) || C0414.m2257((View) viewParent)) {
                return false;
            }
            viewParent = viewParent.getParent();
        }
        return true;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public void m2639() {
        LayoutInflater from = LayoutInflater.from(this.f1421);
        if (from.getFactory() == null) {
            C0397.m2158(from, this);
        } else if (!(from.getFactory2() instanceof C0474)) {
            Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    public final View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        View r0 = m2607(view, str, context, attributeSet);
        if (r0 != null) {
            return r0;
        }
        return m2622(view, str, context, attributeSet);
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public View m2607(View view, String str, Context context, AttributeSet attributeSet) {
        View onCreateView;
        if (!(this.f1423 instanceof LayoutInflater.Factory) || (onCreateView = ((LayoutInflater.Factory) this.f1423).onCreateView(str, context, attributeSet)) == null) {
            return null;
        }
        return onCreateView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
     arg types: [android.support.v7.app.ˑ$ʾ, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m2587(C0478 r14, KeyEvent keyEvent) {
        int i;
        ViewGroup.LayoutParams layoutParams;
        if (!r14.f1505 && !m2556()) {
            if (r14.f1491 == 0) {
                Context context = this.f1421;
                boolean z = (context.getResources().getConfiguration().screenLayout & 15) == 4;
                boolean z2 = context.getApplicationInfo().targetSdkVersion >= 11;
                if (z && z2) {
                    return;
                }
            }
            Window.Callback r0 = m2557();
            if (r0 == null || r0.onMenuOpened(r14.f1491, r14.f1500)) {
                WindowManager windowManager = (WindowManager) this.f1421.getSystemService("window");
                if (windowManager != null && m2593(r14, keyEvent)) {
                    if (r14.f1497 == null || r14.f1507) {
                        if (r14.f1497 == null) {
                            if (!m2589(r14) || r14.f1497 == null) {
                                return;
                            }
                        } else if (r14.f1507 && r14.f1497.getChildCount() > 0) {
                            r14.f1497.removeAllViews();
                        }
                        if (m2594(r14) && r14.m2664()) {
                            ViewGroup.LayoutParams layoutParams2 = r14.f1498.getLayoutParams();
                            if (layoutParams2 == null) {
                                layoutParams2 = new ViewGroup.LayoutParams(-2, -2);
                            }
                            r14.f1497.setBackgroundResource(r14.f1492);
                            ViewParent parent = r14.f1498.getParent();
                            if (parent != null && (parent instanceof ViewGroup)) {
                                ((ViewGroup) parent).removeView(r14.f1498);
                            }
                            r14.f1497.addView(r14.f1498, layoutParams2);
                            if (!r14.f1498.hasFocus()) {
                                r14.f1498.requestFocus();
                            }
                        } else {
                            return;
                        }
                    } else if (!(r14.f1499 == null || (layoutParams = r14.f1499.getLayoutParams()) == null || layoutParams.width != -1)) {
                        i = -1;
                        r14.f1504 = false;
                        WindowManager.LayoutParams layoutParams3 = new WindowManager.LayoutParams(i, -2, r14.f1494, r14.f1495, 1002, 8519680, -3);
                        layoutParams3.gravity = r14.f1493;
                        layoutParams3.windowAnimations = r14.f1496;
                        windowManager.addView(r14.f1497, layoutParams3);
                        r14.f1505 = true;
                        return;
                    }
                    i = -2;
                    r14.f1504 = false;
                    WindowManager.LayoutParams layoutParams32 = new WindowManager.LayoutParams(i, -2, r14.f1494, r14.f1495, 1002, 8519680, -3);
                    layoutParams32.gravity = r14.f1493;
                    layoutParams32.windowAnimations = r14.f1496;
                    windowManager.addView(r14.f1497, layoutParams32);
                    r14.f1505 = true;
                    return;
                }
                return;
            }
            m2612(r14, true);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m2589(C0478 r3) {
        r3.m2662(m2554());
        r3.f1497 = new C0477(r3.f1502);
        r3.f1493 = 81;
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
     arg types: [int, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
     arg types: [android.support.v7.app.ˑ$ʾ, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m2588(C0504 r5, boolean z) {
        C0609 r52 = this.f1473;
        if (r52 == null || !r52.m3862() || (ViewConfiguration.get(this.f1421).hasPermanentMenuKey() && !this.f1473.m3864())) {
            C0478 r53 = m2603(0, true);
            r53.f1507 = true;
            m2612(r53, false);
            m2587(r53, (KeyEvent) null);
            return;
        }
        Window.Callback r54 = m2557();
        if (this.f1473.m3863() && z) {
            this.f1473.m3866();
            if (!m2556()) {
                r54.onPanelClosed(108, m2603(0, true).f1500);
            }
        } else if (r54 != null && !m2556()) {
            if (this.f1470 && (this.f1472 & 1) != 0) {
                this.f1422.getDecorView().removeCallbacks(this.f1466);
                this.f1466.run();
            }
            C0478 r6 = m2603(0, true);
            if (r6.f1500 != null && !r6.f1508 && r54.onPreparePanel(0, r6.f1499, r6.f1500)) {
                r54.onMenuOpened(108, r6.f1500);
                this.f1473.m3865();
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m2592(C0478 r7) {
        Context context = this.f1421;
        if ((r7.f1491 == 0 || r7.f1491 == 108) && this.f1473 != null) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = context.getTheme();
            theme.resolveAttribute(C0727.C0728.actionBarTheme, typedValue, true);
            Resources.Theme theme2 = null;
            if (typedValue.resourceId != 0) {
                theme2 = context.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue.resourceId, true);
                theme2.resolveAttribute(C0727.C0728.actionBarWidgetTheme, typedValue, true);
            } else {
                theme.resolveAttribute(C0727.C0728.actionBarWidgetTheme, typedValue, true);
            }
            if (typedValue.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = context.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue.resourceId, true);
            }
            if (theme2 != null) {
                C0532 r1 = new C0532(context, 0);
                r1.getTheme().setTo(theme2);
                context = r1;
            }
        }
        C0504 r12 = new C0504(context);
        r12.m2893(this);
        r7.m2663(r12);
        return true;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean m2594(C0478 r4) {
        if (r4.f1499 != null) {
            r4.f1498 = r4.f1499;
            return true;
        } else if (r4.f1500 == null) {
            return false;
        } else {
            if (this.f1475 == null) {
                this.f1475 = new C0479();
            }
            r4.f1498 = (View) r4.m2661(this.f1475);
            if (r4.f1498 != null) {
                return true;
            }
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
     arg types: [android.support.v7.app.ˑ$ʾ, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void */
    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m2593(C0478 r9, KeyEvent keyEvent) {
        C0609 r10;
        C0609 r92;
        C0609 r4;
        if (m2556()) {
            return false;
        }
        if (r9.f1503) {
            return true;
        }
        C0478 r0 = this.f1458;
        if (!(r0 == null || r0 == r9)) {
            m2612(r0, false);
        }
        Window.Callback r02 = m2557();
        if (r02 != null) {
            r9.f1499 = r02.onCreatePanelView(r9.f1491);
        }
        boolean z = r9.f1491 == 0 || r9.f1491 == 108;
        if (z && (r4 = this.f1473) != null) {
            r4.m3867();
        }
        if (r9.f1499 == null && (!z || !(m2553() instanceof C0484))) {
            if (r9.f1500 == null || r9.f1508) {
                if (r9.f1500 == null && (!m2592(r9) || r9.f1500 == null)) {
                    return false;
                }
                if (z && this.f1473 != null) {
                    if (this.f1474 == null) {
                        this.f1474 = new C0475();
                    }
                    this.f1473.m3861(r9.f1500, this.f1474);
                }
                r9.f1500.m2921();
                if (!r02.onCreatePanelMenu(r9.f1491, r9.f1500)) {
                    r9.m2663((C0504) null);
                    if (z && (r92 = this.f1473) != null) {
                        r92.m3861(null, this.f1474);
                    }
                    return false;
                }
                r9.f1508 = false;
            }
            r9.f1500.m2921();
            if (r9.f1509 != null) {
                r9.f1500.m2916(r9.f1509);
                r9.f1509 = null;
            }
            if (!r02.onPreparePanel(0, r9.f1499, r9.f1500)) {
                if (z && (r10 = this.f1473) != null) {
                    r10.m3861(null, this.f1474);
                }
                r9.f1500.m2922();
                return false;
            }
            r9.f1506 = KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1;
            r9.f1500.setQwertyMode(r9.f1506);
            r9.f1500.m2922();
        }
        r9.f1503 = true;
        r9.f1504 = false;
        this.f1458 = r9;
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2625(C0504 r3) {
        if (!this.f1456) {
            this.f1456 = true;
            this.f1473.m3868();
            Window.Callback r0 = m2557();
            if (r0 != null && !m2556()) {
                r0.onPanelClosed(108, r3);
            }
            this.f1456 = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
     arg types: [android.support.v7.app.ˑ$ʾ, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
     arg types: [int, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ */
    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m2634(int i) {
        m2612(m2603(i, true), true);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2612(C0478 r4, boolean z) {
        C0609 r0;
        if (!z || r4.f1491 != 0 || (r0 = this.f1473) == null || !r0.m3863()) {
            WindowManager windowManager = (WindowManager) this.f1421.getSystemService("window");
            if (!(windowManager == null || !r4.f1505 || r4.f1497 == null)) {
                windowManager.removeView(r4.f1497);
                if (z) {
                    m2608(r4.f1491, r4, (Menu) null);
                }
            }
            r4.f1503 = false;
            r4.f1504 = false;
            r4.f1505 = false;
            r4.f1498 = null;
            r4.f1507 = true;
            if (this.f1458 == r4) {
                this.f1458 = null;
                return;
            }
            return;
        }
        m2625(r4.f1500);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
     arg types: [int, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ */
    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean m2596(int i, KeyEvent keyEvent) {
        if (keyEvent.getRepeatCount() != 0) {
            return false;
        }
        C0478 r2 = m2603(i, true);
        if (!r2.f1505) {
            return m2593(r2, keyEvent);
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
     arg types: [int, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
     arg types: [android.support.v7.app.ˑ$ʾ, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x006e  */
    /* renamed from: ʿ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m2597(int r4, android.view.KeyEvent r5) {
        /*
            r3 = this;
            android.support.v7.view.ʼ r0 = r3.f1463
            r1 = 0
            if (r0 == 0) goto L_0x0006
            return r1
        L_0x0006:
            r0 = 1
            android.support.v7.app.ˑ$ʾ r2 = r3.m2603(r4, r0)
            if (r4 != 0) goto L_0x0045
            android.support.v7.widget.ʼʼ r4 = r3.f1473
            if (r4 == 0) goto L_0x0045
            boolean r4 = r4.m3862()
            if (r4 == 0) goto L_0x0045
            android.content.Context r4 = r3.f1421
            android.view.ViewConfiguration r4 = android.view.ViewConfiguration.get(r4)
            boolean r4 = r4.hasPermanentMenuKey()
            if (r4 != 0) goto L_0x0045
            android.support.v7.widget.ʼʼ r4 = r3.f1473
            boolean r4 = r4.m3863()
            if (r4 != 0) goto L_0x003e
            boolean r4 = r3.m2556()
            if (r4 != 0) goto L_0x0065
            boolean r4 = r3.m2593(r2, r5)
            if (r4 == 0) goto L_0x0065
            android.support.v7.widget.ʼʼ r4 = r3.f1473
            boolean r4 = r4.m3865()
            goto L_0x006c
        L_0x003e:
            android.support.v7.widget.ʼʼ r4 = r3.f1473
            boolean r4 = r4.m3866()
            goto L_0x006c
        L_0x0045:
            boolean r4 = r2.f1505
            if (r4 != 0) goto L_0x0067
            boolean r4 = r2.f1504
            if (r4 == 0) goto L_0x004e
            goto L_0x0067
        L_0x004e:
            boolean r4 = r2.f1503
            if (r4 == 0) goto L_0x0065
            boolean r4 = r2.f1508
            if (r4 == 0) goto L_0x005d
            r2.f1503 = r1
            boolean r4 = r3.m2593(r2, r5)
            goto L_0x005e
        L_0x005d:
            r4 = 1
        L_0x005e:
            if (r4 == 0) goto L_0x0065
            r3.m2587(r2, r5)
            r4 = 1
            goto L_0x006c
        L_0x0065:
            r4 = 0
            goto L_0x006c
        L_0x0067:
            boolean r4 = r2.f1505
            r3.m2612(r2, r0)
        L_0x006c:
            if (r4 == 0) goto L_0x0085
            android.content.Context r5 = r3.f1421
            java.lang.String r0 = "audio"
            java.lang.Object r5 = r5.getSystemService(r0)
            android.media.AudioManager r5 = (android.media.AudioManager) r5
            if (r5 == 0) goto L_0x007e
            r5.playSoundEffect(r1)
            goto L_0x0085
        L_0x007e:
            java.lang.String r5 = "AppCompatDelegate"
            java.lang.String r0 = "Couldn't get audio manager"
            android.util.Log.w(r5, r0)
        L_0x0085:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.C0474.m2597(int, android.view.KeyEvent):boolean");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2608(int i, C0478 r4, Menu menu) {
        if (menu == null) {
            if (r4 == null && i >= 0) {
                C0478[] r0 = this.f1455;
                if (i < r0.length) {
                    r4 = r0[i];
                }
            }
            if (r4 != null) {
                menu = r4.f1500;
            }
        }
        if ((r4 == null || r4.f1505) && !m2556()) {
            this.f1423.onPanelClosed(i, menu);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0478 m2604(Menu menu) {
        C0478[] r0 = this.f1455;
        int length = r0 != null ? r0.length : 0;
        for (int i = 0; i < length; i++) {
            C0478 r3 = r0[i];
            if (r3 != null && r3.f1500 == menu) {
                return r3;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0478 m2603(int i, boolean z) {
        C0478[] r5 = this.f1455;
        if (r5 == null || r5.length <= i) {
            C0478[] r0 = new C0478[(i + 1)];
            if (r5 != null) {
                System.arraycopy(r5, 0, r0, 0, r5.length);
            }
            this.f1455 = r0;
            r5 = r0;
        }
        C0478 r02 = r5[i];
        if (r02 != null) {
            return r02;
        }
        C0478 r03 = new C0478(i);
        r5[i] = r03;
        return r03;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
     arg types: [android.support.v7.app.ˑ$ʾ, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m2590(C0478 r3, int i, KeyEvent keyEvent, int i2) {
        boolean z = false;
        if (keyEvent.isSystem()) {
            return false;
        }
        if ((r3.f1503 || m2593(r3, keyEvent)) && r3.f1500 != null) {
            z = r3.f1500.performShortcut(i, keyEvent, i2);
        }
        if (z && (i2 & 1) == 0 && this.f1473 == null) {
            m2612(r3, true);
        }
        return z;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m2595(int i) {
        this.f1472 = (1 << i) | this.f1472;
        if (!this.f1470) {
            C0414.m2226(this.f1422.getDecorView(), this.f1466);
            this.f1470 = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
     arg types: [int, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ */
    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m2636(int i) {
        C0478 r0;
        C0478 r1 = m2603(i, true);
        if (r1.f1500 != null) {
            Bundle bundle = new Bundle();
            r1.f1500.m2910(bundle);
            if (bundle.size() > 0) {
                r1.f1509 = bundle;
            }
            r1.f1500.m2921();
            r1.f1500.clear();
        }
        r1.f1508 = true;
        r1.f1507 = true;
        if ((i == 108 || i == 0) && this.f1473 != null && (r0 = m2603(0, false)) != null) {
            r0.f1503 = false;
            m2593(r0, (KeyEvent) null);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public int m2637(int i) {
        boolean z;
        boolean z2;
        boolean z3;
        ActionBarContextView actionBarContextView = this.f1464;
        int i2 = 0;
        if (actionBarContextView == null || !(actionBarContextView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            z = false;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.f1464.getLayoutParams();
            z = true;
            if (this.f1464.isShown()) {
                if (this.f1461 == null) {
                    this.f1461 = new Rect();
                    this.f1460 = new Rect();
                }
                Rect rect = this.f1461;
                Rect rect2 = this.f1460;
                rect.set(0, i, 0, 0);
                C0607.m3857(this.f1477, rect, rect2);
                if (marginLayoutParams.topMargin != (rect2.top == 0 ? i : 0)) {
                    marginLayoutParams.topMargin = i;
                    View view = this.f1469;
                    if (view == null) {
                        this.f1469 = new View(this.f1421);
                        this.f1469.setBackgroundColor(this.f1421.getResources().getColor(C0727.C0730.abc_input_method_navigation_guard));
                        this.f1477.addView(this.f1469, -1, new ViewGroup.LayoutParams(-1, i));
                    } else {
                        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                        if (layoutParams.height != i) {
                            layoutParams.height = i;
                            this.f1469.setLayoutParams(layoutParams);
                        }
                    }
                    z2 = true;
                } else {
                    z2 = false;
                }
                if (this.f1469 == null) {
                    z = false;
                }
                if (!this.f1430 && z) {
                    i = 0;
                }
            } else {
                if (marginLayoutParams.topMargin != 0) {
                    marginLayoutParams.topMargin = 0;
                    z3 = true;
                } else {
                    z3 = false;
                }
                z = false;
            }
            if (z2) {
                this.f1464.setLayoutParams(marginLayoutParams);
            }
        }
        View view2 = this.f1469;
        if (view2 != null) {
            if (!z) {
                i2 = 8;
            }
            view2.setVisibility(i2);
        }
        return i;
    }

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private void m2599() {
        if (this.f1476) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private int m2598(int i) {
        if (i == 8) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            return 108;
        } else if (i != 9) {
            return i;
        } else {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            return 109;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
     arg types: [int, int]
     candidates:
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
      android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
      android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
      android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
      android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
      android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
      android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ */
    /* access modifiers changed from: package-private */
    /* renamed from: ﹳ  reason: contains not printable characters */
    public void m2644() {
        C0609 r0 = this.f1473;
        if (r0 != null) {
            r0.m3868();
        }
        if (this.f1465 != null) {
            this.f1422.getDecorView().removeCallbacks(this.f1467);
            if (this.f1465.isShowing()) {
                try {
                    this.f1465.dismiss();
                } catch (IllegalArgumentException unused) {
                }
            }
            this.f1465 = null;
        }
        m2642();
        C0478 r02 = m2603(0, false);
        if (r02 != null && r02.f1500 != null) {
            r02.f1500.close();
        }
    }

    /* renamed from: android.support.v7.app.ˑ$ʼ  reason: contains not printable characters */
    /* compiled from: AppCompatDelegateImplV9 */
    class C0476 implements C0529.C0530 {

        /* renamed from: ʼ  reason: contains not printable characters */
        private C0529.C0530 f1488;

        public C0476(C0529.C0530 r2) {
            this.f1488 = r2;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2656(C0529 r2, Menu menu) {
            return this.f1488.m3098(r2, menu);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m2658(C0529 r2, Menu menu) {
            return this.f1488.m3100(r2, menu);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2657(C0529 r2, MenuItem menuItem) {
            return this.f1488.m3099(r2, menuItem);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2655(C0529 r3) {
            this.f1488.m3097(r3);
            if (C0474.this.f1465 != null) {
                C0474.this.f1422.getDecorView().removeCallbacks(C0474.this.f1467);
            }
            if (C0474.this.f1464 != null) {
                C0474.this.m2642();
                C0474 r32 = C0474.this;
                r32.f1468 = C0414.m2242(r32.f1464).m2376(0.0f);
                C0474.this.f1468.m2378(new C0437() {
                    /* renamed from: ʼ  reason: contains not printable characters */
                    public void m2659(View view) {
                        C0474.this.f1464.setVisibility(8);
                        if (C0474.this.f1465 != null) {
                            C0474.this.f1465.dismiss();
                        } else if (C0474.this.f1464.getParent() instanceof View) {
                            C0414.m2246((View) C0474.this.f1464.getParent());
                        }
                        C0474.this.f1464.removeAllViews();
                        C0474.this.f1468.m2378((C0436) null);
                        C0474.this.f1468 = null;
                    }
                });
            }
            if (C0474.this.f1425 != null) {
                C0474.this.f1425.onSupportActionModeFinished(C0474.this.f1463);
            }
            C0474.this.f1463 = null;
        }
    }

    /* renamed from: android.support.v7.app.ˑ$ʿ  reason: contains not printable characters */
    /* compiled from: AppCompatDelegateImplV9 */
    private final class C0479 implements C0518.C0519 {
        C0479() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
         arg types: [android.support.v7.app.ˑ$ʾ, int]
         candidates:
          android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
          android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
          android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
          android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
          android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
          android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
          android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
          android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
          android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
          android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
          android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
          android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
          android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
          android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2665(C0504 r5, boolean z) {
            C0504 r0 = r5.m2930();
            boolean z2 = r0 != r5;
            C0474 r3 = C0474.this;
            if (z2) {
                r5 = r0;
            }
            C0478 r52 = r3.m2604((Menu) r5);
            if (r52 == null) {
                return;
            }
            if (z2) {
                C0474.this.m2608(r52.f1491, r52, r0);
                C0474.this.m2612(r52, true);
                return;
            }
            C0474.this.m2612(r52, z);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2666(C0504 r3) {
            Window.Callback r0;
            if (r3 != null || !C0474.this.f1428 || (r0 = C0474.this.m2557()) == null || C0474.this.m2556()) {
                return true;
            }
            r0.onMenuOpened(108, r3);
            return true;
        }
    }

    /* renamed from: android.support.v7.app.ˑ$ʻ  reason: contains not printable characters */
    /* compiled from: AppCompatDelegateImplV9 */
    private final class C0475 implements C0518.C0519 {
        C0475() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2654(C0504 r3) {
            Window.Callback r0 = C0474.this.m2557();
            if (r0 == null) {
                return true;
            }
            r0.onMenuOpened(108, r3);
            return true;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2653(C0504 r1, boolean z) {
            C0474.this.m2625(r1);
        }
    }

    /* renamed from: android.support.v7.app.ˑ$ʾ  reason: contains not printable characters */
    /* compiled from: AppCompatDelegateImplV9 */
    protected static final class C0478 {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f1491;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f1492;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f1493;

        /* renamed from: ʾ  reason: contains not printable characters */
        int f1494;

        /* renamed from: ʿ  reason: contains not printable characters */
        int f1495;

        /* renamed from: ˆ  reason: contains not printable characters */
        int f1496;

        /* renamed from: ˈ  reason: contains not printable characters */
        ViewGroup f1497;

        /* renamed from: ˉ  reason: contains not printable characters */
        View f1498;

        /* renamed from: ˊ  reason: contains not printable characters */
        View f1499;

        /* renamed from: ˋ  reason: contains not printable characters */
        C0504 f1500;

        /* renamed from: ˎ  reason: contains not printable characters */
        C0501 f1501;

        /* renamed from: ˏ  reason: contains not printable characters */
        Context f1502;

        /* renamed from: ˑ  reason: contains not printable characters */
        boolean f1503;

        /* renamed from: י  reason: contains not printable characters */
        boolean f1504;

        /* renamed from: ـ  reason: contains not printable characters */
        boolean f1505;

        /* renamed from: ٴ  reason: contains not printable characters */
        public boolean f1506;

        /* renamed from: ᐧ  reason: contains not printable characters */
        boolean f1507 = false;

        /* renamed from: ᴵ  reason: contains not printable characters */
        boolean f1508;

        /* renamed from: ᵎ  reason: contains not printable characters */
        Bundle f1509;

        C0478(int i) {
            this.f1491 = i;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2664() {
            if (this.f1498 == null) {
                return false;
            }
            if (this.f1499 == null && this.f1501.m2867().getCount() <= 0) {
                return false;
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2662(Context context) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme newTheme = context.getResources().newTheme();
            newTheme.setTo(context.getTheme());
            newTheme.resolveAttribute(C0727.C0728.actionBarPopupTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            }
            newTheme.resolveAttribute(C0727.C0728.panelMenuListTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            } else {
                newTheme.applyStyle(C0727.C0736.Theme_AppCompat_CompactMenu, true);
            }
            C0532 r0 = new C0532(context, 0);
            r0.getTheme().setTo(newTheme);
            this.f1502 = r0;
            TypedArray obtainStyledAttributes = r0.obtainStyledAttributes(C0727.C0737.AppCompatTheme);
            this.f1492 = obtainStyledAttributes.getResourceId(C0727.C0737.AppCompatTheme_panelBackground, 0);
            this.f1496 = obtainStyledAttributes.getResourceId(C0727.C0737.AppCompatTheme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2663(C0504 r3) {
            C0501 r0;
            C0504 r02 = this.f1500;
            if (r3 != r02) {
                if (r02 != null) {
                    r02.m2906(this.f1501);
                }
                this.f1500 = r3;
                if (r3 != null && (r0 = this.f1501) != null) {
                    r3.m2895(r0);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public C0520 m2661(C0518.C0519 r4) {
            if (this.f1500 == null) {
                return null;
            }
            if (this.f1501 == null) {
                this.f1501 = new C0501(this.f1502, C0727.C0734.abc_list_menu_item_layout);
                this.f1501.m2858(r4);
                this.f1500.m2895(this.f1501);
            }
            return this.f1501.m2853(this.f1497);
        }
    }

    /* renamed from: android.support.v7.app.ˑ$ʽ  reason: contains not printable characters */
    /* compiled from: AppCompatDelegateImplV9 */
    private class C0477 extends ContentFrameLayout {
        public C0477(Context context) {
            super(context);
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return C0474.this.m2620(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() != 0 || !m2660((int) motionEvent.getX(), (int) motionEvent.getY())) {
                return super.onInterceptTouchEvent(motionEvent);
            }
            C0474.this.m2634(0);
            return true;
        }

        public void setBackgroundResource(int i) {
            setBackgroundDrawable(C0739.m4883(getContext(), i));
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean m2660(int i, int i2) {
            return i < -5 || i2 < -5 || i > getWidth() + 5 || i2 > getHeight() + 5;
        }
    }
}
