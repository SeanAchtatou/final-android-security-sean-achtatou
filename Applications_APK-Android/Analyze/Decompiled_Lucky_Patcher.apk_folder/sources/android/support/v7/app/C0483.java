package android.support.v7.app;

import android.content.res.Resources;
import android.os.Build;
import android.util.Log;
import android.util.LongSparseArray;
import java.lang.reflect.Field;
import java.util.Map;

/* renamed from: android.support.v7.app.ٴ  reason: contains not printable characters */
/* compiled from: ResourcesFlusher */
class C0483 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Field f1521;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static boolean f1522;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static Class f1523;

    /* renamed from: ʾ  reason: contains not printable characters */
    private static boolean f1524;

    /* renamed from: ʿ  reason: contains not printable characters */
    private static Field f1525;

    /* renamed from: ˆ  reason: contains not printable characters */
    private static boolean f1526;

    /* renamed from: ˈ  reason: contains not printable characters */
    private static Field f1527;

    /* renamed from: ˉ  reason: contains not printable characters */
    private static boolean f1528;

    /* renamed from: ʻ  reason: contains not printable characters */
    static boolean m2676(Resources resources) {
        if (Build.VERSION.SDK_INT >= 24) {
            return m2680(resources);
        }
        if (Build.VERSION.SDK_INT >= 23) {
            return m2679(resources);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            return m2678(resources);
        }
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static boolean m2678(Resources resources) {
        Map map;
        if (!f1522) {
            try {
                f1521 = Resources.class.getDeclaredField("mDrawableCache");
                f1521.setAccessible(true);
            } catch (NoSuchFieldException e) {
                Log.e("ResourcesFlusher", "Could not retrieve Resources#mDrawableCache field", e);
            }
            f1522 = true;
        }
        Field field = f1521;
        if (field == null) {
            return false;
        }
        try {
            map = (Map) field.get(resources);
        } catch (IllegalAccessException e2) {
            Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mDrawableCache", e2);
            map = null;
        }
        if (map == null) {
            return false;
        }
        map.clear();
        return true;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static boolean m2679(Resources resources) {
        if (!f1522) {
            try {
                f1521 = Resources.class.getDeclaredField("mDrawableCache");
                f1521.setAccessible(true);
            } catch (NoSuchFieldException e) {
                Log.e("ResourcesFlusher", "Could not retrieve Resources#mDrawableCache field", e);
            }
            f1522 = true;
        }
        Object obj = null;
        Field field = f1521;
        if (field != null) {
            try {
                obj = field.get(resources);
            } catch (IllegalAccessException e2) {
                Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mDrawableCache", e2);
            }
        }
        if (obj == null || obj == null || !m2677(obj)) {
            return false;
        }
        return true;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private static boolean m2680(Resources resources) {
        Object obj;
        if (!f1528) {
            try {
                f1527 = Resources.class.getDeclaredField("mResourcesImpl");
                f1527.setAccessible(true);
            } catch (NoSuchFieldException e) {
                Log.e("ResourcesFlusher", "Could not retrieve Resources#mResourcesImpl field", e);
            }
            f1528 = true;
        }
        Field field = f1527;
        if (field == null) {
            return false;
        }
        Object obj2 = null;
        try {
            obj = field.get(resources);
        } catch (IllegalAccessException e2) {
            Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mResourcesImpl", e2);
            obj = null;
        }
        if (obj == null) {
            return false;
        }
        if (!f1522) {
            try {
                f1521 = obj.getClass().getDeclaredField("mDrawableCache");
                f1521.setAccessible(true);
            } catch (NoSuchFieldException e3) {
                Log.e("ResourcesFlusher", "Could not retrieve ResourcesImpl#mDrawableCache field", e3);
            }
            f1522 = true;
        }
        Field field2 = f1521;
        if (field2 != null) {
            try {
                obj2 = field2.get(obj);
            } catch (IllegalAccessException e4) {
                Log.e("ResourcesFlusher", "Could not retrieve value from ResourcesImpl#mDrawableCache", e4);
            }
        }
        if (obj2 == null || !m2677(obj2)) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean m2677(Object obj) {
        LongSparseArray longSparseArray;
        if (!f1524) {
            try {
                f1523 = Class.forName("android.content.res.ThemedResourceCache");
            } catch (ClassNotFoundException e) {
                Log.e("ResourcesFlusher", "Could not find ThemedResourceCache class", e);
            }
            f1524 = true;
        }
        Class cls = f1523;
        if (cls == null) {
            return false;
        }
        if (!f1526) {
            try {
                f1525 = cls.getDeclaredField("mUnthemedEntries");
                f1525.setAccessible(true);
            } catch (NoSuchFieldException e2) {
                Log.e("ResourcesFlusher", "Could not retrieve ThemedResourceCache#mUnthemedEntries field", e2);
            }
            f1526 = true;
        }
        Field field = f1525;
        if (field == null) {
            return false;
        }
        try {
            longSparseArray = (LongSparseArray) field.get(obj);
        } catch (IllegalAccessException e3) {
            Log.e("ResourcesFlusher", "Could not retrieve value from ThemedResourceCache#mUnthemedEntries", e3);
            longSparseArray = null;
        }
        if (longSparseArray == null) {
            return false;
        }
        longSparseArray.clear();
        return true;
    }
}
