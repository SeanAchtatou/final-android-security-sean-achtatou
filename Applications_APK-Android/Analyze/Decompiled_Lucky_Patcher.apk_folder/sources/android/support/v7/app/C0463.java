package android.support.v7.app;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.C0448;
import android.support.v7.view.C0529;
import android.support.v7.view.C0536;
import android.support.v7.view.C0540;
import android.support.v7.view.menu.C0504;
import android.support.v7.widget.C0592;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Window;
import java.lang.Thread;

/* renamed from: android.support.v7.app.ˉ  reason: contains not printable characters */
/* compiled from: AppCompatDelegateImplBase */
abstract class C0463 extends C0462 {

    /* renamed from: ˑ  reason: contains not printable characters */
    private static boolean f1418 = true;

    /* renamed from: י  reason: contains not printable characters */
    private static final boolean f1419 = (Build.VERSION.SDK_INT < 21);

    /* renamed from: ـ  reason: contains not printable characters */
    private static final int[] f1420 = {16842836};

    /* renamed from: ʻ  reason: contains not printable characters */
    final Context f1421;

    /* renamed from: ʼ  reason: contains not printable characters */
    final Window f1422;

    /* renamed from: ʽ  reason: contains not printable characters */
    final Window.Callback f1423 = this.f1422.getCallback();

    /* renamed from: ʾ  reason: contains not printable characters */
    final Window.Callback f1424;

    /* renamed from: ʿ  reason: contains not printable characters */
    final C0461 f1425;

    /* renamed from: ˆ  reason: contains not printable characters */
    C0444 f1426;

    /* renamed from: ˈ  reason: contains not printable characters */
    MenuInflater f1427;

    /* renamed from: ˉ  reason: contains not printable characters */
    boolean f1428;

    /* renamed from: ˊ  reason: contains not printable characters */
    boolean f1429;

    /* renamed from: ˋ  reason: contains not printable characters */
    boolean f1430;

    /* renamed from: ˎ  reason: contains not printable characters */
    boolean f1431;

    /* renamed from: ˏ  reason: contains not printable characters */
    boolean f1432;

    /* renamed from: ٴ  reason: contains not printable characters */
    private CharSequence f1433;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f1434;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private boolean f1435;

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m2538(int i, Menu menu);

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract boolean m2540(int i, KeyEvent keyEvent);

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract boolean m2541(KeyEvent keyEvent);

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract C0529 m2542(C0529.C0530 r1);

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract void m2544(CharSequence charSequence);

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract boolean m2545(int i, Menu menu);

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2547(Bundle bundle) {
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean m2551() {
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public abstract void m2552();

    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m2555() {
        return false;
    }

    static {
        if (f1419 && !f1418) {
            final Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                public void uncaughtException(Thread thread, Throwable th) {
                    if (m2559(th)) {
                        Resources.NotFoundException notFoundException = new Resources.NotFoundException(th.getMessage() + ". If the resource you are trying to use is a vector resource, you may be referencing it in an unsupported way. See AppCompatDelegate.setCompatVectorFromResourcesEnabled() for more info.");
                        notFoundException.initCause(th.getCause());
                        notFoundException.setStackTrace(th.getStackTrace());
                        defaultUncaughtExceptionHandler.uncaughtException(thread, notFoundException);
                        return;
                    }
                    defaultUncaughtExceptionHandler.uncaughtException(thread, th);
                }

                /* renamed from: ʻ  reason: contains not printable characters */
                private boolean m2559(Throwable th) {
                    String message;
                    if (!(th instanceof Resources.NotFoundException) || (message = th.getMessage()) == null) {
                        return false;
                    }
                    if (message.contains("drawable") || message.contains("Drawable")) {
                        return true;
                    }
                    return false;
                }
            });
        }
    }

    C0463(Context context, Window window, C0461 r3) {
        this.f1421 = context;
        this.f1422 = window;
        this.f1425 = r3;
        Window.Callback callback = this.f1423;
        if (!(callback instanceof C0465)) {
            this.f1424 = m2537(callback);
            this.f1422.setCallback(this.f1424);
            C0592 r1 = C0592.m3739(context, (AttributeSet) null, f1420);
            Drawable r2 = r1.m3748(0);
            if (r2 != null) {
                this.f1422.setBackgroundDrawable(r2);
            }
            r1.m3745();
            return;
        }
        throw new IllegalStateException("AppCompat has already installed itself into the Window");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Window.Callback m2537(Window.Callback callback) {
        return new C0465(callback);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0444 m2536() {
        m2552();
        return this.f1426;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: י  reason: contains not printable characters */
    public final C0444 m2553() {
        return this.f1426;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public MenuInflater m2543() {
        if (this.f1427 == null) {
            m2552();
            C0444 r1 = this.f1426;
            this.f1427 = new C0536(r1 != null ? r1.m2445() : this.f1421);
        }
        return this.f1427;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final C0448.C0449 m2550() {
        return new C0464();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ـ  reason: contains not printable characters */
    public final Context m2554() {
        C0444 r0 = m2536();
        Context r02 = r0 != null ? r0.m2445() : null;
        return r02 == null ? this.f1421 : r02;
    }

    /* renamed from: android.support.v7.app.ˉ$ʻ  reason: contains not printable characters */
    /* compiled from: AppCompatDelegateImplBase */
    private class C0464 implements C0448.C0449 {
        C0464() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Drawable m2560() {
            C0592 r0 = C0592.m3739(m2563(), (AttributeSet) null, new int[]{C0727.C0728.homeAsUpIndicator});
            Drawable r1 = r0.m3744(0);
            r0.m3745();
            return r1;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public Context m2563() {
            return C0463.this.m2554();
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m2564() {
            C0444 r0 = C0463.this.m2536();
            return (r0 == null || (r0.m2435() & 4) == 0) ? false : true;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2562(Drawable drawable, int i) {
            C0444 r0 = C0463.this.m2536();
            if (r0 != null) {
                r0.m2440(drawable);
                r0.m2438(i);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2561(int i) {
            C0444 r0 = C0463.this.m2536();
            if (r0 != null) {
                r0.m2438(i);
            }
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2546() {
        this.f1434 = true;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m2548() {
        this.f1434 = false;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public void m2549() {
        this.f1435 = true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public final boolean m2556() {
        return this.f1435;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᴵ  reason: contains not printable characters */
    public final Window.Callback m2557() {
        return this.f1422.getCallback();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m2539(CharSequence charSequence) {
        this.f1433 = charSequence;
        m2544(charSequence);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵎ  reason: contains not printable characters */
    public final CharSequence m2558() {
        Window.Callback callback = this.f1423;
        if (callback instanceof Activity) {
            return ((Activity) callback).getTitle();
        }
        return this.f1433;
    }

    /* renamed from: android.support.v7.app.ˉ$ʼ  reason: contains not printable characters */
    /* compiled from: AppCompatDelegateImplBase */
    class C0465 extends C0540 {
        public void onContentChanged() {
        }

        C0465(Window.Callback callback) {
            super(callback);
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return C0463.this.m2541(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
            return super.dispatchKeyShortcutEvent(keyEvent) || C0463.this.m2540(keyEvent.getKeyCode(), keyEvent);
        }

        public boolean onCreatePanelMenu(int i, Menu menu) {
            if (i != 0 || (menu instanceof C0504)) {
                return super.onCreatePanelMenu(i, menu);
            }
            return false;
        }

        public boolean onPreparePanel(int i, View view, Menu menu) {
            C0504 r0 = menu instanceof C0504 ? (C0504) menu : null;
            if (i == 0 && r0 == null) {
                return false;
            }
            if (r0 != null) {
                r0.m2911(true);
            }
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (r0 != null) {
                r0.m2911(false);
            }
            return onPreparePanel;
        }

        public boolean onMenuOpened(int i, Menu menu) {
            super.onMenuOpened(i, menu);
            C0463.this.m2545(i, menu);
            return true;
        }

        public void onPanelClosed(int i, Menu menu) {
            super.onPanelClosed(i, menu);
            C0463.this.m2538(i, menu);
        }
    }
}
