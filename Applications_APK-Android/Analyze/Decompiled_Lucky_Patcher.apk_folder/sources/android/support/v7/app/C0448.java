package android.support.v7.app;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.C0456;
import android.support.v7.widget.Toolbar;
import android.support.v7.ʽ.ʻ.C0743;
import android.util.Log;
import android.view.View;

/* renamed from: android.support.v7.app.ʼ  reason: contains not printable characters */
/* compiled from: ActionBarDrawerToggle */
public class C0448 implements DrawerLayout.C0145 {

    /* renamed from: ʻ  reason: contains not printable characters */
    boolean f1391;

    /* renamed from: ʼ  reason: contains not printable characters */
    View.OnClickListener f1392;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final C0449 f1393;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final DrawerLayout f1394;

    /* renamed from: ʿ  reason: contains not printable characters */
    private C0743 f1395;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f1396;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Drawable f1397;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final int f1398;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final int f1399;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f1400;

    /* renamed from: android.support.v7.app.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: ActionBarDrawerToggle */
    public interface C0449 {
        /* renamed from: ʻ  reason: contains not printable characters */
        Drawable m2471();

        /* renamed from: ʻ  reason: contains not printable characters */
        void m2472(int i);

        /* renamed from: ʻ  reason: contains not printable characters */
        void m2473(Drawable drawable, int i);

        /* renamed from: ʼ  reason: contains not printable characters */
        Context m2474();

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean m2475();
    }

    /* renamed from: android.support.v7.app.ʼ$ʼ  reason: contains not printable characters */
    /* compiled from: ActionBarDrawerToggle */
    public interface C0450 {
        C0449 getDrawerToggleDelegate();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2463(int i) {
    }

    public C0448(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, int i, int i2) {
        this(activity, toolbar, drawerLayout, null, i, i2);
    }

    C0448(Activity activity, Toolbar toolbar, DrawerLayout drawerLayout, C0743 r5, int i, int i2) {
        this.f1396 = true;
        this.f1391 = true;
        this.f1400 = false;
        if (toolbar != null) {
            this.f1393 = new C0455(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (C0448.this.f1391) {
                        C0448.this.m2467();
                    } else if (C0448.this.f1392 != null) {
                        C0448.this.f1392.onClick(view);
                    }
                }
            });
        } else if (activity instanceof C0450) {
            this.f1393 = ((C0450) activity).getDrawerToggleDelegate();
        } else if (Build.VERSION.SDK_INT >= 18) {
            this.f1393 = new C0454(activity);
        } else if (Build.VERSION.SDK_INT >= 14) {
            this.f1393 = new C0453(activity);
        } else if (Build.VERSION.SDK_INT >= 11) {
            this.f1393 = new C0452(activity);
        } else {
            this.f1393 = new C0451(activity);
        }
        this.f1394 = drawerLayout;
        this.f1398 = i;
        this.f1399 = i2;
        if (r5 == null) {
            this.f1395 = new C0743(this.f1393.m2474());
        } else {
            this.f1395 = r5;
        }
        this.f1397 = m2470();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2462() {
        if (this.f1394.m887(8388611)) {
            m2461(1.0f);
        } else {
            m2461(0.0f);
        }
        if (this.f1391) {
            m2464(this.f1395, this.f1394.m887(8388611) ? this.f1399 : this.f1398);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2467() {
        int r0 = this.f1394.m858(8388611);
        if (this.f1394.m890(8388611) && r0 != 2) {
            this.f1394.m885(8388611);
        } else if (r0 != 1) {
            this.f1394.m884(8388611);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2466(View view, float f) {
        if (this.f1396) {
            m2461(Math.min(1.0f, Math.max(0.0f, f)));
        } else {
            m2461(0.0f);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2465(View view) {
        m2461(1.0f);
        if (this.f1391) {
            m2468(this.f1399);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2469(View view) {
        m2461(0.0f);
        if (this.f1391) {
            m2468(this.f1398);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2464(Drawable drawable, int i) {
        if (!this.f1400 && !this.f1393.m2475()) {
            Log.w("ActionBarDrawerToggle", "DrawerToggle may not show up because NavigationIcon is not visible. You may need to call actionbar.setDisplayHomeAsUpEnabled(true);");
            this.f1400 = true;
        }
        this.f1393.m2473(drawable, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2468(int i) {
        this.f1393.m2472(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public Drawable m2470() {
        return this.f1393.m2471();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m2461(float f) {
        if (f == 1.0f) {
            this.f1395.m4897(true);
        } else if (f == 0.0f) {
            this.f1395.m4897(false);
        }
        this.f1395.m4898(f);
    }

    /* renamed from: android.support.v7.app.ʼ$ʾ  reason: contains not printable characters */
    /* compiled from: ActionBarDrawerToggle */
    private static class C0452 implements C0449 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final Activity f1403;

        /* renamed from: ʼ  reason: contains not printable characters */
        C0456.C0457 f1404;

        C0452(Activity activity) {
            this.f1403 = activity;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Drawable m2481() {
            return C0456.m2497(this.f1403);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public Context m2484() {
            return this.f1403;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m2485() {
            ActionBar actionBar = this.f1403.getActionBar();
            return (actionBar == null || (actionBar.getDisplayOptions() & 4) == 0) ? false : true;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2483(Drawable drawable, int i) {
            ActionBar actionBar = this.f1403.getActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowHomeEnabled(true);
                this.f1404 = C0456.m2499(this.f1404, this.f1403, drawable, i);
                actionBar.setDisplayShowHomeEnabled(false);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2482(int i) {
            this.f1404 = C0456.m2498(this.f1404, this.f1403, i);
        }
    }

    /* renamed from: android.support.v7.app.ʼ$ʿ  reason: contains not printable characters */
    /* compiled from: ActionBarDrawerToggle */
    private static class C0453 extends C0452 {
        C0453(Activity activity) {
            super(activity);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public Context m2486() {
            ActionBar actionBar = this.f1403.getActionBar();
            if (actionBar != null) {
                return actionBar.getThemedContext();
            }
            return this.f1403;
        }
    }

    /* renamed from: android.support.v7.app.ʼ$ˆ  reason: contains not printable characters */
    /* compiled from: ActionBarDrawerToggle */
    private static class C0454 implements C0449 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final Activity f1405;

        C0454(Activity activity) {
            this.f1405 = activity;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Drawable m2487() {
            TypedArray obtainStyledAttributes = m2490().obtainStyledAttributes(null, new int[]{16843531}, 16843470, 0);
            Drawable drawable = obtainStyledAttributes.getDrawable(0);
            obtainStyledAttributes.recycle();
            return drawable;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public Context m2490() {
            ActionBar actionBar = this.f1405.getActionBar();
            if (actionBar != null) {
                return actionBar.getThemedContext();
            }
            return this.f1405;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m2491() {
            ActionBar actionBar = this.f1405.getActionBar();
            return (actionBar == null || (actionBar.getDisplayOptions() & 4) == 0) ? false : true;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2489(Drawable drawable, int i) {
            ActionBar actionBar = this.f1405.getActionBar();
            if (actionBar != null) {
                actionBar.setHomeAsUpIndicator(drawable);
                actionBar.setHomeActionContentDescription(i);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2488(int i) {
            ActionBar actionBar = this.f1405.getActionBar();
            if (actionBar != null) {
                actionBar.setHomeActionContentDescription(i);
            }
        }
    }

    /* renamed from: android.support.v7.app.ʼ$ˈ  reason: contains not printable characters */
    /* compiled from: ActionBarDrawerToggle */
    static class C0455 implements C0449 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final Toolbar f1406;

        /* renamed from: ʼ  reason: contains not printable characters */
        final Drawable f1407;

        /* renamed from: ʽ  reason: contains not printable characters */
        final CharSequence f1408;

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m2496() {
            return true;
        }

        C0455(Toolbar toolbar) {
            this.f1406 = toolbar;
            this.f1407 = toolbar.getNavigationIcon();
            this.f1408 = toolbar.getNavigationContentDescription();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2494(Drawable drawable, int i) {
            this.f1406.setNavigationIcon(drawable);
            m2493(i);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2493(int i) {
            if (i == 0) {
                this.f1406.setNavigationContentDescription(this.f1408);
            } else {
                this.f1406.setNavigationContentDescription(i);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Drawable m2492() {
            return this.f1407;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public Context m2495() {
            return this.f1406.getContext();
        }
    }

    /* renamed from: android.support.v7.app.ʼ$ʽ  reason: contains not printable characters */
    /* compiled from: ActionBarDrawerToggle */
    static class C0451 implements C0449 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final Activity f1402;

        /* renamed from: ʻ  reason: contains not printable characters */
        public Drawable m2476() {
            return null;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2477(int i) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2478(Drawable drawable, int i) {
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m2480() {
            return true;
        }

        C0451(Activity activity) {
            this.f1402 = activity;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public Context m2479() {
            return this.f1402;
        }
    }
}
