package android.support.v7.app;

import android.content.Context;
import android.support.v7.app.C0472;
import android.support.v7.app.C0474;
import android.view.KeyboardShortcutGroup;
import android.view.Menu;
import android.view.Window;
import java.util.List;

/* renamed from: android.support.v7.app.ˊ  reason: contains not printable characters */
/* compiled from: AppCompatDelegateImplN */
class C0466 extends C0472 {
    C0466(Context context, Window window, C0461 r3) {
        super(context, window, r3);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Window.Callback m2565(Window.Callback callback) {
        return new C0467(callback);
    }

    /* renamed from: android.support.v7.app.ˊ$ʻ  reason: contains not printable characters */
    /* compiled from: AppCompatDelegateImplN */
    class C0467 extends C0472.C0473 {
        C0467(Window.Callback callback) {
            super(callback);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ
         arg types: [int, int]
         candidates:
          android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, android.view.KeyEvent):void
          android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, boolean):void
          android.support.v7.app.ˑ.ʻ(int, android.view.Menu):void
          android.support.v7.app.ˑ.ʻ(android.support.v7.app.ˑ$ʾ, boolean):void
          android.support.v7.app.ˑ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
          android.support.v7.app.ˑ.ʻ(int, android.view.KeyEvent):boolean
          android.support.v7.app.ˑ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
          android.support.v7.app.ˉ.ʻ(int, android.view.Menu):void
          android.support.v7.app.ˉ.ʻ(int, android.view.KeyEvent):boolean
          android.support.v7.app.ˈ.ʻ(android.app.Activity, android.support.v7.app.ˆ):android.support.v7.app.ˈ
          android.support.v7.app.ˈ.ʻ(android.app.Dialog, android.support.v7.app.ˆ):android.support.v7.app.ˈ
          android.support.v7.app.ˈ.ʻ(android.view.View, android.view.ViewGroup$LayoutParams):void
          android.support.v7.view.menu.ˉ.ʻ.ʻ(android.support.v7.view.menu.ˉ, android.view.MenuItem):boolean
          android.support.v7.app.ˑ.ʻ(int, boolean):android.support.v7.app.ˑ$ʾ */
        public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> list, Menu menu, int i) {
            C0474.C0478 r0 = C0466.this.m2603(0, true);
            if (r0 == null || r0.f1500 == null) {
                super.onProvideKeyboardShortcuts(list, menu, i);
            } else {
                super.onProvideKeyboardShortcuts(list, r0.f1500, i);
            }
        }
    }
}
