package android.support.v7.app;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.support.v4.ˉ.C0414;
import android.support.v7.app.C0444;
import android.support.v7.view.C0540;
import android.support.v7.view.menu.C0504;
import android.support.v7.view.menu.C0518;
import android.support.v7.widget.C0593;
import android.support.v7.widget.C0631;
import android.support.v7.widget.Toolbar;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import java.util.ArrayList;

/* renamed from: android.support.v7.app.ᐧ  reason: contains not printable characters */
/* compiled from: ToolbarActionBar */
class C0484 extends C0444 {

    /* renamed from: ʻ  reason: contains not printable characters */
    C0631 f1529;

    /* renamed from: ʼ  reason: contains not printable characters */
    boolean f1530;

    /* renamed from: ʽ  reason: contains not printable characters */
    Window.Callback f1531;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f1532;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f1533;

    /* renamed from: ˆ  reason: contains not printable characters */
    private ArrayList<C0444.C0446> f1534 = new ArrayList<>();

    /* renamed from: ˈ  reason: contains not printable characters */
    private final Runnable f1535 = new Runnable() {
        public void run() {
            C0484.this.m2701();
        }
    };

    /* renamed from: ˉ  reason: contains not printable characters */
    private final Toolbar.C0572 f1536 = new Toolbar.C0572() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2702(MenuItem menuItem) {
            return C0484.this.f1531.onMenuItemSelected(0, menuItem);
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2688(boolean z) {
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2692(boolean z) {
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m2694(boolean z) {
    }

    C0484(Toolbar toolbar, CharSequence charSequence, Window.Callback callback) {
        this.f1529 = new C0593(toolbar, false);
        this.f1531 = new C0487(callback);
        this.f1529.m3996(this.f1531);
        toolbar.setOnMenuItemClickListener(this.f1536);
        this.f1529.m3997(charSequence);
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public Window.Callback m2700() {
        return this.f1531;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2683(float f) {
        C0414.m2217(this.f1529.m3990(), f);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Context m2691() {
        return this.f1529.m3999();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2686(Drawable drawable) {
        this.f1529.m4001(drawable);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2684(int i) {
        this.f1529.m4006(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2685(Configuration configuration) {
        super.m2439(configuration);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2687(CharSequence charSequence) {
        this.f1529.m3997(charSequence);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m2682() {
        return this.f1529.m4018();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m2693() {
        return this.f1529.m4014();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m2695() {
        return this.f1529.m4015();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m2697() {
        this.f1529.m3990().removeCallbacks(this.f1535);
        C0414.m2226(this.f1529.m3990(), this.f1535);
        return true;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m2698() {
        if (!this.f1529.m4004()) {
            return false;
        }
        this.f1529.m4005();
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public void m2701() {
        Menu r0 = m2681();
        C0504 r1 = r0 instanceof C0504 ? (C0504) r0 : null;
        if (r1 != null) {
            r1.m2921();
        }
        try {
            r0.clear();
            if (!this.f1531.onCreatePanelMenu(0, r0) || !this.f1531.onPreparePanel(0, null, r0)) {
                r0.clear();
            }
        } finally {
            if (r1 != null) {
                r1.m2922();
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2690(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1) {
            m2693();
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2689(int i, KeyEvent keyEvent) {
        Menu r0 = m2681();
        if (r0 == null) {
            return false;
        }
        boolean z = true;
        if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() == 1) {
            z = false;
        }
        r0.setQwertyMode(z);
        return r0.performShortcut(i, keyEvent, 0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m2699() {
        this.f1529.m3990().removeCallbacks(this.f1535);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m2696(boolean z) {
        if (z != this.f1533) {
            this.f1533 = z;
            int size = this.f1534.size();
            for (int i = 0; i < size; i++) {
                this.f1534.get(i).m2455(z);
            }
        }
    }

    /* renamed from: android.support.v7.app.ᐧ$ʽ  reason: contains not printable characters */
    /* compiled from: ToolbarActionBar */
    private class C0487 extends C0540 {
        public C0487(Window.Callback callback) {
            super(callback);
        }

        public boolean onPreparePanel(int i, View view, Menu menu) {
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (onPreparePanel && !C0484.this.f1530) {
                C0484.this.f1529.m4016();
                C0484.this.f1530 = true;
            }
            return onPreparePanel;
        }

        public View onCreatePanelView(int i) {
            if (i == 0) {
                return new View(C0484.this.f1529.m3999());
            }
            return super.onCreatePanelView(i);
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private Menu m2681() {
        if (!this.f1532) {
            this.f1529.m3993(new C0485(), new C0486());
            this.f1532 = true;
        }
        return this.f1529.m4020();
    }

    /* renamed from: android.support.v7.app.ᐧ$ʻ  reason: contains not printable characters */
    /* compiled from: ToolbarActionBar */
    private final class C0485 implements C0518.C0519 {

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean f1540;

        C0485() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2704(C0504 r3) {
            if (C0484.this.f1531 == null) {
                return false;
            }
            C0484.this.f1531.onMenuOpened(108, r3);
            return true;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2703(C0504 r2, boolean z) {
            if (!this.f1540) {
                this.f1540 = true;
                C0484.this.f1529.m4017();
                if (C0484.this.f1531 != null) {
                    C0484.this.f1531.onPanelClosed(108, r2);
                }
                this.f1540 = false;
            }
        }
    }

    /* renamed from: android.support.v7.app.ᐧ$ʼ  reason: contains not printable characters */
    /* compiled from: ToolbarActionBar */
    private final class C0486 implements C0504.C0505 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2706(C0504 r1, MenuItem menuItem) {
            return false;
        }

        C0486() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2705(C0504 r5) {
            if (C0484.this.f1531 == null) {
                return;
            }
            if (C0484.this.f1529.m4012()) {
                C0484.this.f1531.onPanelClosed(108, r5);
            } else if (C0484.this.f1531.onPreparePanel(0, null, r5)) {
                C0484.this.f1531.onMenuOpened(108, r5);
            }
        }
    }
}
