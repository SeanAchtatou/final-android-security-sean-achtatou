package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AlertController;
import android.support.v7.ʻ.C0727;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListAdapter;

/* renamed from: android.support.v7.app.ʾ  reason: contains not printable characters */
/* compiled from: AlertDialog */
public class C0458 extends C0480 implements DialogInterface {

    /* renamed from: ʻ  reason: contains not printable characters */
    final AlertController f1413 = new AlertController(getContext(), this, getWindow());

    protected C0458(Context context, int i) {
        super(context, m2500(context, i));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static int m2500(Context context, int i) {
        if (((i >>> 24) & 255) >= 1) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(C0727.C0728.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.f1413.m2422(charSequence);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f1413.m2417();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.f1413.m2423(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.f1413.m2427(i, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    /* renamed from: android.support.v7.app.ʾ$ʻ  reason: contains not printable characters */
    /* compiled from: AlertDialog */
    public static class C0459 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final AlertController.C0440 f1414;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final int f1415;

        public C0459(Context context) {
            this(context, C0458.m2500(context, 0));
        }

        public C0459(Context context, int i) {
            this.f1414 = new AlertController.C0440(new ContextThemeWrapper(context, C0458.m2500(context, i)));
            this.f1415 = i;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Context m2501() {
            return this.f1414.f1338;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0459 m2506(CharSequence charSequence) {
            this.f1414.f1348 = charSequence;
            return this;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0459 m2504(View view) {
            this.f1414.f1350 = view;
            return this;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0459 m2503(Drawable drawable) {
            this.f1414.f1344 = drawable;
            return this;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0459 m2502(DialogInterface.OnKeyListener onKeyListener) {
            this.f1414.f1367 = onKeyListener;
            return this;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0459 m2505(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
            AlertController.C0440 r0 = this.f1414;
            r0.f1370 = listAdapter;
            r0.f1371 = onClickListener;
            return this;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public C0458 m2507() {
            C0458 r0 = new C0458(this.f1414.f1338, this.f1415);
            this.f1414.m2433(r0.f1413);
            r0.setCancelable(this.f1414.f1362);
            if (this.f1414.f1362) {
                r0.setCanceledOnTouchOutside(true);
            }
            r0.setOnCancelListener(this.f1414.f1364);
            r0.setOnDismissListener(this.f1414.f1365);
            if (this.f1414.f1367 != null) {
                r0.setOnKeyListener(this.f1414.f1367);
            }
            return r0;
        }
    }
}
