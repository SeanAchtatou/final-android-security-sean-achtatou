package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.ˉ.C0414;
import android.support.v4.ˉ.C0434;
import android.support.v4.ˉ.C0436;
import android.support.v4.ˉ.C0437;
import android.support.v4.ˉ.C0438;
import android.support.v7.app.C0444;
import android.support.v7.view.C0528;
import android.support.v7.view.C0529;
import android.support.v7.view.C0536;
import android.support.v7.view.C0539;
import android.support.v7.view.menu.C0504;
import android.support.v7.widget.ActionBarContainer;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.ActionBarOverlayLayout;
import android.support.v7.widget.C0580;
import android.support.v7.widget.C0631;
import android.support.v7.widget.Toolbar;
import android.support.v7.ʻ.C0727;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* renamed from: android.support.v7.app.ᵔ  reason: contains not printable characters */
/* compiled from: WindowDecorActionBar */
public class C0491 extends C0444 implements ActionBarOverlayLayout.C0541 {

    /* renamed from: ᵎ  reason: contains not printable characters */
    static final /* synthetic */ boolean f1557 = (!C0491.class.desiredAssertionStatus());

    /* renamed from: ᵔ  reason: contains not printable characters */
    private static final Interpolator f1558 = new AccelerateInterpolator();

    /* renamed from: ᵢ  reason: contains not printable characters */
    private static final Interpolator f1559 = new DecelerateInterpolator();

    /* renamed from: ʻ  reason: contains not printable characters */
    Context f1560;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private ArrayList<C0444.C0446> f1561 = new ArrayList<>();

    /* renamed from: ʼ  reason: contains not printable characters */
    ActionBarOverlayLayout f1562;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private int f1563 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    ActionBarContainer f1564;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private boolean f1565;

    /* renamed from: ʾ  reason: contains not printable characters */
    C0631 f1566;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private boolean f1567 = true;

    /* renamed from: ʿ  reason: contains not printable characters */
    ActionBarContextView f1568;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private boolean f1569;

    /* renamed from: ˆ  reason: contains not printable characters */
    View f1570;

    /* renamed from: ˈ  reason: contains not printable characters */
    C0580 f1571;

    /* renamed from: ˉ  reason: contains not printable characters */
    C0492 f1572;

    /* renamed from: ˊ  reason: contains not printable characters */
    C0529 f1573;

    /* renamed from: ˋ  reason: contains not printable characters */
    C0529.C0530 f1574;

    /* renamed from: ˎ  reason: contains not printable characters */
    boolean f1575 = true;

    /* renamed from: ˏ  reason: contains not printable characters */
    boolean f1576;

    /* renamed from: ˑ  reason: contains not printable characters */
    boolean f1577;

    /* renamed from: י  reason: contains not printable characters */
    C0539 f1578;

    /* renamed from: ـ  reason: contains not printable characters */
    boolean f1579;

    /* renamed from: ــ  reason: contains not printable characters */
    private boolean f1580;

    /* renamed from: ٴ  reason: contains not printable characters */
    final C0436 f1581 = new C0437() {
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2751(View view) {
            if (C0491.this.f1575 && C0491.this.f1570 != null) {
                C0491.this.f1570.setTranslationY(0.0f);
                C0491.this.f1564.setTranslationY(0.0f);
            }
            C0491.this.f1564.setVisibility(8);
            C0491.this.f1564.setTransitioning(false);
            C0491 r2 = C0491.this;
            r2.f1578 = null;
            r2.m2742();
            if (C0491.this.f1562 != null) {
                C0414.m2246(C0491.this.f1562);
            }
        }
    };

    /* renamed from: ᐧ  reason: contains not printable characters */
    final C0436 f1582 = new C0437() {
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2752(View view) {
            C0491 r2 = C0491.this;
            r2.f1578 = null;
            r2.f1564.requestLayout();
        }
    };

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private boolean f1583;

    /* renamed from: ᴵ  reason: contains not printable characters */
    final C0438 f1584 = new C0438() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2753(View view) {
            ((View) C0491.this.f1564.getParent()).invalidate();
        }
    };

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private boolean f1585;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private Context f1586;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private Activity f1587;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private Dialog f1588;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private ArrayList<Object> f1589 = new ArrayList<>();

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private int f1590 = -1;

    /* renamed from: ʻ  reason: contains not printable characters */
    static boolean m2716(boolean z, boolean z2, boolean z3) {
        if (z3) {
            return true;
        }
        return !z && !z2;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m2750() {
    }

    public C0491(Activity activity, boolean z) {
        this.f1587 = activity;
        View decorView = activity.getWindow().getDecorView();
        m2715(decorView);
        if (!z) {
            this.f1570 = decorView.findViewById(16908290);
        }
    }

    public C0491(Dialog dialog) {
        this.f1588 = dialog;
        m2715(dialog.getWindow().getDecorView());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m2715(View view) {
        this.f1562 = (ActionBarOverlayLayout) view.findViewById(C0727.C0733.decor_content_parent);
        ActionBarOverlayLayout actionBarOverlayLayout = this.f1562;
        if (actionBarOverlayLayout != null) {
            actionBarOverlayLayout.setActionBarVisibilityCallback(this);
        }
        this.f1566 = m2717(view.findViewById(C0727.C0733.action_bar));
        this.f1568 = (ActionBarContextView) view.findViewById(C0727.C0733.action_context_bar);
        this.f1564 = (ActionBarContainer) view.findViewById(C0727.C0733.action_bar_container);
        C0631 r6 = this.f1566;
        if (r6 == null || this.f1568 == null || this.f1564 == null) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with a compatible window decor layout");
        }
        this.f1560 = r6.m3999();
        boolean z = (this.f1566.m4018() & 4) != 0;
        if (z) {
            this.f1583 = true;
        }
        C0528 r2 = C0528.m3072(this.f1560);
        m2731(r2.m3078() || z);
        m2718(r2.m3076());
        TypedArray obtainStyledAttributes = this.f1560.obtainStyledAttributes(null, C0727.C0737.ActionBar, C0727.C0728.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(C0727.C0737.ActionBar_hideOnContentScroll, false)) {
            m2735(true);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(C0727.C0737.ActionBar_elevation, 0);
        if (dimensionPixelSize != 0) {
            m2725((float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private C0631 m2717(View view) {
        if (view instanceof C0631) {
            return (C0631) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Can't make a decor toolbar out of ");
        sb.append(view);
        throw new IllegalStateException(sb.toString() != null ? view.getClass().getSimpleName() : "null");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2725(float f) {
        C0414.m2217(this.f1564, f);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2728(Configuration configuration) {
        m2718(C0528.m3072(this.f1560).m3076());
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private void m2718(boolean z) {
        this.f1565 = z;
        if (!this.f1565) {
            this.f1566.m3994((C0580) null);
            this.f1564.setTabContainer(this.f1571);
        } else {
            this.f1564.setTabContainer(null);
            this.f1566.m3994(this.f1571);
        }
        boolean z2 = true;
        boolean z3 = m2744() == 2;
        C0580 r0 = this.f1571;
        if (r0 != null) {
            if (z3) {
                r0.setVisibility(0);
                ActionBarOverlayLayout actionBarOverlayLayout = this.f1562;
                if (actionBarOverlayLayout != null) {
                    C0414.m2246(actionBarOverlayLayout);
                }
            } else {
                r0.setVisibility(8);
            }
        }
        this.f1566.m3998(!this.f1565 && z3);
        ActionBarOverlayLayout actionBarOverlayLayout2 = this.f1562;
        if (this.f1565 || !z3) {
            z2 = false;
        }
        actionBarOverlayLayout2.setHasNonEmbeddedTabs(z2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public void m2742() {
        C0529.C0530 r0 = this.f1574;
        if (r0 != null) {
            r0.m3097(this.f1573);
            this.f1573 = null;
            this.f1574 = null;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2734(int i) {
        this.f1563 = i;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m2737(boolean z) {
        C0539 r1;
        this.f1580 = z;
        if (!z && (r1 = this.f1578) != null) {
            r1.m3148();
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m2738(boolean z) {
        if (z != this.f1585) {
            this.f1585 = z;
            int size = this.f1561.size();
            for (int i = 0; i < size; i++) {
                this.f1561.get(i).m2455(z);
            }
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m2739(boolean z) {
        m2727(z ? 4 : 0, 4);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2731(boolean z) {
        this.f1566.m4002(z);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2730(CharSequence charSequence) {
        this.f1566.m3997(charSequence);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2727(int i, int i2) {
        int r0 = this.f1566.m4018();
        if ((i2 & 4) != 0) {
            this.f1583 = true;
        }
        this.f1566.m4003((i & i2) | ((i2 ^ -1) & r0));
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public int m2744() {
        return this.f1566.m4019();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m2723() {
        return this.f1566.m4018();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0529 m2724(C0529.C0530 r3) {
        C0492 r0 = this.f1572;
        if (r0 != null) {
            r0.m2764();
        }
        this.f1562.setHideOnContentScrollEnabled(false);
        this.f1568.m3159();
        C0492 r02 = new C0492(this.f1568.getContext(), r3);
        if (!r02.m2766()) {
            return null;
        }
        this.f1572 = r02;
        r02.m2765();
        this.f1568.m3156(r02);
        m2747(true);
        this.f1568.sendAccessibilityEvent(32);
        return r02;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public void m2741(boolean z) {
        this.f1575 = z;
    }

    /* renamed from: י  reason: contains not printable characters */
    private void m2720() {
        if (!this.f1569) {
            this.f1569 = true;
            ActionBarOverlayLayout actionBarOverlayLayout = this.f1562;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(true);
            }
            m2719(false);
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public void m2746() {
        if (this.f1577) {
            this.f1577 = false;
            m2719(true);
        }
    }

    /* renamed from: ـ  reason: contains not printable characters */
    private void m2721() {
        if (this.f1569) {
            this.f1569 = false;
            ActionBarOverlayLayout actionBarOverlayLayout = this.f1562;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(false);
            }
            m2719(false);
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public void m2748() {
        if (!this.f1577) {
            this.f1577 = true;
            m2719(true);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2735(boolean z) {
        if (!z || this.f1562.m3172()) {
            this.f1579 = z;
            this.f1562.setHideOnContentScrollEnabled(z);
            return;
        }
        throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private void m2719(boolean z) {
        if (m2716(this.f1576, this.f1577, this.f1569)) {
            if (!this.f1567) {
                this.f1567 = true;
                m2743(z);
            }
        } else if (this.f1567) {
            this.f1567 = false;
            m2745(z);
        }
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public void m2743(boolean z) {
        View view;
        View view2;
        C0539 r0 = this.f1578;
        if (r0 != null) {
            r0.m3148();
        }
        this.f1564.setVisibility(0);
        if (this.f1563 != 0 || (!this.f1580 && !z)) {
            this.f1564.setAlpha(1.0f);
            this.f1564.setTranslationY(0.0f);
            if (this.f1575 && (view = this.f1570) != null) {
                view.setTranslationY(0.0f);
            }
            this.f1582.m2389(null);
        } else {
            this.f1564.setTranslationY(0.0f);
            float f = (float) (-this.f1564.getHeight());
            if (z) {
                int[] iArr = {0, 0};
                this.f1564.getLocationInWindow(iArr);
                f -= (float) iArr[1];
            }
            this.f1564.setTranslationY(f);
            C0539 r5 = new C0539();
            C0434 r2 = C0414.m2242(this.f1564).m2381(0.0f);
            r2.m2379(this.f1584);
            r5.m3142(r2);
            if (this.f1575 && (view2 = this.f1570) != null) {
                view2.setTranslationY(f);
                r5.m3142(C0414.m2242(this.f1570).m2381(0.0f));
            }
            r5.m3145(f1559);
            r5.m3141(250);
            r5.m3144(this.f1582);
            this.f1578 = r5;
            r5.m3146();
        }
        ActionBarOverlayLayout actionBarOverlayLayout = this.f1562;
        if (actionBarOverlayLayout != null) {
            C0414.m2246(actionBarOverlayLayout);
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public void m2745(boolean z) {
        View view;
        C0539 r0 = this.f1578;
        if (r0 != null) {
            r0.m3148();
        }
        if (this.f1563 != 0 || (!this.f1580 && !z)) {
            this.f1581.m2389(null);
            return;
        }
        this.f1564.setAlpha(1.0f);
        this.f1564.setTransitioning(true);
        C0539 r02 = new C0539();
        float f = (float) (-this.f1564.getHeight());
        if (z) {
            int[] iArr = {0, 0};
            this.f1564.getLocationInWindow(iArr);
            f -= (float) iArr[1];
        }
        C0434 r5 = C0414.m2242(this.f1564).m2381(f);
        r5.m2379(this.f1584);
        r02.m3142(r5);
        if (this.f1575 && (view = this.f1570) != null) {
            r02.m3142(C0414.m2242(view).m2381(f));
        }
        r02.m3145(f1558);
        r02.m3141(250);
        r02.m3144(this.f1581);
        this.f1578 = r02;
        r02.m3146();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public void m2747(boolean z) {
        C0434 r9;
        C0434 r0;
        if (z) {
            m2720();
        } else {
            m2721();
        }
        if (m2722()) {
            if (z) {
                r9 = this.f1566.m3989(4, 100);
                r0 = this.f1568.m3155(0, 200);
            } else {
                r0 = this.f1566.m3989(0, 200);
                r9 = this.f1568.m3155(8, 100);
            }
            C0539 r1 = new C0539();
            r1.m3143(r9, r0);
            r1.m3146();
        } else if (z) {
            this.f1566.m4008(4);
            this.f1568.setVisibility(0);
        } else {
            this.f1566.m4008(0);
            this.f1568.setVisibility(8);
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean m2722() {
        return C0414.m2255(this.f1564);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Context m2733() {
        if (this.f1586 == null) {
            TypedValue typedValue = new TypedValue();
            this.f1560.getTheme().resolveAttribute(C0727.C0728.actionBarWidgetTheme, typedValue, true);
            int i = typedValue.resourceId;
            if (i != 0) {
                this.f1586 = new ContextThemeWrapper(this.f1560, i);
            } else {
                this.f1586 = this.f1560;
            }
        }
        return this.f1586;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2729(Drawable drawable) {
        this.f1566.m4001(drawable);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2726(int i) {
        this.f1566.m4006(i);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public void m2749() {
        C0539 r0 = this.f1578;
        if (r0 != null) {
            r0.m3148();
            this.f1578 = null;
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m2740() {
        C0631 r0 = this.f1566;
        if (r0 == null || !r0.m4004()) {
            return false;
        }
        this.f1566.m4005();
        return true;
    }

    /* renamed from: android.support.v7.app.ᵔ$ʻ  reason: contains not printable characters */
    /* compiled from: WindowDecorActionBar */
    public class C0492 extends C0529 implements C0504.C0505 {

        /* renamed from: ʼ  reason: contains not printable characters */
        private final Context f1595;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final C0504 f1596;

        /* renamed from: ʾ  reason: contains not printable characters */
        private C0529.C0530 f1597;

        /* renamed from: ʿ  reason: contains not printable characters */
        private WeakReference<View> f1598;

        public C0492(Context context, C0529.C0530 r3) {
            this.f1595 = context;
            this.f1597 = r3;
            this.f1596 = new C0504(context).m2885(1);
            this.f1596.m2893(this);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public MenuInflater m2754() {
            return new C0536(this.f1595);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public Menu m2761() {
            return this.f1596;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m2764() {
            if (C0491.this.f1572 == this) {
                if (!C0491.m2716(C0491.this.f1576, C0491.this.f1577, false)) {
                    C0491 r0 = C0491.this;
                    r0.f1573 = this;
                    r0.f1574 = this.f1597;
                } else {
                    this.f1597.m3097(this);
                }
                this.f1597 = null;
                C0491.this.m2747(false);
                C0491.this.f1568.m3158();
                C0491.this.f1566.m3990().sendAccessibilityEvent(32);
                C0491.this.f1562.setHideOnContentScrollEnabled(C0491.this.f1579);
                C0491.this.f1572 = null;
            }
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public void m2765() {
            if (C0491.this.f1572 == this) {
                this.f1596.m2921();
                try {
                    this.f1597.m3100(this, this.f1596);
                } finally {
                    this.f1596.m2922();
                }
            }
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public boolean m2766() {
            this.f1596.m2921();
            try {
                return this.f1597.m3098(this, this.f1596);
            } finally {
                this.f1596.m2922();
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2757(View view) {
            C0491.this.f1568.setCustomView(view);
            this.f1598 = new WeakReference<>(view);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2758(CharSequence charSequence) {
            C0491.this.f1568.setSubtitle(charSequence);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2763(CharSequence charSequence) {
            C0491.this.f1568.setTitle(charSequence);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2755(int i) {
            m2763(C0491.this.f1560.getResources().getString(i));
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2762(int i) {
            m2758((CharSequence) C0491.this.f1560.getResources().getString(i));
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public CharSequence m2767() {
            return C0491.this.f1568.getTitle();
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public CharSequence m2768() {
            return C0491.this.f1568.getSubtitle();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2759(boolean z) {
            super.m3085(z);
            C0491.this.f1568.setTitleOptional(z);
        }

        /* renamed from: ˉ  reason: contains not printable characters */
        public boolean m2769() {
            return C0491.this.f1568.m3160();
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public View m2770() {
            WeakReference<View> weakReference = this.f1598;
            if (weakReference != null) {
                return weakReference.get();
            }
            return null;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m2760(C0504 r1, MenuItem menuItem) {
            C0529.C0530 r12 = this.f1597;
            if (r12 != null) {
                return r12.m3099(this, menuItem);
            }
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2756(C0504 r1) {
            if (this.f1597 != null) {
                m2765();
                C0491.this.f1568.m3157();
            }
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2736(boolean z) {
        if (!this.f1583) {
            m2739(z);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2732(int i, KeyEvent keyEvent) {
        Menu r0;
        C0492 r02 = this.f1572;
        if (r02 == null || (r0 = r02.m2761()) == null) {
            return false;
        }
        boolean z = true;
        if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() == 1) {
            z = false;
        }
        r0.setQwertyMode(z);
        return r0.performShortcut(i, keyEvent, 0);
    }
}
