package android.support.v7.app;

import android.support.v7.view.C0529;

/* renamed from: android.support.v7.app.ˆ  reason: contains not printable characters */
/* compiled from: AppCompatCallback */
public interface C0461 {
    void onSupportActionModeFinished(C0529 r1);

    void onSupportActionModeStarted(C0529 r1);

    C0529 onWindowStartingSupportActionMode(C0529.C0530 r1);
}
