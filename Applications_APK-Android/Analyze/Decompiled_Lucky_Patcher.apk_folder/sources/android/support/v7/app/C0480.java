package android.support.v7.app;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.view.C0529;
import android.support.v7.ʻ.C0727;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: android.support.v7.app.י  reason: contains not printable characters */
/* compiled from: AppCompatDialog */
public class C0480 extends Dialog implements C0461 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private C0462 f1511;

    public void onSupportActionModeFinished(C0529 r1) {
    }

    public void onSupportActionModeStarted(C0529 r1) {
    }

    public C0529 onWindowStartingSupportActionMode(C0529.C0530 r1) {
        return null;
    }

    public C0480(Context context, int i) {
        super(context, m2667(context, i));
        m2668().m2517((Bundle) null);
        m2668().m2535();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        m2668().m2534();
        super.onCreate(bundle);
        m2668().m2517(bundle);
    }

    public void setContentView(int i) {
        m2668().m2523(i);
    }

    public void setContentView(View view) {
        m2668().m2519(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        m2668().m2520(view, layoutParams);
    }

    public <T extends View> T findViewById(int i) {
        return m2668().m2515(i);
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        m2668().m2521(charSequence);
    }

    public void setTitle(int i) {
        super.setTitle(i);
        m2668().m2521(getContext().getString(i));
    }

    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        m2668().m2525(view, layoutParams);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        m2668().m2529();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2669(int i) {
        return m2668().m2528(i);
    }

    public void invalidateOptionsMenu() {
        m2668().m2531();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0462 m2668() {
        if (this.f1511 == null) {
            this.f1511 = C0462.m2509(this, this);
        }
        return this.f1511;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static int m2667(Context context, int i) {
        if (i != 0) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(C0727.C0728.dialogTheme, typedValue, true);
        return typedValue.resourceId;
    }
}
