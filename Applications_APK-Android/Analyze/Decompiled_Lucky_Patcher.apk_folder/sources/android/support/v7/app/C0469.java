package android.support.v7.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.C0463;
import android.support.v7.view.C0529;
import android.support.v7.view.C0534;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ActionMode;
import android.view.Window;

/* renamed from: android.support.v7.app.ˎ  reason: contains not printable characters */
/* compiled from: AppCompatDelegateImplV14 */
class C0469 extends C0468 {

    /* renamed from: ᵔ  reason: contains not printable characters */
    private int f1440 = -100;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private boolean f1441;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private boolean f1442 = true;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private C0471 f1443;

    C0469(Context context, Window window, C0461 r3) {
        super(context, window, r3);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2572(Bundle bundle) {
        super.m2611(bundle);
        if (bundle != null && this.f1440 == -100) {
            this.f1440 = bundle.getInt("appcompat:local_night_mode", -100);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Window.Callback m2571(Window.Callback callback) {
        return new C0470(callback);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m2579() {
        return this.f1442;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean m2578() {
        int r0 = m2568();
        int r1 = m2575(r0);
        boolean r12 = r1 != -1 ? m2567(r1) : false;
        if (r0 == 0) {
            m2569();
            this.f1443.m2583();
        }
        this.f1441 = true;
        return r12;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2573() {
        super.m2546();
        m2578();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m2576() {
        super.m2632();
        C0471 r0 = this.f1443;
        if (r0 != null) {
            r0.m2584();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public int m2575(int i) {
        if (i == -100) {
            return -1;
        }
        if (i != 0) {
            return i;
        }
        m2569();
        return this.f1443.m2581();
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int m2568() {
        int i = this.f1440;
        return i != -100 ? i : m2511();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2574(Bundle bundle) {
        super.m2547(bundle);
        int i = this.f1440;
        if (i != -100) {
            bundle.putInt("appcompat:local_night_mode", i);
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public void m2577() {
        super.m2638();
        C0471 r0 = this.f1443;
        if (r0 != null) {
            r0.m2584();
        }
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean m2567(int i) {
        Resources resources = this.f1421.getResources();
        Configuration configuration = resources.getConfiguration();
        int i2 = configuration.uiMode & 48;
        int i3 = i == 2 ? 32 : 16;
        if (i2 == i3) {
            return false;
        }
        if (m2570()) {
            ((Activity) this.f1421).recreate();
            return true;
        }
        Configuration configuration2 = new Configuration(configuration);
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        configuration2.uiMode = i3 | (configuration2.uiMode & -49);
        resources.updateConfiguration(configuration2, displayMetrics);
        if (Build.VERSION.SDK_INT >= 26) {
            return true;
        }
        C0483.m2676(resources);
        return true;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private void m2569() {
        if (this.f1443 == null) {
            this.f1443 = new C0471(C0489.m2710(this.f1421));
        }
    }

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private boolean m2570() {
        if (!this.f1441 || !(this.f1421 instanceof Activity)) {
            return false;
        }
        try {
            if ((this.f1421.getPackageManager().getActivityInfo(new ComponentName(this.f1421, this.f1421.getClass()), 0).configChanges & 512) == 0) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("AppCompatDelegate", "Exception while getting ActivityInfo", e);
            return true;
        }
    }

    /* renamed from: android.support.v7.app.ˎ$ʻ  reason: contains not printable characters */
    /* compiled from: AppCompatDelegateImplV14 */
    class C0470 extends C0463.C0465 {
        C0470(Window.Callback callback) {
            super(callback);
        }

        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            if (C0469.this.m2579()) {
                return m2580(callback);
            }
            return super.onWindowStartingActionMode(callback);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public final ActionMode m2580(ActionMode.Callback callback) {
            C0534.C0535 r0 = new C0534.C0535(C0469.this.f1421, callback);
            C0529 r3 = C0469.this.m2605(r0);
            if (r3 != null) {
                return r0.m3127(r3);
            }
            return null;
        }
    }

    /* renamed from: android.support.v7.app.ˎ$ʼ  reason: contains not printable characters */
    /* compiled from: AppCompatDelegateImplV14 */
    final class C0471 {

        /* renamed from: ʼ  reason: contains not printable characters */
        private C0489 f1446;

        /* renamed from: ʽ  reason: contains not printable characters */
        private boolean f1447;

        /* renamed from: ʾ  reason: contains not printable characters */
        private BroadcastReceiver f1448;

        /* renamed from: ʿ  reason: contains not printable characters */
        private IntentFilter f1449;

        C0471(C0489 r2) {
            this.f1446 = r2;
            this.f1447 = r2.m2714();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public final int m2581() {
            this.f1447 = this.f1446.m2714();
            return this.f1447 ? 2 : 1;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public final void m2582() {
            boolean r0 = this.f1446.m2714();
            if (r0 != this.f1447) {
                this.f1447 = r0;
                C0469.this.m2578();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public final void m2583() {
            m2584();
            if (this.f1448 == null) {
                this.f1448 = new BroadcastReceiver() {
                    public void onReceive(Context context, Intent intent) {
                        C0471.this.m2582();
                    }
                };
            }
            if (this.f1449 == null) {
                this.f1449 = new IntentFilter();
                this.f1449.addAction("android.intent.action.TIME_SET");
                this.f1449.addAction("android.intent.action.TIMEZONE_CHANGED");
                this.f1449.addAction("android.intent.action.TIME_TICK");
            }
            C0469.this.f1421.registerReceiver(this.f1448, this.f1449);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʾ  reason: contains not printable characters */
        public final void m2584() {
            if (this.f1448 != null) {
                C0469.this.f1421.unregisterReceiver(this.f1448);
                this.f1448 = null;
            }
        }
    }
}
