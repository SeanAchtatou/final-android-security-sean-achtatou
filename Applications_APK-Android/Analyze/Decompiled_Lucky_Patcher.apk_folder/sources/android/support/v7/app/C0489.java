package android.support.v7.app;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.content.C0112;
import android.util.Log;
import java.util.Calendar;

/* renamed from: android.support.v7.app.ᵎ  reason: contains not printable characters */
/* compiled from: TwilightManager */
class C0489 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static C0489 f1547;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Context f1548;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final LocationManager f1549;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final C0490 f1550 = new C0490();

    /* renamed from: ʻ  reason: contains not printable characters */
    static C0489 m2710(Context context) {
        if (f1547 == null) {
            Context applicationContext = context.getApplicationContext();
            f1547 = new C0489(applicationContext, (LocationManager) applicationContext.getSystemService("location"));
        }
        return f1547;
    }

    C0489(Context context, LocationManager locationManager) {
        this.f1548 = context;
        this.f1549 = locationManager;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2714() {
        C0490 r0 = this.f1550;
        if (m2713()) {
            return r0.f1551;
        }
        Location r1 = m2712();
        if (r1 != null) {
            m2711(r1);
            return r0.f1551;
        }
        Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
        int i = Calendar.getInstance().get(11);
        return i < 6 || i >= 22;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private Location m2712() {
        Location location = null;
        Location r0 = C0112.m585(this.f1548, "android.permission.ACCESS_COARSE_LOCATION") == 0 ? m2709("network") : null;
        if (C0112.m585(this.f1548, "android.permission.ACCESS_FINE_LOCATION") == 0) {
            location = m2709("gps");
        }
        return (location == null || r0 == null) ? location != null ? location : r0 : location.getTime() > r0.getTime() ? location : r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private Location m2709(String str) {
        LocationManager locationManager = this.f1549;
        if (locationManager == null) {
            return null;
        }
        try {
            if (locationManager.isProviderEnabled(str)) {
                return this.f1549.getLastKnownLocation(str);
            }
            return null;
        } catch (Exception e) {
            Log.d("TwilightManager", "Failed to get last known location", e);
            return null;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean m2713() {
        C0490 r0 = this.f1550;
        return r0 != null && r0.f1556 > System.currentTimeMillis();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m2711(Location location) {
        long j;
        C0490 r1 = this.f1550;
        long currentTimeMillis = System.currentTimeMillis();
        C0488 r11 = C0488.m2707();
        C0488 r2 = r11;
        r2.m2708(currentTimeMillis - 86400000, location.getLatitude(), location.getLongitude());
        long j2 = r11.f1544;
        r2.m2708(currentTimeMillis, location.getLatitude(), location.getLongitude());
        boolean z = r11.f1546 == 1;
        long j3 = r11.f1545;
        long j4 = j2;
        long j5 = r11.f1544;
        long j6 = j3;
        boolean z2 = z;
        r11.m2708(86400000 + currentTimeMillis, location.getLatitude(), location.getLongitude());
        long j7 = r11.f1545;
        if (j6 == -1 || j5 == -1) {
            j = 43200000 + currentTimeMillis;
        } else {
            j = (currentTimeMillis > j5 ? 0 + j7 : currentTimeMillis > j6 ? 0 + j5 : 0 + j6) + 60000;
        }
        r1.f1551 = z2;
        r1.f1552 = j4;
        r1.f1553 = j6;
        r1.f1554 = j5;
        r1.f1555 = j7;
        r1.f1556 = j;
    }

    /* renamed from: android.support.v7.app.ᵎ$ʻ  reason: contains not printable characters */
    /* compiled from: TwilightManager */
    private static class C0490 {

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean f1551;

        /* renamed from: ʼ  reason: contains not printable characters */
        long f1552;

        /* renamed from: ʽ  reason: contains not printable characters */
        long f1553;

        /* renamed from: ʾ  reason: contains not printable characters */
        long f1554;

        /* renamed from: ʿ  reason: contains not printable characters */
        long f1555;

        /* renamed from: ˆ  reason: contains not printable characters */
        long f1556;

        C0490() {
        }
    }
}
