package android.support.v7.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.view.C0529;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: android.support.v7.app.ʻ  reason: contains not printable characters */
/* compiled from: ActionBar */
public abstract class C0444 {

    /* renamed from: android.support.v7.app.ʻ$ʼ  reason: contains not printable characters */
    /* compiled from: ActionBar */
    public interface C0446 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m2455(boolean z);
    }

    @Deprecated
    /* renamed from: android.support.v7.app.ʻ$ʽ  reason: contains not printable characters */
    /* compiled from: ActionBar */
    public static abstract class C0447 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract Drawable m2456();

        /* renamed from: ʼ  reason: contains not printable characters */
        public abstract CharSequence m2457();

        /* renamed from: ʽ  reason: contains not printable characters */
        public abstract View m2458();

        /* renamed from: ʾ  reason: contains not printable characters */
        public abstract void m2459();

        /* renamed from: ʿ  reason: contains not printable characters */
        public abstract CharSequence m2460();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract int m2435();

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0529 m2436(C0529.C0530 r1) {
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2438(int i) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2439(Configuration configuration) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2440(Drawable drawable) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2441(CharSequence charSequence) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2442(boolean z) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2443(int i, KeyEvent keyEvent) {
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2444(KeyEvent keyEvent) {
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Context m2445() {
        return null;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2447(boolean z) {
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m2448() {
        return false;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m2449(boolean z) {
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m2450() {
        return false;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m2451(boolean z) {
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m2452() {
        return false;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m2453() {
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m2454() {
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2446(boolean z) {
        if (z) {
            throw new UnsupportedOperationException("Hide on content scroll is not supported in this action bar configuration.");
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2437(float f) {
        if (f != 0.0f) {
            throw new UnsupportedOperationException("Setting a non-zero elevation is not supported in this action bar configuration.");
        }
    }

    /* renamed from: android.support.v7.app.ʻ$ʻ  reason: contains not printable characters */
    /* compiled from: ActionBar */
    public static class C0445 extends ViewGroup.MarginLayoutParams {

        /* renamed from: ʻ  reason: contains not printable characters */
        public int f1390;

        public C0445(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.f1390 = 0;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0727.C0737.ActionBarLayout);
            this.f1390 = obtainStyledAttributes.getInt(C0727.C0737.ActionBarLayout_android_layout_gravity, 0);
            obtainStyledAttributes.recycle();
        }

        public C0445(int i, int i2) {
            super(i, i2);
            this.f1390 = 0;
            this.f1390 = 8388627;
        }

        public C0445(C0445 r2) {
            super((ViewGroup.MarginLayoutParams) r2);
            this.f1390 = 0;
            this.f1390 = r2.f1390;
        }

        public C0445(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.f1390 = 0;
        }
    }
}
