package android.support.v7.app;

import android.app.UiModeManager;
import android.content.Context;
import android.support.v7.app.C0469;
import android.view.ActionMode;
import android.view.Window;

/* renamed from: android.support.v7.app.ˏ  reason: contains not printable characters */
/* compiled from: AppCompatDelegateImplV23 */
class C0472 extends C0469 {

    /* renamed from: ᵔ  reason: contains not printable characters */
    private final UiModeManager f1451;

    C0472(Context context, Window window, C0461 r3) {
        super(context, window, r3);
        this.f1451 = (UiModeManager) context.getSystemService("uimode");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Window.Callback m2585(Window.Callback callback) {
        return new C0473(callback);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public int m2586(int i) {
        if (i == 0 && this.f1451.getNightMode() == 0) {
            return -1;
        }
        return super.m2575(i);
    }

    /* renamed from: android.support.v7.app.ˏ$ʻ  reason: contains not printable characters */
    /* compiled from: AppCompatDelegateImplV23 */
    class C0473 extends C0469.C0470 {
        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            return null;
        }

        C0473(Window.Callback callback) {
            super(callback);
        }

        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
            if (!C0472.this.m2579() || i != 0) {
                return super.onWindowStartingActionMode(callback, i);
            }
            return m2580(callback);
        }
    }
}
