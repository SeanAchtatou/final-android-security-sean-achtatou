package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.ˉ.C0414;
import android.support.v7.widget.C0652;
import android.support.v7.ʻ.C0727;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewStub;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.lang.ref.WeakReference;

class AlertController {

    /* renamed from: ʻ  reason: contains not printable characters */
    final C0480 f1282;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private CharSequence f1283;

    /* renamed from: ʼ  reason: contains not printable characters */
    ListView f1284;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private int f1285 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    Button f1286;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private CharSequence f1287;

    /* renamed from: ʾ  reason: contains not printable characters */
    Message f1288;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private ImageView f1289;

    /* renamed from: ʿ  reason: contains not printable characters */
    Button f1290;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private Drawable f1291;

    /* renamed from: ˆ  reason: contains not printable characters */
    Message f1292;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private TextView f1293;

    /* renamed from: ˈ  reason: contains not printable characters */
    Button f1294;

    /* renamed from: ˈˈ  reason: contains not printable characters */
    private int f1295;

    /* renamed from: ˉ  reason: contains not printable characters */
    Message f1296;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private View f1297;

    /* renamed from: ˊ  reason: contains not printable characters */
    NestedScrollView f1298;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private boolean f1299;

    /* renamed from: ˋ  reason: contains not printable characters */
    ListAdapter f1300;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private int f1301;

    /* renamed from: ˎ  reason: contains not printable characters */
    int f1302 = -1;

    /* renamed from: ˎˎ  reason: contains not printable characters */
    private final View.OnClickListener f1303 = new View.OnClickListener() {
        public void onClick(View view) {
            Message message;
            if (view == AlertController.this.f1286 && AlertController.this.f1288 != null) {
                message = Message.obtain(AlertController.this.f1288);
            } else if (view != AlertController.this.f1290 || AlertController.this.f1292 == null) {
                message = (view != AlertController.this.f1294 || AlertController.this.f1296 == null) ? null : Message.obtain(AlertController.this.f1296);
            } else {
                message = Message.obtain(AlertController.this.f1292);
            }
            if (message != null) {
                message.sendToTarget();
            }
            AlertController.this.f1310.obtainMessage(1, AlertController.this.f1282).sendToTarget();
        }
    };

    /* renamed from: ˏ  reason: contains not printable characters */
    int f1304;

    /* renamed from: ˏˏ  reason: contains not printable characters */
    private int f1305 = 0;

    /* renamed from: ˑ  reason: contains not printable characters */
    int f1306;

    /* renamed from: י  reason: contains not printable characters */
    int f1307;

    /* renamed from: ـ  reason: contains not printable characters */
    int f1308;

    /* renamed from: ــ  reason: contains not printable characters */
    private TextView f1309;

    /* renamed from: ٴ  reason: contains not printable characters */
    Handler f1310;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final Context f1311;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private boolean f1312 = false;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final Window f1313;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private CharSequence f1314;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private CharSequence f1315;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private CharSequence f1316;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private View f1317;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private int f1318;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private int f1319;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int f1320;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f1321;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private int f1322;

    /* renamed from: android.support.v7.app.AlertController$ʼ  reason: contains not printable characters */
    private static final class C0442 extends Handler {

        /* renamed from: ʻ  reason: contains not printable characters */
        private WeakReference<DialogInterface> f1389;

        public C0442(DialogInterface dialogInterface) {
            this.f1389 = new WeakReference<>(dialogInterface);
        }

        public void handleMessage(Message message) {
            int i = message.what;
            if (i == -3 || i == -2 || i == -1) {
                ((DialogInterface.OnClickListener) message.obj).onClick(this.f1389.get(), message.what);
            } else if (i == 1) {
                ((DialogInterface) message.obj).dismiss();
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean m2410(Context context) {
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(C0727.C0728.alertDialogCenterButtons, typedValue, true);
        if (typedValue.data != 0) {
            return true;
        }
        return false;
    }

    public AlertController(Context context, C0480 r5, Window window) {
        this.f1311 = context;
        this.f1282 = r5;
        this.f1313 = window;
        this.f1310 = new C0442(r5);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(null, C0727.C0737.AlertDialog, C0727.C0728.alertDialogStyle, 0);
        this.f1295 = obtainStyledAttributes.getResourceId(C0727.C0737.AlertDialog_android_layout, 0);
        this.f1301 = obtainStyledAttributes.getResourceId(C0727.C0737.AlertDialog_buttonPanelSideLayout, 0);
        this.f1304 = obtainStyledAttributes.getResourceId(C0727.C0737.AlertDialog_listLayout, 0);
        this.f1306 = obtainStyledAttributes.getResourceId(C0727.C0737.AlertDialog_multiChoiceItemLayout, 0);
        this.f1307 = obtainStyledAttributes.getResourceId(C0727.C0737.AlertDialog_singleChoiceItemLayout, 0);
        this.f1308 = obtainStyledAttributes.getResourceId(C0727.C0737.AlertDialog_listItemLayout, 0);
        this.f1299 = obtainStyledAttributes.getBoolean(C0727.C0737.AlertDialog_showTitle, true);
        obtainStyledAttributes.recycle();
        r5.m2669(1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static boolean m2411(View view) {
        if (view.onCheckIsTextEditor()) {
            return true;
        }
        if (!(view instanceof ViewGroup)) {
            return false;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        while (childCount > 0) {
            childCount--;
            if (m2411(viewGroup.getChildAt(childCount))) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2417() {
        this.f1282.setContentView(m2412());
        m2414();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int m2412() {
        int i = this.f1301;
        if (i == 0) {
            return this.f1295;
        }
        if (this.f1305 == 1) {
            return i;
        }
        return this.f1295;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2422(CharSequence charSequence) {
        this.f1315 = charSequence;
        TextView textView = this.f1309;
        if (textView != null) {
            textView.setText(charSequence);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2425(View view) {
        this.f1297 = view;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2426(CharSequence charSequence) {
        this.f1316 = charSequence;
        TextView textView = this.f1293;
        if (textView != null) {
            textView.setText(charSequence);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2418(int i) {
        this.f1317 = null;
        this.f1318 = i;
        this.f1312 = false;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2429(View view) {
        this.f1317 = view;
        this.f1318 = 0;
        this.f1312 = false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2421(View view, int i, int i2, int i3, int i4) {
        this.f1317 = view;
        this.f1318 = 0;
        this.f1312 = true;
        this.f1319 = i;
        this.f1320 = i2;
        this.f1321 = i3;
        this.f1322 = i4;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2419(int i, CharSequence charSequence, DialogInterface.OnClickListener onClickListener, Message message) {
        if (message == null && onClickListener != null) {
            message = this.f1310.obtainMessage(i, onClickListener);
        }
        if (i == -3) {
            this.f1287 = charSequence;
            this.f1296 = message;
        } else if (i == -2) {
            this.f1283 = charSequence;
            this.f1292 = message;
        } else if (i == -1) {
            this.f1314 = charSequence;
            this.f1288 = message;
        } else {
            throw new IllegalArgumentException("Button does not exist");
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2424(int i) {
        this.f1291 = null;
        this.f1285 = i;
        ImageView imageView = this.f1289;
        if (imageView == null) {
            return;
        }
        if (i != 0) {
            imageView.setVisibility(0);
            this.f1289.setImageResource(this.f1285);
            return;
        }
        imageView.setVisibility(8);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2420(Drawable drawable) {
        this.f1291 = drawable;
        this.f1285 = 0;
        ImageView imageView = this.f1289;
        if (imageView == null) {
            return;
        }
        if (drawable != null) {
            imageView.setVisibility(0);
            this.f1289.setImageDrawable(drawable);
            return;
        }
        imageView.setVisibility(8);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m2428(int i) {
        TypedValue typedValue = new TypedValue();
        this.f1311.getTheme().resolveAttribute(i, typedValue, true);
        return typedValue.resourceId;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m2423(int i, KeyEvent keyEvent) {
        NestedScrollView nestedScrollView = this.f1298;
        return nestedScrollView != null && nestedScrollView.m948(keyEvent);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m2427(int i, KeyEvent keyEvent) {
        NestedScrollView nestedScrollView = this.f1298;
        return nestedScrollView != null && nestedScrollView.m948(keyEvent);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private ViewGroup m2405(View view, View view2) {
        if (view == null) {
            if (view2 instanceof ViewStub) {
                view2 = ((ViewStub) view2).inflate();
            }
            return (ViewGroup) view2;
        }
        if (view2 != null) {
            ViewParent parent = view2.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(view2);
            }
        }
        if (view instanceof ViewStub) {
            view = ((ViewStub) view).inflate();
        }
        return (ViewGroup) view;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m2414() {
        ListAdapter listAdapter;
        View findViewById;
        View findViewById2;
        View findViewById3 = this.f1313.findViewById(C0727.C0733.parentPanel);
        View findViewById4 = findViewById3.findViewById(C0727.C0733.topPanel);
        View findViewById5 = findViewById3.findViewById(C0727.C0733.contentPanel);
        View findViewById6 = findViewById3.findViewById(C0727.C0733.buttonPanel);
        ViewGroup viewGroup = (ViewGroup) findViewById3.findViewById(C0727.C0733.customPanel);
        m2407(viewGroup);
        View findViewById7 = viewGroup.findViewById(C0727.C0733.topPanel);
        View findViewById8 = viewGroup.findViewById(C0727.C0733.contentPanel);
        View findViewById9 = viewGroup.findViewById(C0727.C0733.buttonPanel);
        ViewGroup r1 = m2405(findViewById7, findViewById4);
        ViewGroup r2 = m2405(findViewById8, findViewById5);
        ViewGroup r3 = m2405(findViewById9, findViewById6);
        m2415(r2);
        m2416(r3);
        m2413(r1);
        char c = 0;
        boolean z = (viewGroup == null || viewGroup.getVisibility() == 8) ? false : true;
        boolean z2 = (r1 == null || r1.getVisibility() == 8) ? false : true;
        boolean z3 = (r3 == null || r3.getVisibility() == 8) ? false : true;
        if (!(z3 || r2 == null || (findViewById2 = r2.findViewById(C0727.C0733.textSpacerNoButtons)) == null)) {
            findViewById2.setVisibility(0);
        }
        if (z2) {
            NestedScrollView nestedScrollView = this.f1298;
            if (nestedScrollView != null) {
                nestedScrollView.setClipToPadding(true);
            }
            View view = null;
            if (!(this.f1316 == null && this.f1284 == null)) {
                view = r1.findViewById(C0727.C0733.titleDividerNoCustom);
            }
            if (view != null) {
                view.setVisibility(0);
            }
        } else if (!(r2 == null || (findViewById = r2.findViewById(C0727.C0733.textSpacerNoTitle)) == null)) {
            findViewById.setVisibility(0);
        }
        ListView listView = this.f1284;
        if (listView instanceof RecycleListView) {
            ((RecycleListView) listView).m2431(z2, z3);
        }
        if (!z) {
            View view2 = this.f1284;
            if (view2 == null) {
                view2 = this.f1298;
            }
            if (view2 != null) {
                if (z3) {
                    c = 2;
                }
                m2408(r2, view2, z2 | c ? 1 : 0, 3);
            }
        }
        ListView listView2 = this.f1284;
        if (listView2 != null && (listAdapter = this.f1300) != null) {
            listView2.setAdapter(listAdapter);
            int i = this.f1302;
            if (i > -1) {
                listView2.setItemChecked(i, true);
                listView2.setSelection(i);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m2408(ViewGroup viewGroup, View view, int i, int i2) {
        final View findViewById = this.f1313.findViewById(C0727.C0733.scrollIndicatorUp);
        View findViewById2 = this.f1313.findViewById(C0727.C0733.scrollIndicatorDown);
        if (Build.VERSION.SDK_INT >= 23) {
            C0414.m2219(view, i, i2);
            if (findViewById != null) {
                viewGroup.removeView(findViewById);
            }
            if (findViewById2 != null) {
                viewGroup.removeView(findViewById2);
                return;
            }
            return;
        }
        final View view2 = null;
        if (findViewById != null && (i & 1) == 0) {
            viewGroup.removeView(findViewById);
            findViewById = null;
        }
        if (findViewById2 == null || (i & 2) != 0) {
            view2 = findViewById2;
        } else {
            viewGroup.removeView(findViewById2);
        }
        if (findViewById != null || view2 != null) {
            if (this.f1316 != null) {
                this.f1298.setOnScrollChangeListener(new NestedScrollView.C0150() {
                    /* renamed from: ʻ  reason: contains not printable characters */
                    public void m2430(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4) {
                        AlertController.m2406(nestedScrollView, findViewById, view2);
                    }
                });
                this.f1298.post(new Runnable() {
                    public void run() {
                        AlertController.m2406(AlertController.this.f1298, findViewById, view2);
                    }
                });
                return;
            }
            ListView listView = this.f1284;
            if (listView != null) {
                listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                    public void onScrollStateChanged(AbsListView absListView, int i) {
                    }

                    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                        AlertController.m2406(absListView, findViewById, view2);
                    }
                });
                this.f1284.post(new Runnable() {
                    public void run() {
                        AlertController.m2406(AlertController.this.f1284, findViewById, view2);
                    }
                });
                return;
            }
            if (findViewById != null) {
                viewGroup.removeView(findViewById);
            }
            if (view2 != null) {
                viewGroup.removeView(view2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m2407(ViewGroup viewGroup) {
        View view = this.f1317;
        boolean z = false;
        if (view == null) {
            view = this.f1318 != 0 ? LayoutInflater.from(this.f1311).inflate(this.f1318, viewGroup, false) : null;
        }
        if (view != null) {
            z = true;
        }
        if (!z || !m2411(view)) {
            this.f1313.setFlags(131072, 131072);
        }
        if (z) {
            FrameLayout frameLayout = (FrameLayout) this.f1313.findViewById(C0727.C0733.custom);
            frameLayout.addView(view, new ViewGroup.LayoutParams(-1, -1));
            if (this.f1312) {
                frameLayout.setPadding(this.f1319, this.f1320, this.f1321, this.f1322);
            }
            if (this.f1284 != null) {
                ((C0652.C0653) viewGroup.getLayoutParams()).f2602 = 0.0f;
                return;
            }
            return;
        }
        viewGroup.setVisibility(8);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m2413(ViewGroup viewGroup) {
        if (this.f1297 != null) {
            viewGroup.addView(this.f1297, 0, new ViewGroup.LayoutParams(-1, -2));
            this.f1313.findViewById(C0727.C0733.title_template).setVisibility(8);
            return;
        }
        this.f1289 = (ImageView) this.f1313.findViewById(16908294);
        if (!(!TextUtils.isEmpty(this.f1315)) || !this.f1299) {
            this.f1313.findViewById(C0727.C0733.title_template).setVisibility(8);
            this.f1289.setVisibility(8);
            viewGroup.setVisibility(8);
            return;
        }
        this.f1309 = (TextView) this.f1313.findViewById(C0727.C0733.alertTitle);
        this.f1309.setText(this.f1315);
        int i = this.f1285;
        if (i != 0) {
            this.f1289.setImageResource(i);
            return;
        }
        Drawable drawable = this.f1291;
        if (drawable != null) {
            this.f1289.setImageDrawable(drawable);
            return;
        }
        this.f1309.setPadding(this.f1289.getPaddingLeft(), this.f1289.getPaddingTop(), this.f1289.getPaddingRight(), this.f1289.getPaddingBottom());
        this.f1289.setVisibility(8);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m2415(ViewGroup viewGroup) {
        this.f1298 = (NestedScrollView) this.f1313.findViewById(C0727.C0733.scrollView);
        this.f1298.setFocusable(false);
        this.f1298.setNestedScrollingEnabled(false);
        this.f1293 = (TextView) viewGroup.findViewById(16908299);
        TextView textView = this.f1293;
        if (textView != null) {
            CharSequence charSequence = this.f1316;
            if (charSequence != null) {
                textView.setText(charSequence);
                return;
            }
            textView.setVisibility(8);
            this.f1298.removeView(this.f1293);
            if (this.f1284 != null) {
                ViewGroup viewGroup2 = (ViewGroup) this.f1298.getParent();
                int indexOfChild = viewGroup2.indexOfChild(this.f1298);
                viewGroup2.removeViewAt(indexOfChild);
                viewGroup2.addView(this.f1284, indexOfChild, new ViewGroup.LayoutParams(-1, -1));
                return;
            }
            viewGroup.setVisibility(8);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m2406(View view, View view2, View view3) {
        int i = 0;
        if (view2 != null) {
            view2.setVisibility(view.canScrollVertically(-1) ? 0 : 4);
        }
        if (view3 != null) {
            if (!view.canScrollVertically(1)) {
                i = 4;
            }
            view3.setVisibility(i);
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m2416(ViewGroup viewGroup) {
        boolean z;
        this.f1286 = (Button) viewGroup.findViewById(16908313);
        this.f1286.setOnClickListener(this.f1303);
        boolean z2 = true;
        if (TextUtils.isEmpty(this.f1314)) {
            this.f1286.setVisibility(8);
            z = false;
        } else {
            this.f1286.setText(this.f1314);
            this.f1286.setVisibility(0);
            z = true;
        }
        this.f1290 = (Button) viewGroup.findViewById(16908314);
        this.f1290.setOnClickListener(this.f1303);
        if (TextUtils.isEmpty(this.f1283)) {
            this.f1290.setVisibility(8);
        } else {
            this.f1290.setText(this.f1283);
            this.f1290.setVisibility(0);
            z |= true;
        }
        this.f1294 = (Button) viewGroup.findViewById(16908315);
        this.f1294.setOnClickListener(this.f1303);
        if (TextUtils.isEmpty(this.f1287)) {
            this.f1294.setVisibility(8);
        } else {
            this.f1294.setText(this.f1287);
            this.f1294.setVisibility(0);
            z |= true;
        }
        if (m2410(this.f1311)) {
            if (z) {
                m2409(this.f1286);
            } else if (z) {
                m2409(this.f1290);
            } else if (z) {
                m2409(this.f1294);
            }
        }
        if (!z) {
            z2 = false;
        }
        if (!z2) {
            viewGroup.setVisibility(8);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m2409(Button button) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) button.getLayoutParams();
        layoutParams.gravity = 1;
        layoutParams.weight = 0.5f;
        button.setLayoutParams(layoutParams);
    }

    public static class RecycleListView extends ListView {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final int f1336;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final int f1337;

        public RecycleListView(Context context) {
            this(context, null);
        }

        public RecycleListView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0727.C0737.RecycleListView);
            this.f1337 = obtainStyledAttributes.getDimensionPixelOffset(C0727.C0737.RecycleListView_paddingBottomNoButtons, -1);
            this.f1336 = obtainStyledAttributes.getDimensionPixelOffset(C0727.C0737.RecycleListView_paddingTopNoTitle, -1);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2431(boolean z, boolean z2) {
            if (!z2 || !z) {
                setPadding(getPaddingLeft(), z ? getPaddingTop() : this.f1336, getPaddingRight(), z2 ? getPaddingBottom() : this.f1337);
            }
        }
    }

    /* renamed from: android.support.v7.app.AlertController$ʻ  reason: contains not printable characters */
    public static class C0440 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public final Context f1338;

        /* renamed from: ʻʻ  reason: contains not printable characters */
        public boolean[] f1339;

        /* renamed from: ʼ  reason: contains not printable characters */
        public final LayoutInflater f1340;

        /* renamed from: ʼʼ  reason: contains not printable characters */
        public boolean f1341;

        /* renamed from: ʽ  reason: contains not printable characters */
        public int f1342 = 0;

        /* renamed from: ʽʽ  reason: contains not printable characters */
        public boolean f1343;

        /* renamed from: ʾ  reason: contains not printable characters */
        public Drawable f1344;

        /* renamed from: ʾʾ  reason: contains not printable characters */
        public DialogInterface.OnMultiChoiceClickListener f1345;

        /* renamed from: ʿ  reason: contains not printable characters */
        public int f1346 = 0;

        /* renamed from: ʿʿ  reason: contains not printable characters */
        public int f1347 = -1;

        /* renamed from: ˆ  reason: contains not printable characters */
        public CharSequence f1348;

        /* renamed from: ˆˆ  reason: contains not printable characters */
        public String f1349;

        /* renamed from: ˈ  reason: contains not printable characters */
        public View f1350;

        /* renamed from: ˈˈ  reason: contains not printable characters */
        public AdapterView.OnItemSelectedListener f1351;

        /* renamed from: ˉ  reason: contains not printable characters */
        public CharSequence f1352;

        /* renamed from: ˉˉ  reason: contains not printable characters */
        public String f1353;

        /* renamed from: ˊ  reason: contains not printable characters */
        public CharSequence f1354;

        /* renamed from: ˊˊ  reason: contains not printable characters */
        public boolean f1355 = true;

        /* renamed from: ˋ  reason: contains not printable characters */
        public DialogInterface.OnClickListener f1356;

        /* renamed from: ˋˋ  reason: contains not printable characters */
        public C0441 f1357;

        /* renamed from: ˎ  reason: contains not printable characters */
        public CharSequence f1358;

        /* renamed from: ˏ  reason: contains not printable characters */
        public DialogInterface.OnClickListener f1359;

        /* renamed from: ˑ  reason: contains not printable characters */
        public CharSequence f1360;

        /* renamed from: י  reason: contains not printable characters */
        public DialogInterface.OnClickListener f1361;

        /* renamed from: ـ  reason: contains not printable characters */
        public boolean f1362;

        /* renamed from: ــ  reason: contains not printable characters */
        public Cursor f1363;

        /* renamed from: ٴ  reason: contains not printable characters */
        public DialogInterface.OnCancelListener f1364;

        /* renamed from: ᐧ  reason: contains not printable characters */
        public DialogInterface.OnDismissListener f1365;

        /* renamed from: ᐧᐧ  reason: contains not printable characters */
        public int f1366;

        /* renamed from: ᴵ  reason: contains not printable characters */
        public DialogInterface.OnKeyListener f1367;

        /* renamed from: ᴵᴵ  reason: contains not printable characters */
        public boolean f1368 = false;

        /* renamed from: ᵎ  reason: contains not printable characters */
        public CharSequence[] f1369;

        /* renamed from: ᵔ  reason: contains not printable characters */
        public ListAdapter f1370;

        /* renamed from: ᵢ  reason: contains not printable characters */
        public DialogInterface.OnClickListener f1371;

        /* renamed from: ⁱ  reason: contains not printable characters */
        public int f1372;

        /* renamed from: ﹳ  reason: contains not printable characters */
        public View f1373;

        /* renamed from: ﹶ  reason: contains not printable characters */
        public int f1374;

        /* renamed from: ﾞ  reason: contains not printable characters */
        public int f1375;

        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        public int f1376;

        /* renamed from: android.support.v7.app.AlertController$ʻ$ʻ  reason: contains not printable characters */
        public interface C0441 {
            /* renamed from: ʻ  reason: contains not printable characters */
            void m2434(ListView listView);
        }

        public C0440(Context context) {
            this.f1338 = context;
            this.f1362 = true;
            this.f1340 = (LayoutInflater) context.getSystemService("layout_inflater");
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2433(AlertController alertController) {
            View view = this.f1350;
            if (view != null) {
                alertController.m2425(view);
            } else {
                CharSequence charSequence = this.f1348;
                if (charSequence != null) {
                    alertController.m2422(charSequence);
                }
                Drawable drawable = this.f1344;
                if (drawable != null) {
                    alertController.m2420(drawable);
                }
                int i = this.f1342;
                if (i != 0) {
                    alertController.m2424(i);
                }
                int i2 = this.f1346;
                if (i2 != 0) {
                    alertController.m2424(alertController.m2428(i2));
                }
            }
            CharSequence charSequence2 = this.f1352;
            if (charSequence2 != null) {
                alertController.m2426(charSequence2);
            }
            CharSequence charSequence3 = this.f1354;
            if (charSequence3 != null) {
                alertController.m2419(-1, charSequence3, this.f1356, (Message) null);
            }
            CharSequence charSequence4 = this.f1358;
            if (charSequence4 != null) {
                alertController.m2419(-2, charSequence4, this.f1359, (Message) null);
            }
            CharSequence charSequence5 = this.f1360;
            if (charSequence5 != null) {
                alertController.m2419(-3, charSequence5, this.f1361, (Message) null);
            }
            if (!(this.f1369 == null && this.f1363 == null && this.f1370 == null)) {
                m2432(alertController);
            }
            View view2 = this.f1373;
            if (view2 == null) {
                int i3 = this.f1372;
                if (i3 != 0) {
                    alertController.m2418(i3);
                }
            } else if (this.f1368) {
                alertController.m2421(view2, this.f1374, this.f1375, this.f1376, this.f1366);
            } else {
                alertController.m2429(view2);
            }
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARN: Type inference failed for: r9v0, types: [android.widget.ListAdapter] */
        /* JADX WARN: Type inference failed for: r9v3 */
        /* JADX WARN: Type inference failed for: r9v4 */
        /* JADX WARN: Type inference failed for: r2v6, types: [android.widget.SimpleCursorAdapter] */
        /* JADX WARN: Type inference failed for: r1v19, types: [android.support.v7.app.AlertController$ʻ$2] */
        /* JADX WARN: Type inference failed for: r1v20, types: [android.support.v7.app.AlertController$ʻ$1] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* renamed from: ʼ  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void m2432(final android.support.v7.app.AlertController r12) {
            /*
                r11 = this;
                android.view.LayoutInflater r0 = r11.f1340
                int r1 = r12.f1304
                r2 = 0
                android.view.View r0 = r0.inflate(r1, r2)
                android.support.v7.app.AlertController$RecycleListView r0 = (android.support.v7.app.AlertController.RecycleListView) r0
                boolean r1 = r11.f1343
                r8 = 1
                if (r1 == 0) goto L_0x0033
                android.database.Cursor r4 = r11.f1363
                if (r4 != 0) goto L_0x0026
                android.support.v7.app.AlertController$ʻ$1 r9 = new android.support.v7.app.AlertController$ʻ$1
                android.content.Context r3 = r11.f1338
                int r4 = r12.f1306
                r5 = 16908308(0x1020014, float:2.3877285E-38)
                java.lang.CharSequence[] r6 = r11.f1369
                r1 = r9
                r2 = r11
                r7 = r0
                r1.<init>(r3, r4, r5, r6, r7)
                goto L_0x0067
            L_0x0026:
                android.support.v7.app.AlertController$ʻ$2 r9 = new android.support.v7.app.AlertController$ʻ$2
                android.content.Context r3 = r11.f1338
                r5 = 0
                r1 = r9
                r2 = r11
                r6 = r0
                r7 = r12
                r1.<init>(r3, r4, r5, r6, r7)
                goto L_0x0067
            L_0x0033:
                boolean r1 = r11.f1341
                if (r1 == 0) goto L_0x003a
                int r1 = r12.f1307
                goto L_0x003c
            L_0x003a:
                int r1 = r12.f1308
            L_0x003c:
                r4 = r1
                android.database.Cursor r5 = r11.f1363
                r1 = 16908308(0x1020014, float:2.3877285E-38)
                if (r5 == 0) goto L_0x0059
                android.widget.SimpleCursorAdapter r9 = new android.widget.SimpleCursorAdapter
                android.content.Context r3 = r11.f1338
                java.lang.String[] r6 = new java.lang.String[r8]
                java.lang.String r2 = r11.f1349
                r7 = 0
                r6[r7] = r2
                int[] r10 = new int[r8]
                r10[r7] = r1
                r2 = r9
                r7 = r10
                r2.<init>(r3, r4, r5, r6, r7)
                goto L_0x0067
            L_0x0059:
                android.widget.ListAdapter r9 = r11.f1370
                if (r9 == 0) goto L_0x005e
                goto L_0x0067
            L_0x005e:
                android.support.v7.app.AlertController$ʽ r9 = new android.support.v7.app.AlertController$ʽ
                android.content.Context r2 = r11.f1338
                java.lang.CharSequence[] r3 = r11.f1369
                r9.<init>(r2, r4, r1, r3)
            L_0x0067:
                android.support.v7.app.AlertController$ʻ$ʻ r1 = r11.f1357
                if (r1 == 0) goto L_0x006e
                r1.m2434(r0)
            L_0x006e:
                r12.f1300 = r9
                int r1 = r11.f1347
                r12.f1302 = r1
                android.content.DialogInterface$OnClickListener r1 = r11.f1371
                if (r1 == 0) goto L_0x0081
                android.support.v7.app.AlertController$ʻ$3 r1 = new android.support.v7.app.AlertController$ʻ$3
                r1.<init>(r12)
                r0.setOnItemClickListener(r1)
                goto L_0x008d
            L_0x0081:
                android.content.DialogInterface$OnMultiChoiceClickListener r1 = r11.f1345
                if (r1 == 0) goto L_0x008d
                android.support.v7.app.AlertController$ʻ$4 r1 = new android.support.v7.app.AlertController$ʻ$4
                r1.<init>(r0, r12)
                r0.setOnItemClickListener(r1)
            L_0x008d:
                android.widget.AdapterView$OnItemSelectedListener r1 = r11.f1351
                if (r1 == 0) goto L_0x0094
                r0.setOnItemSelectedListener(r1)
            L_0x0094:
                boolean r1 = r11.f1341
                if (r1 == 0) goto L_0x009c
                r0.setChoiceMode(r8)
                goto L_0x00a4
            L_0x009c:
                boolean r1 = r11.f1343
                if (r1 == 0) goto L_0x00a4
                r1 = 2
                r0.setChoiceMode(r1)
            L_0x00a4:
                r12.f1284 = r0
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.AlertController.C0440.m2432(android.support.v7.app.AlertController):void");
        }
    }

    /* renamed from: android.support.v7.app.AlertController$ʽ  reason: contains not printable characters */
    private static class C0443 extends ArrayAdapter<CharSequence> {
        public long getItemId(int i) {
            return (long) i;
        }

        public boolean hasStableIds() {
            return true;
        }

        public C0443(Context context, int i, int i2, CharSequence[] charSequenceArr) {
            super(context, i, i2, charSequenceArr);
        }
    }
}
