package android.support.v7.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.ʻ.C0190;
import android.support.v4.ʻ.C0227;
import android.support.v4.ʻ.C0242;
import android.support.v4.ʻ.C0270;
import android.support.v7.app.C0448;
import android.support.v7.view.C0529;
import android.support.v7.widget.C0600;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

/* renamed from: android.support.v7.app.ʿ  reason: contains not printable characters */
/* compiled from: AppCompatActivity */
public class C0460 extends C0227 implements C0242.C0243, C0448.C0450, C0461 {
    private C0462 mDelegate;
    private Resources mResources;
    private int mThemeId = 0;

    public void onPrepareSupportNavigateUpTaskStack(C0242 r1) {
    }

    public void onSupportActionModeFinished(C0529 r1) {
    }

    public void onSupportActionModeStarted(C0529 r1) {
    }

    @Deprecated
    public void onSupportContentChanged() {
    }

    public C0529 onWindowStartingSupportActionMode(C0529.C0530 r1) {
        return null;
    }

    @Deprecated
    public void setSupportProgress(int i) {
    }

    @Deprecated
    public void setSupportProgressBarIndeterminate(boolean z) {
    }

    @Deprecated
    public void setSupportProgressBarIndeterminateVisibility(boolean z) {
    }

    @Deprecated
    public void setSupportProgressBarVisibility(boolean z) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        C0462 delegate = getDelegate();
        delegate.m2534();
        delegate.m2517(bundle);
        if (delegate.m2535() && this.mThemeId != 0) {
            if (Build.VERSION.SDK_INT >= 23) {
                onApplyThemeResource(getTheme(), this.mThemeId, false);
            } else {
                setTheme(this.mThemeId);
            }
        }
        super.onCreate(bundle);
    }

    public void setTheme(int i) {
        super.setTheme(i);
        this.mThemeId = i;
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        getDelegate().m2524(bundle);
    }

    public C0444 getSupportActionBar() {
        return getDelegate().m2513();
    }

    public void setSupportActionBar(Toolbar toolbar) {
        getDelegate().m2518(toolbar);
    }

    public MenuInflater getMenuInflater() {
        return getDelegate().m2522();
    }

    public void setContentView(int i) {
        getDelegate().m2523(i);
    }

    public void setContentView(View view) {
        getDelegate().m2519(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        getDelegate().m2520(view, layoutParams);
    }

    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        getDelegate().m2525(view, layoutParams);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        getDelegate().m2516(configuration);
        if (this.mResources != null) {
            this.mResources.updateConfiguration(configuration, super.getResources().getDisplayMetrics());
        }
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        getDelegate().m2530();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        getDelegate().m2526();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        getDelegate().m2529();
    }

    public <T extends View> T findViewById(int i) {
        return getDelegate().m2515(i);
    }

    public final boolean onMenuItemSelected(int i, MenuItem menuItem) {
        if (super.onMenuItemSelected(i, menuItem)) {
            return true;
        }
        C0444 supportActionBar = getSupportActionBar();
        if (menuItem.getItemId() != 16908332 || supportActionBar == null || (supportActionBar.m2435() & 4) == 0) {
            return false;
        }
        return onSupportNavigateUp();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        getDelegate().m2532();
    }

    /* access modifiers changed from: protected */
    public void onTitleChanged(CharSequence charSequence, int i) {
        super.onTitleChanged(charSequence, i);
        getDelegate().m2521(charSequence);
    }

    public boolean supportRequestWindowFeature(int i) {
        return getDelegate().m2528(i);
    }

    public void supportInvalidateOptionsMenu() {
        getDelegate().m2531();
    }

    public void invalidateOptionsMenu() {
        getDelegate().m2531();
    }

    public C0529 startSupportActionMode(C0529.C0530 r2) {
        return getDelegate().m2514(r2);
    }

    public void onCreateSupportNavigateUpTaskStack(C0242 r1) {
        r1.m1425((Activity) this);
    }

    public boolean onSupportNavigateUp() {
        Intent supportParentActivityIntent = getSupportParentActivityIntent();
        if (supportParentActivityIntent == null) {
            return false;
        }
        if (supportShouldUpRecreateTask(supportParentActivityIntent)) {
            C0242 r0 = C0242.m1424((Context) this);
            onCreateSupportNavigateUpTaskStack(r0);
            onPrepareSupportNavigateUpTaskStack(r0);
            r0.m1428();
            try {
                C0190.m1118((Activity) this);
                return true;
            } catch (IllegalStateException unused) {
                finish();
                return true;
            }
        } else {
            supportNavigateUpTo(supportParentActivityIntent);
            return true;
        }
    }

    public Intent getSupportParentActivityIntent() {
        return C0270.m1653(this);
    }

    public boolean supportShouldUpRecreateTask(Intent intent) {
        return C0270.m1655(this, intent);
    }

    public void supportNavigateUpTo(Intent intent) {
        C0270.m1658(this, intent);
    }

    public void onContentChanged() {
        onSupportContentChanged();
    }

    public C0448.C0449 getDrawerToggleDelegate() {
        return getDelegate().m2533();
    }

    public boolean onMenuOpened(int i, Menu menu) {
        return super.onMenuOpened(i, menu);
    }

    public void onPanelClosed(int i, Menu menu) {
        super.onPanelClosed(i, menu);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        getDelegate().m2527(bundle);
    }

    public C0462 getDelegate() {
        if (this.mDelegate == null) {
            this.mDelegate = C0462.m2508(this, this);
        }
        return this.mDelegate;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        C0444 supportActionBar = getSupportActionBar();
        if (keyCode != 82 || supportActionBar == null || !supportActionBar.m2444(keyEvent)) {
            return super.dispatchKeyEvent(keyEvent);
        }
        return true;
    }

    public Resources getResources() {
        if (this.mResources == null && C0600.m3819()) {
            this.mResources = new C0600(this, super.getResources());
        }
        Resources resources = this.mResources;
        return resources == null ? super.getResources() : resources;
    }

    private boolean performMenuItemShortcut(int i, KeyEvent keyEvent) {
        Window window;
        return Build.VERSION.SDK_INT < 26 && !keyEvent.isCtrlPressed() && !KeyEvent.metaStateHasNoModifiers(keyEvent.getMetaState()) && keyEvent.getRepeatCount() == 0 && !KeyEvent.isModifierKey(keyEvent.getKeyCode()) && (window = getWindow()) != null && window.getDecorView() != null && window.getDecorView().dispatchKeyShortcutEvent(keyEvent);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (performMenuItemShortcut(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public void openOptionsMenu() {
        C0444 supportActionBar = getSupportActionBar();
        if (!getWindow().hasFeature(0)) {
            return;
        }
        if (supportActionBar == null || !supportActionBar.m2448()) {
            super.openOptionsMenu();
        }
    }

    public void closeOptionsMenu() {
        C0444 supportActionBar = getSupportActionBar();
        if (!getWindow().hasFeature(0)) {
            return;
        }
        if (supportActionBar == null || !supportActionBar.m2450()) {
            super.closeOptionsMenu();
        }
    }
}
