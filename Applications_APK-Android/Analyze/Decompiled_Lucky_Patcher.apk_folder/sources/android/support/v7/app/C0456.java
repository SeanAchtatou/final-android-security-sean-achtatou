package android.support.v7.app;

import android.app.ActionBar;
import android.app.Activity;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import java.lang.reflect.Method;

/* renamed from: android.support.v7.app.ʽ  reason: contains not printable characters */
/* compiled from: ActionBarDrawerToggleHoneycomb */
class C0456 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final int[] f1409 = {16843531};

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0457 m2499(C0457 r5, Activity activity, Drawable drawable, int i) {
        C0457 r52 = new C0457(activity);
        if (r52.f1410 != null) {
            try {
                ActionBar actionBar = activity.getActionBar();
                r52.f1410.invoke(actionBar, drawable);
                r52.f1411.invoke(actionBar, Integer.valueOf(i));
            } catch (Exception e) {
                Log.w("ActionBarDrawerToggleHoneycomb", "Couldn't set home-as-up indicator via JB-MR2 API", e);
            }
        } else if (r52.f1412 != null) {
            r52.f1412.setImageDrawable(drawable);
        } else {
            Log.w("ActionBarDrawerToggleHoneycomb", "Couldn't set home-as-up indicator");
        }
        return r52;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0457 m2498(C0457 r3, Activity activity, int i) {
        if (r3 == null) {
            r3 = new C0457(activity);
        }
        if (r3.f1410 != null) {
            try {
                ActionBar actionBar = activity.getActionBar();
                r3.f1411.invoke(actionBar, Integer.valueOf(i));
                if (Build.VERSION.SDK_INT <= 19) {
                    actionBar.setSubtitle(actionBar.getSubtitle());
                }
            } catch (Exception e) {
                Log.w("ActionBarDrawerToggleHoneycomb", "Couldn't set content description via JB-MR2 API", e);
            }
        }
        return r3;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Drawable m2497(Activity activity) {
        TypedArray obtainStyledAttributes = activity.obtainStyledAttributes(f1409);
        Drawable drawable = obtainStyledAttributes.getDrawable(0);
        obtainStyledAttributes.recycle();
        return drawable;
    }

    /* renamed from: android.support.v7.app.ʽ$ʻ  reason: contains not printable characters */
    /* compiled from: ActionBarDrawerToggleHoneycomb */
    static class C0457 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public Method f1410;

        /* renamed from: ʼ  reason: contains not printable characters */
        public Method f1411;

        /* renamed from: ʽ  reason: contains not printable characters */
        public ImageView f1412;

        C0457(Activity activity) {
            try {
                this.f1410 = ActionBar.class.getDeclaredMethod("setHomeAsUpIndicator", Drawable.class);
                this.f1411 = ActionBar.class.getDeclaredMethod("setHomeActionContentDescription", Integer.TYPE);
            } catch (NoSuchMethodException unused) {
                View findViewById = activity.findViewById(16908332);
                if (findViewById != null) {
                    ViewGroup viewGroup = (ViewGroup) findViewById.getParent();
                    if (viewGroup.getChildCount() == 2) {
                        View childAt = viewGroup.getChildAt(0);
                        View childAt2 = childAt.getId() != 16908332 ? childAt : viewGroup.getChildAt(1);
                        if (childAt2 instanceof ImageView) {
                            this.f1412 = (ImageView) childAt2;
                        }
                    }
                }
            }
        }
    }
}
