package android.support.v7.ʾ;

import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: android.support.v7.ʾ.ʻ  reason: contains not printable characters */
/* compiled from: R */
public final class C0744 {

    /* renamed from: android.support.v7.ʾ.ʻ$ʻ  reason: contains not printable characters */
    /* compiled from: R */
    public static final class C0745 {
        public static final int compat_button_inset_horizontal_material = 2131165260;
        public static final int compat_button_inset_vertical_material = 2131165261;
        public static final int compat_button_padding_horizontal_material = 2131165262;
        public static final int compat_button_padding_vertical_material = 2131165263;
        public static final int compat_control_corner_material = 2131165264;
        public static final int fastscroll_default_thickness = 2131165305;
        public static final int fastscroll_margin = 2131165306;
        public static final int fastscroll_minimum_range = 2131165307;
        public static final int item_touch_helper_max_drag_scroll_per_frame = 2131165315;
        public static final int item_touch_helper_swipe_escape_max_velocity = 2131165316;
        public static final int item_touch_helper_swipe_escape_velocity = 2131165317;
        public static final int notification_action_icon_size = 2131165319;
        public static final int notification_action_text_size = 2131165320;
        public static final int notification_big_circle_margin = 2131165321;
        public static final int notification_content_margin_start = 2131165322;
        public static final int notification_large_icon_height = 2131165323;
        public static final int notification_large_icon_width = 2131165324;
        public static final int notification_main_column_padding_top = 2131165325;
        public static final int notification_media_narrow_margin = 2131165326;
        public static final int notification_right_icon_size = 2131165327;
        public static final int notification_right_side_padding_top = 2131165328;
        public static final int notification_small_icon_background_padding = 2131165329;
        public static final int notification_small_icon_size_as_large = 2131165330;
        public static final int notification_subtext_size = 2131165331;
        public static final int notification_top_pad = 2131165332;
        public static final int notification_top_pad_large_text = 2131165333;
    }

    /* renamed from: android.support.v7.ʾ.ʻ$ʼ  reason: contains not printable characters */
    /* compiled from: R */
    public static final class C0746 {
        public static final int[] FontFamily = {R.attr.fontProviderAuthority, R.attr.fontProviderCerts, R.attr.fontProviderFetchStrategy, R.attr.fontProviderFetchTimeout, R.attr.fontProviderPackage, R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {R.attr.font, R.attr.fontStyle, R.attr.fontWeight};
        public static final int FontFamilyFont_font = 0;
        public static final int FontFamilyFont_fontStyle = 1;
        public static final int FontFamilyFont_fontWeight = 2;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] RecyclerView = {16842948, 16842993, R.attr.fastScrollEnabled, R.attr.fastScrollHorizontalThumbDrawable, R.attr.fastScrollHorizontalTrackDrawable, R.attr.fastScrollVerticalThumbDrawable, R.attr.fastScrollVerticalTrackDrawable, R.attr.layoutManager, R.attr.reverseLayout, R.attr.spanCount, R.attr.stackFromEnd};
        public static final int RecyclerView_android_descendantFocusability = 1;
        public static final int RecyclerView_android_orientation = 0;
        public static final int RecyclerView_fastScrollEnabled = 2;
        public static final int RecyclerView_fastScrollHorizontalThumbDrawable = 3;
        public static final int RecyclerView_fastScrollHorizontalTrackDrawable = 4;
        public static final int RecyclerView_fastScrollVerticalThumbDrawable = 5;
        public static final int RecyclerView_fastScrollVerticalTrackDrawable = 6;
        public static final int RecyclerView_layoutManager = 7;
        public static final int RecyclerView_reverseLayout = 8;
        public static final int RecyclerView_spanCount = 9;
        public static final int RecyclerView_stackFromEnd = 10;
    }
}
