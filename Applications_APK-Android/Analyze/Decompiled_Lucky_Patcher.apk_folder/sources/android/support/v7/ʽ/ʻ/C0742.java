package android.support.v7.ʽ.ʻ;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.support.v4.ʼ.ʻ.C0288;

/* renamed from: android.support.v7.ʽ.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: DrawableWrapper */
public class C0742 extends Drawable implements Drawable.Callback {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Drawable f2975;

    public C0742(Drawable drawable) {
        m4890(drawable);
    }

    public void draw(Canvas canvas) {
        this.f2975.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        this.f2975.setBounds(rect);
    }

    public void setChangingConfigurations(int i) {
        this.f2975.setChangingConfigurations(i);
    }

    public int getChangingConfigurations() {
        return this.f2975.getChangingConfigurations();
    }

    public void setDither(boolean z) {
        this.f2975.setDither(z);
    }

    public void setFilterBitmap(boolean z) {
        this.f2975.setFilterBitmap(z);
    }

    public void setAlpha(int i) {
        this.f2975.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f2975.setColorFilter(colorFilter);
    }

    public boolean isStateful() {
        return this.f2975.isStateful();
    }

    public boolean setState(int[] iArr) {
        return this.f2975.setState(iArr);
    }

    public int[] getState() {
        return this.f2975.getState();
    }

    public void jumpToCurrentState() {
        C0288.m1699(this.f2975);
    }

    public Drawable getCurrent() {
        return this.f2975.getCurrent();
    }

    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.f2975.setVisible(z, z2);
    }

    public int getOpacity() {
        return this.f2975.getOpacity();
    }

    public Region getTransparentRegion() {
        return this.f2975.getTransparentRegion();
    }

    public int getIntrinsicWidth() {
        return this.f2975.getIntrinsicWidth();
    }

    public int getIntrinsicHeight() {
        return this.f2975.getIntrinsicHeight();
    }

    public int getMinimumWidth() {
        return this.f2975.getMinimumWidth();
    }

    public int getMinimumHeight() {
        return this.f2975.getMinimumHeight();
    }

    public boolean getPadding(Rect rect) {
        return this.f2975.getPadding(rect);
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        return this.f2975.setLevel(i);
    }

    public void setAutoMirrored(boolean z) {
        C0288.m1707(this.f2975, z);
    }

    public boolean isAutoMirrored() {
        return C0288.m1708(this.f2975);
    }

    public void setTint(int i) {
        C0288.m1701(this.f2975, i);
    }

    public void setTintList(ColorStateList colorStateList) {
        C0288.m1703(this.f2975, colorStateList);
    }

    public void setTintMode(PorterDuff.Mode mode) {
        C0288.m1706(this.f2975, mode);
    }

    public void setHotspot(float f, float f2) {
        C0288.m1700(this.f2975, f, f2);
    }

    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        C0288.m1702(this.f2975, i, i2, i3, i4);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Drawable m4891() {
        return this.f2975;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4890(Drawable drawable) {
        Drawable drawable2 = this.f2975;
        if (drawable2 != null) {
            drawable2.setCallback(null);
        }
        this.f2975 = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
    }
}
