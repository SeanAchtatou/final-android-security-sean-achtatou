package android.support.v7.ʽ.ʻ;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.ʼ.ʻ.C0288;
import android.support.v7.ʻ.C0727;

/* renamed from: android.support.v7.ʽ.ʻ.ʼ  reason: contains not printable characters */
/* compiled from: DrawerArrowDrawable */
public class C0743 extends Drawable {

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final float f2976 = ((float) Math.toRadians(45.0d));

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Paint f2977 = new Paint();

    /* renamed from: ʽ  reason: contains not printable characters */
    private float f2978;

    /* renamed from: ʾ  reason: contains not printable characters */
    private float f2979;

    /* renamed from: ʿ  reason: contains not printable characters */
    private float f2980;

    /* renamed from: ˆ  reason: contains not printable characters */
    private float f2981;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f2982;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final Path f2983 = new Path();

    /* renamed from: ˊ  reason: contains not printable characters */
    private final int f2984;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f2985 = false;

    /* renamed from: ˎ  reason: contains not printable characters */
    private float f2986;

    /* renamed from: ˏ  reason: contains not printable characters */
    private float f2987;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f2988 = 2;

    /* renamed from: ʻ  reason: contains not printable characters */
    private static float m4892(float f, float f2, float f3) {
        return f + ((f2 - f) * f3);
    }

    public int getOpacity() {
        return -3;
    }

    public C0743(Context context) {
        this.f2977.setStyle(Paint.Style.STROKE);
        this.f2977.setStrokeJoin(Paint.Join.MITER);
        this.f2977.setStrokeCap(Paint.Cap.BUTT);
        this.f2977.setAntiAlias(true);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(null, C0727.C0737.DrawerArrowToggle, C0727.C0728.drawerArrowStyle, C0727.C0736.Base_Widget_AppCompat_DrawerArrowToggle);
        m4894(obtainStyledAttributes.getColor(C0727.C0737.DrawerArrowToggle_color, 0));
        m4893(obtainStyledAttributes.getDimension(C0727.C0737.DrawerArrowToggle_thickness, 0.0f));
        m4895(obtainStyledAttributes.getBoolean(C0727.C0737.DrawerArrowToggle_spinBars, true));
        m4896((float) Math.round(obtainStyledAttributes.getDimension(C0727.C0737.DrawerArrowToggle_gapBetweenBars, 0.0f)));
        this.f2984 = obtainStyledAttributes.getDimensionPixelSize(C0727.C0737.DrawerArrowToggle_drawableSize, 0);
        this.f2979 = (float) Math.round(obtainStyledAttributes.getDimension(C0727.C0737.DrawerArrowToggle_barLength, 0.0f));
        this.f2978 = (float) Math.round(obtainStyledAttributes.getDimension(C0727.C0737.DrawerArrowToggle_arrowHeadLength, 0.0f));
        this.f2980 = obtainStyledAttributes.getDimension(C0727.C0737.DrawerArrowToggle_arrowShaftLength, 0.0f);
        obtainStyledAttributes.recycle();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4894(int i) {
        if (i != this.f2977.getColor()) {
            this.f2977.setColor(i);
            invalidateSelf();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4893(float f) {
        if (this.f2977.getStrokeWidth() != f) {
            this.f2977.setStrokeWidth(f);
            double d = (double) (f / 2.0f);
            double cos = Math.cos((double) f2976);
            Double.isNaN(d);
            this.f2987 = (float) (d * cos);
            invalidateSelf();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4896(float f) {
        if (f != this.f2981) {
            this.f2981 = f;
            invalidateSelf();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4895(boolean z) {
        if (this.f2982 != z) {
            this.f2982 = z;
            invalidateSelf();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m4897(boolean z) {
        if (this.f2985 != z) {
            this.f2985 = z;
            invalidateSelf();
        }
    }

    public void draw(Canvas canvas) {
        Canvas canvas2 = canvas;
        Rect bounds = getBounds();
        int i = this.f2988;
        boolean z = false;
        if (i != 0 && (i == 1 || (i == 3 ? C0288.m1714(this) == 0 : C0288.m1714(this) == 1))) {
            z = true;
        }
        float f = this.f2978;
        float r3 = m4892(this.f2979, (float) Math.sqrt((double) (f * f * 2.0f)), this.f2986);
        float r7 = m4892(this.f2979, this.f2980, this.f2986);
        float round = (float) Math.round(m4892(0.0f, this.f2987, this.f2986));
        float r9 = m4892(0.0f, f2976, this.f2986);
        double d = (double) r3;
        float r15 = m4892(z ? 0.0f : -180.0f, z ? 180.0f : 0.0f, this.f2986);
        double d2 = (double) r9;
        double cos = Math.cos(d2);
        Double.isNaN(d);
        boolean z2 = z;
        float round2 = (float) Math.round(cos * d);
        double sin = Math.sin(d2);
        Double.isNaN(d);
        float round3 = (float) Math.round(d * sin);
        this.f2983.rewind();
        float r10 = m4892(this.f2981 + this.f2977.getStrokeWidth(), -this.f2987, this.f2986);
        float f2 = (-r7) / 2.0f;
        this.f2983.moveTo(f2 + round, 0.0f);
        this.f2983.rLineTo(r7 - (round * 2.0f), 0.0f);
        this.f2983.moveTo(f2, r10);
        this.f2983.rLineTo(round2, round3);
        this.f2983.moveTo(f2, -r10);
        this.f2983.rLineTo(round2, -round3);
        this.f2983.close();
        canvas.save();
        float strokeWidth = this.f2977.getStrokeWidth();
        float height = ((float) bounds.height()) - (3.0f * strokeWidth);
        float f3 = this.f2981;
        canvas2.translate((float) bounds.centerX(), ((float) ((((int) (height - (2.0f * f3))) / 4) * 2)) + (strokeWidth * 1.5f) + f3);
        if (this.f2982) {
            canvas2.rotate(r15 * ((float) (this.f2985 ^ z2 ? -1 : 1)));
        } else if (z2) {
            canvas2.rotate(180.0f);
        }
        canvas2.drawPath(this.f2983, this.f2977);
        canvas.restore();
    }

    public void setAlpha(int i) {
        if (i != this.f2977.getAlpha()) {
            this.f2977.setAlpha(i);
            invalidateSelf();
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f2977.setColorFilter(colorFilter);
        invalidateSelf();
    }

    public int getIntrinsicHeight() {
        return this.f2984;
    }

    public int getIntrinsicWidth() {
        return this.f2984;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m4898(float f) {
        if (this.f2986 != f) {
            this.f2986 = f;
            invalidateSelf();
        }
    }
}
