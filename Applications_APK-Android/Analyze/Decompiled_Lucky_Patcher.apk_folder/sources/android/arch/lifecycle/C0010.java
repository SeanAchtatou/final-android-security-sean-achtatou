package android.arch.lifecycle;

import android.arch.lifecycle.C0003;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
/* renamed from: android.arch.lifecycle.ˆ  reason: contains not printable characters */
/* compiled from: OnLifecycleEvent */
public @interface C0010 {
    /* renamed from: ʻ  reason: contains not printable characters */
    C0003.C0004 m22();
}
