package android.arch.lifecycle;

/* renamed from: android.arch.lifecycle.ʼ  reason: contains not printable characters */
/* compiled from: Lifecycle */
public abstract class C0003 {

    /* renamed from: android.arch.lifecycle.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: Lifecycle */
    public enum C0004 {
        ON_CREATE,
        ON_START,
        ON_RESUME,
        ON_PAUSE,
        ON_STOP,
        ON_DESTROY,
        ON_ANY
    }

    /* renamed from: android.arch.lifecycle.ʼ$ʼ  reason: contains not printable characters */
    /* compiled from: Lifecycle */
    public enum C0005 {
        DESTROYED,
        INITIALIZED,
        CREATED,
        STARTED,
        RESUMED
    }
}
