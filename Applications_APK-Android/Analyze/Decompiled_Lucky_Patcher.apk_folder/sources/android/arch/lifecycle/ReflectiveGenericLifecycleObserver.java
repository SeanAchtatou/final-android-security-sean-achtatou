package android.arch.lifecycle;

import android.arch.lifecycle.C0003;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ReflectiveGenericLifecycleObserver implements C0002 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final Map<Class, C0000> f0 = new HashMap();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Object f1;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final C0000 f2 = m0(this.f1.getClass());

    ReflectiveGenericLifecycleObserver(Object obj) {
        this.f1 = obj;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m6(C0006 r2, C0003.C0004 r3) {
        m1(this.f2, r2, r3);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m3(List<C0001> list, C0006 r4, C0003.C0004 r5) {
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                m2(list.get(size), r4, r5);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m1(C0000 r2, C0006 r3, C0003.C0004 r4) {
        m3(r2.f3.get(r4), r3, r4);
        m3(r2.f3.get(C0003.C0004.ON_ANY), r3, r4);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m2(C0001 r5, C0006 r6, C0003.C0004 r7) {
        try {
            int i = r5.f5;
            if (i == 0) {
                r5.f6.invoke(this.f1, new Object[0]);
            } else if (i == 1) {
                r5.f6.invoke(this.f1, r6);
            } else if (i == 2) {
                r5.f6.invoke(this.f1, r6, r7);
            }
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Failed to call observer method", e.getCause());
        } catch (IllegalAccessException e2) {
            throw new RuntimeException(e2);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static C0000 m0(Class cls) {
        C0000 r0 = f0.get(cls);
        if (r0 != null) {
            return r0;
        }
        return m5(cls);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m4(Map<C0001, C0003.C0004> map, C0001 r4, C0003.C0004 r5, Class cls) {
        C0003.C0004 r0 = map.get(r4);
        if (r0 != null && r5 != r0) {
            Method method = r4.f6;
            throw new IllegalArgumentException("Method " + method.getName() + " in " + cls.getName() + " already declared with different @OnLifecycleEvent value: previous" + " value " + r0 + ", new value " + r5);
        } else if (r0 == null) {
            map.put(r4, r5);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static C0000 m5(Class cls) {
        int i;
        C0000 r0;
        Class superclass = cls.getSuperclass();
        HashMap hashMap = new HashMap();
        if (!(superclass == null || (r0 = m0(superclass)) == null)) {
            hashMap.putAll(r0.f4);
        }
        Method[] declaredMethods = cls.getDeclaredMethods();
        for (Class<?> r6 : cls.getInterfaces()) {
            for (Map.Entry next : m0(r6).f4.entrySet()) {
                m4(hashMap, (C0001) next.getKey(), (C0003.C0004) next.getValue(), cls);
            }
        }
        for (Method method : declaredMethods) {
            C0010 r62 = (C0010) method.getAnnotation(C0010.class);
            if (r62 != null) {
                Class<?>[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length <= 0) {
                    i = 0;
                } else if (parameterTypes[0].isAssignableFrom(C0006.class)) {
                    i = 1;
                } else {
                    throw new IllegalArgumentException("invalid parameter type. Must be one and instanceof LifecycleOwner");
                }
                C0003.C0004 r63 = r62.m22();
                if (parameterTypes.length > 1) {
                    if (!parameterTypes[1].isAssignableFrom(C0003.C0004.class)) {
                        throw new IllegalArgumentException("invalid parameter type. second arg must be an event");
                    } else if (r63 == C0003.C0004.ON_ANY) {
                        i = 2;
                    } else {
                        throw new IllegalArgumentException("Second arg is supported only for ON_ANY value");
                    }
                }
                if (parameterTypes.length <= 2) {
                    m4(hashMap, new C0001(i, method), r63, cls);
                } else {
                    throw new IllegalArgumentException("cannot have more than 2 params");
                }
            }
        }
        C0000 r02 = new C0000(hashMap);
        f0.put(cls, r02);
        return r02;
    }

    /* renamed from: android.arch.lifecycle.ReflectiveGenericLifecycleObserver$ʻ  reason: contains not printable characters */
    static class C0000 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final Map<C0003.C0004, List<C0001>> f3 = new HashMap();

        /* renamed from: ʼ  reason: contains not printable characters */
        final Map<C0001, C0003.C0004> f4;

        C0000(Map<C0001, C0003.C0004> map) {
            this.f4 = map;
            for (Map.Entry next : map.entrySet()) {
                C0003.C0004 r1 = (C0003.C0004) next.getValue();
                Object obj = this.f3.get(r1);
                if (obj == null) {
                    obj = new ArrayList();
                    this.f3.put(r1, obj);
                }
                obj.add(next.getKey());
            }
        }
    }

    /* renamed from: android.arch.lifecycle.ReflectiveGenericLifecycleObserver$ʼ  reason: contains not printable characters */
    static class C0001 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final int f5;

        /* renamed from: ʼ  reason: contains not printable characters */
        final Method f6;

        C0001(int i, Method method) {
            this.f5 = i;
            this.f6 = method;
            this.f6.setAccessible(true);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C0001 r5 = (C0001) obj;
            if (this.f5 != r5.f5 || !this.f6.getName().equals(r5.f6.getName())) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return (this.f5 * 31) + this.f6.getName().hashCode();
        }
    }
}
