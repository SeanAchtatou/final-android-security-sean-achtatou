package android.arch.lifecycle;

import android.arch.lifecycle.C0003;
import android.arch.ʻ.ʻ.C0013;
import android.arch.ʻ.ʻ.C0014;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/* renamed from: android.arch.lifecycle.ʾ  reason: contains not printable characters */
/* compiled from: LifecycleRegistry */
public class C0007 extends C0003 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private C0013<Object, C0008> f13 = new C0013<>();

    /* renamed from: ʼ  reason: contains not printable characters */
    private C0003.C0005 f14;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final C0006 f15;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f16 = 0;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f17 = false;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f18 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    private ArrayList<C0003.C0005> f19 = new ArrayList<>();

    public C0007(C0006 r2) {
        this.f15 = r2;
        this.f14 = C0003.C0005.INITIALIZED;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m19(C0003.C0005 r1) {
        this.f14 = r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m18(C0003.C0004 r2) {
        this.f14 = m10(r2);
        if (this.f17 || this.f16 != 0) {
            this.f18 = true;
            return;
        }
        this.f17 = true;
        m17();
        this.f17 = false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m9() {
        if (this.f13.m33() == 0) {
            return true;
        }
        C0003.C0005 r0 = this.f13.m36().getValue().f22;
        C0003.C0005 r2 = this.f13.m37().getValue().f22;
        if (r0 == r2 && this.f14 == r2) {
            return true;
        }
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m11() {
        ArrayList<C0003.C0005> arrayList = this.f19;
        arrayList.remove(arrayList.size() - 1);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m12(C0003.C0005 r2) {
        this.f19.add(r2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static C0003.C0005 m10(C0003.C0004 r3) {
        switch (r3) {
            case ON_CREATE:
            case ON_STOP:
                return C0003.C0005.CREATED;
            case ON_START:
            case ON_PAUSE:
                return C0003.C0005.STARTED;
            case ON_RESUME:
                return C0003.C0005.RESUMED;
            case ON_DESTROY:
                return C0003.C0005.DESTROYED;
            default:
                throw new IllegalArgumentException("Unexpected event value " + r3);
        }
    }

    /* renamed from: android.arch.lifecycle.ʾ$1  reason: invalid class name */
    /* compiled from: LifecycleRegistry */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: ʼ  reason: contains not printable characters */
        static final /* synthetic */ int[] f21 = new int[C0003.C0005.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|(2:17|18)|19|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Can't wrap try/catch for region: R(27:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|(2:17|18)|19|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Can't wrap try/catch for region: R(28:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|(2:17|18)|19|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Can't wrap try/catch for region: R(30:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0053 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x005d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0067 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0071 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x007b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0086 */
        static {
            /*
                android.arch.lifecycle.ʼ$ʼ[] r0 = android.arch.lifecycle.C0003.C0005.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                android.arch.lifecycle.C0007.AnonymousClass1.f21 = r0
                r0 = 1
                int[] r1 = android.arch.lifecycle.C0007.AnonymousClass1.f21     // Catch:{ NoSuchFieldError -> 0x0014 }
                android.arch.lifecycle.ʼ$ʼ r2 = android.arch.lifecycle.C0003.C0005.INITIALIZED     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = android.arch.lifecycle.C0007.AnonymousClass1.f21     // Catch:{ NoSuchFieldError -> 0x001f }
                android.arch.lifecycle.ʼ$ʼ r3 = android.arch.lifecycle.C0003.C0005.CREATED     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                r2 = 3
                int[] r3 = android.arch.lifecycle.C0007.AnonymousClass1.f21     // Catch:{ NoSuchFieldError -> 0x002a }
                android.arch.lifecycle.ʼ$ʼ r4 = android.arch.lifecycle.C0003.C0005.STARTED     // Catch:{ NoSuchFieldError -> 0x002a }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                r3 = 4
                int[] r4 = android.arch.lifecycle.C0007.AnonymousClass1.f21     // Catch:{ NoSuchFieldError -> 0x0035 }
                android.arch.lifecycle.ʼ$ʼ r5 = android.arch.lifecycle.C0003.C0005.RESUMED     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                r4 = 5
                int[] r5 = android.arch.lifecycle.C0007.AnonymousClass1.f21     // Catch:{ NoSuchFieldError -> 0x0040 }
                android.arch.lifecycle.ʼ$ʼ r6 = android.arch.lifecycle.C0003.C0005.DESTROYED     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r6 = r6.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r5[r6] = r4     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                android.arch.lifecycle.ʼ$ʻ[] r5 = android.arch.lifecycle.C0003.C0004.values()
                int r5 = r5.length
                int[] r5 = new int[r5]
                android.arch.lifecycle.C0007.AnonymousClass1.f20 = r5
                int[] r5 = android.arch.lifecycle.C0007.AnonymousClass1.f20     // Catch:{ NoSuchFieldError -> 0x0053 }
                android.arch.lifecycle.ʼ$ʻ r6 = android.arch.lifecycle.C0003.C0004.ON_CREATE     // Catch:{ NoSuchFieldError -> 0x0053 }
                int r6 = r6.ordinal()     // Catch:{ NoSuchFieldError -> 0x0053 }
                r5[r6] = r0     // Catch:{ NoSuchFieldError -> 0x0053 }
            L_0x0053:
                int[] r0 = android.arch.lifecycle.C0007.AnonymousClass1.f20     // Catch:{ NoSuchFieldError -> 0x005d }
                android.arch.lifecycle.ʼ$ʻ r5 = android.arch.lifecycle.C0003.C0004.ON_STOP     // Catch:{ NoSuchFieldError -> 0x005d }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x005d }
                r0[r5] = r1     // Catch:{ NoSuchFieldError -> 0x005d }
            L_0x005d:
                int[] r0 = android.arch.lifecycle.C0007.AnonymousClass1.f20     // Catch:{ NoSuchFieldError -> 0x0067 }
                android.arch.lifecycle.ʼ$ʻ r1 = android.arch.lifecycle.C0003.C0004.ON_START     // Catch:{ NoSuchFieldError -> 0x0067 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0067 }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0067 }
            L_0x0067:
                int[] r0 = android.arch.lifecycle.C0007.AnonymousClass1.f20     // Catch:{ NoSuchFieldError -> 0x0071 }
                android.arch.lifecycle.ʼ$ʻ r1 = android.arch.lifecycle.C0003.C0004.ON_PAUSE     // Catch:{ NoSuchFieldError -> 0x0071 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0071 }
                r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0071 }
            L_0x0071:
                int[] r0 = android.arch.lifecycle.C0007.AnonymousClass1.f20     // Catch:{ NoSuchFieldError -> 0x007b }
                android.arch.lifecycle.ʼ$ʻ r1 = android.arch.lifecycle.C0003.C0004.ON_RESUME     // Catch:{ NoSuchFieldError -> 0x007b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007b }
                r0[r1] = r4     // Catch:{ NoSuchFieldError -> 0x007b }
            L_0x007b:
                int[] r0 = android.arch.lifecycle.C0007.AnonymousClass1.f20     // Catch:{ NoSuchFieldError -> 0x0086 }
                android.arch.lifecycle.ʼ$ʻ r1 = android.arch.lifecycle.C0003.C0004.ON_DESTROY     // Catch:{ NoSuchFieldError -> 0x0086 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0086 }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0086 }
            L_0x0086:
                int[] r0 = android.arch.lifecycle.C0007.AnonymousClass1.f20     // Catch:{ NoSuchFieldError -> 0x0091 }
                android.arch.lifecycle.ʼ$ʻ r1 = android.arch.lifecycle.C0003.C0004.ON_ANY     // Catch:{ NoSuchFieldError -> 0x0091 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0091 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0091 }
            L_0x0091:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.arch.lifecycle.C0007.AnonymousClass1.<clinit>():void");
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static C0003.C0004 m13(C0003.C0005 r3) {
        int i = AnonymousClass1.f21[r3.ordinal()];
        if (i == 1) {
            throw new IllegalArgumentException();
        } else if (i == 2) {
            return C0003.C0004.ON_DESTROY;
        } else {
            if (i == 3) {
                return C0003.C0004.ON_STOP;
            }
            if (i == 4) {
                return C0003.C0004.ON_PAUSE;
            }
            if (i != 5) {
                throw new IllegalArgumentException("Unexpected state value " + r3);
            }
            throw new IllegalArgumentException();
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private static C0003.C0004 m15(C0003.C0005 r3) {
        int i = AnonymousClass1.f21[r3.ordinal()];
        if (i != 1) {
            if (i == 2) {
                return C0003.C0004.ON_START;
            }
            if (i == 3) {
                return C0003.C0004.ON_RESUME;
            }
            if (i == 4) {
                throw new IllegalArgumentException();
            } else if (i != 5) {
                throw new IllegalArgumentException("Unexpected state value " + r3);
            }
        }
        return C0003.C0004.ON_CREATE;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m14() {
        C0014<K, V>.ʾ r0 = this.f13.m35();
        while (r0.hasNext() && !this.f18) {
            Map.Entry entry = (Map.Entry) r0.next();
            C0008 r2 = (C0008) entry.getValue();
            while (r2.f22.compareTo((Enum) this.f14) < 0 && !this.f18 && this.f13.m31(entry.getKey())) {
                m12(r2.f22);
                r2.m20(this.f15, m15(r2.f22));
                m11();
            }
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m16() {
        Iterator<Map.Entry<Object, C0008>> r0 = this.f13.m34();
        while (r0.hasNext() && !this.f18) {
            Map.Entry next = r0.next();
            C0008 r2 = (C0008) next.getValue();
            while (r2.f22.compareTo((Enum) this.f14) > 0 && !this.f18 && this.f13.m31(next.getKey())) {
                C0003.C0004 r3 = m13(r2.f22);
                m12(m10(r3));
                r2.m20(this.f15, r3);
                m11();
            }
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m17() {
        while (!m9()) {
            this.f18 = false;
            if (this.f14.compareTo((Enum) this.f13.m36().getValue().f22) < 0) {
                m16();
            }
            Map.Entry<Object, C0008> r0 = this.f13.m37();
            if (!this.f18 && r0 != null && this.f14.compareTo((Enum) r0.getValue().f22) > 0) {
                m14();
            }
        }
        this.f18 = false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static C0003.C0005 m8(C0003.C0005 r1, C0003.C0005 r2) {
        return (r2 == null || r2.compareTo(r1) >= 0) ? r1 : r2;
    }

    /* renamed from: android.arch.lifecycle.ʾ$ʻ  reason: contains not printable characters */
    /* compiled from: LifecycleRegistry */
    static class C0008 {

        /* renamed from: ʻ  reason: contains not printable characters */
        C0003.C0005 f22;

        /* renamed from: ʼ  reason: contains not printable characters */
        C0002 f23;

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m20(C0006 r3, C0003.C0004 r4) {
            C0003.C0005 r0 = C0007.m10(r4);
            this.f22 = C0007.m8(this.f22, r0);
            this.f23.m7(r3, r4);
            this.f22 = r0;
        }
    }
}
