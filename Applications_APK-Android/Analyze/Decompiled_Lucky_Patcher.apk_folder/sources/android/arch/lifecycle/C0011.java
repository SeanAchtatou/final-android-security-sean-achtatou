package android.arch.lifecycle;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.arch.lifecycle.C0003;
import android.os.Bundle;

/* renamed from: android.arch.lifecycle.ˈ  reason: contains not printable characters */
/* compiled from: ReportFragment */
public class C0011 extends Fragment {

    /* renamed from: ʻ  reason: contains not printable characters */
    private C0012 f24;

    /* renamed from: android.arch.lifecycle.ˈ$ʻ  reason: contains not printable characters */
    /* compiled from: ReportFragment */
    interface C0012 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m28();

        /* renamed from: ʼ  reason: contains not printable characters */
        void m29();

        /* renamed from: ʽ  reason: contains not printable characters */
        void m30();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m23(Activity activity) {
        FragmentManager fragmentManager = activity.getFragmentManager();
        if (fragmentManager.findFragmentByTag("android.arch.lifecycle.LifecycleDispatcher.report_fragment_tag") == null) {
            fragmentManager.beginTransaction().add(new C0011(), "android.arch.lifecycle.LifecycleDispatcher.report_fragment_tag").commit();
            fragmentManager.executePendingTransactions();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m25(C0012 r1) {
        if (r1 != null) {
            r1.m28();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m26(C0012 r1) {
        if (r1 != null) {
            r1.m29();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m27(C0012 r1) {
        if (r1 != null) {
            r1.m30();
        }
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        m25(this.f24);
        m24(C0003.C0004.ON_CREATE);
    }

    public void onStart() {
        super.onStart();
        m26(this.f24);
        m24(C0003.C0004.ON_START);
    }

    public void onResume() {
        super.onResume();
        m27(this.f24);
        m24(C0003.C0004.ON_RESUME);
    }

    public void onPause() {
        super.onPause();
        m24(C0003.C0004.ON_PAUSE);
    }

    public void onStop() {
        super.onStop();
        m24(C0003.C0004.ON_STOP);
    }

    public void onDestroy() {
        super.onDestroy();
        m24(C0003.C0004.ON_DESTROY);
        this.f24 = null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m24(C0003.C0004 r3) {
        Activity activity = getActivity();
        if (activity instanceof C0009) {
            ((C0009) activity).m21().m18(r3);
        } else if (activity instanceof C0006) {
            C0003 lifecycle = ((C0006) activity).getLifecycle();
            if (lifecycle instanceof C0007) {
                ((C0007) lifecycle).m18(r3);
            }
        }
    }
}
