package android.arch.ʻ.ʻ;

import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: android.arch.ʻ.ʻ.ʼ  reason: contains not printable characters */
/* compiled from: SafeIterableMap */
public class C0014<K, V> implements Iterable<Map.Entry<K, V>> {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0017<K, V> f26;

    /* renamed from: ʼ  reason: contains not printable characters */
    private C0017<K, V> f27;

    /* renamed from: ʽ  reason: contains not printable characters */
    private WeakHashMap<Object<K, V>, Boolean> f28 = new WeakHashMap<>();

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f29 = 0;

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m33() {
        return this.f29;
    }

    public Iterator<Map.Entry<K, V>> iterator() {
        C0015 r0 = new C0015(this.f26, this.f27);
        this.f28.put(r0, false);
        return r0;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Iterator<Map.Entry<K, V>> m34() {
        C0016 r0 = new C0016(this.f27, this.f26);
        this.f28.put(r0, false);
        return r0;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public C0014<K, V>.ʾ m35() {
        C0014<K, V>.ʾ r0 = new C0018();
        this.f28.put(r0, false);
        return r0;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public Map.Entry<K, V> m36() {
        return this.f26;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public Map.Entry<K, V> m37() {
        return this.f27;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C0014)) {
            return false;
        }
        C0014 r6 = (C0014) obj;
        if (m33() != r6.m33()) {
            return false;
        }
        Iterator it = iterator();
        Iterator it2 = r6.iterator();
        while (it.hasNext() && it2.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            Object next = it2.next();
            if ((entry == null && next != null) || (entry != null && !entry.equals(next))) {
                return false;
            }
        }
        if (it.hasNext() || it2.hasNext()) {
            return false;
        }
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Iterator it = iterator();
        while (it.hasNext()) {
            sb.append(((Map.Entry) it.next()).toString());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    /* renamed from: android.arch.ʻ.ʻ.ʼ$ʿ  reason: contains not printable characters */
    /* compiled from: SafeIterableMap */
    private static abstract class C0019<K, V> implements Iterator<Map.Entry<K, V>> {

        /* renamed from: ʻ  reason: contains not printable characters */
        C0017<K, V> f37;

        /* renamed from: ʼ  reason: contains not printable characters */
        C0017<K, V> f38;

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract C0017<K, V> m42(C0017<K, V> r1);

        C0019(C0017<K, V> r1, C0017<K, V> r2) {
            this.f37 = r2;
            this.f38 = r1;
        }

        public boolean hasNext() {
            return this.f38 != null;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private C0017<K, V> m41() {
            C0017<K, V> r0 = this.f38;
            C0017<K, V> r1 = this.f37;
            if (r0 == r1 || r1 == null) {
                return null;
            }
            return m42(r0);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Map.Entry<K, V> next() {
            C0017<K, V> r0 = this.f38;
            this.f38 = m41();
            return r0;
        }
    }

    /* renamed from: android.arch.ʻ.ʻ.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: SafeIterableMap */
    static class C0015<K, V> extends C0019<K, V> {
        C0015(C0017<K, V> r1, C0017<K, V> r2) {
            super(r1, r2);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public C0017<K, V> m38(C0017<K, V> r1) {
            return r1.f32;
        }
    }

    /* renamed from: android.arch.ʻ.ʻ.ʼ$ʼ  reason: contains not printable characters */
    /* compiled from: SafeIterableMap */
    private static class C0016<K, V> extends C0019<K, V> {
        C0016(C0017<K, V> r1, C0017<K, V> r2) {
            super(r1, r2);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public C0017<K, V> m39(C0017<K, V> r1) {
            return r1.f33;
        }
    }

    /* renamed from: android.arch.ʻ.ʻ.ʼ$ʾ  reason: contains not printable characters */
    /* compiled from: SafeIterableMap */
    private class C0018 implements Iterator<Map.Entry<K, V>> {

        /* renamed from: ʼ  reason: contains not printable characters */
        private C0017<K, V> f35;

        /* renamed from: ʽ  reason: contains not printable characters */
        private boolean f36;

        private C0018() {
            this.f36 = true;
        }

        public boolean hasNext() {
            if (!this.f36) {
                C0017<K, V> r0 = this.f35;
                if (r0 == null || r0.f32 == null) {
                    return false;
                }
                return true;
            } else if (C0014.this.f26 != null) {
                return true;
            } else {
                return false;
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Map.Entry<K, V> next() {
            if (this.f36) {
                this.f36 = false;
                this.f35 = C0014.this.f26;
            } else {
                C0017<K, V> r0 = this.f35;
                this.f35 = r0 != null ? r0.f32 : null;
            }
            return this.f35;
        }
    }

    /* renamed from: android.arch.ʻ.ʻ.ʼ$ʽ  reason: contains not printable characters */
    /* compiled from: SafeIterableMap */
    static class C0017<K, V> implements Map.Entry<K, V> {

        /* renamed from: ʻ  reason: contains not printable characters */
        final K f30;

        /* renamed from: ʼ  reason: contains not printable characters */
        final V f31;

        /* renamed from: ʽ  reason: contains not printable characters */
        C0017<K, V> f32;

        /* renamed from: ʾ  reason: contains not printable characters */
        C0017<K, V> f33;

        public K getKey() {
            return this.f30;
        }

        public V getValue() {
            return this.f31;
        }

        public V setValue(V v) {
            throw new UnsupportedOperationException("An entry modification is not supported");
        }

        public String toString() {
            return ((Object) this.f30) + "=" + ((Object) this.f31);
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean equals(java.lang.Object r5) {
            /*
                r4 = this;
                r0 = 1
                if (r5 != r4) goto L_0x0004
                return r0
            L_0x0004:
                boolean r1 = r5 instanceof android.arch.ʻ.ʻ.C0014.C0017
                r2 = 0
                if (r1 != 0) goto L_0x000a
                return r2
            L_0x000a:
                android.arch.ʻ.ʻ.ʼ$ʽ r5 = (android.arch.ʻ.ʻ.C0014.C0017) r5
                K r1 = r4.f30
                K r3 = r5.f30
                boolean r1 = r1.equals(r3)
                if (r1 == 0) goto L_0x0021
                V r1 = r4.f31
                V r5 = r5.f31
                boolean r5 = r1.equals(r5)
                if (r5 == 0) goto L_0x0021
                goto L_0x0022
            L_0x0021:
                r0 = 0
            L_0x0022:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: android.arch.ʻ.ʻ.C0014.C0017.equals(java.lang.Object):boolean");
        }
    }
}
