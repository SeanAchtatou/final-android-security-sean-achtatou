package com.xposed;

import android.app.AndroidAppHelper;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Binder;
import android.os.Build;
import com.chelpus.C0776;
import com.chelpus.C0815;
import com.google.android.finsky.billing.iab.InAppBillingService;
import com.google.android.finsky.billing.iab.ʻ.ʻ.C0822;
import com.google.android.finsky.services.LicensingService;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;
import java.security.Signature;
import java.util.List;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.PkgName;

public class XSupport implements IXposedHookLoadPackage, IXposedHookZygoteInit {

    /* renamed from: ʿ  reason: contains not printable characters */
    public static boolean f5184 = true;

    /* renamed from: ˆ  reason: contains not printable characters */
    public static boolean f5185 = true;

    /* renamed from: ˈ  reason: contains not printable characters */
    public static boolean f5186 = false;

    /* renamed from: ˉ  reason: contains not printable characters */
    public static boolean f5187 = false;

    /* renamed from: ˊ  reason: contains not printable characters */
    public static boolean f5188 = false;

    /* renamed from: ˋ  reason: contains not printable characters */
    public static boolean f5189 = true;

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean f5190;

    /* renamed from: ʼ  reason: contains not printable characters */
    public String f5191 = BuildConfig.FLAVOR;

    /* renamed from: ʽ  reason: contains not printable characters */
    public String f5192 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    public long f5193 = 0;

    /* renamed from: ˎ  reason: contains not printable characters */
    Context f5194 = null;

    /* renamed from: ˏ  reason: contains not printable characters */
    Boolean f5195 = null;

    /* renamed from: ˑ  reason: contains not printable characters */
    Context f5196 = null;

    /* renamed from: י  reason: contains not printable characters */
    Context f5197 = null;

    /* renamed from: ـ  reason: contains not printable characters */
    Boolean f5198 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    boolean f5199 = false;

    /* renamed from: ᐧ  reason: contains not printable characters */
    boolean f5200 = false;

    /* renamed from: ᴵ  reason: contains not printable characters */
    boolean f5201 = false;

    /* renamed from: ᵎ  reason: contains not printable characters */
    boolean f5202 = false;

    public void initZygote(IXposedHookZygoteInit.StartupParam startupParam) {
        this.f5190 = true;
        if (Build.VERSION.SDK_INT > 18) {
            try {
                XposedBridge.hookAllMethods(XposedHelpers.findClass("com.android.org.conscrypt.OpenSSLSignature", (ClassLoader) null), "engineVerify", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        System.out.println("engineVerify");
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5184) {
                            methodHookParam.setResult(true);
                        }
                    }
                });
            } catch (Exception unused) {
            }
        }
        if (Build.VERSION.SDK_INT > 14 && Build.VERSION.SDK_INT < 19) {
            try {
                XposedBridge.hookAllMethods(XposedHelpers.findClass("org.apache.harmony.xnet.provider.jsse.OpenSSLSignature", (ClassLoader) null), "engineVerify", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        System.out.println("engineVerify");
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5184) {
                            methodHookParam.setResult(true);
                        }
                    }
                });
            } catch (Exception unused2) {
            }
        }
        if (Build.VERSION.SDK_INT == 10) {
            try {
                XposedBridge.hookAllMethods(XposedHelpers.findClass("org.bouncycastle.jce.provider.JDKDigestSignature", (ClassLoader) null), "engineVerify", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        System.out.println("engineVerify");
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5184) {
                            methodHookParam.setResult(true);
                        }
                    }
                });
            } catch (ClassNotFoundException unused3) {
            }
        }
        XposedHelpers.findAndHookMethod("java.security.MessageDigest", (ClassLoader) null, "isEqual", new Object[]{byte[].class, byte[].class, new XC_MethodHook() {
            /* access modifiers changed from: protected */
            public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                XSupport.this.m6251();
                if (XSupport.f5189 && XSupport.f5185) {
                    methodHookParam.setResult(true);
                }
            }
        }});
        XposedHelpers.findAndHookMethod("java.security.Signature", (ClassLoader) null, "verify", new Object[]{byte[].class, new XC_MethodHook() {
            /* access modifiers changed from: protected */
            public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                XSupport.this.m6251();
                if (XSupport.f5189 && XSupport.f5184) {
                    if ((((Signature) methodHookParam.thisObject).getAlgorithm().toLowerCase().equals("sha256withdsa") || ((Signature) methodHookParam.thisObject).getAlgorithm().toLowerCase().equals("rsa-sha1") || ((Signature) methodHookParam.thisObject).getAlgorithm().toLowerCase().equals("sha1withrsa")) && Integer.valueOf(XposedHelpers.getIntField(methodHookParam.thisObject, "state")).intValue() == 3) {
                        methodHookParam.setResult(true);
                    }
                }
            }
        }});
        XposedHelpers.findAndHookMethod("java.security.Signature", (ClassLoader) null, "verify", new Object[]{byte[].class, Integer.TYPE, Integer.TYPE, new XC_MethodHook() {
            /* access modifiers changed from: protected */
            public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                XSupport.this.m6251();
                if (XSupport.f5189 && XSupport.f5184) {
                    if ((((Signature) methodHookParam.thisObject).getAlgorithm().toLowerCase().equals("sha1withrsa") || ((Signature) methodHookParam.thisObject).getAlgorithm().toLowerCase().equals("rsa-sha1") || ((Signature) methodHookParam.thisObject).getAlgorithm().toLowerCase().equals("sha256withdsa")) && Integer.valueOf(XposedHelpers.getIntField(methodHookParam.thisObject, "state")).intValue() == 3) {
                        methodHookParam.setResult(true);
                    }
                }
            }
        }});
    }

    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam loadPackageParam) {
        Class cls;
        if (PkgName.getPkgName().equals(loadPackageParam.packageName)) {
            XposedHelpers.findAndHookMethod(XposedUtils.class.getName(), loadPackageParam.classLoader, "isXposedEnabled", new Object[]{new XC_MethodHook() {
                /* access modifiers changed from: protected */
                public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                    methodHookParam.setResult(true);
                }
            }});
        }
        AnonymousClass40 r0 = new XC_MethodHook() {
            /* access modifiers changed from: protected */
            public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                XSupport.this.m6251();
                if (XSupport.f5189 && XSupport.f5187) {
                    Intent intent = (Intent) methodHookParam.args[0];
                    if (intent != null && (((intent.getAction() != null && C0815.m5241(intent.getAction())) || (intent.getComponent() != null && intent.getComponent().toString().contains("com.android.vending") && intent.getComponent().toString().toLowerCase().contains("inappbillingservice"))) && XSupport.this.m6253(methodHookParam, intent, 0, 0))) {
                        ComponentName componentName = new ComponentName(PkgName.getPkgName(), InAppBillingService.class.getName());
                        Intent intent2 = new Intent();
                        intent2.setComponent(componentName);
                        methodHookParam.args[0] = intent2;
                    }
                    if (intent != null) {
                        if (((intent.getAction() != null && intent.getAction().toLowerCase().equals("com.android.vending.licensing.ilicensingservice")) || (intent.getComponent() != null && intent.getComponent().toString().contains("com.android.vending") && intent.getComponent().toString().toLowerCase().contains("licensingservice"))) && XSupport.this.m6253(methodHookParam, intent, 1, 0)) {
                            ComponentName componentName2 = new ComponentName(PkgName.getPkgName(), LicensingService.class.getName());
                            Intent intent3 = new Intent();
                            intent3.setComponent(componentName2);
                            methodHookParam.args[0] = intent3;
                        }
                        if (intent.getAction() != null && intent.getAction().equals("com.lp.ITestServiceInterface.BIND")) {
                            Intent intent4 = new Intent("com.lp.ITestServiceInterface.BIND");
                            intent4.setPackage(PkgName.getPkgName());
                            methodHookParam.args[0] = intent4;
                        }
                    }
                }
            }
        };
        AnonymousClass41 r1 = new XC_MethodHook() {
            /* access modifiers changed from: protected */
            public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                XSupport.this.m6251();
                if (XSupport.f5189 && XSupport.f5187) {
                    Intent intent = (Intent) methodHookParam.thisObject;
                    if (intent != null && (((intent.getAction() != null && C0815.m5241(intent.getAction())) || (intent.getComponent() != null && intent.getComponent().toString().contains("com.android.vending") && intent.getComponent().toString().toLowerCase().contains("inappbillingservice"))) && XSupport.this.m6253(methodHookParam, intent, 0, 3))) {
                        ((Intent) methodHookParam.thisObject).setComponent(new ComponentName(PkgName.getPkgName(), InAppBillingService.class.getName()));
                        methodHookParam.args[0] = PkgName.getPkgName();
                    }
                    if (intent != null) {
                        if (((intent.getAction() != null && intent.getAction().toLowerCase().equals("com.android.vending.licensing.ilicensingservice")) || (intent.getComponent() != null && intent.getComponent().toString().contains("com.android.vending") && intent.getComponent().toString().toLowerCase().contains("licensingservice"))) && XSupport.this.m6253(methodHookParam, intent, 1, 3)) {
                            ((Intent) methodHookParam.thisObject).setComponent(new ComponentName(PkgName.getPkgName(), LicensingService.class.getName()));
                            methodHookParam.args[0] = PkgName.getPkgName();
                        }
                        if (intent.getAction() != null && intent.getAction().equals("com.lp.ITestServiceInterface.BIND")) {
                            methodHookParam.args[0] = PkgName.getPkgName();
                        }
                    }
                }
            }
        };
        Class cls2 = null;
        XposedBridge.hookAllMethods(XposedHelpers.findClass("android.content.ContextWrapper", (ClassLoader) null), "bindService", r0);
        XposedBridge.hookAllMethods(XposedHelpers.findClass("android.content.ContextWrapper", loadPackageParam.classLoader), "bindService", r0);
        XposedBridge.hookAllMethods(XposedHelpers.findClass("android.content.Intent", loadPackageParam.classLoader), "setPackage", r1);
        XposedBridge.hookAllMethods(XposedHelpers.findClass("android.content.Intent", (ClassLoader) null), "setPackage", r1);
        if ("android".equals(loadPackageParam.packageName) && loadPackageParam.processName.equals("android")) {
            if (Build.VERSION.SDK_INT > 10) {
                if (Build.VERSION.SDK_INT >= 28) {
                    cls2 = XposedHelpers.findClass("android.content.pm.PackageParser$SigningDetails", loadPackageParam.classLoader);
                    cls = XposedHelpers.findClass("com.android.server.pm.PackageManagerServiceUtils", loadPackageParam.classLoader);
                } else {
                    cls = null;
                }
                Class findClass = XposedHelpers.findClass("com.android.server.pm.PackageManagerService", loadPackageParam.classLoader);
                XposedBridge.hookAllConstructors(XposedHelpers.findClass("com.android.server.pm.PackageManagerService", loadPackageParam.classLoader), new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void afterHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        System.out.println("SetPmContext");
                        XSupport.this.f5197 = (Context) methodHookParam.args[0];
                    }
                });
                if (C0776.f3099) {
                    XposedBridge.hookAllMethods(findClass, "installPackageAsUser", new XC_MethodHook() {
                        /* access modifiers changed from: protected */
                        public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                            XSupport.this.m6251();
                            if (XSupport.f5189 && XSupport.f5186) {
                                int intValue = ((Integer) methodHookParam.args[2]).intValue();
                                if ((intValue & 128) == 0) {
                                    methodHookParam.args[2] = Integer.valueOf(intValue | 128);
                                }
                            }
                        }
                    });
                } else if (C0776.f3096) {
                    XposedBridge.hookAllMethods(findClass, "installPackageWithVerificationAndEncryption", new XC_MethodHook() {
                        /* access modifiers changed from: protected */
                        public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                            XSupport.this.m6251();
                            if (XSupport.f5189 && XSupport.f5186) {
                                char c = C0776.f3096 ? (char) 2 : 1;
                                int intValue = ((Integer) methodHookParam.args[c]).intValue();
                                if ((intValue & 128) == 0) {
                                    methodHookParam.args[c] = Integer.valueOf(intValue | 128);
                                }
                            }
                        }
                    });
                } else {
                    XposedBridge.hookAllMethods(findClass, "installPackageWithVerification", new XC_MethodHook() {
                        /* access modifiers changed from: protected */
                        public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                            int i;
                            XSupport.this.m6251();
                            if (XSupport.f5189 && XSupport.f5186) {
                                char c = Build.VERSION.SDK_INT >= 14 ? (char) 2 : 1;
                                try {
                                    i = ((Integer) methodHookParam.args[c]).intValue();
                                } catch (ClassCastException unused) {
                                    i = ((Integer) methodHookParam.args[1]).intValue();
                                    c = 1;
                                }
                                if ((i & 128) == 0) {
                                    methodHookParam.args[c] = Integer.valueOf(i | 128);
                                }
                            }
                        }
                    });
                }
                XposedBridge.hookAllMethods(findClass, "scanPackageLI", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.f5190 = false;
                    }

                    /* access modifiers changed from: protected */
                    public void afterHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.f5190 = true;
                    }
                });
                XposedBridge.hookAllMethods(findClass, "verifySignaturesLP", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5186) {
                            methodHookParam.setResult(true);
                        }
                    }
                });
                if (Build.VERSION.SDK_INT >= 28) {
                    XposedBridge.hookAllMethods(cls2, "checkCapability", new XC_MethodHook() {
                        /* access modifiers changed from: protected */
                        public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                            XSupport.this.m6251();
                            if (XSupport.f5189 && XSupport.f5186 && XSupport.this.f5190) {
                                methodHookParam.setResult(true);
                            }
                        }
                    });
                }
                if (cls != null) {
                    XposedBridge.hookAllMethods(cls, "compareSignatures", new XC_MethodHook() {
                        /* access modifiers changed from: protected */
                        public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                            boolean z;
                            XSupport.this.m6251();
                            if (XSupport.f5189 && XSupport.f5186 && XSupport.this.f5190) {
                                if (XSupport.this.f5191.equals(BuildConfig.FLAVOR)) {
                                    try {
                                        PackageInfo packageInfo = XSupport.this.f5197.getPackageManager().getPackageInfo("android", 64);
                                        if (packageInfo.signatures[0] != null) {
                                            XSupport.this.f5191 = C0822.m5385(packageInfo.signatures[0].toByteArray()).replaceAll("\n", BuildConfig.FLAVOR);
                                        } else {
                                            return;
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        return;
                                    }
                                }
                                android.content.pm.Signature[] signatureArr = (android.content.pm.Signature[]) methodHookParam.args[0];
                                android.content.pm.Signature[] signatureArr2 = (android.content.pm.Signature[]) methodHookParam.args[1];
                                if (signatureArr == null || signatureArr.length <= 0) {
                                    z = false;
                                } else {
                                    z = false;
                                    for (android.content.pm.Signature byteArray : signatureArr) {
                                        if (C0822.m5385(byteArray.toByteArray()).replaceAll("\n", BuildConfig.FLAVOR).equals(XSupport.this.f5191)) {
                                            z = true;
                                        }
                                    }
                                }
                                if (signatureArr2 != null && signatureArr2.length > 0) {
                                    for (android.content.pm.Signature byteArray2 : signatureArr2) {
                                        if (C0822.m5385(byteArray2.toByteArray()).replaceAll("\n", BuildConfig.FLAVOR).equals(XSupport.this.f5191)) {
                                            z = true;
                                        }
                                    }
                                }
                                if (!z) {
                                    methodHookParam.setResult(0);
                                }
                            }
                        }
                    });
                }
                XposedBridge.hookAllMethods(findClass, "compareSignatures", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        boolean z;
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5186 && XSupport.this.f5190) {
                            if (XSupport.this.f5191.equals(BuildConfig.FLAVOR)) {
                                try {
                                    PackageInfo packageInfo = XSupport.this.f5197.getPackageManager().getPackageInfo("android", 64);
                                    if (packageInfo.signatures[0] != null) {
                                        XSupport.this.f5191 = C0822.m5385(packageInfo.signatures[0].toByteArray()).replaceAll("\n", BuildConfig.FLAVOR);
                                    } else {
                                        return;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    return;
                                }
                            }
                            android.content.pm.Signature[] signatureArr = (android.content.pm.Signature[]) methodHookParam.args[0];
                            android.content.pm.Signature[] signatureArr2 = (android.content.pm.Signature[]) methodHookParam.args[1];
                            if (signatureArr == null || signatureArr.length <= 0) {
                                z = false;
                            } else {
                                z = false;
                                for (android.content.pm.Signature byteArray : signatureArr) {
                                    if (C0822.m5385(byteArray.toByteArray()).replaceAll("\n", BuildConfig.FLAVOR).equals(XSupport.this.f5191)) {
                                        z = true;
                                    }
                                }
                            }
                            if (signatureArr2 != null && signatureArr2.length > 0) {
                                for (android.content.pm.Signature byteArray2 : signatureArr2) {
                                    if (C0822.m5385(byteArray2.toByteArray()).replaceAll("\n", BuildConfig.FLAVOR).equals(XSupport.this.f5191)) {
                                        z = true;
                                    }
                                }
                            }
                            if (!z) {
                                methodHookParam.setResult(0);
                            }
                        }
                    }
                });
                try {
                    XposedBridge.hookAllMethods(findClass, "installStage", new XC_MethodHook() {
                        /* access modifiers changed from: protected */
                        public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                            System.out.println("setSkipDowngrade2");
                            XSupport.this.m6251();
                            if (XSupport.f5189 && XSupport.f5186) {
                                int intValue = ((Integer) XposedHelpers.getObjectField(methodHookParam.args[4], "installFlags")).intValue();
                                if ((intValue & 128) == 0) {
                                    intValue |= 128;
                                }
                                Object obj = methodHookParam.args[4];
                                XposedHelpers.setIntField(obj, "installFlags", intValue);
                                methodHookParam.args[4] = obj;
                            }
                        }
                    });
                } catch (Throwable th) {
                    th.printStackTrace();
                }
                try {
                    XposedBridge.hookAllMethods(findClass, "checkDowngrade", new XC_MethodHook() {
                        /* access modifiers changed from: protected */
                        public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                            System.out.println("checkDowngrade");
                            XSupport.this.m6251();
                            if (XSupport.f5189 && XSupport.f5186) {
                                System.out.println("apply skip checkDowngrade.");
                                methodHookParam.setResult((Object) null);
                            }
                        }
                    });
                } catch (Throwable th2) {
                    th2.printStackTrace();
                }
                XposedBridge.hookAllMethods(findClass, "getPackageInfo", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        String str;
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5188 && (str = (String) methodHookParam.args[0]) != null && str.equals(PkgName.getPkgName()) && XSupport.this.m6252(methodHookParam)) {
                            methodHookParam.setResult((Object) null);
                        }
                    }
                });
                XposedBridge.hookAllMethods(findClass, "getApplicationInfo", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        String str;
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5188 && (str = (String) methodHookParam.args[0]) != null && str.equals(PkgName.getPkgName()) && XSupport.this.m6254(methodHookParam)) {
                            methodHookParam.setResult((Object) null);
                        }
                    }
                });
                XposedBridge.hookAllMethods(findClass, "generatePackageInfo", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        if (XSupport.this.f5199 || XSupport.this.f5200 || XSupport.this.f5201) {
                            try {
                                if (((String) XposedHelpers.getObjectField(methodHookParam.args[0], "packageName")).equals(PkgName.getPkgName())) {
                                    methodHookParam.setResult((Object) null);
                                }
                            } catch (NoSuchFieldError unused) {
                                if (((String) XposedHelpers.getObjectField(XposedHelpers.getObjectField(methodHookParam.args[0], "pkg"), "packageName")).equals(PkgName.getPkgName())) {
                                    methodHookParam.setResult((Object) null);
                                }
                            }
                        }
                    }
                });
                XposedBridge.hookAllMethods(findClass, "getInstalledApplications", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5188 && XSupport.this.m6254(methodHookParam)) {
                            XSupport.this.f5200 = true;
                        }
                    }

                    /* access modifiers changed from: protected */
                    public void afterHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.f5200 = false;
                    }
                });
                XposedBridge.hookAllMethods(findClass, "getInstalledPackages", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5188 && XSupport.this.m6254(methodHookParam)) {
                            XSupport.this.f5199 = true;
                        }
                    }

                    /* access modifiers changed from: protected */
                    public void afterHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.f5199 = false;
                    }
                });
                XposedBridge.hookAllMethods(findClass, "getPreferredPackages", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5188 && XSupport.this.m6254(methodHookParam)) {
                            XSupport.this.f5201 = true;
                        }
                    }

                    /* access modifiers changed from: protected */
                    public void afterHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.f5201 = false;
                    }
                });
            } else {
                XposedBridge.hookAllConstructors(XposedHelpers.findClass("com.android.server.PackageManagerService", loadPackageParam.classLoader), new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void afterHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        System.out.println("SetPmContext");
                        XSupport.this.f5197 = (Context) methodHookParam.args[0];
                    }
                });
                XposedBridge.hookAllMethods(XposedHelpers.findClass("com.android.server.PackageManagerService", loadPackageParam.classLoader), "getPackageInfo", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        String str;
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5188 && (str = (String) methodHookParam.args[0]) != null && str.equals(PkgName.getPkgName()) && XSupport.this.m6252(methodHookParam)) {
                            methodHookParam.setResult((Object) null);
                        }
                    }
                });
                XposedBridge.hookAllMethods(XposedHelpers.findClass("com.android.server.PackageManagerService", loadPackageParam.classLoader), "getApplicationInfo", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        String str;
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5188 && (str = (String) methodHookParam.args[0]) != null && str.equals(PkgName.getPkgName()) && XSupport.this.m6254(methodHookParam)) {
                            methodHookParam.setResult((Object) null);
                        }
                    }
                });
                XposedBridge.hookAllMethods(XposedHelpers.findClass("android.content.pm.PackageParser", loadPackageParam.classLoader), "generatePackageInfo", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5188 && ((String) XposedHelpers.getObjectField(methodHookParam.args[0], "packageName")).equals(PkgName.getPkgName()) && XSupport.this.f5202) {
                            methodHookParam.setResult((Object) null);
                        }
                    }
                });
                XposedBridge.hookAllMethods(XposedHelpers.findClass("android.content.pm.PackageParser", loadPackageParam.classLoader), "generateApplicationInfo", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5188 && ((String) XposedHelpers.getObjectField(methodHookParam.args[0], "packageName")).equals(PkgName.getPkgName()) && XSupport.this.f5202) {
                            methodHookParam.setResult((Object) null);
                        }
                    }
                });
                XposedBridge.hookAllMethods(XposedHelpers.findClass("com.android.server.PackageManagerService", loadPackageParam.classLoader), "getInstalledApplications", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5188 && XSupport.this.m6254(methodHookParam)) {
                            XSupport.this.f5202 = true;
                        }
                    }

                    /* access modifiers changed from: protected */
                    public void afterHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.f5202 = false;
                    }
                });
                XposedBridge.hookAllMethods(XposedHelpers.findClass("com.android.server.PackageManagerService", loadPackageParam.classLoader), "getInstalledPackages", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5188 && XSupport.this.m6254(methodHookParam)) {
                            XSupport.this.f5202 = true;
                        }
                    }

                    /* access modifiers changed from: protected */
                    public void afterHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.f5202 = false;
                    }
                });
                XposedBridge.hookAllMethods(XposedHelpers.findClass("com.android.server.PackageManagerService", loadPackageParam.classLoader), "getPreferredPackages", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5188 && XSupport.this.m6254(methodHookParam)) {
                            XSupport.this.f5202 = true;
                        }
                    }

                    /* access modifiers changed from: protected */
                    public void afterHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.f5202 = false;
                    }
                });
                XposedBridge.hookAllMethods(XposedHelpers.findClass("com.android.server.PackageManagerService", loadPackageParam.classLoader), "checkSignaturesLP", new XC_MethodHook() {
                    /* access modifiers changed from: protected */
                    public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                        XSupport.this.m6251();
                        if (XSupport.f5189 && XSupport.f5186) {
                            methodHookParam.setResult(0);
                        }
                    }
                });
            }
        }
        if (Build.VERSION.SDK_INT > 10) {
            XposedBridge.hookAllConstructors(XposedHelpers.findClass("android.app.ApplicationPackageManager", loadPackageParam.classLoader), new XC_MethodHook() {
                /* access modifiers changed from: protected */
                public void afterHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                    XSupport.this.f5194 = (Context) methodHookParam.args[0];
                    XSupport.this.m6251();
                    if (XSupport.f5189 && XSupport.f5188) {
                        XSupport.this.f5195 = false;
                        if (XSupport.this.f5194 != null && !XSupport.this.f5194.getPackageName().equals(PkgName.getPkgName()) && !XSupport.this.f5194.getPackageName().equals("de.robv.android.xposed.installer") && !XSupport.this.f5194.getPackageName().contains("supersu") && !XSupport.this.f5194.getPackageName().contains("magisk") && !XSupport.this.f5194.getPackageName().contains("superuser") && !XSupport.this.f5194.getPackageName().contains("pro.burgerz.wsm.manager")) {
                            XSupport.this.f5195 = true;
                            try {
                                ApplicationInfo applicationInfo = (ApplicationInfo) XposedHelpers.callMethod(methodHookParam.thisObject, "getApplicationInfo", new Object[]{XSupport.this.f5194.getPackageName(), 0});
                                if (applicationInfo != null) {
                                    if ((applicationInfo.flags & 1) != 0) {
                                        XSupport.this.f5195 = false;
                                        return;
                                    }
                                    XSupport.this.f5195 = true;
                                }
                                Intent intent = new Intent("android.intent.action.MAIN");
                                intent.addCategory("android.intent.category.HOME");
                                intent.addCategory("android.intent.category.DEFAULT");
                                for (ResolveInfo resolveInfo : (List) XposedHelpers.callMethod(methodHookParam.thisObject, "queryIntentActivities", new Object[]{intent, 0})) {
                                    if (resolveInfo.activityInfo.packageName.equals(XSupport.this.f5194.getPackageName())) {
                                        XSupport.this.f5195 = false;
                                        return;
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                XSupport.this.f5195 = false;
                            }
                        }
                    }
                }
            });
            XposedBridge.hookAllMethods(XposedHelpers.findClass("android.app.ApplicationPackageManager", loadPackageParam.classLoader), "getPackageInfo", new XC_MethodHook() {
                /* access modifiers changed from: protected */
                public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                    String str;
                    XSupport.this.m6251();
                    if (XSupport.this.f5195 != null && XSupport.this.f5195.booleanValue() && XSupport.f5189 && XSupport.f5188 && (str = (String) methodHookParam.args[0]) != null && str.equals(PkgName.getPkgName())) {
                        methodHookParam.setThrowable(new PackageManager.NameNotFoundException());
                    }
                }
            });
            XposedBridge.hookAllMethods(XposedHelpers.findClass("android.app.ApplicationPackageManager", loadPackageParam.classLoader), "getApplicationInfo", new XC_MethodHook() {
                /* access modifiers changed from: protected */
                public void beforeHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                    String str;
                    XSupport.this.m6251();
                    if (XSupport.this.f5195 != null && XSupport.this.f5195.booleanValue() && XSupport.f5189 && XSupport.f5188 && (str = (String) methodHookParam.args[0]) != null && str.equals(PkgName.getPkgName())) {
                        methodHookParam.setThrowable(new PackageManager.NameNotFoundException());
                    }
                }
            });
            XposedBridge.hookAllMethods(XposedHelpers.findClass("android.app.ApplicationPackageManager", loadPackageParam.classLoader), "getInstalledApplications", new XC_MethodHook() {
                /* access modifiers changed from: protected */
                public void afterHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                    XSupport.this.m6251();
                    if (XSupport.this.f5195 != null && XSupport.this.f5195.booleanValue() && XSupport.f5189 && XSupport.f5188) {
                        Context context = XSupport.this.f5194;
                        List<ApplicationInfo> list = (List) methodHookParam.getResult();
                        ApplicationInfo applicationInfo = null;
                        for (ApplicationInfo applicationInfo2 : list) {
                            if (applicationInfo2.packageName.equals(PkgName.getPkgName())) {
                                applicationInfo = applicationInfo2;
                            }
                        }
                        if (applicationInfo != null) {
                            list.remove(applicationInfo);
                            methodHookParam.setResult(list);
                        }
                    }
                }
            });
            XposedBridge.hookAllMethods(XposedHelpers.findClass("android.app.ApplicationPackageManager", loadPackageParam.classLoader), "getInstalledPackages", new XC_MethodHook() {
                /* access modifiers changed from: protected */
                public void afterHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                    XSupport.this.m6251();
                    if (XSupport.this.f5195 != null && XSupport.this.f5195.booleanValue() && XSupport.f5189 && XSupport.f5188) {
                        Context context = XSupport.this.f5194;
                        List<PackageInfo> list = (List) methodHookParam.getResult();
                        PackageInfo packageInfo = null;
                        for (PackageInfo packageInfo2 : list) {
                            if (packageInfo2.packageName.equals(PkgName.getPkgName())) {
                                packageInfo = packageInfo2;
                            }
                        }
                        if (packageInfo != null) {
                            methodHookParam.setResult(list);
                        }
                    }
                }
            });
            XposedBridge.hookAllMethods(XposedHelpers.findClass("android.app.ApplicationPackageManager", loadPackageParam.classLoader), "getPreferredPackages", new XC_MethodHook() {
                /* access modifiers changed from: protected */
                public void afterHookedMethod(XC_MethodHook.MethodHookParam methodHookParam) {
                    XSupport.this.m6251();
                    if (XSupport.this.f5195 != null && XSupport.this.f5195.booleanValue() && XSupport.f5189 && XSupport.f5188) {
                        Context context = XSupport.this.f5194;
                        List<PackageInfo> list = (List) methodHookParam.getResult();
                        PackageInfo packageInfo = null;
                        for (PackageInfo packageInfo2 : list) {
                            if (packageInfo2.packageName.equals(PkgName.getPkgName())) {
                                packageInfo = packageInfo2;
                            }
                        }
                        if (packageInfo != null) {
                            list.remove(packageInfo);
                            methodHookParam.setResult(list);
                        }
                    }
                }
            });
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0048, code lost:
        if (new java.io.File(r6 + "/xposed").canRead() == false) goto L_0x004a;
     */
    @android.annotation.SuppressLint({"NewApi"})
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m6251() {
        /*
            r13 = this;
            java.lang.String[] r0 = com.chelpus.C0815.m5222()
            r1 = 0
            r2 = 0
        L_0x0006:
            r3 = 0
            r5 = 1
            int r6 = r0.length     // Catch:{ all -> 0x0119 }
            if (r2 >= r6) goto L_0x0106
            r6 = r0[r2]     // Catch:{ all -> 0x0119 }
            long r7 = r13.f5193     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.String r9 = "/xposed"
            int r10 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r10 != 0) goto L_0x0058
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r8.<init>()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r8.append(r6)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r8.append(r9)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r7.<init>(r8)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            boolean r7 = r7.exists()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            if (r7 == 0) goto L_0x004a
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r8.<init>()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r8.append(r6)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r8.append(r9)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r7.<init>(r8)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            boolean r7 = r7.canRead()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            if (r7 != 0) goto L_0x0058
        L_0x004a:
            com.xposed.XSupport.f5184 = r5     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            com.xposed.XSupport.f5185 = r5     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            com.xposed.XSupport.f5186 = r1     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            com.xposed.XSupport.f5187 = r1     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            com.xposed.XSupport.f5188 = r1     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            com.xposed.XSupport.f5189 = r5     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            goto L_0x0102
        L_0x0058:
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r8.<init>()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r8.append(r6)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r8.append(r9)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r7.<init>(r8)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            boolean r7 = r7.canRead()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            if (r7 == 0) goto L_0x0102
            long r7 = r13.f5193     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            int r10 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r10 == 0) goto L_0x0096
            long r7 = r13.f5193     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.io.File r10 = new java.io.File     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r11.<init>()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r11.append(r6)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r11.append(r9)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r10.<init>(r11)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            long r10 = r10.lastModified()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            int r12 = (r7 > r10 ? 1 : (r7 == r10 ? 0 : -1))
            if (r12 >= 0) goto L_0x0102
        L_0x0096:
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.String r8 = "Update settings xposed"
            r7.println(r8)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r7.<init>()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r7 = 0
            org.json.JSONObject r7 = com.chelpus.C0815.m5314()     // Catch:{ JSONException -> 0x00a8 }
            goto L_0x00ac
        L_0x00a8:
            r8 = move-exception
            r8.printStackTrace()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
        L_0x00ac:
            if (r7 == 0) goto L_0x00de
            java.lang.String r8 = "patch1"
            boolean r8 = r7.optBoolean(r8, r5)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            com.xposed.XSupport.f5184 = r8     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.String r8 = "patch2"
            boolean r8 = r7.optBoolean(r8, r5)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            com.xposed.XSupport.f5185 = r8     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.String r8 = "patch3"
            boolean r8 = r7.optBoolean(r8, r1)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            com.xposed.XSupport.f5186 = r8     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.String r8 = "patch4"
            boolean r8 = r7.optBoolean(r8, r1)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            com.xposed.XSupport.f5187 = r8     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.String r8 = "hide"
            boolean r8 = r7.optBoolean(r8, r1)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            com.xposed.XSupport.f5188 = r8     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.String r8 = "module_on"
            boolean r7 = r7.optBoolean(r8, r5)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            com.xposed.XSupport.f5189 = r7     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
        L_0x00de:
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r8.<init>()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r8.append(r6)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r8.append(r9)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            java.lang.String r6 = r8.toString()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r7.<init>(r6)     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            long r6 = r7.lastModified()     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            r13.f5193 = r6     // Catch:{ Exception -> 0x00fe, Throwable -> 0x00f9 }
            goto L_0x0102
        L_0x00f9:
            r6 = move-exception
            r6.printStackTrace()     // Catch:{ all -> 0x0119 }
            goto L_0x0102
        L_0x00fe:
            r6 = move-exception
            r6.printStackTrace()     // Catch:{ all -> 0x0119 }
        L_0x0102:
            int r2 = r2 + 1
            goto L_0x0006
        L_0x0106:
            long r6 = r13.f5193
            int r0 = (r6 > r3 ? 1 : (r6 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x0118
            com.xposed.XSupport.f5184 = r5
            com.xposed.XSupport.f5185 = r5
            com.xposed.XSupport.f5186 = r1
            com.xposed.XSupport.f5187 = r1
            com.xposed.XSupport.f5188 = r1
            com.xposed.XSupport.f5189 = r5
        L_0x0118:
            return
        L_0x0119:
            r0 = move-exception
            long r6 = r13.f5193
            int r2 = (r6 > r3 ? 1 : (r6 == r3 ? 0 : -1))
            if (r2 != 0) goto L_0x012c
            com.xposed.XSupport.f5184 = r5
            com.xposed.XSupport.f5185 = r5
            com.xposed.XSupport.f5186 = r1
            com.xposed.XSupport.f5187 = r1
            com.xposed.XSupport.f5188 = r1
            com.xposed.XSupport.f5189 = r5
        L_0x012c:
            goto L_0x012e
        L_0x012d:
            throw r0
        L_0x012e:
            goto L_0x012d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xposed.XSupport.m6251():void");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m6252(XC_MethodHook.MethodHookParam methodHookParam) {
        int callingUid = Binder.getCallingUid();
        String str = (String) XposedHelpers.callMethod(methodHookParam.thisObject, "getNameForUid", new Object[]{Integer.valueOf(callingUid)});
        if (!str.contains(":") && !str.contains(PkgName.getPkgName()) && !str.contains("de.robv.android.xposed.installer") && !str.contains("supersu") && !str.contains("magisk") && !str.contains("superuser") && !str.contains("pro.burgerz.wsm.manager")) {
            PackageManager packageManager = this.f5197.getPackageManager();
            try {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 0);
                if (applicationInfo != null && (applicationInfo.flags & 1) != 0) {
                    return false;
                }
                Intent intent = new Intent("android.intent.action.MAIN");
                intent.addCategory("android.intent.category.HOME");
                intent.addCategory("android.intent.category.DEFAULT");
                for (ResolveInfo resolveInfo : packageManager.queryIntentActivities(intent, 0)) {
                    if (resolveInfo.activityInfo.packageName.equals(str)) {
                        return false;
                    }
                }
                return true;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m6254(XC_MethodHook.MethodHookParam methodHookParam) {
        int callingUid = Binder.getCallingUid();
        String str = (String) XposedHelpers.callMethod(methodHookParam.thisObject, "getNameForUid", new Object[]{Integer.valueOf(callingUid)});
        if (!str.contains(":") && !str.contains(PkgName.getPkgName()) && !str.contains("de.robv.android.xposed.installer") && !str.contains("supersu") && !str.contains("magisk") && !str.contains("superuser") && !str.contains("pro.burgerz.wsm.manager")) {
            PackageManager packageManager = this.f5197.getPackageManager();
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo(str, 0);
                if (packageInfo != null && (packageInfo.applicationInfo.flags & 1) != 0) {
                    return false;
                }
                Intent intent = new Intent("android.intent.action.MAIN");
                intent.addCategory("android.intent.category.HOME");
                intent.addCategory("android.intent.category.DEFAULT");
                for (ResolveInfo resolveInfo : packageManager.queryIntentActivities(intent, 0)) {
                    if (resolveInfo.activityInfo.packageName.equals(str)) {
                        return false;
                    }
                }
                return true;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m6253(XC_MethodHook.MethodHookParam methodHookParam, Intent intent, int i, int i2) {
        String str;
        String str2;
        String str3;
        String str4;
        Context context;
        Application application;
        XC_MethodHook.MethodHookParam methodHookParam2 = methodHookParam;
        Intent intent2 = intent;
        int i3 = i;
        int i4 = i2;
        if (i4 == 3) {
            try {
                application = AndroidAppHelper.currentApplication();
            } catch (Throwable th) {
                th.printStackTrace();
                application = null;
            }
            if (application != null) {
                if (i3 == 0 && application.getPackageManager().getComponentEnabledSetting(new ComponentName(C0776.f3092, InAppBillingService.class.getName())) != 2) {
                    return intent2.getStringExtra("xexe") == null || !intent2.getStringExtra("xexe").equals("lp");
                }
                if (i3 == 1 && application.getPackageManager().getComponentEnabledSetting(new ComponentName(C0776.f3092, LicensingService.class.getName())) != 2) {
                    return intent2.getStringExtra("xexe") == null || !intent2.getStringExtra("xexe").equals("lp");
                }
            }
        }
        if (i4 == 0) {
            try {
                context = (Context) XposedHelpers.callMethod(methodHookParam2.thisObject, "getBaseContext", new Object[0]);
            } catch (ClassCastException unused) {
                context = (Context) XposedHelpers.getObjectField(methodHookParam2.thisObject, "mBase");
            }
            if (context != null) {
                if (i3 == 0 && context.getPackageManager().getComponentEnabledSetting(new ComponentName(C0776.f3092, InAppBillingService.class.getName())) != 2) {
                    return intent2.getStringExtra("xexe") == null || !intent2.getStringExtra("xexe").equals("lp");
                }
                if (i3 == 1 && context.getPackageManager().getComponentEnabledSetting(new ComponentName(C0776.f3092, LicensingService.class.getName())) != 2) {
                    return intent2.getStringExtra("xexe") == null || !intent2.getStringExtra("xexe").equals("lp");
                }
            }
        }
        if (i4 == 1) {
            if (i3 == 0 && ((PackageManager) XposedHelpers.callMethod(methodHookParam2.thisObject, "getPackageManager", new Object[0])).getComponentEnabledSetting(new ComponentName(C0776.f3092, InAppBillingService.class.getName())) != 2) {
                try {
                    str4 = intent2.getStringExtra("xexe");
                } catch (Exception unused2) {
                    System.out.println("skip inapp xposed queryIntentServices");
                    str4 = null;
                }
                return str4 == null || !str4.equals("lp");
            } else if (i3 == 1 && ((PackageManager) XposedHelpers.callMethod(methodHookParam2.thisObject, "getPackageManager", new Object[0])).getComponentEnabledSetting(new ComponentName(C0776.f3092, LicensingService.class.getName())) != 2) {
                try {
                    str3 = intent2.getStringExtra("xexe");
                } catch (Exception unused3) {
                    System.out.println("skip inapp xposed queryIntentServices");
                    str3 = null;
                }
                return str3 == null || !str3.equals("lp");
            }
        }
        if (i4 == 2) {
            if (i3 == 0 && this.f5197.getPackageManager().getComponentEnabledSetting(new ComponentName(C0776.f3092, InAppBillingService.class.getName())) != 2) {
                try {
                    str2 = intent2.getStringExtra("xexe");
                } catch (Exception unused4) {
                    System.out.println("skip inapp xposed queryIntentServices");
                    str2 = null;
                }
                return str2 == null || !str2.equals("lp");
            } else if (i3 == 1 && this.f5197.getPackageManager().getComponentEnabledSetting(new ComponentName(C0776.f3092, LicensingService.class.getName())) != 2) {
                try {
                    str = intent2.getStringExtra("xexe");
                } catch (Exception unused5) {
                    System.out.println("skip inapp xposed queryIntentServices");
                    str = null;
                }
                return str == null || !str.equals("lp");
            }
        }
        return false;
    }
}
