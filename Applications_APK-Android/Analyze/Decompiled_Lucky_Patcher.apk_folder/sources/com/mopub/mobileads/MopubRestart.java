package com.mopub.mobileads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MopubRestart extends BroadcastReceiver {
    private static String a = "mopub_ads";

    public void onReceive(Context context, Intent intent) {
        new StringBuilder("MopubRestart: Received intent: ").append(intent.getAction());
    }
}
