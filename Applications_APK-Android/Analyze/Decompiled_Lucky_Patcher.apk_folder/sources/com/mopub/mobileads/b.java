package com.mopub.mobileads;

import android.util.Base64;
import java.net.URLEncoder;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

/* compiled from: AesUtils */
public final class b {
    public static String a = "DE11C6E6A348D537";
    private static final String b = "mopub_ads";
    private static String c = "Vic6302222036ciV";
    private static String d = "AES/CBC/PKCS5Padding";

    private static byte[] c(String str, String str2) {
        Cipher instance = Cipher.getInstance(d);
        instance.init(1, new SecretKeySpec(str2.getBytes("UTF-8"), "AES"), new IvParameterSpec(c.getBytes("UTF-8")));
        return instance.doFinal(str.getBytes("UTF-8"));
    }

    public static String a(String str, String str2) {
        byte[] decode = Base64.decode(str, 0);
        Cipher instance = Cipher.getInstance(d);
        instance.init(2, new SecretKeySpec(str2.getBytes("UTF-8"), "AES"), new IvParameterSpec(c.getBytes("UTF-8")));
        return new String(instance.doFinal(decode), "UTF-8");
    }

    private static String a(byte[] bArr, String str) {
        Cipher instance = Cipher.getInstance(d);
        instance.init(2, new SecretKeySpec(str.getBytes("UTF-8"), "AES"), new IvParameterSpec(c.getBytes("UTF-8")));
        return new String(instance.doFinal(bArr), "UTF-8");
    }

    private static String a(String str) {
        return (str == null || str.equals(BuildConfig.FLAVOR)) ? BuildConfig.FLAVOR : Base64.encodeToString(str.getBytes(), 0);
    }

    private static String b(String str) {
        if (str != null) {
            try {
                if (!str.equals(BuildConfig.FLAVOR)) {
                    return URLEncoder.encode(str, "UTF-8");
                }
            } catch (Exception unused) {
            }
        }
        return BuildConfig.FLAVOR;
    }

    private static String b(String str, String str2) {
        Cipher instance = Cipher.getInstance(d);
        instance.init(1, new SecretKeySpec(str2.getBytes("UTF-8"), "AES"), new IvParameterSpec(c.getBytes("UTF-8")));
        return Base64.encodeToString(instance.doFinal(str.getBytes("UTF-8")), 0);
    }
}
