package com.mopub.mobileads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

public class Install extends BroadcastReceiver {
    private static String a = "mopub_ads";

    public void onReceive(Context context, Intent intent) {
        Bundle extras;
        String string;
        if ("com.android.vending.INSTALL_REFERRER".equals(intent.getAction()) && (extras = intent.getExtras()) != null && (string = extras.getString("referrer")) != null) {
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(context).edit();
            edit.putString("referrer", string);
            edit.commit();
        }
    }
}
