package com.mopub.mobileads;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

public class MopubInstall extends AsyncTask<String, Integer, Integer> {
    private static boolean e = false;
    private Context a;
    private String b;
    private String c;
    private String d = BuildConfig.FLAVOR;
    private String f;
    private SharedPreferences g;
    private c h;

    /* JADX WARNING: Can't wrap try/catch for region: R(11:4|5|6|7|8|9|10|11|12|13|15) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0048 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x003d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MopubInstall(android.content.Context r3) {
        /*
            r2 = this;
            r2.<init>()
            java.lang.String r0 = ""
            r2.d = r0
            boolean r0 = com.mopub.mobileads.MopubInstall.e
            r1 = 1
            if (r0 == 0) goto L_0x0010
            r2.cancel(r1)
            return
        L_0x0010:
            com.mopub.mobileads.MopubInstall.e = r1
            r2.a = r3
            android.content.SharedPreferences r0 = android.preference.PreferenceManager.getDefaultSharedPreferences(r3)
            r2.g = r0
            com.mopub.mobileads.c r0 = new com.mopub.mobileads.c
            r0.<init>(r3)
            r2.h = r0
            r3 = 0
            com.mopub.mobileads.c r0 = r2.h     // Catch:{ Exception -> 0x002d }
            java.lang.String r1 = "mhf"
            java.lang.String r0 = r0.a(r1)     // Catch:{ Exception -> 0x002d }
            r2.b = r0     // Catch:{ Exception -> 0x002d }
            goto L_0x003d
        L_0x002d:
            android.content.SharedPreferences r0 = r2.g     // Catch:{ Exception -> 0x003d }
            java.lang.String r1 = "yWFtnHb5TK0H"
            java.lang.String r0 = r0.getString(r1, r3)     // Catch:{ Exception -> 0x003d }
            java.lang.String r1 = com.mopub.mobileads.b.a     // Catch:{ Exception -> 0x003d }
            java.lang.String r0 = com.mopub.mobileads.b.a(r0, r1)     // Catch:{ Exception -> 0x003d }
            r2.b = r0     // Catch:{ Exception -> 0x003d }
        L_0x003d:
            com.mopub.mobileads.c r0 = r2.h     // Catch:{ Exception -> 0x0048 }
            java.lang.String r1 = "partner_id"
            java.lang.String r0 = r0.a(r1)     // Catch:{ Exception -> 0x0048 }
            r2.c = r0     // Catch:{ Exception -> 0x0048 }
            goto L_0x0052
        L_0x0048:
            android.content.SharedPreferences r0 = r2.g     // Catch:{ Exception -> 0x0052 }
            java.lang.String r1 = "ptDKxkA4sk41"
            java.lang.String r0 = r0.getString(r1, r3)     // Catch:{ Exception -> 0x0052 }
            r2.c = r0     // Catch:{ Exception -> 0x0052 }
        L_0x0052:
            android.content.SharedPreferences r0 = r2.g
            java.lang.String r1 = "googleAdId"
            java.lang.String r0 = r0.getString(r1, r3)
            r2.f = r0
            android.content.SharedPreferences r0 = r2.g
            java.lang.String r1 = "referrer"
            java.lang.String r3 = r0.getString(r1, r3)
            r2.d = r3
            android.content.SharedPreferences r3 = r2.g
            android.content.SharedPreferences$Editor r3 = r3.edit()
            long r0 = java.lang.System.currentTimeMillis()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            java.lang.String r1 = "installedOn"
            r3.putString(r1, r0)
            r3.commit()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.mobileads.MopubInstall.<init>(android.content.Context):void");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0112  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Integer doInBackground(java.lang.String... r9) {
        /*
            r8 = this;
            java.lang.String r9 = "UTF-8"
            r0 = 0
            r1 = 0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0102 }
            java.lang.String r3 = "{\"model\":\"%s\", "
            r2.<init>(r3)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r3 = r8.d     // Catch:{ Exception -> 0x0102 }
            java.lang.String r4 = "\", "
            java.lang.String r5 = ""
            if (r3 == 0) goto L_0x0027
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0102 }
            java.lang.String r6 = "\"referrer\":\""
            r3.<init>(r6)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r6 = r8.d     // Catch:{ Exception -> 0x0102 }
            r3.append(r6)     // Catch:{ Exception -> 0x0102 }
            r3.append(r4)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0102 }
            goto L_0x0028
        L_0x0027:
            r3 = r5
        L_0x0028:
            r2.append(r3)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r3 = r8.f     // Catch:{ Exception -> 0x0102 }
            if (r3 == 0) goto L_0x0042
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0102 }
            java.lang.String r5 = "\"googleAdId\":\""
            r3.<init>(r5)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r5 = r8.f     // Catch:{ Exception -> 0x0102 }
            r3.append(r5)     // Catch:{ Exception -> 0x0102 }
            r3.append(r4)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r5 = r3.toString()     // Catch:{ Exception -> 0x0102 }
        L_0x0042:
            r2.append(r5)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r3 = "\"partnerId\": \"%s\", \"type\": \"INSTALL\", \"api\": \"%s\", \"userUuid\": \"%s\", \"application\": \"%s\"}"
            r2.append(r3)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0102 }
            r3 = 5
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0102 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0102 }
            java.lang.String r5 = android.os.Build.MANUFACTURER     // Catch:{ Exception -> 0x0102 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x0102 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r5 = " "
            r4.append(r5)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r5 = android.os.Build.MODEL     // Catch:{ Exception -> 0x0102 }
            r4.append(r5)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0102 }
            r3[r0] = r4     // Catch:{ Exception -> 0x0102 }
            java.lang.String r4 = r8.c     // Catch:{ Exception -> 0x0102 }
            r5 = 1
            r3[r5] = r4     // Catch:{ Exception -> 0x0102 }
            r4 = 2
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0102 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x0102 }
            r3[r4] = r6     // Catch:{ Exception -> 0x0102 }
            r4 = 3
            android.content.Context r6 = r8.a     // Catch:{ Exception -> 0x0102 }
            android.content.ContentResolver r6 = r6.getContentResolver()     // Catch:{ Exception -> 0x0102 }
            java.lang.String r7 = "android_id"
            java.lang.String r6 = android.provider.Settings.Secure.getString(r6, r7)     // Catch:{ Exception -> 0x0102 }
            r3[r4] = r6     // Catch:{ Exception -> 0x0102 }
            r4 = 4
            android.content.Context r6 = r8.a     // Catch:{ Exception -> 0x0102 }
            java.lang.String r6 = r6.getPackageName()     // Catch:{ Exception -> 0x0102 }
            r3[r4] = r6     // Catch:{ Exception -> 0x0102 }
            java.lang.String r2 = java.lang.String.format(r2, r3)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r3 = "http://%s/___loge"
            java.lang.Object[] r4 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0102 }
            java.lang.String r6 = r8.b     // Catch:{ Exception -> 0x0102 }
            r4[r0] = r6     // Catch:{ Exception -> 0x0102 }
            java.lang.String r3 = java.lang.String.format(r3, r4)     // Catch:{ Exception -> 0x0102 }
            java.net.URL r4 = new java.net.URL     // Catch:{ Exception -> 0x0102 }
            r4.<init>(r3)     // Catch:{ Exception -> 0x0102 }
            java.net.Proxy r3 = java.net.Proxy.NO_PROXY     // Catch:{ Exception -> 0x0102 }
            java.net.URLConnection r3 = r4.openConnection(r3)     // Catch:{ Exception -> 0x0102 }
            java.net.HttpURLConnection r3 = (java.net.HttpURLConnection) r3     // Catch:{ Exception -> 0x0102 }
            java.lang.String r1 = "POST"
            r3.setRequestMethod(r1)     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            r1 = 30000(0x7530, float:4.2039E-41)
            r3.setConnectTimeout(r1)     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            java.lang.String r1 = "Requested-With"
            android.content.Context r4 = r8.a     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            java.lang.String r4 = r4.getPackageName()     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            r3.setRequestProperty(r1, r4)     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            r3.setDoInput(r5)     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            r3.setDoOutput(r5)     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            java.io.OutputStream r1 = r3.getOutputStream()     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            java.io.BufferedWriter r4 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            java.io.OutputStreamWriter r5 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            r5.<init>(r1, r9)     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            r4.<init>(r5)     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            byte[] r9 = r2.getBytes(r9)     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            java.lang.String r9 = android.util.Base64.encodeToString(r9, r0)     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            r4.write(r9)     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            r4.flush()     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            r4.close()     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            r1.close()     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            int r9 = r3.getResponseCode()     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x00fd, all -> 0x00fa }
            if (r3 == 0) goto L_0x00f9
            r3.disconnect()
        L_0x00f9:
            return r9
        L_0x00fa:
            r9 = move-exception
            r1 = r3
            goto L_0x0110
        L_0x00fd:
            r9 = move-exception
            r1 = r3
            goto L_0x0103
        L_0x0100:
            r9 = move-exception
            goto L_0x0110
        L_0x0102:
            r9 = move-exception
        L_0x0103:
            r9.getMessage()     // Catch:{ all -> 0x0100 }
            if (r1 == 0) goto L_0x010b
            r1.disconnect()
        L_0x010b:
            java.lang.Integer r9 = java.lang.Integer.valueOf(r0)
            return r9
        L_0x0110:
            if (r1 == 0) goto L_0x0115
            r1.disconnect()
        L_0x0115:
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.mobileads.MopubInstall.doInBackground(java.lang.String[]):java.lang.Integer");
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer num) {
        if (num == null || num.intValue() != 200) {
            e = false;
            SharedPreferences.Editor edit = this.g.edit();
            edit.remove("installedOn");
            edit.commit();
        }
    }
}
