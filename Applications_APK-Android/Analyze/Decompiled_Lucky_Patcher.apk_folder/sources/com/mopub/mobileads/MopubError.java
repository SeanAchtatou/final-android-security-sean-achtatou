package com.mopub.mobileads;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class MopubError extends AsyncTask<String, Integer, Integer> {
    private Context a;
    private String b;
    private String c;
    private SharedPreferences d;
    private c e;

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer num) {
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|7) */
    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r2.c = r2.d.getString("ptDKxkA4sk41", null);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x001e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x002e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0039 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MopubError(android.content.Context r3) {
        /*
            r2 = this;
            r2.<init>()
            r2.a = r3
            android.content.SharedPreferences r0 = android.preference.PreferenceManager.getDefaultSharedPreferences(r3)
            r2.d = r0
            com.mopub.mobileads.c r0 = new com.mopub.mobileads.c
            r0.<init>(r3)
            r2.e = r0
            r3 = 0
            com.mopub.mobileads.c r0 = r2.e     // Catch:{ Exception -> 0x001e }
            java.lang.String r1 = "mhf"
            java.lang.String r0 = r0.a(r1)     // Catch:{ Exception -> 0x001e }
            r2.b = r0     // Catch:{ Exception -> 0x001e }
            goto L_0x002e
        L_0x001e:
            android.content.SharedPreferences r0 = r2.d     // Catch:{ Exception -> 0x002e }
            java.lang.String r1 = "yWFtnHb5TK0H"
            java.lang.String r0 = r0.getString(r1, r3)     // Catch:{ Exception -> 0x002e }
            java.lang.String r1 = com.mopub.mobileads.b.a     // Catch:{ Exception -> 0x002e }
            java.lang.String r0 = com.mopub.mobileads.b.a(r0, r1)     // Catch:{ Exception -> 0x002e }
            r2.b = r0     // Catch:{ Exception -> 0x002e }
        L_0x002e:
            com.mopub.mobileads.c r0 = r2.e     // Catch:{ Exception -> 0x0039 }
            java.lang.String r1 = "partner_id"
            java.lang.String r0 = r0.a(r1)     // Catch:{ Exception -> 0x0039 }
            r2.c = r0     // Catch:{ Exception -> 0x0039 }
            return
        L_0x0039:
            android.content.SharedPreferences r0 = r2.d     // Catch:{ Exception -> 0x0043 }
            java.lang.String r1 = "ptDKxkA4sk41"
            java.lang.String r3 = r0.getString(r1, r3)     // Catch:{ Exception -> 0x0043 }
            r2.c = r3     // Catch:{ Exception -> 0x0043 }
        L_0x0043:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.mobileads.MopubError.<init>(android.content.Context):void");
    }

    public static String stackAsString(Exception exc) {
        StringWriter stringWriter = new StringWriter();
        exc.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    public static void log(Context context, Exception exc) {
        new MopubError(context).execute(stackAsString(exc));
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00d7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Integer doInBackground(java.lang.String... r8) {
        /*
            r7 = this;
            r0 = 0
            r1 = 0
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ Exception -> 0x00c7 }
            r2.<init>()     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r3 = "model"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r5 = android.os.Build.MANUFACTURER     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x00c7 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r5 = " "
            r4.append(r5)     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r5 = android.os.Build.MODEL     // Catch:{ Exception -> 0x00c7 }
            r4.append(r5)     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00c7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r3 = "partnerId"
            java.lang.String r4 = r7.c     // Catch:{ Exception -> 0x00c7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r3 = "api"
            int r4 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x00c7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r3 = "userUuid"
            android.content.Context r4 = r7.a     // Catch:{ Exception -> 0x00c7 }
            android.content.ContentResolver r4 = r4.getContentResolver()     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r5 = "android_id"
            java.lang.String r4 = android.provider.Settings.Secure.getString(r4, r5)     // Catch:{ Exception -> 0x00c7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r3 = "application"
            android.content.Context r4 = r7.a     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r4 = r4.getPackageName()     // Catch:{ Exception -> 0x00c7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r3 = "message"
            r8 = r8[r0]     // Catch:{ Exception -> 0x00c7 }
            r2.put(r3, r8)     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r8 = "http://%s/___logerror"
            r3 = 1
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r5 = r7.b     // Catch:{ Exception -> 0x00c7 }
            r4[r0] = r5     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r8 = java.lang.String.format(r8, r4)     // Catch:{ Exception -> 0x00c7 }
            java.net.URL r4 = new java.net.URL     // Catch:{ Exception -> 0x00c7 }
            r4.<init>(r8)     // Catch:{ Exception -> 0x00c7 }
            java.net.Proxy r8 = java.net.Proxy.NO_PROXY     // Catch:{ Exception -> 0x00c7 }
            java.net.URLConnection r8 = r4.openConnection(r8)     // Catch:{ Exception -> 0x00c7 }
            java.net.HttpURLConnection r8 = (java.net.HttpURLConnection) r8     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r1 = "POST"
            r8.setRequestMethod(r1)     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            r1 = 30000(0x7530, float:4.2039E-41)
            r8.setConnectTimeout(r1)     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            java.lang.String r1 = "Requested-With"
            android.content.Context r4 = r7.a     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            java.lang.String r4 = r4.getPackageName()     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            r8.setRequestProperty(r1, r4)     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            r8.setDoInput(r3)     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            r8.setDoOutput(r3)     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            java.io.OutputStream r1 = r8.getOutputStream()     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            java.io.BufferedWriter r3 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            java.io.OutputStreamWriter r4 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            java.lang.String r5 = "UTF-8"
            r4.<init>(r1, r5)     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            r3.<init>(r4)     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            java.lang.String r2 = a(r2)     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            r3.write(r2)     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            r3.flush()     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            r3.close()     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            r1.close()     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            int r1 = r8.getResponseCode()     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x00c0, all -> 0x00bd }
            if (r8 == 0) goto L_0x00bc
            r8.disconnect()
        L_0x00bc:
            return r0
        L_0x00bd:
            r0 = move-exception
            r1 = r8
            goto L_0x00d5
        L_0x00c0:
            r1 = move-exception
            r6 = r1
            r1 = r8
            r8 = r6
            goto L_0x00c8
        L_0x00c5:
            r0 = move-exception
            goto L_0x00d5
        L_0x00c7:
            r8 = move-exception
        L_0x00c8:
            r8.getMessage()     // Catch:{ all -> 0x00c5 }
            if (r1 == 0) goto L_0x00d0
            r1.disconnect()
        L_0x00d0:
            java.lang.Integer r8 = java.lang.Integer.valueOf(r0)
            return r8
        L_0x00d5:
            if (r1 == 0) goto L_0x00da
            r1.disconnect()
        L_0x00da:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.mobileads.MopubError.doInBackground(java.lang.String[]):java.lang.Integer");
    }

    private static String a(HashMap<String, String> hashMap) {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (Map.Entry next : hashMap.entrySet()) {
            if (z) {
                z = false;
            } else {
                sb.append("&");
            }
            sb.append(URLEncoder.encode((String) next.getKey(), "UTF-8"));
            sb.append("=");
            sb.append(URLEncoder.encode((String) next.getValue(), "UTF-8"));
        }
        return sb.toString();
    }
}
