package com.mopub.mobileads;

import android.app.Activity;
import android.content.Context;

/* compiled from: FakeR */
public final class c {
    private Context a;
    private String b;

    private c(Activity activity) {
        this.a = activity.getApplicationContext();
        this.b = this.a.getPackageName();
    }

    public c(Context context) {
        this.a = context;
        this.b = context.getPackageName();
    }

    private int a(String str, String str2) {
        return this.a.getResources().getIdentifier(str2, str, this.b);
    }

    private static int a(Context context, String str, String str2) {
        return context.getResources().getIdentifier(str2, str, context.getPackageName());
    }

    public final String a(String str) {
        return this.a.getResources().getString(this.a.getResources().getIdentifier(str, "string", this.a.getPackageName()));
    }
}
