package com.google.ʻ.ʽ;

import com.google.ʻ.ʻ.C0847;
import com.google.ʻ.ʾ.C0932;
import java.io.EOFException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayDeque;
import java.util.Deque;

/* renamed from: com.google.ʻ.ʽ.ʻ  reason: contains not printable characters */
/* compiled from: ByteStreams */
public final class C0931 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final OutputStream f3735 = new OutputStream() {
        public String toString() {
            return "ByteStreams.nullOutputStream()";
        }

        public void write(int i) {
        }

        public void write(byte[] bArr) {
            C0847.m5419(bArr);
        }

        public void write(byte[] bArr, int i, int i2) {
            C0847.m5419(bArr);
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    private static byte[] m5804(InputStream inputStream, Deque<byte[]> deque, int i) {
        int i2 = 8192;
        while (i < 2147483639) {
            byte[] bArr = new byte[Math.min(i2, 2147483639 - i)];
            deque.add(bArr);
            int i3 = 0;
            while (i3 < bArr.length) {
                int read = inputStream.read(bArr, i3, bArr.length - i3);
                if (read == -1) {
                    return m5805(deque, i);
                }
                i3 += read;
                i += read;
            }
            i2 = C0932.m5807(i2, 2);
        }
        if (inputStream.read() == -1) {
            return m5805(deque, 2147483639);
        }
        throw new OutOfMemoryError("input is too large to fit in a byte array");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static byte[] m5805(Deque<byte[]> deque, int i) {
        byte[] bArr = new byte[i];
        int i2 = i;
        while (i2 > 0) {
            byte[] removeFirst = deque.removeFirst();
            int min = Math.min(i2, removeFirst.length);
            System.arraycopy(removeFirst, 0, bArr, i - i2, min);
            i2 -= min;
        }
        return bArr;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static byte[] m5803(InputStream inputStream) {
        C0847.m5419(inputStream);
        return m5804(inputStream, new ArrayDeque(20), 0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5801(InputStream inputStream, byte[] bArr) {
        m5802(inputStream, bArr, 0, bArr.length);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5802(InputStream inputStream, byte[] bArr, int i, int i2) {
        int r1 = m5806(inputStream, bArr, i, i2);
        if (r1 != i2) {
            throw new EOFException("reached end of stream after reading " + r1 + " bytes; " + i2 + " bytes expected");
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int m5806(InputStream inputStream, byte[] bArr, int i, int i2) {
        C0847.m5419(inputStream);
        C0847.m5419(bArr);
        if (i2 >= 0) {
            int i3 = 0;
            while (i3 < i2) {
                int read = inputStream.read(bArr, i + i3, i2 - i3);
                if (read == -1) {
                    break;
                }
                i3 += read;
            }
            return i3;
        }
        throw new IndexOutOfBoundsException("len is negative");
    }
}
