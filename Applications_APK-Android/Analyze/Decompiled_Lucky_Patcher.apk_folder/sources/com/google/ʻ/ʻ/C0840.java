package com.google.ʻ.ʻ;

/* renamed from: com.google.ʻ.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: Absent */
final class C0840<T> extends C0846<T> {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0840<Object> f3595 = new C0840<>();

    public boolean equals(Object obj) {
        return obj == this;
    }

    public int hashCode() {
        return 2040732332;
    }

    public String toString() {
        return "Optional.absent()";
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static <T> C0846<T> m5406() {
        return f3595;
    }

    private C0840() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public T m5407(T t) {
        return C0847.m5420(t, "use Optional.orNull() instead of Optional.or(null)");
    }
}
