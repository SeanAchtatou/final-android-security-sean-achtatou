package com.google.ʻ.ʻ;

/* renamed from: com.google.ʻ.ʻ.ˈ  reason: contains not printable characters */
/* compiled from: Preconditions */
public final class C0847 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5422(boolean z) {
        if (!z) {
            throw new IllegalArgumentException();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5423(boolean z, Object obj) {
        if (!z) {
            throw new IllegalArgumentException(String.valueOf(obj));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5424(boolean z, String str, Object obj) {
        if (!z) {
            throw new IllegalArgumentException(C0850.m5436(str, obj));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5425(boolean z, String str, Object obj, Object obj2) {
        if (!z) {
            throw new IllegalArgumentException(C0850.m5436(str, obj, obj2));
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static void m5429(boolean z) {
        if (!z) {
            throw new IllegalStateException();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static void m5430(boolean z, Object obj) {
        if (!z) {
            throw new IllegalStateException(String.valueOf(obj));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> T m5419(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> T m5420(Object obj, Object obj2) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException(String.valueOf(obj2));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m5417(int i, int i2) {
        return m5418(i, i2, "index");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m5418(int i, int i2, String str) {
        if (i >= 0 && i < i2) {
            return i;
        }
        throw new IndexOutOfBoundsException(m5431(i, i2, str));
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static String m5431(int i, int i2, String str) {
        if (i < 0) {
            return C0850.m5436("%s (%s) must not be negative", str, Integer.valueOf(i));
        } else if (i2 >= 0) {
            return C0850.m5436("%s (%s) must be less than size (%s)", str, Integer.valueOf(i), Integer.valueOf(i2));
        } else {
            throw new IllegalArgumentException("negative size: " + i2);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int m5426(int i, int i2) {
        return m5427(i, i2, "index");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int m5427(int i, int i2, String str) {
        if (i >= 0 && i <= i2) {
            return i;
        }
        throw new IndexOutOfBoundsException(m5432(i, i2, str));
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private static String m5432(int i, int i2, String str) {
        if (i < 0) {
            return C0850.m5436("%s (%s) must not be negative", str, Integer.valueOf(i));
        } else if (i2 >= 0) {
            return C0850.m5436("%s (%s) must not be greater than size (%s)", str, Integer.valueOf(i), Integer.valueOf(i2));
        } else {
            throw new IllegalArgumentException("negative size: " + i2);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5421(int i, int i2, int i3) {
        if (i < 0 || i2 < i || i2 > i3) {
            throw new IndexOutOfBoundsException(m5428(i, i2, i3));
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static String m5428(int i, int i2, int i3) {
        if (i < 0 || i > i3) {
            return m5432(i, i3, "start index");
        }
        if (i2 < 0 || i2 > i3) {
            return m5432(i2, i3, "end index");
        }
        return C0850.m5436("end index (%s) must not be less than start index (%s)", Integer.valueOf(i2), Integer.valueOf(i));
    }
}
