package com.google.ʻ.ʻ;

import java.io.Serializable;

/* renamed from: com.google.ʻ.ʻ.ˆ  reason: contains not printable characters */
/* compiled from: Optional */
public abstract class C0846<T> implements Serializable {
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract T m5416(T t);

    /* renamed from: ʼ  reason: contains not printable characters */
    public static <T> C0846<T> m5414() {
        return C0840.m5406();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static <T> C0846<T> m5415(T t) {
        return t == null ? m5414() : new C0849(t);
    }

    C0846() {
    }
}
