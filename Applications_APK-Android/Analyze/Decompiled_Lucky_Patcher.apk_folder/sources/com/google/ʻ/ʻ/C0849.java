package com.google.ʻ.ʻ;

/* renamed from: com.google.ʻ.ʻ.ˊ  reason: contains not printable characters */
/* compiled from: Present */
final class C0849<T> extends C0846<T> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final T f3598;

    C0849(T t) {
        this.f3598 = t;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public T m5434(T t) {
        C0847.m5420(t, "use Optional.orNull() instead of Optional.or(null)");
        return this.f3598;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean equals(java.lang.Object r2) {
        /*
            r1 = this;
            boolean r0 = r2 instanceof com.google.ʻ.ʻ.C0849
            if (r0 == 0) goto L_0x000f
            com.google.ʻ.ʻ.ˊ r2 = (com.google.ʻ.ʻ.C0849) r2
            T r0 = r1.f3598
            T r2 = r2.f3598
            boolean r2 = r0.equals(r2)
            return r2
        L_0x000f:
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ʻ.ʻ.C0849.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        return this.f3598.hashCode() + 1502476572;
    }

    public String toString() {
        return "Optional.of(" + ((Object) this.f3598) + ")";
    }
}
