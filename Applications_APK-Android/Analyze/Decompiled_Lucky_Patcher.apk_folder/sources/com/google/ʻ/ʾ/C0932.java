package com.google.ʻ.ʾ;

import com.google.ʻ.ʿ.C0935;
import net.lingala.zip4j.util.InternalZipConstants;

/* renamed from: com.google.ʻ.ʾ.ʻ  reason: contains not printable characters */
/* compiled from: IntMath */
public final class C0932 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final byte[] f3736 = {9, 9, 9, 8, 8, 8, 7, 7, 7, 6, 6, 6, 6, 5, 5, 5, 4, 4, 4, 3, 3, 3, 3, 2, 2, 2, 1, 1, 1, 0, 0, 0, 0};

    /* renamed from: ʼ  reason: contains not printable characters */
    static final int[] f3737 = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000};

    /* renamed from: ʽ  reason: contains not printable characters */
    static final int[] f3738 = {3, 31, 316, 3162, 31622, 316227, 3162277, 31622776, 316227766, Integer.MAX_VALUE};

    /* renamed from: ʾ  reason: contains not printable characters */
    static int[] f3739 = {Integer.MAX_VALUE, Integer.MAX_VALUE, InternalZipConstants.MIN_SPLIT_LENGTH, 2345, 477, 193, 110, 75, 58, 49, 43, 39, 37, 35, 34, 34, 33};

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final int[] f3740 = {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800, 39916800, 479001600};

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m5807(int i, int i2) {
        return C0935.m5811(((long) i) * ((long) i2));
    }
}
