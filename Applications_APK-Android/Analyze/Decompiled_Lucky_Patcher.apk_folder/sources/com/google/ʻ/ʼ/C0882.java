package com.google.ʻ.ʼ;

/* renamed from: com.google.ʻ.ʼ.ˋˋ  reason: contains not printable characters */
/* compiled from: RegularImmutableSet */
final class C0882<E> extends C0904<E> {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0882<Object> f3649 = new C0882(new Object[0], 0, null, 0, 0);

    /* renamed from: ʼ  reason: contains not printable characters */
    final transient Object[] f3650;

    /* renamed from: ʽ  reason: contains not printable characters */
    final transient Object[] f3651;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final transient int f3652;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final transient int f3653;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final transient int f3654;

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public int m5569() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m5571() {
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m5572() {
        return true;
    }

    C0882(Object[] objArr, int i, Object[] objArr2, int i2, int i3) {
        this.f3650 = objArr;
        this.f3651 = objArr2;
        this.f3652 = i2;
        this.f3653 = i;
        this.f3654 = i3;
    }

    public boolean contains(Object obj) {
        Object[] objArr = this.f3651;
        if (obj == null || objArr == null) {
            return false;
        }
        int r2 = C0883.m5575(obj);
        while (true) {
            int i = r2 & this.f3652;
            Object obj2 = objArr[i];
            if (obj2 == null) {
                return false;
            }
            if (obj2.equals(obj)) {
                return true;
            }
            r2 = i + 1;
        }
    }

    public int size() {
        return this.f3654;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0900<E> iterator() {
        return m5657().iterator();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public Object[] m5568() {
        return this.f3650;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public int m5570() {
        return this.f3654;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m5566(Object[] objArr, int i) {
        System.arraycopy(this.f3650, 0, objArr, i, this.f3654);
        return i + this.f3654;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public C0891<E> m5573() {
        return C0891.m5602(this.f3650, this.f3654);
    }

    public int hashCode() {
        return this.f3653;
    }
}
