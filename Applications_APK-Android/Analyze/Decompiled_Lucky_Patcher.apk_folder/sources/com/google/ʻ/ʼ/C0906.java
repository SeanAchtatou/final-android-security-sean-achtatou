package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import java.io.Serializable;

/* renamed from: com.google.ʻ.ʼ.ᴵᴵ  reason: contains not printable characters */
/* compiled from: NaturalOrdering */
final class C0906 extends C0858<Comparable> implements Serializable {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0906 f3680 = new C0906();

    public String toString() {
        return "Ordering.natural()";
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compare(Comparable comparable, Comparable comparable2) {
        C0847.m5419(comparable);
        C0847.m5419(comparable2);
        return comparable.compareTo(comparable2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public <S extends Comparable> C0858<S> m5668() {
        return C0888.f3660;
    }

    private C0906() {
    }
}
