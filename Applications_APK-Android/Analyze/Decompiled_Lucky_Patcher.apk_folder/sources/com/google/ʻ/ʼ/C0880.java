package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: com.google.ʻ.ʼ.ˊˊ  reason: contains not printable characters */
/* compiled from: RegularImmutableSortedSet */
final class C0880<E> extends C0912<E> {

    /* renamed from: ʽ  reason: contains not printable characters */
    static final C0880<Comparable> f3642 = new C0880<>(C0891.m5603(), C0858.m5447());

    /* renamed from: ʾ  reason: contains not printable characters */
    final transient C0891<E> f3643;

    C0880(C0891<E> r1, Comparator<? super E> comparator) {
        super(comparator);
        this.f3643 = r1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public Object[] m5544() {
        return this.f3643.m5579();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public int m5545() {
        return this.f3643.m5580();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public int m5547() {
        return this.f3643.m5581();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0900<E> iterator() {
        return this.f3643.iterator();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public C0900<E> descendingIterator() {
        return this.f3643.m5611().iterator();
    }

    public int size() {
        return this.f3643.size();
    }

    public boolean contains(Object obj) {
        if (obj == null) {
            return false;
        }
        try {
            return m5539(obj) >= 0;
        } catch (ClassCastException unused) {
            return false;
        }
    }

    public boolean containsAll(Collection<?> collection) {
        if (collection instanceof C0903) {
            collection = ((C0903) collection).m5647();
        }
        if (!C0909.m5697(comparator(), collection) || collection.size() <= 1) {
            return super.containsAll(collection);
        }
        C0900 r0 = iterator();
        Iterator<?> it = collection.iterator();
        if (!r0.hasNext()) {
            return false;
        }
        Object next = it.next();
        Object next2 = r0.next();
        while (true) {
            try {
                int r5 = m5710(next2, next);
                if (r5 < 0) {
                    if (!r0.hasNext()) {
                        return false;
                    }
                    next2 = r0.next();
                } else if (r5 == 0) {
                    if (!it.hasNext()) {
                        return true;
                    }
                    next = it.next();
                } else if (r5 > 0) {
                    break;
                }
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private int m5539(Object obj) {
        return Collections.binarySearch(this.f3643, obj, m5556());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m5553() {
        return this.f3643.m5583();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m5540(Object[] objArr, int i) {
        return this.f3643.m5604(objArr, i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0034 A[Catch:{ ClassCastException | NoSuchElementException -> 0x0046 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r6) {
        /*
            r5 = this;
            r0 = 1
            if (r6 != r5) goto L_0x0004
            return r0
        L_0x0004:
            boolean r1 = r6 instanceof java.util.Set
            r2 = 0
            if (r1 != 0) goto L_0x000a
            return r2
        L_0x000a:
            java.util.Set r6 = (java.util.Set) r6
            int r1 = r5.size()
            int r3 = r6.size()
            if (r1 == r3) goto L_0x0017
            return r2
        L_0x0017:
            boolean r1 = r5.isEmpty()
            if (r1 == 0) goto L_0x001e
            return r0
        L_0x001e:
            java.util.Comparator r1 = r5.f3690
            boolean r1 = com.google.ʻ.ʼ.C0909.m5697(r1, r6)
            if (r1 == 0) goto L_0x0047
            java.util.Iterator r6 = r6.iterator()
            com.google.ʻ.ʼ.ٴٴ r1 = r5.iterator()     // Catch:{ ClassCastException | NoSuchElementException -> 0x0046 }
        L_0x002e:
            boolean r3 = r1.hasNext()     // Catch:{ ClassCastException | NoSuchElementException -> 0x0046 }
            if (r3 == 0) goto L_0x0045
            java.lang.Object r3 = r1.next()     // Catch:{ ClassCastException | NoSuchElementException -> 0x0046 }
            java.lang.Object r4 = r6.next()     // Catch:{ ClassCastException | NoSuchElementException -> 0x0046 }
            if (r4 == 0) goto L_0x0044
            int r3 = r5.m5710(r3, r4)     // Catch:{ ClassCastException | NoSuchElementException -> 0x0046 }
            if (r3 == 0) goto L_0x002e
        L_0x0044:
            return r2
        L_0x0045:
            return r0
        L_0x0046:
            return r2
        L_0x0047:
            boolean r6 = r5.containsAll(r6)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ʻ.ʼ.C0880.equals(java.lang.Object):boolean");
    }

    public E first() {
        if (!isEmpty()) {
            return this.f3643.get(0);
        }
        throw new NoSuchElementException();
    }

    public E last() {
        if (!isEmpty()) {
            return this.f3643.get(size() - 1);
        }
        throw new NoSuchElementException();
    }

    public E lower(E e) {
        int r2 = m5550(e, false) - 1;
        if (r2 == -1) {
            return null;
        }
        return this.f3643.get(r2);
    }

    public E floor(E e) {
        int r2 = m5550(e, true) - 1;
        if (r2 == -1) {
            return null;
        }
        return this.f3643.get(r2);
    }

    public E ceiling(E e) {
        int r2 = m5552(e, true);
        if (r2 == size()) {
            return null;
        }
        return this.f3643.get(r2);
    }

    public E higher(E e) {
        int r2 = m5552(e, false);
        if (r2 == size()) {
            return null;
        }
        return this.f3643.get(r2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ˊˊ.ʼ(int, int):com.google.ʻ.ʼ.ˊˊ<E>
     arg types: [int, int]
     candidates:
      com.google.ʻ.ʼ.ᵢ.ʼ(java.lang.Object, java.lang.Object):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᵢ.ʼ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᴵ.ʼ(int, java.lang.Object[]):com.google.ʻ.ʼ.ᴵ<E>
      com.google.ʻ.ʼ.ᴵ.ʼ(int, int):boolean
      com.google.ʻ.ʼ.ˊˊ.ʼ(int, int):com.google.ʻ.ʼ.ˊˊ<E> */
    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public C0912<E> m5546(E e, boolean z) {
        return m5542(0, m5550(e, z));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public int m5550(E e, boolean z) {
        int binarySearch = Collections.binarySearch(this.f3643, C0847.m5419(e), comparator());
        if (binarySearch >= 0) {
            return z ? binarySearch + 1 : binarySearch;
        }
        return binarySearch ^ -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public C0912<E> m5543(E e, boolean z, E e2, boolean z2) {
        return m5549(e, z).m5719(e2, z2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ˊˊ.ʼ(int, int):com.google.ʻ.ʼ.ˊˊ<E>
     arg types: [int, int]
     candidates:
      com.google.ʻ.ʼ.ᵢ.ʼ(java.lang.Object, java.lang.Object):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᵢ.ʼ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᴵ.ʼ(int, java.lang.Object[]):com.google.ʻ.ʼ.ᴵ<E>
      com.google.ʻ.ʼ.ᴵ.ʼ(int, int):boolean
      com.google.ʻ.ʼ.ˊˊ.ʼ(int, int):com.google.ʻ.ʼ.ˊˊ<E> */
    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public C0912<E> m5549(E e, boolean z) {
        return m5542(m5552(e, z), size());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public int m5552(E e, boolean z) {
        int binarySearch = Collections.binarySearch(this.f3643, C0847.m5419(e), comparator());
        if (binarySearch >= 0) {
            return z ? binarySearch : binarySearch + 1;
        }
        return binarySearch ^ -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public Comparator<Object> m5556() {
        return this.f3690;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public C0880<E> m5542(int i, int i2) {
        if (i == 0 && i2 == size()) {
            return this;
        }
        if (i < i2) {
            return new C0880<>(this.f3643.subList(i, i2), this.f3690);
        }
        return m5704(this.f3690);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public int m5548(Object obj) {
        if (obj == null) {
            return -1;
        }
        try {
            int binarySearch = Collections.binarySearch(this.f3643, obj, m5556());
            if (binarySearch >= 0) {
                return binarySearch;
            }
            return -1;
        } catch (ClassCastException unused) {
            return -1;
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public C0891<E> m5551() {
        return this.f3643;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public C0912<E> m5554() {
        Comparator reverseOrder = Collections.reverseOrder(this.f3690);
        if (isEmpty()) {
            return m5704(reverseOrder);
        }
        return new C0880(this.f3643.m5611(), reverseOrder);
    }
}
