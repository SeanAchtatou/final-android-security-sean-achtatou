package com.google.ʻ.ʼ;

import java.lang.reflect.Array;
import java.util.Arrays;

/* renamed from: com.google.ʻ.ʼ.ʿʿ  reason: contains not printable characters */
/* compiled from: Platform */
final class C0864 {
    /* renamed from: ʻ  reason: contains not printable characters */
    static <T> T[] m5474(T[] tArr, int i) {
        return (Object[]) Array.newInstance(tArr.getClass().getComponentType(), i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static <T> T[] m5475(Object[] objArr, int i, int i2, T[] tArr) {
        return Arrays.copyOfRange(objArr, i, i2, tArr.getClass());
    }
}
