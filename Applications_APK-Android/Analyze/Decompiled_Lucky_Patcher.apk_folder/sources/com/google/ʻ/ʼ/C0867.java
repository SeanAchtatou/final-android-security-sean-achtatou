package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import java.io.Serializable;
import java.util.Comparator;

/* renamed from: com.google.ʻ.ʼ.ˈ  reason: contains not printable characters */
/* compiled from: ComparatorOrdering */
final class C0867<T> extends C0858<T> implements Serializable {

    /* renamed from: ʻ  reason: contains not printable characters */
    final Comparator<T> f3617;

    C0867(Comparator<T> comparator) {
        this.f3617 = (Comparator) C0847.m5419(comparator);
    }

    public int compare(T t, T t2) {
        return this.f3617.compare(t, t2);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof C0867) {
            return this.f3617.equals(((C0867) obj).f3617);
        }
        return false;
    }

    public int hashCode() {
        return this.f3617.hashCode();
    }

    public String toString() {
        return this.f3617.toString();
    }
}
