package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.Map;
import java.util.NavigableMap;

/* renamed from: com.google.ʻ.ʼ.ᵎ  reason: contains not printable characters */
/* compiled from: ImmutableSortedMap */
public final class C0907<K, V> extends C0910<K, V> implements NavigableMap<K, V> {

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final Comparator<Comparable> f3681 = C0858.m5447();

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final C0907<Comparable, Object> f3682 = new C0907<>(C0912.m5704((Comparator) C0858.m5447()), C0891.m5603());
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public final transient C0880<K> f3683;
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public final transient C0891<V> f3684;

    /* renamed from: ˆ  reason: contains not printable characters */
    private transient C0907<K, V> f3685;

    /* renamed from: ʻ  reason: contains not printable characters */
    static <K, V> C0907<K, V> m5671(Comparator<? super K> comparator) {
        if (C0858.m5447().equals(comparator)) {
            return m5673();
        }
        return new C0907<>(C0912.m5704((Comparator) comparator), C0891.m5603());
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public static <K, V> C0907<K, V> m5673() {
        return f3682;
    }

    C0907(C0880<K> r2, C0891<V> r3) {
        this(r2, r3, null);
    }

    C0907(C0880<K> r1, C0891<V> r2, C0907<K, V> r3) {
        this.f3683 = r1;
        this.f3684 = r2;
        this.f3685 = r3;
    }

    public int size() {
        return this.f3684.size();
    }

    public V get(Object obj) {
        int r2 = this.f3683.m5548(obj);
        if (r2 == -1) {
            return null;
        }
        return this.f3684.get(r2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m5686() {
        return this.f3683.m5553() || this.f3684.m5583();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0904<Map.Entry<K, V>> entrySet() {
        return super.entrySet();
    }

    /* renamed from: com.google.ʻ.ʼ.ᵎ$ʻ  reason: contains not printable characters */
    /* compiled from: ImmutableSortedMap */
    class C0908 extends C0899<K, V> {
        C0908() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0900<Map.Entry<K, V>> iterator() {
            return m5657().iterator();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˋ  reason: contains not printable characters */
        public C0891<Map.Entry<K, V>> m5693() {
            return new C0891<Map.Entry<K, V>>() {
                /* access modifiers changed from: package-private */
                /* renamed from: ˆ  reason: contains not printable characters */
                public boolean m5695() {
                    return true;
                }

                /* renamed from: ʼ  reason: contains not printable characters */
                public Map.Entry<K, V> get(int i) {
                    return new AbstractMap.SimpleImmutableEntry(C0907.this.f3683.m5551().get(i), C0907.this.f3684.get(i));
                }

                public int size() {
                    return C0907.this.size();
                }
            };
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˈ  reason: contains not printable characters */
        public C0897<K, V> m5692() {
            return C0907.this;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public C0904<Map.Entry<K, V>> m5681() {
        return isEmpty() ? C0904.m5655() : new C0908();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public C0912<K> m5682() {
        return this.f3683;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public C0904<K> m5683() {
        throw new AssertionError("should never be called");
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public C0885<V> values() {
        return this.f3684;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public C0885<V> m5685() {
        throw new AssertionError("should never be called");
    }

    public Comparator<? super K> comparator() {
        return m5682().comparator();
    }

    public K firstKey() {
        return m5682().first();
    }

    public K lastKey() {
        return m5682().last();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ˊˊ.ʼ(int, int):com.google.ʻ.ʼ.ˊˊ<E>
     arg types: [int, int]
     candidates:
      com.google.ʻ.ʼ.ᵢ.ʼ(java.lang.Object, java.lang.Object):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᵢ.ʼ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᴵ.ʼ(int, java.lang.Object[]):com.google.ʻ.ʼ.ᴵ<E>
      com.google.ʻ.ʼ.ᴵ.ʼ(int, int):boolean
      com.google.ʻ.ʼ.ˊˊ.ʼ(int, int):com.google.ʻ.ʼ.ˊˊ<E> */
    /* renamed from: ʻ  reason: contains not printable characters */
    private C0907<K, V> m5670(int i, int i2) {
        if (i == 0 && i2 == size()) {
            return this;
        }
        if (i == i2) {
            return m5671(comparator());
        }
        return new C0907<>(this.f3683.m5542(i, i2), this.f3684.subList(i, i2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ᵎ.ʻ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵎ<K, V>
     arg types: [K, int]
     candidates:
      com.google.ʻ.ʼ.ᵎ.ʻ(int, int):com.google.ʻ.ʼ.ᵎ<K, V>
      com.google.ʻ.ʼ.ᵎ.ʻ(java.lang.Object, java.lang.Object):com.google.ʻ.ʼ.ᵎ<K, V>
      com.google.ʻ.ʼ.ᵎ.ʻ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵎ<K, V> */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0907<K, V> headMap(K k) {
        return headMap((Object) k, false);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0907<K, V> headMap(K k, boolean z) {
        return m5670(0, this.f3683.m5550(C0847.m5419(k), z));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0907<K, V> subMap(K k, K k2) {
        return subMap(k, true, k2, false);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0907<K, V> subMap(K k, boolean z, K k2, boolean z2) {
        C0847.m5419(k);
        C0847.m5419(k2);
        C0847.m5425(comparator().compare(k, k2) <= 0, "expected fromKey <= toKey but %s > %s", k, k2);
        return headMap(k2, z2).tailMap(k, z);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0907<K, V> tailMap(K k) {
        return tailMap(k, true);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0907<K, V> tailMap(K k, boolean z) {
        return m5670(this.f3683.m5552(C0847.m5419(k), z), size());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ᵎ.ʻ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵎ<K, V>
     arg types: [K, int]
     candidates:
      com.google.ʻ.ʼ.ᵎ.ʻ(int, int):com.google.ʻ.ʼ.ᵎ<K, V>
      com.google.ʻ.ʼ.ᵎ.ʻ(java.lang.Object, java.lang.Object):com.google.ʻ.ʼ.ᵎ<K, V>
      com.google.ʻ.ʼ.ᵎ.ʻ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵎ<K, V> */
    public Map.Entry<K, V> lowerEntry(K k) {
        return headMap((Object) k, false).lastEntry();
    }

    public K lowerKey(K k) {
        return C0929.m5789(lowerEntry(k));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ᵎ.ʻ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵎ<K, V>
     arg types: [K, int]
     candidates:
      com.google.ʻ.ʼ.ᵎ.ʻ(int, int):com.google.ʻ.ʼ.ᵎ<K, V>
      com.google.ʻ.ʼ.ᵎ.ʻ(java.lang.Object, java.lang.Object):com.google.ʻ.ʼ.ᵎ<K, V>
      com.google.ʻ.ʼ.ᵎ.ʻ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵎ<K, V> */
    public Map.Entry<K, V> floorEntry(K k) {
        return headMap((Object) k, true).lastEntry();
    }

    public K floorKey(K k) {
        return C0929.m5789(floorEntry(k));
    }

    public Map.Entry<K, V> ceilingEntry(K k) {
        return tailMap(k, true).firstEntry();
    }

    public K ceilingKey(K k) {
        return C0929.m5789(ceilingEntry(k));
    }

    public Map.Entry<K, V> higherEntry(K k) {
        return tailMap(k, false).firstEntry();
    }

    public K higherKey(K k) {
        return C0929.m5789(higherEntry(k));
    }

    public Map.Entry<K, V> firstEntry() {
        if (isEmpty()) {
            return null;
        }
        return (Map.Entry) entrySet().m5657().get(0);
    }

    public Map.Entry<K, V> lastEntry() {
        if (isEmpty()) {
            return null;
        }
        return (Map.Entry) entrySet().m5657().get(size() - 1);
    }

    @Deprecated
    public final Map.Entry<K, V> pollFirstEntry() {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final Map.Entry<K, V> pollLastEntry() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public C0907<K, V> descendingMap() {
        C0907<K, V> r0 = this.f3685;
        if (r0 != null) {
            return r0;
        }
        if (isEmpty()) {
            return m5671((Comparator) C0858.m5446(comparator()).m5449());
        }
        return new C0907<>((C0880) this.f3683.descendingSet(), this.f3684.m5611(), this);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public C0912<K> navigableKeySet() {
        return this.f3683;
    }

    /* renamed from: י  reason: contains not printable characters */
    public C0912<K> descendingKeySet() {
        return this.f3683.descendingSet();
    }
}
