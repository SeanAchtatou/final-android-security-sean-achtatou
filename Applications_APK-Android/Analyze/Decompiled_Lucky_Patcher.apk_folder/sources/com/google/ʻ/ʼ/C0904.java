package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import com.google.ʻ.ʼ.C0885;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

/* renamed from: com.google.ʻ.ʼ.ᴵ  reason: contains not printable characters */
/* compiled from: ImmutableSet */
public abstract class C0904<E> extends C0885<E> implements Set<E> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private transient C0891<E> f3677;

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public static boolean m5654(int i, int i2) {
        return i < (i2 >> 1) + (i2 >> 2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract C0900<E> iterator();

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m5658() {
        return false;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static <E> C0904<E> m5655() {
        return C0882.f3649;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> C0904<E> m5650(Object obj) {
        return new C0911(obj);
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public static <E> C0904<E> m5653(int i, Object... objArr) {
        if (i == 0) {
            return m5655();
        }
        if (i == 1) {
            return m5650(objArr[0]);
        }
        int r2 = m5648(i);
        Object[] objArr2 = new Object[r2];
        int i2 = r2 - 1;
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < i; i5++) {
            Object r4 = C0852.m5438(objArr[i5], i5);
            int hashCode = r4.hashCode();
            int r10 = C0883.m5574(hashCode);
            while (true) {
                int i6 = r10 & i2;
                Object obj = objArr2[i6];
                if (obj == null) {
                    objArr[i4] = r4;
                    objArr2[i6] = r4;
                    i3 += hashCode;
                    i4++;
                    break;
                } else if (obj.equals(r4)) {
                    break;
                } else {
                    r10++;
                }
            }
        }
        Arrays.fill(objArr, i4, i, (Object) null);
        if (i4 == 1) {
            return new C0911(objArr[0], i3);
        }
        if (m5648(i4) < r2 / 2) {
            return m5653(i4, objArr);
        }
        if (m5654(i4, objArr.length)) {
            objArr = Arrays.copyOf(objArr, i4);
        }
        return new C0882(objArr, i3, objArr2, i2, i4);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static int m5648(int i) {
        int max = Math.max(i, 2);
        boolean z = true;
        if (max < 751619276) {
            int highestOneBit = Integer.highestOneBit(max - 1) << 1;
            while (true) {
                double d = (double) highestOneBit;
                Double.isNaN(d);
                if (d * 0.7d >= ((double) max)) {
                    return highestOneBit;
                }
                highestOneBit <<= 1;
            }
        } else {
            if (max >= 1073741824) {
                z = false;
            }
            C0847.m5423(z, "collection too large");
            return 1073741824;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> C0904<E> m5651(Iterator it) {
        if (!it.hasNext()) {
            return m5655();
        }
        Object next = it.next();
        if (!it.hasNext()) {
            return m5650(next);
        }
        return new C0905().m5664(next).m5662(it).m5663();
    }

    C0904() {
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C0904) || !m5658() || !((C0904) obj).m5658() || hashCode() == obj.hashCode()) {
            return C0890.m5594(this, obj);
        }
        return false;
    }

    public int hashCode() {
        return C0890.m5592(this);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public C0891<E> m5657() {
        C0891<E> r0 = this.f3677;
        if (r0 != null) {
            return r0;
        }
        C0891<E> r02 = m5659();
        this.f3677 = r02;
        return r02;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public C0891<E> m5659() {
        return C0891.m5600(toArray());
    }

    /* renamed from: com.google.ʻ.ʼ.ᴵ$ʻ  reason: contains not printable characters */
    /* compiled from: ImmutableSet */
    public static class C0905<E> extends C0885.C0886<E> {

        /* renamed from: ʾ  reason: contains not printable characters */
        Object[] f3678;

        /* renamed from: ʿ  reason: contains not printable characters */
        private int f3679;

        public C0905() {
            super(4);
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public C0905<E> m5664(E e) {
            C0847.m5419(e);
            if (this.f3678 == null || C0904.m5648(this.f3658) > this.f3678.length) {
                this.f3678 = null;
                super.m5586((Object) e);
                return this;
            }
            m5660(e);
            return this;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        private void m5660(E e) {
            int length = this.f3678.length - 1;
            int hashCode = e.hashCode();
            int r2 = C0883.m5574(hashCode);
            while (true) {
                int i = r2 & length;
                Object[] objArr = this.f3678;
                Object obj = objArr[i];
                if (obj == null) {
                    objArr[i] = e;
                    this.f3679 += hashCode;
                    super.m5586((Object) e);
                    return;
                } else if (!obj.equals(e)) {
                    r2 = i + 1;
                } else {
                    return;
                }
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public C0905<E> m5662(Iterator<? extends E> it) {
            C0847.m5419(it);
            while (it.hasNext()) {
                m5664(it.next());
            }
            return this;
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: com.google.ʻ.ʼ.ˋˋ} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v8, resolved type: com.google.ʻ.ʼ.ᴵ} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v18, resolved type: com.google.ʻ.ʼ.ˋˋ} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v9, resolved type: com.google.ʻ.ʼ.ˋˋ} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* renamed from: ʻ  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.google.ʻ.ʼ.C0904<E> m5663() {
            /*
                r8 = this;
                int r0 = r8.f3658
                if (r0 == 0) goto L_0x005b
                r1 = 1
                if (r0 == r1) goto L_0x0051
                java.lang.Object[] r0 = r8.f3678
                if (r0 == 0) goto L_0x003d
                int r0 = r8.f3658
                int r0 = com.google.ʻ.ʼ.C0904.m5648(r0)
                java.lang.Object[] r2 = r8.f3678
                int r2 = r2.length
                if (r0 != r2) goto L_0x003d
                int r0 = r8.f3658
                java.lang.Object[] r2 = r8.f3657
                int r2 = r2.length
                boolean r0 = com.google.ʻ.ʼ.C0904.m5654(r0, r2)
                if (r0 == 0) goto L_0x002a
                java.lang.Object[] r0 = r8.f3657
                int r2 = r8.f3658
                java.lang.Object[] r0 = java.util.Arrays.copyOf(r0, r2)
                goto L_0x002c
            L_0x002a:
                java.lang.Object[] r0 = r8.f3657
            L_0x002c:
                r3 = r0
                com.google.ʻ.ʼ.ˋˋ r0 = new com.google.ʻ.ʼ.ˋˋ
                int r4 = r8.f3679
                java.lang.Object[] r5 = r8.f3678
                int r2 = r5.length
                int r6 = r2 + -1
                int r7 = r8.f3658
                r2 = r0
                r2.<init>(r3, r4, r5, r6, r7)
                goto L_0x004b
            L_0x003d:
                int r0 = r8.f3658
                java.lang.Object[] r2 = r8.f3657
                com.google.ʻ.ʼ.ᴵ r0 = com.google.ʻ.ʼ.C0904.m5653(r0, r2)
                int r2 = r0.size()
                r8.f3658 = r2
            L_0x004b:
                r8.f3659 = r1
                r1 = 0
                r8.f3678 = r1
                return r0
            L_0x0051:
                java.lang.Object[] r0 = r8.f3657
                r1 = 0
                r0 = r0[r1]
                com.google.ʻ.ʼ.ᴵ r0 = com.google.ʻ.ʼ.C0904.m5650(r0)
                return r0
            L_0x005b:
                com.google.ʻ.ʼ.ᴵ r0 = com.google.ʻ.ʼ.C0904.m5655()
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.ʻ.ʼ.C0904.C0905.m5663():com.google.ʻ.ʼ.ᴵ");
        }
    }
}
