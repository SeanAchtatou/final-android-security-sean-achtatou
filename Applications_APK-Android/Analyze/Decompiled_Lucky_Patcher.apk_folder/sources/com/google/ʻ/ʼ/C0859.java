package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0842;
import com.google.ʻ.ʻ.C0845;
import com.google.ʻ.ʻ.C0847;
import java.io.Serializable;

/* renamed from: com.google.ʻ.ʼ.ʾ  reason: contains not printable characters */
/* compiled from: ByFunctionOrdering */
final class C0859<F, T> extends C0858<F> implements Serializable {

    /* renamed from: ʻ  reason: contains not printable characters */
    final C0842<F, ? extends T> f3610;

    /* renamed from: ʼ  reason: contains not printable characters */
    final C0858<T> f3611;

    C0859(C0842<F, ? extends T> r1, C0858<T> r2) {
        this.f3610 = (C0842) C0847.m5419(r1);
        this.f3611 = (C0858) C0847.m5419(r2);
    }

    public int compare(F f, F f2) {
        return this.f3611.compare(this.f3610.m5408(f), this.f3610.m5408(f2));
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C0859)) {
            return false;
        }
        C0859 r5 = (C0859) obj;
        if (!this.f3610.equals(r5.f3610) || !this.f3611.equals(r5.f3611)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return C0845.m5412(this.f3610, this.f3611);
    }

    public String toString() {
        return this.f3611 + ".onResultOf(" + this.f3610 + ")";
    }
}
