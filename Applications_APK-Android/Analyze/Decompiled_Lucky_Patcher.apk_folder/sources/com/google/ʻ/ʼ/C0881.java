package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0846;
import com.google.ʻ.ʻ.C0847;
import java.util.Iterator;

/* renamed from: com.google.ʻ.ʼ.ˋ  reason: contains not printable characters */
/* compiled from: FluentIterable */
public abstract class C0881<E> implements Iterable<E> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C0846<Iterable<E>> f3644;

    protected C0881() {
        this.f3644 = C0846.m5414();
    }

    C0881(Iterable<E> iterable) {
        C0847.m5419(iterable);
        this.f3644 = C0846.m5415(this == iterable ? null : iterable);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private Iterable<E> m5560() {
        return this.f3644.m5416(this);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> C0881<E> m5557(final Iterable<E> iterable) {
        return iterable instanceof C0881 ? (C0881) iterable : new C0881<E>(iterable) {
            public Iterator<E> iterator() {
                return iterable.iterator();
            }
        };
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> C0881<T> m5558(Iterable<? extends T> iterable, Iterable<? extends T> iterable2) {
        return m5559(iterable, iterable2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static <T> C0881<T> m5561(final Iterable<? extends Iterable<? extends T>> iterable) {
        C0847.m5419(iterable);
        return new C0881<T>() {
            public Iterator<T> iterator() {
                return C0920.m5767(C0920.m5760(iterable.iterator(), C0918.m5740()));
            }
        };
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static <T> C0881<T> m5559(final Iterable<? extends T>... iterableArr) {
        for (Iterable<? extends T> r2 : iterableArr) {
            C0847.m5419(r2);
        }
        return new C0881<T>() {
            public Iterator<T> iterator() {
                return C0920.m5767(new C0851<Iterator<? extends T>>(iterableArr.length) {
                    /* renamed from: ʼ  reason: contains not printable characters */
                    public Iterator<? extends T> m5564(int i) {
                        return iterableArr[i].iterator();
                    }
                });
            }
        };
    }

    public String toString() {
        return C0918.m5746(m5560());
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [com.google.ʻ.ʻ.ʽ<? super E, T>, com.google.ʻ.ʻ.ʽ] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T> com.google.ʻ.ʼ.C0881<T> m5563(com.google.ʻ.ʻ.C0842<? super E, T> r2) {
        /*
            r1 = this;
            java.lang.Iterable r0 = r1.m5560()
            java.lang.Iterable r2 = com.google.ʻ.ʼ.C0918.m5742(r0, r2)
            com.google.ʻ.ʼ.ˋ r2 = m5557(r2)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ʻ.ʼ.C0881.m5563(com.google.ʻ.ʻ.ʽ):com.google.ʻ.ʼ.ˋ");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final C0881<E> m5562(int i) {
        return m5557(C0918.m5741(m5560(), i));
    }
}
