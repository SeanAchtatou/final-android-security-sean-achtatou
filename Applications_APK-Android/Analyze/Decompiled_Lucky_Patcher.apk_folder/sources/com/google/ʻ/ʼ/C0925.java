package com.google.ʻ.ʼ;

import java.util.ListIterator;

/* renamed from: com.google.ʻ.ʼ.ﹶﹶ  reason: contains not printable characters */
/* compiled from: UnmodifiableListIterator */
public abstract class C0925<E> extends C0900<E> implements ListIterator<E> {
    protected C0925() {
    }

    @Deprecated
    public final void add(E e) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final void set(E e) {
        throw new UnsupportedOperationException();
    }
}
