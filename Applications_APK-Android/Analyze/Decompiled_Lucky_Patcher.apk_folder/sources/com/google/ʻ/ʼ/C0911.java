package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;

/* renamed from: com.google.ʻ.ʼ.ᵔᵔ  reason: contains not printable characters */
/* compiled from: SingletonImmutableSet */
final class C0911<E> extends C0904<E> {

    /* renamed from: ʻ  reason: contains not printable characters */
    final transient E f3688;

    /* renamed from: ʼ  reason: contains not printable characters */
    private transient int f3689;

    public int size() {
        return 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m5700() {
        return false;
    }

    C0911(E e) {
        this.f3688 = C0847.m5419(e);
    }

    C0911(E e, int i) {
        this.f3688 = e;
        this.f3689 = i;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: E
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean contains(java.lang.Object r2) {
        /*
            r1 = this;
            E r0 = r1.f3688
            boolean r2 = r0.equals(r2)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ʻ.ʼ.C0911.contains(java.lang.Object):boolean");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0900<E> iterator() {
        return C0920.m5757((Object) this.f3688);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public C0891<E> m5702() {
        return C0891.m5595((Object) this.f3688);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m5698(Object[] objArr, int i) {
        objArr[i] = this.f3688;
        return i + 1;
    }

    public final int hashCode() {
        int i = this.f3689;
        if (i != 0) {
            return i;
        }
        int hashCode = this.f3688.hashCode();
        this.f3689 = hashCode;
        return hashCode;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m5701() {
        return this.f3689 != 0;
    }

    public String toString() {
        return '[' + this.f3688.toString() + ']';
    }
}
