package com.google.ʻ.ʼ;

import java.io.Serializable;

/* renamed from: com.google.ʻ.ʼ.ˑ  reason: contains not printable characters */
/* compiled from: ImmutableEntry */
class C0889<K, V> extends C0857<K, V> implements Serializable {

    /* renamed from: ʻ  reason: contains not printable characters */
    final K f3661;

    /* renamed from: ʼ  reason: contains not printable characters */
    final V f3662;

    C0889(K k, V v) {
        this.f3661 = k;
        this.f3662 = v;
    }

    public final K getKey() {
        return this.f3661;
    }

    public final V getValue() {
        return this.f3662;
    }

    public final V setValue(V v) {
        throw new UnsupportedOperationException();
    }
}
