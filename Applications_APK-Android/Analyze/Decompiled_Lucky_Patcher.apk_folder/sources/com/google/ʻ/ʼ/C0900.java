package com.google.ʻ.ʼ;

import java.util.Iterator;

/* renamed from: com.google.ʻ.ʼ.ٴٴ  reason: contains not printable characters */
/* compiled from: UnmodifiableIterator */
public abstract class C0900<E> implements Iterator<E> {
    protected C0900() {
    }

    @Deprecated
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
