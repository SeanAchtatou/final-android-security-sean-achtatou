package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0842;
import com.google.ʻ.ʻ.C0847;
import com.google.ʻ.ʻ.C0848;
import java.io.Serializable;
import java.lang.Comparable;

/* renamed from: com.google.ʻ.ʼ.ʾʾ  reason: contains not printable characters */
/* compiled from: Range */
public final class C0860<C extends Comparable> extends C0898 implements C0848<C>, Serializable {

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final C0860<Comparable> f3612 = new C0860<>(C0875.m5515(), C0875.m5516());

    /* renamed from: ʻ  reason: contains not printable characters */
    final C0875<C> f3613;

    /* renamed from: ʼ  reason: contains not printable characters */
    final C0875<C> f3614;

    /* renamed from: com.google.ʻ.ʼ.ʾʾ$ʻ  reason: contains not printable characters */
    /* compiled from: Range */
    static class C0861 implements C0842<C0860, C0875> {

        /* renamed from: ʻ  reason: contains not printable characters */
        static final C0861 f3615 = new C0861();

        C0861() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0875 m5470(C0860 r1) {
            return r1.f3613;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static <C extends Comparable<?>> C0842<C0860<C>, C0875<C>> m5453() {
        return C0861.f3615;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static <C extends Comparable<?>> C0858<C0860<C>> m5457() {
        return C0862.f3616;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static <C extends Comparable<?>> C0860<C> m5454(C0875<C> r1, C0875<C> r2) {
        return new C0860<>(r1, r2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <C extends Comparable<?>> C0860<C> m5456(C c, C c2) {
        return m5454(C0875.m5517((Comparable) c), C0875.m5518(c2));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static <C extends Comparable<?>> C0860<C> m5459(C c, C c2) {
        return m5454(C0875.m5518(c), C0875.m5518(c2));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <C extends Comparable<?>> C0860<C> m5455(C c) {
        return m5454(C0875.m5515(), C0875.m5518(c));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static <C extends Comparable<?>> C0860<C> m5458(C c) {
        return m5454(C0875.m5517((Comparable) c), C0875.m5516());
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static <C extends Comparable<?>> C0860<C> m5462() {
        return f3612;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ʾʾ.ʼ(com.google.ʻ.ʼ.ˊ<?>, com.google.ʻ.ʼ.ˊ<?>):java.lang.String
     arg types: [com.google.ʻ.ʼ.ˊ<C>, com.google.ʻ.ʼ.ˊ<C>]
     candidates:
      com.google.ʻ.ʼ.ʾʾ.ʼ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C>
      com.google.ʻ.ʼ.ʾʾ.ʼ(com.google.ʻ.ʼ.ˊ<?>, com.google.ʻ.ʼ.ˊ<?>):java.lang.String */
    private C0860(C0875<C> r4, C0875<C> r5) {
        this.f3613 = (C0875) C0847.m5419(r4);
        this.f3614 = (C0875) C0847.m5419(r5);
        if (r4.compareTo((C0875) r5) > 0 || r4 == C0875.m5516() || r5 == C0875.m5515()) {
            throw new IllegalArgumentException("Invalid range: " + m5460((C0875<?>) r4, (C0875<?>) r5));
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m5467() {
        return this.f3613.equals(this.f3614);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m5466(C c) {
        C0847.m5419(c);
        return this.f3613.m5521(c) && !this.f3614.m5521(c);
    }

    @Deprecated
    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m5464(C c) {
        return m5466(c);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m5463(C0860<C> r3) {
        return this.f3613.compareTo(r3.f3614) <= 0 && r3.f3613.compareTo(this.f3614) <= 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ʾʾ.ʻ(com.google.ʻ.ʼ.ˊ, com.google.ʻ.ʼ.ˊ):com.google.ʻ.ʼ.ʾʾ<C>
     arg types: [com.google.ʻ.ʼ.ˊ<C>, com.google.ʻ.ʼ.ˊ<C>]
     candidates:
      com.google.ʻ.ʼ.ʾʾ.ʻ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C>
      com.google.ʻ.ʼ.ʾʾ.ʻ(com.google.ʻ.ʼ.ˊ, com.google.ʻ.ʼ.ˊ):com.google.ʻ.ʼ.ʾʾ<C> */
    /* renamed from: ʼ  reason: contains not printable characters */
    public C0860<C> m5465(C0860<C> r4) {
        int r0 = this.f3613.compareTo((C0875) r4.f3613);
        int r1 = this.f3614.compareTo((C0875) r4.f3614);
        if (r0 >= 0 && r1 <= 0) {
            return this;
        }
        if (r0 <= 0 && r1 >= 0) {
            return r4;
        }
        return m5454((C0875) (r0 >= 0 ? this.f3613 : r4.f3613), (C0875) (r1 <= 0 ? this.f3614 : r4.f3614));
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C0860)) {
            return false;
        }
        C0860 r4 = (C0860) obj;
        if (!this.f3613.equals(r4.f3613) || !this.f3614.equals(r4.f3614)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.f3613.hashCode() * 31) + this.f3614.hashCode();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ʾʾ.ʼ(com.google.ʻ.ʼ.ˊ<?>, com.google.ʻ.ʼ.ˊ<?>):java.lang.String
     arg types: [com.google.ʻ.ʼ.ˊ<C>, com.google.ʻ.ʼ.ˊ<C>]
     candidates:
      com.google.ʻ.ʼ.ʾʾ.ʼ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C>
      com.google.ʻ.ʼ.ʾʾ.ʼ(com.google.ʻ.ʼ.ˊ<?>, com.google.ʻ.ʼ.ˊ<?>):java.lang.String */
    public String toString() {
        return m5460((C0875<?>) this.f3613, (C0875<?>) this.f3614);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static String m5460(C0875<?> r2, C0875<?> r3) {
        StringBuilder sb = new StringBuilder(16);
        r2.m5520(sb);
        sb.append("..");
        r3.m5522(sb);
        return sb.toString();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    static int m5461(Comparable comparable, Comparable comparable2) {
        return comparable.compareTo(comparable2);
    }

    /* renamed from: com.google.ʻ.ʼ.ʾʾ$ʼ  reason: contains not printable characters */
    /* compiled from: Range */
    private static class C0862 extends C0858<C0860<?>> implements Serializable {

        /* renamed from: ʻ  reason: contains not printable characters */
        static final C0858<C0860<?>> f3616 = new C0862();

        private C0862() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int compare(C0860<?> r4, C0860<?> r5) {
            return C0872.m5499().m5503(r4.f3613, r5.f3613).m5503(r4.f3614, r5.f3614).m5504();
        }
    }
}
