package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import java.io.Serializable;

/* renamed from: com.google.ʻ.ʼ.ˎˎ  reason: contains not printable characters */
/* compiled from: ReverseOrdering */
final class C0884<T> extends C0858<T> implements Serializable {

    /* renamed from: ʻ  reason: contains not printable characters */
    final C0858<? super T> f3655;

    C0884(C0858<? super T> r1) {
        this.f3655 = (C0858) C0847.m5419(r1);
    }

    public int compare(T t, T t2) {
        return this.f3655.compare(t2, t);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public <S extends T> C0858<S> m5576() {
        return this.f3655;
    }

    public int hashCode() {
        return -this.f3655.hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof C0884) {
            return this.f3655.equals(((C0884) obj).f3655);
        }
        return false;
    }

    public String toString() {
        return this.f3655 + ".reverse()";
    }
}
