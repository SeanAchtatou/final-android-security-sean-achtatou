package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import com.google.ʻ.ʿ.C0933;
import java.io.Serializable;
import java.lang.Comparable;
import net.lingala.zip4j.util.InternalZipConstants;

/* renamed from: com.google.ʻ.ʼ.ˊ  reason: contains not printable characters */
/* compiled from: Cut */
abstract class C0875<C extends Comparable> implements Serializable, Comparable<C0875<C>> {

    /* renamed from: ʻ  reason: contains not printable characters */
    final C f3639;

    public abstract int hashCode();

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m5520(StringBuilder sb);

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract boolean m5521(Comparable comparable);

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract void m5522(StringBuilder sb);

    C0875(C c) {
        this.f3639 = c;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C0875 r3) {
        if (r3 == m5515()) {
            return 1;
        }
        if (r3 == m5516()) {
            return -1;
        }
        int r0 = C0860.m5461(this.f3639, r3.f3639);
        if (r0 != 0) {
            return r0;
        }
        return C0933.m5808(this instanceof C0877, r3 instanceof C0877);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C0875)) {
            return false;
        }
        try {
            if (compareTo((C0875) obj) == 0) {
                return true;
            }
            return false;
        } catch (ClassCastException unused) {
            return false;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static <C extends Comparable> C0875<C> m5515() {
        return C0878.f3641;
    }

    /* renamed from: com.google.ʻ.ʼ.ˊ$ʽ  reason: contains not printable characters */
    /* compiled from: Cut */
    private static final class C0878 extends C0875<Comparable<?>> {
        /* access modifiers changed from: private */

        /* renamed from: ʼ  reason: contains not printable characters */
        public static final C0878 f3641 = new C0878();

        public String toString() {
            return "-∞";
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m5532(C0875<Comparable<?>> r1) {
            return r1 == this ? 0 : -1;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m5534(Comparable<?> comparable) {
            return true;
        }

        public /* synthetic */ int compareTo(Object obj) {
            return m5532((C0875<Comparable<?>>) ((C0875) obj));
        }

        private C0878() {
            super(null);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m5533(StringBuilder sb) {
            sb.append("(-∞");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m5535(StringBuilder sb) {
            throw new AssertionError();
        }

        public int hashCode() {
            return System.identityHashCode(this);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static <C extends Comparable> C0875<C> m5516() {
        return C0876.f3640;
    }

    /* renamed from: com.google.ʻ.ʼ.ˊ$ʻ  reason: contains not printable characters */
    /* compiled from: Cut */
    private static final class C0876 extends C0875<Comparable<?>> {
        /* access modifiers changed from: private */

        /* renamed from: ʼ  reason: contains not printable characters */
        public static final C0876 f3640 = new C0876();

        public String toString() {
            return "+∞";
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m5524(C0875<Comparable<?>> r1) {
            return r1 == this ? 0 : 1;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m5526(Comparable<?> comparable) {
            return false;
        }

        public /* synthetic */ int compareTo(Object obj) {
            return m5524((C0875<Comparable<?>>) ((C0875) obj));
        }

        private C0876() {
            super(null);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m5525(StringBuilder sb) {
            throw new AssertionError();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m5527(StringBuilder sb) {
            sb.append("+∞)");
        }

        public int hashCode() {
            return System.identityHashCode(this);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static <C extends Comparable> C0875<C> m5517(Comparable comparable) {
        return new C0879(comparable);
    }

    /* renamed from: com.google.ʻ.ʼ.ˊ$ʾ  reason: contains not printable characters */
    /* compiled from: Cut */
    private static final class C0879<C extends Comparable> extends C0875<C> {
        public /* synthetic */ int compareTo(Object obj) {
            return C0875.super.compareTo((C0875) obj);
        }

        C0879(C c) {
            super((Comparable) C0847.m5419(c));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m5537(C c) {
            return C0860.m5461(this.f3639, c) <= 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m5536(StringBuilder sb) {
            sb.append('[');
            sb.append(this.f3639);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m5538(StringBuilder sb) {
            sb.append(this.f3639);
            sb.append(')');
        }

        public int hashCode() {
            return this.f3639.hashCode();
        }

        public String toString() {
            return "\\" + this.f3639 + InternalZipConstants.ZIP_FILE_SEPARATOR;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    static <C extends Comparable> C0875<C> m5518(C c) {
        return new C0877(c);
    }

    /* renamed from: com.google.ʻ.ʼ.ˊ$ʼ  reason: contains not printable characters */
    /* compiled from: Cut */
    private static final class C0877<C extends Comparable> extends C0875<C> {
        public /* synthetic */ int compareTo(Object obj) {
            return C0875.super.compareTo((C0875) obj);
        }

        C0877(C c) {
            super((Comparable) C0847.m5419(c));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m5529(C c) {
            return C0860.m5461(this.f3639, c) < 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m5528(StringBuilder sb) {
            sb.append('(');
            sb.append(this.f3639);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m5530(StringBuilder sb) {
            sb.append(this.f3639);
            sb.append(']');
        }

        public int hashCode() {
            return this.f3639.hashCode() ^ -1;
        }

        public String toString() {
            return InternalZipConstants.ZIP_FILE_SEPARATOR + this.f3639 + "\\";
        }
    }
}
