package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0842;
import java.util.Comparator;
import java.util.Map;

/* renamed from: com.google.ʻ.ʼ.ʽʽ  reason: contains not printable characters */
/* compiled from: Ordering */
public abstract class C0858<T> implements Comparator<T> {
    public abstract int compare(T t, T t2);

    /* renamed from: ʼ  reason: contains not printable characters */
    public static <C extends Comparable> C0858<C> m5447() {
        return C0906.f3680;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> C0858<T> m5446(Comparator<T> comparator) {
        return comparator instanceof C0858 ? (C0858) comparator : new C0867(comparator);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static C0858<Object> m5448() {
        return C0853.f3601;
    }

    protected C0858() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public <S extends T> C0858<S> m5449() {
        return new C0884(this);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public <F> C0858<F> m5450(C0842<F, ? extends T> r2) {
        return new C0859(r2, this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public <T2 extends T> C0858<Map.Entry<T2, ?>> m5452() {
        return m5450(C0929.m5788());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.י.ʻ(java.util.Comparator, java.lang.Iterable):com.google.ʻ.ʼ.י<E>
     arg types: [com.google.ʻ.ʼ.ʽʽ, java.lang.Iterable<E>]
     candidates:
      com.google.ʻ.ʼ.י.ʻ(java.lang.Object, java.lang.Object):com.google.ʻ.ʼ.י<E>
      com.google.ʻ.ʼ.י.ʻ(java.lang.Object[], int):int
      com.google.ʻ.ʼ.י.ʻ(int, int):com.google.ʻ.ʼ.י<E>
      com.google.ʻ.ʼ.ˏ.ʻ(java.lang.Object[], int):int
      com.google.ʻ.ʼ.י.ʻ(java.util.Comparator, java.lang.Iterable):com.google.ʻ.ʼ.י<E> */
    /* renamed from: ʻ  reason: contains not printable characters */
    public <E extends T> C0891<E> m5451(Iterable<E> iterable) {
        return C0891.m5598((Comparator) this, (Iterable) iterable);
    }
}
