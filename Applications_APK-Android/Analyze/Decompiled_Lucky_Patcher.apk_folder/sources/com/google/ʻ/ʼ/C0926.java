package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0842;
import com.google.ʻ.ʻ.C0845;
import com.google.ʻ.ʻ.C0847;
import com.google.ʻ.ʿ.C0935;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.AbstractSequentialList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

/* renamed from: com.google.ʻ.ʼ.ﾞ  reason: contains not printable characters */
/* compiled from: Lists */
public final class C0926 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> ArrayList<E> m5776() {
        return new ArrayList<>();
    }

    @SafeVarargs
    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> ArrayList<E> m5779(Object... objArr) {
        C0847.m5419(objArr);
        ArrayList<E> arrayList = new ArrayList<>(m5775(objArr.length));
        Collections.addAll(arrayList, objArr);
        return arrayList;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> ArrayList<E> m5777(Iterable iterable) {
        C0847.m5419(iterable);
        if (iterable instanceof Collection) {
            return new ArrayList<>(C0865.m5477(iterable));
        }
        return m5778(iterable.iterator());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> ArrayList<E> m5778(Iterator it) {
        ArrayList<E> r0 = m5776();
        C0920.m5761(r0, it);
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static int m5775(int i) {
        C0863.m5472(i, "arraySize");
        return C0935.m5811(((long) i) + 5 + ((long) (i / 10)));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <F, T> List<T> m5780(List list, C0842 r2) {
        return list instanceof RandomAccess ? new C0927(list, r2) : new C0928(list, r2);
    }

    /* renamed from: com.google.ʻ.ʼ.ﾞ$ʼ  reason: contains not printable characters */
    /* compiled from: Lists */
    private static class C0928<F, T> extends AbstractSequentialList<T> implements Serializable {

        /* renamed from: ʻ  reason: contains not printable characters */
        final List<F> f3729;

        /* renamed from: ʼ  reason: contains not printable characters */
        final C0842<? super F, ? extends T> f3730;

        C0928(List<F> list, C0842<? super F, ? extends T> r2) {
            this.f3729 = (List) C0847.m5419(list);
            this.f3730 = (C0842) C0847.m5419(r2);
        }

        public void clear() {
            this.f3729.clear();
        }

        public int size() {
            return this.f3729.size();
        }

        public ListIterator<T> listIterator(int i) {
            return new C0919<F, T>(this.f3729.listIterator(i)) {
                /* access modifiers changed from: package-private */
                /* renamed from: ʻ  reason: contains not printable characters */
                public T m5787(F f) {
                    return C0928.this.f3730.m5408(f);
                }
            };
        }
    }

    /* renamed from: com.google.ʻ.ʼ.ﾞ$ʻ  reason: contains not printable characters */
    /* compiled from: Lists */
    private static class C0927<F, T> extends AbstractList<T> implements Serializable, RandomAccess {

        /* renamed from: ʻ  reason: contains not printable characters */
        final List<F> f3726;

        /* renamed from: ʼ  reason: contains not printable characters */
        final C0842<? super F, ? extends T> f3727;

        C0927(List<F> list, C0842<? super F, ? extends T> r2) {
            this.f3726 = (List) C0847.m5419(list);
            this.f3727 = (C0842) C0847.m5419(r2);
        }

        public void clear() {
            this.f3726.clear();
        }

        public T get(int i) {
            return this.f3727.m5408(this.f3726.get(i));
        }

        public Iterator<T> iterator() {
            return listIterator();
        }

        public ListIterator<T> listIterator(int i) {
            return new C0919<F, T>(this.f3726.listIterator(i)) {
                /* access modifiers changed from: package-private */
                /* renamed from: ʻ  reason: contains not printable characters */
                public T m5786(F f) {
                    return C0927.this.f3727.m5408(f);
                }
            };
        }

        public boolean isEmpty() {
            return this.f3726.isEmpty();
        }

        public T remove(int i) {
            return this.f3727.m5408(this.f3726.remove(i));
        }

        public int size() {
            return this.f3726.size();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static boolean m5781(List<?> list, Object obj) {
        if (obj == C0847.m5419(list)) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        List list2 = (List) obj;
        int size = list.size();
        if (size != list2.size()) {
            return false;
        }
        if (!(list instanceof RandomAccess) || !(list2 instanceof RandomAccess)) {
            return C0920.m5763(list.iterator(), (Iterator<?>) list2.iterator());
        }
        for (int i = 0; i < size; i++) {
            if (!C0845.m5413(list.get(i), list2.get(i))) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static int m5782(List<?> list, Object obj) {
        if (list instanceof RandomAccess) {
            return m5784(list, obj);
        }
        ListIterator<?> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            if (C0845.m5413(obj, listIterator.next())) {
                return listIterator.previousIndex();
            }
        }
        return -1;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private static int m5784(List<?> list, Object obj) {
        int size = list.size();
        int i = 0;
        if (obj == null) {
            while (i < size) {
                if (list.get(i) == null) {
                    return i;
                }
                i++;
            }
            return -1;
        }
        while (i < size) {
            if (obj.equals(list.get(i))) {
                return i;
            }
            i++;
        }
        return -1;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    static int m5783(List<?> list, Object obj) {
        if (list instanceof RandomAccess) {
            return m5785(list, obj);
        }
        ListIterator<?> listIterator = list.listIterator(list.size());
        while (listIterator.hasPrevious()) {
            if (C0845.m5413(obj, listIterator.previous())) {
                return listIterator.nextIndex();
            }
        }
        return -1;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private static int m5785(List<?> list, Object obj) {
        if (obj == null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                if (list.get(size) == null) {
                    return size;
                }
            }
            return -1;
        }
        for (int size2 = list.size() - 1; size2 >= 0; size2--) {
            if (obj.equals(list.get(size2))) {
                return size2;
            }
        }
        return -1;
    }
}
