package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0845;
import java.util.Map;

/* renamed from: com.google.ʻ.ʼ.ʽ  reason: contains not printable characters */
/* compiled from: AbstractMapEntry */
abstract class C0857<K, V> implements Map.Entry<K, V> {
    public abstract K getKey();

    public abstract V getValue();

    C0857() {
    }

    public V setValue(V v) {
        throw new UnsupportedOperationException();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        if (!C0845.m5413(getKey(), entry.getKey()) || !C0845.m5413(getValue(), entry.getValue())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        Object key = getKey();
        Object value = getValue();
        int i2 = 0;
        if (key == null) {
            i = 0;
        } else {
            i = key.hashCode();
        }
        if (value != null) {
            i2 = value.hashCode();
        }
        return i ^ i2;
    }

    public String toString() {
        return getKey() + "=" + getValue();
    }
}
