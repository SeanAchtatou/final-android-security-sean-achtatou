package com.google.ʻ.ʼ;

/* renamed from: com.google.ʻ.ʼ.ˉ  reason: contains not printable characters */
/* compiled from: ComparisonChain */
public abstract class C0872 {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final C0872 f3632 = new C0872() {
        /* renamed from: ʼ  reason: contains not printable characters */
        public int m5507() {
            return 0;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0872 m5506(Comparable comparable, Comparable comparable2) {
            return m5505(comparable.compareTo(comparable2));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public C0872 m5505(int i) {
            if (i < 0) {
                return C0872.f3633;
            }
            return i > 0 ? C0872.f3634 : C0872.f3632;
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final C0872 f3633 = new C0873(-1);
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final C0872 f3634 = new C0873(1);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract C0872 m5503(Comparable<?> comparable, Comparable<?> comparable2);

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract int m5504();

    private C0872() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0872 m5499() {
        return f3632;
    }

    /* renamed from: com.google.ʻ.ʼ.ˉ$ʻ  reason: contains not printable characters */
    /* compiled from: ComparisonChain */
    private static final class C0873 extends C0872 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final int f3635;

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0872 m5508(Comparable comparable, Comparable comparable2) {
            return this;
        }

        C0873(int i) {
            super();
            this.f3635 = i;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m5509() {
            return this.f3635;
        }
    }
}
