package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import java.io.Serializable;

/* renamed from: com.google.ʻ.ʼ.ˏˏ  reason: contains not printable characters */
/* compiled from: ReverseNaturalOrdering */
final class C0888 extends C0858<Comparable> implements Serializable {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0888 f3660 = new C0888();

    public String toString() {
        return "Ordering.natural().reverse()";
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compare(Comparable comparable, Comparable comparable2) {
        C0847.m5419(comparable);
        if (comparable == comparable2) {
            return 0;
        }
        return comparable2.compareTo(comparable);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public <S extends Comparable> C0858<S> m5591() {
        return C0858.m5447();
    }

    private C0888() {
    }
}
