package com.google.ʻ.ʼ;

import java.util.Map;

/* renamed from: com.google.ʻ.ʼ.ٴ  reason: contains not printable characters */
/* compiled from: ImmutableMapEntrySet */
abstract class C0899<K, V> extends C0904<Map.Entry<K, V>> {
    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public abstract C0897<K, V> m5639();

    C0899() {
    }

    public int size() {
        return m5639().size();
    }

    public boolean contains(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        Object obj2 = m5639().get(entry.getKey());
        if (obj2 == null || !obj2.equals(entry.getValue())) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m5638() {
        return m5639().m5636();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m5640() {
        return m5639().m5637();
    }

    public int hashCode() {
        return m5639().hashCode();
    }
}
