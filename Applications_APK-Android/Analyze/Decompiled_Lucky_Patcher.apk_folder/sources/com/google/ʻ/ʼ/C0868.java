package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import java.util.AbstractMap;
import java.util.Map;

/* renamed from: com.google.ʻ.ʼ.ˈˈ  reason: contains not printable characters */
/* compiled from: RegularImmutableMap */
final class C0868<K, V> extends C0897<K, V> {

    /* renamed from: ʼ  reason: contains not printable characters */
    static final C0897<Object, Object> f3618 = new C0868(null, new Object[0], 0);

    /* renamed from: ʽ  reason: contains not printable characters */
    final transient Object[] f3619;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final transient int[] f3620;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final transient int f3621;

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m5484() {
        return false;
    }

    private C0868(int[] iArr, Object[] objArr, int i) {
        this.f3620 = iArr;
        this.f3619 = objArr;
        this.f3621 = i;
    }

    public int size() {
        return this.f3621;
    }

    public V get(Object obj) {
        return m5480(this.f3620, this.f3619, this.f3621, 0, obj);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static Object m5480(int[] iArr, Object[] objArr, int i, int i2, Object obj) {
        if (obj == null) {
            return null;
        }
        if (i == 1) {
            if (objArr[i2].equals(obj)) {
                return objArr[i2 ^ 1];
            }
            return null;
        } else if (iArr == null) {
            return null;
        } else {
            int length = iArr.length - 1;
            int r7 = C0883.m5574(obj.hashCode());
            while (true) {
                int i3 = r7 & length;
                int i4 = iArr[i3];
                if (i4 == -1) {
                    return null;
                }
                if (objArr[i4].equals(obj)) {
                    return objArr[i4 ^ 1];
                }
                r7 = i3 + 1;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public C0904<Map.Entry<K, V>> m5481() {
        return new C0869(this, this.f3619, 0, this.f3621);
    }

    /* renamed from: com.google.ʻ.ʼ.ˈˈ$ʻ  reason: contains not printable characters */
    /* compiled from: RegularImmutableMap */
    static class C0869<K, V> extends C0904<Map.Entry<K, V>> {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final transient C0897<K, V> f3622;
        /* access modifiers changed from: private */

        /* renamed from: ʼ  reason: contains not printable characters */
        public final transient Object[] f3623;
        /* access modifiers changed from: private */

        /* renamed from: ʽ  reason: contains not printable characters */
        public final transient int f3624;
        /* access modifiers changed from: private */

        /* renamed from: ʾ  reason: contains not printable characters */
        public final transient int f3625;

        /* access modifiers changed from: package-private */
        /* renamed from: ˆ  reason: contains not printable characters */
        public boolean m5490() {
            return true;
        }

        C0869(C0897<K, V> r1, Object[] objArr, int i, int i2) {
            this.f3622 = r1;
            this.f3623 = objArr;
            this.f3624 = i;
            this.f3625 = i2;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0900<Map.Entry<K, V>> iterator() {
            return m5657().iterator();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public int m5488(Object[] objArr, int i) {
            return m5657().m5604(objArr, i);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˋ  reason: contains not printable characters */
        public C0891<Map.Entry<K, V>> m5491() {
            return new C0891<Map.Entry<K, V>>() {
                /* renamed from: ˆ  reason: contains not printable characters */
                public boolean m5493() {
                    return true;
                }

                /* renamed from: ʼ  reason: contains not printable characters */
                public Map.Entry<K, V> get(int i) {
                    C0847.m5417(i, C0869.this.f3625);
                    int i2 = i * 2;
                    return new AbstractMap.SimpleImmutableEntry(C0869.this.f3623[C0869.this.f3624 + i2], C0869.this.f3623[i2 + (C0869.this.f3624 ^ 1)]);
                }

                public int size() {
                    return C0869.this.f3625;
                }
            };
        }

        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            Object key = entry.getKey();
            Object value = entry.getValue();
            if (value == null || !value.equals(this.f3622.get(key))) {
                return false;
            }
            return true;
        }

        public int size() {
            return this.f3625;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public C0904<K> m5482() {
        return new C0870(this, new C0871(this.f3619, 0, this.f3621));
    }

    /* renamed from: com.google.ʻ.ʼ.ˈˈ$ʽ  reason: contains not printable characters */
    /* compiled from: RegularImmutableMap */
    static final class C0871 extends C0891<Object> {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final transient Object[] f3629;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final transient int f3630;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final transient int f3631;

        /* access modifiers changed from: package-private */
        /* renamed from: ˆ  reason: contains not printable characters */
        public boolean m5498() {
            return true;
        }

        C0871(Object[] objArr, int i, int i2) {
            this.f3629 = objArr;
            this.f3630 = i;
            this.f3631 = i2;
        }

        public Object get(int i) {
            C0847.m5417(i, this.f3631);
            return this.f3629[(i * 2) + this.f3630];
        }

        public int size() {
            return this.f3631;
        }
    }

    /* renamed from: com.google.ʻ.ʼ.ˈˈ$ʼ  reason: contains not printable characters */
    /* compiled from: RegularImmutableMap */
    static final class C0870<K> extends C0904<K> {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final transient C0897<K, ?> f3627;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final transient C0891<K> f3628;

        /* access modifiers changed from: package-private */
        /* renamed from: ˆ  reason: contains not printable characters */
        public boolean m5497() {
            return true;
        }

        C0870(C0897<K, ?> r1, C0891<K> r2) {
            this.f3627 = r1;
            this.f3628 = r2;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0900<K> iterator() {
            return m5496().iterator();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public int m5494(Object[] objArr, int i) {
            return m5496().m5604(objArr, i);
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public C0891<K> m5496() {
            return this.f3628;
        }

        public boolean contains(Object obj) {
            return this.f3627.get(obj) != null;
        }

        public int size() {
            return this.f3627.size();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public C0885<V> m5483() {
        return new C0871(this.f3619, 1, this.f3621);
    }
}
