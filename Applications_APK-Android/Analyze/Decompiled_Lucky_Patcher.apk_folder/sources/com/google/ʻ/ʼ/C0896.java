package com.google.ʻ.ʼ;

import java.util.Comparator;

/* renamed from: com.google.ʻ.ʼ.יי  reason: contains not printable characters */
/* compiled from: SortedIterable */
interface C0896<T> extends Iterable<T> {
    Comparator<? super T> comparator();
}
