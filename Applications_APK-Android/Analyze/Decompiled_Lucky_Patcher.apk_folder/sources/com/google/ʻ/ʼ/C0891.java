package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import com.google.ʻ.ʼ.C0885;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

/* renamed from: com.google.ʻ.ʼ.י  reason: contains not printable characters */
/* compiled from: ImmutableList */
public abstract class C0891<E> extends C0885<E> implements List<E>, RandomAccess {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final C0925<Object> f3663 = new C0893(C0874.f3636, 0);

    /* renamed from: ʿ  reason: contains not printable characters */
    public final C0891<E> m5609() {
        return this;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public static <E> C0891<E> m5603() {
        return C0874.f3636;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> C0891<E> m5595(E e) {
        return m5601(e);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> C0891<E> m5596(Object obj, Object obj2) {
        return m5601(obj, obj2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> C0891<E> m5597(Collection<? extends E> collection) {
        if (!(collection instanceof C0885)) {
            return m5601(collection.toArray());
        }
        C0891<E> r1 = ((C0885) collection).m5582();
        return r1.m5583() ? m5600(r1.toArray()) : r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> C0891<E> m5599(Iterator<? extends E> it) {
        if (!it.hasNext()) {
            return m5603();
        }
        Object next = it.next();
        if (!it.hasNext()) {
            return m5595(next);
        }
        return new C0892().m5615(next).m5613((Iterator) it).m5614();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> C0891<E> m5598(Comparator comparator, Iterable iterable) {
        C0847.m5419(comparator);
        Object[] r1 = C0918.m5747(iterable);
        C0852.m5439(r1);
        Arrays.sort(r1, comparator);
        return m5600(r1);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static <E> C0891<E> m5601(Object... objArr) {
        return m5600(C0852.m5439(objArr));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static <E> C0891<E> m5600(Object[] objArr) {
        return m5602(objArr, objArr.length);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static <E> C0891<E> m5602(Object[] objArr, int i) {
        if (i == 0) {
            return m5603();
        }
        return new C0874(objArr, i);
    }

    C0891() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0900<E> iterator() {
        return listIterator();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public C0925<E> listIterator() {
        return listIterator(0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0925<E> listIterator(int i) {
        C0847.m5426(i, size());
        if (isEmpty()) {
            return f3663;
        }
        return new C0893(this, i);
    }

    /* renamed from: com.google.ʻ.ʼ.י$ʼ  reason: contains not printable characters */
    /* compiled from: ImmutableList */
    static class C0893<E> extends C0851<E> {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final C0891<E> f3664;

        C0893(C0891<E> r2, int i) {
            super(r2.size(), i);
            this.f3664 = r2;
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public E m5618(int i) {
            return this.f3664.get(i);
        }
    }

    public int indexOf(Object obj) {
        if (obj == null) {
            return -1;
        }
        return C0926.m5782(this, obj);
    }

    public int lastIndexOf(Object obj) {
        if (obj == null) {
            return -1;
        }
        return C0926.m5783(this, obj);
    }

    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0891<E> subList(int i, int i2) {
        C0847.m5421(i, i2, size());
        int i3 = i2 - i;
        if (i3 == size()) {
            return this;
        }
        if (i3 == 0) {
            return m5603();
        }
        return m5608(i, i2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public C0891<E> m5608(int i, int i2) {
        return new C0895(i, i2 - i);
    }

    /* renamed from: com.google.ʻ.ʼ.י$ʾ  reason: contains not printable characters */
    /* compiled from: ImmutableList */
    class C0895 extends C0891<E> {

        /* renamed from: ʻ  reason: contains not printable characters */
        final transient int f3666;

        /* renamed from: ʼ  reason: contains not printable characters */
        final transient int f3667;

        /* access modifiers changed from: package-private */
        /* renamed from: ˆ  reason: contains not printable characters */
        public boolean m5628() {
            return true;
        }

        public /* synthetic */ Iterator iterator() {
            return C0891.super.iterator();
        }

        public /* synthetic */ ListIterator listIterator() {
            return C0891.super.listIterator();
        }

        public /* synthetic */ ListIterator listIterator(int i) {
            return C0891.super.listIterator(i);
        }

        C0895(int i, int i2) {
            this.f3666 = i;
            this.f3667 = i2;
        }

        public int size() {
            return this.f3667;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public Object[] m5625() {
            return C0891.this.m5579();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public int m5626() {
            return C0891.this.m5580() + this.f3666;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʾ  reason: contains not printable characters */
        public int m5627() {
            return C0891.this.m5580() + this.f3666 + this.f3667;
        }

        public E get(int i) {
            C0847.m5417(i, this.f3667);
            return C0891.this.get(i + this.f3666);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0891<E> subList(int i, int i2) {
            C0847.m5421(i, i2, this.f3667);
            C0891 r0 = C0891.this;
            int i3 = this.f3666;
            return r0.subList(i + i3, i2 + i3);
        }
    }

    @Deprecated
    public final boolean addAll(int i, Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final E set(int i, E e) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final void add(int i, E e) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final E remove(int i) {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m5604(Object[] objArr, int i) {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            objArr[i + i2] = get(i2);
        }
        return i + size;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public C0891<E> m5611() {
        return size() <= 1 ? this : new C0894(this);
    }

    /* renamed from: com.google.ʻ.ʼ.י$ʽ  reason: contains not printable characters */
    /* compiled from: ImmutableList */
    private static class C0894<E> extends C0891<E> {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final transient C0891<E> f3665;

        public /* synthetic */ Iterator iterator() {
            return C0891.super.iterator();
        }

        public /* synthetic */ ListIterator listIterator() {
            return C0891.super.listIterator();
        }

        public /* synthetic */ ListIterator listIterator(int i) {
            return C0891.super.listIterator(i);
        }

        C0894(C0891<E> r1) {
            this.f3665 = r1;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private int m5619(int i) {
            return (size() - 1) - i;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        private int m5620(int i) {
            return size() - i;
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public C0891<E> m5623() {
            return this.f3665;
        }

        public boolean contains(Object obj) {
            return this.f3665.contains(obj);
        }

        public int indexOf(Object obj) {
            int lastIndexOf = this.f3665.lastIndexOf(obj);
            if (lastIndexOf >= 0) {
                return m5619(lastIndexOf);
            }
            return -1;
        }

        public int lastIndexOf(Object obj) {
            int indexOf = this.f3665.indexOf(obj);
            if (indexOf >= 0) {
                return m5619(indexOf);
            }
            return -1;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0891<E> subList(int i, int i2) {
            C0847.m5421(i, i2, size());
            return this.f3665.subList(m5620(i2), m5620(i)).m5611();
        }

        public E get(int i) {
            C0847.m5417(i, size());
            return this.f3665.get(m5619(i));
        }

        public int size() {
            return this.f3665.size();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˆ  reason: contains not printable characters */
        public boolean m5622() {
            return this.f3665.m5583();
        }
    }

    public boolean equals(Object obj) {
        return C0926.m5781(this, obj);
    }

    public int hashCode() {
        int size = size();
        int i = 1;
        for (int i2 = 0; i2 < size; i2++) {
            i = (((i * 31) + get(i2).hashCode()) ^ -1) ^ -1;
        }
        return i;
    }

    /* renamed from: com.google.ʻ.ʼ.י$ʻ  reason: contains not printable characters */
    /* compiled from: ImmutableList */
    public static final class C0892<E> extends C0885.C0886<E> {
        public C0892() {
            this(4);
        }

        C0892(int i) {
            super(i);
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public C0892<E> m5615(E e) {
            super.m5586((Object) e);
            return this;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public C0892<E> m5613(Iterator<? extends E> it) {
            super.m5588((Iterator) it);
            return this;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0891<E> m5614() {
            this.f3659 = true;
            return C0891.m5602(this.f3657, this.f3658);
        }
    }
}
