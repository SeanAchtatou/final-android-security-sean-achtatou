package com.google.ʻ.ʼ;

/* renamed from: com.google.ʻ.ʼ.ˎ  reason: contains not printable characters */
/* compiled from: Hashing */
final class C0883 {
    /* renamed from: ʻ  reason: contains not printable characters */
    static int m5574(int i) {
        return (int) (((long) Integer.rotateLeft((int) (((long) i) * -862048943), 15)) * 461845907);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static int m5575(Object obj) {
        return m5574(obj == null ? 0 : obj.hashCode());
    }
}
