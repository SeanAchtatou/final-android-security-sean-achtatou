package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;

/* renamed from: com.google.ʻ.ʼ.ˉˉ  reason: contains not printable characters */
/* compiled from: RegularImmutableList */
class C0874<E> extends C0891<E> {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0891<Object> f3636 = new C0874(new Object[0], 0);

    /* renamed from: ʼ  reason: contains not printable characters */
    final transient Object[] f3637;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final transient int f3638;

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public int m5512() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m5514() {
        return false;
    }

    C0874(Object[] objArr, int i) {
        this.f3637 = objArr;
        this.f3638 = i;
    }

    public int size() {
        return this.f3638;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public Object[] m5511() {
        return this.f3637;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public int m5513() {
        return this.f3638;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m5510(Object[] objArr, int i) {
        System.arraycopy(this.f3637, 0, objArr, i, this.f3638);
        return i + this.f3638;
    }

    public E get(int i) {
        C0847.m5417(i, this.f3638);
        return this.f3637[i];
    }
}
