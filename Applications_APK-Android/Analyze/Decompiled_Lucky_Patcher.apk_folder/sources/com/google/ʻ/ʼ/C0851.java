package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import java.util.NoSuchElementException;

/* renamed from: com.google.ʻ.ʼ.ʻ  reason: contains not printable characters */
/* compiled from: AbstractIndexedListIterator */
abstract class C0851<E> extends C0925<E> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int f3599;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f3600;

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract E m5437(int i);

    protected C0851(int i) {
        this(i, 0);
    }

    protected C0851(int i, int i2) {
        C0847.m5426(i2, i);
        this.f3599 = i;
        this.f3600 = i2;
    }

    public final boolean hasNext() {
        return this.f3600 < this.f3599;
    }

    public final E next() {
        if (hasNext()) {
            int i = this.f3600;
            this.f3600 = i + 1;
            return m5437(i);
        }
        throw new NoSuchElementException();
    }

    public final int nextIndex() {
        return this.f3600;
    }

    public final boolean hasPrevious() {
        return this.f3600 > 0;
    }

    public final E previous() {
        if (hasPrevious()) {
            int i = this.f3600 - 1;
            this.f3600 = i;
            return m5437(i);
        }
        throw new NoSuchElementException();
    }

    public final int previousIndex() {
        return this.f3600 - 1;
    }
}
