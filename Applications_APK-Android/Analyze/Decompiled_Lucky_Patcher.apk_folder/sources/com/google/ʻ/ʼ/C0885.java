package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: com.google.ʻ.ʼ.ˏ  reason: contains not printable characters */
/* compiled from: ImmutableCollection */
public abstract class C0885<E> extends AbstractCollection<E> implements Serializable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final Object[] f3656 = new Object[0];

    public abstract boolean contains(Object obj);

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract C0900<E> iterator();

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public Object[] m5579() {
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public abstract boolean m5583();

    C0885() {
    }

    public final Object[] toArray() {
        return toArray(f3656);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ʻʻ.ʻ(java.lang.Object[], int):T[]
     arg types: [T[], int]
     candidates:
      com.google.ʻ.ʼ.ʻʻ.ʻ(java.lang.Object, int):java.lang.Object
      com.google.ʻ.ʼ.ʻʻ.ʻ(java.lang.Object[], int):T[] */
    public final <T> T[] toArray(T[] tArr) {
        C0847.m5419(tArr);
        int size = size();
        if (tArr.length < size) {
            Object[] r1 = m5579();
            if (r1 != null) {
                return C0864.m5475(r1, m5580(), m5581(), tArr);
            }
            tArr = C0852.m5440((Object[]) tArr, size);
        } else if (tArr.length > size) {
            tArr[size] = null;
        }
        m5577(tArr, 0);
        return tArr;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public int m5580() {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public int m5581() {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final boolean add(E e) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final boolean addAll(Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public C0891<E> m5582() {
        return isEmpty() ? C0891.m5603() : C0891.m5600(toArray());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m5577(Object[] objArr, int i) {
        C0900 r0 = iterator();
        while (r0.hasNext()) {
            objArr[i] = r0.next();
            i++;
        }
        return i;
    }

    /* renamed from: com.google.ʻ.ʼ.ˏ$ʼ  reason: contains not printable characters */
    /* compiled from: ImmutableCollection */
    public static abstract class C0887<E> {
        /* renamed from: ʼ  reason: contains not printable characters */
        public abstract C0887<E> m5589(Object obj);

        /* renamed from: ʻ  reason: contains not printable characters */
        static int m5587(int i, int i2) {
            if (i2 >= 0) {
                int i3 = i + (i >> 1) + 1;
                if (i3 < i2) {
                    i3 = Integer.highestOneBit(i2 - 1) << 1;
                }
                if (i3 < 0) {
                    return Integer.MAX_VALUE;
                }
                return i3;
            }
            throw new AssertionError("cannot store more than MAX_VALUE elements");
        }

        C0887() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0887<E> m5588(Iterator it) {
            while (it.hasNext()) {
                m5589(it.next());
            }
            return this;
        }
    }

    /* renamed from: com.google.ʻ.ʼ.ˏ$ʻ  reason: contains not printable characters */
    /* compiled from: ImmutableCollection */
    static abstract class C0886<E> extends C0887<E> {

        /* renamed from: ʻ  reason: contains not printable characters */
        Object[] f3657;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f3658 = 0;

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean f3659;

        C0886(int i) {
            C0863.m5472(i, "initialCapacity");
            this.f3657 = new Object[i];
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m5584(int i) {
            Object[] objArr = this.f3657;
            if (objArr.length < i) {
                this.f3657 = Arrays.copyOf(objArr, m5587(objArr.length, i));
                this.f3659 = false;
            } else if (this.f3659) {
                this.f3657 = (Object[]) objArr.clone();
                this.f3659 = false;
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0886<E> m5586(Object obj) {
            C0847.m5419(obj);
            m5584(this.f3658 + 1);
            Object[] objArr = this.f3657;
            int i = this.f3658;
            this.f3658 = i + 1;
            objArr[i] = obj;
            return this;
        }
    }
}
