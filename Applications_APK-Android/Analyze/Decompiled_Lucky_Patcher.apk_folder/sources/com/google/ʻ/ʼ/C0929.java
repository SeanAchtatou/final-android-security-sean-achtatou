package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0842;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: com.google.ʻ.ʼ.ﾞﾞ  reason: contains not printable characters */
/* compiled from: Maps */
public final class C0929 {

    /* renamed from: com.google.ʻ.ʼ.ﾞﾞ$ʻ  reason: contains not printable characters */
    /* compiled from: Maps */
    private enum C0930 implements C0842<Map.Entry<?, ?>, Object> {
        KEY {
            /* renamed from: ʻ  reason: contains not printable characters */
            public /* bridge */ /* synthetic */ Object m5797(Object obj) {
                return m5798((Map.Entry<?, ?>) ((Map.Entry) obj));
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public Object m5798(Map.Entry<?, ?> entry) {
                return entry.getKey();
            }
        },
        VALUE {
            /* renamed from: ʻ  reason: contains not printable characters */
            public /* bridge */ /* synthetic */ Object m5799(Object obj) {
                return m5800((Map.Entry<?, ?>) ((Map.Entry) obj));
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public Object m5800(Map.Entry<?, ?> entry) {
                return entry.getValue();
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static <K> C0842<Map.Entry<K, ?>, K> m5788() {
        return C0930.KEY;
    }

    /* renamed from: com.google.ʻ.ʼ.ﾞﾞ$1  reason: invalid class name */
    /* compiled from: Maps */
    static class AnonymousClass1 extends C0917<Map.Entry<K, V>, K> {
        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public K m5795(Map.Entry<K, V> entry) {
            return entry.getKey();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static <K, V> HashMap<K, V> m5793() {
        return new HashMap<>();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static <K, V> LinkedHashMap<K, V> m5794() {
        return new LinkedHashMap<>();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <K, V> Map.Entry<K, V> m5791(K k, V v) {
        return new C0889(k, v);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static boolean m5792(Map<?, ?> map, Object obj) {
        if (map == obj) {
            return true;
        }
        if (obj instanceof Map) {
            return map.entrySet().equals(((Map) obj).entrySet());
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static String m5790(Map<?, ?> map) {
        StringBuilder r0 = C0865.m5476(map.size());
        r0.append('{');
        boolean z = true;
        for (Map.Entry next : map.entrySet()) {
            if (!z) {
                r0.append(", ");
            }
            z = false;
            r0.append(next.getKey());
            r0.append('=');
            r0.append(next.getValue());
        }
        r0.append('}');
        return r0.toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static <K> K m5789(Map.Entry<K, ?> entry) {
        if (entry == null) {
            return null;
        }
        return entry.getKey();
    }
}
