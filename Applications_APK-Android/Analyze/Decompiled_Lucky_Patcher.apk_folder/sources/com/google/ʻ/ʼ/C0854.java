package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import java.util.NoSuchElementException;

/* renamed from: com.google.ʻ.ʼ.ʼ  reason: contains not printable characters */
/* compiled from: AbstractIterator */
public abstract class C0854<T> extends C0900<T> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private C0855 f3602 = C0855.NOT_READY;

    /* renamed from: ʼ  reason: contains not printable characters */
    private T f3603;

    /* renamed from: com.google.ʻ.ʼ.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: AbstractIterator */
    private enum C0855 {
        READY,
        NOT_READY,
        DONE,
        FAILED
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract T m5443();

    protected C0854() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final T m5444() {
        this.f3602 = C0855.DONE;
        return null;
    }

    /* renamed from: com.google.ʻ.ʼ.ʼ$1  reason: invalid class name */
    /* compiled from: AbstractIterator */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: ʻ  reason: contains not printable characters */
        static final /* synthetic */ int[] f3604 = new int[C0855.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.google.ʻ.ʼ.ʼ$ʻ[] r0 = com.google.ʻ.ʼ.C0854.C0855.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.google.ʻ.ʼ.C0854.AnonymousClass1.f3604 = r0
                int[] r0 = com.google.ʻ.ʼ.C0854.AnonymousClass1.f3604     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.google.ʻ.ʼ.ʼ$ʻ r1 = com.google.ʻ.ʼ.C0854.C0855.DONE     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.google.ʻ.ʼ.C0854.AnonymousClass1.f3604     // Catch:{ NoSuchFieldError -> 0x001f }
                com.google.ʻ.ʼ.ʼ$ʻ r1 = com.google.ʻ.ʼ.C0854.C0855.READY     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.ʻ.ʼ.C0854.AnonymousClass1.<clinit>():void");
        }
    }

    public final boolean hasNext() {
        C0847.m5429(this.f3602 != C0855.FAILED);
        int i = AnonymousClass1.f3604[this.f3602.ordinal()];
        if (i == 1) {
            return false;
        }
        if (i != 2) {
            return m5442();
        }
        return true;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean m5442() {
        this.f3602 = C0855.FAILED;
        this.f3603 = m5443();
        if (this.f3602 == C0855.DONE) {
            return false;
        }
        this.f3602 = C0855.READY;
        return true;
    }

    public final T next() {
        if (hasNext()) {
            this.f3602 = C0855.NOT_READY;
            T t = this.f3603;
            this.f3603 = null;
            return t;
        }
        throw new NoSuchElementException();
    }
}
