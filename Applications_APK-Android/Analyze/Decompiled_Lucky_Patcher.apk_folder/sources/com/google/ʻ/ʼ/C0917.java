package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import java.util.Iterator;

/* renamed from: com.google.ʻ.ʼ.ⁱⁱ  reason: contains not printable characters */
/* compiled from: TransformedIterator */
abstract class C0917<F, T> implements Iterator<T> {

    /* renamed from: ʼ  reason: contains not printable characters */
    final Iterator<? extends F> f3702;

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract T m5738(Object obj);

    C0917(Iterator<? extends F> it) {
        this.f3702 = (Iterator) C0847.m5419(it);
    }

    public final boolean hasNext() {
        return this.f3702.hasNext();
    }

    public final T next() {
        return m5738(this.f3702.next());
    }

    public final void remove() {
        this.f3702.remove();
    }
}
