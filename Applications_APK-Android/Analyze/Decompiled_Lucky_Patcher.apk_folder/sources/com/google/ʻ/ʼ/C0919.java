package com.google.ʻ.ʼ;

import java.util.ListIterator;

/* renamed from: com.google.ʻ.ʼ.ﹳﹳ  reason: contains not printable characters */
/* compiled from: TransformedListIterator */
abstract class C0919<F, T> extends C0917<F, T> implements ListIterator<T> {
    C0919(ListIterator<? extends F> listIterator) {
        super(listIterator);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private ListIterator<? extends F> m5753() {
        return C0920.m5769(this.f3702);
    }

    public final boolean hasPrevious() {
        return m5753().hasPrevious();
    }

    public final T previous() {
        return m5738(m5753().previous());
    }

    public final int nextIndex() {
        return m5753().nextIndex();
    }

    public final int previousIndex() {
        return m5753().previousIndex();
    }

    public void set(T t) {
        throw new UnsupportedOperationException();
    }

    public void add(T t) {
        throw new UnsupportedOperationException();
    }
}
