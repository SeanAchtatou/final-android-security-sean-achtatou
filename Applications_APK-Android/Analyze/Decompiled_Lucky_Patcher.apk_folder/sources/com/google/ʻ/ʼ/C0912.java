package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.NavigableSet;

/* renamed from: com.google.ʻ.ʼ.ᵢ  reason: contains not printable characters */
/* compiled from: ImmutableSortedSet */
public abstract class C0912<E> extends C0916<E> implements C0896<E>, NavigableSet<E> {

    /* renamed from: ʻ  reason: contains not printable characters */
    final transient Comparator<? super E> f3690;

    /* renamed from: ʼ  reason: contains not printable characters */
    transient C0912<E> f3691;

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract C0900<E> iterator();

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract C0912<E> m5717(E e, boolean z, E e2, boolean z2);

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract C0912<E> m5719(E e, boolean z);

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public abstract int m5720(Object obj);

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public abstract C0912<E> m5721(E e, boolean z);

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public abstract C0912<E> m5723();

    /* renamed from: ˏ  reason: contains not printable characters */
    public abstract C0900<E> descendingIterator();

    /* renamed from: ʻ  reason: contains not printable characters */
    static <E> C0880<E> m5704(Comparator comparator) {
        if (C0858.m5447().equals(comparator)) {
            return C0880.f3642;
        }
        return new C0880<>(C0891.m5603(), comparator);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ᵢ.ʻ(java.util.Comparator, java.lang.Iterable):com.google.ʻ.ʼ.ᵢ<E>
     arg types: [com.google.ʻ.ʼ.ʽʽ, java.lang.Iterable]
     candidates:
      com.google.ʻ.ʼ.ᵢ.ʻ(java.util.Comparator, java.util.Collection):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᵢ.ʻ(java.lang.Object, java.lang.Object):int
      com.google.ʻ.ʼ.ᵢ.ʻ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᴵ.ʻ(int, java.lang.Object[]):com.google.ʻ.ʼ.ᴵ
      com.google.ʻ.ʼ.ᴵ.ʻ(int, int):boolean
      com.google.ʻ.ʼ.ˏ.ʻ(java.lang.Object[], int):int
      com.google.ʻ.ʼ.ᵢ.ʻ(java.util.Comparator, java.lang.Iterable):com.google.ʻ.ʼ.ᵢ<E> */
    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> C0912<E> m5705(Iterable iterable) {
        return m5708((Comparator) C0858.m5447(), iterable);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ᵢ.ʻ(java.util.Comparator, java.util.Collection):com.google.ʻ.ʼ.ᵢ<E>
     arg types: [com.google.ʻ.ʼ.ʽʽ, java.util.Collection]
     candidates:
      com.google.ʻ.ʼ.ᵢ.ʻ(java.util.Comparator, java.lang.Iterable):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᵢ.ʻ(java.lang.Object, java.lang.Object):int
      com.google.ʻ.ʼ.ᵢ.ʻ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᴵ.ʻ(int, java.lang.Object[]):com.google.ʻ.ʼ.ᴵ
      com.google.ʻ.ʼ.ᴵ.ʻ(int, int):boolean
      com.google.ʻ.ʼ.ˏ.ʻ(java.lang.Object[], int):int
      com.google.ʻ.ʼ.ᵢ.ʻ(java.util.Comparator, java.util.Collection):com.google.ʻ.ʼ.ᵢ<E> */
    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> C0912<E> m5706(Collection collection) {
        return m5709((Comparator) C0858.m5447(), collection);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> C0912<E> m5708(Comparator comparator, Iterable iterable) {
        C0847.m5419(comparator);
        if (C0909.m5697(comparator, iterable) && (iterable instanceof C0912)) {
            C0912<E> r0 = (C0912) iterable;
            if (!r0.m5583()) {
                return r0;
            }
        }
        Object[] r3 = C0918.m5747(iterable);
        return m5707(comparator, r3.length, r3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ᵢ.ʻ(java.util.Comparator, java.lang.Iterable):com.google.ʻ.ʼ.ᵢ<E>
     arg types: [java.util.Comparator, java.util.Collection]
     candidates:
      com.google.ʻ.ʼ.ᵢ.ʻ(java.util.Comparator, java.util.Collection):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᵢ.ʻ(java.lang.Object, java.lang.Object):int
      com.google.ʻ.ʼ.ᵢ.ʻ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᴵ.ʻ(int, java.lang.Object[]):com.google.ʻ.ʼ.ᴵ
      com.google.ʻ.ʼ.ᴵ.ʻ(int, int):boolean
      com.google.ʻ.ʼ.ˏ.ʻ(java.lang.Object[], int):int
      com.google.ʻ.ʼ.ᵢ.ʻ(java.util.Comparator, java.lang.Iterable):com.google.ʻ.ʼ.ᵢ<E> */
    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> C0912<E> m5709(Comparator comparator, Collection collection) {
        return m5708(comparator, (Iterable) collection);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static <E> C0912<E> m5707(Comparator<? super E> comparator, int i, E... eArr) {
        if (i == 0) {
            return m5704((Comparator) comparator);
        }
        C0852.m5441(eArr, i);
        Arrays.sort(eArr, 0, i, comparator);
        int i2 = 1;
        for (int i3 = 1; i3 < i; i3++) {
            E e = eArr[i3];
            if (comparator.compare(e, eArr[i2 - 1]) != 0) {
                eArr[i2] = e;
                i2++;
            }
        }
        Arrays.fill(eArr, i2, i, (Object) null);
        if (i2 < eArr.length / 2) {
            eArr = Arrays.copyOf(eArr, i2);
        }
        return new C0880(C0891.m5602(eArr, i2), comparator);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m5710(Object obj, Object obj2) {
        return m5703(this.f3690, obj, obj2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static int m5703(Comparator<?> comparator, Object obj, Object obj2) {
        return comparator.compare(obj, obj2);
    }

    C0912(Comparator<? super E> comparator) {
        this.f3690 = comparator;
    }

    public Comparator<? super E> comparator() {
        return this.f3690;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ᵢ.ʻ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E>
     arg types: [E, int]
     candidates:
      com.google.ʻ.ʼ.ᵢ.ʻ(java.util.Comparator, java.lang.Iterable):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᵢ.ʻ(java.util.Comparator, java.util.Collection):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᵢ.ʻ(java.lang.Object, java.lang.Object):int
      com.google.ʻ.ʼ.ᴵ.ʻ(int, java.lang.Object[]):com.google.ʻ.ʼ.ᴵ
      com.google.ʻ.ʼ.ᴵ.ʻ(int, int):boolean
      com.google.ʻ.ʼ.ˏ.ʻ(java.lang.Object[], int):int
      com.google.ʻ.ʼ.ᵢ.ʻ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E> */
    /* renamed from: ʼ  reason: contains not printable characters */
    public C0912<E> headSet(E e) {
        return headSet((Object) e, false);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0912<E> headSet(Object obj, boolean z) {
        return m5719(C0847.m5419(obj), z);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0912<E> subSet(Object obj, Object obj2) {
        return subSet(obj, true, obj2, false);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0912<E> subSet(E e, boolean z, E e2, boolean z2) {
        C0847.m5419(e);
        C0847.m5419(e2);
        C0847.m5422(this.f3690.compare(e, e2) <= 0);
        return m5717(e, z, e2, z2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ᵢ.ʼ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E>
     arg types: [E, int]
     candidates:
      com.google.ʻ.ʼ.ᵢ.ʼ(java.lang.Object, java.lang.Object):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᴵ.ʼ(int, java.lang.Object[]):com.google.ʻ.ʼ.ᴵ<E>
      com.google.ʻ.ʼ.ᴵ.ʼ(int, int):boolean
      com.google.ʻ.ʼ.ᵢ.ʼ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E> */
    /* renamed from: ʽ  reason: contains not printable characters */
    public C0912<E> tailSet(E e) {
        return tailSet((Object) e, true);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0912<E> tailSet(Object obj, boolean z) {
        return m5721(C0847.m5419(obj), z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ᵢ.ʻ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E>
     arg types: [E, int]
     candidates:
      com.google.ʻ.ʼ.ᵢ.ʻ(java.util.Comparator, java.lang.Iterable):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᵢ.ʻ(java.util.Comparator, java.util.Collection):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᵢ.ʻ(java.lang.Object, java.lang.Object):int
      com.google.ʻ.ʼ.ᴵ.ʻ(int, java.lang.Object[]):com.google.ʻ.ʼ.ᴵ
      com.google.ʻ.ʼ.ᴵ.ʻ(int, int):boolean
      com.google.ʻ.ʼ.ˏ.ʻ(java.lang.Object[], int):int
      com.google.ʻ.ʼ.ᵢ.ʻ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E> */
    public E lower(E e) {
        return C0920.m5758(headSet((Object) e, false).descendingIterator(), (Object) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ᵢ.ʻ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E>
     arg types: [E, int]
     candidates:
      com.google.ʻ.ʼ.ᵢ.ʻ(java.util.Comparator, java.lang.Iterable):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᵢ.ʻ(java.util.Comparator, java.util.Collection):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᵢ.ʻ(java.lang.Object, java.lang.Object):int
      com.google.ʻ.ʼ.ᴵ.ʻ(int, java.lang.Object[]):com.google.ʻ.ʼ.ᴵ
      com.google.ʻ.ʼ.ᴵ.ʻ(int, int):boolean
      com.google.ʻ.ʼ.ˏ.ʻ(java.lang.Object[], int):int
      com.google.ʻ.ʼ.ᵢ.ʻ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E> */
    public E floor(E e) {
        return C0920.m5758(headSet((Object) e, true).descendingIterator(), (Object) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ᵢ.ʼ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E>
     arg types: [E, int]
     candidates:
      com.google.ʻ.ʼ.ᵢ.ʼ(java.lang.Object, java.lang.Object):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᴵ.ʼ(int, java.lang.Object[]):com.google.ʻ.ʼ.ᴵ<E>
      com.google.ʻ.ʼ.ᴵ.ʼ(int, int):boolean
      com.google.ʻ.ʼ.ᵢ.ʼ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E> */
    public E ceiling(E e) {
        return C0918.m5744(tailSet((Object) e, true), (Object) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ᵢ.ʼ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E>
     arg types: [E, int]
     candidates:
      com.google.ʻ.ʼ.ᵢ.ʼ(java.lang.Object, java.lang.Object):com.google.ʻ.ʼ.ᵢ<E>
      com.google.ʻ.ʼ.ᴵ.ʼ(int, java.lang.Object[]):com.google.ʻ.ʼ.ᴵ<E>
      com.google.ʻ.ʼ.ᴵ.ʼ(int, int):boolean
      com.google.ʻ.ʼ.ᵢ.ʼ(java.lang.Object, boolean):com.google.ʻ.ʼ.ᵢ<E> */
    public E higher(E e) {
        return C0918.m5744(tailSet((Object) e, false), (Object) null);
    }

    public E first() {
        return iterator().next();
    }

    public E last() {
        return descendingIterator().next();
    }

    @Deprecated
    public final E pollFirst() {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final E pollLast() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public C0912<E> descendingSet() {
        C0912<E> r0 = this.f3691;
        if (r0 != null) {
            return r0;
        }
        C0912<E> r02 = m5723();
        this.f3691 = r02;
        r02.f3691 = this;
        return r02;
    }
}
