package com.google.ʻ.ʼ;

import java.io.Serializable;
import java.util.Map;

/* renamed from: com.google.ʻ.ʼ.ـ  reason: contains not printable characters */
/* compiled from: ImmutableMap */
public abstract class C0897<K, V> implements Serializable, Map<K, V> {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final Map.Entry<?, ?>[] f3669 = new Map.Entry[0];

    /* renamed from: ʼ  reason: contains not printable characters */
    private transient C0904<Map.Entry<K, V>> f3670;

    /* renamed from: ʽ  reason: contains not printable characters */
    private transient C0904<K> f3671;

    /* renamed from: ʾ  reason: contains not printable characters */
    private transient C0885<V> f3672;

    public abstract V get(Object obj);

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract C0904<Map.Entry<K, V>> m5631();

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public abstract C0904<K> m5633();

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public abstract C0885<V> m5635();

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public abstract boolean m5636();

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean m5637() {
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <K, V> C0897<K, V> m5629() {
        return C0868.f3618;
    }

    C0897() {
    }

    @Deprecated
    public final V put(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final V remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final void putAll(Map<? extends K, ? extends V> map) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public boolean containsKey(Object obj) {
        return get(obj) != null;
    }

    public boolean containsValue(Object obj) {
        return values().contains(obj);
    }

    public final V getOrDefault(Object obj, V v) {
        V v2 = get(obj);
        return v2 != null ? v2 : v;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0904<Map.Entry<K, V>> entrySet() {
        C0904<Map.Entry<K, V>> r0 = this.f3670;
        if (r0 != null) {
            return r0;
        }
        C0904<Map.Entry<K, V>> r02 = m5631();
        this.f3670 = r02;
        return r02;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public C0904<K> keySet() {
        C0904<K> r0 = this.f3671;
        if (r0 != null) {
            return r0;
        }
        C0904<K> r02 = m5633();
        this.f3671 = r02;
        return r02;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public C0885<V> values() {
        C0885<V> r0 = this.f3672;
        if (r0 != null) {
            return r0;
        }
        C0885<V> r02 = m5635();
        this.f3672 = r02;
        return r02;
    }

    public boolean equals(Object obj) {
        return C0929.m5792((Map<?, ?>) this, obj);
    }

    public int hashCode() {
        return C0890.m5592(entrySet());
    }

    public String toString() {
        return C0929.m5790(this);
    }
}
