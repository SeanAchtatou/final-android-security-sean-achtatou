package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0842;
import com.google.ʻ.ʻ.C0847;
import com.google.ʻ.ʻ.C0848;
import com.google.ʻ.ʿ.C0935;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Comparator;
import java.util.Deque;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.PriorityQueue;
import java.util.Queue;

/* renamed from: com.google.ʻ.ʼ.ﹶ  reason: contains not printable characters */
/* compiled from: Iterators */
public final class C0920 {
    /* renamed from: ʻ  reason: contains not printable characters */
    static <T> C0900<T> m5755() {
        return m5765();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static <T> C0925<T> m5765() {
        return C0921.f3713;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m5754(Iterator<?> it) {
        long j = 0;
        while (it.hasNext()) {
            it.next();
            j++;
        }
        return C0935.m5811(j);
    }

    /* JADX WARNING: Removed duplicated region for block: B:2:0x0006  */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m5763(java.util.Iterator<?> r3, java.util.Iterator<?> r4) {
        /*
        L_0x0000:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x001d
            boolean r0 = r4.hasNext()
            r1 = 0
            if (r0 != 0) goto L_0x000e
            return r1
        L_0x000e:
            java.lang.Object r0 = r3.next()
            java.lang.Object r2 = r4.next()
            boolean r0 = com.google.ʻ.ʻ.C0845.m5413(r0, r2)
            if (r0 != 0) goto L_0x0000
            return r1
        L_0x001d:
            boolean r3 = r4.hasNext()
            r3 = r3 ^ 1
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ʻ.ʼ.C0920.m5763(java.util.Iterator, java.util.Iterator):boolean");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static String m5766(Iterator<?> it) {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        boolean z = true;
        while (it.hasNext()) {
            if (!z) {
                sb.append(", ");
            }
            z = false;
            sb.append(it.next());
        }
        sb.append(']');
        return sb.toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> boolean m5761(Collection collection, Iterator it) {
        C0847.m5419(collection);
        C0847.m5419(it);
        boolean z = false;
        while (it.hasNext()) {
            z |= collection.add(it.next());
        }
        return z;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static <T> Iterator<T> m5767(Iterator<? extends Iterator<? extends T>> it) {
        return new C0922(it);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> boolean m5762(Iterator it, C0848 r1) {
        return m5764(it, r1) != -1;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static <T> int m5764(Iterator<T> it, C0848<? super T> r3) {
        C0847.m5420(r3, "predicate");
        int i = 0;
        while (it.hasNext()) {
            if (r3.m5433(it.next())) {
                return i;
            }
            i++;
        }
        return -1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <F, T> Iterator<T> m5760(Iterator it, final C0842 r2) {
        C0847.m5419(r2);
        return new C0917<F, T>(it) {
            /* access modifiers changed from: package-private */
            /* renamed from: ʻ  reason: contains not printable characters */
            public T m5770(F f) {
                return r2.m5408(f);
            }
        };
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> T m5758(Iterator it, Object obj) {
        return it.hasNext() ? it.next() : obj;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> Iterator<T> m5759(final Iterator it, final int i) {
        C0847.m5419(it);
        C0847.m5423(i >= 0, "limit is negative");
        return new Iterator<T>() {

            /* renamed from: ʽ  reason: contains not printable characters */
            private int f3710;

            public boolean hasNext() {
                return this.f3710 < i && it.hasNext();
            }

            public T next() {
                if (hasNext()) {
                    this.f3710++;
                    return it.next();
                }
                throw new NoSuchElementException();
            }

            public void remove() {
                it.remove();
            }
        };
    }

    /* renamed from: com.google.ʻ.ʼ.ﹶ$ʻ  reason: contains not printable characters */
    /* compiled from: Iterators */
    private static final class C0921<T> extends C0851<T> {

        /* renamed from: ʻ  reason: contains not printable characters */
        static final C0925<Object> f3713 = new C0921(new Object[0], 0, 0, 0);

        /* renamed from: ʼ  reason: contains not printable characters */
        private final T[] f3714;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final int f3715;

        C0921(T[] tArr, int i, int i2, int i3) {
            super(i2, i3);
            this.f3714 = tArr;
            this.f3715 = i;
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public T m5771(int i) {
            return this.f3714[this.f3715 + i];
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> C0900<T> m5757(final Object obj) {
        return new C0900<T>() {

            /* renamed from: ʻ  reason: contains not printable characters */
            boolean f3711;

            public boolean hasNext() {
                return !this.f3711;
            }

            public T next() {
                if (!this.f3711) {
                    this.f3711 = true;
                    return obj;
                }
                throw new NoSuchElementException();
            }
        };
    }

    /* renamed from: com.google.ʻ.ʼ.ﹶ$ʾ  reason: contains not printable characters */
    /* compiled from: Iterators */
    private static class C0924<E> implements C0856<E> {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final Iterator<? extends E> f3723;

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean f3724;

        /* renamed from: ʽ  reason: contains not printable characters */
        private E f3725;

        public C0924(Iterator<? extends E> it) {
            this.f3723 = (Iterator) C0847.m5419(it);
        }

        public boolean hasNext() {
            return this.f3724 || this.f3723.hasNext();
        }

        public E next() {
            if (!this.f3724) {
                return this.f3723.next();
            }
            E e = this.f3725;
            this.f3724 = false;
            this.f3725 = null;
            return e;
        }

        public void remove() {
            C0847.m5430(!this.f3724, "Can't remove after you've peeked at next");
            this.f3723.remove();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public E m5774() {
            if (!this.f3724) {
                this.f3725 = this.f3723.next();
                this.f3724 = true;
            }
            return this.f3725;
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static <T> C0856<T> m5768(Iterator<? extends T> it) {
        if (it instanceof C0924) {
            return (C0924) it;
        }
        return new C0924(it);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> C0900<T> m5756(Iterable iterable, Comparator comparator) {
        C0847.m5420(iterable, "iterators");
        C0847.m5420(comparator, "comparator");
        return new C0923(iterable, comparator);
    }

    /* renamed from: com.google.ʻ.ʼ.ﹶ$ʽ  reason: contains not printable characters */
    /* compiled from: Iterators */
    private static class C0923<T> extends C0900<T> {

        /* renamed from: ʻ  reason: contains not printable characters */
        final Queue<C0856<T>> f3720;

        public C0923(Iterable<? extends Iterator<? extends T>> iterable, final Comparator<? super T> comparator) {
            this.f3720 = new PriorityQueue(2, new Comparator<C0856<T>>() {
                /* renamed from: ʻ  reason: contains not printable characters */
                public int compare(C0856<T> r2, C0856<T> r3) {
                    return comparator.compare(r2.m5445(), r3.m5445());
                }
            });
            for (Iterator it : iterable) {
                if (it.hasNext()) {
                    this.f3720.add(C0920.m5768(it));
                }
            }
        }

        public boolean hasNext() {
            return !this.f3720.isEmpty();
        }

        public T next() {
            C0856 remove = this.f3720.remove();
            T next = remove.next();
            if (remove.hasNext()) {
                this.f3720.add(remove);
            }
            return next;
        }
    }

    /* renamed from: com.google.ʻ.ʼ.ﹶ$ʼ  reason: contains not printable characters */
    /* compiled from: Iterators */
    private static class C0922<T> implements Iterator<T> {

        /* renamed from: ʻ  reason: contains not printable characters */
        private Iterator<? extends T> f3716;

        /* renamed from: ʼ  reason: contains not printable characters */
        private Iterator<? extends T> f3717 = C0920.m5755();

        /* renamed from: ʽ  reason: contains not printable characters */
        private Iterator<? extends Iterator<? extends T>> f3718;

        /* renamed from: ʾ  reason: contains not printable characters */
        private Deque<Iterator<? extends Iterator<? extends T>>> f3719;

        C0922(Iterator<? extends Iterator<? extends T>> it) {
            this.f3718 = (Iterator) C0847.m5419(it);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private Iterator<? extends Iterator<? extends T>> m5772() {
            while (true) {
                Iterator<? extends Iterator<? extends T>> it = this.f3718;
                if (it != null && it.hasNext()) {
                    return this.f3718;
                }
                Deque<Iterator<? extends Iterator<? extends T>>> deque = this.f3719;
                if (deque == null || deque.isEmpty()) {
                    return null;
                }
                this.f3718 = this.f3719.removeFirst();
            }
        }

        public boolean hasNext() {
            while (!((Iterator) C0847.m5419(this.f3717)).hasNext()) {
                this.f3718 = m5772();
                Iterator<? extends Iterator<? extends T>> it = this.f3718;
                if (it == null) {
                    return false;
                }
                this.f3717 = (Iterator) it.next();
                Iterator<? extends T> it2 = this.f3717;
                if (it2 instanceof C0922) {
                    C0922 r0 = (C0922) it2;
                    this.f3717 = r0.f3717;
                    if (this.f3719 == null) {
                        this.f3719 = new ArrayDeque();
                    }
                    this.f3719.addFirst(this.f3718);
                    if (r0.f3719 != null) {
                        while (!r0.f3719.isEmpty()) {
                            this.f3719.addFirst(r0.f3719.removeLast());
                        }
                    }
                    this.f3718 = r0.f3718;
                }
            }
            return true;
        }

        public T next() {
            if (hasNext()) {
                Iterator<? extends T> it = this.f3717;
                this.f3716 = it;
                return it.next();
            }
            throw new NoSuchElementException();
        }

        public void remove() {
            C0863.m5473(this.f3716 != null);
            this.f3716.remove();
            this.f3716 = null;
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    static <T> ListIterator<T> m5769(Iterator<T> it) {
        return (ListIterator) it;
    }
}
