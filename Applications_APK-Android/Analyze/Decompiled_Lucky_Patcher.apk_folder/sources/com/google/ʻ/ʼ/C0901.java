package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import com.google.ʻ.ʼ.C0891;
import com.google.ʻ.ʼ.C0913;
import java.io.Serializable;
import java.lang.Comparable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.ʻ.ʼ.ᐧ  reason: contains not printable characters */
/* compiled from: ImmutableRangeMap */
public class C0901<K extends Comparable<?>, V> implements C0866<K, V>, Serializable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final C0901<Comparable<?>, Object> f3673 = new C0901<>(C0891.m5603(), C0891.m5603());

    /* renamed from: ʼ  reason: contains not printable characters */
    private final transient C0891<C0860<K>> f3674;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final transient C0891<V> f3675;

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <K extends Comparable<?>, V> C0902<K, V> m5641() {
        return new C0902<>();
    }

    /* renamed from: com.google.ʻ.ʼ.ᐧ$ʻ  reason: contains not printable characters */
    /* compiled from: ImmutableRangeMap */
    public static final class C0902<K extends Comparable<?>, V> {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final List<Map.Entry<C0860<K>, V>> f3676 = C0926.m5776();

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0902<K, V> m5645(C0860<K> r3, V v) {
            C0847.m5419(r3);
            C0847.m5419(v);
            C0847.m5424(!r3.m5467(), "Range must not be empty, but was %s", r3);
            this.f3676.add(C0929.m5791(r3, v));
            return this;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0901<K, V> m5646() {
            Collections.sort(this.f3676, C0860.m5457().m5452());
            C0891.C0892 r0 = new C0891.C0892(this.f3676.size());
            C0891.C0892 r1 = new C0891.C0892(this.f3676.size());
            for (int i = 0; i < this.f3676.size(); i++) {
                C0860 r3 = (C0860) this.f3676.get(i).getKey();
                if (i > 0) {
                    C0860 r4 = (C0860) this.f3676.get(i - 1).getKey();
                    if (r3.m5463(r4) && !r3.m5465(r4).m5467()) {
                        throw new IllegalArgumentException("Overlapping ranges: range " + r4 + " overlaps with entry " + r3);
                    }
                }
                r0.m5615(r3);
                r1.m5615(this.f3676.get(i).getValue());
            }
            return new C0901<>(r0.m5614(), r1.m5614());
        }
    }

    C0901(C0891<C0860<K>> r1, C0891<V> r2) {
        this.f3674 = r1;
        this.f3675 = r2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public V m5642(K k) {
        int r0 = C0913.m5725(this.f3674, C0860.m5453(), C0875.m5517((Comparable) k), C0913.C0915.ANY_PRESENT, C0913.C0914.NEXT_LOWER);
        if (r0 != -1 && this.f3674.get(r0).m5466(k)) {
            return this.f3675.get(r0);
        }
        return null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0897<C0860<K>, V> m5644() {
        if (this.f3674.isEmpty()) {
            return C0897.m5629();
        }
        return new C0907(new C0880(this.f3674, C0860.m5457()), this.f3675);
    }

    public int hashCode() {
        return m5644().hashCode();
    }

    public boolean equals(Object obj) {
        if (obj instanceof C0866) {
            return m5644().equals(((C0866) obj).m5479());
        }
        return false;
    }

    public String toString() {
        return m5644().toString();
    }
}
