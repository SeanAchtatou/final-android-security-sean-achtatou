package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0842;
import com.google.ʻ.ʻ.C0847;
import java.util.Comparator;
import java.util.List;
import java.util.RandomAccess;

/* renamed from: com.google.ʻ.ʼ.ᵢᵢ  reason: contains not printable characters */
/* compiled from: SortedLists */
final class C0913 {

    /* renamed from: com.google.ʻ.ʼ.ᵢᵢ$ʻ  reason: contains not printable characters */
    /* compiled from: SortedLists */
    enum C0914 {
        NEXT_LOWER {
            /* access modifiers changed from: package-private */
            /* renamed from: ʻ  reason: contains not printable characters */
            public int m5729(int i) {
                return i - 1;
            }
        },
        NEXT_HIGHER {
            /* renamed from: ʻ  reason: contains not printable characters */
            public int m5730(int i) {
                return i;
            }
        },
        INVERTED_INSERTION_INDEX {
            /* renamed from: ʻ  reason: contains not printable characters */
            public int m5731(int i) {
                return i ^ -1;
            }
        };

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract int m5728(int i);
    }

    /* renamed from: com.google.ʻ.ʼ.ᵢᵢ$ʼ  reason: contains not printable characters */
    /* compiled from: SortedLists */
    enum C0915 {
        ANY_PRESENT {
            /* access modifiers changed from: package-private */
            /* renamed from: ʻ  reason: contains not printable characters */
            public <E> int m5733(Comparator<? super E> comparator, E e, List<? extends E> list, int i) {
                return i;
            }
        },
        LAST_PRESENT {
            /* access modifiers changed from: package-private */
            /* renamed from: ʻ  reason: contains not printable characters */
            public <E> int m5734(Comparator<? super E> comparator, E e, List<? extends E> list, int i) {
                int size = list.size() - 1;
                while (i < size) {
                    int i2 = ((i + size) + 1) >>> 1;
                    if (comparator.compare(list.get(i2), e) > 0) {
                        size = i2 - 1;
                    } else {
                        i = i2;
                    }
                }
                return i;
            }
        },
        FIRST_PRESENT {
            /* access modifiers changed from: package-private */
            /* renamed from: ʻ  reason: contains not printable characters */
            public <E> int m5735(Comparator<? super E> comparator, E e, List<? extends E> list, int i) {
                int i2 = 0;
                while (i2 < i) {
                    int i3 = (i2 + i) >>> 1;
                    if (comparator.compare(list.get(i3), e) < 0) {
                        i2 = i3 + 1;
                    } else {
                        i = i3;
                    }
                }
                return i2;
            }
        },
        FIRST_AFTER {
            /* renamed from: ʻ  reason: contains not printable characters */
            public <E> int m5736(Comparator<? super E> comparator, E e, List<? extends E> list, int i) {
                return LAST_PRESENT.m5732(comparator, e, list, i) + 1;
            }
        },
        LAST_BEFORE {
            /* renamed from: ʻ  reason: contains not printable characters */
            public <E> int m5737(Comparator<? super E> comparator, E e, List<? extends E> list, int i) {
                return FIRST_PRESENT.m5732(comparator, e, list, i) - 1;
            }
        };

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract <E> int m5732(Comparator<? super E> comparator, E e, List<? extends E> list, int i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E, K extends Comparable> int m5725(List list, C0842 r7, Comparable comparable, C0915 r9, C0914 r10) {
        return m5726(list, r7, comparable, C0858.m5447(), r9, r10);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E, K> int m5726(List<E> list, C0842<? super E, K> r1, K k, Comparator<? super K> comparator, C0915 r4, C0914 r5) {
        return m5727(C0926.m5780(list, r1), k, comparator, r4, r5);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> int m5727(List list, Object obj, Comparator comparator, C0915 r7, C0914 r8) {
        C0847.m5419(comparator);
        C0847.m5419(list);
        C0847.m5419(r7);
        C0847.m5419(r8);
        if (!(list instanceof RandomAccess)) {
            list = C0926.m5777(list);
        }
        int i = 0;
        int size = list.size() - 1;
        while (i <= size) {
            int i2 = (i + size) >>> 1;
            int compare = comparator.compare(obj, list.get(i2));
            if (compare < 0) {
                size = i2 - 1;
            } else if (compare <= 0) {
                return i + r7.m5732(comparator, obj, list.subList(i, size + 1), i2 - i);
            } else {
                i = i2 + 1;
            }
        }
        return r8.m5728(i);
    }
}
