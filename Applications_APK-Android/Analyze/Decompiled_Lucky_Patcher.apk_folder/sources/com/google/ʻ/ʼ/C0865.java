package com.google.ʻ.ʼ;

import java.util.Collection;

/* renamed from: com.google.ʻ.ʼ.ˆ  reason: contains not printable characters */
/* compiled from: Collections2 */
public final class C0865 {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    /* renamed from: ʻ  reason: contains not printable characters */
    static StringBuilder m5476(int i) {
        C0863.m5472(i, "size");
        return new StringBuilder((int) Math.min(((long) i) * 8, 1073741824L));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static <T> Collection<T> m5477(Iterable iterable) {
        return (Collection) iterable;
    }
}
