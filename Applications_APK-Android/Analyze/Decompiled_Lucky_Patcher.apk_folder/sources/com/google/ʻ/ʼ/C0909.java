package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;
import java.util.Comparator;
import java.util.SortedSet;

/* renamed from: com.google.ʻ.ʼ.ᵎᵎ  reason: contains not printable characters */
/* compiled from: SortedIterables */
final class C0909 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m5697(Comparator<?> comparator, Iterable<?> iterable) {
        Comparator comparator2;
        C0847.m5419(comparator);
        C0847.m5419(iterable);
        if (iterable instanceof SortedSet) {
            comparator2 = m5696((SortedSet) iterable);
        } else if (!(iterable instanceof C0896)) {
            return false;
        } else {
            comparator2 = ((C0896) iterable).comparator();
        }
        return comparator.equals(comparator2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> Comparator<? super E> m5696(SortedSet<E> sortedSet) {
        Comparator<? super E> comparator = sortedSet.comparator();
        return comparator == null ? C0858.m5447() : comparator;
    }
}
