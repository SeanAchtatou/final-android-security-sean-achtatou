package com.google.ʻ.ʼ;

/* renamed from: com.google.ʻ.ʼ.ʻʻ  reason: contains not printable characters */
/* compiled from: ObjectArrays */
public final class C0852 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> T[] m5440(T[] tArr, int i) {
        return C0864.m5474(tArr, i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static Object[] m5439(Object... objArr) {
        return m5441(objArr, objArr.length);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static Object[] m5441(Object[] objArr, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            m5438(objArr[i2], i2);
        }
        return objArr;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static Object m5438(Object obj, int i) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException("at index " + i);
    }
}
