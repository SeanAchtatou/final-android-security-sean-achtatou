package com.google.ʻ.ʼ;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* renamed from: com.google.ʻ.ʼ.ˑˑ  reason: contains not printable characters */
/* compiled from: Sets */
public final class C0890 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static <E> HashSet<E> m5593() {
        return new HashSet<>();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static int m5592(Set<?> set) {
        Iterator<?> it = set.iterator();
        int i = 0;
        while (it.hasNext()) {
            Object next = it.next();
            i = ((i + (next != null ? next.hashCode() : 0)) ^ -1) ^ -1;
        }
        return i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static boolean m5594(Set<?> set, Object obj) {
        if (set == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set2 = (Set) obj;
            try {
                if (set.size() != set2.size() || !set.containsAll(set2)) {
                    return false;
                }
                return true;
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }
}
