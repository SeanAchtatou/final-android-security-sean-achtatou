package com.google.ʻ.ʼ;

import java.lang.Comparable;
import java.util.Map;

/* renamed from: com.google.ʻ.ʼ.ˆˆ  reason: contains not printable characters */
/* compiled from: RangeMap */
public interface C0866<K extends Comparable, V> {
    /* renamed from: ʻ  reason: contains not printable characters */
    V m5478(K k);

    /* renamed from: ʽ  reason: contains not printable characters */
    Map<C0860<K>, V> m5479();
}
