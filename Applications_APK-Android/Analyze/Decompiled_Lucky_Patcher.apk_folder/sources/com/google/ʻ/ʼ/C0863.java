package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0847;

/* renamed from: com.google.ʻ.ʼ.ʿ  reason: contains not printable characters */
/* compiled from: CollectPreconditions */
final class C0863 {
    /* renamed from: ʻ  reason: contains not printable characters */
    static int m5472(int i, String str) {
        if (i >= 0) {
            return i;
        }
        throw new IllegalArgumentException(str + " cannot be negative but was: " + i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m5473(boolean z) {
        C0847.m5430(z, "no calls to next() since the last call to remove()");
    }
}
