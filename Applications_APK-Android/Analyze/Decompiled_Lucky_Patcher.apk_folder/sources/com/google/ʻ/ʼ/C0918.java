package com.google.ʻ.ʼ;

import com.google.ʻ.ʻ.C0842;
import com.google.ʻ.ʻ.C0847;
import com.google.ʻ.ʻ.C0848;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: com.google.ʻ.ʼ.ﹳ  reason: contains not printable characters */
/* compiled from: Iterables */
public final class C0918 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m5739(Iterable<?> iterable) {
        if (iterable instanceof Collection) {
            return ((Collection) iterable).size();
        }
        return C0920.m5754(iterable.iterator());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static String m5746(Iterable<?> iterable) {
        return C0920.m5766(iterable.iterator());
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    static Object[] m5747(Iterable<?> iterable) {
        return m5750(iterable).toArray();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private static <E> Collection<E> m5750(Iterable<E> iterable) {
        if (iterable instanceof Collection) {
            return (Collection) iterable;
        }
        return C0926.m5778(iterable.iterator());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> Iterable<T> m5743(Iterable iterable, Iterable iterable2) {
        return C0881.m5558(iterable, iterable2);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static <T> Iterable<T> m5748(Iterable<? extends Iterable<? extends T>> iterable) {
        return C0881.m5561(iterable);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> boolean m5745(Iterable iterable, C0848 r1) {
        return C0920.m5762(iterable.iterator(), r1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <F, T> Iterable<T> m5742(final Iterable iterable, final C0842 r2) {
        C0847.m5419(iterable);
        C0847.m5419(r2);
        return new C0881<T>() {
            public Iterator<T> iterator() {
                return C0920.m5760(iterable.iterator(), r2);
            }
        };
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> T m5744(Iterable iterable, Object obj) {
        return C0920.m5758(iterable.iterator(), obj);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> Iterable<T> m5741(final Iterable iterable, final int i) {
        C0847.m5419(iterable);
        C0847.m5423(i >= 0, "limit is negative");
        return new C0881<T>() {
            public Iterator<T> iterator() {
                return C0920.m5759(iterable.iterator(), i);
            }
        };
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public static boolean m5749(Iterable<?> iterable) {
        if (iterable instanceof Collection) {
            return ((Collection) iterable).isEmpty();
        }
        return !iterable.iterator().hasNext();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static <T> C0842<Iterable<? extends T>, Iterator<? extends T>> m5740() {
        return new C0842<Iterable<? extends T>, Iterator<? extends T>>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public Iterator<? extends T> m5751(Iterable<? extends T> iterable) {
                return iterable.iterator();
            }
        };
    }
}
