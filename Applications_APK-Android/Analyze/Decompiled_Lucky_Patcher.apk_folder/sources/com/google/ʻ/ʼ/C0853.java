package com.google.ʻ.ʼ;

import java.io.Serializable;

/* renamed from: com.google.ʻ.ʼ.ʻʼ  reason: contains not printable characters */
/* compiled from: UsingToStringOrdering */
final class C0853 extends C0858<Object> implements Serializable {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0853 f3601 = new C0853();

    public String toString() {
        return "Ordering.usingToString()";
    }

    public int compare(Object obj, Object obj2) {
        return obj.toString().compareTo(obj2.toString());
    }

    private C0853() {
    }
}
