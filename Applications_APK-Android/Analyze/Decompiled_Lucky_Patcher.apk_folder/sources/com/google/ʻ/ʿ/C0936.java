package com.google.ʻ.ʿ;

/* renamed from: com.google.ʻ.ʿ.ʾ  reason: contains not printable characters */
/* compiled from: Longs */
public final class C0936 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m5812(long j, long j2) {
        if (j < j2) {
            return -1;
        }
        return j > j2 ? 1 : 0;
    }
}
