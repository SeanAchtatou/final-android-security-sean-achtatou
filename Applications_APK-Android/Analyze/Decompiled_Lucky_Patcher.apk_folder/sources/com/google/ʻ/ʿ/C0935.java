package com.google.ʻ.ʿ;

/* renamed from: com.google.ʻ.ʿ.ʽ  reason: contains not printable characters */
/* compiled from: Ints */
public final class C0935 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m5810(int i, int i2) {
        if (i < i2) {
            return -1;
        }
        return i > i2 ? 1 : 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m5811(long j) {
        if (j > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        if (j < -2147483648L) {
            return Integer.MIN_VALUE;
        }
        return (int) j;
    }
}
