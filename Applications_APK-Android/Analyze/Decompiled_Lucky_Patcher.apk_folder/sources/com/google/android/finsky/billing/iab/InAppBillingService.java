package com.google.android.finsky.billing.iab;

import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.android.vending.ʻ.C0773;
import com.chelpus.C0815;
import com.lp.C0987;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class InAppBillingService extends Service {

    /* renamed from: ˆ  reason: contains not printable characters */
    static ServiceConnection f3538;

    /* renamed from: ʻ  reason: contains not printable characters */
    Context f3539;

    /* renamed from: ʼ  reason: contains not printable characters */
    boolean f3540 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    boolean f3541 = false;

    /* renamed from: ʾ  reason: contains not printable characters */
    boolean f3542 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    C0773 f3543;

    /* renamed from: ˈ  reason: contains not printable characters */
    IBinder f3544 = null;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final C0773.C0774 f3545 = new C0773.C0774() {

        /* renamed from: ʻ  reason: contains not printable characters */
        final ArrayList<String> f3548 = new ArrayList<>();

        /* renamed from: ʼ  reason: contains not printable characters */
        final ArrayList<String> f3549 = new ArrayList<>();

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m5366(int i, String str, String str2) {
            C0987.m6060((Object) ("LuckyPatcher: check api " + i + " isBillingSupported"));
            InAppBillingService.this.m5364();
            return i > 10 ? 3 : 0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chelpus.ˆ.ʻ(long, long):long
         arg types: [int, int]
         candidates:
          com.chelpus.ˆ.ʻ(byte, byte):int
          com.chelpus.ˆ.ʻ(java.io.File, int):int
          com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String[]):int
          com.chelpus.ˆ.ʻ(java.lang.String, boolean):long
          com.chelpus.ˆ.ʻ(java.lang.String, int):android.content.pm.PackageInfo
          com.chelpus.ˆ.ʻ(int, java.lang.String[]):java.lang.String
          com.chelpus.ˆ.ʻ(android.content.Context, android.net.Uri):java.lang.String
          com.chelpus.ˆ.ʻ(android.content.Context, java.lang.String):java.lang.String
          com.chelpus.ˆ.ʻ(java.io.File, java.lang.String):java.lang.String
          com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String):java.lang.String
          com.chelpus.ˆ.ʻ(java.io.File, java.io.File):void
          com.chelpus.ˆ.ʻ(java.io.File, com.chelpus.ʻ.ʼ.י[]):void
          com.chelpus.ˆ.ʻ(java.io.InputStream, java.io.OutputStream):void
          com.chelpus.ˆ.ʻ(java.util.List<java.io.File>, java.util.zip.ZipOutputStream):void
          com.chelpus.ˆ.ʻ(int, java.io.File):boolean
          com.chelpus.ˆ.ʻ(boolean, boolean):boolean
          com.chelpus.ˆ.ʻ(java.lang.String[], java.lang.String):boolean
          com.chelpus.ˆ.ʻ(int, int):byte[]
          com.chelpus.ˆ.ʻ(java.io.File, boolean):float
          com.chelpus.ˆ.ʻ(java.io.File, java.util.ArrayList<java.lang.String>):com.chelpus.ˆ$ʼ
          com.chelpus.ˆ.ʻ(java.lang.String, java.io.InputStream):java.lang.String
          com.chelpus.ˆ.ʻ(boolean, com.chelpus.ˆ$ʾ):java.lang.String
          com.chelpus.ˆ.ʻ(long, long):long */
        /* JADX WARNING: Removed duplicated region for block: B:107:0x020f A[SYNTHETIC, Splitter:B:107:0x020f] */
        /* JADX WARNING: Removed duplicated region for block: B:125:0x02cc  */
        /* JADX WARNING: Removed duplicated region for block: B:97:0x01e2 A[Catch:{ JSONException -> 0x027e, Throwable -> 0x0359 }] */
        /* renamed from: ʻ  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.os.Bundle m5367(int r22, java.lang.String r23, java.lang.String r24, android.os.Bundle r25) {
            /*
                r21 = this;
                r1 = r21
                r2 = r23
                r3 = r24
                r4 = r25
                java.lang.String r5 = "inapp"
                java.lang.String r6 = "DETAILS_LIST"
                java.lang.String r7 = "ITEM_ID_LIST"
                java.lang.String r8 = "RESPONSE_CODE"
                java.lang.String r9 = "type"
                java.lang.String r10 = "productId"
                java.lang.String r0 = "LuckyPatcher: use api 3 getSkuDetails"
                com.lp.C0987.m6060(r0)
                r11 = 1
                r12 = 0
                com.google.android.finsky.billing.iab.InAppBillingService r0 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Exception -> 0x00ce }
                boolean r0 = r0.f3540     // Catch:{ Exception -> 0x00ce }
                if (r0 != 0) goto L_0x002c
                com.google.android.finsky.billing.iab.InAppBillingService r0 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Exception -> 0x00ce }
                boolean r0 = r0.f3541     // Catch:{ Exception -> 0x00ce }
                if (r0 != 0) goto L_0x002c
                com.google.android.finsky.billing.iab.InAppBillingService r0 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Exception -> 0x00ce }
                r0.m5365(r2)     // Catch:{ Exception -> 0x00ce }
            L_0x002c:
                android.os.Bundle r0 = new android.os.Bundle     // Catch:{ Exception -> 0x00ce }
                r0.<init>()     // Catch:{ Exception -> 0x00ce }
                r0.putInt(r8, r12)     // Catch:{ Exception -> 0x00ce }
                com.google.android.finsky.billing.iab.InAppBillingService r0 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Exception -> 0x00ce }
                boolean r0 = r0.f3540     // Catch:{ Exception -> 0x00ce }
                if (r0 == 0) goto L_0x00d2
                com.google.android.finsky.billing.iab.InAppBillingService r0 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Exception -> 0x00ce }
                com.android.vending.ʻ.ʻ r0 = r0.f3543     // Catch:{ Exception -> 0x00ce }
                if (r0 == 0) goto L_0x00d2
                com.google.android.finsky.billing.iab.InAppBillingService r0 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Exception -> 0x00ce }
                boolean r0 = r0.f3541     // Catch:{ Exception -> 0x00ce }
                if (r0 != 0) goto L_0x00d2
                java.lang.String r0 = "Connect to google billing"
                com.lp.C0987.m6060(r0)     // Catch:{ Exception -> 0x00c9 }
                com.google.android.finsky.billing.iab.InAppBillingService r0 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Exception -> 0x00c9 }
                com.android.vending.ʻ.ʻ r0 = r0.f3543     // Catch:{ Exception -> 0x00c9 }
                r13 = r22
                android.os.Bundle r0 = r0.m4980(r13, r2, r3, r4)     // Catch:{ Exception -> 0x00c9 }
                java.util.ArrayList r13 = r0.getStringArrayList(r6)     // Catch:{ Exception -> 0x00c9 }
                r4.getStringArrayList(r7)     // Catch:{ Exception -> 0x00c9 }
                int r14 = r0.getInt(r8)     // Catch:{ Exception -> 0x00c9 }
                java.lang.Integer r15 = java.lang.Integer.valueOf(r14)     // Catch:{ Exception -> 0x00c9 }
                com.lp.C0987.m6060(r15)     // Catch:{ Exception -> 0x00c9 }
                if (r14 == 0) goto L_0x006e
                com.google.android.finsky.billing.iab.InAppBillingService r0 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Exception -> 0x00c9 }
                r0.f3541 = r11     // Catch:{ Exception -> 0x00c9 }
                goto L_0x00d2
            L_0x006e:
                if (r13 == 0) goto L_0x00c8
                int r14 = r13.size()     // Catch:{ Exception -> 0x00c9 }
                if (r14 == 0) goto L_0x00c8
                java.util.Iterator r13 = r13.iterator()     // Catch:{ Exception -> 0x00c9 }
            L_0x007a:
                boolean r14 = r13.hasNext()     // Catch:{ Exception -> 0x00c9 }
                if (r14 == 0) goto L_0x00c8
                java.lang.Object r14 = r13.next()     // Catch:{ Exception -> 0x00c9 }
                java.lang.String r14 = (java.lang.String) r14     // Catch:{ Exception -> 0x00c9 }
                org.json.JSONObject r15 = new org.json.JSONObject     // Catch:{ Exception -> 0x00c9 }
                r15.<init>(r14)     // Catch:{ Exception -> 0x00c9 }
                java.lang.String r14 = r15.getString(r9)     // Catch:{ Exception -> 0x00c9 }
                boolean r14 = r14.equals(r5)     // Catch:{ Exception -> 0x00c9 }
                if (r14 == 0) goto L_0x00c5
                java.util.ArrayList<java.lang.String> r14 = r1.f3548     // Catch:{ Exception -> 0x00c9 }
                java.util.Iterator r14 = r14.iterator()     // Catch:{ Exception -> 0x00c9 }
                r16 = 0
            L_0x009d:
                boolean r17 = r14.hasNext()     // Catch:{ Exception -> 0x00c9 }
                if (r17 == 0) goto L_0x00ba
                java.lang.Object r17 = r14.next()     // Catch:{ Exception -> 0x00c9 }
                r12 = r17
                java.lang.String r12 = (java.lang.String) r12     // Catch:{ Exception -> 0x00c9 }
                java.lang.Object r11 = r15.get(r10)     // Catch:{ Exception -> 0x00c9 }
                boolean r11 = r11.equals(r12)     // Catch:{ Exception -> 0x00c9 }
                if (r11 == 0) goto L_0x00b7
                r16 = 1
            L_0x00b7:
                r11 = 1
                r12 = 0
                goto L_0x009d
            L_0x00ba:
                if (r16 != 0) goto L_0x00c5
                java.util.ArrayList<java.lang.String> r11 = r1.f3548     // Catch:{ Exception -> 0x00c9 }
                java.lang.String r12 = r15.getString(r10)     // Catch:{ Exception -> 0x00c9 }
                r11.add(r12)     // Catch:{ Exception -> 0x00c9 }
            L_0x00c5:
                r11 = 1
                r12 = 0
                goto L_0x007a
            L_0x00c8:
                return r0
            L_0x00c9:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ Exception -> 0x00ce }
                goto L_0x00d2
            L_0x00ce:
                r0 = move-exception
                r0.printStackTrace()
            L_0x00d2:
                java.util.ArrayList r0 = r4.getStringArrayList(r7)
                java.util.ArrayList r4 = new java.util.ArrayList
                r4.<init>()
                com.google.android.finsky.billing.iab.InAppBillingService r7 = com.google.android.finsky.billing.iab.InAppBillingService.this
                boolean r7 = r7.f3540
                if (r7 == 0) goto L_0x00f2
                com.google.android.finsky.billing.iab.InAppBillingService r7 = com.google.android.finsky.billing.iab.InAppBillingService.this
                com.android.vending.ʻ.ʻ r7 = r7.f3543
                if (r7 == 0) goto L_0x00f2
                com.google.android.finsky.billing.iab.InAppBillingService r7 = com.google.android.finsky.billing.iab.InAppBillingService.this
                boolean r7 = r7.f3541
                if (r7 == 0) goto L_0x00ee
                goto L_0x00f2
            L_0x00ee:
                r18 = r6
                goto L_0x0363
            L_0x00f2:
                java.lang.String r7 = "Dont Connect to google billing"
                com.lp.C0987.m6060(r7)
                com.google.android.finsky.billing.iab.ʻ r7 = new com.google.android.finsky.billing.iab.ʻ     // Catch:{ Throwable -> 0x035d }
                com.google.android.finsky.billing.iab.InAppBillingService r11 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Throwable -> 0x035d }
                android.content.Context r11 = r11.f3539     // Catch:{ Throwable -> 0x035d }
                r7.<init>(r11, r2)     // Catch:{ Throwable -> 0x035d }
                java.util.Iterator r2 = r0.iterator()     // Catch:{ Throwable -> 0x035d }
            L_0x0104:
                boolean r11 = r2.hasNext()     // Catch:{ Throwable -> 0x035d }
                java.lang.String r12 = "subs"
                if (r11 == 0) goto L_0x0163
                java.lang.Object r11 = r2.next()     // Catch:{ Throwable -> 0x035d }
                java.lang.String r11 = (java.lang.String) r11     // Catch:{ Throwable -> 0x035d }
                boolean r12 = r3.equals(r12)     // Catch:{ Throwable -> 0x035d }
                if (r12 == 0) goto L_0x013a
                java.util.ArrayList<java.lang.String> r12 = r1.f3549     // Catch:{ Throwable -> 0x035d }
                java.util.Iterator r12 = r12.iterator()     // Catch:{ Throwable -> 0x035d }
                r13 = 0
            L_0x011f:
                boolean r14 = r12.hasNext()     // Catch:{ Throwable -> 0x035d }
                if (r14 == 0) goto L_0x0133
                java.lang.Object r14 = r12.next()     // Catch:{ Throwable -> 0x035d }
                java.lang.String r14 = (java.lang.String) r14     // Catch:{ Throwable -> 0x035d }
                boolean r14 = r11.equals(r14)     // Catch:{ Throwable -> 0x035d }
                if (r14 == 0) goto L_0x011f
                r13 = 1
                goto L_0x011f
            L_0x0133:
                if (r13 != 0) goto L_0x013a
                java.util.ArrayList<java.lang.String> r12 = r1.f3549     // Catch:{ Throwable -> 0x035d }
                r12.add(r11)     // Catch:{ Throwable -> 0x035d }
            L_0x013a:
                boolean r12 = r3.equals(r5)     // Catch:{ Throwable -> 0x035d }
                if (r12 == 0) goto L_0x0104
                java.util.ArrayList<java.lang.String> r12 = r1.f3548     // Catch:{ Throwable -> 0x035d }
                java.util.Iterator r12 = r12.iterator()     // Catch:{ Throwable -> 0x035d }
                r13 = 0
            L_0x0147:
                boolean r14 = r12.hasNext()     // Catch:{ Throwable -> 0x035d }
                if (r14 == 0) goto L_0x015b
                java.lang.Object r14 = r12.next()     // Catch:{ Throwable -> 0x035d }
                java.lang.String r14 = (java.lang.String) r14     // Catch:{ Throwable -> 0x035d }
                boolean r14 = r11.equals(r14)     // Catch:{ Throwable -> 0x035d }
                if (r14 == 0) goto L_0x0147
                r13 = 1
                goto L_0x0147
            L_0x015b:
                if (r13 != 0) goto L_0x0104
                java.util.ArrayList<java.lang.String> r12 = r1.f3548     // Catch:{ Throwable -> 0x035d }
                r12.add(r11)     // Catch:{ Throwable -> 0x035d }
                goto L_0x0104
            L_0x0163:
                java.util.Iterator r2 = r0.iterator()     // Catch:{ Throwable -> 0x035d }
            L_0x0167:
                boolean r0 = r2.hasNext()     // Catch:{ Throwable -> 0x035d }
                if (r0 == 0) goto L_0x00ee
                java.lang.Object r0 = r2.next()     // Catch:{ Throwable -> 0x035d }
                java.lang.String r0 = (java.lang.String) r0     // Catch:{ Throwable -> 0x035d }
                java.lang.String r5 = "\\."
                java.lang.String[] r5 = r0.split(r5)     // Catch:{ Throwable -> 0x035d }
                java.lang.String r11 = ""
                java.lang.String r13 = "_"
                java.lang.String r15 = " "
                if (r5 == 0) goto L_0x01b1
                int r14 = r5.length     // Catch:{ Throwable -> 0x035d }
                if (r14 == 0) goto L_0x01b1
                int r14 = r5.length     // Catch:{ Throwable -> 0x035d }
                r23 = r2
                r2 = 2
                if (r14 < r2) goto L_0x01a5
                java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x035d }
                r11.<init>()     // Catch:{ Throwable -> 0x035d }
                int r14 = r5.length     // Catch:{ Throwable -> 0x035d }
                int r14 = r14 - r2
                r2 = r5[r14]     // Catch:{ Throwable -> 0x035d }
                r11.append(r2)     // Catch:{ Throwable -> 0x035d }
                r11.append(r15)     // Catch:{ Throwable -> 0x035d }
                int r2 = r5.length     // Catch:{ Throwable -> 0x035d }
                r14 = 1
                int r2 = r2 - r14
                r2 = r5[r2]     // Catch:{ Throwable -> 0x035d }
                r11.append(r2)     // Catch:{ Throwable -> 0x035d }
                java.lang.String r11 = r11.toString()     // Catch:{ Throwable -> 0x035d }
            L_0x01a5:
                int r2 = r5.length     // Catch:{ Throwable -> 0x035d }
                r14 = 1
                if (r2 != r14) goto L_0x01b8
                r2 = 0
                r5 = r5[r2]     // Catch:{ Throwable -> 0x035d }
                java.lang.String r11 = r5.replaceAll(r13, r15)     // Catch:{ Throwable -> 0x035d }
                goto L_0x01b8
            L_0x01b1:
                r23 = r2
                r14 = 1
                java.lang.String r11 = r0.replaceAll(r13, r15)     // Catch:{ Throwable -> 0x035d }
            L_0x01b8:
                r25 = r15
                r14 = 0
                r2 = r6
                r5 = 99
                long r5 = com.chelpus.C0815.m5132(r14, r5)     // Catch:{ Throwable -> 0x0359 }
                java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0359 }
                r13.<init>()     // Catch:{ Throwable -> 0x0359 }
                java.lang.String r14 = "0."
                r13.append(r14)     // Catch:{ Throwable -> 0x0359 }
                r13.append(r5)     // Catch:{ Throwable -> 0x0359 }
                java.lang.String r13 = r13.toString()     // Catch:{ Throwable -> 0x0359 }
                java.util.ArrayList r14 = r7.m5381()     // Catch:{ Throwable -> 0x0359 }
                java.util.Iterator r14 = r14.iterator()     // Catch:{ Throwable -> 0x0359 }
            L_0x01dc:
                boolean r15 = r14.hasNext()     // Catch:{ Throwable -> 0x0359 }
                if (r15 == 0) goto L_0x01ff
                java.lang.Object r15 = r14.next()     // Catch:{ Throwable -> 0x0359 }
                com.google.android.finsky.billing.iab.ʼ r15 = (com.google.android.finsky.billing.iab.C0824) r15     // Catch:{ Throwable -> 0x0359 }
                r16 = r7
                java.lang.String r7 = r15.f3560     // Catch:{ Throwable -> 0x0359 }
                boolean r7 = r7.equals(r0)     // Catch:{ Throwable -> 0x0359 }
                if (r7 == 0) goto L_0x01fb
                int r7 = r15.f3563     // Catch:{ Throwable -> 0x0359 }
                r15 = 2
                if (r7 != r15) goto L_0x01fc
                java.lang.String r7 = "Purchased"
                r13 = r7
                goto L_0x01fc
            L_0x01fb:
                r15 = 2
            L_0x01fc:
                r7 = r16
                goto L_0x01dc
            L_0x01ff:
                r16 = r7
                boolean r7 = r3.equals(r12)     // Catch:{ Throwable -> 0x0359 }
                java.lang.String r14 = "title"
                java.lang.String r15 = "return:"
                r22 = r12
                java.lang.String r12 = "price"
                if (r7 == 0) goto L_0x02cc
                java.util.ArrayList<java.lang.String> r7 = r1.f3548     // Catch:{ Throwable -> 0x0359 }
                java.util.Iterator r7 = r7.iterator()     // Catch:{ Throwable -> 0x0359 }
                r18 = 0
            L_0x0217:
                boolean r19 = r7.hasNext()     // Catch:{ Throwable -> 0x0359 }
                if (r19 == 0) goto L_0x0232
                java.lang.Object r19 = r7.next()     // Catch:{ Throwable -> 0x0359 }
                r20 = r7
                r7 = r19
                java.lang.String r7 = (java.lang.String) r7     // Catch:{ Throwable -> 0x0359 }
                boolean r7 = r7.equals(r0)     // Catch:{ Throwable -> 0x0359 }
                if (r7 == 0) goto L_0x022f
                r18 = 1
            L_0x022f:
                r7 = r20
                goto L_0x0217
            L_0x0232:
                if (r18 != 0) goto L_0x02ac
                org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0359 }
                r7.<init>()     // Catch:{ Throwable -> 0x0359 }
                r7.put(r10, r0)     // Catch:{ JSONException -> 0x027e }
                r7.put(r9, r3)     // Catch:{ JSONException -> 0x027e }
                r7.put(r12, r13)     // Catch:{ JSONException -> 0x027e }
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x027e }
                r0.<init>()     // Catch:{ JSONException -> 0x027e }
                r0.append(r11)     // Catch:{ JSONException -> 0x027e }
                r12 = r25
                r0.append(r12)     // Catch:{ JSONException -> 0x027e }
                java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x027e }
                r7.put(r14, r0)     // Catch:{ JSONException -> 0x027e }
                java.lang.String r0 = "description"
                java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x027e }
                r12.<init>()     // Catch:{ JSONException -> 0x027e }
                r12.append(r11)     // Catch:{ JSONException -> 0x027e }
                java.lang.String r11 = "  "
                r12.append(r11)     // Catch:{ JSONException -> 0x027e }
                java.lang.String r11 = r12.toString()     // Catch:{ JSONException -> 0x027e }
                r7.put(r0, r11)     // Catch:{ JSONException -> 0x027e }
                java.lang.String r0 = "price_amount_micros"
                r11 = 1000000(0xf4240, double:4.940656E-318)
                long r5 = r5 * r11
                r7.put(r0, r5)     // Catch:{ JSONException -> 0x027e }
                java.lang.String r0 = "price_currency_code"
                java.lang.String r5 = "USD"
                r7.put(r0, r5)     // Catch:{ JSONException -> 0x027e }
                goto L_0x0282
            L_0x027e:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ Throwable -> 0x0359 }
            L_0x0282:
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0359 }
                r0.<init>()     // Catch:{ Throwable -> 0x0359 }
                r0.append(r15)     // Catch:{ Throwable -> 0x0359 }
                java.lang.String r5 = r7.getString(r10)     // Catch:{ Throwable -> 0x0359 }
                r0.append(r5)     // Catch:{ Throwable -> 0x0359 }
                java.lang.String r5 = " type:"
                r0.append(r5)     // Catch:{ Throwable -> 0x0359 }
                java.lang.String r5 = r7.getString(r9)     // Catch:{ Throwable -> 0x0359 }
                r0.append(r5)     // Catch:{ Throwable -> 0x0359 }
                java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0359 }
                com.lp.C0987.m6060(r0)     // Catch:{ Throwable -> 0x0359 }
                java.lang.String r0 = r7.toString()     // Catch:{ Throwable -> 0x0359 }
                r4.add(r0)     // Catch:{ Throwable -> 0x0359 }
                goto L_0x02c8
            L_0x02ac:
                r12 = r25
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0359 }
                r5.<init>()     // Catch:{ Throwable -> 0x0359 }
                java.lang.String r6 = "skip "
                r5.append(r6)     // Catch:{ Throwable -> 0x0359 }
                r5.append(r0)     // Catch:{ Throwable -> 0x0359 }
                r5.append(r12)     // Catch:{ Throwable -> 0x0359 }
                r5.append(r3)     // Catch:{ Throwable -> 0x0359 }
                java.lang.String r0 = r5.toString()     // Catch:{ Throwable -> 0x0359 }
                com.lp.C0987.m6060(r0)     // Catch:{ Throwable -> 0x0359 }
            L_0x02c8:
                r18 = r2
                goto L_0x034d
            L_0x02cc:
                r7 = r25
                r18 = r2
                java.util.ArrayList<java.lang.String> r2 = r1.f3548     // Catch:{ Throwable -> 0x0357 }
                boolean r2 = r2.contains(r0)     // Catch:{ Throwable -> 0x0357 }
                if (r2 == 0) goto L_0x034d
                org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0357 }
                r2.<init>()     // Catch:{ Throwable -> 0x0357 }
                r2.put(r10, r0)     // Catch:{ JSONException -> 0x0320 }
                r2.put(r9, r3)     // Catch:{ JSONException -> 0x0320 }
                r2.put(r12, r13)     // Catch:{ JSONException -> 0x0320 }
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0320 }
                r0.<init>()     // Catch:{ JSONException -> 0x0320 }
                r0.append(r11)     // Catch:{ JSONException -> 0x0320 }
                r0.append(r7)     // Catch:{ JSONException -> 0x0320 }
                java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x0320 }
                r2.put(r14, r0)     // Catch:{ JSONException -> 0x0320 }
                java.lang.String r0 = "description"
                java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0320 }
                r7.<init>()     // Catch:{ JSONException -> 0x0320 }
                r7.append(r11)     // Catch:{ JSONException -> 0x0320 }
                java.lang.String r11 = "  "
                r7.append(r11)     // Catch:{ JSONException -> 0x0320 }
                java.lang.String r7 = r7.toString()     // Catch:{ JSONException -> 0x0320 }
                r2.put(r0, r7)     // Catch:{ JSONException -> 0x0320 }
                java.lang.String r0 = "price_amount_micros"
                r11 = 1000000(0xf4240, double:4.940656E-318)
                long r5 = r5 * r11
                r2.put(r0, r5)     // Catch:{ JSONException -> 0x0320 }
                java.lang.String r0 = "price_currency_code"
                java.lang.String r5 = "USD"
                r2.put(r0, r5)     // Catch:{ JSONException -> 0x0320 }
                goto L_0x0324
            L_0x0320:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ Throwable -> 0x0357 }
            L_0x0324:
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0357 }
                r0.<init>()     // Catch:{ Throwable -> 0x0357 }
                r0.append(r15)     // Catch:{ Throwable -> 0x0357 }
                java.lang.String r5 = r2.getString(r10)     // Catch:{ Throwable -> 0x0357 }
                r0.append(r5)     // Catch:{ Throwable -> 0x0357 }
                java.lang.String r5 = " type:"
                r0.append(r5)     // Catch:{ Throwable -> 0x0357 }
                java.lang.String r5 = r2.getString(r9)     // Catch:{ Throwable -> 0x0357 }
                r0.append(r5)     // Catch:{ Throwable -> 0x0357 }
                java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0357 }
                com.lp.C0987.m6060(r0)     // Catch:{ Throwable -> 0x0357 }
                java.lang.String r0 = r2.toString()     // Catch:{ Throwable -> 0x0357 }
                r4.add(r0)     // Catch:{ Throwable -> 0x0357 }
            L_0x034d:
                r12 = r22
                r2 = r23
                r7 = r16
                r6 = r18
                goto L_0x0167
            L_0x0357:
                r0 = move-exception
                goto L_0x0360
            L_0x0359:
                r0 = move-exception
                r18 = r2
                goto L_0x0360
            L_0x035d:
                r0 = move-exception
                r18 = r6
            L_0x0360:
                r0.printStackTrace()
            L_0x0363:
                android.os.Bundle r0 = new android.os.Bundle
                r0.<init>()
                r2 = 0
                r0.putInt(r8, r2)
                r2 = r18
                r0.putStringArrayList(r2, r4)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.finsky.billing.iab.InAppBillingService.AnonymousClass2.m5367(int, java.lang.String, java.lang.String, android.os.Bundle):android.os.Bundle");
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Bundle m5371(int i, String str, String str2, String str3, String str4) {
            C0987.m6060((Object) "LuckyPatcher: use api 3 getBuyIntent");
            Bundle bundle = new Bundle();
            bundle.putInt("RESPONSE_CODE", 0);
            Intent intent = new Intent();
            intent.setClass(InAppBillingService.this.getApplicationContext(), BuyActivity.class);
            intent.setAction("com.google.android.finsky.billing.iab.BUY");
            intent.addFlags(3);
            intent.putExtra("packageName", str);
            intent.putExtra("product", str2);
            intent.putExtra("payload", str4);
            intent.putExtra("Type", str3);
            ArrayList<C0824> r6 = new C0821(InAppBillingService.this.f3539, str).m5381();
            if (r6.size() != 0) {
                Iterator<C0824> it = r6.iterator();
                while (it.hasNext()) {
                    C0824 next = it.next();
                    if (next.f3564 == 1 && next.f3560.equals(str2)) {
                        intent.putExtra("autorepeat", next.f3562);
                    }
                }
            }
            bundle.putParcelable("BUY_INTENT", PendingIntent.getActivity(InAppBillingService.this.getApplicationContext(), 0, intent, 134217728));
            return bundle;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Bundle m5369(int i, String str, String str2, String str3) {
            C0987.m6060((Object) "LuckyPatcher: use api 3 getPurchases");
            Bundle bundle = new Bundle();
            try {
                C0821 r9 = new C0821(InAppBillingService.this.f3539, str);
                ArrayList<C0824> r8 = r9.m5381();
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                ArrayList arrayList3 = new ArrayList();
                if (r8.size() != 0) {
                    Iterator<C0824> it = r8.iterator();
                    while (it.hasNext()) {
                        C0824 next = it.next();
                        if (next.f3563 == 2 || next.f3563 == 1) {
                            arrayList.add(next.f3560);
                            C0987.m6060((Object) next.f3560);
                            C0987.m6060((Object) next.f3561);
                            C0987.m6060((Object) next.f3562);
                            arrayList2.add(next.f3561);
                            if (next.f3562.equals("1")) {
                                next.f3562 = C0815.m5200(next.f3561);
                                arrayList3.add(next.f3562);
                                r9.m5382(next);
                            } else {
                                arrayList3.add(next.f3562);
                            }
                            if (next.f3563 == 1) {
                                if (next.f3564 == 1) {
                                    next.f3563 = 3;
                                    r9.m5382(next);
                                } else {
                                    r9.m5383(next);
                                }
                            }
                        }
                    }
                }
                bundle.putInt("RESPONSE_CODE", 0);
                bundle.putStringArrayList("INAPP_PURCHASE_ITEM_LIST", arrayList);
                bundle.putStringArrayList("INAPP_PURCHASE_DATA_LIST", arrayList2);
                bundle.putStringArrayList("INAPP_DATA_SIGNATURE_LIST", arrayList3);
            } catch (Throwable th) {
                th.printStackTrace();
            }
            return bundle;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m5374(int i, String str, String str2) {
            Log.d("BillingHack", "consumePurchase");
            try {
                C0821 r4 = new C0821(InAppBillingService.this.f3539, str);
                Iterator<C0824> it = r4.m5381().iterator();
                while (it.hasNext()) {
                    C0824 next = it.next();
                    if (next.f3563 == 1) {
                        if (next.f3564 == 1) {
                            next.f3563 = 3;
                            r4.m5382(next);
                        } else if (new JSONObject(next.f3561).get("purchaseToken").equals(str2)) {
                            r4.m5383(next);
                            return 0;
                        }
                    }
                }
                return 0;
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Throwable th) {
                th.printStackTrace();
                return 0;
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public int m5377(int i, String str, String str2) {
            C0987.m6060((Object) "LuckyPatcher: use api 6 stub");
            return 0;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Bundle m5373(int i, String str, List<String> list, String str2, String str3, String str4) {
            C0987.m6060((Object) "LuckyPatcher: use api 5 getBuyIntentToReplaceSkus");
            Bundle bundle = new Bundle();
            bundle.putInt("RESPONSE_CODE", 0);
            Intent intent = new Intent();
            intent.setClass(InAppBillingService.this.getApplicationContext(), BuyActivity.class);
            intent.setAction("com.google.android.finsky.billing.iab.BUY");
            intent.addFlags(3);
            intent.putExtra("packageName", str);
            intent.putExtra("product", str2);
            intent.putExtra("payload", str4);
            intent.putExtra("Type", str3);
            ArrayList<C0824> r5 = new C0821(InAppBillingService.this.f3539, str).m5381();
            if (r5.size() != 0) {
                Iterator<C0824> it = r5.iterator();
                while (it.hasNext()) {
                    C0824 next = it.next();
                    if (next.f3564 == 1 && next.f3560.equals(str2)) {
                        intent.putExtra("autorepeat", next.f3562);
                    }
                }
            }
            bundle.putParcelable("BUY_INTENT", PendingIntent.getActivity(InAppBillingService.this.getApplicationContext(), 0, intent, 134217728));
            return bundle;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Bundle m5372(int i, String str, String str2, String str3, String str4, Bundle bundle) {
            C0987.m6060((Object) "LuckyPatcher: use api 6 getBuyIntentExtraParams");
            Bundle bundle2 = new Bundle();
            try {
                bundle2.putInt("RESPONSE_CODE", 0);
                Intent intent = new Intent();
                intent.setClass(InAppBillingService.this.getApplicationContext(), BuyActivity.class);
                intent.setAction("com.google.android.finsky.billing.iab.BUY");
                intent.addFlags(3);
                intent.putExtra("packageName", str);
                intent.putExtra("product", str2);
                intent.putExtra("payload", str4);
                intent.putExtra("Type", str3);
                ArrayList<C0824> r5 = new C0821(InAppBillingService.this.f3539, str).m5381();
                if (r5.size() != 0) {
                    Iterator<C0824> it = r5.iterator();
                    while (it.hasNext()) {
                        C0824 next = it.next();
                        if (next.f3564 == 1 && next.f3560.equals(str2)) {
                            intent.putExtra("autorepeat", next.f3562);
                        }
                    }
                }
                bundle2.putParcelable("BUY_INTENT", PendingIntent.getActivity(InAppBillingService.this.getApplicationContext(), 0, intent, 134217728));
            } catch (Throwable th) {
                th.printStackTrace();
            }
            return bundle2;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Bundle m5370(int i, String str, String str2, String str3, Bundle bundle) {
            C0987.m6060((Object) "LuckyPatcher: use api 6 getBuyIntentExtraParams");
            Bundle bundle2 = new Bundle();
            try {
                C0821 r8 = new C0821(InAppBillingService.this.f3539, str);
                ArrayList<C0824> r7 = r8.m5381();
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                ArrayList arrayList3 = new ArrayList();
                if (r7.size() != 0) {
                    Iterator<C0824> it = r7.iterator();
                    while (it.hasNext()) {
                        C0824 next = it.next();
                        if (next.f3563 == 2 || next.f3563 == 1) {
                            arrayList.add(next.f3560);
                            C0987.m6060((Object) next.f3560);
                            C0987.m6060((Object) next.f3561);
                            C0987.m6060((Object) next.f3562);
                            arrayList2.add(next.f3561);
                            if (next.f3562.equals("1")) {
                                next.f3562 = C0815.m5200(next.f3561);
                                arrayList3.add(next.f3562);
                                r8.m5382(next);
                            } else {
                                arrayList3.add(next.f3562);
                            }
                            if (next.f3563 == 1) {
                                if (next.f3564 == 1) {
                                    next.f3563 = 3;
                                    r8.m5382(next);
                                } else {
                                    r8.m5383(next);
                                }
                            }
                        }
                    }
                }
                bundle2.putInt("RESPONSE_CODE", 0);
                bundle2.putStringArrayList("INAPP_PURCHASE_ITEM_LIST", arrayList);
                bundle2.putStringArrayList("INAPP_PURCHASE_DATA_LIST", arrayList2);
                bundle2.putStringArrayList("INAPP_DATA_SIGNATURE_LIST", arrayList3);
            } catch (Throwable th) {
                th.printStackTrace();
            }
            return bundle2;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m5375(int i, String str, String str2, Bundle bundle) {
            C0987.m6060((Object) ("LuckyPatcher: check api " + i + " isBillingSupportedExtraParams"));
            InAppBillingService.this.m5364();
            return i > 7 ? 3 : 0;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public Bundle m5376(int i, String str, String str2, String str3, Bundle bundle) {
            C0987.m6060((Object) ("LuckyPatcher: check api " + i + " getSubscriptionManagementIntent"));
            Bundle bundle2 = new Bundle();
            try {
                bundle2.putInt("RESPONSE_CODE", 0);
                Intent intent = new Intent();
                intent.setClass(InAppBillingService.this.getApplicationContext(), BuyActivity.class);
                intent.setAction("com.google.android.finsky.billing.iab.BUY");
                intent.addFlags(3);
                intent.putExtra("packageName", str);
                intent.putExtra("product", str2);
                intent.putExtra("Type", str3);
                ArrayList<C0824> r6 = new C0821(InAppBillingService.this.f3539, str).m5381();
                if (r6.size() != 0) {
                    Iterator<C0824> it = r6.iterator();
                    while (it.hasNext()) {
                        C0824 next = it.next();
                        if (next.f3564 == 1 && next.f3560.equals(str2)) {
                            intent.putExtra("autorepeat", next.f3562);
                        }
                    }
                }
                bundle2.putParcelable("SUBS_MANAGEMENT_INTENT", PendingIntent.getActivity(InAppBillingService.this.getApplicationContext(), 0, intent, 134217728));
                bundle2.putInt("RESPONSE_CODE", 0);
            } catch (Throwable th) {
                th.printStackTrace();
            }
            return bundle2;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public Bundle m5379(int i, String str, String str2, String str3, Bundle bundle) {
            C0987.m6060((Object) ("LuckyPatcher: check api " + i + " getPurchasesExtraParams"));
            Bundle bundle2 = new Bundle();
            try {
                return m5369(i, str, str2, str3);
            } catch (Throwable th) {
                th.printStackTrace();
                return bundle2;
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public Bundle m5378(int i, String str, String str2, Bundle bundle) {
            C0987.m6060((Object) ("LuckyPatcher: check api " + i + " consumePurchaseExtraParams"));
            Bundle bundle2 = new Bundle();
            try {
                C0821 r7 = new C0821(InAppBillingService.this.f3539, str);
                Iterator<C0824> it = r7.m5381().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    C0824 next = it.next();
                    if (next.f3563 == 1) {
                        if (next.f3564 == 1) {
                            next.f3563 = 3;
                            r7.m5382(next);
                        } else if (new JSONObject(next.f3561).get("purchaseToken").equals(str2)) {
                            r7.m5383(next);
                            break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Throwable th) {
                try {
                    th.printStackTrace();
                } catch (Throwable th2) {
                    th2.printStackTrace();
                }
            }
            bundle2.putInt("RESPONSE_CODE", 0);
            return bundle2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chelpus.ˆ.ʻ(long, long):long
         arg types: [int, int]
         candidates:
          com.chelpus.ˆ.ʻ(byte, byte):int
          com.chelpus.ˆ.ʻ(java.io.File, int):int
          com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String[]):int
          com.chelpus.ˆ.ʻ(java.lang.String, boolean):long
          com.chelpus.ˆ.ʻ(java.lang.String, int):android.content.pm.PackageInfo
          com.chelpus.ˆ.ʻ(int, java.lang.String[]):java.lang.String
          com.chelpus.ˆ.ʻ(android.content.Context, android.net.Uri):java.lang.String
          com.chelpus.ˆ.ʻ(android.content.Context, java.lang.String):java.lang.String
          com.chelpus.ˆ.ʻ(java.io.File, java.lang.String):java.lang.String
          com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String):java.lang.String
          com.chelpus.ˆ.ʻ(java.io.File, java.io.File):void
          com.chelpus.ˆ.ʻ(java.io.File, com.chelpus.ʻ.ʼ.י[]):void
          com.chelpus.ˆ.ʻ(java.io.InputStream, java.io.OutputStream):void
          com.chelpus.ˆ.ʻ(java.util.List<java.io.File>, java.util.zip.ZipOutputStream):void
          com.chelpus.ˆ.ʻ(int, java.io.File):boolean
          com.chelpus.ˆ.ʻ(boolean, boolean):boolean
          com.chelpus.ˆ.ʻ(java.lang.String[], java.lang.String):boolean
          com.chelpus.ˆ.ʻ(int, int):byte[]
          com.chelpus.ˆ.ʻ(java.io.File, boolean):float
          com.chelpus.ˆ.ʻ(java.io.File, java.util.ArrayList<java.lang.String>):com.chelpus.ˆ$ʼ
          com.chelpus.ˆ.ʻ(java.lang.String, java.io.InputStream):java.lang.String
          com.chelpus.ˆ.ʻ(boolean, com.chelpus.ˆ$ʾ):java.lang.String
          com.chelpus.ˆ.ʻ(long, long):long */
        /* JADX WARNING: Removed duplicated region for block: B:107:0x0212 A[Catch:{ Throwable -> 0x01cd, Throwable -> 0x03ee }] */
        /* JADX WARNING: Removed duplicated region for block: B:116:0x0240 A[Catch:{ Throwable -> 0x01cd, Throwable -> 0x03ee }] */
        /* JADX WARNING: Removed duplicated region for block: B:131:0x02f3 A[Catch:{ Throwable -> 0x01cd, Throwable -> 0x03ee }] */
        /* renamed from: ʻ  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.os.Bundle m5368(int r21, java.lang.String r22, java.lang.String r23, android.os.Bundle r24, android.os.Bundle r25) {
            /*
                r20 = this;
                r1 = r20
                r0 = r21
                r2 = r22
                r3 = r23
                r4 = r24
                java.lang.String r5 = "inapp"
                java.lang.String r6 = "DETAILS_LIST"
                java.lang.String r7 = "ITEM_ID_LIST"
                java.lang.String r8 = "RESPONSE_CODE"
                java.lang.String r9 = "365"
                java.lang.String r10 = "type"
                java.lang.String r11 = "productId"
                java.lang.StringBuilder r12 = new java.lang.StringBuilder
                r12.<init>()
                java.lang.String r13 = "LuckyPatcher: check api "
                r12.append(r13)
                r12.append(r0)
                java.lang.String r13 = " getSkuDetailsExtraParams"
                r12.append(r13)
                java.lang.String r12 = r12.toString()
                com.lp.C0987.m6060(r12)
                r12 = 1
                r13 = 0
                com.google.android.finsky.billing.iab.InAppBillingService r14 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Exception -> 0x00ef }
                boolean r14 = r14.f3540     // Catch:{ Exception -> 0x00ef }
                if (r14 != 0) goto L_0x0044
                com.google.android.finsky.billing.iab.InAppBillingService r14 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Exception -> 0x00ef }
                boolean r14 = r14.f3541     // Catch:{ Exception -> 0x00ef }
                if (r14 != 0) goto L_0x0044
                com.google.android.finsky.billing.iab.InAppBillingService r14 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Exception -> 0x00ef }
                r14.m5365(r2)     // Catch:{ Exception -> 0x00ef }
            L_0x0044:
                android.os.Bundle r14 = new android.os.Bundle     // Catch:{ Exception -> 0x00ef }
                r14.<init>()     // Catch:{ Exception -> 0x00ef }
                r14.putInt(r8, r13)     // Catch:{ Exception -> 0x00ef }
                com.google.android.finsky.billing.iab.InAppBillingService r14 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Exception -> 0x00ef }
                boolean r14 = r14.f3540     // Catch:{ Exception -> 0x00ef }
                if (r14 == 0) goto L_0x00f3
                com.google.android.finsky.billing.iab.InAppBillingService r14 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Exception -> 0x00ef }
                com.android.vending.ʻ.ʻ r14 = r14.f3543     // Catch:{ Exception -> 0x00ef }
                if (r14 == 0) goto L_0x00f3
                com.google.android.finsky.billing.iab.InAppBillingService r14 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Exception -> 0x00ef }
                boolean r14 = r14.f3541     // Catch:{ Exception -> 0x00ef }
                if (r14 != 0) goto L_0x00f3
                java.lang.String r14 = "Connect to google billing"
                com.lp.C0987.m6060(r14)     // Catch:{ Exception -> 0x00ea }
                com.google.android.finsky.billing.iab.InAppBillingService r14 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Exception -> 0x00ea }
                com.android.vending.ʻ.ʻ r14 = r14.f3543     // Catch:{ Exception -> 0x00ea }
                android.os.Bundle r0 = r14.m4980(r0, r2, r3, r4)     // Catch:{ Exception -> 0x00ea }
                java.util.ArrayList r14 = r0.getStringArrayList(r6)     // Catch:{ Exception -> 0x00ea }
                r4.getStringArrayList(r7)     // Catch:{ Exception -> 0x00ea }
                int r15 = r0.getInt(r8)     // Catch:{ Exception -> 0x00ea }
                java.lang.Integer r16 = java.lang.Integer.valueOf(r15)     // Catch:{ Exception -> 0x00ea }
                com.lp.C0987.m6060(r16)     // Catch:{ Exception -> 0x00ea }
                if (r15 == 0) goto L_0x0085
                com.google.android.finsky.billing.iab.InAppBillingService r0 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Exception -> 0x00ea }
                r0.f3541 = r12     // Catch:{ Exception -> 0x00ea }
                goto L_0x00f3
            L_0x0085:
                if (r14 == 0) goto L_0x00e9
                int r15 = r14.size()     // Catch:{ Exception -> 0x00ea }
                if (r15 == 0) goto L_0x00e9
                java.util.Iterator r14 = r14.iterator()     // Catch:{ Exception -> 0x00ea }
            L_0x0091:
                boolean r15 = r14.hasNext()     // Catch:{ Exception -> 0x00ea }
                if (r15 == 0) goto L_0x00e9
                java.lang.Object r15 = r14.next()     // Catch:{ Exception -> 0x00ea }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Exception -> 0x00ea }
                org.json.JSONObject r13 = new org.json.JSONObject     // Catch:{ Exception -> 0x00ea }
                r13.<init>(r15)     // Catch:{ Exception -> 0x00ea }
                java.lang.String r15 = r13.getString(r10)     // Catch:{ Exception -> 0x00ea }
                boolean r15 = r15.equals(r5)     // Catch:{ Exception -> 0x00ea }
                if (r15 == 0) goto L_0x00e2
                java.util.ArrayList<java.lang.String> r15 = r1.f3548     // Catch:{ Exception -> 0x00ea }
                java.util.Iterator r15 = r15.iterator()     // Catch:{ Exception -> 0x00ea }
                r16 = 0
            L_0x00b4:
                boolean r17 = r15.hasNext()     // Catch:{ Exception -> 0x00ea }
                if (r17 == 0) goto L_0x00d4
                java.lang.Object r17 = r15.next()     // Catch:{ Exception -> 0x00ea }
                r12 = r17
                java.lang.String r12 = (java.lang.String) r12     // Catch:{ Exception -> 0x00ea }
                r21 = r14
                java.lang.Object r14 = r13.get(r11)     // Catch:{ Exception -> 0x00ea }
                boolean r12 = r14.equals(r12)     // Catch:{ Exception -> 0x00ea }
                if (r12 == 0) goto L_0x00d0
                r16 = 1
            L_0x00d0:
                r14 = r21
                r12 = 1
                goto L_0x00b4
            L_0x00d4:
                r21 = r14
                if (r16 != 0) goto L_0x00e4
                java.util.ArrayList<java.lang.String> r12 = r1.f3548     // Catch:{ Exception -> 0x00ea }
                java.lang.String r13 = r13.getString(r11)     // Catch:{ Exception -> 0x00ea }
                r12.add(r13)     // Catch:{ Exception -> 0x00ea }
                goto L_0x00e4
            L_0x00e2:
                r21 = r14
            L_0x00e4:
                r14 = r21
                r12 = 1
                r13 = 0
                goto L_0x0091
            L_0x00e9:
                return r0
            L_0x00ea:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ Exception -> 0x00ef }
                goto L_0x00f3
            L_0x00ef:
                r0 = move-exception
                r0.printStackTrace()
            L_0x00f3:
                java.util.ArrayList r0 = r4.getStringArrayList(r7)
                java.util.ArrayList r4 = new java.util.ArrayList
                r4.<init>()
                com.google.android.finsky.billing.iab.InAppBillingService r7 = com.google.android.finsky.billing.iab.InAppBillingService.this
                boolean r7 = r7.f3540
                if (r7 == 0) goto L_0x0115
                com.google.android.finsky.billing.iab.InAppBillingService r7 = com.google.android.finsky.billing.iab.InAppBillingService.this
                com.android.vending.ʻ.ʻ r7 = r7.f3543
                if (r7 == 0) goto L_0x0115
                com.google.android.finsky.billing.iab.InAppBillingService r7 = com.google.android.finsky.billing.iab.InAppBillingService.this
                boolean r7 = r7.f3541
                if (r7 == 0) goto L_0x010f
                goto L_0x0115
            L_0x010f:
                r16 = r6
                r17 = r8
                goto L_0x03f8
            L_0x0115:
                java.lang.String r7 = "Dont Connect to google billing"
                com.lp.C0987.m6060(r7)
                com.google.android.finsky.billing.iab.ʻ r7 = new com.google.android.finsky.billing.iab.ʻ     // Catch:{ Throwable -> 0x03f0 }
                com.google.android.finsky.billing.iab.InAppBillingService r12 = com.google.android.finsky.billing.iab.InAppBillingService.this     // Catch:{ Throwable -> 0x03f0 }
                android.content.Context r12 = r12.f3539     // Catch:{ Throwable -> 0x03f0 }
                r7.<init>(r12, r2)     // Catch:{ Throwable -> 0x03f0 }
                java.util.Iterator r2 = r0.iterator()     // Catch:{ Throwable -> 0x03f0 }
            L_0x0127:
                boolean r12 = r2.hasNext()     // Catch:{ Throwable -> 0x03f0 }
                java.lang.String r13 = "subs"
                if (r12 == 0) goto L_0x0186
                java.lang.Object r12 = r2.next()     // Catch:{ Throwable -> 0x03f0 }
                java.lang.String r12 = (java.lang.String) r12     // Catch:{ Throwable -> 0x03f0 }
                boolean r13 = r3.equals(r13)     // Catch:{ Throwable -> 0x03f0 }
                if (r13 == 0) goto L_0x015d
                java.util.ArrayList<java.lang.String> r13 = r1.f3549     // Catch:{ Throwable -> 0x03f0 }
                java.util.Iterator r13 = r13.iterator()     // Catch:{ Throwable -> 0x03f0 }
                r14 = 0
            L_0x0142:
                boolean r15 = r13.hasNext()     // Catch:{ Throwable -> 0x03f0 }
                if (r15 == 0) goto L_0x0156
                java.lang.Object r15 = r13.next()     // Catch:{ Throwable -> 0x03f0 }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Throwable -> 0x03f0 }
                boolean r15 = r12.equals(r15)     // Catch:{ Throwable -> 0x03f0 }
                if (r15 == 0) goto L_0x0142
                r14 = 1
                goto L_0x0142
            L_0x0156:
                if (r14 != 0) goto L_0x015d
                java.util.ArrayList<java.lang.String> r13 = r1.f3549     // Catch:{ Throwable -> 0x03f0 }
                r13.add(r12)     // Catch:{ Throwable -> 0x03f0 }
            L_0x015d:
                boolean r13 = r3.equals(r5)     // Catch:{ Throwable -> 0x03f0 }
                if (r13 == 0) goto L_0x0127
                java.util.ArrayList<java.lang.String> r13 = r1.f3548     // Catch:{ Throwable -> 0x03f0 }
                java.util.Iterator r13 = r13.iterator()     // Catch:{ Throwable -> 0x03f0 }
                r14 = 0
            L_0x016a:
                boolean r15 = r13.hasNext()     // Catch:{ Throwable -> 0x03f0 }
                if (r15 == 0) goto L_0x017e
                java.lang.Object r15 = r13.next()     // Catch:{ Throwable -> 0x03f0 }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Throwable -> 0x03f0 }
                boolean r15 = r12.equals(r15)     // Catch:{ Throwable -> 0x03f0 }
                if (r15 == 0) goto L_0x016a
                r14 = 1
                goto L_0x016a
            L_0x017e:
                if (r14 != 0) goto L_0x0127
                java.util.ArrayList<java.lang.String> r13 = r1.f3548     // Catch:{ Throwable -> 0x03f0 }
                r13.add(r12)     // Catch:{ Throwable -> 0x03f0 }
                goto L_0x0127
            L_0x0186:
                java.util.Iterator r2 = r0.iterator()     // Catch:{ Throwable -> 0x03f0 }
            L_0x018a:
                boolean r0 = r2.hasNext()     // Catch:{ Throwable -> 0x03f0 }
                if (r0 == 0) goto L_0x010f
                java.lang.Object r0 = r2.next()     // Catch:{ Throwable -> 0x03f0 }
                java.lang.String r0 = (java.lang.String) r0     // Catch:{ Throwable -> 0x03f0 }
                java.lang.String r5 = "\\."
                java.lang.String[] r5 = r0.split(r5)     // Catch:{ Throwable -> 0x03f0 }
                java.lang.String r12 = "_"
                java.lang.String r15 = ""
                java.lang.String r14 = " "
                if (r5 == 0) goto L_0x01df
                r22 = r2
                int r2 = r5.length     // Catch:{ Throwable -> 0x03f0 }
                if (r2 == 0) goto L_0x01e1
                int r2 = r5.length     // Catch:{ Throwable -> 0x03f0 }
                r16 = r6
                r6 = 2
                if (r2 < r6) goto L_0x01d0
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01cd }
                r2.<init>()     // Catch:{ Throwable -> 0x01cd }
                r17 = r8
                int r8 = r5.length     // Catch:{ Throwable -> 0x03ee }
                int r8 = r8 - r6
                r6 = r5[r8]     // Catch:{ Throwable -> 0x03ee }
                r2.append(r6)     // Catch:{ Throwable -> 0x03ee }
                r2.append(r14)     // Catch:{ Throwable -> 0x03ee }
                int r6 = r5.length     // Catch:{ Throwable -> 0x03ee }
                r8 = 1
                int r6 = r6 - r8
                r6 = r5[r6]     // Catch:{ Throwable -> 0x03ee }
                r2.append(r6)     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x03ee }
                goto L_0x01d3
            L_0x01cd:
                r0 = move-exception
                goto L_0x03f3
            L_0x01d0:
                r17 = r8
                r2 = r15
            L_0x01d3:
                int r6 = r5.length     // Catch:{ Throwable -> 0x03ee }
                r8 = 1
                if (r6 != r8) goto L_0x01ea
                r6 = 0
                r2 = r5[r6]     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r2 = r2.replaceAll(r12, r14)     // Catch:{ Throwable -> 0x03ee }
                goto L_0x01ea
            L_0x01df:
                r22 = r2
            L_0x01e1:
                r16 = r6
                r17 = r8
                r8 = 1
                java.lang.String r2 = r0.replaceAll(r12, r14)     // Catch:{ Throwable -> 0x03ee }
            L_0x01ea:
                r5 = 0
                r12 = r9
                r8 = 99
                long r5 = com.chelpus.C0815.m5132(r5, r8)     // Catch:{ Throwable -> 0x03ee }
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x03ee }
                r8.<init>()     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r9 = "0."
                r8.append(r9)     // Catch:{ Throwable -> 0x03ee }
                r8.append(r5)     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r8 = r8.toString()     // Catch:{ Throwable -> 0x03ee }
                java.util.ArrayList r9 = r7.m5381()     // Catch:{ Throwable -> 0x03ee }
                java.util.Iterator r9 = r9.iterator()     // Catch:{ Throwable -> 0x03ee }
            L_0x020c:
                boolean r18 = r9.hasNext()     // Catch:{ Throwable -> 0x03ee }
                if (r18 == 0) goto L_0x0235
                java.lang.Object r18 = r9.next()     // Catch:{ Throwable -> 0x03ee }
                r24 = r7
                r7 = r18
                com.google.android.finsky.billing.iab.ʼ r7 = (com.google.android.finsky.billing.iab.C0824) r7     // Catch:{ Throwable -> 0x03ee }
                r18 = r9
                java.lang.String r9 = r7.f3560     // Catch:{ Throwable -> 0x03ee }
                boolean r9 = r9.equals(r0)     // Catch:{ Throwable -> 0x03ee }
                if (r9 == 0) goto L_0x022f
                int r7 = r7.f3563     // Catch:{ Throwable -> 0x03ee }
                r9 = 2
                if (r7 != r9) goto L_0x0230
                java.lang.String r7 = "Purchased"
                r8 = r7
                goto L_0x0230
            L_0x022f:
                r9 = 2
            L_0x0230:
                r7 = r24
                r9 = r18
                goto L_0x020c
            L_0x0235:
                r24 = r7
                boolean r7 = r3.equals(r13)     // Catch:{ Throwable -> 0x03ee }
                r18 = 1000000(0xf4240, double:4.940656E-318)
                if (r7 == 0) goto L_0x02f3
                java.util.ArrayList<java.lang.String> r7 = r1.f3548     // Catch:{ Throwable -> 0x03ee }
                java.util.Iterator r7 = r7.iterator()     // Catch:{ Throwable -> 0x03ee }
                r9 = 0
            L_0x0247:
                boolean r15 = r7.hasNext()     // Catch:{ Throwable -> 0x03ee }
                if (r15 == 0) goto L_0x025b
                java.lang.Object r15 = r7.next()     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Throwable -> 0x03ee }
                boolean r15 = r15.equals(r0)     // Catch:{ Throwable -> 0x03ee }
                if (r15 == 0) goto L_0x0247
                r9 = 1
                goto L_0x0247
            L_0x025b:
                if (r9 != 0) goto L_0x02d7
                org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ Throwable -> 0x03ee }
                r7.<init>()     // Catch:{ Throwable -> 0x03ee }
                r7.put(r11, r0)     // Catch:{ JSONException -> 0x02a6 }
                r7.put(r10, r3)     // Catch:{ JSONException -> 0x02a6 }
                java.lang.String r0 = "price"
                r7.put(r0, r8)     // Catch:{ JSONException -> 0x02a6 }
                java.lang.String r0 = "title"
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x02a6 }
                r8.<init>()     // Catch:{ JSONException -> 0x02a6 }
                r8.append(r2)     // Catch:{ JSONException -> 0x02a6 }
                r8.append(r14)     // Catch:{ JSONException -> 0x02a6 }
                java.lang.String r8 = r8.toString()     // Catch:{ JSONException -> 0x02a6 }
                r7.put(r0, r8)     // Catch:{ JSONException -> 0x02a6 }
                java.lang.String r0 = "description"
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x02a6 }
                r8.<init>()     // Catch:{ JSONException -> 0x02a6 }
                r8.append(r2)     // Catch:{ JSONException -> 0x02a6 }
                java.lang.String r2 = "  "
                r8.append(r2)     // Catch:{ JSONException -> 0x02a6 }
                java.lang.String r2 = r8.toString()     // Catch:{ JSONException -> 0x02a6 }
                r7.put(r0, r2)     // Catch:{ JSONException -> 0x02a6 }
                java.lang.String r0 = "price_amount_micros"
                long r5 = r5 * r18
                r7.put(r0, r5)     // Catch:{ JSONException -> 0x02a6 }
                java.lang.String r0 = "price_currency_code"
                java.lang.String r2 = "USD"
                r7.put(r0, r2)     // Catch:{ JSONException -> 0x02a6 }
                goto L_0x02aa
            L_0x02a6:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ Throwable -> 0x03ee }
            L_0x02aa:
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x03ee }
                r0.<init>()     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r2 = "return:"
                r0.append(r2)     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r2 = r7.getString(r11)     // Catch:{ Throwable -> 0x03ee }
                r0.append(r2)     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r2 = " type:"
                r0.append(r2)     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r2 = r7.getString(r10)     // Catch:{ Throwable -> 0x03ee }
                r0.append(r2)     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x03ee }
                com.lp.C0987.m6060(r0)     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r0 = r7.toString()     // Catch:{ Throwable -> 0x03ee }
                r4.add(r0)     // Catch:{ Throwable -> 0x03ee }
                goto L_0x03e2
            L_0x02d7:
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x03ee }
                r2.<init>()     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r5 = "skip "
                r2.append(r5)     // Catch:{ Throwable -> 0x03ee }
                r2.append(r0)     // Catch:{ Throwable -> 0x03ee }
                r2.append(r14)     // Catch:{ Throwable -> 0x03ee }
                r2.append(r3)     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r0 = r2.toString()     // Catch:{ Throwable -> 0x03ee }
                com.lp.C0987.m6060(r0)     // Catch:{ Throwable -> 0x03ee }
                goto L_0x03e2
            L_0x02f3:
                java.util.ArrayList<java.lang.String> r7 = r1.f3548     // Catch:{ Throwable -> 0x03ee }
                boolean r7 = r7.contains(r0)     // Catch:{ Throwable -> 0x03ee }
                if (r7 == 0) goto L_0x03e2
                org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ Throwable -> 0x03ee }
                r7.<init>()     // Catch:{ Throwable -> 0x03ee }
                r7.put(r11, r0)     // Catch:{ JSONException -> 0x03b1 }
                r7.put(r10, r3)     // Catch:{ JSONException -> 0x03b1 }
                java.lang.String r0 = "price"
                r7.put(r0, r8)     // Catch:{ JSONException -> 0x03b1 }
                java.lang.String r0 = "title"
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x03b1 }
                r8.<init>()     // Catch:{ JSONException -> 0x03b1 }
                r8.append(r2)     // Catch:{ JSONException -> 0x03b1 }
                r8.append(r14)     // Catch:{ JSONException -> 0x03b1 }
                java.lang.String r8 = r8.toString()     // Catch:{ JSONException -> 0x03b1 }
                r7.put(r0, r8)     // Catch:{ JSONException -> 0x03b1 }
                java.lang.String r0 = "description"
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x03b1 }
                r8.<init>()     // Catch:{ JSONException -> 0x03b1 }
                r8.append(r2)     // Catch:{ JSONException -> 0x03b1 }
                java.lang.String r2 = "  "
                r8.append(r2)     // Catch:{ JSONException -> 0x03b1 }
                java.lang.String r2 = r8.toString()     // Catch:{ JSONException -> 0x03b1 }
                r7.put(r0, r2)     // Catch:{ JSONException -> 0x03b1 }
                java.lang.String r0 = "price_amount_micros"
                long r5 = r5 * r18
                r7.put(r0, r5)     // Catch:{ JSONException -> 0x03b1 }
                java.lang.String r0 = "price_currency_code"
                java.lang.String r2 = "USD"
                r7.put(r0, r2)     // Catch:{ JSONException -> 0x03b1 }
                r5 = 0
                r8 = 99
                long r5 = com.chelpus.C0815.m5132(r5, r8)     // Catch:{ JSONException -> 0x03b1 }
                java.lang.String r0 = "original_price"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x03b1 }
                r2.<init>()     // Catch:{ JSONException -> 0x03b1 }
                r2.append(r15)     // Catch:{ JSONException -> 0x03b1 }
                r2.append(r5)     // Catch:{ JSONException -> 0x03b1 }
                java.lang.String r2 = r2.toString()     // Catch:{ JSONException -> 0x03b1 }
                r7.put(r0, r2)     // Catch:{ JSONException -> 0x03b1 }
                java.lang.String r0 = "original_price_micros"
                long r8 = r5 * r18
                r7.put(r0, r8)     // Catch:{ JSONException -> 0x03b1 }
                java.lang.String r0 = "subscriptionPeriod"
                r2 = r12
                r7.put(r0, r2)     // Catch:{ JSONException -> 0x03af }
                java.lang.String r0 = "freeTrialPeriod"
                r7.put(r0, r2)     // Catch:{ JSONException -> 0x03af }
                java.lang.String r0 = "introductoryPrice"
                java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x03af }
                r12.<init>()     // Catch:{ JSONException -> 0x03af }
                r12.append(r15)     // Catch:{ JSONException -> 0x03af }
                r12.append(r5)     // Catch:{ JSONException -> 0x03af }
                java.lang.String r5 = r12.toString()     // Catch:{ JSONException -> 0x03af }
                r7.put(r0, r5)     // Catch:{ JSONException -> 0x03af }
                java.lang.String r0 = "introductoryPriceAmountMicros"
                r7.put(r0, r8)     // Catch:{ JSONException -> 0x03af }
                java.lang.String r0 = "introductoryPricePeriod"
                r7.put(r0, r2)     // Catch:{ JSONException -> 0x03af }
                java.lang.String r0 = "introductoryPriceCycles"
                r7.put(r0, r2)     // Catch:{ JSONException -> 0x03af }
                java.lang.String r0 = "iconUrl"
                java.lang.String r5 = "http://"
                r7.put(r0, r5)     // Catch:{ JSONException -> 0x03af }
                java.lang.String r0 = "rewardToken"
                java.lang.String r5 = "token"
                r7.put(r0, r5)     // Catch:{ JSONException -> 0x03af }
                java.lang.String r0 = "skuDetailsToken"
                java.lang.String r5 = "token"
                r7.put(r0, r5)     // Catch:{ JSONException -> 0x03af }
                java.lang.String r0 = "introductoryPriceCycles"
                r7.put(r0, r2)     // Catch:{ JSONException -> 0x03af }
                goto L_0x03b6
            L_0x03af:
                r0 = move-exception
                goto L_0x03b3
            L_0x03b1:
                r0 = move-exception
                r2 = r12
            L_0x03b3:
                r0.printStackTrace()     // Catch:{ Throwable -> 0x03ee }
            L_0x03b6:
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x03ee }
                r0.<init>()     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r5 = "return:"
                r0.append(r5)     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r5 = r7.getString(r11)     // Catch:{ Throwable -> 0x03ee }
                r0.append(r5)     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r5 = " type:"
                r0.append(r5)     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r5 = r7.getString(r10)     // Catch:{ Throwable -> 0x03ee }
                r0.append(r5)     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x03ee }
                com.lp.C0987.m6060(r0)     // Catch:{ Throwable -> 0x03ee }
                java.lang.String r0 = r7.toString()     // Catch:{ Throwable -> 0x03ee }
                r4.add(r0)     // Catch:{ Throwable -> 0x03ee }
                goto L_0x03e3
            L_0x03e2:
                r2 = r12
            L_0x03e3:
                r7 = r24
                r9 = r2
                r6 = r16
                r8 = r17
                r2 = r22
                goto L_0x018a
            L_0x03ee:
                r0 = move-exception
                goto L_0x03f5
            L_0x03f0:
                r0 = move-exception
                r16 = r6
            L_0x03f3:
                r17 = r8
            L_0x03f5:
                r0.printStackTrace()
            L_0x03f8:
                android.os.Bundle r0 = new android.os.Bundle
                r0.<init>()
                r2 = r17
                r3 = 0
                r0.putInt(r2, r3)
                r2 = r16
                r0.putStringArrayList(r2, r4)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.finsky.billing.iab.InAppBillingService.AnonymousClass2.m5368(int, java.lang.String, java.lang.String, android.os.Bundle, android.os.Bundle):android.os.Bundle");
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public Bundle m5380(int i, String str, String str2, Bundle bundle) {
            C0987.m6060((Object) ("LuckyPatcher: check api " + i + " acknowledgePurchaseExtraParams"));
            Bundle bundle2 = new Bundle();
            try {
                bundle2.putInt("RESPONSE_CODE", 0);
                bundle2.putString("DEBUG_MESSAGE", "Purchase is verifed.");
            } catch (Throwable th) {
                th.printStackTrace();
            }
            return bundle2;
        }
    };

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5364() {
        C0987.m6079((Context) this);
        if (C0987.f4474) {
            PackageInfo packageInfo = null;
            try {
                packageInfo = C0987.m6068().getPackageInfo("com.android.vending", 516);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if (packageInfo != null && packageInfo.services != null && packageInfo.services.length != 0) {
                for (int i = 0; i < packageInfo.services.length; i++) {
                    try {
                        if ((packageInfo.services[i].name.endsWith("InAppBillingService") || packageInfo.services[i].name.endsWith("MarketBillingService")) && C0987.m6068().getComponentEnabledSetting(new ComponentName("com.android.vending", packageInfo.services[i].name)) != 1) {
                            this.f3542 = true;
                            C0815.m5186(true);
                        }
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                }
            }
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent != null) {
            if (intent.getStringExtra("xexe") == null || !intent.getStringExtra("xexe").equals("lp")) {
                C0987.m6060((Object) "Connect from app.");
            } else {
                this.f3541 = true;
            }
        }
        return super.onStartCommand(intent, i, i2);
    }

    public void onCreate() {
        super.onCreate();
        C0987.m6060((Object) ("create bill+skip:" + this.f3541));
        this.f3539 = this;
        if (C0987.f4474) {
            m5364();
        }
    }

    public void onTaskRemoved(Intent intent) {
        super.onTaskRemoved(intent);
        C0987.m6060((Object) "on Task Removed billing");
    }

    public void onDestroy() {
        C0987.m6060((Object) "destroy billing");
        if (f3538 != null) {
            try {
                C0987.m6072().unbindService(f3538);
            } catch (Throwable unused) {
            }
        }
        super.onDestroy();
    }

    public boolean onUnbind(Intent intent) {
        C0987.m6060((Object) "unbind billing");
        return super.onUnbind(intent);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5365(final String str) {
        m5364();
        if (!this.f3540) {
            f3538 = new ServiceConnection() {
                public void onServiceDisconnected(ComponentName componentName) {
                    C0987.m6060((Object) "Billing service disconnected.");
                    InAppBillingService inAppBillingService = InAppBillingService.this;
                    inAppBillingService.f3543 = null;
                    inAppBillingService.f3540 = false;
                }

                public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                    C0987.m6060((Object) "Billing service try to connect.");
                    InAppBillingService.this.f3543 = C0773.C0774.m4994(iBinder);
                    try {
                        int r4 = InAppBillingService.this.f3543.m4979(3, str, "inapp");
                        if (r4 != 0) {
                            C0987.m6060((Object) ("bill error:" + r4));
                            InAppBillingService.this.f3541 = true;
                            return;
                        }
                        InAppBillingService.this.f3543.m4979(3, str, "subs");
                        C0987.m6060((Object) "Billing service connected.");
                        InAppBillingService.this.f3540 = true;
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            };
            Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
            intent.setPackage("com.android.vending");
            intent.putExtra("xexe", "lp");
            if (C0987.m6068().queryIntentServices(intent, 0).isEmpty()) {
                new C0815("w").m5350(2000L);
            }
            if (!C0987.m6068().queryIntentServices(intent, 0).isEmpty()) {
                for (ResolveInfo next : C0987.m6068().queryIntentServices(intent, 0)) {
                    if (next.serviceInfo.packageName != null && next.serviceInfo.packageName.equals("com.android.vending")) {
                        ComponentName componentName = new ComponentName(next.serviceInfo.packageName, next.serviceInfo.name);
                        Intent intent2 = new Intent();
                        intent2.setComponent(componentName);
                        intent2.putExtra("xexe", "lp");
                        if (C0987.m6072().bindService(intent2, f3538, 1)) {
                            int i = 0;
                            while (!this.f3540 && !this.f3541) {
                                new C0815("w").m5350(500L);
                                i++;
                                if (i == 30) {
                                    break;
                                }
                            }
                        }
                    }
                }
                return;
            }
            C0987.m6060((Object) "Billing service unavailable on device.");
            return;
        }
        throw new IllegalStateException("IAB helper is already set up.");
    }

    public IBinder onBind(Intent intent) {
        if (intent != null) {
            if (intent.getStringExtra("xexe") == null || !intent.getStringExtra("xexe").equals("lp") || intent.getPackage() != null) {
                C0987.m6060((Object) "Connect from patch.");
            } else {
                C0987.m6060((Object) "Connect from proxy.");
                this.f3540 = false;
                this.f3541 = true;
            }
        }
        return this.f3545;
    }
}
