package com.google.android.finsky.billing.iab;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import com.chelpus.C0815;
import com.lp.C0987;
import org.json.JSONException;
import org.json.JSONObject;
import org.tukaani.xz.common.Util;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

public class BuyActivity extends Activity {

    /* renamed from: ʻ  reason: contains not printable characters */
    public BuyActivity f3527 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    String f3528 = BuildConfig.FLAVOR;

    /* renamed from: ʽ  reason: contains not printable characters */
    String f3529 = BuildConfig.FLAVOR;

    /* renamed from: ʾ  reason: contains not printable characters */
    String f3530 = BuildConfig.FLAVOR;

    /* renamed from: ʿ  reason: contains not printable characters */
    String f3531 = BuildConfig.FLAVOR;

    /* renamed from: ˆ  reason: contains not printable characters */
    Bundle f3532 = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chelpus.ˆ.ʻ(long, long):long
     arg types: [int, int]
     candidates:
      com.chelpus.ˆ.ʻ(byte, byte):int
      com.chelpus.ˆ.ʻ(java.io.File, int):int
      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String[]):int
      com.chelpus.ˆ.ʻ(java.lang.String, boolean):long
      com.chelpus.ˆ.ʻ(java.lang.String, int):android.content.pm.PackageInfo
      com.chelpus.ˆ.ʻ(int, java.lang.String[]):java.lang.String
      com.chelpus.ˆ.ʻ(android.content.Context, android.net.Uri):java.lang.String
      com.chelpus.ˆ.ʻ(android.content.Context, java.lang.String):java.lang.String
      com.chelpus.ˆ.ʻ(java.io.File, java.lang.String):java.lang.String
      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String):java.lang.String
      com.chelpus.ˆ.ʻ(java.io.File, java.io.File):void
      com.chelpus.ˆ.ʻ(java.io.File, com.chelpus.ʻ.ʼ.י[]):void
      com.chelpus.ˆ.ʻ(java.io.InputStream, java.io.OutputStream):void
      com.chelpus.ˆ.ʻ(java.util.List<java.io.File>, java.util.zip.ZipOutputStream):void
      com.chelpus.ˆ.ʻ(int, java.io.File):boolean
      com.chelpus.ˆ.ʻ(boolean, boolean):boolean
      com.chelpus.ˆ.ʻ(java.lang.String[], java.lang.String):boolean
      com.chelpus.ˆ.ʻ(int, int):byte[]
      com.chelpus.ˆ.ʻ(java.io.File, boolean):float
      com.chelpus.ˆ.ʻ(java.io.File, java.util.ArrayList<java.lang.String>):com.chelpus.ˆ$ʼ
      com.chelpus.ˆ.ʻ(java.lang.String, java.io.InputStream):java.lang.String
      com.chelpus.ˆ.ʻ(boolean, com.chelpus.ˆ$ʾ):java.lang.String
      com.chelpus.ˆ.ʻ(long, long):long */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f3527 = this;
        if ("com.google.android.finsky.billing.iab.BUY".equals(getIntent().getAction()) || bundle != null) {
            Log.d("BillingHack", "Buy intent!");
            C0987.m6060((Object) Integer.valueOf(getRequestedOrientation()));
            if (bundle != null) {
                this.f3528 = bundle.getString("packageName");
                this.f3529 = bundle.getString("product");
                this.f3530 = bundle.getString("payload");
                this.f3531 = bundle.getString("Type");
            } else {
                this.f3528 = getIntent().getExtras().getString("packageName");
                this.f3529 = getIntent().getExtras().getString("product");
                this.f3530 = getIntent().getExtras().getString("payload");
                this.f3531 = getIntent().getExtras().getString("Type");
            }
            C0987.m6060((Object) this.f3531);
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("orderId", (C0815.m5132(1000000000000000000L, (long) Util.VLI_MAX) + C0815.m5132(0L, 9L)) + "." + C0815.m5132(1000000000000000L, 9999999999999999L));
                jSONObject.put("packageName", this.f3528);
                jSONObject.put("productId", this.f3529);
                jSONObject.put("purchaseTime", new Long(System.currentTimeMillis()));
                jSONObject.put("purchaseState", new Integer(0));
                jSONObject.put("developerPayload", this.f3530);
                jSONObject.put("purchaseToken", C0815.m5225(24) + ".AO-J1O" + C0815.m5233(41) + "-" + C0815.m5233(7) + "_" + C0815.m5233(84));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            final String jSONObject2 = jSONObject.toString();
            if (getIntent().getExtras().getString("autorepeat") != null) {
                Intent intent = new Intent();
                Bundle bundle2 = new Bundle();
                String r9 = C0815.m5200(jSONObject2);
                new C0821(getApplicationContext(), this.f3528).m5382(new C0824(this.f3529, jSONObject2, r9, 1, 1));
                bundle2.putInt("RESPONSE_CODE", 0);
                bundle2.putString("INAPP_PURCHASE_DATA", jSONObject2);
                bundle2.putString("INAPP_DATA_SIGNATURE", r9);
                intent.putExtras(bundle2);
                setResult(-1, intent);
                finish();
            }
            setContentView((int) R.layout.buy_dialog);
            final CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox2);
            final CheckBox checkBox2 = (CheckBox) findViewById(R.id.checkBox3);
            TextView textView = (TextView) findViewById(R.id.textView2);
            textView.setText(C0815.m5205((int) R.string.billing_hack2_auto_repeat_note));
            textView.append("\n" + C0815.m5205((int) R.string.billing_hack2_new));
            ((CheckBox) findViewById(R.id.checkBox)).setVisibility(8);
            checkBox.setChecked(false);
            ((Button) findViewById(R.id.button_yes)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    String r1 = C0815.m5200(jSONObject2);
                    new C0821(BuyActivity.this.getApplicationContext(), BuyActivity.this.f3528).m5382(new C0824(BuyActivity.this.f3529, jSONObject2, "1", checkBox.isChecked() ? 2 : 1, checkBox2.isChecked() ? 1 : 0));
                    bundle.putInt("RESPONSE_CODE", 0);
                    bundle.putString("INAPP_PURCHASE_DATA", jSONObject2);
                    bundle.putString("INAPP_DATA_SIGNATURE", r1);
                    intent.putExtras(bundle);
                    BuyActivity.this.setResult(-1, intent);
                    BuyActivity.this.finish();
                }
            });
            ((Button) findViewById(R.id.button_no)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putInt("RESPONSE_CODE", 1);
                    intent.putExtras(bundle);
                    BuyActivity.this.setResult(0, intent);
                    BuyActivity.this.finish();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        C0987.m6060((Object) "save instance");
        bundle.putString("packageName", this.f3528);
        bundle.putString("product", this.f3529);
        bundle.putString("payload", this.f3530);
        bundle.putString("Type", this.f3531);
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        C0987.m6060((Object) "load instance");
        this.f3528 = bundle.getString("packageName");
        super.onRestoreInstanceState(bundle);
    }

    public void onConfigurationChanged(Configuration configuration) {
        if (configuration.orientation == 2) {
            setRequestedOrientation(0);
        } else {
            setRequestedOrientation(1);
        }
        super.onConfigurationChanged(configuration);
    }
}
