package com.google.android.finsky.billing.iab;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import com.chelpus.C0815;
import com.lp.C0987;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

/* renamed from: com.google.android.finsky.billing.iab.ʻ  reason: contains not printable characters */
/* compiled from: DbHelper */
public class C0821 extends SQLiteOpenHelper {

    /* renamed from: ʻ  reason: contains not printable characters */
    static String f3551 = "Packages";

    /* renamed from: ʼ  reason: contains not printable characters */
    public static Context f3552 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    public static boolean f3553 = false;

    /* renamed from: ʾ  reason: contains not printable characters */
    public static boolean f3554 = false;

    public C0821(Context context, String str) {
        super(context, "BillingRestoreTransactions", (SQLiteDatabase.CursorFactory) null, 48);
        f3552 = context;
        f3551 = str;
        try {
            if (C0987.f4518 == null) {
                C0987.f4518 = getWritableDatabase();
            } else {
                C0987.f4518.close();
                C0987.f4518 = getWritableDatabase();
            }
            C0987.f4518 = C0987.f4518;
            onCreate(C0987.f4518);
        } catch (SQLiteException e) {
            e.printStackTrace();
            if ((e.toString().contains("Could not open the database") || e.toString().contains("readonly")) && C0987.f4474) {
                C0987.m6060((Object) "LP: Delete bad database.");
                C0815 r6 = new C0815(BuildConfig.FLAVOR);
                r6.m5353("rm -r /data/data/" + C0987.m6072().getPackageName() + "/databases/" + getDatabaseName());
                C0815 r62 = new C0815(BuildConfig.FLAVOR);
                r62.m5353("rm /data/data/" + C0987.m6072().getPackageName() + "/databases/" + getDatabaseName());
            }
        }
    }

    public C0821(Context context) {
        super(context, "BillingRestoreTransactions", (SQLiteDatabase.CursorFactory) null, 48);
        f3552 = context;
        f3551 = f3551;
        try {
            if (C0987.f4518 == null) {
                C0987.f4518 = getWritableDatabase();
            } else {
                C0987.f4518.close();
                C0987.f4518 = getWritableDatabase();
            }
            onCreate(C0987.f4518);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS '" + f3551 + "' (" + "itemID" + " TEXT PRIMARY KEY, " + "Data" + " TEXT, " + "Signature" + " TEXT, " + "savePurchase" + " INTEGER, " + "autobuy" + " INTEGER);");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        C0987.m6060((Object) ("SQLite onUpgrade " + i + " " + i2));
        try {
            C0987.m6060((Object) ("SQLite base version is " + sQLiteDatabase.getVersion()));
            if (i2 > i) {
                Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
                if (rawQuery.moveToFirst()) {
                    while (!rawQuery.isAfterLast()) {
                        C0987.m6060((Object) "SQL add new column for version 48.");
                        try {
                            sQLiteDatabase.execSQL("ALTER TABLE '" + rawQuery.getString(0) + "' ADD COLUMN " + "savePurchase" + " INTEGER;");
                            sQLiteDatabase.execSQL("UPDATE '" + rawQuery.getString(0) + "' SET " + "savePurchase" + "= 1;");
                        } catch (SQLiteException e) {
                            e.printStackTrace();
                        }
                        try {
                            sQLiteDatabase.execSQL("ALTER TABLE '" + rawQuery.getString(0) + "' ADD COLUMN " + "autobuy" + " INTEGER;");
                            sQLiteDatabase.execSQL("UPDATE '" + rawQuery.getString(0) + "' SET " + "autobuy" + "= 0;");
                        } catch (SQLiteException e2) {
                            e2.printStackTrace();
                        }
                        rawQuery.moveToNext();
                    }
                }
                onCreate(sQLiteDatabase);
            } else {
                Cursor rawQuery2 = sQLiteDatabase.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
                if (rawQuery2.moveToFirst()) {
                    while (!rawQuery2.isAfterLast()) {
                        C0987.m6060((Object) "SQL recreate table for version 48.");
                        try {
                            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS '" + rawQuery2.getString(0) + "';");
                        } catch (SQLiteException e3) {
                            e3.printStackTrace();
                        }
                        rawQuery2.moveToNext();
                    }
                }
            }
            onCreate(sQLiteDatabase);
        } catch (SQLiteException e4) {
            e4.printStackTrace();
            if (e4.toString().contains("Could not open the database") && C0987.f4474) {
                C0987.m6060((Object) "LP: Delete bad database.");
                C0815 r11 = new C0815(BuildConfig.FLAVOR);
                r11.m5353("rm -r /data/data/" + C0987.m6072().getPackageName() + "/databases/" + getDatabaseName());
                C0815 r112 = new C0815(BuildConfig.FLAVOR);
                r112.m5353("rm /data/data/" + C0987.m6072().getPackageName() + "/databases/" + getDatabaseName());
            }
        }
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        C0987.m6060((Object) ("SQLite onUpgrade " + i + " " + i2));
        try {
            C0987.m6060((Object) ("SQLite base version is " + sQLiteDatabase.getVersion()));
            Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
            if (rawQuery.moveToFirst()) {
                while (!rawQuery.isAfterLast()) {
                    C0987.m6060((Object) "SQL recreate table for version 48.");
                    try {
                        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS '" + rawQuery.getString(0) + "';");
                    } catch (SQLiteException e) {
                        e.printStackTrace();
                    }
                    rawQuery.moveToNext();
                }
            }
            onCreate(sQLiteDatabase);
        } catch (SQLiteException e2) {
            e2.printStackTrace();
            if (e2.toString().contains("Could not open the database") && C0987.f4474) {
                C0987.m6060((Object) "LP: Delete bad database.");
                C0815 r6 = new C0815(BuildConfig.FLAVOR);
                r6.m5353("rm -r /data/data/" + C0987.m6072().getPackageName() + "/databases/" + getDatabaseName());
                C0815 r62 = new C0815(BuildConfig.FLAVOR);
                r62.m5353("rm /data/data/" + C0987.m6072().getPackageName() + "/databases/" + getDatabaseName());
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:16|17) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:5|6|7|8|12|13|(2:24|15)) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r8.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0084 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x008e */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008a A[SYNTHETIC] */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<com.google.android.finsky.billing.iab.C0824> m5381() {
        /*
            r16 = this;
            java.lang.String r1 = "autobuy"
            java.lang.String r2 = "savePurchase"
            java.lang.String r3 = "Signature"
            java.lang.String r4 = "Data"
            java.lang.String r5 = "itemID"
            java.lang.String r0 = "'"
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            r6.clear()
            r7 = 0
            android.database.sqlite.SQLiteDatabase r8 = com.lp.C0987.f4518     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            r9.<init>()     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            r9.append(r0)     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            java.lang.String r10 = com.google.android.finsky.billing.iab.C0821.f3551     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            r9.append(r10)     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            r9.append(r0)     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            java.lang.String r9 = r9.toString()     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            r0 = 5
            java.lang.String[] r10 = new java.lang.String[r0]     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            r10[r7] = r5     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            r0 = 1
            r10[r0] = r4     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            r0 = 2
            r10[r0] = r3     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            r0 = 3
            r10[r0] = r2     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            r0 = 4
            r10[r0] = r1     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            android.database.Cursor r8 = r8.query(r9, r10, r11, r12, r13, r14, r15)     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            int r0 = r8.getCount()     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            if (r0 <= 0) goto L_0x0091
            r8.moveToFirst()     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
        L_0x004e:
            int r0 = r8.getColumnIndexOrThrow(r5)     // Catch:{ Exception -> 0x0080 }
            java.lang.String r10 = r8.getString(r0)     // Catch:{ Exception -> 0x0080 }
            int r0 = r8.getColumnIndex(r4)     // Catch:{ IllegalArgumentException -> 0x0084 }
            java.lang.String r11 = r8.getString(r0)     // Catch:{ IllegalArgumentException -> 0x0084 }
            int r0 = r8.getColumnIndex(r3)     // Catch:{ IllegalArgumentException -> 0x0084 }
            java.lang.String r12 = r8.getString(r0)     // Catch:{ IllegalArgumentException -> 0x0084 }
            int r0 = r8.getColumnIndex(r2)     // Catch:{ IllegalArgumentException -> 0x0084 }
            int r13 = r8.getInt(r0)     // Catch:{ IllegalArgumentException -> 0x0084 }
            int r0 = r8.getColumnIndex(r1)     // Catch:{ IllegalArgumentException -> 0x0084 }
            int r14 = r8.getInt(r0)     // Catch:{ IllegalArgumentException -> 0x0084 }
            com.google.android.finsky.billing.iab.ʼ r0 = new com.google.android.finsky.billing.iab.ʼ     // Catch:{ IllegalArgumentException -> 0x0084 }
            r9 = r0
            r9.<init>(r10, r11, r12, r13, r14)     // Catch:{ IllegalArgumentException -> 0x0084 }
            r6.add(r0)     // Catch:{ IllegalArgumentException -> 0x0084 }
            goto L_0x0084
        L_0x0080:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
        L_0x0084:
            boolean r0 = r8.moveToNext()     // Catch:{ Exception -> 0x008e }
            if (r0 != 0) goto L_0x004e
            r8.close()     // Catch:{ Exception -> 0x008e }
            goto L_0x0091
        L_0x008e:
            r8.close()     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
        L_0x0091:
            com.google.android.finsky.billing.iab.C0821.f3553 = r7     // Catch:{ CursorIndexOutOfBoundsException -> 0x00ac, Throwable -> 0x0094 }
            goto L_0x00b0
        L_0x0094:
            r0 = move-exception
            com.google.android.finsky.billing.iab.C0821.f3553 = r7
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "LuckyPatcher-Error: getPackage "
            r1.append(r2)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.lp.C0987.m6060(r0)
            goto L_0x00b0
        L_0x00ac:
            r0 = move-exception
            r0.printStackTrace()
        L_0x00b0:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.finsky.billing.iab.C0821.m5381():java.util.ArrayList");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(9:0|1|2|3|4|5|6|7|11) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0050 */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m5382(com.google.android.finsky.billing.iab.C0824 r7) {
        /*
            r6 = this;
            java.lang.String r0 = "itemID"
            java.lang.String r1 = "'"
            r2 = 1
            r3 = 0
            com.google.android.finsky.billing.iab.C0821.f3554 = r2     // Catch:{ Exception -> 0x006f }
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ Exception -> 0x006f }
            r2.<init>()     // Catch:{ Exception -> 0x006f }
            java.lang.String r4 = r7.f3560     // Catch:{ Exception -> 0x006f }
            r2.put(r0, r4)     // Catch:{ Exception -> 0x006f }
            java.lang.String r4 = "Data"
            java.lang.String r5 = r7.f3561     // Catch:{ Exception -> 0x006f }
            r2.put(r4, r5)     // Catch:{ Exception -> 0x006f }
            java.lang.String r4 = "Signature"
            java.lang.String r5 = r7.f3562     // Catch:{ Exception -> 0x006f }
            r2.put(r4, r5)     // Catch:{ Exception -> 0x006f }
            java.lang.String r4 = "savePurchase"
            int r5 = r7.f3563     // Catch:{ Exception -> 0x006f }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x006f }
            r2.put(r4, r5)     // Catch:{ Exception -> 0x006f }
            java.lang.String r4 = "autobuy"
            int r7 = r7.f3564     // Catch:{ Exception -> 0x006f }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x006f }
            r2.put(r4, r7)     // Catch:{ Exception -> 0x006f }
            android.database.sqlite.SQLiteDatabase r7 = com.lp.C0987.f4518     // Catch:{ Exception -> 0x0050 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0050 }
            r4.<init>()     // Catch:{ Exception -> 0x0050 }
            r4.append(r1)     // Catch:{ Exception -> 0x0050 }
            java.lang.String r5 = com.google.android.finsky.billing.iab.C0821.f3551     // Catch:{ Exception -> 0x0050 }
            r4.append(r5)     // Catch:{ Exception -> 0x0050 }
            r4.append(r1)     // Catch:{ Exception -> 0x0050 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0050 }
            r7.insertOrThrow(r4, r0, r2)     // Catch:{ Exception -> 0x0050 }
            goto L_0x006a
        L_0x0050:
            android.database.sqlite.SQLiteDatabase r7 = com.lp.C0987.f4518     // Catch:{ Exception -> 0x006f }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006f }
            r0.<init>()     // Catch:{ Exception -> 0x006f }
            r0.append(r1)     // Catch:{ Exception -> 0x006f }
            java.lang.String r4 = com.google.android.finsky.billing.iab.C0821.f3551     // Catch:{ Exception -> 0x006f }
            r0.append(r4)     // Catch:{ Exception -> 0x006f }
            r0.append(r1)     // Catch:{ Exception -> 0x006f }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x006f }
            r1 = 0
            r7.replace(r0, r1, r2)     // Catch:{ Exception -> 0x006f }
        L_0x006a:
            com.google.android.finsky.billing.iab.C0821.f3554 = r3     // Catch:{ Exception -> 0x006f }
            com.google.android.finsky.billing.iab.C0821.f3554 = r3     // Catch:{ Exception -> 0x006f }
            goto L_0x0086
        L_0x006f:
            r7 = move-exception
            com.google.android.finsky.billing.iab.C0821.f3554 = r3
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "LuckyPatcher-Error: savePackage "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r7 = r0.toString()
            com.lp.C0987.m6060(r7)
        L_0x0086:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.finsky.billing.iab.C0821.m5382(com.google.android.finsky.billing.iab.ʼ):void");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m5383(C0824 r6) {
        try {
            C0987.f4518.delete("'" + f3551 + "'", "itemID = '" + r6.f3560 + "'", null);
        } catch (Exception e) {
            C0987.m6060((Object) ("LuckyPatcher-Error: deletePackage " + e));
        }
    }
}
