package com.google.android.finsky.services;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.ResolveInfo;
import android.os.IBinder;
import android.os.RemoteException;
import com.android.vending.licensing.C0770;
import com.android.vending.licensing.ILicensingService;
import com.chelpus.C0815;
import com.google.android.vending.licensing.ʻ.C0837;
import com.google.android.vending.licensing.ʻ.C0838;
import com.lp.C0987;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

public class LicensingService extends Service {

    /* renamed from: ˆ  reason: contains not printable characters */
    static ServiceConnection f3565;

    /* renamed from: ʻ  reason: contains not printable characters */
    ILicensingService f3566;

    /* renamed from: ʼ  reason: contains not printable characters */
    boolean f3567 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    public int f3568 = 255;

    /* renamed from: ʾ  reason: contains not printable characters */
    public String f3569 = BuildConfig.FLAVOR;

    /* renamed from: ʿ  reason: contains not printable characters */
    public String f3570 = BuildConfig.FLAVOR;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final ILicensingService.C0768 f3571 = new ILicensingService.C0768() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m5395(long j, String str, C0770 r11) {
            try {
                PackageInfo packageInfo = LicensingService.this.getPackageManager().getPackageInfo(str, 0);
                LicensingService.this.m5393(j, str);
                if (LicensingService.this.f3568 == 255 || LicensingService.this.f3568 != 0) {
                    new C0815("w").m5350(4000L);
                    r11.m4976(0, "0|" + j + "|" + str + "|" + packageInfo.versionCode + "|ANlOHQOShF3uJUwv3Ql+fbsgEG9FD35Hag==|" + (System.currentTimeMillis() + 31536000000L) + ":GR=10&VT=" + String.valueOf(System.currentTimeMillis() + Long.valueOf("31622400000").longValue()) + "&GT=" + String.valueOf(System.currentTimeMillis() + 31536000000L + 31536000000L), "hL9GqWwZL35OoLxZQN1EYmyylu3zmf8umnXW4P0EPqGjV0QcRYjD+NtiqoDEmxnnocvrqA7Z/0v+i0O4cwgOsD7/Tg3B1QI/ukA7ZUcibvFQUNoq7KjUWSg1Qn5MauaFFhAhZbuP840wnCuntxVDUkVJ6GDymDXLqhFG1LbZmNoPl6QjkschEBLVth1YtBxE4GnbVVI8Cq5LY7/F0N8d6EGLIISD6ekoD4lkhxq3nORsibX7kjmotyhLpO7THNMXvOeXeKhVp6dNSblOHp9tcL6l/NJY7sHPw/DBSxteW2hZ9y7yyaMxMAz+nTIN/V8gJXzeaRlmIXntJpQDEMz5pQ==");
                    return;
                }
                C0987.m6060((Object) "Transfer license from Google Play");
                r11.m4976(LicensingService.this.f3568, LicensingService.this.f3570, LicensingService.this.f3569);
            } catch (Exception unused) {
                try {
                    r11.m4976(258, null, null);
                } catch (RemoteException unused2) {
                }
            }
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5393(final long j, final String str) {
        boolean z;
        f3565 = new ServiceConnection() {
            public void onServiceDisconnected(ComponentName componentName) {
                C0987.m6060((Object) "Licensing service disconnected.");
                LicensingService.this.f3566 = null;
            }

            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                C0987.m6060((Object) "Licensing service try to connect.");
                LicensingService.this.f3566 = ILicensingService.C0768.m4974(iBinder);
                try {
                    C0987.m6060((Object) ("Calling checkLicense on service for " + str));
                    LicensingService.this.f3566.m4973(j, str, new C0770.C0771() {
                        /* renamed from: ʻ  reason: contains not printable characters */
                        public void m5394(int i, String str, String str2) {
                            LicensingService.this.f3568 = i;
                            int i2 = LicensingService.this.f3568;
                            LicensingService.this.f3570 = str;
                            LicensingService.this.f3569 = str2;
                            LicensingService.this.f3567 = true;
                        }
                    });
                } catch (RemoteException e) {
                    e.printStackTrace();
                    LicensingService.this.f3567 = true;
                }
            }
        };
        if (this.f3566 == null) {
            try {
                Intent intent = new Intent(new String(C0837.m5401("Y29tLmFuZHJvaWQudmVuZGluZy5saWNlbnNpbmcuSUxpY2Vuc2luZ1NlcnZpY2U=")));
                intent.setPackage("com.android.vending");
                intent.putExtra("xexe", "lp");
                int i = 0;
                if (C0987.m6068().queryIntentServices(intent, 0).isEmpty()) {
                    new C0815("w").m5350(5000L);
                }
                if (!C0987.m6068().queryIntentServices(intent, 0).isEmpty()) {
                    z = false;
                    for (ResolveInfo next : C0987.m6068().queryIntentServices(intent, 0)) {
                        if (next.serviceInfo.packageName != null && next.serviceInfo.packageName.equals("com.android.vending")) {
                            ComponentName componentName = new ComponentName(next.serviceInfo.packageName, next.serviceInfo.name);
                            Intent intent2 = new Intent();
                            intent2.setComponent(componentName);
                            intent2.putExtra("xexe", "lp");
                            z = C0987.m6072().bindService(intent2, f3565, 1);
                        }
                    }
                } else {
                    z = false;
                }
                if (z) {
                    while (true) {
                        if (this.f3567) {
                            break;
                        }
                        new C0815("w").m5350(2000L);
                        if (i > 10) {
                            break;
                        }
                        i++;
                    }
                    C0987.m6060((Object) "Stop licensing");
                    m5392();
                    return;
                }
                m5392();
            } catch (SecurityException unused) {
                m5392();
            } catch (C0838 e) {
                e.printStackTrace();
                m5392();
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m5392() {
        if (this.f3566 != null) {
            try {
                C0987.m6072().unbindService(f3565);
            } catch (IllegalArgumentException unused) {
            }
            this.f3566 = null;
        }
    }

    public IBinder onBind(Intent intent) {
        return this.f3571;
    }
}
