package com.google.android.gms.dynamite;

import com.google.android.gms.common.util.DynamiteApi;

public final class DynamiteModule {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final C0827 f3577 = new C0830();

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final C0827 f3578 = new C0832();

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final C0827 f3579 = new C0833();

    /* renamed from: ʾ  reason: contains not printable characters */
    public static final C0827 f3580 = new C0834();

    /* renamed from: ʿ  reason: contains not printable characters */
    private static int f3581 = -1;

    /* renamed from: ˆ  reason: contains not printable characters */
    private static final ThreadLocal<Object> f3582 = new ThreadLocal<>();

    /* renamed from: ˈ  reason: contains not printable characters */
    private static final C0827.C0828 f3583 = new C0829();

    /* renamed from: ˉ  reason: contains not printable characters */
    private static final C0827 f3584 = new C0831();

    /* renamed from: ˊ  reason: contains not printable characters */
    private static final C0827 f3585 = new C0835();

    @DynamiteApi
    public static class DynamiteLoaderClassLoader {
        public static ClassLoader sClassLoader;
    }

    /* renamed from: com.google.android.gms.dynamite.DynamiteModule$ʻ  reason: contains not printable characters */
    public interface C0827 {

        /* renamed from: com.google.android.gms.dynamite.DynamiteModule$ʻ$ʻ  reason: contains not printable characters */
        public interface C0828 {
        }
    }
}
