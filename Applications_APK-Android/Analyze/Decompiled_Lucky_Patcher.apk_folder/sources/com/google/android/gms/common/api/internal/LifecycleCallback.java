package com.google.android.gms.common.api.internal;

import android.support.annotation.Keep;

public class LifecycleCallback {
    @Keep
    private static C0826 getChimeraLifecycleFragmentImpl(C0825 r1) {
        throw new IllegalStateException("Method not available in SDK.");
    }
}
