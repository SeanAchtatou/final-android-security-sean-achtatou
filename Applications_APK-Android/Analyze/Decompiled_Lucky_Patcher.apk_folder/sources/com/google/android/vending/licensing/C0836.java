package com.google.android.vending.licensing;

import com.google.android.vending.licensing.ʻ.C0837;
import com.google.android.vending.licensing.ʻ.C0838;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.tukaani.xz.common.Util;

/* renamed from: com.google.android.vending.licensing.ʻ  reason: contains not printable characters */
/* compiled from: AESObfuscator */
public class C0836 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static byte[] f3586 = {16, 74, 71, -80, 32, 101, -47, 72, 117, -14, 0, -29, 70, 65, -12, 74};

    /* renamed from: ʼ  reason: contains not printable characters */
    public static String f3587 = "com.android.vending.licensing.AESObfuscator-1|";

    /* renamed from: ʽ  reason: contains not printable characters */
    private Cipher f3588;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Cipher f3589;

    public C0836(byte[] bArr, String str, String str2) {
        try {
            SecretKeyFactory instance = SecretKeyFactory.getInstance("PBEWITHSHAAND256BITAES-CBC-BC");
            SecretKeySpec secretKeySpec = new SecretKeySpec(instance.generateSecret(new PBEKeySpec((str + str2).toCharArray(), bArr, Util.BLOCK_HEADER_SIZE_MAX, 256)).getEncoded(), "AES");
            this.f3588 = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.f3588.init(1, secretKeySpec, new IvParameterSpec(f3586));
            this.f3589 = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.f3589.init(2, secretKeySpec, new IvParameterSpec(f3586));
        } catch (GeneralSecurityException e) {
            throw new RuntimeException("Invalid environment", e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m5396(String str, String str2) {
        if (str == null) {
            return null;
        }
        try {
            Cipher cipher = this.f3588;
            return C0837.m5399(cipher.doFinal((f3587 + str2 + str).getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Invalid environment", e);
        } catch (GeneralSecurityException e2) {
            throw new RuntimeException("Invalid environment", e2);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m5397(String str, String str2) {
        if (str == null) {
            return null;
        }
        try {
            String str3 = new String(this.f3589.doFinal(C0837.m5401(str)), "UTF-8");
            if (str3.indexOf(f3587 + str2) == 0) {
                return str3.substring(f3587.length() + str2.length(), str3.length());
            }
            throw new C0839("Header not found (invalid data or key):" + str);
        } catch (C0838 e) {
            throw new C0839(e.getMessage() + ":" + str);
        } catch (UnsupportedEncodingException e2) {
            throw new RuntimeException("Invalid environment", e2);
        } catch (BadPaddingException e3) {
            throw new C0839(e3.getMessage() + ":" + str);
        } catch (IllegalBlockSizeException e4) {
            throw new C0839(e4.getMessage() + ":" + str);
        }
    }
}
