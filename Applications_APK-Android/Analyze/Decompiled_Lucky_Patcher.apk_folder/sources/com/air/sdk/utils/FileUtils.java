package com.air.sdk.utils;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.CRC32;

public final class FileUtils {
    private static final int BUFFER_SIZE = 2048;

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long getCrc(java.io.File r3) {
        /*
            java.util.zip.CRC32 r0 = new java.util.zip.CRC32
            r0.<init>()
            r1 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ all -> 0x0020 }
            r2.<init>(r3)     // Catch:{ all -> 0x0020 }
        L_0x000b:
            int r3 = r2.read()     // Catch:{ all -> 0x001e }
            r1 = -1
            if (r3 == r1) goto L_0x0016
            r0.update(r3)     // Catch:{ all -> 0x001e }
            goto L_0x000b
        L_0x0016:
            long r0 = r0.getValue()     // Catch:{ all -> 0x001e }
            r2.close()
            return r0
        L_0x001e:
            r3 = move-exception
            goto L_0x0022
        L_0x0020:
            r3 = move-exception
            r2 = r1
        L_0x0022:
            if (r2 == 0) goto L_0x0027
            r2.close()
        L_0x0027:
            goto L_0x0029
        L_0x0028:
            throw r3
        L_0x0029:
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.air.sdk.utils.FileUtils.getCrc(java.io.File):long");
    }

    public static long copyContentFromStreamToStream(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[2048];
        CRC32 crc32 = new CRC32();
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
                crc32.update(bArr, 0, read);
            } else {
                outputStream.flush();
                return crc32.getValue();
            }
        }
    }
}
