package com.air.sdk.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;

public interface IBundle {
    void clear();

    boolean containsKey(String str);

    Object get(String str);

    boolean getBoolean(String str);

    boolean getBoolean(String str, boolean z);

    boolean[] getBooleanArray(String str);

    IBundle getBundle(String str);

    IBundle getBundle(String str, IBundle iBundle);

    IBundle[] getBundleArray(String str);

    ArrayList<IBundle> getBundleArrayList(String str);

    byte getByte(String str);

    Byte getByte(String str, byte b);

    byte[] getByteArray(String str);

    char getChar(String str);

    char getChar(String str, char c);

    char[] getCharArray(String str);

    double getDouble(String str);

    double getDouble(String str, double d);

    double[] getDoubleArray(String str);

    float getFloat(String str);

    float getFloat(String str, float f);

    float[] getFloatArray(String str);

    int getInt(String str);

    int getInt(String str, int i);

    int[] getIntArray(String str);

    ArrayList<Integer> getIntegerArrayList(String str);

    long getLong(String str);

    long getLong(String str, long j);

    long[] getLongArray(String str);

    Object getObject(String str);

    Object getObject(String str, Object obj);

    Object[] getObjectArray(String str);

    ArrayList<Object> getObjectArrayList(String str);

    Serializable getSerializable(String str);

    short getShort(String str);

    short getShort(String str, short s);

    short[] getShortArray(String str);

    String getString(String str);

    String getString(String str, String str2);

    String[] getStringArray(String str);

    ArrayList<String> getStringArrayList(String str);

    boolean isEmpty();

    Set<String> keySet();

    void putBoolean(String str, boolean z);

    void putBooleanArray(String str, boolean[] zArr);

    void putBundle(String str, IBundle iBundle);

    void putBundleArray(String str, IBundle[] iBundleArr);

    void putBundleArrayList(String str, ArrayList<IBundle> arrayList);

    void putByte(String str, byte b);

    void putByteArray(String str, byte[] bArr);

    void putChar(String str, char c);

    void putCharArray(String str, char[] cArr);

    void putDouble(String str, double d);

    void putDoubleArray(String str, double[] dArr);

    void putFloat(String str, float f);

    void putFloatArray(String str, float[] fArr);

    void putInt(String str, int i);

    void putIntArray(String str, int[] iArr);

    void putIntegerArrayList(String str, ArrayList<Integer> arrayList);

    void putLong(String str, long j);

    void putLongArray(String str, long[] jArr);

    void putObject(String str, Object obj);

    void putObjectArray(String str, Object[] objArr);

    void putObjectArrayList(String str, ArrayList<Object> arrayList);

    void putSerializable(String str, Serializable serializable);

    void putShort(String str, short s);

    void putShortArray(String str, short[] sArr);

    void putString(String str, String str2);

    void putStringArray(String str, String[] strArr);

    void putStringArrayList(String str, ArrayList<String> arrayList);

    void remove(String str);

    int size();
}
