package com.air.sdk.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public final class MemoryBundle implements IBundle {
    private Map<String, Object> memory;

    public MemoryBundle() {
        this.memory = new HashMap();
    }

    public MemoryBundle(IBundle iBundle) {
        if (iBundle != null) {
            for (String next : iBundle.keySet()) {
                putObject(next, iBundle.get(next));
            }
            return;
        }
        throw new IllegalArgumentException("Bundle for copy is null.");
    }

    public void clear() {
        this.memory.clear();
    }

    public boolean containsKey(String str) {
        return this.memory.containsKey(str);
    }

    public Object get(String str) {
        return this.memory.get(str);
    }

    public Object getObject(String str) {
        return get(str);
    }

    public Object getObject(String str, Object obj) {
        return getOrDefault(str, obj);
    }

    public ArrayList<Object> getObjectArrayList(String str) {
        return (ArrayList) get(str);
    }

    public Object[] getObjectArray(String str) {
        return (Object[]) get(str);
    }

    public IBundle getBundle(String str) {
        return (IBundle) get(str);
    }

    public IBundle getBundle(String str, IBundle iBundle) {
        return (IBundle) getOrDefault(str, iBundle);
    }

    public ArrayList<IBundle> getBundleArrayList(String str) {
        return (ArrayList) get(str);
    }

    public IBundle[] getBundleArray(String str) {
        return (IBundle[]) get(str);
    }

    public boolean getBoolean(String str) {
        return ((Boolean) get(str)).booleanValue();
    }

    public boolean getBoolean(String str, boolean z) {
        return ((Boolean) getOrDefault(str, Boolean.valueOf(z))).booleanValue();
    }

    public boolean[] getBooleanArray(String str) {
        return (boolean[]) get(str);
    }

    public byte getByte(String str) {
        return ((Byte) get(str)).byteValue();
    }

    public Byte getByte(String str, byte b) {
        return (Byte) getOrDefault(str, Byte.valueOf(b));
    }

    public byte[] getByteArray(String str) {
        return (byte[]) get(str);
    }

    public char getChar(String str) {
        return ((Character) get(str)).charValue();
    }

    public char getChar(String str, char c) {
        return ((Character) getOrDefault(str, Character.valueOf(c))).charValue();
    }

    public char[] getCharArray(String str) {
        return (char[]) get(str);
    }

    public double getDouble(String str) {
        return ((Double) get(str)).doubleValue();
    }

    public double getDouble(String str, double d) {
        return ((Double) getOrDefault(str, Double.valueOf(d))).doubleValue();
    }

    public double[] getDoubleArray(String str) {
        return (double[]) get(str);
    }

    public float getFloat(String str) {
        return ((Float) get(str)).floatValue();
    }

    public float getFloat(String str, float f) {
        return ((Float) getOrDefault(str, Float.valueOf(f))).floatValue();
    }

    public float[] getFloatArray(String str) {
        return (float[]) get(str);
    }

    public int getInt(String str) {
        return ((Integer) get(str)).intValue();
    }

    public int getInt(String str, int i) {
        return ((Integer) getOrDefault(str, Integer.valueOf(i))).intValue();
    }

    public int[] getIntArray(String str) {
        return (int[]) get(str);
    }

    public ArrayList<Integer> getIntegerArrayList(String str) {
        return (ArrayList) get(str);
    }

    public long getLong(String str) {
        return ((Long) get(str)).longValue();
    }

    public long getLong(String str, long j) {
        return ((Long) getOrDefault(str, Long.valueOf(j))).longValue();
    }

    public long[] getLongArray(String str) {
        return (long[]) get(str);
    }

    public Serializable getSerializable(String str) {
        return (Serializable) get(str);
    }

    public short getShort(String str) {
        return ((Short) get(str)).shortValue();
    }

    public short getShort(String str, short s) {
        return ((Short) getOrDefault(str, Short.valueOf(s))).shortValue();
    }

    public short[] getShortArray(String str) {
        return (short[]) get(str);
    }

    public String getString(String str) {
        return (String) get(str);
    }

    public String getString(String str, String str2) {
        return (String) getOrDefault(str, str2);
    }

    public String[] getStringArray(String str) {
        return (String[]) get(str);
    }

    public ArrayList<String> getStringArrayList(String str) {
        return (ArrayList) get(str);
    }

    public boolean isEmpty() {
        return this.memory.isEmpty();
    }

    public Set<String> keySet() {
        return this.memory.keySet();
    }

    public void putObject(String str, Object obj) {
        this.memory.put(str, obj);
    }

    public void putObjectArray(String str, Object[] objArr) {
        this.memory.put(str, objArr);
    }

    public void putObjectArrayList(String str, ArrayList<Object> arrayList) {
        this.memory.put(str, arrayList);
    }

    public void putBundle(String str, IBundle iBundle) {
        this.memory.put(str, iBundle);
    }

    public void putBundleArray(String str, IBundle[] iBundleArr) {
        this.memory.put(str, iBundleArr);
    }

    public void putBundleArrayList(String str, ArrayList<IBundle> arrayList) {
        this.memory.put(str, arrayList);
    }

    public void putBoolean(String str, boolean z) {
        this.memory.put(str, Boolean.valueOf(z));
    }

    public void putBooleanArray(String str, boolean[] zArr) {
        this.memory.put(str, zArr);
    }

    public void putByte(String str, byte b) {
        this.memory.put(str, Byte.valueOf(b));
    }

    public void putByteArray(String str, byte[] bArr) {
        this.memory.put(str, bArr);
    }

    public void putChar(String str, char c) {
        this.memory.put(str, Character.valueOf(c));
    }

    public void putCharArray(String str, char[] cArr) {
        this.memory.put(str, cArr);
    }

    public void putDouble(String str, double d) {
        this.memory.put(str, Double.valueOf(d));
    }

    public void putDoubleArray(String str, double[] dArr) {
        this.memory.put(str, dArr);
    }

    public void putFloat(String str, float f) {
        this.memory.put(str, Float.valueOf(f));
    }

    public void putFloatArray(String str, float[] fArr) {
        this.memory.put(str, fArr);
    }

    public void putInt(String str, int i) {
        this.memory.put(str, Integer.valueOf(i));
    }

    public void putIntArray(String str, int[] iArr) {
        this.memory.put(str, iArr);
    }

    public void putIntegerArrayList(String str, ArrayList<Integer> arrayList) {
        this.memory.put(str, arrayList);
    }

    public void putLong(String str, long j) {
        this.memory.put(str, Long.valueOf(j));
    }

    public void putLongArray(String str, long[] jArr) {
        this.memory.put(str, jArr);
    }

    public void putSerializable(String str, Serializable serializable) {
        this.memory.put(str, serializable);
    }

    public void putShort(String str, short s) {
        this.memory.put(str, Short.valueOf(s));
    }

    public void putShortArray(String str, short[] sArr) {
        this.memory.put(str, sArr);
    }

    public void putString(String str, String str2) {
        this.memory.put(str, str2);
    }

    public void putStringArray(String str, String[] strArr) {
        this.memory.put(str, strArr);
    }

    public void putStringArrayList(String str, ArrayList<String> arrayList) {
        this.memory.put(str, arrayList);
    }

    public void remove(String str) {
        this.memory.remove(str);
    }

    public int size() {
        return this.memory.size();
    }

    private Object getOrDefault(String str, Object obj) {
        Object obj2 = get(str);
        return obj2 == null ? obj : obj2;
    }
}
