package com.air.sdk.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;

@Deprecated
public final class AndroidBundle implements IBundle {
    @Deprecated
    public AndroidBundle() {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void clear() {
        throw makeDeprecatedException();
    }

    @Deprecated
    public boolean containsKey(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public Object get(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public Object getObject(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public Object getObject(String str, Object obj) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public ArrayList<Object> getObjectArrayList(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public Object[] getObjectArray(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public IBundle getBundle(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public IBundle getBundle(String str, IBundle iBundle) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public ArrayList<IBundle> getBundleArrayList(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public IBundle[] getBundleArray(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public boolean getBoolean(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public boolean getBoolean(String str, boolean z) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public boolean[] getBooleanArray(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public byte getByte(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public Byte getByte(String str, byte b) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public byte[] getByteArray(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public char getChar(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public char getChar(String str, char c) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public char[] getCharArray(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public double getDouble(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public double getDouble(String str, double d) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public double[] getDoubleArray(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public float getFloat(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public float getFloat(String str, float f) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public float[] getFloatArray(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public int getInt(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public int getInt(String str, int i) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public int[] getIntArray(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public ArrayList<Integer> getIntegerArrayList(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public long getLong(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public long getLong(String str, long j) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public long[] getLongArray(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public Serializable getSerializable(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public short getShort(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public short getShort(String str, short s) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public short[] getShortArray(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public String getString(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public String getString(String str, String str2) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public String[] getStringArray(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public ArrayList<String> getStringArrayList(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public boolean isEmpty() {
        throw makeDeprecatedException();
    }

    @Deprecated
    public Set<String> keySet() {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putObject(String str, Object obj) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putObjectArray(String str, Object[] objArr) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putObjectArrayList(String str, ArrayList<Object> arrayList) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putBundle(String str, IBundle iBundle) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putBundleArray(String str, IBundle[] iBundleArr) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putBundleArrayList(String str, ArrayList<IBundle> arrayList) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putBoolean(String str, boolean z) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putBooleanArray(String str, boolean[] zArr) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putByte(String str, byte b) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putByteArray(String str, byte[] bArr) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putChar(String str, char c) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putCharArray(String str, char[] cArr) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putDouble(String str, double d) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putDoubleArray(String str, double[] dArr) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putFloat(String str, float f) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putFloatArray(String str, float[] fArr) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putInt(String str, int i) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putIntArray(String str, int[] iArr) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putIntegerArrayList(String str, ArrayList<Integer> arrayList) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putLong(String str, long j) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putLongArray(String str, long[] jArr) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putSerializable(String str, Serializable serializable) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putShort(String str, short s) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putShortArray(String str, short[] sArr) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putString(String str, String str2) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putStringArray(String str, String[] strArr) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void putStringArrayList(String str, ArrayList<String> arrayList) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public void remove(String str) {
        throw makeDeprecatedException();
    }

    @Deprecated
    public int size() {
        throw makeDeprecatedException();
    }

    private RuntimeException makeDeprecatedException() {
        return new RuntimeException("Android bundle class is deprecated. Use MemoryBundle instead.");
    }
}
