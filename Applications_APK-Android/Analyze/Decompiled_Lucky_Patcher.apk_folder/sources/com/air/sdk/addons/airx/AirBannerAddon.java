package com.air.sdk.addons.airx;

import android.content.Context;
import android.widget.FrameLayout;
import com.air.sdk.injector.IAddon;

@Deprecated
public interface AirBannerAddon extends AirBanner, IAddon {
    void init(Context context);

    void setFrame(FrameLayout frameLayout);
}
