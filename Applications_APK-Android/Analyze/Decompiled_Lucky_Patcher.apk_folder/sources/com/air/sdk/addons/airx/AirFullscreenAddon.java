package com.air.sdk.addons.airx;

import android.content.Context;
import com.air.sdk.injector.IAddon;

@Deprecated
public interface AirFullscreenAddon extends AirFullscreen, IAddon {
    void init(Context context);
}
