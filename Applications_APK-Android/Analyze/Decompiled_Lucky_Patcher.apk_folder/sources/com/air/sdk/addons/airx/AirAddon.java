package com.air.sdk.addons.airx;

import com.air.sdk.injector.IAddon;
import com.air.sdk.utils.IBundle;

public interface AirAddon extends IAddon {
    void closeAd(int i);

    void init(IBundle iBundle);

    void loadAd(IBundle iBundle);

    void showAd(int i);
}
