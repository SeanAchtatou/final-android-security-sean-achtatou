package com.air.sdk.addons.airx;

import android.view.View;

public interface AirBannerListener extends AirListener {
    void onAdLoaded(View view);
}
