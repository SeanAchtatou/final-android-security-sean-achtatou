package com.air.sdk.addons.airx;

public interface AirListener {
    void onAdClicked();

    void onAdClosed();

    void onAdFailed(String str);

    void onLeaveApplication();
}
