package com.air.sdk.addons.airx;

public interface AirFullscreen {
    void closeAd();

    void loadAd();

    void setAdListener(AirFullscreenListener airFullscreenListener);

    void showAd();
}
