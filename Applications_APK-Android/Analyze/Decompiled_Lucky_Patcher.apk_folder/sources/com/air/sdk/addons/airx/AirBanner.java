package com.air.sdk.addons.airx;

public interface AirBanner {
    void closeAd();

    @Deprecated
    void loadAd();

    void loadAd(int i, int i2);

    void setAdListener(AirBannerListener airBannerListener);
}
