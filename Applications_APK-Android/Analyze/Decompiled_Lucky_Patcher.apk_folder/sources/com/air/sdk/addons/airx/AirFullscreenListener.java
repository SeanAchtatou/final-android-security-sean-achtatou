package com.air.sdk.addons.airx;

public interface AirFullscreenListener extends AirListener {
    void onAdLoaded();

    void onAdShown();
}
