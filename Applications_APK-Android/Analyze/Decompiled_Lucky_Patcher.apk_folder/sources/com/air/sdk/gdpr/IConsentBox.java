package com.air.sdk.gdpr;

public interface IConsentBox {
    void setEnableLaterButton(boolean z);

    void show();
}
