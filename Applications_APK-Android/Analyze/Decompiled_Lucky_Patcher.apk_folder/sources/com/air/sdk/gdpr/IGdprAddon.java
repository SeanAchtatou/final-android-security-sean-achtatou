package com.air.sdk.gdpr;

import android.app.Activity;
import com.air.sdk.injector.IAddon;

public interface IGdprAddon extends IAddon {
    IConsentBox getConsentBox();

    void init(Activity activity);
}
