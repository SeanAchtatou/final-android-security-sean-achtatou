package com.air.sdk.gdpr;

import android.app.Activity;
import android.webkit.WebView;
import com.air.sdk.injector.GdprAddon;

public class ConsentBox implements IConsentBox {
    public ConsentBox(Activity activity) {
        if (isWebViewAvailable(activity)) {
            GdprAddon.init(activity);
        }
    }

    public void setEnableLaterButton(boolean z) {
        GdprAddon.setEnableLaterButton(z);
    }

    public void show() {
        GdprAddon.showConsentBox();
    }

    private boolean isWebViewAvailable(Activity activity) {
        try {
            new WebView(activity.getApplication());
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }
}
