package com.air.sdk.injector;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class AirAdLoader extends ThreadPoolExecutor {
    private static final AirAdLoader INSTANCE = new AirAdLoader();

    private AirAdLoader() {
        super(1, 1, 0, TimeUnit.MILLISECONDS, new LinkedBlockingQueue(), new MinPriorityThreadFactory());
    }

    private static class MinPriorityThreadFactory implements ThreadFactory {
        private MinPriorityThreadFactory() {
        }

        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable);
            thread.setPriority(1);
            return thread;
        }
    }

    public static AirAdLoader getInstance() {
        return INSTANCE;
    }
}
