package com.air.sdk.injector;

interface IKitRepoCache {
    void backupCurrentVersionOfKitRepoModule();

    byte[] getBuiltinPack();

    long getDefaultFileVersionFromCache();

    String getKitRepoClassNameFromCache();

    byte[] getKitRepoModuleFileFromCache();

    long getKitRepoModuleVersionFromCache();

    void putBuiltinPack(byte[] bArr);

    void putDefaultFileVersionToCache(long j);

    void putKitRepoModuleToCache(String str, long j, byte[] bArr);

    void removeBuiltinPack();

    void removeKitRepoModuleBackup();

    void restoreKitRepoModuleFromBackup();
}
