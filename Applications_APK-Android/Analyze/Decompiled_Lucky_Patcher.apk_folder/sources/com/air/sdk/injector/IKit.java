package com.air.sdk.injector;

import android.content.Context;
import com.air.sdk.utils.IBundle;

public interface IKit {
    void onStart(Context context, IBundle iBundle);

    void onStop(IBundle iBundle);
}
