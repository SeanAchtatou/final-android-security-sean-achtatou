package com.air.sdk.injector;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Process;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

final class Verifier {
    private static boolean checkRootedDevice() {
        return false;
    }

    Verifier() {
    }

    static boolean verify(Context context) {
        return checkForMainProcess(context) || checkRootedDevice();
    }

    private static boolean checkForMainProcess(Context context) {
        int myPid = Process.myPid();
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        if (activityManager == null) {
            return false;
        }
        for (ActivityManager.RunningAppProcessInfo next : activityManager.getRunningAppProcesses()) {
            if (next.pid == myPid && !next.processName.equals(context.getApplicationInfo().processName)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkSuExists() {
        boolean z = false;
        Process process = null;
        try {
            Process exec = Runtime.getRuntime().exec(new String[]{"which", "su"});
            if (new BufferedReader(new InputStreamReader(exec.getInputStream())).readLine() != null) {
                z = true;
            }
            if (exec != null) {
                exec.destroy();
            }
            return z;
        } catch (IOException unused) {
            if (process != null) {
                process.destroy();
            }
            return false;
        } catch (Throwable th) {
            if (process != null) {
                process.destroy();
            }
            throw th;
        }
    }

    private static boolean checkForBinary() {
        for (String file : new String[]{"/data/local/su", "/data/local/bin/su", "/data/local/xbin/su", "/sbin/su", "/su/bin/su", "/system/bin/su", "/system/bin/.ext/su", "/system/bin/failsafe/su", "/system/sd/xbin/su", "/system/usr/we-need-root/su", "/system/xbin/su", "/cache/su", "/data/su", "/dev/su", "/system/app/Superuser.apk"}) {
            if (new File(file).exists()) {
                return true;
            }
        }
        return false;
    }
}
