package com.air.sdk.injector;

import android.util.Log;
import com.air.sdk.exceptions.AirpushSdkNotInitException;
import com.air.sdk.injector.IAddon;
import com.air.sdk.utils.IBundle;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class AbstractKit<T extends IAddon> {
    /* access modifiers changed from: private */
    public AtomicBoolean getKitInProgress = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public WeakReference<T> kitInstance = new WeakReference<>(null);
    private T linkedDefaultInstance;
    private String linkedKitName;

    private interface IGetKitFromRepoTaskListener {
        void onError(Throwable th);

        void onKit(IKit iKit, IKitRepoDispatcher iKitRepoDispatcher, IBundle iBundle);
    }

    /* access modifiers changed from: protected */
    public abstract T getLinkedDefaultInstance();

    /* access modifiers changed from: protected */
    public abstract String getLinkedKitName();

    private T getCurrentInstance() {
        T t;
        WeakReference<T> weakReference = this.kitInstance;
        if (weakReference == null || (t = (IAddon) weakReference.get()) == null) {
            return null;
        }
        return t;
    }

    /* access modifiers changed from: protected */
    public final T getKit(String str, T t) {
        IBundle iBundle;
        T currentInstance = getCurrentInstance();
        if (currentInstance != null) {
            if (currentInstance != t) {
                return currentInstance;
            }
            iBundle = t.getDefaultInstanceState();
        } else if (t != null) {
            this.kitInstance = new WeakReference<>(t);
            iBundle = t.getDefaultInstanceState();
        } else {
            throw new IllegalArgumentException("Default instance of kit can't be null");
        }
        try {
            getKitFromRepository(str, iBundle);
            T currentInstance2 = getCurrentInstance();
            return currentInstance2 != null ? currentInstance2 : t;
        } catch (Exception unused) {
        }
    }

    private void getKitFromRepository(String str, IBundle iBundle) {
        if (!this.getKitInProgress.get() && this.getKitInProgress.compareAndSet(false, true)) {
            try {
                IKitRepoDispatcher kitRepoDispatcher = AirpushSdk.getKitRepoDispatcher();
                if (kitRepoDispatcher != null) {
                    Thread thread = new Thread(new GetKitFromRepoTask(str, iBundle, kitRepoDispatcher, new IGetKitFromRepoTaskListener() {
                        public void onError(Throwable th) {
                            AbstractKit.this.getKitInProgress.getAndSet(false);
                        }

                        public void onKit(IKit iKit, IKitRepoDispatcher iKitRepoDispatcher, IBundle iBundle) {
                            IAddon iAddon = (IAddon) iKit;
                            WeakReference unused = AbstractKit.this.kitInstance = new WeakReference(iAddon);
                            if (iAddon instanceof IAddonDefaultInstanceStateProvider) {
                                ((IAddonDefaultInstanceStateProvider) iAddon).onProvideDefaultInstanceState(iBundle);
                            }
                            if (iAddon instanceof IAddonWithClasses) {
                                ((IAddonWithClasses) iAddon).registerClasses(iKitRepoDispatcher.getClassLoaderPatcher());
                            }
                            AbstractKit.this.getKitInProgress.getAndSet(false);
                        }
                    }));
                    thread.setPriority(1);
                    thread.setName("ThrGKFR");
                    thread.start();
                    return;
                }
                Log.e("AIRSDK", "AirSDK initialization failed: error code 0");
                throw new AirpushSdkNotInitException();
            } catch (Throwable th) {
                this.getKitInProgress.getAndSet(false);
                throw new Exception("Can't get kit from repository", th);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final T getLinkedKit() {
        if (this.linkedKitName == null) {
            this.linkedKitName = getLinkedKitName();
            if (this.linkedKitName == null) {
                throw new IllegalArgumentException("Linked kit name is null.");
            }
        }
        if (this.linkedDefaultInstance == null) {
            this.linkedDefaultInstance = getLinkedDefaultInstance();
        }
        return getKit(this.linkedKitName, this.linkedDefaultInstance);
    }

    private static class GetKitFromRepoTask implements Runnable {
        private final String kitName;
        private IKitRepoDispatcher kitRepoDispatcher;
        private IGetKitFromRepoTaskListener listener;
        private final IBundle savedState;

        GetKitFromRepoTask(String str, IBundle iBundle, IKitRepoDispatcher iKitRepoDispatcher, IGetKitFromRepoTaskListener iGetKitFromRepoTaskListener) {
            this.kitName = str;
            this.savedState = iBundle;
            this.kitRepoDispatcher = iKitRepoDispatcher;
            this.listener = iGetKitFromRepoTaskListener;
        }

        public void run() {
            try {
                IKit kit = this.kitRepoDispatcher.getKit(this.kitName, this.savedState);
                if (this.listener != null) {
                    this.listener.onKit(kit, this.kitRepoDispatcher, this.savedState);
                }
            } catch (Throwable th) {
                IGetKitFromRepoTaskListener iGetKitFromRepoTaskListener = this.listener;
                if (iGetKitFromRepoTaskListener != null) {
                    iGetKitFromRepoTaskListener.onError(th);
                }
            }
        }
    }
}
