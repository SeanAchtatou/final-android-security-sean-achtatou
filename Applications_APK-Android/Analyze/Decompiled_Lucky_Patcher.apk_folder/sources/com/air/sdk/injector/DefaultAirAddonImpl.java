package com.air.sdk.injector;

import com.air.sdk.addons.airx.AirAddon;
import com.air.sdk.utils.IBundle;
import com.air.sdk.utils.MemoryBundle;
import java.util.ArrayList;

class DefaultAirAddonImpl implements AirAddon {
    private IBundle savedState = new MemoryBundle();

    public void init(IBundle iBundle) {
    }

    DefaultAirAddonImpl() {
    }

    public void loadAd(IBundle iBundle) {
        this.savedState.putBundleArrayList("load", appendMessage(iBundle, "load"));
    }

    public void showAd(int i) {
        this.savedState.putInt("show", i);
    }

    public void closeAd(int i) {
        this.savedState.putInt("close", i);
    }

    private ArrayList<IBundle> appendMessage(IBundle iBundle, String str) {
        ArrayList<IBundle> orCreateQueue = getOrCreateQueue(str);
        orCreateQueue.add(iBundle);
        return orCreateQueue;
    }

    private ArrayList<IBundle> getOrCreateQueue(String str) {
        ArrayList<IBundle> cachedBundles = getCachedBundles(str);
        return listIsEmpty(cachedBundles) ? new ArrayList<>() : cachedBundles;
    }

    private ArrayList<IBundle> getCachedBundles(String str) {
        return this.savedState.getBundleArrayList(str);
    }

    private boolean listIsEmpty(ArrayList<IBundle> arrayList) {
        return arrayList == null || arrayList.isEmpty();
    }

    public IBundle getDefaultInstanceState() {
        return this.savedState;
    }
}
