package com.air.sdk.injector;

import android.content.Context;
import android.widget.FrameLayout;
import com.air.sdk.addons.airx.AirListener;
import com.air.sdk.utils.IBundle;
import com.air.sdk.utils.MemoryBundle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

class AirAd {
    private List<AirListener> references = new ArrayList();

    AirAd() {
    }

    /* access modifiers changed from: package-private */
    public IBundle getDefaultLoadBundle(Context context, AirListener airListener, HashMap<String, String> hashMap) {
        int hashCode = airListener.hashCode();
        MemoryBundle memoryBundle = new MemoryBundle();
        memoryBundle.putObject("ctx", context);
        memoryBundle.putInt("referenceId", hashCode);
        memoryBundle.putObject("listener", airListener);
        memoryBundle.putObject("extras", putTraceIdIfNotExists(hashMap));
        this.references.add(airListener);
        return memoryBundle;
    }

    private HashMap<String, String> putTraceIdIfNotExists(HashMap<String, String> hashMap) {
        if (hashMap == null) {
            hashMap = new HashMap<>();
        }
        if (!hashMap.containsKey("traceId")) {
            hashMap.put("traceId", String.valueOf(UUID.randomUUID().getMostSignificantBits()));
        }
        return hashMap;
    }

    /* access modifiers changed from: package-private */
    public IBundle getLoadBannerBundle(Context context, AirListener airListener, FrameLayout frameLayout, int i, int i2, HashMap<String, String> hashMap) {
        IBundle defaultLoadBundle = getDefaultLoadBundle(context, airListener, hashMap);
        defaultLoadBundle.putObject("frame", frameLayout);
        defaultLoadBundle.putInt("ad_container_width", i);
        defaultLoadBundle.putInt("ad_container_height", i2);
        return defaultLoadBundle;
    }

    /* access modifiers changed from: package-private */
    public List<AirListener> getReferences() {
        return this.references;
    }
}
