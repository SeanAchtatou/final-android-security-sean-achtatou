package com.air.sdk.injector;

interface ISystemClassLoaderPatcher {
    void injectPatch(Object obj);

    void setReferencedClassLoader(Object obj);

    void takeOutPatch();
}
