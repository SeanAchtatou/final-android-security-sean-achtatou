package com.air.sdk.injector;

import com.air.sdk.utils.IBundle;

public interface IAddonDefaultInstanceStateProvider {
    void onProvideDefaultInstanceState(IBundle iBundle);
}
