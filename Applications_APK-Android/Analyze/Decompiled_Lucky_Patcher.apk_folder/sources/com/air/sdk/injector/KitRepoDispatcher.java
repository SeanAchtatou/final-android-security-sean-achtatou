package com.air.sdk.injector;

import android.content.Context;
import com.air.sdk.exceptions.KitNotFoundException;
import com.air.sdk.utils.IBundle;
import com.air.sdk.utils.MemoryBundle;
import java.io.File;
import java.io.IOException;
import java.util.zip.ZipFile;

final class KitRepoDispatcher implements IKitRepoDispatcher {
    private Context context;
    private volatile IKitRepo kitRepo;
    /* access modifiers changed from: private */
    public IKitRepoCache kitRepoCache;
    private volatile ClassLoader kitRepoClassLoader;
    private IBundle lastSavedState = null;
    private ISystemClassLoaderPatcher systemClassLoaderPatcher;

    KitRepoDispatcher(Context context2, IKitRepoCache iKitRepoCache) {
        this.kitRepoCache = iKitRepoCache;
        this.context = context2;
    }

    public IKit getKit(String str, IBundle iBundle) {
        if (this.kitRepo == null) {
            try {
                loadInstanceOfKitRepo();
            } catch (Throwable th) {
                throw new KitNotFoundException(str, th);
            }
        }
        try {
            return this.kitRepo.getKit(str, iBundle);
        } catch (Exception e) {
            throw new KitNotFoundException(str, e);
        }
    }

    public void initKitRepoDispatcher() {
        patchSystemClassLoader();
        prepareKitRepoClassloader();
        try {
            loadInstanceOfKitRepo();
        } catch (Throwable th) {
            throw new Exception(th);
        }
    }

    public IPatcher getClassLoaderPatcher() {
        return (IPatcher) this.systemClassLoaderPatcher;
    }

    /* access modifiers changed from: private */
    public void updateKitRepoModule(String str, long j, byte[] bArr) {
        this.kitRepoCache.backupCurrentVersionOfKitRepoModule();
        this.kitRepoCache.putKitRepoModuleToCache(str, j, bArr);
        try {
            reloadKitRepoClassLoader();
            this.kitRepoCache.removeKitRepoModuleBackup();
        } catch (IOException unused) {
            this.kitRepoCache.restoreKitRepoModuleFromBackup();
            reloadKitRepoClassLoader();
        }
    }

    private void patchSystemClassLoader() {
        if (this.systemClassLoaderPatcher == null) {
            this.systemClassLoaderPatcher = new ClassLoaderParentInjection(this.kitRepoClassLoader);
            this.systemClassLoaderPatcher.injectPatch(this.context.getClassLoader());
        }
    }

    private void prepareKitRepoClassloader() {
        if (this.kitRepoClassLoader == null) {
            this.kitRepoClassLoader = makeNewClassLoaderForKitRepo();
            ISystemClassLoaderPatcher iSystemClassLoaderPatcher = this.systemClassLoaderPatcher;
            if (iSystemClassLoaderPatcher != null) {
                iSystemClassLoaderPatcher.setReferencedClassLoader(this.kitRepoClassLoader);
            }
        }
    }

    private synchronized void loadInstanceOfKitRepo() {
        if (this.kitRepo != null) {
            throw new Exception("Kit repo instance is not null, but load new instance has been invoked.");
        } else if (this.kitRepoClassLoader != null) {
            IKit iKit = (IKit) Class.forName(this.kitRepoCache.getKitRepoClassNameFromCache(), true, this.kitRepoClassLoader).newInstance();
            if (iKit instanceof IKitRepo) {
                this.kitRepo = (IKitRepo) iKit;
                this.kitRepo.setup(this.context, new IKitRepoController() {
                    public void update(String str, long j, byte[] bArr) {
                        try {
                            KitRepoDispatcher.this.updateKitRepoModule(str, j, bArr);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    public long getVersion() {
                        return KitRepoDispatcher.this.kitRepoCache.getKitRepoModuleVersionFromCache();
                    }
                });
                byte[] builtinPack = this.kitRepoCache.getBuiltinPack();
                if (builtinPack != null) {
                    this.kitRepo.applyDefaultKits(builtinPack, new Runnable() {
                        public void run() {
                            KitRepoDispatcher.this.kitRepoCache.removeBuiltinPack();
                        }
                    });
                }
                iKit.onStart(this.context, this.lastSavedState);
                this.lastSavedState = null;
            } else {
                throw new IOException("KitRepo in cache is not a KitRepo");
            }
        } else {
            throw new Exception("Kit repo class loader is null.");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x00f0 A[Catch:{ Exception -> 0x00e1, all -> 0x00f4 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.ClassLoader makeNewClassLoaderForKitRepo() {
        /*
            r8 = this;
            com.air.sdk.injector.IKitRepoCache r0 = r8.kitRepoCache
            byte[] r0 = r0.getKitRepoModuleFileFromCache()
            java.io.File r1 = new java.io.File
            android.content.Context r2 = r8.context
            java.io.File r2 = r2.getCacheDir()
            java.util.UUID r3 = java.util.UUID.randomUUID()
            java.lang.String r3 = r3.toString()
            r1.<init>(r2, r3)
            java.io.File r2 = new java.io.File
            java.util.UUID r3 = java.util.UUID.randomUUID()
            java.lang.String r3 = r3.toString()
            r2.<init>(r1, r3)
            java.io.File r3 = new java.io.File
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.util.UUID r5 = java.util.UUID.randomUUID()
            java.lang.String r5 = r5.toString()
            r4.append(r5)
            java.lang.String r5 = ".jar"
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.<init>(r1, r4)
            boolean r4 = r1.exists()     // Catch:{ all -> 0x00f4 }
            if (r4 != 0) goto L_0x006c
            boolean r4 = r1.mkdirs()     // Catch:{ all -> 0x00f4 }
            if (r4 == 0) goto L_0x0051
            goto L_0x006c
        L_0x0051:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x00f4 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f4 }
            r4.<init>()     // Catch:{ all -> 0x00f4 }
            java.lang.String r5 = "Can't create cch folder "
            r4.append(r5)     // Catch:{ all -> 0x00f4 }
            java.lang.String r5 = r1.getAbsolutePath()     // Catch:{ all -> 0x00f4 }
            r4.append(r5)     // Catch:{ all -> 0x00f4 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00f4 }
            r0.<init>(r4)     // Catch:{ all -> 0x00f4 }
            throw r0     // Catch:{ all -> 0x00f4 }
        L_0x006c:
            boolean r4 = r2.exists()     // Catch:{ all -> 0x00f4 }
            if (r4 != 0) goto L_0x0094
            boolean r4 = r2.mkdirs()     // Catch:{ all -> 0x00f4 }
            if (r4 == 0) goto L_0x0079
            goto L_0x0094
        L_0x0079:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x00f4 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f4 }
            r4.<init>()     // Catch:{ all -> 0x00f4 }
            java.lang.String r5 = "Can't create opt folder "
            r4.append(r5)     // Catch:{ all -> 0x00f4 }
            java.lang.String r5 = r2.getAbsolutePath()     // Catch:{ all -> 0x00f4 }
            r4.append(r5)     // Catch:{ all -> 0x00f4 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00f4 }
            r0.<init>(r4)     // Catch:{ all -> 0x00f4 }
            throw r0     // Catch:{ all -> 0x00f4 }
        L_0x0094:
            r4 = 0
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ all -> 0x00ec }
            r5.<init>(r3)     // Catch:{ all -> 0x00ec }
            r6 = 80
            r5.write(r6)     // Catch:{ all -> 0x00ea }
            r6 = 75
            r5.write(r6)     // Catch:{ all -> 0x00ea }
            r5.write(r0)     // Catch:{ all -> 0x00ea }
            r5.flush()     // Catch:{ all -> 0x00ea }
            r5.close()     // Catch:{ all -> 0x00f4 }
            r8.validateZipFile(r3)     // Catch:{ Exception -> 0x00e1 }
            dalvik.system.DexClassLoader r0 = new dalvik.system.DexClassLoader     // Catch:{ all -> 0x00f4 }
            java.lang.String r5 = r3.getAbsolutePath()     // Catch:{ all -> 0x00f4 }
            java.lang.String r6 = r2.getAbsolutePath()     // Catch:{ all -> 0x00f4 }
            java.lang.Class r7 = r8.getClass()     // Catch:{ all -> 0x00f4 }
            java.lang.ClassLoader r7 = r7.getClassLoader()     // Catch:{ all -> 0x00f4 }
            r0.<init>(r5, r6, r4, r7)     // Catch:{ all -> 0x00f4 }
            boolean r4 = r3.exists()
            if (r4 == 0) goto L_0x00ce
            r3.delete()
        L_0x00ce:
            boolean r3 = r2.exists()
            if (r3 == 0) goto L_0x00d7
            r2.delete()
        L_0x00d7:
            boolean r2 = r1.exists()
            if (r2 == 0) goto L_0x00e0
            r1.delete()
        L_0x00e0:
            return r0
        L_0x00e1:
            r0 = move-exception
            java.lang.RuntimeException r4 = new java.lang.RuntimeException     // Catch:{ all -> 0x00f4 }
            java.lang.String r5 = "Zip file is not valid"
            r4.<init>(r5, r0)     // Catch:{ all -> 0x00f4 }
            throw r4     // Catch:{ all -> 0x00f4 }
        L_0x00ea:
            r0 = move-exception
            goto L_0x00ee
        L_0x00ec:
            r0 = move-exception
            r5 = r4
        L_0x00ee:
            if (r5 == 0) goto L_0x00f3
            r5.close()     // Catch:{ all -> 0x00f4 }
        L_0x00f3:
            throw r0     // Catch:{ all -> 0x00f4 }
        L_0x00f4:
            r0 = move-exception
            boolean r4 = r3.exists()
            if (r4 == 0) goto L_0x00fe
            r3.delete()
        L_0x00fe:
            boolean r3 = r2.exists()
            if (r3 == 0) goto L_0x0107
            r2.delete()
        L_0x0107:
            boolean r2 = r1.exists()
            if (r2 == 0) goto L_0x0110
            r1.delete()
        L_0x0110:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.air.sdk.injector.KitRepoDispatcher.makeNewClassLoaderForKitRepo():java.lang.ClassLoader");
    }

    private void reloadKitRepoClassLoader() {
        this.lastSavedState = unloadKitRepoClassLoader();
        prepareKitRepoClassloader();
        try {
            loadInstanceOfKitRepo();
        } catch (Throwable th) {
            throw new IOException("Failed to reload kit-repo-module", th);
        }
    }

    private IBundle unloadKitRepoClassLoader() {
        IBundle unloadKitRepoInstance = unloadKitRepoInstance();
        this.kitRepoClassLoader = null;
        ISystemClassLoaderPatcher iSystemClassLoaderPatcher = this.systemClassLoaderPatcher;
        if (iSystemClassLoaderPatcher != null) {
            iSystemClassLoaderPatcher.setReferencedClassLoader(this.kitRepoClassLoader);
        }
        return unloadKitRepoInstance;
    }

    private synchronized IBundle unloadKitRepoInstance() {
        if (this.kitRepo == null) {
            return null;
        }
        MemoryBundle memoryBundle = new MemoryBundle();
        ((IKit) this.kitRepo).onStop(memoryBundle);
        this.kitRepo = null;
        return memoryBundle;
    }

    private void validateZipFile(File file) {
        try {
            new ZipFile(file).close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
