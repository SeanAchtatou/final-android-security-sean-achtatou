package com.air.sdk.injector;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import com.air.sdk.addons.airx.AirAddon;
import com.air.sdk.addons.airx.AirBannerListener;
import com.air.sdk.addons.airx.AirListener;
import java.util.HashMap;
import java.util.List;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

public class AirBanner extends FrameLayout implements com.air.sdk.addons.airx.AirBanner {
    public static final int BANNER = 1;
    public static final int LEADERBOARD = 3;
    public static final int MREC = 2;
    private AirBannerListener adListener;
    private AirAd airAd;
    private HashMap<String, String> extras;
    private int height = 50;
    private BannerListener listener;
    private int width = 320;

    public AirBanner(Context context) {
        super(context);
        init();
    }

    public AirBanner(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public AirBanner(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    public AirBanner(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        init();
    }

    private void init() {
        try {
            this.airAd = new AirAd();
        } catch (Throwable unused) {
        }
    }

    public void setAdListener(AirBannerListener airBannerListener) {
        this.adListener = airBannerListener;
    }

    @Deprecated
    public void loadAd() {
        loadAd(1);
    }

    public void loadAd(int i) {
        if (i == 1 || i == 2 || i == 3) {
            if (i == 1) {
                this.width = 320;
                this.height = 50;
            } else if (i == 2) {
                this.width = 300;
                this.height = 250;
            } else if (i != 3) {
                this.width = 320;
                this.height = 50;
            } else {
                this.width = 728;
                this.height = 90;
            }
            loadAd(this.width, this.height);
            return;
        }
        onFailed("Wrong ad size");
    }

    public void loadAd(int i, int i2) {
        this.width = i;
        this.height = i2;
        if (i < 300 || i2 < 50) {
            onFailed("Wrong ad size");
        } else {
            AirAdLoader.getInstance().submit(new Runnable() {
                public void run() {
                    AirBanner.this.loadInternal();
                }
            });
        }
    }

    private void onFailed(String str) {
        try {
            if (this.listener != null) {
                this.listener.onAdFailed(str);
            }
        } catch (Throwable unused) {
        }
    }

    /* access modifiers changed from: private */
    public void loadInternal() {
        try {
            if (this.airAd != null) {
                this.listener = new BannerListener(this.adListener);
                getAddon().loadAd(this.airAd.getLoadBannerBundle(getContext(), this.listener, this, this.width, this.height, this.extras));
            }
        } catch (Throwable unused) {
            logEror("Load banner failed");
        }
    }

    private AirAddon getAddon() {
        AirAddon airAddon;
        AirBannerAddonImpl airBannerAddonImpl = new AirBannerAddonImpl();
        do {
            airAddon = (AirAddon) airBannerAddonImpl.getLinkedKit();
        } while (airAddon instanceof DefaultAirAddonImpl);
        return airAddon;
    }

    public void closeAd() {
        try {
            makeCloseAllAd();
            this.adListener = null;
            this.listener = null;
            this.extras = null;
        } catch (Throwable unused) {
            logEror("Close banner failed");
        }
    }

    private void makeCloseAllAd() {
        try {
            if (this.airAd != null) {
                List<AirListener> references = this.airAd.getReferences();
                for (int i = 0; i < references.size(); i++) {
                    BannerListener bannerListener = (BannerListener) references.get(i);
                    makeCloseAd(bannerListener.hashCode());
                    invalidateListener(bannerListener);
                }
            }
        } catch (Throwable unused) {
        }
    }

    /* access modifiers changed from: private */
    public void makeCloseAd(int i) {
        try {
            getAddon().closeAd(i);
        } catch (Throwable unused) {
            logEror("Close banner failed: " + i);
        }
    }

    /* access modifiers changed from: private */
    public void invalidateListener(BannerListener bannerListener) {
        if (bannerListener != null) {
            try {
                bannerListener.invalidate();
                if (invalidatingActiveListener(bannerListener)) {
                    this.listener = null;
                }
            } catch (Throwable unused) {
            }
        }
    }

    private boolean invalidatingActiveListener(BannerListener bannerListener) {
        return bannerListener != null && bannerListener.hashCode() == bannerListener.hashCode();
    }

    private void logEror(String str) {
        Log.e(BuildConfig.FLAVOR, str);
    }

    public void setExtras(HashMap<String, String> hashMap) {
        if (hashMap != null) {
            this.extras.putAll(hashMap);
        } else {
            this.extras = hashMap;
        }
    }

    public void enableCloseButton() {
        if (this.extras == null) {
            this.extras = new HashMap<>();
        }
        this.extras.put("closeButtonState", "enabled");
    }

    private static class BannerListener implements AirBannerListener {
        private AirBanner airBanner;
        private AirBannerListener listener;

        private BannerListener(AirBanner airBanner2, AirBannerListener airBannerListener) {
            this.listener = airBannerListener;
            this.airBanner = airBanner2;
        }

        public void onAdFailed(String str) {
            try {
                if (this.listener != null) {
                    this.listener.onAdFailed(str);
                }
                closeAd();
            } catch (Throwable unused) {
            }
        }

        public void onAdLoaded(View view) {
            try {
                if (this.listener != null) {
                    this.listener.onAdLoaded(view);
                }
            } catch (Throwable unused) {
            }
        }

        public void onAdClosed() {
            try {
                if (this.listener != null) {
                    this.listener.onAdClosed();
                }
                closeAd();
            } catch (Throwable unused) {
            }
        }

        public void onAdClicked() {
            try {
                if (this.listener != null) {
                    this.listener.onAdClicked();
                }
            } catch (Throwable unused) {
            }
        }

        public void onLeaveApplication() {
            try {
                if (this.listener != null) {
                    this.listener.onLeaveApplication();
                }
            } catch (Throwable unused) {
            }
        }

        private void closeAd() {
            try {
                if (this.airBanner != null) {
                    this.airBanner.makeCloseAd(hashCode());
                    this.airBanner.invalidateListener(this);
                }
            } catch (Throwable unused) {
            }
        }

        /* access modifiers changed from: private */
        public void invalidate() {
            this.listener = null;
            this.airBanner = null;
        }
    }
}
