package com.air.sdk.injector;

import com.air.sdk.utils.IBundle;

public interface IAddon {
    IBundle getDefaultInstanceState();
}
