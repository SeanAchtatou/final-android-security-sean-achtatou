package com.air.sdk.injector;

import java.io.InputStream;

public interface IBuiltinPackProvider {
    InputStream getBuiltinPack();
}
