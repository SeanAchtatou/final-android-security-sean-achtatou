package com.air.sdk.injector;

import java.io.IOException;
import java.io.InputStream;

final class AdvInputStream extends InputStream {
    private byte[] advancedBytes;
    private int cnt = 0;
    private InputStream linkedInputStream;

    AdvInputStream(InputStream inputStream, byte[] bArr) {
        this.linkedInputStream = inputStream;
        this.advancedBytes = bArr;
    }

    public int read() {
        int i;
        byte[] bArr = this.advancedBytes;
        if (bArr == null || bArr.length <= (i = this.cnt)) {
            InputStream inputStream = this.linkedInputStream;
            if (inputStream != null) {
                return inputStream.read();
            }
            throw new IOException("Stream is not defined");
        }
        this.cnt = i + 1;
        return bArr[i];
    }

    public void close() {
        InputStream inputStream = this.linkedInputStream;
        if (inputStream != null) {
            inputStream.close();
        }
        super.close();
    }
}
