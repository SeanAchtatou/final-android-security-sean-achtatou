package com.air.sdk.injector;

public interface IPatcher {
    void registerClass(Class<?> cls);

    void unregisterClass(Class<?> cls);
}
