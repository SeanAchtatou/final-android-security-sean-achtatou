package com.air.sdk.injector;

import com.air.sdk.utils.IBundle;

interface IKitRepoDispatcher {
    IPatcher getClassLoaderPatcher();

    IKit getKit(String str, IBundle iBundle);

    void initKitRepoDispatcher();
}
