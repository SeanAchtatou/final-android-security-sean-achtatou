package com.air.sdk.injector;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import java.io.IOException;
import java.util.Locale;

final class KitRepoDbCache extends SQLiteOpenHelper implements IKitRepoCache {
    private static final String FIELD_FILE = "data";
    private static final String FIELD_KEY = "key";
    private static final String FIELD_KIT_CLASS_NAME = "kcn";
    private static final String FIELD_VERSION = "ver";
    private static final int KEY_BUILTIN_PACK_FILE = 4;
    private static final int KEY_DEFAULT_FILE_VERSION = 1;
    private static final int KEY_KIT_BACKUP = 3;
    private static final int KEY_KIT_FILE = 2;
    private static final String TABLE_NAME = "kitrepodata";

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    KitRepoDbCache(Context context) {
        super(context, "kitrepo", (SQLiteDatabase.CursorFactory) null, 1);
        getWritableDatabase().execSQL("CREATE TABLE IF NOT EXISTS kitrepodata (key INTEGER PRIMARY KEY, kcn VARCHAR(128), ver LONG, data BLOB)");
    }

    public void putDefaultFileVersionToCache(long j) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.execSQL("INSERT OR REPLACE INTO kitrepodata (key, ver) VALUES (1," + j + ")");
    }

    public long getDefaultFileVersionFromCache() {
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{FIELD_VERSION}, "key=1", null, null, null, null);
        try {
            if (readVersionFromCursor(query)) {
                return query.getLong(query.getColumnIndexOrThrow(FIELD_VERSION));
            }
            if (query == null) {
                return 0;
            }
            query.close();
            return 0;
        } finally {
            if (query != null) {
                query.close();
            }
        }
    }

    public void putKitRepoModuleToCache(String str, long j, byte[] bArr) {
        SQLiteStatement compileStatement = getWritableDatabase().compileStatement(String.format("INSERT OR REPLACE INTO %s (%s, %s,%s,%s) VALUES (?,?,?,?)", TABLE_NAME, FIELD_KEY, FIELD_KIT_CLASS_NAME, FIELD_VERSION, FIELD_FILE));
        try {
            compileStatement.bindLong(1, 2);
            compileStatement.bindString(2, str);
            compileStatement.bindLong(3, j);
            compileStatement.bindBlob(4, bArr);
            compileStatement.execute();
        } finally {
            compileStatement.close();
        }
    }

    public String getKitRepoClassNameFromCache() {
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{FIELD_KIT_CLASS_NAME}, "key=2", null, null, null, null);
        try {
            if (query.moveToNext()) {
                return query.getString(query.getColumnIndexOrThrow(FIELD_KIT_CLASS_NAME));
            }
            if (query != null) {
                query.close();
            }
            throw new IOException("Kit repo module class name not found");
        } finally {
            if (query != null) {
                query.close();
            }
        }
    }

    public long getKitRepoModuleVersionFromCache() {
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{FIELD_VERSION}, "key=2", null, null, null, null);
        if (readVersionFromCursor(query)) {
            return query.getLong(query.getColumnIndexOrThrow(FIELD_VERSION));
        }
        return 0;
    }

    public byte[] getKitRepoModuleFileFromCache() {
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{FIELD_FILE}, "key=2", null, null, null, null);
        try {
            if (query.moveToNext()) {
                return query.getBlob(query.getColumnIndexOrThrow(FIELD_FILE));
            }
            if (query != null) {
                query.close();
            }
            throw new IOException("Kit repo module file not found");
        } finally {
            if (query != null) {
                query.close();
            }
        }
    }

    public void putBuiltinPack(byte[] bArr) {
        SQLiteStatement compileStatement = getWritableDatabase().compileStatement(String.format("INSERT OR REPLACE INTO %s (%s, %s,%s,%s) VALUES (?,?,?,?)", TABLE_NAME, FIELD_KEY, FIELD_KIT_CLASS_NAME, FIELD_VERSION, FIELD_FILE));
        try {
            compileStatement.bindLong(1, 4);
            compileStatement.bindString(2, "c.bip.f");
            compileStatement.bindLong(3, System.currentTimeMillis());
            compileStatement.bindBlob(4, bArr);
            compileStatement.execute();
        } finally {
            compileStatement.close();
        }
    }

    public byte[] getBuiltinPack() {
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{FIELD_FILE}, "key=4", null, null, null, null);
        try {
            if (query.moveToNext()) {
                return query.getBlob(query.getColumnIndexOrThrow(FIELD_FILE));
            }
            if (query == null) {
                return null;
            }
            query.close();
            return null;
        } finally {
            if (query != null) {
                query.close();
            }
        }
    }

    public void removeBuiltinPack() {
        getWritableDatabase().execSQL(String.format(Locale.US, "delete from %1$s where %2$s=%3$d;", TABLE_NAME, FIELD_KEY, 4));
    }

    public void backupCurrentVersionOfKitRepoModule() {
        getWritableDatabase().execSQL(String.format(Locale.US, "insert or replace  into %1$s (%2$s, %3$s, %4$s, %5$s) select '%6$s' as %2$s, %3$s, %4$s, %5$s from %1$s where %2$s=%7$d", TABLE_NAME, FIELD_KEY, FIELD_KIT_CLASS_NAME, FIELD_VERSION, FIELD_FILE, 3, 2));
    }

    public void restoreKitRepoModuleFromBackup() {
        getWritableDatabase().execSQL(String.format(Locale.US, "insert or replace  into %1$s (%2$s, %3$s, %4$s, %5$s) select '%6$s' as %2$s, %3$s, %4$s, %5$s from %1$s where %2$s=%7$d", TABLE_NAME, FIELD_KEY, FIELD_KIT_CLASS_NAME, FIELD_VERSION, FIELD_FILE, 2, 3));
        removeKitRepoModuleBackup();
    }

    public void removeKitRepoModuleBackup() {
        getWritableDatabase().execSQL(String.format(Locale.US, "delete from %1$s where %2$s=%3$d;", TABLE_NAME, FIELD_KEY, 3));
    }

    private boolean readVersionFromCursor(Cursor cursor) {
        return cursor.moveToNext();
    }
}
