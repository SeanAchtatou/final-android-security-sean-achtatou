package com.air.sdk.injector;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

final class ClassLoaderParentInjection implements IPatcher, ISystemClassLoaderPatcher {
    /* access modifiers changed from: private */
    public static HashMap<String, Class<?>> classMap = new HashMap<>();
    private Object classLoaderForInjection = null;
    private InjectedClassLoader injectedClassLoader = null;
    private Object referencedClassLoader = null;

    ClassLoaderParentInjection(Object obj) {
        setReferencedClassLoader(obj);
    }

    public void injectPatch(Object obj) {
        if (this.injectedClassLoader != null) {
            takeOutPatch();
        }
        this.classLoaderForInjection = obj;
        this.injectedClassLoader = new InjectedClassLoader(((ClassLoader) this.classLoaderForInjection).getParent());
        this.injectedClassLoader.setReferencedClassLoader(this.referencedClassLoader);
        try {
            setField(this.classLoaderForInjection, ClassLoader.class, "parent", this.injectedClassLoader);
        } catch (Exception e) {
            takeOutPatch();
            throw new Exception("Injection failed.", e);
        }
    }

    public void takeOutPatch() {
        InjectedClassLoader injectedClassLoader2 = this.injectedClassLoader;
        if (injectedClassLoader2 != null) {
            injectedClassLoader2.setReferencedClassLoader(null);
            try {
                setField(this.classLoaderForInjection, ClassLoader.class, "parent", this.injectedClassLoader.getParent());
            } catch (Exception e) {
                throw new Exception("Taking out injection filed.", e);
            }
        }
        this.injectedClassLoader = null;
    }

    public void setReferencedClassLoader(Object obj) {
        this.referencedClassLoader = obj;
        InjectedClassLoader injectedClassLoader2 = this.injectedClassLoader;
        if (injectedClassLoader2 != null) {
            injectedClassLoader2.setReferencedClassLoader(obj);
        }
    }

    public void registerClass(Class<?> cls) {
        classMap.put(cls.getName(), cls);
    }

    public void unregisterClass(Class<?> cls) {
        classMap.remove(cls.getName());
    }

    private void setField(Object obj, Class cls, String str, Object obj2) {
        Field declaredField = cls.getDeclaredField(str);
        declaredField.setAccessible(true);
        declaredField.set(obj, obj2);
    }

    private static final class InjectedClassLoader extends ClassLoader {
        List<String> busyClass;
        private Method findClassMethod;
        private Object referencedClassLoader;

        private void doNothing() {
        }

        private InjectedClassLoader(ClassLoader classLoader) {
            super(classLoader);
            this.busyClass = Collections.synchronizedList(new ArrayList());
            this.referencedClassLoader = null;
            this.findClassMethod = null;
            try {
                this.findClassMethod = ClassLoader.class.getDeclaredMethod("findClass", String.class);
                this.findClassMethod.setAccessible(true);
            } catch (NoSuchMethodException unused) {
                doNothing();
            }
        }

        /* access modifiers changed from: protected */
        public Class<?> loadClass(String str, boolean z) {
            if (!str.startsWith("java") && !str.startsWith("android") && this.referencedClassLoader != null && !this.busyClass.contains(str)) {
                this.busyClass.add(str);
                Class<?> loadClass = ((ClassLoader) this.referencedClassLoader).loadClass(str);
                this.busyClass.remove(str);
                if (loadClass != null) {
                    return loadClass;
                }
            }
            if (ClassLoaderParentInjection.classMap.containsKey(str)) {
                return (Class) ClassLoaderParentInjection.classMap.get(str);
            }
            return super.loadClass(str, z);
        }

        /* access modifiers changed from: protected */
        public Class<?> findClass(String str) {
            Object obj;
            if (str.equals("com.air.sdk.components.AirReceiver") && (obj = this.referencedClassLoader) != null) {
                try {
                    Class<?> cls = (Class) this.findClassMethod.invoke(obj, str);
                    if (cls != null) {
                        return cls;
                    }
                } catch (Throwable unused) {
                    doNothing();
                }
            }
            return super.findClass(str);
        }

        /* access modifiers changed from: private */
        public void setReferencedClassLoader(Object obj) {
            this.referencedClassLoader = obj;
        }
    }
}
