package com.air.sdk.injector;

public interface IKitRepoController {
    long getVersion();

    void update(String str, long j, byte[] bArr);
}
