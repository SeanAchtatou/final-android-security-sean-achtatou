package com.air.sdk.injector;

public interface IAddonWithClasses {
    void registerClasses(IPatcher iPatcher);
}
