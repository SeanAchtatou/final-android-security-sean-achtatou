package com.air.sdk.injector;

import android.content.Context;
import com.air.sdk.utils.FileUtils;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

final class BuiltinPackV1 extends BaseBuiltinReader {
    private static final String KIT_REPO_MODULE_CONFIG_ENTRY_NAME = "krmcen";
    private static final String KIT_REPO_MODULE_FILE_ENTRY_NAME = "krmfen";
    private final IBuiltinPackProvider builtinPackProvider;
    private File temporaryFile;
    private long temporaryFileCrc32;

    BuiltinPackV1(Context context, IKitRepoCache iKitRepoCache, IBuiltinPackProvider iBuiltinPackProvider) {
        super(iKitRepoCache);
        this.temporaryFile = new File(context.getCacheDir(), AssetsBuiltinPackProvider.ASSET_NAME_FOR_BUILTIN_PACK);
        this.builtinPackProvider = iBuiltinPackProvider;
    }

    /* access modifiers changed from: protected */
    public void prepareEnvironment() {
        FileOutputStream fileOutputStream;
        InputStream builtinPack = this.builtinPackProvider.getBuiltinPack();
        try {
            fileOutputStream = new FileOutputStream(this.temporaryFile);
            this.temporaryFileCrc32 = FileUtils.copyContentFromStreamToStream(builtinPack, fileOutputStream);
            fileOutputStream.close();
            if (builtinPack != null) {
                builtinPack.close();
            }
        } catch (Throwable th) {
            if (builtinPack != null) {
                builtinPack.close();
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public boolean isUpdateNeed() {
        return this.temporaryFileCrc32 != getKitRepoCache().getDefaultFileVersionFromCache();
    }

    /* access modifiers changed from: protected */
    public void updateModule() {
        byte[] rawFileContent = getRawFileContent(this.temporaryFile);
        int i = 2;
        ZipInputStream zipInputStream = new ZipInputStream(new AdvInputStream(new ByteArrayInputStream(rawFileContent), new byte[]{80, 75}));
        String str = null;
        byte[] bArr = null;
        do {
            try {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry == null) {
                    break;
                }
                if (nextEntry.getName().equals(KIT_REPO_MODULE_CONFIG_ENTRY_NAME)) {
                    str = new String(readStream(zipInputStream));
                } else if (nextEntry.getName().equals(KIT_REPO_MODULE_FILE_ENTRY_NAME)) {
                    bArr = readStream(zipInputStream);
                } else {
                    continue;
                }
                i--;
                continue;
            } finally {
                zipInputStream.close();
            }
        } while (i > 0);
        if (str == null || bArr == null) {
            throw new IOException("Wrong built-in format");
        }
        String[] split = str.split(":");
        String trim = split[0].trim();
        long parseLong = Long.parseLong(split[1].trim());
        if (parseLong > getKitRepoCache().getKitRepoModuleVersionFromCache()) {
            getKitRepoCache().putKitRepoModuleToCache(trim, parseLong, bArr);
        }
        getKitRepoCache().putBuiltinPack(rawFileContent);
    }

    /* access modifiers changed from: protected */
    public void cleanEnvironment() {
        if (this.temporaryFile.exists() && !this.temporaryFile.delete()) {
            throw new IOException("Can't delete temp builtin pack file.");
        }
    }

    /* access modifiers changed from: protected */
    public void updateAppliedSnapshot() {
        getKitRepoCache().putDefaultFileVersionToCache(this.temporaryFileCrc32);
    }

    private byte[] getRawFileContent(File file) {
        ByteArrayOutputStream byteArrayOutputStream;
        FileInputStream fileInputStream = new FileInputStream(file);
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            FileUtils.copyContentFromStreamToStream(fileInputStream, byteArrayOutputStream);
            byteArrayOutputStream.flush();
            byteArrayOutputStream.close();
            fileInputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (Throwable th) {
            fileInputStream.close();
            throw th;
        }
    }

    private byte[] readStream(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            FileUtils.copyContentFromStreamToStream(inputStream, byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        } finally {
            byteArrayOutputStream.close();
        }
    }
}
