package com.air.sdk.injector;

import android.content.Context;
import com.air.sdk.utils.IBundle;
import java.io.File;

public interface IKitRepo {
    @Deprecated
    void applyDefaultKits(File file);

    void applyDefaultKits(byte[] bArr, Runnable runnable);

    IKit getKit(String str, IBundle iBundle);

    void setup(Context context, IKitRepoController iKitRepoController);
}
