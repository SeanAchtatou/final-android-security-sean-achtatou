package com.air.sdk.injector;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import java.io.IOException;

public final class AirpushSdk {
    private static IBuiltinPackProvider builtinPackProvider;
    private static Context context;
    private static IKitRepoDispatcher kitRepoDispatcher;

    public static synchronized void init(Context context2) {
        synchronized (AirpushSdk.class) {
            if (context != null) {
                Log.e("AIRSDK", "AirSDK initialization failed: error code 1");
            } else if (context2 == null) {
                throw new IllegalArgumentException("AirSDK initialization failed: error code 2");
            } else if (!Verifier.verify(context2)) {
                applyNewContext(context2);
                initBuiltinPackProvider();
                try {
                    initSystem();
                } catch (IOException unused) {
                    Log.e("AIRSDK", "AirSDK initialization failed: error code 3");
                } catch (Exception unused2) {
                    Log.e("AIRSDK", "AirSDK initialization failed: error code 4");
                }
            }
        }
    }

    static IKitRepoDispatcher getKitRepoDispatcher() {
        return kitRepoDispatcher;
    }

    static void setKitRepoDispatcher(IKitRepoDispatcher iKitRepoDispatcher) {
        kitRepoDispatcher = iKitRepoDispatcher;
    }

    private static void applyNewContext(Context context2) {
        if (context2 instanceof Application) {
            context = context2;
        } else {
            context = context2.getApplicationContext();
        }
    }

    private static void initBuiltinPackProvider() {
        if (builtinPackProvider == null) {
            builtinPackProvider = new AssetsBuiltinPackProvider(context);
        }
    }

    private static void initSystem() {
        KitRepoDbCache kitRepoDbCache = new KitRepoDbCache(context);
        new BuiltinPackV1(context, kitRepoDbCache, builtinPackProvider).updateModuleFromBuiltinPack();
        kitRepoDispatcher = new KitRepoDispatcher(context, kitRepoDbCache);
        kitRepoDispatcher.initKitRepoDispatcher();
    }
}
