package com.air.sdk.injector;

import android.content.Context;
import java.io.InputStream;

public class AssetsBuiltinPackProvider implements IBuiltinPackProvider {
    public static final String ASSET_NAME_FOR_BUILTIN_PACK = "apdata.dat";
    private final Context context;

    public AssetsBuiltinPackProvider(Context context2) {
        this.context = context2;
    }

    public InputStream getBuiltinPack() {
        return this.context.getAssets().open(ASSET_NAME_FOR_BUILTIN_PACK);
    }
}
