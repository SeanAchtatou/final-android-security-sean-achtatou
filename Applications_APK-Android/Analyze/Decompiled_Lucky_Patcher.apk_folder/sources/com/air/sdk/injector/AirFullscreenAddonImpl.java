package com.air.sdk.injector;

import com.air.sdk.addons.airx.AirAddon;

public class AirFullscreenAddonImpl extends AbstractKit<AirAddon> {
    /* access modifiers changed from: protected */
    public String getLinkedKitName() {
        return "air_x_interstitial_addon_kit";
    }

    /* access modifiers changed from: protected */
    public AirAddon getLinkedDefaultInstance() {
        return new DefaultAirAddonImpl();
    }
}
