package com.air.sdk.injector;

import android.content.Context;
import android.util.Log;
import com.air.sdk.addons.airx.AirAddon;
import com.air.sdk.addons.airx.AirFullscreenListener;
import com.air.sdk.addons.airx.AirListener;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.List;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

public class AirFullscreen implements com.air.sdk.addons.airx.AirFullscreen {
    private AirFullscreenListener adListener;
    private AirAd airAd;
    private SoftReference<Context> contextReference;
    private HashMap<String, String> extras;
    private FullscreenListener fullscreenListener;

    public AirFullscreen(Context context) {
        try {
            this.airAd = new AirAd();
            this.contextReference = new SoftReference<>(context);
        } catch (Throwable unused) {
        }
    }

    public void setAdListener(AirFullscreenListener airFullscreenListener) {
        this.adListener = airFullscreenListener;
    }

    public void loadAd() {
        AirAdLoader.getInstance().submit(new Runnable() {
            public void run() {
                AirFullscreen.this.loadInternal();
            }
        });
    }

    /* access modifiers changed from: private */
    public void loadInternal() {
        try {
            if (this.airAd != null && this.contextReference != null && this.contextReference.get() != null) {
                this.fullscreenListener = new FullscreenListener(this.adListener);
                getAddon().loadAd(this.airAd.getDefaultLoadBundle(this.contextReference.get(), this.fullscreenListener, this.extras));
            }
        } catch (Throwable unused) {
            logEror("Load fullscreen failed");
        }
    }

    private AirAddon getAddon() {
        AirAddon airAddon;
        AirFullscreenAddonImpl airFullscreenAddonImpl = new AirFullscreenAddonImpl();
        do {
            airAddon = (AirAddon) airFullscreenAddonImpl.getLinkedKit();
        } while (airAddon instanceof DefaultAirAddonImpl);
        return airAddon;
    }

    public void showAd() {
        try {
            getAddon().showAd(this.fullscreenListener.hashCode());
        } catch (Throwable unused) {
            logEror("Show fullscreen failed");
        }
    }

    public void closeAd() {
        try {
            makeCloseAllAd();
            this.adListener = null;
            this.fullscreenListener = null;
            this.extras = null;
        } catch (Throwable unused) {
            logEror("Close fullscreen failed");
        }
    }

    private void makeCloseAllAd() {
        try {
            if (this.airAd != null) {
                List<AirListener> references = this.airAd.getReferences();
                for (int i = 0; i < references.size(); i++) {
                    FullscreenListener fullscreenListener2 = (FullscreenListener) references.get(i);
                    makeCloseAd(fullscreenListener2.hashCode());
                    invalidateListener(fullscreenListener2);
                }
            }
        } catch (Throwable unused) {
        }
    }

    /* access modifiers changed from: private */
    public void makeCloseAd(int i) {
        try {
            getAddon().closeAd(i);
        } catch (Throwable unused) {
            logEror("Close fullscreen failed: " + i);
        }
    }

    /* access modifiers changed from: private */
    public void invalidateListener(FullscreenListener fullscreenListener2) {
        if (fullscreenListener2 != null) {
            try {
                fullscreenListener2.invalidate();
                if (invalidatingActiveListener(fullscreenListener2)) {
                    this.fullscreenListener = null;
                }
            } catch (Throwable unused) {
            }
        }
    }

    private boolean invalidatingActiveListener(FullscreenListener fullscreenListener2) {
        FullscreenListener fullscreenListener3 = this.fullscreenListener;
        return fullscreenListener3 != null && fullscreenListener3.hashCode() == fullscreenListener2.hashCode();
    }

    private void logEror(String str) {
        Log.e(BuildConfig.FLAVOR, str);
    }

    public void setExtras(HashMap<String, String> hashMap) {
        this.extras = hashMap;
    }

    private static class FullscreenListener implements AirFullscreenListener {
        private AirFullscreen fullscreen;
        private AirFullscreenListener listener;

        private FullscreenListener(AirFullscreen airFullscreen, AirFullscreenListener airFullscreenListener) {
            this.fullscreen = airFullscreen;
            this.listener = airFullscreenListener;
        }

        public void onAdFailed(String str) {
            try {
                if (this.listener != null) {
                    this.listener.onAdFailed(str);
                }
                closeAd();
            } catch (Throwable unused) {
            }
        }

        public void onAdLoaded() {
            try {
                if (this.listener != null) {
                    this.listener.onAdLoaded();
                }
            } catch (Throwable unused) {
            }
        }

        public void onAdShown() {
            try {
                if (this.listener != null) {
                    this.listener.onAdShown();
                }
            } catch (Throwable unused) {
            }
        }

        public void onAdClosed() {
            try {
                if (this.listener != null) {
                    this.listener.onAdClosed();
                }
                closeAd();
            } catch (Throwable unused) {
            }
        }

        public void onAdClicked() {
            try {
                if (this.listener != null) {
                    this.listener.onAdClicked();
                }
            } catch (Throwable unused) {
            }
        }

        public void onLeaveApplication() {
            try {
                if (this.listener != null) {
                    this.listener.onLeaveApplication();
                }
            } catch (Throwable unused) {
            }
        }

        private void closeAd() {
            try {
                if (this.fullscreen != null) {
                    this.fullscreen.makeCloseAd(hashCode());
                    this.fullscreen.invalidateListener(this);
                }
            } catch (Throwable unused) {
            }
        }

        /* access modifiers changed from: private */
        public void invalidate() {
            this.fullscreen = null;
            this.listener = null;
        }
    }
}
