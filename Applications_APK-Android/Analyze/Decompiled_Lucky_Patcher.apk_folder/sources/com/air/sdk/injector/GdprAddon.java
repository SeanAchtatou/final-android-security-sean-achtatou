package com.air.sdk.injector;

import android.app.Activity;
import com.air.sdk.gdpr.IConsentBox;
import com.air.sdk.gdpr.IGdprAddon;
import com.air.sdk.utils.IBundle;
import com.air.sdk.utils.MemoryBundle;

public class GdprAddon extends AbstractKit<IGdprAddon> {
    private static final IGdprAddon DEFAULT_INSTANCE = new DefaultGdprAddon();
    private static GdprAddon instance;

    /* access modifiers changed from: protected */
    public String getLinkedKitName() {
        return "air_gdpr_module";
    }

    private GdprAddon() {
    }

    private static IGdprAddon getInstance() {
        if (instance == null) {
            instance = new GdprAddon();
        }
        return (IGdprAddon) instance.getLinkedKit();
    }

    public static void init(Activity activity) {
        try {
            getInstance().init(activity);
        } catch (Throwable unused) {
        }
    }

    public static void showConsentBox() {
        try {
            getInstance().getConsentBox().show();
        } catch (Throwable unused) {
        }
    }

    public static void setEnableLaterButton(boolean z) {
        try {
            getInstance().getConsentBox().setEnableLaterButton(z);
        } catch (Throwable unused) {
        }
    }

    /* access modifiers changed from: protected */
    public IGdprAddon getLinkedDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    private static class DefaultGdprAddon implements IGdprAddon {
        /* access modifiers changed from: private */
        public final IBundle savedState;

        private DefaultGdprAddon() {
            this.savedState = new MemoryBundle();
        }

        public IBundle getDefaultInstanceState() {
            return this.savedState;
        }

        public void init(Activity activity) {
            this.savedState.putObject("init", activity);
        }

        public IConsentBox getConsentBox() {
            return new IConsentBox() {
                public void setEnableLaterButton(boolean z) {
                    DefaultGdprAddon.this.savedState.putBoolean("isLaterButtonEnable", z);
                }

                public void show() {
                    DefaultGdprAddon.this.savedState.putBoolean("needToShow", true);
                }
            };
        }
    }
}
