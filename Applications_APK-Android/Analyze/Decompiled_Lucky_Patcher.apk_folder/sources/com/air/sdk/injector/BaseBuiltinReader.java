package com.air.sdk.injector;

abstract class BaseBuiltinReader implements IKitRepoBuiltinReader {
    private IKitRepoCache kitRepoCache;

    /* access modifiers changed from: protected */
    public abstract void cleanEnvironment();

    /* access modifiers changed from: protected */
    public abstract boolean isUpdateNeed();

    /* access modifiers changed from: protected */
    public abstract void prepareEnvironment();

    /* access modifiers changed from: protected */
    public abstract void updateAppliedSnapshot();

    /* access modifiers changed from: protected */
    public abstract void updateModule();

    BaseBuiltinReader(IKitRepoCache iKitRepoCache) {
        this.kitRepoCache = iKitRepoCache;
    }

    public final void updateModuleFromBuiltinPack() {
        try {
            prepareEnvironment();
            if (isUpdateNeed()) {
                updateModule();
                updateAppliedSnapshot();
            }
        } finally {
            cleanEnvironment();
        }
    }

    /* access modifiers changed from: protected */
    public final IKitRepoCache getKitRepoCache() {
        return this.kitRepoCache;
    }
}
