package com.air.sdk.exceptions;

public class KitNotFoundException extends Exception {
    public KitNotFoundException(String str, Throwable th) {
        super("Kit " + str + " not registered in a system", th);
    }
}
