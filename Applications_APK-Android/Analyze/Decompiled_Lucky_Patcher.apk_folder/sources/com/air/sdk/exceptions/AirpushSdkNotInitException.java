package com.air.sdk.exceptions;

import com.air.sdk.injector.AirpushSdk;

public class AirpushSdkNotInitException extends Exception {
    public AirpushSdkNotInitException() {
        super(String.format("AirpushSdk SDK hasn't been initialized. Make sure you call %s.init(this) in your Application.onCreate() method.", AirpushSdk.class.getName()));
    }
}
