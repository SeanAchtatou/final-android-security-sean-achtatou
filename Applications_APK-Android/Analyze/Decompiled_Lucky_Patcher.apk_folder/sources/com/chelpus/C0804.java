package com.chelpus;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetEncoder;
import java.util.concurrent.Callable;
import java.util.zip.GZIPInputStream;

/* renamed from: com.chelpus.ʼ  reason: contains not printable characters */
/* compiled from: HttpRequest */
public class C0804 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final String[] f3358 = new String[0];

    /* renamed from: ʼ  reason: contains not printable characters */
    private static C0806 f3359 = C0806.f3378;

    /* renamed from: ʽ  reason: contains not printable characters */
    private HttpURLConnection f3360 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final URL f3361;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final String f3362;

    /* renamed from: ˆ  reason: contains not printable characters */
    private C0809 f3363;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f3364;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f3365 = true;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f3366 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public int f3367 = 8192;
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public long f3368 = -1;
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public long f3369 = 0;

    /* renamed from: ˑ  reason: contains not printable characters */
    private String f3370;

    /* renamed from: י  reason: contains not printable characters */
    private int f3371;
    /* access modifiers changed from: private */

    /* renamed from: ـ  reason: contains not printable characters */
    public C0810 f3372 = C0810.f3380;

    /* renamed from: com.chelpus.ʼ$ʼ  reason: contains not printable characters */
    /* compiled from: HttpRequest */
    public interface C0806 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public static final C0806 f3378 = new C0806() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public HttpURLConnection m5097(URL url) {
                return (HttpURLConnection) url.openConnection();
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public HttpURLConnection m5098(URL url, Proxy proxy) {
                return (HttpURLConnection) url.openConnection(proxy);
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        HttpURLConnection m5095(URL url);

        /* renamed from: ʻ  reason: contains not printable characters */
        HttpURLConnection m5096(URL url, Proxy proxy);
    }

    /* renamed from: com.chelpus.ʼ$ˆ  reason: contains not printable characters */
    /* compiled from: HttpRequest */
    public interface C0810 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public static final C0810 f3380 = new C0810() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public void m5104(long j, long j2) {
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        void m5103(long j, long j2);
    }

    /* renamed from: com.chelpus.ʼ$ʽ  reason: contains not printable characters */
    /* compiled from: HttpRequest */
    public static class C0807 extends RuntimeException {
        public C0807(IOException iOException) {
            super(iOException);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public IOException getCause() {
            return (IOException) super.getCause();
        }
    }

    /* renamed from: com.chelpus.ʼ$ʾ  reason: contains not printable characters */
    /* compiled from: HttpRequest */
    protected static abstract class C0808<V> implements Callable<V> {
        /* access modifiers changed from: protected */
        /* renamed from: ʼ  reason: contains not printable characters */
        public abstract V m5100();

        /* access modifiers changed from: protected */
        /* renamed from: ʽ  reason: contains not printable characters */
        public abstract void m5101();

        protected C0808() {
        }

        public V call() {
            boolean z = true;
            try {
                V r0 = m5100();
                try {
                    m5101();
                    return r0;
                } catch (IOException e) {
                    throw new C0807(e);
                }
            } catch (C0807 e2) {
                throw e2;
            } catch (IOException e3) {
                throw new C0807(e3);
            } catch (Throwable th) {
                th = th;
                m5101();
                throw th;
            }
        }
    }

    /* renamed from: com.chelpus.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: HttpRequest */
    protected static abstract class C0805<V> extends C0808<V> {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final Closeable f3376;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final boolean f3377;

        protected C0805(Closeable closeable, boolean z) {
            this.f3376 = closeable;
            this.f3377 = z;
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʽ  reason: contains not printable characters */
        public void m5094() {
            Closeable closeable = this.f3376;
            if (closeable instanceof Flushable) {
                ((Flushable) closeable).flush();
            }
            if (this.f3377) {
                try {
                    this.f3376.close();
                } catch (IOException unused) {
                }
            } else {
                this.f3376.close();
            }
        }
    }

    /* renamed from: com.chelpus.ʼ$ʿ  reason: contains not printable characters */
    /* compiled from: HttpRequest */
    public static class C0809 extends BufferedOutputStream {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final CharsetEncoder f3379;

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0809 m5102(String str) {
            ByteBuffer encode = this.f3379.encode(CharBuffer.wrap(str));
            super.write(encode.array(), 0, encode.limit());
            return this;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0804 m5063(CharSequence charSequence) {
        return new C0804(charSequence, "GET");
    }

    public C0804(CharSequence charSequence, String str) {
        try {
            this.f3361 = new URL(charSequence.toString());
            this.f3362 = str;
        } catch (MalformedURLException e) {
            throw new C0807(e);
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private Proxy m5067() {
        return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(this.f3370, this.f3371));
    }

    /* renamed from: י  reason: contains not printable characters */
    private HttpURLConnection m5068() {
        HttpURLConnection httpURLConnection;
        try {
            if (this.f3370 != null) {
                httpURLConnection = f3359.m5096(this.f3361, m5067());
            } else {
                httpURLConnection = f3359.m5095(this.f3361);
            }
            httpURLConnection.setRequestMethod(this.f3362);
            return httpURLConnection;
        } catch (IOException e) {
            throw new C0807(e);
        }
    }

    public String toString() {
        return m5091() + ' ' + m5090();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public HttpURLConnection m5077() {
        if (this.f3360 == null) {
            this.f3360 = m5068();
        }
        return this.f3360;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m5078() {
        try {
            m5088();
            return m5077().getResponseCode();
        } catch (IOException e) {
            throw new C0807(e);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m5082() {
        return 200 == m5078();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public C0804 m5083() {
        m5077().disconnect();
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0804 m5075(boolean z) {
        this.f3366 = z;
        return this;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public BufferedInputStream m5084() {
        return new BufferedInputStream(m5085(), this.f3367);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public InputStream m5085() {
        InputStream inputStream;
        if (m5078() < 400) {
            try {
                inputStream = m5077().getInputStream();
            } catch (IOException e) {
                throw new C0807(e);
            }
        } else {
            inputStream = m5077().getErrorStream();
            if (inputStream == null) {
                try {
                    inputStream = m5077().getInputStream();
                } catch (IOException e2) {
                    if (m5087() <= 0) {
                        inputStream = new ByteArrayInputStream(new byte[0]);
                    } else {
                        throw new C0807(e2);
                    }
                }
            }
        }
        if (!this.f3366 || !"gzip".equals(m5086())) {
            return inputStream;
        }
        try {
            return new GZIPInputStream(inputStream);
        } catch (IOException e3) {
            throw new C0807(e3);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0804 m5073(OutputStream outputStream) {
        try {
            return m5072(m5084(), outputStream);
        } catch (IOException e) {
            throw new C0807(e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0804 m5070(int i) {
        m5077().setConnectTimeout(i);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0804 m5074(String str, String str2) {
        m5077().setRequestProperty(str, str2);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m5076(String str) {
        m5089();
        return m5077().getHeaderField(str);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m5079(String str) {
        return m5069(str, -1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m5069(String str, int i) {
        m5089();
        return m5077().getHeaderFieldInt(str, i);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0804 m5080(boolean z) {
        m5077().setUseCaches(z);
        return this;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public String m5086() {
        return m5076("Content-Encoding");
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public int m5087() {
        return m5079("Content-Length");
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0804 m5072(InputStream inputStream, OutputStream outputStream) {
        final InputStream inputStream2 = inputStream;
        final OutputStream outputStream2 = outputStream;
        return (C0804) new C0805<C0804>(inputStream, this.f3365) {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C0804 m5093() {
                byte[] bArr = new byte[C0804.this.f3367];
                while (true) {
                    int read = inputStream2.read(bArr);
                    if (read == -1) {
                        return C0804.this;
                    }
                    outputStream2.write(bArr, 0, read);
                    C0804 r2 = C0804.this;
                    long unused = r2.f3369 = r2.f3369 + ((long) read);
                    C0804.this.f3372.m5103(C0804.this.f3369, C0804.this.f3368);
                }
            }
        }.call();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0804 m5071(C0810 r1) {
        if (r1 == null) {
            this.f3372 = C0810.f3380;
        } else {
            this.f3372 = r1;
        }
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˊ  reason: contains not printable characters */
    public C0804 m5088() {
        m5071((C0810) null);
        C0809 r1 = this.f3363;
        if (r1 == null) {
            return this;
        }
        if (this.f3364) {
            r1.m5102("\r\n--00content0boundary00--\r\n");
        }
        if (this.f3365) {
            try {
                this.f3363.close();
            } catch (IOException unused) {
            }
        } else {
            this.f3363.close();
        }
        this.f3363 = null;
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˋ  reason: contains not printable characters */
    public C0804 m5089() {
        try {
            return m5088();
        } catch (IOException e) {
            throw new C0807(e);
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public URL m5090() {
        return m5077().getURL();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public String m5091() {
        return m5077().getRequestMethod();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public C0804 m5081(boolean z) {
        m5077().setInstanceFollowRedirects(z);
        return this;
    }
}
