package com.chelpus;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import com.lp.C0987;
import com.lp.ʻ.C0947;
import java.io.File;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.chelpus.ˈ  reason: contains not printable characters */
/* compiled from: mFurtherInstallationEventsReceiver */
public class C0820 extends BroadcastReceiver {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chelpus.ˆ.ʻ(android.app.Activity, java.lang.String, java.lang.String):void
     arg types: [ru.pKkcGXHI.kKSaIWSZS.MainActivity, java.lang.String, java.lang.String]
     candidates:
      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String[], java.lang.String[]):int
      com.chelpus.ˆ.ʻ(java.lang.String, int, java.lang.String):android.text.SpannableString
      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, java.lang.String):android.text.SpannableString
      com.chelpus.ˆ.ʻ(java.io.File, java.util.ArrayList<java.io.File>, java.lang.String):java.io.File
      com.chelpus.ˆ.ʻ(java.lang.String, java.util.ArrayList<java.lang.String>, boolean):java.util.ArrayList<com.chelpus.ʻ.ʼ.ᵔ>
      com.chelpus.ˆ.ʻ(int, int, java.io.File):void
      com.chelpus.ˆ.ʻ(android.app.Activity, java.io.File, java.lang.Integer):void
      com.chelpus.ˆ.ʻ(android.app.Activity, java.lang.String, java.lang.Integer):void
      com.chelpus.ˆ.ʻ(android.content.Context, java.lang.String, java.lang.String):void
      com.chelpus.ˆ.ʻ(java.io.File, java.io.File, java.lang.String[]):void
      com.chelpus.ˆ.ʻ(java.lang.String, java.io.File, java.util.ArrayList<java.io.File>):void
      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.Runnable, java.lang.Runnable):void
      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, java.util.ArrayList<com.lp.ʻ>):void
      com.chelpus.ˆ.ʻ(java.util.ArrayList<com.chelpus.ʻ.ʼ.ˏ>, java.lang.Runnable, java.lang.Runnable):void
      com.chelpus.ˆ.ʻ(boolean, boolean, boolean):void
      com.chelpus.ˆ.ʻ(java.nio.MappedByteBuffer, com.chelpus.ʻ.ʼ.ˉ, int):boolean
      com.chelpus.ˆ.ʻ(java.io.File, java.lang.String, java.util.ArrayList<java.io.File>):java.lang.String
      com.chelpus.ˆ.ʻ(java.util.ArrayList<java.io.File>, java.io.File, java.lang.String):void
      com.chelpus.ˆ.ʻ(android.app.Activity, java.lang.String, java.lang.String):void */
    public void onReceive(Context context, Intent intent) {
        C0987.m6060((Object) ("EXTRA_INSTALLATION_STATUS:" + intent.getIntExtra("com.lp.extra.INSTALLATION_STATUS", -1)));
        int intExtra = intent.getIntExtra("com.lp.extra.INSTALLATION_STATUS", -1);
        if (intExtra == 0) {
            try {
                C0815.m5155((Activity) C0987.f4447, C0815.m5205((int) R.string.well_done), C0815.m5139((int) R.string.installer_slit_successfully_installed, C0987.m6068().getPackageInfo(intent.getStringExtra("com.lp.extra.PACKAGE_NAME"), 0).applicationInfo.loadLabel(C0987.m6068()).toString()));
                C0815.m5326(new File(C0815.m5267()));
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            C0947.m5873();
        } else if (intExtra == 1) {
            C0947.m5879(C0815.m5205((int) R.string.wait), C0815.m5205((int) R.string.installer_slit_install_in_progress), BuildConfig.FLAVOR, 2, 3, true);
        } else if (intExtra == 2) {
            C0815.m5155((Activity) C0987.f4447, C0815.m5205((int) R.string.error), intent.getStringExtra("com.lp.extra.ERROR_DESCRIPTION"));
            C0947.m5873();
            C0815.m5326(new File(C0815.m5267()));
        }
    }
}
