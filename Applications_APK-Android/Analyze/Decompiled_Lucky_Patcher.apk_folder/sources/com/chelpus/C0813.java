package com.chelpus;

import java.io.InputStream;
import java.io.OutputStream;

/* renamed from: com.chelpus.ʾ  reason: contains not printable characters */
/* compiled from: Utils */
class C0813 implements Runnable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private InputStream f3381;

    /* renamed from: ʼ  reason: contains not printable characters */
    private OutputStream f3382;

    public C0813(InputStream inputStream, OutputStream outputStream) {
        this.f3381 = inputStream;
        this.f3382 = outputStream;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:12|19|20|21|22|24) */
    /* JADX WARNING: Can't wrap try/catch for region: R(9:0|1|2|(4:5|(2:7|27)(1:26)|25|3)|8|9|10|11|29) */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x001d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0035 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r5 = this;
            r0 = 512(0x200, float:7.175E-43)
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x0025 }
            r1 = 1
        L_0x0005:
            r2 = -1
            if (r1 <= r2) goto L_0x0018
            java.io.InputStream r1 = r5.f3381     // Catch:{ Exception -> 0x0025 }
            int r3 = r0.length     // Catch:{ Exception -> 0x0025 }
            r4 = 0
            int r1 = r1.read(r0, r4, r3)     // Catch:{ Exception -> 0x0025 }
            if (r1 <= r2) goto L_0x0005
            java.io.OutputStream r2 = r5.f3382     // Catch:{ Exception -> 0x0025 }
            r2.write(r0, r4, r1)     // Catch:{ Exception -> 0x0025 }
            goto L_0x0005
        L_0x0018:
            java.io.InputStream r0 = r5.f3381     // Catch:{ Exception -> 0x001d }
            r0.close()     // Catch:{ Exception -> 0x001d }
        L_0x001d:
            java.io.OutputStream r0 = r5.f3382     // Catch:{ Exception -> 0x002f }
            r0.close()     // Catch:{ Exception -> 0x002f }
            goto L_0x002f
        L_0x0023:
            r0 = move-exception
            goto L_0x0030
        L_0x0025:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0023 }
            java.io.InputStream r0 = r5.f3381     // Catch:{ Exception -> 0x001d }
            r0.close()     // Catch:{ Exception -> 0x001d }
            goto L_0x001d
        L_0x002f:
            return
        L_0x0030:
            java.io.InputStream r1 = r5.f3381     // Catch:{ Exception -> 0x0035 }
            r1.close()     // Catch:{ Exception -> 0x0035 }
        L_0x0035:
            java.io.OutputStream r1 = r5.f3382     // Catch:{ Exception -> 0x003a }
            r1.close()     // Catch:{ Exception -> 0x003a }
        L_0x003a:
            goto L_0x003c
        L_0x003b:
            throw r0
        L_0x003c:
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chelpus.C0813.run():void");
    }
}
