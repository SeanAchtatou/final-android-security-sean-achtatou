package com.chelpus;

import android.content.SharedPreferences;
import com.lp.C0987;
import com.lp.ʻ.C0947;
import java.io.File;
import java.io.IOException;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.chelpus.ʿ  reason: contains not printable characters */
/* compiled from: ResumableDownloader */
public class C0814 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f3383;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f3384;

    /* renamed from: ʽ  reason: contains not printable characters */
    private File f3385;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f3386;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f3387 = 0;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f3388;

    /* renamed from: ˈ  reason: contains not printable characters */
    private String[] f3389;

    /* renamed from: ˉ  reason: contains not printable characters */
    private String f3390 = BuildConfig.FLAVOR;

    /* renamed from: ˊ  reason: contains not printable characters */
    private String f3391 = BuildConfig.FLAVOR;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f3392 = 0;

    public C0814(String str) {
        this.f3390 = str;
        SharedPreferences r2 = C0987.m6073();
        this.f3383 = r2.getString("download_resume_lastModified_" + str, BuildConfig.FLAVOR);
        C0987.m6060((Object) ("Server lastModified:" + this.f3383));
        this.f3384 = 9000;
        this.f3386 = true;
        this.f3388 = 2;
        this.f3389 = new String[]{"Downloading", "Complete", "Pause", "Error"};
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007e A[Catch:{ all -> 0x01d8, IOException -> 0x01e9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01e5 A[Catch:{ all -> 0x01d8, IOException -> 0x01e9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01f2 A[SYNTHETIC] */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m5113() {
        /*
            r21 = this;
            r1 = r21
            java.io.File r2 = new java.io.File
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = com.lp.C0987.f4438
            r0.append(r3)
            java.lang.String r3 = "/Download/"
            r0.append(r3)
            java.lang.String r3 = r1.f3390
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            r3 = 0
            r0 = 0
            r4 = r0
            r0 = 0
        L_0x0023:
            r5 = 30
            if (r0 >= r5) goto L_0x01f5
            int r5 = r21.m5116()
            r6 = 1
            if (r5 == r6) goto L_0x01f5
            int r5 = r0 + 1
            r0 = 5
            if (r5 <= r0) goto L_0x003e
            r7 = 5000(0x1388, double:2.4703E-320)
            java.lang.Thread.sleep(r7)     // Catch:{ InterruptedException -> 0x0039 }
            goto L_0x003e
        L_0x0039:
            r0 = move-exception
            r7 = r0
            r7.printStackTrace()
        L_0x003e:
            r1.m5112(r2)     // Catch:{ IOException -> 0x01e9 }
            boolean r0 = r1.f3386     // Catch:{ IOException -> 0x01e9 }
            if (r0 != 0) goto L_0x006f
            if (r4 == 0) goto L_0x004a
            r4.m5083()     // Catch:{ IOException -> 0x01e9 }
        L_0x004a:
            r0 = 2
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r7 = r1.f3390     // Catch:{ IOException -> 0x01e9 }
            r0[r3] = r7     // Catch:{ IOException -> 0x01e9 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01e9 }
            r7.<init>()     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r8 = ""
            r7.append(r8)     // Catch:{ IOException -> 0x01e9 }
            java.io.File r8 = r1.f3385     // Catch:{ IOException -> 0x01e9 }
            long r8 = r8.length()     // Catch:{ IOException -> 0x01e9 }
            r7.append(r8)     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r7 = r7.toString()     // Catch:{ IOException -> 0x01e9 }
            r0[r6] = r7     // Catch:{ IOException -> 0x01e9 }
            com.chelpus.ʼ r0 = com.chelpus.C0815.m5242(r0)     // Catch:{ IOException -> 0x01e9 }
            goto L_0x007b
        L_0x006f:
            if (r4 != 0) goto L_0x007c
            java.lang.String[] r0 = new java.lang.String[r6]     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r7 = r1.f3390     // Catch:{ IOException -> 0x01e9 }
            r0[r3] = r7     // Catch:{ IOException -> 0x01e9 }
            com.chelpus.ʼ r0 = com.chelpus.C0815.m5242(r0)     // Catch:{ IOException -> 0x01e9 }
        L_0x007b:
            r4 = r0
        L_0x007c:
            if (r4 == 0) goto L_0x01e3
            r1.m5114(r3)     // Catch:{ IOException -> 0x01e9 }
            r0 = 2131689815(0x7f0f0157, float:1.9008656E38)
            java.lang.String r7 = com.chelpus.C0815.m5205(r0)     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r8 = r1.f3390     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r9 = ""
            java.io.File r10 = r1.f3385     // Catch:{ IOException -> 0x01e9 }
            long r10 = r10.length()     // Catch:{ IOException -> 0x01e9 }
            int r11 = (int) r10     // Catch:{ IOException -> 0x01e9 }
            int r10 = r11 / 1024
            int r11 = r1.f3387     // Catch:{ IOException -> 0x01e9 }
            int r11 = r11 / 1024
            r12 = 1
            com.lp.ʻ.C0947.m5879(r7, r8, r9, r10, r11, r12)     // Catch:{ IOException -> 0x01e9 }
            java.io.BufferedInputStream r7 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x01e9 }
            java.net.HttpURLConnection r8 = r4.m5077()     // Catch:{ IOException -> 0x01e9 }
            java.io.InputStream r8 = r8.getInputStream()     // Catch:{ IOException -> 0x01e9 }
            r9 = 8192(0x2000, float:1.14794E-41)
            r7.<init>(r8, r9)     // Catch:{ IOException -> 0x01e9 }
            boolean r8 = r1.f3386     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r10 = "\n"
            r11 = 0
            if (r8 != 0) goto L_0x00fb
            java.io.File r8 = r1.f3385     // Catch:{ IOException -> 0x01e9 }
            long r13 = r8.length()     // Catch:{ IOException -> 0x01e9 }
            long r13 = r13 + r11
            java.lang.String r15 = com.chelpus.C0815.m5205(r0)     // Catch:{ IOException -> 0x01e9 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01e9 }
            r0.<init>()     // Catch:{ IOException -> 0x01e9 }
            r8 = 2131689818(0x7f0f015a, float:1.9008662E38)
            java.lang.String r8 = com.chelpus.C0815.m5205(r8)     // Catch:{ IOException -> 0x01e9 }
            r0.append(r8)     // Catch:{ IOException -> 0x01e9 }
            r0.append(r10)     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r8 = r1.f3390     // Catch:{ IOException -> 0x01e9 }
            r0.append(r8)     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r16 = r0.toString()     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r17 = ""
            java.io.File r0 = r1.f3385     // Catch:{ IOException -> 0x01e9 }
            long r11 = r0.length()     // Catch:{ IOException -> 0x01e9 }
            int r0 = (int) r11     // Catch:{ IOException -> 0x01e9 }
            int r0 = r0 / 1024
            int r8 = r1.f3387     // Catch:{ IOException -> 0x01e9 }
            int r8 = r8 / 1024
            r20 = 1
            r18 = r0
            r19 = r8
            com.lp.ʻ.C0947.m5879(r15, r16, r17, r18, r19, r20)     // Catch:{ IOException -> 0x01e9 }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x01e9 }
            r0.<init>(r2, r6)     // Catch:{ IOException -> 0x01e9 }
            r8 = r0
            r11 = r13
            goto L_0x0196
        L_0x00fb:
            boolean r8 = r2.exists()     // Catch:{ IOException -> 0x01e9 }
            if (r8 == 0) goto L_0x0104
            r2.delete()     // Catch:{ IOException -> 0x01e9 }
        L_0x0104:
            java.io.File r8 = com.chelpus.C0815.m5256(r2)     // Catch:{ IOException -> 0x01e9 }
            boolean r8 = r8.exists()     // Catch:{ IOException -> 0x01e9 }
            if (r8 != 0) goto L_0x0115
            java.io.File r8 = com.chelpus.C0815.m5256(r2)     // Catch:{ IOException -> 0x01e9 }
            r8.mkdirs()     // Catch:{ IOException -> 0x01e9 }
        L_0x0115:
            java.lang.String r11 = com.chelpus.C0815.m5205(r0)     // Catch:{ IOException -> 0x01e9 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01e9 }
            r0.<init>()     // Catch:{ IOException -> 0x01e9 }
            r8 = 2131689816(0x7f0f0158, float:1.9008658E38)
            java.lang.String r8 = com.chelpus.C0815.m5205(r8)     // Catch:{ IOException -> 0x01e9 }
            r0.append(r8)     // Catch:{ IOException -> 0x01e9 }
            r0.append(r10)     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r8 = r1.f3390     // Catch:{ IOException -> 0x01e9 }
            r0.append(r8)     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r12 = r0.toString()     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r13 = ""
            java.io.File r0 = r1.f3385     // Catch:{ IOException -> 0x01e9 }
            long r14 = r0.length()     // Catch:{ IOException -> 0x01e9 }
            int r0 = (int) r14     // Catch:{ IOException -> 0x01e9 }
            int r14 = r0 / 1024
            int r0 = r1.f3387     // Catch:{ IOException -> 0x01e9 }
            int r15 = r0 / 1024
            r16 = 1
            com.lp.ʻ.C0947.m5879(r11, r12, r13, r14, r15, r16)     // Catch:{ IOException -> 0x01e9 }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x01e9 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x01e9 }
            java.net.HttpURLConnection r8 = r4.m5077()     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r10 = "Last-Modified"
            java.lang.String r8 = r8.getHeaderField(r10)     // Catch:{ IOException -> 0x01e9 }
            r1.f3383 = r8     // Catch:{ IOException -> 0x01e9 }
            android.content.SharedPreferences r8 = com.lp.C0987.m6073()     // Catch:{ IOException -> 0x01e9 }
            android.content.SharedPreferences$Editor r8 = r8.edit()     // Catch:{ IOException -> 0x01e9 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01e9 }
            r10.<init>()     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r11 = "download_resume_lastModified_"
            r10.append(r11)     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r11 = r1.f3390     // Catch:{ IOException -> 0x01e9 }
            r10.append(r11)     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r10 = r10.toString()     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r11 = r1.f3383     // Catch:{ IOException -> 0x01e9 }
            android.content.SharedPreferences$Editor r8 = r8.putString(r10, r11)     // Catch:{ IOException -> 0x01e9 }
            r8.commit()     // Catch:{ IOException -> 0x01e9 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01e9 }
            r8.<init>()     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r10 = "Write server lastModified:"
            r8.append(r10)     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r10 = r1.f3383     // Catch:{ IOException -> 0x01e9 }
            r8.append(r10)     // Catch:{ IOException -> 0x01e9 }
            java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x01e9 }
            com.lp.C0987.m6060(r8)     // Catch:{ IOException -> 0x01e9 }
            r8 = r0
            r11 = 0
        L_0x0196:
            byte[] r0 = new byte[r9]     // Catch:{ all -> 0x01d8 }
        L_0x0198:
            int r9 = r21.m5116()     // Catch:{ all -> 0x01d8 }
            if (r9 != 0) goto L_0x01ce
            int r9 = r7.read(r0)     // Catch:{ all -> 0x01d8 }
            r10 = -1
            if (r9 == r10) goto L_0x01ce
            long r13 = (long) r9     // Catch:{ all -> 0x01d8 }
            long r11 = r11 + r13
            r8.write(r0, r3, r9)     // Catch:{ all -> 0x01d8 }
            r9 = 1024(0x400, double:5.06E-321)
            long r9 = r11 / r9
            int r10 = (int) r9     // Catch:{ all -> 0x01d8 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x01d8 }
            com.lp.ʻ.C0947.m5876(r9)     // Catch:{ all -> 0x01d8 }
            int r9 = r1.f3387     // Catch:{ all -> 0x01d8 }
            long r9 = (long) r9     // Catch:{ all -> 0x01d8 }
            int r13 = (r11 > r9 ? 1 : (r11 == r9 ? 0 : -1))
            if (r13 != 0) goto L_0x0198
            int r9 = r1.f3387     // Catch:{ all -> 0x01d8 }
            long r9 = (long) r9     // Catch:{ all -> 0x01d8 }
            long r11 = r2.length()     // Catch:{ all -> 0x01d8 }
            int r13 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r13 != 0) goto L_0x01cb
            r1.m5114(r6)     // Catch:{ all -> 0x01d8 }
        L_0x01cb:
            r11 = 0
            goto L_0x0198
        L_0x01ce:
            r8.close()     // Catch:{ IOException -> 0x01e9 }
            r7.close()     // Catch:{ IOException -> 0x01e9 }
            r4.m5083()     // Catch:{ IOException -> 0x01e9 }
            goto L_0x01e3
        L_0x01d8:
            r0 = move-exception
            r8.close()     // Catch:{ IOException -> 0x01e9 }
            r7.close()     // Catch:{ IOException -> 0x01e9 }
            r4.m5083()     // Catch:{ IOException -> 0x01e9 }
            throw r0     // Catch:{ IOException -> 0x01e9 }
        L_0x01e3:
            if (r4 == 0) goto L_0x01f2
            r4.m5083()     // Catch:{ IOException -> 0x01e9 }
            goto L_0x01f2
        L_0x01e9:
            r0 = move-exception
            r0.printStackTrace()
            if (r4 == 0) goto L_0x01f2
            r4.m5083()
        L_0x01f2:
            r0 = r5
            goto L_0x0023
        L_0x01f5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chelpus.C0814.m5113():void");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m5112(File file) {
        C0947.m5877(C0815.m5205((int) R.string.download_progress_dialog_message_prepare_downloading));
        boolean z = true;
        C0804 r1 = C0815.m5242(this.f3390);
        if (r1 != null) {
            this.f3391 = r1.m5077().getHeaderField("Last-Modified");
            this.f3387 = r1.m5077().getContentLength();
            this.f3385 = file;
            C0987.m6060((Object) "10");
            if (this.f3385.exists() && this.f3385.length() < ((long) this.f3387) && this.f3391.equalsIgnoreCase(this.f3383)) {
                z = false;
            }
            this.f3386 = z;
            return;
        }
        throw new IOException("Prepare download is fail!");
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x00b0 A[SYNTHETIC, Splitter:B:36:0x00b0] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0221 A[Catch:{ all -> 0x0219, IOException -> 0x0229 }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0225 A[Catch:{ all -> 0x0219, IOException -> 0x0229 }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x023f  */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] m5115(boolean r30) {
        /*
            r29 = this;
            r1 = r29
            java.lang.String r2 = "last_download_"
            r0 = 0
            r3 = 0
            r4 = r0
            r5 = r4
            r0 = 0
        L_0x0009:
            r6 = 30
            if (r0 >= r6) goto L_0x0246
            int r6 = r29.m5116()
            r7 = 1
            if (r6 == r7) goto L_0x0246
            int r6 = r29.m5116()
            r8 = 4
            if (r6 == r8) goto L_0x0246
            int r6 = r0 + 1
            r0 = 5
            if (r6 <= r0) goto L_0x002b
            r9 = 5000(0x1388, double:2.4703E-320)
            java.lang.Thread.sleep(r9)     // Catch:{ InterruptedException -> 0x0026 }
            goto L_0x002b
        L_0x0026:
            r0 = move-exception
            r9 = r0
            r9.printStackTrace()
        L_0x002b:
            int r0 = r1.f3392     // Catch:{ IOException -> 0x0237 }
            r9 = r30
            r1.m5111(r0, r4, r9)     // Catch:{ IOException -> 0x0235 }
            android.content.SharedPreferences r0 = com.lp.C0987.m6073()     // Catch:{ IOException -> 0x0235 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0235 }
            r10.<init>()     // Catch:{ IOException -> 0x0235 }
            r10.append(r2)     // Catch:{ IOException -> 0x0235 }
            java.lang.String r11 = r1.f3390     // Catch:{ IOException -> 0x0235 }
            r10.append(r11)     // Catch:{ IOException -> 0x0235 }
            java.lang.String r10 = r10.toString()     // Catch:{ IOException -> 0x0235 }
            int r0 = r0.getInt(r10, r3)     // Catch:{ IOException -> 0x0235 }
            int r10 = r1.f3387     // Catch:{ IOException -> 0x0235 }
            if (r0 != r10) goto L_0x0070
            r1.m5114(r8)     // Catch:{ IOException -> 0x0235 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0235 }
            r0.<init>()     // Catch:{ IOException -> 0x0235 }
            java.lang.String r7 = "file "
            r0.append(r7)     // Catch:{ IOException -> 0x0235 }
            java.lang.String r7 = r1.f3390     // Catch:{ IOException -> 0x0235 }
            r0.append(r7)     // Catch:{ IOException -> 0x0235 }
            java.lang.String r7 = " downloaded before. Skip dowbload."
            r0.append(r7)     // Catch:{ IOException -> 0x0235 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0235 }
            com.lp.C0987.m6060(r0)     // Catch:{ IOException -> 0x0235 }
            byte[] r0 = new byte[r3]     // Catch:{ IOException -> 0x0235 }
            return r0
        L_0x0070:
            if (r4 != 0) goto L_0x0077
            int r0 = r1.f3387     // Catch:{ IOException -> 0x0235 }
            byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x0235 }
            r4 = r0
        L_0x0077:
            boolean r0 = r1.f3386     // Catch:{ IOException -> 0x0231 }
            if (r0 != 0) goto L_0x00a1
            if (r5 == 0) goto L_0x0080
            r5.m5083()     // Catch:{ IOException -> 0x0235 }
        L_0x0080:
            r0 = 2
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ IOException -> 0x0235 }
            java.lang.String r8 = r1.f3390     // Catch:{ IOException -> 0x0235 }
            r0[r3] = r8     // Catch:{ IOException -> 0x0235 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0235 }
            r8.<init>()     // Catch:{ IOException -> 0x0235 }
            java.lang.String r10 = ""
            r8.append(r10)     // Catch:{ IOException -> 0x0235 }
            int r10 = r1.f3392     // Catch:{ IOException -> 0x0235 }
            r8.append(r10)     // Catch:{ IOException -> 0x0235 }
            java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x0235 }
            r0[r7] = r8     // Catch:{ IOException -> 0x0235 }
            com.chelpus.ʼ r0 = com.chelpus.C0815.m5242(r0)     // Catch:{ IOException -> 0x0235 }
            goto L_0x00ad
        L_0x00a1:
            if (r5 != 0) goto L_0x00ae
            java.lang.String[] r0 = new java.lang.String[r7]     // Catch:{ IOException -> 0x0235 }
            java.lang.String r8 = r1.f3390     // Catch:{ IOException -> 0x0235 }
            r0[r3] = r8     // Catch:{ IOException -> 0x0235 }
            com.chelpus.ʼ r0 = com.chelpus.C0815.m5242(r0)     // Catch:{ IOException -> 0x0235 }
        L_0x00ad:
            r5 = r0
        L_0x00ae:
            if (r5 == 0) goto L_0x0221
            r1.m5114(r3)     // Catch:{ IOException -> 0x0231 }
            r0 = 2131689815(0x7f0f0157, float:1.9008656E38)
            java.lang.String r10 = com.chelpus.C0815.m5205(r0)     // Catch:{ IOException -> 0x0231 }
            java.lang.String r11 = r1.f3390     // Catch:{ IOException -> 0x0231 }
            java.lang.String r12 = ""
            int r8 = r1.f3392     // Catch:{ IOException -> 0x0231 }
            int r13 = r8 / 1024
            int r8 = r1.f3387     // Catch:{ IOException -> 0x0231 }
            int r14 = r8 / 1024
            r15 = 1
            com.lp.ʻ.C0947.m5879(r10, r11, r12, r13, r14, r15)     // Catch:{ IOException -> 0x0231 }
            java.io.BufferedInputStream r8 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x0231 }
            java.net.HttpURLConnection r10 = r5.m5077()     // Catch:{ IOException -> 0x0231 }
            java.io.InputStream r10 = r10.getInputStream()     // Catch:{ IOException -> 0x0231 }
            r11 = 8192(0x2000, float:1.14794E-41)
            r8.<init>(r10, r11)     // Catch:{ IOException -> 0x0231 }
            boolean r10 = r1.f3386     // Catch:{ IOException -> 0x0231 }
            java.lang.String r12 = "\n"
            r13 = 0
            if (r10 != 0) goto L_0x011b
            int r10 = r1.f3392     // Catch:{ IOException -> 0x0231 }
            r16 = r4
            long r3 = (long) r10
            long r3 = r3 + r13
            java.lang.String r17 = com.chelpus.C0815.m5205(r0)     // Catch:{ IOException -> 0x0229 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0229 }
            r0.<init>()     // Catch:{ IOException -> 0x0229 }
            r10 = 2131689818(0x7f0f015a, float:1.9008662E38)
            java.lang.String r10 = com.chelpus.C0815.m5205(r10)     // Catch:{ IOException -> 0x0229 }
            r0.append(r10)     // Catch:{ IOException -> 0x0229 }
            r0.append(r12)     // Catch:{ IOException -> 0x0229 }
            java.lang.String r10 = r1.f3390     // Catch:{ IOException -> 0x0229 }
            r0.append(r10)     // Catch:{ IOException -> 0x0229 }
            java.lang.String r18 = r0.toString()     // Catch:{ IOException -> 0x0229 }
            java.lang.String r19 = ""
            int r0 = r1.f3392     // Catch:{ IOException -> 0x0229 }
            int r0 = r0 / 1024
            int r10 = r1.f3387     // Catch:{ IOException -> 0x0229 }
            int r10 = r10 / 1024
            r22 = 1
            r20 = r0
            r21 = r10
            com.lp.ʻ.C0947.m5879(r17, r18, r19, r20, r21, r22)     // Catch:{ IOException -> 0x0229 }
            goto L_0x0196
        L_0x011b:
            r16 = r4
            java.lang.String r23 = com.chelpus.C0815.m5205(r0)     // Catch:{ IOException -> 0x0229 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0229 }
            r0.<init>()     // Catch:{ IOException -> 0x0229 }
            r3 = 2131689816(0x7f0f0158, float:1.9008658E38)
            java.lang.String r3 = com.chelpus.C0815.m5205(r3)     // Catch:{ IOException -> 0x0229 }
            r0.append(r3)     // Catch:{ IOException -> 0x0229 }
            r0.append(r12)     // Catch:{ IOException -> 0x0229 }
            java.lang.String r3 = r1.f3390     // Catch:{ IOException -> 0x0229 }
            r0.append(r3)     // Catch:{ IOException -> 0x0229 }
            java.lang.String r24 = r0.toString()     // Catch:{ IOException -> 0x0229 }
            java.lang.String r25 = ""
            int r0 = r1.f3392     // Catch:{ IOException -> 0x0229 }
            int r0 = r0 / 1024
            int r3 = r1.f3387     // Catch:{ IOException -> 0x0229 }
            int r3 = r3 / 1024
            r28 = 1
            r26 = r0
            r27 = r3
            com.lp.ʻ.C0947.m5879(r23, r24, r25, r26, r27, r28)     // Catch:{ IOException -> 0x0229 }
            java.net.HttpURLConnection r0 = r5.m5077()     // Catch:{ IOException -> 0x0229 }
            java.lang.String r3 = "Last-Modified"
            java.lang.String r0 = r0.getHeaderField(r3)     // Catch:{ IOException -> 0x0229 }
            r1.f3383 = r0     // Catch:{ IOException -> 0x0229 }
            android.content.SharedPreferences r0 = com.lp.C0987.m6073()     // Catch:{ IOException -> 0x0229 }
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ IOException -> 0x0229 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0229 }
            r3.<init>()     // Catch:{ IOException -> 0x0229 }
            java.lang.String r4 = "download_resume_lastModified_"
            r3.append(r4)     // Catch:{ IOException -> 0x0229 }
            java.lang.String r4 = r1.f3390     // Catch:{ IOException -> 0x0229 }
            r3.append(r4)     // Catch:{ IOException -> 0x0229 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0229 }
            java.lang.String r4 = r1.f3383     // Catch:{ IOException -> 0x0229 }
            android.content.SharedPreferences$Editor r0 = r0.putString(r3, r4)     // Catch:{ IOException -> 0x0229 }
            r0.commit()     // Catch:{ IOException -> 0x0229 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0229 }
            r0.<init>()     // Catch:{ IOException -> 0x0229 }
            java.lang.String r3 = "Write server lastModified:"
            r0.append(r3)     // Catch:{ IOException -> 0x0229 }
            java.lang.String r3 = r1.f3383     // Catch:{ IOException -> 0x0229 }
            r0.append(r3)     // Catch:{ IOException -> 0x0229 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0229 }
            com.lp.C0987.m6060(r0)     // Catch:{ IOException -> 0x0229 }
            r3 = r13
        L_0x0196:
            byte[] r0 = new byte[r11]     // Catch:{ all -> 0x0219 }
        L_0x0198:
            int r10 = r29.m5116()     // Catch:{ all -> 0x0219 }
            if (r10 != 0) goto L_0x0212
            int r10 = r8.read(r0)     // Catch:{ all -> 0x0219 }
            r11 = -1
            if (r10 == r11) goto L_0x0212
            long r11 = (long) r10     // Catch:{ all -> 0x0219 }
            long r3 = r3 + r11
            r11 = 0
        L_0x01a8:
            if (r11 >= r10) goto L_0x01b4
            int r12 = r1.f3392     // Catch:{ all -> 0x0219 }
            int r12 = r12 + r11
            byte r17 = r0[r11]     // Catch:{ all -> 0x0219 }
            r16[r12] = r17     // Catch:{ all -> 0x0219 }
            int r11 = r11 + 1
            goto L_0x01a8
        L_0x01b4:
            int r11 = r1.f3392     // Catch:{ all -> 0x0219 }
            int r11 = r11 + r10
            r1.f3392 = r11     // Catch:{ all -> 0x0219 }
            r10 = 1024(0x400, double:5.06E-321)
            long r10 = r3 / r10
            int r11 = (int) r10     // Catch:{ all -> 0x0219 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x0219 }
            com.lp.ʻ.C0947.m5876(r10)     // Catch:{ all -> 0x0219 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x0219 }
            r10.<init>()     // Catch:{ all -> 0x0219 }
            r10.append(r3)     // Catch:{ all -> 0x0219 }
            java.lang.String r11 = " "
            r10.append(r11)     // Catch:{ all -> 0x0219 }
            int r11 = r1.f3387     // Catch:{ all -> 0x0219 }
            r10.append(r11)     // Catch:{ all -> 0x0219 }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x0219 }
            com.lp.C0987.m6060(r10)     // Catch:{ all -> 0x0219 }
            int r10 = r1.f3387     // Catch:{ all -> 0x0219 }
            long r10 = (long) r10     // Catch:{ all -> 0x0219 }
            int r12 = (r3 > r10 ? 1 : (r3 == r10 ? 0 : -1))
            if (r12 != 0) goto L_0x0198
            int r3 = r1.f3387     // Catch:{ all -> 0x0219 }
            int r4 = r1.f3392     // Catch:{ all -> 0x0219 }
            if (r3 != r4) goto L_0x0210
            r1.m5114(r7)     // Catch:{ all -> 0x0219 }
            android.content.SharedPreferences r3 = com.lp.C0987.m6073()     // Catch:{ all -> 0x0219 }
            android.content.SharedPreferences$Editor r3 = r3.edit()     // Catch:{ all -> 0x0219 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0219 }
            r4.<init>()     // Catch:{ all -> 0x0219 }
            r4.append(r2)     // Catch:{ all -> 0x0219 }
            java.lang.String r10 = r1.f3390     // Catch:{ all -> 0x0219 }
            r4.append(r10)     // Catch:{ all -> 0x0219 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0219 }
            int r10 = r1.f3392     // Catch:{ all -> 0x0219 }
            android.content.SharedPreferences$Editor r3 = r3.putInt(r4, r10)     // Catch:{ all -> 0x0219 }
            r3.commit()     // Catch:{ all -> 0x0219 }
        L_0x0210:
            r3 = r13
            goto L_0x0198
        L_0x0212:
            r8.close()     // Catch:{ IOException -> 0x0229 }
            r5.m5083()     // Catch:{ IOException -> 0x0229 }
            goto L_0x0223
        L_0x0219:
            r0 = move-exception
            r8.close()     // Catch:{ IOException -> 0x0229 }
            r5.m5083()     // Catch:{ IOException -> 0x0229 }
            throw r0     // Catch:{ IOException -> 0x0229 }
        L_0x0221:
            r16 = r4
        L_0x0223:
            if (r5 == 0) goto L_0x022d
            r5.m5083()     // Catch:{ IOException -> 0x0229 }
            goto L_0x022d
        L_0x0229:
            r0 = move-exception
            r4 = r16
            goto L_0x023a
        L_0x022d:
            r0 = r6
            r4 = r16
            goto L_0x0243
        L_0x0231:
            r0 = move-exception
            r16 = r4
            goto L_0x023a
        L_0x0235:
            r0 = move-exception
            goto L_0x023a
        L_0x0237:
            r0 = move-exception
            r9 = r30
        L_0x023a:
            r0.printStackTrace()
            if (r5 == 0) goto L_0x0242
            r5.m5083()
        L_0x0242:
            r0 = r6
        L_0x0243:
            r3 = 0
            goto L_0x0009
        L_0x0246:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chelpus.C0814.m5115(boolean):byte[]");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m5111(int i, byte[] bArr, boolean z) {
        C0987.m6060((Object) "Prepare download fro array");
        C0947.m5877(C0815.m5205((int) R.string.download_progress_dialog_message_prepare_downloading));
        boolean z2 = true;
        C0804 r1 = C0815.m5242(this.f3390);
        if (r1 != null) {
            this.f3391 = r1.m5077().getHeaderField("Last-Modified");
            this.f3387 = r1.m5077().getContentLength();
            if (bArr == null) {
                byte[] bArr2 = new byte[this.f3387];
            }
            C0987.m6060((Object) "10");
            if (i < this.f3387 && this.f3391.equalsIgnoreCase(this.f3383)) {
                z2 = false;
            }
            this.f3386 = z2;
            return;
        }
        if (z) {
            m5114(4);
            C0987.m6060((Object) "File not found on server");
        }
        throw new IOException("Prepare download is fail!");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m5116() {
        return this.f3388;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5114(int i) {
        this.f3388 = i;
    }
}
