package com.chelpus;

import android.os.Build;
import ru.pKkcGXHI.kKSaIWSZS.PkgName;

/* renamed from: com.chelpus.ʻ  reason: contains not printable characters */
/* compiled from: Common */
public class C0776 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final String f3092 = PkgName.getPkgName();

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final String f3093 = (f3092 + "_preferences");

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final int f3094 = Build.VERSION.SDK_INT;

    /* renamed from: ʾ  reason: contains not printable characters */
    public static final boolean f3095 = (f3094 >= 16);

    /* renamed from: ʿ  reason: contains not printable characters */
    public static final boolean f3096 = (f3094 >= 17);

    /* renamed from: ˆ  reason: contains not printable characters */
    public static final boolean f3097 = (f3094 >= 18);

    /* renamed from: ˈ  reason: contains not printable characters */
    public static final boolean f3098 = (f3094 >= 19);

    /* renamed from: ˉ  reason: contains not printable characters */
    public static final boolean f3099;

    /* renamed from: ˊ  reason: contains not printable characters */
    public static final String f3100 = (f3099 ? "com.android.server.devicepolicy.DevicePolicyManagerService" : "com.android.server.DevicePolicyManagerService");

    static {
        boolean z = true;
        if (f3094 < 21) {
            z = false;
        }
        f3099 = z;
    }
}
