package com.chelpus;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;

/* renamed from: com.chelpus.ʽ  reason: contains not printable characters */
/* compiled from: ITransferFilesServiceInterface */
public interface C0811 extends IInterface {
    /* renamed from: ʻ  reason: contains not printable characters */
    ParcelFileDescriptor m5105(String str);

    /* renamed from: ʻ  reason: contains not printable characters */
    boolean m5106();

    /* renamed from: ʼ  reason: contains not printable characters */
    ParcelFileDescriptor m5107();

    /* renamed from: ʼ  reason: contains not printable characters */
    String m5108(String str);

    /* renamed from: ʽ  reason: contains not printable characters */
    ParcelFileDescriptor m5109();

    /* renamed from: ʾ  reason: contains not printable characters */
    ParcelFileDescriptor m5110();

    /* renamed from: com.chelpus.ʽ$ʻ  reason: contains not printable characters */
    /* compiled from: ITransferFilesServiceInterface */
    public static abstract class C0812 extends Binder implements C0811 {
        public IBinder asBinder() {
            return this;
        }

        public C0812() {
            attachInterface(this, "com.chelpus.ITransferFilesServiceInterface");
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            if (i != 1598968902) {
                switch (i) {
                    case 1:
                        parcel.enforceInterface("com.chelpus.ITransferFilesServiceInterface");
                        boolean r4 = m5106();
                        parcel2.writeNoException();
                        parcel2.writeInt(r4 ? 1 : 0);
                        return true;
                    case 2:
                        parcel.enforceInterface("com.chelpus.ITransferFilesServiceInterface");
                        ParcelFileDescriptor r42 = m5107();
                        parcel2.writeNoException();
                        if (r42 != null) {
                            parcel2.writeInt(1);
                            r42.writeToParcel(parcel2, 1);
                        } else {
                            parcel2.writeInt(0);
                        }
                        return true;
                    case 3:
                        parcel.enforceInterface("com.chelpus.ITransferFilesServiceInterface");
                        ParcelFileDescriptor r43 = m5109();
                        parcel2.writeNoException();
                        if (r43 != null) {
                            parcel2.writeInt(1);
                            r43.writeToParcel(parcel2, 1);
                        } else {
                            parcel2.writeInt(0);
                        }
                        return true;
                    case 4:
                        parcel.enforceInterface("com.chelpus.ITransferFilesServiceInterface");
                        ParcelFileDescriptor r44 = m5110();
                        parcel2.writeNoException();
                        if (r44 != null) {
                            parcel2.writeInt(1);
                            r44.writeToParcel(parcel2, 1);
                        } else {
                            parcel2.writeInt(0);
                        }
                        return true;
                    case 5:
                        parcel.enforceInterface("com.chelpus.ITransferFilesServiceInterface");
                        ParcelFileDescriptor r45 = m5105(parcel.readString());
                        parcel2.writeNoException();
                        if (r45 != null) {
                            parcel2.writeInt(1);
                            r45.writeToParcel(parcel2, 1);
                        } else {
                            parcel2.writeInt(0);
                        }
                        return true;
                    case 6:
                        parcel.enforceInterface("com.chelpus.ITransferFilesServiceInterface");
                        String r46 = m5108(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeString(r46);
                        return true;
                    default:
                        return super.onTransact(i, parcel, parcel2, i2);
                }
            } else {
                parcel2.writeString("com.chelpus.ITransferFilesServiceInterface");
                return true;
            }
        }
    }
}
