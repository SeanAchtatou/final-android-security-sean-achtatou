package com.chelpus.ʻ.ʼ;

import java.util.ArrayList;
import java.util.Iterator;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

/* renamed from: com.chelpus.ʻ.ʼ.ٴ  reason: contains not printable characters */
/* compiled from: PatchesItemAuto */
public class C0796 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public byte[] f3272;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public byte[][] f3273;

    /* renamed from: ʼ  reason: contains not printable characters */
    public int[] f3274;

    /* renamed from: ʽ  reason: contains not printable characters */
    public byte[] f3275;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public boolean f3276 = false;

    /* renamed from: ʾ  reason: contains not printable characters */
    public int[] f3277;

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean f3278 = false;

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean f3279 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    public String f3280 = BuildConfig.FLAVOR;

    /* renamed from: ˉ  reason: contains not printable characters */
    public String f3281 = BuildConfig.FLAVOR;

    /* renamed from: ˊ  reason: contains not printable characters */
    public String f3282 = BuildConfig.FLAVOR;

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean f3283 = false;

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean f3284 = false;

    /* renamed from: ˏ  reason: contains not printable characters */
    public ArrayList<Byte> f3285 = new ArrayList<>();

    /* renamed from: ˑ  reason: contains not printable characters */
    public ArrayList<Byte> f3286 = new ArrayList<>();

    /* renamed from: י  reason: contains not printable characters */
    public boolean f3287 = false;

    /* renamed from: ـ  reason: contains not printable characters */
    public boolean f3288 = false;

    /* renamed from: ٴ  reason: contains not printable characters */
    public String f3289 = null;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public String f3290 = null;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public boolean f3291 = false;

    /* renamed from: ᴵ  reason: contains not printable characters */
    public String f3292 = null;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public boolean f3293 = false;

    /* renamed from: ᵎ  reason: contains not printable characters */
    public String f3294 = null;

    /* renamed from: ᵔ  reason: contains not printable characters */
    public String f3295 = null;

    /* renamed from: ᵢ  reason: contains not printable characters */
    public String f3296 = null;

    /* renamed from: ⁱ  reason: contains not printable characters */
    public String f3297 = null;

    /* renamed from: ﹳ  reason: contains not printable characters */
    public int f3298 = 0;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public int f3299 = 0;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public boolean f3300 = false;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public boolean f3301 = false;

    public C0796(byte[] bArr, int[] iArr, byte[] bArr2, int[] iArr2, boolean z, String str, String str2, String str3, String str4, String str5, String str6, int i, String str7, String str8, String str9, String str10) {
        byte[] bArr3 = bArr;
        int[] iArr3 = iArr;
        byte[] bArr4 = bArr2;
        int[] iArr4 = iArr2;
        boolean z2 = z;
        this.f3272 = new byte[bArr3.length];
        this.f3272 = bArr3;
        this.f3274 = new int[iArr3.length];
        this.f3274 = iArr3;
        this.f3275 = new byte[bArr4.length];
        this.f3275 = bArr4;
        this.f3277 = new int[iArr4.length];
        this.f3277 = iArr4;
        this.f3279 = z2;
        this.f3287 = z2;
        this.f3280 = str;
        this.f3281 = str2;
        this.f3289 = str3;
        this.f3290 = str4;
        this.f3294 = str5;
        this.f3295 = str6;
        this.f3299 = i;
        this.f3296 = str7;
        this.f3297 = str10;
        this.f3292 = str8;
        this.f3282 = str9;
    }

    public C0796(C0796 r8, ArrayList<C0788> arrayList) {
        this.f3272 = new byte[r8.f3272.length];
        byte[] bArr = r8.f3272;
        System.arraycopy(bArr, 0, this.f3272, 0, bArr.length);
        this.f3274 = new int[r8.f3274.length];
        int[] iArr = r8.f3274;
        System.arraycopy(iArr, 0, this.f3274, 0, iArr.length);
        this.f3275 = new byte[r8.f3275.length];
        byte[] bArr2 = r8.f3275;
        System.arraycopy(bArr2, 0, this.f3275, 0, bArr2.length);
        this.f3277 = new int[r8.f3277.length];
        int[] iArr2 = r8.f3277;
        System.arraycopy(iArr2, 0, this.f3277, 0, iArr2.length);
        this.f3279 = r8.f3279;
        this.f3287 = r8.f3287;
        this.f3280 = r8.f3280;
        this.f3281 = r8.f3281;
        this.f3289 = r8.f3289;
        this.f3290 = r8.f3290;
        this.f3294 = r8.f3294;
        this.f3295 = r8.f3295;
        this.f3299 = r8.f3299;
        this.f3296 = r8.f3296;
        this.f3297 = r8.f3297;
        this.f3292 = r8.f3292;
        this.f3282 = r8.f3282;
        ArrayList arrayList2 = new ArrayList();
        Iterator<C0788> it = arrayList.iterator();
        while (it.hasNext()) {
            C0788 next = it.next();
            if (next.f3212.equals(BuildConfig.FLAVOR) && next.f3213.equals(r8.f3295) && next.f3214.equals(r8.f3297)) {
                arrayList2.add(new byte[]{next.f3218[0], next.f3218[1]});
            }
        }
        if (arrayList2.size() > 0) {
            this.f3273 = new byte[arrayList2.size()][];
            this.f3273 = (byte[][]) arrayList2.toArray(this.f3273);
            this.f3276 = true;
            return;
        }
        this.f3288 = true;
    }
}
