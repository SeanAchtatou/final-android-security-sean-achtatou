package com.chelpus.ʻ.ʼ;

import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

/* renamed from: com.chelpus.ʻ.ʼ.ˉ  reason: contains not printable characters */
/* compiled from: CommandItem */
public class C0788 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public String f3212 = BuildConfig.FLAVOR;

    /* renamed from: ʼ  reason: contains not printable characters */
    public String f3213 = BuildConfig.FLAVOR;

    /* renamed from: ʽ  reason: contains not printable characters */
    public String f3214 = BuildConfig.FLAVOR;

    /* renamed from: ʾ  reason: contains not printable characters */
    public byte[] f3215 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    public byte[] f3216 = null;

    /* renamed from: ˆ  reason: contains not printable characters */
    public byte[] f3217 = null;

    /* renamed from: ˈ  reason: contains not printable characters */
    public byte[] f3218 = null;

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean f3219 = false;

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean f3220 = false;

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean f3221 = false;

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean f3222 = false;

    /* renamed from: ˏ  reason: contains not printable characters */
    public byte[] f3223 = null;

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean f3224 = false;

    public C0788(String str, String str2) {
        this.f3212 = str;
        this.f3213 = str2;
        this.f3215 = new byte[4];
        this.f3216 = new byte[4];
        this.f3219 = false;
        this.f3217 = new byte[2];
        this.f3218 = new byte[2];
        this.f3214 = BuildConfig.FLAVOR;
        this.f3222 = false;
    }

    public C0788(String str, String str2, String str3) {
        this.f3212 = str;
        this.f3213 = str2;
        if (str3 == null || str3.equals(BuildConfig.FLAVOR)) {
            this.f3214 = BuildConfig.FLAVOR;
            this.f3222 = false;
        } else {
            this.f3222 = true;
            this.f3214 = str3;
            this.f3223 = new byte[4];
        }
        this.f3215 = new byte[4];
        this.f3216 = new byte[4];
        this.f3219 = false;
        this.f3217 = new byte[2];
        this.f3218 = new byte[2];
    }
}
