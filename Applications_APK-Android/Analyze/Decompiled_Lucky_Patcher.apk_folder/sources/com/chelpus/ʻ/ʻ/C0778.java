package com.chelpus.ʻ.ʻ;

import com.chelpus.C0815;
import java.util.ArrayList;
import java.util.Iterator;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.chelpus.ʻ.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: CustomPatchIdStrings */
public class C0778 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String f3111 = "for_insert_string_id_";

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int f3112 = 1;

    /* renamed from: ʽ  reason: contains not printable characters */
    public static int f3113 = 2;

    /* renamed from: ʾ  reason: contains not printable characters */
    public static int f3114 = 3;

    /* renamed from: ˆ  reason: contains not printable characters */
    public static ArrayList<C0779> f3115;

    /* renamed from: ʿ  reason: contains not printable characters */
    public final ArrayList<C0779> f3116 = new ArrayList<C0779>() {
        {
            add(new C0779("Run next custom patch", 100));
            add(new C0779("Bytes by serach", 101));
            add(new C0779("Bytes not found!", 102));
            add(new C0779("Patch on Reboot added!", 103));
            add(new C0779("Skip patch for build", 104));
            add(new C0779("Pattern", 105));
            add(new C0779("Patch done!", 106));
            add(new C0779("Offset", 107));
            add(new C0779("Pattern not found!", 108));
            add(new C0779("or patch is already applied?!", 109));
            add(new C0779("Patch for", 110));
            add(new C0779("Patch for libraries", 111));
            add(new C0779("Patch for files from directory", 112));
            add(new C0779("Blocking internet hosts", 113));
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m5025(String str) {
        if (f3115 == null) {
            f3115 = new C0778().f3116;
        }
        Iterator<C0779> it = f3115.iterator();
        while (it.hasNext()) {
            C0779 next = it.next();
            if (next.f3118.equals(str)) {
                return f3111 + next.f3119;
            }
        }
        return "bad_string_id:" + str;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static String m5026(String str) {
        return str.replaceAll(m5025("Run next custom patch"), C0815.m5205((int) R.string.custom_patch_log_next_custom_patch)).replaceAll(m5025("Bytes by serach"), C0815.m5205((int) R.string.custom_patch_log_search_bytes)).replaceAll(m5025("Bytes not found!"), C0815.m5205((int) R.string.custom_patch_log_search_bytes_error)).replaceAll(m5025("Patch on Reboot added!"), C0815.m5205((int) R.string.custom_patch_log_patch_on_reboot_added)).replaceAll(m5025("Skip patch for build"), C0815.m5205((int) R.string.custom_patch_log_skip_patch_for_build)).replaceAll(m5025("Pattern"), C0815.m5205((int) R.string.custom_patch_log_pattern)).replaceAll(m5025("Patch done!"), C0815.m5205((int) R.string.custom_patch_log_patch_done)).replaceAll(m5025("Offset"), C0815.m5205((int) R.string.custom_patch_log_offset)).replaceAll(m5025("Pattern not found!"), C0815.m5205((int) R.string.custom_patch_log_pattern_not_found)).replaceAll(m5025("or patch is already applied?!"), C0815.m5205((int) R.string.custom_patch_log_or_patch_is_already_applied)).replaceAll(m5025("Patch for"), C0815.m5205((int) R.string.custom_patch_log_patch_for)).replaceAll(m5025("Patch for libraries"), C0815.m5205((int) R.string.custom_patch_log_patch_for_libraries)).replaceAll(m5025("Patch for files from directory"), C0815.m5205((int) R.string.custom_patch_log_patch_all_files)).replaceAll(m5025("Blocking internet hosts"), C0815.m5205((int) R.string.custom_patch_log_block_internet_hosts));
    }

    /* renamed from: com.chelpus.ʻ.ʻ.ʻ$ʻ  reason: contains not printable characters */
    /* compiled from: CustomPatchIdStrings */
    public class C0779 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public String f3118 = BuildConfig.FLAVOR;

        /* renamed from: ʼ  reason: contains not printable characters */
        public int f3119 = 0;

        public C0779(String str, int i) {
            this.f3118 = str;
            this.f3119 = i;
        }
    }
}
