package com.chelpus;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.util.Log;
import ru.pKkcGXHI.kKSaIWSZS.R;

public class RootlessInstallService extends Service {
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        int intExtra = intent.getIntExtra("android.content.pm.extra.STATUS", -999);
        if (intExtra == -1) {
            Log.d("RootlessInstallService", "Requesting user confirmation for installation");
            m5011(intent.getIntExtra("android.content.pm.extra.SESSION_ID", -1), 1, intent.getStringExtra("android.content.pm.extra.PACKAGE_NAME"));
            Intent intent2 = (Intent) intent.getParcelableExtra("android.intent.extra.INTENT");
            intent2.addFlags(268435456);
            try {
                startActivity(intent2);
            } catch (Exception unused) {
                m5012(intent.getIntExtra("android.content.pm.extra.SESSION_ID", -1), getString(R.string.installer_error_lidl_rom));
            }
        } else if (intExtra != 0) {
            Log.d("RootlessInstallService", "Installation failed");
            m5012(intent.getIntExtra("android.content.pm.extra.SESSION_ID", -1), m5013(intExtra, intent.getStringExtra("android.content.pm.extra.OTHER_PACKAGE_NAME")));
        } else {
            Log.d("RootlessInstallService", "Installation succeed");
            m5011(intent.getIntExtra("android.content.pm.extra.SESSION_ID", -1), 0, intent.getStringExtra("android.content.pm.extra.PACKAGE_NAME"));
        }
        stopSelf();
        return 2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m5011(int i, int i2, String str) {
        Intent intent = new Intent("com.lp.action.INSTALLATION_STATUS_NOTIFICATION");
        intent.putExtra("com.lp.extra.INSTALLATION_STATUS", i2);
        intent.putExtra("com.lp.extra.SESSION_ID", i);
        if (str != null) {
            intent.putExtra("com.lp.extra.PACKAGE_NAME", str);
        }
        sendBroadcast(intent);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m5012(int i, String str) {
        Intent intent = new Intent("com.lp.action.INSTALLATION_STATUS_NOTIFICATION");
        intent.putExtra("com.lp.extra.INSTALLATION_STATUS", 2);
        intent.putExtra("com.lp.extra.SESSION_ID", i);
        intent.putExtra("com.lp.extra.ERROR_DESCRIPTION", str);
        sendBroadcast(intent);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m5013(int i, String str) {
        String r4;
        switch (i) {
            case 2:
                String string = getString(R.string.installer_error_blocked_device);
                if (!(str == null || (r4 = m5010(getApplicationContext(), str)) == null)) {
                    string = r4;
                }
                return getString(R.string.installer_error_blocked, new Object[]{string});
            case 3:
                return getString(R.string.installer_error_aborted);
            case 4:
                return getString(R.string.installer_error_bad_apks);
            case 5:
                return getString(R.string.installer_error_conflict);
            case 6:
                return getString(R.string.installer_error_storage);
            case 7:
                return getString(R.string.installer_error_incompatible);
            default:
                return getString(R.string.installer_error_generic);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m5010(Context context, String str) {
        try {
            PackageManager packageManager = context.getPackageManager();
            return packageManager.getApplicationLabel(packageManager.getApplicationInfo(str, 0)).toString();
        } catch (Exception unused) {
            return null;
        }
    }
}
