package com.lp;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import com.chelpus.C0815;
import com.chelpus.C0820;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

public class LuckyApp extends Application {

    /* renamed from: ʻ  reason: contains not printable characters */
    static Context f3803 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    static String f3804 = "";

    /* renamed from: ʽ  reason: contains not printable characters */
    static String f3805 = "";

    /* renamed from: ʾ  reason: contains not printable characters */
    public static Activity f3806 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    public static Application f3807 = null;

    /* renamed from: ˆ  reason: contains not printable characters */
    private static volatile boolean f3808 = false;

    public void onCreate() {
        super.onCreate();
        f3807 = this;
        f3803 = getApplicationContext();
        m5830(this);
        String string = f3803.getSharedPreferences("config", 4).getString("basepath", BuildConfig.FLAVOR);
        C0987.f4438 = string;
        f3805 = string;
        try {
            f3804 = f3803.getPackageManager().getPackageInfo(f3803.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread thread, Throwable th) {
                C0987.m6060((Object) ("FATAL Exception LP " + th.toString()));
                if (C0987.f4474 && C0815.m5316().equals("enforce")) {
                    try {
                        new C0815(BuildConfig.FLAVOR).m5353("setenforce 1");
                    } catch (Exception unused) {
                    }
                }
                if (th.toString().contains("No space left on device")) {
                    LuckyApp.this.getSharedPreferences("config", 4).edit().putString("force_close_info", "No space left on device").commit();
                }
                if (th.toString().contains("OutOfMemoryError")) {
                    LuckyApp.this.getSharedPreferences("config", 4).edit().putString("force_close_info", "OutOfMemoryError").commit();
                }
                try {
                    C0987.m6079(LuckyApp.this.getApplicationContext());
                    String str = LuckyApp.f3804;
                    if (LuckyApp.f3805.equals(BuildConfig.FLAVOR)) {
                        LuckyApp.f3805 = LuckyApp.f3803.getDir("error_log", 0).getAbsolutePath();
                    }
                    File file = new File(LuckyApp.f3805 + "/Log/Exception." + str + ".txt");
                    StringWriter stringWriter = new StringWriter();
                    th.printStackTrace(new PrintWriter(stringWriter));
                    C0815.m5278(file, "Lucky Pacther ver. " + str + "\n\n " + stringWriter.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                th.printStackTrace();
                boolean z = LuckyApp.this.getSharedPreferences("config", 4).getBoolean("force_close", false);
                LuckyApp.this.getSharedPreferences("config", 4).edit().putBoolean("force_close", true).commit();
                try {
                    new C0973().m5994(LuckyApp.f3803, true);
                    if (!z) {
                        LuckyApp.this.startActivity(C0987.m6068().getLaunchIntentForPackage(LuckyApp.this.getPackageName()));
                    }
                } catch (RuntimeException e2) {
                    e2.printStackTrace();
                } catch (Exception e3) {
                    System.exit(0);
                    e3.printStackTrace();
                }
                if (C0987.f4474) {
                    C0987.m6075();
                }
                System.exit(0);
            }
        });
    }

    public void onTerminate() {
        super.onTerminate();
        if (C0987.f4474 && C0815.m5316().equals("enforce")) {
            try {
                new C0815(BuildConfig.FLAVOR).m5353("setenforce 1");
            } catch (Exception unused) {
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Context m5829() {
        return f3803;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m5830(Context context) {
        if (!f3808) {
            Context applicationContext = context.getApplicationContext();
            C0987.m6060((Object) "LuckyPatcher: PackageChangeReceiver");
            PackageChangeReceiver packageChangeReceiver = new PackageChangeReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
            intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
            intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
            intentFilter.addAction("android.intent.action.PACKAGE_CHANGED");
            intentFilter.addAction("android.intent.action.MY_PACKAGE_REPLACED");
            intentFilter.addDataScheme("package");
            applicationContext.registerReceiver(packageChangeReceiver, intentFilter);
            C0987.m6060((Object) "LuckyPatcher: BinderLuckyPatcher");
            BinderLuckyPatcher binderLuckyPatcher = new BinderLuckyPatcher();
            IntentFilter intentFilter2 = new IntentFilter();
            intentFilter2.addAction("android.intent.action.BOOT_COMPLETED");
            intentFilter2.addAction("android.intent.action.MEDIA_SCANNER_FINISHED");
            intentFilter2.addAction("android.intent.action.MEDIA_SCANNER_STARTED");
            intentFilter2.addAction("android.intent.action.MEDIA_MOUNTED");
            intentFilter2.addAction("android.intent.action.MEDIA_EJECT");
            intentFilter2.addAction("android.intent.action.MEDIA_UNMOUNTED");
            intentFilter2.addAction("android.intent.action.MEDIA_REMOVED");
            intentFilter2.addDataScheme("file");
            applicationContext.registerReceiver(binderLuckyPatcher, intentFilter2);
            C0987.m6060((Object) "LuckyPatcher: OnBootLuckyPatcher");
            OnBootLuckyPatcher onBootLuckyPatcher = new OnBootLuckyPatcher();
            IntentFilter intentFilter3 = new IntentFilter();
            intentFilter3.addAction("android.intent.action.BOOT_COMPLETED");
            intentFilter3.addAction("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE");
            intentFilter3.addAction("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE");
            intentFilter3.addAction("android.intent.action.UMS_DISCONNECTED");
            intentFilter3.addAction("android.intent.action.POWER_DISCONNECTED");
            intentFilter3.addAction("android.intent.action.MY_PACKAGE_REPLACED");
            applicationContext.registerReceiver(onBootLuckyPatcher, intentFilter3);
            C0987.m6060((Object) "LuckyPatcher: OnAlarmReceiver");
            OnAlarmReceiver onAlarmReceiver = new OnAlarmReceiver();
            IntentFilter intentFilter4 = new IntentFilter();
            intentFilter4.addAction("com.lp.OnAlarmReceiver.ACTION_WIDGET_RECEIVER");
            applicationContext.registerReceiver(onAlarmReceiver, intentFilter4);
            C0987.m6060((Object) "LuckyPatcher: RootlessInstallerReceiver");
            applicationContext.registerReceiver(new C0820(), new IntentFilter("com.lp.action.INSTALLATION_STATUS_NOTIFICATION"));
            C0987.m6060((Object) ("LuckyPatcher:Registered receivers from " + context.getClass().getName()));
            f3808 = true;
        }
    }
}
