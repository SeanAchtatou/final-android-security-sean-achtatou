package com.lp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.chelpus.C0815;
import java.util.ArrayList;
import java.util.List;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ٴ  reason: contains not printable characters */
/* compiled from: MenuItemAdapter */
public class C0976 extends BaseExpandableListAdapter {

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0975[] f4294 = new C0975[0];

    /* renamed from: ʼ  reason: contains not printable characters */
    public Context f4295 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    public ArrayList<View> f4296 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f4297;

    public long getChildId(int i, int i2) {
        return 0;
    }

    public long getGroupId(int i) {
        return 0;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int i, int i2) {
        return true;
    }

    public C0976(Context context, int i, List<C0975> list) {
        this.f4295 = context;
        this.f4297 = i;
        this.f4294 = (C0975[]) list.toArray(new C0975[list.size()]);
        C0987.f4522 = this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5999(ArrayList<C0975> arrayList) {
        this.f4294 = (C0975[]) arrayList.toArray(new C0975[arrayList.size()]);
        notifyDataSetChanged();
    }

    public int getGroupCount() {
        C0975[] r0 = this.f4294;
        if (r0 == null || r0.length == 0) {
            return 0;
        }
        return r0.length;
    }

    public int getChildrenCount(int i) {
        C0975[] r0 = this.f4294;
        if (r0 == null || r0.length == 0) {
            return 0;
        }
        return r0[i].f4287.size();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0975 getGroup(int i) {
        return this.f4294[i];
    }

    public void onGroupCollapsed(int i) {
        super.onGroupCollapsed(i);
    }

    public void onGroupExpanded(int i) {
        super.onGroupExpanded(i);
    }

    public View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        View view2;
        View inflate;
        C0975 r11 = getGroup(i);
        int i2 = r11.f4285;
        try {
            view2 = this.f4296.get(i);
        } catch (Exception unused) {
            view2 = null;
        }
        if (view2 == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.f4295.getSystemService("layout_inflater");
            if (r11.f4289 != 3) {
                inflate = layoutInflater.inflate((int) R.layout.group_view, (ViewGroup) null);
            } else {
                inflate = layoutInflater.inflate((int) R.layout.group_check_view, (ViewGroup) null);
            }
            if (this.f4296 == null) {
                this.f4296 = new ArrayList<>(this.f4297);
                for (C0975 r6 : this.f4294) {
                    this.f4296.add(null);
                }
            }
        }
        view2.setClickable(false);
        int i3 = r11.f4289;
        if (i3 == 0) {
            TextView textView = (TextView) view2.findViewById(R.id.textGroup);
            textView.setTextAppearance(this.f4295, C0987.m6076());
            textView.setTextAppearance(this.f4295, C0987.m6076());
            textView.setText(C0815.m5205(i2));
            textView.setTypeface(textView.getTypeface(), 1);
            ImageView imageView = (ImageView) view2.findViewById(R.id.group_image);
            imageView.setImageDrawable(m6000(i2));
            imageView.setVisibility(0);
            int r12 = m5995(i2);
            textView.setTextColor(r12);
            imageView.setColorFilter(r12, PorterDuff.Mode.MULTIPLY);
        } else if (i3 == 1) {
            ((LinearLayout) view2.findViewById(R.id.parentViewGroup)).setPadding(10, 5, 50, 5);
            TextView textView2 = (TextView) view2.findViewById(R.id.textGroup);
            textView2.setTextAppearance(this.f4295, C0987.m6076());
            textView2.setTextAppearance(this.f4295, C0987.m6076());
            textView2.setTextColor(Color.parseColor("#000000"));
            textView2.setTypeface(textView2.getTypeface(), 1);
            ((ImageView) view2.findViewById(R.id.group_image)).setVisibility(8);
            view2.setBackgroundColor(Color.parseColor("#9F9F9F"));
            textView2.setText(C0815.m5205(i2));
        } else if (i3 == 2) {
            TextView textView3 = (TextView) view2.findViewById(R.id.textGroup);
            textView3.setTextAppearance(this.f4295, C0987.m6076());
            textView3.setTextAppearance(this.f4295, C0987.m6076());
            textView3.setText(C0815.m5205(i2));
            textView3.setTypeface(textView3.getTypeface(), 1);
            textView3.setTextColor(Color.parseColor("#feeb9c"));
            ImageView imageView2 = (ImageView) view2.findViewById(R.id.group_image);
            imageView2.setImageDrawable(m6000(i2));
            imageView2.setVisibility(0);
        } else if (i3 == 3) {
            TextView textView4 = (TextView) view2.findViewById(R.id.textGroup);
            textView4.setTextAppearance(this.f4295, C0987.m6076());
            textView4.setTextAppearance(this.f4295, C0987.m6076());
            textView4.setText(C0815.m5205(i2));
            textView4.setTypeface(textView4.getTypeface(), 1);
            textView4.setTextColor(Color.parseColor("#feeb9c"));
            ImageView imageView3 = (ImageView) view2.findViewById(R.id.group_image);
            CheckBox checkBox = (CheckBox) view2.findViewById(R.id.checkBoxPref);
            checkBox.setChecked(C0987.m6073().getBoolean(r11.f4292, r11.f4293));
            checkBox.setClickable(false);
            imageView3.setImageDrawable(m6000(i2));
            imageView3.setVisibility(0);
        }
        if (r11.f4286 != 0) {
            TextView textView5 = (TextView) view2.findViewById(R.id.textGroupDescr);
            textView5.setText(C0815.m5205(r11.f4286));
            textView5.setTextColor(-3355444);
        } else {
            ((TextView) view2.findViewById(R.id.textGroupDescr)).setVisibility(8);
        }
        this.f4296.set(i, view2);
        return view2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Integer getChild(int i, int i2) {
        C0975[] r0 = this.f4294;
        if (r0 == null || r0.length == 0) {
            return null;
        }
        return r0[i].f4287.get(i2);
    }

    public View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        C0975 r7 = getGroup(i);
        int intValue = getChild(i, i2).intValue();
        switch (intValue) {
            case 1:
                View inflate = ((LayoutInflater) this.f4295.getSystemService("layout_inflater")).inflate((int) R.layout.child_text_size_pref_view, (ViewGroup) null);
                RadioGroup radioGroup = (RadioGroup) inflate.findViewById(R.id.radioGroupView);
                int i3 = C0987.m6073().getInt("viewsize", 0);
                if (i3 == 0) {
                    ((RadioButton) radioGroup.findViewById(R.id.radioSmall)).setChecked(true);
                } else if (i3 == 1) {
                    ((RadioButton) radioGroup.findViewById(R.id.radioMedium)).setChecked(true);
                } else if (i3 == 2) {
                    ((RadioButton) radioGroup.findViewById(R.id.radioLarge)).setChecked(true);
                }
                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        switch (i) {
                            case R.id.radioLarge /*2131296530*/:
                                C0987.m6073().edit().putInt("viewsize", 2).commit();
                                C0987.m6073().edit().putBoolean("settings_change", true).commit();
                                C0987.f4430 = true;
                                return;
                            case R.id.radioMedium /*2131296531*/:
                                C0987.m6073().edit().putInt("viewsize", 1).commit();
                                C0987.m6073().edit().putBoolean("settings_change", true).commit();
                                C0987.f4430 = true;
                                return;
                            case R.id.radioSmall /*2131296540*/:
                                C0987.m6073().edit().putInt("viewsize", 0).commit();
                                C0987.m6073().edit().putBoolean("settings_change", true).commit();
                                C0987.f4430 = true;
                                return;
                            default:
                                return;
                        }
                    }
                });
                return inflate;
            case 2:
                View inflate2 = ((LayoutInflater) this.f4295.getSystemService("layout_inflater")).inflate((int) R.layout.child_orientation_pref_view, (ViewGroup) null);
                RadioGroup radioGroup2 = (RadioGroup) inflate2.findViewById(R.id.radioGroupOrient);
                int i4 = C0987.m6073().getInt("orientstion", 3);
                if (i4 == 1) {
                    ((RadioButton) radioGroup2.findViewById(R.id.radioLandscape)).setChecked(true);
                } else if (i4 == 2) {
                    ((RadioButton) radioGroup2.findViewById(R.id.radioPortret)).setChecked(true);
                } else if (i4 == 3) {
                    ((RadioButton) radioGroup2.findViewById(R.id.radioSensor)).setChecked(true);
                }
                radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        if (i == R.id.radioLandscape) {
                            C0987.m6073().edit().putInt("orientstion", 1).commit();
                            C0987.m6073().edit().putBoolean("settings_change", true).commit();
                            C0987.f4430 = true;
                        } else if (i == R.id.radioPortret) {
                            C0987.m6073().edit().putInt("orientstion", 2).commit();
                            C0987.m6073().edit().putBoolean("settings_change", true).commit();
                            C0987.f4430 = true;
                        } else if (i == R.id.radioSensor) {
                            C0987.m6073().edit().putInt("orientstion", 3).commit();
                            C0987.m6073().edit().putBoolean("settings_change", true).commit();
                            C0987.f4430 = true;
                        }
                    }
                });
                return inflate2;
            case 3:
                View inflate3 = ((LayoutInflater) this.f4295.getSystemService("layout_inflater")).inflate((int) R.layout.child_sort_pref_view, (ViewGroup) null);
                RadioGroup radioGroup3 = (RadioGroup) inflate3.findViewById(R.id.radioGroupSort);
                int i5 = C0987.m6073().getInt("sortby", 2);
                if (i5 == 1) {
                    ((RadioButton) radioGroup3.findViewById(R.id.radioName)).setChecked(true);
                } else if (i5 == 2) {
                    ((RadioButton) radioGroup3.findViewById(R.id.radioStatus)).setChecked(true);
                } else if (i5 == 3) {
                    ((RadioButton) radioGroup3.findViewById(R.id.radioTime)).setChecked(true);
                }
                radioGroup3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        if (i == R.id.radioName) {
                            C0987.m6073().edit().putInt("sortby", 1).commit();
                            C0987.m6073().edit().putBoolean("settings_change", true).commit();
                            C0987.f4430 = true;
                        } else if (i == R.id.radioStatus) {
                            C0987.m6073().edit().putInt("sortby", 2).commit();
                            C0987.m6073().edit().putBoolean("settings_change", true).commit();
                            C0987.f4430 = true;
                        } else if (i == R.id.radioTime) {
                            C0987.m6073().edit().putInt("sortby", 3).commit();
                            C0987.m6073().edit().putBoolean("settings_change", true).commit();
                            C0987.f4430 = true;
                        }
                    }
                });
                return inflate3;
            case 4:
                View inflate4 = ((LayoutInflater) this.f4295.getSystemService("layout_inflater")).inflate((int) R.layout.child_categoty_pref_view, (ViewGroup) null);
                CheckBox checkBox = (CheckBox) inflate4.findViewById(R.id.checkBoxLVL);
                checkBox.setChecked(C0987.m6073().getBoolean("lvlapp", true));
                checkBox.append(C0815.m5136("\n" + C0815.m5205((int) R.string.lvldescr), -7829368, BuildConfig.FLAVOR));
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                        C0987.m6073().edit().putBoolean("lvlapp", z).commit();
                        C0987.m6073().edit().putBoolean("settings_change", true).commit();
                        C0987.m6073().edit().putBoolean("lang_change", true).commit();
                        C0987.f4430 = true;
                    }
                });
                CheckBox checkBox2 = (CheckBox) inflate4.findViewById(R.id.checkBoxADS);
                checkBox2.setChecked(C0987.m6073().getBoolean("adsapp", true));
                checkBox2.append(C0815.m5136("\n" + C0815.m5205((int) R.string.adsdescr), -7829368, BuildConfig.FLAVOR));
                checkBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                        C0987.m6073().edit().putBoolean("adsapp", z).commit();
                        C0987.m6073().edit().putBoolean("settings_change", true).commit();
                        C0987.m6073().edit().putBoolean("lang_change", true).commit();
                        C0987.f4430 = true;
                    }
                });
                CheckBox checkBox3 = (CheckBox) inflate4.findViewById(R.id.checkBoxCUSTOM);
                checkBox3.setChecked(C0987.m6073().getBoolean("customapp", true));
                checkBox3.append(C0815.m5136("\n" + C0815.m5205((int) R.string.customdescr), -7829368, BuildConfig.FLAVOR));
                checkBox3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                        C0987.m6073().edit().putBoolean("customapp", z).commit();
                        C0987.m6073().edit().putBoolean("settings_change", true).commit();
                        C0987.m6073().edit().putBoolean("lang_change", true).commit();
                        C0987.f4430 = true;
                    }
                });
                CheckBox checkBox4 = (CheckBox) inflate4.findViewById(R.id.checkBoxModify);
                checkBox4.setChecked(C0987.m6073().getBoolean("modifapp", true));
                checkBox4.append(C0815.m5136("\n" + C0815.m5205((int) R.string.modifdescr), -7829368, BuildConfig.FLAVOR));
                checkBox4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                        C0987.m6073().edit().putBoolean("modifapp", z).commit();
                        C0987.m6073().edit().putBoolean("settings_change", true).commit();
                        C0987.m6073().edit().putBoolean("lang_change", true).commit();
                        C0987.f4430 = true;
                    }
                });
                CheckBox checkBox5 = (CheckBox) inflate4.findViewById(R.id.checkBoxFIX);
                checkBox5.setChecked(C0987.m6073().getBoolean("fixedapp", true));
                checkBox5.append(C0815.m5136("\n" + C0815.m5205((int) R.string.fixeddescr), -7829368, BuildConfig.FLAVOR));
                checkBox5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                        C0987.m6073().edit().putBoolean("fixedapp", z).commit();
                        C0987.m6073().edit().putBoolean("settings_change", true).commit();
                        C0987.m6073().edit().putBoolean("lang_change", true).commit();
                        C0987.f4430 = true;
                    }
                });
                CheckBox checkBox6 = (CheckBox) inflate4.findViewById(R.id.checkBoxNONE);
                checkBox6.setChecked(C0987.m6073().getBoolean("noneapp", true));
                checkBox6.append(C0815.m5136("\n" + C0815.m5205((int) R.string.nonedescr), -7829368, BuildConfig.FLAVOR));
                checkBox6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                        C0987.m6073().edit().putBoolean("noneapp", z).commit();
                        C0987.m6073().edit().putBoolean("settings_change", true).commit();
                        C0987.m6073().edit().putBoolean("lang_change", true).commit();
                        C0987.f4430 = true;
                    }
                });
                CheckBox checkBox7 = (CheckBox) inflate4.findViewById(R.id.checkBoxSYS);
                checkBox7.setChecked(C0987.m6073().getBoolean("systemapp", true));
                checkBox7.append(C0815.m5136("\n" + C0815.m5205((int) R.string.sysdescr), -7829368, BuildConfig.FLAVOR));
                checkBox7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                        C0987.m6073().edit().putBoolean("systemapp", z).commit();
                        C0987.m6073().edit().putBoolean("settings_change", true).commit();
                        C0987.m6073().edit().putBoolean("lang_change", true).commit();
                        C0987.f4430 = true;
                    }
                });
                return inflate4;
            case 5:
                View inflate5 = ((LayoutInflater) this.f4295.getSystemService("layout_inflater")).inflate((int) R.layout.child_root_pref_view, (ViewGroup) null);
                RadioGroup radioGroup4 = (RadioGroup) inflate5.findViewById(R.id.radioGroupRoot);
                int i6 = C0987.m6073().getInt("root_force", 0);
                if (i6 == 0) {
                    ((RadioButton) radioGroup4.findViewById(R.id.radioRootAuto)).setChecked(true);
                } else if (i6 == 1) {
                    ((RadioButton) radioGroup4.findViewById(R.id.radioRootOn)).setChecked(true);
                } else if (i6 == 2) {
                    ((RadioButton) radioGroup4.findViewById(R.id.radioRootOff)).setChecked(true);
                }
                radioGroup4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        switch (i) {
                            case R.id.radioRootAuto /*2131296536*/:
                                C0987.m6073().edit().putInt("root_force", 0).commit();
                                C0987.m6073().edit().putBoolean("settings_change", true).commit();
                                C0987.f4430 = true;
                                return;
                            case R.id.radioRootOff /*2131296537*/:
                                C0987.m6073().edit().putInt("root_force", 2).commit();
                                C0987.m6073().edit().putBoolean("settings_change", true).commit();
                                C0987.f4430 = true;
                                return;
                            case R.id.radioRootOn /*2131296538*/:
                                C0987.m6073().edit().putInt("root_force", 1).commit();
                                C0987.m6073().edit().putBoolean("settings_change", true).commit();
                                C0987.f4430 = true;
                                return;
                            default:
                                return;
                        }
                    }
                });
                return inflate5;
            case 6:
                View inflate6 = ((LayoutInflater) this.f4295.getSystemService("layout_inflater")).inflate((int) R.layout.child_name_sheme_pref_view, (ViewGroup) null);
                RadioGroup radioGroup5 = (RadioGroup) inflate6.findViewById(R.id.radioGroupNameSheme);
                int i7 = C0987.m6073().getInt("apkname", 1);
                if (i7 == 0) {
                    ((RadioButton) radioGroup5.findViewById(R.id.radioLabel)).setChecked(true);
                } else if (i7 == 1) {
                    ((RadioButton) radioGroup5.findViewById(R.id.radioPkg)).setChecked(true);
                }
                radioGroup5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        if (i == R.id.radioLabel) {
                            C0987.m6073().edit().putInt("apkname", 0).commit();
                            C0987.m6073().edit().putBoolean("settings_change", true).commit();
                            C0987.f4430 = true;
                        } else if (i == R.id.radioPkg) {
                            C0987.m6073().edit().putInt("apkname", 1).commit();
                            C0987.m6073().edit().putBoolean("settings_change", true).commit();
                            C0987.f4430 = true;
                        }
                    }
                });
                return inflate6;
            case 7:
                View inflate7 = ((LayoutInflater) this.f4295.getSystemService("layout_inflater")).inflate((int) R.layout.child_icon_change_pref_view, (ViewGroup) null);
                RadioGroup radioGroup6 = (RadioGroup) inflate7.findViewById(R.id.radioGroupIconChange);
                switch (C0987.m6073().getInt("default_icon_for_lp", 0)) {
                    case 0:
                        ((RadioButton) radioGroup6.findViewById(R.id.radioDefIcon)).setChecked(true);
                        break;
                    case 1:
                        ((RadioButton) radioGroup6.findViewById(R.id.radioIcon2)).setChecked(true);
                        break;
                    case 2:
                        ((RadioButton) radioGroup6.findViewById(R.id.radioIcon3)).setChecked(true);
                        break;
                    case 3:
                        ((RadioButton) radioGroup6.findViewById(R.id.radioIcon4)).setChecked(true);
                        break;
                    case 4:
                        ((RadioButton) radioGroup6.findViewById(R.id.radioIcon5)).setChecked(true);
                        break;
                    case 5:
                        ((RadioButton) radioGroup6.findViewById(R.id.radioIcon6)).setChecked(true);
                        break;
                    case 6:
                        ((RadioButton) radioGroup6.findViewById(R.id.radioIcon7)).setChecked(true);
                        break;
                    case 7:
                        ((RadioButton) radioGroup6.findViewById(R.id.radioIcon8)).setChecked(true);
                        break;
                }
                radioGroup6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        if (i != R.id.radioDefIcon) {
                            switch (i) {
                                case R.id.radioIcon2 /*2131296521*/:
                                    C0987.m6073().edit().putInt("default_icon_for_lp", 1).commit();
                                    C0987 r4 = C0987.f4432;
                                    C0987.m6061((Runnable) new Runnable() {
                                        public void run() {
                                            C0987 r0 = C0987.f4432;
                                            C0987.m6084((Integer) 11);
                                            C0987.f4468.m5918(false);
                                            C0987.f4468.m5917(C0815.m5205((int) R.string.wait));
                                        }
                                    });
                                    C0815.m5244(1);
                                    C0987.m6073().edit().putBoolean("settings_change", true).commit();
                                    C0987.f4430 = true;
                                    break;
                                case R.id.radioIcon3 /*2131296522*/:
                                    C0987.m6073().edit().putInt("default_icon_for_lp", 2).commit();
                                    C0987 r42 = C0987.f4432;
                                    C0987.m6061((Runnable) new Runnable() {
                                        public void run() {
                                            C0987 r0 = C0987.f4432;
                                            C0987.m6084((Integer) 11);
                                            C0987.f4468.m5918(false);
                                            C0987.f4468.m5917(C0815.m5205((int) R.string.wait));
                                        }
                                    });
                                    C0815.m5244(2);
                                    C0987.m6073().edit().putBoolean("settings_change", true).commit();
                                    C0987.f4430 = true;
                                    break;
                                case R.id.radioIcon4 /*2131296523*/:
                                    C0987.m6073().edit().putInt("default_icon_for_lp", 3).commit();
                                    C0987 r43 = C0987.f4432;
                                    C0987.m6061((Runnable) new Runnable() {
                                        public void run() {
                                            C0987 r0 = C0987.f4432;
                                            C0987.m6084((Integer) 11);
                                            C0987.f4468.m5918(false);
                                            C0987.f4468.m5917(C0815.m5205((int) R.string.wait));
                                        }
                                    });
                                    C0815.m5244(3);
                                    C0987.m6073().edit().putBoolean("settings_change", true).commit();
                                    C0987.f4430 = true;
                                    break;
                                case R.id.radioIcon5 /*2131296524*/:
                                    C0987.m6073().edit().putInt("default_icon_for_lp", 4).commit();
                                    C0987 r44 = C0987.f4432;
                                    C0987.m6061((Runnable) new Runnable() {
                                        public void run() {
                                            C0987 r0 = C0987.f4432;
                                            C0987.m6084((Integer) 11);
                                            C0987.f4468.m5918(false);
                                            C0987.f4468.m5917(C0815.m5205((int) R.string.wait));
                                        }
                                    });
                                    C0815.m5244(4);
                                    C0987.m6073().edit().putBoolean("settings_change", true).commit();
                                    C0987.f4430 = true;
                                    break;
                                case R.id.radioIcon6 /*2131296525*/:
                                    C0987.m6073().edit().putInt("default_icon_for_lp", 5).commit();
                                    C0987 r5 = C0987.f4432;
                                    C0987.m6061((Runnable) new Runnable() {
                                        public void run() {
                                            C0987 r0 = C0987.f4432;
                                            C0987.m6084((Integer) 11);
                                            C0987.f4468.m5918(false);
                                            C0987.f4468.m5917(C0815.m5205((int) R.string.wait));
                                        }
                                    });
                                    C0815.m5244(5);
                                    C0987.m6073().edit().putBoolean("settings_change", true).commit();
                                    C0987.f4430 = true;
                                    break;
                                case R.id.radioIcon7 /*2131296526*/:
                                    C0987.m6073().edit().putInt("default_icon_for_lp", 5).commit();
                                    C0987 r45 = C0987.f4432;
                                    C0987.m6061((Runnable) new Runnable() {
                                        public void run() {
                                            C0987 r0 = C0987.f4432;
                                            C0987.m6084((Integer) 11);
                                            C0987.f4468.m5918(false);
                                            C0987.f4468.m5917(C0815.m5205((int) R.string.wait));
                                        }
                                    });
                                    C0815.m5244(6);
                                    C0987.m6073().edit().putBoolean("settings_change", true).commit();
                                    C0987.f4430 = true;
                                    break;
                                case R.id.radioIcon8 /*2131296527*/:
                                    C0987.m6073().edit().putInt("default_icon_for_lp", 5).commit();
                                    C0987 r46 = C0987.f4432;
                                    C0987.m6061((Runnable) new Runnable() {
                                        public void run() {
                                            C0987 r0 = C0987.f4432;
                                            C0987.m6084((Integer) 11);
                                            C0987.f4468.m5918(false);
                                            C0987.f4468.m5917(C0815.m5205((int) R.string.wait));
                                        }
                                    });
                                    C0815.m5244(7);
                                    C0987.m6073().edit().putBoolean("settings_change", true).commit();
                                    C0987.f4430 = true;
                                    break;
                            }
                        } else {
                            C0987.m6073().edit().putInt("default_icon_for_lp", 0).commit();
                            C0987 r47 = C0987.f4432;
                            C0987.m6061((Runnable) new Runnable() {
                                public void run() {
                                    C0987.m6084((Integer) 11);
                                    C0987.f4468.m5918(false);
                                    C0987.f4468.m5917(C0815.m5205((int) R.string.wait));
                                }
                            });
                            C0815.m5244(0);
                            C0987.m6073().edit().putBoolean("settings_change", true).commit();
                            C0987.f4430 = true;
                        }
                        C0987 r48 = C0987.f4432;
                        C0987.m6061((Runnable) new Runnable() {
                            public void run() {
                                C0987.m6094((Integer) 11);
                            }
                        });
                    }
                });
                return inflate7;
            case 8:
                View inflate8 = ((LayoutInflater) this.f4295.getSystemService("layout_inflater")).inflate((int) R.layout.child_java_root_method_pref_view, (ViewGroup) null);
                RadioGroup radioGroup7 = (RadioGroup) inflate8.findViewById(R.id.radioGroupJavaRoot);
                if (C0987.m6073().getBoolean("use_app_process", false)) {
                    RadioButton radioButton = (RadioButton) radioGroup7.findViewById(R.id.radioAppProcess);
                    if (radioButton != null) {
                        radioButton.setChecked(true);
                    }
                } else {
                    RadioButton radioButton2 = (RadioButton) radioGroup7.findViewById(R.id.radioDalvikvm);
                    if (radioButton2 != null) {
                        radioButton2.setChecked(true);
                    }
                }
                radioGroup7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        switch (i) {
                            case R.id.radioAppProcess /*2131296510*/:
                                C0987.m6073().edit().putBoolean("use_app_process", true).commit();
                                C0987.m6073().edit().putBoolean("force_use_app_process", true).commit();
                                C0987.f4475 = C0815.m5250();
                                C0987.f4476 = C0815.m5257();
                                return;
                            case R.id.radioDalvikvm /*2131296511*/:
                                C0987.m6073().edit().putBoolean("use_app_process", false).commit();
                                C0987.m6073().edit().putBoolean("force_use_app_process", false).commit();
                                C0987.f4475 = C0815.m5250();
                                C0987.f4476 = C0815.m5257();
                                return;
                            default:
                                return;
                        }
                    }
                });
                return inflate8;
            default:
                View inflate9 = ((LayoutInflater) this.f4295.getSystemService("layout_inflater")).inflate((int) R.layout.child_view, (ViewGroup) null);
                TextView textView = (TextView) inflate9.findViewById(R.id.textChild);
                textView.setTextAppearance(this.f4295, C0987.m6076());
                textView.setTextAppearance(this.f4295, C0987.m6076());
                textView.setText(C0815.m5205(intValue));
                ImageView imageView = (ImageView) inflate9.findViewById(R.id.child_image);
                imageView.setImageDrawable(m6000(r7.f4285));
                int r5 = m5995(getGroup(i).f4285);
                imageView.setColorFilter(r5, PorterDuff.Mode.MULTIPLY);
                textView.setTextColor(r5);
                return inflate9;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5998() {
        this.f4294 = new C0975[0];
        this.f4296 = null;
        notifyDataSetChanged();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public Drawable m6000(int i) {
        switch (i) {
            case R.string.aboutmenu /*2131689510*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_run);
            case R.string.apk_create_option /*2131689522*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_selectable);
            case R.string.back /*2131689539*/:
                return C0987.m6069().getDrawable(R.drawable.menu_back);
            case R.string.billing_hack_menu_disable /*2131689555*/:
                Drawable drawable = C0987.m6069().getDrawable(R.drawable.f7505android);
                drawable.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_IN);
                return drawable;
            case R.string.billing_hack_menu_enable /*2131689556*/:
                Drawable drawable2 = C0987.m6069().getDrawable(R.drawable.f7505android);
                drawable2.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_IN);
                return drawable2;
            case R.string.bootview /*2131689575*/:
                Drawable drawable3 = C0987.m6069().getDrawable(R.drawable.auto);
                drawable3.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_IN);
                return drawable3;
            case R.string.change_icon_lp /*2131689593*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_selectable);
            case R.string.cleardalvik /*2131689598*/:
                return C0987.m6069().getDrawable(R.drawable.context_recycle);
            case R.string.context_batch_operations /*2131689612*/:
                return C0987.m6069().getDrawable(R.drawable.auto);
            case R.string.context_move_selected_apps_to_sdcard /*2131689637*/:
                return C0987.m6069().getDrawable(R.drawable.auto);
            case R.string.context_uninstall_selected_apps /*2131689655*/:
                return C0987.m6069().getDrawable(R.drawable.context_recycle);
            case R.string.corepatches /*2131689739*/:
                Drawable drawable4 = C0987.m6069().getDrawable(R.drawable.f7505android);
                drawable4.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_IN);
                return drawable4;
            case R.string.days_on_up /*2131689792*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_editable);
            case R.string.dir_change /*2131689800*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_editable);
            case R.string.dirbinder /*2131689802*/:
                return C0987.m6069().getDrawable(R.drawable.context_permissions);
            case R.string.disable_autoupdate /*2131689804*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_checkable);
            case R.string.fast_start /*2131689858*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_checkable);
            case R.string.filter /*2131689862*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_check_items);
            case R.string.help /*2131689901*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_run);
            case R.string.hide_notify /*2131689904*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_checkable);
            case R.string.hide_title_app /*2131689906*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_checkable);
            case R.string.langmenu /*2131689953*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_check_items);
            case R.string.licensing_hack_menu_disable /*2131689957*/:
                Drawable drawable5 = C0987.m6069().getDrawable(R.drawable.f7505android);
                drawable5.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_IN);
                return drawable5;
            case R.string.licensing_hack_menu_enable /*2131689958*/:
                Drawable drawable6 = C0987.m6069().getDrawable(R.drawable.f7505android);
                drawable6.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_IN);
                return drawable6;
            case R.string.market_install /*2131689967*/:
                return C0987.m6069().getDrawable(R.drawable.context_restore);
            case R.string.mod_market_check /*2131690002*/:
                return C0987.m6069().getDrawable(R.drawable.context_restore);
            case R.string.no_icon /*2131690026*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_checkable);
            case R.string.odex_all_system_app /*2131690048*/:
                return C0987.m6069().getDrawable(R.drawable.context_deodex);
            case R.string.option_SELinux_permissive /*2131690054*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_checkable);
            case R.string.option_analytic /*2131690056*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_checkable);
            case R.string.option_autoupdate_custom_patches /*2131690058*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_checkable);
            case R.string.option_detailed_logs /*2131690059*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_checkable);
            case R.string.option_remove_ads /*2131690061*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_checkable);
            case R.string.orientmenu /*2131690063*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_selectable);
            case R.string.proxyGP_install /*2131690121*/:
                return C0987.m6069().getDrawable(R.drawable.context_restore);
            case R.string.reboot /*2131690129*/:
                return C0987.m6069().getDrawable(R.drawable.context_patch_reboot);
            case R.string.reboot_to_bootloader /*2131690131*/:
                return C0987.m6069().getDrawable(R.drawable.context_patch_reboot);
            case R.string.reboot_to_recovery /*2131690132*/:
                return C0987.m6069().getDrawable(R.drawable.context_patch_reboot);
            case R.string.remove_all_saved_purchases /*2131690136*/:
                Drawable drawable7 = C0987.m6069().getDrawable(R.drawable.f7505android);
                drawable7.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_IN);
                return drawable7;
            case R.string.removefixes /*2131690138*/:
                return C0987.m6069().getDrawable(R.drawable.context_recycle);
            case R.string.sendlog /*2131690157*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_run);
            case R.string.set_default_to_install /*2131690158*/:
                Drawable drawable8 = C0987.m6069().getDrawable(R.drawable.auto);
                drawable8.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_IN);
                return drawable8;
            case R.string.setting_confirm_exit /*2131690163*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_checkable);
            case R.string.settings_force_java_root_method /*2131690166*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_selectable);
            case R.string.settings_force_root /*2131690170*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_selectable);
            case R.string.sortmenu /*2131690183*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_selectable);
            case R.string.text_size /*2131690242*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_selectable);
            case R.string.toolbar_adfree /*2131690256*/:
                return C0987.m6069().getDrawable(R.drawable.context_ads);
            case R.string.update_lp /*2131690276*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_run);
            case R.string.vibration /*2131690280*/:
                return C0987.m6069().getDrawable(R.mipmap.ic_group_checkable);
            case R.string.xposed_settings /*2131690316*/:
                Drawable drawable9 = C0987.m6069().getDrawable(R.drawable.f7505android);
                drawable9.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_IN);
                return drawable9;
            default:
                return null;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private int m5995(int i) {
        int parseColor = Color.parseColor("#DDDDDD");
        switch (i) {
            case R.string.bootview /*2131689575*/:
                return Color.parseColor("#99cccc");
            case R.string.cleardalvik /*2131689598*/:
                return Color.parseColor("#c2f055");
            case R.string.context_batch_operations /*2131689612*/:
                return Color.parseColor("#99cccc");
            case R.string.context_clearhosts /*2131689616*/:
                return Color.parseColor("#c2f055");
            case R.string.corepatches /*2131689739*/:
                return Color.parseColor("#cc99cc");
            case R.string.dirbinder /*2131689802*/:
                return Color.parseColor("#c2f055");
            case R.string.market_install /*2131689967*/:
                return Color.parseColor("#cc99cc");
            case R.string.mod_market_check /*2131690002*/:
                return Color.parseColor("#cc99cc");
            case R.string.odex_all_system_app /*2131690048*/:
                return Color.parseColor("#ffffbb");
            case R.string.proxyGP_install /*2131690121*/:
                return Color.parseColor("#cc99cc");
            case R.string.reboot /*2131690129*/:
                return Color.parseColor("#fe6c00");
            case R.string.reboot_to_bootloader /*2131690131*/:
                return Color.parseColor("#fe6c00");
            case R.string.reboot_to_recovery /*2131690132*/:
                return Color.parseColor("#fe6c00");
            case R.string.remove_all_saved_purchases /*2131690136*/:
                return Color.parseColor("#c2f055");
            case R.string.removefixes /*2131690138*/:
                return Color.parseColor("#c2f055");
            case R.string.root_needed /*2131690147*/:
                return Color.parseColor("#fe6c00");
            case R.string.set_default_to_install /*2131690158*/:
                return Color.parseColor("#ffffbb");
            case R.string.toolbar_adfree /*2131690256*/:
                return Color.parseColor("#c2f055");
            case R.string.xposed_settings /*2131690316*/:
                return Color.parseColor("#cc99cc");
            default:
                return parseColor;
        }
    }
}
