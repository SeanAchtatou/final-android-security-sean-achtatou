package com.lp.ʻ;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.ʻ.C0239;
import com.chelpus.C0815;
import com.lp.C0982;
import com.lp.C0987;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ʻ.י  reason: contains not printable characters */
/* compiled from: Progress_Dialog_2 */
public class C0957 {

    /* renamed from: ʾ  reason: contains not printable characters */
    public static C0982 f4151;

    /* renamed from: ʻ  reason: contains not printable characters */
    String f4152 = BuildConfig.FLAVOR;

    /* renamed from: ʼ  reason: contains not printable characters */
    String f4153 = BuildConfig.FLAVOR;

    /* renamed from: ʽ  reason: contains not printable characters */
    C0239 f4154 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    Dialog f4155 = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0957 m5916() {
        return new C0957();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m5919() {
        if (this.f4155 == null) {
            this.f4155 = m5920();
        }
        Dialog dialog = this.f4155;
        if (dialog != null) {
            dialog.show();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Dialog m5920() {
        f4151 = new C0982(C0987.f4432.m6233());
        if (this.f4152.equals(BuildConfig.FLAVOR)) {
            this.f4152 = C0815.m5205((int) R.string.download);
        }
        f4151.m6030(this.f4152);
        f4151.m6026(true);
        f4151.m6024(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                if (C0987.f4474) {
                    C0815.m5270();
                }
            }
        });
        return f4151.m6034();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5917(String str) {
        this.f4152 = str;
        if (f4151 == null) {
            m5920();
        }
        f4151.m6030(this.f4152);
        C0987.f4432.m6233().runOnUiThread(new Runnable() {
            public void run() {
                if (C0987.f4432 != null) {
                    C0987.f4432.m1253().m1405();
                }
            }
        });
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5918(boolean z) {
        Dialog dialog = this.f4155;
        if (dialog != null) {
            dialog.setCancelable(z);
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m5921() {
        Dialog dialog = this.f4155;
        return dialog != null && dialog.isShowing();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m5922() {
        Dialog dialog = this.f4155;
        if (dialog != null) {
            dialog.dismiss();
            this.f4155 = null;
        }
    }
}
