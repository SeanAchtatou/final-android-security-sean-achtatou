package com.lp.ʻ;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.support.v4.ʻ.C0227;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.chelpus.C0815;
import com.chelpus.ʻ.C0780;
import com.google.android.finsky.billing.iab.ʻ.ʻ.C0822;
import com.lp.C0959;
import com.lp.C0960;
import com.lp.C0964;
import com.lp.C0965;
import com.lp.C0966;
import com.lp.C0968;
import com.lp.C0973;
import com.lp.C0979;
import com.lp.C0980;
import com.lp.C0987;
import com.xposed.XposedUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import javaroot.utils.WriteSettingsXposed;
import net.lingala.zip4j.util.InternalZipConstants;
import org.json.JSONException;
import org.json.JSONObject;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.MainActivity;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ʻ.ʼ  reason: contains not printable characters */
/* compiled from: All_Dialogs */
public class C0944 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public int f3978 = 255;

    /* renamed from: ʼ  reason: contains not printable characters */
    public ArrayAdapter f3979 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    Dialog f3980 = null;

    /* renamed from: com.lp.ʻ.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: All_Dialogs */
    public class C0945 implements Comparator<C0980> {
        public C0945() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int compare(C0980 r3, C0980 r4) {
            if (r3 == null || r4 == null) {
                throw new ClassCastException();
            } else if (!r3.f4340 && !r4.f4340) {
                return r3.toString().compareToIgnoreCase(r4.toString());
            } else {
                int compareTo = Boolean.valueOf(r3.f4340).compareTo(Boolean.valueOf(r4.f4340));
                return compareTo == 0 ? r3.toString().compareToIgnoreCase(r4.toString()) : compareTo;
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5861() {
        if (this.f3980 == null) {
            this.f3980 = m5864();
        }
        Dialog dialog = this.f3980;
        if (dialog != null) {
            dialog.show();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m5862() {
        Dialog dialog = this.f3980;
        if (dialog != null) {
            dialog.dismiss();
            this.f3980 = null;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m5863() {
        Dialog dialog = this.f3980;
        if (dialog != null) {
            return dialog.isShowing();
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lp.ʼ.ʻ(com.lp.ʽ, android.widget.AdapterView$OnItemClickListener):com.lp.ʼ
     arg types: [com.lp.ʽ<com.lp.ˈ>, com.lp.ʻ.ʼ$20]
     candidates:
      com.lp.ʼ.ʻ(int, android.content.DialogInterface$OnClickListener):com.lp.ʼ
      com.lp.ʼ.ʻ(android.widget.ArrayAdapter, android.widget.AdapterView$OnItemClickListener):com.lp.ʼ
      com.lp.ʼ.ʻ(java.lang.String, android.content.DialogInterface$OnClickListener):com.lp.ʼ
      com.lp.ʼ.ʻ(com.lp.ʽ, android.widget.AdapterView$OnItemClickListener):com.lp.ʼ */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lp.ʼ.ʻ(com.lp.ʽ, android.widget.AdapterView$OnItemClickListener):com.lp.ʼ
     arg types: [com.lp.ʽ<com.lp.ᵎ>, com.lp.ʻ.ʼ$18]
     candidates:
      com.lp.ʼ.ʻ(int, android.content.DialogInterface$OnClickListener):com.lp.ʼ
      com.lp.ʼ.ʻ(android.widget.ArrayAdapter, android.widget.AdapterView$OnItemClickListener):com.lp.ʼ
      com.lp.ʼ.ʻ(java.lang.String, android.content.DialogInterface$OnClickListener):com.lp.ʼ
      com.lp.ʼ.ʻ(com.lp.ʽ, android.widget.AdapterView$OnItemClickListener):com.lp.ʼ */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lp.ʼ.ʻ(com.lp.ʽ, android.widget.AdapterView$OnItemClickListener):com.lp.ʼ
     arg types: [com.lp.ʽ<com.lp.ᵎ>, com.lp.ʻ.ʼ$1]
     candidates:
      com.lp.ʼ.ʻ(int, android.content.DialogInterface$OnClickListener):com.lp.ʼ
      com.lp.ʼ.ʻ(android.widget.ArrayAdapter, android.widget.AdapterView$OnItemClickListener):com.lp.ʼ
      com.lp.ʼ.ʻ(java.lang.String, android.content.DialogInterface$OnClickListener):com.lp.ʼ
      com.lp.ʼ.ʻ(com.lp.ʽ, android.widget.AdapterView$OnItemClickListener):com.lp.ʼ */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lp.ʼ.ʻ(com.lp.ʽ, android.widget.AdapterView$OnItemClickListener):com.lp.ʼ
     arg types: [com.lp.ʽ<com.lp.ᵎ>, com.lp.ʻ.ʼ$24]
     candidates:
      com.lp.ʼ.ʻ(int, android.content.DialogInterface$OnClickListener):com.lp.ʼ
      com.lp.ʼ.ʻ(android.widget.ArrayAdapter, android.widget.AdapterView$OnItemClickListener):com.lp.ʼ
      com.lp.ʼ.ʻ(java.lang.String, android.content.DialogInterface$OnClickListener):com.lp.ʼ
      com.lp.ʼ.ʻ(com.lp.ʽ, android.widget.AdapterView$OnItemClickListener):com.lp.ʼ */
    /* renamed from: ʾ  reason: contains not printable characters */
    public Dialog m5864() {
        int i;
        String str;
        int i2;
        String str2;
        String str3;
        int i3;
        String str4;
        C0987.m6060((Object) "Create dialog");
        this.f3978 = C0987.f4431;
        final C0980 r0 = C0987.f4440;
        if (C0987.f4432 == null || C0987.f4432.m6233() == null) {
            m5862();
        }
        try {
            C0987.m6060((Object) "All Dialog create.");
            int i4 = 1;
            switch (C0987.f4431) {
                case 3:
                    C0959 r4 = new C0959(C0987.f4432.m6233());
                    C0987.f4432.m6178();
                    C0227 r5 = C0987.f4432.m6233();
                    SharedPreferences r7 = C0987.m6073();
                    C0987 r10 = C0987.f4432;
                    C0987.f4456 = new C0964(r5, R.layout.bootlistitemview, r7.getInt("viewsize", 0), C0987.f4449);
                    C0987.f4456.f4244 = new C0945();
                    try {
                        C0987.f4439.notifyDataSetChanged();
                    } catch (Exception e) {
                        C0987.m6060((Object) ("LuckyPatcher(Bootlist dialog): " + e));
                    }
                    C0987.f4456.notifyDataSetChanged();
                    r4.m5943(C0815.m5205((int) R.string.delboot));
                    r4.m5941(C0987.f4456, new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                            C0980 r6 = (C0980) C0987.f4456.getItem(i);
                            if (r6.f4337.equals(C0815.m5205((int) R.string.android_patches_for_bootlist))) {
                                C0815.m5240();
                            } else {
                                try {
                                    C0987.m6068().getPackageInfo(r6.f4337, 0);
                                    for (int i2 = 0; i2 < C0987.f4439.f4361.length; i2++) {
                                        if (C0987.f4439.f4361[i2].f4337.equals(r6.f4337)) {
                                            C0987.f4439.f4361[i2].f4343 = false;
                                            C0987.f4439.f4361[i2].f4344 = false;
                                            if (C0987.f4439.f4361[i2].f4345) {
                                                if (new File(C0987.f4432.m6233().getDir("bootlist", 0) + InternalZipConstants.ZIP_FILE_SEPARATOR + C0987.f4439.f4361[i2].f4337).exists()) {
                                                    new File(C0987.f4432.m6233().getDir("bootlist", 0) + InternalZipConstants.ZIP_FILE_SEPARATOR + C0987.f4439.f4361[i2].f4337).delete();
                                                }
                                                C0987.f4439.f4361[i2].f4345 = false;
                                            }
                                            C0987.f4439.f4361[i2].f4346 = false;
                                            C0987.f4439.m6015(C0987.f4439.f4361[i2]);
                                        }
                                    }
                                } catch (PackageManager.NameNotFoundException unused) {
                                    if (new File(C0987.f4432.m6233().getDir("bootlist", 0) + InternalZipConstants.ZIP_FILE_SEPARATOR + r6.f4337).exists()) {
                                        new File(C0987.f4432.m6233().getDir("bootlist", 0) + InternalZipConstants.ZIP_FILE_SEPARATOR + r6.f4337).delete();
                                    }
                                }
                                new StringBuilder();
                                StringBuilder sb = new StringBuilder();
                                for (int i3 = 0; i3 < C0987.f4439.f4361.length; i3++) {
                                    if (C0987.f4439.f4361[i3].f4343 || C0987.f4439.f4361[i3].f4345 || C0987.f4439.f4361[i3].f4344 || C0987.f4439.f4361[i3].f4346) {
                                        sb.append(C0987.f4439.f4361[i3].f4337);
                                        if (C0987.f4439.f4361[i3].f4343) {
                                            sb.append("%ads");
                                        }
                                        if (C0987.f4439.f4361[i3].f4344) {
                                            sb.append("%lvl");
                                        }
                                        if (C0987.f4439.f4361[i3].f4345) {
                                            sb.append("%custom");
                                        }
                                        if (C0987.f4439.f4361[i3].f4346) {
                                            sb.append("%object");
                                        }
                                        sb.append(",");
                                    }
                                }
                                sb.append("\n");
                                try {
                                    FileOutputStream fileOutputStream = new FileOutputStream(new File(C0987.f4432.m6233().getDir("bootlist", 0) + "/bootlist"));
                                    fileOutputStream.write(sb.toString().getBytes());
                                    fileOutputStream.close();
                                } catch (FileNotFoundException | Exception unused2) {
                                }
                            }
                            C0987.f4439.notifyDataSetChanged();
                            C0987.m6060((Object) "asd1");
                            C0987 r4 = C0987.f4432;
                            C0987.m6084((Integer) 3);
                        }
                    });
                    C0987.m6060((Object) "asd2");
                    return r4.m5947();
                case 10:
                    C0987.f4541 = new ArrayList<>();
                    C0959 r42 = new C0959(C0987.f4432.m6233());
                    if (C0987.f4454 != null) {
                        C0987.f4454.setNotifyOnChange(true);
                        r42.m5955(true);
                        r42.m5942((C0960) C0987.f4454, (AdapterView.OnItemClickListener) new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long j) {
                                C0987 r1 = C0987.f4432;
                                C0987.m6085((Runnable) new Runnable() {
                                    public void run() {
                                        C0987.f4432.m6125(C0987.f4440, C0987.f4454.getItem(i));
                                        C0987.f4432.m6205();
                                        C0987 r0 = C0987.f4432;
                                        C0987.m6061((Runnable) new Runnable() {
                                            public void run() {
                                                C0987 r0 = C0987.f4432;
                                                C0987.m6094((Integer) 10);
                                                C0987 r1 = C0987.f4432;
                                                C0987.m6084((Integer) 10);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                        r42.m5944(C0815.m5205((int) R.string.perm_ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                C0987 r1 = C0987.f4432;
                                C0987.m6085((Runnable) new Runnable() {
                                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                     method: com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, boolean, boolean):boolean
                                     arg types: [java.lang.String, java.lang.String, int, int]
                                     candidates:
                                      com.chelpus.ˆ.ʻ(byte, byte, byte, byte):int
                                      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String[], boolean, java.lang.String[]):int
                                      com.chelpus.ˆ.ʻ(java.lang.String, byte[][], byte, java.lang.String):int
                                      com.chelpus.ˆ.ʻ(int, java.lang.String, java.lang.String, java.lang.String):void
                                      com.chelpus.ˆ.ʻ(android.widget.LinearLayout, java.lang.Runnable, java.util.Timer, java.util.TimerTask):void
                                      com.chelpus.ˆ.ʻ(java.io.File, java.util.ArrayList<java.io.File>, java.lang.String, java.lang.String):void
                                      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener, android.content.DialogInterface$OnCancelListener):void
                                      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, android.view.View$OnClickListener, android.view.View$OnClickListener):void
                                      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, java.lang.String, java.lang.String):void
                                      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, java.lang.String, boolean):void
                                      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, java.util.ArrayList<java.io.File>, java.lang.String):void
                                      com.chelpus.ˆ.ʻ(java.lang.String, java.util.ArrayList<com.chelpus.ʻ.ʼ.ˉ>, boolean, boolean):boolean
                                      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, boolean, boolean):boolean */
                                    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000b, code lost:
                                        if (com.chelpus.C0815.m5307(r2) != false) goto L_0x0024;
                                     */
                                    /* Code decompiled incorrectly, please refer to instructions dump. */
                                    public void run() {
                                        /*
                                            r5 = this;
                                            java.lang.String r0 = "/dbdata/system/packages.xml"
                                            java.lang.String r1 = "LuckyPatcher (Get packages.xml Error): "
                                            java.lang.String r2 = "/data/system/packages.xml"
                                            r3 = 0
                                            boolean r4 = com.chelpus.C0815.m5307(r2)     // Catch:{ Exception -> 0x0010 }
                                            if (r4 == 0) goto L_0x000e
                                            goto L_0x0024
                                        L_0x000e:
                                            r2 = r3
                                            goto L_0x0024
                                        L_0x0010:
                                            r2 = move-exception
                                            java.lang.StringBuilder r4 = new java.lang.StringBuilder
                                            r4.<init>()
                                            r4.append(r1)
                                            r4.append(r2)
                                            java.lang.String r2 = r4.toString()
                                            com.lp.C0987.m6060(r2)
                                            goto L_0x000e
                                        L_0x0024:
                                            boolean r1 = com.chelpus.C0815.m5307(r0)     // Catch:{ Exception -> 0x002c }
                                            if (r1 == 0) goto L_0x003f
                                            r2 = r0
                                            goto L_0x003f
                                        L_0x002c:
                                            r0 = move-exception
                                            java.lang.StringBuilder r3 = new java.lang.StringBuilder
                                            r3.<init>()
                                            r3.append(r1)
                                            r3.append(r0)
                                            java.lang.String r0 = r3.toString()
                                            com.lp.C0987.m6060(r0)
                                        L_0x003f:
                                            java.lang.String r0 = com.chelpus.C0815.m5317(r2)
                                            java.lang.String r1 = ""
                                            r0.equals(r1)     // Catch:{ Exception -> 0x0048 }
                                        L_0x0048:
                                            java.lang.StringBuilder r0 = new java.lang.StringBuilder
                                            r0.<init>()
                                            com.lp.ﾞﾞ r1 = com.lp.C0987.f4432
                                            android.support.v4.ʻ.ˊ r1 = r1.m6233()
                                            r3 = 0
                                            java.lang.String r4 = "packages"
                                            java.io.File r1 = r1.getDir(r4, r3)
                                            java.lang.String r1 = r1.getAbsolutePath()
                                            r0.append(r1)
                                            java.lang.String r1 = "/packages.xml"
                                            r0.append(r1)
                                            java.lang.String r0 = r0.toString()
                                            com.chelpus.C0815.m5194(r0, r2, r3, r3)
                                            java.lang.StringBuilder r0 = new java.lang.StringBuilder
                                            r0.<init>()
                                            java.lang.String r1 = "chmod 777 "
                                            r0.append(r1)
                                            r0.append(r2)
                                            java.lang.String r0 = r0.toString()
                                            com.chelpus.C0815.m5301(r0)
                                            java.lang.StringBuilder r0 = new java.lang.StringBuilder
                                            r0.<init>()
                                            java.lang.String r1 = "chown 1000.1000 "
                                            r0.append(r1)
                                            r0.append(r2)
                                            java.lang.String r0 = r0.toString()
                                            com.chelpus.C0815.m5301(r0)
                                            java.lang.StringBuilder r0 = new java.lang.StringBuilder
                                            r0.<init>()
                                            java.lang.String r1 = "chown 1000:1000 "
                                            r0.append(r1)
                                            r0.append(r2)
                                            java.lang.String r0 = r0.toString()
                                            com.chelpus.C0815.m5301(r0)
                                            com.lp.ᵢ r0 = com.lp.C0987.f4439
                                            com.lp.ʻ.ʼ$2 r1 = com.lp.ʻ.C0944.AnonymousClass2.this
                                            com.lp.ᵔ r1 = r0
                                            java.lang.String r1 = r1.f4337
                                            com.lp.ᵔ r0 = r0.m6013(r1)
                                            r1 = 1
                                            r0.f4351 = r1
                                            android.content.SharedPreferences r0 = com.lp.C0987.m6073()
                                            android.content.SharedPreferences$Editor r0 = r0.edit()
                                            com.lp.ᵢ r2 = com.lp.C0987.f4439
                                            com.lp.ʻ.ʼ$2 r3 = com.lp.ʻ.C0944.AnonymousClass2.this
                                            com.lp.ᵔ r3 = r0
                                            java.lang.String r3 = r3.f4337
                                            com.lp.ᵔ r2 = r2.m6013(r3)
                                            java.lang.String r2 = r2.f4337
                                            android.content.SharedPreferences$Editor r0 = r0.putBoolean(r2, r1)
                                            r0.commit()
                                            com.chelpus.C0815.m5330()
                                            java.lang.String r0 = "killall -9 zygote"
                                            com.chelpus.C0815.m5301(r0)
                                            return
                                        */
                                        throw new UnsupportedOperationException("Method not decompiled: com.lp.ʻ.C0944.AnonymousClass2.AnonymousClass1.run():void");
                                    }
                                });
                            }
                        });
                    }
                    return r42.m5947();
                case 21:
                    C0959 r02 = new C0959(C0987.f4432.m6233());
                    if (C0987.f4454 != null) {
                        C0987.f4454.setNotifyOnChange(true);
                        r02.m5955(true);
                        r02.m5942((C0960) C0987.f4454, (AdapterView.OnItemClickListener) new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                                C0979 item = C0987.f4454.getItem(i);
                                if (item.f4336) {
                                    item.f4336 = false;
                                } else {
                                    item.f4336 = true;
                                }
                                C0987.f4454.notifyDataSetChanged();
                            }
                        });
                        r02.m5944(C0815.m5205((int) R.string.create_perm_ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ArrayList<String> arrayList = new ArrayList<>();
                                ArrayList<String> arrayList2 = new ArrayList<>();
                                for (int i2 = 0; i2 < C0987.f4454.getCount(); i2++) {
                                    C0979 item = C0987.f4454.getItem(i2);
                                    if (!item.f4336 && !item.f4335.contains("chelpus_")) {
                                        arrayList.add(item.f4335);
                                    }
                                    if (item.f4336 && item.f4335.startsWith("chelpa_per_")) {
                                        arrayList.add(item.f4335.replace("chelpa_per_", BuildConfig.FLAVOR));
                                        C0987.m6060((Object) item.f4335.replace("chelpa_per_", BuildConfig.FLAVOR));
                                    }
                                    if (!item.f4336 && item.f4335.contains("chelpus_") && !item.f4335.contains("disabled_")) {
                                        arrayList2.add(item.f4335.replace("chelpus_", BuildConfig.FLAVOR));
                                        C0987.m6060((Object) (BuildConfig.FLAVOR + item.f4335.replace("chelpus_", BuildConfig.FLAVOR)));
                                    }
                                    if (item.f4336 && item.f4335.contains("chelpus_disabled_")) {
                                        arrayList2.add(item.f4335.replace("chelpus_", BuildConfig.FLAVOR));
                                        C0987.m6060((Object) (BuildConfig.FLAVOR + item.f4335.replace("chelpus_", BuildConfig.FLAVOR)));
                                    }
                                }
                                C0780 r0 = new C0780();
                                r0.f3148 = arrayList;
                                r0.f3158 = arrayList2;
                                r0.f3131 = true;
                                r0.f3142 = true;
                                if (!C0987.f4473.equals(BuildConfig.FLAVOR)) {
                                    r0.f3141 = new File(C0987.f4473);
                                }
                                C0987 r7 = C0987.f4432;
                                C0987.m6055(C0987.f4440, r0);
                            }
                        });
                    }
                    return r02.m5947();
                case 24:
                    LinearLayout linearLayout = (LinearLayout) View.inflate(C0987.f4432.m6233(), R.layout.corepatch_layout, null);
                    ListView listView = (ListView) linearLayout.findViewById(R.id.coreListView);
                    final CheckBox checkBox = (CheckBox) linearLayout.findViewById(R.id.checkBoxDalvik);
                    checkBox.setChecked(true);
                    checkBox.setText(C0815.m5205((int) R.string.core_option));
                    checkBox.setMaxLines(1);
                    if (C0815.m5318("/system/framework/core.jar") != null && !C0815.m5282().equals("ART")) {
                        checkBox.setChecked(true);
                        C0987.f4508 = true;
                    } else if (C0987.f4489 < 20 || !C0815.m5282().equals("ART") || (!C0815.m5322() && !C0815.m5327())) {
                        C0987.f4508 = false;
                        checkBox.setChecked(false);
                        checkBox.setEnabled(false);
                    } else {
                        checkBox.setChecked(true);
                        C0987.f4508 = true;
                    }
                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                            if (!C0815.m5282().equals("ART")) {
                                C0987.f4508 = z;
                            } else if (C0987.f4489 < 20 || !C0815.m5282().equals("ART") || (!C0815.m5322() && !C0815.m5327())) {
                                checkBox.setChecked(false);
                                checkBox.setEnabled(false);
                                C0987.f4508 = false;
                            } else {
                                C0987.f4508 = z;
                            }
                        }
                    });
                    C0959 r6 = new C0959(C0987.f4432.m6233());
                    if (this.f3979 != null) {
                        this.f3979.setNotifyOnChange(true);
                        listView.setAdapter((ListAdapter) this.f3979);
                        listView.setDivider(new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[]{-1715492012, -4215980, -1715492012}));
                        listView.setDividerHeight(1);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                                int i2 = i;
                                C0966 r2 = (C0966) C0944.this.f3979.getItem(i2);
                                C0966 r3 = (C0966) C0944.this.f3979.getItem(2);
                                C0966 r5 = (C0966) C0944.this.f3979.getItem(3);
                                C0966 r7 = (C0966) C0944.this.f3979.getItem(4);
                                if (r2.f4259) {
                                    r2.f4259 = false;
                                } else if (!r5.f4259 && !r7.f4259 && !r2.f4260) {
                                    r2.f4259 = true;
                                }
                                if ((i2 == 0 || i2 == 1) && r2.f4259 && ((r3.f4259 || r5.f4259) && ((!C0815.m5307("/system/framework/core.odex") && !C0815.m5307("/system/framework/core-libart.odex")) || C0815.m5282().contains("ART")))) {
                                    r3.f4259 = false;
                                    C0987 r32 = C0987.f4432;
                                    C0987.m6062(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.patch_to_Android_At_once));
                                }
                                if (i2 == 2 && r2.f4259 && (((!C0815.m5307("/system/framework/core.odex") && !C0815.m5307("/system/framework/core-libart.odex")) || C0815.m5282().contains("ART")) && (((C0966) C0944.this.f3979.getItem(0)).f4259 || ((C0966) C0944.this.f3979.getItem(1)).f4259))) {
                                    ((C0966) C0944.this.f3979.getItem(0)).f4259 = false;
                                    ((C0966) C0944.this.f3979.getItem(1)).f4259 = false;
                                    C0987 r33 = C0987.f4432;
                                    C0987.m6062(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.patch_to_Android_At_once));
                                }
                                if (!r5.f4259 && !r7.f4259 && C0987.f4489 >= 20 && C0987.f4508 && C0815.m5282().equals("ART")) {
                                    if (i2 == 2 && !C0815.m5327()) {
                                        C0987 r34 = C0987.f4432;
                                        C0987.m6062(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.patch_to_Android_only_dalvik_cahce_services_disable));
                                        ((C0966) C0944.this.f3979.getItem(0)).f4259 = false;
                                        ((C0966) C0944.this.f3979.getItem(1)).f4259 = false;
                                        C0987.f4508 = false;
                                        checkBox.setChecked(false);
                                    }
                                    if (i2 == 2 && !C0815.m5322()) {
                                        C0987 r35 = C0987.f4432;
                                        C0987.m6062(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.patch_to_Android_only_dalvik_cahce_core_disable));
                                        ((C0966) C0944.this.f3979.getItem(2)).f4259 = false;
                                        C0987.f4508 = false;
                                        checkBox.setChecked(false);
                                    }
                                }
                                if (i2 == 3 && r2.f4259) {
                                    ((C0966) C0944.this.f3979.getItem(0)).f4259 = false;
                                    ((C0966) C0944.this.f3979.getItem(1)).f4259 = false;
                                    ((C0966) C0944.this.f3979.getItem(2)).f4259 = false;
                                }
                                if (i2 == 4 && r2.f4259) {
                                    if (i2 == 4 && !C0815.m5327()) {
                                        C0987 r1 = C0987.f4432;
                                        C0987.m6062(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.patch_to_Android_only_dalvik_cahce_services_disable));
                                        C0987.f4508 = false;
                                        checkBox.setChecked(false);
                                    }
                                    ((C0966) C0944.this.f3979.getItem(0)).f4259 = false;
                                    ((C0966) C0944.this.f3979.getItem(1)).f4259 = false;
                                    ((C0966) C0944.this.f3979.getItem(2)).f4259 = false;
                                }
                                C0944.this.f3979.notifyDataSetChanged();
                            }
                        });
                        r6.m5940(linearLayout);
                        r6.m5944(C0815.m5205((int) R.string.patch_ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                int count = C0944.this.f3979.getCount();
                                String str = "patch";
                                boolean z = false;
                                boolean z2 = false;
                                boolean z3 = false;
                                boolean z4 = false;
                                for (int i2 = 0; i2 < count; i2++) {
                                    C0966 r5 = (C0966) C0944.this.f3979.getItem(i2);
                                    if (i2 == 0 && r5.f4259) {
                                        str = str + "_patch1";
                                        z = true;
                                    }
                                    if (i2 == 1 && r5.f4259) {
                                        str = str + "_patch2";
                                        z2 = true;
                                    }
                                    if (i2 == 2 && r5.f4259) {
                                        str = str + "_patch3";
                                        z3 = true;
                                    }
                                    if (i2 == 3 && r5.f4259) {
                                        str = "restoreCore";
                                        z4 = true;
                                    }
                                    if (i2 == 4 && r5.f4259) {
                                        if (str.contains("restoreCore")) {
                                            str = str + "_restoreServices";
                                        } else {
                                            str = "restoreServices";
                                        }
                                        z4 = true;
                                    }
                                }
                                if (z || z2 || z3 || z4) {
                                    C0987.f4432.m6181(str);
                                }
                            }
                        });
                    }
                    return r6.m5947();
                case 26:
                    C0959 r03 = new C0959(C0987.f4432.m6233());
                    if (C0987.f4452 != null) {
                        C0987.f4452.setNotifyOnChange(true);
                        r03.m5941(C0987.f4452, new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                                C0987 r1 = C0987.f4432;
                                C0987.m6094((Integer) 26);
                                C0987.f4432.m6126(C0987.f4440, (File) C0987.f4452.getItem(i));
                            }
                        });
                    }
                    return r03.m5947();
                case 28:
                    if (C0987.f4452 != null) {
                        C0987.f4452.setNotifyOnChange(true);
                    }
                    C0959 r04 = new C0959(C0987.f4432.m6233(), true);
                    r04.m5941(C0987.f4452, new AdapterView.OnItemClickListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.lp.ﾞﾞ.ʼ(com.lp.ˋ, boolean):void
                         arg types: [com.lp.ˋ, int]
                         candidates:
                          com.lp.ﾞﾞ.ʼ(java.io.File, boolean):void
                          com.lp.ﾞﾞ.ʼ(java.lang.String, java.lang.String):void
                          com.lp.ﾞﾞ.ʼ(java.lang.String, boolean):boolean
                          android.support.v4.ʻ.ˉ.ʼ(android.view.Menu, android.view.MenuInflater):boolean
                          com.lp.ﾞﾞ.ʼ(com.lp.ˋ, boolean):void */
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                            C0987 r1 = C0987.f4432;
                            C0987.m6094((Integer) 28);
                            C0987 r2 = C0987.f4432;
                            C0987.m6081((C0968) C0987.f4452.getItem(i), false);
                        }
                    });
                    r04.m5952(true);
                    r04.m5938(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialogInterface) {
                        }
                    });
                    return r04.m5947();
                case 29:
                    C0959 r43 = new C0959(C0987.f4432.m6233());
                    if (C0987.f4454 != null) {
                        C0987.f4454.setNotifyOnChange(true);
                        r43.m5955(true);
                        r43.m5942((C0960) C0987.f4454, (AdapterView.OnItemClickListener) new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                                C0979 item = C0987.f4454.getItem(i);
                                if (!item.f4336) {
                                    C0815 r6 = new C0815(BuildConfig.FLAVOR);
                                    r6.m5353("pm enable '" + r0.f4337 + InternalZipConstants.ZIP_FILE_SEPARATOR + item.f4335 + "'");
                                } else {
                                    C0815 r62 = new C0815(BuildConfig.FLAVOR);
                                    r62.m5353("pm disable '" + r0.f4337 + InternalZipConstants.ZIP_FILE_SEPARATOR + item.f4335 + "'");
                                }
                                if (C0987.m6068().getComponentEnabledSetting(new ComponentName(r0.f4337, item.f4335)) == 2) {
                                    item.f4336 = false;
                                } else {
                                    item.f4336 = true;
                                }
                                C0987.f4454.notifyDataSetChanged();
                                r0.f4351 = true;
                                C0987.f4439.m6015(C0987.f4440);
                                C0987.m6073().edit().putBoolean(r0.f4337, true).commit();
                            }
                        });
                        r43.m5944(C0815.m5205((int) R.string.launchbutton), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                try {
                                    Intent launchIntentForPackage = C0987.m6068().getLaunchIntentForPackage(r0.f4337);
                                    if (C0987.f4474) {
                                        new Thread(new Runnable() {
                                            public void run() {
                                                try {
                                                    C0815.m5310(C0987.m6072().getPackageManager().getPackageInfo(r0.f4337, 0).applicationInfo.processName);
                                                } catch (PackageManager.NameNotFoundException e) {
                                                    e.printStackTrace();
                                                }
                                                C0815.m5315(r0.f4337);
                                            }
                                        }).start();
                                    } else if (C0987.f4447 != null) {
                                        C0987.f4447.startActivity(launchIntentForPackage);
                                    }
                                } catch (Exception unused) {
                                    Toast.makeText(C0987.f4432.m6233(), C0815.m5205((int) R.string.error_launch), 1).show();
                                }
                                C0987.m6094((Integer) 29);
                            }
                        });
                    }
                    return r43.m5947();
                case 31:
                    final ArrayList arrayList = new ArrayList();
                    C0959 r52 = new C0959(C0987.f4432.m6233());
                    if (C0987.f4455 != null) {
                        C0987.f4455.setNotifyOnChange(true);
                        r52.m5955(true);
                        r52.m5942((C0960) C0987.f4455, (AdapterView.OnItemClickListener) new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                                C0965 item = C0987.f4455.getItem(i);
                                if (!item.f4257) {
                                    if (item.f4251) {
                                        item.f4251 = false;
                                    } else {
                                        item.f4251 = true;
                                    }
                                }
                                arrayList.add(item);
                            }
                        });
                        r52.f4175 = true;
                        r52.m5954(C0815.m5205((int) R.string.usepatch), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                try {
                                    C0987.m6085((Runnable) new Runnable() {
                                        public void run() {
                                            if (arrayList != null && arrayList.size() > 0) {
                                                Iterator it = arrayList.iterator();
                                                while (it.hasNext()) {
                                                    C0965 r1 = (C0965) it.next();
                                                    if (!r1.f4257 && r1.f4251) {
                                                        C0815 r2 = new C0815(BuildConfig.FLAVOR);
                                                        r2.m5353("pm enable '" + r0.f4337 + InternalZipConstants.ZIP_FILE_SEPARATOR + r1.f4250 + "'");
                                                    } else if (!r1.f4257) {
                                                        C0815 r22 = new C0815(BuildConfig.FLAVOR);
                                                        r22.m5353("pm disable '" + r0.f4337 + InternalZipConstants.ZIP_FILE_SEPARATOR + r1.f4250 + "'");
                                                    }
                                                }
                                            }
                                            C0987.m6061((Runnable) new Runnable() {
                                                public void run() {
                                                    C0987.f4455.notifyDataSetChanged();
                                                    r0.f4351 = true;
                                                    C0987.f4439.m6015(C0987.f4440);
                                                    C0987.m6073().edit().putBoolean(r0.f4337, true).commit();
                                                }
                                            });
                                        }
                                    });
                                } catch (Throwable th) {
                                    th.printStackTrace();
                                }
                            }
                        });
                        r52.m5951(C0815.m5205((int) R.string.close), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                try {
                                    dialogInterface.dismiss();
                                } catch (Throwable th) {
                                    th.printStackTrace();
                                }
                            }
                        });
                        r52.m5944(C0815.m5205((int) R.string.launchbutton), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                try {
                                    Intent launchIntentForPackage = C0987.m6068().getLaunchIntentForPackage(r0.f4337);
                                    if (C0987.f4474) {
                                        new Thread(new Runnable() {
                                            public void run() {
                                                try {
                                                    C0815.m5310(C0987.m6072().getPackageManager().getPackageInfo(r0.f4337, 0).applicationInfo.processName);
                                                } catch (PackageManager.NameNotFoundException e) {
                                                    e.printStackTrace();
                                                }
                                                C0815.m5315(r0.f4337);
                                            }
                                        }).start();
                                    } else if (C0987.f4447 != null) {
                                        C0987.f4447.startActivity(launchIntentForPackage);
                                    }
                                } catch (Exception unused) {
                                    Toast.makeText(C0987.f4432.m6233(), C0815.m5205((int) R.string.error_launch), 1).show();
                                }
                                try {
                                    dialogInterface.dismiss();
                                } catch (Throwable th) {
                                    th.printStackTrace();
                                }
                                C0987 r3 = C0987.f4432;
                                C0987.m6094((Integer) 31);
                            }
                        });
                    }
                    return r52.m5947();
                case 37:
                    LinearLayout linearLayout2 = (LinearLayout) View.inflate(C0987.f4432.m6233(), R.layout.core_patch_result, null);
                    TextView textView = (TextView) linearLayout2.findViewById(R.id.textResults);
                    final CheckBox checkBox2 = (CheckBox) linearLayout2.findViewById(R.id.CheckBox1);
                    textView.setText(C0815.m5205((int) R.string.core_only_dalvik_cache_finished));
                    if (C0987.f4540.contains("_patch1")) {
                        if (!C0987.f4435.contains("Core patched!")) {
                            if (!C0987.f4435.contains("Core 2 patched!")) {
                                str3 = C0815.m5205((int) R.string.core_patch_nopatch);
                                i3 = -65536;
                                i4 = 0;
                                textView.append(C0815.m5205((int) R.string.contextcorepatch1) + " " + i4 + "/2 ");
                                StringBuilder sb = new StringBuilder();
                                sb.append(str3);
                                sb.append("\n");
                                textView.append(C0815.m5136(sb.toString(), i3, BuildConfig.FLAVOR));
                            }
                        }
                        str3 = C0815.m5205((int) R.string.core_patch_apply);
                        if (!C0987.f4435.contains("Core patched!")) {
                            i4 = 0;
                        }
                        if (C0987.f4435.contains("Core 2 patched!")) {
                            i4++;
                        }
                        i3 = -16711936;
                        textView.append(C0815.m5205((int) R.string.contextcorepatch1) + " " + i4 + "/2 ");
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(str3);
                        sb2.append("\n");
                        textView.append(C0815.m5136(sb2.toString(), i3, BuildConfig.FLAVOR));
                    }
                    if (C0987.f4540.contains("_patch2")) {
                        if (C0987.f4435.contains("Core unsigned install patched!")) {
                            str2 = C0815.m5205((int) R.string.core_patch_apply);
                            i2 = -16711936;
                        } else {
                            str2 = C0815.m5205((int) R.string.core_patch_nopatch);
                            i2 = -65536;
                        }
                        textView.append(C0815.m5205((int) R.string.contextcorepatch2) + " ");
                        textView.append(C0815.m5136(str2 + "\n", i2, BuildConfig.FLAVOR));
                    }
                    if (C0987.f4540.contains("_patch3")) {
                        if (C0987.f4435.contains("Core4 patched!")) {
                            str = C0815.m5205((int) R.string.core_patch_apply);
                            i = -16711936;
                        } else {
                            str = C0815.m5205((int) R.string.core_patch_apply);
                            i = -65536;
                        }
                        textView.append(C0815.m5205((int) R.string.contextcorepatch3) + " ");
                        textView.append(C0815.m5136(str + "\n", i, BuildConfig.FLAVOR));
                    }
                    if (C0987.f4540.contains("restore")) {
                        checkBox2.setChecked(false);
                        checkBox2.setEnabled(false);
                        C0815.m5240();
                        if (C0987.f4435.contains("Core restored!") || C0987.f4435.contains("Core 2 restored!")) {
                            String r44 = C0815.m5205((int) R.string.restored);
                            textView.append(C0815.m5205((int) R.string.contextcorepatch1) + " ");
                            textView.append(C0815.m5136(r44 + "\n", -16711936, BuildConfig.FLAVOR));
                        }
                        if (C0987.f4435.contains("Core unsigned install restored!")) {
                            String r45 = C0815.m5205((int) R.string.restored);
                            textView.append(C0815.m5205((int) R.string.contextcorepatch2) + " ");
                            textView.append(C0815.m5136(r45 + "\n", -16711936, BuildConfig.FLAVOR));
                        }
                        if (C0987.f4435.contains("Core4 restored!")) {
                            String r46 = C0815.m5205((int) R.string.restored);
                            textView.append(C0815.m5205((int) R.string.contextcorepatch3) + " ");
                            textView.append(C0815.m5136(r46 + "\n", -16711936, BuildConfig.FLAVOR));
                        }
                    }
                    textView.append("\n" + C0815.m5205((int) R.string.core_only_dalvik_cache_end));
                    checkBox2.setChecked(false);
                    checkBox2.setText(C0815.m5205((int) R.string.core_result_checkbox));
                    if (checkBox2.isChecked()) {
                        C0815.m5303(C0987.f4540);
                    } else {
                        C0815.m5255(C0987.f4540);
                    }
                    checkBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                            if (z) {
                                C0815.m5303(C0987.f4540);
                            } else {
                                C0815.m5255(C0987.f4540);
                            }
                        }
                    });
                    C0959 r47 = new C0959(C0987.f4432.m6233());
                    r47.m5940(linearLayout2);
                    r47.m5944(C0815.m5205((int) R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (checkBox2.isChecked()) {
                                C0815.m5303(C0987.f4540);
                            } else {
                                C0815.m5255(C0987.f4540);
                            }
                        }
                    }).m5948((int) R.string.information);
                    return r47.m5947();
                case 39:
                    LinearLayout linearLayout3 = (LinearLayout) View.inflate(C0987.f4432.m6233(), R.layout.corepatch_layout, null);
                    final CheckBox checkBox3 = (CheckBox) linearLayout3.findViewById(R.id.checkBoxDalvik);
                    JSONObject r62 = C0815.m5314();
                    if (r62 == null) {
                        new Thread(new Runnable() {
                            public void run() {
                                String[] r0 = C0815.m5222();
                                for (String str : r0) {
                                    try {
                                        C0815.m5246(str + "/xposed");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }).start();
                    }
                    if (r62 != null) {
                        checkBox3.setChecked(r62.getBoolean("module_on"));
                    } else {
                        checkBox3.setChecked(true);
                    }
                    if (XposedUtils.isXposedEnabled()) {
                        checkBox3.setEnabled(true);
                        checkBox3.setText(C0815.m5205((int) R.string.xposed_module_option));
                    } else {
                        checkBox3.setEnabled(false);
                        checkBox3.setChecked(false);
                        checkBox3.setText(C0815.m5205((int) R.string.xposed_option_off));
                    }
                    checkBox3.setMaxLines(2);
                    C0959 r63 = new C0959(C0987.f4432.m6233());
                    if (this.f3979 != null) {
                        this.f3979.setNotifyOnChange(true);
                        ListView listView2 = (ListView) linearLayout3.findViewById(R.id.coreListView);
                        listView2.setAdapter((ListAdapter) this.f3979);
                        listView2.setDivider(new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[]{-1715492012, -4215980, -1715492012}));
                        listView2.setDividerHeight(1);
                        if (XposedUtils.isXposedEnabled()) {
                            listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                                    C0966 r1 = (C0966) C0944.this.f3979.getItem(i);
                                    C0987.m6060((Object) (BuildConfig.FLAVOR + r1.f4259));
                                    if (r1.f4259) {
                                        r1.f4259 = false;
                                    } else {
                                        r1.f4259 = true;
                                    }
                                    C0987.m6060((Object) (BuildConfig.FLAVOR + ((C0966) C0944.this.f3979.getItem(i)).f4259));
                                    C0944.this.f3979.notifyDataSetChanged();
                                }
                            });
                        }
                        r63.m5940(linearLayout3);
                        if (!XposedUtils.isXposedEnabled()) {
                            r63.m5944(C0815.m5205((int) R.string.ok), (DialogInterface.OnClickListener) null);
                        } else {
                            r63.m5944(C0815.m5205((int) R.string.usepatch), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    new Thread(new Runnable() {
                                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                         method: com.chelpus.ˆ.ʼ(java.lang.String, boolean):java.lang.String
                                         arg types: [java.lang.String, int]
                                         candidates:
                                          com.chelpus.ˆ.ʼ(int, int):int
                                          com.chelpus.ˆ.ʼ(java.io.File, java.lang.String):java.lang.String
                                          com.chelpus.ˆ.ʼ(java.io.File, java.util.ArrayList<java.io.File>):void
                                          com.chelpus.ˆ.ʼ(java.lang.String, java.lang.String):void
                                          com.chelpus.ˆ.ʼ(boolean, boolean):void
                                          com.chelpus.ˆ.ʼ(java.io.File, java.io.File):boolean
                                          com.chelpus.ˆ.ʼ(java.io.File, int):byte[]
                                          com.chelpus.ˆ.ʼ(java.lang.String, boolean):java.lang.String */
                                        public void run() {
                                            long j;
                                            int count = C0944.this.f3979.getCount();
                                            final JSONObject jSONObject = new JSONObject();
                                            for (int i = 0; i < count; i++) {
                                                C0966 r8 = (C0966) C0944.this.f3979.getItem(i);
                                                C0987.m6060((Object) (BuildConfig.FLAVOR + r8.f4259));
                                                if (i == 0) {
                                                    try {
                                                        jSONObject.put("patch1", r8.f4259);
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                                if (i == 1) {
                                                    try {
                                                        jSONObject.put("patch2", r8.f4259);
                                                    } catch (JSONException e2) {
                                                        e2.printStackTrace();
                                                    }
                                                }
                                                if (i == 2) {
                                                    try {
                                                        jSONObject.put("patch3", r8.f4259);
                                                    } catch (JSONException e3) {
                                                        e3.printStackTrace();
                                                    }
                                                }
                                                if (i == 3) {
                                                    try {
                                                        PackageInfo r0 = C0815.m5134("com.android.vending", 0);
                                                        if (r0 != null) {
                                                            try {
                                                                j = new File(C0815.m5208(r0.applicationInfo.sourceDir, false)).length();
                                                            } catch (Exception unused) {
                                                                j = 0;
                                                            }
                                                            if (j <= 1048576 && j != 0) {
                                                                try {
                                                                    new C0815(BuildConfig.FLAVOR).m5353(C0987.f4475 + ".pinfo " + r0.applicationInfo.sourceDir + " " + C0987.f4437 + " " + String.valueOf(r0.applicationInfo.uid) + " recovery");
                                                                    C0815.m5186(true);
                                                                    C0815.m5227(true);
                                                                    C0815.m5310("com.android.vending");
                                                                } catch (Exception e4) {
                                                                    e4.printStackTrace();
                                                                }
                                                            }
                                                        }
                                                        jSONObject.put("patch4", r8.f4259);
                                                    } catch (JSONException e5) {
                                                        e5.printStackTrace();
                                                    }
                                                }
                                                if (i == 4) {
                                                    try {
                                                        jSONObject.put("hide", r8.f4259);
                                                    } catch (JSONException e6) {
                                                        e6.printStackTrace();
                                                    }
                                                }
                                            }
                                            try {
                                                jSONObject.put("module_on", checkBox3.isChecked());
                                            } catch (JSONException e7) {
                                                e7.printStackTrace();
                                            }
                                            new Thread(new Runnable() {
                                                public void run() {
                                                    C0987.m6060((Object) ("write:" + jSONObject.toString()));
                                                    String r0 = C0822.m5385(jSONObject.toString().getBytes());
                                                    if (C0987.f4474) {
                                                        C0815 r1 = new C0815(BuildConfig.FLAVOR);
                                                        r1.m5353(C0987.f4475 + ".WriteSettingsXposed " + r0);
                                                    }
                                                    if (C0987.f4532.booleanValue()) {
                                                        WriteSettingsXposed.main(new String[]{r0});
                                                    }
                                                }
                                            }).start();
                                        }
                                    }).start();
                                }
                            });
                        }
                    }
                    return r63.m5947();
                case 3255:
                    ProgressDialog progressDialog = new ProgressDialog(C0987.f4432.m6233());
                    progressDialog.setTitle("Progress");
                    progressDialog.setMessage(C0815.m5205((int) R.string.collect_logs));
                    progressDialog.setIndeterminate(true);
                    return progressDialog;
                case 345350:
                case MainActivity.DIALOG_REPORT_FORCE_CLOSE /*3535788*/:
                    C0959 r05 = new C0959(C0987.f4432.m6233());
                    if (C0987.f4431 == 345350) {
                        str4 = C0815.m5205((int) R.string.send_logs);
                    } else {
                        str4 = C0815.m5205((int) R.string.error_detect);
                    }
                    r05.m5943(C0815.m5205((int) R.string.warning)).m5936((int) R.drawable.ic_bolen).m5950(str4).m5944(C0815.m5205((int) R.string.Yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            new AsyncTask<Void, Void, Boolean>() {
                                /* access modifiers changed from: protected */
                                /* renamed from: ʻ  reason: contains not printable characters */
                                public Boolean doInBackground(Void... voidArr) {
                                    return Boolean.valueOf(C0987.f4478.m5994(C0987.f4432.m6233(), true));
                                }

                                /* access modifiers changed from: protected */
                                public void onPreExecute() {
                                    C0987 r0 = C0987.f4432;
                                    C0987.m6084((Integer) 3255);
                                }

                                /* access modifiers changed from: protected */
                                /* renamed from: ʻ  reason: contains not printable characters */
                                public void onPostExecute(Boolean bool) {
                                    C0987 r0 = C0987.f4432;
                                    C0987.m6094((Integer) 3255);
                                    if (bool.booleanValue()) {
                                        try {
                                            C0973 r8 = C0987.f4478;
                                            C0227 r02 = C0987.f4432.m6233();
                                            r8.m5993(r02, "lp.chelpus@gmail.com", "Error Log", "Lucky Patcher " + C0987.m6068().getPackageInfo(C0987.f4432.m6233().getPackageName(), 0).versionName);
                                        } catch (PackageManager.NameNotFoundException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        C0987 r82 = C0987.f4432;
                                        C0987.m6084((Integer) 3535122);
                                    }
                                }
                            }.execute(new Void[0]);
                        }
                    }).m5954(C0815.m5205((int) R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    return r05.m5947();
                case 3535122:
                    C0959 r06 = new C0959(C0987.f4432.m6233());
                    r06.m5943("Error").m5950(C0815.m5205((int) R.string.error_collect_logs)).m5954("OK", (DialogInterface.OnClickListener) null);
                    return r06.m5947();
                default:
                    return null;
            }
        } catch (Exception e2) {
            C0987.m6060((Object) ("LuckyPatcher (Create Dialog): " + e2));
            e2.printStackTrace();
            C0959 r07 = new C0959(C0987.f4432.m6233());
            r07.m5943("Error").m5950("Sorry...\nShow Dialog - Error...").m5954("OK", (DialogInterface.OnClickListener) null);
            return r07.m5947();
        }
        C0987.m6060((Object) ("LuckyPatcher (Create Dialog): " + e2));
        e2.printStackTrace();
        C0959 r072 = new C0959(C0987.f4432.m6233());
        r072.m5943("Error").m5950("Sorry...\nShow Dialog - Error...").m5954("OK", (DialogInterface.OnClickListener) null);
        return r072.m5947();
    }
}
