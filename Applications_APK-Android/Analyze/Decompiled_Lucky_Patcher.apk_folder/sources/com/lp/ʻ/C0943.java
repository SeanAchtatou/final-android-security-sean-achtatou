package com.lp.ʻ;

import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.chelpus.C0815;
import com.lp.C0959;
import com.lp.C0987;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: AdvancedFilter */
public class C0943 {

    /* renamed from: ʻ  reason: contains not printable characters */
    C0943 f3972;

    /* renamed from: ʼ  reason: contains not printable characters */
    Dialog f3973;

    public C0943() {
        this.f3972 = null;
        this.f3973 = null;
        this.f3972 = this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5858() {
        if (this.f3973 == null) {
            this.f3973 = m5859();
        }
        Dialog dialog = this.f3973;
        if (dialog != null) {
            dialog.show();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Dialog m5859() {
        C0987.m6060((Object) "Market install Dialog create.");
        if (C0987.f4432 == null || C0987.f4432.m6233() == null) {
            m5860();
        }
        LinearLayout linearLayout = (LinearLayout) View.inflate(C0987.f4432.m6233(), R.layout.advanced_filter, null);
        LinearLayout linearLayout2 = (LinearLayout) linearLayout.findViewById(R.id.filterbodyscroll).findViewById(R.id.appdialogbody);
        TextView textView = (TextView) linearLayout2.findViewById(R.id.app_textView);
        RadioGroup radioGroup = (RadioGroup) linearLayout2.findViewById(R.id.radioGroup1);
        radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton) radioGroup.findViewById(R.id.nofilter);
        radioButton.setText(C0815.m5205((int) R.string.nofilter));
        if (C0987.f4507 == 0 || C0987.f4507 == R.id.nofilter) {
            radioButton.setChecked(true);
        }
        RadioButton radioButton2 = (RadioButton) radioGroup.findViewById(R.id.filter0);
        radioButton2.setText(C0815.m5205((int) R.string.filter0));
        if (C0987.f4507 == R.id.filter0) {
            radioButton2.setChecked(true);
        }
        RadioButton radioButton3 = (RadioButton) radioGroup.findViewById(R.id.filter1);
        radioButton3.setText(C0815.m5205((int) R.string.filter1));
        if (C0987.f4507 == R.id.filter1) {
            radioButton3.setChecked(true);
        }
        RadioButton radioButton4 = (RadioButton) radioGroup.findViewById(R.id.filter2);
        radioButton4.setText(C0815.m5205((int) R.string.filter2));
        if (C0987.f4507 == R.id.filter2) {
            radioButton4.setChecked(true);
        }
        RadioButton radioButton5 = (RadioButton) radioGroup.findViewById(R.id.filter11);
        radioButton5.setText(C0815.m5205((int) R.string.filter11));
        if (C0987.f4507 == R.id.filter11) {
            radioButton5.setChecked(true);
        }
        RadioButton radioButton6 = (RadioButton) radioGroup.findViewById(R.id.filter12);
        radioButton6.setText(C0815.m5205((int) R.string.filter12));
        if (C0987.f4507 == R.id.filter12) {
            radioButton6.setChecked(true);
        }
        RadioButton radioButton7 = (RadioButton) radioGroup.findViewById(R.id.filter13);
        radioButton7.setText(C0815.m5205((int) R.string.filter13));
        if (C0987.f4507 == R.id.filter13) {
            radioButton7.setChecked(true);
        }
        RadioButton radioButton8 = (RadioButton) radioGroup.findViewById(R.id.filter4);
        radioButton8.setText(C0815.m5205((int) R.string.filter4));
        if (C0987.f4507 == R.id.filter4) {
            radioButton8.setChecked(true);
        }
        RadioButton radioButton9 = (RadioButton) radioGroup.findViewById(R.id.filter5);
        radioButton9.setText(C0815.m5205((int) R.string.filter5));
        if (C0987.f4507 == R.id.filter5) {
            radioButton9.setChecked(true);
        }
        RadioButton radioButton10 = (RadioButton) radioGroup.findViewById(R.id.filter6);
        radioButton10.setText(C0815.m5205((int) R.string.filter6));
        if (C0987.f4507 == R.id.filter6) {
            radioButton10.setChecked(true);
        }
        RadioButton radioButton11 = (RadioButton) radioGroup.findViewById(R.id.filter7);
        radioButton11.setText(C0815.m5205((int) R.string.filter7));
        if (C0987.f4507 == R.id.filter7) {
            radioButton11.setChecked(true);
        }
        RadioButton radioButton12 = (RadioButton) radioGroup.findViewById(R.id.filter8);
        radioButton12.setText(C0815.m5205((int) R.string.filter8));
        if (C0987.f4507 == R.id.filter8) {
            radioButton12.setChecked(true);
        }
        RadioButton radioButton13 = (RadioButton) radioGroup.findViewById(R.id.filter9);
        radioButton13.setText(C0815.m5205((int) R.string.filter9));
        if (C0987.f4507 == R.id.filter9) {
            radioButton13.setChecked(true);
        }
        RadioButton radioButton14 = (RadioButton) radioGroup.findViewById(R.id.filter10);
        radioButton14.setText(C0815.m5205((int) R.string.filter10));
        if (C0987.f4507 == R.id.filter10) {
            radioButton14.setChecked(true);
        }
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.nofilter) {
                    C0987.f4507 = 0;
                } else {
                    C0987.f4507 = i;
                }
            }
        });
        ((Button) linearLayout.findViewById(R.id.ok_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (C0987.f4439 != null) {
                    C0987.m6111(true);
                }
                C0943.this.f3972.m5860();
            }
        });
        ((Button) linearLayout.findViewById(R.id.cancel_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                C0943.this.f3972.m5860();
            }
        });
        Dialog r0 = new C0959(C0987.f4432.m6233(), true).m5940(linearLayout).m5947();
        r0.setCancelable(true);
        r0.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
            }
        });
        return r0;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m5860() {
        Dialog dialog = this.f3973;
        if (dialog != null) {
            dialog.dismiss();
            this.f3973 = null;
        }
    }
}
