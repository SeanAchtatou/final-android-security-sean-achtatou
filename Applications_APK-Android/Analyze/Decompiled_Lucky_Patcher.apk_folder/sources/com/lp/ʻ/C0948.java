package com.lp.ʻ;

import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.chelpus.C0815;
import com.lp.C0959;
import com.lp.C0987;
import org.json.JSONException;
import org.json.JSONObject;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ʻ.ʿ  reason: contains not printable characters */
/* compiled from: Ext_Patch_Dialog */
public class C0948 {

    /* renamed from: ʻ  reason: contains not printable characters */
    Dialog f4099 = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5889() {
        if (this.f4099 == null) {
            this.f4099 = m5890();
        }
        Dialog dialog = this.f4099;
        if (dialog != null) {
            dialog.show();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Dialog m5890() {
        int i;
        C0987.m6060((Object) "Ext Patch Dialog create.");
        if (C0987.f4432 == null || C0987.f4432.m6233() == null) {
            m5891();
        }
        String[] strArr = null;
        LinearLayout linearLayout = (LinearLayout) View.inflate(C0987.f4432.m6233(), R.layout.extpatchdialog, null);
        LinearLayout linearLayout2 = (LinearLayout) linearLayout.findViewById(R.id.extpatchbodyscroll).findViewById(R.id.extdialogbodypatch);
        if (C0987.f4435 == null) {
            C0987.f4435 = " ";
        }
        C0987.f4445 = 0;
        C0987.f4460 = C0987.f4432.m6233();
        String[] strArr2 = new String[C0987.f4435.split("\n").length];
        String[] split = C0987.f4435.split("\n");
        String str = null;
        boolean z = true;
        loop0:
        while (true) {
            i = 0;
            while (z && i < split.length) {
                try {
                    str = new JSONObject(split[i]).getString("objects");
                    z = false;
                } catch (JSONException unused) {
                    i++;
                    z = true;
                }
            }
        }
        int parseInt = str != null ? Integer.parseInt(str) : 0;
        if (parseInt != 0) {
            strArr = new String[(parseInt + 1)];
            while (i < strArr.length) {
                if (i == 0) {
                    strArr[0] = "Please Select";
                } else {
                    strArr[i] = "Object N" + i;
                }
                i++;
            }
        }
        C0987.f4458 = (TextView) linearLayout2.findViewById(R.id.doetogo);
        C0987.f4458.append(C0815.m5136(C0815.m5205((int) R.string.extpat1) + " " + parseInt + " " + C0815.m5205((int) R.string.extpat2), -16711821, "bold"));
        Spinner spinner = (Spinner) linearLayout2.findViewById(R.id.spinner1);
        if (strArr != null) {
            C0987.f4445 = 0;
            spinner.setAdapter((SpinnerAdapter) new ArrayAdapter(C0987.f4432.m6233(), 17367048, strArr));
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onNothingSelected(AdapterView<?> adapterView) {
                }

                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                    if (i != 0) {
                        C0987.f4445 = i;
                    }
                    C0987.f4458.invalidate();
                }
            });
        }
        if (parseInt == 0) {
            spinner.setEnabled(false);
            C0987.f4445 = 0;
        }
        String r2 = C0815.m5205((int) R.string.extpat3);
        C0987.f4458 = (TextView) linearLayout2.findViewById(R.id.posle);
        C0987.f4458.append(C0815.m5136(r2, -990142, "bold"));
        if (!C0987.f4435.contains("SU Java-Code Running!")) {
            C0987.f4458 = (TextView) linearLayout2.findViewById(R.id.posle);
            C0987.f4458.append(C0815.m5136("\n" + C0815.m5205((int) R.string.no_root) + "\n", -16711681, "bold"));
        }
        Dialog r0 = new C0959(C0987.f4432.m6233(), true).m5940(linearLayout).m5947();
        r0.setTitle(C0815.m5205((int) R.string.PatchResult));
        r0.setCancelable(true);
        r0.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
            }
        });
        return r0;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m5891() {
        Dialog dialog = this.f4099;
        if (dialog != null) {
            dialog.dismiss();
            this.f4099 = null;
        }
    }
}
