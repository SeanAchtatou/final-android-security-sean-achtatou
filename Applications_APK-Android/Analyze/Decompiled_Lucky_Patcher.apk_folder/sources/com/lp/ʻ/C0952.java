package com.lp.ʻ;

import android.graphics.drawable.Drawable;
import com.lp.C0987;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

/* renamed from: com.lp.ʻ.ˊ  reason: contains not printable characters */
/* compiled from: MenuItemDialog */
public class C0952 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public int f4127 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    public int f4128 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    public String f4129 = BuildConfig.FLAVOR;

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean f4130;

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean f4131 = false;

    /* renamed from: ˆ  reason: contains not printable characters */
    public Drawable f4132 = null;

    public C0952(int i, int i2, String str) {
        this.f4127 = i;
        this.f4128 = i2;
        this.f4129 = str;
    }

    public C0952(int i, int i2, String str, int i3) {
        this.f4127 = i;
        this.f4128 = i2;
        this.f4129 = str;
        try {
            this.f4132 = C0987.m6069().getDrawable(i3);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public C0952(int i, int i2, String str, boolean z) {
        this.f4127 = i;
        this.f4128 = i2;
        this.f4129 = str;
        this.f4130 = z;
    }

    public C0952(int i, boolean z) {
        this.f4127 = i;
        this.f4128 = this.f4128;
        this.f4129 = this.f4129;
        this.f4130 = this.f4130;
        this.f4131 = z;
    }

    public C0952(int i, int i2, String str, boolean z, int i3) {
        this.f4127 = i;
        this.f4128 = i2;
        this.f4129 = str;
        this.f4130 = z;
        try {
            this.f4132 = C0987.m6069().getDrawable(i3);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
