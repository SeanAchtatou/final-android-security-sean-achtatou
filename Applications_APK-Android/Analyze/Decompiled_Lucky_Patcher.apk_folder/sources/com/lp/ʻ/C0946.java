package com.lp.ʻ;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.chelpus.C0815;
import com.lp.C0959;
import com.lp.C0987;
import ru.pKkcGXHI.kKSaIWSZS.MainActivity;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ʻ.ʽ  reason: contains not printable characters */
/* compiled from: App_Dialog */
public class C0946 {

    /* renamed from: ʻ  reason: contains not printable characters */
    Dialog f4029 = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5868() {
        if (this.f4029 == null) {
            this.f4029 = m5869();
        }
        Dialog dialog = this.f4029;
        if (dialog != null) {
            dialog.show();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Dialog m5869() {
        C0987.m6060((Object) "App Dialog create.");
        if (C0987.f4432 == null || C0987.f4432.m6233() == null) {
            m5870();
        }
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        final SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder();
        final SpannableStringBuilder spannableStringBuilder3 = new SpannableStringBuilder();
        LinearLayout linearLayout = (LinearLayout) View.inflate(C0987.f4432.m6233(), R.layout.applicationdialog, null);
        LinearLayout linearLayout2 = (LinearLayout) linearLayout.findViewById(R.id.appbodyscroll).findViewById(R.id.appdialogbody);
        ImageView imageView = (ImageView) linearLayout2.findViewById(R.id.app_imageView);
        final TextView textView = (TextView) linearLayout2.findViewById(R.id.app_textView);
        final TextView textView2 = (TextView) linearLayout2.findViewById(R.id.app_textView2);
        final TextView textView3 = (TextView) linearLayout2.findViewById(R.id.app_textView3);
        final TextView textView4 = (TextView) linearLayout2.findViewById(R.id.app_textView31);
        final ProgressBar progressBar = (ProgressBar) linearLayout2.findViewById(R.id.progressBar1);
        ((LinearLayout) linearLayout2.findViewById(R.id.google_play_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    if (C0987.f4447 != null) {
                        MainActivity mainActivity = C0987.f4447;
                        mainActivity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + C0987.f4440.f4337)));
                    }
                } catch (ActivityNotFoundException unused) {
                    if (C0987.f4447 != null) {
                        MainActivity mainActivity2 = C0987.f4447;
                        mainActivity2.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=" + C0987.f4440.f4337)));
                    }
                }
            }
        });
        textView2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    ((ClipboardManager) C0987.m6072().getSystemService("clipboard")).setText(textView2.getText());
                    Toast.makeText(C0987.m6072(), C0815.m5205((int) R.string.clipboard_message_for_app_dilaog_app_additional_info), 0).show();
                } catch (NoClassDefFoundError unused) {
                }
            }
        });
        textView3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    ((ClipboardManager) C0987.m6072().getSystemService("clipboard")).setText(textView3.getText());
                    Toast.makeText(C0987.m6072(), C0815.m5205((int) R.string.clipboard_message_for_app_dilaog_app_info), 0).show();
                } catch (NoClassDefFoundError unused) {
                }
            }
        });
        textView4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    ((ClipboardManager) C0987.m6072().getSystemService("clipboard")).setText(textView4.getText());
                    Toast.makeText(C0987.m6072(), C0815.m5205((int) R.string.clipboard_message_for_app_dilaog_app_permissions), 0).show();
                } catch (NoClassDefFoundError unused) {
                }
            }
        });
        try {
            imageView.setImageDrawable(C0987.m6068().getApplicationIcon(C0987.f4440.f4337));
        } catch (PackageManager.NameNotFoundException e) {
            try {
                e.printStackTrace();
            } catch (OutOfMemoryError e2) {
                e2.printStackTrace();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        new Thread(new Runnable() {
            /* JADX WARNING: Can't wrap try/catch for region: R(43:0|1|2|3|(2:5|6)|7|(1:9)(1:10)|11|(1:13)|14|(1:16)|17|(1:19)|20|(1:26)|27|(1:29)(1:30)|31|(1:33)(1:34)|35|(1:37)(1:38)|39|(3:40|41|(3:45|(8:48|49|50|51|(2:54|55)(1:56)|57|58|46)|114))|63|64|65|70|71|72|(3:74|(2:77|75)|115)|78|83|84|85|86|87|88|89|90|91|(8:93|94|95|96|97|98|99|106)|111|117) */
            /* JADX WARNING: Can't wrap try/catch for region: R(45:0|1|2|3|(2:5|6)|7|(1:9)(1:10)|11|(1:13)|14|(1:16)|17|(1:19)|20|(1:26)|27|(1:29)(1:30)|31|(1:33)(1:34)|35|(1:37)(1:38)|39|40|41|(3:45|(8:48|49|50|51|(2:54|55)(1:56)|57|58|46)|114)|63|64|65|70|71|72|(3:74|(2:77|75)|115)|78|83|84|85|86|87|88|89|90|91|(8:93|94|95|96|97|98|99|106)|111|117) */
            /* JADX WARNING: Missing exception handler attribute for start block: B:88:0x05ba */
            /* JADX WARNING: Missing exception handler attribute for start block: B:90:0x05e3 */
            /* JADX WARNING: Removed duplicated region for block: B:93:0x05e7 A[SYNTHETIC, Splitter:B:93:0x05e7] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r18 = this;
                    r1 = r18
                    java.lang.String r2 = "%.3f"
                    java.lang.String r3 = " Mb"
                    java.lang.String r4 = "\n\n"
                    java.lang.String r5 = " "
                    java.lang.String r6 = "#6699cc"
                    java.lang.String r7 = "bold"
                    com.lp.ᵔ r8 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r8 = r8.f4338     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r9 = "#be6e17"
                    android.text.SpannableString r8 = com.chelpus.C0815.m5137(r8, r9, r7)     // Catch:{ NullPointerException -> 0x0683 }
                    android.os.Handler r9 = com.lp.C0987.f4502     // Catch:{ NullPointerException -> 0x0683 }
                    com.lp.ʻ.ʽ$5$1 r10 = new com.lp.ʻ.ʽ$5$1     // Catch:{ NullPointerException -> 0x0683 }
                    r10.<init>(r8)     // Catch:{ NullPointerException -> 0x0683 }
                    r9.post(r10)     // Catch:{ NullPointerException -> 0x0683 }
                    com.lp.ᵔ r8 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r8 = r8.f4337     // Catch:{ NullPointerException -> 0x0683 }
                    boolean r8 = com.chelpus.C0815.m5286(r8)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r9 = "\n"
                    java.lang.String r10 = ""
                    if (r8 == 0) goto L_0x0052
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0683 }
                    r8.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    r11 = 2131689531(0x7f0f003b, float:1.900808E38)
                    java.lang.String r11 = com.chelpus.C0815.m5205(r11)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r11)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r9)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r8 = r8.toString()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r11 = r5     // Catch:{ NullPointerException -> 0x0683 }
                    r12 = -16728876(0xffffffffff00bcd4, float:-1.7112164E38)
                    android.text.SpannableString r8 = com.chelpus.C0815.m5136(r8, r12, r10)     // Catch:{ NullPointerException -> 0x0683 }
                    r11.append(r8)     // Catch:{ NullPointerException -> 0x0683 }
                L_0x0052:
                    com.lp.ᵔ r8 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x0683 }
                    boolean r8 = r8.f4355     // Catch:{ NullPointerException -> 0x0683 }
                    if (r8 == 0) goto L_0x0078
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0683 }
                    r8.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    r11 = 2131689822(0x7f0f015e, float:1.900867E38)
                    java.lang.String r11 = com.chelpus.C0815.m5205(r11)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r11)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r9)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r8 = r8.toString()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r11 = r5     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableString r8 = com.chelpus.C0815.m5137(r8, r10, r10)     // Catch:{ NullPointerException -> 0x0683 }
                    r11.append(r8)     // Catch:{ NullPointerException -> 0x0683 }
                    goto L_0x0097
                L_0x0078:
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0683 }
                    r8.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    r11 = 2131689811(0x7f0f0153, float:1.9008648E38)
                    java.lang.String r11 = com.chelpus.C0815.m5205(r11)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r11)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r9)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r8 = r8.toString()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r11 = r5     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableString r8 = com.chelpus.C0815.m5137(r8, r10, r10)     // Catch:{ NullPointerException -> 0x0683 }
                    r11.append(r8)     // Catch:{ NullPointerException -> 0x0683 }
                L_0x0097:
                    com.lp.ᵔ r8 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x0683 }
                    boolean r8 = r8.f4347     // Catch:{ NullPointerException -> 0x0683 }
                    r11 = -990142(0xfffffffffff0e442, float:NaN)
                    if (r8 == 0) goto L_0x00bf
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0683 }
                    r8.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    r12 = 2131690195(0x7f0f02d3, float:1.9009427E38)
                    java.lang.String r12 = com.chelpus.C0815.m5205(r12)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r12)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r9)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r8 = r8.toString()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r12 = r5     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableString r8 = com.chelpus.C0815.m5136(r8, r11, r10)     // Catch:{ NullPointerException -> 0x0683 }
                    r12.append(r8)     // Catch:{ NullPointerException -> 0x0683 }
                L_0x00bf:
                    com.lp.ᵔ r8 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x0683 }
                    boolean r8 = r8.f4348     // Catch:{ NullPointerException -> 0x0683 }
                    r12 = -16711821(0xffffffffff00ff73, float:-1.7146755E38)
                    if (r8 == 0) goto L_0x00e7
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0683 }
                    r8.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    r13 = 2131690198(0x7f0f02d6, float:1.9009433E38)
                    java.lang.String r13 = com.chelpus.C0815.m5205(r13)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r13)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r9)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r8 = r8.toString()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r13 = r5     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableString r8 = com.chelpus.C0815.m5136(r8, r12, r10)     // Catch:{ NullPointerException -> 0x0683 }
                    r13.append(r8)     // Catch:{ NullPointerException -> 0x0683 }
                L_0x00e7:
                    com.lp.ᵔ r8 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x0683 }
                    boolean r8 = r8.f4349     // Catch:{ NullPointerException -> 0x0683 }
                    if (r8 == 0) goto L_0x010c
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0683 }
                    r8.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    r13 = 2131690191(0x7f0f02cf, float:1.9009419E38)
                    java.lang.String r13 = com.chelpus.C0815.m5205(r13)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r13)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r9)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r8 = r8.toString()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r13 = r5     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableString r8 = com.chelpus.C0815.m5136(r8, r11, r10)     // Catch:{ NullPointerException -> 0x0683 }
                    r13.append(r8)     // Catch:{ NullPointerException -> 0x0683 }
                L_0x010c:
                    com.lp.ᵔ r8 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x0683 }
                    boolean r8 = r8.f4349     // Catch:{ NullPointerException -> 0x0683 }
                    if (r8 != 0) goto L_0x0140
                    com.lp.ᵔ r8 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x0683 }
                    boolean r8 = r8.f4348     // Catch:{ NullPointerException -> 0x0683 }
                    if (r8 != 0) goto L_0x0140
                    com.lp.ᵔ r8 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x0683 }
                    boolean r8 = r8.f4347     // Catch:{ NullPointerException -> 0x0683 }
                    if (r8 != 0) goto L_0x0140
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0683 }
                    r8.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    r13 = 2131690204(0x7f0f02dc, float:1.9009445E38)
                    java.lang.String r13 = com.chelpus.C0815.m5205(r13)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r13)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r9)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r8 = r8.toString()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r13 = r5     // Catch:{ NullPointerException -> 0x0683 }
                    r14 = -65451(0xffffffffffff0055, float:NaN)
                    android.text.SpannableString r8 = com.chelpus.C0815.m5136(r8, r14, r10)     // Catch:{ NullPointerException -> 0x0683 }
                    r13.append(r8)     // Catch:{ NullPointerException -> 0x0683 }
                L_0x0140:
                    com.lp.ᵔ r8 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x0683 }
                    boolean r8 = r8.f4351     // Catch:{ NullPointerException -> 0x0683 }
                    if (r8 == 0) goto L_0x0166
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0683 }
                    r8.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    r13 = 2131690199(0x7f0f02d7, float:1.9009435E38)
                    java.lang.String r13 = com.chelpus.C0815.m5205(r13)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r13)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r9)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r8 = r8.toString()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r13 = r5     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableString r8 = com.chelpus.C0815.m5136(r8, r12, r10)     // Catch:{ NullPointerException -> 0x0683 }
                    r13.append(r8)     // Catch:{ NullPointerException -> 0x0683 }
                    goto L_0x0185
                L_0x0166:
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0683 }
                    r8.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    r13 = 2131690202(0x7f0f02da, float:1.900944E38)
                    java.lang.String r13 = com.chelpus.C0815.m5205(r13)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r13)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r9)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r8 = r8.toString()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r13 = r5     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableString r8 = com.chelpus.C0815.m5137(r8, r10, r10)     // Catch:{ NullPointerException -> 0x0683 }
                    r13.append(r8)     // Catch:{ NullPointerException -> 0x0683 }
                L_0x0185:
                    com.lp.ᵔ r8 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x0683 }
                    boolean r8 = r8.f4353     // Catch:{ NullPointerException -> 0x0683 }
                    com.lp.ᵔ r13 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x0683 }
                    boolean r13 = r13.f4352     // Catch:{ NullPointerException -> 0x0683 }
                    boolean r8 = com.chelpus.C0815.m5197(r8, r13)     // Catch:{ NullPointerException -> 0x0683 }
                    if (r8 == 0) goto L_0x01b3
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0683 }
                    r8.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    r13 = 2131690197(0x7f0f02d5, float:1.900943E38)
                    java.lang.String r13 = com.chelpus.C0815.m5205(r13)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r13)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r9)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r8 = r8.toString()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r13 = r5     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableString r8 = com.chelpus.C0815.m5136(r8, r11, r10)     // Catch:{ NullPointerException -> 0x0683 }
                    r13.append(r8)     // Catch:{ NullPointerException -> 0x0683 }
                    goto L_0x01d2
                L_0x01b3:
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0683 }
                    r8.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    r11 = 2131690200(0x7f0f02d8, float:1.9009437E38)
                    java.lang.String r11 = com.chelpus.C0815.m5205(r11)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r11)     // Catch:{ NullPointerException -> 0x0683 }
                    r8.append(r9)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r8 = r8.toString()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r11 = r5     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableString r8 = com.chelpus.C0815.m5137(r8, r10, r10)     // Catch:{ NullPointerException -> 0x0683 }
                    r11.append(r8)     // Catch:{ NullPointerException -> 0x0683 }
                L_0x01d2:
                    com.lp.ᵔ r8 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x0683 }
                    boolean r8 = r8.f4352     // Catch:{ NullPointerException -> 0x0683 }
                    if (r8 == 0) goto L_0x01ec
                    r8 = 2131690205(0x7f0f02dd, float:1.9009447E38)
                    java.lang.String r8 = com.chelpus.C0815.m5205(r8)     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r11 = r5     // Catch:{ NullPointerException -> 0x0683 }
                    r12 = -162281(0xfffffffffffd8617, float:NaN)
                    android.text.SpannableString r8 = com.chelpus.C0815.m5136(r8, r12, r10)     // Catch:{ NullPointerException -> 0x0683 }
                    r11.append(r8)     // Catch:{ NullPointerException -> 0x0683 }
                    goto L_0x01fc
                L_0x01ec:
                    r8 = 2131690203(0x7f0f02db, float:1.9009443E38)
                    java.lang.String r8 = com.chelpus.C0815.m5205(r8)     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r11 = r5     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableString r8 = com.chelpus.C0815.m5136(r8, r12, r10)     // Catch:{ NullPointerException -> 0x0683 }
                    r11.append(r8)     // Catch:{ NullPointerException -> 0x0683 }
                L_0x01fc:
                    android.os.Handler r8 = com.lp.C0987.f4502     // Catch:{ NullPointerException -> 0x0683 }
                    com.lp.ʻ.ʽ$5$2 r11 = new com.lp.ʻ.ʽ$5$2     // Catch:{ NullPointerException -> 0x0683 }
                    r11.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    r8.post(r11)     // Catch:{ NullPointerException -> 0x0683 }
                    r8 = 0
                    java.lang.String[] r11 = new java.lang.String[r8]     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String[] r11 = new java.lang.String[r8]     // Catch:{ NullPointerException -> 0x0683 }
                    android.content.pm.PackageManager r11 = com.lp.C0987.m6068()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r12 = r6     // Catch:{ NullPointerException -> 0x0683 }
                    r12.clear()     // Catch:{ NullPointerException -> 0x0683 }
                    com.lp.ᵔ r12 = com.lp.C0987.f4440     // Catch:{ NameNotFoundException -> 0x028b }
                    java.lang.String r12 = r12.f4337     // Catch:{ NameNotFoundException -> 0x028b }
                    r13 = 4096(0x1000, float:5.74E-42)
                    android.content.pm.PackageInfo r12 = r11.getPackageInfo(r12, r13)     // Catch:{ NameNotFoundException -> 0x028b }
                    java.lang.String[] r12 = r12.requestedPermissions     // Catch:{ NameNotFoundException -> 0x028b }
                    if (r12 == 0) goto L_0x0290
                    int r13 = r12.length     // Catch:{ NameNotFoundException -> 0x028b }
                    if (r13 <= 0) goto L_0x0290
                    int r13 = r12.length     // Catch:{ NameNotFoundException -> 0x028b }
                    java.lang.String[] r13 = new java.lang.String[r13]     // Catch:{ NameNotFoundException -> 0x028b }
                    r13 = 0
                L_0x0229:
                    int r14 = r12.length     // Catch:{ NameNotFoundException -> 0x028b }
                    if (r13 >= r14) goto L_0x0290
                    java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x028b }
                    r14.<init>()     // Catch:{ NameNotFoundException -> 0x028b }
                    r15 = r12[r13]     // Catch:{ NameNotFoundException -> 0x028b }
                    r14.append(r15)     // Catch:{ NameNotFoundException -> 0x028b }
                    r14.append(r9)     // Catch:{ NameNotFoundException -> 0x028b }
                    java.lang.String r14 = r14.toString()     // Catch:{ NameNotFoundException -> 0x028b }
                    android.text.SpannableStringBuilder r15 = r6     // Catch:{ NameNotFoundException -> 0x028b }
                    android.text.SpannableString r14 = com.chelpus.C0815.m5137(r14, r6, r7)     // Catch:{ NameNotFoundException -> 0x028b }
                    r15.append(r14)     // Catch:{ NameNotFoundException -> 0x028b }
                    r14 = 0
                    r15 = r12[r13]     // Catch:{ NameNotFoundException | Exception -> 0x0256 }
                    android.content.pm.PermissionInfo r15 = r11.getPermissionInfo(r15, r8)     // Catch:{ NameNotFoundException | Exception -> 0x0256 }
                    java.lang.CharSequence r15 = r15.loadDescription(r11)     // Catch:{ NameNotFoundException | Exception -> 0x0256 }
                    java.lang.String r14 = r15.toString()     // Catch:{ NameNotFoundException | Exception -> 0x0256 }
                    goto L_0x0257
                L_0x0256:
                L_0x0257:
                    if (r14 != 0) goto L_0x0270
                    java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x028b }
                    r14.<init>()     // Catch:{ NameNotFoundException -> 0x028b }
                    r15 = 2131690111(0x7f0f027f, float:1.9009256E38)
                    java.lang.String r15 = com.chelpus.C0815.m5205(r15)     // Catch:{ NameNotFoundException -> 0x028b }
                    r14.append(r15)     // Catch:{ NameNotFoundException -> 0x028b }
                    r14.append(r4)     // Catch:{ NameNotFoundException -> 0x028b }
                    java.lang.String r14 = r14.toString()     // Catch:{ NameNotFoundException -> 0x028b }
                    goto L_0x027f
                L_0x0270:
                    java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x028b }
                    r15.<init>()     // Catch:{ NameNotFoundException -> 0x028b }
                    r15.append(r14)     // Catch:{ NameNotFoundException -> 0x028b }
                    r15.append(r4)     // Catch:{ NameNotFoundException -> 0x028b }
                    java.lang.String r14 = r15.toString()     // Catch:{ NameNotFoundException -> 0x028b }
                L_0x027f:
                    android.text.SpannableStringBuilder r15 = r6     // Catch:{ NameNotFoundException -> 0x028b }
                    android.text.SpannableString r14 = com.chelpus.C0815.m5137(r14, r10, r10)     // Catch:{ NameNotFoundException -> 0x028b }
                    r15.append(r14)     // Catch:{ NameNotFoundException -> 0x028b }
                    int r13 = r13 + 1
                    goto L_0x0229
                L_0x028b:
                    r0 = move-exception
                    r11 = r0
                    r11.printStackTrace()     // Catch:{ NullPointerException -> 0x0683 }
                L_0x0290:
                    android.os.Handler r11 = com.lp.C0987.f4502     // Catch:{ NullPointerException -> 0x0683 }
                    com.lp.ʻ.ʽ$5$3 r12 = new com.lp.ʻ.ʽ$5$3     // Catch:{ NullPointerException -> 0x0683 }
                    r12.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    r11.post(r12)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0683 }
                    r11.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    r12 = 2131690065(0x7f0f0251, float:1.9009163E38)
                    java.lang.String r12 = com.chelpus.C0815.m5205(r12)     // Catch:{ NullPointerException -> 0x0683 }
                    r11.append(r12)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r12 = ":\n"
                    r11.append(r12)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r11 = r11.toString()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r12 = r8     // Catch:{ NullPointerException -> 0x0683 }
                    r12.clear()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r12 = r8     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableString r11 = com.chelpus.C0815.m5137(r11, r6, r7)     // Catch:{ NullPointerException -> 0x0683 }
                    r12.append(r11)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0683 }
                    r11.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    com.lp.ᵔ r12 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r12 = r12.f4337     // Catch:{ NullPointerException -> 0x0683 }
                    r11.append(r12)     // Catch:{ NullPointerException -> 0x0683 }
                    r11.append(r9)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r11 = r11.toString()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r12 = r8     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableString r11 = com.chelpus.C0815.m5137(r11, r10, r10)     // Catch:{ NullPointerException -> 0x0683 }
                    r12.append(r11)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0683 }
                    r11.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    r12 = 2131690190(0x7f0f02ce, float:1.9009417E38)
                    java.lang.String r12 = com.chelpus.C0815.m5205(r12)     // Catch:{ NullPointerException -> 0x0683 }
                    r11.append(r12)     // Catch:{ NullPointerException -> 0x0683 }
                    r11.append(r9)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r11 = r11.toString()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r12 = r8     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableString r11 = com.chelpus.C0815.m5137(r11, r6, r7)     // Catch:{ NullPointerException -> 0x0683 }
                    r12.append(r11)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0328 }
                    r11.<init>()     // Catch:{ Exception -> 0x0328 }
                    android.content.pm.PackageManager r12 = com.lp.C0987.m6068()     // Catch:{ Exception -> 0x0328 }
                    com.lp.ᵔ r13 = com.lp.C0987.f4440     // Catch:{ Exception -> 0x0328 }
                    java.lang.String r13 = r13.f4337     // Catch:{ Exception -> 0x0328 }
                    android.content.Intent r12 = r12.getLaunchIntentForPackage(r13)     // Catch:{ Exception -> 0x0328 }
                    android.content.ComponentName r12 = r12.getComponent()     // Catch:{ Exception -> 0x0328 }
                    java.lang.String r12 = r12.getClassName()     // Catch:{ Exception -> 0x0328 }
                    r11.append(r12)     // Catch:{ Exception -> 0x0328 }
                    r11.append(r9)     // Catch:{ Exception -> 0x0328 }
                    java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x0328 }
                    android.text.SpannableStringBuilder r12 = r8     // Catch:{ Exception -> 0x0328 }
                    android.text.SpannableString r11 = com.chelpus.C0815.m5137(r11, r10, r10)     // Catch:{ Exception -> 0x0328 }
                    r12.append(r11)     // Catch:{ Exception -> 0x0328 }
                    goto L_0x032d
                L_0x0328:
                    r0 = move-exception
                    r11 = r0
                    r11.printStackTrace()     // Catch:{ NullPointerException -> 0x0683 }
                L_0x032d:
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0683 }
                    r11.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    r12 = 2131690192(0x7f0f02d0, float:1.900942E38)
                    java.lang.String r12 = com.chelpus.C0815.m5205(r12)     // Catch:{ NullPointerException -> 0x0683 }
                    r11.append(r12)     // Catch:{ NullPointerException -> 0x0683 }
                    r11.append(r9)     // Catch:{ NullPointerException -> 0x0683 }
                    java.lang.String r11 = r11.toString()     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableStringBuilder r12 = r8     // Catch:{ NullPointerException -> 0x0683 }
                    android.text.SpannableString r11 = com.chelpus.C0815.m5137(r11, r6, r7)     // Catch:{ NullPointerException -> 0x0683 }
                    r12.append(r11)     // Catch:{ NullPointerException -> 0x0683 }
                    android.content.pm.PackageManager r13 = com.lp.C0987.m6068()     // Catch:{ NameNotFoundException -> 0x0579 }
                    com.lp.ᵔ r14 = com.lp.C0987.f4440     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r14 = r14.f4337     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.content.pm.PackageInfo r13 = r13.getPackageInfo(r14, r8)     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.content.pm.ApplicationInfo r13 = r13.applicationInfo     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r13 = r13.sourceDir     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableStringBuilder r14 = r8     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableString r13 = com.chelpus.C0815.m5137(r13, r10, r10)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14.append(r13)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.<init>()     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r9)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14 = 2131690196(0x7f0f02d4, float:1.9009429E38)
                    java.lang.String r14 = com.chelpus.C0815.m5205(r14)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r14)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r9)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r13 = r13.toString()     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableStringBuilder r14 = r8     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableString r13 = com.chelpus.C0815.m5137(r13, r6, r7)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14.append(r13)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.<init>()     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.content.pm.PackageManager r14 = com.lp.C0987.m6068()     // Catch:{ NameNotFoundException -> 0x0579 }
                    com.lp.ᵔ r15 = com.lp.C0987.f4440     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r15 = r15.f4337     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.content.pm.PackageInfo r14 = r14.getPackageInfo(r15, r8)     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.content.pm.ApplicationInfo r14 = r14.applicationInfo     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r14 = r14.dataDir     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r14)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r14 = "/"
                    r13.append(r14)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r13 = r13.toString()     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableStringBuilder r14 = r8     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableString r13 = com.chelpus.C0815.m5137(r13, r10, r10)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14.append(r13)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.<init>()     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r9)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14 = 2131690208(0x7f0f02e0, float:1.9009453E38)
                    java.lang.String r14 = com.chelpus.C0815.m5205(r14)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r14)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r5)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r13 = r13.toString()     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableStringBuilder r14 = r8     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableString r13 = com.chelpus.C0815.m5137(r13, r6, r7)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14.append(r13)     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.content.pm.PackageManager r13 = com.lp.C0987.m6068()     // Catch:{ NameNotFoundException -> 0x0579 }
                    com.lp.ᵔ r14 = com.lp.C0987.f4440     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r14 = r14.f4337     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.content.pm.PackageInfo r13 = r13.getPackageInfo(r14, r8)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r13 = r13.versionName     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableStringBuilder r14 = r8     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableString r13 = com.chelpus.C0815.m5137(r13, r10, r10)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14.append(r13)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.<init>()     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r9)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14 = 2131690194(0x7f0f02d2, float:1.9009425E38)
                    java.lang.String r14 = com.chelpus.C0815.m5205(r14)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r14)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r5)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r13 = r13.toString()     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableStringBuilder r14 = r8     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableString r13 = com.chelpus.C0815.m5137(r13, r6, r7)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14.append(r13)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.<init>()     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r10)     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.content.pm.PackageManager r14 = com.lp.C0987.m6068()     // Catch:{ NameNotFoundException -> 0x0579 }
                    com.lp.ᵔ r15 = com.lp.C0987.f4440     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r15 = r15.f4337     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.content.pm.PackageInfo r14 = r14.getPackageInfo(r15, r8)     // Catch:{ NameNotFoundException -> 0x0579 }
                    int r14 = r14.versionCode     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r14)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r13 = r13.toString()     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableStringBuilder r14 = r8     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableString r13 = com.chelpus.C0815.m5137(r13, r10, r10)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14.append(r13)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.<init>()     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r9)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14 = 2131690207(0x7f0f02df, float:1.9009451E38)
                    java.lang.String r14 = com.chelpus.C0815.m5205(r14)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r14)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r5)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r13 = r13.toString()     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableStringBuilder r14 = r8     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableString r13 = com.chelpus.C0815.m5137(r13, r6, r7)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14.append(r13)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.<init>()     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r10)     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.content.pm.PackageManager r14 = com.lp.C0987.m6068()     // Catch:{ NameNotFoundException -> 0x0579 }
                    com.lp.ᵔ r15 = com.lp.C0987.f4440     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r15 = r15.f4337     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.content.pm.PackageInfo r14 = r14.getPackageInfo(r15, r8)     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.content.pm.ApplicationInfo r14 = r14.applicationInfo     // Catch:{ NameNotFoundException -> 0x0579 }
                    int r14 = r14.uid     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r14)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r13 = r13.toString()     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableStringBuilder r14 = r8     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableString r13 = com.chelpus.C0815.m5137(r13, r10, r10)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14.append(r13)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.<init>()     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r9)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14 = 2131689925(0x7f0f01c5, float:1.900888E38)
                    java.lang.String r14 = com.chelpus.C0815.m5205(r14)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r14)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13.append(r5)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r13 = r13.toString()     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableStringBuilder r14 = r8     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableString r13 = com.chelpus.C0815.m5137(r13, r6, r7)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14.append(r13)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.text.SimpleDateFormat r13 = new java.text.SimpleDateFormat     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r14 = "yyyy-MM-dd HH:mm"
                    r13.<init>(r14)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14.<init>()     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14.append(r10)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.util.Date r15 = new java.util.Date     // Catch:{ NameNotFoundException -> 0x0579 }
                    com.lp.ᵔ r12 = com.lp.C0987.f4440     // Catch:{ NameNotFoundException -> 0x0579 }
                    int r12 = r12.f4354     // Catch:{ NameNotFoundException -> 0x0579 }
                    long r11 = (long) r12     // Catch:{ NameNotFoundException -> 0x0579 }
                    r16 = 1000(0x3e8, double:4.94E-321)
                    long r11 = r11 * r16
                    r15.<init>(r11)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r11 = r13.format(r15)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r14.append(r11)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r11 = r14.toString()     // Catch:{ NameNotFoundException -> 0x0579 }
                    com.lp.ᵔ r12 = com.lp.C0987.f4440     // Catch:{ NameNotFoundException -> 0x0579 }
                    int r12 = r12.f4354     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.Integer r12 = java.lang.Integer.valueOf(r12)     // Catch:{ NameNotFoundException -> 0x0579 }
                    com.lp.C0987.m6060(r12)     // Catch:{ NameNotFoundException -> 0x0579 }
                    com.lp.ᵔ r12 = com.lp.C0987.f4440     // Catch:{ NameNotFoundException -> 0x0579 }
                    int r12 = r12.f4354     // Catch:{ NameNotFoundException -> 0x0579 }
                    long r12 = (long) r12     // Catch:{ NameNotFoundException -> 0x0579 }
                    long r12 = r12 * r16
                    java.lang.Long r12 = java.lang.Long.valueOf(r12)     // Catch:{ NameNotFoundException -> 0x0579 }
                    com.lp.C0987.m6060(r12)     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableStringBuilder r12 = r8     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableString r11 = com.chelpus.C0815.m5137(r11, r10, r10)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r12.append(r11)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x0579 }
                    r11.<init>()     // Catch:{ NameNotFoundException -> 0x0579 }
                    r11.append(r4)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r4 = 2131689524(0x7f0f0034, float:1.9008066E38)
                    java.lang.String r4 = com.chelpus.C0815.m5205(r4)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r11.append(r4)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r11.append(r5)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r4 = r11.toString()     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableStringBuilder r11 = r8     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableString r4 = com.chelpus.C0815.m5137(r4, r6, r7)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r11.append(r4)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.io.File r4 = new java.io.File     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.content.pm.PackageManager r11 = com.lp.C0987.m6068()     // Catch:{ NameNotFoundException -> 0x0579 }
                    com.lp.ᵔ r12 = com.lp.C0987.f4440     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r12 = r12.f4337     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.content.pm.PackageInfo r11 = r11.getPackageInfo(r12, r8)     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.content.pm.ApplicationInfo r11 = r11.applicationInfo     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r11 = r11.sourceDir     // Catch:{ NameNotFoundException -> 0x0579 }
                    r4.<init>(r11)     // Catch:{ NameNotFoundException -> 0x0579 }
                    long r11 = r4.length()     // Catch:{ NameNotFoundException -> 0x0579 }
                    float r4 = (float) r11     // Catch:{ NameNotFoundException -> 0x0579 }
                    r11 = 1233125376(0x49800000, float:1048576.0)
                    float r4 = r4 / r11
                    com.lp.ᵔ r11 = com.lp.C0987.f4440     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r11 = r11.f4337     // Catch:{ NameNotFoundException -> 0x0579 }
                    boolean r11 = com.chelpus.C0815.m5286(r11)     // Catch:{ NameNotFoundException -> 0x0579 }
                    if (r11 == 0) goto L_0x0553
                    com.lp.ᵔ r11 = com.lp.C0987.f4440     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r11 = r11.f4337     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.util.ArrayList r11 = com.chelpus.C0815.m5295(r11)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.util.Iterator r11 = r11.iterator()     // Catch:{ NameNotFoundException -> 0x0579 }
                L_0x053d:
                    boolean r12 = r11.hasNext()     // Catch:{ NameNotFoundException -> 0x0579 }
                    if (r12 == 0) goto L_0x0553
                    java.lang.Object r12 = r11.next()     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.io.File r12 = (java.io.File) r12     // Catch:{ NameNotFoundException -> 0x0579 }
                    long r12 = r12.length()     // Catch:{ NameNotFoundException -> 0x0579 }
                    float r12 = (float) r12     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13 = 1233125376(0x49800000, float:1048576.0)
                    float r12 = r12 / r13
                    float r4 = r4 + r12
                    goto L_0x053d
                L_0x0553:
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x0579 }
                    r11.<init>()     // Catch:{ NameNotFoundException -> 0x0579 }
                    r12 = 1
                    java.lang.Object[] r13 = new java.lang.Object[r12]     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.Float r4 = java.lang.Float.valueOf(r4)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r13[r8] = r4     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r4 = java.lang.String.format(r2, r13)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r11.append(r4)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r11.append(r3)     // Catch:{ NameNotFoundException -> 0x0579 }
                    java.lang.String r4 = r11.toString()     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableStringBuilder r11 = r8     // Catch:{ NameNotFoundException -> 0x0579 }
                    android.text.SpannableString r4 = com.chelpus.C0815.m5137(r4, r10, r10)     // Catch:{ NameNotFoundException -> 0x0579 }
                    r11.append(r4)     // Catch:{ NameNotFoundException -> 0x0579 }
                    goto L_0x057e
                L_0x0579:
                    r0 = move-exception
                    r4 = r0
                    r4.printStackTrace()     // Catch:{ NullPointerException -> 0x0683 }
                L_0x057e:
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x05e3 }
                    r4.<init>()     // Catch:{ Exception -> 0x05e3 }
                    r4.append(r9)     // Catch:{ Exception -> 0x05e3 }
                    r11 = 2131689789(0x7f0f013d, float:1.9008603E38)
                    java.lang.String r11 = com.chelpus.C0815.m5205(r11)     // Catch:{ Exception -> 0x05e3 }
                    r4.append(r11)     // Catch:{ Exception -> 0x05e3 }
                    r4.append(r5)     // Catch:{ Exception -> 0x05e3 }
                    java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x05e3 }
                    android.text.SpannableStringBuilder r11 = r8     // Catch:{ Exception -> 0x05e3 }
                    android.text.SpannableString r4 = com.chelpus.C0815.m5137(r4, r6, r7)     // Catch:{ Exception -> 0x05e3 }
                    r11.append(r4)     // Catch:{ Exception -> 0x05e3 }
                    r11 = 0
                    android.content.pm.PackageManager r4 = com.lp.C0987.m6068()     // Catch:{ Exception -> 0x05ba }
                    com.lp.ᵔ r13 = com.lp.C0987.f4440     // Catch:{ Exception -> 0x05ba }
                    java.lang.String r13 = r13.f4337     // Catch:{ Exception -> 0x05ba }
                    android.content.pm.PackageInfo r4 = r4.getPackageInfo(r13, r8)     // Catch:{ Exception -> 0x05ba }
                    android.content.pm.ApplicationInfo r4 = r4.applicationInfo     // Catch:{ Exception -> 0x05ba }
                    java.lang.String r4 = r4.sourceDir     // Catch:{ Exception -> 0x05ba }
                    java.io.File r4 = com.chelpus.C0815.m5318(r4)     // Catch:{ Exception -> 0x05ba }
                    long r11 = r4.length()     // Catch:{ Exception -> 0x05ba }
                L_0x05ba:
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x05e3 }
                    r4.<init>()     // Catch:{ Exception -> 0x05e3 }
                    r13 = 1
                    java.lang.Object[] r14 = new java.lang.Object[r13]     // Catch:{ Exception -> 0x05e3 }
                    float r11 = (float) r11     // Catch:{ Exception -> 0x05e3 }
                    r12 = 1233125376(0x49800000, float:1048576.0)
                    float r11 = r11 / r12
                    java.lang.Float r11 = java.lang.Float.valueOf(r11)     // Catch:{ Exception -> 0x05e3 }
                    r14[r8] = r11     // Catch:{ Exception -> 0x05e3 }
                    java.lang.String r2 = java.lang.String.format(r2, r14)     // Catch:{ Exception -> 0x05e3 }
                    r4.append(r2)     // Catch:{ Exception -> 0x05e3 }
                    r4.append(r3)     // Catch:{ Exception -> 0x05e3 }
                    java.lang.String r2 = r4.toString()     // Catch:{ Exception -> 0x05e3 }
                    android.text.SpannableStringBuilder r4 = r8     // Catch:{ Exception -> 0x05e3 }
                    android.text.SpannableString r2 = com.chelpus.C0815.m5137(r2, r10, r10)     // Catch:{ Exception -> 0x05e3 }
                    r4.append(r2)     // Catch:{ Exception -> 0x05e3 }
                L_0x05e3:
                    boolean r2 = com.lp.C0987.f4474     // Catch:{ NullPointerException -> 0x0683 }
                    if (r2 == 0) goto L_0x067a
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0675 }
                    r2.<init>()     // Catch:{ Throwable -> 0x0675 }
                    r2.append(r9)     // Catch:{ Throwable -> 0x0675 }
                    r4 = 2131689790(0x7f0f013e, float:1.9008605E38)
                    java.lang.String r4 = com.chelpus.C0815.m5205(r4)     // Catch:{ Throwable -> 0x0675 }
                    r2.append(r4)     // Catch:{ Throwable -> 0x0675 }
                    r2.append(r5)     // Catch:{ Throwable -> 0x0675 }
                    java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0675 }
                    android.text.SpannableStringBuilder r4 = r8     // Catch:{ Throwable -> 0x0675 }
                    android.text.SpannableString r2 = com.chelpus.C0815.m5137(r2, r6, r7)     // Catch:{ Throwable -> 0x0675 }
                    r4.append(r2)     // Catch:{ Throwable -> 0x0675 }
                    com.chelpus.ˆ r2 = new com.chelpus.ˆ     // Catch:{ Exception -> 0x064b }
                    r2.<init>(r10)     // Catch:{ Exception -> 0x064b }
                    r4 = 1
                    java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x064b }
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x064b }
                    r5.<init>()     // Catch:{ Exception -> 0x064b }
                    java.lang.String r6 = com.lp.C0987.f4475     // Catch:{ Exception -> 0x064b }
                    r5.append(r6)     // Catch:{ Exception -> 0x064b }
                    java.lang.String r6 = ".checkDataSize "
                    r5.append(r6)     // Catch:{ Exception -> 0x064b }
                    android.content.pm.PackageManager r6 = com.lp.C0987.m6068()     // Catch:{ Exception -> 0x064b }
                    com.lp.ᵔ r7 = com.lp.C0987.f4440     // Catch:{ Exception -> 0x064b }
                    java.lang.String r7 = r7.f4337     // Catch:{ Exception -> 0x064b }
                    android.content.pm.PackageInfo r6 = r6.getPackageInfo(r7, r8)     // Catch:{ Exception -> 0x064b }
                    android.content.pm.ApplicationInfo r6 = r6.applicationInfo     // Catch:{ Exception -> 0x064b }
                    java.lang.String r6 = r6.dataDir     // Catch:{ Exception -> 0x064b }
                    r5.append(r6)     // Catch:{ Exception -> 0x064b }
                    java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x064b }
                    r4[r8] = r5     // Catch:{ Exception -> 0x064b }
                    java.lang.String r2 = r2.m5353(r4)     // Catch:{ Exception -> 0x064b }
                    java.lang.String r4 = "SU Java-Code Running!\n"
                    java.lang.String r2 = r2.replace(r4, r10)     // Catch:{ Exception -> 0x0647 }
                    com.lp.C0987.m6060(r2)     // Catch:{ Exception -> 0x0647 }
                    goto L_0x0652
                L_0x0647:
                    r0 = move-exception
                    r4 = r2
                    r2 = r0
                    goto L_0x064e
                L_0x064b:
                    r0 = move-exception
                    r2 = r0
                    r4 = r10
                L_0x064e:
                    r2.printStackTrace()     // Catch:{ Throwable -> 0x0675 }
                    r2 = r4
                L_0x0652:
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0675 }
                    r4.<init>()     // Catch:{ Throwable -> 0x0675 }
                    java.lang.String r2 = r2.replace(r9, r10)     // Catch:{ Throwable -> 0x0675 }
                    java.lang.String r5 = "\r"
                    java.lang.String r2 = r2.replace(r5, r10)     // Catch:{ Throwable -> 0x0675 }
                    r4.append(r2)     // Catch:{ Throwable -> 0x0675 }
                    r4.append(r3)     // Catch:{ Throwable -> 0x0675 }
                    java.lang.String r2 = r4.toString()     // Catch:{ Throwable -> 0x0675 }
                    android.text.SpannableStringBuilder r3 = r8     // Catch:{ Throwable -> 0x0675 }
                    android.text.SpannableString r2 = com.chelpus.C0815.m5137(r2, r10, r10)     // Catch:{ Throwable -> 0x0675 }
                    r3.append(r2)     // Catch:{ Throwable -> 0x0675 }
                    goto L_0x067a
                L_0x0675:
                    r0 = move-exception
                    r2 = r0
                    r2.printStackTrace()     // Catch:{ NullPointerException -> 0x0683 }
                L_0x067a:
                    com.lp.ʻ.ʽ$5$4 r2 = new com.lp.ʻ.ʽ$5$4     // Catch:{ NullPointerException -> 0x0683 }
                    r2.<init>()     // Catch:{ NullPointerException -> 0x0683 }
                    com.lp.C0987.m6061(r2)     // Catch:{ NullPointerException -> 0x0683 }
                    goto L_0x0688
                L_0x0683:
                    r0 = move-exception
                    r2 = r0
                    r2.printStackTrace()
                L_0x0688:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.lp.ʻ.C0946.AnonymousClass5.run():void");
            }
        }).start();
        Dialog r0 = new C0959(C0987.f4432.m6233(), true).m5940(linearLayout).m5947();
        r0.setCancelable(true);
        r0.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
            }
        });
        return r0;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m5870() {
        Dialog dialog = this.f4029;
        if (dialog != null) {
            dialog.dismiss();
            this.f4029 = null;
        }
    }
}
