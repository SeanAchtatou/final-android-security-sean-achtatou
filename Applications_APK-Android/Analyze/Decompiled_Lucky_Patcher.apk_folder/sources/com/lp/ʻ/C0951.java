package com.lp.ʻ;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.chelpus.C0815;
import com.lp.C0959;
import com.lp.C0968;
import com.lp.C0987;
import java.io.File;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ʻ.ˉ  reason: contains not printable characters */
/* compiled from: Market_Install_Dialog */
public class C0951 {

    /* renamed from: ʻ  reason: contains not printable characters */
    Dialog f4110 = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5898() {
        if (this.f4110 == null) {
            this.f4110 = m5899();
        }
        Dialog dialog = this.f4110;
        if (dialog != null) {
            dialog.show();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Dialog m5899() {
        C0987.m6060((Object) "Market install Dialog create.");
        C0959 r0 = new C0959(C0987.f4432.m6233(), true);
        if (C0987.f4432 == null || C0987.f4432.m6233() == null) {
            m5900();
        }
        LinearLayout linearLayout = (LinearLayout) View.inflate(C0987.f4432.m6233(), R.layout.gm_installer, null);
        LinearLayout linearLayout2 = (LinearLayout) linearLayout.findViewById(R.id.marketbodyscroll).findViewById(R.id.appdialogbody);
        ((TextView) linearLayout.findViewById(R.id.app_textView2)).setText(C0815.m5205((int) R.string.select_market));
        ((TextView) linearLayout.findViewById(R.id.checkUserInstall)).setText(C0815.m5205((int) R.string.install_as_user_app));
        ((Button) linearLayout.findViewById(R.id.market_button)).setText(C0815.m5205((int) R.string.install));
        ((Button) linearLayout.findViewById(R.id.market_test_button)).setText(C0815.m5205((int) R.string.mod_market_check));
        TextView textView = (TextView) linearLayout2.findViewById(R.id.market_textView);
        RadioGroup radioGroup = (RadioGroup) linearLayout2.findViewById(R.id.radioGroup1);
        radioGroup.getCheckedRadioButtonId();
        C0987.f4573 = "mod.market16.apk";
        ((RadioButton) radioGroup.findViewById(R.id.radioProxy)).setText(C0815.m5205((int) R.string.proxyGP) + "\n" + C0815.m5205((int) R.string.describe_for_Proxy_gp));
        StringBuilder sb = new StringBuilder();
        sb.append(C0815.m5205((int) R.string.market_version5));
        sb.append(" 9.2.33 (Android TV)");
        ((RadioButton) radioGroup.findViewById(R.id.radioTV)).setText(sb.toString());
        ((RadioButton) radioGroup.findViewById(R.id.radioTVo)).setText(C0815.m5205((int) R.string.market_version6) + " 9.2.33 (Android TV)");
        ((RadioButton) radioGroup.findViewById(R.id.radio9)).setText(C0815.m5205((int) R.string.market_version5) + " 9.8.07 (Android 4.1+ and UP)");
        ((RadioButton) radioGroup.findViewById(R.id.radio9o)).setText(C0815.m5205((int) R.string.market_version6) + " 9.8.07 (Android 4.1+ and UP)");
        ((RadioButton) radioGroup.findViewById(R.id.radio10)).setText(C0815.m5205((int) R.string.market_version5) + " 10.7.19 (Android 5+ and UP)");
        ((RadioButton) radioGroup.findViewById(R.id.radio10o)).setText(C0815.m5205((int) R.string.market_version6) + " 10.7.19 (Android 5+ and UP)");
        ((RadioButton) radioGroup.findViewById(R.id.radio11)).setText(C0815.m5205((int) R.string.market_version5) + " 11.8.09 (Android 5+ and UP)");
        ((RadioButton) radioGroup.findViewById(R.id.radio11o)).setText(C0815.m5205((int) R.string.market_version6) + " 11.8.09 (Android 5+ and UP)");
        ((RadioButton) radioGroup.findViewById(R.id.radio12)).setText(C0815.m5205((int) R.string.market_version5) + " 14.5.52 (Android 5+ and UP)");
        ((RadioButton) radioGroup.findViewById(R.id.radio12o)).setText(C0815.m5205((int) R.string.market_version6) + " 14.5.52 (Android 5+ and UP)");
        ((RadioButton) radioGroup.findViewById(R.id.radio13)).setText(C0815.m5205((int) R.string.market_version5) + " 16.5.15 (Android 5+ and UP)");
        ((RadioButton) radioGroup.findViewById(R.id.radio13o)).setText(C0815.m5205((int) R.string.market_version6) + " 16.5.15 (Android 5+ and UP)");
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i != R.id.radioProxy) {
                    switch (i) {
                        case R.id.radio10 /*2131296500*/:
                            C0987.f4573 = "mod.market10.apk";
                            return;
                        case R.id.radio10o /*2131296501*/:
                            C0987.f4573 = "market10.apk";
                            return;
                        case R.id.radio11 /*2131296502*/:
                            C0987.f4573 = "mod.market11.apk";
                            return;
                        case R.id.radio11o /*2131296503*/:
                            C0987.f4573 = "market11.apk";
                            return;
                        case R.id.radio12 /*2131296504*/:
                            C0987.f4573 = "mod.market14.apk";
                            return;
                        case R.id.radio12o /*2131296505*/:
                            C0987.f4573 = "market14.apk";
                            return;
                        case R.id.radio13 /*2131296506*/:
                            C0987.f4573 = "mod.market16.apk";
                            return;
                        case R.id.radio13o /*2131296507*/:
                            C0987.f4573 = "market16.apk";
                            return;
                        case R.id.radio9 /*2131296508*/:
                            C0987.f4573 = "mod.market9.apk";
                            return;
                        case R.id.radio9o /*2131296509*/:
                            C0987.f4573 = "market9.apk";
                            return;
                        default:
                            switch (i) {
                                case R.id.radioTV /*2131296542*/:
                                    C0987.f4573 = "mod.market9tv.apk";
                                    return;
                                case R.id.radioTVo /*2131296543*/:
                                    C0987.f4573 = "market9tv.apk";
                                    return;
                                default:
                                    return;
                            }
                    }
                } else {
                    C0987.f4573 = "p.apk";
                    if (!new File(C0987.f4437 + "/p.apk").exists() && C0987.f4432 != null) {
                        C0815.m5254();
                    }
                }
            }
        });
        Button button = (Button) linearLayout.findViewById(R.id.market_button);
        CheckBox checkBox = (CheckBox) linearLayout.findViewById(R.id.checkUserInstall);
        C0987.f4572 = false;
        C0987.f4574 = false;
        if (!C0987.f4573.equals("p.apk")) {
            try {
                ApplicationInfo applicationInfo = C0987.m6068().getApplicationInfo("com.android.vending", 0);
                if (!C0815.m5289() || !C0815.m5298() || (applicationInfo.flags & 1) == 0) {
                    C0987.f4572 = false;
                    C0987.f4574 = false;
                    checkBox.setEnabled(true);
                    checkBox.setChecked(false);
                } else {
                    checkBox.setEnabled(true);
                    checkBox.setChecked(true);
                    C0987.f4572 = true;
                    C0987.f4574 = true;
                }
            } catch (PackageManager.NameNotFoundException unused) {
                C0987.f4572 = false;
                C0987.f4574 = false;
                checkBox.setEnabled(true);
                checkBox.setChecked(false);
            } catch (Exception e) {
                e.printStackTrace();
                C0987.f4572 = false;
                C0987.f4574 = false;
                checkBox.setEnabled(true);
                checkBox.setChecked(false);
            }
        } else if (!C0815.m5289() || !C0815.m5298()) {
            C0987.f4572 = false;
            C0987.f4574 = false;
            checkBox.setEnabled(true);
            checkBox.setChecked(false);
        } else {
            checkBox.setEnabled(true);
            checkBox.setChecked(true);
            C0987.f4572 = true;
            C0987.f4574 = true;
        }
        checkBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CheckBox checkBox = (CheckBox) view;
                if (!C0987.f4574) {
                    if (((!C0987.f4573.equals("p.apk") && !C0987.f4573.startsWith("mod.market")) || !C0815.m5289() || !C0815.m5298()) && !C0987.f4573.startsWith("market")) {
                        C0987 r0 = C0987.f4432;
                        C0987.m6062(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.install_as_user_app_note));
                        checkBox.setChecked(false);
                        C0987.f4572 = false;
                    } else if (checkBox.isChecked()) {
                        C0987.f4572 = true;
                    } else {
                        C0987.f4572 = false;
                    }
                } else if (checkBox.isChecked()) {
                    C0987.f4572 = true;
                } else if (!C0987.f4573.startsWith("mod.") || Integer.valueOf(C0987.f4573.replace("mod.market", BuildConfig.FLAVOR).replace(".apk", BuildConfig.FLAVOR).replace("tv", BuildConfig.FLAVOR)).intValue() < 13) {
                    C0987.f4572 = false;
                } else {
                    C0987.m6062(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.warning_install_new_mod_market));
                    C0987.f4572 = true;
                    checkBox.setChecked(true);
                }
            }
        });
        textView.setText(C0815.m5136("  " + C0815.m5205((int) R.string.internet_not_found), 65280, "bold"));
        textView.append("\n\n" + C0815.m5205((int) R.string.market_description_text));
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!C0987.f4573.equals("p.apk")) {
                    AnonymousClass3 r2 = new Runnable() {
                        public void run() {
                            if (new File(C0987.f4438 + "/Download/" + C0987.f4573).exists()) {
                                try {
                                    Context r5 = C0987.m6072();
                                    final C0968 r3 = new C0968(r5, new File(C0987.f4438 + "/Download/" + C0987.f4573), false);
                                    if (C0987.f4572) {
                                        C0987.m6085((Runnable) new Runnable() {
                                            public void run() {
                                                new C0815(BuildConfig.FLAVOR).m5353("pm uninstall com.android.vending");
                                                C0987.m6061((Runnable) new Runnable() {
                                                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                                     method: com.lp.ﾞﾞ.ʼ(com.lp.ˋ, boolean):void
                                                     arg types: [com.lp.ˋ, int]
                                                     candidates:
                                                      com.lp.ﾞﾞ.ʼ(java.io.File, boolean):void
                                                      com.lp.ﾞﾞ.ʼ(java.lang.String, java.lang.String):void
                                                      com.lp.ﾞﾞ.ʼ(java.lang.String, boolean):boolean
                                                      android.support.v4.ʻ.ˉ.ʼ(android.view.Menu, android.view.MenuInflater):boolean
                                                      com.lp.ﾞﾞ.ʼ(com.lp.ˋ, boolean):void */
                                                    public void run() {
                                                        C0987 r0 = C0987.f4432;
                                                        C0987.m6081(r3, true);
                                                    }
                                                });
                                            }
                                        });
                                    } else if (C0815.m5232(C0987.m6072().getPackageName())) {
                                        C0987 r0 = C0987.f4432;
                                        C0987.m6062(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.error_app_installed_on_sdcard_block));
                                    } else if (C0987.f4573.contains("mod.") && (!C0815.m5289() || !C0815.m5298())) {
                                        C0987.m6061((Runnable) new Runnable() {
                                            public void run() {
                                                C0815.m5170(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.start_market_message), new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        if (C0815.m5318("/system/framework/core.jar") != null && !C0815.m5282().equals("ART")) {
                                                            C0987.f4508 = true;
                                                        } else if (C0987.f4489 < 20 || !C0815.m5282().equals("ART") || !C0815.m5322()) {
                                                            C0987.f4508 = false;
                                                        } else {
                                                            C0987.f4508 = true;
                                                        }
                                                        C0987.f4509 = true;
                                                        C0987.f4432.m6181("_patch1_patch2");
                                                    }
                                                }, (DialogInterface.OnClickListener) null, (DialogInterface.OnCancelListener) null);
                                            }
                                        });
                                    } else if (!C0987.f4573.startsWith("mod.") || Integer.valueOf(C0987.f4573.replace("mod.market", BuildConfig.FLAVOR).replace(".apk", BuildConfig.FLAVOR).replace("tv", BuildConfig.FLAVOR)).intValue() < 13) {
                                        C0987.f4432.m6179(r3);
                                    } else {
                                        C0987.m6062(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.warning_install_new_mod_market));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    C0987 r02 = C0987.f4432;
                                    C0987.m6062(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.corrupt_download));
                                }
                            }
                        }
                    };
                    C0815.m5168(C0987.f4573, r2, r2);
                } else if (!C0815.m5289()) {
                    C0987.m6061((Runnable) new Runnable() {
                        public void run() {
                            C0815.m5170(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.start_market_message), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (C0815.m5318("/system/framework/core.jar") != null && !C0815.m5282().equals("ART")) {
                                        C0987.f4508 = true;
                                    } else if (C0987.f4489 < 20 || !C0815.m5282().equals("ART") || !C0815.m5322()) {
                                        C0987.f4508 = false;
                                    } else {
                                        C0987.f4508 = true;
                                    }
                                    C0987.f4509 = true;
                                    C0987.f4432.m6181("_patch1_patch2");
                                }
                            }, (DialogInterface.OnClickListener) null, (DialogInterface.OnCancelListener) null);
                        }
                    });
                } else {
                    new Thread(new Runnable() {
                        public void run() {
                            if (!C0987.f4572) {
                                if (new File(C0987.f4437 + "/p.apk").exists()) {
                                    Context r1 = C0987.m6072();
                                    C0968 r0 = new C0968(r1, new File(C0987.f4437 + "/p.apk"), false);
                                    if (C0987.f4432 != null) {
                                        C0987.f4432.m6179(r0);
                                        return;
                                    }
                                    return;
                                }
                                C0987.m6061((Runnable) new Runnable() {
                                    public void run() {
                                        C0987.m6062(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.proxyGP_install_error));
                                    }
                                });
                                return;
                            }
                            C0987.m6085((Runnable) new Runnable() {
                                public void run() {
                                    new C0815(BuildConfig.FLAVOR).m5353("pm uninstall com.android.vending");
                                    C0987.m6061((Runnable) new Runnable() {
                                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                         method: com.lp.ﾞﾞ.ʼ(com.lp.ˋ, boolean):void
                                         arg types: [com.lp.ˋ, int]
                                         candidates:
                                          com.lp.ﾞﾞ.ʼ(java.io.File, boolean):void
                                          com.lp.ﾞﾞ.ʼ(java.lang.String, java.lang.String):void
                                          com.lp.ﾞﾞ.ʼ(java.lang.String, boolean):boolean
                                          android.support.v4.ʻ.ˉ.ʼ(android.view.Menu, android.view.MenuInflater):boolean
                                          com.lp.ﾞﾞ.ʼ(com.lp.ˋ, boolean):void */
                                        public void run() {
                                            Context r1 = C0987.m6072();
                                            C0968 r0 = new C0968(r1, new File(C0987.f4437 + "/p.apk"), false);
                                            C0987 r12 = C0987.f4432;
                                            C0987.m6081(r0, true);
                                        }
                                    });
                                }
                            });
                        }
                    }).start();
                }
            }
        });
        r0.m5940(linearLayout);
        r0.m5952(true);
        r0.m5938(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
            }
        });
        return r0.m5947();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m5900() {
        Dialog dialog = this.f4110;
        if (dialog != null) {
            dialog.dismiss();
            this.f4110 = null;
        }
    }
}
