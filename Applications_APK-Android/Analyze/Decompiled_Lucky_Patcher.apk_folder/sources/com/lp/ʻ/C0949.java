package com.lp.ʻ;

import android.os.Bundle;
import android.support.v4.ʻ.C0222;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import com.lp.C0987;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ʻ.ˆ  reason: contains not printable characters */
/* compiled from: FilterFragment */
public class C0949 extends C0222 {

    /* renamed from: ﹳﹳ  reason: contains not printable characters */
    EditText f4102 = null;

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m5893(Bundle bundle) {
        super.m1254(bundle);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m5894() {
        EditText editText;
        InputMethodManager inputMethodManager = (InputMethodManager) C0987.m6072().getSystemService("input_method");
        if (!(inputMethodManager == null || (editText = this.f4102) == null)) {
            inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
        super.m1284();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ʻ  reason: contains not printable characters */
    public View m5892(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        super.m1196(layoutInflater, viewGroup, bundle);
        View inflate = layoutInflater.inflate((int) R.layout.fragment_filter, viewGroup, false);
        this.f4102 = (EditText) inflate.findViewById(R.id.editTextFilter);
        this.f4102.requestFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) m1236().getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(1, 1);
        }
        ((Button) inflate.findViewById(R.id.button_clear)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    if (C0987.f4439 != null) {
                        C0987.f4439.m6020().filter(BuildConfig.FLAVOR);
                        C0987.f4439.notifyDataSetChanged();
                    }
                    C0949.this.f4102.setText(BuildConfig.FLAVOR);
                } catch (Exception unused) {
                }
            }
        });
        ((Button) inflate.findViewById(R.id.button_adv)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    new C0943().m5858();
                } catch (Exception unused) {
                }
            }
        });
        this.f4102.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void afterTextChanged(Editable editable) {
                try {
                    if (C0987.f4439 != null) {
                        C0987.f4439.m6020().filter(editable.toString());
                        C0987.f4439.notifyDataSetChanged();
                    }
                } catch (Exception unused) {
                }
            }
        });
        return inflate;
    }
}
