package com.lp.ʻ;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.ʻ.C0227;
import android.support.v4.ʻ.C0239;
import com.chelpus.C0815;
import com.lp.C0982;
import com.lp.C0987;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ʻ.ـ  reason: contains not printable characters */
/* compiled from: Progress_Dialog_Loading */
public class C0958 {

    /* renamed from: ʿ  reason: contains not printable characters */
    public static C0982 f4158;

    /* renamed from: ʻ  reason: contains not printable characters */
    String f4159 = BuildConfig.FLAVOR;

    /* renamed from: ʼ  reason: contains not printable characters */
    String f4160 = BuildConfig.FLAVOR;

    /* renamed from: ʽ  reason: contains not printable characters */
    C0239 f4161 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    C0227 f4162 = null;

    /* renamed from: ˆ  reason: contains not printable characters */
    Dialog f4163 = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0958 m5923() {
        return new C0958();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m5928() {
        if (this.f4163 == null) {
            this.f4163 = m5931();
        }
        Dialog dialog = this.f4163;
        if (dialog != null) {
            dialog.show();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Dialog m5931() {
        f4158 = new C0982(C0987.f4432.m6233());
        f4158.m6022();
        if (this.f4160.equals(BuildConfig.FLAVOR)) {
            this.f4160 = C0815.m5205((int) R.string.download);
        }
        f4158.m6025(this.f4160);
        if (this.f4159.equals(BuildConfig.FLAVOR)) {
            this.f4159 = C0815.m5205((int) R.string.wait);
        }
        f4158.m6030(this.f4159);
        f4158.m6023((int) R.drawable.ic_wait);
        f4158.m6026(false);
        f4158.m6024(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                if (C0987.f4474) {
                    C0815.m5270();
                }
            }
        });
        return f4158.m6034();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5925(String str) {
        this.f4159 = str;
        if (f4158 == null) {
            m5931();
        }
        f4158.m6030(this.f4159);
        try {
            if (!C0815.m5337()) {
                C0987 r2 = C0987.f4432;
                C0987.m6061((Runnable) new Runnable() {
                    public void run() {
                        if (C0987.f4432 != null) {
                            C0987.f4432.m1253().m1405();
                        }
                    }
                });
            } else if (C0987.f4432 != null) {
                C0987.f4432.m1253().m1405();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5927(boolean z, Activity activity) {
        if (f4158 == null) {
            m5931();
        }
        if (z) {
            f4158.m6032(BuildConfig.FLAVOR);
        } else {
            f4158.m6032("%1d/%2d");
        }
        if (!z) {
            f4158.m6022();
        } else {
            f4158.m6028();
        }
        try {
            if (!C0815.m5337()) {
                C0987 r2 = C0987.f4432;
                C0987.m6061((Runnable) new Runnable() {
                    public void run() {
                        if (C0987.f4432 != null) {
                            C0987.f4432.m1253().m1405();
                        }
                    }
                });
            } else if (C0987.f4432 != null) {
                C0987.f4432.m1253().m1405();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5924(int i) {
        if (f4158 == null) {
            m5931();
        }
        f4158.m6029(i);
        try {
            if (!C0815.m5337()) {
                C0987 r2 = C0987.f4432;
                C0987.m6061((Runnable) new Runnable() {
                    public void run() {
                        if (C0987.f4432 != null) {
                            C0987.f4432.m1253().m1405();
                        }
                    }
                });
            } else if (C0987.f4432 != null) {
                C0987.f4432.m1253().m1405();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m5930(String str) {
        if (f4158 == null) {
            m5931();
        }
        f4158.m6032(str);
        try {
            if (!C0815.m5337()) {
                C0987 r2 = C0987.f4432;
                C0987.m6061((Runnable) new Runnable() {
                    public void run() {
                        if (C0987.f4432 != null) {
                            C0987.f4432.m1253().m1405();
                        }
                    }
                });
            } else if (C0987.f4432 != null) {
                C0987.f4432.m1253().m1405();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m5929(int i) {
        if (f4158 == null) {
            m5931();
        }
        f4158.m6031(i);
        try {
            if (!C0815.m5337()) {
                C0987 r2 = C0987.f4432;
                C0987.m6061((Runnable) new Runnable() {
                    public void run() {
                        if (C0987.f4432 != null) {
                            C0987.f4432.m1253().m1405();
                        }
                    }
                });
            } else if (C0987.f4432 != null) {
                C0987.f4432.m1253().m1405();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m5933() {
        C0982 r0 = f4158;
        return r0 != null && r0.m6033();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m5932(String str) {
        this.f4160 = str;
        if (f4158 == null) {
            m5931();
        }
        C0982 r2 = f4158;
        if (r2 != null) {
            r2.m6025(this.f4160);
            try {
                if (!C0815.m5337()) {
                    C0987 r22 = C0987.f4432;
                    C0987.m6061((Runnable) new Runnable() {
                        public void run() {
                            if (C0987.f4432 != null) {
                                C0987.f4432.m1253().m1405();
                            }
                        }
                    });
                } else if (C0987.f4432 != null) {
                    C0987.f4432.m1253().m1405();
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5926(boolean z) {
        Dialog dialog = this.f4163;
        if (dialog != null) {
            dialog.setCancelable(z);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m5934() {
        Dialog dialog = this.f4163;
        if (dialog != null) {
            dialog.dismiss();
            this.f4163 = null;
        }
    }
}
