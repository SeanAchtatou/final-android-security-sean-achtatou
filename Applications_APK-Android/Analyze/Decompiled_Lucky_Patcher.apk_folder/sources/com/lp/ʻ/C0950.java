package com.lp.ʻ;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.chelpus.C0815;
import com.chelpus.ʻ.C0780;
import com.lp.C0959;
import com.lp.C0980;
import com.lp.C0987;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ʻ.ˈ  reason: contains not printable characters */
/* compiled from: Integrate_Dalvik_Code_Dialog */
public class C0950 {

    /* renamed from: ʻ  reason: contains not printable characters */
    Dialog f4106 = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5895() {
        if (this.f4106 == null) {
            this.f4106 = m5896();
        }
        Dialog dialog = this.f4106;
        if (dialog != null) {
            dialog.show();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Dialog m5896() {
        C0987.m6060((Object) "Integrate Dalvik Code Dialog create.");
        final C0980 r0 = C0987.f4440;
        if (C0987.f4432 == null || C0987.f4432.m6233() == null) {
            m5897();
        }
        LinearLayout linearLayout = (LinearLayout) View.inflate(C0987.f4432.m6233(), R.layout.patchdialog, null);
        TextView textView = (TextView) ((LinearLayout) linearLayout.findViewById(R.id.patchbodyscroll).findViewById(R.id.dialogbodypatch)).findViewById(R.id.patchdesc);
        try {
            C0780 r5 = new C0780();
            r5.f3135 = true;
            r5.f3142 = false;
            C0987 r6 = C0987.f4432;
            C0987.m6054(textView, C0987.f4435, r5);
            textView.setText(C0815.m5137(C0815.m5205((int) R.string.integrate_dalvik_code_dialog_good_message), "#00FF00", BuildConfig.FLAVOR));
            textView.setTextAppearance(C0987.f4432.m6233(), C0987.m6076());
        } catch (Exception e) {
            e.printStackTrace();
        }
        C0959 r2 = new C0959(C0987.f4432.m6233());
        r2.m5936((int) R.drawable.ic_angel);
        r2.m5943(C0815.m5205((int) R.string.integrate_dalvik_code_dialog_title));
        return r2.m5952(true).m5944(C0815.m5205((int) R.string.ok), (DialogInterface.OnClickListener) null).m5949((int) R.string.launchbutton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    Intent launchIntentForPackage = C0987.m6068().getLaunchIntentForPackage(r0.f4337);
                    if (C0987.f4474) {
                        new Thread(new Runnable() {
                            public void run() {
                                try {
                                    C0815.m5310(C0987.m6072().getPackageManager().getPackageInfo(r0.f4337, 0).applicationInfo.processName);
                                } catch (PackageManager.NameNotFoundException e) {
                                    e.printStackTrace();
                                }
                                C0815.m5315(r0.f4337);
                            }
                        }).start();
                    } else if (C0987.f4447 != null) {
                        C0987.f4447.startActivity(launchIntentForPackage);
                    }
                } catch (Exception unused) {
                    Toast.makeText(C0987.m6072(), C0815.m5205((int) R.string.error_launch), 1).show();
                }
                C0950.this.m5897();
            }
        }).m5940(linearLayout).m5947();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m5897() {
        Dialog dialog = this.f4106;
        if (dialog != null) {
            dialog.dismiss();
            this.f4106 = null;
        }
    }
}
