package com.lp.ʻ;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.ʻ.C0239;
import com.chelpus.C0815;
import com.lp.C0982;
import com.lp.C0987;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ʻ.ˑ  reason: contains not printable characters */
/* compiled from: Progress_Dialog */
public class C0956 {

    /* renamed from: ʾ  reason: contains not printable characters */
    public static C0982 f4144;

    /* renamed from: ʻ  reason: contains not printable characters */
    String f4145 = BuildConfig.FLAVOR;

    /* renamed from: ʼ  reason: contains not printable characters */
    String f4146 = BuildConfig.FLAVOR;

    /* renamed from: ʽ  reason: contains not printable characters */
    C0239 f4147 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    Dialog f4148 = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0956 m5910() {
        return new C0956();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m5913() {
        if (this.f4148 == null) {
            this.f4148 = m5914();
        }
        Dialog dialog = this.f4148;
        if (dialog != null) {
            dialog.show();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Dialog m5914() {
        f4144 = new C0982(C0987.f4432.m6233());
        if (this.f4146.equals(BuildConfig.FLAVOR)) {
            this.f4146 = C0815.m5205((int) R.string.wait);
        }
        f4144.m6025(this.f4146);
        if (this.f4145.equals(BuildConfig.FLAVOR)) {
            this.f4145 = C0815.m5205((int) R.string.loadpkg);
        }
        f4144.m6030(this.f4145);
        f4144.m6026(true);
        f4144.m6024(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                if (C0987.f4474) {
                    C0815.m5270();
                }
            }
        });
        return f4144.m6034();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5911(String str) {
        if (f4144 == null) {
            m5914();
        }
        this.f4145 = str;
        f4144.m6030(this.f4145);
        C0987.f4432.m6233().runOnUiThread(new Runnable() {
            public void run() {
                C0987.f4432.m1253().m1405();
            }
        });
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5912(boolean z) {
        Dialog dialog = this.f4148;
        if (dialog != null) {
            dialog.setCancelable(z);
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m5915() {
        Dialog dialog = this.f4148;
        if (dialog != null) {
            dialog.dismiss();
            this.f4148 = null;
        }
    }
}
