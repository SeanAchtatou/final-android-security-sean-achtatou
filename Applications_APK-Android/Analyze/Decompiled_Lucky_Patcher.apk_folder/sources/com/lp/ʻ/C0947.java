package com.lp.ʻ;

import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.chelpus.C0815;
import com.chelpus.ʻ.C0780;
import com.lp.C0959;
import com.lp.C0960;
import com.lp.C0979;
import com.lp.C0987;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ʻ.ʾ  reason: contains not printable characters */
/* compiled from: Dialogs */
public class C0947 {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lp.ʻ.ˊ.<init>(int, int, java.lang.String, boolean):void
     arg types: [?, ?, java.lang.String, int]
     candidates:
      com.lp.ʻ.ˊ.<init>(int, int, java.lang.String, int):void
      com.lp.ʻ.ˊ.<init>(int, int, java.lang.String, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5883(final boolean z, final File file, final C0780 r11) {
        String str;
        C0959 r0 = new C0959(C0987.f4432.m6233());
        ArrayList arrayList = new ArrayList();
        arrayList.add(new C0952((int) R.string.contextpattlvl1, (int) R.string.contextpattlvl1_descr, "#ff00ff00", true));
        arrayList.add(new C0952((int) R.string.contextpattlvl2, (int) R.string.contextpattlvl2_descr, "#ff00ff00", false));
        arrayList.add(new C0952((int) R.string.contextpattlvl3, (int) R.string.contextpattlvl3_descr, "#ff00ff00", false));
        arrayList.add(new C0952((int) R.string.contextpattamazon, (int) R.string.contextpattamazon_descr, "#ff00ff00", false));
        arrayList.add(new C0952((int) R.string.contextpattsamsung, (int) R.string.contextpattsamsung_descr, "#ff00ff00", false));
        arrayList.add(new C0952((int) R.string.context_dependencies, (int) R.string.contextpattdependencies_descr, "#ff00ff00", false));
        if (!z && C0987.f4489 < 23) {
            arrayList.add(new C0952((int) R.string.contextpattlvl4, (int) R.string.contextpattlvl4_descr, "#ffffff00", false));
        }
        if (!z) {
            arrayList.add(new C0952((int) R.string.contextpattlvl5, (int) R.string.contextpattlvl5_descr, "#ffffff00", false));
        }
        if (!z && C0987.f4489 < 23) {
            arrayList.add(new C0952((int) R.string.contextpattlvl6, (int) R.string.contextpattlvl6_descr, "#ffff0000", false));
        }
        final ArrayAdapter r1 = m5871(arrayList);
        if (r1 != null) {
            r1.setNotifyOnChange(true);
            r0.m5955(true);
            r0.m5941(r1, new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    C0952 r5 = (C0952) r1.getItem(i);
                    if (r5.f4130) {
                        r5.f4130 = false;
                    } else {
                        r5.f4130 = true;
                    }
                    if (r5.f4127 == R.string.contextpattlvl1 && r5.f4130) {
                        C0947.m5872((int) R.string.contextpattlvl2, r1).f4130 = false;
                    }
                    if (r5.f4127 == R.string.contextpattlvl2 && r5.f4130) {
                        C0947.m5872((int) R.string.contextpattlvl1, r1).f4130 = false;
                    }
                    if (r5.f4127 == R.string.contextpattamazon && r5.f4130) {
                        C0947.m5872((int) R.string.contextpattlvl1, r1).f4130 = false;
                        C0947.m5872((int) R.string.contextpattlvl2, r1).f4130 = false;
                        C0947.m5872((int) R.string.contextpattlvl3, r1).f4130 = false;
                        C0947.m5872((int) R.string.contextpattsamsung, r1).f4130 = false;
                    }
                    if (r5.f4127 == R.string.contextpattsamsung && r5.f4130) {
                        C0947.m5872((int) R.string.contextpattlvl1, r1).f4130 = false;
                        C0947.m5872((int) R.string.contextpattlvl2, r1).f4130 = false;
                        C0947.m5872((int) R.string.contextpattlvl3, r1).f4130 = false;
                        C0947.m5872((int) R.string.contextpattamazon, r1).f4130 = false;
                    }
                    if (r5.f4127 == R.string.contextpattlvl4 && r5.f4130) {
                        C0947.m5872((int) R.string.contextpattlvl5, r1).f4130 = true;
                        C0947.m5872((int) R.string.contextpattlvl6, r1).f4130 = false;
                    }
                    if (r5.f4127 == R.string.contextpattlvl6 && r5.f4130) {
                        C0947.m5872((int) R.string.contextpattlvl5, r1).f4130 = true;
                        C0947.m5872((int) R.string.contextpattlvl4, r1).f4130 = false;
                    }
                    r1.notifyDataSetChanged();
                }
            });
            AnonymousClass12 r2 = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    C0780 r6 = new C0780();
                    C0780 r7 = r11;
                    if (r7 != null) {
                        r6 = r7;
                    }
                    int count = r1.getCount();
                    String str = "1";
                    for (int i2 = 0; i2 < count; i2++) {
                        C0952 r3 = (C0952) r1.getItem(i2);
                        if (r3.f4130) {
                            switch (r3.f4127) {
                                case R.string.context_dependencies /*2131689618*/:
                                    str = str + "dependencies_";
                                    continue;
                                case R.string.contextpattamazon /*2131689710*/:
                                    str = str + "amazon_";
                                    continue;
                                case R.string.contextpattlvl1 /*2131689713*/:
                                    str = "pattern1_pattern2_pattern3_pattern5_";
                                    continue;
                                case R.string.contextpattlvl2 /*2131689715*/:
                                    str = "pattern1_pattern2_pattern3_pattern6_";
                                    continue;
                                case R.string.contextpattlvl3 /*2131689717*/:
                                    str = str + "pattern4_";
                                    continue;
                                case R.string.contextpattlvl4 /*2131689719*/:
                                    r6.f3138 = true;
                                    continue;
                                case R.string.contextpattlvl5 /*2131689721*/:
                                    r6.f3140 = true;
                                    continue;
                                case R.string.contextpattlvl6 /*2131689723*/:
                                    r6.f3144 = true;
                                    continue;
                                case R.string.contextpattsamsung /*2131689725*/:
                                    str = str + "samsung_";
                                    continue;
                            }
                        }
                    }
                    r6.f3128 = str;
                    r6.f3123 = true;
                    r6.f3142 = z;
                    r6.f3141 = file;
                    if (r11 == null) {
                        C0987 r72 = C0987.f4432;
                        C0987.m6055(C0987.f4440, r6);
                        return;
                    }
                    C0987.f4432.m6121(r6, z);
                }
            };
            if (!z) {
                if (r11 == null) {
                    str = C0815.m5205((int) R.string.patch_ok);
                } else {
                    str = C0815.m5205((int) R.string.multipatch_next_step);
                }
            } else if (r11 == null) {
                str = C0815.m5205((int) R.string.create_perm_ok);
            } else {
                str = C0815.m5205((int) R.string.multipatch_next_step);
            }
            r0.m5944(str, r2);
        }
        r0.m5947().show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lp.ʻ.ˊ.<init>(int, int, java.lang.String, boolean):void
     arg types: [?, ?, java.lang.String, int]
     candidates:
      com.lp.ʻ.ˊ.<init>(int, int, java.lang.String, int):void
      com.lp.ʻ.ˊ.<init>(int, int, java.lang.String, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5882(final boolean z, final File file) {
        String str;
        C0959 r0 = new C0959(C0987.f4432.m6233());
        ArrayList arrayList = new ArrayList();
        arrayList.add(new C0952((int) R.string.contextpatt1, (int) R.string.contextpatt1_descr, "#ff00ff00", true));
        arrayList.add(new C0952((int) R.string.contextpatt2, (int) R.string.contextpatt2_descr, "#ff00ff00", false));
        arrayList.add(new C0952((int) R.string.contextpatt3, (int) R.string.contextpatt3_descr, "#ff00ff00", false));
        arrayList.add(new C0952((int) R.string.contextpatt4, (int) R.string.contextpatt4_descr, "#ff00ff00", false));
        arrayList.add(new C0952((int) R.string.contextpatt5, (int) R.string.contextpatt5_descr, "#ff00ff00", false));
        arrayList.add(new C0952((int) R.string.contextpatt6, (int) R.string.contextpatt6_descr, "#ff00ff00", false));
        if (!z && C0987.f4489 < 23) {
            arrayList.add(new C0952((int) R.string.contextpattlvl4, (int) R.string.contextpattlvl4_descr, "#ffffff00", false));
        }
        if (!z) {
            arrayList.add(new C0952((int) R.string.contextpattlvl5, (int) R.string.contextpattlvl5_descr, "#ffffff00", false));
        }
        if (!z && C0987.f4489 < 23) {
            arrayList.add(new C0952((int) R.string.contextpattlvl6, (int) R.string.contextpattlvl6_descr, "#ffff0000", false));
        }
        final ArrayAdapter r1 = m5871(arrayList);
        if (r1 != null) {
            r1.setNotifyOnChange(true);
            r0.m5955(true);
            r0.m5941(r1, new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    C0952 r3 = (C0952) r1.getItem(i);
                    if (r3.f4130) {
                        r3.f4130 = false;
                    } else {
                        r3.f4130 = true;
                    }
                    if (r3.f4127 == R.string.contextpatt5 && r3.f4130) {
                        C0947.m5872((int) R.string.contextpatt6, r1).f4130 = false;
                    }
                    if (r3.f4127 == R.string.contextpatt6 && r3.f4130) {
                        C0947.m5872((int) R.string.contextpatt5, r1).f4130 = false;
                    }
                    if (r3.f4127 == R.string.contextpattlvl4 && r3.f4130) {
                        C0947.m5872((int) R.string.contextpattlvl5, r1).f4130 = true;
                        C0947.m5872((int) R.string.contextpattlvl6, r1).f4130 = false;
                    }
                    if (r3.f4127 == R.string.contextpattlvl6 && r3.f4130) {
                        C0947.m5872((int) R.string.contextpattlvl5, r1).f4130 = true;
                        C0947.m5872((int) R.string.contextpattlvl4, r1).f4130 = false;
                    }
                    r1.notifyDataSetChanged();
                }
            });
            AnonymousClass18 r2 = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    C0780 r6 = new C0780();
                    int count = r1.getCount();
                    String str = "1";
                    for (int i2 = 0; i2 < count; i2++) {
                        C0952 r3 = (C0952) r1.getItem(i2);
                        if (r3.f4130) {
                            switch (r3.f4127) {
                                case R.string.contextpatt1 /*2131689686*/:
                                    str = str + "pattern1_";
                                    continue;
                                case R.string.contextpatt2 /*2131689688*/:
                                    str = str + "pattern2_";
                                    continue;
                                case R.string.contextpatt3 /*2131689690*/:
                                    str = str + "pattern3_";
                                    continue;
                                case R.string.contextpatt4 /*2131689692*/:
                                    str = str + "pattern4_";
                                    continue;
                                case R.string.contextpatt5 /*2131689694*/:
                                    str = str + "pattern5_";
                                    continue;
                                case R.string.contextpatt6 /*2131689696*/:
                                    str = str + "pattern6_";
                                    continue;
                                case R.string.contextpattlvl4 /*2131689719*/:
                                    r6.f3138 = true;
                                    continue;
                                case R.string.contextpattlvl5 /*2131689721*/:
                                    r6.f3140 = true;
                                    continue;
                                case R.string.contextpattlvl6 /*2131689723*/:
                                    r6.f3144 = true;
                                    continue;
                            }
                        }
                    }
                    r6.f3128 = str;
                    r6.f3123 = true;
                    r6.f3142 = z;
                    r6.f3141 = file;
                    C0987 r7 = C0987.f4432;
                    C0987.m6055(C0987.f4440, r6);
                }
            };
            if (!z) {
                str = C0815.m5205((int) R.string.patch_ok);
            } else {
                str = C0815.m5205((int) R.string.create_perm_ok);
            }
            r0.m5944(str, r2);
        }
        r0.m5947().show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lp.ʻ.ˊ.<init>(int, int, java.lang.String, boolean):void
     arg types: [?, ?, java.lang.String, int]
     candidates:
      com.lp.ʻ.ˊ.<init>(int, int, java.lang.String, int):void
      com.lp.ʻ.ˊ.<init>(int, int, java.lang.String, boolean):void */
    /* renamed from: ʼ  reason: contains not printable characters */
    public static void m5886(final boolean z, final File file, final C0780 r11) {
        String str;
        C0959 r0 = new C0959(C0987.f4432.m6233());
        ArrayList arrayList = new ArrayList();
        arrayList.add(new C0952((int) R.string.contextblockfileads, (int) R.string.contextblockfileads_descr, "#ff00ff00", true));
        arrayList.add(new C0952((int) R.string.contextpattads3, (int) R.string.contextpattads3_descr, "#ff00ff00", true));
        arrayList.add(new C0952((int) R.string.contextpattads6, (int) R.string.contextpattads6_descr, "#ff00ff00", true));
        arrayList.add(new C0952((int) R.string.contextpattads4, (int) R.string.contextpattads4_descr, "#ff00ff00", true));
        arrayList.add(new C0952((int) R.string.context_dependencies, (int) R.string.contextpattdependencies_descr, "#ff00ff00", false));
        arrayList.add(new C0952((int) R.string.contextfulloffline, (int) R.string.contextfulloffline_descr, "#ff00ff00", false));
        if (!z && C0987.f4489 < 23) {
            arrayList.add(new C0952((int) R.string.contextpattlvl4, (int) R.string.contextpattlvl4_descr, "#ffffff00", false));
        }
        if (!z) {
            arrayList.add(new C0952((int) R.string.contextpattlvl5, (int) R.string.contextpattlvl5_descr, "#ffffff00", false));
        }
        if (!z && C0987.f4489 < 23) {
            arrayList.add(new C0952((int) R.string.contextpattlvl6, (int) R.string.contextpattlvl6_descr, "#ffff0000", false));
        }
        final ArrayAdapter r1 = m5871(arrayList);
        if (r1 != null) {
            r1.setNotifyOnChange(true);
            r0.m5955(true);
            r0.m5941(r1, new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    C0952 r3 = (C0952) r1.getItem(i);
                    if (r3.f4130) {
                        r3.f4130 = false;
                    } else {
                        r3.f4130 = true;
                    }
                    if (r3.f4127 == R.string.contextfulloffline && r3.f4130) {
                        C0947.m5872((int) R.string.contextpattads6, r1).f4130 = false;
                    }
                    if (r3.f4127 == R.string.contextpattads6 && r3.f4130) {
                        C0947.m5872((int) R.string.contextfulloffline, r1).f4130 = false;
                    }
                    if (r3.f4127 == R.string.contextpattlvl4 && r3.f4130) {
                        C0947.m5872((int) R.string.contextpattlvl5, r1).f4130 = true;
                        C0947.m5872((int) R.string.contextpattlvl6, r1).f4130 = false;
                    }
                    if (r3.f4127 == R.string.contextpattlvl6 && r3.f4130) {
                        C0947.m5872((int) R.string.contextpattlvl5, r1).f4130 = true;
                        C0947.m5872((int) R.string.contextpattlvl4, r1).f4130 = false;
                    }
                    r1.notifyDataSetChanged();
                }
            });
            AnonymousClass20 r2 = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    C0780 r6 = new C0780();
                    C0780 r7 = r11;
                    if (r7 != null) {
                        r6 = r7;
                    }
                    int count = r1.getCount();
                    String str = "1";
                    for (int i2 = 0; i2 < count; i2++) {
                        C0952 r3 = (C0952) r1.getItem(i2);
                        if (r3.f4130) {
                            switch (r3.f4127) {
                                case R.string.context_dependencies /*2131689618*/:
                                    str = str + "dependencies_";
                                    continue;
                                case R.string.contextblockfileads /*2131689658*/:
                                    str = str + "pattern0_";
                                    continue;
                                case R.string.contextfulloffline /*2131689679*/:
                                    str = str + "fulloffline_";
                                    continue;
                                case R.string.contextpattads3 /*2131689702*/:
                                    str = str + "pattern3_";
                                    continue;
                                case R.string.contextpattads4 /*2131689704*/:
                                    str = str + "pattern6_";
                                    continue;
                                case R.string.contextpattads6 /*2131689708*/:
                                    str = str + "pattern5_";
                                    continue;
                                case R.string.contextpattlvl4 /*2131689719*/:
                                    r6.f3138 = true;
                                    continue;
                                case R.string.contextpattlvl5 /*2131689721*/:
                                    r6.f3140 = true;
                                    continue;
                                case R.string.contextpattlvl6 /*2131689723*/:
                                    r6.f3144 = true;
                                    continue;
                            }
                        }
                    }
                    r6.f3130 = str;
                    r6.f3121 = true;
                    r6.f3142 = z;
                    r6.f3141 = file;
                    if (r11 == null) {
                        C0987 r72 = C0987.f4432;
                        C0987.m6055(C0987.f4440, r6);
                        return;
                    }
                    C0987.f4432.m6121(r6, z);
                }
            };
            if (!z) {
                if (r11 == null) {
                    str = C0815.m5205((int) R.string.patch_ok);
                } else {
                    str = C0815.m5205((int) R.string.multipatch_next_step);
                }
            } else if (r11 == null) {
                str = C0815.m5205((int) R.string.create_perm_ok);
            } else {
                str = C0815.m5205((int) R.string.multipatch_next_step);
            }
            r0.m5944(str, r2);
        }
        r0.m5947().show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lp.ʻ.ˊ.<init>(int, int, java.lang.String, boolean):void
     arg types: [?, ?, java.lang.String, int]
     candidates:
      com.lp.ʻ.ˊ.<init>(int, int, java.lang.String, int):void
      com.lp.ʻ.ˊ.<init>(int, int, java.lang.String, boolean):void */
    /* renamed from: ʽ  reason: contains not printable characters */
    public static void m5888(final boolean z, final File file, final C0780 r11) {
        String str;
        C0959 r0 = new C0959(C0987.f4432.m6233());
        ArrayList arrayList = new ArrayList();
        arrayList.add(new C0952(R.string.support_patch_header_lvl, true));
        arrayList.add(new C0952((int) R.string.support_patch_lvl, (int) R.string.support_patch_lvl_descr, "#ff00ff00", false));
        arrayList.add(new C0952(R.string.support_patch_header_inapp, true));
        arrayList.add(new C0952((int) R.string.support_patch_billing_rebuild_dex, (int) R.string.support_patch_billing_rebuild_dex_descr, "#ff00ff00", true));
        arrayList.add(new C0952((int) R.string.support_patch_billing_redirect_to_proxy, (int) R.string.support_patch_billing_redirect_to_proxy_descr, "#ff00ff00", false));
        arrayList.add(new C0952(R.string.support_patch_header_additional, true));
        arrayList.add(new C0952((int) R.string.support_patch_billing_repatch_patched_app, (int) R.string.support_patch_billing_repatch_patched_app_descr, "#ff00ff00", false));
        arrayList.add(new C0952(R.string.support_patch_header_options, true));
        if (!z && C0987.f4489 < 23) {
            arrayList.add(new C0952((int) R.string.contextpattlvl4, (int) R.string.contextpattlvl4_descr, "#ffffff00", false));
        }
        if (!z) {
            arrayList.add(new C0952((int) R.string.contextpattlvl5, (int) R.string.contextpattlvl5_descr, "#ffffff00", false));
        }
        if (!z && C0987.f4489 < 23) {
            arrayList.add(new C0952((int) R.string.contextpattlvl6, (int) R.string.contextpattlvl6_descr, "#ffff0000", false));
        }
        final ArrayAdapter r1 = m5871(arrayList);
        if (r1 != null) {
            r1.setNotifyOnChange(true);
            r0.m5955(true);
            r0.m5941(r1, new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    C0952 r3 = (C0952) r1.getItem(i);
                    if (r3.f4130) {
                        r3.f4130 = false;
                    } else {
                        r3.f4130 = true;
                    }
                    if (r3.f4127 == R.string.support_patch_billing_redirect_to_proxy && r3.f4130) {
                        C0947.m5872((int) R.string.support_patch_billing_rebuild_dex, r1).f4130 = false;
                    }
                    if (r3.f4127 == R.string.support_patch_billing_rebuild_dex && r3.f4130) {
                        C0947.m5872((int) R.string.support_patch_billing_redirect_to_proxy, r1).f4130 = false;
                    }
                    if (r3.f4127 == R.string.contextpattlvl4 && r3.f4130) {
                        C0947.m5872((int) R.string.contextpattlvl5, r1).f4130 = true;
                        C0947.m5872((int) R.string.contextpattlvl6, r1).f4130 = false;
                    }
                    if (r3.f4127 == R.string.contextpattlvl6 && r3.f4130) {
                        C0947.m5872((int) R.string.contextpattlvl5, r1).f4130 = true;
                        C0947.m5872((int) R.string.contextpattlvl4, r1).f4130 = false;
                    }
                    r1.notifyDataSetChanged();
                }
            });
            AnonymousClass22 r2 = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    C0780 r6 = new C0780();
                    C0780 r7 = r11;
                    if (r7 != null) {
                        r6 = r7;
                    }
                    int count = r1.getCount();
                    String str = "1";
                    for (int i2 = 0; i2 < count; i2++) {
                        C0952 r3 = (C0952) r1.getItem(i2);
                        if (r3.f4130) {
                            switch (r3.f4127) {
                                case R.string.contextpattlvl4 /*2131689719*/:
                                    r6.f3138 = true;
                                    continue;
                                case R.string.contextpattlvl5 /*2131689721*/:
                                    r6.f3140 = true;
                                    continue;
                                case R.string.contextpattlvl6 /*2131689723*/:
                                    r6.f3144 = true;
                                    continue;
                                case R.string.support_patch_billing_rebuild_dex /*2131690210*/:
                                    str = str + "newInappPatch_";
                                    continue;
                                case R.string.support_patch_billing_redirect_to_proxy /*2131690214*/:
                                    str = str + "pattern1_";
                                    continue;
                                case R.string.support_patch_billing_repatch_patched_app /*2131690216*/:
                                    str = str + "updatePatch_";
                                    continue;
                                case R.string.support_patch_lvl /*2131690222*/:
                                    str = str + "pattern0_";
                                    continue;
                            }
                        }
                    }
                    r6.f3150 = str;
                    r6.f3125 = true;
                    r6.f3142 = z;
                    r6.f3141 = file;
                    if (r11 == null) {
                        C0987 r72 = C0987.f4432;
                        C0987.m6055(C0987.f4440, r6);
                        return;
                    }
                    C0987.f4432.m6121(r6, z);
                }
            };
            if (!z) {
                if (r11 == null) {
                    str = C0815.m5205((int) R.string.patch_ok);
                } else {
                    str = C0815.m5205((int) R.string.multipatch_next_step);
                }
            } else if (r11 == null) {
                str = C0815.m5205((int) R.string.create_perm_ok);
            } else {
                str = C0815.m5205((int) R.string.multipatch_next_step);
            }
            r0.m5944(str, r2);
        }
        r0.m5947().show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lp.ʻ.ˊ.<init>(int, int, java.lang.String, boolean):void
     arg types: [?, ?, java.lang.String, int]
     candidates:
      com.lp.ʻ.ˊ.<init>(int, int, java.lang.String, int):void
      com.lp.ʻ.ˊ.<init>(int, int, java.lang.String, boolean):void */
    /* renamed from: ʼ  reason: contains not printable characters */
    public static void m5885(boolean z, final File file) {
        String str;
        C0959 r0 = new C0959(C0987.f4432.m6233());
        ArrayList arrayList = new ArrayList();
        arrayList.add(new C0952((int) R.string.create_clone_option2, (int) R.string.create_clone_option2_descr, "#ff00ff00", true));
        arrayList.add(new C0952((int) R.string.create_clone_option1, (int) R.string.create_clone_option1_descr, "#ff00ff00", false));
        final ArrayAdapter r1 = m5871(arrayList);
        if (r1 != null) {
            r1.setNotifyOnChange(true);
            r0.m5955(true);
            r0.m5941(r1, new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    C0952 r1 = (C0952) r1.getItem(i);
                    if (r1.f4130) {
                        r1.f4130 = false;
                    } else {
                        r1.f4130 = true;
                    }
                    if (r1.f4127 == R.string.create_clone_option2 && r1.f4130) {
                        C0947.m5872((int) R.string.create_clone_option1, r1).f4130 = false;
                    }
                    if (r1.f4127 == R.string.create_clone_option1 && r1.f4130) {
                        C0947.m5872((int) R.string.create_clone_option2, r1).f4130 = false;
                    }
                    r1.notifyDataSetChanged();
                }
            });
            AnonymousClass2 r2 = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    C0780 r5 = new C0780();
                    int count = r1.getCount();
                    String str = "1";
                    for (int i2 = 0; i2 < count; i2++) {
                        C0952 r2 = (C0952) r1.getItem(i2);
                        if (r2.f4130) {
                            int i3 = r2.f4127;
                            if (i3 == R.string.create_clone_option1) {
                                str = str + "onlyManifest_";
                            } else if (i3 == R.string.create_clone_option2) {
                                str = str + "DeepWork_";
                            }
                        }
                    }
                    r5.f3132 = str;
                    r5.f3129 = true;
                    r5.f3142 = true;
                    r5.f3141 = file;
                    C0987 r6 = C0987.f4432;
                    C0987.m6055(C0987.f4440, r5);
                }
            };
            if (!z) {
                str = C0815.m5205((int) R.string.patch_ok);
            } else {
                str = C0815.m5205((int) R.string.create_perm_ok);
            }
            r0.m5944(str, r2);
        }
        r0.m5947().show();
    }

    /* JADX WARNING: Removed duplicated region for block: B:59:0x02a3  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x02b2  */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void m5875(java.io.File r16, boolean r17, java.io.File r18, boolean r19, com.chelpus.ʻ.C0780 r20) {
        /*
            java.lang.String r1 = "-----------------------------------------------\n"
            java.lang.String r2 = "\n"
            java.lang.String r3 = "bold"
            com.lp.ﾞﾞ r0 = com.lp.C0987.f4432
            if (r0 == 0) goto L_0x02eb
            com.lp.ﾞﾞ r0 = com.lp.C0987.f4432
            android.support.v4.ʻ.ˊ r0 = r0.m6233()
            if (r0 != 0) goto L_0x0014
            goto L_0x02eb
        L_0x0014:
            java.lang.String r0 = "Custom patch dialog create."
            com.lp.C0987.m6060(r0)
            com.lp.ʼ r7 = new com.lp.ʼ
            com.lp.ﾞﾞ r0 = com.lp.C0987.f4432
            android.support.v4.ʻ.ˊ r0 = r0.m6233()
            r7.<init>(r0)
            java.lang.String r4 = ""
            r7.m5950(r4)
            r8 = 1
            android.widget.TextView r5 = r7.f4177     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.text.SpannableString r0 = com.chelpus.C0815.m5137(r1, r4, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.setText(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.<init>()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r6 = 2131689786(0x7f0f013a, float:1.9008597E38)
            java.lang.String r6 = com.chelpus.C0815.m5205(r6)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r6)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r2)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r0 = r0.toString()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r6 = -16711821(0xffffffffff00ff73, float:-1.7146755E38)
            android.text.SpannableString r0 = com.chelpus.C0815.m5136(r0, r6, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.text.SpannableString r0 = com.chelpus.C0815.m5137(r1, r4, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r0 = com.lp.C0987.f4473     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            boolean r0 = r0.equals(r4)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r9 = 2131690194(0x7f0f02d2, float:1.9009425E38)
            r10 = 2131690208(0x7f0f02e0, float:1.9009453E38)
            java.lang.String r11 = " "
            r12 = 0
            if (r0 == 0) goto L_0x00fd
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.<init>()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r2)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            com.lp.ᵔ r13 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r13 = r13.f4338     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r13)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r0 = r0.toString()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.text.SpannableString r0 = com.chelpus.C0815.m5136(r0, r6, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.<init>()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r2)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r10 = com.chelpus.C0815.m5205(r10)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r10)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r11)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r0 = r0.toString()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.text.SpannableString r0 = com.chelpus.C0815.m5136(r0, r6, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.content.pm.PackageManager r0 = com.lp.C0987.m6068()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            com.lp.ᵔ r10 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r10 = r10.f4337     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.content.pm.PackageInfo r0 = r0.getPackageInfo(r10, r12)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r0 = r0.versionName     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.text.SpannableString r0 = com.chelpus.C0815.m5136(r0, r6, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.<init>()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r2)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r9 = com.chelpus.C0815.m5205(r9)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r9)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r11)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r0 = r0.toString()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.text.SpannableString r0 = com.chelpus.C0815.m5136(r0, r6, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.<init>()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r4)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.content.pm.PackageManager r9 = com.lp.C0987.m6068()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            com.lp.ᵔ r10 = com.lp.C0987.f4440     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r10 = r10.f4337     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.content.pm.PackageInfo r9 = r9.getPackageInfo(r10, r12)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            int r9 = r9.versionCode     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r9)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r2)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r0 = r0.toString()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.text.SpannableString r0 = com.chelpus.C0815.m5136(r0, r6, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            goto L_0x0183
        L_0x00fd:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.<init>()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r2)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r13 = com.lp.C0987.f4473     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r13 = com.chelpus.C0815.m5297(r13)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r13)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r0 = r0.toString()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.text.SpannableString r0 = com.chelpus.C0815.m5136(r0, r6, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.<init>()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r2)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r10 = com.chelpus.C0815.m5205(r10)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r10)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r11)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r0 = r0.toString()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.text.SpannableString r0 = com.chelpus.C0815.m5136(r0, r6, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r0 = com.lp.C0987.f4473     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.content.pm.PackageInfo r0 = com.chelpus.C0815.m5292(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r0 = r0.versionName     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.text.SpannableString r0 = com.chelpus.C0815.m5136(r0, r6, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.<init>()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r2)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r9 = com.chelpus.C0815.m5205(r9)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r9)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r11)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r0 = r0.toString()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.text.SpannableString r0 = com.chelpus.C0815.m5136(r0, r6, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.<init>()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r4)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r9 = com.lp.C0987.f4473     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.content.pm.PackageInfo r9 = com.chelpus.C0815.m5292(r9)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            int r9 = r9.versionCode     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r9)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r2)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r0 = r0.toString()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.text.SpannableString r0 = com.chelpus.C0815.m5136(r0, r6, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
        L_0x0183:
            java.lang.String r0 = "\n-----------------------------------------------\n"
            android.text.SpannableString r0 = com.chelpus.C0815.m5137(r0, r4, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r0 = r16.toString()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.io.FileInputStream r6 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x024c, IOException -> 0x0237 }
            r6.<init>(r0)     // Catch:{ FileNotFoundException -> 0x024c, IOException -> 0x0237 }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ FileNotFoundException -> 0x024c, IOException -> 0x0237 }
            java.lang.String r9 = "UTF-8"
            r0.<init>(r6, r9)     // Catch:{ FileNotFoundException -> 0x024c, IOException -> 0x0237 }
            java.io.BufferedReader r9 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x024c, IOException -> 0x0237 }
            r9.<init>(r0)     // Catch:{ FileNotFoundException -> 0x024c, IOException -> 0x0237 }
            r0 = 2000(0x7d0, float:2.803E-42)
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ FileNotFoundException -> 0x024c, IOException -> 0x0237 }
            r11 = r4
            r10 = 0
            r13 = 0
        L_0x01a8:
            java.lang.String r14 = r9.readLine()     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            if (r14 == 0) goto L_0x0231
            r0[r12] = r14     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            if (r10 == 0) goto L_0x01e8
            r14 = r0[r12]     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            java.lang.String r15 = "["
            boolean r14 = r14.contains(r15)     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            if (r14 != 0) goto L_0x01d0
            r14 = r0[r12]     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            java.lang.String r15 = "]"
            boolean r14 = r14.contains(r15)     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            if (r14 != 0) goto L_0x01d0
            r14 = r0[r12]     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            java.lang.String r15 = "{"
            boolean r14 = r14.contains(r15)     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            if (r14 == 0) goto L_0x01e8
        L_0x01d0:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            r10.<init>()     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            r10.append(r4)     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            r10.append(r11)     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            r10.append(r2)     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            java.lang.String r10 = r10.toString()     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            com.lp.C0987.m6060(r10)     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            int r13 = r13 + 1
            r10 = 0
        L_0x01e8:
            if (r10 == 0) goto L_0x01fe
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            r14.<init>()     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            r14.append(r11)     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            r14.append(r2)     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            r15 = r0[r12]     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            r14.append(r15)     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            java.lang.String r11 = r14.toString()     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
        L_0x01fe:
            r14 = r0[r12]     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            java.lang.String r14 = r14.toUpperCase()     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            java.lang.String r15 = "[BEGIN]"
            boolean r14 = r14.contains(r15)     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            if (r14 == 0) goto L_0x01a8
            if (r13 <= 0) goto L_0x022e
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            r10.<init>()     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            r10.append(r11)     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            java.lang.String r14 = "\n-------- "
            r10.append(r14)     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            r14 = 2131690023(0x7f0f0227, float:1.9009078E38)
            java.lang.String r14 = com.chelpus.C0815.m5205(r14)     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            r10.append(r14)     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            java.lang.String r14 = " --------\n"
            r10.append(r14)     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            java.lang.String r11 = r10.toString()     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
        L_0x022e:
            r10 = 1
            goto L_0x01a8
        L_0x0231:
            r6.close()     // Catch:{ FileNotFoundException -> 0x024d, IOException -> 0x0235 }
            goto L_0x0252
        L_0x0235:
            r0 = move-exception
            goto L_0x0239
        L_0x0237:
            r0 = move-exception
            r11 = r4
        L_0x0239:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r6.<init>()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r6.append(r4)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r6.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r0 = r6.toString()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            com.lp.C0987.m6060(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            goto L_0x0252
        L_0x024c:
            r11 = r4
        L_0x024d:
            java.lang.String r0 = "Custom Patch not Found in\n/sdcard/LuckyPatcher/\n"
            com.lp.C0987.m6060(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
        L_0x0252:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.<init>()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r6 = 2131689761(0x7f0f0121, float:1.9008547E38)
            java.lang.String r6 = com.chelpus.C0815.m5205(r6)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r6)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0.append(r2)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            java.lang.String r0 = r0.toString()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r2 = -990142(0xfffffffffff0e442, float:NaN)
            android.text.SpannableString r0 = com.chelpus.C0815.m5136(r0, r2, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.text.SpannableString r0 = com.chelpus.C0815.m5137(r1, r4, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.text.SpannableString r0 = com.chelpus.C0815.m5136(r11, r2, r3)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.append(r0)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r0 = 3
            android.text.util.Linkify.addLinks(r5, r0)     // Catch:{ Throwable -> 0x0285 }
            goto L_0x028a
        L_0x0285:
            r0 = move-exception
            r1 = r0
            r1.printStackTrace()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
        L_0x028a:
            com.lp.ﾞﾞ r0 = com.lp.C0987.f4432     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            android.support.v4.ʻ.ˊ r0 = r0.m6233()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            int r1 = com.lp.C0987.m6076()     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            r5.setTextAppearance(r0, r1)     // Catch:{ NullPointerException -> 0x029d, NameNotFoundException -> 0x0298 }
            goto L_0x029e
        L_0x0298:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x029e
        L_0x029d:
        L_0x029e:
            r0 = 2131690016(0x7f0f0220, float:1.9009064E38)
            if (r17 != 0) goto L_0x02b2
            if (r20 != 0) goto L_0x02ad
            r0 = 2131690278(0x7f0f0326, float:1.9009595E38)
            java.lang.String r0 = com.chelpus.C0815.m5205(r0)
            goto L_0x02c0
        L_0x02ad:
            java.lang.String r0 = com.chelpus.C0815.m5205(r0)
            goto L_0x02c0
        L_0x02b2:
            if (r20 != 0) goto L_0x02bc
            r0 = 2131689749(0x7f0f0115, float:1.9008522E38)
            java.lang.String r0 = com.chelpus.C0815.m5205(r0)
            goto L_0x02c0
        L_0x02bc:
            java.lang.String r0 = com.chelpus.C0815.m5205(r0)
        L_0x02c0:
            com.lp.ʻ.ʾ$3 r9 = new com.lp.ʻ.ʾ$3
            r1 = r9
            r2 = r19
            r3 = r16
            r4 = r20
            r5 = r17
            r6 = r18
            r1.<init>(r2, r3, r4, r5, r6)
            com.lp.ʼ r0 = r7.m5944(r0, r9)
            r1 = 2131689814(0x7f0f0156, float:1.9008654E38)
            java.lang.String r1 = com.chelpus.C0815.m5205(r1)
            r2 = 0
            com.lp.ʼ r0 = r0.m5954(r1, r2)
            com.lp.ʼ r0 = r0.m5952(r8)
            android.app.Dialog r0 = r0.m5947()
            r0.show()
        L_0x02eb:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lp.ʻ.C0947.m5875(java.io.File, boolean, java.io.File, boolean, com.chelpus.ʻ.ʼ):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lp.ʼ.ʻ(com.lp.ʽ, android.widget.AdapterView$OnItemClickListener):com.lp.ʼ
     arg types: [com.lp.ʽ<com.lp.ᵎ>, com.lp.ʻ.ʾ$5]
     candidates:
      com.lp.ʼ.ʻ(int, android.content.DialogInterface$OnClickListener):com.lp.ʼ
      com.lp.ʼ.ʻ(android.widget.ArrayAdapter, android.widget.AdapterView$OnItemClickListener):com.lp.ʼ
      com.lp.ʼ.ʻ(java.lang.String, android.content.DialogInterface$OnClickListener):com.lp.ʼ
      com.lp.ʼ.ʻ(com.lp.ʽ, android.widget.AdapterView$OnItemClickListener):com.lp.ʼ */
    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5874(int i, boolean z, File file, C0780 r19) {
        File file2 = file;
        ArrayList arrayList = new ArrayList();
        C0987.m6060((Object) (BuildConfig.FLAVOR + file2));
        if (file2 == null) {
            PackageManager r10 = C0987.m6068();
            try {
                String[] strArr = r10.getPackageInfo(C0987.f4440.f4337, 4608).requestedPermissions;
                if (strArr != null) {
                    for (String str : strArr) {
                        if (str.startsWith("disabled_")) {
                            arrayList.add(new C0979("chelpa_per_" + str.replace("disabled_", BuildConfig.FLAVOR), false));
                        } else {
                            arrayList.add(new C0979(str, true));
                        }
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            } catch (NullPointerException e2) {
                e2.printStackTrace();
            }
            try {
                PackageInfo packageInfo = r10.getPackageInfo(C0987.f4440.f4337, 513);
                for (int i2 = 0; i2 < packageInfo.activities.length; i2++) {
                    if (!packageInfo.activities[i2].name.startsWith("disabled_")) {
                        arrayList.add(new C0979("chelpus_" + packageInfo.activities[i2].name, true));
                    }
                    if (packageInfo.activities[i2].name.startsWith("disabled_")) {
                        arrayList.add(new C0979("chelpus_" + packageInfo.activities[i2].name, false));
                    }
                }
            } catch (PackageManager.NameNotFoundException e3) {
                e3.printStackTrace();
            } catch (NullPointerException e4) {
                e4.printStackTrace();
            }
        } else {
            if (file.getName().toLowerCase().endsWith(".apks")) {
                file2 = C0815.m5293(file);
            }
            PackageManager r102 = C0987.m6068();
            PackageInfo packageArchiveInfo = r102.getPackageArchiveInfo(file2.getAbsolutePath(), 4608);
            if (packageArchiveInfo != null) {
                ApplicationInfo applicationInfo = packageArchiveInfo.applicationInfo;
                if (Build.VERSION.SDK_INT >= 8) {
                    applicationInfo.sourceDir = file2.getAbsolutePath();
                    applicationInfo.publicSourceDir = file2.getAbsolutePath();
                }
            }
            arrayList.clear();
            try {
                String[] strArr2 = packageArchiveInfo.requestedPermissions;
                C0987.m6060(strArr2);
                if (strArr2 != null) {
                    for (String str2 : strArr2) {
                        C0987.m6060((Object) str2);
                        if (str2.startsWith("disabled_")) {
                            arrayList.add(new C0979("chelpa_per_" + str2.replace("disabled_", BuildConfig.FLAVOR), false));
                        } else {
                            arrayList.add(new C0979(str2, true));
                        }
                    }
                }
            } catch (NullPointerException unused) {
            }
            PackageInfo packageArchiveInfo2 = r102.getPackageArchiveInfo(file2.getAbsolutePath(), 513);
            int i3 = 0;
            while (i3 < packageArchiveInfo2.activities.length) {
                try {
                    if (!packageArchiveInfo2.activities[i3].name.startsWith("disabled_")) {
                        arrayList.add(new C0979("chelpus_" + packageArchiveInfo2.activities[i3].name, true));
                    }
                    if (packageArchiveInfo2.activities[i3].name.startsWith("disabled_")) {
                        arrayList.add(new C0979("chelpus_" + packageArchiveInfo2.activities[i3].name, false));
                    }
                    i3++;
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
            }
        }
        if (arrayList.size() <= 0) {
            arrayList.add(new C0979(C0815.m5205((int) R.string.permission_not_found), false));
        }
        if (C0987.f4454 != null) {
            C0987.f4454.notifyDataSetChanged();
        }
        C0987.f4454 = new C0960<C0979>(C0987.f4432.m6233(), R.layout.contextmenu, arrayList) {
            public View getView(int i, View view, ViewGroup viewGroup) {
                View view2 = super.getView(i, view, viewGroup);
                TextView textView = (TextView) view2.findViewById(R.id.conttext);
                textView.setTextAppearance(getContext(), C0987.m6076());
                textView.setCompoundDrawablePadding((int) ((C0987.m6069().getDisplayMetrics().density * 5.0f) + 0.5f));
                String str = ((C0979) getItem(i)).f4335.replace("disabled_", BuildConfig.FLAVOR).replace("chelpa_per_", BuildConfig.FLAVOR).replace("chelpus_", BuildConfig.FLAVOR).replace("android.permission.", BuildConfig.FLAVOR).replace("com.android.vending.", BuildConfig.FLAVOR) + "\n";
                if (!((C0979) getItem(i)).f4336) {
                    textView.setText(C0815.m5137(str, "#ffff0000", "bold"));
                } else if (!((C0979) getItem(i)).f4336 || !((C0979) getItem(i)).f4335.contains("chelpus_")) {
                    textView.setText(C0815.m5137(str, "#ff00ff00", "bold"));
                } else {
                    textView.setText(C0815.m5137(str, "#ff00ffff", "bold"));
                    if (C0815.m5336(((C0979) getItem(i)).f4335)) {
                        textView.setText(C0815.m5137(str, "#ffffff00", "bold"));
                    }
                }
                PackageManager r2 = C0987.m6068();
                try {
                    String charSequence = r2.getPermissionInfo(((C0979) getItem(i)).f4335.replace("chelpa_per_", BuildConfig.FLAVOR).replace("chelpus_", BuildConfig.FLAVOR), 0).loadDescription(r2).toString();
                    if (charSequence == null) {
                        String r22 = C0815.m5205((int) R.string.permission_not_descr);
                        if (((C0979) getItem(i)).f4335.contains("chelpus_")) {
                            r22 = C0815.m5205((int) R.string.activity_descr);
                        }
                        if (((C0979) getItem(i)).f4335.contains("chelpus_") && C0815.m5336(((C0979) getItem(i)).f4335)) {
                            r22 = C0815.m5205((int) R.string.activity_ads_descr);
                        }
                        textView.append(C0815.m5137(r22, "#ff888888", "italic"));
                    } else {
                        textView.append(C0815.m5137(charSequence, "#ff888888", "italic"));
                    }
                } catch (PackageManager.NameNotFoundException unused) {
                    String r23 = C0815.m5205((int) R.string.permission_not_descr);
                    if (((C0979) getItem(i)).f4335.contains("chelpus_")) {
                        r23 = C0815.m5205((int) R.string.activity_descr);
                    }
                    if (((C0979) getItem(i)).f4335.contains("chelpus_") && C0815.m5336(((C0979) getItem(i)).f4335)) {
                        r23 = C0815.m5205((int) R.string.activity_ads_descr);
                    }
                    textView.append(C0815.m5137(r23, "#ff888888", "italic"));
                } catch (NullPointerException unused2) {
                    String r24 = C0815.m5205((int) R.string.permission_not_descr);
                    if (((C0979) getItem(i)).f4335.contains("chelpus_")) {
                        r24 = C0815.m5205((int) R.string.activity_descr);
                    }
                    if (((C0979) getItem(i)).f4335.contains("chelpus_") && C0815.m5336(((C0979) getItem(i)).f4335)) {
                        r24 = C0815.m5205((int) R.string.activity_ads_descr);
                    }
                    textView.append(C0815.m5137(r24, "#ff888888", "italic"));
                }
                return view2;
            }

            public Filter getFilter() {
                return new Filter() {
                    /* access modifiers changed from: protected */
                    public Filter.FilterResults performFiltering(CharSequence charSequence) {
                        Filter.FilterResults filterResults = new Filter.FilterResults();
                        ArrayList arrayList = new ArrayList();
                        if (AnonymousClass4.this.f4207 == null) {
                            AnonymousClass4 r2 = AnonymousClass4.this;
                            r2.f4207 = r2.f4206;
                        }
                        if (charSequence != null) {
                            if (AnonymousClass4.this.f4207 != null && AnonymousClass4.this.f4207.size() > 0) {
                                for (C0979 r3 : AnonymousClass4.this.f4207) {
                                    if (r3.f4335.toLowerCase().contains(charSequence.toString().toLowerCase())) {
                                        arrayList.add(r3);
                                    }
                                }
                            }
                            arrayList.toArray(new C0979[arrayList.size()]);
                            filterResults.values = arrayList;
                        }
                        return filterResults;
                    }

                    /* access modifiers changed from: protected */
                    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
                        AnonymousClass4.this.f4206 = (List) filterResults.values;
                        AnonymousClass4.this.notifyDataSetChanged();
                    }
                };
            }
        };
        C0959 r0 = new C0959(C0987.f4432.m6233());
        if (C0987.f4454 != null) {
            C0987.f4454.setNotifyOnChange(true);
            r0.m5955(true);
            r0.m5942((C0960) C0987.f4454, (AdapterView.OnItemClickListener) new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    C0979 item = C0987.f4454.getItem(i);
                    if (item.f4336) {
                        item.f4336 = false;
                    } else {
                        item.f4336 = true;
                    }
                    C0987.f4454.notifyDataSetChanged();
                }
            });
            final int i4 = i;
            final boolean z2 = z;
            final C0780 r5 = r19;
            r0.m5944(C0815.m5205((int) R.string.create_perm_ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    ArrayList<String> arrayList = new ArrayList<>();
                    ArrayList<String> arrayList2 = new ArrayList<>();
                    for (int i2 = 0; i2 < C0987.f4454.f4207.size(); i2++) {
                        C0979 r1 = (C0979) C0987.f4454.f4207.get(i2);
                        if (!r1.f4336 && !r1.f4335.contains("chelpus_")) {
                            arrayList.add(r1.f4335);
                        }
                        if (r1.f4336 && r1.f4335.startsWith("chelpa_per_")) {
                            arrayList.add(r1.f4335.replace("chelpa_per_", BuildConfig.FLAVOR));
                            C0987.m6060((Object) r1.f4335.replace("chelpa_per_", BuildConfig.FLAVOR));
                        }
                        if (!r1.f4336 && r1.f4335.contains("chelpus_") && !r1.f4335.contains("disabled_")) {
                            C0987.m6060((Object) ("add activity to list:" + r1.f4335));
                            arrayList2.add(r1.f4335.replace("chelpus_", BuildConfig.FLAVOR));
                            C0987.m6060((Object) (BuildConfig.FLAVOR + r1.f4335.replace("chelpus_", BuildConfig.FLAVOR)));
                        }
                        if (r1.f4336 && r1.f4335.contains("chelpus_disabled_")) {
                            arrayList2.add(r1.f4335.replace("chelpus_", BuildConfig.FLAVOR));
                            C0987.m6060((Object) (BuildConfig.FLAVOR + r1.f4335.replace("chelpus_", BuildConfig.FLAVOR)));
                        }
                    }
                    C0780 r0 = new C0780();
                    C0780 r12 = r5;
                    if (r12 != null) {
                        r0 = r12;
                    }
                    r0.f3148 = arrayList;
                    r0.f3158 = arrayList2;
                    if (i4 == R.string.context_crt_apk_permission) {
                        r0.f3131 = true;
                        r0.f3142 = true;
                    }
                    if (i4 == R.string.context_safe_sign_apk_permission) {
                        r0.f3133 = true;
                        r0.f3142 = true;
                    }
                    if (!C0987.f4473.equals(BuildConfig.FLAVOR)) {
                        r0.f3141 = new File(C0987.f4473);
                    }
                    if (r5 != null) {
                        C0987.f4432.m6121(r0, z2);
                        return;
                    }
                    C0987 r7 = C0987.f4432;
                    C0987.m6055(C0987.f4440, r0);
                }
            });
            r0.m5947().show();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lp.ʻ.ʾ.ʼ(java.util.ArrayList<com.lp.ʻ.ˊ>, boolean):android.widget.ArrayAdapter
     arg types: [java.util.ArrayList, int]
     candidates:
      com.lp.ʻ.ʾ.ʼ(boolean, java.io.File):void
      com.lp.ʻ.ʾ.ʼ(java.util.ArrayList<com.lp.ʻ.ˊ>, boolean):android.widget.ArrayAdapter */
    /* renamed from: ʽ  reason: contains not printable characters */
    public static void m5887(boolean z, File file) {
        String str;
        final boolean z2 = z;
        final File file2 = file;
        C0959 r3 = new C0959(C0987.f4432.m6233());
        ArrayList arrayList = new ArrayList();
        String str2 = "try_get_pkg_name";
        if (file2 != null) {
            try {
                if (file.exists() && file.canRead()) {
                    str2 = C0815.m5292(file.getAbsolutePath()).packageName;
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        } else {
            str2 = C0987.f4440.f4337;
        }
        if (C0815.m5272(str2)) {
            arrayList.add(new C0952(R.string.contextcustompatch, 0, "#ffff99", false, R.drawable.context_custom_patch));
        }
        arrayList.add(new C0952(R.string.contextignore, 0, "#ff00ff00", false, R.drawable.context_lvl));
        arrayList.add(new C0952(R.string.contextads, 0, "#99cccc", false, R.drawable.context_ads));
        arrayList.add(new C0952(R.string.context_support_patch, 0, "#ff00ff00", false, R.drawable.context_lvl));
        if (z2) {
            arrayList.add(new C0952(R.string.context_crt_apk_permission, 0, "#cc99cc", false, R.drawable.context_permissions));
        }
        final ArrayAdapter r4 = m5884((ArrayList<C0952>) arrayList, true);
        if (r4 != null) {
            r4.setNotifyOnChange(true);
            r3.m5955(true);
            r3.m5941(r4, new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    C0952 r1 = (C0952) r4.getItem(i);
                    if (r1.f4130) {
                        r1.f4130 = false;
                    } else {
                        r1.f4130 = true;
                    }
                    r4.notifyDataSetChanged();
                }
            });
            AnonymousClass8 r0 = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    C0780 r4 = new C0780();
                    r4.f3142 = z2;
                    r4.f3141 = file2;
                    int count = r4.getCount();
                    for (int i2 = 0; i2 < count; i2++) {
                        C0952 r1 = (C0952) r4.getItem(i2);
                        if (r1.f4130) {
                            switch (r1.f4127) {
                                case R.string.context_crt_apk_permission /*2131689617*/:
                                    r4.f3131 = true;
                                    r4.f3137 = true;
                                    continue;
                                case R.string.context_support_patch /*2131689648*/:
                                    r4.f3125 = true;
                                    r4.f3137 = true;
                                    continue;
                                case R.string.contextads /*2131689656*/:
                                    r4.f3121 = true;
                                    r4.f3137 = true;
                                    continue;
                                case R.string.contextcustompatch /*2131689677*/:
                                    r4.f3127 = true;
                                    r4.f3137 = true;
                                    continue;
                                case R.string.contextignore /*2131689681*/:
                                    r4.f3123 = true;
                                    r4.f3137 = true;
                                    continue;
                            }
                        }
                    }
                    r4.f3141 = file2;
                    C0987.f4432.m6121(r4, z2);
                }
            };
            if (!z2) {
                str = C0815.m5205((int) R.string.patch_ok);
            } else {
                str = C0815.m5205((int) R.string.create_perm_ok);
            }
            r3.m5944(str, r0);
        }
        r3.m5947().show();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5881(final ArrayList<C0952> arrayList, boolean z) {
        if (C0987.f4432 != null) {
            C0959 r0 = new C0959(C0987.f4432.m6233());
            ArrayAdapter r3 = m5884(arrayList, z);
            if (r3 != null) {
                r3.setNotifyOnChange(true);
                r0.m5955(false);
                r0.m5941(r3, new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                        if (C0987.f4432 != null) {
                            C0987.f4432.m6207(((C0952) arrayList.get(i)).f4127);
                        }
                    }
                });
            }
            r0.m5947().show();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static ArrayAdapter m5871(ArrayList<C0952> arrayList) {
        return new ArrayAdapter<C0952>(C0987.f4432.m6233(), R.layout.contextmenu, arrayList) {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
             arg types: [?, android.view.ViewGroup, int]
             candidates:
              ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
              ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
            public View getView(int i, View view, ViewGroup viewGroup) {
                C0952 r11 = (C0952) getItem(i);
                View inflate = ((LayoutInflater) C0987.m6072().getSystemService("layout_inflater")).inflate((int) R.layout.selectpatch, viewGroup, false);
                TextView textView = (TextView) inflate.findViewById(R.id.txtTitle);
                TextView textView2 = (TextView) inflate.findViewById(R.id.txtStatus);
                CheckBox checkBox = (CheckBox) inflate.findViewById(R.id.CheckBox1);
                if (((C0952) getItem(i)).f4131) {
                    textView.setTextAppearance(getContext(), C0987.m6076());
                    textView.setTextColor(-1);
                    textView.setText(C0815.m5137(C0815.m5205(r11.f4127), r11.f4129, "bold"));
                    textView.setTypeface(null, 1);
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) textView.getLayoutParams();
                    layoutParams.setMargins(10, 5, 0, 5);
                    textView.setLayoutParams(layoutParams);
                    inflate.setBackgroundColor(-12303292);
                    checkBox.setVisibility(8);
                    textView2.setVisibility(8);
                } else {
                    textView.setTextAppearance(getContext(), C0987.m6076());
                    textView2.setTextAppearance(getContext(), C0987.m6076());
                    checkBox.setChecked(r11.f4130);
                    checkBox.setClickable(false);
                    textView2.setTextAppearance(getContext(), 16973894);
                    textView2.setTextColor(-7829368);
                    textView.setTextColor(-1);
                    textView.setText(C0815.m5137(C0815.m5205(r11.f4127), r11.f4129, "bold"));
                    textView.setTypeface(null, 1);
                    if (r11.f4128 != 0) {
                        textView2.append(C0815.m5137(C0815.m5205(r11.f4128), "#ff888888", "italic"));
                        textView2.setVisibility(0);
                    }
                }
                return inflate;
            }
        };
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static ArrayAdapter m5884(ArrayList<C0952> arrayList, final boolean z) {
        return new ArrayAdapter<C0952>(C0987.f4432.m6233(), R.layout.icon_context_menu, arrayList) {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
             arg types: [?, android.view.ViewGroup, int]
             candidates:
              ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
              ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
            public View getView(int i, View view, ViewGroup viewGroup) {
                if (view == null) {
                    view = ((LayoutInflater) C0987.m6072().getSystemService("layout_inflater")).inflate((int) R.layout.icon_context_menu, viewGroup, false);
                }
                C0952 r6 = (C0952) getItem(i);
                TextView textView = (TextView) view.findViewById(R.id.conttext);
                ImageView imageView = (ImageView) view.findViewById(R.id.imgIcon);
                imageView.setImageDrawable(null);
                textView.setTextAppearance(getContext(), C0987.m6076());
                textView.setTextColor(-1);
                if (r6.f4132 != null) {
                    imageView.setImageDrawable(r6.f4132);
                    imageView.setColorFilter(Color.parseColor(r6.f4129), PorterDuff.Mode.MULTIPLY);
                } else {
                    imageView.setVisibility(8);
                    textView.setGravity(17);
                }
                if (z) {
                    CheckBox checkBox = (CheckBox) view.findViewById(R.id.CheckBox1);
                    checkBox.setVisibility(0);
                    checkBox.setChecked(r6.f4130);
                    checkBox.setClickable(false);
                }
                textView.setCompoundDrawablePadding((int) ((C0987.m6069().getDisplayMetrics().density * 5.0f) + 0.5f));
                textView.setTextColor(Color.parseColor(r6.f4129));
                textView.setText(C0815.m5205(r6.f4127));
                if (r6.f4128 != 0) {
                    textView.append(C0815.m5137("\n" + C0815.m5205(r6.f4128), "#CCCCCC", BuildConfig.FLAVOR));
                }
                textView.setTypeface(null, 1);
                return view;
            }
        };
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0952 m5872(int i, ArrayAdapter<C0952> arrayAdapter) {
        for (int i2 = 0; i2 < arrayAdapter.getCount(); i2++) {
            if (arrayAdapter.getItem(i2).f4127 == i) {
                return arrayAdapter.getItem(i2);
            }
        }
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5878(final String str, final String str2, final int i, final boolean z) {
        if (C0987.f4469 == null || !C0987.f4469.m5933()) {
            C0987.m6061((Runnable) new Runnable() {
                public void run() {
                    C0987.m6084((Integer) 23);
                    C0987.f4469.m5925(str2);
                    C0987.f4469.m5926(z);
                    C0987.f4469.m5924(i);
                    C0987.f4469.m5932(str);
                    C0987.f4469.m5927(false, C0987.f4432.m6233());
                }
            });
            return;
        }
        C0987.f4469.m5925(str2);
        C0987.f4469.m5926(z);
        C0987.f4469.m5924(i);
        C0987.f4469.m5932(str);
        C0987.f4469.m5927(false, C0987.f4432.m6233());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5879(String str, String str2, String str3, int i, int i2, boolean z) {
        if (C0987.f4469 == null || !C0987.f4469.m5933()) {
            final String str4 = str2;
            final boolean z2 = z;
            final int i3 = i;
            final int i4 = i2;
            final String str5 = str;
            final String str6 = str3;
            C0987.m6061((Runnable) new Runnable() {
                public void run() {
                    C0987.m6094((Integer) 23);
                    C0987.m6084((Integer) 23);
                    if (C0987.f4469 != null && C0987.f4469.m5933()) {
                        C0987.f4469.m5925(str4);
                        C0987.f4469.m5926(z2);
                        C0987.f4469.m5929(i3);
                        C0987.f4469.m5924(i4);
                        C0987.f4469.m5932(str5);
                        C0987.f4469.m5927(false, C0987.f4432.m6233());
                        C0987.f4469.m5930(str6);
                    }
                }
            });
            return;
        }
        C0987.f4469.m5925(str2);
        C0987.f4469.m5926(z);
        C0987.f4469.m5929(i);
        C0987.f4469.m5924(i2);
        C0987.f4469.m5932(str);
        C0987.f4469.m5927(false, C0987.f4432.m6233());
        C0987.f4469.m5930(str3);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5877(String str) {
        if (C0987.f4469 != null && C0987.f4469.m5933()) {
            C0987.f4469.m5925(str);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5876(Integer num) {
        if (C0987.f4469 != null && C0987.f4469.m5933()) {
            C0987.f4469.m5929(num.intValue());
            C0987.f4469.m5927(false, C0987.f4432.m6233());
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5873() {
        if (C0987.f4432 != null) {
            C0987 r0 = C0987.f4432;
            C0987.m6061((Runnable) new Runnable() {
                public void run() {
                    C0987.m6094((Integer) 23);
                }
            });
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5880(Throwable th, boolean z) {
        if (z || th.toString().contains("No space left on device") || th.toString().contains("ENOSPC")) {
            C0987.m6061((Runnable) new Runnable() {
                public void run() {
                    if (C0987.f4432 != null) {
                        C0987 r0 = C0987.f4432;
                        C0987.m6062(C0815.m5205((int) R.string.error), C0815.m5205((int) R.string.warning_for_no_free_space_on_device_to_patch));
                    }
                }
            });
        }
    }
}
