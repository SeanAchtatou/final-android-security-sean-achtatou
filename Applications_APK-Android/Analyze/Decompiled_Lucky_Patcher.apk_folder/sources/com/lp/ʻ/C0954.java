package com.lp.ʻ;

import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;
import com.lp.C0959;
import com.lp.C0987;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ʻ.ˎ  reason: contains not printable characters */
/* compiled from: Menu_Dialog_Static */
public class C0954 {

    /* renamed from: ʻ  reason: contains not printable characters */
    Dialog f4136 = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5904() {
        if (this.f4136 == null) {
            this.f4136 = m5905();
        }
        Dialog dialog = this.f4136;
        if (dialog != null) {
            dialog.show();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Dialog m5905() {
        try {
            C0987.m6060((Object) "Menu Dialog create.");
            if (C0987.f4432 == null || C0987.f4432.m6233() == null) {
                m5906();
            }
            C0959 r0 = new C0959(C0987.f4432.m6233());
            r0.m5937((int) R.string.set_switchers_def, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    C0987.f4432.m6207((int) R.string.set_switchers_def);
                }
            });
            if (C0987.f4453 != null) {
                C0987.f4453.setNotifyOnChange(true);
                r0.m5955(true);
                r0.m5941(C0987.f4453, new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                        try {
                            C0987.f4432.m6207(((Integer) C0987.f4453.getItem(i)).intValue());
                            C0987.f4453.notifyDataSetChanged();
                        } catch (Exception e) {
                            C0987.m6060((Object) ("LuckyPatcher (ContextMenu): Error open! " + e));
                            e.printStackTrace();
                        }
                    }
                });
            }
            r0.m5938(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialogInterface) {
                    C0987.f4473 = BuildConfig.FLAVOR;
                    C0987.m6060((Object) C0987.f4473);
                }
            });
            return r0.m5947();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m5906() {
        Dialog dialog = this.f4136;
        if (dialog != null) {
            dialog.dismiss();
            this.f4136 = null;
        }
    }
}
