package com.lp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import com.chelpus.C0815;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ˊ  reason: contains not printable characters */
/* compiled from: DatabaseHelper */
public class C0967 extends SQLiteOpenHelper {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Context f4261 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    public static SQLiteDatabase f4262 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    public static boolean f4263 = false;

    /* renamed from: ʾ  reason: contains not printable characters */
    public static boolean f4264 = false;

    public C0967(Context context) {
        super(context, "PackagesDB", (SQLiteDatabase.CursorFactory) null, 43);
        f4261 = context;
        try {
            f4262 = getWritableDatabase();
            C0987.m6060((Object) ("SQLite base version is " + f4262.getVersion()));
        } catch (SQLiteException e) {
            e.printStackTrace();
            if (C0987.f4432 != null) {
                C0987 r4 = C0987.f4432;
                C0987.m6062(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.warning_internal_data_folder_corrupt));
            }
            C0987.m6060((Object) "LP: Delete bad database.");
        }
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        C0987.m6060((Object) ("LuckyPatcher: --- onUpgrade database from " + i + " to " + i2 + " version --- "));
        if (i == 42 && i2 == 43) {
            try {
                sQLiteDatabase.execSQL("alter table Packages add column install_dir TEXT;");
                C0987.m6060((Object) "LuckyPatcher:col added for db ver.43");
            } catch (Throwable th) {
                th.printStackTrace();
            }
        } else {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS Packages");
            onCreate(sQLiteDatabase);
        }
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE Packages (pkgName TEXT PRIMARY KEY, pkgLabel TEXT, stored Integer, storepref Integer, hidden Integer, statusi TEXT, boot_ads Integer, boot_lvl Integer, boot_custom Integer, boot_manual Integer, custom Integer, lvl Integer, ads Integer, modified Integer, system Integer, odex Integer, icon BLOB, updatetime Integer, billing Integer, install_dir TEXT);");
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS Packages");
        onCreate(sQLiteDatabase);
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:418:0x0790 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:88:0x0331 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:386:? */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:388:? */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: java.lang.String[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r18v17, resolved type: java.lang.String[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v4, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v6, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v7, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v9, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v10, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v11, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v12, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v13, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v15, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v16, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v17, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v18, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v19, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v20, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v22, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v23, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v33, resolved type: boolean} */
    /* JADX WARN: Type inference failed for: r4v37 */
    /* JADX WARN: Type inference failed for: r4v38 */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:(34:3|4|5|6|7|8|9|10|11|12|13|14|15|16|(1:18)(1:19)|20|(1:22)(1:23)|24|25|(4:27|28|29|(19:31|32|33|44|(2:46|47)(1:48)|49|(9:51|52|53|(3:55|56|57)|58|59|60|61|62)(3:67|68|(8:70|71|72|73|74|75|76|77)(1:86))|(3:88|89|(14:91|92|93|94|100|101|(4:103|104|105|(1:221)(18:109|(2:112|(1:114)(3:472|115|116))|117|(2:127|(1:129)(3:476|130|131))|132|(2:142|(1:144)(3:481|145|146))|147|(2:159|(1:161)(3:449|162|163))|164|(2:176|(1:178)(3:453|179|180))|181|(2:187|(1:189)(3:455|190|191))|192|(2:196|(1:198)(3:458|199|200))|201|(2:205|(1:207)(3:461|208|209))|210|(2:214|(1:219)(1:218))))(2:223|(1:382)(8:227|(5:229|230|231|(2:233|(2:235|236)(1:361))(2:362|(3:460|365|366))|364)(1:378)|379|380|381|422|423|(0)(0)))|238|239|380|381|422|423|(0)(0)))|99|100|101|(0)(0)|238|239|380|381|422|423|(0)(0)))|35|44|(0)(0)|49|(0)(0)|(0)|99|100|101|(0)(0)|238|239|380|381)|422|423|(0)(0)) */
    /* JADX WARNING: Code restructure failed: missing block: B:220:0x04d7, code lost:
        throw new com.lp.C0983("package scan error");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:222:0x04dd, code lost:
        throw new com.lp.C0983("package scan error");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:264:0x0569, code lost:
        throw new com.lp.C0983("package scan error");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:331:0x063b, code lost:
        throw new com.lp.C0983("package scan error");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:360:0x0691, code lost:
        throw new com.lp.C0983("package scan error");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0198, code lost:
        r40 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:383:0x06cb, code lost:
        throw new com.lp.C0983("package scan error");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:398:0x06ef, code lost:
        r46 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:402:0x06f6, code lost:
        r46 = r2;
        r40 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:404:0x06fa, code lost:
        r44 = r8;
        r52 = r9;
        r53 = r10;
        r54 = r11;
        r55 = r12;
        r56 = r13;
        r57 = r14;
        r38 = r16;
        r6 = r18;
        r1 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x019e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:412:0x075c, code lost:
        r46 = r2;
        r40 = r3;
        r1 = r5;
        r44 = r8;
        r52 = r9;
        r53 = r10;
        r54 = r11;
        r55 = r12;
        r56 = r13;
        r57 = r14;
        r38 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:415:0x0776, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:416:0x0777, code lost:
        r46 = r2;
        r40 = r3;
        r1 = r5;
        r44 = r8;
        r52 = r9;
        r53 = r10;
        r54 = r11;
        r55 = r12;
        r56 = r13;
        r57 = r14;
        r38 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x019f, code lost:
        r40 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:428:?, code lost:
        r46.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:443:0x0836, code lost:
        r0 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:422:0x07aa */
    /* JADX WARNING: Missing exception handler attribute for start block: B:427:0x07d0 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x035b  */
    /* JADX WARNING: Removed duplicated region for block: B:223:0x04de A[Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:399:? A[ExcHandler: NameNotFoundException | ﹳ (unused java.lang.Throwable), PHI: r40 
      PHI: (r40v16 java.util.HashSet) = (r40v17 java.util.HashSet), (r40v17 java.util.HashSet), (r40v19 java.util.HashSet) binds: [B:55:0x01dc, B:56:?, B:42:0x01a1] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:42:0x01a1] */
    /* JADX WARNING: Removed duplicated region for block: B:403:? A[ExcHandler: NameNotFoundException | ﹳ | Exception (unused java.lang.Throwable), SYNTHETIC, Splitter:B:15:0x00bf] */
    /* JADX WARNING: Removed duplicated region for block: B:413:? A[ExcHandler: Exception (unused java.lang.Exception), SYNTHETIC, Splitter:B:6:0x00ad] */
    /* JADX WARNING: Removed duplicated region for block: B:426:0x07b4 A[LOOP:0: B:3:0x00a7->B:426:0x07b4, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01b4 A[Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }] */
    /* JADX WARNING: Removed duplicated region for block: B:478:0x07b0 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01c1  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01ca A[SYNTHETIC, Splitter:B:51:0x01ca] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x028b A[Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06de, IllegalArgumentException -> 0x06e6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0331 A[SYNTHETIC, Splitter:B:88:0x0331] */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<com.lp.C0980> m5985(boolean r59, boolean r60, boolean r61) {
        /*
            r58 = this;
            r1 = r58
            r15 = r60
            java.lang.String r14 = "boot_lvl"
            java.lang.String r13 = "boot_ads"
            java.lang.String r12 = "statusi"
            java.lang.String r11 = "hidden"
            java.lang.String r10 = "storepref"
            java.lang.String r9 = "stored"
            java.lang.String r8 = "pkgLabel"
            java.lang.String r7 = "pkgName"
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            r6.clear()
            android.content.pm.PackageManager r5 = com.lp.C0987.m6068()
            r4 = 1
            com.lp.C0967.f4263 = r4
            java.util.HashSet r3 = new java.util.HashSet
            r3.<init>()
            r2 = 0
            android.database.sqlite.SQLiteDatabase r16 = com.lp.C0967.f4262     // Catch:{ Exception -> 0x0838 }
            java.lang.String r17 = "Packages"
            r0 = 20
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x0838 }
            r0[r2] = r7     // Catch:{ Exception -> 0x0838 }
            r0[r4] = r8     // Catch:{ Exception -> 0x0838 }
            r4 = 2
            r0[r4] = r9     // Catch:{ Exception -> 0x0838 }
            r18 = 3
            r0[r18] = r10     // Catch:{ Exception -> 0x0838 }
            r18 = 4
            r0[r18] = r11     // Catch:{ Exception -> 0x0838 }
            r18 = 5
            r0[r18] = r12     // Catch:{ Exception -> 0x0838 }
            r18 = 6
            r0[r18] = r13     // Catch:{ Exception -> 0x0838 }
            r18 = 7
            r0[r18] = r14     // Catch:{ Exception -> 0x0838 }
            r18 = 8
            java.lang.String r19 = "boot_custom"
            r0[r18] = r19     // Catch:{ Exception -> 0x0838 }
            r18 = 9
            java.lang.String r19 = "boot_manual"
            r0[r18] = r19     // Catch:{ Exception -> 0x0838 }
            r18 = 10
            java.lang.String r19 = "custom"
            r0[r18] = r19     // Catch:{ Exception -> 0x0838 }
            r18 = 11
            java.lang.String r19 = "lvl"
            r0[r18] = r19     // Catch:{ Exception -> 0x0838 }
            r18 = 12
            java.lang.String r19 = "ads"
            r0[r18] = r19     // Catch:{ Exception -> 0x0838 }
            r18 = 13
            java.lang.String r19 = "modified"
            r0[r18] = r19     // Catch:{ Exception -> 0x0838 }
            r18 = 14
            java.lang.String r19 = "system"
            r0[r18] = r19     // Catch:{ Exception -> 0x0838 }
            r18 = 15
            java.lang.String r19 = "odex"
            r0[r18] = r19     // Catch:{ Exception -> 0x0838 }
            r18 = 16
            java.lang.String r19 = "icon"
            r0[r18] = r19     // Catch:{ Exception -> 0x0838 }
            r18 = 17
            java.lang.String r19 = "updatetime"
            r0[r18] = r19     // Catch:{ Exception -> 0x0838 }
            r18 = 18
            java.lang.String r19 = "billing"
            r0[r18] = r19     // Catch:{ Exception -> 0x0838 }
            r18 = 19
            java.lang.String r19 = "install_dir"
            r0[r18] = r19     // Catch:{ Exception -> 0x0838 }
            r19 = 0
            r20 = 0
            r21 = 0
            r22 = 0
            r23 = 0
            r18 = r0
            android.database.Cursor r2 = r16.query(r17, r18, r19, r20, r21, r22, r23)     // Catch:{ Exception -> 0x0838 }
            r2.moveToFirst()     // Catch:{ Exception -> 0x0838 }
        L_0x00a7:
            int r0 = r2.getColumnIndexOrThrow(r7)     // Catch:{ Exception -> 0x0794 }
            r16 = r7
            java.lang.String r7 = r2.getString(r0)     // Catch:{ Exception -> 0x075c }
            r3.add(r7)     // Catch:{ Exception -> 0x075c }
            r4 = 0
            android.content.pm.PackageInfo r0 = r5.getPackageInfo(r7, r4)     // Catch:{ IllegalArgumentException -> 0x0776, Exception -> 0x075c, Exception -> 0x075c }
            r18 = r6
            android.content.pm.ApplicationInfo r6 = r0.applicationInfo     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x072b, IllegalArgumentException -> 0x0743 }
            r19 = r5
            boolean r5 = r6.enabled     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r0 = r6.flags     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            r20 = 262144(0x40000, float:3.67342E-40)
            r0 = r0 & r20
            r4 = 262144(0x40000, float:3.67342E-40)
            if (r0 != r4) goto L_0x00ce
            r26 = 1
            goto L_0x00d0
        L_0x00ce:
            r26 = 0
        L_0x00d0:
            java.lang.String r4 = r6.sourceDir     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r0 = r2.getColumnIndex(r8)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            java.lang.String r20 = r2.getString(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r0 = r2.getColumnIndex(r9)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r0 = r2.getInt(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            r21 = r5
            r5 = 1
            if (r0 != r5) goto L_0x00ea
            r22 = 1
            goto L_0x00ec
        L_0x00ea:
            r22 = 0
        L_0x00ec:
            int r0 = r2.getColumnIndex(r10)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            r2.getInt(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r0 = r2.getColumnIndex(r11)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r23 = r2.getInt(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r0 = r2.getColumnIndex(r12)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            java.lang.String r24 = r2.getString(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r0 = r2.getColumnIndex(r13)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r27 = r2.getInt(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r0 = r2.getColumnIndex(r14)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r28 = r2.getInt(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            java.lang.String r0 = "boot_custom"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r29 = r2.getInt(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            java.lang.String r0 = "boot_manual"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r30 = r2.getInt(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            java.lang.String r0 = "custom"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r31 = r2.getInt(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            java.lang.String r0 = "lvl"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r32 = r2.getInt(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            java.lang.String r0 = "ads"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r33 = r2.getInt(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            java.lang.String r0 = "modified"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r34 = r2.getInt(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            java.lang.String r0 = "system"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r35 = r2.getInt(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            java.lang.String r0 = "odex"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r36 = r2.getInt(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            java.lang.String r0 = "billing"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            int r37 = r2.getInt(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            java.lang.String r0 = "install_dir"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            java.lang.String r5 = r2.getString(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06f6, IllegalArgumentException -> 0x0710 }
            r39 = 0
            if (r15 != 0) goto L_0x0195
            java.lang.String r0 = "icon"
            int r0 = r2.getColumnIndexOrThrow(r0)     // Catch:{ Exception -> 0x019e, OutOfMemoryError -> 0x0198, NameNotFoundException | ﹳ | Exception -> 0x06f6 }
            byte[] r0 = r2.getBlob(r0)     // Catch:{ Exception -> 0x019e, OutOfMemoryError -> 0x0198, NameNotFoundException | ﹳ | Exception -> 0x06f6 }
            if (r0 == 0) goto L_0x0195
            r40 = r3
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0193, OutOfMemoryError -> 0x019a }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0193, OutOfMemoryError -> 0x019a }
            android.graphics.Bitmap r39 = android.graphics.BitmapFactory.decodeStream(r3)     // Catch:{ Exception -> 0x0193, OutOfMemoryError -> 0x019a }
            goto L_0x01a4
        L_0x0193:
            r0 = move-exception
            goto L_0x01a1
        L_0x0195:
            r40 = r3
            goto L_0x01a4
        L_0x0198:
            r40 = r3
        L_0x019a:
            r0 = 0
            r39 = r0
            goto L_0x01a4
        L_0x019e:
            r0 = move-exception
            r40 = r3
        L_0x01a1:
            r0.printStackTrace()     // Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }
        L_0x01a4:
            java.lang.String r0 = "updatetime"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }
            int r0 = r2.getInt(r0)     // Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }
            boolean r3 = r4.equals(r5)     // Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }
            if (r3 != 0) goto L_0x01c1
            com.lp.ᵔ r3 = new com.lp.ᵔ     // Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }
            int r5 = com.lp.C0987.f4490     // Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }
            r3.<init>(r7, r5, r15)     // Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }
            r1.m5986(r3)     // Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }
            r41 = r4
            goto L_0x01c3
        L_0x01c1:
            r41 = r5
        L_0x01c3:
            r5 = 86400(0x15180, float:1.21072E-40)
            r42 = 1000(0x3e8, double:4.94E-321)
            if (r22 != 0) goto L_0x028b
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x0256, IllegalArgumentException -> 0x0270 }
            long r3 = r3 / r42
            int r4 = (int) r3     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x0256, IllegalArgumentException -> 0x0270 }
            int r4 = r4 - r0
            int r3 = java.lang.Math.abs(r4)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x0256, IllegalArgumentException -> 0x0270 }
            int r4 = com.lp.C0987.f4490     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x0256, IllegalArgumentException -> 0x0270 }
            int r4 = r4 * r5
            if (r3 >= r4) goto L_0x01f9
            android.database.sqlite.SQLiteDatabase r3 = com.lp.C0967.f4262     // Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }
            r4.<init>()     // Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }
            java.lang.String r5 = "UPDATE Packages SET stored=0 WHERE pkgName='"
            r4.append(r5)     // Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }
            r4.append(r7)     // Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }
            java.lang.String r5 = "'"
            r4.append(r5)     // Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }
            java.lang.String r4 = r4.toString()     // Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }
            r3.execSQL(r4)     // Catch:{ NameNotFoundException | ﹳ -> 0x06ef, IllegalArgumentException -> 0x06f2, NameNotFoundException | ﹳ -> 0x06ef, NameNotFoundException | ﹳ -> 0x06ef }
            r22 = 1
        L_0x01f9:
            com.lp.ᵔ r45 = new com.lp.ᵔ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x0256, IllegalArgumentException -> 0x0270 }
            android.content.Context r3 = com.lp.C0987.m6072()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x0256, IllegalArgumentException -> 0x0270 }
            r46 = r2
            r47 = 0
            r2 = r45
            r5 = r40
            r4 = r7
            r48 = r19
            r25 = r21
            r1 = 86400(0x15180, float:1.21072E-40)
            r5 = r20
            r50 = r6
            r49 = r18
            r6 = r22
            r51 = r7
            r38 = r16
            r7 = r23
            r44 = r8
            r8 = r24
            r52 = r9
            r9 = r27
            r53 = r10
            r10 = r28
            r54 = r11
            r11 = r29
            r55 = r12
            r12 = r30
            r56 = r13
            r13 = r31
            r57 = r14
            r14 = r32
            r15 = r33
            r16 = r34
            r17 = r35
            r18 = r36
            r19 = r39
            r20 = r0
            r21 = r37
            r22 = r41
            r23 = r25
            r24 = r26
            r25 = r60
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06de, IllegalArgumentException -> 0x06e6 }
            r1 = r51
            goto L_0x032f
        L_0x0256:
            r46 = r2
            r44 = r8
            r52 = r9
            r53 = r10
            r54 = r11
            r55 = r12
            r56 = r13
            r57 = r14
            r38 = r16
            r47 = 0
            r6 = r18
            r1 = r19
            goto L_0x06e2
        L_0x0270:
            r0 = move-exception
            r46 = r2
            r44 = r8
            r52 = r9
            r53 = r10
            r54 = r11
            r55 = r12
            r56 = r13
            r57 = r14
            r38 = r16
            r47 = 0
            r6 = r18
            r1 = r19
            goto L_0x06eb
        L_0x028b:
            r46 = r2
            r50 = r6
            r51 = r7
            r44 = r8
            r52 = r9
            r53 = r10
            r54 = r11
            r55 = r12
            r56 = r13
            r57 = r14
            r38 = r16
            r49 = r18
            r48 = r19
            r25 = r21
            r1 = 86400(0x15180, float:1.21072E-40)
            r47 = 0
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06de, IllegalArgumentException -> 0x06e6 }
            long r2 = r2 / r42
            int r3 = (int) r2     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06de, IllegalArgumentException -> 0x06e6 }
            int r3 = r3 - r0
            int r2 = java.lang.Math.abs(r3)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06de, IllegalArgumentException -> 0x06e6 }
            int r3 = com.lp.C0987.f4490     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06de, IllegalArgumentException -> 0x06e6 }
            int r3 = r3 * r1
            if (r2 <= r3) goto L_0x02ec
            com.lp.ᵔ r2 = new com.lp.ᵔ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x02df, IllegalArgumentException -> 0x02e5 }
            int r3 = com.lp.C0987.f4490     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x02df, IllegalArgumentException -> 0x02e5 }
            r15 = r60
            r14 = r51
            r2.<init>(r14, r3, r15)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x02d6, IllegalArgumentException -> 0x02da }
            r13 = 86400(0x15180, float:1.21072E-40)
            r1 = r58
            r1.m5986(r2)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06de, IllegalArgumentException -> 0x06e6 }
            r45 = r2
            r1 = r14
            goto L_0x032f
        L_0x02d6:
            r1 = r58
            goto L_0x06de
        L_0x02da:
            r0 = move-exception
            r1 = r58
            goto L_0x06e7
        L_0x02df:
            r1 = r58
            r15 = r60
            goto L_0x06de
        L_0x02e5:
            r0 = move-exception
            r1 = r58
            r15 = r60
            goto L_0x06e7
        L_0x02ec:
            r13 = 86400(0x15180, float:1.21072E-40)
            r1 = r58
            r15 = r60
            r14 = r51
            com.lp.ᵔ r45 = new com.lp.ᵔ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06de, IllegalArgumentException -> 0x06e6 }
            android.content.Context r3 = com.lp.C0987.m6072()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06de, IllegalArgumentException -> 0x06e6 }
            r2 = r45
            r4 = r14
            r5 = r20
            r6 = r22
            r7 = r23
            r8 = r24
            r9 = r27
            r10 = r28
            r11 = r29
            r12 = r30
            r1 = 86400(0x15180, float:1.21072E-40)
            r13 = r31
            r1 = r14
            r14 = r32
            r15 = r33
            r16 = r34
            r17 = r35
            r18 = r36
            r19 = r39
            r20 = r0
            r21 = r37
            r22 = r41
            r23 = r25
            r24 = r26
            r25 = r60
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06de, IllegalArgumentException -> 0x06e6 }
        L_0x032f:
            if (r61 == 0) goto L_0x034a
            boolean r2 = com.chelpus.C0815.m5192(r45)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x0343, IllegalArgumentException -> 0x0346 }
            if (r2 == 0) goto L_0x034a
            com.lp.ᵔ r2 = new com.lp.ᵔ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x0343, IllegalArgumentException -> 0x0346 }
            int r3 = com.lp.C0987.f4490     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x0343, IllegalArgumentException -> 0x0346 }
            r4 = 1
            r2.<init>(r1, r3, r4)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r2.m6005()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            goto L_0x034d
        L_0x0343:
            r4 = 1
            goto L_0x06cf
        L_0x0346:
            r0 = move-exception
            r4 = 1
            goto L_0x06d7
        L_0x034a:
            r4 = 1
            r2 = r45
        L_0x034d:
            android.content.SharedPreferences r3 = com.lp.C0987.m6073()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r5 = "systemapp"
            boolean r3 = r3.getBoolean(r5, r4)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r5 = "package scan error"
            if (r59 != 0) goto L_0x04de
            java.lang.String r0 = "android"
            boolean r0 = r1.equals(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x04d8
            java.lang.String r0 = ru.pKkcGXHI.kKSaIWSZS.PkgName.getPkgName()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r0 = r1.equals(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x04d8
            boolean r0 = r2.f4352     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x0380
            if (r3 != 0) goto L_0x0380
            boolean r0 = r2.f4347     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x0378
            goto L_0x0380
        L_0x0378:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r1 = "package scan filter"
            r0.<init>(r1)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x0380:
            android.content.SharedPreferences r0 = com.lp.C0987.m6073()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r1 = "lvlapp"
            boolean r0 = r0.getBoolean(r1, r4)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x03ad
            boolean r0 = r2.f4348     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x03ad
            boolean r0 = r2.f4349     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x03ad
            boolean r0 = r2.f4347     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x03ad
            boolean r0 = r2.f4353     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r1 = r2.f4352     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r0 = com.chelpus.C0815.m5197(r0, r1)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x03ad
            boolean r0 = r2.f4351     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x03a7
            goto L_0x03ad
        L_0x03a7:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x03ad:
            android.content.SharedPreferences r0 = com.lp.C0987.m6073()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r1 = "adsapp"
            boolean r0 = r0.getBoolean(r1, r4)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x03da
            boolean r0 = r2.f4349     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x03da
            boolean r0 = r2.f4348     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x03da
            boolean r0 = r2.f4347     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x03da
            boolean r0 = r2.f4353     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r1 = r2.f4352     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r0 = com.chelpus.C0815.m5197(r0, r1)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x03da
            boolean r0 = r2.f4351     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x03d4
            goto L_0x03da
        L_0x03d4:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x03da:
            android.content.SharedPreferences r0 = com.lp.C0987.m6073()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r1 = "lvlapp"
            boolean r0 = r0.getBoolean(r1, r4)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x0413
            android.content.SharedPreferences r0 = com.lp.C0987.m6073()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r1 = "adsapp"
            boolean r0 = r0.getBoolean(r1, r4)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x0413
            boolean r0 = r2.f4348     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x0413
            boolean r0 = r2.f4349     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x0413
            boolean r0 = r2.f4347     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x0413
            boolean r0 = r2.f4353     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r1 = r2.f4352     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r0 = com.chelpus.C0815.m5197(r0, r1)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x0413
            boolean r0 = r2.f4351     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x040d
            goto L_0x0413
        L_0x040d:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x0413:
            android.content.SharedPreferences r0 = com.lp.C0987.m6073()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r1 = "noneapp"
            boolean r0 = r0.getBoolean(r1, r4)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x0444
            boolean r0 = r2.f4347     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x0444
            boolean r0 = r2.f4349     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x0444
            boolean r0 = r2.f4350     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x0444
            boolean r0 = r2.f4348     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x0444
            boolean r0 = r2.f4353     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r1 = r2.f4352     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r0 = com.chelpus.C0815.m5197(r0, r1)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x0444
            boolean r0 = r2.f4351     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x043e
            goto L_0x0444
        L_0x043e:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x0444:
            android.content.SharedPreferences r0 = com.lp.C0987.m6073()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r1 = "customapp"
            boolean r0 = r0.getBoolean(r1, r4)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x0469
            boolean r0 = r2.f4347     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x0469
            boolean r0 = r2.f4353     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r1 = r2.f4352     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r0 = com.chelpus.C0815.m5197(r0, r1)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x0469
            boolean r0 = r2.f4351     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x0463
            goto L_0x0469
        L_0x0463:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x0469:
            android.content.SharedPreferences r0 = com.lp.C0987.m6073()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r1 = "fixedapp"
            boolean r0 = r0.getBoolean(r1, r4)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x048a
            boolean r0 = r2.f4353     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r1 = r2.f4352     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r0 = com.chelpus.C0815.m5197(r0, r1)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x048a
            boolean r0 = r2.f4351     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x0484
            goto L_0x048a
        L_0x0484:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x048a:
            android.content.SharedPreferences r0 = com.lp.C0987.m6073()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r1 = "modifapp"
            boolean r0 = r0.getBoolean(r1, r4)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x04ab
            boolean r0 = r2.f4353     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r1 = r2.f4352     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r0 = com.chelpus.C0815.m5197(r0, r1)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x04ab
            boolean r0 = r2.f4351     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x04a5
            goto L_0x04ab
        L_0x04a5:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x04ab:
            android.content.SharedPreferences r0 = com.lp.C0987.m6073()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r1 = "fixedapp"
            boolean r0 = r0.getBoolean(r1, r4)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x0509
            android.content.SharedPreferences r0 = com.lp.C0987.m6073()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r1 = "modifapp"
            boolean r0 = r0.getBoolean(r1, r4)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x0509
            boolean r0 = r2.f4353     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r1 = r2.f4352     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r0 = com.chelpus.C0815.m5197(r0, r1)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x04d2
            boolean r0 = r2.f4351     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x04d2
            goto L_0x0509
        L_0x04d2:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x04d8:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x04de:
            java.lang.String r3 = "android"
            boolean r3 = r1.equals(r3)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r3 != 0) goto L_0x06c1
            android.content.Context r3 = com.lp.C0967.f4261     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r3 = r3.getPackageName()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r1 = r1.equals(r3)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r1 != 0) goto L_0x06c1
            int r1 = com.lp.C0987.f4507     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r1 == 0) goto L_0x06b7
            int r1 = com.lp.C0987.f4507     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06a8, IllegalArgumentException -> 0x06af }
            r3 = 333(0x14d, float:4.67E-43)
            if (r1 == r3) goto L_0x0698
            r3 = 2131296472(0x7f0900d8, float:1.8210862E38)
            if (r1 == r3) goto L_0x0692
            java.lang.String r3 = "android.permission.INTERNET"
            switch(r1) {
                case 1929: goto L_0x067c;
                case 1930: goto L_0x066e;
                case 1931: goto L_0x0645;
                case 1932: goto L_0x060f;
                default: goto L_0x0506;
            }
        L_0x0506:
            switch(r1) {
                case 2131296403: goto L_0x05f6;
                case 2131296404: goto L_0x05ea;
                case 2131296405: goto L_0x05de;
                case 2131296406: goto L_0x05d2;
                case 2131296407: goto L_0x05c6;
                case 2131296408: goto L_0x05b4;
                case 2131296409: goto L_0x05a8;
                case 2131296410: goto L_0x059c;
                case 2131296411: goto L_0x0588;
                case 2131296412: goto L_0x0575;
                case 2131296413: goto L_0x056a;
                case 2131296414: goto L_0x0549;
                case 2131296415: goto L_0x0510;
                default: goto L_0x0509;
            }
        L_0x0509:
            r1 = r48
        L_0x050b:
            r6 = r49
            r3 = 2
            goto L_0x06bc
        L_0x0510:
            android.content.pm.PackageManager r0 = com.lp.C0987.m6068()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r1 = r2.f4337     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r3 = 4096(0x1000, float:5.74E-42)
            android.content.pm.PackageInfo r0 = r0.getPackageInfo(r1, r3)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String[] r0 = r0.requestedPermissions     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x053f
            int r1 = r0.length     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r3 = 0
            r6 = 0
        L_0x0523:
            if (r3 >= r1) goto L_0x0540
            r7 = r0[r3]     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r8 = r7.toUpperCase()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r9 = "SEND_SMS"
            boolean r8 = r8.contains(r9)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r8 != 0) goto L_0x053b
            java.lang.String r8 = "CALL_PHONE"
            boolean r7 = r7.contains(r8)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r7 == 0) goto L_0x053c
        L_0x053b:
            r6 = 1
        L_0x053c:
            int r3 = r3 + 1
            goto L_0x0523
        L_0x053f:
            r6 = 0
        L_0x0540:
            if (r6 == 0) goto L_0x0543
            goto L_0x0509
        L_0x0543:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x0549:
            boolean r0 = r2.f4352     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x0564
            r1 = r50
            java.lang.String r0 = r1.sourceDir     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r3 = "/data/"
            boolean r0 = r0.startsWith(r3)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x0509
            java.lang.String r0 = r1.sourceDir     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r1 = "/mnt/"
            boolean r0 = r0.startsWith(r1)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x0564
            goto L_0x0509
        L_0x0564:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x056a:
            boolean r0 = r2.f4352     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x056f
            goto L_0x0509
        L_0x056f:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x0575:
            r1 = r50
            java.lang.String r0 = r1.sourceDir     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r1 = "/mnt/"
            boolean r0 = r0.startsWith(r1)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x0582
            goto L_0x0509
        L_0x0582:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x0588:
            r1 = r50
            java.lang.String r0 = r1.sourceDir     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            java.lang.String r1 = "/data/"
            boolean r0 = r0.startsWith(r1)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x0596
            goto L_0x0509
        L_0x0596:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x059c:
            boolean r0 = r2.f4350     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x05a2
            goto L_0x0509
        L_0x05a2:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x05a8:
            boolean r0 = r2.f4349     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x05ae
            goto L_0x0509
        L_0x05ae:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x05b4:
            boolean r0 = r2.f4353     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r1 = r2.f4352     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            boolean r0 = com.chelpus.C0815.m5197(r0, r1)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x05c0
            goto L_0x0509
        L_0x05c0:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x05c6:
            boolean r0 = r2.f4351     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x05cc
            goto L_0x0509
        L_0x05cc:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x05d2:
            boolean r0 = r2.f4347     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x05d8
            goto L_0x0509
        L_0x05d8:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x05de:
            boolean r0 = r2.f4355     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 != 0) goto L_0x05e4
            goto L_0x0509
        L_0x05e4:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x05ea:
            boolean r0 = r2.f4348     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            if (r0 == 0) goto L_0x05f0
            goto L_0x0509
        L_0x05f0:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x05f6:
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            long r6 = r6 / r42
            int r1 = (int) r6     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            int r1 = r1 - r0
            int r0 = java.lang.Math.abs(r1)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r1 = 86400(0x15180, float:1.21072E-40)
            if (r0 > r1) goto L_0x0609
            goto L_0x0509
        L_0x0609:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
        L_0x060f:
            java.lang.String r0 = r2.f4337     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06cf, IllegalArgumentException -> 0x06d6 }
            r1 = r48
            int r0 = r1.checkPermission(r3, r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06d1, IllegalArgumentException -> 0x0642 }
            r6 = -1
            if (r0 == r6) goto L_0x063c
            android.content.ComponentName r0 = new android.content.ComponentName     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06d1, IllegalArgumentException -> 0x0642 }
            java.lang.String r6 = r2.f4337     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06d1, IllegalArgumentException -> 0x0642 }
            r0.<init>(r6, r3)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06d1, IllegalArgumentException -> 0x0642 }
            int r0 = r1.getComponentEnabledSetting(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06d1, IllegalArgumentException -> 0x0642 }
            if (r0 == r4) goto L_0x0636
            android.content.ComponentName r0 = new android.content.ComponentName     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06d1, IllegalArgumentException -> 0x0642 }
            java.lang.String r6 = r2.f4337     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06d1, IllegalArgumentException -> 0x0642 }
            r0.<init>(r6, r3)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06d1, IllegalArgumentException -> 0x0642 }
            int r0 = r1.getComponentEnabledSetting(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06d1, IllegalArgumentException -> 0x0642 }
            if (r0 == 0) goto L_0x0636
            goto L_0x050b
        L_0x0636:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06d1, IllegalArgumentException -> 0x0642 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06d1, IllegalArgumentException -> 0x0642 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06d1, IllegalArgumentException -> 0x0642 }
        L_0x063c:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06d1, IllegalArgumentException -> 0x0642 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06d1, IllegalArgumentException -> 0x0642 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06d1, IllegalArgumentException -> 0x0642 }
        L_0x0642:
            r0 = move-exception
            goto L_0x06d9
        L_0x0645:
            r1 = r48
            java.lang.String r0 = r2.f4337     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06aa, IllegalArgumentException -> 0x066c }
            int r0 = r1.checkPermission(r3, r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06aa, IllegalArgumentException -> 0x066c }
            r6 = -1
            if (r0 == r6) goto L_0x0665
            android.content.ComponentName r0 = new android.content.ComponentName     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06aa, IllegalArgumentException -> 0x066c }
            java.lang.String r6 = r2.f4337     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06aa, IllegalArgumentException -> 0x066c }
            r0.<init>(r6, r3)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06aa, IllegalArgumentException -> 0x066c }
            int r0 = r1.getComponentEnabledSetting(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06aa, IllegalArgumentException -> 0x066c }
            r3 = 2
            if (r0 == r3) goto L_0x065f
            goto L_0x069f
        L_0x065f:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
        L_0x0665:
            r3 = 2
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
        L_0x066c:
            r0 = move-exception
            goto L_0x06b2
        L_0x066e:
            r1 = r48
            r3 = 2
            boolean r0 = r2.f4352     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
            if (r0 != 0) goto L_0x0676
            goto L_0x069f
        L_0x0676:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
        L_0x067c:
            r1 = r48
            r3 = 2
            boolean r0 = r2.f4352     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
            if (r0 != 0) goto L_0x068c
            java.lang.String r0 = r2.f4337     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
            boolean r0 = com.chelpus.C0815.m5232(r0)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
            if (r0 != 0) goto L_0x068c
            goto L_0x069f
        L_0x068c:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
        L_0x0692:
            r1 = r48
            r3 = 2
            com.lp.C0987.f4507 = r47     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
            goto L_0x06ba
        L_0x0698:
            r1 = r48
            r3 = 2
            boolean r0 = r2.f4355     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
            if (r0 == 0) goto L_0x06a0
        L_0x069f:
            goto L_0x06ba
        L_0x06a0:
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
            r0.<init>(r5)     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
            throw r0     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x06ab, IllegalArgumentException -> 0x06a6 }
        L_0x06a6:
            r0 = move-exception
            goto L_0x06b3
        L_0x06a8:
            r1 = r48
        L_0x06aa:
            r3 = 2
        L_0x06ab:
            r6 = r49
            goto L_0x07aa
        L_0x06af:
            r0 = move-exception
            r1 = r48
        L_0x06b2:
            r3 = 2
        L_0x06b3:
            r6 = r49
            goto L_0x0790
        L_0x06b7:
            r1 = r48
            r3 = 2
        L_0x06ba:
            r6 = r49
        L_0x06bc:
            r6.add(r2)     // Catch:{ IllegalArgumentException -> 0x06cc }
            goto L_0x07aa
        L_0x06c1:
            r1 = r48
            r6 = r49
            r3 = 2
            com.lp.ﹳ r0 = new com.lp.ﹳ     // Catch:{ IllegalArgumentException -> 0x06cc }
            r0.<init>(r5)     // Catch:{ IllegalArgumentException -> 0x06cc }
            throw r0     // Catch:{ IllegalArgumentException -> 0x06cc }
        L_0x06cc:
            r0 = move-exception
            goto L_0x0790
        L_0x06cf:
            r1 = r48
        L_0x06d1:
            r6 = r49
            r3 = 2
            goto L_0x07aa
        L_0x06d6:
            r0 = move-exception
        L_0x06d7:
            r1 = r48
        L_0x06d9:
            r6 = r49
            r3 = 2
            goto L_0x0790
        L_0x06de:
            r1 = r48
            r6 = r49
        L_0x06e2:
            r3 = 2
            r4 = 1
            goto L_0x07aa
        L_0x06e6:
            r0 = move-exception
        L_0x06e7:
            r1 = r48
            r6 = r49
        L_0x06eb:
            r3 = 2
            r4 = 1
            goto L_0x0790
        L_0x06ef:
            r46 = r2
            goto L_0x06fa
        L_0x06f2:
            r0 = move-exception
            r46 = r2
            goto L_0x0715
        L_0x06f6:
            r46 = r2
            r40 = r3
        L_0x06fa:
            r44 = r8
            r52 = r9
            r53 = r10
            r54 = r11
            r55 = r12
            r56 = r13
            r57 = r14
            r38 = r16
            r6 = r18
            r1 = r19
            goto L_0x0771
        L_0x0710:
            r0 = move-exception
            r46 = r2
            r40 = r3
        L_0x0715:
            r44 = r8
            r52 = r9
            r53 = r10
            r54 = r11
            r55 = r12
            r56 = r13
            r57 = r14
            r38 = r16
            r6 = r18
            r1 = r19
            goto L_0x078c
        L_0x072b:
            r46 = r2
            r40 = r3
            r1 = r5
            r44 = r8
            r52 = r9
            r53 = r10
            r54 = r11
            r55 = r12
            r56 = r13
            r57 = r14
            r38 = r16
            r6 = r18
            goto L_0x0771
        L_0x0743:
            r0 = move-exception
            r46 = r2
            r40 = r3
            r1 = r5
            r44 = r8
            r52 = r9
            r53 = r10
            r54 = r11
            r55 = r12
            r56 = r13
            r57 = r14
            r38 = r16
            r6 = r18
            goto L_0x078c
        L_0x075c:
            r46 = r2
            r40 = r3
            r1 = r5
            r44 = r8
            r52 = r9
            r53 = r10
            r54 = r11
            r55 = r12
            r56 = r13
            r57 = r14
            r38 = r16
        L_0x0771:
            r3 = 2
            r4 = 1
            r47 = 0
            goto L_0x07aa
        L_0x0776:
            r0 = move-exception
            r46 = r2
            r40 = r3
            r1 = r5
            r44 = r8
            r52 = r9
            r53 = r10
            r54 = r11
            r55 = r12
            r56 = r13
            r57 = r14
            r38 = r16
        L_0x078c:
            r3 = 2
            r4 = 1
            r47 = 0
        L_0x0790:
            r0.printStackTrace()     // Catch:{ NameNotFoundException | ﹳ | Exception -> 0x07aa }
            goto L_0x07aa
        L_0x0794:
            r46 = r2
            r40 = r3
            r1 = r5
            r38 = r7
            r44 = r8
            r52 = r9
            r53 = r10
            r54 = r11
            r55 = r12
            r56 = r13
            r57 = r14
            goto L_0x0771
        L_0x07aa:
            boolean r0 = r46.moveToNext()     // Catch:{ Exception -> 0x07d0 }
            if (r0 != 0) goto L_0x07b4
            r46.close()     // Catch:{ Exception -> 0x07d0 }
            goto L_0x07d3
        L_0x07b4:
            r15 = r60
            r5 = r1
            r7 = r38
            r3 = r40
            r8 = r44
            r2 = r46
            r9 = r52
            r10 = r53
            r11 = r54
            r12 = r55
            r13 = r56
            r14 = r57
            r4 = 2
            r1 = r58
            goto L_0x00a7
        L_0x07d0:
            r46.close()     // Catch:{ Exception -> 0x0836 }
        L_0x07d3:
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0836 }
            int r0 = r40.size()     // Catch:{ Exception -> 0x0836 }
            if (r0 <= 0) goto L_0x081a
            java.lang.String[] r3 = com.lp.C0987.m6102()     // Catch:{ Exception -> 0x0836 }
            int r5 = r3.length     // Catch:{ Exception -> 0x0836 }
            r7 = 0
        L_0x07e3:
            if (r7 >= r5) goto L_0x081a
            r0 = r3[r7]     // Catch:{ Exception -> 0x0836 }
            r8 = r40
            boolean r9 = r8.contains(r0)     // Catch:{ Exception -> 0x0836 }
            if (r9 != 0) goto L_0x0815
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0836 }
            r9.<init>()     // Catch:{ Exception -> 0x0836 }
            java.lang.String r10 = "Lucky Patcher: add new pkg name "
            r9.append(r10)     // Catch:{ Exception -> 0x0836 }
            r9.append(r0)     // Catch:{ Exception -> 0x0836 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0836 }
            com.lp.C0987.m6060(r9)     // Catch:{ Exception -> 0x0836 }
            com.lp.ᵔ r9 = new com.lp.ᵔ     // Catch:{ Throwable -> 0x0811 }
            int r10 = com.lp.C0987.f4490     // Catch:{ Throwable -> 0x0811 }
            r9.<init>(r0, r10, r4)     // Catch:{ Throwable -> 0x0811 }
            r6.add(r9)     // Catch:{ Throwable -> 0x0811 }
            r9.m6005()     // Catch:{ Throwable -> 0x0811 }
            goto L_0x0815
        L_0x0811:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x0836 }
        L_0x0815:
            int r7 = r7 + 1
            r40 = r8
            goto L_0x07e3
        L_0x081a:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0836 }
            r0.<init>()     // Catch:{ Exception -> 0x0836 }
            java.lang.String r3 = "time for compare pkgs:"
            r0.append(r3)     // Catch:{ Exception -> 0x0836 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0836 }
            long r3 = r3 - r1
            r0.append(r3)     // Catch:{ Exception -> 0x0836 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0836 }
            com.lp.C0987.m6060(r0)     // Catch:{ Exception -> 0x0836 }
            com.lp.C0967.f4263 = r47     // Catch:{ Exception -> 0x0836 }
            goto L_0x0851
        L_0x0836:
            r0 = move-exception
            goto L_0x083b
        L_0x0838:
            r0 = move-exception
            r47 = 0
        L_0x083b:
            com.lp.C0967.f4263 = r47
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "LuckyPatcher-Error: getPackage "
            r1.append(r2)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.lp.C0987.m6060(r0)
        L_0x0851:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lp.C0967.m5985(boolean, boolean, boolean):java.util.ArrayList");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Can't wrap try/catch for region: R(17:0|1|2|3|(2:5|6)(1:7)|8|9|10|11|(1:13)(1:14)|(1:16)|17|18|19|20|21|25) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0104 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x010a */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m5986(com.lp.C0980 r9) {
        /*
            r8 = this;
            java.lang.String r0 = "Packages"
            java.lang.String r1 = "pkgName"
            r2 = 1
            r3 = 0
            com.lp.C0967.f4264 = r2     // Catch:{ Exception -> 0x0114 }
            android.content.ContentValues r4 = new android.content.ContentValues     // Catch:{ Exception -> 0x0114 }
            r4.<init>()     // Catch:{ Exception -> 0x0114 }
            java.lang.String r5 = r9.f4337     // Catch:{ Exception -> 0x0114 }
            r4.put(r1, r5)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r5 = "pkgLabel"
            java.lang.String r6 = r9.f4338     // Catch:{ Exception -> 0x0114 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0114 }
            boolean r5 = r9.f4340     // Catch:{ Exception -> 0x0114 }
            java.lang.String r6 = "stored"
            if (r5 == 0) goto L_0x0027
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0114 }
            r4.put(r6, r2)     // Catch:{ Exception -> 0x0114 }
            goto L_0x002e
        L_0x0027:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0114 }
            r4.put(r6, r2)     // Catch:{ Exception -> 0x0114 }
        L_0x002e:
            java.lang.String r2 = "storepref"
            java.lang.Integer r5 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0114 }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r2 = "hidden"
            boolean r5 = r9.f4341     // Catch:{ Exception -> 0x0114 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x0114 }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r2 = "statusi"
            java.lang.String r5 = r9.f4342     // Catch:{ Exception -> 0x0114 }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r2 = "boot_ads"
            boolean r5 = r9.f4343     // Catch:{ Exception -> 0x0114 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x0114 }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r2 = "boot_lvl"
            boolean r5 = r9.f4344     // Catch:{ Exception -> 0x0114 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x0114 }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r2 = "boot_custom"
            boolean r5 = r9.f4345     // Catch:{ Exception -> 0x0114 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x0114 }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r2 = "boot_manual"
            boolean r5 = r9.f4346     // Catch:{ Exception -> 0x0114 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x0114 }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r2 = "custom"
            boolean r5 = r9.f4347     // Catch:{ Exception -> 0x0114 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x0114 }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r2 = "lvl"
            boolean r5 = r9.f4348     // Catch:{ Exception -> 0x0114 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x0114 }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r2 = "ads"
            boolean r5 = r9.f4349     // Catch:{ Exception -> 0x0114 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x0114 }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r2 = "modified"
            boolean r5 = r9.f4351     // Catch:{ Exception -> 0x0114 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x0114 }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r2 = "system"
            boolean r5 = r9.f4352     // Catch:{ Exception -> 0x0114 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x0114 }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r2 = "odex"
            boolean r5 = r9.f4353     // Catch:{ Exception -> 0x0114 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x0114 }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r2 = "updatetime"
            int r5 = r9.f4354     // Catch:{ Exception -> 0x0114 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x0114 }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r2 = "billing"
            boolean r5 = r9.f4350     // Catch:{ Exception -> 0x0114 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x0114 }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r2 = "install_dir"
            java.lang.String r5 = r9.f4358     // Catch:{ Exception -> 0x0114 }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x0114 }
            android.content.pm.PackageManager r2 = com.lp.C0987.m6068()     // Catch:{ Exception -> 0x0114 }
            java.lang.String r5 = r9.f4337     // Catch:{ Exception -> 0x0114 }
            android.content.pm.ApplicationInfo r2 = r2.getApplicationInfo(r5, r3)     // Catch:{ Exception -> 0x0114 }
            java.lang.String r2 = r2.sourceDir     // Catch:{ Exception -> 0x0114 }
            r2 = 0
            android.graphics.drawable.Drawable r5 = r9.f4339     // Catch:{ OutOfMemoryError -> 0x0104 }
            if (r5 == 0) goto L_0x00ec
            android.graphics.drawable.Drawable r9 = r9.f4339     // Catch:{ OutOfMemoryError -> 0x0104 }
            android.graphics.Bitmap r9 = com.chelpus.C0815.m5135(r9)     // Catch:{ OutOfMemoryError -> 0x0104 }
            goto L_0x00ed
        L_0x00ec:
            r9 = r2
        L_0x00ed:
            if (r9 == 0) goto L_0x0104
            java.io.ByteArrayOutputStream r5 = new java.io.ByteArrayOutputStream     // Catch:{ OutOfMemoryError -> 0x0104 }
            r5.<init>()     // Catch:{ OutOfMemoryError -> 0x0104 }
            android.graphics.Bitmap$CompressFormat r6 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ OutOfMemoryError -> 0x0104 }
            r7 = 100
            r9.compress(r6, r7, r5)     // Catch:{ OutOfMemoryError -> 0x0104 }
            byte[] r9 = r5.toByteArray()     // Catch:{ OutOfMemoryError -> 0x0104 }
            java.lang.String r5 = "icon"
            r4.put(r5, r9)     // Catch:{ OutOfMemoryError -> 0x0104 }
        L_0x0104:
            android.database.sqlite.SQLiteDatabase r9 = com.lp.C0967.f4262     // Catch:{ Exception -> 0x010a }
            r9.insertOrThrow(r0, r1, r4)     // Catch:{ Exception -> 0x010a }
            goto L_0x010f
        L_0x010a:
            android.database.sqlite.SQLiteDatabase r9 = com.lp.C0967.f4262     // Catch:{ Exception -> 0x0114 }
            r9.replace(r0, r2, r4)     // Catch:{ Exception -> 0x0114 }
        L_0x010f:
            com.lp.C0967.f4264 = r3     // Catch:{ Exception -> 0x0114 }
            com.lp.C0967.f4264 = r3     // Catch:{ Exception -> 0x0114 }
            goto L_0x012b
        L_0x0114:
            r9 = move-exception
            com.lp.C0967.f4264 = r3
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "LuckyPatcher-Error: savePackage "
            r0.append(r1)
            r0.append(r9)
            java.lang.String r9 = r0.toString()
            com.lp.C0987.m6060(r9)
        L_0x012b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lp.C0967.m5986(com.lp.ᵔ):void");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5987(String str) {
        try {
            SQLiteDatabase sQLiteDatabase = f4262;
            sQLiteDatabase.delete("Packages", "pkgName = '" + str + "'", null);
        } catch (Exception e) {
            C0987.m6060((Object) ("LuckyPatcher-Error: deletePackage " + e));
        }
    }
}
