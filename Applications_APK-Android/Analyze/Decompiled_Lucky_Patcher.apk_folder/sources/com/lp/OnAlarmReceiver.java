package com.lp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class OnAlarmReceiver extends BroadcastReceiver {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String f3811 = "ActionOnTimeLucky";

    public void onReceive(final Context context, final Intent intent) {
        new Thread(new Runnable() {
            public void run() {
                C0987.m6060((Object) intent.getAction());
                if (intent.getAction().equals(OnAlarmReceiver.f3811)) {
                    C0987.m6073().edit().putBoolean("trigger_for_good_android_patch_on_boot", false).commit();
                    if (C0987.f4474 && C0987.m6073().getBoolean("OnBootService", false)) {
                        C0987.m6073().edit().putBoolean("OnBootService", false).commit();
                        C0987.f4484 = true;
                        new Intent(C0987.m6072(), PatchService.class);
                        try {
                            if (Build.VERSION.SDK_INT >= 26) {
                                context.startForegroundService(new Intent(context, PatchService.class));
                            } else {
                                context.startService(new Intent(context, PatchService.class));
                            }
                        } catch (Throwable th) {
                            th.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }
}
