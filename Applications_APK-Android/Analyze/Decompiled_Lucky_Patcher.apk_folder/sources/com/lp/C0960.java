package com.lp;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.lp.ʽ  reason: contains not printable characters */
/* compiled from: ArrayAdapterWithFilter */
public class C0960<Object> extends ArrayAdapter<Object> implements Filterable {

    /* renamed from: ʻ  reason: contains not printable characters */
    public List<Object> f4206;

    /* renamed from: ʼ  reason: contains not printable characters */
    public List<Object> f4207;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Context f4208;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Filter f4209;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f4210;

    public C0960(Context context, int i, List<Object> list) {
        super(context, i, list);
        this.f4210 = i;
        this.f4206 = list;
        this.f4208 = context;
        this.f4207 = list;
    }

    public int getCount() {
        return this.f4206.size();
    }

    public Object getItem(int i) {
        return this.f4206.get(i);
    }

    public long getItemId(int i) {
        return (long) this.f4206.get(i).hashCode();
    }

    public Filter getFilter() {
        if (this.f4209 == null) {
            this.f4209 = new C0961();
        }
        return this.f4209;
    }

    /* renamed from: com.lp.ʽ$ʻ  reason: contains not printable characters */
    /* compiled from: ArrayAdapterWithFilter */
    private class C0961 extends Filter {
        private C0961() {
        }

        /* access modifiers changed from: protected */
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            Filter.FilterResults filterResults = new Filter.FilterResults();
            if (charSequence == null || charSequence.length() == 0) {
                filterResults.values = C0960.this.f4207;
                filterResults.count = C0960.this.f4207.size();
            } else {
                ArrayList arrayList = new ArrayList();
                filterResults.values = arrayList;
                filterResults.count = arrayList.size();
            }
            return filterResults;
        }

        /* access modifiers changed from: protected */
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            if (filterResults.count == 0) {
                C0960.this.notifyDataSetInvalidated();
                return;
            }
            C0960.this.f4206 = (List) filterResults.values;
            C0960.this.notifyDataSetChanged();
        }
    }
}
