package com.lp;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.ʻ.C0273;

public class NotificationService extends Service {

    /* renamed from: ʻ  reason: contains not printable characters */
    public Context f3810 = null;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        C0987.m6060((Object) "LuckyPatcher: Notify service create.");
        startForeground(50, new C0273.C0275(this).m1674());
        this.f3810 = this;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        C0987.f4484 = true;
        C0987.m6060((Object) "LuckyPatcher: Start notify service!");
        return 2;
    }

    public void onDestroy() {
        super.onDestroy();
        C0987.m6060((Object) "Killing Notify Service!!!!!!!!!!!!!!!!!!!!!!!");
    }
}
