package com.lp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.chelpus.C0815;
import com.lp.widgets.BinderWidget;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import net.lingala.zip4j.util.InternalZipConstants;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

public class BinderActivity extends Activity {

    /* renamed from: ʻ  reason: contains not printable characters */
    public Context f3741;

    /* renamed from: ʼ  reason: contains not printable characters */
    public Dialog f3742 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    public ArrayList<C0963> f3743 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    public ListView f3744 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    public int f3745 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ˆ  reason: contains not printable characters */
    public String f3746;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public TextView f3747;
    /* access modifiers changed from: private */

    /* renamed from: ˉ  reason: contains not printable characters */
    public C0938 f3748;
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public ListView f3749;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f3741 = this;
        File file = new File(getDir("binder", 0) + "/bind.txt");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.f3743 = m5819(this.f3741);
        setContentView((int) R.layout.binder);
        this.f3744 = (ListView) findViewById(R.id.bindlistView);
        C0987.f4491 = new ArrayAdapter<C0963>(this, R.layout.binditemview, this.f3743) {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
             arg types: [?, android.view.ViewGroup, int]
             candidates:
              ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
              ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
            public View getView(final int i, View view, ViewGroup viewGroup) {
                if (view == null) {
                    view = ((Activity) BinderActivity.this.f3741).getLayoutInflater().inflate((int) R.layout.binditemview, viewGroup, false);
                }
                TextView textView = (TextView) view.findViewById(R.id.source);
                TextView textView2 = (TextView) view.findViewById(R.id.target);
                textView.setTextAppearance(getContext(), C0987.m6076());
                textView2.setTextAppearance(getContext(), C0987.m6076());
                final ToggleButton toggleButton = (ToggleButton) view.findViewById(R.id.toggleButton_off);
                final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
                toggleButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        RelativeLayout relativeLayout = (RelativeLayout) View.inflate(BinderActivity.this.f3741, R.layout.binderdialog, null);
                        toggleButton.setEnabled(false);
                        progressBar.setVisibility(0);
                        Thread thread = new Thread(new Runnable() {
                            public void run() {
                                if (!C0987.f4491.getItem(i).f4242.startsWith("~chelpus_disabled~")) {
                                    if (C0987.f4474) {
                                        C0815.m5301("umount -f '" + C0987.f4491.getItem(i).f4242 + "'");
                                        C0815.m5301("umount -l '" + C0987.f4491.getItem(i).f4242 + "'");
                                    } else {
                                        C0815.m5148("umount '" + C0987.f4491.getItem(i).f4242 + "'");
                                    }
                                    C0987.f4491.getItem(i).f4242 = "~chelpus_disabled~" + C0987.f4491.getItem(i).f4242;
                                } else {
                                    C0987.f4491.getItem(i).f4242 = C0987.f4491.getItem(i).f4242.replace("~chelpus_disabled~", BuildConfig.FLAVOR);
                                    C0815.m5178("mount", "-o bind '" + C0987.f4491.getItem(i).f4241 + "' '" + C0987.f4491.getItem(i).f4242 + "'", C0987.f4491.getItem(i).f4241, C0987.f4491.getItem(i).f4242);
                                }
                                BinderActivity.m5822(BinderActivity.this.f3743, BinderActivity.this.f3741);
                                BinderActivity.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        C0987.f4491.notifyDataSetChanged();
                                        BinderActivity.this.f3744.setAdapter((ListAdapter) C0987.f4491);
                                        progressBar.setVisibility(8);
                                        Intent intent = new Intent(BinderActivity.this.f3741, BinderWidget.class);
                                        intent.setAction(BinderWidget.f3931);
                                        BinderActivity.this.f3741.sendBroadcast(intent);
                                    }
                                });
                            }
                        });
                        thread.setPriority(10);
                        thread.start();
                    }
                });
                toggleButton.setChecked(C0815.m5191((C0963) getItem(i)));
                ((Button) view.findViewById(R.id.button_delete)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        C0815.m5156((Dialog) new AlertDialog.Builder(BinderActivity.this.f3741).setCancelable(false).setTitle(C0815.m5205((int) R.string.warning)).setMessage(C0815.m5205((int) R.string.bind_delete)).setPositiveButton(C0815.m5205((int) R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                try {
                                    if (C0987.f4474) {
                                        C0815.m5301("umount -f " + C0987.f4491.getItem(i).f4242);
                                        C0815.m5301("umount -l " + C0987.f4491.getItem(i).f4242);
                                    } else {
                                        C0815.m5148("umount " + C0987.f4491.getItem(i).f4242);
                                    }
                                    C0987.f4491.remove(C0987.f4491.getItem(i));
                                    BinderActivity.m5822(BinderActivity.this.f3743, BinderActivity.this.f3741);
                                    C0987.f4491.notifyDataSetChanged();
                                    BinderActivity.this.f3744.setAdapter((ListAdapter) C0987.f4491);
                                    Intent intent = new Intent(BinderActivity.this.f3741, BinderWidget.class);
                                    intent.setAction(BinderWidget.f3931);
                                    BinderActivity.this.f3741.sendBroadcast(intent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }).setNegativeButton(C0815.m5205((int) R.string.no), (DialogInterface.OnClickListener) null).create());
                    }
                });
                textView.setTextAppearance(BinderActivity.this.f3741, BinderActivity.this.f3745);
                textView2.setTextAppearance(BinderActivity.this.f3741, BinderActivity.this.f3745);
                textView.setText(C0815.m5137(C0815.m5205((int) R.string.bind_source) + "\n", "#ff00ff00", "bold"));
                textView.append(C0815.m5137(((C0963) getItem(i)).f4241, "#ffffffff", "italic"));
                textView2.setText(C0815.m5137(C0815.m5205((int) R.string.bind_target) + "\n", "#ffffff00", "bold"));
                textView2.append(C0815.m5137(((C0963) getItem(i)).f4242.replace("~chelpus_disabled~", BuildConfig.FLAVOR), "#ffffffff", "italic"));
                return view;
            }
        };
        this.f3744.setAdapter((ListAdapter) C0987.f4491);
        this.f3744.invalidateViews();
        this.f3744.setBackgroundColor(-16777216);
        ((Button) findViewById(R.id.newbindbutton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                final RelativeLayout relativeLayout = (RelativeLayout) View.inflate(BinderActivity.this.f3741, R.layout.binderdialog, null);
                final EditText editText = (EditText) relativeLayout.findViewById(R.id.editText1);
                final EditText editText2 = (EditText) relativeLayout.findViewById(R.id.editText2);
                AnonymousClass1 r4 = new View.OnClickListener() {
                    public void onClick(View view) {
                        final String str;
                        final String str2;
                        String obj = editText.getText().toString();
                        String obj2 = editText2.getText().toString();
                        if (!obj.endsWith(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                            str = obj.trim() + InternalZipConstants.ZIP_FILE_SEPARATOR;
                        } else {
                            str = obj.trim();
                        }
                        if (!obj2.endsWith(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                            str2 = obj2.trim() + InternalZipConstants.ZIP_FILE_SEPARATOR;
                        } else {
                            str2 = obj2.trim();
                        }
                        if (!new File(str.trim()).exists() || str2.equals(InternalZipConstants.ZIP_FILE_SEPARATOR) || str.equals(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                            Toast.makeText(BinderActivity.this.getApplication().getApplicationContext(), C0815.m5205((int) R.string.bind_error), 1).show();
                            return;
                        }
                        boolean z = false;
                        if (C0987.f4474) {
                            new Thread(new Runnable() {
                                public void run() {
                                    C0815.m5178("mount", "-o bind '" + str + "' '" + str2 + "'", str, str2);
                                }
                            }).start();
                        } else {
                            C0815.m5148("umount '" + str2 + "'");
                            C0815.m5148("mount -o bind '" + str + "' '" + str2 + "'");
                        }
                        Iterator<C0963> it = BinderActivity.this.f3743.iterator();
                        while (it.hasNext()) {
                            C0963 next = it.next();
                            if (next.f4242.equals(str2)) {
                                next.f4241 = str;
                                next.f4242 = str2;
                                z = true;
                            }
                        }
                        if (!z) {
                            BinderActivity.this.f3743.add(new C0963(str, str2));
                        }
                        BinderActivity.m5822(BinderActivity.this.f3743, BinderActivity.this.f3741);
                        C0987.f4491.notifyDataSetChanged();
                        BinderActivity.this.f3744.setAdapter((ListAdapter) C0987.f4491);
                        if (BinderActivity.this.f3742.isShowing()) {
                            BinderActivity.this.f3742.dismiss();
                        }
                    }
                };
                AnonymousClass2 r5 = new View.OnClickListener() {
                    /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|9) */
                    /* JADX WARNING: Code restructure failed: missing block: B:5:0x00e0, code lost:
                        com.lp.BinderActivity.m5818(r6.f3769.f3760, com.lp.C0987.f4438);
                        com.lp.BinderActivity.m5820(r6.f3769.f3760, com.lp.BinderActivity.m5825(r6.f3769.f3760), (android.widget.ListView) r9.findViewById(ru.pKkcGXHI.kKSaIWSZS.R.id.list));
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
                        return;
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:8:?, code lost:
                        return;
                     */
                    /* JADX WARNING: Failed to process nested try/catch */
                    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x00b6 */
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public void onClick(android.view.View r7) {
                        /*
                            r6 = this;
                            com.lp.BinderActivity$2 r7 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r7 = com.lp.BinderActivity.this
                            android.content.Context r7 = r7.f3741
                            r0 = 2131427366(0x7f0b0026, float:1.8476346E38)
                            r1 = 0
                            android.view.View r7 = android.view.View.inflate(r7, r0, r1)
                            android.widget.LinearLayout r7 = (android.widget.LinearLayout) r7
                            com.lp.ʼ r0 = new com.lp.ʼ
                            com.lp.BinderActivity$2 r1 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r1 = com.lp.BinderActivity.this
                            android.content.Context r1 = r1.f3741
                            r2 = 1
                            r0.<init>(r1, r2)
                            r0.m5940(r7)
                            r1 = 0
                            r0.m5952(r1)
                            android.app.Dialog r0 = r0.m5947()
                            com.lp.BinderActivity$2$2$1 r1 = new com.lp.BinderActivity$2$2$1
                            r1.<init>()
                            r0.setOnKeyListener(r1)
                            com.chelpus.C0815.m5156(r0)
                            com.lp.BinderActivity$2 r1 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r1 = com.lp.BinderActivity.this
                            r2 = 2131296487(0x7f0900e7, float:1.8210892E38)
                            android.view.View r2 = r0.findViewById(r2)
                            android.widget.TextView r2 = (android.widget.TextView) r2
                            android.widget.TextView unused = r1.f3747 = r2
                            r1 = 2131296327(0x7f090047, float:1.8210568E38)
                            android.view.View r1 = r0.findViewById(r1)
                            android.widget.Button r1 = (android.widget.Button) r1
                            r2 = 2131296320(0x7f090040, float:1.8210553E38)
                            android.view.View r2 = r0.findViewById(r2)
                            android.widget.Button r2 = (android.widget.Button) r2
                            com.lp.BinderActivity$2 r3 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r3 = com.lp.BinderActivity.this
                            android.widget.TextView r3 = r3.f3747
                            com.lp.BinderActivity$2 r4 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r4 = com.lp.BinderActivity.this
                            android.content.Context r4 = r4.f3741
                            int r5 = com.lp.C0987.m6076()
                            r3.setTextAppearance(r4, r5)
                            com.lp.BinderActivity$2$2$2 r3 = new com.lp.BinderActivity$2$2$2
                            r3.<init>(r7)
                            r2.setOnClickListener(r3)
                            com.lp.BinderActivity$2$2$3 r2 = new com.lp.BinderActivity$2$2$3
                            r2.<init>(r0)
                            r1.setOnClickListener(r2)
                            com.lp.BinderActivity$2 r0 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r0 = com.lp.BinderActivity.this
                            java.lang.String r1 = "/"
                            java.lang.String unused = r0.f3746 = r1
                            r0 = 2131296445(0x7f0900bd, float:1.8210807E38)
                            android.view.View r1 = r7.findViewById(r0)
                            android.widget.ListView r1 = (android.widget.ListView) r1
                            com.lp.BinderActivity$2$2$4 r2 = new com.lp.BinderActivity$2$2$4
                            r2.<init>()
                            r1.setOnItemClickListener(r2)
                            com.lp.BinderActivity$2 r1 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r1 = com.lp.BinderActivity.this
                            android.view.View r2 = r7.findViewById(r0)
                            android.widget.ListView r2 = (android.widget.ListView) r2
                            android.widget.ListView unused = r1.f3749 = r2
                            com.lp.BinderActivity$2 r1 = com.lp.BinderActivity.AnonymousClass2.this     // Catch:{ Exception -> 0x00b6 }
                            com.lp.BinderActivity r1 = com.lp.BinderActivity.this     // Catch:{ Exception -> 0x00b6 }
                            com.lp.BinderActivity$2 r2 = com.lp.BinderActivity.AnonymousClass2.this     // Catch:{ Exception -> 0x00b6 }
                            com.lp.BinderActivity r2 = com.lp.BinderActivity.this     // Catch:{ Exception -> 0x00b6 }
                            java.lang.String r2 = r2.f3746     // Catch:{ Exception -> 0x00b6 }
                            android.view.View r7 = r7.findViewById(r0)     // Catch:{ Exception -> 0x00b6 }
                            android.widget.ListView r7 = (android.widget.ListView) r7     // Catch:{ Exception -> 0x00b6 }
                            r1.m5821(r2, r7)     // Catch:{ Exception -> 0x00b6 }
                            goto L_0x0100
                        L_0x00b6:
                            com.lp.BinderActivity$2 r7 = com.lp.BinderActivity.AnonymousClass2.this     // Catch:{ Exception -> 0x00e0 }
                            com.lp.BinderActivity r7 = com.lp.BinderActivity.this     // Catch:{ Exception -> 0x00e0 }
                            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x00e0 }
                            java.lang.String r2 = com.lp.C0987.f4438     // Catch:{ Exception -> 0x00e0 }
                            r1.<init>(r2)     // Catch:{ Exception -> 0x00e0 }
                            java.lang.String r1 = r1.getParent()     // Catch:{ Exception -> 0x00e0 }
                            java.lang.String unused = r7.f3746 = r1     // Catch:{ Exception -> 0x00e0 }
                            com.lp.BinderActivity$2 r7 = com.lp.BinderActivity.AnonymousClass2.this     // Catch:{ Exception -> 0x00e0 }
                            com.lp.BinderActivity r7 = com.lp.BinderActivity.this     // Catch:{ Exception -> 0x00e0 }
                            com.lp.BinderActivity$2 r1 = com.lp.BinderActivity.AnonymousClass2.this     // Catch:{ Exception -> 0x00e0 }
                            com.lp.BinderActivity r1 = com.lp.BinderActivity.this     // Catch:{ Exception -> 0x00e0 }
                            java.lang.String r1 = r1.f3746     // Catch:{ Exception -> 0x00e0 }
                            android.widget.RelativeLayout r2 = r9     // Catch:{ Exception -> 0x00e0 }
                            android.view.View r2 = r2.findViewById(r0)     // Catch:{ Exception -> 0x00e0 }
                            android.widget.ListView r2 = (android.widget.ListView) r2     // Catch:{ Exception -> 0x00e0 }
                            r7.m5821(r1, r2)     // Catch:{ Exception -> 0x00e0 }
                            goto L_0x0100
                        L_0x00e0:
                            com.lp.BinderActivity$2 r7 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r7 = com.lp.BinderActivity.this
                            java.lang.String r1 = com.lp.C0987.f4438
                            java.lang.String unused = r7.f3746 = r1
                            com.lp.BinderActivity$2 r7 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r7 = com.lp.BinderActivity.this
                            com.lp.BinderActivity$2 r1 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r1 = com.lp.BinderActivity.this
                            java.lang.String r1 = r1.f3746
                            android.widget.RelativeLayout r2 = r9
                            android.view.View r0 = r2.findViewById(r0)
                            android.widget.ListView r0 = (android.widget.ListView) r0
                            r7.m5821(r1, r0)
                        L_0x0100:
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.lp.BinderActivity.AnonymousClass2.AnonymousClass2.onClick(android.view.View):void");
                    }
                };
                AnonymousClass3 r6 = new View.OnClickListener() {
                    /* JADX WARNING: Code restructure failed: missing block: B:8:?, code lost:
                        return;
                     */
                    /* JADX WARNING: Failed to process nested try/catch */
                    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x00a2 */
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public void onClick(android.view.View r5) {
                        /*
                            r4 = this;
                            com.lp.BinderActivity$2 r5 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r5 = com.lp.BinderActivity.this
                            android.content.Context r5 = r5.f3741
                            r0 = 2131427366(0x7f0b0026, float:1.8476346E38)
                            r1 = 0
                            android.view.View r5 = android.view.View.inflate(r5, r0, r1)
                            android.widget.LinearLayout r5 = (android.widget.LinearLayout) r5
                            com.lp.ʼ r0 = new com.lp.ʼ
                            com.lp.BinderActivity$2 r1 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r1 = com.lp.BinderActivity.this
                            android.content.Context r1 = r1.f3741
                            r2 = 1
                            r0.<init>(r1, r2)
                            com.lp.ʼ r0 = r0.m5940(r5)
                            android.app.Dialog r0 = r0.m5947()
                            r1 = 0
                            r0.setCancelable(r1)
                            com.lp.BinderActivity$2$3$1 r1 = new com.lp.BinderActivity$2$3$1
                            r1.<init>()
                            r0.setOnKeyListener(r1)
                            com.chelpus.C0815.m5156(r0)
                            com.lp.BinderActivity$2 r1 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r1 = com.lp.BinderActivity.this
                            r2 = 2131296487(0x7f0900e7, float:1.8210892E38)
                            android.view.View r2 = r0.findViewById(r2)
                            android.widget.TextView r2 = (android.widget.TextView) r2
                            android.widget.TextView unused = r1.f3747 = r2
                            r1 = 2131296327(0x7f090047, float:1.8210568E38)
                            android.view.View r1 = r0.findViewById(r1)
                            android.widget.Button r1 = (android.widget.Button) r1
                            r2 = 2131296320(0x7f090040, float:1.8210553E38)
                            android.view.View r2 = r0.findViewById(r2)
                            android.widget.Button r2 = (android.widget.Button) r2
                            com.lp.BinderActivity$2$3$2 r3 = new com.lp.BinderActivity$2$3$2
                            r3.<init>(r5)
                            r2.setOnClickListener(r3)
                            com.lp.BinderActivity$2$3$3 r2 = new com.lp.BinderActivity$2$3$3
                            r2.<init>(r0)
                            r1.setOnClickListener(r2)
                            com.lp.BinderActivity$2 r0 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r0 = com.lp.BinderActivity.this
                            java.lang.String r1 = "/"
                            java.lang.String unused = r0.f3746 = r1
                            r0 = 2131296445(0x7f0900bd, float:1.8210807E38)
                            android.view.View r1 = r5.findViewById(r0)
                            android.widget.ListView r1 = (android.widget.ListView) r1
                            com.lp.BinderActivity$2$3$4 r2 = new com.lp.BinderActivity$2$3$4
                            r2.<init>()
                            r1.setOnItemClickListener(r2)
                            com.lp.BinderActivity$2 r1 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r1 = com.lp.BinderActivity.this
                            android.view.View r2 = r5.findViewById(r0)
                            android.widget.ListView r2 = (android.widget.ListView) r2
                            android.widget.ListView unused = r1.f3749 = r2
                            com.lp.BinderActivity$2 r1 = com.lp.BinderActivity.AnonymousClass2.this     // Catch:{ Exception -> 0x00a2 }
                            com.lp.BinderActivity r1 = com.lp.BinderActivity.this     // Catch:{ Exception -> 0x00a2 }
                            com.lp.BinderActivity$2 r2 = com.lp.BinderActivity.AnonymousClass2.this     // Catch:{ Exception -> 0x00a2 }
                            com.lp.BinderActivity r2 = com.lp.BinderActivity.this     // Catch:{ Exception -> 0x00a2 }
                            java.lang.String r2 = r2.f3746     // Catch:{ Exception -> 0x00a2 }
                            android.view.View r5 = r5.findViewById(r0)     // Catch:{ Exception -> 0x00a2 }
                            android.widget.ListView r5 = (android.widget.ListView) r5     // Catch:{ Exception -> 0x00a2 }
                            r1.m5821(r2, r5)     // Catch:{ Exception -> 0x00a2 }
                            goto L_0x00ec
                        L_0x00a2:
                            com.lp.BinderActivity$2 r5 = com.lp.BinderActivity.AnonymousClass2.this     // Catch:{ Exception -> 0x00cc }
                            com.lp.BinderActivity r5 = com.lp.BinderActivity.this     // Catch:{ Exception -> 0x00cc }
                            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x00cc }
                            java.lang.String r2 = com.lp.C0987.f4438     // Catch:{ Exception -> 0x00cc }
                            r1.<init>(r2)     // Catch:{ Exception -> 0x00cc }
                            java.lang.String r1 = r1.getParent()     // Catch:{ Exception -> 0x00cc }
                            java.lang.String unused = r5.f3746 = r1     // Catch:{ Exception -> 0x00cc }
                            com.lp.BinderActivity$2 r5 = com.lp.BinderActivity.AnonymousClass2.this     // Catch:{ Exception -> 0x00cc }
                            com.lp.BinderActivity r5 = com.lp.BinderActivity.this     // Catch:{ Exception -> 0x00cc }
                            com.lp.BinderActivity$2 r1 = com.lp.BinderActivity.AnonymousClass2.this     // Catch:{ Exception -> 0x00cc }
                            com.lp.BinderActivity r1 = com.lp.BinderActivity.this     // Catch:{ Exception -> 0x00cc }
                            java.lang.String r1 = r1.f3746     // Catch:{ Exception -> 0x00cc }
                            android.widget.RelativeLayout r2 = r9     // Catch:{ Exception -> 0x00cc }
                            android.view.View r2 = r2.findViewById(r0)     // Catch:{ Exception -> 0x00cc }
                            android.widget.ListView r2 = (android.widget.ListView) r2     // Catch:{ Exception -> 0x00cc }
                            r5.m5821(r1, r2)     // Catch:{ Exception -> 0x00cc }
                            goto L_0x00ec
                        L_0x00cc:
                            com.lp.BinderActivity$2 r5 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r5 = com.lp.BinderActivity.this
                            java.lang.String r1 = com.lp.C0987.f4438
                            java.lang.String unused = r5.f3746 = r1
                            com.lp.BinderActivity$2 r5 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r5 = com.lp.BinderActivity.this
                            com.lp.BinderActivity$2 r1 = com.lp.BinderActivity.AnonymousClass2.this
                            com.lp.BinderActivity r1 = com.lp.BinderActivity.this
                            java.lang.String r1 = r1.f3746
                            android.widget.RelativeLayout r2 = r9
                            android.view.View r0 = r2.findViewById(r0)
                            android.widget.ListView r0 = (android.widget.ListView) r0
                            r5.m5821(r1, r0)
                        L_0x00ec:
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.lp.BinderActivity.AnonymousClass2.AnonymousClass3.onClick(android.view.View):void");
                    }
                };
                ((Button) relativeLayout.findViewById(R.id.button_browser1)).setOnClickListener(r5);
                ((Button) relativeLayout.findViewById(R.id.button_browser2)).setOnClickListener(r6);
                ((Button) relativeLayout.findViewById(R.id.button1)).setOnClickListener(r4);
                editText.setText("/mnt/sdcard/external_sd/Android/");
                editText2.setText("/mnt/sdcard/Android/");
                BinderActivity binderActivity = BinderActivity.this;
                binderActivity.f3742 = new C0959(binderActivity.f3741).m5938(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                    }
                }).m5952(true).m5936(17301659).m5940(relativeLayout).m5947();
                C0815.m5156(BinderActivity.this.f3742);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static ArrayList<C0963> m5819(Context context) {
        ArrayList<C0963> arrayList = new ArrayList<>();
        File file = new File(context.getDir("binder", 0) + "/bind.txt");
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            FileInputStream fileInputStream = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream, "UTF-8"));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                String[] split = readLine.split(";");
                if (split.length == 2) {
                    C0987.m6060((Object) (split[0] + " ; " + split[1]));
                    arrayList.add(new C0963(split[0], split[1]));
                }
            }
            fileInputStream.close();
        } catch (FileNotFoundException unused) {
            C0987.m6060((Object) "Not found bind.txt");
        } catch (IOException e) {
            C0987.m6060((Object) (BuildConfig.FLAVOR + e));
        }
        return arrayList;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m5822(ArrayList<C0963> arrayList, Context context) {
        File file = new File(context.getDir("binder", 0) + "/bind.txt");
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            StringBuilder sb = new StringBuilder();
            Iterator<C0963> it = arrayList.iterator();
            while (it.hasNext()) {
                C0963 next = it.next();
                sb.append(next.f4241 + ";" + next.f4242 + "\n");
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(sb.toString().getBytes());
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5821(String str, ListView listView) {
        this.f3747.setText(C0815.m5205((int) R.string.binder_browser_current_dir) + " " + str);
        this.f3748 = new C0938(InternalZipConstants.ZIP_FILE_SEPARATOR, str);
        ArrayList arrayList = new ArrayList();
        File file = new File(str);
        File[] listFiles = file.listFiles();
        if (!str.equals(this.f3746)) {
            arrayList.add(new C0938(this.f3746));
            if (file.getParent() != null) {
                arrayList.add(new C0938("../", file.getParent()));
            }
        }
        if (listFiles != null && listFiles.length > 0) {
            for (File file2 : listFiles) {
                if (file2.canRead() && (file2.isDirectory() || file2.toString().toLowerCase().endsWith(".apk"))) {
                    arrayList.add(new C0938(file2.toString()));
                }
            }
        }
        AnonymousClass3 r6 = new ArrayAdapter<C0938>(this, R.layout.file_browser_row, arrayList) {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
             arg types: [?, android.view.ViewGroup, int]
             candidates:
              ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
              ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
            public View getView(int i, View view, ViewGroup viewGroup) {
                C0938 r0 = (C0938) getItem(i);
                if (view == null) {
                    view = BinderActivity.this.getLayoutInflater().inflate((int) R.layout.file_browser_row, viewGroup, false);
                }
                TextView textView = (TextView) view.findViewById(R.id.rowtext);
                textView.setTextAppearance(BinderActivity.this.f3741, C0987.m6076());
                textView.setText(r0.f3794);
                ImageView imageView = (ImageView) view.findViewById(R.id.image);
                if (i == 0 || i == 1) {
                    try {
                        if (((C0938) getItem(1)).f3794.equals("../")) {
                            imageView.setImageDrawable(BinderActivity.this.getResources().getDrawable(R.drawable.fb_back));
                            return view;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } catch (OutOfMemoryError e2) {
                        e2.printStackTrace();
                        System.gc();
                    }
                }
                if (new File(r0.f3795).isDirectory()) {
                    imageView.setImageDrawable(BinderActivity.this.getResources().getDrawable(R.drawable.fb_folder));
                } else {
                    imageView.setImageDrawable(BinderActivity.this.getResources().getDrawable(R.drawable.file_icon));
                }
                return view;
            }
        };
        r6.sort(new C0939());
        listView.setAdapter((ListAdapter) r6);
        r6.notifyDataSetChanged();
    }

    /* renamed from: com.lp.BinderActivity$ʼ  reason: contains not printable characters */
    private final class C0939 implements Comparator<C0938> {
        private C0939() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int compare(C0938 r3, C0938 r4) {
            if (r3 == null || r4 == null) {
                throw new ClassCastException();
            }
            try {
                if (new File(r3.f3795).isDirectory() && new File(r4.f3795).isDirectory()) {
                    return r3.f3795.compareToIgnoreCase(r4.f3795);
                }
                if (new File(r3.f3795).isDirectory() && !new File(r4.f3795).isDirectory()) {
                    return -1;
                }
                if (!new File(r3.f3795).isDirectory() && !new File(r4.f3795).isDirectory()) {
                    return r3.f3795.compareToIgnoreCase(r4.f3795);
                }
                if (new File(r3.f3795).isDirectory() || !new File(r4.f3795).isDirectory()) {
                    return r3.f3795.compareToIgnoreCase(r4.f3795);
                }
                return 1;
            } catch (Exception unused) {
                return 0;
            }
        }
    }

    /* renamed from: com.lp.BinderActivity$ʻ  reason: contains not printable characters */
    public class C0938 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public String f3793 = BuildConfig.FLAVOR;

        /* renamed from: ʼ  reason: contains not printable characters */
        public String f3794 = BuildConfig.FLAVOR;

        /* renamed from: ʽ  reason: contains not printable characters */
        public String f3795 = BuildConfig.FLAVOR;

        public C0938(String str) {
            this.f3795 = str;
            this.f3793 = new File(str).getPath();
            this.f3794 = new File(str).getName();
        }

        public C0938(String str, String str2) {
            this.f3795 = str2;
            this.f3793 = str2;
            this.f3794 = str;
        }
    }
}
