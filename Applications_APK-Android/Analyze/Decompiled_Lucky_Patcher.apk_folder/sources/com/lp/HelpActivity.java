package com.lp;

import android.app.Activity;
import android.app.LocalActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;
import ru.pKkcGXHI.kKSaIWSZS.R;

public class HelpActivity extends Activity {

    /* renamed from: ʻ  reason: contains not printable characters */
    TabHost f3801;

    /* renamed from: ʼ  reason: contains not printable characters */
    LocalActivityManager f3802;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.help);
        m5828(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.f3802.dispatchResume();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.f3802.dispatchPause(isFinishing());
        super.onPause();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m5828(Bundle bundle) {
        Resources resources = getResources();
        this.f3801 = (TabHost) findViewById(16908306);
        this.f3802 = new LocalActivityManager(this, false);
        this.f3802.dispatchCreate(bundle);
        this.f3801.setup(this.f3802);
        TabHost tabHost = (TabHost) findViewById(16908306);
        tabHost.getTabWidget().setDividerDrawable((int) R.drawable.tab_divider);
        View r1 = m5827(tabHost.getContext(), resources.getString(R.string.help_common));
        View r0 = m5827(tabHost.getContext(), resources.getString(R.string.help_custom));
        tabHost.addTab(tabHost.newTabSpec("Common").setIndicator(r1).setContent(new Intent().setClass(this, HelpCommon.class)));
        tabHost.addTab(tabHost.newTabSpec("Create").setIndicator(r0).setContent(new Intent().setClass(this, HelpCustom.class)));
        tabHost.setCurrentTab(0);
        tabHost.getTabWidget().setCurrentTab(0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static View m5827(Context context, String str) {
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.tabs_bg, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.tabsText)).setText(str);
        return inflate;
    }
}
