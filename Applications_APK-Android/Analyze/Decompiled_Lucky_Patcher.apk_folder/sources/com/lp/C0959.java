package com.lp;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.chelpus.C0815;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ʼ  reason: contains not printable characters */
/* compiled from: AlertDlg */
public class C0959 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public Dialog f4171 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    public View f4172 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    public Context f4173 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean f4174 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean f4175 = false;

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean f4176 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    public TextView f4177 = null;

    /* renamed from: ˉ  reason: contains not printable characters */
    public ArrayAdapter f4178 = null;

    public C0959(Context context) {
        this.f4173 = context;
        this.f4171 = new Dialog(context);
        this.f4171.requestWindowFeature(1);
        this.f4171.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        this.f4171.getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            /* renamed from: ʻ  reason: contains not printable characters */
            int f4179 = 0;

            /* renamed from: ʼ  reason: contains not printable characters */
            int f4180 = 0;

            /* renamed from: ʽ  reason: contains not printable characters */
            int f4181 = 255;

            public void onGlobalLayout() {
                Window window = C0959.this.f4171.getWindow();
                int width = window.getDecorView().getWidth();
                window.getDecorView().getHeight();
                int i = C0987.m6069().getDisplayMetrics().heightPixels;
                int i2 = C0987.m6069().getDisplayMetrics().widthPixels;
                if (i > i2) {
                    this.f4181 = 0;
                } else {
                    this.f4181 = 1;
                }
                int i3 = this.f4180;
                if (i3 == 0 || i3 != i2) {
                    this.f4179 = 0;
                    this.f4180 = i2;
                }
                int i4 = this.f4179;
                if (i4 == 0 || width != i4) {
                    window.setGravity(17);
                    WindowManager.LayoutParams attributes = window.getAttributes();
                    float f = (float) i2;
                    double r3 = (double) C0815.m5117(f);
                    Double.isNaN(r3);
                    int i5 = (int) (r3 * 0.98d);
                    double r6 = (double) C0815.m5117(f);
                    Double.isNaN(r6);
                    int i6 = (int) (r6 * 0.75d);
                    int i7 = this.f4181;
                    if (i7 == 0) {
                        attributes.width = (int) C0815.m5202((float) i5);
                        this.f4179 = attributes.width;
                    } else if (i7 == 1) {
                        attributes.width = (int) C0815.m5202((float) i6);
                        this.f4179 = attributes.width;
                    }
                    this.f4179 = attributes.width;
                    if (C0815.m5117((float) attributes.width) < 330.0f && C0815.m5117((float) this.f4180) >= 330.0f) {
                        attributes.width = (int) C0815.m5202(330.0f);
                        this.f4179 = attributes.width;
                    }
                    window.setAttributes(attributes);
                }
            }
        });
        this.f4171.setContentView((int) R.layout.alert_dialog);
        this.f4177 = (TextView) this.f4171.findViewById(R.id.dialog_message);
        this.f4171.setCancelable(true);
    }

    public C0959(Context context, boolean z) {
        this.f4173 = context;
        this.f4171 = new Dialog(context, R.style.full_screen_dialog) {
            /* access modifiers changed from: protected */
            public void onCreate(Bundle bundle) {
                super.onCreate(bundle);
                ((LinearLayout) findViewById(R.id.alert_dialog_body)).setBackgroundColor(Color.parseColor("#000000"));
                getWindow().setLayout(-1, -1);
            }
        };
        this.f4171.setCanceledOnTouchOutside(false);
        this.f4171.setContentView((int) R.layout.alert_dialog);
        this.f4171.setCancelable(true);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0959 m5940(View view) {
        LinearLayout linearLayout = (LinearLayout) this.f4171.findViewById(R.id.button_alert_layout);
        ((LinearLayout) this.f4171.findViewById(R.id.message_layout)).addView(view);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0959 m5936(int i) {
        ((LinearLayout) this.f4171.findViewById(R.id.title_layout)).setVisibility(0);
        ImageView imageView = (ImageView) this.f4171.findViewById(R.id.title_icon);
        imageView.setVisibility(0);
        imageView.setImageDrawable(this.f4173.getResources().getDrawable(i));
        ((TextView) this.f4171.findViewById(R.id.line0)).setVisibility(0);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0959 m5943(String str) {
        ((LinearLayout) this.f4171.findViewById(R.id.title_layout)).setVisibility(0);
        ((TextView) this.f4171.findViewById(R.id.title_text)).setText(str);
        ((TextView) this.f4171.findViewById(R.id.line0)).setVisibility(0);
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0959 m5948(int i) {
        ((LinearLayout) this.f4171.findViewById(R.id.title_layout)).setVisibility(0);
        ((TextView) this.f4171.findViewById(R.id.title_text)).setText(i);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0959 m5935() {
        ((LinearLayout) this.f4171.findViewById(R.id.alert_dialog_body)).setPadding(0, 0, 0, 0);
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0959 m5950(String str) {
        TextView textView = (TextView) this.f4171.findViewById(R.id.dialog_message);
        ((ScrollView) this.f4171.findViewById(R.id.dialog_message_container)).setVisibility(0);
        textView.setMovementMethod(new ScrollingMovementMethod());
        textView.setText(str);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0959 m5939(SpannableStringBuilder spannableStringBuilder) {
        TextView textView = (TextView) this.f4171.findViewById(R.id.dialog_message);
        ((ScrollView) this.f4171.findViewById(R.id.dialog_message_container)).setVisibility(0);
        textView.setMovementMethod(new ScrollingMovementMethod());
        textView.setText(spannableStringBuilder);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0959 m5945(String str, CompoundButton.OnCheckedChangeListener onCheckedChangeListener, boolean z) {
        LinearLayout linearLayout = (LinearLayout) this.f4171.findViewById(R.id.checkbox_layout);
        linearLayout.setVisibility(0);
        CheckBox checkBox = (CheckBox) linearLayout.findViewById(R.id.checkBox_alert);
        checkBox.setText(str);
        checkBox.setOnCheckedChangeListener(onCheckedChangeListener);
        checkBox.setChecked(z);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0959 m5946(boolean z) {
        this.f4176 = z;
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0959 m5952(boolean z) {
        this.f4171.setCancelable(z);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0959 m5944(String str, final DialogInterface.OnClickListener onClickListener) {
        Button button = (Button) this.f4171.findViewById(R.id.button_yes);
        button.setVisibility(0);
        button.setText(str);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DialogInterface.OnClickListener onClickListener = onClickListener;
                if (onClickListener != null) {
                    onClickListener.onClick(C0959.this.f4171, -1);
                }
                C0959.this.m5956();
            }
        });
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0959 m5951(String str, final DialogInterface.OnClickListener onClickListener) {
        Button button = (Button) this.f4171.findViewById(R.id.button_neutral);
        button.setVisibility(0);
        button.setText(str);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DialogInterface.OnClickListener onClickListener = onClickListener;
                if (onClickListener != null) {
                    onClickListener.onClick(C0959.this.f4171, -3);
                }
                C0959.this.m5956();
            }
        });
        return this;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public C0959 m5954(String str, final DialogInterface.OnClickListener onClickListener) {
        Button button = (Button) this.f4171.findViewById(R.id.button_no);
        button.setVisibility(0);
        button.setText(str);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DialogInterface.OnClickListener onClickListener = onClickListener;
                if (onClickListener != null) {
                    onClickListener.onClick(C0959.this.f4171, -2);
                }
                C0959.this.m5956();
            }
        });
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0959 m5937(int i, final DialogInterface.OnClickListener onClickListener) {
        Button button = (Button) this.f4171.findViewById(R.id.button_yes);
        button.setVisibility(0);
        button.setText(i);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DialogInterface.OnClickListener onClickListener = onClickListener;
                if (onClickListener != null) {
                    onClickListener.onClick(C0959.this.f4171, -1);
                }
                C0959.this.m5956();
            }
        });
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0959 m5949(int i, final DialogInterface.OnClickListener onClickListener) {
        Button button = (Button) this.f4171.findViewById(R.id.button_neutral);
        button.setVisibility(0);
        button.setText(i);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DialogInterface.OnClickListener onClickListener = onClickListener;
                if (onClickListener != null) {
                    onClickListener.onClick(C0959.this.f4171, -3);
                }
                C0959.this.m5956();
            }
        });
        return this;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public C0959 m5953(int i, final DialogInterface.OnClickListener onClickListener) {
        Button button = (Button) this.f4171.findViewById(R.id.button_no);
        button.setVisibility(0);
        button.setText(i);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DialogInterface.OnClickListener onClickListener = onClickListener;
                if (onClickListener != null) {
                    onClickListener.onClick(C0959.this.f4171, -2);
                }
                C0959.this.m5956();
            }
        });
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0959 m5938(DialogInterface.OnCancelListener onCancelListener) {
        this.f4171.setOnCancelListener(onCancelListener);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0959 m5941(final ArrayAdapter arrayAdapter, final AdapterView.OnItemClickListener onItemClickListener) {
        this.f4178 = arrayAdapter;
        ListView listView = (ListView) this.f4171.findViewById(R.id.dialog_listview);
        LinearLayout linearLayout = (LinearLayout) this.f4171.findViewById(R.id.button_alert_layout);
        listView.setVisibility(0);
        listView.setAdapter((ListAdapter) arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                onItemClickListener.onItemClick(adapterView, view, i, j);
                arrayAdapter.notifyDataSetChanged();
                if (!C0959.this.f4174) {
                    C0959.this.m5956();
                }
            }
        });
        listView.setDivider(new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[]{868199252, 868199252, 868199252}));
        listView.setDividerHeight(3);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0959 m5942(final C0960 r5, final AdapterView.OnItemClickListener onItemClickListener) {
        this.f4178 = r5;
        ListView listView = (ListView) this.f4171.findViewById(R.id.dialog_listview);
        final EditText editText = (EditText) this.f4171.findViewById(R.id.editTextFilter);
        ((Button) this.f4171.findViewById(R.id.button_clear)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    if (C0959.this.f4178 != null) {
                        C0959.this.f4178.getFilter().filter(BuildConfig.FLAVOR);
                        C0959.this.f4178.notifyDataSetChanged();
                    }
                    editText.setText(BuildConfig.FLAVOR);
                } catch (Exception unused) {
                }
            }
        });
        editText.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void afterTextChanged(Editable editable) {
                try {
                    if (C0959.this.f4178 != null) {
                        C0959.this.f4178.getFilter().filter(editable.toString());
                        C0959.this.f4178.notifyDataSetChanged();
                    }
                } catch (Exception unused) {
                }
            }
        });
        LinearLayout linearLayout = (LinearLayout) this.f4171.findViewById(R.id.button_alert_layout);
        listView.setVisibility(0);
        ((LinearLayout) this.f4171.findViewById(R.id.filter_dialog)).setVisibility(0);
        listView.setAdapter((ListAdapter) r5);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                onItemClickListener.onItemClick(adapterView, view, i, j);
                r5.notifyDataSetChanged();
                if (!C0959.this.f4174) {
                    C0959.this.m5956();
                }
            }
        });
        listView.setDivider(new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[]{868199252, 868199252, 868199252}));
        listView.setDividerHeight(3);
        return this;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public C0959 m5955(boolean z) {
        this.f4174 = z;
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Dialog m5947() {
        if (this.f4176 && !C0987.f4536.contains(this.f4171)) {
            this.f4171.setOnDismissListener(new DialogInterface.OnDismissListener() {
                public void onDismiss(DialogInterface dialogInterface) {
                    if (C0987.f4536.contains(dialogInterface)) {
                        C0987.f4536.remove(dialogInterface);
                        if (C0987.f4536.size() > 0) {
                            C0987.f4536.get(0).show();
                        }
                    }
                }
            });
            C0987.f4536.add(this.f4171);
        }
        return this.f4171;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m5956() {
        Dialog dialog = this.f4171;
        if (dialog != null && !this.f4175) {
            dialog.dismiss();
        }
    }
}
