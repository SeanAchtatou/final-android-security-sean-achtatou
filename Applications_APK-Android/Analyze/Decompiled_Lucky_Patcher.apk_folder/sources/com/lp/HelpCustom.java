package com.lp;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import ru.pKkcGXHI.kKSaIWSZS.R;

public class HelpCustom extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        WebView webView = new WebView(this);
        setContentView(webView);
        webView.getSettings().setDefaultFontSize(14);
        InputStream openRawResource = getResources().openRawResource(R.raw.help_custom_patch);
        byte[] bArr = new byte[512];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            try {
                int read = openRawResource.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    webView.loadData(byteArrayOutputStream.toString(), "text/html; charset=UTF-8", null);
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
    }
}
