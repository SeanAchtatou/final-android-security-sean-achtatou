package com.lp;

import com.chelpus.C0815;
import com.chelpus.ʻ.ʼ.C0781;
import com.chelpus.ʻ.ʼ.C0785;
import com.lp.ʻ.C0947;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.security.DigestOutputStream;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.TreeMap;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.regex.Pattern;
import java.util.zip.CRC32;
import javax.crypto.Cipher;
import javax.crypto.EncryptedPrivateKeyInfo;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import net.lingala.zip4j.util.InternalZipConstants;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;
import ʼ.ʻ.C1421;
import ʼ.ʻ.C1422;
import ʼ.ʼ.ʻ.C1425;
import ʼ.ʼ.ʻ.C1428;
import ʼ.ʼ.ʻ.C1430;
import ʼ.ʽ.C1434;
import ʼ.ʽ.C1437;
import ʼ.ʽ.C1439;

/* renamed from: com.lp.ﾞ  reason: contains not printable characters */
/* compiled from: ZipSignerLP */
public class C0985 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static C1421 f4412;

    /* renamed from: ʾ  reason: contains not printable characters */
    public static final String[] f4413 = {"auto-testkey", "auto", "auto-none", "media", "platform", "shared", "testkey", "none"};

    /* renamed from: ـ  reason: contains not printable characters */
    private static Pattern f4414 = Pattern.compile("^META-INF/(.*)[.](SF|RSA|DSA)$");

    /* renamed from: ʼ  reason: contains not printable characters */
    Map<String, C1428> f4415 = new HashMap();

    /* renamed from: ʽ  reason: contains not printable characters */
    C1428 f4416 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    String f4417 = "testkey";

    /* renamed from: ˆ  reason: contains not printable characters */
    Map<String, String> f4418 = new HashMap();

    /* renamed from: ˈ  reason: contains not printable characters */
    C0986 f4419 = new C0986();

    /* renamed from: ˉ  reason: contains not printable characters */
    boolean f4420 = false;

    /* renamed from: ˊ  reason: contains not printable characters */
    ArrayList<C0781> f4421 = null;

    /* renamed from: ˋ  reason: contains not printable characters */
    ArrayList<String> f4422 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    ArrayList<C0785> f4423 = null;

    /* renamed from: ˏ  reason: contains not printable characters */
    boolean f4424 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f4425 = false;

    /* renamed from: י  reason: contains not printable characters */
    private C1430 f4426 = new C1430();

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1421 m6037() {
        if (f4412 == null) {
            f4412 = C1422.m7912(C0985.class.getName());
        }
        return f4412;
    }

    public C0985() {
        this.f4418.put("aa9852bc5a53272ac8031d49b65e4b0e", "media");
        this.f4418.put("e60418c4b638f20d0721e115674ca11f", "platform");
        this.f4418.put("3e24e49741b60c215c010dc6048fca7d", "shared");
        this.f4418.put("dab2cead827ef5313f28e22b6fa8479f", "testkey");
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public String m6042(String str, Map<String, C1434> map) {
        boolean r0 = m6037().m7908();
        if (!str.startsWith("auto")) {
            return str;
        }
        String str2 = null;
        for (Map.Entry next : map.entrySet()) {
            String str3 = (String) next.getKey();
            if (str3.startsWith("META-INF/") && str3.endsWith(".RSA")) {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                byte[] r3 = ((C1434) next.getValue()).m7957();
                if (r3.length < 1458) {
                    break;
                }
                instance.update(r3, 0, 1458);
                byte[] digest = instance.digest();
                StringBuilder sb = new StringBuilder();
                int length = digest.length;
                for (int i = 0; i < length; i++) {
                    sb.append(String.format("%02x", Byte.valueOf(digest[i])));
                }
                String sb2 = sb.toString();
                String str4 = this.f4418.get(sb2);
                if (r0) {
                    if (str4 != null) {
                        m6037().m7911(String.format("Auto-determined key=%s using md5=%s", str4, sb2));
                    } else {
                        m6037().m7911(String.format("Auto key determination failed for md5=%s", sb2));
                    }
                }
                if (str4 != null) {
                    return str4;
                }
                str2 = str4;
            }
        }
        if (str.equals("auto-testkey")) {
            if (!r0) {
                return "testkey";
            }
            m6037().m7911("Falling back to key=" + str2);
            return "testkey";
        } else if (!str.equals("auto-none")) {
            return null;
        } else {
            if (!r0) {
                return "none";
            }
            m6037().m7911("Unable to determine key, returning: none");
            return "none";
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m6045(String str) {
        this.f4416 = this.f4415.get(str);
        if (this.f4416 == null) {
            this.f4416 = new C1428();
            this.f4416.m7925(str);
            this.f4415.put(str, this.f4416);
            if (!"none".equals(str)) {
                this.f4426.m7937(1, "Loading certificate and private key");
                Class<?> cls = getClass();
                this.f4416.m7926(m6043(cls.getResource("/keys/" + str + ".pk8"), (String) null));
                Class<?> cls2 = getClass();
                this.f4416.m7927(m6044(cls2.getResource("/keys/" + str + ".x509.pem")));
                Class<?> cls3 = getClass();
                URL resource = cls3.getResource("/keys/" + str + ".sbt");
                if (resource != null) {
                    this.f4416.m7928(m6051(resource));
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m6048(String str, X509Certificate x509Certificate, PrivateKey privateKey, byte[] bArr) {
        this.f4416 = new C1428(str, x509Certificate, privateKey, bArr);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public X509Certificate m6044(URL url) {
        InputStream openStream = url.openStream();
        try {
            return (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(openStream);
        } finally {
            openStream.close();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private KeySpec m6035(byte[] bArr, String str) {
        try {
            EncryptedPrivateKeyInfo encryptedPrivateKeyInfo = new EncryptedPrivateKeyInfo(bArr);
            SecretKey generateSecret = SecretKeyFactory.getInstance(encryptedPrivateKeyInfo.getAlgName()).generateSecret(new PBEKeySpec(str.toCharArray()));
            Cipher instance = Cipher.getInstance(encryptedPrivateKeyInfo.getAlgName());
            instance.init(2, generateSecret, encryptedPrivateKeyInfo.getAlgParameters());
            try {
                return encryptedPrivateKeyInfo.getKeySpec(instance);
            } catch (InvalidKeySpecException e) {
                m6037().m7906("signapk: Password for private key may be bad.");
                throw e;
            }
        } catch (IOException unused) {
            return null;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public byte[] m6051(URL url) {
        return m6050(url.openStream());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public byte[] m6050(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[InternalZipConstants.UFT8_NAMES_FLAG];
        int read = inputStream.read(bArr);
        while (read != -1) {
            byteArrayOutputStream.write(bArr, 0, read);
            read = inputStream.read(bArr);
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        inputStream.close();
        byteArrayOutputStream.close();
        return byteArray;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public PrivateKey m6043(URL url, String str) {
        KeySpec r3;
        DataInputStream dataInputStream = new DataInputStream(url.openStream());
        try {
            byte[] r2 = m6050(dataInputStream);
            r3 = m6035(r2, str);
            if (r3 == null) {
                r3 = new PKCS8EncodedKeySpec(r2);
            }
            return KeyFactory.getInstance("RSA").generatePrivate(r3);
        } catch (InvalidKeySpecException unused) {
            return KeyFactory.getInstance("DSA").generatePrivate(r3);
        } finally {
            dataInputStream.close();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private Manifest m6036(Map<String, C1434> map, ArrayList<C0942> arrayList) {
        InputStream inputStream;
        Exception e;
        Manifest manifest = new Manifest();
        Attributes mainAttributes = manifest.getMainAttributes();
        mainAttributes.putValue("Manifest-Version", "1.0");
        mainAttributes.putValue("Created-By", "1.0 (Android SignApk)");
        MessageDigest instance = MessageDigest.getInstance("SHA1");
        byte[] bArr = new byte[512];
        TreeMap treeMap = new TreeMap();
        treeMap.putAll(map);
        if (m6037().m7908()) {
            m6037().m7911("Manifest entries:");
        }
        for (C1434 r3 : treeMap.values()) {
            if (this.f4425) {
                break;
            }
            String r4 = r3.m7962();
            if (!r3.m7961() && !r4.equals("META-INF/MANIFEST.MF") && !r4.equals("META-INF/CERT.SF") && !r4.equals("META-INF/CERT.RSA")) {
                Pattern pattern = f4414;
                if (pattern == null || !pattern.matcher(r4).matches()) {
                    this.f4426.m7937(0, "Generating manifest");
                    InputStream r32 = r3.m7958();
                    Iterator<C0942> it = arrayList.iterator();
                    while (it.hasNext()) {
                        C0942 next = it.next();
                        if (r4.equals(next.f3970.replace(next.f3971, BuildConfig.FLAVOR))) {
                            try {
                                inputStream = new FileInputStream(next.f3970);
                                try {
                                    C0987.m6060((Object) ("LuckyPatcher (signer): Update digest for file to manifest: " + new File(next.f3970).getName()));
                                } catch (Exception e2) {
                                    e = e2;
                                }
                            } catch (Exception e3) {
                                inputStream = r32;
                                e = e3;
                                e.printStackTrace();
                                r32 = inputStream;
                            }
                            r32 = inputStream;
                        }
                    }
                    while (true) {
                        int read = r32.read(bArr);
                        if (read <= 0) {
                            break;
                        }
                        instance.update(bArr, 0, read);
                    }
                    r32.close();
                    Attributes attributes = new Attributes();
                    attributes.putValue("SHA1-Digest", C1425.m7920(instance.digest()));
                    manifest.getEntries().put(r4, attributes);
                }
            }
        }
        return manifest;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m6039(Manifest manifest, OutputStream outputStream) {
        outputStream.write("Signature-Version: 1.0\r\n".getBytes());
        outputStream.write("Created-By: 1.0 (Android SignApk)\r\n".getBytes());
        MessageDigest instance = MessageDigest.getInstance("SHA1");
        PrintStream printStream = new PrintStream(new DigestOutputStream(new ByteArrayOutputStream(), instance), true, "UTF-8");
        manifest.write(printStream);
        printStream.flush();
        outputStream.write(("SHA1-Digest-Manifest: " + C1425.m7920(instance.digest()) + "\r\n\r\n").getBytes());
        for (Map.Entry next : manifest.getEntries().entrySet()) {
            if (!this.f4425) {
                this.f4426.m7937(0, "Generating signature file");
                String str = "Name: " + ((String) next.getKey()) + "\r\n";
                printStream.print(str);
                for (Map.Entry next2 : ((Attributes) next.getValue()).entrySet()) {
                    printStream.print(next2.getKey() + ": " + next2.getValue() + "\r\n");
                }
                printStream.print("\r\n");
                printStream.flush();
                outputStream.write(str.getBytes());
                outputStream.write(("SHA1-Digest: " + C1425.m7920(instance.digest()) + "\r\n\r\n").getBytes());
            } else {
                return;
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m6041(byte[] bArr, byte[] bArr2, X509Certificate x509Certificate, OutputStream outputStream) {
        if (bArr != null) {
            outputStream.write(bArr);
            outputStream.write(bArr2);
            return;
        }
        try {
            Method method = Class.forName("kellinwood.sigblock.SignatureBlockWriter").getMethod("writeSignatureBlock", new byte[1].getClass(), X509Certificate.class, OutputStream.class);
            if (method != null) {
                method.invoke(null, bArr2, x509Certificate, outputStream);
                return;
            }
            throw new IllegalStateException("writeSignatureBlock() method not found.");
        } catch (Exception e) {
            m6037().m7907(e.getMessage(), e);
            throw new IllegalStateException("Failed to invoke writeSignatureBlock(): " + e.getClass().getName() + ": " + e.getMessage());
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m6040(Manifest manifest, Map<String, C1434> map, JarOutputStream jarOutputStream, long j, ArrayList<C0942> arrayList) {
        JarEntry jarEntry;
        Map<String, C1434> map2 = map;
        JarOutputStream jarOutputStream2 = jarOutputStream;
        ArrayList<String> arrayList2 = new ArrayList<>(manifest.getEntries().keySet());
        Collections.sort(arrayList2);
        for (String str : arrayList2) {
            try {
                C0947.m5877(C0815.m5205((int) R.string.patch_step7) + "\n" + str);
            } catch (Throwable unused) {
            }
            C1434 r5 = map2.get(str);
            try {
                if (map2.get(str).m7963() == 0) {
                    jarEntry = new JarEntry(map2.get(str).m7962());
                    jarEntry.setMethod(0);
                    Iterator<C0942> it = arrayList.iterator();
                    boolean z = false;
                    while (it.hasNext()) {
                        C0942 next = it.next();
                        if (str.equals(next.f3970.replace(next.f3971, BuildConfig.FLAVOR))) {
                            try {
                                File file = new File(next.f3970);
                                byte[] bArr = new byte[((int) file.length())];
                                FileInputStream fileInputStream = new FileInputStream(next.f3970);
                                fileInputStream.read(bArr);
                                fileInputStream.close();
                                jarEntry.setCompressedSize(file.length());
                                jarEntry.setSize(file.length());
                                CRC32 crc32 = new CRC32();
                                crc32.update(bArr);
                                jarEntry.setCrc(crc32.getValue());
                                jarEntry.setTime(map2.get(str).m7960());
                                z = true;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    if (!z) {
                        jarEntry.setCompressedSize((long) map2.get(str).m7966());
                        jarEntry.setSize((long) map2.get(str).m7966());
                        CRC32 crc322 = new CRC32();
                        crc322.update(r5.m7957());
                        jarEntry.setCrc(crc322.getValue());
                        jarEntry.setTime(map2.get(str).m7960());
                    }
                } else {
                    jarEntry = new JarEntry(str);
                    jarEntry.setTime(map2.get(str).m7960());
                    jarEntry.setMethod(r5.m7963());
                }
                Iterator<C0942> it2 = arrayList.iterator();
                boolean z2 = false;
                while (it2.hasNext()) {
                    C0942 next2 = it2.next();
                    if (str.equals(next2.f3970.replace(next2.f3971, BuildConfig.FLAVOR))) {
                        try {
                            File file2 = new File(next2.f3970);
                            byte[] bArr2 = new byte[8192];
                            FileInputStream fileInputStream2 = new FileInputStream(next2.f3970);
                            jarOutputStream2.putNextEntry(jarEntry);
                            while (true) {
                                int read = fileInputStream2.read(bArr2);
                                if (read <= 0) {
                                    break;
                                }
                                jarOutputStream2.write(bArr2, 0, read);
                            }
                            jarOutputStream.flush();
                            fileInputStream2.close();
                            try {
                                file2.delete();
                                C0987.m6060((Object) ("LuckyPatcher (signer): Replace file to apk: " + new File(next2.f3970).getName()));
                                z2 = true;
                            } catch (Exception e2) {
                                e = e2;
                                z2 = true;
                                e.printStackTrace();
                            }
                        } catch (Exception e3) {
                            e = e3;
                            e.printStackTrace();
                        }
                    }
                }
                if (!z2) {
                    jarOutputStream2.putNextEntry(jarEntry);
                    InputStream r0 = r5.m7958();
                    byte[] bArr3 = new byte[8192];
                    while (true) {
                        int read2 = r0.read(bArr3);
                        if (read2 <= 0) {
                            break;
                        }
                        jarOutputStream2.write(bArr3, 0, read2);
                    }
                    r0.close();
                    jarOutputStream.flush();
                }
            } catch (Exception e4) {
                C0987.m6060(e4);
                e4.printStackTrace();
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m6038(Map<String, C1434> map, C1439 r17, ArrayList<C0942> arrayList) {
        C1439 r1 = r17;
        for (C1434 next : map.values()) {
            String r4 = next.m7962();
            Iterator<C0942> it = arrayList.iterator();
            boolean z = false;
            while (it.hasNext()) {
                C0942 next2 = it.next();
                if (r4.equals(next2.f3970.replace(next2.f3971, BuildConfig.FLAVOR))) {
                    try {
                        File file = new File(next2.f3970);
                        byte[] bArr = new byte[8192];
                        FileInputStream fileInputStream = new FileInputStream(next2.f3970);
                        C1434 r11 = new C1434(r4);
                        r11.m7951((int) next.m7963());
                        r11.m7952(next.m7960());
                        OutputStream r12 = r11.m7959();
                        CRC32 crc32 = new CRC32();
                        crc32.reset();
                        while (true) {
                            int read = fileInputStream.read(bArr);
                            if (read <= 0) {
                                break;
                            }
                            r12.write(bArr, 0, read);
                            crc32.update(bArr, 0, read);
                        }
                        r12.flush();
                        fileInputStream.close();
                        r1.m7995(r11);
                        z = true;
                        file.delete();
                        C0987.m6060((Object) ("LuckyPatcher (signer): Additional files added! " + next2.f3970));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            if (!z) {
                r1.m7995(next);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m6047(String str, String str2, ArrayList<C0942> arrayList, boolean z) {
        this.f4424 = false;
        this.f4420 = z;
        m6046(str, str2, arrayList);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m6046(String str, String str2, ArrayList<C0942> arrayList) {
        this.f4424 = false;
        if (!new File(str).getCanonicalFile().equals(new File(str2).getCanonicalFile())) {
            Map<String, C1434> r10 = C1437.m7975(str).m7983();
            ArrayList<C0785> arrayList2 = this.f4423;
            if (arrayList2 != null) {
                Iterator<C0785> it = arrayList2.iterator();
                while (it.hasNext()) {
                    C0785 next = it.next();
                    if (r10.containsKey(next.f3205)) {
                        C0987.m6060((Object) ("Found file to zip:" + next.f3205));
                        C1434 r4 = r10.get(next.f3205);
                        if (r4.m7962().equals(next.f3205)) {
                            r4.m7953(next.f3206);
                            r10.put(next.f3206, r4);
                            r10.remove(next.f3205);
                        }
                    }
                }
            }
            ArrayList<String> arrayList3 = this.f4422;
            if (arrayList3 != null) {
                Iterator<String> it2 = arrayList3.iterator();
                while (it2.hasNext()) {
                    String next2 = it2.next();
                    if (r10.containsKey(next2)) {
                        C0987.m6060((Object) ("Found file to zip:" + next2));
                        r10.remove(next2);
                        C0987.m6060((Object) ("Remove file from zip:" + next2));
                    }
                }
            }
            ArrayList<C0781> arrayList4 = this.f4421;
            if (arrayList4 != null) {
                Iterator<C0781> it3 = arrayList4.iterator();
                while (it3.hasNext()) {
                    C0781 next3 = it3.next();
                    if (r10.containsKey(next3.f3174)) {
                        C0987.m6060((Object) ("Found file to zip:" + next3.f3173));
                        r10.remove(next3.f3174);
                        C0987.m6060((Object) ("Remove file from zip:" + next3.f3174));
                    }
                    try {
                        File file = new File(next3.f3173);
                        new FileInputStream(next3.f3173);
                        C1434 r6 = new C1434(next3.f3174, next3.f3173);
                        r6.m7951(0);
                        r6.m7952(file.lastModified());
                        r10.put(next3.f3174, r6);
                        C0987.m6060((Object) ("LuckyPatcher (signer): Add file to entries: " + next3.f3173));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            m6049(r10, new FileOutputStream(str2), str2, arrayList);
            return;
        }
        throw new IllegalArgumentException("Input and output files are the same.  Specify a different name for the output.");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(17:80|81|82|83|84|85|(6:87|88|89|(3:91|92|(2:94|(1:96)))|98|(5:100|(3:102|(2:103|(3:105|(2:107|322)(1:321)|319)(1:320))|108)(3:109|(2:110|(1:112)(1:323))|113)|114|316|189))|115|116|117|(6:119|120|(7:123|124|125|126|(10:128|129|130|131|132|133|134|135|136|326)(2:144|325)|145|121)|324|148|(1:150))(1:151)|152|(4:155|(8:157|158|(2:159|(3:161|162|163)(1:332))|164|165|166|167|330)(2:175|329)|176|153)|328|(4:178|(2:179|(1:181)(1:333))|182|318)(1:317)|189|78) */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x025e, code lost:
        if (r2.m7962().toLowerCase().endsWith(".mf") == false) goto L_0x02eb;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:83:0x0218 */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x02f3 A[SYNTHETIC, Splitter:B:119:0x02f3] */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x03c3 A[Catch:{ Exception -> 0x0483 }] */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x03eb A[Catch:{ Exception -> 0x0483 }] */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x0466 A[Catch:{ Exception -> 0x0483 }] */
    /* JADX WARNING: Removed duplicated region for block: B:294:0x07be  */
    /* JADX WARNING: Removed duplicated region for block: B:317:0x0492 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0220 A[SYNTHETIC, Splitter:B:87:0x0220] */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m6049(java.util.Map<java.lang.String, ʼ.ʽ.C1434> r21, java.io.OutputStream r22, java.lang.String r23, java.util.ArrayList<com.lp.C0942> r24) {
        /*
            r20 = this;
            r8 = r20
            r0 = r21
            r9 = r23
            r7 = r24
            java.lang.String r10 = ":"
            java.lang.String r11 = "none"
            ʼ.ʻ.ʼ r1 = m6037()
            boolean r1 = r1.m7908()
            ʼ.ʼ.ʻ.ˈ r2 = r8.f4426
            r2.m7935()
            ʼ.ʼ.ʻ.ʿ r2 = r8.f4416
            if (r2 != 0) goto L_0x0060
            java.lang.String r2 = r8.f4417
            java.lang.String r3 = "auto"
            boolean r2 = r2.startsWith(r3)
            if (r2 == 0) goto L_0x0058
            java.lang.String r2 = r8.f4417
            java.lang.String r2 = r8.m6042(r2, r0)
            if (r2 == 0) goto L_0x0038
            com.lp.ﾞ$ʻ r3 = r8.f4419
            r3.notifyObservers(r2)
            r8.m6045(r2)
            goto L_0x0060
        L_0x0038:
            ʼ.ʼ.ʻ.ʻ r0 = new ʼ.ʼ.ʻ.ʻ
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unable to auto-select key for signing "
            r1.append(r2)
            java.io.File r2 = new java.io.File
            r2.<init>(r9)
            java.lang.String r2 = r2.getName()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0058:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "No keys configured for signing the file!"
            r0.<init>(r1)
            throw r0
        L_0x0060:
            r2 = 0
            r12 = 2131690077(0x7f0f025d, float:1.9009187E38)
            r13 = 1
            r14 = 0
            ʼ.ʽ.ˈ r3 = new ʼ.ʽ.ˈ     // Catch:{ all -> 0x07a3 }
            r4 = r22
            r3.<init>(r4)     // Catch:{ all -> 0x07a3 }
            java.util.jar.JarOutputStream r15 = new java.util.jar.JarOutputStream     // Catch:{ all -> 0x07a3 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ all -> 0x07a3 }
            r4.<init>(r9)     // Catch:{ all -> 0x07a3 }
            r15.<init>(r4)     // Catch:{ all -> 0x07a3 }
            ʼ.ʼ.ʻ.ʿ r2 = r8.f4416     // Catch:{ all -> 0x07a0 }
            java.lang.String r2 = r2.m7924()     // Catch:{ all -> 0x07a0 }
            boolean r2 = r11.equals(r2)     // Catch:{ all -> 0x07a0 }
            if (r2 == 0) goto L_0x00ff
            ʼ.ʼ.ʻ.ˈ r1 = r8.f4426     // Catch:{ all -> 0x07a0 }
            int r2 = r21.size()     // Catch:{ all -> 0x07a0 }
            r1.m7936(r2)     // Catch:{ all -> 0x07a0 }
            ʼ.ʼ.ʻ.ˈ r1 = r8.f4426     // Catch:{ all -> 0x07a0 }
            r1.m7938(r14)     // Catch:{ all -> 0x07a0 }
            r8.m6038(r0, r3, r7)     // Catch:{ all -> 0x07a0 }
            r3.m7992()     // Catch:{ all -> 0x07a0 }
            java.lang.String r0 = r8.f4417
            boolean r0 = r0.equals(r11)
            if (r0 != 0) goto L_0x00c9
            r15.close()
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x00c9
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            java.lang.String[] r1 = new java.lang.String[r13]     // Catch:{ Throwable -> 0x00c1 }
            java.lang.String r0 = r0.getName()     // Catch:{ Throwable -> 0x00c1 }
            r1[r14] = r0     // Catch:{ Throwable -> 0x00c1 }
            java.lang.String r0 = com.chelpus.C0815.m5139(r12, r1)     // Catch:{ Throwable -> 0x00c1 }
            com.lp.ʻ.C0947.m5877(r0)     // Catch:{ Throwable -> 0x00c1 }
        L_0x00c1:
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            com.chelpus.C0815.m5323(r0)
        L_0x00c9:
            boolean r0 = r8.f4425
            if (r0 == 0) goto L_0x00fe
            if (r9 == 0) goto L_0x00fe
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x00d8 }
            r0.<init>(r9)     // Catch:{ Throwable -> 0x00d8 }
            r0.delete()     // Catch:{ Throwable -> 0x00d8 }
            goto L_0x00fe
        L_0x00d8:
            r0 = move-exception
            ʼ.ʻ.ʼ r1 = m6037()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r0.getClass()
            java.lang.String r3 = r3.getName()
            r2.append(r3)
            r2.append(r10)
            java.lang.String r0 = r0.getMessage()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.m7909(r0)
        L_0x00fe:
            return
        L_0x00ff:
            java.util.Collection r2 = r21.values()     // Catch:{ all -> 0x07a0 }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x07a0 }
            r3 = 0
        L_0x0108:
            boolean r4 = r2.hasNext()     // Catch:{ all -> 0x07a0 }
            java.lang.String r5 = "META-INF/CERT.RSA"
            java.lang.String r6 = "META-INF/CERT.SF"
            java.lang.String r12 = "META-INF/MANIFEST.MF"
            if (r4 == 0) goto L_0x014d
            java.lang.Object r4 = r2.next()     // Catch:{ all -> 0x07a0 }
            ʼ.ʽ.ʼ r4 = (ʼ.ʽ.C1434) r4     // Catch:{ all -> 0x07a0 }
            java.lang.String r14 = r4.m7962()     // Catch:{ all -> 0x07a0 }
            boolean r4 = r4.m7961()     // Catch:{ all -> 0x07a0 }
            if (r4 != 0) goto L_0x0148
            boolean r4 = r14.equals(r12)     // Catch:{ all -> 0x07a0 }
            if (r4 != 0) goto L_0x0148
            boolean r4 = r14.equals(r6)     // Catch:{ all -> 0x07a0 }
            if (r4 != 0) goto L_0x0148
            boolean r4 = r14.equals(r5)     // Catch:{ all -> 0x07a0 }
            if (r4 != 0) goto L_0x0148
            java.util.regex.Pattern r4 = com.lp.C0985.f4414     // Catch:{ all -> 0x07a0 }
            if (r4 == 0) goto L_0x0146
            java.util.regex.Pattern r4 = com.lp.C0985.f4414     // Catch:{ all -> 0x07a0 }
            java.util.regex.Matcher r4 = r4.matcher(r14)     // Catch:{ all -> 0x07a0 }
            boolean r4 = r4.matches()     // Catch:{ all -> 0x07a0 }
            if (r4 != 0) goto L_0x0148
        L_0x0146:
            int r3 = r3 + 3
        L_0x0148:
            r12 = 2131690077(0x7f0f025d, float:1.9009187E38)
            r14 = 0
            goto L_0x0108
        L_0x014d:
            int r3 = r3 + r13
            ʼ.ʼ.ʻ.ˈ r2 = r8.f4426     // Catch:{ all -> 0x07a0 }
            r2.m7936(r3)     // Catch:{ all -> 0x07a0 }
            ʼ.ʼ.ʻ.ˈ r2 = r8.f4426     // Catch:{ all -> 0x07a0 }
            r3 = 0
            r2.m7938(r3)     // Catch:{ all -> 0x07a0 }
            ʼ.ʼ.ʻ.ʿ r2 = r8.f4416     // Catch:{ all -> 0x07a0 }
            java.security.cert.X509Certificate r2 = r2.m7929()     // Catch:{ all -> 0x07a0 }
            java.util.Date r2 = r2.getNotBefore()     // Catch:{ all -> 0x07a0 }
            long r2 = r2.getTime()     // Catch:{ all -> 0x07a0 }
            r16 = 3600000(0x36ee80, double:1.7786363E-317)
            long r13 = r2 + r16
            boolean r2 = r8.f4425     // Catch:{ all -> 0x07a0 }
            if (r2 == 0) goto L_0x01dd
            java.lang.String r0 = r8.f4417
            boolean r0 = r0.equals(r11)
            if (r0 != 0) goto L_0x01a7
            r15.close()
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x01a7
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            r1 = 1
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Throwable -> 0x019f }
            java.lang.String r0 = r0.getName()     // Catch:{ Throwable -> 0x019f }
            r2 = 0
            r1[r2] = r0     // Catch:{ Throwable -> 0x019f }
            r2 = 2131690077(0x7f0f025d, float:1.9009187E38)
            java.lang.String r0 = com.chelpus.C0815.m5139(r2, r1)     // Catch:{ Throwable -> 0x019f }
            com.lp.ʻ.C0947.m5877(r0)     // Catch:{ Throwable -> 0x019f }
        L_0x019f:
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            com.chelpus.C0815.m5323(r0)
        L_0x01a7:
            boolean r0 = r8.f4425
            if (r0 == 0) goto L_0x01dc
            if (r9 == 0) goto L_0x01dc
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x01b6 }
            r0.<init>(r9)     // Catch:{ Throwable -> 0x01b6 }
            r0.delete()     // Catch:{ Throwable -> 0x01b6 }
            goto L_0x01dc
        L_0x01b6:
            r0 = move-exception
            ʼ.ʻ.ʼ r1 = m6037()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r0.getClass()
            java.lang.String r3 = r3.getName()
            r2.append(r3)
            r2.append(r10)
            java.lang.String r0 = r0.getMessage()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.m7909(r0)
        L_0x01dc:
            return
        L_0x01dd:
            boolean r2 = r8.f4420     // Catch:{ all -> 0x07a0 }
            java.lang.String r3 = "\n"
            if (r2 == 0) goto L_0x04ad
            java.util.Collection r0 = r21.values()     // Catch:{ all -> 0x04a8 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x04a8 }
        L_0x01eb:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x04a8 }
            if (r0 == 0) goto L_0x04a4
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x04a8 }
            r2 = r0
            ʼ.ʽ.ʼ r2 = (ʼ.ʽ.C1434) r2     // Catch:{ all -> 0x04a8 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0218 }
            r0.<init>()     // Catch:{ Throwable -> 0x0218 }
            r4 = 2131690076(0x7f0f025c, float:1.9009185E38)
            java.lang.String r4 = com.chelpus.C0815.m5205(r4)     // Catch:{ Throwable -> 0x0218 }
            r0.append(r4)     // Catch:{ Throwable -> 0x0218 }
            r0.append(r3)     // Catch:{ Throwable -> 0x0218 }
            java.lang.String r4 = r2.m7962()     // Catch:{ Throwable -> 0x0218 }
            r0.append(r4)     // Catch:{ Throwable -> 0x0218 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0218 }
            com.lp.ʻ.C0947.m5877(r0)     // Catch:{ Throwable -> 0x0218 }
        L_0x0218:
            boolean r0 = r2.m7961()     // Catch:{ all -> 0x04a8 }
            r4 = 8192(0x2000, float:1.14794E-41)
            if (r0 != 0) goto L_0x02eb
            java.lang.String r0 = r2.m7962()     // Catch:{ all -> 0x07a0 }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ all -> 0x07a0 }
            java.lang.String r5 = ".dsa"
            boolean r0 = r0.endsWith(r5)     // Catch:{ all -> 0x07a0 }
            java.lang.String r5 = ".sf"
            if (r0 != 0) goto L_0x0260
            java.lang.String r0 = r2.m7962()     // Catch:{ all -> 0x07a0 }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ all -> 0x07a0 }
            java.lang.String r6 = ".rsa"
            boolean r0 = r0.endsWith(r6)     // Catch:{ all -> 0x07a0 }
            if (r0 != 0) goto L_0x0260
            java.lang.String r0 = r2.m7962()     // Catch:{ all -> 0x07a0 }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ all -> 0x07a0 }
            boolean r0 = r0.endsWith(r5)     // Catch:{ all -> 0x07a0 }
            if (r0 != 0) goto L_0x0260
            java.lang.String r0 = r2.m7962()     // Catch:{ all -> 0x07a0 }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ all -> 0x07a0 }
            java.lang.String r6 = ".mf"
            boolean r0 = r0.endsWith(r6)     // Catch:{ all -> 0x07a0 }
            if (r0 == 0) goto L_0x02eb
        L_0x0260:
            java.lang.String r0 = r2.m7962()     // Catch:{ all -> 0x07a0 }
            java.lang.String r0 = r0.toUpperCase()     // Catch:{ all -> 0x07a0 }
            java.lang.String r6 = "META-INF/"
            boolean r0 = r0.startsWith(r6)     // Catch:{ all -> 0x07a0 }
            if (r0 == 0) goto L_0x02eb
            java.util.jar.JarEntry r0 = new java.util.jar.JarEntry     // Catch:{ all -> 0x07a0 }
            java.lang.String r6 = r2.m7962()     // Catch:{ all -> 0x07a0 }
            r0.<init>(r6)     // Catch:{ all -> 0x07a0 }
            r0.setTime(r13)     // Catch:{ all -> 0x07a0 }
            r15.putNextEntry(r0)     // Catch:{ all -> 0x07a0 }
            java.io.InputStream r0 = r2.m7958()     // Catch:{ all -> 0x07a0 }
            java.lang.String r2 = r2.m7962()     // Catch:{ all -> 0x07a0 }
            java.lang.String r2 = r2.toLowerCase()     // Catch:{ all -> 0x07a0 }
            boolean r2 = r2.endsWith(r5)     // Catch:{ all -> 0x07a0 }
            if (r2 == 0) goto L_0x02d0
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ all -> 0x07a0 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ all -> 0x07a0 }
            r4.<init>(r0)     // Catch:{ all -> 0x07a0 }
            r2.<init>(r4)     // Catch:{ all -> 0x07a0 }
        L_0x029b:
            java.lang.String r4 = r2.readLine()     // Catch:{ all -> 0x07a0 }
            if (r4 == 0) goto L_0x02c9
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x07a0 }
            r5.<init>()     // Catch:{ all -> 0x07a0 }
            r5.append(r4)     // Catch:{ all -> 0x07a0 }
            java.lang.String r4 = "\r\n"
            r5.append(r4)     // Catch:{ all -> 0x07a0 }
            java.lang.String r4 = r5.toString()     // Catch:{ all -> 0x07a0 }
            java.lang.String r5 = "X-Android-APK-Signed:"
            boolean r5 = r4.contains(r5)     // Catch:{ all -> 0x07a0 }
            if (r5 != 0) goto L_0x029b
            java.lang.String r5 = "UTF8"
            byte[] r5 = r4.getBytes(r5)     // Catch:{ all -> 0x07a0 }
            int r4 = r4.length()     // Catch:{ all -> 0x07a0 }
            r6 = 0
            r15.write(r5, r6, r4)     // Catch:{ all -> 0x07a0 }
            goto L_0x029b
        L_0x02c9:
            r0.close()     // Catch:{ all -> 0x07a0 }
            r15.flush()     // Catch:{ all -> 0x07a0 }
            goto L_0x02e3
        L_0x02d0:
            byte[] r2 = new byte[r4]     // Catch:{ all -> 0x07a0 }
        L_0x02d2:
            int r4 = r0.read(r2)     // Catch:{ all -> 0x07a0 }
            if (r4 <= 0) goto L_0x02dd
            r5 = 0
            r15.write(r2, r5, r4)     // Catch:{ all -> 0x07a0 }
            goto L_0x02d2
        L_0x02dd:
            r0.close()     // Catch:{ all -> 0x07a0 }
            r15.flush()     // Catch:{ all -> 0x07a0 }
        L_0x02e3:
            r22 = r1
            r18 = r3
            r19 = r10
            goto L_0x0492
        L_0x02eb:
            short r0 = r2.m7963()     // Catch:{ Exception -> 0x0485 }
            java.lang.String r5 = ""
            if (r0 != 0) goto L_0x03c3
            java.util.jar.JarEntry r6 = new java.util.jar.JarEntry     // Catch:{ Exception -> 0x0485 }
            java.lang.String r0 = r2.m7962()     // Catch:{ Exception -> 0x0485 }
            r6.<init>(r0)     // Catch:{ Exception -> 0x0485 }
            r12 = 0
            r6.setMethod(r12)     // Catch:{ Exception -> 0x0485 }
            java.util.Iterator r12 = r24.iterator()     // Catch:{ Exception -> 0x0485 }
            r16 = 0
        L_0x0306:
            boolean r0 = r12.hasNext()     // Catch:{ Exception -> 0x0485 }
            if (r0 == 0) goto L_0x0390
            java.lang.Object r0 = r12.next()     // Catch:{ Exception -> 0x0485 }
            com.lp.ʻ r0 = (com.lp.C0942) r0     // Catch:{ Exception -> 0x0485 }
            java.lang.String r4 = r2.m7962()     // Catch:{ Exception -> 0x0485 }
            r22 = r1
            java.lang.String r1 = r0.f3970     // Catch:{ Exception -> 0x038d }
            r17 = r12
            java.lang.String r12 = r0.f3971     // Catch:{ Exception -> 0x038d }
            java.lang.String r1 = r1.replace(r12, r5)     // Catch:{ Exception -> 0x038d }
            boolean r1 = r4.equals(r1)     // Catch:{ Exception -> 0x038d }
            if (r1 == 0) goto L_0x037b
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0372 }
            java.lang.String r4 = r0.f3970     // Catch:{ Exception -> 0x0372 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0372 }
            r18 = r3
            long r3 = r1.length()     // Catch:{ Exception -> 0x0370 }
            int r4 = (int) r3     // Catch:{ Exception -> 0x0370 }
            byte[] r3 = new byte[r4]     // Catch:{ Exception -> 0x0370 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0370 }
            java.lang.String r0 = r0.f3970     // Catch:{ Exception -> 0x0370 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0370 }
            r4.read(r3)     // Catch:{ Exception -> 0x0370 }
            r4.close()     // Catch:{ Exception -> 0x0370 }
            r19 = r10
            long r9 = r1.length()     // Catch:{ Exception -> 0x036e }
            r6.setCompressedSize(r9)     // Catch:{ Exception -> 0x036e }
            long r0 = r1.length()     // Catch:{ Exception -> 0x036e }
            r6.setSize(r0)     // Catch:{ Exception -> 0x036e }
            java.util.zip.CRC32 r0 = new java.util.zip.CRC32     // Catch:{ Exception -> 0x036e }
            r0.<init>()     // Catch:{ Exception -> 0x036e }
            r0.update(r3)     // Catch:{ Exception -> 0x036e }
            long r0 = r0.getValue()     // Catch:{ Exception -> 0x036e }
            r6.setCrc(r0)     // Catch:{ Exception -> 0x036e }
            long r0 = r2.m7960()     // Catch:{ Exception -> 0x036e }
            r6.setTime(r0)     // Catch:{ Exception -> 0x036e }
            r16 = 1
            goto L_0x037f
        L_0x036e:
            r0 = move-exception
            goto L_0x0377
        L_0x0370:
            r0 = move-exception
            goto L_0x0375
        L_0x0372:
            r0 = move-exception
            r18 = r3
        L_0x0375:
            r19 = r10
        L_0x0377:
            r0.printStackTrace()     // Catch:{ Exception -> 0x0483 }
            goto L_0x037f
        L_0x037b:
            r18 = r3
            r19 = r10
        L_0x037f:
            r1 = r22
            r9 = r23
            r12 = r17
            r3 = r18
            r10 = r19
            r4 = 8192(0x2000, float:1.14794E-41)
            goto L_0x0306
        L_0x038d:
            r0 = move-exception
            goto L_0x0488
        L_0x0390:
            r22 = r1
            r18 = r3
            r19 = r10
            if (r16 != 0) goto L_0x03e0
            int r0 = r2.m7966()     // Catch:{ Exception -> 0x0483 }
            long r0 = (long) r0     // Catch:{ Exception -> 0x0483 }
            r6.setCompressedSize(r0)     // Catch:{ Exception -> 0x0483 }
            int r0 = r2.m7966()     // Catch:{ Exception -> 0x0483 }
            long r0 = (long) r0     // Catch:{ Exception -> 0x0483 }
            r6.setSize(r0)     // Catch:{ Exception -> 0x0483 }
            java.util.zip.CRC32 r0 = new java.util.zip.CRC32     // Catch:{ Exception -> 0x0483 }
            r0.<init>()     // Catch:{ Exception -> 0x0483 }
            byte[] r1 = r2.m7957()     // Catch:{ Exception -> 0x0483 }
            r0.update(r1)     // Catch:{ Exception -> 0x0483 }
            long r0 = r0.getValue()     // Catch:{ Exception -> 0x0483 }
            r6.setCrc(r0)     // Catch:{ Exception -> 0x0483 }
            long r0 = r2.m7960()     // Catch:{ Exception -> 0x0483 }
            r6.setTime(r0)     // Catch:{ Exception -> 0x0483 }
            goto L_0x03e0
        L_0x03c3:
            r22 = r1
            r18 = r3
            r19 = r10
            java.util.jar.JarEntry r6 = new java.util.jar.JarEntry     // Catch:{ Exception -> 0x0483 }
            java.lang.String r0 = r2.m7962()     // Catch:{ Exception -> 0x0483 }
            r6.<init>(r0)     // Catch:{ Exception -> 0x0483 }
            long r0 = r2.m7960()     // Catch:{ Exception -> 0x0483 }
            r6.setTime(r0)     // Catch:{ Exception -> 0x0483 }
            short r0 = r2.m7963()     // Catch:{ Exception -> 0x0483 }
            r6.setMethod(r0)     // Catch:{ Exception -> 0x0483 }
        L_0x03e0:
            java.util.Iterator r1 = r24.iterator()     // Catch:{ Exception -> 0x0483 }
            r3 = 0
        L_0x03e5:
            boolean r0 = r1.hasNext()     // Catch:{ Exception -> 0x0483 }
            if (r0 == 0) goto L_0x0464
            java.lang.Object r0 = r1.next()     // Catch:{ Exception -> 0x0483 }
            com.lp.ʻ r0 = (com.lp.C0942) r0     // Catch:{ Exception -> 0x0483 }
            java.lang.String r4 = r2.m7962()     // Catch:{ Exception -> 0x0483 }
            java.lang.String r9 = r0.f3970     // Catch:{ Exception -> 0x0483 }
            java.lang.String r10 = r0.f3971     // Catch:{ Exception -> 0x0483 }
            java.lang.String r9 = r9.replace(r10, r5)     // Catch:{ Exception -> 0x0483 }
            boolean r4 = r4.equals(r9)     // Catch:{ Exception -> 0x0483 }
            if (r4 == 0) goto L_0x045f
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0458 }
            java.lang.String r9 = r0.f3970     // Catch:{ Exception -> 0x0458 }
            r4.<init>(r9)     // Catch:{ Exception -> 0x0458 }
            r9 = 8192(0x2000, float:1.14794E-41)
            byte[] r10 = new byte[r9]     // Catch:{ Exception -> 0x0458 }
            java.io.FileInputStream r9 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0458 }
            java.lang.String r12 = r0.f3970     // Catch:{ Exception -> 0x0458 }
            r9.<init>(r12)     // Catch:{ Exception -> 0x0458 }
            r15.putNextEntry(r6)     // Catch:{ Exception -> 0x0458 }
        L_0x0418:
            int r12 = r9.read(r10)     // Catch:{ Exception -> 0x0458 }
            if (r12 <= 0) goto L_0x0427
            r16 = r1
            r1 = 0
            r15.write(r10, r1, r12)     // Catch:{ Exception -> 0x0456 }
            r1 = r16
            goto L_0x0418
        L_0x0427:
            r16 = r1
            r15.flush()     // Catch:{ Exception -> 0x0456 }
            r9.close()     // Catch:{ Exception -> 0x0456 }
            r4.delete()     // Catch:{ Exception -> 0x0453 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0453 }
            r1.<init>()     // Catch:{ Exception -> 0x0453 }
            java.lang.String r3 = "LuckyPatcher (signer): Replace file to apk: "
            r1.append(r3)     // Catch:{ Exception -> 0x0453 }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0453 }
            java.lang.String r0 = r0.f3970     // Catch:{ Exception -> 0x0453 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0453 }
            java.lang.String r0 = r3.getName()     // Catch:{ Exception -> 0x0453 }
            r1.append(r0)     // Catch:{ Exception -> 0x0453 }
            java.lang.String r0 = r1.toString()     // Catch:{ Exception -> 0x0453 }
            com.lp.C0987.m6060(r0)     // Catch:{ Exception -> 0x0453 }
            r3 = 1
            goto L_0x0461
        L_0x0453:
            r0 = move-exception
            r3 = 1
            goto L_0x045b
        L_0x0456:
            r0 = move-exception
            goto L_0x045b
        L_0x0458:
            r0 = move-exception
            r16 = r1
        L_0x045b:
            r0.printStackTrace()     // Catch:{ Exception -> 0x0483 }
            goto L_0x0461
        L_0x045f:
            r16 = r1
        L_0x0461:
            r1 = r16
            goto L_0x03e5
        L_0x0464:
            if (r3 != 0) goto L_0x0492
            r15.putNextEntry(r6)     // Catch:{ Exception -> 0x0483 }
            java.io.InputStream r0 = r2.m7958()     // Catch:{ Exception -> 0x0483 }
            r1 = 8192(0x2000, float:1.14794E-41)
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x0483 }
        L_0x0471:
            int r2 = r0.read(r1)     // Catch:{ Exception -> 0x0483 }
            if (r2 <= 0) goto L_0x047c
            r3 = 0
            r15.write(r1, r3, r2)     // Catch:{ Exception -> 0x0483 }
            goto L_0x0471
        L_0x047c:
            r0.close()     // Catch:{ Exception -> 0x0483 }
            r15.flush()     // Catch:{ Exception -> 0x0483 }
            goto L_0x0492
        L_0x0483:
            r0 = move-exception
            goto L_0x048c
        L_0x0485:
            r0 = move-exception
            r22 = r1
        L_0x0488:
            r18 = r3
            r19 = r10
        L_0x048c:
            com.lp.C0987.m6060(r0)     // Catch:{ all -> 0x049c }
            r0.printStackTrace()     // Catch:{ all -> 0x049c }
        L_0x0492:
            r1 = r22
            r9 = r23
            r3 = r18
            r10 = r19
            goto L_0x01eb
        L_0x049c:
            r0 = move-exception
            r9 = r23
            r1 = r0
            r10 = r19
            goto L_0x07a6
        L_0x04a4:
            r9 = r23
            goto L_0x072d
        L_0x04a8:
            r0 = move-exception
            r9 = r23
            goto L_0x07a1
        L_0x04ad:
            r18 = r3
            r19 = r10
            java.util.jar.Manifest r2 = r8.m6036(r0, r7)     // Catch:{ all -> 0x079a }
            java.util.jar.JarEntry r3 = new java.util.jar.JarEntry     // Catch:{ all -> 0x079a }
            r3.<init>(r12)     // Catch:{ all -> 0x079a }
            r3.setTime(r13)     // Catch:{ all -> 0x079a }
            r15.putNextEntry(r3)     // Catch:{ all -> 0x079a }
            r2.write(r15)     // Catch:{ all -> 0x079a }
            ʼ.ʼ.ʻ.ˊ r3 = new ʼ.ʼ.ʻ.ˊ     // Catch:{ all -> 0x079a }
            r3.<init>()     // Catch:{ all -> 0x079a }
            ʼ.ʼ.ʻ.ʿ r4 = r8.f4416     // Catch:{ all -> 0x079a }
            java.security.PrivateKey r4 = r4.m7930()     // Catch:{ all -> 0x079a }
            r3.m7940(r4)     // Catch:{ all -> 0x079a }
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x079a }
            r4.<init>()     // Catch:{ all -> 0x079a }
            r8.m6039(r2, r4)     // Catch:{ all -> 0x079a }
            boolean r9 = r8.f4425     // Catch:{ all -> 0x079a }
            if (r9 == 0) goto L_0x0551
            java.lang.String r0 = r8.f4417
            boolean r0 = r0.equals(r11)
            if (r0 != 0) goto L_0x0517
            r15.close()
            java.io.File r0 = new java.io.File
            r9 = r23
            r0.<init>(r9)
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x0519
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            r1 = 1
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Throwable -> 0x050e }
            java.lang.String r0 = r0.getName()     // Catch:{ Throwable -> 0x050e }
            r2 = 0
            r1[r2] = r0     // Catch:{ Throwable -> 0x050e }
            r2 = 2131690077(0x7f0f025d, float:1.9009187E38)
            java.lang.String r0 = com.chelpus.C0815.m5139(r2, r1)     // Catch:{ Throwable -> 0x050e }
            com.lp.ʻ.C0947.m5877(r0)     // Catch:{ Throwable -> 0x050e }
        L_0x050e:
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            com.chelpus.C0815.m5323(r0)
            goto L_0x0519
        L_0x0517:
            r9 = r23
        L_0x0519:
            boolean r0 = r8.f4425
            if (r0 == 0) goto L_0x0550
            if (r9 == 0) goto L_0x0550
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x0528 }
            r0.<init>(r9)     // Catch:{ Throwable -> 0x0528 }
            r0.delete()     // Catch:{ Throwable -> 0x0528 }
            goto L_0x0550
        L_0x0528:
            r0 = move-exception
            ʼ.ʻ.ʼ r1 = m6037()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r0.getClass()
            java.lang.String r3 = r3.getName()
            r2.append(r3)
            r10 = r19
            r2.append(r10)
            java.lang.String r0 = r0.getMessage()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.m7909(r0)
        L_0x0550:
            return
        L_0x0551:
            r9 = r23
            r10 = r19
            byte[] r4 = r4.toByteArray()     // Catch:{ all -> 0x07a0 }
            if (r1 == 0) goto L_0x0587
            ʼ.ʻ.ʼ r12 = m6037()     // Catch:{ all -> 0x07a0 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x07a0 }
            r0.<init>()     // Catch:{ all -> 0x07a0 }
            r22 = r6
            java.lang.String r6 = "Signature File: \n"
            r0.append(r6)     // Catch:{ all -> 0x07a0 }
            java.lang.String r6 = new java.lang.String     // Catch:{ all -> 0x07a0 }
            r6.<init>(r4)     // Catch:{ all -> 0x07a0 }
            r0.append(r6)     // Catch:{ all -> 0x07a0 }
            r6 = r18
            r0.append(r6)     // Catch:{ all -> 0x07a0 }
            java.lang.String r6 = ʼ.ʼ.ʻ.C1426.m7921(r4)     // Catch:{ all -> 0x07a0 }
            r0.append(r6)     // Catch:{ all -> 0x07a0 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x07a0 }
            r12.m7911(r0)     // Catch:{ all -> 0x07a0 }
            goto L_0x0589
        L_0x0587:
            r22 = r6
        L_0x0589:
            boolean r0 = r8.f4424     // Catch:{ all -> 0x07a0 }
            if (r0 == 0) goto L_0x0590
            java.lang.String r6 = "META-INF/LP.SF"
            goto L_0x0592
        L_0x0590:
            r6 = r22
        L_0x0592:
            java.util.jar.JarEntry r0 = new java.util.jar.JarEntry     // Catch:{ all -> 0x07a0 }
            r0.<init>(r6)     // Catch:{ all -> 0x07a0 }
            r0.setTime(r13)     // Catch:{ all -> 0x07a0 }
            r15.putNextEntry(r0)     // Catch:{ all -> 0x07a0 }
            r15.write(r4)     // Catch:{ all -> 0x07a0 }
            r3.m7941(r4)     // Catch:{ all -> 0x07a0 }
            byte[] r0 = r3.m7942()     // Catch:{ all -> 0x07a0 }
            if (r1 == 0) goto L_0x061e
            java.lang.String r1 = "SHA1"
            java.security.MessageDigest r1 = java.security.MessageDigest.getInstance(r1)     // Catch:{ all -> 0x07a0 }
            r1.update(r4)     // Catch:{ all -> 0x07a0 }
            byte[] r1 = r1.digest()     // Catch:{ all -> 0x07a0 }
            ʼ.ʻ.ʼ r3 = m6037()     // Catch:{ all -> 0x07a0 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x07a0 }
            r4.<init>()     // Catch:{ all -> 0x07a0 }
            java.lang.String r6 = "Sig File SHA1: \n"
            r4.append(r6)     // Catch:{ all -> 0x07a0 }
            java.lang.String r1 = ʼ.ʼ.ʻ.C1426.m7921(r1)     // Catch:{ all -> 0x07a0 }
            r4.append(r1)     // Catch:{ all -> 0x07a0 }
            java.lang.String r1 = r4.toString()     // Catch:{ all -> 0x07a0 }
            r3.m7911(r1)     // Catch:{ all -> 0x07a0 }
            ʼ.ʻ.ʼ r1 = m6037()     // Catch:{ all -> 0x07a0 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x07a0 }
            r3.<init>()     // Catch:{ all -> 0x07a0 }
            java.lang.String r4 = "Signature: \n"
            r3.append(r4)     // Catch:{ all -> 0x07a0 }
            java.lang.String r4 = ʼ.ʼ.ʻ.C1426.m7921(r0)     // Catch:{ all -> 0x07a0 }
            r3.append(r4)     // Catch:{ all -> 0x07a0 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x07a0 }
            r1.m7911(r3)     // Catch:{ all -> 0x07a0 }
            java.lang.String r1 = "RSA/ECB/PKCS1Padding"
            javax.crypto.Cipher r1 = javax.crypto.Cipher.getInstance(r1)     // Catch:{ all -> 0x07a0 }
            r3 = 2
            ʼ.ʼ.ʻ.ʿ r4 = r8.f4416     // Catch:{ all -> 0x07a0 }
            java.security.cert.X509Certificate r4 = r4.m7929()     // Catch:{ all -> 0x07a0 }
            r1.init(r3, r4)     // Catch:{ all -> 0x07a0 }
            byte[] r1 = r1.doFinal(r0)     // Catch:{ all -> 0x07a0 }
            ʼ.ʻ.ʼ r3 = m6037()     // Catch:{ all -> 0x07a0 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x07a0 }
            r4.<init>()     // Catch:{ all -> 0x07a0 }
            java.lang.String r6 = "Signature Decrypted: \n"
            r4.append(r6)     // Catch:{ all -> 0x07a0 }
            java.lang.String r1 = ʼ.ʼ.ʻ.C1426.m7921(r1)     // Catch:{ all -> 0x07a0 }
            r4.append(r1)     // Catch:{ all -> 0x07a0 }
            java.lang.String r1 = r4.toString()     // Catch:{ all -> 0x07a0 }
            r3.m7911(r1)     // Catch:{ all -> 0x07a0 }
        L_0x061e:
            ʼ.ʼ.ʻ.ˈ r1 = r8.f4426     // Catch:{ all -> 0x07a0 }
            java.lang.String r3 = "Generating signature block file"
            r4 = 0
            r1.m7937(r4, r3)     // Catch:{ all -> 0x07a0 }
            java.util.jar.JarEntry r1 = new java.util.jar.JarEntry     // Catch:{ all -> 0x07a0 }
            r1.<init>(r5)     // Catch:{ all -> 0x07a0 }
            r1.setTime(r13)     // Catch:{ all -> 0x07a0 }
            r15.putNextEntry(r1)     // Catch:{ all -> 0x07a0 }
            ʼ.ʼ.ʻ.ʿ r1 = r8.f4416     // Catch:{ all -> 0x07a0 }
            byte[] r1 = r1.m7931()     // Catch:{ all -> 0x07a0 }
            ʼ.ʼ.ʻ.ʿ r3 = r8.f4416     // Catch:{ all -> 0x07a0 }
            java.security.cert.X509Certificate r3 = r3.m7929()     // Catch:{ all -> 0x07a0 }
            r8.m6041(r1, r0, r3, r15)     // Catch:{ all -> 0x07a0 }
            boolean r0 = r8.f4425     // Catch:{ all -> 0x07a0 }
            if (r0 == 0) goto L_0x06b1
            java.lang.String r0 = r8.f4417
            boolean r0 = r0.equals(r11)
            if (r0 != 0) goto L_0x067b
            r15.close()
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x067b
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            r1 = 1
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Throwable -> 0x0673 }
            java.lang.String r0 = r0.getName()     // Catch:{ Throwable -> 0x0673 }
            r2 = 0
            r1[r2] = r0     // Catch:{ Throwable -> 0x0673 }
            r2 = 2131690077(0x7f0f025d, float:1.9009187E38)
            java.lang.String r0 = com.chelpus.C0815.m5139(r2, r1)     // Catch:{ Throwable -> 0x0673 }
            com.lp.ʻ.C0947.m5877(r0)     // Catch:{ Throwable -> 0x0673 }
        L_0x0673:
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            com.chelpus.C0815.m5323(r0)
        L_0x067b:
            boolean r0 = r8.f4425
            if (r0 == 0) goto L_0x06b0
            if (r9 == 0) goto L_0x06b0
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x068a }
            r0.<init>(r9)     // Catch:{ Throwable -> 0x068a }
            r0.delete()     // Catch:{ Throwable -> 0x068a }
            goto L_0x06b0
        L_0x068a:
            r0 = move-exception
            ʼ.ʻ.ʼ r1 = m6037()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r0.getClass()
            java.lang.String r3 = r3.getName()
            r2.append(r3)
            r2.append(r10)
            java.lang.String r0 = r0.getMessage()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.m7909(r0)
        L_0x06b0:
            return
        L_0x06b1:
            r1 = r20
            r3 = r21
            r4 = r15
            r5 = r13
            r7 = r24
            r1.m6040(r2, r3, r4, r5, r7)     // Catch:{ all -> 0x07a0 }
            boolean r0 = r8.f4425     // Catch:{ all -> 0x07a0 }
            if (r0 == 0) goto L_0x072d
            java.lang.String r0 = r8.f4417
            boolean r0 = r0.equals(r11)
            if (r0 != 0) goto L_0x06f7
            r15.close()
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x06f7
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            r1 = 1
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Throwable -> 0x06ef }
            java.lang.String r0 = r0.getName()     // Catch:{ Throwable -> 0x06ef }
            r2 = 0
            r1[r2] = r0     // Catch:{ Throwable -> 0x06ef }
            r2 = 2131690077(0x7f0f025d, float:1.9009187E38)
            java.lang.String r0 = com.chelpus.C0815.m5139(r2, r1)     // Catch:{ Throwable -> 0x06ef }
            com.lp.ʻ.C0947.m5877(r0)     // Catch:{ Throwable -> 0x06ef }
        L_0x06ef:
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            com.chelpus.C0815.m5323(r0)
        L_0x06f7:
            boolean r0 = r8.f4425
            if (r0 == 0) goto L_0x072c
            if (r9 == 0) goto L_0x072c
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x0706 }
            r0.<init>(r9)     // Catch:{ Throwable -> 0x0706 }
            r0.delete()     // Catch:{ Throwable -> 0x0706 }
            goto L_0x072c
        L_0x0706:
            r0 = move-exception
            ʼ.ʻ.ʼ r1 = m6037()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r0.getClass()
            java.lang.String r3 = r3.getName()
            r2.append(r3)
            r2.append(r10)
            java.lang.String r0 = r0.getMessage()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.m7909(r0)
        L_0x072c:
            return
        L_0x072d:
            java.lang.String r0 = r8.f4417
            boolean r0 = r0.equals(r11)
            if (r0 != 0) goto L_0x0764
            r15.close()
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x0764
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            r1 = 1
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Throwable -> 0x075c }
            java.lang.String r0 = r0.getName()     // Catch:{ Throwable -> 0x075c }
            r2 = 0
            r1[r2] = r0     // Catch:{ Throwable -> 0x075c }
            r2 = 2131690077(0x7f0f025d, float:1.9009187E38)
            java.lang.String r0 = com.chelpus.C0815.m5139(r2, r1)     // Catch:{ Throwable -> 0x075c }
            com.lp.ʻ.C0947.m5877(r0)     // Catch:{ Throwable -> 0x075c }
        L_0x075c:
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            com.chelpus.C0815.m5323(r0)
        L_0x0764:
            boolean r0 = r8.f4425
            if (r0 == 0) goto L_0x0799
            if (r9 == 0) goto L_0x0799
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x0773 }
            r0.<init>(r9)     // Catch:{ Throwable -> 0x0773 }
            r0.delete()     // Catch:{ Throwable -> 0x0773 }
            goto L_0x0799
        L_0x0773:
            r0 = move-exception
            ʼ.ʻ.ʼ r1 = m6037()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r0.getClass()
            java.lang.String r3 = r3.getName()
            r2.append(r3)
            r2.append(r10)
            java.lang.String r0 = r0.getMessage()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.m7909(r0)
        L_0x0799:
            return
        L_0x079a:
            r0 = move-exception
            r9 = r23
            r10 = r19
            goto L_0x07a1
        L_0x07a0:
            r0 = move-exception
        L_0x07a1:
            r1 = r0
            goto L_0x07a6
        L_0x07a3:
            r0 = move-exception
            r1 = r0
            r15 = r2
        L_0x07a6:
            if (r15 == 0) goto L_0x07df
            java.lang.String r0 = r8.f4417
            boolean r0 = r0.equals(r11)
            if (r0 != 0) goto L_0x07df
            r15.close()
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x07df
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Throwable -> 0x07d7 }
            java.lang.String r0 = r0.getName()     // Catch:{ Throwable -> 0x07d7 }
            r3 = 0
            r2[r3] = r0     // Catch:{ Throwable -> 0x07d7 }
            r3 = 2131690077(0x7f0f025d, float:1.9009187E38)
            java.lang.String r0 = com.chelpus.C0815.m5139(r3, r2)     // Catch:{ Throwable -> 0x07d7 }
            com.lp.ʻ.C0947.m5877(r0)     // Catch:{ Throwable -> 0x07d7 }
        L_0x07d7:
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            com.chelpus.C0815.m5323(r0)
        L_0x07df:
            boolean r0 = r8.f4425
            if (r0 == 0) goto L_0x0814
            if (r9 == 0) goto L_0x0814
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x07ee }
            r0.<init>(r9)     // Catch:{ Throwable -> 0x07ee }
            r0.delete()     // Catch:{ Throwable -> 0x07ee }
            goto L_0x0814
        L_0x07ee:
            r0 = move-exception
            ʼ.ʻ.ʼ r2 = m6037()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.Class r4 = r0.getClass()
            java.lang.String r4 = r4.getName()
            r3.append(r4)
            r3.append(r10)
            java.lang.String r0 = r0.getMessage()
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r2.m7909(r0)
        L_0x0814:
            goto L_0x0816
        L_0x0815:
            throw r1
        L_0x0816:
            goto L_0x0815
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lp.C0985.m6049(java.util.Map, java.io.OutputStream, java.lang.String, java.util.ArrayList):void");
    }

    /* renamed from: com.lp.ﾞ$ʻ  reason: contains not printable characters */
    /* compiled from: ZipSignerLP */
    public static class C0986 extends Observable {
        public void notifyObservers(Object obj) {
            super.setChanged();
            super.notifyObservers(obj);
        }
    }
}
