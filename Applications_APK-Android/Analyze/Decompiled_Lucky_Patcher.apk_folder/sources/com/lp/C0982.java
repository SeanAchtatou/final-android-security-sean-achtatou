package com.lp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ⁱ  reason: contains not printable characters */
/* compiled from: ProgressDlg */
public class C0982 {

    /* renamed from: ʿ  reason: contains not printable characters */
    public static int f4382;

    /* renamed from: ʻ  reason: contains not printable characters */
    public Dialog f4383 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    public View f4384 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    public Context f4385 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    public String f4386 = "%1d/%2d";

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean f4387 = false;

    public C0982(Context context) {
        this.f4385 = context;
        this.f4383 = new Dialog(context);
        this.f4383.setCanceledOnTouchOutside(false);
        this.f4383.requestWindowFeature(1);
        this.f4383.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        this.f4383.getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            /* renamed from: ʻ  reason: contains not printable characters */
            int f4388 = 0;

            /* renamed from: ʼ  reason: contains not printable characters */
            int f4389 = 0;

            /* renamed from: ʽ  reason: contains not printable characters */
            int f4390 = 255;

            /* renamed from: ʾ  reason: contains not printable characters */
            int f4391 = 0;

            public void onGlobalLayout() {
                if (C0982.this.f4387) {
                    Window window = C0982.this.f4383.getWindow();
                    this.f4391 = window.getDecorView().getWidth();
                    int i = C0987.m6069().getDisplayMetrics().heightPixels;
                    int i2 = C0987.m6069().getDisplayMetrics().widthPixels;
                    if (i > i2) {
                        this.f4390 = 0;
                    } else {
                        this.f4390 = 1;
                    }
                    int i3 = this.f4389;
                    if (i3 == 0 || i3 != i2) {
                        this.f4388 = 0;
                        this.f4389 = i2;
                    }
                    int i4 = this.f4388;
                    if (i4 == 0 || this.f4391 != i4) {
                        window.setGravity(17);
                        WindowManager.LayoutParams attributes = window.getAttributes();
                        float f = (float) i2;
                        double r5 = (double) C0982.this.m6021(f);
                        Double.isNaN(r5);
                        int i5 = (int) (r5 * 0.98d);
                        double r52 = (double) C0982.this.m6021(f);
                        Double.isNaN(r52);
                        int i6 = (int) (r52 * 0.75d);
                        C0987.m6060((Object) ("wight:" + this.f4391));
                        int i7 = this.f4390;
                        if (i7 == 0) {
                            attributes.width = (int) C0982.this.m6027((float) i5);
                            this.f4388 = attributes.width;
                        } else if (i7 == 1) {
                            attributes.width = (int) C0982.this.m6027((float) i6);
                            this.f4388 = attributes.width;
                        }
                        this.f4388 = attributes.width;
                        if (C0982.this.m6021((float) attributes.width) < 330.0f && C0982.this.m6021((float) this.f4389) >= 330.0f) {
                            attributes.width = (int) C0982.this.m6027(330.0f);
                            this.f4388 = attributes.width;
                        }
                        window.setAttributes(attributes);
                    }
                }
            }
        });
        this.f4383.setContentView((int) R.layout.progress_dialog);
        this.f4383.setCancelable(false);
        ProgressBar progressBar = (ProgressBar) this.f4383.findViewById(R.id.progressBarIncrement);
        progressBar.setMax(0);
        progressBar.setProgress(0);
        progressBar.setSecondaryProgress(0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0982 m6023(final int i) {
        try {
            ((Activity) this.f4385).runOnUiThread(new Runnable() {
                public void run() {
                    ((LinearLayout) C0982.this.f4383.findViewById(R.id.title_layout)).setVisibility(0);
                    ImageView imageView = (ImageView) C0982.this.f4383.findViewById(R.id.title_icon);
                    imageView.setVisibility(0);
                    imageView.setImageDrawable(C0982.this.f4385.getResources().getDrawable(i));
                    ((TextView) C0982.this.f4383.findViewById(R.id.line0)).setVisibility(0);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0982 m6025(final String str) {
        try {
            ((Activity) this.f4385).runOnUiThread(new Runnable() {
                public void run() {
                    ((LinearLayout) C0982.this.f4383.findViewById(R.id.title_layout)).setVisibility(0);
                    ((TextView) C0982.this.f4383.findViewById(R.id.title_text)).setText(str);
                    ((TextView) C0982.this.f4383.findViewById(R.id.line0)).setVisibility(0);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0982 m6030(final String str) {
        try {
            ((Activity) this.f4385).runOnUiThread(new Runnable() {
                public void run() {
                    ((TextView) C0982.this.f4383.findViewById(R.id.dialog_message)).setText(str);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0982 m6026(boolean z) {
        this.f4383.setCancelable(z);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0982 m6024(DialogInterface.OnCancelListener onCancelListener) {
        this.f4383.setOnCancelListener(onCancelListener);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0982 m6022() {
        this.f4387 = true;
        try {
            ((Activity) this.f4385).runOnUiThread(new Runnable() {
                public void run() {
                    ((ProgressBar) C0982.this.f4383.findViewById(R.id.progressBarIncrement)).setVisibility(0);
                    ((LinearLayout) C0982.this.f4383.findViewById(R.id.increment_info_layout)).setVisibility(0);
                    TextView textView = (TextView) C0982.this.f4383.findViewById(R.id.increment_text_progress);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0982 m6028() {
        this.f4387 = false;
        try {
            ((Activity) this.f4385).runOnUiThread(new Runnable() {
                public void run() {
                    ((ProgressBar) C0982.this.f4383.findViewById(R.id.progressBarIncrement)).setVisibility(8);
                    ((LinearLayout) C0982.this.f4383.findViewById(R.id.increment_info_layout)).setVisibility(8);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0982 m6029(final int i) {
        try {
            f4382 = i;
            ProgressBar progressBar = (ProgressBar) this.f4383.findViewById(R.id.progressBarIncrement);
            progressBar.setVisibility(0);
            progressBar.setMax(f4382);
            ((Activity) this.f4385).runOnUiThread(new Runnable() {
                public void run() {
                    ((TextView) C0982.this.f4383.findViewById(R.id.increment_text_progress)).setText(String.format(C0982.this.f4386, Integer.valueOf(((ProgressBar) C0982.this.f4383.findViewById(R.id.progressBarIncrement)).getProgress()), Integer.valueOf(i)));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public C0982 m6031(final int i) {
        try {
            ((Activity) this.f4385).runOnUiThread(new Runnable() {
                public void run() {
                    ProgressBar progressBar = (ProgressBar) C0982.this.f4383.findViewById(R.id.progressBarIncrement);
                    progressBar.setVisibility(0);
                    progressBar.setProgress(i);
                    ((TextView) C0982.this.f4383.findViewById(R.id.increment_text_progress)).setText(String.format(C0982.this.f4386, Integer.valueOf(i), Integer.valueOf(C0982.f4382)));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m6033() {
        return this.f4383.isShowing();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m6032(String str) {
        try {
            this.f4386 = str;
            ((Activity) this.f4385).runOnUiThread(new Runnable() {
                public void run() {
                    ((TextView) C0982.this.f4383.findViewById(R.id.increment_text_progress)).setText(String.format(C0982.this.f4386, Integer.valueOf(((ProgressBar) C0982.this.f4383.findViewById(R.id.progressBarIncrement)).getProgress()), Integer.valueOf(C0982.f4382)));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public Dialog m6034() {
        return this.f4383;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public float m6021(float f) {
        return f / this.f4385.getResources().getDisplayMetrics().density;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public float m6027(float f) {
        return f * this.f4385.getResources().getDisplayMetrics().density;
    }
}
