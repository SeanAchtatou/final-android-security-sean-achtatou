package com.lp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.chelpus.C0815;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import net.lingala.zip4j.util.InternalZipConstants;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

public class SetPrefs extends PreferenceActivity {

    /* renamed from: ʻ  reason: contains not printable characters */
    public Context f3859;

    /* renamed from: ʼ  reason: contains not printable characters */
    Preference.OnPreferenceChangeListener f3860 = new Preference.OnPreferenceChangeListener() {
        public boolean onPreferenceChange(Preference preference, Object obj) {
            SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 4);
            if (preference != SetPrefs.this.findPreference("days_on_up") || !SetPrefs.this.m5841(obj)) {
                return false;
            }
            sharedPreferences.edit().putInt("days_on_up", Integer.valueOf(obj.toString()).intValue()).commit();
            preference.getEditor().putString("days_on_up", obj.toString());
            sharedPreferences.edit().putBoolean("settings_change", true).commit();
            sharedPreferences.edit().putBoolean("lang_change", true).commit();
            return true;
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f3859 = this;
        SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
        SharedPreferences sharedPreferences2 = C0987.m6072().getSharedPreferences("SetPrefs", 0);
        SharedPreferences.Editor edit = C0987.m6072().getSharedPreferences("config", 0).edit();
        edit.putBoolean("Update0", true);
        edit.commit();
        String string = sharedPreferences.getString("force_language", "default");
        if (string.equals("default")) {
            Locale locale = Resources.getSystem().getConfiguration().locale;
            Locale.setDefault(Locale.getDefault());
            Configuration configuration = new Configuration();
            configuration.locale = locale;
            C0987.m6069().updateConfiguration(configuration, C0987.m6069().getDisplayMetrics());
        } else {
            Locale locale2 = null;
            String[] split = string.split("_");
            if (split.length == 1) {
                locale2 = new Locale(split[0]);
            }
            if (split.length == 2) {
                locale2 = new Locale(split[0], split[1], BuildConfig.FLAVOR);
                if (split[1].equals("rBR")) {
                    locale2 = new Locale(split[0], "BR");
                }
            }
            if (split.length == 3) {
                locale2 = new Locale(split[0], split[1], split[2]);
            }
            Locale.setDefault(locale2);
            Configuration configuration2 = new Configuration();
            configuration2.locale = locale2;
            C0987.m6069().updateConfiguration(configuration2, C0987.m6069().getDisplayMetrics());
        }
        if (sharedPreferences.getInt("orientstion", 3) == 1) {
            setRequestedOrientation(0);
        }
        if (sharedPreferences.getInt("orientstion", 3) == 2) {
            setRequestedOrientation(1);
        }
        if (sharedPreferences.getInt("orientstion", 3) == 3) {
            setRequestedOrientation(4);
        }
        PreferenceManager preferenceManager = getPreferenceManager();
        preferenceManager.setSharedPreferencesName("SetPrefs");
        preferenceManager.setSharedPreferencesMode(0);
        SharedPreferences.Editor edit2 = sharedPreferences2.edit();
        edit2.putString("viewsize", BuildConfig.FLAVOR + sharedPreferences.getInt("viewsize", 0)).commit();
        SharedPreferences.Editor edit3 = sharedPreferences2.edit();
        edit3.putString("root_force", BuildConfig.FLAVOR + sharedPreferences.getInt("root_force", 0)).commit();
        sharedPreferences2.edit().putBoolean("confirm_exit", sharedPreferences.getBoolean("confirm_exit", true)).commit();
        SharedPreferences.Editor edit4 = sharedPreferences2.edit();
        edit4.putString("orientstion", BuildConfig.FLAVOR + sharedPreferences.getInt("orientstion", 3)).commit();
        SharedPreferences.Editor edit5 = sharedPreferences2.edit();
        edit5.putString("sortby", BuildConfig.FLAVOR + sharedPreferences.getInt("sortby", 2)).commit();
        SharedPreferences.Editor edit6 = sharedPreferences2.edit();
        edit6.putString("language", BuildConfig.FLAVOR + sharedPreferences.getInt("language", 1)).commit();
        sharedPreferences2.edit().putBoolean("systemapp", sharedPreferences.getBoolean("systemapp", true)).commit();
        SharedPreferences.Editor edit7 = sharedPreferences2.edit();
        edit7.putString("apkname", BuildConfig.FLAVOR + sharedPreferences.getInt("apkname", 0)).commit();
        sharedPreferences2.edit().putBoolean("lvlapp", sharedPreferences.getBoolean("lvlapp", true)).commit();
        sharedPreferences2.edit().putBoolean("adsapp", sharedPreferences.getBoolean("adsapp", true)).commit();
        sharedPreferences2.edit().putBoolean("customapp", sharedPreferences.getBoolean("customapp", true)).commit();
        sharedPreferences2.edit().putBoolean("modifapp", sharedPreferences.getBoolean("modifapp", true)).commit();
        sharedPreferences2.edit().putBoolean("hide_notify", sharedPreferences.getBoolean("hide_notify", false)).commit();
        sharedPreferences2.edit().putBoolean("fixedapp", sharedPreferences.getBoolean("fixedapp", true)).commit();
        sharedPreferences2.edit().putBoolean("noneapp", sharedPreferences.getBoolean("noneapp", true)).commit();
        sharedPreferences2.edit().putString("path", sharedPreferences.getString("basepath", "Noting")).commit();
        sharedPreferences2.edit().putString("extStorageDirectory", sharedPreferences.getString("extStorageDirectory", "Noting")).commit();
        sharedPreferences2.edit().putBoolean("vibration", sharedPreferences.getBoolean("vibration", false)).commit();
        sharedPreferences2.edit().putBoolean("disable_autoupdate", sharedPreferences.getBoolean("disable_autoupdate", false)).commit();
        sharedPreferences2.edit().putBoolean("fast_start", sharedPreferences.getBoolean("fast_start", false)).commit();
        sharedPreferences2.edit().putBoolean("no_icon", sharedPreferences.getBoolean("no_icon", false)).commit();
        sharedPreferences2.edit().putBoolean("hide_title", sharedPreferences.getBoolean("hide_title", false)).commit();
        SharedPreferences.Editor edit8 = sharedPreferences2.edit();
        edit8.putString("days_on_up", BuildConfig.FLAVOR + sharedPreferences.getInt("days_on_up", 1)).commit();
        addPreferencesFromResource(R.xml.preferences);
        getPreferenceScreen().findPreference("days_on_up").setOnPreferenceChangeListener(this.f3860);
        findPreference("viewsize").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("viewsize") && obj.equals("2")) {
                    sharedPreferences.edit().putInt("viewsize", 2).commit();
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    return true;
                } else if (preference == SetPrefs.this.findPreference("viewsize") && obj.equals("1")) {
                    sharedPreferences.edit().putInt("viewsize", 1).commit();
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    return true;
                } else if (preference != SetPrefs.this.findPreference("viewsize") || !obj.equals("0")) {
                    return false;
                } else {
                    sharedPreferences.edit().putInt("viewsize", 0).commit();
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("root_force").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("root_force") && obj.equals("2")) {
                    C0987.f4474 = false;
                    sharedPreferences.edit().putInt("root_force", 2).commit();
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    return true;
                } else if (preference == SetPrefs.this.findPreference("root_force") && obj.equals("1")) {
                    C0987.f4474 = true;
                    sharedPreferences.edit().putInt("root_force", 1).commit();
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    return true;
                } else if (preference != SetPrefs.this.findPreference("root_force") || !obj.equals("0")) {
                    return false;
                } else {
                    sharedPreferences.edit().putInt("root_force", 0).commit();
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("orientstion").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("orientstion") && obj.equals("2")) {
                    sharedPreferences.edit().putInt("orientstion", 3).commit();
                    SetPrefs.this.setRequestedOrientation(1);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    return true;
                } else if (preference == SetPrefs.this.findPreference("orientstion") && obj.equals("1")) {
                    sharedPreferences.edit().putInt("orientstion", 1).commit();
                    SetPrefs.this.setRequestedOrientation(0);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    return true;
                } else if (preference != SetPrefs.this.findPreference("orientstion") || !obj.equals("3")) {
                    return false;
                } else {
                    sharedPreferences.edit().putInt("orientstion", 3).commit();
                    SetPrefs.this.setRequestedOrientation(4);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("sortby").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("sortby") && obj.equals("1")) {
                    sharedPreferences.edit().putInt("sortby", 1).commit();
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    return true;
                } else if (preference == SetPrefs.this.findPreference("sortby") && obj.equals("3")) {
                    sharedPreferences.edit().putInt("sortby", 3).commit();
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    return true;
                } else if (preference != SetPrefs.this.findPreference("sortby") || !obj.equals("2")) {
                    return false;
                } else {
                    sharedPreferences.edit().putInt("sortby", 2).commit();
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("language").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("language") && obj.equals("1")) {
                    Locale locale = Resources.getSystem().getConfiguration().locale;
                    Locale.setDefault(Locale.getDefault());
                    Configuration configuration = new Configuration();
                    configuration.locale = locale;
                    C0987.m6069().updateConfiguration(configuration, C0987.m6069().getDisplayMetrics());
                    sharedPreferences.edit().putInt("language", 1).commit();
                    Intent intent = SetPrefs.this.getIntent();
                    intent.addFlags(131072);
                    SetPrefs.this.finish();
                    SetPrefs.this.startActivity(intent);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    return true;
                } else if (preference != SetPrefs.this.findPreference("language") || !obj.equals("2")) {
                    return false;
                } else {
                    Locale locale2 = new Locale("en");
                    Locale.setDefault(locale2);
                    Configuration configuration2 = new Configuration();
                    configuration2.locale = locale2;
                    C0987.m6069().updateConfiguration(configuration2, C0987.m6069().getDisplayMetrics());
                    sharedPreferences.edit().putInt("language", 2).commit();
                    Intent intent2 = SetPrefs.this.getIntent();
                    intent2.addFlags(131072);
                    SetPrefs.this.finish();
                    SetPrefs.this.startActivity(intent2);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("systemapp").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("systemapp") && ((Boolean) obj).booleanValue()) {
                    sharedPreferences.edit().putBoolean("systemapp", true).commit();
                    preference.getEditor().putBoolean("systemapp", true);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    return true;
                } else if (preference != SetPrefs.this.findPreference("systemapp") || ((Boolean) obj).booleanValue()) {
                    return false;
                } else {
                    sharedPreferences.edit().putBoolean("systemapp", false).commit();
                    preference.getEditor().putBoolean("systemapp", false);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("confirm_exit").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("confirm_exit") && ((Boolean) obj).booleanValue()) {
                    sharedPreferences.edit().putBoolean("confirm_exit", true).commit();
                    preference.getEditor().putBoolean("confirm_exit", true);
                    return true;
                } else if (preference != SetPrefs.this.findPreference("confirm_exit") || ((Boolean) obj).booleanValue()) {
                    return false;
                } else {
                    sharedPreferences.edit().putBoolean("confirm_exit", false).commit();
                    preference.getEditor().putBoolean("confirm_exit", false);
                    return true;
                }
            }
        });
        findPreference("apkname").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("apkname") && obj.equals("0")) {
                    sharedPreferences.edit().putInt("apkname", 0).commit();
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    return true;
                } else if (preference != SetPrefs.this.findPreference("apkname") || !obj.equals("1")) {
                    return false;
                } else {
                    sharedPreferences.edit().putInt("apkname", 1).commit();
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("lvlapp").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("lvlapp") && ((Boolean) obj).booleanValue()) {
                    sharedPreferences.edit().putBoolean("lvlapp", true).commit();
                    preference.getEditor().putBoolean("lvlapp", true);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    return true;
                } else if (preference != SetPrefs.this.findPreference("lvlapp") || ((Boolean) obj).booleanValue()) {
                    return false;
                } else {
                    sharedPreferences.edit().putBoolean("lvlapp", false).commit();
                    preference.getEditor().putBoolean("lvlapp", false);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("adsapp").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("adsapp") && ((Boolean) obj).booleanValue()) {
                    sharedPreferences.edit().putBoolean("adsapp", true).commit();
                    preference.getEditor().putBoolean("adsapp", true);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    return true;
                } else if (preference != SetPrefs.this.findPreference("adsapp") || ((Boolean) obj).booleanValue()) {
                    return false;
                } else {
                    sharedPreferences.edit().putBoolean("adsapp", false).commit();
                    preference.getEditor().putBoolean("adsapp", false);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("customapp").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("customapp") && ((Boolean) obj).booleanValue()) {
                    sharedPreferences.edit().putBoolean("customapp", true).commit();
                    preference.getEditor().putBoolean("customapp", true);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    return true;
                } else if (preference != SetPrefs.this.findPreference("customapp") || ((Boolean) obj).booleanValue()) {
                    return false;
                } else {
                    sharedPreferences.edit().putBoolean("customapp", false).commit();
                    preference.getEditor().putBoolean("customapp", false);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("modifapp").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("modifapp") && ((Boolean) obj).booleanValue()) {
                    sharedPreferences.edit().putBoolean("modifapp", true).commit();
                    preference.getEditor().putBoolean("modifapp", true);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    return true;
                } else if (preference != SetPrefs.this.findPreference("modifapp") || ((Boolean) obj).booleanValue()) {
                    return false;
                } else {
                    sharedPreferences.edit().putBoolean("modifapp", false).commit();
                    preference.getEditor().putBoolean("modifapp", false);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("fixedapp").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("fixedapp") && ((Boolean) obj).booleanValue()) {
                    sharedPreferences.edit().putBoolean("fixedapp", true).commit();
                    preference.getEditor().putBoolean("fixedapp", true);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    return true;
                } else if (preference != SetPrefs.this.findPreference("fixedapp") || ((Boolean) obj).booleanValue()) {
                    return false;
                } else {
                    sharedPreferences.edit().putBoolean("fixedapp", false).commit();
                    preference.getEditor().putBoolean("fixedapp", false);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("noneapp").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("noneapp") && ((Boolean) obj).booleanValue()) {
                    sharedPreferences.edit().putBoolean("noneapp", true).commit();
                    preference.getEditor().putBoolean("noneapp", true);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    return true;
                } else if (preference != SetPrefs.this.findPreference("noneapp") || ((Boolean) obj).booleanValue()) {
                    return false;
                } else {
                    sharedPreferences.edit().putBoolean("noneapp", false).commit();
                    preference.getEditor().putBoolean("noneapp", false);
                    sharedPreferences.edit().putBoolean("settings_change", true).commit();
                    sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("hide_notify").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("hide_notify") && ((Boolean) obj).booleanValue()) {
                    sharedPreferences.edit().putBoolean("hide_notify", true).commit();
                    preference.getEditor().putBoolean("hide_notify", true);
                    return true;
                } else if (preference != SetPrefs.this.findPreference("hide_notify") || ((Boolean) obj).booleanValue()) {
                    return false;
                } else {
                    sharedPreferences.edit().putBoolean("hide_notify", false).commit();
                    preference.getEditor().putBoolean("hide_notify", false);
                    return true;
                }
            }
        });
        findPreference("vibration").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("vibration") && ((Boolean) obj).booleanValue()) {
                    sharedPreferences.edit().putBoolean("vibration", true).commit();
                    preference.getEditor().putBoolean("vibration", true);
                    return true;
                } else if (preference != SetPrefs.this.findPreference("vibration") || ((Boolean) obj).booleanValue()) {
                    return false;
                } else {
                    sharedPreferences.edit().putBoolean("vibration", false).commit();
                    preference.getEditor().putBoolean("vibration", false);
                    return true;
                }
            }
        });
        findPreference("disable_autoupdate").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("disable_autoupdate") && ((Boolean) obj).booleanValue()) {
                    sharedPreferences.edit().putBoolean("disable_autoupdate", true).commit();
                    preference.getEditor().putBoolean("disable_autoupdate", true);
                    return true;
                } else if (preference != SetPrefs.this.findPreference("disable_autoupdate") || ((Boolean) obj).booleanValue()) {
                    return false;
                } else {
                    sharedPreferences.edit().putBoolean("disable_autoupdate", false).commit();
                    preference.getEditor().putBoolean("disable_autoupdate", false);
                    return true;
                }
            }
        });
        findPreference("fast_start").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("fast_start") && ((Boolean) obj).booleanValue()) {
                    sharedPreferences.edit().putBoolean("fast_start", true).commit();
                    preference.getEditor().putBoolean("fast_start", true);
                    return true;
                } else if (preference != SetPrefs.this.findPreference("fast_start") || ((Boolean) obj).booleanValue()) {
                    return false;
                } else {
                    sharedPreferences.edit().putBoolean("fast_start", false).commit();
                    preference.getEditor().putBoolean("fast_start", false);
                    return true;
                }
            }
        });
        findPreference("no_icon").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("no_icon") && ((Boolean) obj).booleanValue()) {
                    sharedPreferences.edit().putBoolean("no_icon", true).commit();
                    preference.getEditor().putBoolean("no_icon", true);
                    return true;
                } else if (preference != SetPrefs.this.findPreference("no_icon") || ((Boolean) obj).booleanValue()) {
                    return false;
                } else {
                    sharedPreferences.edit().putBoolean("no_icon", false).commit();
                    preference.getEditor().putBoolean("no_icon", false);
                    return true;
                }
            }
        });
        findPreference("hide_title").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference == SetPrefs.this.findPreference("hide_title") && ((Boolean) obj).booleanValue()) {
                    sharedPreferences.edit().putBoolean("hide_title", true).commit();
                    preference.getEditor().putBoolean("hide_title", true);
                    return true;
                } else if (preference != SetPrefs.this.findPreference("hide_title") || ((Boolean) obj).booleanValue()) {
                    return false;
                } else {
                    sharedPreferences.edit().putBoolean("hide_title", false).commit();
                    preference.getEditor().putBoolean("hide_title", false);
                    return true;
                }
            }
        });
        findPreference("path").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

            /* renamed from: ʻ  reason: contains not printable characters */
            ProgressDialog f3866 = null;

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.lp.SetPrefs.ʻ(boolean, java.lang.String):boolean
             arg types: [int, java.lang.String]
             candidates:
              com.lp.SetPrefs.ʻ(com.lp.SetPrefs, java.lang.Object):boolean
              com.lp.SetPrefs.ʻ(boolean, java.lang.String):boolean */
            public boolean onPreferenceChange(Preference preference, Object obj) {
                final SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                if (preference != SetPrefs.this.findPreference("path") || !obj.toString().contains(InternalZipConstants.ZIP_FILE_SEPARATOR) || obj.toString().equals(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                    SetPrefs setPrefs = SetPrefs.this;
                    setPrefs.m5838(setPrefs, C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.error_path));
                } else {
                    final String str = BuildConfig.FLAVOR;
                    for (String str2 : obj.toString().trim().replaceAll("\\s+", ".").split("\\/+")) {
                        if (!str2.equals(BuildConfig.FLAVOR)) {
                            str = str + InternalZipConstants.ZIP_FILE_SEPARATOR + str2;
                        }
                    }
                    if (new File(str).exists()) {
                        SetPrefs setPrefs2 = SetPrefs.this;
                        setPrefs2.m5838(setPrefs2, C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.error_path_exists));
                    } else if (SetPrefs.this.m5842(true, str) && !str.startsWith(SetPrefs.this.getDir("sdcard", 0).getAbsolutePath())) {
                        sharedPreferences.edit().putString("path", str).commit();
                        sharedPreferences.edit().putBoolean("manual_path", true).commit();
                        preference.getEditor().putString("path", str).commit();
                        sharedPreferences.edit().putBoolean("path_changed", true).commit();
                        final File file = new File(sharedPreferences.getString("basepath", "Noting"));
                        final File file2 = new File(str);
                        if (!file.exists()) {
                            C0987.m6060((Object) "Directory does not exist.");
                        } else {
                            this.f3866 = new ProgressDialog(SetPrefs.this);
                            this.f3866.setCancelable(false);
                            this.f3866.setMessage(SetPrefs.this.getString(R.string.wait));
                            this.f3866.show();
                            final AnonymousClass1 r1 = new Handler() {
                                public void handleMessage(Message message) {
                                    C0987.m6060((Object) ("LuckyPatcher: message poluchil: !" + message));
                                    try {
                                        if (AnonymousClass14.this.f3866.isShowing()) {
                                            AnonymousClass14.this.f3866.dismiss();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    if (message.what == 0) {
                                        sharedPreferences.edit().putString("basepath", str).commit();
                                        C0987.f4438 = str;
                                        SetPrefs setPrefs = SetPrefs.this;
                                        SetPrefs setPrefs2 = SetPrefs.this;
                                        String r3 = C0815.m5205((int) R.string.warning);
                                        setPrefs.m5838(setPrefs2, r3, C0815.m5205((int) R.string.move_data) + str);
                                    }
                                    if (message.what == 1) {
                                        SetPrefs.this.m5838(SetPrefs.this, C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.no_space));
                                    }
                                }
                            };
                            new Thread(new Runnable() {
                                public void run() {
                                    try {
                                        C0815.m5161(file, file2, (String[]) null);
                                        new C0815(BuildConfig.FLAVOR).m5347(file);
                                        r1.sendEmptyMessage(0);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        r1.sendEmptyMessage(1);
                                    }
                                }
                            }).start();
                        }
                        return true;
                    }
                }
                return false;
            }
        });
        findPreference("help").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                SetPrefs.this.startActivity(new Intent(SetPrefs.this.getBaseContext(), HelpActivity.class));
                return true;
            }
        });
        findPreference("upd").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (C0987.f4432 != null) {
                    C0987.f4432.m6221();
                }
                SetPrefs.this.finish();
                return true;
            }
        });
        findPreference("sendlog").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                new AsyncTask<Void, Void, Boolean>() {

                    /* renamed from: ʻ  reason: contains not printable characters */
                    ProgressDialog f3878;

                    /* access modifiers changed from: protected */
                    /* renamed from: ʻ  reason: contains not printable characters */
                    public Boolean doInBackground(Void... voidArr) {
                        try {
                            if (C0987.f4478 == null) {
                                C0987.f4478 = new C0973();
                            }
                            return Boolean.valueOf(C0987.f4478.m5994(C0987.m6072(), true));
                        } catch (Exception e) {
                            e.printStackTrace();
                            return false;
                        }
                    }

                    /* access modifiers changed from: protected */
                    public void onPreExecute() {
                        this.f3878 = new ProgressDialog(SetPrefs.this.f3859);
                        this.f3878.setTitle("Progress");
                        this.f3878.setMessage(C0815.m5205((int) R.string.collect_logs));
                        this.f3878.setIndeterminate(true);
                        this.f3878.show();
                    }

                    /* access modifiers changed from: protected */
                    /* renamed from: ʻ  reason: contains not printable characters */
                    public void onPostExecute(Boolean bool) {
                        try {
                            if (this.f3878 != null && this.f3878.isShowing()) {
                                this.f3878.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (bool.booleanValue()) {
                            try {
                                C0973 r8 = C0987.f4478;
                                Context r0 = C0987.m6072();
                                r8.m5993(r0, "lp.chelpus@gmail.com", "Error Log", "Lucky Patcher " + C0987.m6068().getPackageInfo(C0987.f4432.m6233().getPackageName(), 0).versionName);
                            } catch (PackageManager.NameNotFoundException e2) {
                                e2.printStackTrace();
                            }
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(SetPrefs.this.f3859);
                            builder.setTitle("Error").setMessage(C0815.m5205((int) R.string.error_collect_logs)).setNegativeButton("OK", (DialogInterface.OnClickListener) null);
                            builder.create().show();
                        }
                    }
                }.execute(new Void[0]);
                return true;
            }
        });
        findPreference("about").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                LinearLayout linearLayout = (LinearLayout) View.inflate(SetPrefs.this.f3859, R.layout.aboutdialog, null);
                TextView textView = (TextView) ((LinearLayout) linearLayout.findViewById(R.id.dialogbodyscroll).findViewById(R.id.dialogbody)).findViewById(R.id.intonlydesc);
                textView.append(C0815.m5137(C0815.m5205((int) R.string.vers) + C0987.f4465 + "\n", "#ffffffff", "bold"));
                textView.append(C0815.m5137("----------------------------------\n\n", "#ffffffff", "bold"));
                textView.append(C0815.m5137(C0815.m5205((int) R.string.about_working_dir) + " " + C0987.f4438 + "\n\n", "#ffffffff", "bold"));
                StringBuilder sb = new StringBuilder();
                sb.append(C0815.m5205((int) R.string.punkt1));
                sb.append("\n\n");
                textView.append(C0815.m5137(sb.toString(), "#ffff0000", "bold"));
                textView.append(C0815.m5137(C0815.m5205((int) R.string.aboutlp) + "\n", "#ffffff00", "bold"));
                new AlertDialog.Builder(SetPrefs.this.f3859).setTitle((int) R.string.abouttitle).setCancelable(true).setIcon(17301659).setPositiveButton(C0815.m5205((int) R.string.ok), (DialogInterface.OnClickListener) null).setNeutralButton("Changelog", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (new File(C0987.f4438 + "/Changes/changelog.txt").exists()) {
                            SetPrefs setPrefs = SetPrefs.this;
                            Context context = SetPrefs.this.f3859;
                            String r7 = C0815.m5205((int) R.string.changelog);
                            setPrefs.m5838(context, r7, C0815.m5251(new File(C0987.f4438 + "/Changes/changelog.txt")));
                            return;
                        }
                        SetPrefs.this.m5838(SetPrefs.this.f3859, C0815.m5205((int) R.string.changelog), C0815.m5205((int) R.string.not_found_changelog));
                    }
                }).setView(linearLayout).setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
                return true;
            }
        });
        findPreference("language").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

            /* renamed from: ʻ  reason: contains not printable characters */
            Dialog f3883;

            public boolean onPreferenceClick(Preference preference) {
                C0987.m6069();
                SetPrefs.this.getAssets().getLocales();
                ArrayList arrayList = new ArrayList();
                arrayList.add("en_US");
                arrayList.add("ar");
                arrayList.add("bg");
                arrayList.add("cs");
                arrayList.add("da");
                arrayList.add("de");
                arrayList.add("el");
                arrayList.add("es");
                arrayList.add("fa");
                arrayList.add("fi");
                arrayList.add("fr");
                arrayList.add("he");
                arrayList.add("hi");
                arrayList.add("hu");
                arrayList.add("id");
                arrayList.add("in");
                arrayList.add("it");
                arrayList.add("iw");
                arrayList.add("ja");
                arrayList.add("km");
                arrayList.add("km_KH");
                arrayList.add("ko");
                arrayList.add("lt");
                arrayList.add("ml");
                arrayList.add("my");
                arrayList.add("nl");
                arrayList.add("pl");
                arrayList.add("pt");
                arrayList.add("pt_BR");
                arrayList.add("ro");
                arrayList.add("ru");
                arrayList.add("sk");
                arrayList.add("tr");
                arrayList.add("ug");
                arrayList.add("uk");
                arrayList.add("vi");
                arrayList.add("zh_CN");
                arrayList.add("zh_HK");
                arrayList.add("zh_TW");
                AnonymousClass1 r0 = new ArrayAdapter<String>(SetPrefs.this.f3859, R.layout.contextmenu, arrayList) {
                    public View getView(int i, View view, ViewGroup viewGroup) {
                        View view2 = super.getView(i, view, viewGroup);
                        TextView textView = (TextView) view2.findViewById(R.id.conttext);
                        textView.setTextColor(-1);
                        String str = (String) getItem(i);
                        Locale locale = new Locale(str, str);
                        textView.setText(locale.getDisplayName(locale));
                        if (str.equals("my")) {
                            textView.setText("Malay translation");
                        }
                        textView.setTypeface(null, 1);
                        return view2;
                    }
                };
                ListView listView = new ListView(SetPrefs.this.f3859);
                AlertDialog.Builder builder = new AlertDialog.Builder(SetPrefs.this.f3859);
                r0.setNotifyOnChange(true);
                listView.setAdapter((ListAdapter) r0);
                listView.invalidateViews();
                listView.setCacheColorHint(-16777216);
                listView.setBackgroundColor(-16777216);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                        if (AnonymousClass19.this.f3883.isShowing()) {
                            AnonymousClass19.this.f3883.dismiss();
                        }
                        SharedPreferences sharedPreferences = C0987.m6072().getSharedPreferences("config", 0);
                        String[] split = ((String) adapterView.getItemAtPosition(i)).split("_");
                        Locale locale = null;
                        if (split.length == 1) {
                            locale = new Locale(split[0]);
                        }
                        if (split.length == 2) {
                            locale = new Locale(split[0], split[1], BuildConfig.FLAVOR);
                            if (split[1].equals("rBR")) {
                                locale = new Locale(split[0], "BR");
                            }
                        }
                        if (split.length == 3) {
                            locale = new Locale(split[0], split[1], split[2]);
                        }
                        Locale.setDefault(locale);
                        Configuration configuration = new Configuration();
                        configuration.locale = locale;
                        C0987.m6069().updateConfiguration(configuration, C0987.m6069().getDisplayMetrics());
                        sharedPreferences.edit().putString("force_language", (String) adapterView.getItemAtPosition(i)).commit();
                        Intent intent = SetPrefs.this.getIntent();
                        intent.addFlags(131072);
                        SetPrefs.this.finish();
                        SetPrefs.this.startActivity(intent);
                        sharedPreferences.edit().putBoolean("settings_change", true).commit();
                        sharedPreferences.edit().putBoolean("lang_change", true).commit();
                    }
                });
                builder.setView(listView);
                this.f3883 = builder.create();
                this.f3883.show();
                return true;
            }
        });
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m5842(boolean z, String str) {
        C0987.m6060((Object) "test path.");
        try {
            if (!new File(str).exists()) {
                new File(str).mkdirs();
            }
            if (!new File(str).exists()) {
                m5838(this, C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.messagePath));
                return false;
            }
            if (new File(str + "/tmp.txt").createNewFile()) {
                new File(str + "/tmp.txt").delete();
                return true;
            }
            if (z) {
                m5838(this, C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.messagePath));
            }
            return false;
        } catch (IOException unused) {
            if (z) {
                m5838(this, C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.messagePath));
            }
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5838(Context context, String str, String str2) {
        final Dialog dialog = new Dialog(context);
        dialog.setTitle(str);
        dialog.setContentView((int) R.layout.message);
        ((TextView) dialog.findViewById(R.id.messageText)).setText(str2);
        ((Button) dialog.findViewById(R.id.buttonOk)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m5841(Object obj) {
        if (!obj.toString().equals(BuildConfig.FLAVOR) && obj.toString().matches("\\d*")) {
            return true;
        }
        m5838(this, C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.is_an_invalid_number));
        return false;
    }
}
