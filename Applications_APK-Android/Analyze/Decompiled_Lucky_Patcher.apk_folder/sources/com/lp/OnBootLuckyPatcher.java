package com.lp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.widget.Toast;
import com.chelpus.C0815;
import com.lp.widgets.BinderWidget;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import net.lingala.zip4j.util.InternalZipConstants;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.MainActivity;
import ru.pKkcGXHI.kKSaIWSZS.R;

public class OnBootLuckyPatcher extends BroadcastReceiver {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String[] f3815 = {"empty"};

    /* renamed from: ʼ  reason: contains not printable characters */
    public static Context f3816 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    public static int f3817 = 0;

    public void onReceive(final Context context, Intent intent) {
        C0987.m6060((Object) "load LP");
        f3816 = context;
        C0987.m6079(context);
        new Thread(new Runnable() {
            public void run() {
                int i = C0987.m6073().getInt("Install_location", 3);
                if (i == 3) {
                    return;
                }
                if (C0987.f4474) {
                    C0815 r1 = new C0815(BuildConfig.FLAVOR);
                    r1.m5353("pm setInstallLocation " + i, "skipOut");
                    C0815 r12 = new C0815(BuildConfig.FLAVOR);
                    r12.m5353("pm set-install-location " + i, "skipOut");
                    return;
                }
                C0815.m5148("pm setInstallLocation " + i, "skipOut");
                C0815.m5148("pm set-install-location " + i, "skipOut");
            }
        }).start();
        if (C0987.f4474) {
            if (intent.getAction().equals("android.intent.action.UMS_DISCONNECTED") || intent.getAction().equals("android.intent.action.ACTION_POWER_DISCONNECTED")) {
                C0987.m6060((Object) "load LP");
                new Thread(new Runnable() {
                    public void run() {
                        File file = new File(context.getDir("binder", 0) + "/bind.txt");
                        if (file.exists() && file.length() > 0) {
                            C0987.m6060((Object) "LuckyPatcher binder start!");
                            try {
                                if (!file.exists()) {
                                    file.createNewFile();
                                }
                                FileInputStream fileInputStream = new FileInputStream(file);
                                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream, "UTF-8"));
                                while (true) {
                                    String readLine = bufferedReader.readLine();
                                    if (readLine == null) {
                                        break;
                                    }
                                    String[] split = readLine.split(";");
                                    if (split.length == 2) {
                                        C0815.m5178("mount", "-o bind '" + split[0] + "' '" + split[1] + "'", split[0], split[1]);
                                    }
                                }
                                Intent intent = new Intent(context, BinderWidget.class);
                                intent.setAction(BinderWidget.f3931);
                                context.sendBroadcast(intent);
                                fileInputStream.close();
                            } catch (FileNotFoundException unused) {
                                C0987.m6060((Object) "Not found bind.txt");
                            } catch (IOException e) {
                                C0987.m6060((Object) (BuildConfig.FLAVOR + e));
                            }
                            C0987.f4484 = false;
                        }
                    }
                }).start();
                C0987.f4484 = true;
            }
            if (intent.getAction().equals("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE")) {
                C0987.m6060((Object) "LuckyPatcher: ACTION_EXTERNAL_APPLICATIONS_AVAILABLE");
                if (C0987.m6073().getBoolean("OnBootService", false)) {
                    C0987.m6073().edit().putBoolean("OnBootService", false).commit();
                    C0987.f4484 = true;
                    new Intent(C0987.m6072(), PatchService.class);
                    try {
                        if (Build.VERSION.SDK_INT >= 26) {
                            context.startForegroundService(new Intent(context, PatchService.class));
                        } else {
                            context.startService(new Intent(context, PatchService.class));
                        }
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                }
            }
            final Handler handler = new Handler();
            if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
                C0987.m6073().edit().putBoolean("OnBootService", true).commit();
                Intent intent2 = new Intent(context, OnAlarmReceiver.class);
                intent2.setAction(OnAlarmReceiver.f3811);
                ((AlarmManager) context.getSystemService("alarm")).set(2, 300000, PendingIntent.getBroadcast(context, 0, intent2, 0));
                new Thread(new Runnable() {
                    public void run() {
                        String str;
                        String str2;
                        Object obj;
                        if (C0815.m5307(C0987.f4437 + "/ClearDalvik.on")) {
                            C0815.m5230("/system", InternalZipConstants.WRITE_MODE);
                            try {
                                C0987.m6060((Object) new C0815(BuildConfig.FLAVOR).m5353(C0987.f4475 + ".clearDalvikCache " + OnBootLuckyPatcher.f3816.getApplicationContext().getFilesDir().getAbsolutePath()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            handler.post(new Runnable() {
                                public void run() {
                                    Toast.makeText(OnBootLuckyPatcher.f3816.getApplicationContext(), "LuckyPatcher: clear dalvik-cache failed. Please clear dalvik-cache manual.", 1).show();
                                }
                            });
                            C0987.m6060((Object) (OnBootLuckyPatcher.f3816.getApplicationContext().getFilesDir() + "/reboot"));
                            C0815.m5330();
                        }
                        if (!C0815.m5282().equals("ART")) {
                            if (C0987.m6073().getBoolean("trigger_for_good_android_patch_on_boot", false)) {
                                C0815.m5240();
                            }
                            C0987.m6073().edit().putBoolean("trigger_for_good_android_patch_on_boot", true).commit();
                            String string = C0987.m6073().getString("patch_dalvik_on_boot_patterns", BuildConfig.FLAVOR);
                            if (!string.equals(BuildConfig.FLAVOR)) {
                                if (!string.contains("patch1") || C0815.m5289() || C0815.m5294()) {
                                    str = BuildConfig.FLAVOR;
                                } else {
                                    str = BuildConfig.FLAVOR + "_patch1_";
                                }
                                if (string.contains("patch2") && !C0815.m5298()) {
                                    str = str + "_patch2_";
                                }
                                if (!str.equals(BuildConfig.FLAVOR)) {
                                    C0987.f4508 = true;
                                    C0987.f4509 = false;
                                    C0987.m6060((Object) "patch only dalvik cache mode on boot");
                                    String absolutePath = C0815.m5318("/system/framework/core.jar").getAbsolutePath();
                                    if (absolutePath == null) {
                                        C0987.m6060((Object) "dalvik cache for core.jar not found");
                                    }
                                    String absolutePath2 = C0815.m5318("/system/framework/services.jar").getAbsolutePath();
                                    if (absolutePath2 == null) {
                                        C0987.m6060((Object) "dalvik cache for services.jar not found");
                                    }
                                    if (absolutePath == null || absolutePath2 == null || absolutePath.equals(BuildConfig.FLAVOR) || absolutePath2.equals(BuildConfig.FLAVOR)) {
                                        obj = "dalvik cache for services.jar not found";
                                        str2 = "/system/framework/services.jar";
                                        C0987.m6060((Object) "dalvik cache patch on boot skip");
                                    } else {
                                        C0815 r1 = new C0815(BuildConfig.FLAVOR);
                                        obj = "dalvik cache for services.jar not found";
                                        StringBuilder sb = new StringBuilder();
                                        str2 = "/system/framework/services.jar";
                                        sb.append(C0987.f4475);
                                        sb.append(".corepatch ");
                                        sb.append(str);
                                        sb.append(" ");
                                        sb.append(absolutePath);
                                        sb.append(" ");
                                        sb.append(absolutePath2);
                                        sb.append(" ");
                                        sb.append(C0987.m6072().getFilesDir());
                                        sb.append(" OnlyDalvik");
                                        C0987.f4435 = r1.m5353(sb.toString());
                                        C0987.m6060((Object) C0987.f4435);
                                        new C0815("w").m5350(4000L);
                                        if (C0987.f4435.contains("SU Java-Code Running!")) {
                                            if (string.contains("patch1") && !C0815.m5289() && !C0815.m5294()) {
                                                C0815.m5255("patch1");
                                            }
                                            if (string.contains("patch2") && !C0815.m5298()) {
                                                C0815.m5255("patch2");
                                            }
                                            if (C0815.m5289() || C0815.m5294() || C0815.m5298()) {
                                                C0815.m5152(254, "Lucky Patcher - " + C0815.m5205((int) R.string.notify_android_patch_on_boot), C0815.m5205((int) R.string.notify_android_patch_on_boot), C0815.m5205((int) R.string.f7305notify_android_patch_on_boot_mes_corejar));
                                            }
                                        }
                                    }
                                } else {
                                    obj = "dalvik cache for services.jar not found";
                                    str2 = "/system/framework/services.jar";
                                }
                                String str3 = (!string.contains("patch3") || C0815.m5302()) ? BuildConfig.FLAVOR : "_patch3_";
                                if (!str3.equals(BuildConfig.FLAVOR)) {
                                    C0987.f4508 = true;
                                    C0987.f4509 = false;
                                    C0987.m6060((Object) "patch only dalvik cache mode on boot");
                                    String absolutePath3 = C0815.m5318("/system/framework/core.jar").getAbsolutePath();
                                    if (absolutePath3 == null) {
                                        C0987.m6060((Object) "dalvik cache for core.jar not found");
                                    }
                                    String absolutePath4 = C0815.m5318(str2).getAbsolutePath();
                                    if (absolutePath4 == null) {
                                        C0987.m6060(obj);
                                    }
                                    if (absolutePath3 == null || absolutePath4 == null || absolutePath3.equals(BuildConfig.FLAVOR) || absolutePath4.equals(BuildConfig.FLAVOR)) {
                                        C0987.m6060((Object) "dalvik cache patch on boot skip");
                                        return;
                                    }
                                    C0987.f4435 = new C0815(BuildConfig.FLAVOR).m5353(C0987.f4475 + ".corepatch " + str3 + " " + absolutePath3 + " " + absolutePath4 + " " + C0987.m6072().getFilesDir() + " OnlyDalvik");
                                    C0987.m6060((Object) C0987.f4435);
                                    if (C0987.f4435.contains("SU Java-Code Running!")) {
                                        new C0815("w").m5350(4000L);
                                        if (!string.contains("patch3") || C0815.m5302()) {
                                            C0815.m5152(255, "Lucky Patcher - " + C0815.m5205((int) R.string.notify_android_patch_on_boot), C0815.m5205((int) R.string.notify_android_patch_on_boot), C0815.m5205((int) R.string.f7306notify_android_patch_on_boot_mes_servicesjar));
                                            return;
                                        }
                                        C0815.m5255("patch3");
                                    }
                                }
                            }
                        }
                    }
                }).start();
            }
        }
        String action = intent.getAction();
        if (action != null) {
            if (!action.equals("android.intent.action.BOOT_COMPLETED")) {
                action.equals("android.intent.action.MY_PACKAGE_REPLACED");
            }
            if (intent.getAction().equals("android.intent.action.MY_PACKAGE_REPLACED")) {
                C0987.m6060((Object) "Start updated LP (boot).");
                Intent intent3 = new Intent("android.intent.action.MAIN");
                intent3.setComponent(new ComponentName(C0987.m6072().getPackageName(), MainActivity.class.getName()));
                intent3.setFlags(270532608);
                intent3.addCategory("android.intent.category.LAUNCHER");
                try {
                    context.startActivity(intent3);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
