package com.lp;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: com.lp.ˏ  reason: contains not printable characters */
/* compiled from: ITestServiceInterface */
public interface C0970 extends IInterface {
    /* renamed from: ʻ  reason: contains not printable characters */
    boolean m5988();

    /* renamed from: com.lp.ˏ$ʻ  reason: contains not printable characters */
    /* compiled from: ITestServiceInterface */
    public static abstract class C0971 extends Binder implements C0970 {
        public IBinder asBinder() {
            return this;
        }

        public C0971() {
            attachInterface(this, "com.lp.ITestServiceInterface");
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static C0970 m5989(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.lp.ITestServiceInterface");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof C0970)) {
                return new C0972(iBinder);
            }
            return (C0970) queryLocalInterface;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            if (i == 1) {
                parcel.enforceInterface("com.lp.ITestServiceInterface");
                boolean r4 = m5988();
                parcel2.writeNoException();
                parcel2.writeInt(r4 ? 1 : 0);
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("com.lp.ITestServiceInterface");
                return true;
            }
        }

        /* renamed from: com.lp.ˏ$ʻ$ʻ  reason: contains not printable characters */
        /* compiled from: ITestServiceInterface */
        private static class C0972 implements C0970 {

            /* renamed from: ʻ  reason: contains not printable characters */
            private IBinder f4276;

            C0972(IBinder iBinder) {
                this.f4276 = iBinder;
            }

            public IBinder asBinder() {
                return this.f4276;
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public boolean m5990() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.lp.ITestServiceInterface");
                    boolean z = false;
                    this.f4276.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
