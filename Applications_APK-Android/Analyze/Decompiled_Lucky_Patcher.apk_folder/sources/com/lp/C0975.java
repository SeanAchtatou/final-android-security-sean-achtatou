package com.lp;

import java.util.List;

/* renamed from: com.lp.ـ  reason: contains not printable characters */
/* compiled from: MenuItem */
public class C0975 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public int f4285;

    /* renamed from: ʼ  reason: contains not printable characters */
    public int f4286 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    public List<Integer> f4287;

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean f4288 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    public int f4289 = 0;

    /* renamed from: ˆ  reason: contains not printable characters */
    public int f4290 = 0;

    /* renamed from: ˈ  reason: contains not printable characters */
    public Runnable f4291 = null;

    /* renamed from: ˉ  reason: contains not printable characters */
    public String f4292 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean f4293 = false;

    public C0975(int i, List<Integer> list, boolean z) {
        this.f4285 = i;
        this.f4287 = list;
        this.f4288 = z;
    }

    public C0975(int i, List<Integer> list, int i2, boolean z) {
        this.f4285 = i;
        this.f4287 = list;
        this.f4288 = z;
        this.f4289 = i2;
    }

    public C0975(int i, int i2, List<Integer> list, int i3, boolean z) {
        this.f4285 = i;
        this.f4287 = list;
        this.f4288 = z;
        this.f4289 = i3;
        this.f4286 = i2;
    }

    public C0975(int i, int i2, List<Integer> list, int i3, boolean z, String str, boolean z2, Runnable runnable) {
        this.f4285 = i;
        this.f4287 = list;
        this.f4288 = z;
        this.f4289 = i3;
        this.f4286 = i2;
        this.f4291 = runnable;
        this.f4292 = str;
        this.f4293 = z2;
    }
}
