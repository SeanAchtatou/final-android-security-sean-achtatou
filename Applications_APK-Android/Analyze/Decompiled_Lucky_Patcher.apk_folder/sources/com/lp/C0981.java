package com.lp;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import com.chelpus.C0815;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ᵢ  reason: contains not printable characters */
/* compiled from: PkgListItemAdapter */
public class C0981 extends BaseExpandableListAdapter {

    /* renamed from: ʻ  reason: contains not printable characters */
    public Comparator<C0980> f4360;

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0980[] f4361;

    /* renamed from: ʽ  reason: contains not printable characters */
    public C0980[] f4362;

    /* renamed from: ʾ  reason: contains not printable characters */
    boolean f4363 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    public int[] f4364 = null;

    /* renamed from: ˆ  reason: contains not printable characters */
    public int[] f4365 = null;

    /* renamed from: ˈ  reason: contains not printable characters */
    public int[] f4366 = null;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f4367;

    /* renamed from: ˊ  reason: contains not printable characters */
    private TextView f4368;

    /* renamed from: ˋ  reason: contains not printable characters */
    private TextView f4369;

    /* renamed from: ˎ  reason: contains not printable characters */
    private TextView f4370;

    /* renamed from: ˏ  reason: contains not printable characters */
    private ImageView f4371;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f4372;

    /* renamed from: י  reason: contains not printable characters */
    private Drawable f4373 = null;

    /* renamed from: ـ  reason: contains not printable characters */
    private Drawable f4374 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Drawable f4375 = null;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Drawable f4376 = null;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private Drawable f4377 = null;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private Drawable f4378 = null;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private Context f4379;

    public long getChildId(int i, int i2) {
        return (long) ((i * 10) + i2);
    }

    public long getGroupId(int i) {
        return (long) i;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int i, int i2) {
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m6009() {
        if (this.f4373 == null) {
            try {
                this.f4373 = C0987.m6069().getDrawable(R.drawable.context_lvl);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (this.f4374 == null) {
            try {
                this.f4374 = C0987.m6069().getDrawable(R.drawable.round_scatter_plot_white_36);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        if (this.f4375 == null) {
            try {
                this.f4375 = C0987.m6069().getDrawable(R.drawable.star);
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        if (this.f4376 == null) {
            try {
                this.f4376 = C0987.m6069().getDrawable(R.drawable.star);
            } catch (Exception e4) {
                e4.printStackTrace();
            }
        }
        if (this.f4377 == null) {
            try {
                this.f4377 = C0987.m6069().getDrawable(R.drawable.klev_down);
            } catch (Exception e5) {
                e5.printStackTrace();
            }
        }
        if (this.f4378 == null) {
            try {
                this.f4378 = C0987.m6069().getDrawable(R.drawable.klev_down);
            } catch (Exception e6) {
                e6.printStackTrace();
            }
        }
    }

    public C0981(Context context, int i, int i2, List<C0980> list) {
        m6009();
        this.f4379 = context;
        this.f4372 = i;
        this.f4367 = i2;
        this.f4361 = (C0980[]) list.toArray(new C0980[list.size()]);
        C0987.f4439 = this;
        Drawable drawable = this.f4373;
        try {
            drawable.setColorFilter(Color.parseColor("#fe6c00"), PorterDuff.Mode.MULTIPLY);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        this.f4373 = drawable;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Integer getChild(int i, int i2) {
        return Integer.valueOf(this.f4364[i2]);
    }

    public int getChildrenCount(int i) {
        if (getGroup(i) != null) {
            String str = null;
            try {
                str = C0987.m6068().getPackageInfo(getGroup(i).f4337, 0).applicationInfo.sourceDir;
            } catch (PackageManager.NameNotFoundException e) {
                try {
                    e.printStackTrace();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (C0987.f4474 && str != null && !str.startsWith("/system/")) {
                this.f4364 = new int[9];
                int[] iArr = this.f4364;
                iArr[0] = R.string.app_info;
                iArr[1] = R.string.contextlaunchapp;
                iArr[2] = R.string.menu_close_application;
                iArr[3] = R.string.advanced_menu;
                iArr[4] = R.string.context_tools;
                iArr[5] = R.string.uninstallapp;
                iArr[6] = R.string.cleardata;
                if (!str.startsWith("/mnt/")) {
                    this.f4364[7] = R.string.move_to_sdcard;
                } else {
                    this.f4364[7] = R.string.move_to_internal;
                }
                this.f4364[8] = R.string.app_dialog_no_root_app_control;
            }
            if (C0987.f4474 && str != null && str.startsWith("/system/")) {
                this.f4364 = new int[8];
                int[] iArr2 = this.f4364;
                iArr2[0] = R.string.app_info;
                iArr2[1] = R.string.contextlaunchapp;
                iArr2[2] = R.string.menu_close_application;
                iArr2[3] = R.string.advanced_menu;
                iArr2[4] = R.string.context_tools;
                iArr2[5] = R.string.uninstallapp;
                iArr2[6] = R.string.cleardata;
                iArr2[7] = R.string.app_dialog_no_root_app_control;
            }
            if (!C0987.f4474) {
                this.f4364 = new int[6];
                int[] iArr3 = this.f4364;
                iArr3[0] = R.string.app_info;
                iArr3[1] = R.string.contextlaunchapp;
                iArr3[2] = R.string.advanced_menu;
                iArr3[3] = R.string.context_tools;
                iArr3[4] = R.string.uninstallapp;
                iArr3[5] = R.string.app_dialog_no_root_app_control;
            }
            if (C0987.f4532.booleanValue()) {
                this.f4364 = new int[3];
                int[] iArr4 = this.f4364;
                iArr4[0] = R.string.app_info;
                iArr4[1] = R.string.contextlaunchapp;
                iArr4[2] = R.string.advanced_menu;
            }
            if (str == null) {
                this.f4364 = new int[1];
                this.f4364[0] = R.string.app_not_found_adapter;
            }
        }
        int[] iArr5 = this.f4364;
        if (iArr5 == null) {
            return 0;
        }
        return iArr5.length;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0980 m6007(int i) {
        return this.f4361[i];
    }

    public int getGroupCount() {
        C0980[] r0 = this.f4361;
        if (r0 != null) {
            return r0.length;
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0273, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:?, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0283, code lost:
        m6011(r6.f4337);
        notifyDataSetChanged();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x028b, code lost:
        com.lp.C0987.m6060((java.lang.Object) ("LuckyPatcher(PackageListItemAdapter): " + r0));
     */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0273 A[ExcHandler: Exception (r0v108 'e' java.lang.Exception A[CUSTOM_DECLARE]), Splitter:B:62:0x01c8] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getGroupView(int r20, boolean r21, android.view.View r22, android.view.ViewGroup r23) {
        /*
            r19 = this;
            r1 = r19
            r2 = r22
            java.lang.String r3 = "default"
            java.lang.String r4 = "no_icon"
            java.lang.String r5 = ""
            android.graphics.drawable.Drawable r0 = r1.f4373
            if (r0 != 0) goto L_0x0020
            android.content.res.Resources r0 = com.lp.C0987.m6069()     // Catch:{ Exception -> 0x001c }
            r6 = 2131230837(0x7f080075, float:1.8077738E38)
            android.graphics.drawable.Drawable r0 = r0.getDrawable(r6)     // Catch:{ Exception -> 0x001c }
            r1.f4373 = r0     // Catch:{ Exception -> 0x001c }
            goto L_0x0020
        L_0x001c:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0020:
            com.lp.ᵔ r6 = r19.getGroup(r20)     // Catch:{ Throwable -> 0x053e }
            if (r6 == 0) goto L_0x0536
            boolean r0 = r6.f4341     // Catch:{ Throwable -> 0x053e }
            if (r0 == 0) goto L_0x002c
            goto L_0x0536
        L_0x002c:
            r0 = 2131296463(0x7f0900cf, float:1.8210843E38)
            r7 = 2131296478(0x7f0900de, float:1.8210874E38)
            r8 = 2131296581(0x7f090145, float:1.8211083E38)
            r9 = 0
            if (r2 != 0) goto L_0x0062
            android.content.Context r2 = r1.f4379     // Catch:{ Throwable -> 0x053e }
            java.lang.String r10 = "layout_inflater"
            java.lang.Object r2 = r2.getSystemService(r10)     // Catch:{ Throwable -> 0x053e }
            android.view.LayoutInflater r2 = (android.view.LayoutInflater) r2     // Catch:{ Throwable -> 0x053e }
            int r10 = r1.f4372     // Catch:{ Throwable -> 0x053e }
            r11 = r23
            android.view.View r2 = r2.inflate(r10, r11, r9)     // Catch:{ Throwable -> 0x053e }
            android.view.View r8 = r2.findViewById(r8)     // Catch:{ Throwable -> 0x053e }
            android.widget.ImageView r8 = (android.widget.ImageView) r8     // Catch:{ Throwable -> 0x053e }
            android.view.View r7 = r2.findViewById(r7)     // Catch:{ Throwable -> 0x053e }
            android.widget.ImageView r7 = (android.widget.ImageView) r7     // Catch:{ Throwable -> 0x053e }
            android.view.View r0 = r2.findViewById(r0)     // Catch:{ Throwable -> 0x053e }
            android.widget.ImageView r0 = (android.widget.ImageView) r0     // Catch:{ Throwable -> 0x053e }
            android.graphics.drawable.Drawable r10 = r1.f4374     // Catch:{ Throwable -> 0x053e }
            r8.setImageDrawable(r10)     // Catch:{ Throwable -> 0x053e }
            goto L_0x0079
        L_0x0062:
            android.view.View r8 = r2.findViewById(r8)     // Catch:{ Throwable -> 0x053e }
            android.widget.ImageView r8 = (android.widget.ImageView) r8     // Catch:{ Throwable -> 0x053e }
            android.view.View r7 = r2.findViewById(r7)     // Catch:{ Throwable -> 0x053e }
            android.widget.ImageView r7 = (android.widget.ImageView) r7     // Catch:{ Throwable -> 0x053e }
            android.view.View r0 = r2.findViewById(r0)     // Catch:{ Throwable -> 0x053e }
            android.widget.ImageView r0 = (android.widget.ImageView) r0     // Catch:{ Throwable -> 0x053e }
            android.graphics.drawable.Drawable r10 = r1.f4374     // Catch:{ Throwable -> 0x053e }
            r8.setImageDrawable(r10)     // Catch:{ Throwable -> 0x053e }
        L_0x0079:
            r10 = r8
            r8 = r0
            r0 = 2131296633(0x7f090179, float:1.8211188E38)
            android.view.View r0 = r2.findViewById(r0)     // Catch:{ Throwable -> 0x053e }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Throwable -> 0x053e }
            r1.f4368 = r0     // Catch:{ Throwable -> 0x053e }
            r0 = 2131296631(0x7f090177, float:1.8211184E38)
            android.view.View r0 = r2.findViewById(r0)     // Catch:{ Throwable -> 0x053e }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Throwable -> 0x053e }
            r1.f4369 = r0     // Catch:{ Throwable -> 0x053e }
            r0 = 2131296431(0x7f0900af, float:1.8210778E38)
            android.view.View r0 = r2.findViewById(r0)     // Catch:{ Throwable -> 0x053e }
            android.widget.ImageView r0 = (android.widget.ImageView) r0     // Catch:{ Throwable -> 0x053e }
            r1.f4371 = r0     // Catch:{ Throwable -> 0x053e }
            r0 = 2131296464(0x7f0900d0, float:1.8210845E38)
            android.view.View r0 = r2.findViewById(r0)     // Catch:{ Throwable -> 0x053e }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Throwable -> 0x053e }
            r1.f4370 = r0     // Catch:{ Throwable -> 0x053e }
            r0 = 2131296484(0x7f0900e4, float:1.8210886E38)
            android.view.View r0 = r2.findViewById(r0)     // Catch:{ Throwable -> 0x053e }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Throwable -> 0x053e }
            r11 = 2131296259(0x7f090003, float:1.821043E38)
            android.view.View r11 = r2.findViewById(r11)     // Catch:{ Throwable -> 0x053e }
            android.widget.CheckBox r11 = (android.widget.CheckBox) r11     // Catch:{ Throwable -> 0x053e }
            boolean r12 = com.lp.C0987.f4514     // Catch:{ Throwable -> 0x053e }
            r13 = 8
            if (r12 == 0) goto L_0x00e3
            r11.setVisibility(r9)     // Catch:{ Throwable -> 0x053e }
            r7.setVisibility(r13)     // Catch:{ Throwable -> 0x053e }
            r8.setVisibility(r13)     // Catch:{ Throwable -> 0x053e }
            r0.setVisibility(r13)     // Catch:{ Throwable -> 0x053e }
            android.widget.TextView r12 = r1.f4370     // Catch:{ Throwable -> 0x053e }
            r12.setVisibility(r13)     // Catch:{ Throwable -> 0x053e }
            r10.setVisibility(r13)     // Catch:{ Throwable -> 0x053e }
            java.lang.Integer r12 = java.lang.Integer.valueOf(r20)     // Catch:{ Throwable -> 0x053e }
            r11.setTag(r12)     // Catch:{ Throwable -> 0x053e }
            com.lp.ᵢ$1 r12 = new com.lp.ᵢ$1     // Catch:{ Throwable -> 0x053e }
            r12.<init>()     // Catch:{ Throwable -> 0x053e }
            r11.setOnClickListener(r12)     // Catch:{ Throwable -> 0x053e }
            goto L_0x00fd
        L_0x00e3:
            int r12 = r11.getVisibility()     // Catch:{ Throwable -> 0x053e }
            if (r12 != 0) goto L_0x00fd
            r11.setVisibility(r13)     // Catch:{ Throwable -> 0x053e }
            r7.setVisibility(r9)     // Catch:{ Throwable -> 0x053e }
            r8.setVisibility(r9)     // Catch:{ Throwable -> 0x053e }
            r0.setVisibility(r9)     // Catch:{ Throwable -> 0x053e }
            android.widget.TextView r12 = r1.f4370     // Catch:{ Throwable -> 0x053e }
            r12.setVisibility(r9)     // Catch:{ Throwable -> 0x053e }
            r10.setVisibility(r9)     // Catch:{ Throwable -> 0x053e }
        L_0x00fd:
            boolean r12 = r6.f4356     // Catch:{ Throwable -> 0x053e }
            r14 = 1
            if (r12 == 0) goto L_0x0106
            r11.setChecked(r14)     // Catch:{ Throwable -> 0x053e }
            goto L_0x0109
        L_0x0106:
            r11.setChecked(r9)     // Catch:{ Throwable -> 0x053e }
        L_0x0109:
            boolean r11 = r6.f4351     // Catch:{ Throwable -> 0x053e }
            java.lang.String r12 = "#FF999999"
            if (r11 == 0) goto L_0x0123
            r8.setVisibility(r9)     // Catch:{ Throwable -> 0x053e }
            android.graphics.drawable.Drawable r11 = r1.f4377     // Catch:{ Throwable -> 0x053e }
            r8.setImageDrawable(r11)     // Catch:{ Throwable -> 0x053e }
            java.lang.String r11 = "#FFcaff00"
            int r11 = android.graphics.Color.parseColor(r11)     // Catch:{ Throwable -> 0x053e }
            android.graphics.PorterDuff$Mode r15 = android.graphics.PorterDuff.Mode.MULTIPLY     // Catch:{ Throwable -> 0x053e }
            r8.setColorFilter(r11, r15)     // Catch:{ Throwable -> 0x053e }
            goto L_0x0134
        L_0x0123:
            r8.setVisibility(r9)     // Catch:{ Throwable -> 0x053e }
            android.graphics.drawable.Drawable r11 = r1.f4378     // Catch:{ Throwable -> 0x053e }
            r8.setImageDrawable(r11)     // Catch:{ Throwable -> 0x053e }
            int r11 = android.graphics.Color.parseColor(r12)     // Catch:{ Throwable -> 0x053e }
            android.graphics.PorterDuff$Mode r15 = android.graphics.PorterDuff.Mode.MULTIPLY     // Catch:{ Throwable -> 0x053e }
            r8.setColorFilter(r11, r15)     // Catch:{ Throwable -> 0x053e }
        L_0x0134:
            boolean r11 = r6.f4352     // Catch:{ Throwable -> 0x053e }
            r15 = -16711936(0xffffffffff00ff00, float:-1.7146522E38)
            java.lang.String r14 = "INT"
            r9 = 23
            if (r11 != 0) goto L_0x0160
            int r11 = com.lp.C0987.f4489     // Catch:{ Throwable -> 0x053e }
            if (r11 < r9) goto L_0x0148
            r7.setVisibility(r13)     // Catch:{ Throwable -> 0x053e }
            r9 = 0
            goto L_0x0149
        L_0x0148:
            r9 = 1
        L_0x0149:
            boolean r11 = r6.f4357     // Catch:{ Throwable -> 0x053e }
            if (r11 == 0) goto L_0x0158
            java.lang.String r11 = "SD"
            r0.setText(r11)     // Catch:{ Throwable -> 0x053e }
            r11 = -256(0xffffffffffffff00, float:NaN)
            r0.setTextColor(r11)     // Catch:{ Throwable -> 0x053e }
            goto L_0x015e
        L_0x0158:
            r0.setText(r14)     // Catch:{ Throwable -> 0x053e }
            r0.setTextColor(r15)     // Catch:{ Throwable -> 0x053e }
        L_0x015e:
            r11 = 0
            goto L_0x0191
        L_0x0160:
            java.lang.String r11 = r6.f4358     // Catch:{ Throwable -> 0x053e }
            java.lang.String r13 = "/system"
            boolean r11 = r11.startsWith(r13)     // Catch:{ Throwable -> 0x053e }
            if (r11 == 0) goto L_0x017e
            java.lang.String r11 = "SYS"
            r0.setText(r11)     // Catch:{ Throwable -> 0x053e }
            r11 = -65281(0xffffffffffff00ff, float:NaN)
            r0.setTextColor(r11)     // Catch:{ Throwable -> 0x053e }
            int r11 = com.lp.C0987.f4489     // Catch:{ Throwable -> 0x053e }
            if (r11 < r9) goto L_0x018f
            r9 = 0
            r7.setVisibility(r9)     // Catch:{ Throwable -> 0x053e }
            goto L_0x018f
        L_0x017e:
            r0.setText(r14)     // Catch:{ Throwable -> 0x053e }
            r0.setTextColor(r15)     // Catch:{ Throwable -> 0x053e }
            int r11 = com.lp.C0987.f4489     // Catch:{ Throwable -> 0x053e }
            if (r11 < r9) goto L_0x018f
            r9 = 8
            r7.setVisibility(r9)     // Catch:{ Throwable -> 0x053e }
            r9 = 0
            goto L_0x015e
        L_0x018f:
            r9 = 1
            goto L_0x015e
        L_0x0191:
            r0.setClickable(r11)     // Catch:{ Throwable -> 0x053e }
            if (r9 == 0) goto L_0x01c0
            boolean r0 = r6.f4353     // Catch:{ Throwable -> 0x053e }
            if (r0 == 0) goto L_0x01ae
            r7.setVisibility(r11)     // Catch:{ Throwable -> 0x053e }
            android.graphics.drawable.Drawable r0 = r1.f4375     // Catch:{ Throwable -> 0x053e }
            r7.setImageDrawable(r0)     // Catch:{ Throwable -> 0x053e }
            java.lang.String r0 = "#FFffe200"
            int r0 = android.graphics.Color.parseColor(r0)     // Catch:{ Throwable -> 0x053e }
            android.graphics.PorterDuff$Mode r9 = android.graphics.PorterDuff.Mode.MULTIPLY     // Catch:{ Throwable -> 0x053e }
            r7.setColorFilter(r0, r9)     // Catch:{ Throwable -> 0x053e }
            goto L_0x01c0
        L_0x01ae:
            r9 = 0
            r7.setVisibility(r9)     // Catch:{ Throwable -> 0x053e }
            android.graphics.drawable.Drawable r0 = r1.f4376     // Catch:{ Throwable -> 0x053e }
            r7.setImageDrawable(r0)     // Catch:{ Throwable -> 0x053e }
            int r0 = android.graphics.Color.parseColor(r12)     // Catch:{ Throwable -> 0x053e }
            android.graphics.PorterDuff$Mode r9 = android.graphics.PorterDuff.Mode.MULTIPLY     // Catch:{ Throwable -> 0x053e }
            r7.setColorFilter(r0, r9)     // Catch:{ Throwable -> 0x053e }
        L_0x01c0:
            android.widget.TextView r0 = r1.f4368     // Catch:{ Throwable -> 0x053e }
            java.lang.String r9 = r6.f4338     // Catch:{ Throwable -> 0x053e }
            r0.setText(r9)     // Catch:{ Throwable -> 0x053e }
            r9 = 0
            android.content.SharedPreferences r0 = com.lp.C0987.m6073()     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            r11 = 0
            boolean r0 = r0.getBoolean(r4, r11)     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            if (r0 != 0) goto L_0x02b7
            boolean r0 = com.lp.C0987.f4466     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            if (r0 == 0) goto L_0x0266
            android.widget.ImageView r0 = r1.f4371     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            r0.setImageDrawable(r9)     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            android.graphics.drawable.Drawable r0 = r6.f4339     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            if (r0 != 0) goto L_0x0252
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            r0.<init>()     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            java.lang.String r11 = "load icon for "
            r0.append(r11)     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            java.lang.String r11 = r6.f4338     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            r0.append(r11)     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            java.lang.String r0 = r0.toString()     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            com.lp.C0987.m6060(r0)     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            android.content.res.Resources r0 = com.lp.C0987.m6069()     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            float r0 = r0.density     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            r11 = 1108082688(0x420c0000, float:35.0)
            float r0 = r0 * r11
            r11 = 1056964608(0x3f000000, float:0.5)
            float r0 = r0 + r11
            int r11 = (int) r0
            android.content.pm.PackageManager r0 = com.lp.C0987.m6068()     // Catch:{ NameNotFoundException -> 0x0218 }
            java.lang.String r12 = r6.f4337     // Catch:{ NameNotFoundException -> 0x0218 }
            android.graphics.drawable.Drawable r0 = r0.getApplicationIcon(r12)     // Catch:{ NameNotFoundException -> 0x0218 }
            android.graphics.Bitmap r0 = com.chelpus.C0815.m5135(r0)     // Catch:{ NameNotFoundException -> 0x0218 }
            r12 = r0
            goto L_0x021d
        L_0x0218:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            r12 = r9
        L_0x021d:
            int r15 = r12.getWidth()     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            int r0 = r12.getHeight()     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            float r11 = (float) r11     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            float r13 = (float) r15     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            float r13 = r11 / r13
            float r14 = (float) r0     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            float r11 = r11 / r14
            android.graphics.Matrix r14 = new android.graphics.Matrix     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            r14.<init>()     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            r14.postScale(r13, r11)     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            r13 = 0
            r11 = 0
            r18 = 1
            r17 = r14
            r14 = r11
            r16 = r0
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createBitmap(r12, r13, r14, r15, r16, r17, r18)     // Catch:{ OutOfMemoryError -> 0x0248, Exception -> 0x0273 }
            android.graphics.drawable.BitmapDrawable r11 = new android.graphics.drawable.BitmapDrawable     // Catch:{ OutOfMemoryError -> 0x0248, Exception -> 0x0273 }
            r11.<init>(r0)     // Catch:{ OutOfMemoryError -> 0x0248, Exception -> 0x0273 }
            r6.f4339 = r11     // Catch:{ OutOfMemoryError -> 0x0248, Exception -> 0x0273 }
            goto L_0x024f
        L_0x0248:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            r11 = 0
            com.lp.C0987.f4466 = r11     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
        L_0x024f:
            r6.m6005()     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
        L_0x0252:
            boolean r0 = r6.f4355     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            if (r0 != 0) goto L_0x025e
            android.widget.ImageView r0 = r1.f4371     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            android.graphics.drawable.Drawable r11 = r1.f4373     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            r0.setImageDrawable(r11)     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            goto L_0x02b7
        L_0x025e:
            android.widget.ImageView r0 = r1.f4371     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            android.graphics.drawable.Drawable r11 = r6.f4339     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            r0.setImageDrawable(r11)     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            goto L_0x02b7
        L_0x0266:
            android.content.Context r0 = r1.f4379     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            java.lang.String r11 = "Out of memory! Icon not loaded!"
            r12 = 0
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r11, r12)     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            r0.show()     // Catch:{ OutOfMemoryError -> 0x02a0, Exception -> 0x0273 }
            goto L_0x02b7
        L_0x0273:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x053e }
            java.lang.String r4 = r6.f4338     // Catch:{ Throwable -> 0x053e }
            if (r4 == 0) goto L_0x0283
            java.lang.String r4 = r6.f4338     // Catch:{ Throwable -> 0x053e }
            boolean r4 = r4.equals(r5)     // Catch:{ Throwable -> 0x053e }
            if (r4 == 0) goto L_0x028b
        L_0x0283:
            java.lang.String r4 = r6.f4337     // Catch:{ Throwable -> 0x053e }
            r1.m6011(r4)     // Catch:{ Throwable -> 0x053e }
            r19.notifyDataSetChanged()     // Catch:{ Throwable -> 0x053e }
        L_0x028b:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x053e }
            r4.<init>()     // Catch:{ Throwable -> 0x053e }
            java.lang.String r11 = "LuckyPatcher(PackageListItemAdapter): "
            r4.append(r11)     // Catch:{ Throwable -> 0x053e }
            r4.append(r0)     // Catch:{ Throwable -> 0x053e }
            java.lang.String r0 = r4.toString()     // Catch:{ Throwable -> 0x053e }
            com.lp.C0987.m6060(r0)     // Catch:{ Throwable -> 0x053e }
            goto L_0x02b7
        L_0x02a0:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x053e }
            java.lang.System.gc()     // Catch:{ Throwable -> 0x053e }
            android.content.SharedPreferences r0 = com.lp.C0987.m6073()     // Catch:{ Throwable -> 0x053e }
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ Throwable -> 0x053e }
            r11 = 1
            android.content.SharedPreferences$Editor r0 = r0.putBoolean(r4, r11)     // Catch:{ Throwable -> 0x053e }
            r0.commit()     // Catch:{ Throwable -> 0x053e }
        L_0x02b7:
            android.widget.TextView r0 = r1.f4368     // Catch:{ Throwable -> 0x053e }
            android.content.Context r4 = r1.f4379     // Catch:{ Throwable -> 0x053e }
            int r11 = com.lp.C0987.m6076()     // Catch:{ Throwable -> 0x053e }
            r0.setTextAppearance(r4, r11)     // Catch:{ Throwable -> 0x053e }
            android.widget.TextView r0 = r1.f4369     // Catch:{ Throwable -> 0x053e }
            android.content.Context r4 = r1.f4379     // Catch:{ Throwable -> 0x053e }
            int r11 = com.lp.C0987.m6076()     // Catch:{ Throwable -> 0x053e }
            r0.setTextAppearance(r4, r11)     // Catch:{ Throwable -> 0x053e }
            android.widget.TextView r0 = r1.f4369     // Catch:{ Throwable -> 0x053e }
            android.content.Context r4 = r1.f4379     // Catch:{ Throwable -> 0x053e }
            r11 = 16973894(0x1030046, float:2.4061096E-38)
            r0.setTextAppearance(r4, r11)     // Catch:{ Throwable -> 0x053e }
            java.lang.String r0 = "#ffcc7943"
            boolean r4 = r6.f4349     // Catch:{ Throwable -> 0x053e }
            if (r4 == 0) goto L_0x02df
            java.lang.String r0 = "#ff00ffff"
        L_0x02df:
            boolean r4 = r6.f4350     // Catch:{ Throwable -> 0x053e }
            if (r4 == 0) goto L_0x02e5
            java.lang.String r0 = "#c5b5ff"
        L_0x02e5:
            boolean r4 = r6.f4348     // Catch:{ Throwable -> 0x053e }
            if (r4 == 0) goto L_0x02eb
            java.lang.String r0 = "#ff00ff73"
        L_0x02eb:
            boolean r4 = r6.f4349     // Catch:{ Throwable -> 0x053e }
            if (r4 != 0) goto L_0x02fd
            boolean r4 = r6.f4348     // Catch:{ Throwable -> 0x053e }
            if (r4 != 0) goto L_0x02fd
            boolean r4 = r6.f4350     // Catch:{ Throwable -> 0x053e }
            if (r4 != 0) goto L_0x02fd
            boolean r4 = r6.f4352     // Catch:{ Throwable -> 0x053e }
            if (r4 != 0) goto L_0x02fd
            java.lang.String r0 = "#ffff0055"
        L_0x02fd:
            boolean r4 = r6.f4352     // Catch:{ Throwable -> 0x053e }
            if (r4 == 0) goto L_0x0303
            java.lang.String r0 = "#fffd8617"
        L_0x0303:
            boolean r4 = r6.f4347     // Catch:{ Throwable -> 0x053e }
            if (r4 == 0) goto L_0x0309
            java.lang.String r0 = "#fff0e442"
        L_0x0309:
            boolean r4 = r6.f4343     // Catch:{ Throwable -> 0x053e }
            if (r4 != 0) goto L_0x0319
            boolean r4 = r6.f4345     // Catch:{ Throwable -> 0x053e }
            if (r4 != 0) goto L_0x0319
            boolean r4 = r6.f4344     // Catch:{ Throwable -> 0x053e }
            if (r4 != 0) goto L_0x0319
            boolean r4 = r6.f4346     // Catch:{ Throwable -> 0x053e }
            if (r4 == 0) goto L_0x031b
        L_0x0319:
            java.lang.String r0 = "#ffff00ff"
        L_0x031b:
            boolean r4 = r6.f4355     // Catch:{ Throwable -> 0x053e }
            if (r4 != 0) goto L_0x0321
            java.lang.String r0 = "#ff888888"
        L_0x0321:
            android.widget.TextView r4 = r1.f4368     // Catch:{ Throwable -> 0x053e }
            int r0 = android.graphics.Color.parseColor(r0)     // Catch:{ Throwable -> 0x053e }
            r4.setTextColor(r0)     // Catch:{ Throwable -> 0x053e }
            android.widget.TextView r0 = r1.f4369     // Catch:{ Throwable -> 0x053e }
            r4 = -7829368(0xffffffffff888888, float:NaN)
            r0.setTextColor(r4)     // Catch:{ Throwable -> 0x053e }
            android.content.SharedPreferences r0 = com.lp.C0987.m6073()     // Catch:{ Throwable -> 0x053e }
            java.lang.String r4 = "force_language"
            java.lang.String r0 = r0.getString(r4, r3)     // Catch:{ Throwable -> 0x053e }
            boolean r3 = r0.equals(r3)     // Catch:{ Throwable -> 0x053e }
            if (r3 == 0) goto L_0x036f
            android.content.res.Resources r0 = android.content.res.Resources.getSystem()     // Catch:{ Throwable -> 0x053e }
            android.content.res.Configuration r0 = r0.getConfiguration()     // Catch:{ Throwable -> 0x053e }
            java.util.Locale r0 = r0.locale     // Catch:{ Throwable -> 0x053e }
            java.util.Locale r3 = java.util.Locale.getDefault()     // Catch:{ Throwable -> 0x053e }
            java.util.Locale.setDefault(r3)     // Catch:{ Throwable -> 0x053e }
            android.content.res.Configuration r3 = new android.content.res.Configuration     // Catch:{ Throwable -> 0x053e }
            r3.<init>()     // Catch:{ Throwable -> 0x053e }
            r3.locale = r0     // Catch:{ Throwable -> 0x053e }
            android.content.res.Resources r0 = com.lp.C0987.m6069()     // Catch:{ Exception -> 0x036a }
            android.content.res.Resources r4 = com.lp.C0987.m6069()     // Catch:{ Exception -> 0x036a }
            android.util.DisplayMetrics r4 = r4.getDisplayMetrics()     // Catch:{ Exception -> 0x036a }
            r0.updateConfiguration(r3, r4)     // Catch:{ Exception -> 0x036a }
            goto L_0x03d3
        L_0x036a:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x053e }
            goto L_0x03d3
        L_0x036f:
            java.lang.String r3 = "_"
            java.lang.String[] r0 = r0.split(r3)     // Catch:{ Throwable -> 0x053e }
            int r3 = r0.length     // Catch:{ Throwable -> 0x053e }
            r4 = 1
            if (r3 != r4) goto L_0x0381
            java.util.Locale r9 = new java.util.Locale     // Catch:{ Throwable -> 0x053e }
            r3 = 0
            r4 = r0[r3]     // Catch:{ Throwable -> 0x053e }
            r9.<init>(r4)     // Catch:{ Throwable -> 0x053e }
        L_0x0381:
            int r3 = r0.length     // Catch:{ Throwable -> 0x053e }
            r4 = 2
            if (r3 != r4) goto L_0x03a4
            java.util.Locale r9 = new java.util.Locale     // Catch:{ Throwable -> 0x053e }
            r3 = 0
            r11 = r0[r3]     // Catch:{ Throwable -> 0x053e }
            r3 = 1
            r12 = r0[r3]     // Catch:{ Throwable -> 0x053e }
            r9.<init>(r11, r12, r5)     // Catch:{ Throwable -> 0x053e }
            r11 = r0[r3]     // Catch:{ Throwable -> 0x053e }
            java.lang.String r3 = "rBR"
            boolean r3 = r11.equals(r3)     // Catch:{ Throwable -> 0x053e }
            if (r3 == 0) goto L_0x03a4
            java.util.Locale r9 = new java.util.Locale     // Catch:{ Throwable -> 0x053e }
            r3 = 0
            r11 = r0[r3]     // Catch:{ Throwable -> 0x053e }
            java.lang.String r3 = "BR"
            r9.<init>(r11, r3)     // Catch:{ Throwable -> 0x053e }
        L_0x03a4:
            int r3 = r0.length     // Catch:{ Throwable -> 0x053e }
            r11 = 3
            if (r3 != r11) goto L_0x03b5
            java.util.Locale r9 = new java.util.Locale     // Catch:{ Throwable -> 0x053e }
            r3 = 0
            r11 = r0[r3]     // Catch:{ Throwable -> 0x053e }
            r3 = 1
            r12 = r0[r3]     // Catch:{ Throwable -> 0x053e }
            r0 = r0[r4]     // Catch:{ Throwable -> 0x053e }
            r9.<init>(r11, r12, r0)     // Catch:{ Throwable -> 0x053e }
        L_0x03b5:
            java.util.Locale.setDefault(r9)     // Catch:{ Throwable -> 0x053e }
            android.content.res.Configuration r0 = new android.content.res.Configuration     // Catch:{ Throwable -> 0x053e }
            r0.<init>()     // Catch:{ Throwable -> 0x053e }
            r0.locale = r9     // Catch:{ Throwable -> 0x053e }
            android.content.res.Resources r3 = com.lp.C0987.m6069()     // Catch:{ Exception -> 0x03cf }
            android.content.res.Resources r4 = com.lp.C0987.m6069()     // Catch:{ Exception -> 0x03cf }
            android.util.DisplayMetrics r4 = r4.getDisplayMetrics()     // Catch:{ Exception -> 0x03cf }
            r3.updateConfiguration(r0, r4)     // Catch:{ Exception -> 0x03cf }
            goto L_0x03d3
        L_0x03cf:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x053e }
        L_0x03d3:
            boolean r0 = r6.f4349     // Catch:{ Throwable -> 0x053e }
            if (r0 == 0) goto L_0x03df
            r0 = 2131690191(0x7f0f02cf, float:1.9009419E38)
            java.lang.String r0 = com.chelpus.C0815.m5205(r0)     // Catch:{ Throwable -> 0x053e }
            goto L_0x03e0
        L_0x03df:
            r0 = r5
        L_0x03e0:
            boolean r3 = r6.f4348     // Catch:{ Throwable -> 0x053e }
            if (r3 != 0) goto L_0x03f4
            boolean r3 = r6.f4349     // Catch:{ Throwable -> 0x053e }
            if (r3 != 0) goto L_0x041b
            boolean r3 = r6.f4350     // Catch:{ Throwable -> 0x053e }
            if (r3 != 0) goto L_0x041b
            r0 = 2131690201(0x7f0f02d9, float:1.9009439E38)
            java.lang.String r0 = com.chelpus.C0815.m5205(r0)     // Catch:{ Throwable -> 0x053e }
            goto L_0x041b
        L_0x03f4:
            boolean r3 = r6.f4349     // Catch:{ Throwable -> 0x053e }
            if (r3 != 0) goto L_0x0400
            r0 = 2131690198(0x7f0f02d6, float:1.9009433E38)
            java.lang.String r0 = com.chelpus.C0815.m5205(r0)     // Catch:{ Throwable -> 0x053e }
            goto L_0x041b
        L_0x0400:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x053e }
            r3.<init>()     // Catch:{ Throwable -> 0x053e }
            r3.append(r0)     // Catch:{ Throwable -> 0x053e }
            java.lang.String r0 = "\n"
            r3.append(r0)     // Catch:{ Throwable -> 0x053e }
            r0 = 2131690198(0x7f0f02d6, float:1.9009433E38)
            java.lang.String r0 = com.chelpus.C0815.m5205(r0)     // Catch:{ Throwable -> 0x053e }
            r3.append(r0)     // Catch:{ Throwable -> 0x053e }
            java.lang.String r0 = r3.toString()     // Catch:{ Throwable -> 0x053e }
        L_0x041b:
            boolean r3 = r6.f4347     // Catch:{ Throwable -> 0x053e }
            if (r3 == 0) goto L_0x0426
            r0 = 2131690195(0x7f0f02d3, float:1.9009427E38)
            java.lang.String r0 = com.chelpus.C0815.m5205(r0)     // Catch:{ Throwable -> 0x053e }
        L_0x0426:
            boolean r3 = r6.f4350     // Catch:{ Throwable -> 0x053e }
            if (r3 == 0) goto L_0x0453
            boolean r3 = r0.equals(r5)     // Catch:{ Throwable -> 0x053e }
            if (r3 == 0) goto L_0x0438
            r0 = 2131690193(0x7f0f02d1, float:1.9009423E38)
            java.lang.String r0 = com.chelpus.C0815.m5205(r0)     // Catch:{ Throwable -> 0x053e }
            goto L_0x0453
        L_0x0438:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x053e }
            r3.<init>()     // Catch:{ Throwable -> 0x053e }
            r3.append(r0)     // Catch:{ Throwable -> 0x053e }
            java.lang.String r0 = "\n"
            r3.append(r0)     // Catch:{ Throwable -> 0x053e }
            r0 = 2131690193(0x7f0f02d1, float:1.9009423E38)
            java.lang.String r0 = com.chelpus.C0815.m5205(r0)     // Catch:{ Throwable -> 0x053e }
            r3.append(r0)     // Catch:{ Throwable -> 0x053e }
            java.lang.String r0 = r3.toString()     // Catch:{ Throwable -> 0x053e }
        L_0x0453:
            android.widget.TextView r3 = r1.f4369     // Catch:{ Throwable -> 0x053e }
            r3.setText(r0)     // Catch:{ Throwable -> 0x053e }
            android.widget.TextView r0 = r1.f4368     // Catch:{ Throwable -> 0x053e }
            android.graphics.Typeface r0 = r0.getTypeface()     // Catch:{ Throwable -> 0x053e }
            android.widget.TextView r3 = r1.f4368     // Catch:{ Throwable -> 0x053e }
            r4 = 0
            r3.setTypeface(r0, r4)     // Catch:{ Throwable -> 0x053e }
            boolean r0 = r6.f4340     // Catch:{ Throwable -> 0x053e }
            if (r0 == 0) goto L_0x047a
            android.widget.TextView r0 = r1.f4370     // Catch:{ Throwable -> 0x053e }
            r0.setVisibility(r4)     // Catch:{ Throwable -> 0x053e }
            android.widget.TextView r0 = r1.f4370     // Catch:{ Throwable -> 0x053e }
            r3 = 2131690052(0x7f0f0244, float:1.9009137E38)
            java.lang.String r3 = com.chelpus.C0815.m5205(r3)     // Catch:{ Throwable -> 0x053e }
            r0.setText(r3)     // Catch:{ Throwable -> 0x053e }
            goto L_0x0480
        L_0x047a:
            android.widget.TextView r0 = r1.f4370     // Catch:{ Throwable -> 0x053e }
            r3 = 4
            r0.setVisibility(r3)     // Catch:{ Throwable -> 0x053e }
        L_0x0480:
            boolean r0 = r6.f4359     // Catch:{ Throwable -> 0x053e }
            if (r0 == 0) goto L_0x0496
            r3 = 0
            r10.setVisibility(r3)     // Catch:{ Throwable -> 0x053e }
            java.lang.String r0 = "#FF00BCD4"
            int r0 = android.graphics.Color.parseColor(r0)     // Catch:{ Throwable -> 0x053e }
            android.graphics.PorterDuff$Mode r3 = android.graphics.PorterDuff.Mode.MULTIPLY     // Catch:{ Throwable -> 0x053e }
            r10.setColorFilter(r0, r3)     // Catch:{ Throwable -> 0x053e }
            r3 = 8
            goto L_0x049b
        L_0x0496:
            r3 = 8
            r10.setVisibility(r3)     // Catch:{ Throwable -> 0x053e }
        L_0x049b:
            boolean r0 = com.lp.C0987.f4514     // Catch:{ Throwable -> 0x053e }
            if (r0 == 0) goto L_0x0535
            android.widget.TextView r0 = r1.f4370     // Catch:{ Throwable -> 0x053e }
            r0.setVisibility(r3)     // Catch:{ Throwable -> 0x053e }
            if (r10 == 0) goto L_0x04a9
            r10.setVisibility(r3)     // Catch:{ Throwable -> 0x053e }
        L_0x04a9:
            if (r7 == 0) goto L_0x04ae
            r7.setVisibility(r3)     // Catch:{ Throwable -> 0x053e }
        L_0x04ae:
            if (r8 == 0) goto L_0x04b3
            r8.setVisibility(r3)     // Catch:{ Throwable -> 0x053e }
        L_0x04b3:
            int r0 = com.lp.C0987.f4515     // Catch:{ Throwable -> 0x053e }
            r3 = 2131689589(0x7f0f0075, float:1.9008198E38)
            if (r0 == r3) goto L_0x04c1
            int r0 = com.lp.C0987.f4515     // Catch:{ Throwable -> 0x053e }
            r3 = 2131689591(0x7f0f0077, float:1.9008202E38)
            if (r0 != r3) goto L_0x0535
        L_0x04c1:
            android.widget.TextView r0 = r1.f4370     // Catch:{ Throwable -> 0x053e }
            r3 = 8
            r0.setVisibility(r3)     // Catch:{ Throwable -> 0x053e }
            if (r10 == 0) goto L_0x04cd
            r10.setVisibility(r3)     // Catch:{ Throwable -> 0x053e }
        L_0x04cd:
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x053e }
            java.lang.String r3 = r6.f4358     // Catch:{ Throwable -> 0x053e }
            r0.<init>(r3)     // Catch:{ Throwable -> 0x053e }
            long r3 = r0.length()     // Catch:{ Throwable -> 0x053e }
            float r0 = (float) r3     // Catch:{ Throwable -> 0x053e }
            r3 = 1233125376(0x49800000, float:1048576.0)
            float r0 = r0 / r3
            boolean r3 = r6.f4359     // Catch:{ Throwable -> 0x053e }
            if (r3 == 0) goto L_0x0500
            java.lang.String r3 = r6.f4337     // Catch:{ Throwable -> 0x053e }
            java.util.ArrayList r3 = com.chelpus.C0815.m5295(r3)     // Catch:{ Throwable -> 0x053e }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ Throwable -> 0x053e }
        L_0x04ea:
            boolean r4 = r3.hasNext()     // Catch:{ Throwable -> 0x053e }
            if (r4 == 0) goto L_0x0500
            java.lang.Object r4 = r3.next()     // Catch:{ Throwable -> 0x053e }
            java.io.File r4 = (java.io.File) r4     // Catch:{ Throwable -> 0x053e }
            long r4 = r4.length()     // Catch:{ Throwable -> 0x053e }
            float r4 = (float) r4     // Catch:{ Throwable -> 0x053e }
            r5 = 1233125376(0x49800000, float:1048576.0)
            float r4 = r4 / r5
            float r0 = r0 + r4
            goto L_0x04ea
        L_0x0500:
            android.widget.TextView r3 = r1.f4369     // Catch:{ Throwable -> 0x053e }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x053e }
            r4.<init>()     // Catch:{ Throwable -> 0x053e }
            r5 = 2131689524(0x7f0f0034, float:1.9008066E38)
            java.lang.String r5 = com.chelpus.C0815.m5205(r5)     // Catch:{ Throwable -> 0x053e }
            r4.append(r5)     // Catch:{ Throwable -> 0x053e }
            java.lang.String r5 = " "
            r4.append(r5)     // Catch:{ Throwable -> 0x053e }
            java.lang.String r5 = "%.3f"
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x053e }
            java.lang.Float r0 = java.lang.Float.valueOf(r0)     // Catch:{ Throwable -> 0x053e }
            r7 = 0
            r6[r7] = r0     // Catch:{ Throwable -> 0x053e }
            java.lang.String r0 = java.lang.String.format(r5, r6)     // Catch:{ Throwable -> 0x053e }
            r4.append(r0)     // Catch:{ Throwable -> 0x053e }
            java.lang.String r0 = " Mb"
            r4.append(r0)     // Catch:{ Throwable -> 0x053e }
            java.lang.String r0 = r4.toString()     // Catch:{ Throwable -> 0x053e }
            r3.setText(r0)     // Catch:{ Throwable -> 0x053e }
        L_0x0535:
            return r2
        L_0x0536:
            android.view.View r0 = new android.view.View     // Catch:{ Throwable -> 0x053e }
            android.content.Context r2 = r1.f4379     // Catch:{ Throwable -> 0x053e }
            r0.<init>(r2)     // Catch:{ Throwable -> 0x053e }
            return r0
        L_0x053e:
            r0 = move-exception
            r0.printStackTrace()
            android.view.View r0 = new android.view.View
            android.content.Context r2 = r1.f4379
            r0.<init>(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lp.C0981.getGroupView(int, boolean, android.view.View, android.view.ViewGroup):android.view.View");
    }

    public void onGroupCollapsed(int i) {
        super.onGroupCollapsed(i);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m6014() {
        C0987.m6060((Object) Integer.valueOf(this.f4361.length));
        int length = this.f4361.length;
        if (length > 0) {
            for (int i = 0; i < length; i++) {
                this.f4361[i].f4356 = true;
                if (C0987.f4513.size() == 0) {
                    ((Button) C0987.f4447.findViewById(R.id.title_button)).setText(C0987.f4515);
                }
                C0987.f4513.add(this.f4361[i]);
                notifyDataSetChanged();
            }
        }
    }

    public void onGroupExpanded(int i) {
        if (i != C0987.f4441) {
            C0987.f4446.collapseGroup(C0987.f4441);
        }
        super.onGroupExpanded(i);
        C0987.f4440 = getGroup(i);
        C0987.f4441 = i;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m6016() {
        if (this.f4361.length > 0) {
            for (int i = 0; i < this.f4361.length; i++) {
                C0987.f4440 = null;
                C0987.f4446.collapseGroup(i);
            }
            C0987.f4440 = null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m6011(String str) {
        if (m6017(str)) {
            try {
                C0980[] r0 = new C0980[(this.f4361.length - 1)];
                int i = 0;
                for (int i2 = 0; i2 < this.f4361.length; i2++) {
                    if (!this.f4361[i2].f4337.equals(str)) {
                        r0[i] = this.f4361[i2];
                        i++;
                    } else {
                        C0987.f4446.collapseGroup(i2);
                    }
                }
                this.f4361 = r0;
                notifyDataSetChanged();
            } catch (Exception unused) {
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m6010(C0980 r3) {
        C0987.m6060((Object) ("add " + r3.f4337));
        ArrayList arrayList = new ArrayList(Arrays.asList(this.f4361));
        arrayList.add(r3);
        this.f4361 = new C0980[arrayList.size()];
        arrayList.toArray(this.f4361);
        m6018();
        notifyDataSetChanged();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m6018() {
        try {
            Arrays.sort(this.f4361, this.f4360);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0980 m6013(String str) {
        for (C0980 r3 : this.f4361) {
            if (r3.f4337.contentEquals(str)) {
                return r3;
            }
        }
        return null;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m6017(String str) {
        try {
            for (C0980 r4 : this.f4361) {
                if (r4.f4337.contentEquals(str)) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m6015(C0980 r3) {
        super.notifyDataSetChanged();
        if (C0987.f4436 == null) {
            C0987.f4436 = new C0967(this.f4379);
        }
        C0987.f4436.m5986(r3);
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0980 getGroup(int i) {
        return this.f4361[i];
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m6019(String str) {
        try {
            for (C0980 r4 : this.f4361) {
                if (r4.f4337.contentEquals(str)) {
                    C0980 r0 = new C0980(str, C0987.f4490, false);
                    if (!r0.m6006(r4)) {
                        r0.m6005();
                        r4.f4337 = r0.f4337;
                        r4.f4338 = r0.f4338;
                        r4.f4341 = r0.f4341;
                        r4.f4343 = r0.f4343;
                        r4.f4344 = r0.f4344;
                        r4.f4345 = r0.f4345;
                        r4.f4346 = r0.f4346;
                        r4.f4347 = r0.f4347;
                        r4.f4357 = C0815.m5232(r0.f4337);
                        r4.f4348 = r0.f4348;
                        r4.f4349 = r0.f4349;
                        r4.f4350 = r0.f4350;
                        r4.f4351 = r0.f4351;
                        r4.f4352 = r0.f4352;
                        r4.f4354 = r0.f4354;
                        r4.f4353 = r0.f4353;
                        r4.f4355 = r0.f4355;
                        return;
                    }
                    return;
                }
            }
        } catch (Exception e) {
            C0987.m6060((Object) ("LuckyPatcher (updateItem PkgListItemAdapter):" + e));
            e.printStackTrace();
        }
    }

    public View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = ((LayoutInflater) this.f4379.getSystemService("layout_inflater")).inflate((int) R.layout.child_view, (ViewGroup) null);
        }
        TextView textView = (TextView) view.findViewById(R.id.textChild);
        textView.setTextAppearance(this.f4379, C0987.m6076());
        textView.setTextAppearance(this.f4379, C0987.m6076());
        textView.setText(C0815.m5205(getChild(i, i2).intValue()));
        ImageView imageView = (ImageView) view.findViewById(R.id.child_image);
        switch (getChild(i, i2).intValue()) {
            case R.string.advanced_menu /*2131689517*/:
                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.menu));
                break;
            case R.string.app_dialog_no_root_app_control /*2131689528*/:
                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.move_to));
                break;
            case R.string.app_info /*2131689530*/:
                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.info_app));
                break;
            case R.string.cleardata /*2131689599*/:
                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.clear));
                break;
            case R.string.context_tools /*2131689649*/:
                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.menu));
                break;
            case R.string.contextlaunchapp /*2131689682*/:
                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.run));
                break;
            case R.string.menu_close_application /*2131689970*/:
                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.clear));
                break;
            case R.string.move_to_internal /*2131690010*/:
                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.move_to));
                break;
            case R.string.move_to_sdcard /*2131690012*/:
                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.move_to));
                break;
            case R.string.uninstallapp /*2131690269*/:
                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.remove));
                break;
        }
        return view;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public Filter m6020() {
        return new Filter() {
            /* access modifiers changed from: protected */
            public Filter.FilterResults performFiltering(CharSequence charSequence) {
                Filter.FilterResults filterResults = new Filter.FilterResults();
                ArrayList arrayList = new ArrayList();
                if (C0981.this.f4362 == null) {
                    C0981 r2 = C0981.this;
                    r2.f4362 = r2.f4361;
                }
                if (charSequence != null) {
                    if (C0981.this.f4362 != null && C0981.this.f4362.length > 0) {
                        for (C0980 r5 : C0981.this.f4362) {
                            if (!(r5.f4338 == null || r5.f4337 == null || (!r5.f4338.toLowerCase().contains(charSequence.toString().toLowerCase()) && !r5.f4337.toLowerCase().contains(charSequence.toString().toLowerCase())))) {
                                arrayList.add(r5);
                            }
                        }
                    }
                    C0980[] r9 = new C0980[arrayList.size()];
                    arrayList.toArray(r9);
                    filterResults.values = r9;
                }
                return filterResults;
            }

            /* access modifiers changed from: protected */
            public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
                C0981.this.f4361 = (C0980[]) filterResults.values;
                C0981.this.notifyDataSetChanged();
            }
        };
    }
}
