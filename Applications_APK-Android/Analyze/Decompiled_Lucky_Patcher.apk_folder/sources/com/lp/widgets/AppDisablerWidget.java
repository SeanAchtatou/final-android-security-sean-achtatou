package com.lp.widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Handler;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.chelpus.C0815;
import com.lp.C0987;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

public class AppDisablerWidget extends AppWidgetProvider {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String f3907 = "ActionReceiverWidgetAppDisabler";

    /* renamed from: ʼ  reason: contains not printable characters */
    public static String f3908 = "ActionReceiverWidgetAppDisablerUpdate";

    public void onDisabled(Context context) {
    }

    public void onEnabled(Context context) {
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        for (int r2 : iArr) {
            m5847(context, appWidgetManager, r2);
        }
    }

    public void onDeleted(Context context, int[] iArr) {
        for (int r2 : iArr) {
            AppDisablerWidgetConfigureActivity.m5852(context, r2);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m5847(final Context context, final AppWidgetManager appWidgetManager, final int i) {
        C0987.m6079(context);
        Thread thread = new Thread(new Runnable() {
            public void run() {
                if (C0987.f4474) {
                    String r0 = AppDisablerWidgetConfigureActivity.m5848(context, i);
                    AppDisablerWidgetConfigureActivity.m5851(context, i);
                    try {
                        PackageManager packageManager = context.getPackageManager();
                        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.app_disabler_widget);
                        remoteViews.setTextViewText(R.id.appwidget_text, packageManager.getApplicationLabel(packageManager.getApplicationInfo(r0, 0)));
                        if (packageManager.getPackageInfo(r0, 0).applicationInfo.enabled) {
                            remoteViews.setTextColor(R.id.appwidget_text, Color.parseColor("#00FF00"));
                            remoteViews.setInt(R.id.toggleButton_off, "setBackgroundResource", R.drawable.switch_on);
                        } else {
                            remoteViews.setTextColor(R.id.appwidget_text, Color.parseColor("#FF0000"));
                            remoteViews.setInt(R.id.toggleButton_off, "setBackgroundResource", R.drawable.switch_off);
                        }
                        Intent intent = new Intent(context, AppDisablerWidget.class);
                        intent.setAction(AppDisablerWidget.f3907);
                        intent.putExtra("appWidgetId", i);
                        intent.putExtra("msg", r0);
                        remoteViews.setOnClickPendingIntent(R.id.button, PendingIntent.getBroadcast(context, i, intent, 0));
                        try {
                            appWidgetManager.updateAppWidget(i, remoteViews);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (PackageManager.NameNotFoundException unused) {
                        RemoteViews remoteViews2 = new RemoteViews(context.getPackageName(), (int) R.layout.app_disabler_widget);
                        remoteViews2.setOnClickPendingIntent(R.id.button, PendingIntent.getBroadcast(context, i, new Intent(context, AppDisablerWidget.class), 0));
                        remoteViews2.setInt(R.id.toggleButton_off, "setBackgroundResource", R.drawable.switch_off);
                        remoteViews2.setTextColor(R.id.appwidget_text, Color.parseColor("#AAAAAA"));
                        remoteViews2.setTextViewText(R.id.appwidget_text, C0815.m5205((int) R.string.app_disabler_widget_pkg_not_found));
                        try {
                            appWidgetManager.updateAppWidget(i, remoteViews2);
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                } else {
                    RemoteViews remoteViews3 = new RemoteViews(context.getPackageName(), (int) R.layout.app_disabler_widget);
                    remoteViews3.setOnClickPendingIntent(R.id.button, PendingIntent.getBroadcast(context, i, new Intent(context, AppDisablerWidget.class), 0));
                    remoteViews3.setInt(R.id.toggleButton_off, "setBackgroundResource", R.drawable.switch_off);
                    remoteViews3.setTextColor(R.id.appwidget_text, Color.parseColor("#AAAAAA"));
                    remoteViews3.setTextViewText(R.id.appwidget_text, "you need root access");
                    try {
                        AppWidgetManager.getInstance(context).updateAppWidget(i, remoteViews3);
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                }
            }
        });
        thread.setPriority(10);
        thread.start();
    }

    public void onReceive(final Context context, final Intent intent) {
        String action = intent.getAction();
        if (f3907.equals(action)) {
            C0987.m6079(context);
            final Handler handler = new Handler();
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    try {
                        PackageManager packageManager = context.getPackageManager();
                        int intExtra = intent.getIntExtra("appWidgetId", -1);
                        if (intExtra != -1 && !AppDisablerWidgetConfigureActivity.m5848(context, intExtra).equals("NOT_SAVED_APP_DISABLER")) {
                            final String r8 = AppDisablerWidgetConfigureActivity.m5848(context, intExtra);
                            boolean r7 = AppDisablerWidgetConfigureActivity.m5851(context, intExtra);
                            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.app_disabler_widget);
                            if (packageManager.getPackageInfo(r8, 0).applicationInfo.enabled) {
                                C0815 r6 = new C0815(BuildConfig.FLAVOR);
                                r6.m5353("pm disable " + r8);
                                remoteViews.setInt(R.id.toggleButton_off, "setBackgroundResource", R.drawable.switch_off);
                                remoteViews.setTextColor(R.id.appwidget_text, Color.parseColor("#FF0000"));
                                handler.post(new Runnable() {
                                    public void run() {
                                        Context context = context;
                                        Toast.makeText(context, "OFF " + r8, 0).show();
                                        AppWidgetManager instance = AppWidgetManager.getInstance(context);
                                        AppDisablerWidget.this.onUpdate(context, instance, instance.getAppWidgetIds(new ComponentName(context, AppDisablerWidget.class)));
                                    }
                                });
                                return;
                            }
                            C0815 r62 = new C0815(BuildConfig.FLAVOR);
                            r62.m5353("pm enable " + r8);
                            if (r7) {
                                C0815.m5315(r8);
                            }
                            remoteViews.setTextColor(R.id.appwidget_text, Color.parseColor("#00FF00"));
                            remoteViews.setInt(R.id.toggleButton_off, "setBackgroundResource", R.drawable.switch_on);
                            handler.post(new Runnable() {
                                public void run() {
                                    Context context = context;
                                    Toast.makeText(context, "ON " + r8, 0).show();
                                    AppWidgetManager instance = AppWidgetManager.getInstance(context);
                                    AppDisablerWidget.this.onUpdate(context, instance, instance.getAppWidgetIds(new ComponentName(context, AppDisablerWidget.class)));
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        RemoteViews remoteViews2 = new RemoteViews(context.getPackageName(), (int) R.layout.app_disabler_widget);
                        remoteViews2.setBoolean(R.id.toggleButton_off, "setEnabled", false);
                        remoteViews2.setInt(R.id.toggleButton_off, "setBackgroundResource", R.drawable.switch_off);
                        remoteViews2.setTextColor(R.id.appwidget_text, Color.parseColor("#AAAAAA"));
                        remoteViews2.setTextViewText(R.id.appwidget_text, C0815.m5205((int) R.string.app_disabler_widget_pkg_not_found));
                        handler.post(new Runnable() {
                            public void run() {
                                AppWidgetManager instance = AppWidgetManager.getInstance(context);
                                AppDisablerWidget.this.onUpdate(context, instance, instance.getAppWidgetIds(new ComponentName(context, AppDisablerWidget.class)));
                            }
                        });
                    }
                }
            });
            thread.setPriority(10);
            thread.start();
        }
        if (f3908.equals(action)) {
            try {
                C0987.f4500 = true;
                AppWidgetManager instance = AppWidgetManager.getInstance(context);
                onUpdate(context, instance, instance.getAppWidgetIds(new ComponentName(context, AppDisablerWidget.class)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onReceive(context, intent);
    }
}
