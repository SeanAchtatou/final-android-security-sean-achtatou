package com.lp.widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.google.android.finsky.billing.iab.InAppBillingService;
import com.lp.C0987;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

public class inapp_widget extends AppWidgetProvider {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String f3950 = "ActionReceiverInAppWidget";

    /* renamed from: ʼ  reason: contains not printable characters */
    public static String f3951 = "ActionReceiverWidgetInAppUpdate";

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.widget_inapp);
        Intent intent = new Intent(context, inapp_widget.class);
        intent.setAction(f3950);
        intent.putExtra("msg", "Hello Habrahabr");
        remoteViews.setOnClickPendingIntent(R.id.widget_button2, PendingIntent.getBroadcast(context, 0, intent, 0));
        remoteViews.setTextViewText(R.id.widget_button2, "InApp");
        remoteViews.setViewVisibility(R.id.progressBar_widget_inapp, 8);
        if (context.getPackageManager().getComponentEnabledSetting(new ComponentName(context, InAppBillingService.class)) == 2) {
            remoteViews.setTextColor(R.id.widget_button2, Color.parseColor("#FF0000"));
        } else {
            remoteViews.setTextColor(R.id.widget_button2, Color.parseColor("#00FF00"));
        }
        appWidgetManager.updateAppWidget(iArr, remoteViews);
    }

    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        String action = intent.getAction();
        if (f3950.equals(action)) {
            C0987.m6079(context);
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.widget_inapp);
            remoteViews.setTextViewText(R.id.widget_button2, BuildConfig.FLAVOR);
            remoteViews.setViewVisibility(R.id.progressBar_widget_inapp, 0);
            AppWidgetManager instance = AppWidgetManager.getInstance(context);
            instance.updateAppWidget(instance.getAppWidgetIds(new ComponentName(context, inapp_widget.class)), remoteViews);
            if (context.getPackageManager().getComponentEnabledSetting(new ComponentName(context, InAppBillingService.class)) == 2) {
                C0987.m6068().setComponentEnabledSetting(new ComponentName(C0987.m6072(), InAppBillingService.class), 1, 1);
                Toast.makeText(context, "InApp-ON", 0).show();
            } else {
                C0987.m6068().setComponentEnabledSetting(new ComponentName(C0987.m6072(), InAppBillingService.class), 2, 1);
                Intent intent2 = new Intent(C0987.m6072(), inapp_widget.class);
                intent2.setPackage(C0987.m6072().getPackageName());
                intent2.setAction(f3951);
                C0987.m6072().sendBroadcast(intent2);
                Toast.makeText(context, "InApp-OFF", 0).show();
            }
        }
        if (f3951.equals(action)) {
            try {
                C0987.f4500 = true;
                AppWidgetManager instance2 = AppWidgetManager.getInstance(context);
                onUpdate(context, instance2, instance2.getAppWidgetIds(new ComponentName(context, inapp_widget.class)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
