package com.lp.widgets;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.chelpus.C0815;
import com.lp.C0987;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import ru.pKkcGXHI.kKSaIWSZS.R;

public class AppDisablerWidgetConfigureActivity extends Activity {

    /* renamed from: ʻ  reason: contains not printable characters */
    int f3921 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    public Context f3922;

    /* renamed from: ʽ  reason: contains not printable characters */
    public String[] f3923 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    public ListView f3924 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    public int f3925 = 0;

    /* renamed from: com.lp.widgets.AppDisablerWidgetConfigureActivity$ʻ  reason: contains not printable characters */
    class C0940 implements Comparator<C0941> {
        C0940() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int compare(C0941 r1, C0941 r2) {
            return r1.f3969.compareToIgnoreCase(r2.f3969);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setResult(0);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.f3921 = extras.getInt("appWidgetId", 0);
        }
        if (this.f3921 == 0) {
            finish();
            return;
        }
        this.f3922 = this;
        this.f3923 = C0987.m6102();
        ArrayList arrayList = new ArrayList();
        for (String str : this.f3923) {
            if (!str.equals("android") || !str.equals(this.f3922.getPackageName())) {
                try {
                    arrayList.add(new C0941(str, C0987.m6068().getPackageInfo(str, 0).applicationInfo.loadLabel(C0987.m6068()).toString()));
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        C0941[] r9 = new C0941[arrayList.size()];
        arrayList.toArray(r9);
        try {
            Arrays.sort(r9, new C0940());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.f3924 = new ListView(this.f3922);
        C0987.f4452 = new ArrayAdapter<C0941>(this, R.layout.icon_app_disabler_menu, r9) {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
             arg types: [?, android.view.ViewGroup, int]
             candidates:
              ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
              ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
             arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
             candidates:
              ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
              ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
            public View getView(int i, View view, ViewGroup viewGroup) {
                if (view == null) {
                    view = ((LayoutInflater) C0987.m6072().getSystemService("layout_inflater")).inflate((int) R.layout.icon_app_disabler_menu, viewGroup, false);
                }
                TextView textView = (TextView) view.findViewById(R.id.conttext);
                ImageView imageView = (ImageView) view.findViewById(R.id.imgIcon);
                imageView.setImageDrawable(null);
                textView.setTextAppearance(getContext(), C0987.m6076());
                textView.setTextColor(-1);
                C0941 r13 = (C0941) getItem(i);
                try {
                    PackageManager r3 = C0987.m6068();
                    int i2 = (int) ((C0987.m6069().getDisplayMetrics().density * 25.0f) + 0.5f);
                    try {
                        Bitmap r5 = C0815.m5135(r3.getApplicationIcon(r13.f3968));
                        int width = r5.getWidth();
                        int height = r5.getHeight();
                        float f = (float) i2;
                        Matrix matrix = new Matrix();
                        matrix.postScale(f / ((float) width), f / ((float) height));
                        imageView.setImageDrawable(new BitmapDrawable(Bitmap.createBitmap(r5, 0, 0, width, height, matrix, true)));
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    } catch (OutOfMemoryError unused) {
                    }
                } catch (OutOfMemoryError e3) {
                    e3.printStackTrace();
                    System.gc();
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
                textView.setCompoundDrawablePadding((int) ((C0987.m6069().getDisplayMetrics().density * 5.0f) + 0.5f));
                textView.setText(r13.f3969 + "\n" + r13.f3968);
                textView.setTypeface(null, 1);
                return view;
            }
        };
        this.f3924.setAdapter((ListAdapter) C0987.f4452);
        this.f3924.invalidateViews();
        this.f3924.setBackgroundColor(-16777216);
        this.f3924.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                AppDisablerWidgetConfigureActivity appDisablerWidgetConfigureActivity = AppDisablerWidgetConfigureActivity.this;
                AppDisablerWidgetConfigureActivity.m5849(appDisablerWidgetConfigureActivity, appDisablerWidgetConfigureActivity.f3921, ((C0941) adapterView.getItemAtPosition(i)).f3968);
                AppDisablerWidget.m5847(appDisablerWidgetConfigureActivity, AppWidgetManager.getInstance(appDisablerWidgetConfigureActivity), AppDisablerWidgetConfigureActivity.this.f3921);
                Intent intent = new Intent();
                intent.putExtra("appWidgetId", AppDisablerWidgetConfigureActivity.this.f3921);
                AppDisablerWidgetConfigureActivity.this.setResult(-1, intent);
                AppDisablerWidgetConfigureActivity.this.finish();
            }
        });
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        CheckBox checkBox = new CheckBox(this);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                AppDisablerWidgetConfigureActivity.m5850(AppDisablerWidgetConfigureActivity.this.f3922, AppDisablerWidgetConfigureActivity.this.f3921, z);
            }
        });
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.leftMargin = 30;
        layoutParams.topMargin = 20;
        layoutParams.bottomMargin = 20;
        checkBox.setLayoutParams(layoutParams);
        checkBox.setTextAppearance(this, 16973892);
        checkBox.setText(C0815.m5205((int) R.string.widget_app_disable_checkbox));
        linearLayout.addView(checkBox);
        linearLayout.addView(this.f3924);
        setContentView(linearLayout);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m5849(Context context, int i, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences("com.lp.widgets.AppDisablerWidget", 4).edit();
        edit.putString("appwidget_" + i, str);
        edit.commit();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m5850(Context context, int i, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences("com.lp.widgets.AppDisablerWidget", 4).edit();
        edit.putBoolean("appwidget_" + i + "_run", z);
        edit.commit();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static String m5848(Context context, int i) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.lp.widgets.AppDisablerWidget", 4);
        String string = sharedPreferences.getString("appwidget_" + i, null);
        return string != null ? string : "NOT_SAVED_APP_DISABLER";
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static boolean m5851(Context context, int i) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.lp.widgets.AppDisablerWidget", 4);
        return sharedPreferences.getBoolean("appwidget_" + i + "_run", false);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    static void m5852(Context context, int i) {
        SharedPreferences.Editor edit = context.getSharedPreferences("com.lp.widgets.AppDisablerWidget", 4).edit();
        edit.remove("appwidget_" + i);
        edit.commit();
    }
}
