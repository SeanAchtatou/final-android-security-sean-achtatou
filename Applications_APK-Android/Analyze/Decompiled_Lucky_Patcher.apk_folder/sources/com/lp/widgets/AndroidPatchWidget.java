package com.lp.widgets;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.widget.RemoteViews;
import com.chelpus.C0815;
import com.lp.C0987;
import ru.pKkcGXHI.kKSaIWSZS.R;

public class AndroidPatchWidget extends AppWidgetProvider {
    public void onDisabled(Context context) {
    }

    public void onEnabled(Context context) {
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        new RemoteViews(context.getPackageName(), (int) R.layout.android_patch_widget);
        int length = iArr.length;
        for (int r2 : iArr) {
            m5846(context, appWidgetManager, r2);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m5846(Context context, AppWidgetManager appWidgetManager, int i) {
        boolean z;
        int i2;
        String str;
        boolean z2;
        String str2;
        boolean z3;
        String str3;
        int i3;
        int i4;
        context.getString(R.string.appwidget_text);
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.android_patch_widget);
        if (C0815.m5289()) {
            i2 = 1;
            z = true;
        } else {
            i2 = 0;
            z = false;
        }
        if (C0815.m5294()) {
            i2++;
            z = true;
        }
        if (i2 > 0) {
            str = C0815.m5205((int) R.string.contextcorepatch1) + "\n(" + i2 + "/2 patched)";
        } else {
            str = C0815.m5205((int) R.string.contextcorepatch1) + "\n(not patched)";
        }
        if (C0815.m5298()) {
            str2 = str + "\n" + C0815.m5205((int) R.string.contextcorepatch2) + "\n(patched)";
            z2 = true;
        } else {
            str2 = str + "\n" + C0815.m5205((int) R.string.contextcorepatch2) + "\n(not patched)";
            z2 = false;
        }
        if (C0815.m5302()) {
            str3 = str2 + "\n" + C0815.m5205((int) R.string.contextcorepatch3) + "\n(patched)";
            z3 = true;
        } else {
            str3 = str2 + "\n" + C0815.m5205((int) R.string.contextcorepatch3) + "\n(not patched)";
            z3 = false;
        }
        SpannableString spannableString = new SpannableString(str3);
        if (z) {
            i3 = C0815.m5205((int) R.string.contextcorepatch1).length() + (" (" + i2 + "/2 patched)").length();
            spannableString.setSpan(new ForegroundColorSpan(-16711936), C0815.m5205((int) R.string.contextcorepatch1).length(), i3, 0);
        } else {
            i3 = C0815.m5205((int) R.string.contextcorepatch1).length() + 14;
            spannableString.setSpan(new ForegroundColorSpan(-65536), C0815.m5205((int) R.string.contextcorepatch1).length(), i3, 0);
        }
        if (z2) {
            i4 = C0815.m5205((int) R.string.contextcorepatch2).length() + i3 + 1 + 10;
            spannableString.setSpan(new ForegroundColorSpan(-16711936), C0815.m5205((int) R.string.contextcorepatch2).length() + i3 + 1, i4, 0);
        } else {
            i4 = C0815.m5205((int) R.string.contextcorepatch2).length() + 1 + i3 + 14;
            spannableString.setSpan(new ForegroundColorSpan(-65536), C0815.m5205((int) R.string.contextcorepatch2).length() + 1 + i3, i4, 0);
        }
        if (z3) {
            spannableString.setSpan(new ForegroundColorSpan(-16711936), i4 + C0815.m5205((int) R.string.contextcorepatch3).length() + 1, C0815.m5205((int) R.string.contextcorepatch3).length() + i4 + 1 + 10, 0);
        } else {
            C0987.m6060((Object) Integer.valueOf(i3));
            C0987.m6060((Object) Integer.valueOf(i4));
            C0987.m6060((Object) Integer.valueOf(str3.length()));
            spannableString.setSpan(new ForegroundColorSpan(-65536), i4 + C0815.m5205((int) R.string.contextcorepatch3).length() + 1, C0815.m5205((int) R.string.contextcorepatch3).length() + i4 + 1 + 14, 0);
        }
        remoteViews.setTextViewText(R.id.appwidget_text, spannableString);
        appWidgetManager.updateAppWidget(i, remoteViews);
    }
}
