package com.lp.widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.chelpus.C0815;
import com.lp.C0987;
import ru.pKkcGXHI.kKSaIWSZS.R;

public class proxyGP_widget extends AppWidgetProvider {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String f3954 = "ActionReceiverProxyGPWidget";

    /* renamed from: ʼ  reason: contains not printable characters */
    public static String f3955 = "ActionReceiverWidgetProxyGPUpdate";

    /* renamed from: ʽ  reason: contains not printable characters */
    Context f3956 = null;

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        PackageInfo packageInfo;
        this.f3956 = context;
        C0987.f4512 = false;
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.widget_proxy_gp);
        Intent intent = new Intent(context, proxyGP_widget.class);
        intent.setAction(f3954);
        intent.putExtra("msg", "Hello Habrahabr");
        remoteViews.setOnClickPendingIntent(R.id.widget_button_proxyGP, PendingIntent.getBroadcast(context, 0, intent, 0));
        remoteViews.setTextViewText(R.id.widget_button_proxyGP, "Proxy GP");
        remoteViews.setViewVisibility(R.id.progressBar_widget_proxyGP, 8);
        try {
            packageInfo = context.getPackageManager().getPackageInfo("com.android.vending", 0);
        } catch (PackageManager.NameNotFoundException unused) {
            Toast.makeText(context, "Google Play not installed.", 0).show();
            packageInfo = null;
        }
        if (packageInfo == null) {
            remoteViews.setTextColor(R.id.widget_button_proxyGP, Color.parseColor("#FF0000"));
        } else if (C0815.m5304()) {
            remoteViews.setTextColor(R.id.widget_button_proxyGP, Color.parseColor("#00FF00"));
        } else {
            remoteViews.setTextColor(R.id.widget_button_proxyGP, Color.parseColor("#FF0000"));
        }
        appWidgetManager.updateAppWidget(iArr, remoteViews);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chelpus.ˆ.ʼ(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.chelpus.ˆ.ʼ(int, int):int
      com.chelpus.ˆ.ʼ(java.io.File, java.lang.String):java.lang.String
      com.chelpus.ˆ.ʼ(java.io.File, java.util.ArrayList<java.io.File>):void
      com.chelpus.ˆ.ʼ(java.lang.String, java.lang.String):void
      com.chelpus.ˆ.ʼ(boolean, boolean):void
      com.chelpus.ˆ.ʼ(java.io.File, java.io.File):boolean
      com.chelpus.ˆ.ʼ(java.io.File, int):byte[]
      com.chelpus.ˆ.ʼ(java.lang.String, boolean):java.lang.String */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00ed, code lost:
        if (new java.io.File(com.lp.C0987.f4437 + "/empty_class").exists() == false) goto L_0x00ef;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x012f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r11, android.content.Intent r12) {
        /*
            r10 = this;
            r0 = 0
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            com.lp.C0987.f4512 = r1
            java.lang.String r1 = r12.getAction()
            java.lang.String r2 = com.lp.widgets.proxyGP_widget.f3954
            boolean r2 = r2.equals(r1)
            r3 = 1
            if (r2 == 0) goto L_0x015a
            com.lp.C0987.m6079(r11)
            android.os.Handler r2 = new android.os.Handler
            r2.<init>()
            android.widget.RemoteViews r4 = new android.widget.RemoteViews
            java.lang.String r5 = r11.getPackageName()
            r6 = 2131427451(0x7f0b007b, float:1.8476519E38)
            r4.<init>(r5, r6)
            r5 = 2131296641(0x7f090181, float:1.8211204E38)
            java.lang.String r6 = ""
            r4.setTextViewText(r5, r6)
            r5 = 2131296496(0x7f0900f0, float:1.821091E38)
            r4.setViewVisibility(r5, r0)
            android.appwidget.AppWidgetManager r5 = android.appwidget.AppWidgetManager.getInstance(r11)
            android.content.ComponentName r6 = new android.content.ComponentName
            java.lang.Class<com.lp.widgets.proxyGP_widget> r7 = com.lp.widgets.proxyGP_widget.class
            r6.<init>(r11, r7)
            int[] r6 = r5.getAppWidgetIds(r6)
            r5.updateAppWidget(r6, r4)
            boolean r4 = com.lp.C0987.f4474
            if (r4 == 0) goto L_0x0145
            boolean r4 = com.chelpus.C0815.m5289()
            if (r4 == 0) goto L_0x0145
            r4 = 0
            android.content.pm.PackageManager r7 = r11.getPackageManager()     // Catch:{ NameNotFoundException -> 0x005e }
            java.lang.String r8 = "com.android.vending"
            android.content.pm.PackageInfo r7 = r7.getPackageInfo(r8, r0)     // Catch:{ NameNotFoundException -> 0x005e }
            goto L_0x0068
        L_0x005e:
            java.lang.String r7 = "Google Play not installed."
            android.widget.Toast r7 = android.widget.Toast.makeText(r11, r7, r0)
            r7.show()
            r7 = r4
        L_0x0068:
            if (r7 == 0) goto L_0x015a
            android.content.pm.ApplicationInfo r8 = r7.applicationInfo
            java.lang.String r8 = r8.sourceDir
            com.chelpus.C0815.m5318(r8)
            java.io.File r8 = new java.io.File
            android.content.pm.ApplicationInfo r9 = r7.applicationInfo
            java.lang.String r9 = r9.sourceDir
            java.lang.String r9 = com.chelpus.C0815.m5208(r9, r3)
            r8.<init>(r9)
            boolean r8 = com.xposed.XposedUtils.isXposedEnabled()
            if (r8 == 0) goto L_0x00b0
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            org.json.JSONObject r4 = com.chelpus.C0815.m5314()     // Catch:{ JSONException -> 0x009d }
            if (r4 != 0) goto L_0x00a1
            java.lang.Thread r8 = new java.lang.Thread     // Catch:{ JSONException -> 0x009d }
            com.lp.widgets.proxyGP_widget$1 r9 = new com.lp.widgets.proxyGP_widget$1     // Catch:{ JSONException -> 0x009d }
            r9.<init>()     // Catch:{ JSONException -> 0x009d }
            r8.<init>(r9)     // Catch:{ JSONException -> 0x009d }
            r8.start()     // Catch:{ JSONException -> 0x009d }
            goto L_0x00a1
        L_0x009d:
            r8 = move-exception
            r8.printStackTrace()
        L_0x00a1:
            if (r4 == 0) goto L_0x00aa
            java.lang.String r8 = "patch4"
            boolean r4 = r4.optBoolean(r8, r0)
            goto L_0x00ab
        L_0x00aa:
            r4 = 0
        L_0x00ab:
            if (r4 != 0) goto L_0x00ae
            goto L_0x00b0
        L_0x00ae:
            r4 = 0
            goto L_0x00b1
        L_0x00b0:
            r4 = 1
        L_0x00b1:
            if (r4 == 0) goto L_0x012f
            java.io.File r0 = new java.io.File
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = com.lp.C0987.f4437
            r4.append(r5)
            java.lang.String r5 = "/p.apk"
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r0.<init>(r4)
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x00ef
            java.io.File r0 = new java.io.File
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = com.lp.C0987.f4437
            r4.append(r5)
            java.lang.String r5 = "/empty_class"
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r0.<init>(r4)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x00f2
        L_0x00ef:
            com.chelpus.C0815.m5254()
        L_0x00f2:
            com.chelpus.C0815.m5158(r7)
            boolean r0 = com.chelpus.C0815.m5304()
            if (r0 == 0) goto L_0x0115
            android.content.pm.ApplicationInfo r0 = r7.applicationInfo
            java.lang.String r0 = r0.sourceDir
            android.content.pm.ApplicationInfo r4 = r7.applicationInfo
            int r4 = r4.uid
            java.lang.String r4 = java.lang.String.valueOf(r4)
            java.lang.Thread r5 = new java.lang.Thread
            com.lp.widgets.proxyGP_widget$2 r6 = new com.lp.widgets.proxyGP_widget$2
            r6.<init>(r0, r4, r2)
            r5.<init>(r6)
            r5.start()
            goto L_0x015a
        L_0x0115:
            android.content.pm.ApplicationInfo r0 = r7.applicationInfo
            java.lang.String r0 = r0.sourceDir
            android.content.pm.ApplicationInfo r4 = r7.applicationInfo
            int r4 = r4.uid
            java.lang.String r4 = java.lang.String.valueOf(r4)
            java.lang.Thread r5 = new java.lang.Thread
            com.lp.widgets.proxyGP_widget$3 r6 = new com.lp.widgets.proxyGP_widget$3
            r6.<init>(r0, r4, r2)
            r5.<init>(r6)
            r5.start()
            goto L_0x015a
        L_0x012f:
            r2 = 2131690124(0x7f0f028c, float:1.9009283E38)
            java.lang.CharSequence r2 = r11.getText(r2)
            android.widget.Toast r0 = android.widget.Toast.makeText(r11, r2, r0)
            r0.show()
            android.content.Context r0 = com.lp.C0987.m6072()
            r10.onUpdate(r0, r5, r6)
            goto L_0x015a
        L_0x0145:
            r2 = 2131690125(0x7f0f028d, float:1.9009285E38)
            java.lang.CharSequence r2 = r11.getText(r2)
            android.widget.Toast r0 = android.widget.Toast.makeText(r11, r2, r0)
            r0.show()
            android.content.Context r0 = com.lp.C0987.m6072()
            r10.onUpdate(r0, r5, r6)
        L_0x015a:
            java.lang.String r0 = com.lp.widgets.proxyGP_widget.f3955
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x017b
            com.lp.C0987.f4500 = r3     // Catch:{ Exception -> 0x0177 }
            android.appwidget.AppWidgetManager r0 = android.appwidget.AppWidgetManager.getInstance(r11)     // Catch:{ Exception -> 0x0177 }
            android.content.ComponentName r1 = new android.content.ComponentName     // Catch:{ Exception -> 0x0177 }
            java.lang.Class<com.lp.widgets.proxyGP_widget> r2 = com.lp.widgets.proxyGP_widget.class
            r1.<init>(r11, r2)     // Catch:{ Exception -> 0x0177 }
            int[] r1 = r0.getAppWidgetIds(r1)     // Catch:{ Exception -> 0x0177 }
            r10.onUpdate(r11, r0, r1)     // Catch:{ Exception -> 0x0177 }
            goto L_0x017b
        L_0x0177:
            r0 = move-exception
            r0.printStackTrace()
        L_0x017b:
            super.onReceive(r11, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lp.widgets.proxyGP_widget.onReceive(android.content.Context, android.content.Intent):void");
    }
}
