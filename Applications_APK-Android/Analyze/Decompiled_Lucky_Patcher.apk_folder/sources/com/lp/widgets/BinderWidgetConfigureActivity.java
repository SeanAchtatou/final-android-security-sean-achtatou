package com.lp.widgets;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.chelpus.C0815;
import com.lp.BinderActivity;
import com.lp.C0963;
import com.lp.C0987;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

public class BinderWidgetConfigureActivity extends Activity {

    /* renamed from: ʻ  reason: contains not printable characters */
    int f3943 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    public Context f3944;

    /* renamed from: ʽ  reason: contains not printable characters */
    public ArrayList<C0963> f3945 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    public ListView f3946 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    public int f3947 = 0;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setResult(0);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.f3943 = extras.getInt("appWidgetId", 0);
        }
        if (this.f3943 == 0) {
            finish();
            return;
        }
        this.f3944 = this;
        File file = new File(getDir("binder", 0) + "/bind.txt");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.f3945 = BinderActivity.m5819(this.f3944);
        this.f3946 = new ListView(this.f3944);
        if (this.f3945.size() == 0) {
            this.f3945.add(new C0963("empty", "empty"));
        }
        C0987.f4491 = new ArrayAdapter<C0963>(this, R.layout.contextmenu, this.f3945) {
            public View getView(int i, View view, ViewGroup viewGroup) {
                View view2 = super.getView(i, view, viewGroup);
                TextView textView = (TextView) view2.findViewById(R.id.conttext);
                textView.setTextAppearance(getContext(), C0987.m6076());
                textView.setTextAppearance(BinderWidgetConfigureActivity.this.f3944, BinderWidgetConfigureActivity.this.f3947);
                C0963 r7 = (C0963) getItem(i);
                if (!r7.f4241.equals("empty") || !r7.f4242.equals("empty")) {
                    textView.setText(C0815.m5137(C0815.m5205((int) R.string.bind_source) + "\n", "#ff00ff00", "bold"));
                    textView.append(C0815.m5137(r7.f4241, "#ffffffff", "italic"));
                    textView.append(C0815.m5137("\n" + C0815.m5205((int) R.string.bind_target) + "\n", "#ffffff00", "bold"));
                    textView.append(C0815.m5137(r7.f4242.replace("~chelpus_disabled~", BuildConfig.FLAVOR), "#ffffffff", "italic"));
                } else {
                    textView.setText(C0815.m5205((int) R.string.empty) + "\n" + C0815.m5205((int) R.string.empty_binders));
                    view2.setEnabled(false);
                    view2.setClickable(false);
                }
                return view2;
            }
        };
        this.f3946.setAdapter((ListAdapter) C0987.f4491);
        this.f3946.invalidateViews();
        this.f3946.setBackgroundColor(-16777216);
        if (this.f3945.size() != 1 || !this.f3945.get(0).f4242.equals("empty") || !this.f3945.get(0).f4241.equals("empty")) {
            this.f3946.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    BinderWidgetConfigureActivity binderWidgetConfigureActivity = BinderWidgetConfigureActivity.this;
                    BinderWidgetConfigureActivity.m5856(binderWidgetConfigureActivity, binderWidgetConfigureActivity.f3943, ((C0963) adapterView.getItemAtPosition(i)).toString());
                    BinderWidget.m5854(binderWidgetConfigureActivity, AppWidgetManager.getInstance(binderWidgetConfigureActivity), BinderWidgetConfigureActivity.this.f3943);
                    Intent intent = new Intent();
                    intent.putExtra("appWidgetId", BinderWidgetConfigureActivity.this.f3943);
                    BinderWidgetConfigureActivity.this.setResult(-1, intent);
                    BinderWidgetConfigureActivity.this.finish();
                }
            });
        }
        setContentView(this.f3946);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m5856(Context context, int i, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences("com.lp.widgets.BinderWidget", 4).edit();
        edit.putString("appwidget_" + i, str);
        edit.commit();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static String m5855(Context context, int i) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.lp.widgets.BinderWidget", 4);
        String string = sharedPreferences.getString("appwidget_" + i, null);
        return string != null ? string : "NOT_SAVED_BIND";
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static void m5857(Context context, int i) {
        SharedPreferences.Editor edit = context.getSharedPreferences("com.lp.widgets.BinderWidget", 4).edit();
        edit.remove("appwidget_" + i);
        edit.commit();
    }
}
