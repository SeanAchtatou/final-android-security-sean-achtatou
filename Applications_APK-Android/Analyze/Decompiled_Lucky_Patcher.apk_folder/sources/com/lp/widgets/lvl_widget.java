package com.lp.widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.chelpus.C0815;
import com.google.android.finsky.services.LicensingService;
import com.lp.C0987;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

public class lvl_widget extends AppWidgetProvider {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String f3952 = "ActionReceiverLVLWidget";

    /* renamed from: ʼ  reason: contains not printable characters */
    public static String f3953 = "ActionReceiverWidgetLVLUpdate";

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.widget_lvl);
        Intent intent = new Intent(context, lvl_widget.class);
        intent.setAction(f3952);
        intent.putExtra("msg", "Hello Habrahabr");
        remoteViews.setOnClickPendingIntent(R.id.widget_button, PendingIntent.getBroadcast(context, 0, intent, 0));
        remoteViews.setTextViewText(R.id.widget_button, "LVL");
        remoteViews.setViewVisibility(R.id.progressBar_widget_lvl, 8);
        if (context.getPackageManager().getComponentEnabledSetting(new ComponentName(context, LicensingService.class)) == 2) {
            remoteViews.setTextColor(R.id.widget_button, Color.parseColor("#FF0000"));
        } else {
            remoteViews.setTextColor(R.id.widget_button, Color.parseColor("#00FF00"));
        }
        appWidgetManager.updateAppWidget(iArr, remoteViews);
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (f3952.equals(action)) {
            C0987.m6079(context);
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.widget_lvl);
            remoteViews.setTextViewText(R.id.widget_button, BuildConfig.FLAVOR);
            remoteViews.setViewVisibility(R.id.progressBar_widget_lvl, 0);
            AppWidgetManager instance = AppWidgetManager.getInstance(context);
            instance.updateAppWidget(instance.getAppWidgetIds(new ComponentName(context, lvl_widget.class)), remoteViews);
            if (context.getPackageManager().getComponentEnabledSetting(new ComponentName(context, LicensingService.class)) == 2) {
                C0987.m6068().setComponentEnabledSetting(new ComponentName(C0987.m6072(), LicensingService.class), 1, 1);
                if (!C0987.m6073().getBoolean("LVL_enable", false)) {
                    C0987.m6073().edit().putBoolean("LVL_enable", true).commit();
                }
                Toast.makeText(context, "LVL-ON", 0).show();
            } else {
                C0987.m6068().setComponentEnabledSetting(new ComponentName(C0987.m6072(), LicensingService.class), 2, 1);
                if (C0987.f4474) {
                    C0815.m5227(true);
                }
                Toast.makeText(context, "LVL-OFF", 0).show();
            }
        }
        if (f3953.equals(action)) {
            try {
                C0987.f4500 = true;
                AppWidgetManager instance2 = AppWidgetManager.getInstance(context);
                onUpdate(context, instance2, instance2.getAppWidgetIds(new ComponentName(context, lvl_widget.class)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onReceive(context, intent);
    }
}
