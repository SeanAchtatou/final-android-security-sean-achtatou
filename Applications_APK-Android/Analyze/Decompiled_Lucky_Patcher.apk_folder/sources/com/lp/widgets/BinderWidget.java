package com.lp.widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.chelpus.C0815;
import com.lp.BinderActivity;
import com.lp.C0963;
import com.lp.C0987;
import java.util.ArrayList;
import java.util.Iterator;
import net.lingala.zip4j.util.InternalZipConstants;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

public class BinderWidget extends AppWidgetProvider {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String f3930 = "ActionReceiverWidgetBinder";

    /* renamed from: ʼ  reason: contains not printable characters */
    public static String f3931 = "ActionReceiverWidgetBinderUpdate";

    /* renamed from: ʽ  reason: contains not printable characters */
    public static BinderWidget f3932;

    public void onDisabled(Context context) {
    }

    public void onEnabled(Context context) {
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        for (int r2 : iArr) {
            m5854(context, appWidgetManager, r2);
        }
    }

    public void onDeleted(Context context, int[] iArr) {
        for (int r2 : iArr) {
            BinderWidgetConfigureActivity.m5857(context, r2);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m5854(final Context context, AppWidgetManager appWidgetManager, final int i) {
        C0987.m6079(context);
        Thread thread = new Thread(new Runnable() {
            public void run() {
                RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.binder_widget);
                Intent intent = new Intent(context, BinderWidget.class);
                intent.setAction(BinderWidget.f3930);
                intent.putExtra("appWidgetId", i);
                remoteViews.setOnClickPendingIntent(R.id.button, PendingIntent.getBroadcast(context, i, intent, 0));
                remoteViews.setInt(R.id.toggleButton_off, "setBackgroundResource", R.drawable.switch_off);
                remoteViews.setTextColor(R.id.appwidget_text, Color.parseColor("#AAAAAA"));
                remoteViews.setTextViewText(R.id.appwidget_text, "wait");
                try {
                    AppWidgetManager.getInstance(context).updateAppWidget(i, remoteViews);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (C0987.f4474) {
                    String r0 = BinderWidgetConfigureActivity.m5855(context, i);
                    C0987.m6060((Object) r0);
                    C0963 r9 = new C0963(r0.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR));
                    if (r9.f4242 != null && !r9.f4242.equals(BuildConfig.FLAVOR) && r9.f4241 != null && !r9.f4241.equals(BuildConfig.FLAVOR)) {
                        String[] split = r9.f4242.split(InternalZipConstants.ZIP_FILE_SEPARATOR);
                        remoteViews.setTextViewText(R.id.appwidget_text, split[split.length - 1]);
                        if (C0815.m5191(r9)) {
                            remoteViews.setTextColor(R.id.appwidget_text, Color.parseColor("#00FF00"));
                            remoteViews.setInt(R.id.toggleButton_off, "setBackgroundResource", R.drawable.switch_on);
                        } else {
                            remoteViews.setTextColor(R.id.appwidget_text, Color.parseColor("#FF0000"));
                            remoteViews.setInt(R.id.toggleButton_off, "setBackgroundResource", R.drawable.switch_off);
                        }
                        Iterator<C0963> it = BinderActivity.m5819(context).iterator();
                        boolean z = false;
                        while (it.hasNext()) {
                            C0963 next = it.next();
                            if (next.f4241.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR).equals(r9.f4241.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR)) && next.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR).equals(r9.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR))) {
                                next.f4242 = next.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR);
                                next.f4241 = next.f4241.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR);
                                z = true;
                            }
                        }
                        if (!z) {
                            if (C0987.f4474) {
                                C0815.m5301("umount -f '" + r9.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR) + "'");
                                C0815.m5301("umount -l '" + r9.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR) + "'");
                            } else {
                                C0815.m5148("umount '" + r9.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR) + "'");
                            }
                            remoteViews.setInt(R.id.toggleButton_off, "setBackgroundResource", R.drawable.switch_off);
                            remoteViews.setTextColor(R.id.appwidget_text, Color.parseColor("#AAAAAA"));
                            remoteViews.setTextViewText(R.id.appwidget_text, "unknown bind");
                            try {
                                AppWidgetManager.getInstance(context).updateAppWidget(i, remoteViews);
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                        }
                        try {
                            AppWidgetManager.getInstance(context).updateAppWidget(i, remoteViews);
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                    }
                } else {
                    remoteViews.setInt(R.id.toggleButton_off, "setBackgroundResource", R.drawable.switch_off);
                    remoteViews.setTextColor(R.id.appwidget_text, Color.parseColor("#AAAAAA"));
                    remoteViews.setTextViewText(R.id.appwidget_text, "you need root access");
                    try {
                        AppWidgetManager.getInstance(context).updateAppWidget(i, remoteViews);
                    } catch (Exception e4) {
                        e4.printStackTrace();
                    }
                }
            }
        });
        thread.setPriority(10);
        thread.start();
    }

    public void onReceive(final Context context, final Intent intent) {
        super.onReceive(context, intent);
        String action = intent.getAction();
        if (f3930.equals(action)) {
            C0987.m6079(context);
            if (!BinderWidgetConfigureActivity.m5855(context, intent.getIntExtra("appWidgetId", -1)).equals("NOT_SAVED_BIND")) {
                f3932 = this;
            }
            final Handler handler = new Handler();
            try {
                Thread thread = new Thread(new Runnable() {
                    public void run() {
                        int i;
                        C0987.f4477 = true;
                        C0815.m5270();
                        int intExtra = intent.getIntExtra("appWidgetId", -1);
                        if (intExtra != -1 && !BinderWidgetConfigureActivity.m5855(context, intExtra).equals("NOT_SAVED_BIND")) {
                            final C0963 r3 = new C0963(BinderWidgetConfigureActivity.m5855(context, intExtra));
                            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.binder_widget);
                            r3.f4242 = r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR);
                            r3.f4241 = r3.f4241.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR);
                            if (!C0815.m5191(r3)) {
                                ArrayList<C0963> r5 = BinderActivity.m5819(context);
                                Iterator<C0963> it = r5.iterator();
                                boolean z = false;
                                while (it.hasNext()) {
                                    C0963 next = it.next();
                                    if (next.f4241.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR).equals(r3.f4241.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR)) && next.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR).equals(r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR))) {
                                        next.f4242 = next.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR);
                                        next.f4241 = next.f4241.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR);
                                        BinderWidgetConfigureActivity.m5856(context, intExtra, next.toString());
                                        z = true;
                                    }
                                }
                                if (!z) {
                                    if (C0987.f4474) {
                                        C0815.m5301("umount -f '" + r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR) + "'");
                                        C0815.m5301("umount -l '" + r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR) + "'");
                                    } else {
                                        C0815.m5148("umount '" + r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR) + "'");
                                    }
                                    remoteViews.setInt(R.id.toggleButton_off, "setBackgroundResource", R.drawable.switch_off);
                                    remoteViews.setTextColor(R.id.appwidget_text, Color.parseColor("#AAAAAA"));
                                }
                                BinderActivity.m5822(r5, context);
                                C0815.m5178("mount", "-o bind '" + r3.f4241.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR) + "' '" + r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR) + "'", r3.f4241.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR), r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR));
                                i = intExtra;
                            } else {
                                r3.f4242 = r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR);
                                r3.f4241 = r3.f4241.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR);
                                ArrayList<C0963> r1 = BinderActivity.m5819(context);
                                Iterator<C0963> it2 = r1.iterator();
                                boolean z2 = false;
                                while (it2.hasNext()) {
                                    C0963 next2 = it2.next();
                                    Iterator<C0963> it3 = it2;
                                    int i2 = intExtra;
                                    if (next2.f4241.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR).equals(r3.f4241.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR)) && next2.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR).equals(r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR))) {
                                        next2.f4242 = "~chelpus_disabled~" + next2.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR);
                                        next2.f4241 = next2.f4241;
                                        z2 = true;
                                    }
                                    it2 = it3;
                                    intExtra = i2;
                                }
                                i = intExtra;
                                if (!z2) {
                                    if (C0987.f4474) {
                                        C0815.m5301("umount -f '" + r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR) + "'");
                                        C0815.m5301("umount -l '" + r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR) + "'");
                                    } else {
                                        C0815.m5148("umount '" + r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR) + "'");
                                    }
                                    remoteViews.setInt(R.id.toggleButton_off, "setBackgroundResource", R.drawable.switch_off);
                                    remoteViews.setTextColor(R.id.appwidget_text, Color.parseColor("#AAAAAA"));
                                } else {
                                    BinderActivity.m5822(r1, context);
                                    if (C0987.f4474) {
                                        C0815.m5301("umount -f '" + r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR) + "'");
                                        C0815.m5301("umount -l '" + r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR) + "'");
                                    } else {
                                        C0815.m5148("umount '" + r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR) + "'");
                                    }
                                }
                            }
                            if (C0815.m5191(r3)) {
                                remoteViews.setTextColor(R.id.appwidget_text, Color.parseColor("#00FF00"));
                                remoteViews.setInt(R.id.toggleButton_off, "setBackgroundResource", R.drawable.switch_on);
                                handler.post(new Runnable() {
                                    public void run() {
                                        Context context = context;
                                        Toast.makeText(context, "ON " + r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR), 0).show();
                                    }
                                });
                            } else {
                                remoteViews.setTextColor(R.id.appwidget_text, Color.parseColor("#FF0000"));
                                remoteViews.setInt(R.id.toggleButton_off, "setBackgroundResource", R.drawable.switch_off);
                                handler.post(new Runnable() {
                                    public void run() {
                                        Context context = context;
                                        Toast.makeText(context, "OFF " + r3.f4242.replaceAll("~chelpus_disabled~", BuildConfig.FLAVOR), 0).show();
                                    }
                                });
                            }
                            new ComponentName(context, BinderWidget.class);
                            AppWidgetManager.getInstance(context).updateAppWidget(i, remoteViews);
                            AppWidgetManager instance = AppWidgetManager.getInstance(context);
                            BinderWidget.f3932.onUpdate(context, instance, instance.getAppWidgetIds(new ComponentName(context, BinderWidget.class)));
                            C0987.f4477 = false;
                        }
                    }
                });
                thread.setPriority(10);
                if (!C0987.f4477) {
                    thread.start();
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        if (f3931.equals(action)) {
            C0987.f4501 = true;
            AppWidgetManager instance = AppWidgetManager.getInstance(context);
            onUpdate(context, instance, instance.getAppWidgetIds(new ComponentName(context, BinderWidget.class)));
        }
    }
}
