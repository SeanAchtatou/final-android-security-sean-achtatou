package com.lp;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.chelpus.C0815;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

/* renamed from: com.lp.ᵔ  reason: contains not printable characters */
/* compiled from: PkgListItem */
public class C0980 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public String f4337;

    /* renamed from: ʼ  reason: contains not printable characters */
    public String f4338;

    /* renamed from: ʽ  reason: contains not printable characters */
    public Drawable f4339;

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean f4340 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean f4341 = false;

    /* renamed from: ˆ  reason: contains not printable characters */
    public String f4342 = BuildConfig.FLAVOR;

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean f4343 = false;

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean f4344 = false;

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean f4345 = false;

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean f4346 = false;

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean f4347 = false;

    /* renamed from: ˏ  reason: contains not printable characters */
    public boolean f4348 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean f4349 = false;

    /* renamed from: י  reason: contains not printable characters */
    public boolean f4350 = false;

    /* renamed from: ـ  reason: contains not printable characters */
    public boolean f4351 = false;

    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean f4352 = false;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean f4353 = false;

    /* renamed from: ᴵ  reason: contains not printable characters */
    public int f4354 = 0;

    /* renamed from: ᵎ  reason: contains not printable characters */
    public boolean f4355 = false;

    /* renamed from: ᵔ  reason: contains not printable characters */
    public boolean f4356 = false;

    /* renamed from: ᵢ  reason: contains not printable characters */
    public boolean f4357 = false;

    /* renamed from: ⁱ  reason: contains not printable characters */
    public String f4358 = BuildConfig.FLAVOR;

    /* renamed from: ﹳ  reason: contains not printable characters */
    public boolean f4359 = false;

    public C0980(Context context, C0980 r3) {
        this.f4337 = r3.f4337;
        this.f4338 = r3.f4338;
        this.f4340 = r3.f4340;
        this.f4341 = r3.f4341;
        this.f4342 = r3.f4342;
        this.f4343 = r3.f4343;
        this.f4344 = r3.f4344;
        this.f4345 = r3.f4345;
        this.f4346 = r3.f4346;
        this.f4347 = r3.f4347;
        this.f4348 = r3.f4348;
        this.f4349 = r3.f4349;
        this.f4351 = r3.f4351;
        this.f4357 = C0815.m5232(this.f4337);
        this.f4352 = r3.f4352;
        this.f4353 = r3.f4353;
        this.f4339 = r3.f4339;
        this.f4355 = r3.f4355;
        this.f4359 = r3.f4359;
        this.f4354 = r3.f4354;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public C0980(Context context, String str, String str2, boolean z, int i, String str3, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, Bitmap bitmap, int i12, int i13, String str4, boolean z2, boolean z3, boolean z4) {
        Bitmap bitmap2 = bitmap;
        int i14 = i12;
        this.f4337 = str;
        this.f4338 = str2;
        this.f4340 = z;
        this.f4355 = z2;
        if (i == 0) {
            this.f4341 = false;
        } else {
            this.f4341 = true;
        }
        this.f4342 = str3;
        if (i2 == 0) {
            this.f4343 = false;
        } else {
            this.f4343 = true;
        }
        if (i3 == 0) {
            this.f4344 = false;
        } else {
            this.f4344 = true;
        }
        if (i4 == 0) {
            this.f4345 = false;
        } else {
            this.f4345 = true;
        }
        if (i5 == 0) {
            this.f4346 = false;
        } else {
            this.f4346 = true;
        }
        if (i6 == 0) {
            this.f4347 = false;
        } else {
            this.f4347 = true;
        }
        if (i7 == 0) {
            this.f4348 = false;
        } else {
            this.f4348 = true;
        }
        if (i8 == 0) {
            this.f4349 = false;
        } else {
            this.f4349 = true;
        }
        if (i9 == 0) {
            this.f4351 = false;
        } else {
            this.f4351 = true;
        }
        this.f4357 = z3;
        if (i10 == 0) {
            this.f4352 = false;
        } else {
            this.f4352 = true;
        }
        if (i11 == 0) {
            this.f4353 = false;
        } else {
            this.f4353 = true;
        }
        if (i13 == 0) {
            this.f4350 = false;
        } else {
            this.f4350 = true;
        }
        this.f4359 = C0815.m5286(str);
        this.f4358 = str4;
        this.f4354 = i14;
        if (z4) {
            return;
        }
        if (bitmap2 != null) {
            this.f4354 = i14;
            this.f4339 = new BitmapDrawable(bitmap2);
            return;
        }
        PackageManager packageManager = context.getPackageManager();
        int i15 = (int) ((C0987.m6069().getDisplayMetrics().density * 35.0f) + 0.5f);
        try {
            Bitmap r0 = C0815.m5135(packageManager.getApplicationIcon(this.f4337));
            int width = r0.getWidth();
            int height = r0.getHeight();
            float f = (float) i15;
            Matrix matrix = new Matrix();
            matrix.postScale(f / ((float) width), f / ((float) height));
            this.f4339 = new BitmapDrawable(Bitmap.createBitmap(r0, 0, 0, width, height, matrix, true));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        } catch (OutOfMemoryError unused) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chelpus.ˆ.ʻ(java.lang.String, boolean):long
     arg types: [java.lang.String, int]
     candidates:
      com.chelpus.ˆ.ʻ(byte, byte):int
      com.chelpus.ˆ.ʻ(java.io.File, int):int
      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String[]):int
      com.chelpus.ˆ.ʻ(long, long):long
      com.chelpus.ˆ.ʻ(java.lang.String, int):android.content.pm.PackageInfo
      com.chelpus.ˆ.ʻ(int, java.lang.String[]):java.lang.String
      com.chelpus.ˆ.ʻ(android.content.Context, android.net.Uri):java.lang.String
      com.chelpus.ˆ.ʻ(android.content.Context, java.lang.String):java.lang.String
      com.chelpus.ˆ.ʻ(java.io.File, java.lang.String):java.lang.String
      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String):java.lang.String
      com.chelpus.ˆ.ʻ(java.io.File, java.io.File):void
      com.chelpus.ˆ.ʻ(java.io.File, com.chelpus.ʻ.ʼ.י[]):void
      com.chelpus.ˆ.ʻ(java.io.InputStream, java.io.OutputStream):void
      com.chelpus.ˆ.ʻ(java.util.List<java.io.File>, java.util.zip.ZipOutputStream):void
      com.chelpus.ˆ.ʻ(int, java.io.File):boolean
      com.chelpus.ˆ.ʻ(boolean, boolean):boolean
      com.chelpus.ˆ.ʻ(java.lang.String[], java.lang.String):boolean
      com.chelpus.ˆ.ʻ(int, int):byte[]
      com.chelpus.ˆ.ʻ(java.io.File, boolean):float
      com.chelpus.ˆ.ʻ(java.io.File, java.util.ArrayList<java.lang.String>):com.chelpus.ˆ$ʼ
      com.chelpus.ˆ.ʻ(java.lang.String, java.io.InputStream):java.lang.String
      com.chelpus.ˆ.ʻ(boolean, com.chelpus.ˆ$ʾ):java.lang.String
      com.chelpus.ˆ.ʻ(java.lang.String, boolean):long */
    public C0980(String str, int i, boolean z) {
        String str2 = str;
        int i2 = 0;
        PackageManager r0 = C0987.m6068();
        if (z) {
            try {
                int i3 = (int) ((C0987.m6069().getDisplayMetrics().density * 35.0f) + 0.5f);
                try {
                    Bitmap r9 = C0815.m5135(r0.getApplicationIcon(str2));
                    int width = r9.getWidth();
                    int height = r9.getHeight();
                    float f = (float) i3;
                    Matrix matrix = new Matrix();
                    matrix.postScale(f / ((float) width), f / ((float) height));
                    this.f4339 = new BitmapDrawable(Bitmap.createBitmap(r9, 0, 0, width, height, matrix, true));
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            } catch (Exception unused) {
            }
        }
        this.f4337 = str2;
        this.f4359 = C0815.m5286(this.f4337);
        this.f4357 = C0815.m5232(this.f4337);
        this.f4343 = false;
        this.f4344 = false;
        this.f4345 = false;
        this.f4346 = false;
        this.f4347 = false;
        this.f4348 = false;
        this.f4349 = false;
        this.f4351 = false;
        this.f4352 = false;
        if (str2 == null || str2 == BuildConfig.FLAVOR) {
            throw new IllegalArgumentException("package scan error");
        }
        PackageManager r7 = C0987.m6068();
        PackageInfo packageInfo = null;
        try {
            this.f4337 = str2;
            packageInfo = r7.getPackageInfo(str2, 4097);
            ApplicationInfo applicationInfo = packageInfo.applicationInfo;
            this.f4355 = applicationInfo.enabled;
            this.f4338 = r7.getPackageInfo(str2, 0).applicationInfo.loadLabel(r7).toString();
            this.f4358 = r7.getApplicationInfo(this.f4337, 0).sourceDir;
            this.f4354 = (int) (C0815.m5133(this.f4337, false) / 1000);
            if (C0815.m5334(this.f4358)) {
                this.f4353 = true;
            } else {
                this.f4353 = false;
            }
            this.f4351 = C0815.m5260(this.f4337);
            this.f4345 = C0815.m5253(this.f4337);
            C0987.m6069();
            if (applicationInfo != null) {
                try {
                    if ((applicationInfo.flags & 1) != 0) {
                        this.f4352 = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    C0987.m6060((Object) ("LuckyPatcher (PkgListItem): " + e));
                }
            }
            if (packageInfo != null) {
                try {
                    if (packageInfo.activities != null) {
                        for (ActivityInfo activityInfo : packageInfo.activities) {
                            if (C0815.m5336(activityInfo.name)) {
                                this.f4349 = true;
                            }
                        }
                    }
                    if (packageInfo.requestedPermissions != null && packageInfo.requestedPermissions.length != 0) {
                        int i4 = 0;
                        while (true) {
                            if (i4 < packageInfo.requestedPermissions.length) {
                                if (packageInfo.requestedPermissions[i4].toUpperCase().contains("BILLING") && !this.f4337.equals(C0987.m6072().getPackageName())) {
                                    this.f4350 = true;
                                }
                                if (packageInfo.requestedPermissions[i4].contains("CHECK_LICENSE") && !this.f4337.equals(C0987.m6072().getPackageName())) {
                                    if (!this.f4349) {
                                        this.f4348 = true;
                                        if (this.f4348 && this.f4350) {
                                            break;
                                        }
                                    } else {
                                        this.f4348 = true;
                                        if (!this.f4348) {
                                            continue;
                                        } else if (!this.f4350) {
                                        }
                                    }
                                }
                                i4++;
                            }
                        }
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            try {
                if (C0815.m5272(this.f4337)) {
                    this.f4347 = true;
                }
                if (Math.abs(((int) (System.currentTimeMillis() / 1000)) - this.f4354) < i * 86400) {
                    this.f4340 = true;
                }
            } catch (Exception e3) {
                C0987.m6060((Object) ("LuckyPatcher (PkgListItem): Custom patches not found! " + e3));
                e3.printStackTrace();
            }
        } catch (PackageManager.NameNotFoundException unused2) {
            C0987.m6060((Object) this.f4337);
            throw new PackageManager.NameNotFoundException("package scan error");
        } catch (NullPointerException e4) {
            e4.printStackTrace();
            C0987.m6069();
            if (packageInfo != null) {
                int i5 = 0;
                while (i5 < packageInfo.activities.length) {
                    try {
                        if (C0815.m5336(packageInfo.activities[i5].name)) {
                            this.f4349 = true;
                        }
                        i5++;
                    } catch (Exception e5) {
                        e5.printStackTrace();
                    }
                }
                if (packageInfo.requestedPermissions != null && packageInfo.requestedPermissions.length != 0) {
                    while (true) {
                        if (i2 < packageInfo.requestedPermissions.length) {
                            if (packageInfo.requestedPermissions[i2].toUpperCase().contains("BILLING") && !this.f4337.equals(C0987.m6072().getPackageName())) {
                                this.f4350 = true;
                            }
                            if (packageInfo.requestedPermissions[i2].contains("CHECK_LICENSE") && !this.f4337.equals(C0987.m6072().getPackageName())) {
                                if (!this.f4349) {
                                    this.f4348 = true;
                                    if (this.f4348 && this.f4350) {
                                        break;
                                    }
                                } else {
                                    this.f4348 = true;
                                    if (!this.f4348) {
                                        continue;
                                    } else if (!this.f4350) {
                                    }
                                }
                            }
                            i2++;
                        }
                    }
                }
            }
            try {
                if (C0815.m5272(this.f4337)) {
                    this.f4347 = true;
                }
                if (Math.abs(((int) (System.currentTimeMillis() / 1000)) - this.f4354) < i * 86400) {
                    this.f4340 = true;
                }
            } catch (Exception e6) {
                C0987.m6060((Object) ("LuckyPatcher (PkgListItem): Custom patches not found! " + e6));
                e6.printStackTrace();
            }
        }
    }

    public String toString() {
        return this.f4338;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m6006(C0980 r5) {
        try {
            if (this.f4337.equals(r5.f4337) && this.f4338.equals(r5.f4338) && this.f4340 == r5.f4340 && this.f4341 == r5.f4341 && this.f4343 == r5.f4343 && this.f4344 == r5.f4344 && this.f4345 == r5.f4345 && this.f4346 == r5.f4346 && this.f4347 == r5.f4347 && this.f4348 == r5.f4348 && this.f4349 == r5.f4349 && this.f4351 == r5.f4351 && this.f4357 == r5.f4357 && this.f4352 == r5.f4352 && this.f4353 == r5.f4353 && this.f4355 == r5.f4355 && this.f4350 == r5.f4350 && this.f4359 == r5.f4359 && this.f4358.equals(r5.f4358) && this.f4354 == r5.f4354) {
                return true;
            }
            return false;
        } catch (NullPointerException e) {
            C0987.m6060((Object) ("LuckyPacther: " + r5.f4337));
            e.printStackTrace();
            return false;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m6005() {
        if (C0987.f4436 == null) {
            C0987.f4436 = new C0967(C0987.m6072());
        }
        C0987.f4436.m5986(this);
    }
}
