package com.lp;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.chelpus.C0815;
import java.util.Comparator;
import java.util.List;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

/* renamed from: com.lp.ˆ  reason: contains not printable characters */
/* compiled from: BootListItemAdapter */
public class C0964 extends ArrayAdapter<C0980> {

    /* renamed from: ʻ  reason: contains not printable characters */
    Context f4243;

    /* renamed from: ʼ  reason: contains not printable characters */
    public Comparator<C0980> f4244;

    /* renamed from: ʽ  reason: contains not printable characters */
    int f4245;

    /* renamed from: ʾ  reason: contains not printable characters */
    TextView f4246;

    /* renamed from: ʿ  reason: contains not printable characters */
    TextView f4247;

    /* renamed from: ˆ  reason: contains not printable characters */
    ImageView f4248;

    /* renamed from: ˈ  reason: contains not printable characters */
    int f4249;

    public int getViewTypeCount() {
        return 2;
    }

    public C0964(Context context, int i, int i2, List<C0980> list) {
        super(context, i, list);
        this.f4243 = context;
        this.f4249 = i;
        this.f4245 = i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        C0980 r7 = (C0980) getItem(i);
        viewGroup.setBackgroundColor(-16777216);
        if (r7.f4338 != null && !r7.f4343 && !r7.f4345 && !r7.f4344 && !r7.f4346) {
            return new View(this.f4243);
        }
        View inflate = ((Activity) this.f4243).getLayoutInflater().inflate(this.f4249, viewGroup, false);
        inflate.setBackgroundColor(-16777216);
        this.f4246 = (TextView) inflate.findViewById(R.id.txtTitle);
        this.f4247 = (TextView) inflate.findViewById(R.id.txtStatus);
        this.f4248 = (ImageView) inflate.findViewById(R.id.imgIcon);
        this.f4246.setText(r7.f4338);
        this.f4248.setMaxHeight(1);
        this.f4248.setMaxWidth(1);
        try {
            this.f4248.setImageDrawable(C0987.m6068().getApplicationIcon(r7.f4337));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        this.f4246.setTextAppearance(this.f4243, C0987.m6076());
        this.f4247.setTextAppearance(this.f4243, C0987.m6076());
        this.f4246.setTextColor(-7829368);
        String str = BuildConfig.FLAVOR;
        Resources r2 = C0987.m6069();
        if (r7.f4343) {
            str = str + r2.getString(R.string.stat_boot_ads) + "; ";
            this.f4246.setTextColor(-16711681);
        }
        if (r7.f4344) {
            str = str + r2.getString(R.string.stat_boot_lvl) + "; ";
            this.f4246.setTextColor(-16711936);
        }
        if (r7.f4345) {
            str = str + r2.getString(R.string.stat_boot_custom) + "; ";
            this.f4246.setTextColor(-256);
        }
        this.f4247.setText(str);
        if (r7.f4337.equals(C0815.m5205((int) R.string.android_patches_for_bootlist))) {
            this.f4248.setImageResource(R.drawable.ic_launcher);
            this.f4247.setText(C0815.m5205((int) R.string.android_patches_for_bootlist_status));
        }
        this.f4247.setTextAppearance(this.f4243, 16973894);
        this.f4247.setTextColor(-7829368);
        this.f4246.setBackgroundColor(-16777216);
        this.f4247.setBackgroundColor(-16777216);
        return inflate;
    }
}
