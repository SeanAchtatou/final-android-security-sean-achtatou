package com.lp;

import android.app.Notification;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;
import com.chelpus.C0815;
import com.chelpus.ʻ.C0780;
import com.lp.widgets.AppDisablerWidget;
import com.lp.widgets.BinderWidget;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import net.lingala.zip4j.util.InternalZipConstants;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.R;

public class PatchService extends Service {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int f3851 = 50;

    /* renamed from: ʼ  reason: contains not printable characters */
    public Context f3852 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    final Handler f3853 = new Handler();

    /* renamed from: ʾ  reason: contains not printable characters */
    private Thread f3854;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        C0987.m6060((Object) "LuckyPatcher: Create service");
        C0987.f4484 = true;
        startForeground(1, new Notification());
        C0987.m6079((Context) this);
        this.f3852 = this;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        C0987.f4484 = true;
        this.f3854 = new Thread(new Runnable() {
            public void run() {
                if (C0815.m5307(C0987.f4437 + "/ClearDalvik.on")) {
                    C0815.m5230("/system", InternalZipConstants.WRITE_MODE);
                    try {
                        C0987.m6060((Object) new C0815(BuildConfig.FLAVOR).m5353(C0987.f4475 + ".clearDalvikCache " + PatchService.this.getApplicationContext().getFilesDir().getAbsolutePath()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    PatchService.this.f3853.post(new Runnable() {
                        public void run() {
                            Toast.makeText(PatchService.this.getApplicationContext(), "LuckyPatcher: clear dalvik-cache failed. Please clear dalvik-cache manual.", 1).show();
                        }
                    });
                    C0987.m6060((Object) (PatchService.this.getApplicationContext().getFilesDir() + "/reboot"));
                    C0815.m5330();
                }
                String str = PatchService.this.getApplicationInfo().sourceDir;
                PackageManager packageManager = PatchService.this.getPackageManager();
                try {
                    File[] listFiles = PatchService.this.getApplicationContext().getDir("bootlist", 0).listFiles();
                    C0987.m6060((Object) ("LuckyPatcher (count patches to boot): " + listFiles.length));
                    if (listFiles.length > 0) {
                        PatchService.this.m5837();
                    }
                    for (int i = 0; i < listFiles.length; i++) {
                        if (!listFiles[i].getName().equals("bootlist")) {
                            try {
                                C0987.m6060((Object) ("LuckyPatcher (Apply Patch on Boot): " + listFiles[i].getName() + " index:" + i));
                                String str2 = "empty";
                                try {
                                    str2 = packageManager.getPackageInfo(listFiles[i].getName(), 0).applicationInfo.sourceDir;
                                } catch (Exception e2) {
                                    C0987.m6060((Object) ("LuckyPatcher (Boot - PackageManager): " + e2));
                                }
                                C0987.m6060((Object) ("LuckyPatcher (AppDir to patch): " + str2));
                                try {
                                    C0780 r9 = new C0780();
                                    r9.f3127 = true;
                                    r9.f3142 = false;
                                    r9.f3152 = true;
                                    C0980 r10 = new C0980(listFiles[i].getName(), 0, false);
                                    r9.f3162 = listFiles[i];
                                    C0987.m6055(r10, r9);
                                    C0987.m6060((Object) ("LuckyPatcher (BootResult):\n" + BuildConfig.FLAVOR));
                                    C0815.m5152(PatchService.f3851, "Lucky Patcher - " + C0815.m5205((int) R.string.notify_patch_on_boot), C0815.m5205((int) R.string.notify_patch_on_boot), listFiles[i].getName() + " " + C0815.m5205((int) R.string.notify_patched));
                                    PatchService.f3851 = PatchService.f3851 + 1;
                                } catch (Exception e3) {
                                    C0987.m6060((Object) ("LuckyPatcher (BootPatchError3): " + e3));
                                }
                            } catch (Exception e4) {
                                C0987.m6060((Object) ("LuckyPatcher (noBootError2): " + e4));
                            } catch (Throwable th) {
                                th.printStackTrace();
                                C0987.m6060((Object) ("LuckyPatcher (noBootError3): " + th));
                            }
                        }
                    }
                } catch (Exception e5) {
                    C0987.m6060((Object) ("LuckyPatcher OnBootError: " + e5));
                    e5.printStackTrace();
                }
                File file = new File(PatchService.this.getDir("binder", 0) + "/bind.txt");
                if (file.exists() && file.length() > 0) {
                    C0987.m6060((Object) "LuckyPatcher binder start!");
                    try {
                        if (!file.exists()) {
                            file.createNewFile();
                        }
                        FileInputStream fileInputStream = new FileInputStream(file);
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream, "UTF-8"));
                        String str3 = BuildConfig.FLAVOR;
                        while (true) {
                            String readLine = bufferedReader.readLine();
                            if (readLine == null) {
                                break;
                            }
                            String[] split = readLine.split(";");
                            if (split.length == 2) {
                                C0815.m5178("mount", "-o bind '" + split[0] + "' '" + split[1] + "'", split[0], split[1]);
                                if (C0815.m5191(new C0963(split[0], split[1]))) {
                                    C0987.m6060((Object) "LuckyPatcher: show notify");
                                    str3 = str3 + PatchService.this.getString(R.string.notify_directory_binder_from) + " " + split[0] + " " + PatchService.this.getString(R.string.notify_directory_binder_to) + " " + split[1] + "\n";
                                }
                            }
                        }
                        C0815.m5152(PatchService.f3851, "Lucky Patcher - " + PatchService.this.getString(R.string.notify_directory_binder), PatchService.this.getString(R.string.notify_directory_binder), str3);
                        PatchService.f3851 = PatchService.f3851 + 1;
                        try {
                            int[] appWidgetIds = AppWidgetManager.getInstance(PatchService.this.f3852).getAppWidgetIds(new ComponentName(PatchService.this.f3852, BinderWidget.class));
                            if (appWidgetIds != null) {
                                C0987.m6060((Object) ("Binders " + appWidgetIds.length));
                                if (appWidgetIds.length > 0) {
                                    C0987.f4501 = true;
                                    PatchService.this.f3853.post(new Runnable() {
                                        public void run() {
                                            C0987.f4501 = true;
                                            Intent intent = new Intent(PatchService.this.f3852, BinderWidget.class);
                                            intent.setAction(BinderWidget.f3931);
                                            PatchService.this.sendBroadcast(intent);
                                        }
                                    });
                                }
                            }
                        } catch (Exception e6) {
                            e6.printStackTrace();
                        }
                        fileInputStream.close();
                    } catch (FileNotFoundException unused) {
                        C0987.m6060((Object) "Not found bind.txt");
                    } catch (IOException e7) {
                        C0987.m6060((Object) (BuildConfig.FLAVOR + e7));
                    }
                }
                try {
                    int[] appWidgetIds2 = AppWidgetManager.getInstance(PatchService.this.f3852).getAppWidgetIds(new ComponentName(PatchService.this.f3852, AppDisablerWidget.class));
                    if (appWidgetIds2 != null) {
                        C0987.m6060((Object) ("AppDisablers " + appWidgetIds2.length));
                        if (appWidgetIds2.length > 0) {
                            C0987.f4500 = true;
                            PatchService.this.f3853.post(new Runnable() {
                                public void run() {
                                    C0987.f4500 = true;
                                    Intent intent = new Intent(PatchService.this.getApplicationContext(), AppDisablerWidget.class);
                                    intent.setAction(AppDisablerWidget.f3908);
                                    PatchService.this.sendBroadcast(intent);
                                }
                            });
                        }
                    }
                } catch (Exception e8) {
                    e8.printStackTrace();
                }
                C0987.f4484 = false;
                C0987.m6073().edit().putBoolean("trigger_for_good_android_patch_on_boot", false).commit();
                PatchService.this.stopSelf();
            }
        });
        C0987.m6060((Object) "LuckyPatcher: Start thread patch!");
        this.f3854.setPriority(10);
        this.f3854.start();
        return 2;
    }

    public void onDestroy() {
        super.onDestroy();
        C0987.m6060((Object) "Killing Service!!!!!!!!!!!!!!!!!!!!!!!");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:5|6|7|8|11|12) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x00fd, code lost:
        com.lp.C0987.m6060(r0);
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x00fc, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x00da */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m5837() {
        /*
            r10 = this;
            java.io.File r0 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = com.lp.C0987.f4437
            r1.append(r2)
            java.lang.String r2 = "/dexopt-wrapper"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            boolean r1 = r0.exists()
            java.lang.String r3 = "chmod 0:0 "
            java.lang.String r4 = "chown 0.0 "
            java.lang.String r5 = "chmod 777 "
            r6 = 2131623943(0x7f0e0007, float:1.8875052E38)
            if (r1 == 0) goto L_0x00c1
            long r0 = r0.length()
            long r7 = com.chelpus.C0815.m5130(r6)
            int r9 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r9 == 0) goto L_0x0035
            goto L_0x00c1
        L_0x0035:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r5)
            java.io.File r1 = new java.io.File
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            android.content.Context r6 = r10.getApplicationContext()
            java.io.File r6 = r6.getFilesDir()
            r5.append(r6)
            r5.append(r2)
            java.lang.String r5 = r5.toString()
            r1.<init>(r5)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.chelpus.C0815.m5301(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r4)
            java.io.File r1 = new java.io.File
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            android.content.Context r5 = r10.getApplicationContext()
            java.io.File r5 = r5.getFilesDir()
            r4.append(r5)
            r4.append(r2)
            java.lang.String r4 = r4.toString()
            r1.<init>(r4)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.chelpus.C0815.m5301(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r3)
            java.io.File r1 = new java.io.File
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            android.content.Context r4 = r10.getApplicationContext()
            java.io.File r4 = r4.getFilesDir()
            r3.append(r4)
            r3.append(r2)
            java.lang.String r2 = r3.toString()
            r1.<init>(r2)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.chelpus.C0815.m5301(r0)
            goto L_0x018d
        L_0x00c1:
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x00da }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00da }
            r1.<init>()     // Catch:{ Exception -> 0x00da }
            java.lang.String r7 = com.lp.C0987.f4437     // Catch:{ Exception -> 0x00da }
            r1.append(r7)     // Catch:{ Exception -> 0x00da }
            r1.append(r2)     // Catch:{ Exception -> 0x00da }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00da }
            r0.<init>(r1)     // Catch:{ Exception -> 0x00da }
            com.chelpus.C0815.m5188(r6, r0)     // Catch:{ Exception -> 0x00da }
        L_0x00da:
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x00fc }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fc }
            r1.<init>()     // Catch:{ Exception -> 0x00fc }
            android.content.Context r6 = r10.getApplicationContext()     // Catch:{ Exception -> 0x00fc }
            java.io.File r6 = r6.getFilesDir()     // Catch:{ Exception -> 0x00fc }
            r1.append(r6)     // Catch:{ Exception -> 0x00fc }
            r1.append(r2)     // Catch:{ Exception -> 0x00fc }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00fc }
            r0.<init>(r1)     // Catch:{ Exception -> 0x00fc }
            r1 = 777(0x309, float:1.089E-42)
            com.chelpus.C0815.m5121(r0, r1)     // Catch:{ Exception -> 0x00fc }
            goto L_0x0103
        L_0x00fc:
            r0 = move-exception
            com.lp.C0987.m6060(r0)
            r0.printStackTrace()
        L_0x0103:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r5)
            java.io.File r1 = new java.io.File
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            android.content.Context r6 = r10.getApplicationContext()
            java.io.File r6 = r6.getFilesDir()
            r5.append(r6)
            r5.append(r2)
            java.lang.String r5 = r5.toString()
            r1.<init>(r5)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.chelpus.C0815.m5301(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r4)
            java.io.File r1 = new java.io.File
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            android.content.Context r5 = r10.getApplicationContext()
            java.io.File r5 = r5.getFilesDir()
            r4.append(r5)
            r4.append(r2)
            java.lang.String r4 = r4.toString()
            r1.<init>(r4)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.chelpus.C0815.m5301(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r3)
            java.io.File r1 = new java.io.File
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            android.content.Context r4 = r10.getApplicationContext()
            java.io.File r4 = r4.getFilesDir()
            r3.append(r4)
            r3.append(r2)
            java.lang.String r2 = r3.toString()
            r1.<init>(r2)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.chelpus.C0815.m5301(r0)
        L_0x018d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lp.PatchService.m5837():void");
    }
}
