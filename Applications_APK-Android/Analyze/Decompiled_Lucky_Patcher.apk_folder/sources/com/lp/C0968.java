package com.lp;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import com.chelpus.C0815;
import java.io.File;

/* renamed from: com.lp.ˋ  reason: contains not printable characters */
/* compiled from: FileApkListItem */
public class C0968 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public String f4265;

    /* renamed from: ʼ  reason: contains not printable characters */
    public String f4266 = "Bad file";

    /* renamed from: ʽ  reason: contains not printable characters */
    public Drawable f4267;

    /* renamed from: ʾ  reason: contains not printable characters */
    public String f4268;

    /* renamed from: ʿ  reason: contains not printable characters */
    public int f4269;

    /* renamed from: ˆ  reason: contains not printable characters */
    public File f4270;

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean f4271 = false;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public C0968(Context context, File file, boolean z) {
        this.f4270 = file;
        if (file.getName().toLowerCase().endsWith(".apks")) {
            this.f4271 = true;
            this.f4265 = C0815.m5300(file);
            this.f4266 = C0815.m5296(file);
            this.f4268 = C0815.m5305(file);
            this.f4269 = Integer.valueOf(C0815.m5309(file)).intValue();
            if (z) {
                try {
                    this.f4267 = C0815.m5313(file);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        } else {
            PackageManager r11 = C0987.m6068();
            PackageInfo packageArchiveInfo = r11.getPackageArchiveInfo(file.toString(), 1);
            if (packageArchiveInfo != null) {
                ApplicationInfo applicationInfo = packageArchiveInfo.applicationInfo;
                if (Build.VERSION.SDK_INT >= 8) {
                    applicationInfo.sourceDir = file.toString();
                    applicationInfo.publicSourceDir = file.toString();
                }
                this.f4265 = packageArchiveInfo.packageName;
                this.f4266 = applicationInfo.loadLabel(r11).toString();
                if (z) {
                    int i = (int) ((C0987.m6069().getDisplayMetrics().density * 35.0f) + 0.5f);
                    Bitmap bitmap = null;
                    try {
                        bitmap = C0815.m5135(applicationInfo.loadIcon(r11));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Bitmap bitmap2 = bitmap;
                    int width = bitmap2.getWidth();
                    int height = bitmap2.getHeight();
                    float f = (float) i;
                    Matrix matrix = new Matrix();
                    matrix.postScale(f / ((float) width), f / ((float) height));
                    this.f4267 = new BitmapDrawable(Bitmap.createBitmap(bitmap2, 0, 0, width, height, matrix, true));
                    applicationInfo.loadIcon(r11);
                }
                this.f4268 = packageArchiveInfo.versionName;
                this.f4269 = packageArchiveInfo.versionCode;
                return;
            }
            throw new IllegalArgumentException("oblom");
        }
    }
}
