package com.lp;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/* renamed from: com.lp.ᐧ  reason: contains not printable characters */
/* compiled from: Mount */
public class C0977 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final File f4321;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected final File f4322;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected final String f4323;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected final Set<String> f4324;

    public C0977(File file, File file2, String str, String str2) {
        this.f4321 = file;
        this.f4322 = file2;
        this.f4323 = str;
        this.f4324 = new HashSet(Arrays.asList(str2.split(",")));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public File m6001() {
        return this.f4321;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public File m6002() {
        return this.f4322;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m6003() {
        return this.f4323;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public Set<String> m6004() {
        return this.f4324;
    }

    public String toString() {
        return String.format("%s on %s type %s %s", this.f4321, this.f4322, this.f4323, this.f4324);
    }
}
