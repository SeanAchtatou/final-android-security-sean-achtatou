package com.lp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.FileProvider;
import com.chelpus.C0815;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ru.pKkcGXHI.kKSaIWSZS.PkgName;

/* renamed from: com.lp.ˑ  reason: contains not printable characters */
/* compiled from: LogCollector */
public class C0973 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final String f4277 = "ˑ";

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f4278 = C0987.m6072().getPackageName();

    /* renamed from: ʽ  reason: contains not printable characters */
    private Pattern f4279 = Pattern.compile(String.format("(.*)E\\/AndroidRuntime\\(\\s*\\d+\\)\\:\\s*at\\s%s.*", this.f4278.replace(".", "\\.")));

    /* renamed from: ʾ  reason: contains not printable characters */
    private SharedPreferences f4280;

    /* renamed from: ʿ  reason: contains not printable characters */
    private ArrayList<String> f4281;

    public C0973() {
        C0987.m6072();
        this.f4280 = C0987.m6072().getSharedPreferences("LogCollector", 0);
        this.f4281 = new ArrayList<>();
        this.f4280.edit().clear().commit();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:55|56|(1:58)(1:59)|60|(2:61|(1:63)(3:71|64|75))) */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        r0 = java.lang.Runtime.getRuntime().exec("su");
        r5 = new java.io.DataOutputStream(r0.getOutputStream());
        r7 = new java.io.DataInputStream(r0.getErrorStream());
        r8 = new byte[r7.available()];
        r7.read(r8);
        com.lp.C0987.m6060((java.lang.Object) new java.lang.String(r8));
        r7 = new java.io.BufferedReader(new java.io.InputStreamReader(r0.getInputStream()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0239, code lost:
        if (com.lp.C0987.f4438.startsWith(com.lp.C0987.f4447.getDir("sdcard", 0).getAbsolutePath()) != false) goto L_0x023b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x023b, code lost:
        r5.writeBytes("logcat -d System.out:* *:S\n");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x023f, code lost:
        r5.writeBytes("logcat -d\n");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0242, code lost:
        r5.flush();
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0248, code lost:
        r2 = r7.readLine();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x024c, code lost:
        if (r2 != null) goto L_0x024e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x024e, code lost:
        r1.add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0252, code lost:
        r0.destroy();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0256, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0257, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:55:0x01f0 */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m5992(java.util.List<java.lang.String> r18, java.lang.String r19, java.lang.String r20, java.lang.String[] r21, boolean r22) {
        /*
            r17 = this;
            r1 = r18
            r0 = r20
            r2 = r21
            java.lang.String r3 = "logcat -d\n"
            java.lang.String r4 = "logcat -d System.out:* *:S\n"
            java.lang.String r5 = "su"
            java.lang.String r6 = "sdcard"
            r18.clear()
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            if (r19 != 0) goto L_0x001b
            java.lang.String r8 = "time"
            goto L_0x001d
        L_0x001b:
            r8 = r19
        L_0x001d:
            java.lang.String r9 = "-v"
            r7.add(r9)
            r7.add(r8)
            if (r0 == 0) goto L_0x002f
            java.lang.String r8 = "-b"
            r7.add(r8)
            r7.add(r0)
        L_0x002f:
            if (r2 == 0) goto L_0x0034
            java.util.Collections.addAll(r7, r2)
        L_0x0034:
            r2 = 0
            boolean r0 = com.lp.C0987.f4474     // Catch:{ IOException -> 0x01f0 }
            java.lang.String r8 = "*:S"
            java.lang.String r10 = "System.out:*"
            java.lang.String r14 = "Collect logs no root."
            java.lang.String r15 = "-d"
            java.lang.String r9 = "logcat"
            if (r0 == 0) goto L_0x011e
            if (r22 == 0) goto L_0x0047
            goto L_0x011e
        L_0x0047:
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x01f0 }
            java.lang.Process r16 = r0.exec(r5)     // Catch:{ IOException -> 0x01f0 }
            java.io.DataOutputStream r0 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x01f0 }
            java.io.OutputStream r11 = r16.getOutputStream()     // Catch:{ IOException -> 0x01f0 }
            r0.<init>(r11)     // Catch:{ IOException -> 0x01f0 }
            java.io.DataInputStream r11 = new java.io.DataInputStream     // Catch:{ IOException -> 0x01f0 }
            java.io.InputStream r12 = r16.getErrorStream()     // Catch:{ IOException -> 0x01f0 }
            r11.<init>(r12)     // Catch:{ IOException -> 0x01f0 }
            int r12 = r11.available()     // Catch:{ IOException -> 0x01f0 }
            byte[] r12 = new byte[r12]     // Catch:{ IOException -> 0x01f0 }
            r11.read(r12)     // Catch:{ IOException -> 0x01f0 }
            java.lang.String r11 = new java.lang.String     // Catch:{ IOException -> 0x01f0 }
            r11.<init>(r12)     // Catch:{ IOException -> 0x01f0 }
            com.lp.C0987.m6060(r11)     // Catch:{ IOException -> 0x01f0 }
            java.io.BufferedReader r11 = new java.io.BufferedReader     // Catch:{ IOException -> 0x01f0 }
            java.io.InputStreamReader r12 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x01f0 }
            java.io.InputStream r13 = r16.getInputStream()     // Catch:{ IOException -> 0x01f0 }
            r12.<init>(r13)     // Catch:{ IOException -> 0x01f0 }
            r11.<init>(r12)     // Catch:{ IOException -> 0x01f0 }
            java.lang.String r12 = com.lp.C0987.f4438     // Catch:{ IOException -> 0x01f0 }
            ru.pKkcGXHI.kKSaIWSZS.MainActivity r13 = com.lp.C0987.f4447     // Catch:{ IOException -> 0x01f0 }
            java.io.File r13 = r13.getDir(r6, r2)     // Catch:{ IOException -> 0x01f0 }
            java.lang.String r13 = r13.getAbsolutePath()     // Catch:{ IOException -> 0x01f0 }
            boolean r12 = r12.startsWith(r13)     // Catch:{ IOException -> 0x01f0 }
            if (r12 == 0) goto L_0x0096
            r0.writeBytes(r4)     // Catch:{ IOException -> 0x01f0 }
            goto L_0x009b
        L_0x0096:
            java.lang.String r12 = "logcat -d -v time\n"
            r0.writeBytes(r12)     // Catch:{ IOException -> 0x01f0 }
        L_0x009b:
            r0.flush()     // Catch:{ IOException -> 0x01f0 }
            r0.close()     // Catch:{ IOException -> 0x01f0 }
        L_0x00a1:
            java.lang.String r0 = r11.readLine()     // Catch:{ Exception -> 0x00ab }
            if (r0 == 0) goto L_0x00af
            r1.add(r0)     // Catch:{ Exception -> 0x00ab }
            goto L_0x00a1
        L_0x00ab:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ IOException -> 0x01f0 }
        L_0x00af:
            r11.close()     // Catch:{ IOException -> 0x01f0 }
            r16.destroy()     // Catch:{ IOException -> 0x01f0 }
            com.lp.C0987.m6060(r14)     // Catch:{ IOException -> 0x01f0 }
            java.lang.String r0 = "\n\n\n*********************************** NO ROOT *******************************************************\n\n\n"
            r1.add(r0)     // Catch:{ IOException -> 0x01f0 }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ IOException -> 0x01f0 }
            r0.<init>()     // Catch:{ IOException -> 0x01f0 }
            r0.add(r9)     // Catch:{ IOException -> 0x01f0 }
            r0.add(r15)     // Catch:{ IOException -> 0x01f0 }
            r0.addAll(r7)     // Catch:{ IOException -> 0x01f0 }
            int r7 = r0.size()     // Catch:{ IOException -> 0x01f0 }
            java.lang.String[] r7 = new java.lang.String[r7]     // Catch:{ IOException -> 0x01f0 }
            java.lang.Object[] r0 = r0.toArray(r7)     // Catch:{ IOException -> 0x01f0 }
            java.lang.String[] r0 = (java.lang.String[]) r0     // Catch:{ IOException -> 0x01f0 }
            java.lang.String r7 = com.lp.C0987.f4438     // Catch:{ IOException -> 0x01f0 }
            ru.pKkcGXHI.kKSaIWSZS.MainActivity r11 = com.lp.C0987.f4447     // Catch:{ IOException -> 0x01f0 }
            java.io.File r11 = r11.getDir(r6, r2)     // Catch:{ IOException -> 0x01f0 }
            java.lang.String r11 = r11.getAbsolutePath()     // Catch:{ IOException -> 0x01f0 }
            boolean r7 = r7.startsWith(r11)     // Catch:{ IOException -> 0x01f0 }
            if (r7 == 0) goto L_0x00f7
            r7 = 4
            java.lang.String[] r0 = new java.lang.String[r7]     // Catch:{ IOException -> 0x01f0 }
            r0[r2] = r9     // Catch:{ IOException -> 0x01f0 }
            r7 = 1
            r0[r7] = r15     // Catch:{ IOException -> 0x01f0 }
            r7 = 2
            r0[r7] = r10     // Catch:{ IOException -> 0x01f0 }
            r7 = 3
            r0[r7] = r8     // Catch:{ IOException -> 0x01f0 }
        L_0x00f7:
            java.lang.Runtime r7 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x01f0 }
            java.lang.Process r0 = r7.exec(r0)     // Catch:{ IOException -> 0x01f0 }
            java.io.BufferedReader r7 = new java.io.BufferedReader     // Catch:{ IOException -> 0x01f0 }
            java.io.InputStreamReader r8 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x01f0 }
            java.io.InputStream r9 = r0.getInputStream()     // Catch:{ IOException -> 0x01f0 }
            r8.<init>(r9)     // Catch:{ IOException -> 0x01f0 }
            r7.<init>(r8)     // Catch:{ IOException -> 0x01f0 }
        L_0x010d:
            java.lang.String r8 = r7.readLine()     // Catch:{ IOException -> 0x01f0 }
            if (r8 == 0) goto L_0x0117
            r1.add(r8)     // Catch:{ IOException -> 0x01f0 }
            goto L_0x010d
        L_0x0117:
            r0.destroy()     // Catch:{ IOException -> 0x01f0 }
            r7.close()     // Catch:{ IOException -> 0x01f0 }
            goto L_0x0181
        L_0x011e:
            com.lp.C0987.m6060(r14)     // Catch:{ IOException -> 0x01f0 }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ IOException -> 0x01f0 }
            r0.<init>()     // Catch:{ IOException -> 0x01f0 }
            r0.add(r9)     // Catch:{ IOException -> 0x01f0 }
            r0.add(r15)     // Catch:{ IOException -> 0x01f0 }
            r0.addAll(r7)     // Catch:{ IOException -> 0x01f0 }
            int r7 = r0.size()     // Catch:{ IOException -> 0x01f0 }
            java.lang.String[] r7 = new java.lang.String[r7]     // Catch:{ IOException -> 0x01f0 }
            java.lang.Object[] r0 = r0.toArray(r7)     // Catch:{ IOException -> 0x01f0 }
            java.lang.String[] r0 = (java.lang.String[]) r0     // Catch:{ IOException -> 0x01f0 }
            java.lang.String r7 = com.lp.C0987.f4438     // Catch:{ IOException -> 0x01f0 }
            ru.pKkcGXHI.kKSaIWSZS.MainActivity r11 = com.lp.C0987.f4447     // Catch:{ IOException -> 0x01f0 }
            java.io.File r11 = r11.getDir(r6, r2)     // Catch:{ IOException -> 0x01f0 }
            java.lang.String r11 = r11.getAbsolutePath()     // Catch:{ IOException -> 0x01f0 }
            boolean r7 = r7.startsWith(r11)     // Catch:{ IOException -> 0x01f0 }
            if (r7 == 0) goto L_0x015b
            r7 = 4
            java.lang.String[] r0 = new java.lang.String[r7]     // Catch:{ IOException -> 0x01f0 }
            r0[r2] = r9     // Catch:{ IOException -> 0x01f0 }
            r7 = 1
            r0[r7] = r15     // Catch:{ IOException -> 0x01f0 }
            r7 = 2
            r0[r7] = r10     // Catch:{ IOException -> 0x01f0 }
            r7 = 3
            r0[r7] = r8     // Catch:{ IOException -> 0x01f0 }
        L_0x015b:
            java.lang.Runtime r7 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x01f0 }
            java.lang.Process r0 = r7.exec(r0)     // Catch:{ IOException -> 0x01f0 }
            java.io.BufferedReader r7 = new java.io.BufferedReader     // Catch:{ IOException -> 0x01f0 }
            java.io.InputStreamReader r8 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x01f0 }
            java.io.InputStream r9 = r0.getInputStream()     // Catch:{ IOException -> 0x01f0 }
            r8.<init>(r9)     // Catch:{ IOException -> 0x01f0 }
            r7.<init>(r8)     // Catch:{ IOException -> 0x01f0 }
        L_0x0171:
            java.lang.String r8 = r7.readLine()     // Catch:{ IOException -> 0x01f0 }
            if (r8 == 0) goto L_0x017b
            r1.add(r8)     // Catch:{ IOException -> 0x01f0 }
            goto L_0x0171
        L_0x017b:
            r0.destroy()     // Catch:{ IOException -> 0x01f0 }
            r7.close()     // Catch:{ IOException -> 0x01f0 }
        L_0x0181:
            int r0 = r18.size()     // Catch:{ IOException -> 0x01f0 }
            if (r0 != 0) goto L_0x025a
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x01f0 }
            java.lang.Process r0 = r0.exec(r5)     // Catch:{ IOException -> 0x01f0 }
            java.io.DataOutputStream r7 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x01f0 }
            java.io.OutputStream r8 = r0.getOutputStream()     // Catch:{ IOException -> 0x01f0 }
            r7.<init>(r8)     // Catch:{ IOException -> 0x01f0 }
            java.io.DataInputStream r8 = new java.io.DataInputStream     // Catch:{ IOException -> 0x01f0 }
            java.io.InputStream r9 = r0.getErrorStream()     // Catch:{ IOException -> 0x01f0 }
            r8.<init>(r9)     // Catch:{ IOException -> 0x01f0 }
            int r9 = r8.available()     // Catch:{ IOException -> 0x01f0 }
            byte[] r9 = new byte[r9]     // Catch:{ IOException -> 0x01f0 }
            r8.read(r9)     // Catch:{ IOException -> 0x01f0 }
            java.lang.String r8 = new java.lang.String     // Catch:{ IOException -> 0x01f0 }
            r8.<init>(r9)     // Catch:{ IOException -> 0x01f0 }
            com.lp.C0987.m6060(r8)     // Catch:{ IOException -> 0x01f0 }
            java.io.BufferedReader r8 = new java.io.BufferedReader     // Catch:{ IOException -> 0x01f0 }
            java.io.InputStreamReader r9 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x01f0 }
            java.io.InputStream r10 = r0.getInputStream()     // Catch:{ IOException -> 0x01f0 }
            r9.<init>(r10)     // Catch:{ IOException -> 0x01f0 }
            r8.<init>(r9)     // Catch:{ IOException -> 0x01f0 }
            java.lang.String r9 = com.lp.C0987.f4438     // Catch:{ IOException -> 0x01f0 }
            ru.pKkcGXHI.kKSaIWSZS.MainActivity r10 = com.lp.C0987.f4447     // Catch:{ IOException -> 0x01f0 }
            java.io.File r10 = r10.getDir(r6, r2)     // Catch:{ IOException -> 0x01f0 }
            java.lang.String r10 = r10.getAbsolutePath()     // Catch:{ IOException -> 0x01f0 }
            boolean r9 = r9.startsWith(r10)     // Catch:{ IOException -> 0x01f0 }
            if (r9 == 0) goto L_0x01d6
            r7.writeBytes(r4)     // Catch:{ IOException -> 0x01f0 }
            goto L_0x01d9
        L_0x01d6:
            r7.writeBytes(r3)     // Catch:{ IOException -> 0x01f0 }
        L_0x01d9:
            r7.flush()     // Catch:{ IOException -> 0x01f0 }
            r7.close()     // Catch:{ IOException -> 0x01f0 }
        L_0x01df:
            java.lang.String r7 = r8.readLine()     // Catch:{ IOException -> 0x01f0 }
            if (r7 == 0) goto L_0x01e9
            r1.add(r7)     // Catch:{ IOException -> 0x01f0 }
            goto L_0x01df
        L_0x01e9:
            r0.destroy()     // Catch:{ IOException -> 0x01f0 }
            r8.close()     // Catch:{ IOException -> 0x01f0 }
            goto L_0x025a
        L_0x01f0:
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x0256 }
            java.lang.Process r0 = r0.exec(r5)     // Catch:{ IOException -> 0x0256 }
            java.io.DataOutputStream r5 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x0256 }
            java.io.OutputStream r7 = r0.getOutputStream()     // Catch:{ IOException -> 0x0256 }
            r5.<init>(r7)     // Catch:{ IOException -> 0x0256 }
            java.io.DataInputStream r7 = new java.io.DataInputStream     // Catch:{ IOException -> 0x0256 }
            java.io.InputStream r8 = r0.getErrorStream()     // Catch:{ IOException -> 0x0256 }
            r7.<init>(r8)     // Catch:{ IOException -> 0x0256 }
            int r8 = r7.available()     // Catch:{ IOException -> 0x0256 }
            byte[] r8 = new byte[r8]     // Catch:{ IOException -> 0x0256 }
            r7.read(r8)     // Catch:{ IOException -> 0x0256 }
            java.lang.String r7 = new java.lang.String     // Catch:{ IOException -> 0x0256 }
            r7.<init>(r8)     // Catch:{ IOException -> 0x0256 }
            com.lp.C0987.m6060(r7)     // Catch:{ IOException -> 0x0256 }
            java.io.BufferedReader r7 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0256 }
            java.io.InputStreamReader r8 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0256 }
            java.io.InputStream r9 = r0.getInputStream()     // Catch:{ IOException -> 0x0256 }
            r8.<init>(r9)     // Catch:{ IOException -> 0x0256 }
            r7.<init>(r8)     // Catch:{ IOException -> 0x0256 }
            java.lang.String r8 = com.lp.C0987.f4438     // Catch:{ IOException -> 0x0256 }
            ru.pKkcGXHI.kKSaIWSZS.MainActivity r9 = com.lp.C0987.f4447     // Catch:{ IOException -> 0x0256 }
            java.io.File r2 = r9.getDir(r6, r2)     // Catch:{ IOException -> 0x0256 }
            java.lang.String r2 = r2.getAbsolutePath()     // Catch:{ IOException -> 0x0256 }
            boolean r2 = r8.startsWith(r2)     // Catch:{ IOException -> 0x0256 }
            if (r2 == 0) goto L_0x023f
            r5.writeBytes(r4)     // Catch:{ IOException -> 0x0256 }
            goto L_0x0242
        L_0x023f:
            r5.writeBytes(r3)     // Catch:{ IOException -> 0x0256 }
        L_0x0242:
            r5.flush()     // Catch:{ IOException -> 0x0256 }
            r5.close()     // Catch:{ IOException -> 0x0256 }
        L_0x0248:
            java.lang.String r2 = r7.readLine()     // Catch:{ IOException -> 0x0256 }
            if (r2 == 0) goto L_0x0252
            r1.add(r2)     // Catch:{ IOException -> 0x0256 }
            goto L_0x0248
        L_0x0252:
            r0.destroy()     // Catch:{ IOException -> 0x0256 }
            goto L_0x025a
        L_0x0256:
            r0 = move-exception
            r0.printStackTrace()
        L_0x025a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lp.C0973.m5992(java.util.List, java.lang.String, java.lang.String, java.lang.String[], boolean):void");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private String m5991() {
        return String.format("Carrier:%s\nModel:%s\nFirmware:%s\n", Build.BRAND, Build.MODEL, Build.VERSION.RELEASE);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m5994(Context context, boolean z) {
        String str;
        Iterator<String> it;
        ArrayList<String> arrayList = this.f4281;
        if (C0987.f4474) {
            m5992(arrayList, null, null, null, false);
        } else {
            m5992(arrayList, null, null, null, true);
        }
        if (z && arrayList.size() > 0) {
            try {
                str = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                str = BuildConfig.FLAVOR;
            }
            StringBuilder sb = new StringBuilder("Lucky Patcher " + str);
            sb.append("\n");
            String r3 = m5991();
            sb.append("\n");
            sb.append(r3);
            while (true) {
                try {
                    it = arrayList.iterator();
                    break;
                } catch (ConcurrentModificationException unused) {
                    C0987.m6060((Object) "LuckyPatcher (LogCollector): try again collect error_log.");
                    sb = new StringBuilder(str);
                    sb.append("\n");
                    sb.append("\n");
                    sb.append(r3);
                }
            }
            while (it.hasNext()) {
                sb.append("\n");
                sb.append(it.next());
            }
            String sb2 = sb.toString();
            new File("abrakakdabra");
            try {
                File file = new File(C0987.f4438 + "/Log/error_log.txt");
                new File(C0987.f4438 + "/Log").mkdirs();
                if (file.exists()) {
                    file.delete();
                }
                if (new File(C0987.f4438 + "/Log/error_log.zip").exists()) {
                    new File(C0987.f4438 + "/Log/error_log.zip").delete();
                }
                file.createNewFile();
                new FileOutputStream(file).write(sb2.getBytes());
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        if (arrayList.size() > 0) {
            return true;
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5993(Context context, String str, String str2, String str3) {
        String str4;
        if (C0815.m5279() == 32) {
            File file = new File(C0987.f4438 + "/Log/error_log.txt");
            Uri parse = Uri.parse("file://");
            try {
                ZipFile zipFile = new ZipFile(C0987.f4438 + "/Log/error_log.zip");
                ZipParameters zipParameters = new ZipParameters();
                zipParameters.setCompressionMethod(8);
                zipParameters.setCompressionLevel(5);
                zipFile.addFile(file, zipParameters);
                parse = Uri.parse("file://" + C0987.f4438 + "/Log/error_log.zip");
                if (C0987.f4489 >= 24) {
                    parse = FileProvider.m528(C0987.f4447, PkgName.getPkgName() + ".provider", new File(C0987.f4438 + "/Log/error_log.zip"));
                }
            } catch (ZipException e) {
                e.printStackTrace();
            } catch (Exception unused) {
            }
            String str5 = "/system/bin/dalvikvm";
            if (!C0815.m5307(str5)) {
                str5 = "not found";
            }
            if (C0815.m5307("/system/bin/dalvikvm32")) {
                str5 = "/system/bin/dalvikvm32";
            }
            if (C0987.f4438.startsWith(C0987.f4447.getDir("sdcard", 0).getAbsolutePath())) {
                str4 = "Email contain error_log.txt with bugs by Lucky Patcher! Please describe the Problem and tap to send email.\n\n\n" + C0815.m5285() + "\nRoot:" + C0987.f4474 + "\nRuntime: " + C0815.m5282() + "\nDeviceID: " + Settings.Secure.getString(C0987.m6072().getContentResolver(), "android_id") + "\n" + Build.BOARD + "\n" + Build.BRAND + "\n" + Build.CPU_ABI + "\n" + Build.DEVICE + "\n" + Build.DISPLAY + "\n" + Build.FINGERPRINT + "\nLucky Patcher ver: " + C0987.f4465 + "\nLP files directory: " + C0987.f4438 + "\ndalvikvm file:" + str5 + "\n\n" + BuildConfig.FLAVOR;
            } else {
                str4 = "Email contain error_log.txt with bugs by Lucky Patcher! Please describe the Problem and tap to send email.\n\n\n" + C0815.m5285() + "\nRoot:" + C0987.f4474 + "\nRuntime: " + C0815.m5282() + "\nDeviceID: " + Settings.Secure.getString(C0987.m6072().getContentResolver(), "android_id") + "\n" + Build.BOARD + "\n" + Build.BRAND + "\n" + Build.CPU_ABI + "\n" + Build.DEVICE + "\n" + Build.DISPLAY + "\n" + Build.FINGERPRINT + "\nLucky Patcher ver: " + C0987.f4465 + "\nLP files directory: " + C0987.f4438 + "\ndalvikvm file:" + str5 + "\n\n";
            }
            String str6 = str4;
            try {
                for (Map.Entry next : System.getenv().entrySet()) {
                    str6 = str6 + "\n" + ((String) next.getKey()) + ":" + ((String) next.getValue());
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            Intent intent = new Intent("android.intent.action.SEND_MULTIPLE");
            intent.setType("text/plain");
            intent.putExtra("android.intent.extra.EMAIL", new String[]{str});
            intent.putExtra("android.intent.extra.SUBJECT", str2);
            intent.putExtra("android.intent.extra.TEXT", str6);
            ArrayList arrayList = new ArrayList();
            arrayList.add(parse);
            File file2 = new File(LuckyApp.f3803.getDir("error_log", 0).getAbsolutePath() + "/Log/Exception." + C0987.f4465 + ".txt");
            File file3 = new File(C0987.f4438 + "/Log/Exception." + C0987.f4465 + ".txt");
            if (file2.exists()) {
                C0815.m5160(file2, file3);
                file2.delete();
            }
            if (file3.exists()) {
                if (C0987.f4489 >= 24) {
                    arrayList.add(FileProvider.m528(C0987.f4447, PkgName.getPkgName() + ".provider", file3));
                } else {
                    arrayList.add(Uri.fromFile(file3));
                }
            }
            intent.addFlags(1);
            intent.putParcelableArrayListExtra("android.intent.extra.STREAM", arrayList);
            try {
                C0987.f4447.startActivity(Intent.createChooser(intent, "Send mail"));
            } catch (RuntimeException e3) {
                e3.printStackTrace();
            }
        }
    }
}
