package com.android.vending.ʻ;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import java.util.List;

/* renamed from: com.android.vending.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: IInAppBillingServiceMod */
public interface C0773 extends IInterface {
    /* renamed from: ʻ  reason: contains not printable characters */
    int m4979(int i, String str, String str2);

    /* renamed from: ʻ  reason: contains not printable characters */
    Bundle m4980(int i, String str, String str2, Bundle bundle);

    /* renamed from: ʻ  reason: contains not printable characters */
    Bundle m4981(int i, String str, String str2, Bundle bundle, Bundle bundle2);

    /* renamed from: ʻ  reason: contains not printable characters */
    Bundle m4982(int i, String str, String str2, String str3);

    /* renamed from: ʻ  reason: contains not printable characters */
    Bundle m4983(int i, String str, String str2, String str3, Bundle bundle);

    /* renamed from: ʻ  reason: contains not printable characters */
    Bundle m4984(int i, String str, String str2, String str3, String str4);

    /* renamed from: ʻ  reason: contains not printable characters */
    Bundle m4985(int i, String str, String str2, String str3, String str4, Bundle bundle);

    /* renamed from: ʻ  reason: contains not printable characters */
    Bundle m4986(int i, String str, List<String> list, String str2, String str3, String str4);

    /* renamed from: ʼ  reason: contains not printable characters */
    int m4987(int i, String str, String str2);

    /* renamed from: ʼ  reason: contains not printable characters */
    int m4988(int i, String str, String str2, Bundle bundle);

    /* renamed from: ʼ  reason: contains not printable characters */
    Bundle m4989(int i, String str, String str2, String str3, Bundle bundle);

    /* renamed from: ʽ  reason: contains not printable characters */
    int m4990(int i, String str, String str2);

    /* renamed from: ʽ  reason: contains not printable characters */
    Bundle m4991(int i, String str, String str2, Bundle bundle);

    /* renamed from: ʽ  reason: contains not printable characters */
    Bundle m4992(int i, String str, String str2, String str3, Bundle bundle);

    /* renamed from: ʾ  reason: contains not printable characters */
    Bundle m4993(int i, String str, String str2, Bundle bundle);

    /* renamed from: com.android.vending.ʻ.ʻ$ʻ  reason: contains not printable characters */
    /* compiled from: IInAppBillingServiceMod */
    public static abstract class C0774 extends Binder implements C0773 {
        public IBinder asBinder() {
            return this;
        }

        public C0774() {
            attachInterface(this, "com.android.vending.billing.IInAppBillingService");
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static C0773 m4994(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.android.vending.billing.IInAppBillingService");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof C0773)) {
                return new C0775(iBinder);
            }
            return (C0773) queryLocalInterface;
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v4, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v9, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v1, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v9, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v3, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v13, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v30, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v16, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v34, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v19, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v7, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v23, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v40, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v26, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v8, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v30, resolved type: android.os.Bundle} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onTransact(int r12, android.os.Parcel r13, android.os.Parcel r14, int r15) {
            /*
                r11 = this;
                r0 = 801(0x321, float:1.122E-42)
                r1 = 0
                r2 = 0
                r3 = 1
                java.lang.String r4 = "com.android.vending.billing.IInAppBillingService"
                if (r12 == r0) goto L_0x02a3
                r0 = 1598968902(0x5f4e5446, float:1.4867585E19)
                if (r12 == r0) goto L_0x029f
                r0 = 901(0x385, float:1.263E-42)
                if (r12 == r0) goto L_0x025a
                r0 = 902(0x386, float:1.264E-42)
                if (r12 == r0) goto L_0x0228
                switch(r12) {
                    case 1: goto L_0x020e;
                    case 2: goto L_0x01dc;
                    case 3: goto L_0x01b0;
                    case 4: goto L_0x0189;
                    case 5: goto L_0x016f;
                    case 6: goto L_0x0155;
                    case 7: goto L_0x0125;
                    case 8: goto L_0x00e9;
                    case 9: goto L_0x00b1;
                    case 10: goto L_0x0088;
                    case 11: goto L_0x0050;
                    case 12: goto L_0x001e;
                    default: goto L_0x0019;
                }
            L_0x0019:
                boolean r12 = super.onTransact(r12, r13, r14, r15)
                return r12
            L_0x001e:
                r13.enforceInterface(r4)
                int r12 = r13.readInt()
                java.lang.String r15 = r13.readString()
                java.lang.String r0 = r13.readString()
                int r4 = r13.readInt()
                if (r4 == 0) goto L_0x003c
                android.os.Parcelable$Creator r1 = android.os.Bundle.CREATOR
                java.lang.Object r13 = r1.createFromParcel(r13)
                r1 = r13
                android.os.Bundle r1 = (android.os.Bundle) r1
            L_0x003c:
                android.os.Bundle r12 = r11.m4991(r12, r15, r0, r1)
                r14.writeNoException()
                if (r12 == 0) goto L_0x004c
                r14.writeInt(r3)
                r12.writeToParcel(r14, r3)
                goto L_0x004f
            L_0x004c:
                r14.writeInt(r2)
            L_0x004f:
                return r3
            L_0x0050:
                r13.enforceInterface(r4)
                int r5 = r13.readInt()
                java.lang.String r6 = r13.readString()
                java.lang.String r7 = r13.readString()
                java.lang.String r8 = r13.readString()
                int r12 = r13.readInt()
                if (r12 == 0) goto L_0x0072
                android.os.Parcelable$Creator r12 = android.os.Bundle.CREATOR
                java.lang.Object r12 = r12.createFromParcel(r13)
                r1 = r12
                android.os.Bundle r1 = (android.os.Bundle) r1
            L_0x0072:
                r9 = r1
                r4 = r11
                android.os.Bundle r12 = r4.m4992(r5, r6, r7, r8, r9)
                r14.writeNoException()
                if (r12 == 0) goto L_0x0084
                r14.writeInt(r3)
                r12.writeToParcel(r14, r3)
                goto L_0x0087
            L_0x0084:
                r14.writeInt(r2)
            L_0x0087:
                return r3
            L_0x0088:
                r13.enforceInterface(r4)
                int r12 = r13.readInt()
                java.lang.String r15 = r13.readString()
                java.lang.String r0 = r13.readString()
                int r2 = r13.readInt()
                if (r2 == 0) goto L_0x00a6
                android.os.Parcelable$Creator r1 = android.os.Bundle.CREATOR
                java.lang.Object r13 = r1.createFromParcel(r13)
                r1 = r13
                android.os.Bundle r1 = (android.os.Bundle) r1
            L_0x00a6:
                int r12 = r11.m4988(r12, r15, r0, r1)
                r14.writeNoException()
                r14.writeInt(r12)
                return r3
            L_0x00b1:
                r13.enforceInterface(r4)
                int r5 = r13.readInt()
                java.lang.String r6 = r13.readString()
                java.lang.String r7 = r13.readString()
                java.lang.String r8 = r13.readString()
                int r12 = r13.readInt()
                if (r12 == 0) goto L_0x00d3
                android.os.Parcelable$Creator r12 = android.os.Bundle.CREATOR
                java.lang.Object r12 = r12.createFromParcel(r13)
                r1 = r12
                android.os.Bundle r1 = (android.os.Bundle) r1
            L_0x00d3:
                r9 = r1
                r4 = r11
                android.os.Bundle r12 = r4.m4983(r5, r6, r7, r8, r9)
                r14.writeNoException()
                if (r12 == 0) goto L_0x00e5
                r14.writeInt(r3)
                r12.writeToParcel(r14, r3)
                goto L_0x00e8
            L_0x00e5:
                r14.writeInt(r2)
            L_0x00e8:
                return r3
            L_0x00e9:
                r13.enforceInterface(r4)
                int r5 = r13.readInt()
                java.lang.String r6 = r13.readString()
                java.lang.String r7 = r13.readString()
                java.lang.String r8 = r13.readString()
                java.lang.String r9 = r13.readString()
                int r12 = r13.readInt()
                if (r12 == 0) goto L_0x010f
                android.os.Parcelable$Creator r12 = android.os.Bundle.CREATOR
                java.lang.Object r12 = r12.createFromParcel(r13)
                r1 = r12
                android.os.Bundle r1 = (android.os.Bundle) r1
            L_0x010f:
                r10 = r1
                r4 = r11
                android.os.Bundle r12 = r4.m4985(r5, r6, r7, r8, r9, r10)
                r14.writeNoException()
                if (r12 == 0) goto L_0x0121
                r14.writeInt(r3)
                r12.writeToParcel(r14, r3)
                goto L_0x0124
            L_0x0121:
                r14.writeInt(r2)
            L_0x0124:
                return r3
            L_0x0125:
                r13.enforceInterface(r4)
                int r5 = r13.readInt()
                java.lang.String r6 = r13.readString()
                java.util.ArrayList r7 = r13.createStringArrayList()
                java.lang.String r8 = r13.readString()
                java.lang.String r9 = r13.readString()
                java.lang.String r10 = r13.readString()
                r4 = r11
                android.os.Bundle r12 = r4.m4986(r5, r6, r7, r8, r9, r10)
                r14.writeNoException()
                if (r12 == 0) goto L_0x0151
                r14.writeInt(r3)
                r12.writeToParcel(r14, r3)
                goto L_0x0154
            L_0x0151:
                r14.writeInt(r2)
            L_0x0154:
                return r3
            L_0x0155:
                r13.enforceInterface(r4)
                int r12 = r13.readInt()
                java.lang.String r15 = r13.readString()
                java.lang.String r13 = r13.readString()
                int r12 = r11.m4990(r12, r15, r13)
                r14.writeNoException()
                r14.writeInt(r12)
                return r3
            L_0x016f:
                r13.enforceInterface(r4)
                int r12 = r13.readInt()
                java.lang.String r15 = r13.readString()
                java.lang.String r13 = r13.readString()
                int r12 = r11.m4987(r12, r15, r13)
                r14.writeNoException()
                r14.writeInt(r12)
                return r3
            L_0x0189:
                r13.enforceInterface(r4)
                int r12 = r13.readInt()
                java.lang.String r15 = r13.readString()
                java.lang.String r0 = r13.readString()
                java.lang.String r13 = r13.readString()
                android.os.Bundle r12 = r11.m4982(r12, r15, r0, r13)
                r14.writeNoException()
                if (r12 == 0) goto L_0x01ac
                r14.writeInt(r3)
                r12.writeToParcel(r14, r3)
                goto L_0x01af
            L_0x01ac:
                r14.writeInt(r2)
            L_0x01af:
                return r3
            L_0x01b0:
                r13.enforceInterface(r4)
                int r5 = r13.readInt()
                java.lang.String r6 = r13.readString()
                java.lang.String r7 = r13.readString()
                java.lang.String r8 = r13.readString()
                java.lang.String r9 = r13.readString()
                r4 = r11
                android.os.Bundle r12 = r4.m4984(r5, r6, r7, r8, r9)
                r14.writeNoException()
                if (r12 == 0) goto L_0x01d8
                r14.writeInt(r3)
                r12.writeToParcel(r14, r3)
                goto L_0x01db
            L_0x01d8:
                r14.writeInt(r2)
            L_0x01db:
                return r3
            L_0x01dc:
                r13.enforceInterface(r4)
                int r12 = r13.readInt()
                java.lang.String r15 = r13.readString()
                java.lang.String r0 = r13.readString()
                int r4 = r13.readInt()
                if (r4 == 0) goto L_0x01fa
                android.os.Parcelable$Creator r1 = android.os.Bundle.CREATOR
                java.lang.Object r13 = r1.createFromParcel(r13)
                r1 = r13
                android.os.Bundle r1 = (android.os.Bundle) r1
            L_0x01fa:
                android.os.Bundle r12 = r11.m4980(r12, r15, r0, r1)
                r14.writeNoException()
                if (r12 == 0) goto L_0x020a
                r14.writeInt(r3)
                r12.writeToParcel(r14, r3)
                goto L_0x020d
            L_0x020a:
                r14.writeInt(r2)
            L_0x020d:
                return r3
            L_0x020e:
                r13.enforceInterface(r4)
                int r12 = r13.readInt()
                java.lang.String r15 = r13.readString()
                java.lang.String r13 = r13.readString()
                int r12 = r11.m4979(r12, r15, r13)
                r14.writeNoException()
                r14.writeInt(r12)
                return r3
            L_0x0228:
                r13.enforceInterface(r4)
                int r12 = r13.readInt()
                java.lang.String r15 = r13.readString()
                java.lang.String r0 = r13.readString()
                int r4 = r13.readInt()
                if (r4 == 0) goto L_0x0246
                android.os.Parcelable$Creator r1 = android.os.Bundle.CREATOR
                java.lang.Object r13 = r1.createFromParcel(r13)
                r1 = r13
                android.os.Bundle r1 = (android.os.Bundle) r1
            L_0x0246:
                android.os.Bundle r12 = r11.m4993(r12, r15, r0, r1)
                r14.writeNoException()
                if (r12 == 0) goto L_0x0256
                r14.writeInt(r3)
                r12.writeToParcel(r14, r3)
                goto L_0x0259
            L_0x0256:
                r14.writeInt(r2)
            L_0x0259:
                return r3
            L_0x025a:
                r13.enforceInterface(r4)
                int r5 = r13.readInt()
                java.lang.String r6 = r13.readString()
                java.lang.String r7 = r13.readString()
                int r12 = r13.readInt()
                if (r12 == 0) goto L_0x0279
                android.os.Parcelable$Creator r12 = android.os.Bundle.CREATOR
                java.lang.Object r12 = r12.createFromParcel(r13)
                android.os.Bundle r12 = (android.os.Bundle) r12
                r8 = r12
                goto L_0x027a
            L_0x0279:
                r8 = r1
            L_0x027a:
                int r12 = r13.readInt()
                if (r12 == 0) goto L_0x0289
                android.os.Parcelable$Creator r12 = android.os.Bundle.CREATOR
                java.lang.Object r12 = r12.createFromParcel(r13)
                r1 = r12
                android.os.Bundle r1 = (android.os.Bundle) r1
            L_0x0289:
                r9 = r1
                r4 = r11
                android.os.Bundle r12 = r4.m4981(r5, r6, r7, r8, r9)
                r14.writeNoException()
                if (r12 == 0) goto L_0x029b
                r14.writeInt(r3)
                r12.writeToParcel(r14, r3)
                goto L_0x029e
            L_0x029b:
                r14.writeInt(r2)
            L_0x029e:
                return r3
            L_0x029f:
                r14.writeString(r4)
                return r3
            L_0x02a3:
                r13.enforceInterface(r4)
                int r5 = r13.readInt()
                java.lang.String r6 = r13.readString()
                java.lang.String r7 = r13.readString()
                java.lang.String r8 = r13.readString()
                int r12 = r13.readInt()
                if (r12 == 0) goto L_0x02c5
                android.os.Parcelable$Creator r12 = android.os.Bundle.CREATOR
                java.lang.Object r12 = r12.createFromParcel(r13)
                r1 = r12
                android.os.Bundle r1 = (android.os.Bundle) r1
            L_0x02c5:
                r9 = r1
                r4 = r11
                android.os.Bundle r12 = r4.m4989(r5, r6, r7, r8, r9)
                r14.writeNoException()
                if (r12 == 0) goto L_0x02d7
                r14.writeInt(r3)
                r12.writeToParcel(r14, r3)
                goto L_0x02da
            L_0x02d7:
                r14.writeInt(r2)
            L_0x02da:
                return r3
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.vending.ʻ.C0773.C0774.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
        }

        /* renamed from: com.android.vending.ʻ.ʻ$ʻ$ʻ  reason: contains not printable characters */
        /* compiled from: IInAppBillingServiceMod */
        private static class C0775 implements C0773 {

            /* renamed from: ʻ  reason: contains not printable characters */
            private IBinder f3089;

            C0775(IBinder iBinder) {
                this.f3089 = iBinder;
            }

            public IBinder asBinder() {
                return this.f3089;
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public int m4995(int i, String str, String str2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.f3089.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public Bundle m4996(int i, String str, String str2, Bundle bundle) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f3089.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public Bundle m5000(int i, String str, String str2, String str3, String str4) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeString(str3);
                    obtain.writeString(str4);
                    this.f3089.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public Bundle m4998(int i, String str, String str2, String str3) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeString(str3);
                    this.f3089.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public int m5003(int i, String str, String str2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.f3089.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: ʽ  reason: contains not printable characters */
            public int m5006(int i, String str, String str2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.f3089.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public Bundle m5002(int i, String str, List<String> list, String str2, String str3, String str4) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeStringList(list);
                    obtain.writeString(str2);
                    obtain.writeString(str3);
                    obtain.writeString(str4);
                    this.f3089.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public Bundle m5001(int i, String str, String str2, String str3, String str4, Bundle bundle) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeString(str3);
                    obtain.writeString(str4);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f3089.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public Bundle m4999(int i, String str, String str2, String str3, Bundle bundle) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeString(str3);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f3089.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public int m5004(int i, String str, String str2, Bundle bundle) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f3089.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public Bundle m5005(int i, String str, String str2, String str3, Bundle bundle) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeString(str3);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f3089.transact(801, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: ʽ  reason: contains not printable characters */
            public Bundle m5008(int i, String str, String str2, String str3, Bundle bundle) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeString(str3);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f3089.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: ʽ  reason: contains not printable characters */
            public Bundle m5007(int i, String str, String str2, Bundle bundle) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f3089.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public Bundle m4997(int i, String str, String str2, Bundle bundle, Bundle bundle2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (bundle2 != null) {
                        obtain.writeInt(1);
                        bundle2.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f3089.transact(901, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            /* renamed from: ʾ  reason: contains not printable characters */
            public Bundle m5009(int i, String str, String str2, Bundle bundle) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f3089.transact(902, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
