package com.android.vending.licensing;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.android.vending.licensing.C0770;

public interface ILicensingService extends IInterface {
    /* renamed from: ʻ  reason: contains not printable characters */
    void m4973(long j, String str, C0770 r4);

    /* renamed from: com.android.vending.licensing.ILicensingService$ʻ  reason: contains not printable characters */
    public static abstract class C0768 extends Binder implements ILicensingService {
        public IBinder asBinder() {
            return this;
        }

        public C0768() {
            attachInterface(this, "com.android.vending.licensing.ILicensingService");
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static ILicensingService m4974(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.android.vending.licensing.ILicensingService");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof ILicensingService)) {
                return new C0769(iBinder);
            }
            return (ILicensingService) queryLocalInterface;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            if (i == 1) {
                parcel.enforceInterface("com.android.vending.licensing.ILicensingService");
                m4973(parcel.readLong(), parcel.readString(), C0770.C0771.m4977(parcel.readStrongBinder()));
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("com.android.vending.licensing.ILicensingService");
                return true;
            }
        }

        /* renamed from: com.android.vending.licensing.ILicensingService$ʻ$ʻ  reason: contains not printable characters */
        private static class C0769 implements ILicensingService {

            /* renamed from: ʻ  reason: contains not printable characters */
            private IBinder f3087;

            C0769(IBinder iBinder) {
                this.f3087 = iBinder;
            }

            public IBinder asBinder() {
                return this.f3087;
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m4975(long j, String str, C0770 r6) {
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.licensing.ILicensingService");
                    obtain.writeLong(j);
                    obtain.writeString(str);
                    obtain.writeStrongBinder(r6 != null ? r6.asBinder() : null);
                    this.f3087.transact(1, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }
        }
    }
}
