package com.android.vending.licensing;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: com.android.vending.licensing.ʻ  reason: contains not printable characters */
/* compiled from: ILicenseResultListener */
public interface C0770 extends IInterface {
    /* renamed from: ʻ  reason: contains not printable characters */
    void m4976(int i, String str, String str2);

    /* renamed from: com.android.vending.licensing.ʻ$ʻ  reason: contains not printable characters */
    /* compiled from: ILicenseResultListener */
    public static abstract class C0771 extends Binder implements C0770 {
        public IBinder asBinder() {
            return this;
        }

        public C0771() {
            attachInterface(this, "com.android.vending.licensing.ILicenseResultListener");
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public static C0770 m4977(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.android.vending.licensing.ILicenseResultListener");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof C0770)) {
                return new C0772(iBinder);
            }
            return (C0770) queryLocalInterface;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            if (i == 1) {
                parcel.enforceInterface("com.android.vending.licensing.ILicenseResultListener");
                m4976(parcel.readInt(), parcel.readString(), parcel.readString());
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("com.android.vending.licensing.ILicenseResultListener");
                return true;
            }
        }

        /* renamed from: com.android.vending.licensing.ʻ$ʻ$ʻ  reason: contains not printable characters */
        /* compiled from: ILicenseResultListener */
        private static class C0772 implements C0770 {

            /* renamed from: ʻ  reason: contains not printable characters */
            private IBinder f3088;

            C0772(IBinder iBinder) {
                this.f3088 = iBinder;
            }

            public IBinder asBinder() {
                return this.f3088;
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m4978(int i, String str, String str2) {
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.android.vending.licensing.ILicenseResultListener");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.f3088.transact(1, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }
        }
    }
}
