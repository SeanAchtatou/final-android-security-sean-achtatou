package ʽ.ʻ;

/* renamed from: ʽ.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: StringItem */
public class C1440 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public String f7452;

    /* renamed from: ʼ  reason: contains not printable characters */
    public int f7453;

    /* renamed from: ʽ  reason: contains not printable characters */
    public int f7454;

    public C1440() {
    }

    public C1440(String str) {
        this.f7452 = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        C1440 r5 = (C1440) obj;
        String str = this.f7452;
        if (str == null) {
            if (r5.f7452 != null) {
                return false;
            }
        } else if (!str.equals(r5.f7452)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        String str = this.f7452;
        return 31 + (str == null ? 0 : str.hashCode());
    }

    public String toString() {
        return String.format("S%04d %s", Integer.valueOf(this.f7454), this.f7452);
    }
}
