package ʽ.ʻ.ʻ;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import net.lingala.zip4j.util.InternalZipConstants;
import ʽ.ʻ.C1450;

/* renamed from: ʽ.ʻ.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: AxmlParser */
public class C1441 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f7455;

    /* renamed from: ʼ  reason: contains not printable characters */
    private IntBuffer f7456;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f7457;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f7458;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f7459;

    /* renamed from: ˆ  reason: contains not printable characters */
    private ByteBuffer f7460;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f7461;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f7462;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f7463;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f7464;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int[] f7465;

    /* renamed from: ˏ  reason: contains not printable characters */
    private String[] f7466;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f7467;

    /* renamed from: י  reason: contains not printable characters */
    private int f7468;

    public C1441(byte[] bArr) {
        this(ByteBuffer.wrap(bArr));
    }

    public C1441(ByteBuffer byteBuffer) {
        this.f7458 = -1;
        this.f7460 = byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m8000() {
        return this.f7455;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m8001(int i) {
        return this.f7466[this.f7456.get((i * 5) + 1)];
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m8003(int i) {
        int i2 = this.f7456.get((i * 5) + 0);
        if (i2 >= 0) {
            return this.f7466[i2];
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public String m8005(int i) {
        int i2 = this.f7456.get((i * 5) + 2);
        if (i2 >= 0) {
            return this.f7466[i2];
        }
        return null;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m8006(int i) {
        int i2;
        if (this.f7465 == null || (i2 = this.f7456.get((i * 5) + 1)) < 0) {
            return -1;
        }
        int[] iArr = this.f7465;
        if (i2 < iArr.length) {
            return iArr[i2];
        }
        return -1;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m8008(int i) {
        return this.f7456.get((i * 5) + 3) >> 24;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public Object m8010(int i) {
        int i2 = this.f7456.get((i * 5) + 4);
        if (i == this.f7459) {
            return C1449.m8038(i2, m8005(i));
        }
        if (i == this.f7467) {
            return C1449.m8039(i2, m8005(i));
        }
        if (i == this.f7457) {
            return C1449.m8040(i2, m8005(i));
        }
        int r3 = m8008(i);
        if (r3 == 3) {
            return this.f7466[i2];
        }
        if (r3 != 18) {
            return Integer.valueOf(i2);
        }
        return Boolean.valueOf(i2 != 0);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m8002() {
        return this.f7461;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m8004() {
        return this.f7466[this.f7462];
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public String m8007() {
        return this.f7466[this.f7464];
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public String m8009() {
        int i = this.f7463;
        if (i >= 0) {
            return this.f7466[i];
        }
        return null;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public String m8011() {
        return this.f7466[this.f7468];
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public int m8012() {
        int i = 3;
        if (this.f7458 >= 0) {
            while (true) {
                int position = this.f7460.position();
                if (position >= this.f7458) {
                    return 7;
                }
                int i2 = this.f7460.getInt() & InternalZipConstants.MAX_ALLOWED_ZIP_COMMENT_LENGTH;
                int i3 = this.f7460.getInt();
                if (i2 == 1) {
                    this.f7466 = C1450.m8041(this.f7460);
                    this.f7460.position(position + i3);
                } else if (i2 != 384) {
                    switch (i2) {
                        case 256:
                            this.f7461 = this.f7460.getInt();
                            this.f7460.getInt();
                            this.f7464 = this.f7460.getInt();
                            this.f7463 = this.f7460.getInt();
                            i = 4;
                            break;
                        case 257:
                            this.f7460.position(position + i3);
                            i = 5;
                            break;
                        case 258:
                            this.f7461 = this.f7460.getInt();
                            this.f7460.getInt();
                            this.f7463 = this.f7460.getInt();
                            this.f7462 = this.f7460.getInt();
                            if (this.f7460.getInt() == 1310740) {
                                this.f7455 = this.f7460.getShort() & 65535;
                                this.f7459 = (this.f7460.getShort() & 65535) - 1;
                                this.f7457 = (this.f7460.getShort() & 65535) - 1;
                                this.f7467 = (this.f7460.getShort() & 65535) - 1;
                                this.f7456 = this.f7460.asIntBuffer();
                                i = 2;
                                break;
                            } else {
                                throw new RuntimeException();
                            }
                        case 259:
                            this.f7460.position(position + i3);
                            break;
                        case 260:
                            this.f7461 = this.f7460.getInt();
                            this.f7460.getInt();
                            this.f7468 = this.f7460.getInt();
                            this.f7460.getInt();
                            this.f7460.getInt();
                            i = 6;
                            break;
                        default:
                            throw new RuntimeException();
                    }
                    this.f7460.position(position + i3);
                    return i;
                } else {
                    int i4 = (i3 / 4) - 2;
                    this.f7465 = new int[i4];
                    for (int i5 = 0; i5 < i4; i5++) {
                        this.f7465[i5] = this.f7460.getInt();
                    }
                    this.f7460.position(position + i3);
                }
            }
        } else if ((this.f7460.getInt() & InternalZipConstants.MAX_ALLOWED_ZIP_COMMENT_LENGTH) == 3) {
            this.f7458 = this.f7460.getInt();
            return 1;
        } else {
            throw new RuntimeException();
        }
    }
}
