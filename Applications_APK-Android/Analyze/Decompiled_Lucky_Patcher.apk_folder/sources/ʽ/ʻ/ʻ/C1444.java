package ʽ.ʻ.ʻ;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;
import ʽ.ʻ.C1440;
import ʽ.ʻ.C1450;

/* renamed from: ʽ.ʻ.ʻ.ʾ  reason: contains not printable characters */
/* compiled from: AxmlWriter */
public class C1444 extends C1443 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final Comparator<C1445> f7471 = new Comparator<C1445>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public int compare(C1445 r5, C1445 r6) {
            int i = r5.f7482 - r6.f7482;
            if (i != 0) {
                return i;
            }
            int compareTo = r5.f7480.f7452.compareTo(r6.f7480.f7452);
            if (compareTo != 0) {
                return compareTo;
            }
            boolean z = r5.f7481 == null;
            boolean z2 = r6.f7481 == null;
            if (z) {
                return z2 ? 0 : -1;
            }
            if (z2) {
                return 1;
            }
            return r5.f7481.f7452.compareTo(r6.f7481.f7452);
        }
    };

    /* renamed from: ʼ  reason: contains not printable characters */
    private List<C1446> f7472 = new ArrayList(3);

    /* renamed from: ʽ  reason: contains not printable characters */
    private Map<String, C1447> f7473 = new HashMap();

    /* renamed from: ʿ  reason: contains not printable characters */
    private List<C1440> f7474 = new ArrayList();

    /* renamed from: ˆ  reason: contains not printable characters */
    private Map<String, C1440> f7475 = new HashMap();

    /* renamed from: ˈ  reason: contains not printable characters */
    private List<Integer> f7476 = new ArrayList();

    /* renamed from: ˉ  reason: contains not printable characters */
    private List<C1440> f7477 = new ArrayList();

    /* renamed from: ˊ  reason: contains not printable characters */
    private C1450 f7478 = new C1450();

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m8020() {
    }

    /* renamed from: ʽ.ʻ.ʻ.ʾ$ʻ  reason: contains not printable characters */
    /* compiled from: AxmlWriter */
    static class C1445 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public int f7479;

        /* renamed from: ʼ  reason: contains not printable characters */
        public C1440 f7480;

        /* renamed from: ʽ  reason: contains not printable characters */
        public C1440 f7481;

        /* renamed from: ʾ  reason: contains not printable characters */
        public int f7482;

        /* renamed from: ʿ  reason: contains not printable characters */
        public int f7483;

        /* renamed from: ˆ  reason: contains not printable characters */
        public Object f7484;

        /* renamed from: ˈ  reason: contains not printable characters */
        public C1440 f7485;

        public C1445(C1440 r1, C1440 r2, int i) {
            this.f7481 = r1;
            this.f7480 = r2;
            this.f7482 = i;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m8025(C1444 r4) {
            this.f7481 = r4.m8022(this.f7481);
            C1440 r0 = this.f7480;
            if (r0 != null) {
                int i = this.f7482;
                if (i != -1) {
                    this.f7480 = r4.m8019(r0, i);
                } else {
                    this.f7480 = r4.m8018(r0);
                }
            }
            Object obj = this.f7484;
            if (obj instanceof C1440) {
                this.f7484 = r4.m8018((C1440) obj);
            }
            C1440 r02 = this.f7485;
            if (r02 != null) {
                this.f7485 = r4.m8018(r02);
            }
        }
    }

    /* renamed from: ʽ.ʻ.ʻ.ʾ$ʼ  reason: contains not printable characters */
    /* compiled from: AxmlWriter */
    static class C1446 extends C1448 {

        /* renamed from: ʻ  reason: contains not printable characters */
        C1445 f7486;

        /* renamed from: ʼ  reason: contains not printable characters */
        C1445 f7487;

        /* renamed from: ʽ  reason: contains not printable characters */
        C1445 f7488;

        /* renamed from: ʿ  reason: contains not printable characters */
        private Set<C1445> f7489 = new TreeSet(C1444.f7471);

        /* renamed from: ˆ  reason: contains not printable characters */
        private List<C1446> f7490 = new ArrayList();

        /* renamed from: ˈ  reason: contains not printable characters */
        private int f7491;

        /* renamed from: ˉ  reason: contains not printable characters */
        private C1440 f7492;

        /* renamed from: ˊ  reason: contains not printable characters */
        private C1440 f7493;

        /* renamed from: ˋ  reason: contains not printable characters */
        private C1440 f7494;

        /* renamed from: ˎ  reason: contains not printable characters */
        private int f7495;

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m8028() {
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C1446(String str, String str2) {
            super(null);
            C1440 r0 = null;
            this.f7493 = str == null ? null : new C1440(str);
            this.f7492 = str2 != null ? new C1440(str2) : r0;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m8031(String str, String str2, int i, int i2, Object obj) {
            if (str2 != null) {
                C1445 r0 = new C1445(str == null ? null : new C1440(str), new C1440(str2), i);
                r0.f7483 = i2;
                if (obj instanceof C1449) {
                    C1449 r8 = (C1449) obj;
                    if (r8.f7501 != null) {
                        r0.f7485 = new C1440(r8.f7501);
                    }
                    r0.f7484 = Integer.valueOf(r8.f7502);
                    int i3 = r8.f7500;
                    if (i3 == 1) {
                        this.f7486 = r0;
                    } else if (i3 == 2) {
                        this.f7487 = r0;
                    } else if (i3 == 3) {
                        this.f7488 = r0;
                    }
                } else if (i2 == 3) {
                    C1440 r4 = new C1440((String) obj);
                    r0.f7485 = r4;
                    r0.f7484 = r4;
                } else {
                    r0.f7485 = null;
                    r0.f7484 = obj;
                }
                this.f7489.add(r0);
                return;
            }
            throw new RuntimeException("name can't be null");
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C1448 m8027(String str, String str2) {
            C1446 r0 = new C1446(str, str2);
            this.f7490.add(r0);
            return r0;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m8029(int i) {
            this.f7491 = i;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m8026(C1444 r5) {
            this.f7493 = r5.m8022(this.f7493);
            this.f7492 = r5.m8018(this.f7492);
            int i = 0;
            for (C1445 next : this.f7489) {
                next.f7479 = i;
                next.m8025(r5);
                i++;
            }
            this.f7494 = r5.m8018(this.f7494);
            int size = (this.f7489.size() * 20) + 60;
            for (C1446 r2 : this.f7490) {
                size += r2.m8026(r5);
            }
            return this.f7494 != null ? size + 28 : size;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m8030(int i, String str) {
            this.f7494 = new C1440(str);
            this.f7495 = i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m8032(ByteBuffer byteBuffer) {
            byteBuffer.putInt(1048834);
            byteBuffer.putInt((this.f7489.size() * 20) + 36);
            byteBuffer.putInt(this.f7491);
            int i = -1;
            byteBuffer.putInt(-1);
            C1440 r1 = this.f7493;
            byteBuffer.putInt(r1 != null ? r1.f7454 : -1);
            byteBuffer.putInt(this.f7492.f7454);
            byteBuffer.putInt(1310740);
            byteBuffer.putShort((short) this.f7489.size());
            C1445 r12 = this.f7486;
            byteBuffer.putShort((short) (r12 == null ? 0 : r12.f7479 + 1));
            C1445 r13 = this.f7488;
            byteBuffer.putShort((short) (r13 == null ? 0 : r13.f7479 + 1));
            C1445 r14 = this.f7487;
            byteBuffer.putShort((short) (r14 == null ? 0 : r14.f7479 + 1));
            for (C1445 next : this.f7489) {
                byteBuffer.putInt(next.f7481 == null ? -1 : next.f7481.f7454);
                byteBuffer.putInt(next.f7480.f7454);
                byteBuffer.putInt(next.f7485 != null ? next.f7485.f7454 : -1);
                byteBuffer.putInt((next.f7483 << 24) | 8);
                Object obj = next.f7484;
                if (obj instanceof C1440) {
                    byteBuffer.putInt(((C1440) next.f7484).f7454);
                } else if (obj instanceof Boolean) {
                    byteBuffer.putInt(Boolean.TRUE.equals(obj) ? -1 : 0);
                } else {
                    byteBuffer.putInt(((Integer) next.f7484).intValue());
                }
            }
            if (this.f7494 != null) {
                byteBuffer.putInt(1048836);
                byteBuffer.putInt(28);
                byteBuffer.putInt(this.f7495);
                byteBuffer.putInt(-1);
                byteBuffer.putInt(this.f7494.f7454);
                byteBuffer.putInt(8);
                byteBuffer.putInt(0);
            }
            for (C1446 r2 : this.f7490) {
                r2.m8032(byteBuffer);
            }
            byteBuffer.putInt(1048835);
            byteBuffer.putInt(24);
            byteBuffer.putInt(-1);
            byteBuffer.putInt(-1);
            C1440 r15 = this.f7493;
            if (r15 != null) {
                i = r15.f7454;
            }
            byteBuffer.putInt(i);
            byteBuffer.putInt(this.f7492.f7454);
        }
    }

    /* renamed from: ʽ.ʻ.ʻ.ʾ$ʽ  reason: contains not printable characters */
    /* compiled from: AxmlWriter */
    static class C1447 {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f7496;

        /* renamed from: ʼ  reason: contains not printable characters */
        C1440 f7497;

        /* renamed from: ʽ  reason: contains not printable characters */
        C1440 f7498;

        public C1447(C1440 r1, C1440 r2, int i) {
            this.f7497 = r1;
            this.f7498 = r2;
            this.f7496 = i;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C1448 m8017(String str, String str2) {
        C1446 r0 = new C1446(str, str2);
        this.f7472.add(r0);
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m8021(String str, String str2, int i) {
        this.f7473.put(str2, new C1447(str == null ? null : new C1440(str), new C1440(str2), i));
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private int m8016() {
        int i = 0;
        for (C1446 r3 : this.f7472) {
            i += r3.m8026(this);
        }
        int i2 = 0;
        for (Map.Entry next : this.f7473.entrySet()) {
            C1447 r6 = (C1447) next.getValue();
            if (r6 == null) {
                r6 = new C1447(null, new C1440((String) next.getKey()), 0);
                next.setValue(r6);
            }
            if (r6.f7497 == null) {
                r6.f7497 = new C1440(String.format("axml_auto_%02d", Integer.valueOf(i2)));
                i2++;
            }
            r6.f7497 = m8018(r6.f7497);
            r6.f7498 = m8018(r6.f7498);
        }
        int size = i + (this.f7473.size() * 24 * 2);
        this.f7478.addAll(this.f7477);
        this.f7477 = null;
        this.f7478.addAll(this.f7474);
        this.f7474 = null;
        this.f7478.m8045();
        int r0 = this.f7478.m8044();
        int i3 = r0 % 4;
        if (i3 != 0) {
            r0 += 4 - i3;
        }
        return size + r0 + 8 + (this.f7476.size() * 4) + 8;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public byte[] m8023() {
        int r0 = m8016() + 8;
        ByteBuffer order = ByteBuffer.allocate(r0).order(ByteOrder.LITTLE_ENDIAN);
        order.putInt(524291);
        order.putInt(r0);
        int r02 = this.f7478.m8044();
        int i = r02 % 4;
        int i2 = i != 0 ? 4 - i : 0;
        order.putInt(1835009);
        order.putInt(r02 + i2 + 8);
        this.f7478.m8046(order);
        order.put(new byte[i2]);
        order.putInt(524672);
        order.putInt((this.f7476.size() * 4) + 8);
        for (Integer intValue : this.f7476) {
            order.putInt(intValue.intValue());
        }
        Stack stack = new Stack();
        for (Map.Entry<String, C1447> value : this.f7473.entrySet()) {
            C1447 r3 = (C1447) value.getValue();
            stack.push(r3);
            order.putInt(1048832);
            order.putInt(24);
            order.putInt(-1);
            order.putInt(-1);
            order.putInt(r3.f7497.f7454);
            order.putInt(r3.f7498.f7454);
        }
        for (C1446 r32 : this.f7472) {
            r32.m8032(order);
        }
        while (stack.size() > 0) {
            C1447 r2 = (C1447) stack.pop();
            order.putInt(1048833);
            order.putInt(24);
            order.putInt(r2.f7496);
            order.putInt(-1);
            order.putInt(r2.f7497.f7454);
            order.putInt(r2.f7498.f7454);
        }
        return order.array();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C1440 m8018(C1440 r2) {
        if (r2 == null) {
            return null;
        }
        int indexOf = this.f7474.indexOf(r2);
        if (indexOf >= 0) {
            return this.f7474.get(indexOf);
        }
        C1440 r0 = new C1440(r2.f7452);
        this.f7474.add(r0);
        return r0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public C1440 m8022(C1440 r4) {
        if (r4 == null) {
            return null;
        }
        String str = r4.f7452;
        if (!this.f7473.containsKey(str)) {
            this.f7473.put(str, null);
        }
        return m8018(r4);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C1440 m8019(C1440 r3, int i) {
        String str = r3.f7452 + i;
        C1440 r1 = this.f7475.get(str);
        if (r1 != null) {
            return r1;
        }
        C1440 r12 = new C1440(r3.f7452);
        this.f7476.add(Integer.valueOf(i));
        this.f7477.add(r12);
        this.f7475.put(str, r12);
        return r12;
    }
}
