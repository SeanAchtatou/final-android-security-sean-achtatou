package ʽ.ʻ;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/* renamed from: ʽ.ʻ.ʼ  reason: contains not printable characters */
/* compiled from: StringItems */
public class C1450 extends ArrayList<C1440> {

    /* renamed from: ʻ  reason: contains not printable characters */
    byte[] f7503;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f7504 = true;

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String[] m8041(ByteBuffer byteBuffer) {
        String str;
        int position = byteBuffer.position() - 8;
        int i = byteBuffer.getInt();
        byteBuffer.getInt();
        int i2 = byteBuffer.getInt();
        int i3 = byteBuffer.getInt();
        int i4 = byteBuffer.getInt();
        int[] iArr = new int[i];
        String[] strArr = new String[i];
        for (int i5 = 0; i5 < i; i5++) {
            iArr[i5] = byteBuffer.getInt();
        }
        if (i4 != 0) {
            System.err.println("ignore style offset at 0x" + Integer.toHexString(position));
        }
        int i6 = position + i3;
        for (int i7 = 0; i7 < iArr.length; i7++) {
            byteBuffer.position(iArr[i7] + i6);
            if ((i2 & 256) != 0) {
                m8043(byteBuffer);
                int r1 = m8043(byteBuffer);
                int position2 = byteBuffer.position();
                while (byteBuffer.get(position2 + r1) != 0) {
                    r1++;
                }
                str = new String(byteBuffer.array(), position2, r1, "UTF-8");
            } else {
                str = new String(byteBuffer.array(), byteBuffer.position(), m8042(byteBuffer) * 2, "UTF-16LE");
            }
            strArr[i7] = str;
        }
        return strArr;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static int m8042(ByteBuffer byteBuffer) {
        short s = byteBuffer.getShort() & 65535;
        return s > Short.MAX_VALUE ? ((s & Short.MAX_VALUE) << 8) | (byteBuffer.getShort() & 65535) : s;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    static int m8043(ByteBuffer byteBuffer) {
        byte b = byteBuffer.get() & 255;
        return (b & 128) != 0 ? ((b & Byte.MAX_VALUE) << 8) | (byteBuffer.get() & 255) : b;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m8044() {
        return (size() * 4) + 20 + this.f7503.length + 0;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m8045() {
        Iterator it = iterator();
        while (it.hasNext()) {
            if (((C1440) it.next()).f7452.length() > 32767) {
                this.f7504 = false;
            }
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.reset();
        HashMap hashMap = new HashMap();
        Iterator it2 = iterator();
        int i = 0;
        int i2 = 0;
        while (it2.hasNext()) {
            C1440 r7 = (C1440) it2.next();
            int i3 = i + 1;
            r7.f7454 = i;
            String str = r7.f7452;
            Integer num = (Integer) hashMap.get(str);
            if (num != null) {
                r7.f7453 = num.intValue();
            } else {
                r7.f7453 = i2;
                hashMap.put(str, Integer.valueOf(i2));
                if (this.f7504) {
                    int length = str.length();
                    byte[] bytes = str.getBytes("UTF-8");
                    int length2 = bytes.length;
                    if (length > 127) {
                        i2++;
                        byteArrayOutputStream.write((length >> 8) | 128);
                    }
                    byteArrayOutputStream.write(length);
                    if (length2 > 127) {
                        i2++;
                        byteArrayOutputStream.write((length2 >> 8) | 128);
                    }
                    byteArrayOutputStream.write(length2);
                    byteArrayOutputStream.write(bytes);
                    byteArrayOutputStream.write(0);
                    i2 += length2 + 3;
                } else {
                    int length3 = str.length();
                    byte[] bytes2 = str.getBytes("UTF-16LE");
                    if (length3 > 32767) {
                        int i4 = (length3 >> 16) | 32768;
                        byteArrayOutputStream.write(i4);
                        byteArrayOutputStream.write(i4 >> 8);
                        i2 += 2;
                    }
                    byteArrayOutputStream.write(length3);
                    byteArrayOutputStream.write(length3 >> 8);
                    byteArrayOutputStream.write(bytes2);
                    byteArrayOutputStream.write(0);
                    byteArrayOutputStream.write(0);
                    i2 += bytes2.length + 4;
                }
            }
            i = i3;
        }
        this.f7503 = byteArrayOutputStream.toByteArray();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m8046(ByteBuffer byteBuffer) {
        byteBuffer.putInt(size());
        byteBuffer.putInt(0);
        byteBuffer.putInt(this.f7504 ? 256 : 0);
        byteBuffer.putInt((size() * 4) + 28);
        byteBuffer.putInt(0);
        Iterator it = iterator();
        while (it.hasNext()) {
            byteBuffer.putInt(((C1440) it.next()).f7453);
        }
        byteBuffer.put(this.f7503);
    }
}
