package javaroot.utils;

import com.chelpus.C0815;
import com.lp.C0987;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import net.lingala.zip4j.util.InternalZipConstants;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

public class copyLibsFilesToSystemBackup {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String f5246 = "";

    /* renamed from: ʼ  reason: contains not printable characters */
    public static String f5247 = "";

    /* renamed from: ʽ  reason: contains not printable characters */
    public static String f5248 = "";

    /* renamed from: ʾ  reason: contains not printable characters */
    public static String f5249 = "";

    /* renamed from: ʿ  reason: contains not printable characters */
    public static boolean f5250 = false;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      com.chelpus.ˆ.ʻ(byte, byte, byte, byte):int
      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String[], boolean, java.lang.String[]):int
      com.chelpus.ˆ.ʻ(java.lang.String, byte[][], byte, java.lang.String):int
      com.chelpus.ˆ.ʻ(int, java.lang.String, java.lang.String, java.lang.String):void
      com.chelpus.ˆ.ʻ(android.widget.LinearLayout, java.lang.Runnable, java.util.Timer, java.util.TimerTask):void
      com.chelpus.ˆ.ʻ(java.io.File, java.util.ArrayList<java.io.File>, java.lang.String, java.lang.String):void
      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener, android.content.DialogInterface$OnCancelListener):void
      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, android.view.View$OnClickListener, android.view.View$OnClickListener):void
      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, java.lang.String, java.lang.String):void
      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, java.util.ArrayList<java.io.File>, java.lang.String):void
      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, boolean, boolean):boolean
      com.chelpus.ˆ.ʻ(java.lang.String, java.util.ArrayList<com.chelpus.ʻ.ʼ.ˉ>, boolean, boolean):boolean
      com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, java.lang.String, boolean):void */
    public static void main(String[] strArr) {
        C0815.m5165(new Object() {
        });
        f5247 = strArr[0];
        String str = strArr[1];
        String str2 = strArr[2];
        f5249 = C0815.m5256(new File(strArr[3])).getAbsolutePath();
        String str3 = strArr[4];
        if (str3.equals("copyLibs")) {
            System.out.println("copyLibs");
            f5246 = "/system/lib/";
            ArrayList<String> r0 = m6255(new File(str), str2);
            if (r0.size() > 0) {
                Iterator<String> it = r0.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    File file = new File(it.next());
                    if (!new File(f5246).exists()) {
                        new File(f5246).mkdirs();
                    }
                    C0815.m5180(f5249, f5249 + f5248, "755", true);
                    C0815.m5160(file, new File(f5246 + file.getName() + ".chelpus"));
                    m6256("chmod 0644 " + f5246 + file.getName() + ".chelpus");
                    m6256("chown 0.0 " + f5246 + file.getName() + ".chelpus");
                    m6256("chown 0:0 " + f5246 + file.getName() + ".chelpus");
                    if (file.length() == new File(f5246 + file.getName() + ".chelpus").length()) {
                        System.out.println("LuckyPatcher: copy lib " + f5246 + file.getName() + ".chelpus");
                    } else {
                        Iterator<String> it2 = r0.iterator();
                        while (it2.hasNext()) {
                            File file2 = new File(it2.next());
                            if (C0987.f4489 < 21) {
                                new File(f5246 + file2.getName() + ".chelpus").delete();
                            } else if (f5246.startsWith("/system/priv-app/")) {
                                try {
                                    new C0815(BuildConfig.FLAVOR).m5347(new File(f5246));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        System.out.println("In /system space not found!");
                        f5250 = true;
                    }
                }
            }
        }
        if (str3.equals("replaceOldLibs")) {
            System.out.println("replaceOldLibs");
            f5246 = "/system/lib/";
            if (C0987.f4489 >= 21) {
                if (new File(f5249 + "/lib/x86").exists()) {
                    f5246 = f5249 + "/lib/x86/";
                }
                if (new File(f5249 + "/lib/x86_64").exists()) {
                    f5246 = f5249 + "/lib/x86_64/";
                }
                if (new File(f5249 + "/lib/arm").exists()) {
                    f5246 = f5249 + "/lib/arm/";
                }
                if (new File(f5249 + "/lib/arm64").exists()) {
                    f5246 = f5249 + "/lib/arm64/";
                }
                if (new File(f5249 + "/lib/mips").exists()) {
                    f5246 = f5249 + "/lib/mips/";
                }
            }
            new ArrayList();
            File[] listFiles = new File(f5246).listFiles();
            if (listFiles != null && listFiles.length > 0) {
                for (File file3 : listFiles) {
                    if (file3.isFile() && file3.getName().endsWith(".chelpus")) {
                        System.out.println("Replace system lib:" + file3.getAbsolutePath().replace(".chelpus", BuildConfig.FLAVOR));
                        new File(file3.getAbsolutePath().replace(".chelpus", BuildConfig.FLAVOR)).delete();
                        file3.renameTo(new File(file3.getAbsolutePath().replace(".chelpus", BuildConfig.FLAVOR)));
                    }
                }
            }
            m6257(new File(str), str2);
        }
        if (str3.equals("deleteBigLibs")) {
            System.out.println("deleteBigLibs");
            f5246 = "/system/lib/";
            ArrayList<String> r02 = m6255(new File(str), str2);
            if (r02.size() > 0) {
                Iterator<String> it3 = r02.iterator();
                while (it3.hasNext()) {
                    File file4 = new File(it3.next());
                    new File(f5246 + file4.getName() + ".chelpus").delete();
                }
            }
        }
        C0815.m5312();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static ArrayList<String> m6255(File file, String str) {
        String str2;
        File[] listFiles;
        File file2 = file;
        String str3 = str;
        ArrayList<String> arrayList = new ArrayList<>();
        if (!str3.startsWith("/mnt/")) {
            String str4 = " is dir:";
            if (C0987.f4489 >= 21) {
                File r0 = C0815.m5256(new File(str3));
                if (r0.exists()) {
                    File[] listFiles2 = r0.listFiles();
                    int length = listFiles2.length;
                    int i = 0;
                    while (i < length) {
                        File file3 = listFiles2[i];
                        System.out.println("LuckyPatcher: file found in data dir - " + file3 + str4 + file3.isDirectory());
                        if (file3.isDirectory()) {
                            if (file3.getAbsolutePath().equals(r0 + "/lib") && (listFiles = file3.listFiles()) != null && listFiles.length > 0) {
                                int length2 = listFiles.length;
                                int i2 = 0;
                                while (i2 < length2) {
                                    File[] listFiles3 = listFiles[i2].listFiles();
                                    if (listFiles3 != null && listFiles3.length > 0) {
                                        int length3 = listFiles3.length;
                                        int i3 = 0;
                                        while (i3 < length3) {
                                            File file4 = listFiles3[i3];
                                            arrayList.add(file4.getAbsolutePath());
                                            File file5 = r0;
                                            f5248 = C0815.m5256(file4).getAbsolutePath().replace(r0.getAbsolutePath(), BuildConfig.FLAVOR);
                                            f5246 = f5249 + f5248 + InternalZipConstants.ZIP_FILE_SEPARATOR;
                                            System.out.println("libdir" + f5246);
                                            new File(f5246).mkdirs();
                                            System.out.println("LuckyPatcher: found lib - " + file4.getAbsolutePath());
                                            i3++;
                                            listFiles2 = listFiles2;
                                            length = length;
                                            r0 = file5;
                                        }
                                    }
                                    i2++;
                                    listFiles2 = listFiles2;
                                    length = length;
                                    r0 = r0;
                                }
                            }
                        }
                        i++;
                        listFiles2 = listFiles2;
                        length = length;
                        r0 = r0;
                    }
                }
            } else if (file.exists()) {
                String[] list = file.list();
                int length4 = list.length;
                int i4 = 0;
                while (i4 < length4) {
                    String str5 = list[i4];
                    PrintStream printStream = System.out;
                    StringBuilder sb = new StringBuilder();
                    sb.append("LuckyPatcher: file found in data dir - ");
                    sb.append(file2);
                    sb.append(InternalZipConstants.ZIP_FILE_SEPARATOR);
                    sb.append(str5);
                    sb.append("id dir:");
                    sb.append(new File(file2 + InternalZipConstants.ZIP_FILE_SEPARATOR + str5).isDirectory());
                    printStream.println(sb.toString());
                    if (new File(file2 + InternalZipConstants.ZIP_FILE_SEPARATOR + str5).isDirectory()) {
                        if ((file2 + InternalZipConstants.ZIP_FILE_SEPARATOR + str5).equals(file2 + "/lib")) {
                            String[] list2 = new File(file2 + InternalZipConstants.ZIP_FILE_SEPARATOR + str5).list();
                            int length5 = list2.length;
                            int i5 = 0;
                            while (i5 < length5) {
                                String str6 = list2[i5];
                                String[] strArr = list;
                                int i6 = length4;
                                if (!new File(file2 + InternalZipConstants.ZIP_FILE_SEPARATOR + str5 + InternalZipConstants.ZIP_FILE_SEPARATOR + str6).isFile() || str6.contains("libjnigraphics.so")) {
                                    str2 = str4;
                                } else {
                                    arrayList.add(file2 + InternalZipConstants.ZIP_FILE_SEPARATOR + str5 + InternalZipConstants.ZIP_FILE_SEPARATOR + str6);
                                    PrintStream printStream2 = System.out;
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append("LuckyPatcher: found lib - ");
                                    sb2.append(file2);
                                    sb2.append(InternalZipConstants.ZIP_FILE_SEPARATOR);
                                    sb2.append(str5);
                                    sb2.append(InternalZipConstants.ZIP_FILE_SEPARATOR);
                                    sb2.append(str6);
                                    sb2.append(str4);
                                    str2 = str4;
                                    sb2.append(new File(file2 + InternalZipConstants.ZIP_FILE_SEPARATOR + str5).isDirectory());
                                    printStream2.println(sb2.toString());
                                }
                                i5++;
                                list = strArr;
                                length4 = i6;
                                str4 = str2;
                            }
                        }
                    }
                    i4++;
                    list = list;
                    length4 = length4;
                    str4 = str4;
                }
            }
        } else {
            File r02 = C0815.m5256(new File(str3));
            C0815.m5230(r02.getAbsolutePath(), InternalZipConstants.WRITE_MODE);
            if (r02.exists()) {
                String[] list3 = r02.list();
                int length6 = list3.length;
                int i7 = 0;
                while (i7 < length6) {
                    String str7 = list3[i7];
                    System.out.println("LuckyPatcher: file found in data dir - " + r02 + InternalZipConstants.ZIP_FILE_SEPARATOR + str7);
                    if (new File(r02 + InternalZipConstants.ZIP_FILE_SEPARATOR + str7).isDirectory()) {
                        if ((r02 + InternalZipConstants.ZIP_FILE_SEPARATOR + str7).equals(r02 + "/lib")) {
                            String[] list4 = new File(r02 + InternalZipConstants.ZIP_FILE_SEPARATOR + str7).list();
                            int length7 = list4.length;
                            int i8 = 0;
                            while (i8 < length7) {
                                String str8 = list4[i8];
                                String[] strArr2 = list3;
                                if (new File(r02 + InternalZipConstants.ZIP_FILE_SEPARATOR + str7 + InternalZipConstants.ZIP_FILE_SEPARATOR + str8).isFile() && !str8.contains("libjnigraphics.so")) {
                                    arrayList.add(r02 + InternalZipConstants.ZIP_FILE_SEPARATOR + str7 + InternalZipConstants.ZIP_FILE_SEPARATOR + str8);
                                    System.out.println("LuckyPatcher: found lib - " + r02 + InternalZipConstants.ZIP_FILE_SEPARATOR + str7 + InternalZipConstants.ZIP_FILE_SEPARATOR + str8);
                                }
                                i8++;
                                list3 = strArr2;
                            }
                        }
                    }
                    i7++;
                    list3 = list3;
                }
            }
        }
        return arrayList;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static void m6257(File file, String str) {
        if (C0987.f4489 >= 21) {
            System.out.println("remove libs in data for api 21");
            File r14 = C0815.m5256(new File(str));
            if (C0815.m5256(new File(str)).exists()) {
                for (File file2 : r14.listFiles()) {
                    System.out.println("LuckyPatcher: file found in data dir - " + r14 + InternalZipConstants.ZIP_FILE_SEPARATOR + file2);
                    if (file2.isDirectory() && file2.getAbsolutePath().endsWith("/lib")) {
                        try {
                            new C0815(BuildConfig.FLAVOR).m5347(file2);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else if (!str.startsWith("/mnt/")) {
            System.out.println("remove libs in data");
            if (file.exists()) {
                for (String str2 : file.list()) {
                    System.out.println("LuckyPatcher: file found in data dir - " + file + InternalZipConstants.ZIP_FILE_SEPARATOR + str2);
                    if (new File(file + InternalZipConstants.ZIP_FILE_SEPARATOR + str2).isDirectory()) {
                        if ((file + InternalZipConstants.ZIP_FILE_SEPARATOR + str2).equals(file + "/lib")) {
                            String[] list = new File(file + InternalZipConstants.ZIP_FILE_SEPARATOR + str2).list();
                            if (list != null && list.length > 0) {
                                for (String str3 : list) {
                                    if (new File(file + InternalZipConstants.ZIP_FILE_SEPARATOR + str2 + InternalZipConstants.ZIP_FILE_SEPARATOR + str3).isFile()) {
                                        new File(file + InternalZipConstants.ZIP_FILE_SEPARATOR + str2 + InternalZipConstants.ZIP_FILE_SEPARATOR + str3).delete();
                                        System.out.println("LuckyPatcher: remove lib - " + file + InternalZipConstants.ZIP_FILE_SEPARATOR + str2 + InternalZipConstants.ZIP_FILE_SEPARATOR + str3);
                                    }
                                }
                            }
                            System.out.println("delete dir" + file + InternalZipConstants.ZIP_FILE_SEPARATOR + str2);
                            StringBuilder sb = new StringBuilder();
                            sb.append(file);
                            sb.append(InternalZipConstants.ZIP_FILE_SEPARATOR);
                            sb.append(str2);
                            new File(sb.toString()).delete();
                        }
                    }
                    System.out.println("file not dir lib" + file + InternalZipConstants.ZIP_FILE_SEPARATOR + str2 + " is File:" + new File(file + InternalZipConstants.ZIP_FILE_SEPARATOR + str2).isFile());
                    if ((file + InternalZipConstants.ZIP_FILE_SEPARATOR + str2).equals(file + "/lib")) {
                        System.out.println("delete file" + file + InternalZipConstants.ZIP_FILE_SEPARATOR + str2);
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(file);
                        sb2.append(InternalZipConstants.ZIP_FILE_SEPARATOR);
                        sb2.append(str2);
                        new File(sb2.toString()).delete();
                    }
                }
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x001f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0041 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0063 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0085 */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m6256(java.lang.String r4) {
        /*
            java.lang.String r0 = "\n"
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x001f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x001f }
            r2.<init>()     // Catch:{ Exception -> 0x001f }
            r2.append(r4)     // Catch:{ Exception -> 0x001f }
            r2.append(r0)     // Catch:{ Exception -> 0x001f }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x001f }
            java.lang.Process r1 = r1.exec(r2)     // Catch:{ Exception -> 0x001f }
            r1.waitFor()     // Catch:{ Exception -> 0x001f }
            r1.destroy()     // Catch:{ Exception -> 0x001f }
        L_0x001f:
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x0041 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0041 }
            r2.<init>()     // Catch:{ Exception -> 0x0041 }
            java.lang.String r3 = "toolbox "
            r2.append(r3)     // Catch:{ Exception -> 0x0041 }
            r2.append(r4)     // Catch:{ Exception -> 0x0041 }
            r2.append(r0)     // Catch:{ Exception -> 0x0041 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0041 }
            java.lang.Process r1 = r1.exec(r2)     // Catch:{ Exception -> 0x0041 }
            r1.waitFor()     // Catch:{ Exception -> 0x0041 }
            r1.destroy()     // Catch:{ Exception -> 0x0041 }
        L_0x0041:
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x0063 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0063 }
            r2.<init>()     // Catch:{ Exception -> 0x0063 }
            java.lang.String r3 = "/system/bin/failsafe/toolbox "
            r2.append(r3)     // Catch:{ Exception -> 0x0063 }
            r2.append(r4)     // Catch:{ Exception -> 0x0063 }
            r2.append(r0)     // Catch:{ Exception -> 0x0063 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0063 }
            java.lang.Process r1 = r1.exec(r2)     // Catch:{ Exception -> 0x0063 }
            r1.waitFor()     // Catch:{ Exception -> 0x0063 }
            r1.destroy()     // Catch:{ Exception -> 0x0063 }
        L_0x0063:
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x0085 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0085 }
            r2.<init>()     // Catch:{ Exception -> 0x0085 }
            java.lang.String r3 = "busybox "
            r2.append(r3)     // Catch:{ Exception -> 0x0085 }
            r2.append(r4)     // Catch:{ Exception -> 0x0085 }
            r2.append(r0)     // Catch:{ Exception -> 0x0085 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0085 }
            java.lang.Process r1 = r1.exec(r2)     // Catch:{ Exception -> 0x0085 }
            r1.waitFor()     // Catch:{ Exception -> 0x0085 }
            r1.destroy()     // Catch:{ Exception -> 0x0085 }
        L_0x0085:
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x00ac }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ac }
            r2.<init>()     // Catch:{ Exception -> 0x00ac }
            java.lang.String r3 = com.lp.C0987.f4437     // Catch:{ Exception -> 0x00ac }
            r2.append(r3)     // Catch:{ Exception -> 0x00ac }
            java.lang.String r3 = "/busybox "
            r2.append(r3)     // Catch:{ Exception -> 0x00ac }
            r2.append(r4)     // Catch:{ Exception -> 0x00ac }
            r2.append(r0)     // Catch:{ Exception -> 0x00ac }
            java.lang.String r4 = r2.toString()     // Catch:{ Exception -> 0x00ac }
            java.lang.Process r4 = r1.exec(r4)     // Catch:{ Exception -> 0x00ac }
            r4.waitFor()     // Catch:{ Exception -> 0x00ac }
            r4.destroy()     // Catch:{ Exception -> 0x00ac }
        L_0x00ac:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: javaroot.utils.copyLibsFilesToSystemBackup.m6256(java.lang.String):void");
    }
}
