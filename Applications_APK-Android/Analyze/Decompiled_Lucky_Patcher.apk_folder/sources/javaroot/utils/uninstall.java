package javaroot.utils;

import com.chelpus.C0815;
import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;

public class uninstall {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String f6286 = "/data/app/";

    /* renamed from: ʼ  reason: contains not printable characters */
    public static String f6287 = "/data/data/";

    /* renamed from: ʽ  reason: contains not printable characters */
    public static boolean f6288 = false;

    /* renamed from: ʾ  reason: contains not printable characters */
    public static boolean f6289 = false;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chelpus.ˆ.ʼ(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.chelpus.ˆ.ʼ(int, int):int
      com.chelpus.ˆ.ʼ(java.io.File, java.lang.String):java.lang.String
      com.chelpus.ˆ.ʼ(java.io.File, java.util.ArrayList<java.io.File>):void
      com.chelpus.ˆ.ʼ(java.lang.String, java.lang.String):void
      com.chelpus.ˆ.ʼ(boolean, boolean):void
      com.chelpus.ˆ.ʼ(java.io.File, java.io.File):boolean
      com.chelpus.ˆ.ʼ(java.io.File, int):byte[]
      com.chelpus.ˆ.ʼ(java.lang.String, boolean):java.lang.String */
    public static void main(String[] strArr) {
        try {
            C0815.m5165(new Object() {
            });
            f6286 = strArr[1];
            f6288 = true;
            f6287 = strArr[2];
            File file = new File(f6286);
            File file2 = new File(C0815.m5208(f6286, true));
            System.out.println("Start getLibs!");
            ArrayList<String> r6 = m6289(file);
            System.out.println("Start delete lib!");
            if (!r6.isEmpty()) {
                Iterator<String> it = r6.iterator();
                while (it.hasNext()) {
                    File file3 = new File("/system/lib/" + it.next());
                    if (file3.exists()) {
                        file3.delete();
                    }
                }
            }
            System.out.println("Start delete data directory!");
            C0815 r62 = new C0815("uninstall system");
            try {
                System.out.println("Start delete dir!");
                r62.m5347(new File(f6287));
                r62.m5347(new File(f6287.replace("/data/data/", "/dbdata/databases/")));
            } catch (Exception e) {
                PrintStream printStream = System.out;
                printStream.println("LuckyPatcher Error uninstall: " + e);
                e.printStackTrace();
            }
            System.out.println("Start delete dc!");
            try {
                File r1 = C0815.m5318(f6286);
                if (r1 != null) {
                    r1.delete();
                    PrintStream printStream2 = System.out;
                    printStream2.println("Dalvik-cache " + r1 + " deleted.");
                } else {
                    System.out.println("dalvik-cache not found.");
                }
                System.out.println("Start delete odex.");
                if (file2.exists()) {
                    file2.delete();
                }
            } catch (Exception e2) {
                PrintStream printStream3 = System.out;
                printStream3.println("Error: Exception e" + e2.toString());
                e2.printStackTrace();
            }
            File r12 = C0815.m5256(new File(f6286));
            PrintStream printStream4 = System.out;
            printStream4.println("LuckyPatcher: dir for apk " + r12);
            if (r12.getAbsolutePath().startsWith("/system") && !r12.getName().toLowerCase().equals("app") && !r12.getName().toLowerCase().equals("priv-app")) {
                System.out.println("Start delete apk dir!");
                r62.m5347(r12);
            }
            System.out.println("Start delete apk!");
            File file4 = new File(f6286);
            if (file4.exists()) {
                file4.delete();
                PrintStream printStream5 = System.out;
                printStream5.println("Delete apk:" + f6286);
            }
        } catch (Exception e3) {
            PrintStream printStream6 = System.out;
            printStream6.println("LuckyPatcher Error uninstall: " + e3);
            e3.printStackTrace();
        }
        C0815.m5312();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0089, code lost:
        r6 = r9.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0099, code lost:
        if (r6.getFileName().endsWith(".so") != false) goto L_0x009b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x009b, code lost:
        java.lang.System.out.println(r6.getFileName());
        r6 = r6.getFileName().split(net.lingala.zip4j.util.InternalZipConstants.ZIP_FILE_SEPARATOR);
        r6 = r6[r6.length - 1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00c3, code lost:
        r3.add(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00d8, code lost:
        r3.add(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00ea, code lost:
        if (r5 != null) goto L_0x0063;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x0063 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x0076 */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0089 A[Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc, all -> 0x006e }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00e7 A[SYNTHETIC, Splitter:B:63:0x00e7] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00f1 A[SYNTHETIC, Splitter:B:70:0x00f1] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x00f6 A[SYNTHETIC, Splitter:B:74:0x00f6] */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList<java.lang.String> m6289(java.io.File r9) {
        /*
            java.lang.String r0 = "/"
            java.lang.String r1 = "libjnigraphics.so"
            java.lang.String r2 = ""
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r4 = 0
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0075, all -> 0x0071 }
            r5.<init>(r9)     // Catch:{ IOException -> 0x0075, all -> 0x0071 }
            java.util.zip.ZipInputStream r6 = new java.util.zip.ZipInputStream     // Catch:{ IOException -> 0x0076 }
            r6.<init>(r5)     // Catch:{ IOException -> 0x0076 }
        L_0x0016:
            java.util.zip.ZipEntry r4 = r6.getNextEntry()     // Catch:{ IOException -> 0x006c, all -> 0x0068 }
            if (r4 == 0) goto L_0x0060
            java.lang.String r7 = r4.getName()     // Catch:{ IOException -> 0x006c, all -> 0x0068 }
            java.lang.String r8 = "lib/"
            boolean r7 = r7.startsWith(r8)     // Catch:{ IOException -> 0x006c, all -> 0x0068 }
            if (r7 == 0) goto L_0x0016
            java.lang.String r4 = r4.getName()     // Catch:{ IOException -> 0x006c, all -> 0x0068 }
            java.lang.String[] r4 = r4.split(r0)     // Catch:{ IOException -> 0x006c, all -> 0x0068 }
            int r7 = r4.length     // Catch:{ IOException -> 0x006c, all -> 0x0068 }
            int r7 = r7 + -1
            r4 = r4[r7]     // Catch:{ IOException -> 0x006c, all -> 0x0068 }
            boolean r7 = r3.isEmpty()     // Catch:{ IOException -> 0x006c, all -> 0x0068 }
            if (r7 == 0) goto L_0x004a
            boolean r7 = r4.equals(r2)     // Catch:{ IOException -> 0x006c, all -> 0x0068 }
            if (r7 != 0) goto L_0x004a
            boolean r7 = r4.contains(r1)     // Catch:{ IOException -> 0x006c, all -> 0x0068 }
            if (r7 != 0) goto L_0x004a
            r3.add(r4)     // Catch:{ IOException -> 0x006c, all -> 0x0068 }
        L_0x004a:
            boolean r7 = r3.contains(r4)     // Catch:{ IOException -> 0x006c, all -> 0x0068 }
            if (r7 != 0) goto L_0x0016
            boolean r7 = r4.equals(r2)     // Catch:{ IOException -> 0x006c, all -> 0x0068 }
            if (r7 != 0) goto L_0x0016
            boolean r7 = r4.contains(r1)     // Catch:{ IOException -> 0x006c, all -> 0x0068 }
            if (r7 != 0) goto L_0x0016
            r3.add(r4)     // Catch:{ IOException -> 0x006c, all -> 0x0068 }
            goto L_0x0016
        L_0x0060:
            r6.close()     // Catch:{ IOException -> 0x0063 }
        L_0x0063:
            r5.close()     // Catch:{ IOException -> 0x00ee }
            goto L_0x00ee
        L_0x0068:
            r9 = move-exception
            r4 = r6
            goto L_0x00ef
        L_0x006c:
            r4 = r6
            goto L_0x0076
        L_0x006e:
            r9 = move-exception
            goto L_0x00ef
        L_0x0071:
            r9 = move-exception
            r5 = r4
            goto L_0x00ef
        L_0x0075:
            r5 = r4
        L_0x0076:
            net.lingala.zip4j.core.ZipFile r6 = new net.lingala.zip4j.core.ZipFile     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            r6.<init>(r9)     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            java.util.List r9 = r6.getFileHeaders()     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            java.util.Iterator r9 = r9.iterator()     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
        L_0x0083:
            boolean r6 = r9.hasNext()     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            if (r6 == 0) goto L_0x00e5
            java.lang.Object r6 = r9.next()     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            net.lingala.zip4j.model.FileHeader r6 = (net.lingala.zip4j.model.FileHeader) r6     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            java.lang.String r7 = r6.getFileName()     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            java.lang.String r8 = ".so"
            boolean r7 = r7.endsWith(r8)     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            if (r7 == 0) goto L_0x0083
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            java.lang.String r8 = r6.getFileName()     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            r7.println(r8)     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            java.lang.String r6 = r6.getFileName()     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            java.lang.String[] r6 = r6.split(r0)     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            int r7 = r6.length     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            int r7 = r7 + -1
            r6 = r6[r7]     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            boolean r7 = r3.isEmpty()     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            if (r7 == 0) goto L_0x00c6
            boolean r7 = r6.equals(r2)     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            if (r7 != 0) goto L_0x00c6
            boolean r7 = r6.contains(r1)     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            if (r7 != 0) goto L_0x00c6
            r3.add(r6)     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
        L_0x00c6:
            boolean r7 = r3.contains(r6)     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            if (r7 != 0) goto L_0x0083
            boolean r7 = r6.equals(r2)     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            if (r7 != 0) goto L_0x0083
            boolean r7 = r6.contains(r1)     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            if (r7 != 0) goto L_0x0083
            r3.add(r6)     // Catch:{ ZipException -> 0x00e1, Exception -> 0x00dc }
            goto L_0x0083
        L_0x00dc:
            r9 = move-exception
            r9.printStackTrace()     // Catch:{ all -> 0x006e }
            goto L_0x00e5
        L_0x00e1:
            r9 = move-exception
            r9.printStackTrace()     // Catch:{ all -> 0x006e }
        L_0x00e5:
            if (r4 == 0) goto L_0x00ea
            r4.close()     // Catch:{ IOException -> 0x00ea }
        L_0x00ea:
            if (r5 == 0) goto L_0x00ee
            goto L_0x0063
        L_0x00ee:
            return r3
        L_0x00ef:
            if (r4 == 0) goto L_0x00f4
            r4.close()     // Catch:{ IOException -> 0x00f4 }
        L_0x00f4:
            if (r5 == 0) goto L_0x00f9
            r5.close()     // Catch:{ IOException -> 0x00f9 }
        L_0x00f9:
            goto L_0x00fb
        L_0x00fa:
            throw r9
        L_0x00fb:
            goto L_0x00fa
        */
        throw new UnsupportedOperationException("Method not decompiled: javaroot.utils.uninstall.m6289(java.io.File):java.util.ArrayList");
    }
}
