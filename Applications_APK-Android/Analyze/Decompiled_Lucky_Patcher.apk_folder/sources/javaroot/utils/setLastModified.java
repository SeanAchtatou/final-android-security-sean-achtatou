package javaroot.utils;

import com.chelpus.C0815;
import java.io.File;
import java.io.FileInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import net.lingala.zip4j.util.InternalZipConstants;

public class setLastModified {
    public static void main(String[] strArr) {
        C0815.m5165(new Object() {
        });
        File file = new File(strArr[0]);
        String str = strArr[1];
        System.out.println(file);
        System.out.println(str);
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            ZipInputStream zipInputStream = new ZipInputStream(fileInputStream);
            while (true) {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry == null) {
                    break;
                }
                new File(str + InternalZipConstants.ZIP_FILE_SEPARATOR + nextEntry.getName()).setLastModified(nextEntry.getTime());
            }
            zipInputStream.close();
            fileInputStream.close();
        } catch (Throwable th) {
            th.printStackTrace();
        }
        C0815.m5312();
    }
}
