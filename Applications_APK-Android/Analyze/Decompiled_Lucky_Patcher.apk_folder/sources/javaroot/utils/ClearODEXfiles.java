package javaroot.utils;

public class ClearODEXfiles {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chelpus.ˆ.ʼ(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.chelpus.ˆ.ʼ(int, int):int
      com.chelpus.ˆ.ʼ(java.io.File, java.lang.String):java.lang.String
      com.chelpus.ˆ.ʼ(java.io.File, java.util.ArrayList<java.io.File>):void
      com.chelpus.ˆ.ʼ(java.lang.String, java.lang.String):void
      com.chelpus.ˆ.ʼ(boolean, boolean):void
      com.chelpus.ˆ.ʼ(java.io.File, java.io.File):boolean
      com.chelpus.ˆ.ʼ(java.io.File, int):byte[]
      com.chelpus.ˆ.ʼ(java.lang.String, boolean):java.lang.String */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:100:0x01e5 */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x01f4 A[Catch:{ Exception -> 0x0224 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void main(java.lang.String[] r14) {
        /*
            java.lang.String r0 = ".odex"
            java.lang.String r1 = "rw"
            java.lang.String r2 = "/data/app"
            java.lang.String r3 = ".apk"
            r4 = 0
            r14 = r14[r4]
            javaroot.utils.ClearODEXfiles$1 r14 = new javaroot.utils.ClearODEXfiles$1
            r14.<init>()
            com.chelpus.C0815.m5165(r14)
            r14 = 21
            r5 = 1
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0096 }
            r6.<init>(r2)     // Catch:{ Exception -> 0x0096 }
            boolean r6 = r6.exists()     // Catch:{ Exception -> 0x0096 }
            if (r6 == 0) goto L_0x0096
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0096 }
            r6.<init>(r2)     // Catch:{ Exception -> 0x0096 }
            java.io.File[] r2 = r6.listFiles()     // Catch:{ Exception -> 0x0096 }
            int r6 = r2.length     // Catch:{ Exception -> 0x0096 }
            r7 = 0
        L_0x002c:
            if (r7 >= r6) goto L_0x0096
            r8 = r2[r7]     // Catch:{ Exception -> 0x0096 }
            int r9 = com.lp.C0987.f4489     // Catch:{ Exception -> 0x0096 }
            if (r9 < r14) goto L_0x0086
            boolean r9 = r8.isDirectory()     // Catch:{ Exception -> 0x0096 }
            if (r9 == 0) goto L_0x0093
            java.io.File[] r8 = r8.listFiles()     // Catch:{ Exception -> 0x0096 }
            if (r8 == 0) goto L_0x0093
            int r9 = r8.length     // Catch:{ Exception -> 0x0096 }
            if (r9 <= 0) goto L_0x0093
            int r9 = r8.length     // Catch:{ Exception -> 0x0096 }
            r10 = 0
        L_0x0045:
            if (r10 >= r9) goto L_0x0093
            r11 = r8[r10]     // Catch:{ Exception -> 0x0096 }
            boolean r12 = r11.isFile()     // Catch:{ Exception -> 0x0096 }
            if (r12 == 0) goto L_0x0083
            java.lang.String r12 = r11.getAbsolutePath()     // Catch:{ Exception -> 0x0096 }
            boolean r12 = r12.endsWith(r3)     // Catch:{ Exception -> 0x0096 }
            if (r12 == 0) goto L_0x0083
            java.io.File r12 = new java.io.File     // Catch:{ Exception -> 0x0096 }
            java.lang.String r13 = r11.getAbsolutePath()     // Catch:{ Exception -> 0x0096 }
            java.lang.String r13 = com.chelpus.C0815.m5208(r13, r5)     // Catch:{ Exception -> 0x0096 }
            r12.<init>(r13)     // Catch:{ Exception -> 0x0096 }
            boolean r13 = r12.exists()     // Catch:{ Exception -> 0x0096 }
            if (r13 == 0) goto L_0x0083
            java.io.File r13 = r11.getAbsoluteFile()     // Catch:{ Exception -> 0x0096 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0096 }
            boolean r13 = r13.endsWith(r3)     // Catch:{ Exception -> 0x0096 }
            if (r13 == 0) goto L_0x0083
            boolean r11 = com.chelpus.C0815.m5277(r11)     // Catch:{ Exception -> 0x0096 }
            if (r11 == 0) goto L_0x0083
            r12.delete()     // Catch:{ Exception -> 0x0096 }
        L_0x0083:
            int r10 = r10 + 1
            goto L_0x0045
        L_0x0086:
            java.lang.String r9 = r8.getAbsolutePath()     // Catch:{ Exception -> 0x0096 }
            boolean r9 = r9.endsWith(r0)     // Catch:{ Exception -> 0x0096 }
            if (r9 == 0) goto L_0x0093
            r8.delete()     // Catch:{ Exception -> 0x0096 }
        L_0x0093:
            int r7 = r7 + 1
            goto L_0x002c
        L_0x0096:
            java.io.PrintStream r2 = java.lang.System.out
            java.lang.String r6 = "LuckyPatcher: Dalvik-Cache deleted."
            r2.println(r6)
            java.lang.String r2 = "/system"
            com.chelpus.C0815.m5230(r2, r1)     // Catch:{ Exception -> 0x01e5 }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r6 = "/system/app"
            r2.<init>(r6)     // Catch:{ Exception -> 0x01e5 }
            java.io.File[] r2 = r2.listFiles()     // Catch:{ Exception -> 0x01e5 }
            int r6 = r2.length     // Catch:{ Exception -> 0x01e5 }
            r7 = 0
        L_0x00af:
            if (r7 >= r6) goto L_0x0140
            r8 = r2[r7]     // Catch:{ Exception -> 0x01e5 }
            int r9 = com.lp.C0987.f4489     // Catch:{ Exception -> 0x01e5 }
            if (r9 < r14) goto L_0x0109
            boolean r9 = r8.isDirectory()     // Catch:{ Exception -> 0x01e5 }
            if (r9 == 0) goto L_0x013c
            java.io.File[] r8 = r8.listFiles()     // Catch:{ Exception -> 0x01e5 }
            if (r8 == 0) goto L_0x013c
            int r9 = r8.length     // Catch:{ Exception -> 0x01e5 }
            if (r9 <= 0) goto L_0x013c
            int r9 = r8.length     // Catch:{ Exception -> 0x01e5 }
            r10 = 0
        L_0x00c8:
            if (r10 >= r9) goto L_0x013c
            r11 = r8[r10]     // Catch:{ Exception -> 0x01e5 }
            boolean r12 = r11.isFile()     // Catch:{ Exception -> 0x01e5 }
            if (r12 == 0) goto L_0x0106
            java.lang.String r12 = r11.getAbsolutePath()     // Catch:{ Exception -> 0x01e5 }
            boolean r12 = r12.endsWith(r3)     // Catch:{ Exception -> 0x01e5 }
            if (r12 == 0) goto L_0x0106
            java.io.File r12 = new java.io.File     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r13 = r11.getAbsolutePath()     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r13 = com.chelpus.C0815.m5208(r13, r5)     // Catch:{ Exception -> 0x01e5 }
            r12.<init>(r13)     // Catch:{ Exception -> 0x01e5 }
            boolean r13 = r12.exists()     // Catch:{ Exception -> 0x01e5 }
            if (r13 == 0) goto L_0x0106
            java.io.File r13 = r11.getAbsoluteFile()     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x01e5 }
            boolean r13 = r13.endsWith(r3)     // Catch:{ Exception -> 0x01e5 }
            if (r13 == 0) goto L_0x0106
            boolean r11 = com.chelpus.C0815.m5277(r11)     // Catch:{ Exception -> 0x01e5 }
            if (r11 == 0) goto L_0x0106
            r12.delete()     // Catch:{ Exception -> 0x01e5 }
        L_0x0106:
            int r10 = r10 + 1
            goto L_0x00c8
        L_0x0109:
            java.io.File r9 = new java.io.File     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r10 = r8.getAbsolutePath()     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r10 = com.chelpus.C0815.m5208(r10, r5)     // Catch:{ Exception -> 0x01e5 }
            r9.<init>(r10)     // Catch:{ Exception -> 0x01e5 }
            java.io.PrintStream r10 = java.lang.System.out     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r11 = r9.getAbsolutePath()     // Catch:{ Exception -> 0x01e5 }
            r10.println(r11)     // Catch:{ Exception -> 0x01e5 }
            boolean r10 = r9.exists()     // Catch:{ Exception -> 0x01e5 }
            if (r10 == 0) goto L_0x013c
            java.io.File r10 = r8.getAbsoluteFile()     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x01e5 }
            boolean r10 = r10.endsWith(r3)     // Catch:{ Exception -> 0x01e5 }
            if (r10 == 0) goto L_0x013c
            boolean r8 = com.chelpus.C0815.m5277(r8)     // Catch:{ Exception -> 0x01e5 }
            if (r8 == 0) goto L_0x013c
            r9.delete()     // Catch:{ Exception -> 0x01e5 }
        L_0x013c:
            int r7 = r7 + 1
            goto L_0x00af
        L_0x0140:
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r6 = "/system/priv-app"
            r2.<init>(r6)     // Catch:{ Exception -> 0x01e5 }
            java.io.File[] r2 = r2.listFiles()     // Catch:{ Exception -> 0x01e5 }
            int r6 = r2.length     // Catch:{ Exception -> 0x01e5 }
            r7 = 0
        L_0x014d:
            if (r7 >= r6) goto L_0x01de
            r8 = r2[r7]     // Catch:{ Exception -> 0x01e5 }
            int r9 = com.lp.C0987.f4489     // Catch:{ Exception -> 0x01e5 }
            if (r9 < r14) goto L_0x01a7
            boolean r9 = r8.isDirectory()     // Catch:{ Exception -> 0x01e5 }
            if (r9 == 0) goto L_0x01da
            java.io.File[] r8 = r8.listFiles()     // Catch:{ Exception -> 0x01e5 }
            if (r8 == 0) goto L_0x01da
            int r9 = r8.length     // Catch:{ Exception -> 0x01e5 }
            if (r9 <= 0) goto L_0x01da
            int r9 = r8.length     // Catch:{ Exception -> 0x01e5 }
            r10 = 0
        L_0x0166:
            if (r10 >= r9) goto L_0x01da
            r11 = r8[r10]     // Catch:{ Exception -> 0x01e5 }
            boolean r12 = r11.isFile()     // Catch:{ Exception -> 0x01e5 }
            if (r12 == 0) goto L_0x01a4
            java.lang.String r12 = r11.getAbsolutePath()     // Catch:{ Exception -> 0x01e5 }
            boolean r12 = r12.endsWith(r3)     // Catch:{ Exception -> 0x01e5 }
            if (r12 == 0) goto L_0x01a4
            java.io.File r12 = new java.io.File     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r13 = r11.getAbsolutePath()     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r13 = com.chelpus.C0815.m5208(r13, r5)     // Catch:{ Exception -> 0x01e5 }
            r12.<init>(r13)     // Catch:{ Exception -> 0x01e5 }
            boolean r13 = r12.exists()     // Catch:{ Exception -> 0x01e5 }
            if (r13 == 0) goto L_0x01a4
            java.io.File r13 = r11.getAbsoluteFile()     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x01e5 }
            boolean r13 = r13.endsWith(r3)     // Catch:{ Exception -> 0x01e5 }
            if (r13 == 0) goto L_0x01a4
            boolean r11 = com.chelpus.C0815.m5277(r11)     // Catch:{ Exception -> 0x01e5 }
            if (r11 == 0) goto L_0x01a4
            r12.delete()     // Catch:{ Exception -> 0x01e5 }
        L_0x01a4:
            int r10 = r10 + 1
            goto L_0x0166
        L_0x01a7:
            java.io.File r9 = new java.io.File     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r10 = r8.getAbsolutePath()     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r10 = com.chelpus.C0815.m5208(r10, r5)     // Catch:{ Exception -> 0x01e5 }
            r9.<init>(r10)     // Catch:{ Exception -> 0x01e5 }
            java.io.PrintStream r10 = java.lang.System.out     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r11 = r9.getAbsolutePath()     // Catch:{ Exception -> 0x01e5 }
            r10.println(r11)     // Catch:{ Exception -> 0x01e5 }
            boolean r10 = r9.exists()     // Catch:{ Exception -> 0x01e5 }
            if (r10 == 0) goto L_0x01da
            java.io.File r10 = r8.getAbsoluteFile()     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x01e5 }
            boolean r10 = r10.endsWith(r3)     // Catch:{ Exception -> 0x01e5 }
            if (r10 == 0) goto L_0x01da
            boolean r8 = com.chelpus.C0815.m5277(r8)     // Catch:{ Exception -> 0x01e5 }
            if (r8 == 0) goto L_0x01da
            r9.delete()     // Catch:{ Exception -> 0x01e5 }
        L_0x01da:
            int r7 = r7 + 1
            goto L_0x014d
        L_0x01de:
            java.io.PrintStream r14 = java.lang.System.out     // Catch:{ Exception -> 0x01e5 }
            java.lang.String r2 = "LuckyPatcher: System apps deodex all."
            r14.println(r2)     // Catch:{ Exception -> 0x01e5 }
        L_0x01e5:
            java.io.File r14 = new java.io.File     // Catch:{ Exception -> 0x0224 }
            java.lang.String r2 = "/mnt/asec"
            r14.<init>(r2)     // Catch:{ Exception -> 0x0224 }
            java.io.File[] r14 = r14.listFiles()     // Catch:{ Exception -> 0x0224 }
            int r2 = r14.length     // Catch:{ Exception -> 0x0224 }
            r3 = 0
        L_0x01f2:
            if (r3 >= r2) goto L_0x0224
            r5 = r14[r3]     // Catch:{ Exception -> 0x0224 }
            boolean r6 = r5.isDirectory()     // Catch:{ Exception -> 0x0224 }
            if (r6 == 0) goto L_0x0221
            java.io.File[] r5 = r5.listFiles()     // Catch:{ Exception -> 0x0224 }
            int r6 = r5.length     // Catch:{ Exception -> 0x0224 }
            r7 = 0
        L_0x0202:
            if (r7 >= r6) goto L_0x0221
            r8 = r5[r7]     // Catch:{ Exception -> 0x0224 }
            java.io.File r9 = r8.getAbsoluteFile()     // Catch:{ Exception -> 0x0224 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0224 }
            boolean r9 = r9.endsWith(r0)     // Catch:{ Exception -> 0x0224 }
            if (r9 == 0) goto L_0x021e
            java.lang.String r9 = r8.getAbsolutePath()     // Catch:{ Exception -> 0x0224 }
            com.chelpus.C0815.m5230(r9, r1)     // Catch:{ Exception -> 0x0224 }
            r8.delete()     // Catch:{ Exception -> 0x0224 }
        L_0x021e:
            int r7 = r7 + 1
            goto L_0x0202
        L_0x0221:
            int r3 = r3 + 1
            goto L_0x01f2
        L_0x0224:
            com.chelpus.C0815.m5312()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: javaroot.utils.ClearODEXfiles.main(java.lang.String[]):void");
    }
}
