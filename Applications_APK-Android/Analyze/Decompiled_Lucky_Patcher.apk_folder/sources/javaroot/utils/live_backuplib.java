package javaroot.utils;

import com.chelpus.C0815;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

public class live_backuplib {
    public static void main(String[] strArr) {
        C0815.m5165(new Object() {
        });
        String str = strArr[0];
        try {
            File file = new File(str);
            File file2 = new File(str.replace("/data/data/", "/mnt/asec/"));
            if (!file.exists()) {
                file = file2;
            }
            if (!file.exists()) {
                file = new File(str + "-1");
            }
            if (!file.exists()) {
                file = new File(str.replace("-1", "-2"));
            }
            if (file.exists()) {
                C0815.m5160(file, new File(file.getAbsolutePath() + ".backup"));
                System.out.println("Backup - done!");
                C0815.m5312();
                return;
            }
            throw new FileNotFoundException();
        } catch (FileNotFoundException unused) {
            System.out.println("Error: Backup failed!\n\nMove Program to internal storage.");
        } catch (Exception e) {
            PrintStream printStream = System.out;
            printStream.println("Exception e" + e.toString());
        }
    }
}
