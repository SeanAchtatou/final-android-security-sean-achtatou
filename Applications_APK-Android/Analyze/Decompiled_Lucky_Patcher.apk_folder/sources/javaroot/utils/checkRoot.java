package javaroot.utils;

public class checkRoot {
    /* JADX WARNING: Removed duplicated region for block: B:114:0x02b7  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x02bf  */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x036f  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x03bf  */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x040f  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x0446  */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x0454 A[Catch:{ Exception -> 0x04b4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x04b2  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x04d2  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0185  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void main(java.lang.String[] r20) {
        /*
            java.lang.String r1 = "permissive"
            java.lang.String r0 = "enablemountnamespaceseparation=1"
            javaroot.utils.checkRoot$1 r2 = new javaroot.utils.checkRoot$1
            r2.<init>()
            com.chelpus.C0815.m5165(r2)
            r2 = 0
            r3 = r20[r2]
            java.lang.String r4 = ""
            if (r3 == 0) goto L_0x0016
            r3 = r20[r2]
            goto L_0x0017
        L_0x0016:
            r3 = r4
        L_0x0017:
            r5 = 1
            r6 = r20[r5]
            if (r6 == 0) goto L_0x001f
            r6 = r20[r5]
            goto L_0x0020
        L_0x001f:
            r6 = r4
        L_0x0020:
            r7 = 2
            r8 = r20[r7]
            if (r8 == 0) goto L_0x0028
            r8 = r20[r7]
            goto L_0x0029
        L_0x0028:
            r8 = r4
        L_0x0029:
            r9 = 3
            r10 = r20[r9]
            if (r10 == 0) goto L_0x0031
            r10 = r20[r9]
            goto L_0x0032
        L_0x0031:
            r10 = r4
        L_0x0032:
            r11 = 4
            r12 = r20[r11]
            if (r12 == 0) goto L_0x0043
            r11 = r20[r11]
            java.lang.String r12 = "set_permissive"
            boolean r11 = r11.contains(r12)
            if (r11 == 0) goto L_0x0043
            r11 = 1
            goto L_0x0044
        L_0x0043:
            r11 = 0
        L_0x0044:
            java.lang.String r12 = "none"
            boolean r12 = r10.equals(r12)
            if (r12 != 0) goto L_0x00ec
            java.lang.Integer r12 = java.lang.Integer.valueOf(r6)
            int r12 = r12.intValue()
            r13 = 14
            if (r12 < r13) goto L_0x00ec
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e8 }
            r12.<init>()     // Catch:{ Exception -> 0x00e8 }
            r12.append(r10)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r13 = "/files/supersu.cfg"
            r12.append(r13)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x00e8 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e8 }
            r13.<init>()     // Catch:{ Exception -> 0x00e8 }
            r13.append(r10)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r10 = "/shared_prefs/eu.chainfire.supersu_preferences.xml"
            r13.append(r10)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r10 = r13.toString()     // Catch:{ Exception -> 0x00e8 }
            java.io.File r13 = new java.io.File     // Catch:{ Exception -> 0x00e8 }
            r13.<init>(r12)     // Catch:{ Exception -> 0x00e8 }
            boolean r13 = r13.exists()     // Catch:{ Exception -> 0x00e8 }
            if (r13 == 0) goto L_0x00ec
            java.io.File r13 = new java.io.File     // Catch:{ Exception -> 0x00e8 }
            r13.<init>(r12)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r13 = com.chelpus.C0815.m5251(r13)     // Catch:{ Exception -> 0x00e8 }
            boolean r13 = r13.contains(r0)     // Catch:{ Exception -> 0x00e8 }
            if (r13 == 0) goto L_0x00ec
            java.io.File r13 = new java.io.File     // Catch:{ Exception -> 0x00e8 }
            r13.<init>(r10)     // Catch:{ Exception -> 0x00e8 }
            boolean r13 = r13.exists()     // Catch:{ Exception -> 0x00e8 }
            if (r13 == 0) goto L_0x00d0
            java.io.File r13 = new java.io.File     // Catch:{ Exception -> 0x00e8 }
            r13.<init>(r10)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r10 = com.chelpus.C0815.m5251(r13)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r13 = "<boolean name=\"config_default_enablemountnamespaceseparation\" value=\"true\" />"
            boolean r10 = r10.contains(r13)     // Catch:{ Exception -> 0x00e8 }
            if (r10 == 0) goto L_0x00b8
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r10 = "Mount name space separation in SuperSu detected."
            r0.println(r10)     // Catch:{ Exception -> 0x00e8 }
            goto L_0x00ec
        L_0x00b8:
            java.io.File r10 = new java.io.File     // Catch:{ Exception -> 0x00e8 }
            r10.<init>(r12)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r10 = com.chelpus.C0815.m5251(r10)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r13 = "enablemountnamespaceseparation=0"
            java.lang.String r0 = r10.replace(r0, r13)     // Catch:{ Exception -> 0x00e8 }
            java.io.File r10 = new java.io.File     // Catch:{ Exception -> 0x00e8 }
            r10.<init>(r12)     // Catch:{ Exception -> 0x00e8 }
            com.chelpus.C0815.m5278(r10, r0)     // Catch:{ Exception -> 0x00e8 }
            goto L_0x00ec
        L_0x00d0:
            java.io.File r10 = new java.io.File     // Catch:{ Exception -> 0x00e8 }
            r10.<init>(r12)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r10 = com.chelpus.C0815.m5251(r10)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r13 = "enablemountnamespaceseparation=0"
            java.lang.String r0 = r10.replace(r0, r13)     // Catch:{ Exception -> 0x00e8 }
            java.io.File r10 = new java.io.File     // Catch:{ Exception -> 0x00e8 }
            r10.<init>(r12)     // Catch:{ Exception -> 0x00e8 }
            com.chelpus.C0815.m5278(r10, r0)     // Catch:{ Exception -> 0x00e8 }
            goto L_0x00ec
        L_0x00e8:
            r0 = move-exception
            r0.printStackTrace()
        L_0x00ec:
            com.lp.C0987.f4437 = r3
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.String r10 = "root found!"
            r0.println(r10)
            boolean r10 = com.chelpus.C0815.m5228()     // Catch:{ Exception -> 0x017d }
            java.lang.ProcessBuilder r0 = new java.lang.ProcessBuilder     // Catch:{ IOException | InterruptedException -> 0x0148, Throwable -> 0x013d }
            java.lang.String[] r12 = new java.lang.String[r2]     // Catch:{ IOException | InterruptedException -> 0x0148, Throwable -> 0x013d }
            r0.<init>(r12)     // Catch:{ IOException | InterruptedException -> 0x0148, Throwable -> 0x013d }
            java.lang.String[] r12 = new java.lang.String[r5]     // Catch:{ IOException | InterruptedException -> 0x0148, Throwable -> 0x013d }
            java.lang.String r13 = "getenforce"
            r12[r2] = r13     // Catch:{ IOException | InterruptedException -> 0x0148, Throwable -> 0x013d }
            r0.command(r12)     // Catch:{ IOException | InterruptedException -> 0x0148, Throwable -> 0x013d }
            java.lang.Process r0 = r0.start()     // Catch:{ IOException | InterruptedException -> 0x0148, Throwable -> 0x013d }
            java.io.BufferedReader r12 = new java.io.BufferedReader     // Catch:{ IOException | InterruptedException -> 0x0148, Throwable -> 0x013d }
            java.io.InputStreamReader r13 = new java.io.InputStreamReader     // Catch:{ IOException | InterruptedException -> 0x0148, Throwable -> 0x013d }
            java.io.InputStream r14 = r0.getInputStream()     // Catch:{ IOException | InterruptedException -> 0x0148, Throwable -> 0x013d }
            r13.<init>(r14)     // Catch:{ IOException | InterruptedException -> 0x0148, Throwable -> 0x013d }
            r12.<init>(r13)     // Catch:{ IOException | InterruptedException -> 0x0148, Throwable -> 0x013d }
            r13 = r4
        L_0x011c:
            java.lang.String r14 = r12.readLine()     // Catch:{ IOException | InterruptedException -> 0x0149, Throwable -> 0x013b }
            if (r14 == 0) goto L_0x0137
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ IOException | InterruptedException -> 0x0149, Throwable -> 0x013b }
            r15.<init>()     // Catch:{ IOException | InterruptedException -> 0x0149, Throwable -> 0x013b }
            r15.append(r13)     // Catch:{ IOException | InterruptedException -> 0x0149, Throwable -> 0x013b }
            r15.append(r14)     // Catch:{ IOException | InterruptedException -> 0x0149, Throwable -> 0x013b }
            java.lang.String r14 = "\n"
            r15.append(r14)     // Catch:{ IOException | InterruptedException -> 0x0149, Throwable -> 0x013b }
            java.lang.String r13 = r15.toString()     // Catch:{ IOException | InterruptedException -> 0x0149, Throwable -> 0x013b }
            goto L_0x011c
        L_0x0137:
            r0.waitFor()     // Catch:{ IOException | InterruptedException -> 0x0149, Throwable -> 0x013b }
            goto L_0x0149
        L_0x013b:
            r0 = move-exception
            goto L_0x013f
        L_0x013d:
            r0 = move-exception
            r13 = r4
        L_0x013f:
            r0.printStackTrace()     // Catch:{ Exception -> 0x017d }
            java.lang.String r0 = "util not work correct"
            com.lp.C0987.m6060(r0)     // Catch:{ Exception -> 0x017d }
            goto L_0x0149
        L_0x0148:
            r13 = r4
        L_0x0149:
            java.lang.String r0 = r13.toLowerCase()     // Catch:{ Exception -> 0x017d }
            java.lang.String r12 = "enforcing"
            boolean r0 = r0.contains(r12)     // Catch:{ Exception -> 0x017d }
            if (r0 == 0) goto L_0x0176
            if (r10 != 0) goto L_0x0176
            if (r11 == 0) goto L_0x0176
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x017d }
            java.lang.String r10 = "selinux is enforcing!"
            r0.println(r10)     // Catch:{ Exception -> 0x017d }
            java.lang.String r0 = "enforce"
            com.chelpus.ˆ r1 = new com.chelpus.ˆ     // Catch:{ Exception -> 0x0174 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0174 }
            java.lang.String[] r1 = new java.lang.String[r7]     // Catch:{ Exception -> 0x0174 }
            java.lang.String r10 = "setenforce"
            r1[r2] = r10     // Catch:{ Exception -> 0x0174 }
            java.lang.String r10 = "0"
            r1[r5] = r10     // Catch:{ Exception -> 0x0174 }
            com.chelpus.C0815.m5209(r1)     // Catch:{ Exception -> 0x0174 }
        L_0x0174:
            r1 = r0
            goto L_0x017d
        L_0x0176:
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x017d }
            java.lang.String r10 = "selinux is permissive!"
            r0.println(r10)     // Catch:{ Exception -> 0x017d }
        L_0x017d:
            java.lang.String[] r10 = com.chelpus.C0815.m5222()
            r11 = 0
        L_0x0182:
            int r0 = r10.length
            if (r11 >= r0) goto L_0x02b1
            r0 = r10[r11]
            java.io.File r12 = new java.io.File
            r12.<init>(r0)
            java.io.File r13 = new java.io.File
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            r14.append(r0)
            java.lang.String r0 = "/lp_utils"
            r14.append(r0)
            java.lang.String r0 = r14.toString()
            r13.<init>(r0)
            boolean r0 = r13.exists()
            java.lang.String r14 = "%chelpus%"
            if (r0 != 0) goto L_0x01fb
            r12.mkdirs()
            boolean r0 = r12.exists()
            if (r0 != 0) goto L_0x01ba
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.String r15 = "LP: dirs for utils not created."
            r0.println(r15)
        L_0x01ba:
            r13.createNewFile()     // Catch:{ IOException -> 0x01ee }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01ee }
            r0.<init>()     // Catch:{ IOException -> 0x01ee }
            r0.append(r3)     // Catch:{ IOException -> 0x01ee }
            r0.append(r14)     // Catch:{ IOException -> 0x01ee }
            r0.append(r6)     // Catch:{ IOException -> 0x01ee }
            r0.append(r14)     // Catch:{ IOException -> 0x01ee }
            r0.append(r8)     // Catch:{ IOException -> 0x01ee }
            r0.append(r14)     // Catch:{ IOException -> 0x01ee }
            r0.append(r1)     // Catch:{ IOException -> 0x01ee }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x01ee }
            com.chelpus.C0815.m5278(r13, r0)     // Catch:{ IOException -> 0x01ee }
            java.lang.String r0 = r12.getAbsolutePath()     // Catch:{ IOException -> 0x01ee }
            com.chelpus.C0815.m5246(r0)     // Catch:{ IOException -> 0x01ee }
            java.lang.String r0 = r13.getAbsolutePath()     // Catch:{ IOException -> 0x01ee }
            com.chelpus.C0815.m5246(r0)     // Catch:{ IOException -> 0x01ee }
            goto L_0x02ab
        L_0x01ee:
            r0 = move-exception
            r0.printStackTrace()
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.String r12 = "LP: file for utils not created."
            r0.println(r12)
            goto L_0x02ab
        L_0x01fb:
            java.lang.String r0 = com.chelpus.C0815.m5251(r13)
            java.lang.String[] r0 = r0.split(r14)
            if (r0 == 0) goto L_0x0234
            int r15 = r0.length
            if (r15 <= 0) goto L_0x0234
            r16 = r4
            r17 = r16
            r18 = r17
            r19 = r18
            r15 = 0
        L_0x0211:
            int r2 = r0.length
            if (r15 >= r2) goto L_0x022b
            if (r15 == 0) goto L_0x0226
            if (r15 == r5) goto L_0x0223
            if (r15 == r7) goto L_0x0220
            if (r15 == r9) goto L_0x021d
            goto L_0x0228
        L_0x021d:
            r16 = r0[r15]
            goto L_0x0228
        L_0x0220:
            r19 = r0[r15]
            goto L_0x0228
        L_0x0223:
            r18 = r0[r15]
            goto L_0x0228
        L_0x0226:
            r17 = r0[r15]
        L_0x0228:
            int r15 = r15 + 1
            goto L_0x0211
        L_0x022b:
            r7 = r16
            r0 = r17
            r2 = r18
            r15 = r19
            goto L_0x0238
        L_0x0234:
            r0 = r4
            r2 = r0
            r7 = r2
            r15 = r7
        L_0x0238:
            boolean r17 = r0.equals(r3)
            if (r17 == 0) goto L_0x0266
            boolean r2 = r6.equals(r2)
            if (r2 == 0) goto L_0x0266
            boolean r2 = r8.equals(r15)
            if (r2 == 0) goto L_0x0266
            boolean r2 = r1.equals(r7)
            if (r2 == 0) goto L_0x0266
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.String r2 = "Utils file found and correct"
            r0.println(r2)
            java.lang.String r0 = r13.getAbsolutePath()
            com.chelpus.C0815.m5246(r0)
            java.lang.String r0 = r12.getAbsolutePath()
            com.chelpus.C0815.m5246(r0)
            goto L_0x02ab
        L_0x0266:
            java.io.PrintStream r2 = java.lang.System.out
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r15 = "Utils file contain incorrect path "
            r7.append(r15)
            r7.append(r0)
            java.lang.String r0 = r7.toString()
            r2.println(r0)
            java.lang.String r0 = r13.getAbsolutePath()
            com.chelpus.C0815.m5246(r0)
            java.lang.String r0 = r12.getAbsolutePath()
            com.chelpus.C0815.m5246(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r3)
            r0.append(r14)
            r0.append(r6)
            r0.append(r14)
            r0.append(r8)
            r0.append(r14)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.chelpus.C0815.m5278(r13, r0)
        L_0x02ab:
            int r11 = r11 + 1
            r2 = 0
            r7 = 2
            goto L_0x0182
        L_0x02b1:
            int r0 = com.chelpus.C0815.m5308()
            if (r0 != 0) goto L_0x02bf
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.String r1 = "Xposed settings not initialized"
            r0.println(r1)
            goto L_0x02c8
        L_0x02bf:
            java.lang.String[] r0 = com.chelpus.C0815.m5222()
            java.lang.String r1 = "xposed"
            com.chelpus.C0815.m5198(r0, r1)
        L_0x02c8:
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "/system/xbin/busybox"
            r0.<init>(r1)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x034b
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "/system/bin/busybox"
            r0.<init>(r1)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x034b
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "/bin/busybox"
            r0.<init>(r1)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x034b
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "/sbin/busybox"
            r0.<init>(r1)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x034b
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "/su/bin/busybox"
            r0.<init>(r1)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x034b
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "/su/xbin/busybox"
            r0.<init>(r1)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x034b
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "/su/xbin_bind/busybox"
            r0.<init>(r1)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x034b
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "/sbin/supersu/xbin/busybox"
            r0.<init>(r1)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x034b
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "/data/adb/su/xbin/busybox"
            r0.<init>(r1)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x034b
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.String r1 = "LuckyPatcher: busybox not install!"
            r0.println(r1)
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.String r1 = "busybox not found!"
            r0.println(r1)
        L_0x034b:
            java.io.File r0 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r3)
            java.lang.String r2 = "/dalvikvm"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            boolean r0 = r0.exists()
            java.lang.String r1 = "0:0"
            java.lang.String r4 = "0.0"
            java.lang.String r6 = "chmod"
            java.lang.String r7 = "chown"
            if (r0 == 0) goto L_0x03a3
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r3)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.String[] r2 = new java.lang.String[r9]
            r8 = 0
            r2[r8] = r6
            java.lang.String r10 = "777"
            r2[r5] = r10
            r10 = 2
            r2[r10] = r0
            com.chelpus.C0815.m5238(r2)
            java.lang.String[] r2 = new java.lang.String[r9]
            r2[r8] = r7
            r2[r5] = r4
            r2[r10] = r0
            com.chelpus.C0815.m5238(r2)
            java.lang.String[] r2 = new java.lang.String[r9]
            r2[r8] = r7
            r2[r5] = r1
            r2[r10] = r0
            com.chelpus.C0815.m5238(r2)
        L_0x03a3:
            java.io.File r0 = new java.io.File
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r3)
            java.lang.String r8 = "/busybox"
            r2.append(r8)
            java.lang.String r2 = r2.toString()
            r0.<init>(r2)
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x03f3
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r3)
            r0.append(r8)
            java.lang.String r0 = r0.toString()
            java.lang.String[] r2 = new java.lang.String[r9]
            r8 = 0
            r2[r8] = r6
            java.lang.String r10 = "06777"
            r2[r5] = r10
            r10 = 2
            r2[r10] = r0
            com.chelpus.C0815.m5238(r2)
            java.lang.String[] r2 = new java.lang.String[r9]
            r2[r8] = r7
            r2[r5] = r4
            r2[r10] = r0
            com.chelpus.C0815.m5238(r2)
            java.lang.String[] r2 = new java.lang.String[r9]
            r2[r8] = r7
            r2[r5] = r1
            r2[r10] = r0
            com.chelpus.C0815.m5238(r2)
        L_0x03f3:
            java.io.File r0 = new java.io.File
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r3)
            java.lang.String r8 = "/reboot"
            r2.append(r8)
            java.lang.String r2 = r2.toString()
            r0.<init>(r2)
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x0446
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r3)
            java.lang.String r2 = "/reboot"
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.String[] r2 = new java.lang.String[r9]
            r8 = 0
            r2[r8] = r6
            java.lang.String r3 = "777"
            r2[r5] = r3
            r3 = 2
            r2[r3] = r0
            com.chelpus.C0815.m5238(r2)
            java.lang.String[] r2 = new java.lang.String[r9]
            r2[r8] = r7
            r2[r5] = r4
            r2[r3] = r0
            com.chelpus.C0815.m5238(r2)
            java.lang.String[] r2 = new java.lang.String[r9]
            r2[r8] = r7
            r2[r5] = r1
            r2[r3] = r0
            com.chelpus.C0815.m5238(r2)
            goto L_0x0447
        L_0x0446:
            r8 = 0
        L_0x0447:
            java.lang.String r0 = "/data/app/"
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x04b4 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x04b4 }
            boolean r0 = r1.exists()     // Catch:{ Exception -> 0x04b4 }
            if (r0 == 0) goto L_0x04b2
            java.io.File[] r0 = r1.listFiles()     // Catch:{ Exception -> 0x04b4 }
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ Exception -> 0x04b4 }
            java.lang.String r2 = "\nUnusedOdexList:"
            r1.print(r2)     // Catch:{ Exception -> 0x04b4 }
            int r1 = r0.length     // Catch:{ Exception -> 0x04b4 }
            r2 = 0
        L_0x0461:
            if (r8 >= r1) goto L_0x04a8
            r3 = r0[r8]     // Catch:{ Exception -> 0x04b0 }
            java.lang.String r4 = r3.toString()     // Catch:{ Exception -> 0x04b0 }
            java.lang.String r5 = "apk"
            java.lang.String r4 = com.chelpus.C0815.m5145(r4, r5)     // Catch:{ Exception -> 0x04b0 }
            java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x04b0 }
            r5.<init>(r4)     // Catch:{ Exception -> 0x04b0 }
            java.lang.String r4 = r3.toString()     // Catch:{ Exception -> 0x04b0 }
            java.lang.String r4 = r4.toLowerCase()     // Catch:{ Exception -> 0x04b0 }
            java.lang.String r6 = ".odex"
            boolean r4 = r4.endsWith(r6)     // Catch:{ Exception -> 0x04b0 }
            if (r4 == 0) goto L_0x04a5
            boolean r4 = r5.exists()     // Catch:{ Exception -> 0x04b0 }
            if (r4 != 0) goto L_0x04a5
            r3.delete()     // Catch:{ Exception -> 0x04b0 }
            java.io.PrintStream r4 = java.lang.System.out     // Catch:{ Exception -> 0x04b0 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04b0 }
            r5.<init>()     // Catch:{ Exception -> 0x04b0 }
            r5.append(r3)     // Catch:{ Exception -> 0x04b0 }
            java.lang.String r3 = "|"
            r5.append(r3)     // Catch:{ Exception -> 0x04b0 }
            java.lang.String r3 = r5.toString()     // Catch:{ Exception -> 0x04b0 }
            r4.print(r3)     // Catch:{ Exception -> 0x04b0 }
            int r2 = r2 + 1
        L_0x04a5:
            int r8 = r8 + 1
            goto L_0x0461
        L_0x04a8:
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x04b0 }
            java.lang.String r1 = "\n"
            r0.print(r1)     // Catch:{ Exception -> 0x04b0 }
            goto L_0x04d0
        L_0x04b0:
            r0 = move-exception
            goto L_0x04b6
        L_0x04b2:
            r2 = 0
            goto L_0x04d0
        L_0x04b4:
            r0 = move-exception
            r2 = 0
        L_0x04b6:
            java.io.PrintStream r1 = java.lang.System.out
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Exception e"
            r3.append(r4)
            java.lang.String r0 = r0.toString()
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.println(r0)
        L_0x04d0:
            if (r2 <= 0) goto L_0x04d9
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.String r1 = "Unused ODEX in /data/app/ removed!"
            r0.println(r1)
        L_0x04d9:
            com.chelpus.C0815.m5312()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: javaroot.utils.checkRoot.main(java.lang.String[]):void");
    }
}
