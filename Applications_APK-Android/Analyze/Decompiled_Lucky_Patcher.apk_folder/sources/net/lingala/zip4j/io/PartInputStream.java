package net.lingala.zip4j.io;

import java.io.IOException;
import java.io.RandomAccessFile;
import net.lingala.zip4j.crypto.AESDecrypter;
import net.lingala.zip4j.crypto.IDecrypter;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.unzip.UnzipEngine;

public class PartInputStream extends BaseInputStream {
    private byte[] aesBlockByte = new byte[16];
    private int aesBytesReturned = 0;
    private long bytesRead;
    private int count = -1;
    private IDecrypter decrypter;
    private boolean isAESEncryptedFile = false;
    private long length;
    private byte[] oneByteBuff = new byte[1];
    private RandomAccessFile raf;
    private UnzipEngine unzipEngine;

    public PartInputStream(RandomAccessFile randomAccessFile, long j, long j2, UnzipEngine unzipEngine2) {
        boolean z = true;
        this.raf = randomAccessFile;
        this.unzipEngine = unzipEngine2;
        this.decrypter = unzipEngine2.getDecrypter();
        this.bytesRead = 0;
        this.length = j2;
        this.isAESEncryptedFile = (!unzipEngine2.getFileHeader().isEncrypted() || unzipEngine2.getFileHeader().getEncryptionMethod() != 99) ? false : z;
    }

    public int available() {
        long j = this.length - this.bytesRead;
        if (j > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        return (int) j;
    }

    public int read() {
        if (this.bytesRead >= this.length) {
            return -1;
        }
        if (this.isAESEncryptedFile) {
            int i = this.aesBytesReturned;
            if (i == 0 || i == 16) {
                if (read(this.aesBlockByte) == -1) {
                    return -1;
                }
                this.aesBytesReturned = 0;
            }
            byte[] bArr = this.aesBlockByte;
            int i2 = this.aesBytesReturned;
            this.aesBytesReturned = i2 + 1;
            return bArr[i2] & 255;
        } else if (read(this.oneByteBuff, 0, 1) == -1) {
            return -1;
        } else {
            return this.oneByteBuff[0] & 255;
        }
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) {
        int i3;
        long j = this.length;
        long j2 = this.bytesRead;
        if (((long) i2) <= j - j2 || (i2 = (int) (j - j2)) != 0) {
            if ((this.unzipEngine.getDecrypter() instanceof AESDecrypter) && this.bytesRead + ((long) i2) < this.length && (i3 = i2 % 16) != 0) {
                i2 -= i3;
            }
            synchronized (this.raf) {
                this.count = this.raf.read(bArr, i, i2);
                if (this.count < i2 && this.unzipEngine.getZipModel().isSplitArchive()) {
                    this.raf.close();
                    this.raf = this.unzipEngine.startNextSplitFile();
                    if (this.count < 0) {
                        this.count = 0;
                    }
                    int read = this.raf.read(bArr, this.count, i2 - this.count);
                    if (read > 0) {
                        this.count += read;
                    }
                }
            }
            int i4 = this.count;
            if (i4 > 0) {
                IDecrypter iDecrypter = this.decrypter;
                if (iDecrypter != null) {
                    try {
                        iDecrypter.decryptData(bArr, i, i4);
                    } catch (ZipException e) {
                        throw new IOException(e.getMessage());
                    }
                }
                this.bytesRead += (long) this.count;
            }
            if (this.bytesRead >= this.length) {
                checkAndReadAESMacBytes();
            }
            return this.count;
        }
        checkAndReadAESMacBytes();
        return -1;
    }

    /* access modifiers changed from: protected */
    public void checkAndReadAESMacBytes() {
        IDecrypter iDecrypter;
        if (this.isAESEncryptedFile && (iDecrypter = this.decrypter) != null && (iDecrypter instanceof AESDecrypter) && ((AESDecrypter) iDecrypter).getStoredMac() == null) {
            byte[] bArr = new byte[10];
            int read = this.raf.read(bArr);
            if (read != 10) {
                if (this.unzipEngine.getZipModel().isSplitArchive()) {
                    this.raf.close();
                    this.raf = this.unzipEngine.startNextSplitFile();
                    this.raf.read(bArr, read, 10 - read);
                } else {
                    throw new IOException("Error occured while reading stored AES authentication bytes");
                }
            }
            ((AESDecrypter) this.unzipEngine.getDecrypter()).setStoredMac(bArr);
        }
    }

    public long skip(long j) {
        if (j >= 0) {
            long j2 = this.length;
            long j3 = this.bytesRead;
            if (j > j2 - j3) {
                j = j2 - j3;
            }
            this.bytesRead += j;
            return j;
        }
        throw new IllegalArgumentException();
    }

    public void close() {
        this.raf.close();
    }

    public void seek(long j) {
        this.raf.seek(j);
    }

    public UnzipEngine getUnzipEngine() {
        return this.unzipEngine;
    }
}
