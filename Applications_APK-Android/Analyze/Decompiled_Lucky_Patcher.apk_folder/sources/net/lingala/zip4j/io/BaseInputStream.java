package net.lingala.zip4j.io;

import java.io.InputStream;
import net.lingala.zip4j.unzip.UnzipEngine;

public abstract class BaseInputStream extends InputStream {
    public int available() {
        return 0;
    }

    public UnzipEngine getUnzipEngine() {
        return null;
    }

    public int read() {
        return 0;
    }

    public void seek(long j) {
    }
}
