package net.lingala.zip4j.unzip;

import java.io.File;
import java.util.ArrayList;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.ZipInputStream;
import net.lingala.zip4j.model.CentralDirectory;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.UnzipParameters;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.progress.ProgressMonitor;
import net.lingala.zip4j.util.InternalZipConstants;
import net.lingala.zip4j.util.Zip4jUtil;

public class Unzip {
    private ZipModel zipModel;

    public Unzip(ZipModel zipModel2) {
        if (zipModel2 != null) {
            this.zipModel = zipModel2;
            return;
        }
        throw new ZipException("ZipModel is null");
    }

    public void extractAll(UnzipParameters unzipParameters, String str, ProgressMonitor progressMonitor, boolean z) {
        CentralDirectory centralDirectory = this.zipModel.getCentralDirectory();
        if (centralDirectory == null || centralDirectory.getFileHeaders() == null) {
            throw new ZipException("invalid central directory in zipModel");
        }
        final ArrayList fileHeaders = centralDirectory.getFileHeaders();
        progressMonitor.setCurrentOperation(1);
        progressMonitor.setTotalWork(calculateTotalWork(fileHeaders));
        progressMonitor.setState(1);
        if (z) {
            final UnzipParameters unzipParameters2 = unzipParameters;
            final ProgressMonitor progressMonitor2 = progressMonitor;
            final String str2 = str;
            new Thread(InternalZipConstants.THREAD_NAME) {
                public void run() {
                    try {
                        Unzip.this.initExtractAll(fileHeaders, unzipParameters2, progressMonitor2, str2);
                        progressMonitor2.endProgressMonitorSuccess();
                    } catch (ZipException unused) {
                    }
                }
            }.start();
            return;
        }
        initExtractAll(fileHeaders, unzipParameters, progressMonitor, str);
    }

    /* access modifiers changed from: private */
    public void initExtractAll(ArrayList arrayList, UnzipParameters unzipParameters, ProgressMonitor progressMonitor, String str) {
        for (int i = 0; i < arrayList.size(); i++) {
            initExtractFile((FileHeader) arrayList.get(i), str, unzipParameters, null, progressMonitor);
            if (progressMonitor.isCancelAllTasks()) {
                progressMonitor.setResult(3);
                progressMonitor.setState(0);
                return;
            }
        }
    }

    public void extractFile(FileHeader fileHeader, String str, UnzipParameters unzipParameters, String str2, ProgressMonitor progressMonitor, boolean z) {
        if (fileHeader != null) {
            progressMonitor.setCurrentOperation(1);
            progressMonitor.setTotalWork(fileHeader.getCompressedSize());
            progressMonitor.setState(1);
            progressMonitor.setPercentDone(0);
            progressMonitor.setFileName(fileHeader.getFileName());
            if (z) {
                final FileHeader fileHeader2 = fileHeader;
                final String str3 = str;
                final UnzipParameters unzipParameters2 = unzipParameters;
                final String str4 = str2;
                final ProgressMonitor progressMonitor2 = progressMonitor;
                new Thread(InternalZipConstants.THREAD_NAME) {
                    public void run() {
                        try {
                            Unzip.this.initExtractFile(fileHeader2, str3, unzipParameters2, str4, progressMonitor2);
                            progressMonitor2.endProgressMonitorSuccess();
                        } catch (ZipException unused) {
                        }
                    }
                }.start();
                return;
            }
            initExtractFile(fileHeader, str, unzipParameters, str2, progressMonitor);
            progressMonitor.endProgressMonitorSuccess();
            return;
        }
        throw new ZipException("fileHeader is null");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x007d, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x007e, code lost:
        r7.endProgressMonitorError(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0081, code lost:
        throw r3;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x007d A[ExcHandler: ZipException (r3v2 'e' net.lingala.zip4j.exception.ZipException A[CUSTOM_DECLARE]), Splitter:B:1:0x0002] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void initExtractFile(net.lingala.zip4j.model.FileHeader r3, java.lang.String r4, net.lingala.zip4j.model.UnzipParameters r5, java.lang.String r6, net.lingala.zip4j.progress.ProgressMonitor r7) {
        /*
            r2 = this;
            if (r3 == 0) goto L_0x0082
            java.lang.String r0 = r3.getFileName()     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            r7.setFileName(r0)     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            java.lang.String r0 = net.lingala.zip4j.util.InternalZipConstants.FILE_SEPARATOR     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            boolean r0 = r4.endsWith(r0)     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            if (r0 != 0) goto L_0x0022
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            r0.<init>()     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            r0.append(r4)     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            java.lang.String r4 = net.lingala.zip4j.util.InternalZipConstants.FILE_SEPARATOR     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            r0.append(r4)     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            java.lang.String r4 = r0.toString()     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
        L_0x0022:
            boolean r0 = r3.isDirectory()     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            if (r0 == 0) goto L_0x005b
            java.lang.String r3 = r3.getFileName()     // Catch:{ Exception -> 0x0051, ZipException -> 0x007d }
            boolean r5 = net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(r3)     // Catch:{ Exception -> 0x0051, ZipException -> 0x007d }
            if (r5 != 0) goto L_0x0033
            return
        L_0x0033:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0051, ZipException -> 0x007d }
            r5.<init>()     // Catch:{ Exception -> 0x0051, ZipException -> 0x007d }
            r5.append(r4)     // Catch:{ Exception -> 0x0051, ZipException -> 0x007d }
            r5.append(r3)     // Catch:{ Exception -> 0x0051, ZipException -> 0x007d }
            java.lang.String r3 = r5.toString()     // Catch:{ Exception -> 0x0051, ZipException -> 0x007d }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0051, ZipException -> 0x007d }
            r4.<init>(r3)     // Catch:{ Exception -> 0x0051, ZipException -> 0x007d }
            boolean r3 = r4.exists()     // Catch:{ Exception -> 0x0051, ZipException -> 0x007d }
            if (r3 != 0) goto L_0x0068
            r4.mkdirs()     // Catch:{ Exception -> 0x0051, ZipException -> 0x007d }
            goto L_0x0068
        L_0x0051:
            r3 = move-exception
            r7.endProgressMonitorError(r3)     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            net.lingala.zip4j.exception.ZipException r4 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            r4.<init>(r3)     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            throw r4     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
        L_0x005b:
            r2.checkOutputDirectoryStructure(r3, r4, r6)     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            net.lingala.zip4j.unzip.UnzipEngine r0 = new net.lingala.zip4j.unzip.UnzipEngine     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            net.lingala.zip4j.model.ZipModel r1 = r2.zipModel     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            r0.<init>(r1, r3)     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            r0.unzipFile(r7, r4, r6, r5)     // Catch:{ Exception -> 0x0069, ZipException -> 0x007d }
        L_0x0068:
            return
        L_0x0069:
            r3 = move-exception
            r7.endProgressMonitorError(r3)     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            net.lingala.zip4j.exception.ZipException r4 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            r4.<init>(r3)     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
            throw r4     // Catch:{ ZipException -> 0x007d, Exception -> 0x0073 }
        L_0x0073:
            r3 = move-exception
            r7.endProgressMonitorError(r3)
            net.lingala.zip4j.exception.ZipException r4 = new net.lingala.zip4j.exception.ZipException
            r4.<init>(r3)
            throw r4
        L_0x007d:
            r3 = move-exception
            r7.endProgressMonitorError(r3)
            throw r3
        L_0x0082:
            net.lingala.zip4j.exception.ZipException r3 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r4 = "fileHeader is null"
            r3.<init>(r4)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: net.lingala.zip4j.unzip.Unzip.initExtractFile(net.lingala.zip4j.model.FileHeader, java.lang.String, net.lingala.zip4j.model.UnzipParameters, java.lang.String, net.lingala.zip4j.progress.ProgressMonitor):void");
    }

    public ZipInputStream getInputStream(FileHeader fileHeader) {
        return new UnzipEngine(this.zipModel, fileHeader).getInputStream();
    }

    private void checkOutputDirectoryStructure(FileHeader fileHeader, String str, String str2) {
        if (fileHeader == null || !Zip4jUtil.isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("Cannot check output directory structure...one of the parameters was null");
        }
        String fileName = fileHeader.getFileName();
        if (Zip4jUtil.isStringNotNullAndNotEmpty(str2)) {
            fileName = str2;
        }
        if (Zip4jUtil.isStringNotNullAndNotEmpty(fileName)) {
            try {
                File file = new File(new File(str + fileName).getParent());
                if (!file.exists()) {
                    file.mkdirs();
                }
            } catch (Exception e) {
                throw new ZipException(e);
            }
        }
    }

    private long calculateTotalWork(ArrayList arrayList) {
        long j;
        if (arrayList != null) {
            long j2 = 0;
            for (int i = 0; i < arrayList.size(); i++) {
                FileHeader fileHeader = (FileHeader) arrayList.get(i);
                if (fileHeader.getZip64ExtendedInfo() == null || fileHeader.getZip64ExtendedInfo().getUnCompressedSize() <= 0) {
                    j = fileHeader.getCompressedSize();
                } else {
                    j = fileHeader.getZip64ExtendedInfo().getCompressedSize();
                }
                j2 += j;
            }
            return j2;
        }
        throw new ZipException("fileHeaders is null, cannot calculate total work");
    }
}
