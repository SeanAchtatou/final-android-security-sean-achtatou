package net.lingala.zip4j.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.progress.ProgressMonitor;

public class ArchiveMaintainer {
    public HashMap removeZipFile(ZipModel zipModel, FileHeader fileHeader, ProgressMonitor progressMonitor, boolean z) {
        if (z) {
            final ZipModel zipModel2 = zipModel;
            final FileHeader fileHeader2 = fileHeader;
            final ProgressMonitor progressMonitor2 = progressMonitor;
            new Thread(InternalZipConstants.THREAD_NAME) {
                public void run() {
                    try {
                        ArchiveMaintainer.this.initRemoveZipFile(zipModel2, fileHeader2, progressMonitor2);
                        progressMonitor2.endProgressMonitorSuccess();
                    } catch (ZipException unused) {
                    }
                }
            }.start();
            return null;
        }
        HashMap initRemoveZipFile = initRemoveZipFile(zipModel, fileHeader, progressMonitor);
        progressMonitor.endProgressMonitorSuccess();
        return initRemoveZipFile;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r7v2 */
    /* JADX WARN: Type inference failed for: r7v3 */
    /* JADX WARN: Type inference failed for: r7v4 */
    /* JADX WARN: Type inference failed for: r7v7 */
    /* JADX WARN: Type inference failed for: r14v14 */
    /* JADX WARN: Type inference failed for: r14v15 */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x01b4, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01b6, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x01fc, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x01fd, code lost:
        r2 = r29;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x0315, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x031b, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x031c, code lost:
        r2 = r29;
        r3 = r30;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x0321, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x034a, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x034b, code lost:
        r28 = r4;
        r3 = r6;
        r2 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x0352, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x0353, code lost:
        r28 = r4;
        r3 = r6;
        r2 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x0357, code lost:
        r2 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x035a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:?, code lost:
        r28.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:257:0x040b, code lost:
        r14.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:260:0x0414, code lost:
        throw new net.lingala.zip4j.exception.ZipException("cannot close input stream or output stream when trying to delete a file from zip file");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:262:0x0417, code lost:
        restoreFileName(r3, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:263:0x041b, code lost:
        new java.io.File(r2).delete();
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:79:0x0166, B:93:0x019e] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x01b4 A[ExcHandler: Exception (e java.lang.Exception), PHI: r28 r29 r30 
      PHI: (r28v23 java.io.RandomAccessFile) = (r28v22 java.io.RandomAccessFile), (r28v22 java.io.RandomAccessFile), (r28v24 java.io.RandomAccessFile), (r28v24 java.io.RandomAccessFile), (r28v31 java.io.RandomAccessFile), (r28v31 java.io.RandomAccessFile) binds: [B:111:0x01df, B:112:?, B:98:0x01b0, B:99:?, B:79:0x0166, B:80:?] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r29v4 java.lang.String) = (r29v2 java.lang.String), (r29v2 java.lang.String), (r29v5 java.lang.String), (r29v5 java.lang.String), (r29v12 java.lang.String), (r29v12 java.lang.String) binds: [B:111:0x01df, B:112:?, B:98:0x01b0, B:99:?, B:79:0x0166, B:80:?] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r30v4 java.io.File) = (r30v2 java.io.File), (r30v2 java.io.File), (r30v5 java.io.File), (r30v5 java.io.File), (r30v14 java.io.File), (r30v14 java.io.File) binds: [B:111:0x01df, B:112:?, B:98:0x01b0, B:99:?, B:79:0x0166, B:80:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:79:0x0166] */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x01b6 A[ExcHandler: ZipException (e net.lingala.zip4j.exception.ZipException), PHI: r29 r30 
      PHI: (r29v3 java.lang.String) = (r29v2 java.lang.String), (r29v2 java.lang.String), (r29v5 java.lang.String), (r29v5 java.lang.String), (r29v12 java.lang.String), (r29v12 java.lang.String) binds: [B:111:0x01df, B:112:?, B:98:0x01b0, B:99:?, B:79:0x0166, B:80:?] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r30v3 java.io.File) = (r30v2 java.io.File), (r30v2 java.io.File), (r30v5 java.io.File), (r30v5 java.io.File), (r30v14 java.io.File), (r30v14 java.io.File) binds: [B:111:0x01df, B:112:?, B:98:0x01b0, B:99:?, B:79:0x0166, B:80:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:79:0x0166] */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x0315 A[ExcHandler: all (th java.lang.Throwable), PHI: r28 r29 r30 
      PHI: (r28v21 java.io.RandomAccessFile) = (r28v24 java.io.RandomAccessFile), (r28v24 java.io.RandomAccessFile), (r28v24 java.io.RandomAccessFile), (r28v24 java.io.RandomAccessFile), (r28v31 java.io.RandomAccessFile), (r28v31 java.io.RandomAccessFile) binds: [B:93:0x019e, B:103:0x01c3, B:98:0x01b0, B:99:?, B:79:0x0166, B:80:?] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r29v1 java.lang.String) = (r29v5 java.lang.String), (r29v5 java.lang.String), (r29v5 java.lang.String), (r29v5 java.lang.String), (r29v12 java.lang.String), (r29v12 java.lang.String) binds: [B:93:0x019e, B:103:0x01c3, B:98:0x01b0, B:99:?, B:79:0x0166, B:80:?] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r30v1 java.io.File) = (r30v5 java.io.File), (r30v5 java.io.File), (r30v5 java.io.File), (r30v5 java.io.File), (r30v14 java.io.File), (r30v14 java.io.File) binds: [B:93:0x019e, B:103:0x01c3, B:98:0x01b0, B:99:?, B:79:0x0166, B:80:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:79:0x0166] */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x034a A[ExcHandler: all (th java.lang.Throwable), Splitter:B:31:0x0098] */
    /* JADX WARNING: Removed duplicated region for block: B:254:0x0406 A[SYNTHETIC, Splitter:B:254:0x0406] */
    /* JADX WARNING: Removed duplicated region for block: B:257:0x040b A[Catch:{ IOException -> 0x040f }] */
    /* JADX WARNING: Removed duplicated region for block: B:262:0x0417  */
    /* JADX WARNING: Removed duplicated region for block: B:263:0x041b  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00e4 A[SYNTHETIC, Splitter:B:50:0x00e4] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0105 A[Catch:{ ZipException -> 0x035a, Exception -> 0x0352, all -> 0x034a }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x010a A[Catch:{ ZipException -> 0x035a, Exception -> 0x0352, all -> 0x034a }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x013f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.HashMap initRemoveZipFile(net.lingala.zip4j.model.ZipModel r32, net.lingala.zip4j.model.FileHeader r33, net.lingala.zip4j.progress.ProgressMonitor r34) {
        /*
            r31 = this;
            r9 = r31
            r0 = r32
            r1 = r33
            r10 = r34
            java.lang.String r11 = "cannot close input stream or output stream when trying to delete a file from zip file"
            if (r1 == 0) goto L_0x0424
            if (r0 == 0) goto L_0x0424
            java.util.HashMap r12 = new java.util.HashMap
            r12.<init>()
            r15 = 1
            int r8 = net.lingala.zip4j.util.Zip4jUtil.getIndexOfFileHeader(r32, r33)     // Catch:{ ZipException -> 0x03f5, Exception -> 0x03e3, all -> 0x03da }
            if (r8 < 0) goto L_0x03ca
            boolean r2 = r32.isSplitArchive()     // Catch:{ ZipException -> 0x03f5, Exception -> 0x03e3, all -> 0x03da }
            if (r2 != 0) goto L_0x03c0
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ ZipException -> 0x03f5, Exception -> 0x03e3, all -> 0x03da }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ ZipException -> 0x03f5, Exception -> 0x03e3, all -> 0x03da }
            r4.<init>()     // Catch:{ ZipException -> 0x03f5, Exception -> 0x03e3, all -> 0x03da }
            java.lang.String r5 = r32.getZipFile()     // Catch:{ ZipException -> 0x03f5, Exception -> 0x03e3, all -> 0x03da }
            r4.append(r5)     // Catch:{ ZipException -> 0x03f5, Exception -> 0x03e3, all -> 0x03da }
            r5 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 % r5
            r4.append(r2)     // Catch:{ ZipException -> 0x03f5, Exception -> 0x03e3, all -> 0x03da }
            java.lang.String r2 = r4.toString()     // Catch:{ ZipException -> 0x03f5, Exception -> 0x03e3, all -> 0x03da }
            java.io.File r3 = new java.io.File     // Catch:{ ZipException -> 0x03bb, Exception -> 0x03b6, all -> 0x03b1 }
            r3.<init>(r2)     // Catch:{ ZipException -> 0x03bb, Exception -> 0x03b6, all -> 0x03b1 }
            r7 = r2
        L_0x0040:
            boolean r2 = r3.exists()     // Catch:{ ZipException -> 0x03ae, Exception -> 0x03ab, all -> 0x03a8 }
            if (r2 == 0) goto L_0x007f
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ ZipException -> 0x0076, Exception -> 0x006d, all -> 0x0064 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ ZipException -> 0x0076, Exception -> 0x006d, all -> 0x0064 }
            r4.<init>()     // Catch:{ ZipException -> 0x0076, Exception -> 0x006d, all -> 0x0064 }
            java.lang.String r14 = r32.getZipFile()     // Catch:{ ZipException -> 0x0076, Exception -> 0x006d, all -> 0x0064 }
            r4.append(r14)     // Catch:{ ZipException -> 0x0076, Exception -> 0x006d, all -> 0x0064 }
            long r2 = r2 % r5
            r4.append(r2)     // Catch:{ ZipException -> 0x0076, Exception -> 0x006d, all -> 0x0064 }
            java.lang.String r7 = r4.toString()     // Catch:{ ZipException -> 0x0076, Exception -> 0x006d, all -> 0x0064 }
            java.io.File r3 = new java.io.File     // Catch:{ ZipException -> 0x0076, Exception -> 0x006d, all -> 0x0064 }
            r3.<init>(r7)     // Catch:{ ZipException -> 0x0076, Exception -> 0x006d, all -> 0x0064 }
            goto L_0x0040
        L_0x0064:
            r0 = move-exception
            r2 = r7
            r1 = 0
            r3 = 0
            r14 = 0
            r28 = 0
            goto L_0x0404
        L_0x006d:
            r0 = move-exception
            r2 = r7
            r3 = 0
            r13 = 0
            r14 = 0
            r28 = 0
            goto L_0x03ec
        L_0x0076:
            r0 = move-exception
            r2 = r7
            r3 = 0
            r13 = 0
            r14 = 0
            r28 = 0
            goto L_0x03fe
        L_0x007f:
            net.lingala.zip4j.io.SplitOutputStream r14 = new net.lingala.zip4j.io.SplitOutputStream     // Catch:{ FileNotFoundException -> 0x0398 }
            java.io.File r2 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0398 }
            r2.<init>(r7)     // Catch:{ FileNotFoundException -> 0x0398 }
            r14.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0398 }
            java.io.File r6 = new java.io.File     // Catch:{ ZipException -> 0x038f, Exception -> 0x0386, all -> 0x037d }
            java.lang.String r2 = r32.getZipFile()     // Catch:{ ZipException -> 0x038f, Exception -> 0x0386, all -> 0x037d }
            r6.<init>(r2)     // Catch:{ ZipException -> 0x038f, Exception -> 0x0386, all -> 0x037d }
            java.lang.String r2 = "r"
            java.io.RandomAccessFile r4 = r9.createFileHandler(r0, r2)     // Catch:{ ZipException -> 0x0374, Exception -> 0x036b, all -> 0x0362 }
            net.lingala.zip4j.core.HeaderReader r2 = new net.lingala.zip4j.core.HeaderReader     // Catch:{ ZipException -> 0x035a, Exception -> 0x0352, all -> 0x034a }
            r2.<init>(r4)     // Catch:{ ZipException -> 0x035a, Exception -> 0x0352, all -> 0x034a }
            net.lingala.zip4j.model.LocalFileHeader r2 = r2.readLocalFileHeader(r1)     // Catch:{ ZipException -> 0x035a, Exception -> 0x0352, all -> 0x034a }
            if (r2 == 0) goto L_0x0334
            long r2 = r33.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x035a, Exception -> 0x0352, all -> 0x034a }
            net.lingala.zip4j.model.Zip64ExtendedInfo r5 = r33.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x035a, Exception -> 0x0352, all -> 0x034a }
            r16 = -1
            if (r5 == 0) goto L_0x00d4
            net.lingala.zip4j.model.Zip64ExtendedInfo r5 = r33.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x00cd, Exception -> 0x00c6, all -> 0x034a }
            long r18 = r5.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x00cd, Exception -> 0x00c6, all -> 0x034a }
            int r5 = (r18 > r16 ? 1 : (r18 == r16 ? 0 : -1))
            if (r5 == 0) goto L_0x00d4
            net.lingala.zip4j.model.Zip64ExtendedInfo r1 = r33.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x00cd, Exception -> 0x00c6, all -> 0x034a }
            long r1 = r1.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x00cd, Exception -> 0x00c6, all -> 0x034a }
            r18 = r1
            goto L_0x00d6
        L_0x00c6:
            r0 = move-exception
            r28 = r4
            r3 = r6
            r2 = r7
            goto L_0x03eb
        L_0x00cd:
            r0 = move-exception
            r28 = r4
            r3 = r6
            r2 = r7
            goto L_0x03fd
        L_0x00d4:
            r18 = r2
        L_0x00d6:
            net.lingala.zip4j.model.EndCentralDirRecord r1 = r32.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x035a, Exception -> 0x0352, all -> 0x034a }
            long r1 = r1.getOffsetOfStartOfCentralDir()     // Catch:{ ZipException -> 0x035a, Exception -> 0x0352, all -> 0x034a }
            boolean r3 = r32.isZip64Format()     // Catch:{ ZipException -> 0x035a, Exception -> 0x0352, all -> 0x034a }
            if (r3 == 0) goto L_0x00f2
            net.lingala.zip4j.model.Zip64EndCentralDirRecord r3 = r32.getZip64EndCentralDirRecord()     // Catch:{ ZipException -> 0x00cd, Exception -> 0x00c6, all -> 0x034a }
            if (r3 == 0) goto L_0x00f2
            net.lingala.zip4j.model.Zip64EndCentralDirRecord r1 = r32.getZip64EndCentralDirRecord()     // Catch:{ ZipException -> 0x00cd, Exception -> 0x00c6, all -> 0x034a }
            long r1 = r1.getOffsetStartCenDirWRTStartDiskNo()     // Catch:{ ZipException -> 0x00cd, Exception -> 0x00c6, all -> 0x034a }
        L_0x00f2:
            r20 = r1
            net.lingala.zip4j.model.CentralDirectory r1 = r32.getCentralDirectory()     // Catch:{ ZipException -> 0x035a, Exception -> 0x0352, all -> 0x034a }
            java.util.ArrayList r1 = r1.getFileHeaders()     // Catch:{ ZipException -> 0x035a, Exception -> 0x0352, all -> 0x034a }
            int r2 = r1.size()     // Catch:{ ZipException -> 0x035a, Exception -> 0x0352, all -> 0x034a }
            int r2 = r2 - r15
            r22 = 1
            if (r8 != r2) goto L_0x010a
            long r2 = r20 - r22
        L_0x0107:
            r24 = r2
            goto L_0x0139
        L_0x010a:
            int r2 = r8 + 1
            java.lang.Object r2 = r1.get(r2)     // Catch:{ ZipException -> 0x035a, Exception -> 0x0352, all -> 0x034a }
            net.lingala.zip4j.model.FileHeader r2 = (net.lingala.zip4j.model.FileHeader) r2     // Catch:{ ZipException -> 0x035a, Exception -> 0x0352, all -> 0x034a }
            if (r2 == 0) goto L_0x0137
            long r24 = r2.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x00cd, Exception -> 0x00c6, all -> 0x034a }
            long r24 = r24 - r22
            net.lingala.zip4j.model.Zip64ExtendedInfo r3 = r2.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x00cd, Exception -> 0x00c6, all -> 0x034a }
            if (r3 == 0) goto L_0x0139
            net.lingala.zip4j.model.Zip64ExtendedInfo r3 = r2.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x00cd, Exception -> 0x00c6, all -> 0x034a }
            long r26 = r3.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x00cd, Exception -> 0x00c6, all -> 0x034a }
            int r3 = (r26 > r16 ? 1 : (r26 == r16 ? 0 : -1))
            if (r3 == 0) goto L_0x0139
            net.lingala.zip4j.model.Zip64ExtendedInfo r2 = r2.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x00cd, Exception -> 0x00c6, all -> 0x034a }
            long r2 = r2.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x00cd, Exception -> 0x00c6, all -> 0x034a }
            long r2 = r2 - r22
            goto L_0x0107
        L_0x0137:
            r24 = r16
        L_0x0139:
            r2 = 0
            int r5 = (r18 > r2 ? 1 : (r18 == r2 ? 0 : -1))
            if (r5 < 0) goto L_0x0327
            int r5 = (r24 > r2 ? 1 : (r24 == r2 ? 0 : -1))
            if (r5 < 0) goto L_0x0327
            if (r8 != 0) goto L_0x0197
            net.lingala.zip4j.model.CentralDirectory r1 = r32.getCentralDirectory()     // Catch:{ ZipException -> 0x018a, Exception -> 0x017d, all -> 0x0174 }
            java.util.ArrayList r1 = r1.getFileHeaders()     // Catch:{ ZipException -> 0x018a, Exception -> 0x017d, all -> 0x0174 }
            int r1 = r1.size()     // Catch:{ ZipException -> 0x018a, Exception -> 0x017d, all -> 0x0174 }
            if (r1 <= r15) goto L_0x016b
            long r26 = r24 + r22
            r1 = r31
            r2 = r4
            r3 = r14
            r28 = r4
            r4 = r26
            r30 = r6
            r29 = r7
            r6 = r20
            r13 = r8
            r8 = r34
            r1.copyFile(r2, r3, r4, r6, r8)     // Catch:{ ZipException -> 0x01b6, Exception -> 0x01b4, all -> 0x0315 }
            goto L_0x01d4
        L_0x016b:
            r28 = r4
            r30 = r6
            r29 = r7
            r13 = r8
            goto L_0x01d4
        L_0x0174:
            r0 = move-exception
            r28 = r4
            r30 = r6
            r29 = r7
            goto L_0x0316
        L_0x017d:
            r0 = move-exception
            r28 = r4
            r30 = r6
            r29 = r7
        L_0x0184:
            r2 = r29
        L_0x0186:
            r3 = r30
            goto L_0x03eb
        L_0x018a:
            r0 = move-exception
            r28 = r4
            r30 = r6
            r29 = r7
        L_0x0191:
            r2 = r29
        L_0x0193:
            r3 = r30
            goto L_0x03fd
        L_0x0197:
            r28 = r4
            r30 = r6
            r29 = r7
            r13 = r8
            int r1 = r1.size()     // Catch:{ ZipException -> 0x0321, Exception -> 0x031b, all -> 0x0315 }
            int r1 = r1 - r15
            if (r13 != r1) goto L_0x01b8
            r4 = 0
            r1 = r31
            r2 = r28
            r3 = r14
            r6 = r18
            r8 = r34
            r1.copyFile(r2, r3, r4, r6, r8)     // Catch:{ ZipException -> 0x01b6, Exception -> 0x01b4, all -> 0x0315 }
            goto L_0x01d4
        L_0x01b4:
            r0 = move-exception
            goto L_0x0184
        L_0x01b6:
            r0 = move-exception
            goto L_0x0191
        L_0x01b8:
            r4 = 0
            r1 = r31
            r2 = r28
            r3 = r14
            r6 = r18
            r8 = r34
            r1.copyFile(r2, r3, r4, r6, r8)     // Catch:{ ZipException -> 0x0321, Exception -> 0x031b, all -> 0x0315 }
            long r4 = r24 + r22
            r1 = r31
            r2 = r28
            r3 = r14
            r6 = r20
            r8 = r34
            r1.copyFile(r2, r3, r4, r6, r8)     // Catch:{ ZipException -> 0x0321, Exception -> 0x031b, all -> 0x0315 }
        L_0x01d4:
            boolean r1 = r34.isCancelAllTasks()     // Catch:{ ZipException -> 0x0321, Exception -> 0x031b, all -> 0x0315 }
            if (r1 == 0) goto L_0x0212
            r0 = 3
            r10.setResult(r0)     // Catch:{ ZipException -> 0x020d, Exception -> 0x0207, all -> 0x0201 }
            r1 = 0
            r10.setState(r1)     // Catch:{ ZipException -> 0x01b6, Exception -> 0x01b4, all -> 0x01fc }
            if (r28 == 0) goto L_0x01e7
            r28.close()     // Catch:{ IOException -> 0x01f6 }
        L_0x01e7:
            r14.close()     // Catch:{ IOException -> 0x01f6 }
            java.io.File r0 = new java.io.File
            r2 = r29
            r0.<init>(r2)
            r0.delete()
            r7 = 0
            return r7
        L_0x01f6:
            net.lingala.zip4j.exception.ZipException r0 = new net.lingala.zip4j.exception.ZipException
            r0.<init>(r11)
            throw r0
        L_0x01fc:
            r0 = move-exception
            r2 = r29
            goto L_0x030b
        L_0x0201:
            r0 = move-exception
            r2 = r29
            r1 = 0
            goto L_0x030b
        L_0x0207:
            r0 = move-exception
            r2 = r29
            r1 = 0
            goto L_0x0186
        L_0x020d:
            r0 = move-exception
            r2 = r29
            r1 = 0
            goto L_0x0193
        L_0x0212:
            r2 = r29
            r1 = 0
            net.lingala.zip4j.model.EndCentralDirRecord r3 = r32.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            r4 = r14
            net.lingala.zip4j.io.SplitOutputStream r4 = (net.lingala.zip4j.io.SplitOutputStream) r4     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            long r4 = r4.getFilePointer()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            r3.setOffsetOfStartOfCentralDir(r4)     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            net.lingala.zip4j.model.EndCentralDirRecord r3 = r32.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            net.lingala.zip4j.model.EndCentralDirRecord r4 = r32.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            int r4 = r4.getTotNoOfEntriesInCentralDir()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            int r4 = r4 - r15
            r3.setTotNoOfEntriesInCentralDir(r4)     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            net.lingala.zip4j.model.EndCentralDirRecord r3 = r32.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            net.lingala.zip4j.model.EndCentralDirRecord r4 = r32.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            int r4 = r4.getTotNoOfEntriesInCentralDirOnThisDisk()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            int r4 = r4 - r15
            r3.setTotNoOfEntriesInCentralDirOnThisDisk(r4)     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            net.lingala.zip4j.model.CentralDirectory r3 = r32.getCentralDirectory()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            java.util.ArrayList r3 = r3.getFileHeaders()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            r3.remove(r13)     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
        L_0x024e:
            net.lingala.zip4j.model.CentralDirectory r3 = r32.getCentralDirectory()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            java.util.ArrayList r3 = r3.getFileHeaders()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            int r3 = r3.size()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            if (r13 >= r3) goto L_0x02cb
            net.lingala.zip4j.model.CentralDirectory r3 = r32.getCentralDirectory()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            java.util.ArrayList r3 = r3.getFileHeaders()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            java.lang.Object r3 = r3.get(r13)     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            net.lingala.zip4j.model.FileHeader r3 = (net.lingala.zip4j.model.FileHeader) r3     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            long r3 = r3.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            net.lingala.zip4j.model.CentralDirectory r5 = r32.getCentralDirectory()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            java.util.ArrayList r5 = r5.getFileHeaders()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            java.lang.Object r5 = r5.get(r13)     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            net.lingala.zip4j.model.FileHeader r5 = (net.lingala.zip4j.model.FileHeader) r5     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            net.lingala.zip4j.model.Zip64ExtendedInfo r5 = r5.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            if (r5 == 0) goto L_0x02b2
            net.lingala.zip4j.model.CentralDirectory r5 = r32.getCentralDirectory()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            java.util.ArrayList r5 = r5.getFileHeaders()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            java.lang.Object r5 = r5.get(r13)     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            net.lingala.zip4j.model.FileHeader r5 = (net.lingala.zip4j.model.FileHeader) r5     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            net.lingala.zip4j.model.Zip64ExtendedInfo r5 = r5.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            long r5 = r5.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            int r7 = (r5 > r16 ? 1 : (r5 == r16 ? 0 : -1))
            if (r7 == 0) goto L_0x02b2
            net.lingala.zip4j.model.CentralDirectory r3 = r32.getCentralDirectory()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            java.util.ArrayList r3 = r3.getFileHeaders()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            java.lang.Object r3 = r3.get(r13)     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            net.lingala.zip4j.model.FileHeader r3 = (net.lingala.zip4j.model.FileHeader) r3     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            net.lingala.zip4j.model.Zip64ExtendedInfo r3 = r3.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            long r3 = r3.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
        L_0x02b2:
            net.lingala.zip4j.model.CentralDirectory r5 = r32.getCentralDirectory()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            java.util.ArrayList r5 = r5.getFileHeaders()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            java.lang.Object r5 = r5.get(r13)     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            net.lingala.zip4j.model.FileHeader r5 = (net.lingala.zip4j.model.FileHeader) r5     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            long r6 = r24 - r18
            long r3 = r3 - r6
            long r3 = r3 - r22
            r5.setOffsetLocalHeader(r3)     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            int r13 = r13 + 1
            goto L_0x024e
        L_0x02cb:
            net.lingala.zip4j.core.HeaderWriter r3 = new net.lingala.zip4j.core.HeaderWriter     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            r3.<init>()     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            r3.finalizeZipFile(r0, r14)     // Catch:{ ZipException -> 0x0312, Exception -> 0x030f, all -> 0x030a }
            java.lang.String r1 = "offsetCentralDir"
            net.lingala.zip4j.model.EndCentralDirRecord r0 = r32.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x0304, Exception -> 0x02fe, all -> 0x02f8 }
            long r3 = r0.getOffsetOfStartOfCentralDir()     // Catch:{ ZipException -> 0x0304, Exception -> 0x02fe, all -> 0x02f8 }
            java.lang.String r0 = java.lang.Long.toString(r3)     // Catch:{ ZipException -> 0x0304, Exception -> 0x02fe, all -> 0x02f8 }
            r12.put(r1, r0)     // Catch:{ ZipException -> 0x0304, Exception -> 0x02fe, all -> 0x02f8 }
            if (r28 == 0) goto L_0x02e9
            r28.close()     // Catch:{ IOException -> 0x02f2 }
        L_0x02e9:
            r14.close()     // Catch:{ IOException -> 0x02f2 }
            r3 = r30
            r9.restoreFileName(r3, r2)
            return r12
        L_0x02f2:
            net.lingala.zip4j.exception.ZipException r0 = new net.lingala.zip4j.exception.ZipException
            r0.<init>(r11)
            throw r0
        L_0x02f8:
            r0 = move-exception
            r3 = r30
            r1 = 1
            goto L_0x0404
        L_0x02fe:
            r0 = move-exception
            r3 = r30
            r13 = 1
            goto L_0x03ec
        L_0x0304:
            r0 = move-exception
            r3 = r30
            r13 = 1
            goto L_0x03fe
        L_0x030a:
            r0 = move-exception
        L_0x030b:
            r3 = r30
            goto L_0x0404
        L_0x030f:
            r0 = move-exception
            goto L_0x0186
        L_0x0312:
            r0 = move-exception
            goto L_0x0193
        L_0x0315:
            r0 = move-exception
        L_0x0316:
            r2 = r29
            r3 = r30
            goto L_0x034f
        L_0x031b:
            r0 = move-exception
            r2 = r29
            r3 = r30
            goto L_0x0357
        L_0x0321:
            r0 = move-exception
            r2 = r29
            r3 = r30
            goto L_0x035f
        L_0x0327:
            r28 = r4
            r3 = r6
            r2 = r7
            r1 = 0
            net.lingala.zip4j.exception.ZipException r0 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x0347, Exception -> 0x0344, all -> 0x0341 }
            java.lang.String r4 = "invalid offset for start and end of local file, cannot remove file"
            r0.<init>(r4)     // Catch:{ ZipException -> 0x0347, Exception -> 0x0344, all -> 0x0341 }
            throw r0     // Catch:{ ZipException -> 0x0347, Exception -> 0x0344, all -> 0x0341 }
        L_0x0334:
            r28 = r4
            r3 = r6
            r2 = r7
            r1 = 0
            net.lingala.zip4j.exception.ZipException r0 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x0347, Exception -> 0x0344, all -> 0x0341 }
            java.lang.String r4 = "invalid local file header, cannot remove file from archive"
            r0.<init>(r4)     // Catch:{ ZipException -> 0x0347, Exception -> 0x0344, all -> 0x0341 }
            throw r0     // Catch:{ ZipException -> 0x0347, Exception -> 0x0344, all -> 0x0341 }
        L_0x0341:
            r0 = move-exception
            goto L_0x0404
        L_0x0344:
            r0 = move-exception
            goto L_0x03eb
        L_0x0347:
            r0 = move-exception
            goto L_0x03fd
        L_0x034a:
            r0 = move-exception
            r28 = r4
            r3 = r6
            r2 = r7
        L_0x034f:
            r1 = 0
            goto L_0x0404
        L_0x0352:
            r0 = move-exception
            r28 = r4
            r3 = r6
            r2 = r7
        L_0x0357:
            r1 = 0
            goto L_0x03eb
        L_0x035a:
            r0 = move-exception
            r28 = r4
            r3 = r6
            r2 = r7
        L_0x035f:
            r1 = 0
            goto L_0x03fd
        L_0x0362:
            r0 = move-exception
            r3 = r6
            r2 = r7
            r1 = 0
            r7 = 0
            r28 = r7
            goto L_0x0404
        L_0x036b:
            r0 = move-exception
            r3 = r6
            r2 = r7
            r1 = 0
            r7 = 0
            r28 = r7
            goto L_0x03eb
        L_0x0374:
            r0 = move-exception
            r3 = r6
            r2 = r7
            r1 = 0
            r7 = 0
            r28 = r7
            goto L_0x03fd
        L_0x037d:
            r0 = move-exception
            r2 = r7
            r1 = 0
            r7 = 0
            r3 = r7
            r28 = r3
            goto L_0x0404
        L_0x0386:
            r0 = move-exception
            r2 = r7
            r1 = 0
            r7 = 0
            r3 = r7
            r28 = r3
            goto L_0x03eb
        L_0x038f:
            r0 = move-exception
            r2 = r7
            r1 = 0
            r7 = 0
            r3 = r7
            r28 = r3
            goto L_0x03fd
        L_0x0398:
            r0 = move-exception
            r2 = r7
            r1 = 0
            r7 = 0
            net.lingala.zip4j.exception.ZipException r3 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x03a6, Exception -> 0x03a4, all -> 0x03a2 }
            r3.<init>(r0)     // Catch:{ ZipException -> 0x03a6, Exception -> 0x03a4, all -> 0x03a2 }
            throw r3     // Catch:{ ZipException -> 0x03a6, Exception -> 0x03a4, all -> 0x03a2 }
        L_0x03a2:
            r0 = move-exception
            goto L_0x03b4
        L_0x03a4:
            r0 = move-exception
            goto L_0x03b9
        L_0x03a6:
            r0 = move-exception
            goto L_0x03be
        L_0x03a8:
            r0 = move-exception
            r2 = r7
            goto L_0x03b2
        L_0x03ab:
            r0 = move-exception
            r2 = r7
            goto L_0x03b7
        L_0x03ae:
            r0 = move-exception
            r2 = r7
            goto L_0x03bc
        L_0x03b1:
            r0 = move-exception
        L_0x03b2:
            r1 = 0
            r7 = 0
        L_0x03b4:
            r3 = r7
            goto L_0x03df
        L_0x03b6:
            r0 = move-exception
        L_0x03b7:
            r1 = 0
            r7 = 0
        L_0x03b9:
            r3 = r7
            goto L_0x03e8
        L_0x03bb:
            r0 = move-exception
        L_0x03bc:
            r1 = 0
            r7 = 0
        L_0x03be:
            r3 = r7
            goto L_0x03fa
        L_0x03c0:
            r1 = 0
            r7 = 0
            net.lingala.zip4j.exception.ZipException r0 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x03d8, Exception -> 0x03d6, all -> 0x03d4 }
            java.lang.String r2 = "This is a split archive. Zip file format does not allow updating split/spanned files"
            r0.<init>(r2)     // Catch:{ ZipException -> 0x03d8, Exception -> 0x03d6, all -> 0x03d4 }
            throw r0     // Catch:{ ZipException -> 0x03d8, Exception -> 0x03d6, all -> 0x03d4 }
        L_0x03ca:
            r1 = 0
            r7 = 0
            net.lingala.zip4j.exception.ZipException r0 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x03d8, Exception -> 0x03d6, all -> 0x03d4 }
            java.lang.String r2 = "file header not found in zip model, cannot remove file"
            r0.<init>(r2)     // Catch:{ ZipException -> 0x03d8, Exception -> 0x03d6, all -> 0x03d4 }
            throw r0     // Catch:{ ZipException -> 0x03d8, Exception -> 0x03d6, all -> 0x03d4 }
        L_0x03d4:
            r0 = move-exception
            goto L_0x03dd
        L_0x03d6:
            r0 = move-exception
            goto L_0x03e6
        L_0x03d8:
            r0 = move-exception
            goto L_0x03f8
        L_0x03da:
            r0 = move-exception
            r1 = 0
            r7 = 0
        L_0x03dd:
            r2 = r7
            r3 = r2
        L_0x03df:
            r14 = r3
            r28 = r14
            goto L_0x0404
        L_0x03e3:
            r0 = move-exception
            r1 = 0
            r7 = 0
        L_0x03e6:
            r2 = r7
            r3 = r2
        L_0x03e8:
            r14 = r3
            r28 = r14
        L_0x03eb:
            r13 = 0
        L_0x03ec:
            r10.endProgressMonitorError(r0)     // Catch:{ all -> 0x0402 }
            net.lingala.zip4j.exception.ZipException r1 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x0402 }
            r1.<init>(r0)     // Catch:{ all -> 0x0402 }
            throw r1     // Catch:{ all -> 0x0402 }
        L_0x03f5:
            r0 = move-exception
            r1 = 0
            r7 = 0
        L_0x03f8:
            r2 = r7
            r3 = r2
        L_0x03fa:
            r14 = r3
            r28 = r14
        L_0x03fd:
            r13 = 0
        L_0x03fe:
            r10.endProgressMonitorError(r0)     // Catch:{ all -> 0x0402 }
            throw r0     // Catch:{ all -> 0x0402 }
        L_0x0402:
            r0 = move-exception
            r1 = r13
        L_0x0404:
            if (r28 == 0) goto L_0x0409
            r28.close()     // Catch:{ IOException -> 0x040f }
        L_0x0409:
            if (r14 == 0) goto L_0x0415
            r14.close()     // Catch:{ IOException -> 0x040f }
            goto L_0x0415
        L_0x040f:
            net.lingala.zip4j.exception.ZipException r0 = new net.lingala.zip4j.exception.ZipException
            r0.<init>(r11)
            throw r0
        L_0x0415:
            if (r1 == 0) goto L_0x041b
            r9.restoreFileName(r3, r2)
            goto L_0x0423
        L_0x041b:
            java.io.File r1 = new java.io.File
            r1.<init>(r2)
            r1.delete()
        L_0x0423:
            throw r0
        L_0x0424:
            net.lingala.zip4j.exception.ZipException r0 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r1 = "input parameters is null in maintain zip file, cannot remove file from archive"
            r0.<init>(r1)
            goto L_0x042d
        L_0x042c:
            throw r0
        L_0x042d:
            goto L_0x042c
        */
        throw new UnsupportedOperationException("Method not decompiled: net.lingala.zip4j.util.ArchiveMaintainer.initRemoveZipFile(net.lingala.zip4j.model.ZipModel, net.lingala.zip4j.model.FileHeader, net.lingala.zip4j.progress.ProgressMonitor):java.util.HashMap");
    }

    private void restoreFileName(File file, String str) {
        if (!file.delete()) {
            throw new ZipException("cannot delete old zip file");
        } else if (!new File(str).renameTo(file)) {
            throw new ZipException("cannot rename modified zip file");
        }
    }

    private void copyFile(RandomAccessFile randomAccessFile, OutputStream outputStream, long j, long j2, ProgressMonitor progressMonitor) {
        byte[] bArr;
        if (randomAccessFile == null || outputStream == null) {
            throw new ZipException("input or output stream is null, cannot copy file");
        }
        long j3 = 0;
        if (j < 0) {
            throw new ZipException("starting offset is negative, cannot copy file");
        } else if (j2 < 0) {
            throw new ZipException("end offset is negative, cannot copy file");
        } else if (j > j2) {
            throw new ZipException("start offset is greater than end offset, cannot copy file");
        } else if (j != j2) {
            if (progressMonitor.isCancelAllTasks()) {
                progressMonitor.setResult(3);
                progressMonitor.setState(0);
                return;
            }
            try {
                randomAccessFile.seek(j);
                long j4 = j2 - j;
                if (j4 < 4096) {
                    bArr = new byte[((int) j4)];
                } else {
                    bArr = new byte[4096];
                }
                while (true) {
                    int read = randomAccessFile.read(bArr);
                    if (read != -1) {
                        outputStream.write(bArr, 0, read);
                        long j5 = (long) read;
                        progressMonitor.updateWorkCompleted(j5);
                        if (progressMonitor.isCancelAllTasks()) {
                            progressMonitor.setResult(3);
                            return;
                        }
                        j3 += j5;
                        if (j3 != j4) {
                            if (((long) bArr.length) + j3 > j4) {
                                bArr = new byte[((int) (j4 - j3))];
                            }
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                }
            } catch (IOException e) {
                throw new ZipException(e);
            } catch (Exception e2) {
                throw new ZipException(e2);
            }
        }
    }

    private RandomAccessFile createFileHandler(ZipModel zipModel, String str) {
        if (zipModel == null || !Zip4jUtil.isStringNotNullAndNotEmpty(zipModel.getZipFile())) {
            throw new ZipException("input parameter is null in getFilePointer, cannot create file handler to remove file");
        }
        try {
            return new RandomAccessFile(new File(zipModel.getZipFile()), str);
        } catch (FileNotFoundException e) {
            throw new ZipException(e);
        }
    }

    public void mergeSplitZipFiles(ZipModel zipModel, File file, ProgressMonitor progressMonitor, boolean z) {
        if (z) {
            final ZipModel zipModel2 = zipModel;
            final File file2 = file;
            final ProgressMonitor progressMonitor2 = progressMonitor;
            new Thread(InternalZipConstants.THREAD_NAME) {
                public void run() {
                    try {
                        ArchiveMaintainer.this.initMergeSplitZipFile(zipModel2, file2, progressMonitor2);
                    } catch (ZipException unused) {
                    }
                }
            }.start();
            return;
        }
        initMergeSplitZipFile(zipModel, file, progressMonitor);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:127:?, code lost:
        r13.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00e4, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00e5, code lost:
        r7 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00eb, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00ec, code lost:
        r1 = r13;
        r7 = r24;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x0181 A[SYNTHETIC, Splitter:B:126:0x0181] */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x0188 A[SYNTHETIC, Splitter:B:130:0x0188] */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x00b9 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0080 A[Catch:{ IOException -> 0x0079, Exception -> 0x0076, all -> 0x0073 }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00e4 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:40:0x00a8] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00eb A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:40:0x00a8] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void initMergeSplitZipFile(net.lingala.zip4j.model.ZipModel r26, java.io.File r27, net.lingala.zip4j.progress.ProgressMonitor r28) {
        /*
            r25 = this;
            r9 = r25
            r0 = r26
            r10 = r28
            if (r0 == 0) goto L_0x0198
            boolean r1 = r26.isSplitArchive()
            if (r1 == 0) goto L_0x018c
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            r1 = 0
            net.lingala.zip4j.model.EndCentralDirRecord r2 = r26.getEndCentralDirRecord()     // Catch:{ IOException -> 0x0171, Exception -> 0x0165, all -> 0x0160 }
            int r12 = r2.getNoOfThisDisk()     // Catch:{ IOException -> 0x0171, Exception -> 0x0165, all -> 0x0160 }
            if (r12 <= 0) goto L_0x0151
            r2 = r27
            java.io.OutputStream r13 = r9.prepareOutputStreamForMerge(r2)     // Catch:{ IOException -> 0x0171, Exception -> 0x0165, all -> 0x0160 }
            r14 = 0
            r2 = r1
            r4 = r14
            r1 = 0
            r6 = 0
        L_0x002a:
            if (r6 > r12) goto L_0x010e
            java.io.RandomAccessFile r7 = r9.createSplitZipFileHandler(r0, r6)     // Catch:{ IOException -> 0x014b, Exception -> 0x0145, all -> 0x0140 }
            java.lang.Long r2 = new java.lang.Long     // Catch:{ IOException -> 0x0108, Exception -> 0x0102, all -> 0x00fb }
            long r8 = r7.length()     // Catch:{ IOException -> 0x0108, Exception -> 0x0102, all -> 0x00fb }
            r2.<init>(r8)     // Catch:{ IOException -> 0x0108, Exception -> 0x0102, all -> 0x00fb }
            r8 = 4
            if (r6 != 0) goto L_0x007c
            net.lingala.zip4j.model.CentralDirectory r3 = r26.getCentralDirectory()     // Catch:{ IOException -> 0x0079, Exception -> 0x0076, all -> 0x0073 }
            if (r3 == 0) goto L_0x007c
            net.lingala.zip4j.model.CentralDirectory r3 = r26.getCentralDirectory()     // Catch:{ IOException -> 0x0079, Exception -> 0x0076, all -> 0x0073 }
            java.util.ArrayList r3 = r3.getFileHeaders()     // Catch:{ IOException -> 0x0079, Exception -> 0x0076, all -> 0x0073 }
            if (r3 == 0) goto L_0x007c
            net.lingala.zip4j.model.CentralDirectory r3 = r26.getCentralDirectory()     // Catch:{ IOException -> 0x0079, Exception -> 0x0076, all -> 0x0073 }
            java.util.ArrayList r3 = r3.getFileHeaders()     // Catch:{ IOException -> 0x0079, Exception -> 0x0076, all -> 0x0073 }
            int r3 = r3.size()     // Catch:{ IOException -> 0x0079, Exception -> 0x0076, all -> 0x0073 }
            if (r3 <= 0) goto L_0x007c
            byte[] r3 = new byte[r8]     // Catch:{ IOException -> 0x0079, Exception -> 0x0076, all -> 0x0073 }
            r7.seek(r14)     // Catch:{ IOException -> 0x0079, Exception -> 0x0076, all -> 0x0073 }
            r7.read(r3)     // Catch:{ IOException -> 0x0079, Exception -> 0x0076, all -> 0x0073 }
            r9 = 0
            int r3 = net.lingala.zip4j.util.Raw.readIntLittleEndian(r3, r9)     // Catch:{ IOException -> 0x0079, Exception -> 0x0076, all -> 0x0073 }
            long r8 = (long) r3     // Catch:{ IOException -> 0x0079, Exception -> 0x0076, all -> 0x0073 }
            r16 = 134695760(0x8074b50, double:6.65485477E-316)
            int r3 = (r8 > r16 ? 1 : (r8 == r16 ? 0 : -1))
            if (r3 != 0) goto L_0x007c
            r1 = 1
            r1 = 4
            r9 = 1
            goto L_0x007e
        L_0x0073:
            r0 = move-exception
            goto L_0x00fe
        L_0x0076:
            r0 = move-exception
            goto L_0x0105
        L_0x0079:
            r0 = move-exception
            goto L_0x010b
        L_0x007c:
            r9 = r1
            r1 = 0
        L_0x007e:
            if (r6 != r12) goto L_0x008d
            java.lang.Long r2 = new java.lang.Long     // Catch:{ IOException -> 0x0079, Exception -> 0x0076, all -> 0x0073 }
            net.lingala.zip4j.model.EndCentralDirRecord r3 = r26.getEndCentralDirRecord()     // Catch:{ IOException -> 0x0079, Exception -> 0x0076, all -> 0x0073 }
            long r14 = r3.getOffsetOfStartOfCentralDir()     // Catch:{ IOException -> 0x0079, Exception -> 0x0076, all -> 0x0073 }
            r2.<init>(r14)     // Catch:{ IOException -> 0x0079, Exception -> 0x0076, all -> 0x0073 }
        L_0x008d:
            r14 = r2
            long r2 = (long) r1
            long r18 = r14.longValue()     // Catch:{ IOException -> 0x0108, Exception -> 0x0102, all -> 0x00fb }
            r1 = r25
            r20 = r2
            r2 = r7
            r3 = r13
            r15 = r11
            r22 = r12
            r11 = r4
            r4 = r20
            r23 = r6
            r24 = r7
            r6 = r18
            r0 = 0
            r8 = r28
            r1.copyFile(r2, r3, r4, r6, r8)     // Catch:{ IOException -> 0x00f3, Exception -> 0x00eb, all -> 0x00e4 }
            long r1 = r14.longValue()     // Catch:{ IOException -> 0x00f3, Exception -> 0x00eb, all -> 0x00e4 }
            long r1 = r1 - r20
            long r4 = r11 + r1
            boolean r1 = r28.isCancelAllTasks()     // Catch:{ IOException -> 0x00f3, Exception -> 0x00eb, all -> 0x00e4 }
            if (r1 == 0) goto L_0x00cd
            r1 = 3
            r10.setResult(r1)     // Catch:{ IOException -> 0x00f3, Exception -> 0x00eb, all -> 0x00e4 }
            r10.setState(r0)     // Catch:{ IOException -> 0x00f3, Exception -> 0x00eb, all -> 0x00e4 }
            if (r13 == 0) goto L_0x00c7
            r13.close()     // Catch:{ IOException -> 0x00c6 }
            goto L_0x00c7
        L_0x00c6:
        L_0x00c7:
            if (r24 == 0) goto L_0x00cc
            r24.close()     // Catch:{ IOException -> 0x00cc }
        L_0x00cc:
            return
        L_0x00cd:
            r3 = r15
            r3.add(r14)     // Catch:{ IOException -> 0x00f3, Exception -> 0x00eb, all -> 0x00e4 }
            r24.close()     // Catch:{ IOException -> 0x00d4, Exception -> 0x00eb, all -> 0x00e4 }
        L_0x00d4:
            int r6 = r23 + 1
            r0 = r26
            r11 = r3
            r1 = r9
            r12 = r22
            r2 = r24
            r14 = 0
            r9 = r25
            goto L_0x002a
        L_0x00e4:
            r0 = move-exception
            r4 = r25
            r7 = r24
            goto L_0x017f
        L_0x00eb:
            r0 = move-exception
            r4 = r25
            r1 = r13
            r7 = r24
            goto L_0x0168
        L_0x00f3:
            r0 = move-exception
            r4 = r25
            r1 = r13
            r7 = r24
            goto L_0x0174
        L_0x00fb:
            r0 = move-exception
            r24 = r7
        L_0x00fe:
            r4 = r25
            goto L_0x017f
        L_0x0102:
            r0 = move-exception
            r24 = r7
        L_0x0105:
            r4 = r25
            goto L_0x0149
        L_0x0108:
            r0 = move-exception
            r24 = r7
        L_0x010b:
            r4 = r25
            goto L_0x014f
        L_0x010e:
            r3 = r11
            r11 = r4
            java.lang.Object r0 = r26.clone()     // Catch:{ IOException -> 0x014b, Exception -> 0x0145, all -> 0x0140 }
            net.lingala.zip4j.model.ZipModel r0 = (net.lingala.zip4j.model.ZipModel) r0     // Catch:{ IOException -> 0x014b, Exception -> 0x0145, all -> 0x0140 }
            net.lingala.zip4j.model.EndCentralDirRecord r4 = r0.getEndCentralDirRecord()     // Catch:{ IOException -> 0x014b, Exception -> 0x0145, all -> 0x0140 }
            r4.setOffsetOfStartOfCentralDir(r11)     // Catch:{ IOException -> 0x014b, Exception -> 0x0145, all -> 0x0140 }
            r4 = r25
            r4.updateSplitZipModel(r0, r3, r1)     // Catch:{ IOException -> 0x013e, Exception -> 0x013c, all -> 0x013a }
            net.lingala.zip4j.core.HeaderWriter r1 = new net.lingala.zip4j.core.HeaderWriter     // Catch:{ IOException -> 0x013e, Exception -> 0x013c, all -> 0x013a }
            r1.<init>()     // Catch:{ IOException -> 0x013e, Exception -> 0x013c, all -> 0x013a }
            r1.finalizeZipFileWithoutValidations(r0, r13)     // Catch:{ IOException -> 0x013e, Exception -> 0x013c, all -> 0x013a }
            r28.endProgressMonitorSuccess()     // Catch:{ IOException -> 0x013e, Exception -> 0x013c, all -> 0x013a }
            if (r13 == 0) goto L_0x0134
            r13.close()     // Catch:{ IOException -> 0x0133 }
            goto L_0x0134
        L_0x0133:
        L_0x0134:
            if (r2 == 0) goto L_0x0139
            r2.close()     // Catch:{ IOException -> 0x0139 }
        L_0x0139:
            return
        L_0x013a:
            r0 = move-exception
            goto L_0x0143
        L_0x013c:
            r0 = move-exception
            goto L_0x0148
        L_0x013e:
            r0 = move-exception
            goto L_0x014e
        L_0x0140:
            r0 = move-exception
            r4 = r25
        L_0x0143:
            r7 = r2
            goto L_0x017f
        L_0x0145:
            r0 = move-exception
            r4 = r25
        L_0x0148:
            r7 = r2
        L_0x0149:
            r1 = r13
            goto L_0x0168
        L_0x014b:
            r0 = move-exception
            r4 = r25
        L_0x014e:
            r7 = r2
        L_0x014f:
            r1 = r13
            goto L_0x0174
        L_0x0151:
            r4 = r9
            net.lingala.zip4j.exception.ZipException r0 = new net.lingala.zip4j.exception.ZipException     // Catch:{ IOException -> 0x015e, Exception -> 0x015c, all -> 0x015a }
            java.lang.String r2 = "corrupt zip model, archive not a split zip file"
            r0.<init>(r2)     // Catch:{ IOException -> 0x015e, Exception -> 0x015c, all -> 0x015a }
            throw r0     // Catch:{ IOException -> 0x015e, Exception -> 0x015c, all -> 0x015a }
        L_0x015a:
            r0 = move-exception
            goto L_0x0162
        L_0x015c:
            r0 = move-exception
            goto L_0x0167
        L_0x015e:
            r0 = move-exception
            goto L_0x0173
        L_0x0160:
            r0 = move-exception
            r4 = r9
        L_0x0162:
            r7 = r1
            r13 = r7
            goto L_0x017f
        L_0x0165:
            r0 = move-exception
            r4 = r9
        L_0x0167:
            r7 = r1
        L_0x0168:
            r10.endProgressMonitorError(r0)     // Catch:{ all -> 0x017d }
            net.lingala.zip4j.exception.ZipException r2 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x017d }
            r2.<init>(r0)     // Catch:{ all -> 0x017d }
            throw r2     // Catch:{ all -> 0x017d }
        L_0x0171:
            r0 = move-exception
            r4 = r9
        L_0x0173:
            r7 = r1
        L_0x0174:
            r10.endProgressMonitorError(r0)     // Catch:{ all -> 0x017d }
            net.lingala.zip4j.exception.ZipException r2 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x017d }
            r2.<init>(r0)     // Catch:{ all -> 0x017d }
            throw r2     // Catch:{ all -> 0x017d }
        L_0x017d:
            r0 = move-exception
            r13 = r1
        L_0x017f:
            if (r13 == 0) goto L_0x0186
            r13.close()     // Catch:{ IOException -> 0x0185 }
            goto L_0x0186
        L_0x0185:
        L_0x0186:
            if (r7 == 0) goto L_0x018b
            r7.close()     // Catch:{ IOException -> 0x018b }
        L_0x018b:
            throw r0
        L_0x018c:
            r4 = r9
            net.lingala.zip4j.exception.ZipException r0 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r1 = "archive not a split zip file"
            r0.<init>(r1)
            r10.endProgressMonitorError(r0)
            throw r0
        L_0x0198:
            r4 = r9
            net.lingala.zip4j.exception.ZipException r0 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r1 = "one of the input parameters is null, cannot merge split zip file"
            r0.<init>(r1)
            r10.endProgressMonitorError(r0)
            goto L_0x01a5
        L_0x01a4:
            throw r0
        L_0x01a5:
            goto L_0x01a4
        */
        throw new UnsupportedOperationException("Method not decompiled: net.lingala.zip4j.util.ArchiveMaintainer.initMergeSplitZipFile(net.lingala.zip4j.model.ZipModel, java.io.File, net.lingala.zip4j.progress.ProgressMonitor):void");
    }

    private RandomAccessFile createSplitZipFileHandler(ZipModel zipModel, int i) {
        String str;
        if (zipModel == null) {
            throw new ZipException("zip model is null, cannot create split file handler");
        } else if (i >= 0) {
            try {
                String zipFile = zipModel.getZipFile();
                if (i == zipModel.getEndCentralDirRecord().getNoOfThisDisk()) {
                    str = zipModel.getZipFile();
                } else if (i >= 9) {
                    str = zipFile.substring(0, zipFile.lastIndexOf(".")) + ".z" + (i + 1);
                } else {
                    str = zipFile.substring(0, zipFile.lastIndexOf(".")) + ".z0" + (i + 1);
                }
                File file = new File(str);
                if (Zip4jUtil.checkFileExists(file)) {
                    return new RandomAccessFile(file, InternalZipConstants.READ_MODE);
                }
                throw new ZipException("split file does not exist: " + str);
            } catch (FileNotFoundException e) {
                throw new ZipException(e);
            } catch (Exception e2) {
                throw new ZipException(e2);
            }
        } else {
            throw new ZipException("invlaid part number, cannot create split file handler");
        }
    }

    private OutputStream prepareOutputStreamForMerge(File file) {
        if (file != null) {
            try {
                return new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                throw new ZipException(e);
            } catch (Exception e2) {
                throw new ZipException(e2);
            }
        } else {
            throw new ZipException("outFile is null, cannot create outputstream");
        }
    }

    private void updateSplitZipModel(ZipModel zipModel, ArrayList arrayList, boolean z) {
        if (zipModel != null) {
            zipModel.setSplitArchive(false);
            updateSplitFileHeader(zipModel, arrayList, z);
            updateSplitEndCentralDirectory(zipModel);
            if (zipModel.isZip64Format()) {
                updateSplitZip64EndCentralDirLocator(zipModel, arrayList);
                updateSplitZip64EndCentralDirRec(zipModel, arrayList);
                return;
            }
            return;
        }
        throw new ZipException("zip model is null, cannot update split zip model");
    }

    private void updateSplitFileHeader(ZipModel zipModel, ArrayList arrayList, boolean z) {
        try {
            if (zipModel.getCentralDirectory() != null) {
                int size = zipModel.getCentralDirectory().getFileHeaders().size();
                int i = z ? 4 : 0;
                for (int i2 = 0; i2 < size; i2++) {
                    long j = 0;
                    for (int i3 = 0; i3 < ((FileHeader) zipModel.getCentralDirectory().getFileHeaders().get(i2)).getDiskNumberStart(); i3++) {
                        j += ((Long) arrayList.get(i3)).longValue();
                    }
                    ((FileHeader) zipModel.getCentralDirectory().getFileHeaders().get(i2)).setOffsetLocalHeader((((FileHeader) zipModel.getCentralDirectory().getFileHeaders().get(i2)).getOffsetLocalHeader() + j) - ((long) i));
                    ((FileHeader) zipModel.getCentralDirectory().getFileHeaders().get(i2)).setDiskNumberStart(0);
                }
                return;
            }
            throw new ZipException("corrupt zip model - getCentralDirectory, cannot update split zip model");
        } catch (ZipException e) {
            throw e;
        } catch (Exception e2) {
            throw new ZipException(e2);
        }
    }

    private void updateSplitEndCentralDirectory(ZipModel zipModel) {
        if (zipModel != null) {
            try {
                if (zipModel.getCentralDirectory() != null) {
                    zipModel.getEndCentralDirRecord().setNoOfThisDisk(0);
                    zipModel.getEndCentralDirRecord().setNoOfThisDiskStartOfCentralDir(0);
                    zipModel.getEndCentralDirRecord().setTotNoOfEntriesInCentralDir(zipModel.getCentralDirectory().getFileHeaders().size());
                    zipModel.getEndCentralDirRecord().setTotNoOfEntriesInCentralDirOnThisDisk(zipModel.getCentralDirectory().getFileHeaders().size());
                    return;
                }
                throw new ZipException("corrupt zip model - getCentralDirectory, cannot update split zip model");
            } catch (ZipException e) {
                throw e;
            } catch (Exception e2) {
                throw new ZipException(e2);
            }
        } else {
            throw new ZipException("zip model is null - cannot update end of central directory for split zip model");
        }
    }

    private void updateSplitZip64EndCentralDirLocator(ZipModel zipModel, ArrayList arrayList) {
        if (zipModel == null) {
            throw new ZipException("zip model is null, cannot update split Zip64 end of central directory locator");
        } else if (zipModel.getZip64EndCentralDirLocator() != null) {
            zipModel.getZip64EndCentralDirLocator().setNoOfDiskStartOfZip64EndOfCentralDirRec(0);
            long j = 0;
            for (int i = 0; i < arrayList.size(); i++) {
                j += ((Long) arrayList.get(i)).longValue();
            }
            zipModel.getZip64EndCentralDirLocator().setOffsetZip64EndOfCentralDirRec(zipModel.getZip64EndCentralDirLocator().getOffsetZip64EndOfCentralDirRec() + j);
            zipModel.getZip64EndCentralDirLocator().setTotNumberOfDiscs(1);
        }
    }

    private void updateSplitZip64EndCentralDirRec(ZipModel zipModel, ArrayList arrayList) {
        if (zipModel == null) {
            throw new ZipException("zip model is null, cannot update split Zip64 end of central directory record");
        } else if (zipModel.getZip64EndCentralDirRecord() != null) {
            zipModel.getZip64EndCentralDirRecord().setNoOfThisDisk(0);
            zipModel.getZip64EndCentralDirRecord().setNoOfThisDiskStartOfCentralDir(0);
            zipModel.getZip64EndCentralDirRecord().setTotNoOfEntriesInCentralDirOnThisDisk((long) zipModel.getEndCentralDirRecord().getTotNoOfEntriesInCentralDir());
            long j = 0;
            for (int i = 0; i < arrayList.size(); i++) {
                j += ((Long) arrayList.get(i)).longValue();
            }
            zipModel.getZip64EndCentralDirRecord().setOffsetStartCenDirWRTStartDiskNo(zipModel.getZip64EndCentralDirRecord().getOffsetStartCenDirWRTStartDiskNo() + j);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x009a A[SYNTHETIC, Splitter:B:38:0x009a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setComment(net.lingala.zip4j.model.ZipModel r6, java.lang.String r7) {
        /*
            r5 = this;
            if (r7 == 0) goto L_0x00ae
            if (r6 == 0) goto L_0x00a6
            byte[] r0 = r7.getBytes()
            int r1 = r7.length()
            java.lang.String r2 = "UTF8"
            boolean r3 = net.lingala.zip4j.util.Zip4jUtil.isSupportedCharset(r2)
            if (r3 == 0) goto L_0x0032
            java.lang.String r0 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x002a }
            byte[] r1 = r7.getBytes(r2)     // Catch:{ UnsupportedEncodingException -> 0x002a }
            r0.<init>(r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x002a }
            byte[] r1 = r0.getBytes(r2)     // Catch:{ UnsupportedEncodingException -> 0x002a }
            int r7 = r0.length()     // Catch:{ UnsupportedEncodingException -> 0x002a }
            r4 = r1
            r1 = r7
            r7 = r0
            r0 = r4
            goto L_0x0032
        L_0x002a:
            byte[] r0 = r7.getBytes()
            int r1 = r7.length()
        L_0x0032:
            r2 = 65535(0xffff, float:9.1834E-41)
            if (r1 > r2) goto L_0x009e
            net.lingala.zip4j.model.EndCentralDirRecord r2 = r6.getEndCentralDirRecord()
            r2.setComment(r7)
            net.lingala.zip4j.model.EndCentralDirRecord r7 = r6.getEndCentralDirRecord()
            r7.setCommentBytes(r0)
            net.lingala.zip4j.model.EndCentralDirRecord r7 = r6.getEndCentralDirRecord()
            r7.setCommentLength(r1)
            r7 = 0
            net.lingala.zip4j.core.HeaderWriter r0 = new net.lingala.zip4j.core.HeaderWriter     // Catch:{ FileNotFoundException -> 0x0091, IOException -> 0x008a }
            r0.<init>()     // Catch:{ FileNotFoundException -> 0x0091, IOException -> 0x008a }
            net.lingala.zip4j.io.SplitOutputStream r1 = new net.lingala.zip4j.io.SplitOutputStream     // Catch:{ FileNotFoundException -> 0x0091, IOException -> 0x008a }
            java.lang.String r2 = r6.getZipFile()     // Catch:{ FileNotFoundException -> 0x0091, IOException -> 0x008a }
            r1.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0091, IOException -> 0x008a }
            boolean r7 = r6.isZip64Format()     // Catch:{ FileNotFoundException -> 0x0085, IOException -> 0x0082, all -> 0x007f }
            if (r7 == 0) goto L_0x006d
            net.lingala.zip4j.model.Zip64EndCentralDirRecord r7 = r6.getZip64EndCentralDirRecord()     // Catch:{ FileNotFoundException -> 0x0085, IOException -> 0x0082, all -> 0x007f }
            long r2 = r7.getOffsetStartCenDirWRTStartDiskNo()     // Catch:{ FileNotFoundException -> 0x0085, IOException -> 0x0082, all -> 0x007f }
            r1.seek(r2)     // Catch:{ FileNotFoundException -> 0x0085, IOException -> 0x0082, all -> 0x007f }
            goto L_0x0078
        L_0x006d:
            net.lingala.zip4j.model.EndCentralDirRecord r7 = r6.getEndCentralDirRecord()     // Catch:{ FileNotFoundException -> 0x0085, IOException -> 0x0082, all -> 0x007f }
            long r2 = r7.getOffsetOfStartOfCentralDir()     // Catch:{ FileNotFoundException -> 0x0085, IOException -> 0x0082, all -> 0x007f }
            r1.seek(r2)     // Catch:{ FileNotFoundException -> 0x0085, IOException -> 0x0082, all -> 0x007f }
        L_0x0078:
            r0.finalizeZipFileWithoutValidations(r6, r1)     // Catch:{ FileNotFoundException -> 0x0085, IOException -> 0x0082, all -> 0x007f }
            r1.close()     // Catch:{ IOException -> 0x007e }
        L_0x007e:
            return
        L_0x007f:
            r6 = move-exception
            r7 = r1
            goto L_0x0098
        L_0x0082:
            r6 = move-exception
            r7 = r1
            goto L_0x008b
        L_0x0085:
            r6 = move-exception
            r7 = r1
            goto L_0x0092
        L_0x0088:
            r6 = move-exception
            goto L_0x0098
        L_0x008a:
            r6 = move-exception
        L_0x008b:
            net.lingala.zip4j.exception.ZipException r0 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x0088 }
            r0.<init>(r6)     // Catch:{ all -> 0x0088 }
            throw r0     // Catch:{ all -> 0x0088 }
        L_0x0091:
            r6 = move-exception
        L_0x0092:
            net.lingala.zip4j.exception.ZipException r0 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x0088 }
            r0.<init>(r6)     // Catch:{ all -> 0x0088 }
            throw r0     // Catch:{ all -> 0x0088 }
        L_0x0098:
            if (r7 == 0) goto L_0x009d
            r7.close()     // Catch:{ IOException -> 0x009d }
        L_0x009d:
            throw r6
        L_0x009e:
            net.lingala.zip4j.exception.ZipException r6 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r7 = "comment length exceeds maximum length"
            r6.<init>(r7)
            throw r6
        L_0x00a6:
            net.lingala.zip4j.exception.ZipException r6 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r7 = "zipModel is null, cannot update Zip file with comment"
            r6.<init>(r7)
            throw r6
        L_0x00ae:
            net.lingala.zip4j.exception.ZipException r6 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r7 = "comment is null, cannot update Zip file with comment"
            r6.<init>(r7)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: net.lingala.zip4j.util.ArchiveMaintainer.setComment(net.lingala.zip4j.model.ZipModel, java.lang.String):void");
    }

    public void initProgressMonitorForRemoveOp(ZipModel zipModel, FileHeader fileHeader, ProgressMonitor progressMonitor) {
        if (zipModel == null || fileHeader == null || progressMonitor == null) {
            throw new ZipException("one of the input parameters is null, cannot calculate total work");
        }
        progressMonitor.setCurrentOperation(2);
        progressMonitor.setFileName(fileHeader.getFileName());
        progressMonitor.setTotalWork(calculateTotalWorkForRemoveOp(zipModel, fileHeader));
        progressMonitor.setState(1);
    }

    private long calculateTotalWorkForRemoveOp(ZipModel zipModel, FileHeader fileHeader) {
        return Zip4jUtil.getFileLengh(new File(zipModel.getZipFile())) - fileHeader.getCompressedSize();
    }

    public void initProgressMonitorForMergeOp(ZipModel zipModel, ProgressMonitor progressMonitor) {
        if (zipModel != null) {
            progressMonitor.setCurrentOperation(4);
            progressMonitor.setFileName(zipModel.getZipFile());
            progressMonitor.setTotalWork(calculateTotalWorkForMergeOp(zipModel));
            progressMonitor.setState(1);
            return;
        }
        throw new ZipException("zip model is null, cannot calculate total work for merge op");
    }

    private long calculateTotalWorkForMergeOp(ZipModel zipModel) {
        String str;
        if (!zipModel.isSplitArchive()) {
            return 0;
        }
        int noOfThisDisk = zipModel.getEndCentralDirRecord().getNoOfThisDisk();
        String zipFile = zipModel.getZipFile();
        long j = 0;
        for (int i = 0; i <= noOfThisDisk; i++) {
            if (zipModel.getEndCentralDirRecord().getNoOfThisDisk() == 0) {
                str = zipModel.getZipFile();
            } else {
                str = zipFile.substring(0, zipFile.lastIndexOf(".")) + ".z0" + 1;
            }
            j += Zip4jUtil.getFileLengh(new File(str));
        }
        return j;
    }
}
