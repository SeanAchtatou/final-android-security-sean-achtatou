package net.lingala.zip4j.crypto;

public interface IDecrypter {
    int decryptData(byte[] bArr);

    int decryptData(byte[] bArr, int i, int i2);
}
