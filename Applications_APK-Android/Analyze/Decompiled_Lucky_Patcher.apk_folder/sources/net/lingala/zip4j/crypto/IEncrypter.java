package net.lingala.zip4j.crypto;

public interface IEncrypter {
    int encryptData(byte[] bArr);

    int encryptData(byte[] bArr, int i, int i2);
}
