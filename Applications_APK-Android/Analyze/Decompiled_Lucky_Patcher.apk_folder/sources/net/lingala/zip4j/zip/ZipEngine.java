package net.lingala.zip4j.zip;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.EndCentralDirRecord;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.progress.ProgressMonitor;
import net.lingala.zip4j.util.ArchiveMaintainer;
import net.lingala.zip4j.util.InternalZipConstants;
import net.lingala.zip4j.util.Zip4jUtil;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

public class ZipEngine {
    private ZipModel zipModel;

    public ZipEngine(ZipModel zipModel2) {
        if (zipModel2 != null) {
            this.zipModel = zipModel2;
            return;
        }
        throw new ZipException("zip model is null in ZipEngine constructor");
    }

    public void addFiles(ArrayList arrayList, ZipParameters zipParameters, ProgressMonitor progressMonitor, boolean z) {
        if (arrayList == null || zipParameters == null) {
            throw new ZipException("one of the input parameters is null when adding files");
        } else if (arrayList.size() > 0) {
            progressMonitor.setCurrentOperation(0);
            progressMonitor.setState(1);
            progressMonitor.setResult(1);
            if (z) {
                progressMonitor.setTotalWork(calculateTotalWork(arrayList, zipParameters));
                progressMonitor.setFileName(((File) arrayList.get(0)).getAbsolutePath());
                final ArrayList arrayList2 = arrayList;
                final ZipParameters zipParameters2 = zipParameters;
                final ProgressMonitor progressMonitor2 = progressMonitor;
                new Thread(InternalZipConstants.THREAD_NAME) {
                    public void run() {
                        try {
                            ZipEngine.this.initAddFiles(arrayList2, zipParameters2, progressMonitor2);
                        } catch (ZipException unused) {
                        }
                    }
                }.start();
                return;
            }
            initAddFiles(arrayList, zipParameters, progressMonitor);
        } else {
            throw new ZipException("no files to add");
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't wrap try/catch for region: R(15:4|(1:6)|7|8|9|(3:11|12|(1:14)(2:15|16))|17|18|(4:22|(6:129|24|(2:26|27)|28|29|134)(6:31|32|(5:34|(2:38|(6:128|40|(2:42|43)|44|45|136))|47|48|(1:50))|51|(2:56|(2:58|132)(2:59|(3:60|61|(2:63|(7:127|65|66|67|68|69|138)(2:71|72))(4:133|73|74|130))))(2:55|131)|75)|19|20)|82|83|(2:85|86)|87|88|140) */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:128|40|(2:42|43)|44|45|136) */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:129|24|(2:26|27)|28|29|134) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:127|65|66|67|68|69|138) */
    /* JADX WARNING: Code restructure failed: missing block: B:135:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:28:0x0088 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:44:0x00e0 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:68:0x0153 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:87:0x0180 */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x01ae A[SYNTHETIC, Splitter:B:115:0x01ae] */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x01b5 A[SYNTHETIC, Splitter:B:119:0x01b5] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void initAddFiles(java.util.ArrayList r13, net.lingala.zip4j.model.ZipParameters r14, net.lingala.zip4j.progress.ProgressMonitor r15) {
        /*
            r12 = this;
            if (r13 == 0) goto L_0x01c1
            if (r14 == 0) goto L_0x01c1
            int r0 = r13.size()
            if (r0 <= 0) goto L_0x01b9
            net.lingala.zip4j.model.ZipModel r0 = r12.zipModel
            net.lingala.zip4j.model.EndCentralDirRecord r0 = r0.getEndCentralDirRecord()
            if (r0 != 0) goto L_0x001b
            net.lingala.zip4j.model.ZipModel r0 = r12.zipModel
            net.lingala.zip4j.model.EndCentralDirRecord r1 = r12.createEndOfCentralDirectoryRecord()
            r0.setEndCentralDirRecord(r1)
        L_0x001b:
            r0 = 0
            r12.checkParameters(r14)     // Catch:{ ZipException -> 0x01a4, Exception -> 0x0199, all -> 0x0195 }
            r12.removeFilesIfExists(r13, r14, r15)     // Catch:{ ZipException -> 0x01a4, Exception -> 0x0199, all -> 0x0195 }
            net.lingala.zip4j.model.ZipModel r1 = r12.zipModel     // Catch:{ ZipException -> 0x01a4, Exception -> 0x0199, all -> 0x0195 }
            java.lang.String r1 = r1.getZipFile()     // Catch:{ ZipException -> 0x01a4, Exception -> 0x0199, all -> 0x0195 }
            boolean r1 = net.lingala.zip4j.util.Zip4jUtil.checkFileExists(r1)     // Catch:{ ZipException -> 0x01a4, Exception -> 0x0199, all -> 0x0195 }
            net.lingala.zip4j.io.SplitOutputStream r2 = new net.lingala.zip4j.io.SplitOutputStream     // Catch:{ ZipException -> 0x01a4, Exception -> 0x0199, all -> 0x0195 }
            java.io.File r3 = new java.io.File     // Catch:{ ZipException -> 0x01a4, Exception -> 0x0199, all -> 0x0195 }
            net.lingala.zip4j.model.ZipModel r4 = r12.zipModel     // Catch:{ ZipException -> 0x01a4, Exception -> 0x0199, all -> 0x0195 }
            java.lang.String r4 = r4.getZipFile()     // Catch:{ ZipException -> 0x01a4, Exception -> 0x0199, all -> 0x0195 }
            r3.<init>(r4)     // Catch:{ ZipException -> 0x01a4, Exception -> 0x0199, all -> 0x0195 }
            net.lingala.zip4j.model.ZipModel r4 = r12.zipModel     // Catch:{ ZipException -> 0x01a4, Exception -> 0x0199, all -> 0x0195 }
            long r4 = r4.getSplitLength()     // Catch:{ ZipException -> 0x01a4, Exception -> 0x0199, all -> 0x0195 }
            r2.<init>(r3, r4)     // Catch:{ ZipException -> 0x01a4, Exception -> 0x0199, all -> 0x0195 }
            net.lingala.zip4j.io.ZipOutputStream r3 = new net.lingala.zip4j.io.ZipOutputStream     // Catch:{ ZipException -> 0x01a4, Exception -> 0x0199, all -> 0x0195 }
            net.lingala.zip4j.model.ZipModel r4 = r12.zipModel     // Catch:{ ZipException -> 0x01a4, Exception -> 0x0199, all -> 0x0195 }
            r3.<init>(r2, r4)     // Catch:{ ZipException -> 0x01a4, Exception -> 0x0199, all -> 0x0195 }
            if (r1 == 0) goto L_0x0069
            net.lingala.zip4j.model.ZipModel r1 = r12.zipModel     // Catch:{ ZipException -> 0x0191, Exception -> 0x018d, all -> 0x018a }
            net.lingala.zip4j.model.EndCentralDirRecord r1 = r1.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x0191, Exception -> 0x018d, all -> 0x018a }
            if (r1 == 0) goto L_0x0061
            net.lingala.zip4j.model.ZipModel r1 = r12.zipModel     // Catch:{ ZipException -> 0x0191, Exception -> 0x018d, all -> 0x018a }
            net.lingala.zip4j.model.EndCentralDirRecord r1 = r1.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x0191, Exception -> 0x018d, all -> 0x018a }
            long r4 = r1.getOffsetOfStartOfCentralDir()     // Catch:{ ZipException -> 0x0191, Exception -> 0x018d, all -> 0x018a }
            r2.seek(r4)     // Catch:{ ZipException -> 0x0191, Exception -> 0x018d, all -> 0x018a }
            goto L_0x0069
        L_0x0061:
            net.lingala.zip4j.exception.ZipException r13 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x0191, Exception -> 0x018d, all -> 0x018a }
            java.lang.String r14 = "invalid end of central directory record"
            r13.<init>(r14)     // Catch:{ ZipException -> 0x0191, Exception -> 0x018d, all -> 0x018a }
            throw r13     // Catch:{ ZipException -> 0x0191, Exception -> 0x018d, all -> 0x018a }
        L_0x0069:
            r1 = 4096(0x1000, float:5.74E-42)
            byte[] r1 = new byte[r1]     // Catch:{ ZipException -> 0x0191, Exception -> 0x018d, all -> 0x018a }
            r2 = 0
            r4 = r0
            r0 = 0
        L_0x0070:
            int r5 = r13.size()     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            if (r0 >= r5) goto L_0x0175
            boolean r5 = r15.isCancelAllTasks()     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            r6 = 3
            if (r5 == 0) goto L_0x008c
            r15.setResult(r6)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            r15.setState(r2)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            if (r4 == 0) goto L_0x0088
            r4.close()     // Catch:{ IOException -> 0x0088 }
        L_0x0088:
            r3.close()     // Catch:{ IOException -> 0x008b }
        L_0x008b:
            return
        L_0x008c:
            java.lang.Object r5 = r14.clone()     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            net.lingala.zip4j.model.ZipParameters r5 = (net.lingala.zip4j.model.ZipParameters) r5     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.lang.Object r7 = r13.get(r0)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.io.File r7 = (java.io.File) r7     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.lang.String r7 = r7.getAbsolutePath()     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            r15.setFileName(r7)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.lang.Object r7 = r13.get(r0)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.io.File r7 = (java.io.File) r7     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            boolean r7 = r7.isDirectory()     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            if (r7 != 0) goto L_0x00f7
            boolean r7 = r5.isEncryptFiles()     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            if (r7 == 0) goto L_0x00e4
            int r7 = r5.getEncryptionMethod()     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            if (r7 != 0) goto L_0x00e4
            r15.setCurrentOperation(r6)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.lang.Object r7 = r13.get(r0)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.io.File r7 = (java.io.File) r7     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.lang.String r7 = r7.getAbsolutePath()     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            long r7 = net.lingala.zip4j.util.CRCUtil.computeFileCRC(r7, r15)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            int r8 = (int) r7     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            r5.setSourceFileCRC(r8)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            r15.setCurrentOperation(r2)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            boolean r7 = r15.isCancelAllTasks()     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            if (r7 == 0) goto L_0x00e4
            r15.setResult(r6)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            r15.setState(r2)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            if (r4 == 0) goto L_0x00e0
            r4.close()     // Catch:{ IOException -> 0x00e0 }
        L_0x00e0:
            r3.close()     // Catch:{ IOException -> 0x00e3 }
        L_0x00e3:
            return
        L_0x00e4:
            java.lang.Object r7 = r13.get(r0)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.io.File r7 = (java.io.File) r7     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            long r7 = net.lingala.zip4j.util.Zip4jUtil.getFileLengh(r7)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            r9 = 0
            int r11 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r11 != 0) goto L_0x00f7
            r5.setCompressionMethod(r2)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
        L_0x00f7:
            java.lang.Object r7 = r13.get(r0)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.io.File r7 = (java.io.File) r7     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            boolean r8 = r7.isFile()     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            if (r8 != 0) goto L_0x010a
            boolean r7 = r7.isDirectory()     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            if (r7 != 0) goto L_0x010a
            goto L_0x0166
        L_0x010a:
            java.lang.Object r7 = r13.get(r0)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.io.File r7 = (java.io.File) r7     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            r3.putNextEntry(r7, r5)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.lang.Object r5 = r13.get(r0)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.io.File r5 = (java.io.File) r5     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            boolean r5 = r5.isDirectory()     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            if (r5 == 0) goto L_0x0123
            r3.closeEntry()     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            goto L_0x0166
        L_0x0123:
            java.io.PrintStream r5 = java.lang.System.out     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.lang.Object r7 = r13.get(r0)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.io.File r7 = (java.io.File) r7     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.lang.String r7 = r7.getAbsolutePath()     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            r5.println(r7)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.lang.Object r7 = r13.get(r0)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            java.io.File r7 = (java.io.File) r7     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            r5.<init>(r7)     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
        L_0x013d:
            int r4 = r5.read(r1)     // Catch:{ ZipException -> 0x0171, Exception -> 0x016d, all -> 0x016a }
            r7 = -1
            if (r4 == r7) goto L_0x015f
            boolean r7 = r15.isCancelAllTasks()     // Catch:{ ZipException -> 0x0171, Exception -> 0x016d, all -> 0x016a }
            if (r7 == 0) goto L_0x0157
            r15.setResult(r6)     // Catch:{ ZipException -> 0x0171, Exception -> 0x016d, all -> 0x016a }
            r15.setState(r2)     // Catch:{ ZipException -> 0x0171, Exception -> 0x016d, all -> 0x016a }
            r5.close()     // Catch:{ IOException -> 0x0153 }
        L_0x0153:
            r3.close()     // Catch:{ IOException -> 0x0156 }
        L_0x0156:
            return
        L_0x0157:
            r3.write(r1, r2, r4)     // Catch:{ ZipException -> 0x0171, Exception -> 0x016d, all -> 0x016a }
            long r7 = (long) r4     // Catch:{ ZipException -> 0x0171, Exception -> 0x016d, all -> 0x016a }
            r15.updateWorkCompleted(r7)     // Catch:{ ZipException -> 0x0171, Exception -> 0x016d, all -> 0x016a }
            goto L_0x013d
        L_0x015f:
            r3.closeEntry()     // Catch:{ ZipException -> 0x0171, Exception -> 0x016d, all -> 0x016a }
            r5.close()     // Catch:{ ZipException -> 0x0171, Exception -> 0x016d, all -> 0x016a }
            r4 = r5
        L_0x0166:
            int r0 = r0 + 1
            goto L_0x0070
        L_0x016a:
            r13 = move-exception
            r4 = r5
            goto L_0x01ac
        L_0x016d:
            r13 = move-exception
            r0 = r3
            r4 = r5
            goto L_0x019b
        L_0x0171:
            r13 = move-exception
            r0 = r3
            r4 = r5
            goto L_0x01a6
        L_0x0175:
            r3.finish()     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            r15.endProgressMonitorSuccess()     // Catch:{ ZipException -> 0x0188, Exception -> 0x0186, all -> 0x0184 }
            if (r4 == 0) goto L_0x0180
            r4.close()     // Catch:{ IOException -> 0x0180 }
        L_0x0180:
            r3.close()     // Catch:{ IOException -> 0x0183 }
        L_0x0183:
            return
        L_0x0184:
            r13 = move-exception
            goto L_0x01ac
        L_0x0186:
            r13 = move-exception
            goto L_0x018f
        L_0x0188:
            r13 = move-exception
            goto L_0x0193
        L_0x018a:
            r13 = move-exception
            r4 = r0
            goto L_0x01ac
        L_0x018d:
            r13 = move-exception
            r4 = r0
        L_0x018f:
            r0 = r3
            goto L_0x019b
        L_0x0191:
            r13 = move-exception
            r4 = r0
        L_0x0193:
            r0 = r3
            goto L_0x01a6
        L_0x0195:
            r13 = move-exception
            r3 = r0
            r4 = r3
            goto L_0x01ac
        L_0x0199:
            r13 = move-exception
            r4 = r0
        L_0x019b:
            r15.endProgressMonitorError(r13)     // Catch:{ all -> 0x01aa }
            net.lingala.zip4j.exception.ZipException r14 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x01aa }
            r14.<init>(r13)     // Catch:{ all -> 0x01aa }
            throw r14     // Catch:{ all -> 0x01aa }
        L_0x01a4:
            r13 = move-exception
            r4 = r0
        L_0x01a6:
            r15.endProgressMonitorError(r13)     // Catch:{ all -> 0x01aa }
            throw r13     // Catch:{ all -> 0x01aa }
        L_0x01aa:
            r13 = move-exception
            r3 = r0
        L_0x01ac:
            if (r4 == 0) goto L_0x01b3
            r4.close()     // Catch:{ IOException -> 0x01b2 }
            goto L_0x01b3
        L_0x01b2:
        L_0x01b3:
            if (r3 == 0) goto L_0x01b8
            r3.close()     // Catch:{ IOException -> 0x01b8 }
        L_0x01b8:
            throw r13
        L_0x01b9:
            net.lingala.zip4j.exception.ZipException r13 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r14 = "no files to add"
            r13.<init>(r14)
            throw r13
        L_0x01c1:
            net.lingala.zip4j.exception.ZipException r13 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r14 = "one of the input parameters is null when adding files"
            r13.<init>(r14)
            goto L_0x01ca
        L_0x01c9:
            throw r13
        L_0x01ca:
            goto L_0x01c9
        */
        throw new UnsupportedOperationException("Method not decompiled: net.lingala.zip4j.zip.ZipEngine.initAddFiles(java.util.ArrayList, net.lingala.zip4j.model.ZipParameters, net.lingala.zip4j.progress.ProgressMonitor):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x009a A[SYNTHETIC, Splitter:B:37:0x009a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void addStreamToZip(java.io.InputStream r7, net.lingala.zip4j.model.ZipParameters r8) {
        /*
            r6 = this;
            if (r7 == 0) goto L_0x009e
            if (r8 == 0) goto L_0x009e
            r0 = 0
            r6.checkParameters(r8)     // Catch:{ ZipException -> 0x0096, Exception -> 0x008f }
            net.lingala.zip4j.model.ZipModel r1 = r6.zipModel     // Catch:{ ZipException -> 0x0096, Exception -> 0x008f }
            java.lang.String r1 = r1.getZipFile()     // Catch:{ ZipException -> 0x0096, Exception -> 0x008f }
            boolean r1 = net.lingala.zip4j.util.Zip4jUtil.checkFileExists(r1)     // Catch:{ ZipException -> 0x0096, Exception -> 0x008f }
            net.lingala.zip4j.io.SplitOutputStream r2 = new net.lingala.zip4j.io.SplitOutputStream     // Catch:{ ZipException -> 0x0096, Exception -> 0x008f }
            java.io.File r3 = new java.io.File     // Catch:{ ZipException -> 0x0096, Exception -> 0x008f }
            net.lingala.zip4j.model.ZipModel r4 = r6.zipModel     // Catch:{ ZipException -> 0x0096, Exception -> 0x008f }
            java.lang.String r4 = r4.getZipFile()     // Catch:{ ZipException -> 0x0096, Exception -> 0x008f }
            r3.<init>(r4)     // Catch:{ ZipException -> 0x0096, Exception -> 0x008f }
            net.lingala.zip4j.model.ZipModel r4 = r6.zipModel     // Catch:{ ZipException -> 0x0096, Exception -> 0x008f }
            long r4 = r4.getSplitLength()     // Catch:{ ZipException -> 0x0096, Exception -> 0x008f }
            r2.<init>(r3, r4)     // Catch:{ ZipException -> 0x0096, Exception -> 0x008f }
            net.lingala.zip4j.io.ZipOutputStream r3 = new net.lingala.zip4j.io.ZipOutputStream     // Catch:{ ZipException -> 0x0096, Exception -> 0x008f }
            net.lingala.zip4j.model.ZipModel r4 = r6.zipModel     // Catch:{ ZipException -> 0x0096, Exception -> 0x008f }
            r3.<init>(r2, r4)     // Catch:{ ZipException -> 0x0096, Exception -> 0x008f }
            if (r1 == 0) goto L_0x004f
            net.lingala.zip4j.model.ZipModel r1 = r6.zipModel     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            net.lingala.zip4j.model.EndCentralDirRecord r1 = r1.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            if (r1 == 0) goto L_0x0047
            net.lingala.zip4j.model.ZipModel r1 = r6.zipModel     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            net.lingala.zip4j.model.EndCentralDirRecord r1 = r1.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            long r4 = r1.getOffsetOfStartOfCentralDir()     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            r2.seek(r4)     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            goto L_0x004f
        L_0x0047:
            net.lingala.zip4j.exception.ZipException r7 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            java.lang.String r8 = "invalid end of central directory record"
            r7.<init>(r8)     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            throw r7     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
        L_0x004f:
            r1 = 4096(0x1000, float:5.74E-42)
            byte[] r1 = new byte[r1]     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            r3.putNextEntry(r0, r8)     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            java.lang.String r0 = r8.getFileNameInZip()     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            java.lang.String r2 = "/"
            boolean r0 = r0.endsWith(r2)     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            if (r0 != 0) goto L_0x007a
            java.lang.String r8 = r8.getFileNameInZip()     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            java.lang.String r0 = "\\"
            boolean r8 = r8.endsWith(r0)     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            if (r8 != 0) goto L_0x007a
        L_0x006e:
            int r8 = r7.read(r1)     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            r0 = -1
            if (r8 == r0) goto L_0x007a
            r0 = 0
            r3.write(r1, r0, r8)     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            goto L_0x006e
        L_0x007a:
            r3.closeEntry()     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            r3.finish()     // Catch:{ ZipException -> 0x0089, Exception -> 0x0086, all -> 0x0084 }
            r3.close()     // Catch:{ IOException -> 0x0083 }
        L_0x0083:
            return
        L_0x0084:
            r7 = move-exception
            goto L_0x0098
        L_0x0086:
            r7 = move-exception
            r0 = r3
            goto L_0x0090
        L_0x0089:
            r7 = move-exception
            r0 = r3
            goto L_0x0097
        L_0x008c:
            r7 = move-exception
            r3 = r0
            goto L_0x0098
        L_0x008f:
            r7 = move-exception
        L_0x0090:
            net.lingala.zip4j.exception.ZipException r8 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x008c }
            r8.<init>(r7)     // Catch:{ all -> 0x008c }
            throw r8     // Catch:{ all -> 0x008c }
        L_0x0096:
            r7 = move-exception
        L_0x0097:
            throw r7     // Catch:{ all -> 0x008c }
        L_0x0098:
            if (r3 == 0) goto L_0x009d
            r3.close()     // Catch:{ IOException -> 0x009d }
        L_0x009d:
            throw r7
        L_0x009e:
            net.lingala.zip4j.exception.ZipException r7 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r8 = "one of the input parameters is null, cannot add stream to zip"
            r7.<init>(r8)
            goto L_0x00a7
        L_0x00a6:
            throw r7
        L_0x00a7:
            goto L_0x00a6
        */
        throw new UnsupportedOperationException("Method not decompiled: net.lingala.zip4j.zip.ZipEngine.addStreamToZip(java.io.InputStream, net.lingala.zip4j.model.ZipParameters):void");
    }

    public void addFolderToZip(File file, ZipParameters zipParameters, ProgressMonitor progressMonitor, boolean z) {
        String str;
        if (file == null || zipParameters == null) {
            throw new ZipException("one of the input parameters is null, cannot add folder to zip");
        } else if (!Zip4jUtil.checkFileExists(file.getAbsolutePath())) {
            throw new ZipException("input folder does not exist");
        } else if (!file.isDirectory()) {
            throw new ZipException("input file is not a folder, user addFileToZip method to add files");
        } else if (Zip4jUtil.checkFileReadAccess(file.getAbsolutePath())) {
            if (zipParameters.isIncludeRootFolder()) {
                String absolutePath = file.getAbsolutePath();
                str = BuildConfig.FLAVOR;
                if (absolutePath != null) {
                    if (file.getAbsoluteFile().getParentFile() != null) {
                        str = file.getAbsoluteFile().getParentFile().getAbsolutePath();
                    }
                } else if (file.getParentFile() != null) {
                    str = file.getParentFile().getAbsolutePath();
                }
            } else {
                str = file.getAbsolutePath();
            }
            zipParameters.setDefaultFolderPath(str);
            ArrayList filesInDirectoryRec = Zip4jUtil.getFilesInDirectoryRec(file, zipParameters.isReadHiddenFiles());
            if (zipParameters.isIncludeRootFolder()) {
                if (filesInDirectoryRec == null) {
                    filesInDirectoryRec = new ArrayList();
                }
                filesInDirectoryRec.add(file);
            }
            addFiles(filesInDirectoryRec, zipParameters, progressMonitor, z);
        } else {
            throw new ZipException("cannot read folder: " + file.getAbsolutePath());
        }
    }

    private void checkParameters(ZipParameters zipParameters) {
        if (zipParameters == null) {
            throw new ZipException("cannot validate zip parameters");
        } else if (zipParameters.getCompressionMethod() != 0 && zipParameters.getCompressionMethod() != 8) {
            throw new ZipException("unsupported compression type");
        } else if (zipParameters.getCompressionMethod() == 8 && zipParameters.getCompressionLevel() < 0 && zipParameters.getCompressionLevel() > 9) {
            throw new ZipException("invalid compression level. compression level dor deflate should be in the range of 0-9");
        } else if (!zipParameters.isEncryptFiles()) {
            zipParameters.setAesKeyStrength(-1);
            zipParameters.setEncryptionMethod(-1);
        } else if (zipParameters.getEncryptionMethod() != 0 && zipParameters.getEncryptionMethod() != 99) {
            throw new ZipException("unsupported encryption method");
        } else if (zipParameters.getPassword() == null || zipParameters.getPassword().length <= 0) {
            throw new ZipException("input password is empty or null");
        }
    }

    private void removeFilesIfExists(ArrayList arrayList, ZipParameters zipParameters, ProgressMonitor progressMonitor) {
        ZipModel zipModel2 = this.zipModel;
        if (zipModel2 != null && zipModel2.getCentralDirectory() != null && this.zipModel.getCentralDirectory().getFileHeaders() != null && this.zipModel.getCentralDirectory().getFileHeaders().size() > 0) {
            RandomAccessFile randomAccessFile = null;
            int i = 0;
            while (i < arrayList.size()) {
                try {
                    FileHeader fileHeader = Zip4jUtil.getFileHeader(this.zipModel, Zip4jUtil.getRelativeFileName(((File) arrayList.get(i)).getAbsolutePath(), zipParameters.getRootFolderInZip(), zipParameters.getDefaultFolderPath()));
                    if (fileHeader != null) {
                        if (randomAccessFile != null) {
                            randomAccessFile.close();
                            randomAccessFile = null;
                        }
                        ArchiveMaintainer archiveMaintainer = new ArchiveMaintainer();
                        progressMonitor.setCurrentOperation(2);
                        HashMap initRemoveZipFile = archiveMaintainer.initRemoveZipFile(this.zipModel, fileHeader, progressMonitor);
                        if (progressMonitor.isCancelAllTasks()) {
                            progressMonitor.setResult(3);
                            progressMonitor.setState(0);
                            if (randomAccessFile != null) {
                                try {
                                    randomAccessFile.close();
                                    return;
                                } catch (IOException unused) {
                                    return;
                                }
                            } else {
                                return;
                            }
                        } else {
                            progressMonitor.setCurrentOperation(0);
                            if (randomAccessFile == null) {
                                randomAccessFile = prepareFileOutputStream();
                                if (!(initRemoveZipFile == null || initRemoveZipFile.get(InternalZipConstants.OFFSET_CENTRAL_DIR) == null)) {
                                    long parseLong = Long.parseLong((String) initRemoveZipFile.get(InternalZipConstants.OFFSET_CENTRAL_DIR));
                                    if (parseLong >= 0) {
                                        randomAccessFile.seek(parseLong);
                                    }
                                }
                            }
                        }
                    }
                    i++;
                } catch (NumberFormatException unused2) {
                    throw new ZipException("NumberFormatException while parsing offset central directory. Cannot update already existing file header");
                } catch (Exception unused3) {
                    throw new ZipException("Error while parsing offset central directory. Cannot update already existing file header");
                } catch (IOException e) {
                    try {
                        throw new ZipException(e);
                    } catch (Throwable th) {
                        if (randomAccessFile != null) {
                            try {
                                randomAccessFile.close();
                            } catch (IOException unused4) {
                            }
                        }
                        throw th;
                    }
                }
            }
            if (randomAccessFile != null) {
                try {
                    randomAccessFile.close();
                } catch (IOException unused5) {
                }
            }
        }
    }

    private RandomAccessFile prepareFileOutputStream() {
        String zipFile = this.zipModel.getZipFile();
        if (Zip4jUtil.isStringNotNullAndNotEmpty(zipFile)) {
            try {
                File file = new File(zipFile);
                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }
                return new RandomAccessFile(file, InternalZipConstants.WRITE_MODE);
            } catch (FileNotFoundException e) {
                throw new ZipException(e);
            }
        } else {
            throw new ZipException("invalid output path");
        }
    }

    private EndCentralDirRecord createEndOfCentralDirectoryRecord() {
        EndCentralDirRecord endCentralDirRecord = new EndCentralDirRecord();
        endCentralDirRecord.setSignature(InternalZipConstants.ENDSIG);
        endCentralDirRecord.setNoOfThisDisk(0);
        endCentralDirRecord.setTotNoOfEntriesInCentralDir(0);
        endCentralDirRecord.setTotNoOfEntriesInCentralDirOnThisDisk(0);
        endCentralDirRecord.setOffsetOfStartOfCentralDir(0);
        return endCentralDirRecord;
    }

    private long calculateTotalWork(ArrayList arrayList, ZipParameters zipParameters) {
        long j;
        FileHeader fileHeader;
        if (arrayList != null) {
            long j2 = 0;
            for (int i = 0; i < arrayList.size(); i++) {
                if ((arrayList.get(i) instanceof File) && ((File) arrayList.get(i)).exists()) {
                    if (!zipParameters.isEncryptFiles() || zipParameters.getEncryptionMethod() != 0) {
                        j = Zip4jUtil.getFileLengh((File) arrayList.get(i));
                    } else {
                        j = Zip4jUtil.getFileLengh((File) arrayList.get(i)) * 2;
                    }
                    j2 += j;
                    if (!(this.zipModel.getCentralDirectory() == null || this.zipModel.getCentralDirectory().getFileHeaders() == null || this.zipModel.getCentralDirectory().getFileHeaders().size() <= 0 || (fileHeader = Zip4jUtil.getFileHeader(this.zipModel, Zip4jUtil.getRelativeFileName(((File) arrayList.get(i)).getAbsolutePath(), zipParameters.getRootFolderInZip(), zipParameters.getDefaultFolderPath()))) == null)) {
                        j2 += Zip4jUtil.getFileLengh(new File(this.zipModel.getZipFile())) - fileHeader.getCompressedSize();
                    }
                }
            }
            return j2;
        }
        throw new ZipException("file list is null, cannot calculate total work");
    }
}
