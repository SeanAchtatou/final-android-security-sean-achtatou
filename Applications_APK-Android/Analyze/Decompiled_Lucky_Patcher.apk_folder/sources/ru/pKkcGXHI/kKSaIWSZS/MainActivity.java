package ru.pKkcGXHI.kKSaIWSZS;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.ʻ.C0260;
import android.support.v7.app.C0448;
import android.support.v7.app.C0460;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.chelpus.C0815;
import com.lp.C0959;
import com.lp.C0968;
import com.lp.C0973;
import com.lp.C0975;
import com.lp.C0987;
import com.lp.HelpActivity;
import com.lp.LuckyApp;
import com.lp.ʻ.C0949;
import com.xposed.XposedUtils;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Comparator;
import net.lingala.zip4j.util.InternalZipConstants;

public class MainActivity extends C0460 {
    public static final int APP_DIALOG = 6;
    public static final int CONTEXT_DIALOG = 7;
    public static final int CREATE_APK = 0;
    public static final int CUSTOM2_DIALOG = 15;
    public static final int CUSTOM_PATCH = 1;
    public static final int DIALOG_REPORT_FORCE_CLOSE = 3535788;
    public static final int MARKET_INSTALL_DIALOG = 30;
    public static final int PROGRESS_DIALOG2 = 11;
    public static final int RESTORE_FROM_BACKUP = 28;
    private static final int SETTINGS_ORIENT_LANDSCAPE = 1;
    private static final int SETTINGS_ORIENT_PORTRET = 2;
    public static final int SETTINGS_VIEWSIZE_DEFAULT = 0;
    private static final int SETTINGS_VIEWSIZE_SMALL = 0;
    public static C0987 frag;
    public DrawerLayout mDrawerLayout = null;
    boolean mIsRestoredToTop = false;

    @SuppressLint({"NewApi"})
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        System.out.println("LuckyPatcher: create activity");
        LuckyApp.f3806 = this;
        C0987.f4447 = this;
        String string = getSharedPreferences("config", 4).getString("force_close_info", BuildConfig.FLAVOR);
        if (!getSharedPreferences("config", 4).getBoolean("force_close", false) || !string.equals(BuildConfig.FLAVOR)) {
            C0987.f4485 = true;
            C0987.f4447 = this;
            C0987.f4461 = this;
            try {
                if (!string.equals(BuildConfig.FLAVOR)) {
                    if (string.equals("No space left on device")) {
                        C0815.m5252(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.warning_for_no_free_space_on_device));
                    }
                    if (string.equals("OutOfMemoryError")) {
                        C0815.m5252(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.warning_for_out_of_memory_on_device));
                    }
                    getSharedPreferences("config", 4).edit().putString("force_close_info", BuildConfig.FLAVOR).commit();
                    getSharedPreferences("config", 4).edit().putBoolean("force_close", false).commit();
                }
            } catch (Exception unused) {
                getSharedPreferences("config", 4).edit().putString("force_close_info", BuildConfig.FLAVOR).commit();
                getSharedPreferences("config", 4).edit().putBoolean("force_close", false).commit();
            }
            setContentView((int) R.layout.activity_main);
            if (Build.VERSION.SDK_INT >= 11) {
                this.mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_top);
                toolbar.setTitle("LP");
                setSupportActionBar(toolbar);
                C0448 r7 = new C0448(C0987.f4447, this.mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
                this.mDrawerLayout.m864(r7);
                r7.m2462();
                C0987.m6066(this);
            }
            getSupportFragmentManager().m1403().m1568(R.id.main_content, new C0987()).m1567();
            try {
                if (C0815.m5205((int) R.string.busybox_not_found) == null) {
                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
                finish();
            }
            if (C0987.m6073().getInt("orientstion", 3) == 1) {
                setRequestedOrientation(0);
            }
            if (C0987.m6073().getInt("orientstion", 3) == 2) {
                setRequestedOrientation(1);
            }
            if (C0987.m6073().getInt("orientstion", 3) == 3) {
                setRequestedOrientation(4);
            }
            getWindow().addFlags(128);
            return;
        }
        System.out.println("LP FC detected!");
        try {
            getSharedPreferences("config", 4).edit().putBoolean("force_close", false).commit();
            C0987.f4478 = new C0973();
            C0987.m6079((Context) this);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            if (C0987.f4479 >= C0987.m6073().getInt("http_versionCode", 0)) {
                builder.setTitle(C0815.m5205((int) R.string.warning)).setIcon(17301543).setMessage(C0815.m5205((int) R.string.error_detect)).setPositiveButton(C0815.m5205((int) R.string.Yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        new AsyncTask<Void, Void, Boolean>() {
                            /* access modifiers changed from: protected */
                            public void onPreExecute() {
                            }

                            /* access modifiers changed from: protected */
                            public Boolean doInBackground(Void... voidArr) {
                                return Boolean.valueOf(new File(C0987.f4438 + "/Log/error_log.txt").exists());
                            }

                            /* access modifiers changed from: protected */
                            public void onPostExecute(Boolean bool) {
                                if (bool.booleanValue()) {
                                    try {
                                        C0973 r0 = C0987.f4478;
                                        MainActivity mainActivity = MainActivity.this;
                                        r0.m5993(mainActivity, "lp.chelpus@gmail.com", "Error Log", "Lucky Patcher " + MainActivity.this.getPackageManager().getPackageInfo(MainActivity.this.getPackageName(), 0).versionName);
                                    } catch (PackageManager.NameNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                    MainActivity.this.finish();
                                    if (C0987.f4474) {
                                        C0987.m6075();
                                    }
                                    System.exit(0);
                                    return;
                                }
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setTitle("Error").setMessage(C0815.m5205((int) R.string.error_collect_logs)).setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        MainActivity.this.finish();
                                        if (C0987.f4474) {
                                            C0987.m6075();
                                        }
                                        System.exit(0);
                                    }
                                }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                                    public void onCancel(DialogInterface dialogInterface) {
                                        MainActivity.this.finish();
                                        if (C0987.f4474) {
                                            C0987.m6075();
                                        }
                                        System.exit(0);
                                    }
                                });
                                builder.create().show();
                            }
                        }.execute(new Void[0]);
                    }
                }).setNegativeButton(C0815.m5205((int) R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MainActivity.this.finish();
                        if (C0987.f4474) {
                            C0987.m6075();
                        }
                        System.exit(0);
                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                        MainActivity.this.finish();
                        if (C0987.f4474) {
                            C0987.m6075();
                        }
                        System.exit(0);
                    }
                });
            } else {
                builder.setTitle(C0815.m5205((int) R.string.warning)).setIcon(17301543).setMessage(C0815.m5205((int) R.string.warning_force_close_please_update_lp)).setPositiveButton(C0815.m5205((int) R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MainActivity.this.finish();
                        if (C0987.f4474) {
                            C0987.m6075();
                        }
                        System.exit(0);
                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                        MainActivity.this.finish();
                        if (C0987.f4474) {
                            C0987.m6075();
                        }
                        System.exit(0);
                    }
                });
            }
            builder.create().show();
        } catch (Exception e2) {
            e2.printStackTrace();
            getSharedPreferences("config", 4).edit().putBoolean("force_close", false).commit();
            finish();
            if (C0987.f4474) {
                C0987.m6075();
            }
            System.exit(0);
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        C0987.m6078(i, i2, intent);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(bundle);
    }

    public void onBackPressed() {
        System.out.println("backPressed");
        try {
            if (C0987.f4521) {
                C0987.f4432.m6140();
            } else if (C0987.f4514) {
                C0987 r0 = C0987.f4432;
                C0987.m6103();
            } else if (this.mDrawerLayout != null && this.mDrawerLayout.m887(8388611)) {
                this.mDrawerLayout.m871();
            } else if (C0987.m6073().getBoolean("confirm_exit", true)) {
                AnonymousClass6 r02 = new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == -1) {
                            MainActivity.this.finish();
                        }
                    }
                };
                C0815.m5170(C0815.m5205((int) R.string.app_name), C0815.m5205((int) R.string.message_exit), r02, r02, (DialogInterface.OnCancelListener) null);
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        System.out.println("Lucky Patcher: on new intent activity.");
        C0987.f4447 = this;
        C0987.f4502 = new Handler();
        C0987.f4450 = true;
        if (C0987.f4432 != null) {
            C0987.f4432.m6154(intent);
        }
    }

    @SuppressLint({"NewApi"})
    public void finish() {
        if (C0987.f4474) {
            new Thread(new Runnable() {
                public void run() {
                    String r0 = C0815.m5316();
                    PrintStream printStream = System.out;
                    printStream.println("EnfFinish: " + r0);
                    if (r0.equals("enforce")) {
                        try {
                            new C0815(BuildConfig.FLAVOR).m5353("setenforce 1");
                        } catch (Exception unused) {
                        }
                    }
                    if (C0987.f4474) {
                        C0987.m6075();
                    }
                    System.exit(0);
                }
            }).start();
            super.finish();
        } else {
            super.finish();
            if (C0987.f4474) {
                C0987.m6075();
            }
            System.exit(0);
        }
        if (Build.VERSION.SDK_INT >= 19 && !isTaskRoot() && this.mIsRestoredToTop) {
            ((ActivityManager) getSystemService("activity")).moveTaskToFront(getTaskId(), 2);
        }
    }

    public void onStart() {
        super.onStart();
        System.out.println("Lucky Patcher: start activity.");
        C0987.f4447 = this;
        LuckyApp.f3806 = this;
        C0987.f4502 = new Handler();
    }

    public void onPause() {
        try {
            super.onPause();
        } catch (Throwable th) {
            th.printStackTrace();
        }
        System.out.println("Lucky Patcher: activity pause.");
        C0987.f4447 = this;
        C0987.f4502 = new Handler();
    }

    public void onResume() {
        try {
            super.onResume();
        } catch (Throwable th) {
            th.printStackTrace();
        }
        System.out.println("Lucky Patcher: activity resume.");
        C0987.f4447 = this;
        LuckyApp.f3806 = this;
        C0987.f4502 = new Handler();
    }

    public void onMemoryLow() {
        C0987.f4466 = false;
        System.out.println("LuckyPatcher (onMemoryLow): started!");
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        System.out.println("onWindowFocusChanged");
        C0987.f4447 = this;
        C0987.f4502 = new Handler();
        if (C0987.f4480 && z) {
            C0987.m6091();
            C0987.f4480 = false;
            if (C0987.f4440 != null) {
                C0987.m6073().edit().remove(C0987.f4440.f4337).commit();
            }
            if (C0987.f4439 != null) {
                C0987.f4439.notifyDataSetChanged();
            }
            C0987.m6094((Integer) 6);
            new Thread(new Runnable() {
                public void run() {
                    int i = C0987.m6073().getInt("Install_location", 3);
                    if (i == 3) {
                        return;
                    }
                    if (C0987.f4474) {
                        C0815 r1 = new C0815(BuildConfig.FLAVOR);
                        r1.m5353("pm setInstallLocation " + i, "skipOut");
                        C0815 r12 = new C0815(BuildConfig.FLAVOR);
                        r12.m5353("pm set-install-location " + i, "skipOut");
                        return;
                    }
                    if (Build.VERSION.SDK_INT < 19) {
                        C0815.m5148("pm setInstallLocation " + i, "skipOut");
                    }
                    if (Build.VERSION.SDK_INT < 19) {
                        C0815.m5148("pm set-install-location " + i, "skipOut");
                    }
                }
            }).start();
        }
        if (C0987.f4439 != null) {
            C0987.f4439.notifyDataSetChanged();
        }
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ((ActivityManager) C0987.m6072().getSystemService("activity")).getMemoryInfo(memoryInfo);
        PrintStream printStream = System.out;
        printStream.println("LuckyPatcher " + C0987.f4465 + " (FreeMemory): " + (memoryInfo.availMem / 1048576) + " lowMemory:" + memoryInfo.lowMemory + " TrashOld:" + (memoryInfo.threshold / 1048576));
        if ((C0987.f4481 == null || C0987.f4481.booleanValue()) && C0987.f4439 != null && z) {
            if (!C0987.f4472) {
                C0987.m6112(true);
            }
            C0987.f4481 = false;
        }
    }

    public void launch_click(View view) {
        if (C0987.m6073().getBoolean("vibration", false)) {
            C0987 r3 = frag;
            C0987.f4530 = (Vibrator) getSystemService("vibrator");
            C0987 r32 = frag;
            C0987.f4530.vibrate(50);
        }
        C0987 r33 = frag;
        C0987.m6094((Integer) 6);
        try {
            Intent launchIntentForPackage = C0987.m6068().getLaunchIntentForPackage(C0987.f4440.f4337);
            if (C0987.f4474) {
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            C0815.m5310(C0987.m6072().getPackageManager().getPackageInfo(C0987.f4440.f4337, 0).applicationInfo.processName);
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                        C0815.m5315(C0987.f4440.f4337);
                    }
                }).start();
            } else {
                startActivity(launchIntentForPackage);
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        } catch (Exception unused) {
            Toast.makeText(this, C0815.m5205((int) R.string.error_launch), 1).show();
        }
    }

    public void saveobject_click(View view) {
        try {
            File file = new File(C0987.f4438 + InternalZipConstants.ZIP_FILE_SEPARATOR + C0987.f4440.f4337 + ".txt");
            if (file.exists()) {
                file.delete();
            }
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write("[BEGIN]\ngetActivity() Custom Patch generated by Luckypatcher the manual mode! For Object N" + C0987.f4445 + "...\n[CLASSES]\n{\"object\":\"" + C0987.f4445 + "\"}\n[ODEX]\n[END]\nApplication patched on object N" + C0987.f4445 + ". Please test...\nIf all works well. Make a \"Dalvik-cache Fix Apply\".");
            fileWriter.close();
            StringBuilder sb = new StringBuilder();
            sb.append("Object N");
            sb.append(C0987.f4445);
            sb.append(" ");
            sb.append(C0815.m5205((int) R.string.saved_object));
            C0987.f4458.setText(C0815.m5137(sb.toString(), "#ff00ff73", "bold"));
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error while saving file", 1).show();
        } catch (Exception e2) {
            e2.printStackTrace();
            Toast.makeText(this, "Error while saving file", 1).show();
        }
    }

    public void fixobject_click(View view) {
        try {
            frag.m6124(C0987.f4440);
            C0987.f4458.setText(C0815.m5137(BuildConfig.FLAVOR + C0815.m5205((int) R.string.allchanges), "#ff00ff73", "bold"));
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Error while saving file", 1).show();
        }
    }

    public void patch_click(View view) {
        if (C0987.f4445 != 0) {
            C0815.m5310(C0987.f4440.f4337);
            C0987.f4435 = BuildConfig.FLAVOR;
            C0815.m5310(C0987.f4462);
            C0815 r0 = new C0815(BuildConfig.FLAVOR);
            C0987.f4435 = r0.m5353(C0987.f4475 + ".nerorunpatch " + C0987.f4462 + " object" + C0987.f4445);
            if (C0987.f4435.contains("Done")) {
                C0987.f4458.setText(C0815.m5137("Object N" + C0987.f4445 + "\n" + C0815.m5205((int) R.string.extres1), "#ff00ff73", "bold"));
                return;
            }
            C0987.f4458.setText(C0815.m5137("Object N" + C0987.f4445 + "\n" + C0815.m5205((int) R.string.extres2), "#ffff0055", "bold"));
        }
    }

    public void restore_click(View view) {
        try {
            C0987.f4435 = BuildConfig.FLAVOR;
            C0815 r0 = new C0815(BuildConfig.FLAVOR);
            C0987.f4458.setText(C0815.m5137(r0.m5353(C0987.f4475 + ".restore " + C0987.f4440.f4337), "#ff00ff73", "bold"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void backup_click(View view) {
        try {
            C0987.f4435 = BuildConfig.FLAVOR;
            C0815 r0 = new C0815(BuildConfig.FLAVOR);
            C0987.f4458.setText(C0815.m5137(r0.m5353(C0987.f4475 + ".backup " + C0987.f4440.f4337), "#ff00ff73", "bold"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void toolbar_menu_click(View view) {
        frag.m1236().openOptionsMenu();
    }

    public void toolbar_market_install_click(View view) {
        C0987 r2 = frag;
        C0987.m6094((Integer) 30);
        C0987 r0 = frag;
        C0987.m6084((Integer) 30);
    }

    public void toolbar_switchers_click(View view) {
        Thread thread = new Thread(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:17:0x0075  */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x008d  */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x00b1  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x00c8  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r4 = this;
                    android.content.pm.PackageManager r0 = com.lp.C0987.m6068()     // Catch:{ NameNotFoundException -> 0x0011, IllegalArgumentException -> 0x000c }
                    java.lang.String r1 = "com.android.vending"
                    r2 = 0
                    android.content.pm.PackageInfo r0 = r0.getPackageInfo(r1, r2)     // Catch:{ NameNotFoundException -> 0x0011, IllegalArgumentException -> 0x000c }
                    goto L_0x0016
                L_0x000c:
                    r0 = move-exception
                    r0.printStackTrace()
                    goto L_0x0015
                L_0x0011:
                    r0 = move-exception
                    r0.printStackTrace()
                L_0x0015:
                    r0 = 0
                L_0x0016:
                    java.util.ArrayList r1 = new java.util.ArrayList
                    r1.<init>()
                    r2 = 2131689955(0x7f0f01e3, float:1.900894E38)
                    java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
                    r1.add(r2)
                    boolean r2 = com.lp.C0987.f4474
                    if (r2 == 0) goto L_0x0035
                    if (r0 == 0) goto L_0x0035
                    r2 = 2131689808(0x7f0f0150, float:1.9008642E38)
                    java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
                    r1.add(r2)
                L_0x0035:
                    r2 = 2131689553(0x7f0f0051, float:1.9008125E38)
                    java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
                    r1.add(r2)
                    r2 = 2131689560(0x7f0f0058, float:1.9008139E38)
                    java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
                    r1.add(r2)
                    r2 = 2131689561(0x7f0f0059, float:1.900814E38)
                    java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
                    r1.add(r2)
                    boolean r2 = com.lp.C0987.f4474
                    if (r2 == 0) goto L_0x006d
                    if (r0 == 0) goto L_0x006d
                    r0 = 2131689806(0x7f0f014e, float:1.9008638E38)
                    java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
                    r1.add(r0)
                    r0 = 2131690119(0x7f0f0287, float:1.9009273E38)
                    java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
                    r1.add(r0)
                L_0x006d:
                    java.lang.Boolean r0 = com.lp.C0987.f4532
                    boolean r0 = r0.booleanValue()
                    if (r0 != 0) goto L_0x0089
                    r0 = 2131690224(0x7f0f02f0, float:1.9009486E38)
                    java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
                    r1.add(r0)
                    r0 = 2131690226(0x7f0f02f2, float:1.900949E38)
                    java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
                    r1.add(r0)
                L_0x0089:
                    boolean r0 = com.lp.C0987.f4474
                    if (r0 == 0) goto L_0x00ab
                    r0 = 2131690228(0x7f0f02f4, float:1.9009494E38)
                    java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
                    r1.add(r0)
                    r0 = 2131690232(0x7f0f02f8, float:1.9009502E38)
                    java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
                    r1.add(r0)
                    r0 = 2131690230(0x7f0f02f6, float:1.9009498E38)
                    java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
                    r1.add(r0)
                L_0x00ab:
                    int r0 = r1.size()
                    if (r0 == 0) goto L_0x00c8
                    ru.pKkcGXHI.kKSaIWSZS.MainActivity$10$1 r0 = new ru.pKkcGXHI.kKSaIWSZS.MainActivity$10$1
                    ru.pKkcGXHI.kKSaIWSZS.MainActivity r2 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.this
                    r3 = 2131427444(0x7f0b0074, float:1.8476504E38)
                    r0.<init>(r2, r3, r1)
                    com.lp.C0987.f4453 = r0
                    ru.pKkcGXHI.kKSaIWSZS.MainActivity r0 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.this
                    ru.pKkcGXHI.kKSaIWSZS.MainActivity$10$2 r1 = new ru.pKkcGXHI.kKSaIWSZS.MainActivity$10$2
                    r1.<init>()
                    r0.runOnUiThread(r1)
                    goto L_0x00d2
                L_0x00c8:
                    ru.pKkcGXHI.kKSaIWSZS.MainActivity r0 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.this
                    ru.pKkcGXHI.kKSaIWSZS.MainActivity$10$3 r1 = new ru.pKkcGXHI.kKSaIWSZS.MainActivity$10$3
                    r1.<init>()
                    r0.runOnUiThread(r1)
                L_0x00d2:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: ru.pKkcGXHI.kKSaIWSZS.MainActivity.AnonymousClass10.run():void");
            }
        });
        thread.setPriority(10);
        thread.start();
    }

    public void toolbar_system_utils_click(View view) {
        if (C0987.f4521) {
            C0987.f4432.m6140();
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(new C0975(R.string.back, new ArrayList(), true));
        if (C0987.f4474) {
            arrayList.add(new C0975(R.string.bootview, new ArrayList(), true));
            arrayList.add(new C0975(R.string.context_batch_operations, new ArrayList<Integer>() {
                {
                    add(Integer.valueOf((int) R.string.context_backup_apk_selected_apps));
                    add(Integer.valueOf((int) R.string.context_move_selected_apps_to_sdcard));
                    add(Integer.valueOf((int) R.string.context_move_selected_apps_to_internal));
                    add(Integer.valueOf((int) R.string.context_uninstall_selected_apps));
                    add(Integer.valueOf((int) R.string.context_disable_package_for_selected_apps));
                    add(Integer.valueOf((int) R.string.context_enable_package_for_selected_apps));
                    add(Integer.valueOf((int) R.string.context_integrate_update_selected_apps));
                }
            }, true));
            arrayList.add(new C0975(R.string.toolbar_adfree, new ArrayList<Integer>() {
                {
                    add(Integer.valueOf((int) R.string.context_blockads));
                    add(Integer.valueOf((int) R.string.context_unblockads));
                    add(Integer.valueOf((int) R.string.context_clearhosts));
                }
            }, true));
        }
        arrayList.add(new C0975(R.string.remove_all_saved_purchases, new ArrayList(), true));
        if (C0987.f4474) {
            arrayList.add(new C0975(R.string.dirbinder, new ArrayList(), true));
            arrayList.add(new C0975(R.string.corepatches, new ArrayList(), true));
            if (XposedUtils.isXposedEnabled()) {
                arrayList.add(new C0975(R.string.xposed_settings, new ArrayList(), true));
            }
            arrayList.add(new C0975(R.string.mod_market_check, new ArrayList(), true));
            arrayList.add(new C0975(R.string.market_install, new ArrayList(), true));
            arrayList.add(new C0975(R.string.odex_all_system_app, new ArrayList(), true));
            arrayList.add(new C0975(R.string.removefixes, new ArrayList(), true));
            arrayList.add(new C0975(R.string.cleardalvik, new ArrayList(), true));
        }
        if (C0987.f4532.booleanValue() && XposedUtils.isXposedEnabled()) {
            arrayList.add(new C0975(R.string.xposed_settings, new ArrayList(), true));
        }
        if (C0987.f4489 < 19 || C0987.f4474) {
            arrayList.add(new C0975(R.string.set_default_to_install, new ArrayList<Integer>() {
                {
                    add(Integer.valueOf((int) R.string.set_default_to_install_auto));
                    add(Integer.valueOf((int) R.string.set_default_to_install_internal_memory));
                    add(Integer.valueOf((int) R.string.set_default_to_install_sdcard));
                }
            }, true));
        }
        if (C0987.f4474) {
            arrayList.add(new C0975(R.string.reboot_to_bootloader, new ArrayList(), true));
            arrayList.add(new C0975(R.string.reboot_to_recovery, new ArrayList(), true));
            arrayList.add(new C0975(R.string.reboot, new ArrayList(), true));
        }
        try {
            if (C0987.f4522 != null) {
                C0987.f4522.m5999(arrayList);
                C0987.f4432.m6139();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void toolbar_settings_click() {
        if (C0987.f4521) {
            C0987.f4432.m6140();
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(new C0975(R.string.back, new ArrayList(), true));
        arrayList.add(new C0975(R.string.viewmenu, new ArrayList(), 1, true));
        arrayList.add(new C0975(R.string.text_size, new ArrayList<Integer>() {
            {
                add(1);
            }
        }, 2, true));
        arrayList.add(new C0975(R.string.orientmenu, new ArrayList<Integer>() {
            {
                add(2);
            }
        }, 2, true));
        arrayList.add(new C0975(R.string.sortmenu, new ArrayList<Integer>() {
            {
                add(3);
            }
        }, 2, true));
        arrayList.add(new C0975(R.string.filter, R.string.filterdescr, new ArrayList<Integer>() {
            {
                add(4);
            }
        }, 2, true));
        arrayList.add(new C0975(R.string.others, new ArrayList(), 1, true));
        arrayList.add(new C0975(R.string.langmenu, new ArrayList(), 2, true));
        arrayList.add(new C0975(R.string.settings_force_root, R.string.settings_force_root_description, new ArrayList<Integer>() {
            {
                add(5);
            }
        }, 2, true));
        if (C0987.f4489 >= 21) {
            arrayList.add(new C0975(R.string.settings_force_java_root_method, R.string.settings_force_java_root_method_description, new ArrayList<Integer>() {
                {
                    add(8);
                }
            }, 2, true));
        }
        arrayList.add(new C0975(R.string.dir_change, R.string.dir_change_descr, new ArrayList(), 2, true));
        arrayList.add(new C0975(R.string.days_on_up, R.string.days_on_up_descr, new ArrayList(), 2, true));
        arrayList.add(new C0975(R.string.setting_confirm_exit, R.string.setting_confirm_exit_descr, new ArrayList(), 3, true, "confirm_exit", true, new Runnable() {
            public void run() {
                if (C0987.m6073().getBoolean("confirm_exit", true)) {
                    C0987.m6073().edit().putBoolean("confirm_exit", false).commit();
                } else {
                    C0987.m6073().edit().putBoolean("confirm_exit", true).commit();
                }
                C0987.f4522.notifyDataSetChanged();
            }
        }));
        arrayList.add(new C0975(R.string.fast_start, R.string.fast_start_descr, new ArrayList(), 3, true, "fast_start", false, new Runnable() {
            public void run() {
                if (C0987.m6073().getBoolean("fast_start", true)) {
                    C0987.m6073().edit().putBoolean("fast_start", false).commit();
                } else {
                    C0987.m6073().edit().putBoolean("fast_start", true).commit();
                }
                C0987.f4522.notifyDataSetChanged();
                C0987.f4430 = true;
            }
        }));
        arrayList.add(new C0975(R.string.no_icon, R.string.no_icon_descr, new ArrayList(), 3, true, "no_icon", false, new Runnable() {
            public void run() {
                if (C0987.m6073().getBoolean("no_icon", false)) {
                    C0987.m6073().edit().putBoolean("no_icon", false).commit();
                } else {
                    C0987.m6073().edit().putBoolean("no_icon", true).commit();
                }
                C0987.f4522.notifyDataSetChanged();
                C0987.f4430 = true;
            }
        }));
        arrayList.add(new C0975(R.string.apk_create_option, R.string.apkdescr, new ArrayList<Integer>() {
            {
                add(6);
            }
        }, 2, true));
        arrayList.add(new C0975(R.string.change_icon_lp, R.string.change_icon_lp_descr, new ArrayList<Integer>() {
            {
                add(7);
            }
        }, 2, true));
        arrayList.add(new C0975(R.string.hide_notify, R.string.hide_notify_descr, new ArrayList(), 3, true, "hide_notify", false, new Runnable() {
            public void run() {
                if (C0987.m6073().getBoolean("hide_notify", false)) {
                    C0987.m6073().edit().putBoolean("hide_notify", false).commit();
                } else {
                    C0987.m6073().edit().putBoolean("hide_notify", true).commit();
                }
                C0987.f4522.notifyDataSetChanged();
                C0987.f4430 = true;
            }
        }));
        arrayList.add(new C0975(R.string.disable_autoupdate, R.string.disable_autoupdate_descr, new ArrayList(), 3, true, "disable_autoupdate", false, new Runnable() {
            public void run() {
                if (C0987.m6073().getBoolean("disable_autoupdate", false)) {
                    C0987.m6073().edit().putBoolean("disable_autoupdate", false).commit();
                } else {
                    C0987.m6073().edit().putBoolean("disable_autoupdate", true).commit();
                }
                C0987.f4522.notifyDataSetChanged();
                C0987.f4430 = true;
            }
        }));
        arrayList.add(new C0975(R.string.option_autoupdate_custom_patches, R.string.disable_autoupdate_descr, new ArrayList(), 3, true, "disable_autoupdate_custom_patches", false, new Runnable() {
            public void run() {
                if (C0987.m6073().getBoolean("disable_autoupdate_custom_patches", false)) {
                    C0987.m6073().edit().putBoolean("disable_autoupdate_custom_patches", false).commit();
                } else {
                    C0987.m6073().edit().putBoolean("disable_autoupdate_custom_patches", true).commit();
                }
                C0987.f4522.notifyDataSetChanged();
                C0987.f4430 = true;
            }
        }));
        arrayList.add(new C0975(R.string.vibration, R.string.vibration_descr, new ArrayList(), 3, true, "vibration", false, new Runnable() {
            public void run() {
                if (C0987.m6073().getBoolean("vibration", false)) {
                    C0987.m6073().edit().putBoolean("vibration", false).commit();
                } else {
                    C0987.m6073().edit().putBoolean("vibration", true).commit();
                }
                C0987.f4522.notifyDataSetChanged();
                C0987.f4430 = true;
            }
        }));
        arrayList.add(new C0975(R.string.option_remove_ads, R.string.option_remove_ads_descr, new ArrayList(), 3, true, "remove_ads", false, new Runnable() {
            public void run() {
                if (C0987.m6073().getBoolean("remove_ads", false)) {
                    C0987.m6073().edit().putBoolean("remove_ads", false).commit();
                } else {
                    C0987.m6073().edit().putBoolean("remove_ads", true).commit();
                }
                C0987.f4522.notifyDataSetChanged();
                C0987.f4430 = true;
            }
        }));
        arrayList.add(new C0975(R.string.option_analytic, R.string.option_analytic_descr, new ArrayList(), 3, true, "analytics", true, new Runnable() {
            public void run() {
                if (C0987.m6073().getBoolean("analytics", true)) {
                    C0987.m6073().edit().putBoolean("analytics", false).commit();
                } else {
                    C0987.m6073().edit().putBoolean("analytics", true).commit();
                }
                C0987.f4522.notifyDataSetChanged();
                C0987.f4430 = true;
            }
        }));
        arrayList.add(new C0975(R.string.option_SELinux_permissive, R.string.option_SELinux_permissive_descr, new ArrayList(), 3, true, "selinux_set_to_permissive", false, new Runnable() {
            public void run() {
                if (C0987.m6073().getBoolean("selinux_set_to_permissive", false)) {
                    C0987.m6073().edit().putBoolean("selinux_set_to_permissive", false).commit();
                } else {
                    C0987.m6073().edit().putBoolean("selinux_set_to_permissive", true).commit();
                }
                C0987.f4522.notifyDataSetChanged();
                C0987.f4430 = true;
            }
        }));
        arrayList.add(new C0975(R.string.option_detailed_logs, R.string.option_detailed_logs_descr, new ArrayList(), 3, true, "showLog", false, new Runnable() {
            public void run() {
                if (C0987.m6073().getBoolean("showLog", false)) {
                    C0987.m6073().edit().putBoolean("showLog", false).commit();
                    C0987.f4531 = false;
                } else {
                    C0987.m6073().edit().putBoolean("showLog", true).commit();
                    C0987.f4531 = true;
                }
                C0987.f4522.notifyDataSetChanged();
                C0987.f4430 = true;
            }
        }));
        arrayList.add(new C0975(R.string.help, new ArrayList(), 2, true));
        arrayList.add(new C0975(R.string.update_lp, new ArrayList(), 2, true));
        arrayList.add(new C0975(R.string.sendlog, new ArrayList(), 2, true));
        arrayList.add(new C0975(R.string.aboutmenu, new ArrayList(), 2, true));
        try {
            if (C0987.f4522 != null) {
                C0987.f4522.m5999(arrayList);
                C0987.f4432.m6139();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void toolbar_refresh_click(View view) {
        try {
            C0260 r3 = getSupportFragmentManager().m1403();
            C0987 r0 = frag;
            if (C0987.f4504 == null) {
                C0987 r02 = frag;
                C0987.f4504 = new C0949();
                C0987 r1 = frag;
                r3.m1565(R.id.fragment_filter, C0987.f4504);
                r3.m1567();
                return;
            }
            C0987 r03 = frag;
            r3.m1566(C0987.f4504);
            r3.m1567();
            C0987 r32 = frag;
            C0987.f4504.m5894();
            C0987 r33 = frag;
            C0987.f4504 = null;
            C0987 r34 = frag;
            C0987.f4439.m6020().filter(BuildConfig.FLAVOR);
            C0987 r35 = frag;
            C0987.f4439.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void toolbar_options_show(View view) {
        try {
            if (frag != null) {
                frag.m6148();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void toolbar_backups_click(View view) {
        new File(C0987.f4438 + "/Backup/").mkdirs();
        C0987 r3 = frag;
        C0987.m6084((Integer) 11);
        C0987.f4468.m5918(false);
        C0987.f4468.m5917(C0815.m5205((int) R.string.wait));
        Thread thread = new Thread(new Runnable() {
            public void run() {
                ArrayList arrayList = new ArrayList();
                String[] list = new File(C0987.f4438 + "/Backup").list();
                if (list == null || list.length == 0) {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            C0987 r0 = MainActivity.frag;
                            String r02 = C0815.m5205((int) R.string.warning);
                            C0987.m6062(r02, C0815.m5205((int) R.string.backups_not_found) + " " + C0987.f4438 + "/Backup");
                            C0987 r03 = MainActivity.frag;
                            C0987.m6094((Integer) 11);
                        }
                    });
                    return;
                }
                for (String str : list) {
                    try {
                        arrayList.add(new C0968(C0987.m6072(), new File(C0987.f4438 + "/Backup/" + str), true));
                    } catch (Exception unused) {
                    }
                }
                if (arrayList.size() != 0) {
                    C0987.f4452 = new ArrayAdapter<C0968>(MainActivity.this, R.layout.backup_context_menu, arrayList) {
                        ArrayAdapter<C0968> mAdapter = this;

                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
                         arg types: [?, android.view.ViewGroup, int]
                         candidates:
                          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
                          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
                        public View getView(int i, View view, ViewGroup viewGroup) {
                            C0968 r5 = (C0968) getItem(i);
                            if (view == null) {
                                view = ((LayoutInflater) C0987.m6072().getSystemService("layout_inflater")).inflate((int) R.layout.backup_context_menu, viewGroup, false);
                            }
                            TextView textView = (TextView) view.findViewById(R.id.conttext);
                            ImageView imageView = (ImageView) view.findViewById(R.id.imgIcon);
                            Button button = (Button) view.findViewById(R.id.button_share);
                            button.setTag(r5);
                            button.setOnClickListener(new View.OnClickListener() {
                                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                 method: com.chelpus.ˆ.ʻ(android.app.Activity, java.lang.String, java.lang.String):void
                                 arg types: [android.support.v4.ʻ.ˊ, java.lang.String, java.lang.String]
                                 candidates:
                                  com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String[], java.lang.String[]):int
                                  com.chelpus.ˆ.ʻ(java.lang.String, int, java.lang.String):android.text.SpannableString
                                  com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, java.lang.String):android.text.SpannableString
                                  com.chelpus.ˆ.ʻ(java.io.File, java.util.ArrayList<java.io.File>, java.lang.String):java.io.File
                                  com.chelpus.ˆ.ʻ(java.lang.String, java.util.ArrayList<java.lang.String>, boolean):java.util.ArrayList<com.chelpus.ʻ.ʼ.ᵔ>
                                  com.chelpus.ˆ.ʻ(int, int, java.io.File):void
                                  com.chelpus.ˆ.ʻ(android.app.Activity, java.io.File, java.lang.Integer):void
                                  com.chelpus.ˆ.ʻ(android.app.Activity, java.lang.String, java.lang.Integer):void
                                  com.chelpus.ˆ.ʻ(android.content.Context, java.lang.String, java.lang.String):void
                                  com.chelpus.ˆ.ʻ(java.io.File, java.io.File, java.lang.String[]):void
                                  com.chelpus.ˆ.ʻ(java.lang.String, java.io.File, java.util.ArrayList<java.io.File>):void
                                  com.chelpus.ˆ.ʻ(java.lang.String, java.lang.Runnable, java.lang.Runnable):void
                                  com.chelpus.ˆ.ʻ(java.lang.String, java.lang.String, java.util.ArrayList<com.lp.ʻ>):void
                                  com.chelpus.ˆ.ʻ(java.util.ArrayList<com.chelpus.ʻ.ʼ.ˏ>, java.lang.Runnable, java.lang.Runnable):void
                                  com.chelpus.ˆ.ʻ(boolean, boolean, boolean):void
                                  com.chelpus.ˆ.ʻ(java.nio.MappedByteBuffer, com.chelpus.ʻ.ʼ.ˉ, int):boolean
                                  com.chelpus.ˆ.ʻ(java.io.File, java.lang.String, java.util.ArrayList<java.io.File>):java.lang.String
                                  com.chelpus.ˆ.ʻ(java.util.ArrayList<java.io.File>, java.io.File, java.lang.String):void
                                  com.chelpus.ˆ.ʻ(android.app.Activity, java.lang.String, java.lang.String):void */
                                public void onClick(View view) {
                                    C0987.f4473 = ((C0968) view.getTag()).f4270.getAbsolutePath();
                                    if (C0987.f4473.toLowerCase().endsWith(".apks") && C0815.m5293(new File(C0987.f4473)) == null) {
                                        C0815.m5155((Activity) C0987.f4432.m6233(), C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.installer_error_bad_apks_file));
                                    } else if (C0987.f4432 != null) {
                                        C0987.f4432.m6216();
                                    }
                                }
                            });
                            Button button2 = (Button) view.findViewById(R.id.button_del);
                            button2.setTag(r5);
                            button2.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    final C0968 r5 = (C0968) view.getTag();
                                    AnonymousClass1 r0 = new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (i == -1) {
                                                r5.f4270.delete();
                                                AnonymousClass1.this.mAdapter.remove(r5);
                                                AnonymousClass1.this.mAdapter.notifyDataSetChanged();
                                            }
                                        }
                                    };
                                    C0959 r1 = new C0959(C0987.f4432.m6233()).m5943(C0815.m5205((int) R.string.warning));
                                    C0815.m5156(r1.m5950(C0815.m5205((int) R.string.backup_delete_message) + " " + r5.f4266 + "?").m5944(C0815.m5205((int) R.string.Yes), r0).m5954(C0815.m5205((int) R.string.no), r0).m5947());
                                }
                            });
                            imageView.setImageDrawable(null);
                            textView.setTextAppearance(getContext(), C0987.m6076());
                            textView.setCompoundDrawablePadding((int) ((C0987.m6069().getDisplayMetrics().density * 5.0f) + 0.5f));
                            textView.setTextColor(-1);
                            textView.setText(C0815.m5137(r5.f4266, "#A1C2F3", "bold"));
                            if (r5.f4271) {
                                textView.append(C0815.m5137(" - " + C0815.m5205((int) R.string.splitted_apk_attribute) + " -", "#A10000", "bold"));
                            }
                            textView.append(C0815.m5137("\n" + C0815.m5205((int) R.string.restore_version) + ": " + r5.f4268 + " " + C0815.m5205((int) R.string.restore_build) + ": " + r5.f4269, "#a0a0a0", BuildConfig.FLAVOR));
                            imageView.setImageDrawable(r5.f4267);
                            return view;
                        }
                    };
                    MainActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            C0987.f4452.sort(new byNameApkFile());
                            C0987 r0 = MainActivity.frag;
                            C0987.m6094((Integer) 28);
                            C0987 r1 = MainActivity.frag;
                            C0987.m6084((Integer) 28);
                            C0987 r02 = MainActivity.frag;
                            C0987.m6094((Integer) 11);
                        }
                    });
                    return;
                }
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        C0987 r0 = MainActivity.frag;
                        String r02 = C0815.m5205((int) R.string.warning);
                        C0987.m6062(r02, C0815.m5205((int) R.string.backups_not_found) + " " + C0987.f4438 + "/Backup");
                        C0987 r03 = MainActivity.frag;
                        C0987.m6094((Integer) 11);
                    }
                });
            }
        });
        thread.setPriority(10);
        thread.start();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0 || keyEvent.getKeyCode() != 82) {
            return super.dispatchKeyEvent(keyEvent);
        }
        if (Build.VERSION.SDK_INT < 11) {
            return true;
        }
        DrawerLayout drawerLayout = this.mDrawerLayout;
        if (drawerLayout == null || !drawerLayout.m887(8388611)) {
            this.mDrawerLayout.m884(8388611);
            return true;
        }
        this.mDrawerLayout.m871();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lp.ﾞﾞ.ʻ(java.lang.String, android.widget.ListView, boolean):void
     arg types: [java.lang.String, android.widget.ListView, int]
     candidates:
      com.lp.ﾞﾞ.ʻ(android.content.Context, java.lang.String, java.lang.String):void
      com.lp.ﾞﾞ.ʻ(android.widget.TextView, java.lang.String, com.chelpus.ʻ.ʼ):void
      com.lp.ﾞﾞ.ʻ(com.lp.ﾞﾞ, java.lang.String, java.lang.String):void
      com.lp.ﾞﾞ.ʻ(com.lp.ﾞﾞ, java.util.ArrayList, java.util.Comparator):void
      com.lp.ﾞﾞ.ʻ(java.util.ArrayList<com.lp.ᵔ>, boolean, boolean):void
      com.lp.ﾞﾞ.ʻ(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle):android.view.View
      com.lp.ﾞﾞ.ʻ(int, int, android.content.Intent):void
      com.lp.ﾞﾞ.ʻ(int, java.lang.String[], int[]):void
      com.lp.ﾞﾞ.ʻ(java.lang.String, java.lang.Runnable, java.lang.Runnable):boolean
      android.support.v4.ʻ.ˉ.ʻ(android.content.Context, java.lang.String, android.os.Bundle):android.support.v4.ʻ.ˉ
      android.support.v4.ʻ.ˉ.ʻ(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle):android.view.View
      android.support.v4.ʻ.ˉ.ʻ(int, boolean, int):android.view.animation.Animation
      android.support.v4.ʻ.ˉ.ʻ(int, int, android.content.Intent):void
      android.support.v4.ʻ.ˉ.ʻ(int, java.lang.String[], int[]):void
      android.support.v4.ʻ.ˉ.ʻ(android.app.Activity, android.util.AttributeSet, android.os.Bundle):void
      android.support.v4.ʻ.ˉ.ʻ(android.content.Context, android.util.AttributeSet, android.os.Bundle):void
      android.support.v4.ʻ.ˉ.ʻ(android.content.Intent, int, android.os.Bundle):void
      com.lp.ﾞﾞ.ʻ(java.lang.String, android.widget.ListView, boolean):void */
    public void toolbar_rebuild_click(View view) {
        try {
            C0987.f4439.m6016();
            C0987.f4440 = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        LinearLayout linearLayout = (LinearLayout) View.inflate(this, R.layout.file_browser, null);
        Dialog r0 = new C0959(this, true).m5940(linearLayout).m5947();
        r0.setCancelable(false);
        r0.setOnKeyListener(new DialogInterface.OnKeyListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.lp.ﾞﾞ.ʻ(java.lang.String, android.widget.ListView, boolean):void
             arg types: [java.lang.String, android.widget.ListView, int]
             candidates:
              com.lp.ﾞﾞ.ʻ(android.content.Context, java.lang.String, java.lang.String):void
              com.lp.ﾞﾞ.ʻ(android.widget.TextView, java.lang.String, com.chelpus.ʻ.ʼ):void
              com.lp.ﾞﾞ.ʻ(com.lp.ﾞﾞ, java.lang.String, java.lang.String):void
              com.lp.ﾞﾞ.ʻ(com.lp.ﾞﾞ, java.util.ArrayList, java.util.Comparator):void
              com.lp.ﾞﾞ.ʻ(java.util.ArrayList<com.lp.ᵔ>, boolean, boolean):void
              com.lp.ﾞﾞ.ʻ(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle):android.view.View
              com.lp.ﾞﾞ.ʻ(int, int, android.content.Intent):void
              com.lp.ﾞﾞ.ʻ(int, java.lang.String[], int[]):void
              com.lp.ﾞﾞ.ʻ(java.lang.String, java.lang.Runnable, java.lang.Runnable):boolean
              android.support.v4.ʻ.ˉ.ʻ(android.content.Context, java.lang.String, android.os.Bundle):android.support.v4.ʻ.ˉ
              android.support.v4.ʻ.ˉ.ʻ(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle):android.view.View
              android.support.v4.ʻ.ˉ.ʻ(int, boolean, int):android.view.animation.Animation
              android.support.v4.ʻ.ˉ.ʻ(int, int, android.content.Intent):void
              android.support.v4.ʻ.ˉ.ʻ(int, java.lang.String[], int[]):void
              android.support.v4.ʻ.ˉ.ʻ(android.app.Activity, android.util.AttributeSet, android.os.Bundle):void
              android.support.v4.ʻ.ˉ.ʻ(android.content.Context, android.util.AttributeSet, android.os.Bundle):void
              android.support.v4.ʻ.ˉ.ʻ(android.content.Intent, int, android.os.Bundle):void
              com.lp.ﾞﾞ.ʻ(java.lang.String, android.widget.ListView, boolean):void */
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                System.out.println(keyEvent);
                if (i == 4 && keyEvent.getAction() == 1) {
                    try {
                        C0987.f4473 = BuildConfig.FLAVOR;
                        if (((C0987.C0992) MainActivity.frag.f4588.getAdapter().getItem(0)).f5168.equals("../")) {
                            MainActivity.frag.m6131(((C0987.C0992) MainActivity.frag.f4588.getAdapter().getItem(0)).f5167, MainActivity.frag.f4588, true);
                        } else {
                            dialogInterface.dismiss();
                        }
                    } catch (IndexOutOfBoundsException unused) {
                        dialogInterface.dismiss();
                    } catch (Exception unused2) {
                        dialogInterface.dismiss();
                    }
                }
                return true;
            }
        });
        r0.show();
        frag.f4586 = (TextView) r0.findViewById(R.id.path);
        String parent = new File(C0987.f4483).getParent();
        while (new File(new File(parent).getParent()).getParent() != null) {
            try {
                parent = new File(parent).getParent();
                PrintStream printStream = System.out;
                printStream.println("Parent directory:" + parent);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        frag.f4585 = parent;
        ((ListView) linearLayout.findViewById(R.id.list)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* JADX WARN: Type inference failed for: r2v1, types: [android.widget.Adapter] */
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public void onItemClick(android.widget.AdapterView<?> r1, android.view.View r2, int r3, long r4) {
                /*
                    r0 = this;
                    android.widget.Adapter r2 = r1.getAdapter()
                    java.lang.Object r2 = r2.getItem(r3)
                    com.lp.ﾞﾞ$ʿ r2 = (com.lp.C0987.C0992) r2
                    java.io.File r3 = new java.io.File
                    java.lang.String r4 = r2.f5169
                    r3.<init>(r4)
                    boolean r4 = r3.isDirectory()
                    if (r4 == 0) goto L_0x0084
                    boolean r4 = r3.canRead()
                    if (r4 == 0) goto L_0x003b
                    java.io.File[] r4 = r3.listFiles()
                    if (r4 == 0) goto L_0x003b
                    com.lp.ﾞﾞ r3 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag
                    android.widget.ListView r1 = (android.widget.ListView) r1
                    r3.f4588 = r1
                    com.lp.ﾞﾞ r3 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag
                    java.io.File r4 = new java.io.File
                    java.lang.String r2 = r2.f5169
                    r4.<init>(r2)
                    java.lang.String r2 = r4.getPath()
                    r4 = 1
                    r3.m6131(r2, r1, r4)
                    goto L_0x0098
                L_0x003b:
                    com.lp.ʼ r1 = new com.lp.ʼ
                    com.lp.ﾞﾞ r2 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag
                    android.support.v4.ʻ.ˊ r2 = r2.m6233()
                    r1.<init>(r2)
                    r2 = 17301659(0x108009b, float:2.497969E-38)
                    com.lp.ʼ r1 = r1.m5936(r2)
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r4 = "["
                    r2.append(r4)
                    java.lang.String r3 = r3.getName()
                    r2.append(r3)
                    java.lang.String r3 = "] "
                    r2.append(r3)
                    r3 = 2131689849(0x7f0f0179, float:1.9008725E38)
                    java.lang.String r3 = com.chelpus.C0815.m5205(r3)
                    r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    com.lp.ʼ r1 = r1.m5943(r2)
                    r2 = 0
                    java.lang.String r3 = "OK"
                    com.lp.ʼ r1 = r1.m5944(r3, r2)
                    android.app.Dialog r1 = r1.m5947()
                    com.chelpus.C0815.m5156(r1)
                    goto L_0x0098
                L_0x0084:
                    com.lp.ﾞﾞ r1 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag
                    r1.f4587 = r2
                    ru.pKkcGXHI.kKSaIWSZS.MainActivity r1 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.this
                    java.io.File r2 = new java.io.File
                    com.lp.ﾞﾞ r3 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag
                    com.lp.ﾞﾞ$ʿ r3 = r3.f4587
                    java.lang.String r3 = r3.f5169
                    r2.<init>(r3)
                    r1.showFileMenu(r2)
                L_0x0098:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: ru.pKkcGXHI.kKSaIWSZS.MainActivity.AnonymousClass35.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
            }
        });
        frag.f4588 = (ListView) linearLayout.findViewById(R.id.list);
        try {
            frag.m6131(frag.f4585, (ListView) linearLayout.findViewById(R.id.list), true);
        } catch (Exception e3) {
            e3.printStackTrace();
            try {
                frag.f4585 = new File(C0987.f4438).getParent();
                frag.m6131(frag.f4585, (ListView) linearLayout.findViewById(R.id.list), true);
            } catch (Exception unused) {
                frag.f4585 = C0987.f4438;
                C0987 r2 = frag;
                r2.m6131(r2.f4585, (ListView) linearLayout.findViewById(R.id.list), true);
            }
        }
    }

    public void showFileMenu(File file) {
        C0987.f4473 = file.getAbsolutePath();
        ArrayList arrayList = new ArrayList();
        String name = file.getName();
        if (name.equals("core.jar") || name.equals("core.odex") || name.equals("core-libart.odex") || name.equals("services.jar") || name.equals("services.odex") || name.equals("core-libart.jar") || name.equals("core-oj.jar") || name.equals("conscrypt.jar") || name.equals("boot.oat") || name.endsWith("conscrypt.oat") || name.endsWith("core-oj.oat") || name.endsWith("framework.oat")) {
            if (!name.endsWith(".jar")) {
                arrayList.add(Integer.valueOf((int) R.string.context_patch_framework));
            } else if (C0815.m5277(file)) {
                arrayList.add(Integer.valueOf((int) R.string.context_patch_framework));
            } else {
                C0987 r2 = frag;
                C0987.m6062(C0815.m5205((int) R.string.warning), C0815.m5205((int) R.string.error_classes_not_found));
            }
        }
        if (name.toLowerCase().endsWith(".lpzip")) {
            arrayList.add(Integer.valueOf((int) R.string.import_to_lucky_patcher));
        }
        if (name.endsWith(".apk")) {
            System.out.println("integrate dalvik code...");
            if (C0987.f4474) {
                try {
                    if (C0815.m5134(C0815.m5292(file.getAbsolutePath()).packageName, 0) != null) {
                        arrayList.add(Integer.valueOf((int) R.string.context_integrate_dalvik));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            arrayList.add(Integer.valueOf((int) R.string.context_install));
            if (!C0987.f4474 && !C0987.f4532.booleanValue()) {
                arrayList.add(Integer.valueOf((int) R.string.context_uninstall_if_installed));
            }
            arrayList.add(Integer.valueOf((int) R.string.context_rebuild));
            if (C0987.f4474) {
                arrayList.add(Integer.valueOf((int) R.string.context_install_as_system));
            }
            arrayList.add(Integer.valueOf((int) R.string.delete_file));
            arrayList.add(Integer.valueOf((int) R.string.share_this_app));
        }
        if (name.endsWith(".apks")) {
            System.out.println("integrate dalvik code...");
            if (C0987.f4474) {
                try {
                    if (C0815.m5134(C0815.m5300(file), 0) != null) {
                        arrayList.add(Integer.valueOf((int) R.string.context_integrate_dalvik));
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            arrayList.add(Integer.valueOf((int) R.string.context_install));
            if (!C0987.f4474 && !C0987.f4532.booleanValue()) {
                arrayList.add(Integer.valueOf((int) R.string.context_uninstall_if_installed));
            }
            arrayList.add(Integer.valueOf((int) R.string.context_rebuild));
            arrayList.add(Integer.valueOf((int) R.string.delete_file));
            arrayList.add(Integer.valueOf((int) R.string.share_this_app));
        }
        if (arrayList.size() != 0) {
            C0987.f4453 = new ArrayAdapter<Integer>(frag.m6233(), R.layout.icon_context_menu, arrayList) {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
                 arg types: [?, android.view.ViewGroup, int]
                 candidates:
                  ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
                  ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
                public View getView(int i, View view, ViewGroup viewGroup) {
                    if (view == null) {
                        view = ((LayoutInflater) C0987.m6072().getSystemService("layout_inflater")).inflate((int) R.layout.icon_context_menu, viewGroup, false);
                    }
                    TextView textView = (TextView) view.findViewById(R.id.conttext);
                    ImageView imageView = (ImageView) view.findViewById(R.id.imgIcon);
                    imageView.setImageDrawable(null);
                    textView.setTextAppearance(getContext(), C0987.m6076());
                    textView.setTextColor(-1);
                    try {
                        switch (((Integer) getItem(i)).intValue()) {
                            case R.string.context_install /*2131689631*/:
                                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.context_custom_patch));
                                imageView.setColorFilter(Color.parseColor("#ffff99"), PorterDuff.Mode.MULTIPLY);
                                textView.setTextColor(Color.parseColor("#ffff99"));
                                break;
                            case R.string.context_install_as_system /*2131689632*/:
                                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.move_to));
                                imageView.setColorFilter(Color.parseColor("#ffcc66"), PorterDuff.Mode.MULTIPLY);
                                textView.setTextColor(Color.parseColor("#ffcc66"));
                                break;
                            case R.string.context_integrate_dalvik /*2131689633*/:
                                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.context_permissions));
                                imageView.setColorFilter(Color.parseColor("#9999cc"), PorterDuff.Mode.MULTIPLY);
                                textView.setTextColor(Color.parseColor("#9999cc"));
                                break;
                            case R.string.context_patch_framework /*2131689638*/:
                                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.context_permissions));
                                imageView.setColorFilter(Color.parseColor("#9999cc"), PorterDuff.Mode.MULTIPLY);
                                textView.setTextColor(Color.parseColor("#9999cc"));
                                break;
                            case R.string.context_rebuild /*2131689639*/:
                                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.context_create_apk));
                                imageView.setColorFilter(Color.parseColor("#c2f055"), PorterDuff.Mode.MULTIPLY);
                                textView.setTextColor(Color.parseColor("#c2f055"));
                                break;
                            case R.string.context_uninstall_if_installed /*2131689654*/:
                                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.context_custom_patch));
                                imageView.setColorFilter(Color.parseColor("#ffff99"), PorterDuff.Mode.MULTIPLY);
                                textView.setTextColor(Color.parseColor("#ffff99"));
                                break;
                            case R.string.delete_file /*2131689795*/:
                                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.context_recycle));
                                imageView.setColorFilter(Color.parseColor("#fe6c00"), PorterDuff.Mode.MULTIPLY);
                                textView.setTextColor(Color.parseColor("#fe6c00"));
                                break;
                            case R.string.import_to_lucky_patcher /*2131689912*/:
                                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.context_custom_patch));
                                imageView.setColorFilter(Color.parseColor("#ffff99"), PorterDuff.Mode.MULTIPLY);
                                textView.setTextColor(Color.parseColor("#ffff99"));
                                break;
                            case R.string.share_this_app /*2131690178*/:
                                imageView.setImageDrawable(C0987.m6069().getDrawable(R.drawable.context_share));
                                imageView.setColorFilter(Color.parseColor("#cccccc"), PorterDuff.Mode.MULTIPLY);
                                textView.setTextColor(Color.parseColor("#cccccc"));
                                break;
                        }
                    } catch (OutOfMemoryError e) {
                        e.printStackTrace();
                        System.gc();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    textView.setCompoundDrawablePadding((int) ((C0987.m6069().getDisplayMetrics().density * 5.0f) + 0.5f));
                    textView.setText(C0815.m5205(((Integer) getItem(i)).intValue()));
                    textView.setTypeface(null, 1);
                    return view;
                }
            };
            C0987 r12 = frag;
            C0987.m6094((Integer) 7);
            C0987 r0 = frag;
            C0987.m6084((Integer) 7);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lp.ﾞﾞ.ʻ(java.lang.String, android.widget.ListView, boolean):void
     arg types: [java.lang.String, android.widget.ListView, int]
     candidates:
      com.lp.ﾞﾞ.ʻ(android.content.Context, java.lang.String, java.lang.String):void
      com.lp.ﾞﾞ.ʻ(android.widget.TextView, java.lang.String, com.chelpus.ʻ.ʼ):void
      com.lp.ﾞﾞ.ʻ(com.lp.ﾞﾞ, java.lang.String, java.lang.String):void
      com.lp.ﾞﾞ.ʻ(com.lp.ﾞﾞ, java.util.ArrayList, java.util.Comparator):void
      com.lp.ﾞﾞ.ʻ(java.util.ArrayList<com.lp.ᵔ>, boolean, boolean):void
      com.lp.ﾞﾞ.ʻ(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle):android.view.View
      com.lp.ﾞﾞ.ʻ(int, int, android.content.Intent):void
      com.lp.ﾞﾞ.ʻ(int, java.lang.String[], int[]):void
      com.lp.ﾞﾞ.ʻ(java.lang.String, java.lang.Runnable, java.lang.Runnable):boolean
      android.support.v4.ʻ.ˉ.ʻ(android.content.Context, java.lang.String, android.os.Bundle):android.support.v4.ʻ.ˉ
      android.support.v4.ʻ.ˉ.ʻ(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle):android.view.View
      android.support.v4.ʻ.ˉ.ʻ(int, boolean, int):android.view.animation.Animation
      android.support.v4.ʻ.ˉ.ʻ(int, int, android.content.Intent):void
      android.support.v4.ʻ.ˉ.ʻ(int, java.lang.String[], int[]):void
      android.support.v4.ʻ.ˉ.ʻ(android.app.Activity, android.util.AttributeSet, android.os.Bundle):void
      android.support.v4.ʻ.ˉ.ʻ(android.content.Context, android.util.AttributeSet, android.os.Bundle):void
      android.support.v4.ʻ.ˉ.ʻ(android.content.Intent, int, android.os.Bundle):void
      com.lp.ﾞﾞ.ʻ(java.lang.String, android.widget.ListView, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0086 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void show_file_explorer(java.lang.String r6) {
        /*
            r5 = this;
            r0 = 0
            com.lp.ᵢ r1 = com.lp.C0987.f4439     // Catch:{ Exception -> 0x0009 }
            r1.m6016()     // Catch:{ Exception -> 0x0009 }
            com.lp.C0987.f4440 = r0     // Catch:{ Exception -> 0x0009 }
            goto L_0x000d
        L_0x0009:
            r1 = move-exception
            r1.printStackTrace()
        L_0x000d:
            r1 = 2131427403(0x7f0b004b, float:1.8476421E38)
            android.view.View r0 = android.view.View.inflate(r5, r1, r0)
            android.widget.LinearLayout r0 = (android.widget.LinearLayout) r0
            com.lp.ʼ r1 = new com.lp.ʼ
            r2 = 1
            r1.<init>(r5, r2)
            com.lp.ʼ r1 = r1.m5940(r0)
            android.app.Dialog r1 = r1.m5947()
            r2 = 0
            r1.setCancelable(r2)
            ru.pKkcGXHI.kKSaIWSZS.MainActivity$37 r3 = new ru.pKkcGXHI.kKSaIWSZS.MainActivity$37
            r3.<init>()
            r1.setOnKeyListener(r3)
            r1.show()
            com.lp.ﾞﾞ r3 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag
            r4 = 2131296487(0x7f0900e7, float:1.8210892E38)
            android.view.View r1 = r1.findViewById(r4)
            android.widget.TextView r1 = (android.widget.TextView) r1
            r3.f4586 = r1
            com.lp.ﾞﾞ r1 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag
            r1.f4585 = r6
            r1 = 2131296445(0x7f0900bd, float:1.8210807E38)
            android.view.View r3 = r0.findViewById(r1)
            android.widget.ListView r3 = (android.widget.ListView) r3
            ru.pKkcGXHI.kKSaIWSZS.MainActivity$38 r4 = new ru.pKkcGXHI.kKSaIWSZS.MainActivity$38
            r4.<init>()
            r3.setOnItemClickListener(r4)
            com.lp.ﾞﾞ r3 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag
            android.view.View r4 = r0.findViewById(r1)
            android.widget.ListView r4 = (android.widget.ListView) r4
            r3.f4588 = r4
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x0086 }
            com.lp.ﾞﾞ r4 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag     // Catch:{ Exception -> 0x0086 }
            java.lang.String r4 = r4.f4585     // Catch:{ Exception -> 0x0086 }
            r3.println(r4)     // Catch:{ Exception -> 0x0086 }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x0086 }
            r3.println(r6)     // Catch:{ Exception -> 0x0086 }
            java.io.PrintStream r6 = java.lang.System.out     // Catch:{ Exception -> 0x0086 }
            com.lp.ﾞﾞ r3 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag     // Catch:{ Exception -> 0x0086 }
            java.lang.String r3 = r3.f4585     // Catch:{ Exception -> 0x0086 }
            r6.println(r3)     // Catch:{ Exception -> 0x0086 }
            com.lp.ﾞﾞ r6 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag     // Catch:{ Exception -> 0x0086 }
            com.lp.ﾞﾞ r3 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag     // Catch:{ Exception -> 0x0086 }
            java.lang.String r3 = r3.f4585     // Catch:{ Exception -> 0x0086 }
            android.view.View r4 = r0.findViewById(r1)     // Catch:{ Exception -> 0x0086 }
            android.widget.ListView r4 = (android.widget.ListView) r4     // Catch:{ Exception -> 0x0086 }
            r6.m6131(r3, r4, r2)     // Catch:{ Exception -> 0x0086 }
            goto L_0x00b8
        L_0x0086:
            com.lp.ﾞﾞ r6 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag     // Catch:{ Exception -> 0x00a5 }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r4 = com.lp.C0987.f4438     // Catch:{ Exception -> 0x00a5 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r3 = r3.getParent()     // Catch:{ Exception -> 0x00a5 }
            r6.f4585 = r3     // Catch:{ Exception -> 0x00a5 }
            com.lp.ﾞﾞ r6 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag     // Catch:{ Exception -> 0x00a5 }
            com.lp.ﾞﾞ r3 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r3 = r3.f4585     // Catch:{ Exception -> 0x00a5 }
            android.view.View r4 = r0.findViewById(r1)     // Catch:{ Exception -> 0x00a5 }
            android.widget.ListView r4 = (android.widget.ListView) r4     // Catch:{ Exception -> 0x00a5 }
            r6.m6131(r3, r4, r2)     // Catch:{ Exception -> 0x00a5 }
            goto L_0x00b8
        L_0x00a5:
            com.lp.ﾞﾞ r6 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag
            java.lang.String r3 = com.lp.C0987.f4438
            r6.f4585 = r3
            com.lp.ﾞﾞ r6 = ru.pKkcGXHI.kKSaIWSZS.MainActivity.frag
            java.lang.String r3 = r6.f4585
            android.view.View r0 = r0.findViewById(r1)
            android.widget.ListView r0 = (android.widget.ListView) r0
            r6.m6131(r3, r0, r2)
        L_0x00b8:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: ru.pKkcGXHI.kKSaIWSZS.MainActivity.show_file_explorer(java.lang.String):void");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.action_help) {
            startActivity(new Intent(this, HelpActivity.class));
        } else if (itemId != R.id.action_info) {
            if (itemId == R.id.action_search) {
                toolbar_refresh_click(null);
            }
        } else if (C0987.f4432 != null) {
            C0987.f4432.m6160();
        }
        menuItem.getItemId();
        menuItem.getItemId();
        return super.onOptionsItemSelected(menuItem);
    }

    final class byNameApkFile implements Comparator<C0968> {
        byNameApkFile() {
        }

        public int compare(C0968 r3, C0968 r4) {
            if (r3 == null || r4 == null) {
                throw new ClassCastException();
            }
            if (r3.f4265.equals(r4.f4265)) {
                if (r3.f4269 > r4.f4269) {
                    return 1;
                }
                if (r3.f4269 < r4.f4269) {
                    return -1;
                }
                if (r3.f4269 == r4.f4269) {
                    return 0;
                }
            }
            return r3.f4266.compareToIgnoreCase(r4.f4266);
        }
    }

    public void mod_market_check(View view) {
        C0987.f4432.m6202();
    }
}
