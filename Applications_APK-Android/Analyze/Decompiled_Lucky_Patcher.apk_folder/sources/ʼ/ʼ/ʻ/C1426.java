package ʼ.ʼ.ʻ;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/* renamed from: ʼ.ʼ.ʻ.ʽ  reason: contains not printable characters */
/* compiled from: HexDumpEncoder */
public class C1426 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static C1427 f7375 = new C1427();

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m7921(byte[] bArr) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            f7375.m7922(bArr, 0, bArr.length, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            StringBuilder sb = new StringBuilder();
            int i = 0;
            while (i < byteArray.length) {
                int i2 = i + 32;
                int min = Math.min(i2, byteArray.length);
                StringBuilder sb2 = new StringBuilder();
                StringBuilder sb3 = new StringBuilder();
                sb2.append(String.format("%08x: ", Integer.valueOf(i / 2)));
                while (i < min) {
                    sb2.append(Character.valueOf((char) byteArray[i]));
                    sb2.append(Character.valueOf((char) byteArray[i + 1]));
                    int i3 = i + 2;
                    if (i3 % 4 == 0) {
                        sb2.append(' ');
                    }
                    byte b = bArr[i / 2];
                    if (b < 32 || b >= Byte.MAX_VALUE) {
                        sb3.append('.');
                    } else {
                        sb3.append(Character.valueOf((char) b));
                    }
                    i = i3;
                }
                sb.append(sb2.toString());
                for (int length = sb2.length(); length < 50; length++) {
                    sb.append(' ');
                }
                sb.append("  ");
                sb.append((CharSequence) sb3);
                sb.append("\n");
                i = i2;
            }
            return sb.toString();
        } catch (IOException e) {
            throw new IllegalStateException(e.getClass().getName() + ": " + e.getMessage());
        }
    }
}
