package ʼ.ʼ.ʻ;

import java.security.MessageDigest;
import java.security.PrivateKey;
import javax.crypto.Cipher;

/* renamed from: ʼ.ʼ.ʻ.ˊ  reason: contains not printable characters */
/* compiled from: ZipSignature */
public class C1432 {

    /* renamed from: ʻ  reason: contains not printable characters */
    byte[] f7390 = {48, 33};

    /* renamed from: ʼ  reason: contains not printable characters */
    byte[] f7391 = {48, 9, 6, 5, 43, 14, 3, 2, 26, 5, 0};

    /* renamed from: ʽ  reason: contains not printable characters */
    byte[] f7392 = {4, 20};

    /* renamed from: ʾ  reason: contains not printable characters */
    Cipher f7393 = Cipher.getInstance("RSA/ECB/PKCS1Padding");

    /* renamed from: ʿ  reason: contains not printable characters */
    MessageDigest f7394 = MessageDigest.getInstance("SHA1");

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7940(PrivateKey privateKey) {
        this.f7393.init(1, privateKey);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7941(byte[] bArr) {
        this.f7394.update(bArr);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public byte[] m7942() {
        this.f7393.update(this.f7390);
        this.f7393.update(this.f7391);
        this.f7393.update(this.f7392);
        this.f7393.update(this.f7394.digest());
        return this.f7393.doFinal();
    }
}
