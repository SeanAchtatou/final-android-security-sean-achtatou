package ʼ.ʼ.ʻ;

import java.io.OutputStream;

/* renamed from: ʼ.ʼ.ʻ.ʾ  reason: contains not printable characters */
/* compiled from: HexEncoder */
public class C1427 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final byte[] f7376 = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};

    /* renamed from: ʼ  reason: contains not printable characters */
    protected final byte[] f7377 = new byte[128];

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7923() {
        int i = 0;
        while (true) {
            byte[] bArr = this.f7376;
            if (i < bArr.length) {
                this.f7377[bArr[i]] = (byte) i;
                i++;
            } else {
                byte[] bArr2 = this.f7377;
                bArr2[65] = bArr2[97];
                bArr2[66] = bArr2[98];
                bArr2[67] = bArr2[99];
                bArr2[68] = bArr2[100];
                bArr2[69] = bArr2[101];
                bArr2[70] = bArr2[102];
                return;
            }
        }
    }

    public C1427() {
        m7923();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m7922(byte[] bArr, int i, int i2, OutputStream outputStream) {
        for (int i3 = i; i3 < i + i2; i3++) {
            byte b = bArr[i3] & 255;
            outputStream.write(this.f7376[b >>> 4]);
            outputStream.write(this.f7376[b & 15]);
        }
        return i2 * 2;
    }
}
