package ʼ.ʼ.ʻ;

import java.security.PrivateKey;
import java.security.cert.X509Certificate;

/* renamed from: ʼ.ʼ.ʻ.ʿ  reason: contains not printable characters */
/* compiled from: KeySet */
public class C1428 {

    /* renamed from: ʻ  reason: contains not printable characters */
    String f7378;

    /* renamed from: ʼ  reason: contains not printable characters */
    X509Certificate f7379 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    PrivateKey f7380 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    byte[] f7381 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    String f7382 = "SHA1withRSA";

    public C1428() {
    }

    public C1428(String str, X509Certificate x509Certificate, PrivateKey privateKey, byte[] bArr) {
        this.f7378 = str;
        this.f7379 = x509Certificate;
        this.f7380 = privateKey;
        this.f7381 = bArr;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m7924() {
        return this.f7378;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7925(String str) {
        this.f7378 = str;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public X509Certificate m7929() {
        return this.f7379;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7927(X509Certificate x509Certificate) {
        this.f7379 = x509Certificate;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public PrivateKey m7930() {
        return this.f7380;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7926(PrivateKey privateKey) {
        this.f7380 = privateKey;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public byte[] m7931() {
        return this.f7381;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7928(byte[] bArr) {
        this.f7381 = bArr;
    }
}
