package ʼ.ʼ.ʻ;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import ʼ.ʻ.C1421;
import ʼ.ʻ.C1422;

/* renamed from: ʼ.ʼ.ʻ.ʼ  reason: contains not printable characters */
/* compiled from: Base64 */
public class C1425 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static Method f7368;

    /* renamed from: ʼ  reason: contains not printable characters */
    static Method f7369;

    /* renamed from: ʽ  reason: contains not printable characters */
    static Object f7370;

    /* renamed from: ʾ  reason: contains not printable characters */
    static Method f7371;

    /* renamed from: ʿ  reason: contains not printable characters */
    static Object f7372;

    /* renamed from: ˆ  reason: contains not printable characters */
    static Method f7373;

    /* renamed from: ˈ  reason: contains not printable characters */
    static C1421 f7374 = C1422.m7912(C1425.class.getName());

    static {
        try {
            Class<?> cls = Class.forName("android.util.Base64");
            f7368 = cls.getMethod("encode", byte[].class, Integer.TYPE);
            f7369 = cls.getMethod("decode", byte[].class, Integer.TYPE);
            C1421 r7 = f7374;
            r7.m7910(cls.getName() + " is available.");
        } catch (ClassNotFoundException unused) {
        } catch (Exception e) {
            f7374.m7907("Failed to initialize use of android.util.Base64", e);
        }
        try {
            Class<?> cls2 = Class.forName("org.bouncycastle.util.encoders.Base64Encoder");
            f7370 = cls2.newInstance();
            f7371 = cls2.getMethod("encode", byte[].class, Integer.TYPE, Integer.TYPE, OutputStream.class);
            C1421 r2 = f7374;
            r2.m7910(cls2.getName() + " is available.");
            f7373 = cls2.getMethod("decode", byte[].class, Integer.TYPE, Integer.TYPE, OutputStream.class);
        } catch (ClassNotFoundException unused2) {
        } catch (Exception e2) {
            f7374.m7907("Failed to initialize use of org.bouncycastle.util.encoders.Base64Encoder", e2);
        }
        if (f7368 == null && f7371 == null) {
            throw new IllegalStateException("No base64 encoder implementation is available.");
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m7920(byte[] bArr) {
        try {
            if (f7368 != null) {
                return new String((byte[]) f7368.invoke(null, bArr, 2));
            } else if (f7371 != null) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                f7371.invoke(f7370, bArr, 0, Integer.valueOf(bArr.length), byteArrayOutputStream);
                return new String(byteArrayOutputStream.toByteArray());
            } else {
                throw new IllegalStateException("No base64 encoder implementation is available.");
            }
        } catch (Exception e) {
            throw new IllegalStateException(e.getClass().getName() + ": " + e.getMessage());
        }
    }
}
