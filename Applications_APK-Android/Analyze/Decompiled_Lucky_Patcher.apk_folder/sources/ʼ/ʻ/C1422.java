package ʼ.ʻ;

import java.util.Map;
import java.util.TreeMap;

/* renamed from: ʼ.ʻ.ʽ  reason: contains not printable characters */
/* compiled from: LoggerManager */
public class C1422 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static C1420 f7365 = new C1423();

    /* renamed from: ʼ  reason: contains not printable characters */
    static Map<String, C1421> f7366 = new TreeMap();

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1421 m7912(String str) {
        C1421 r0 = f7366.get(str);
        if (r0 != null) {
            return r0;
        }
        C1421 r02 = f7365.m7905(str);
        f7366.put(str, r02);
        return r02;
    }
}
