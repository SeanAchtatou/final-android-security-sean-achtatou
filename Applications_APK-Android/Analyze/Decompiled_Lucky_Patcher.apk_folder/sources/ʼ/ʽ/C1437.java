package ʼ.ʽ;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.LinkedHashMap;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipConstants;
import ʼ.ʻ.C1421;
import ʼ.ʻ.C1422;

/* renamed from: ʼ.ʽ.ʿ  reason: contains not printable characters */
/* compiled from: ZipInput */
public class C1437 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static C1421 f7439;

    /* renamed from: ʼ  reason: contains not printable characters */
    public String f7440;

    /* renamed from: ʽ  reason: contains not printable characters */
    RandomAccessFile f7441 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    long f7442;

    /* renamed from: ʿ  reason: contains not printable characters */
    int f7443 = 0;

    /* renamed from: ˆ  reason: contains not printable characters */
    Map<String, C1434> f7444 = new LinkedHashMap();

    /* renamed from: ˈ  reason: contains not printable characters */
    C1433 f7445;

    public C1437(String str) {
        this.f7440 = str;
        this.f7441 = new RandomAccessFile(new File(this.f7440), InternalZipConstants.READ_MODE);
        this.f7442 = this.f7441.length();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private static C1421 m7976() {
        if (f7439 == null) {
            f7439 = C1422.m7912(C1437.class.getName());
        }
        return f7439;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public long m7979() {
        return this.f7442;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1437 m7975(String str) {
        C1437 r0 = new C1437(str);
        r0.m7977();
        return r0;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Map<String, C1434> m7983() {
        return this.f7444;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public long m7980(int i) {
        long j = (long) i;
        long j2 = this.f7442;
        if (j > j2 || i > 65536) {
            throw new IllegalStateException("End of central directory not found in " + this.f7440);
        }
        int min = (int) Math.min(j2, j);
        byte[] bArr = new byte[min];
        long j3 = (long) min;
        this.f7441.seek(this.f7442 - j3);
        this.f7441.readFully(bArr);
        for (int i2 = min - 22; i2 >= 0; i2--) {
            this.f7443++;
            if (bArr[i2] == 80 && bArr[i2 + 1] == 75 && bArr[i2 + 2] == 5 && bArr[i2 + 3] == 6) {
                return (this.f7442 - j3) + ((long) i2);
            }
        }
        return m7980(i * 2);
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private void m7977() {
        try {
            this.f7441.seek(m7980(256));
            this.f7445 = C1433.m7944(this);
            boolean r0 = m7976().m7908();
            if (r0) {
                m7976().m7911(String.format("EOCD found in %d iterations", Integer.valueOf(this.f7443)));
                m7976().m7911(String.format("Directory entries=%d, size=%d, offset=%d/0x%08x", Short.valueOf(this.f7445.f7400), Integer.valueOf(this.f7445.f7401), Integer.valueOf(this.f7445.f7402), Integer.valueOf(this.f7445.f7402)));
                C1438.m7989(m7976());
            }
            this.f7441.seek((long) this.f7445.f7402);
            for (int i = 0; i < this.f7445.f7400; i++) {
                C1434 r2 = C1434.m7948(this);
                this.f7444.put(r2.m7962(), r2);
                if (r0) {
                    C1438.m7990(m7976(), r2);
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m7984() {
        RandomAccessFile randomAccessFile = this.f7441;
        if (randomAccessFile != null) {
            try {
                randomAccessFile.close();
            } catch (Throwable unused) {
            }
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public long m7986() {
        return this.f7441.getFilePointer();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7981(long j) {
        this.f7441.seek(j);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m7987() {
        int i = 0;
        for (int i2 = 0; i2 < 4; i2++) {
            i |= this.f7441.readUnsignedByte() << (i2 * 8);
        }
        return i;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public short m7988() {
        short s = 0;
        for (int i = 0; i < 2; i++) {
            s = (short) (s | (this.f7441.readUnsignedByte() << (i * 8)));
        }
        return s;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m7982(int i) {
        byte[] bArr = new byte[i];
        for (int i2 = 0; i2 < i; i2++) {
            bArr[i2] = this.f7441.readByte();
        }
        return new String(bArr);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public byte[] m7985(int i) {
        byte[] bArr = new byte[i];
        for (int i2 = 0; i2 < i; i2++) {
            bArr[i2] = this.f7441.readByte();
        }
        return bArr;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m7978(byte[] bArr, int i, int i2) {
        return this.f7441.read(bArr, i, i2);
    }
}
