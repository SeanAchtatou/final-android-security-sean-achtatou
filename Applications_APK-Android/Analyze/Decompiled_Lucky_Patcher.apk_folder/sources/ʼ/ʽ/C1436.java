package ʼ.ʽ;

import java.io.OutputStream;
import java.util.zip.CRC32;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

/* renamed from: ʼ.ʽ.ʾ  reason: contains not printable characters */
/* compiled from: ZioEntryOutputStream */
public class C1436 extends OutputStream {

    /* renamed from: ʻ  reason: contains not printable characters */
    int f7434 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    CRC32 f7435 = new CRC32();

    /* renamed from: ʽ  reason: contains not printable characters */
    int f7436 = 0;

    /* renamed from: ʾ  reason: contains not printable characters */
    OutputStream f7437;

    /* renamed from: ʿ  reason: contains not printable characters */
    OutputStream f7438;

    public C1436(int i, OutputStream outputStream) {
        this.f7437 = outputStream;
        if (i != 0) {
            this.f7438 = new DeflaterOutputStream(outputStream, new Deflater(9, true));
        } else {
            this.f7438 = outputStream;
        }
    }

    public void close() {
        this.f7438.flush();
        this.f7438.close();
        this.f7436 = (int) this.f7435.getValue();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m7972() {
        return this.f7436;
    }

    public void flush() {
        this.f7438.flush();
    }

    public void write(byte[] bArr) {
        this.f7438.write(bArr);
        this.f7435.update(bArr);
        this.f7434 += bArr.length;
    }

    public void write(byte[] bArr, int i, int i2) {
        this.f7438.write(bArr, i, i2);
        this.f7435.update(bArr, i, i2);
        this.f7434 += i2;
    }

    public void write(int i) {
        this.f7438.write(i);
        this.f7435.update(i);
        this.f7434++;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m7973() {
        return this.f7434;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public OutputStream m7974() {
        return this.f7437;
    }
}
