package ʼ.ʽ;

import java.io.OutputStream;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ʼ.ʻ.C1421;
import ʼ.ʻ.C1422;

/* renamed from: ʼ.ʽ.ˈ  reason: contains not printable characters */
/* compiled from: ZipOutput */
public class C1439 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static C1421 f7447;

    /* renamed from: ʼ  reason: contains not printable characters */
    OutputStream f7448 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    int f7449 = 0;

    /* renamed from: ʾ  reason: contains not printable characters */
    List<C1434> f7450 = new LinkedList();

    /* renamed from: ʿ  reason: contains not printable characters */
    Set<String> f7451 = new HashSet();

    public C1439(OutputStream outputStream) {
        this.f7448 = outputStream;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static C1421 m7991() {
        if (f7447 == null) {
            f7447 = C1422.m7912(C1439.class.getName());
        }
        return f7447;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7995(C1434 r4) {
        String r0 = r4.m7962();
        if (this.f7451.contains(r0)) {
            C1421 r42 = m7991();
            r42.m7909("Skipping duplicate file in output: " + r0);
            return;
        }
        r4.m7954(this);
        this.f7450.add(r4);
        this.f7451.add(r0);
        if (m7991().m7908()) {
            C1438.m7990(m7991(), r4);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7992() {
        C1433 r0 = new C1433();
        r0.f7402 = m7999();
        short size = (short) this.f7450.size();
        r0.f7400 = size;
        r0.f7399 = size;
        for (C1434 r2 : this.f7450) {
            r2.m7956(this);
        }
        r0.f7401 = m7999() - r0.f7402;
        r0.f7403 = BuildConfig.FLAVOR;
        r0.m7946(this);
        OutputStream outputStream = this.f7448;
        if (outputStream != null) {
            try {
                outputStream.close();
            } catch (Throwable unused) {
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m7999() {
        return this.f7449;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7993(int i) {
        byte[] bArr = new byte[4];
        for (int i2 = 0; i2 < 4; i2++) {
            bArr[i2] = (byte) (i & 255);
            i >>= 8;
        }
        this.f7448.write(bArr);
        this.f7449 += 4;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7996(short s) {
        byte[] bArr = new byte[2];
        for (int i = 0; i < 2; i++) {
            bArr[i] = (byte) (s & 255);
            s = (short) (s >> 8);
        }
        this.f7448.write(bArr);
        this.f7449 += 2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7994(String str) {
        byte[] bytes = str.getBytes();
        this.f7448.write(bytes);
        this.f7449 += bytes.length;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7997(byte[] bArr) {
        this.f7448.write(bArr);
        this.f7449 += bArr.length;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7998(byte[] bArr, int i, int i2) {
        this.f7448.write(bArr, i, i2);
        this.f7449 += i2;
    }
}
