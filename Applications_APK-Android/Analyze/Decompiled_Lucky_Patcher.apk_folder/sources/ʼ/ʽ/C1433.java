package ʼ.ʽ;

import ʼ.ʻ.C1421;
import ʼ.ʻ.C1422;

/* renamed from: ʼ.ʽ.ʻ  reason: contains not printable characters */
/* compiled from: CentralEnd */
public class C1433 {

    /* renamed from: ˊ  reason: contains not printable characters */
    private static C1421 f7395;

    /* renamed from: ʻ  reason: contains not printable characters */
    public int f7396 = 101010256;

    /* renamed from: ʼ  reason: contains not printable characters */
    public short f7397 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    public short f7398 = 0;

    /* renamed from: ʾ  reason: contains not printable characters */
    public short f7399;

    /* renamed from: ʿ  reason: contains not printable characters */
    public short f7400;

    /* renamed from: ˆ  reason: contains not printable characters */
    public int f7401;

    /* renamed from: ˈ  reason: contains not printable characters */
    public int f7402;

    /* renamed from: ˉ  reason: contains not printable characters */
    public String f7403;

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1433 m7944(C1437 r4) {
        if (r4.m7987() != 101010256) {
            r4.m7981(r4.m7986() - 4);
            return null;
        }
        C1433 r0 = new C1433();
        r0.m7945(r4);
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1421 m7943() {
        if (f7395 == null) {
            f7395 = C1422.m7912(C1433.class.getName());
        }
        return f7395;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m7945(C1437 r7) {
        boolean r0 = m7943().m7908();
        this.f7397 = r7.m7988();
        if (r0) {
            f7395.m7911(String.format("This disk number: 0x%04x", Short.valueOf(this.f7397)));
        }
        this.f7398 = r7.m7988();
        if (r0) {
            f7395.m7911(String.format("Central dir start disk number: 0x%04x", Short.valueOf(this.f7398)));
        }
        this.f7399 = r7.m7988();
        if (r0) {
            f7395.m7911(String.format("Central entries on this disk: 0x%04x", Short.valueOf(this.f7399)));
        }
        this.f7400 = r7.m7988();
        if (r0) {
            f7395.m7911(String.format("Total number of central entries: 0x%04x", Short.valueOf(this.f7400)));
        }
        this.f7401 = r7.m7987();
        if (r0) {
            f7395.m7911(String.format("Central directory size: 0x%08x", Integer.valueOf(this.f7401)));
        }
        this.f7402 = r7.m7987();
        if (r0) {
            f7395.m7911(String.format("Central directory offset: 0x%08x", Integer.valueOf(this.f7402)));
        }
        this.f7403 = r7.m7982(r7.m7988());
        if (r0) {
            C1421 r72 = f7395;
            r72.m7911(".ZIP file comment: " + this.f7403);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7946(C1439 r2) {
        m7943().m7908();
        r2.m7993(this.f7396);
        r2.m7996(this.f7397);
        r2.m7996(this.f7398);
        r2.m7996(this.f7399);
        r2.m7996(this.f7400);
        r2.m7993(this.f7401);
        r2.m7993(this.f7402);
        r2.m7996((short) this.f7403.length());
        r2.m7994(this.f7403);
    }
}
