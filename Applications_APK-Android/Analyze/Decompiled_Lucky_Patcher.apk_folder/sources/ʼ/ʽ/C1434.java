package ʼ.ʽ;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.SequenceInputStream;
import java.util.Date;
import java.util.zip.CRC32;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;
import net.lingala.zip4j.util.InternalZipConstants;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;
import ʼ.ʻ.C1421;
import ʼ.ʻ.C1422;

/* renamed from: ʼ.ʽ.ʼ  reason: contains not printable characters */
/* compiled from: ZioEntry */
public class C1434 implements Cloneable {

    /* renamed from: ⁱ  reason: contains not printable characters */
    private static byte[] f7404 = new byte[4];

    /* renamed from: ﹳ  reason: contains not printable characters */
    private static C1421 f7405;

    /* renamed from: ʻ  reason: contains not printable characters */
    private C1437 f7406;

    /* renamed from: ʼ  reason: contains not printable characters */
    private short f7407;

    /* renamed from: ʽ  reason: contains not printable characters */
    private short f7408;

    /* renamed from: ʾ  reason: contains not printable characters */
    private short f7409;

    /* renamed from: ʿ  reason: contains not printable characters */
    private short f7410;

    /* renamed from: ˆ  reason: contains not printable characters */
    private short f7411;

    /* renamed from: ˈ  reason: contains not printable characters */
    private short f7412;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f7413;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f7414;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f7415;

    /* renamed from: ˎ  reason: contains not printable characters */
    private String f7416;

    /* renamed from: ˏ  reason: contains not printable characters */
    private byte[] f7417;

    /* renamed from: ˑ  reason: contains not printable characters */
    private short f7418 = 0;

    /* renamed from: י  reason: contains not printable characters */
    private String f7419;

    /* renamed from: ـ  reason: contains not printable characters */
    private short f7420;

    /* renamed from: ٴ  reason: contains not printable characters */
    private short f7421;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f7422;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private int f7423;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private long f7424 = -1;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private byte[] f7425 = null;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private C1436 f7426 = null;

    public C1434(C1437 r3) {
        this.f7406 = r3;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1421 m7947() {
        if (f7405 == null) {
            f7405 = C1422.m7912(C1434.class.getName());
        }
        return f7405;
    }

    public C1434(String str) {
        this.f7416 = str;
        this.f7419 = BuildConfig.FLAVOR;
        this.f7410 = 8;
        this.f7417 = new byte[0];
        m7952(System.currentTimeMillis());
    }

    public C1434(String str, String str2) {
        this.f7406 = new C1437(str2);
        this.f7416 = str;
        this.f7419 = BuildConfig.FLAVOR;
        this.f7410 = 0;
        this.f7415 = (int) this.f7406.m7979();
        this.f7414 = this.f7415;
        if (m7947().m7908()) {
            m7947().m7911(String.format("Computing CRC for %s, size=%d", str2, Integer.valueOf(this.f7415)));
        }
        CRC32 crc32 = new CRC32();
        byte[] bArr = new byte[8096];
        int i = 0;
        while (true) {
            int i2 = this.f7415;
            if (i != i2) {
                int r3 = this.f7406.m7978(bArr, 0, Math.min(bArr.length, i2 - i));
                if (r3 > 0) {
                    crc32.update(bArr, 0, r3);
                    i += r3;
                }
            } else {
                this.f7413 = (int) crc32.getValue();
                this.f7406.m7981(0L);
                this.f7424 = 0;
                this.f7417 = new byte[0];
                m7952(new File(str2).lastModified());
                return;
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7955() {
        C1437 r0 = this.f7406;
        boolean r1 = m7947().m7908();
        r0.m7981((long) this.f7423);
        if (r1) {
            m7947().m7911(String.format("FILE POSITION: 0x%08x", Long.valueOf(r0.m7986())));
        }
        if (r0.m7987() == 67324752) {
            short r4 = r0.m7988();
            if (r1) {
                f7405.m7911(String.format("Version required: 0x%04x", Short.valueOf(r4)));
            }
            short r42 = r0.m7988();
            if (r1) {
                f7405.m7911(String.format("General purpose bits: 0x%04x", Short.valueOf(r42)));
            }
            short r43 = r0.m7988();
            if (r1) {
                f7405.m7911(String.format("Compression: 0x%04x", Short.valueOf(r43)));
            }
            short r44 = r0.m7988();
            if (r1) {
                f7405.m7911(String.format("Modification time: 0x%04x", Short.valueOf(r44)));
            }
            short r45 = r0.m7988();
            if (r1) {
                f7405.m7911(String.format("Modification date: 0x%04x", Short.valueOf(r45)));
            }
            int r46 = r0.m7987();
            if (r1) {
                f7405.m7911(String.format("CRC-32: 0x%04x", Integer.valueOf(r46)));
            }
            int r47 = r0.m7987();
            if (r1) {
                f7405.m7911(String.format("Compressed size: 0x%04x", Integer.valueOf(r47)));
            }
            int r48 = r0.m7987();
            if (r1) {
                f7405.m7911(String.format("Size: 0x%04x", Integer.valueOf(r48)));
            }
            short r49 = r0.m7988();
            if (r1) {
                f7405.m7911(String.format("File name length: 0x%04x", Short.valueOf(r49)));
            }
            short r5 = r0.m7988();
            if (r1) {
                f7405.m7911(String.format("Extra length: 0x%04x", Short.valueOf(r5)));
            }
            String r410 = r0.m7982(r49);
            if (r1) {
                C1421 r6 = f7405;
                r6.m7911("Filename: " + r410);
            }
            r0.m7985(r5);
            this.f7424 = r0.m7986();
            if (r1) {
                f7405.m7911(String.format("Data position: 0x%08x", Long.valueOf(this.f7424)));
                return;
            }
            return;
        }
        throw new IllegalStateException(String.format("Local header not found at pos=0x%08x, file=%s", Long.valueOf(r0.m7986()), this.f7416));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7954(C1439 r15) {
        short r6;
        long j = 0;
        if (this.f7425 == null && this.f7424 < 0 && this.f7406 != null) {
            m7955();
        }
        this.f7423 = r15.m7999();
        boolean r0 = m7947().m7908();
        if (r0) {
            m7947().m7911(String.format("Writing local header at 0x%08x - %s", Integer.valueOf(this.f7423), this.f7416));
        }
        C1436 r62 = this.f7426;
        if (r62 != null) {
            r62.close();
            this.f7415 = this.f7426.m7973();
            this.f7425 = ((ByteArrayOutputStream) this.f7426.m7974()).toByteArray();
            this.f7414 = this.f7425.length;
            this.f7413 = this.f7426.m7972();
        }
        r15.m7993(67324752);
        r15.m7996(this.f7408);
        r15.m7996(this.f7409);
        r15.m7996(this.f7410);
        r15.m7996(this.f7411);
        r15.m7996(this.f7412);
        r15.m7993(this.f7413);
        r15.m7993(this.f7414);
        r15.m7993(this.f7415);
        r15.m7996((short) this.f7416.length());
        this.f7418 = 0;
        if (this.f7410 == 0 && (r6 = (short) ((int) (((long) (((r15.m7999() + 2) + this.f7416.length()) + this.f7417.length)) % 4))) > 0) {
            this.f7418 = (short) (4 - r6);
        }
        r15.m7996((short) (this.f7417.length + this.f7418));
        r15.m7994(this.f7416);
        r15.m7997(this.f7417);
        short s = this.f7418;
        if (s > 0) {
            r15.m7998(f7404, 0, s);
        }
        if (r0) {
            m7947().m7911(String.format("Data position 0x%08x", Integer.valueOf(r15.m7999())));
        }
        byte[] bArr = this.f7425;
        if (bArr != null) {
            r15.m7997(bArr);
            if (r0) {
                m7947().m7911(String.format("Wrote %d bytes", Integer.valueOf(this.f7425.length)));
                return;
            }
            return;
        }
        if (r0) {
            m7947().m7911(String.format("Seeking to position 0x%08x", Long.valueOf(this.f7424)));
        }
        this.f7406.m7981(this.f7424);
        int min = Math.min(this.f7414, 8096);
        byte[] bArr2 = new byte[min];
        while (j != ((long) this.f7414)) {
            int read = this.f7406.f7441.read(bArr2, 0, (int) Math.min(((long) this.f7414) - j, (long) min));
            if (read > 0) {
                r15.m7998(bArr2, 0, read);
                if (r0) {
                    m7947().m7911(String.format("Wrote %d bytes", Integer.valueOf(read)));
                }
                j += (long) read;
            } else {
                throw new IllegalStateException(String.format("EOF reached while copying %s with %d bytes left to go", this.f7416, Long.valueOf(((long) this.f7414) - j)));
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1434 m7948(C1437 r4) {
        if (r4.m7987() != 33639248) {
            r4.m7981(r4.m7986() - 4);
            return null;
        }
        C1434 r0 = new C1434(r4);
        r0.m7949(r4);
        return r0;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m7949(C1437 r10) {
        boolean r0 = m7947().m7908();
        this.f7407 = r10.m7988();
        if (r0) {
            f7405.m7911(String.format("Version made by: 0x%04x", Short.valueOf(this.f7407)));
        }
        this.f7408 = r10.m7988();
        if (r0) {
            f7405.m7911(String.format("Version required: 0x%04x", Short.valueOf(this.f7408)));
        }
        this.f7409 = r10.m7988();
        if (r0) {
            f7405.m7911(String.format("General purpose bits: 0x%04x", Short.valueOf(this.f7409)));
        }
        if ((this.f7409 & 63473) == 0) {
            this.f7410 = r10.m7988();
            if (r0) {
                f7405.m7911(String.format("Compression: 0x%04x", Short.valueOf(this.f7410)));
            }
            this.f7411 = r10.m7988();
            if (r0) {
                f7405.m7911(String.format("Modification time: 0x%04x", Short.valueOf(this.f7411)));
            }
            this.f7412 = r10.m7988();
            if (r0) {
                f7405.m7911(String.format("Modification date: 0x%04x", Short.valueOf(this.f7412)));
            }
            this.f7413 = r10.m7987();
            if (r0) {
                f7405.m7911(String.format("CRC-32: 0x%04x", Integer.valueOf(this.f7413)));
            }
            this.f7414 = r10.m7987();
            if (r0) {
                f7405.m7911(String.format("Compressed size: 0x%04x", Integer.valueOf(this.f7414)));
            }
            this.f7415 = r10.m7987();
            if (r0) {
                f7405.m7911(String.format("Size: 0x%04x", Integer.valueOf(this.f7415)));
            }
            short r3 = r10.m7988();
            if (r0) {
                f7405.m7911(String.format("File name length: 0x%04x", Short.valueOf(r3)));
            }
            short r4 = r10.m7988();
            if (r0) {
                f7405.m7911(String.format("Extra length: 0x%04x", Short.valueOf(r4)));
            }
            short r5 = r10.m7988();
            if (r0) {
                f7405.m7911(String.format("File comment length: 0x%04x", Short.valueOf(r5)));
            }
            this.f7420 = r10.m7988();
            if (r0) {
                f7405.m7911(String.format("Disk number start: 0x%04x", Short.valueOf(this.f7420)));
            }
            this.f7421 = r10.m7988();
            if (r0) {
                f7405.m7911(String.format("Internal attributes: 0x%04x", Short.valueOf(this.f7421)));
            }
            this.f7422 = r10.m7987();
            if (r0) {
                f7405.m7911(String.format("External attributes: 0x%08x", Integer.valueOf(this.f7422)));
            }
            this.f7423 = r10.m7987();
            if (r0) {
                f7405.m7911(String.format("Local header offset: 0x%08x", Integer.valueOf(this.f7423)));
            }
            this.f7416 = r10.m7982(r3);
            if (r0) {
                C1421 r1 = f7405;
                r1.m7911("Filename: " + this.f7416);
            }
            this.f7417 = r10.m7985(r4);
            this.f7419 = r10.m7982(r5);
            if (r0) {
                C1421 r102 = f7405;
                r102.m7911("File comment: " + this.f7419);
            }
            this.f7409 = (short) (this.f7409 & 2048);
            if (this.f7415 == 0) {
                this.f7414 = 0;
                this.f7410 = 0;
                this.f7413 = 0;
                return;
            }
            return;
        }
        throw new IllegalStateException("Can't handle general purpose bits == " + String.format("0x%04x", Short.valueOf(this.f7409)));
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public byte[] m7957() {
        byte[] bArr = this.f7425;
        if (bArr != null) {
            return bArr;
        }
        byte[] bArr2 = new byte[this.f7415];
        InputStream r1 = m7958();
        int i = 0;
        while (true) {
            int i2 = this.f7415;
            if (i == i2) {
                return bArr2;
            }
            int read = r1.read(bArr2, i, i2 - i);
            if (read >= 0) {
                i += read;
            } else {
                throw new IllegalStateException(String.format("Read failed, expecting %d bytes, got %d instead", Integer.valueOf(this.f7415), Integer.valueOf(i)));
            }
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public InputStream m7958() {
        return m7950((OutputStream) null);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public InputStream m7950(OutputStream outputStream) {
        C1436 r0 = this.f7426;
        if (r0 != null) {
            r0.close();
            this.f7415 = this.f7426.m7973();
            this.f7425 = ((ByteArrayOutputStream) this.f7426.m7974()).toByteArray();
            this.f7414 = this.f7425.length;
            this.f7413 = this.f7426.m7972();
            this.f7426 = null;
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.f7425);
            if (this.f7410 == 0) {
                return byteArrayInputStream;
            }
            return new InflaterInputStream(new SequenceInputStream(byteArrayInputStream, new ByteArrayInputStream(new byte[1])), new Inflater(true));
        }
        C1435 r02 = new C1435(this);
        if (outputStream != null) {
            r02.m7970(outputStream);
        }
        if (this.f7410 == 0) {
            return r02;
        }
        r02.m7971(true);
        return new InflaterInputStream(r02, new Inflater(true));
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public OutputStream m7959() {
        this.f7426 = new C1436(this.f7410, new ByteArrayOutputStream());
        return this.f7426;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7956(C1439 r4) {
        m7947().m7908();
        r4.m7993(33639248);
        r4.m7996(this.f7407);
        r4.m7996(this.f7408);
        r4.m7996(this.f7409);
        r4.m7996(this.f7410);
        r4.m7996(this.f7411);
        r4.m7996(this.f7412);
        r4.m7993(this.f7413);
        r4.m7993(this.f7414);
        r4.m7993(this.f7415);
        r4.m7996((short) this.f7416.length());
        r4.m7996((short) (this.f7417.length + this.f7418));
        r4.m7996((short) this.f7419.length());
        r4.m7996(this.f7420);
        r4.m7996(this.f7421);
        r4.m7993(this.f7422);
        r4.m7993(this.f7423);
        r4.m7994(this.f7416);
        r4.m7997(this.f7417);
        short s = this.f7418;
        if (s > 0) {
            r4.m7998(f7404, 0, s);
        }
        r4.m7994(this.f7419);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public long m7960() {
        short s = this.f7412;
        short s2 = s & 31;
        short s3 = this.f7411;
        return new Date(((s >> 9) & 127) + 80, ((s >> 5) & 15) - 1, s2, (s3 >> 11) & 31, (s3 >> 5) & 63, (s3 << 1) & 62).getTime();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7952(long j) {
        long j2;
        Date date = new Date(j);
        int year = date.getYear() + 1900;
        if (year < 1980) {
            j2 = 2162688;
        } else {
            j2 = (long) (((year - 1980) << 25) | ((date.getMonth() + 1) << 21) | (date.getDate() << 16) | (date.getHours() << 11) | (date.getMinutes() << 5) | (date.getSeconds() >> 1));
        }
        this.f7412 = (short) ((int) (j2 >> 16));
        this.f7411 = (short) ((int) (65535 & j2));
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m7961() {
        return this.f7416.endsWith(InternalZipConstants.ZIP_FILE_SEPARATOR);
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public String m7962() {
        return this.f7416;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7953(String str) {
        this.f7416 = str;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7951(int i) {
        this.f7410 = (short) i;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public short m7963() {
        return this.f7410;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public int m7964() {
        return this.f7413;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public int m7965() {
        return this.f7414;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public int m7966() {
        return this.f7415;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public long m7967() {
        return this.f7424;
    }

    /* renamed from: י  reason: contains not printable characters */
    public C1437 m7968() {
        return this.f7406;
    }
}
