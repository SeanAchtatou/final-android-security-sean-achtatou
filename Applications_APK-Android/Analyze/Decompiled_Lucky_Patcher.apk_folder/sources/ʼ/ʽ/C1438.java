package ʼ.ʽ;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import ʼ.ʻ.C1421;

/* renamed from: ʼ.ʽ.ˆ  reason: contains not printable characters */
/* compiled from: ZipListingHelper */
public class C1438 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static DateFormat f7446 = new SimpleDateFormat("MM-dd-yy HH:mm");

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m7989(C1421 r1) {
        r1.m7911(" Length   Method    Size  Ratio   Date   Time   CRC-32    Name");
        r1.m7911("--------  ------  ------- -----   ----   ----   ------    ----");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m7990(C1421 r6, C1434 r7) {
        int r0 = r7.m7966() > 0 ? ((r7.m7966() - r7.m7965()) * 100) / r7.m7966() : 0;
        Object[] objArr = new Object[7];
        objArr[0] = Integer.valueOf(r7.m7966());
        objArr[1] = r7.m7963() == 0 ? "Stored" : "Defl:N";
        objArr[2] = Integer.valueOf(r7.m7965());
        objArr[3] = Integer.valueOf(r0);
        objArr[4] = f7446.format(new Date(r7.m7960()));
        objArr[5] = Integer.valueOf(r7.m7964());
        objArr[6] = r7.m7962();
        r6.m7911(String.format("%8d  %6s %8d %4d%% %s  %08x  %s", objArr));
    }
}
