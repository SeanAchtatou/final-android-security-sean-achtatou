package ʼ.ʽ;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import ʼ.ʻ.C1421;
import ʼ.ʻ.C1422;

/* renamed from: ʼ.ʽ.ʽ  reason: contains not printable characters */
/* compiled from: ZioEntryInputStream */
public class C1435 extends InputStream {

    /* renamed from: ʻ  reason: contains not printable characters */
    RandomAccessFile f7427;

    /* renamed from: ʼ  reason: contains not printable characters */
    int f7428;

    /* renamed from: ʽ  reason: contains not printable characters */
    int f7429 = 0;

    /* renamed from: ʾ  reason: contains not printable characters */
    C1421 f7430 = C1422.m7912(getClass().getName());

    /* renamed from: ʿ  reason: contains not printable characters */
    boolean f7431 = this.f7430.m7908();

    /* renamed from: ˆ  reason: contains not printable characters */
    boolean f7432 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    OutputStream f7433 = null;

    public void close() {
    }

    public boolean markSupported() {
        return false;
    }

    public C1435(C1434 r7) {
        this.f7428 = r7.m7965();
        this.f7427 = r7.m7968().f7441;
        if (r7.m7967() >= 0) {
            if (this.f7431) {
                this.f7430.m7911(String.format("Seeking to %d", Long.valueOf(r7.m7967())));
            }
            this.f7427.seek(r7.m7967());
            return;
        }
        r7.m7955();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7971(boolean z) {
        this.f7432 = z;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7970(OutputStream outputStream) {
        this.f7433 = outputStream;
    }

    public int available() {
        int i = this.f7428 - this.f7429;
        if (this.f7431) {
            this.f7430.m7911(String.format("Available = %d", Integer.valueOf(i)));
        }
        if (i != 0 || !this.f7432) {
            return i;
        }
        return 1;
    }

    public int read() {
        if (this.f7428 - this.f7429 != 0) {
            int read = this.f7427.read();
            if (read >= 0) {
                OutputStream outputStream = this.f7433;
                if (outputStream != null) {
                    outputStream.write(read);
                }
                if (this.f7431) {
                    this.f7430.m7911("Read 1 byte");
                }
                this.f7429++;
            } else if (this.f7431) {
                this.f7430.m7911("Read 0 bytes");
            }
            return read;
        } else if (!this.f7432) {
            return -1;
        } else {
            this.f7432 = false;
            return 0;
        }
    }

    public int read(byte[] bArr, int i, int i2) {
        return m7969(bArr, i, i2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m7969(byte[] bArr, int i, int i2) {
        if (this.f7428 - this.f7429 != 0) {
            int read = this.f7427.read(bArr, i, Math.min(i2, available()));
            if (read > 0) {
                OutputStream outputStream = this.f7433;
                if (outputStream != null) {
                    outputStream.write(bArr, i, read);
                }
                this.f7429 += read;
            }
            if (this.f7431) {
                this.f7430.m7911(String.format("Read %d bytes for read(b,%d,%d)", Integer.valueOf(read), Integer.valueOf(i), Integer.valueOf(i2)));
            }
            return read;
        } else if (!this.f7432) {
            return -1;
        } else {
            this.f7432 = false;
            bArr[i] = 0;
            return 1;
        }
    }

    public int read(byte[] bArr) {
        return m7969(bArr, 0, bArr.length);
    }

    public long skip(long j) {
        long min = Math.min(j, (long) available());
        RandomAccessFile randomAccessFile = this.f7427;
        randomAccessFile.seek(randomAccessFile.getFilePointer() + min);
        if (this.f7431) {
            this.f7430.m7911(String.format("Skipped %d bytes", Long.valueOf(min)));
        }
        return min;
    }
}
