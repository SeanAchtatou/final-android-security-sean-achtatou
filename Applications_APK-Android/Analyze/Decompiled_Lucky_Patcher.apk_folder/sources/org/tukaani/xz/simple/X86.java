package org.tukaani.xz.simple;

public final class X86 implements SimpleFilter {
    private static final boolean[] MASK_TO_ALLOWED_STATUS = {true, true, true, false, true, false, false, false};
    private static final int[] MASK_TO_BIT_NUMBER = {0, 1, 2, 2, 3, 3, 3, 3};
    private final boolean isEncoder;
    private int pos;
    private int prevMask = 0;

    private static boolean test86MSByte(byte b) {
        byte b2 = b & 255;
        return b2 == 0 || b2 == 255;
    }

    public X86(boolean z, int i) {
        this.isEncoder = z;
        this.pos = i + 5;
    }

    public int code(byte[] bArr, int i, int i2) {
        int i3;
        int i4;
        int i5 = (i2 + i) - 5;
        int i6 = i - 1;
        int i7 = i;
        while (true) {
            i3 = 0;
            if (i7 > i5) {
                break;
            }
            if ((bArr[i7] & 254) == 232) {
                int i8 = i7 - i6;
                if ((i8 & -4) != 0) {
                    this.prevMask = 0;
                } else {
                    this.prevMask = (this.prevMask << (i8 - 1)) & 7;
                    int i9 = this.prevMask;
                    if (i9 != 0 && (!MASK_TO_ALLOWED_STATUS[i9] || test86MSByte(bArr[(i7 + 4) - MASK_TO_BIT_NUMBER[i9]]))) {
                        this.prevMask = (this.prevMask << 1) | 1;
                        i6 = i7;
                    }
                }
                int i10 = i7 + 4;
                if (test86MSByte(bArr[i10])) {
                    int i11 = i7 + 1;
                    int i12 = i7 + 2;
                    int i13 = i7 + 3;
                    byte b = (bArr[i11] & 255) | ((bArr[i12] & 255) << 8) | ((bArr[i13] & 255) << 16) | ((bArr[i10] & 255) << 24);
                    while (true) {
                        if (this.isEncoder) {
                            i4 = b + ((this.pos + i7) - i);
                        } else {
                            i4 = b - ((this.pos + i7) - i);
                        }
                        int i14 = this.prevMask;
                        if (i14 == 0) {
                            break;
                        }
                        int i15 = MASK_TO_BIT_NUMBER[i14] * 8;
                        if (!test86MSByte((byte) (i4 >>> (24 - i15)))) {
                            break;
                        }
                        b = i4 ^ ((1 << (32 - i15)) - 1);
                    }
                    bArr[i11] = (byte) i4;
                    bArr[i12] = (byte) (i4 >>> 8);
                    bArr[i13] = (byte) (i4 >>> 16);
                    bArr[i10] = (byte) ((((i4 >>> 24) & 1) - 1) ^ -1);
                    int i16 = i10;
                    i6 = i7;
                    i7 = i16;
                } else {
                    this.prevMask = (this.prevMask << 1) | 1;
                    i6 = i7;
                }
            }
            i7++;
        }
        int i17 = i7 - i6;
        if ((i17 & -4) == 0) {
            i3 = this.prevMask << (i17 - 1);
        }
        this.prevMask = i3;
        int i18 = i7 - i;
        this.pos += i18;
        return i18;
    }
}
