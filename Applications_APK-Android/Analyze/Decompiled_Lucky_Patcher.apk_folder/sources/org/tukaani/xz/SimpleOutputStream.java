package org.tukaani.xz;

import java.io.IOException;
import org.tukaani.xz.simple.SimpleFilter;

class SimpleOutputStream extends FinishableOutputStream {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static final int FILTER_BUF_SIZE = 4096;
    private IOException exception = null;
    private final byte[] filterBuf = new byte[4096];
    private boolean finished = false;
    private FinishableOutputStream out;
    private int pos = 0;
    private final SimpleFilter simpleFilter;
    private final byte[] tempBuf = new byte[1];
    private int unfiltered = 0;

    static int getMemoryUsage() {
        return 5;
    }

    SimpleOutputStream(FinishableOutputStream finishableOutputStream, SimpleFilter simpleFilter2) {
        if (finishableOutputStream != null) {
            this.out = finishableOutputStream;
            this.simpleFilter = simpleFilter2;
            return;
        }
        throw new NullPointerException();
    }

    public void write(int i) {
        byte[] bArr = this.tempBuf;
        bArr[0] = (byte) i;
        write(bArr, 0, 1);
    }

    public void write(byte[] bArr, int i, int i2) {
        int i3;
        if (i < 0 || i2 < 0 || (i3 = i + i2) < 0 || i3 > bArr.length) {
            throw new IndexOutOfBoundsException();
        }
        IOException iOException = this.exception;
        if (iOException != null) {
            throw iOException;
        } else if (!this.finished) {
            while (i2 > 0) {
                int min = Math.min(i2, 4096 - (this.pos + this.unfiltered));
                System.arraycopy(bArr, i, this.filterBuf, this.pos + this.unfiltered, min);
                i += min;
                i2 -= min;
                this.unfiltered += min;
                int code = this.simpleFilter.code(this.filterBuf, this.pos, this.unfiltered);
                this.unfiltered -= code;
                try {
                    this.out.write(this.filterBuf, this.pos, code);
                    this.pos += code;
                    int i4 = this.pos;
                    int i5 = this.unfiltered;
                    if (i4 + i5 == 4096) {
                        byte[] bArr2 = this.filterBuf;
                        System.arraycopy(bArr2, i4, bArr2, 0, i5);
                        this.pos = 0;
                    }
                } catch (IOException e) {
                    this.exception = e;
                    throw e;
                }
            }
        } else {
            throw new XZIOException("Stream finished or closed");
        }
    }

    private void writePending() {
        IOException iOException = this.exception;
        if (iOException == null) {
            try {
                this.out.write(this.filterBuf, this.pos, this.unfiltered);
                this.finished = true;
            } catch (IOException e) {
                this.exception = e;
                throw e;
            }
        } else {
            throw iOException;
        }
    }

    public void flush() {
        throw new UnsupportedOptionsException("Flushing is not supported");
    }

    public void finish() {
        if (!this.finished) {
            writePending();
            try {
                this.out.finish();
            } catch (IOException e) {
                this.exception = e;
                throw e;
            }
        }
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x000b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void close() {
        /*
            r2 = this;
            org.tukaani.xz.FinishableOutputStream r0 = r2.out
            if (r0 == 0) goto L_0x001b
            boolean r0 = r2.finished
            if (r0 != 0) goto L_0x000b
            r2.writePending()     // Catch:{ IOException -> 0x000b }
        L_0x000b:
            org.tukaani.xz.FinishableOutputStream r0 = r2.out     // Catch:{ IOException -> 0x0011 }
            r0.close()     // Catch:{ IOException -> 0x0011 }
            goto L_0x0018
        L_0x0011:
            r0 = move-exception
            java.io.IOException r1 = r2.exception
            if (r1 != 0) goto L_0x0018
            r2.exception = r0
        L_0x0018:
            r0 = 0
            r2.out = r0
        L_0x001b:
            java.io.IOException r0 = r2.exception
            if (r0 != 0) goto L_0x0020
            return
        L_0x0020:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.tukaani.xz.SimpleOutputStream.close():void");
    }
}
