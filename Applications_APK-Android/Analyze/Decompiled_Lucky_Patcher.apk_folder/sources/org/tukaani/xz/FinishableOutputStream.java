package org.tukaani.xz;

import java.io.OutputStream;

public abstract class FinishableOutputStream extends OutputStream {
    public void finish() {
    }
}
