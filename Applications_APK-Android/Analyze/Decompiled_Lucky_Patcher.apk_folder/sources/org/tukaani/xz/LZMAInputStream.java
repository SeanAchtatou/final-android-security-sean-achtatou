package org.tukaani.xz;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.tukaani.xz.common.Util;
import org.tukaani.xz.lz.LZDecoder;
import org.tukaani.xz.lzma.LZMADecoder;
import org.tukaani.xz.rangecoder.RangeDecoderFromStream;

public class LZMAInputStream extends InputStream {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    public static final int DICT_SIZE_MAX = 2147483632;
    private boolean endReached;
    private IOException exception;
    private InputStream in;
    private LZDecoder lz;
    private LZMADecoder lzma;
    private RangeDecoderFromStream rc;
    private long remainingSize;
    private final byte[] tempBuf;

    public static int getMemoryUsage(int i, byte b) {
        if (i < 0 || i > 2147483632) {
            throw new UnsupportedOptionsException("LZMA dictionary is too big for this implementation");
        }
        byte b2 = b & 255;
        if (b2 <= 224) {
            int i2 = b2 % 45;
            int i3 = i2 / 9;
            return getMemoryUsage(i, i2 - (i3 * 9), i3);
        }
        throw new CorruptedInputException("Invalid LZMA properties byte");
    }

    public static int getMemoryUsage(int i, int i2, int i3) {
        if (i2 >= 0 && i2 <= 8 && i3 >= 0 && i3 <= 4) {
            return (getDictSize(i) / Util.BLOCK_HEADER_SIZE_MAX) + 10 + ((1536 << (i2 + i3)) / Util.BLOCK_HEADER_SIZE_MAX);
        }
        throw new IllegalArgumentException("Invalid lc or lp");
    }

    private static int getDictSize(int i) {
        if (i < 0 || i > 2147483632) {
            throw new IllegalArgumentException("LZMA dictionary is too big for this implementation");
        }
        if (i < 4096) {
            i = 4096;
        }
        return (i + 15) & -16;
    }

    public LZMAInputStream(InputStream inputStream) {
        this(inputStream, -1);
    }

    public LZMAInputStream(InputStream inputStream, int i) {
        this.endReached = false;
        this.tempBuf = new byte[1];
        this.exception = null;
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        byte readByte = dataInputStream.readByte();
        int i2 = 0;
        for (int i3 = 0; i3 < 4; i3++) {
            i2 |= dataInputStream.readUnsignedByte() << (i3 * 8);
        }
        long j = 0;
        for (int i4 = 0; i4 < 8; i4++) {
            j |= ((long) dataInputStream.readUnsignedByte()) << (i4 * 8);
        }
        int memoryUsage = getMemoryUsage(i2, readByte);
        if (i == -1 || memoryUsage <= i) {
            initialize(inputStream, j, readByte, i2, null);
            return;
        }
        throw new MemoryLimitException(memoryUsage, i);
    }

    public LZMAInputStream(InputStream inputStream, long j, byte b, int i) {
        this.endReached = false;
        this.tempBuf = new byte[1];
        this.exception = null;
        initialize(inputStream, j, b, i, null);
    }

    public LZMAInputStream(InputStream inputStream, long j, byte b, int i, byte[] bArr) {
        this.endReached = false;
        this.tempBuf = new byte[1];
        this.exception = null;
        initialize(inputStream, j, b, i, bArr);
    }

    public LZMAInputStream(InputStream inputStream, long j, int i, int i2, int i3, int i4, byte[] bArr) {
        this.endReached = false;
        this.tempBuf = new byte[1];
        this.exception = null;
        initialize(inputStream, j, i, i2, i3, i4, bArr);
    }

    private void initialize(InputStream inputStream, long j, byte b, int i, byte[] bArr) {
        if (j >= -1) {
            byte b2 = b & 255;
            if (b2 <= 224) {
                int i2 = b2 / 45;
                int i3 = b2 - ((i2 * 9) * 5);
                int i4 = i3 / 9;
                int i5 = i3 - (i4 * 9);
                if (i < 0 || i > 2147483632) {
                    throw new UnsupportedOptionsException("LZMA dictionary is too big for this implementation");
                }
                initialize(inputStream, j, i5, i4, i2, i, bArr);
                return;
            }
            throw new CorruptedInputException("Invalid LZMA properties byte");
        }
        throw new UnsupportedOptionsException("Uncompressed size is too big");
    }

    private void initialize(InputStream inputStream, long j, int i, int i2, int i3, int i4, byte[] bArr) {
        if (j < -1 || i < 0 || i > 8 || i2 < 0 || i2 > 4 || i3 < 0 || i3 > 4) {
            throw new IllegalArgumentException();
        }
        this.in = inputStream;
        int dictSize = getDictSize(i4);
        if (j >= 0 && ((long) dictSize) > j) {
            dictSize = getDictSize((int) j);
        }
        this.lz = new LZDecoder(getDictSize(dictSize), bArr);
        this.rc = new RangeDecoderFromStream(inputStream);
        this.lzma = new LZMADecoder(this.lz, this.rc, i, i2, i3);
        this.remainingSize = j;
    }

    public int read() {
        if (read(this.tempBuf, 0, 1) == -1) {
            return -1;
        }
        return this.tempBuf[0] & 255;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int read(byte[] r12, int r13, int r14) {
        /*
            r11 = this;
            if (r13 < 0) goto L_0x00a1
            if (r14 < 0) goto L_0x00a1
            int r0 = r13 + r14
            if (r0 < 0) goto L_0x00a1
            int r1 = r12.length
            if (r0 > r1) goto L_0x00a1
            r0 = 0
            if (r14 != 0) goto L_0x000f
            return r0
        L_0x000f:
            java.io.InputStream r1 = r11.in
            if (r1 == 0) goto L_0x0099
            java.io.IOException r1 = r11.exception
            if (r1 != 0) goto L_0x0098
            boolean r1 = r11.endReached
            r2 = -1
            if (r1 == 0) goto L_0x001d
            return r2
        L_0x001d:
            if (r14 <= 0) goto L_0x0097
            long r3 = r11.remainingSize     // Catch:{ IOException -> 0x0093 }
            r5 = 0
            int r1 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r1 < 0) goto L_0x0032
            long r3 = r11.remainingSize     // Catch:{ IOException -> 0x0093 }
            long r7 = (long) r14     // Catch:{ IOException -> 0x0093 }
            int r1 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r1 >= 0) goto L_0x0032
            long r3 = r11.remainingSize     // Catch:{ IOException -> 0x0093 }
            int r1 = (int) r3     // Catch:{ IOException -> 0x0093 }
            goto L_0x0033
        L_0x0032:
            r1 = r14
        L_0x0033:
            org.tukaani.xz.lz.LZDecoder r3 = r11.lz     // Catch:{ IOException -> 0x0093 }
            r3.setLimit(r1)     // Catch:{ IOException -> 0x0093 }
            r1 = 1
            org.tukaani.xz.lzma.LZMADecoder r3 = r11.lzma     // Catch:{ CorruptedInputException -> 0x003f }
            r3.decode()     // Catch:{ CorruptedInputException -> 0x003f }
            goto L_0x0057
        L_0x003f:
            r3 = move-exception
            long r7 = r11.remainingSize     // Catch:{ IOException -> 0x0093 }
            r9 = -1
            int r4 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r4 != 0) goto L_0x0092
            org.tukaani.xz.lzma.LZMADecoder r4 = r11.lzma     // Catch:{ IOException -> 0x0093 }
            boolean r4 = r4.endMarkerDetected()     // Catch:{ IOException -> 0x0093 }
            if (r4 == 0) goto L_0x0092
            r11.endReached = r1     // Catch:{ IOException -> 0x0093 }
            org.tukaani.xz.rangecoder.RangeDecoderFromStream r3 = r11.rc     // Catch:{ IOException -> 0x0093 }
            r3.normalize()     // Catch:{ IOException -> 0x0093 }
        L_0x0057:
            org.tukaani.xz.lz.LZDecoder r3 = r11.lz     // Catch:{ IOException -> 0x0093 }
            int r3 = r3.flush(r12, r13)     // Catch:{ IOException -> 0x0093 }
            int r13 = r13 + r3
            int r14 = r14 - r3
            int r0 = r0 + r3
            long r7 = r11.remainingSize     // Catch:{ IOException -> 0x0093 }
            int r4 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r4 < 0) goto L_0x0074
            long r7 = r11.remainingSize     // Catch:{ IOException -> 0x0093 }
            long r3 = (long) r3     // Catch:{ IOException -> 0x0093 }
            long r7 = r7 - r3
            r11.remainingSize = r7     // Catch:{ IOException -> 0x0093 }
            long r3 = r11.remainingSize     // Catch:{ IOException -> 0x0093 }
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 != 0) goto L_0x0074
            r11.endReached = r1     // Catch:{ IOException -> 0x0093 }
        L_0x0074:
            boolean r1 = r11.endReached     // Catch:{ IOException -> 0x0093 }
            if (r1 == 0) goto L_0x001d
            org.tukaani.xz.rangecoder.RangeDecoderFromStream r12 = r11.rc     // Catch:{ IOException -> 0x0093 }
            boolean r12 = r12.isFinished()     // Catch:{ IOException -> 0x0093 }
            if (r12 == 0) goto L_0x008c
            org.tukaani.xz.lz.LZDecoder r12 = r11.lz     // Catch:{ IOException -> 0x0093 }
            boolean r12 = r12.hasPending()     // Catch:{ IOException -> 0x0093 }
            if (r12 != 0) goto L_0x008c
            if (r0 != 0) goto L_0x008b
            r0 = -1
        L_0x008b:
            return r0
        L_0x008c:
            org.tukaani.xz.CorruptedInputException r12 = new org.tukaani.xz.CorruptedInputException     // Catch:{ IOException -> 0x0093 }
            r12.<init>()     // Catch:{ IOException -> 0x0093 }
            throw r12     // Catch:{ IOException -> 0x0093 }
        L_0x0092:
            throw r3     // Catch:{ IOException -> 0x0093 }
        L_0x0093:
            r12 = move-exception
            r11.exception = r12
            throw r12
        L_0x0097:
            return r0
        L_0x0098:
            throw r1
        L_0x0099:
            org.tukaani.xz.XZIOException r12 = new org.tukaani.xz.XZIOException
            java.lang.String r13 = "Stream closed"
            r12.<init>(r13)
            throw r12
        L_0x00a1:
            java.lang.IndexOutOfBoundsException r12 = new java.lang.IndexOutOfBoundsException
            r12.<init>()
            goto L_0x00a8
        L_0x00a7:
            throw r12
        L_0x00a8:
            goto L_0x00a7
        */
        throw new UnsupportedOperationException("Method not decompiled: org.tukaani.xz.LZMAInputStream.read(byte[], int, int):int");
    }

    public void close() {
        InputStream inputStream = this.in;
        if (inputStream != null) {
            try {
                inputStream.close();
            } finally {
                this.in = null;
            }
        }
    }
}
