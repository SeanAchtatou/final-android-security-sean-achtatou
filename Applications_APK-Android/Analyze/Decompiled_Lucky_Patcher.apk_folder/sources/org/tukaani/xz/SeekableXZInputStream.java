package org.tukaani.xz;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import org.tukaani.xz.check.Check;
import org.tukaani.xz.common.DecoderUtil;
import org.tukaani.xz.common.StreamFlags;
import org.tukaani.xz.index.BlockInfo;
import org.tukaani.xz.index.IndexDecoder;

public class SeekableXZInputStream extends SeekableInputStream {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private int blockCount;
    private BlockInputStream blockDecoder;
    private Check check;
    private int checkTypes;
    private final BlockInfo curBlockInfo;
    private long curPos;
    private boolean endReached;
    private IOException exception;
    private SeekableInputStream in;
    private int indexMemoryUsage;
    private long largestBlockSize;
    private final int memoryLimit;
    private final BlockInfo queriedBlockInfo;
    private boolean seekNeeded;
    private long seekPos;
    private final ArrayList streams;
    private final byte[] tempBuf;
    private long uncompressedSize;

    public SeekableXZInputStream(SeekableInputStream seekableInputStream) {
        this(seekableInputStream, -1);
    }

    public SeekableXZInputStream(SeekableInputStream seekableInputStream, int i) {
        SeekableInputStream seekableInputStream2 = seekableInputStream;
        this.indexMemoryUsage = 0;
        this.streams = new ArrayList();
        this.checkTypes = 0;
        long j = 0;
        this.uncompressedSize = 0;
        this.largestBlockSize = 0;
        this.blockCount = 0;
        this.blockDecoder = null;
        this.curPos = 0;
        this.seekNeeded = false;
        this.endReached = false;
        this.exception = null;
        int i2 = 1;
        this.tempBuf = new byte[1];
        this.in = seekableInputStream2;
        DataInputStream dataInputStream = new DataInputStream(seekableInputStream2);
        seekableInputStream2.seek(0);
        byte[] bArr = new byte[XZ.HEADER_MAGIC.length];
        dataInputStream.readFully(bArr);
        if (Arrays.equals(bArr, XZ.HEADER_MAGIC)) {
            long length = seekableInputStream.length();
            if ((3 & length) == 0) {
                byte[] bArr2 = new byte[12];
                int i3 = i;
                while (true) {
                    long j2 = j;
                    while (length > j) {
                        if (length >= 12) {
                            long j3 = length - 12;
                            seekableInputStream2.seek(j3);
                            dataInputStream.readFully(bArr2);
                            if (bArr2[8] == 0 && bArr2[9] == 0 && bArr2[10] == 0 && bArr2[11] == 0) {
                                j2 += 4;
                                length -= 4;
                                j = 0;
                            } else {
                                StreamFlags decodeStreamFooter = DecoderUtil.decodeStreamFooter(bArr2);
                                if (decodeStreamFooter.backwardSize < j3) {
                                    this.check = Check.getInstance(decodeStreamFooter.checkType);
                                    this.checkTypes |= i2 << decodeStreamFooter.checkType;
                                    seekableInputStream2.seek(j3 - decodeStreamFooter.backwardSize);
                                    try {
                                        IndexDecoder indexDecoder = r2;
                                        StreamFlags streamFlags = decodeStreamFooter;
                                        IndexDecoder indexDecoder2 = new IndexDecoder(seekableInputStream, decodeStreamFooter, j2, i3);
                                        this.indexMemoryUsage += indexDecoder.getMemoryUsage();
                                        i3 = i3 >= 0 ? i3 - indexDecoder.getMemoryUsage() : i3;
                                        if (this.largestBlockSize < indexDecoder.getLargestBlockSize()) {
                                            this.largestBlockSize = indexDecoder.getLargestBlockSize();
                                        }
                                        long streamSize = indexDecoder.getStreamSize() - 12;
                                        if (j3 >= streamSize) {
                                            length = j3 - streamSize;
                                            seekableInputStream2.seek(length);
                                            dataInputStream.readFully(bArr2);
                                            if (DecoderUtil.areStreamFlagsEqual(DecoderUtil.decodeStreamHeader(bArr2), streamFlags)) {
                                                this.uncompressedSize += indexDecoder.getUncompressedSize();
                                                if (this.uncompressedSize >= 0) {
                                                    this.blockCount += indexDecoder.getRecordCount();
                                                    if (this.blockCount >= 0) {
                                                        this.streams.add(indexDecoder);
                                                        j = 0;
                                                        i2 = 1;
                                                    } else {
                                                        throw new UnsupportedOptionsException("XZ file has over 2147483647 Blocks");
                                                    }
                                                } else {
                                                    throw new UnsupportedOptionsException("XZ file is too big");
                                                }
                                            } else {
                                                throw new CorruptedInputException("XZ Stream Footer does not match Stream Header");
                                            }
                                        } else {
                                            throw new CorruptedInputException("XZ Index indicates too big compressed size for the XZ Stream");
                                        }
                                    } catch (MemoryLimitException e) {
                                        int memoryNeeded = e.getMemoryNeeded();
                                        int i4 = this.indexMemoryUsage;
                                        throw new MemoryLimitException(memoryNeeded + i4, i3 + i4);
                                    }
                                } else {
                                    throw new CorruptedInputException("Backward Size in XZ Stream Footer is too big");
                                }
                            }
                        } else {
                            throw new CorruptedInputException();
                        }
                    }
                    this.memoryLimit = i3;
                    ArrayList arrayList = this.streams;
                    IndexDecoder indexDecoder3 = (IndexDecoder) arrayList.get(arrayList.size() - 1);
                    int size = this.streams.size() - 2;
                    while (size >= 0) {
                        IndexDecoder indexDecoder4 = (IndexDecoder) this.streams.get(size);
                        indexDecoder4.setOffsets(indexDecoder3);
                        size--;
                        indexDecoder3 = indexDecoder4;
                    }
                    ArrayList arrayList2 = this.streams;
                    IndexDecoder indexDecoder5 = (IndexDecoder) arrayList2.get(arrayList2.size() - 1);
                    this.curBlockInfo = new BlockInfo(indexDecoder5);
                    this.queriedBlockInfo = new BlockInfo(indexDecoder5);
                    return;
                }
            }
            throw new CorruptedInputException("XZ file size is not a multiple of 4 bytes");
        }
        throw new XZFormatException();
    }

    public int getCheckTypes() {
        return this.checkTypes;
    }

    public int getIndexMemoryUsage() {
        return this.indexMemoryUsage;
    }

    public long getLargestBlockSize() {
        return this.largestBlockSize;
    }

    public int getStreamCount() {
        return this.streams.size();
    }

    public int getBlockCount() {
        return this.blockCount;
    }

    public long getBlockPos(int i) {
        locateBlockByNumber(this.queriedBlockInfo, i);
        return this.queriedBlockInfo.uncompressedOffset;
    }

    public long getBlockSize(int i) {
        locateBlockByNumber(this.queriedBlockInfo, i);
        return this.queriedBlockInfo.uncompressedSize;
    }

    public long getBlockCompPos(int i) {
        locateBlockByNumber(this.queriedBlockInfo, i);
        return this.queriedBlockInfo.compressedOffset;
    }

    public long getBlockCompSize(int i) {
        locateBlockByNumber(this.queriedBlockInfo, i);
        return (this.queriedBlockInfo.unpaddedSize + 3) & -4;
    }

    public int getBlockCheckType(int i) {
        locateBlockByNumber(this.queriedBlockInfo, i);
        return this.queriedBlockInfo.getCheckType();
    }

    public int getBlockNumber(long j) {
        locateBlockByPos(this.queriedBlockInfo, j);
        return this.queriedBlockInfo.blockNumber;
    }

    public int read() {
        if (read(this.tempBuf, 0, 1) == -1) {
            return -1;
        }
        return this.tempBuf[0] & 255;
    }

    public int read(byte[] bArr, int i, int i2) {
        int i3;
        if (i < 0 || i2 < 0 || (i3 = i + i2) < 0 || i3 > bArr.length) {
            throw new IndexOutOfBoundsException();
        }
        int i4 = 0;
        if (i2 == 0) {
            return 0;
        }
        if (this.in != null) {
            IOException iOException = this.exception;
            if (iOException == null) {
                try {
                    if (this.seekNeeded) {
                        seek();
                    }
                    if (this.endReached) {
                        return -1;
                    }
                    while (true) {
                        if (i2 <= 0) {
                            break;
                        }
                        if (this.blockDecoder == null) {
                            seek();
                            if (this.endReached) {
                                break;
                            }
                        }
                        int read = this.blockDecoder.read(bArr, i, i2);
                        if (read > 0) {
                            this.curPos += (long) read;
                            i4 += read;
                            i += read;
                            i2 -= read;
                        } else if (read == -1) {
                            this.blockDecoder = null;
                        }
                    }
                    return i4;
                } catch (IOException e) {
                    e = e;
                    if (e instanceof EOFException) {
                        e = new CorruptedInputException();
                    }
                    this.exception = e;
                    if (0 == 0) {
                        throw e;
                    }
                }
            } else {
                throw iOException;
            }
        } else {
            throw new XZIOException("Stream closed");
        }
    }

    public int available() {
        BlockInputStream blockInputStream;
        if (this.in != null) {
            IOException iOException = this.exception;
            if (iOException != null) {
                throw iOException;
            } else if (this.endReached || this.seekNeeded || (blockInputStream = this.blockDecoder) == null) {
                return 0;
            } else {
                return blockInputStream.available();
            }
        } else {
            throw new XZIOException("Stream closed");
        }
    }

    public void close() {
        SeekableInputStream seekableInputStream = this.in;
        if (seekableInputStream != null) {
            try {
                seekableInputStream.close();
            } finally {
                this.in = null;
            }
        }
    }

    public long length() {
        return this.uncompressedSize;
    }

    public long position() {
        if (this.in != null) {
            return this.seekNeeded ? this.seekPos : this.curPos;
        }
        throw new XZIOException("Stream closed");
    }

    public void seek(long j) {
        if (this.in == null) {
            throw new XZIOException("Stream closed");
        } else if (j >= 0) {
            this.seekPos = j;
            this.seekNeeded = true;
        } else {
            throw new XZIOException("Negative seek position: " + j);
        }
    }

    public void seekToBlock(int i) {
        if (this.in == null) {
            throw new XZIOException("Stream closed");
        } else if (i < 0 || i >= this.blockCount) {
            throw new XZIOException("Invalid XZ Block number: " + i);
        } else {
            this.seekPos = getBlockPos(i);
            this.seekNeeded = true;
        }
    }

    private void seek() {
        if (!this.seekNeeded) {
            if (this.curBlockInfo.hasNext()) {
                this.curBlockInfo.setNext();
                initBlockDecoder();
                return;
            }
            this.seekPos = this.curPos;
        }
        this.seekNeeded = false;
        long j = this.seekPos;
        if (j >= this.uncompressedSize) {
            this.curPos = j;
            this.blockDecoder = null;
            this.endReached = true;
            return;
        }
        this.endReached = false;
        locateBlockByPos(this.curBlockInfo, j);
        if (this.curPos <= this.curBlockInfo.uncompressedOffset || this.curPos > this.seekPos) {
            this.in.seek(this.curBlockInfo.compressedOffset);
            this.check = Check.getInstance(this.curBlockInfo.getCheckType());
            initBlockDecoder();
            this.curPos = this.curBlockInfo.uncompressedOffset;
        }
        long j2 = this.seekPos;
        long j3 = this.curPos;
        if (j2 > j3) {
            long j4 = j2 - j3;
            if (this.blockDecoder.skip(j4) == j4) {
                this.curPos = this.seekPos;
                return;
            }
            throw new CorruptedInputException();
        }
    }

    private void locateBlockByPos(BlockInfo blockInfo, long j) {
        if (j < 0 || j >= this.uncompressedSize) {
            throw new IndexOutOfBoundsException("Invalid uncompressed position: " + j);
        }
        int i = 0;
        while (true) {
            IndexDecoder indexDecoder = (IndexDecoder) this.streams.get(i);
            if (indexDecoder.hasUncompressedOffset(j)) {
                indexDecoder.locateBlock(blockInfo, j);
                return;
            }
            i++;
        }
    }

    private void locateBlockByNumber(BlockInfo blockInfo, int i) {
        if (i < 0 || i >= this.blockCount) {
            throw new IndexOutOfBoundsException("Invalid XZ Block number: " + i);
        } else if (blockInfo.blockNumber != i) {
            int i2 = 0;
            while (true) {
                IndexDecoder indexDecoder = (IndexDecoder) this.streams.get(i2);
                if (indexDecoder.hasRecord(i)) {
                    indexDecoder.setBlockInfo(blockInfo, i);
                    return;
                }
                i2++;
            }
        }
    }

    private void initBlockDecoder() {
        try {
            this.blockDecoder = null;
            this.blockDecoder = new BlockInputStream(this.in, this.check, this.memoryLimit, this.curBlockInfo.unpaddedSize, this.curBlockInfo.uncompressedSize);
        } catch (MemoryLimitException e) {
            int memoryNeeded = e.getMemoryNeeded();
            int i = this.indexMemoryUsage;
            throw new MemoryLimitException(memoryNeeded + i, this.memoryLimit + i);
        } catch (IndexIndicatorException unused) {
            throw new CorruptedInputException();
        }
    }
}
