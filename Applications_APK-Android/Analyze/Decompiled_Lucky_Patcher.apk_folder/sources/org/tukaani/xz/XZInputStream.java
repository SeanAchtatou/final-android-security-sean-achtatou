package org.tukaani.xz;

import java.io.IOException;
import java.io.InputStream;

public class XZInputStream extends InputStream {
    private boolean endReached;
    private IOException exception;
    private InputStream in;
    private final int memoryLimit;
    private final byte[] tempBuf;
    private SingleXZInputStream xzIn;

    public XZInputStream(InputStream inputStream) {
        this(inputStream, -1);
    }

    public XZInputStream(InputStream inputStream, int i) {
        this.endReached = false;
        this.exception = null;
        this.tempBuf = new byte[1];
        this.in = inputStream;
        this.memoryLimit = i;
        this.xzIn = new SingleXZInputStream(inputStream, i);
    }

    public int read() {
        if (read(this.tempBuf, 0, 1) == -1) {
            return -1;
        }
        return this.tempBuf[0] & 255;
    }

    public int read(byte[] bArr, int i, int i2) {
        int i3;
        if (i < 0 || i2 < 0 || (i3 = i + i2) < 0 || i3 > bArr.length) {
            throw new IndexOutOfBoundsException();
        }
        int i4 = 0;
        if (i2 == 0) {
            return 0;
        }
        if (this.in != null) {
            IOException iOException = this.exception;
            if (iOException != null) {
                throw iOException;
            } else if (this.endReached) {
                return -1;
            } else {
                while (i2 > 0) {
                    try {
                        if (this.xzIn == null) {
                            prepareNextStream();
                            if (this.endReached) {
                                if (i4 == 0) {
                                    return -1;
                                }
                                return i4;
                            }
                        }
                        int read = this.xzIn.read(bArr, i, i2);
                        if (read > 0) {
                            i4 += read;
                            i += read;
                            i2 -= read;
                        } else if (read == -1) {
                            this.xzIn = null;
                        }
                    } catch (IOException e) {
                        this.exception = e;
                        if (i4 == 0) {
                            throw e;
                        }
                    }
                }
                return i4;
            }
        } else {
            throw new XZIOException("Stream closed");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x0017  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void prepareNextStream() {
        /*
            r6 = this;
            java.io.DataInputStream r0 = new java.io.DataInputStream
            java.io.InputStream r1 = r6.in
            r0.<init>(r1)
            r1 = 12
            byte[] r1 = new byte[r1]
        L_0x000b:
            r2 = 0
            r3 = 1
            int r4 = r0.read(r1, r2, r3)
            r5 = -1
            if (r4 != r5) goto L_0x0017
            r6.endReached = r3
            return
        L_0x0017:
            r4 = 3
            r0.readFully(r1, r3, r4)
            byte r2 = r1[r2]
            if (r2 != 0) goto L_0x002c
            byte r2 = r1[r3]
            if (r2 != 0) goto L_0x002c
            r2 = 2
            byte r2 = r1[r2]
            if (r2 != 0) goto L_0x002c
            byte r2 = r1[r4]
            if (r2 == 0) goto L_0x000b
        L_0x002c:
            r2 = 4
            r3 = 8
            r0.readFully(r1, r2, r3)
            org.tukaani.xz.SingleXZInputStream r0 = new org.tukaani.xz.SingleXZInputStream     // Catch:{ XZFormatException -> 0x003e }
            java.io.InputStream r2 = r6.in     // Catch:{ XZFormatException -> 0x003e }
            int r3 = r6.memoryLimit     // Catch:{ XZFormatException -> 0x003e }
            r0.<init>(r2, r3, r1)     // Catch:{ XZFormatException -> 0x003e }
            r6.xzIn = r0     // Catch:{ XZFormatException -> 0x003e }
            return
        L_0x003e:
            org.tukaani.xz.CorruptedInputException r0 = new org.tukaani.xz.CorruptedInputException
            java.lang.String r1 = "Garbage after a valid XZ Stream"
            r0.<init>(r1)
            goto L_0x0047
        L_0x0046:
            throw r0
        L_0x0047:
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: org.tukaani.xz.XZInputStream.prepareNextStream():void");
    }

    public int available() {
        if (this.in != null) {
            IOException iOException = this.exception;
            if (iOException == null) {
                SingleXZInputStream singleXZInputStream = this.xzIn;
                if (singleXZInputStream == null) {
                    return 0;
                }
                return singleXZInputStream.available();
            }
            throw iOException;
        }
        throw new XZIOException("Stream closed");
    }

    public void close() {
        InputStream inputStream = this.in;
        if (inputStream != null) {
            try {
                inputStream.close();
            } finally {
                this.in = null;
            }
        }
    }
}
