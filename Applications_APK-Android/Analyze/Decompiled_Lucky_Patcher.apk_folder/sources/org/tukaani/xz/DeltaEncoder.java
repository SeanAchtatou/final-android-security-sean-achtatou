package org.tukaani.xz;

class DeltaEncoder extends DeltaCoder implements FilterEncoder {
    private final DeltaOptions options;
    private final byte[] props = new byte[1];

    public long getFilterID() {
        return 3;
    }

    public boolean supportsFlushing() {
        return true;
    }

    DeltaEncoder(DeltaOptions deltaOptions) {
        this.props[0] = (byte) (deltaOptions.getDistance() - 1);
        this.options = (DeltaOptions) deltaOptions.clone();
    }

    public byte[] getFilterProps() {
        return this.props;
    }

    public FinishableOutputStream getOutputStream(FinishableOutputStream finishableOutputStream) {
        return this.options.getOutputStream(finishableOutputStream);
    }
}
