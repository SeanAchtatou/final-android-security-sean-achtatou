package org.tukaani.xz;

import java.io.InputStream;

public abstract class SeekableInputStream extends InputStream {
    public abstract long length();

    public abstract long position();

    public abstract void seek(long j);

    public long skip(long j) {
        if (j <= 0) {
            return 0;
        }
        long length = length();
        long position = position();
        if (position >= length) {
            return 0;
        }
        long j2 = length - position;
        if (j2 < j) {
            j = j2;
        }
        seek(position + j);
        return j;
    }
}
