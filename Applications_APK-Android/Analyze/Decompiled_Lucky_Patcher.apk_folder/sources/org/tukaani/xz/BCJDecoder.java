package org.tukaani.xz;

import java.io.InputStream;
import org.tukaani.xz.simple.ARM;
import org.tukaani.xz.simple.ARMThumb;
import org.tukaani.xz.simple.IA64;
import org.tukaani.xz.simple.PowerPC;
import org.tukaani.xz.simple.SPARC;
import org.tukaani.xz.simple.SimpleFilter;
import org.tukaani.xz.simple.X86;

class BCJDecoder extends BCJCoder implements FilterDecoder {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private final long filterID;
    private final int startOffset;

    BCJDecoder(long j, byte[] bArr) {
        this.filterID = j;
        if (bArr.length == 0) {
            this.startOffset = 0;
        } else if (bArr.length == 4) {
            int i = 0;
            for (int i2 = 0; i2 < 4; i2++) {
                i |= (bArr[i2] & 255) << (i2 * 8);
            }
            this.startOffset = i;
        } else {
            throw new UnsupportedOptionsException("Unsupported BCJ filter properties");
        }
    }

    public int getMemoryUsage() {
        return SimpleInputStream.getMemoryUsage();
    }

    public InputStream getInputStream(InputStream inputStream) {
        SimpleFilter simpleFilter;
        long j = this.filterID;
        if (j == 4) {
            simpleFilter = new X86(false, this.startOffset);
        } else if (j == 5) {
            simpleFilter = new PowerPC(false, this.startOffset);
        } else if (j == 6) {
            simpleFilter = new IA64(false, this.startOffset);
        } else if (j == 7) {
            simpleFilter = new ARM(false, this.startOffset);
        } else if (j == 8) {
            simpleFilter = new ARMThumb(false, this.startOffset);
        } else {
            simpleFilter = j == 9 ? new SPARC(false, this.startOffset) : null;
        }
        return new SimpleInputStream(inputStream, simpleFilter);
    }
}
