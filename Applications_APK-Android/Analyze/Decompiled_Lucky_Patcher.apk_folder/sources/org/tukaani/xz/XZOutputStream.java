package org.tukaani.xz;

import java.io.IOException;
import java.io.OutputStream;
import org.tukaani.xz.check.Check;
import org.tukaani.xz.common.EncoderUtil;
import org.tukaani.xz.common.StreamFlags;
import org.tukaani.xz.index.IndexEncoder;

public class XZOutputStream extends FinishableOutputStream {
    private BlockOutputStream blockEncoder;
    private final Check check;
    private IOException exception;
    private FilterEncoder[] filters;
    private boolean filtersSupportFlushing;
    private boolean finished;
    private final IndexEncoder index;
    private OutputStream out;
    private final StreamFlags streamFlags;
    private final byte[] tempBuf;

    public XZOutputStream(OutputStream outputStream, FilterOptions filterOptions) {
        this(outputStream, filterOptions, 4);
    }

    public XZOutputStream(OutputStream outputStream, FilterOptions filterOptions, int i) {
        this(outputStream, new FilterOptions[]{filterOptions}, i);
    }

    public XZOutputStream(OutputStream outputStream, FilterOptions[] filterOptionsArr) {
        this(outputStream, filterOptionsArr, 4);
    }

    public XZOutputStream(OutputStream outputStream, FilterOptions[] filterOptionsArr, int i) {
        this.streamFlags = new StreamFlags();
        this.index = new IndexEncoder();
        this.blockEncoder = null;
        this.exception = null;
        this.finished = false;
        this.tempBuf = new byte[1];
        this.out = outputStream;
        updateFilters(filterOptionsArr);
        this.streamFlags.checkType = i;
        this.check = Check.getInstance(i);
        encodeStreamHeader();
    }

    public void updateFilters(FilterOptions filterOptions) {
        updateFilters(new FilterOptions[]{filterOptions});
    }

    public void updateFilters(FilterOptions[] filterOptionsArr) {
        if (this.blockEncoder != null) {
            throw new UnsupportedOptionsException("Changing filter options in the middle of a XZ Block not implemented");
        } else if (filterOptionsArr.length < 1 || filterOptionsArr.length > 4) {
            throw new UnsupportedOptionsException("XZ filter chain must be 1-4 filters");
        } else {
            this.filtersSupportFlushing = true;
            FilterEncoder[] filterEncoderArr = new FilterEncoder[filterOptionsArr.length];
            for (int i = 0; i < filterOptionsArr.length; i++) {
                filterEncoderArr[i] = filterOptionsArr[i].getFilterEncoder();
                this.filtersSupportFlushing &= filterEncoderArr[i].supportsFlushing();
            }
            RawCoder.validate(filterEncoderArr);
            this.filters = filterEncoderArr;
        }
    }

    public void write(int i) {
        byte[] bArr = this.tempBuf;
        bArr[0] = (byte) i;
        write(bArr, 0, 1);
    }

    public void write(byte[] bArr, int i, int i2) {
        int i3;
        if (i < 0 || i2 < 0 || (i3 = i + i2) < 0 || i3 > bArr.length) {
            throw new IndexOutOfBoundsException();
        }
        IOException iOException = this.exception;
        if (iOException != null) {
            throw iOException;
        } else if (!this.finished) {
            try {
                if (this.blockEncoder == null) {
                    this.blockEncoder = new BlockOutputStream(this.out, this.filters, this.check);
                }
                this.blockEncoder.write(bArr, i, i2);
            } catch (IOException e) {
                this.exception = e;
                throw e;
            }
        } else {
            throw new XZIOException("Stream finished or closed");
        }
    }

    public void endBlock() {
        IOException iOException = this.exception;
        if (iOException != null) {
            throw iOException;
        } else if (!this.finished) {
            BlockOutputStream blockOutputStream = this.blockEncoder;
            if (blockOutputStream != null) {
                try {
                    blockOutputStream.finish();
                    this.index.add(this.blockEncoder.getUnpaddedSize(), this.blockEncoder.getUncompressedSize());
                    this.blockEncoder = null;
                } catch (IOException e) {
                    this.exception = e;
                    throw e;
                }
            }
        } else {
            throw new XZIOException("Stream finished or closed");
        }
    }

    public void flush() {
        IOException iOException = this.exception;
        if (iOException != null) {
            throw iOException;
        } else if (!this.finished) {
            try {
                if (this.blockEncoder == null) {
                    this.out.flush();
                } else if (this.filtersSupportFlushing) {
                    this.blockEncoder.flush();
                } else {
                    endBlock();
                    this.out.flush();
                }
            } catch (IOException e) {
                this.exception = e;
                throw e;
            }
        } else {
            throw new XZIOException("Stream finished or closed");
        }
    }

    public void finish() {
        if (!this.finished) {
            endBlock();
            try {
                this.index.encode(this.out);
                encodeStreamFooter();
                this.finished = true;
            } catch (IOException e) {
                this.exception = e;
                throw e;
            }
        }
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:4:0x0007 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void close() {
        /*
            r2 = this;
            java.io.OutputStream r0 = r2.out
            if (r0 == 0) goto L_0x0017
            r2.finish()     // Catch:{ IOException -> 0x0007 }
        L_0x0007:
            java.io.OutputStream r0 = r2.out     // Catch:{ IOException -> 0x000d }
            r0.close()     // Catch:{ IOException -> 0x000d }
            goto L_0x0014
        L_0x000d:
            r0 = move-exception
            java.io.IOException r1 = r2.exception
            if (r1 != 0) goto L_0x0014
            r2.exception = r0
        L_0x0014:
            r0 = 0
            r2.out = r0
        L_0x0017:
            java.io.IOException r0 = r2.exception
            if (r0 != 0) goto L_0x001c
            return
        L_0x001c:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.tukaani.xz.XZOutputStream.close():void");
    }

    private void encodeStreamFlags(byte[] bArr, int i) {
        bArr[i] = 0;
        bArr[i + 1] = (byte) this.streamFlags.checkType;
    }

    private void encodeStreamHeader() {
        this.out.write(XZ.HEADER_MAGIC);
        byte[] bArr = new byte[2];
        encodeStreamFlags(bArr, 0);
        this.out.write(bArr);
        EncoderUtil.writeCRC32(this.out, bArr);
    }

    private void encodeStreamFooter() {
        byte[] bArr = new byte[6];
        long indexSize = (this.index.getIndexSize() / 4) - 1;
        for (int i = 0; i < 4; i++) {
            bArr[i] = (byte) ((int) (indexSize >>> (i * 8)));
        }
        encodeStreamFlags(bArr, 4);
        EncoderUtil.writeCRC32(this.out, bArr);
        this.out.write(bArr);
        this.out.write(XZ.FOOTER_MAGIC);
    }
}
