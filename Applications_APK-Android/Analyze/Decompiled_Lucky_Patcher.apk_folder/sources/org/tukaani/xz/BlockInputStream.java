package org.tukaani.xz;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import org.tukaani.xz.check.Check;
import org.tukaani.xz.common.DecoderUtil;
import org.tukaani.xz.common.Util;

class BlockInputStream extends InputStream {
    private final Check check;
    private long compressedSizeInHeader = -1;
    private long compressedSizeLimit;
    private boolean endReached = false;
    private InputStream filterChain;
    private final int headerSize;
    private final CountingInputStream inCounted;
    private final DataInputStream inData;
    private final byte[] tempBuf = new byte[1];
    private long uncompressedSize = 0;
    private long uncompressedSizeInHeader = -1;

    public BlockInputStream(InputStream inputStream, Check check2, int i, long j, long j2) {
        InputStream inputStream2 = inputStream;
        int i2 = i;
        long j3 = j2;
        this.check = check2;
        this.inData = new DataInputStream(inputStream2);
        byte[] bArr = new byte[Util.BLOCK_HEADER_SIZE_MAX];
        this.inData.readFully(bArr, 0, 1);
        if (bArr[0] != 0) {
            this.headerSize = ((bArr[0] & 255) + 1) * 4;
            this.inData.readFully(bArr, 1, this.headerSize - 1);
            int i3 = this.headerSize;
            if (!DecoderUtil.isCRC32Valid(bArr, 0, i3 - 4, i3 - 4)) {
                throw new CorruptedInputException("XZ Block Header is corrupt");
            } else if ((bArr[1] & 60) == 0) {
                int i4 = (bArr[1] & 3) + 1;
                long[] jArr = new long[i4];
                byte[][] bArr2 = new byte[i4][];
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr, 2, this.headerSize - 6);
                try {
                    this.compressedSizeLimit = (9223372036854775804L - ((long) this.headerSize)) - ((long) check2.getSize());
                    if ((bArr[1] & 64) != 0) {
                        this.compressedSizeInHeader = DecoderUtil.decodeVLI(byteArrayInputStream);
                        if (this.compressedSizeInHeader == 0 || this.compressedSizeInHeader > this.compressedSizeLimit) {
                            throw new CorruptedInputException();
                        }
                        this.compressedSizeLimit = this.compressedSizeInHeader;
                    }
                    if ((bArr[1] & 128) != 0) {
                        this.uncompressedSizeInHeader = DecoderUtil.decodeVLI(byteArrayInputStream);
                    }
                    int i5 = 0;
                    while (i5 < i4) {
                        jArr[i5] = DecoderUtil.decodeVLI(byteArrayInputStream);
                        long decodeVLI = DecoderUtil.decodeVLI(byteArrayInputStream);
                        int i6 = i4;
                        if (decodeVLI <= ((long) byteArrayInputStream.available())) {
                            bArr2[i5] = new byte[((int) decodeVLI)];
                            byteArrayInputStream.read(bArr2[i5]);
                            i5++;
                            i4 = i6;
                        } else {
                            throw new CorruptedInputException();
                        }
                    }
                    int available = byteArrayInputStream.available();
                    while (available > 0) {
                        if (byteArrayInputStream.read() == 0) {
                            available--;
                        } else {
                            throw new UnsupportedOptionsException("Unsupported options in XZ Block Header");
                        }
                    }
                    if (j != -1) {
                        long size = (long) (this.headerSize + check2.getSize());
                        if (size < j) {
                            long j4 = j - size;
                            if (j4 <= this.compressedSizeLimit) {
                                long j5 = this.compressedSizeInHeader;
                                if (j5 == -1 || j5 == j4) {
                                    long j6 = this.uncompressedSizeInHeader;
                                    if (j6 == -1 || j6 == j3) {
                                        this.compressedSizeLimit = j4;
                                        this.compressedSizeInHeader = j4;
                                        this.uncompressedSizeInHeader = j3;
                                    } else {
                                        throw new CorruptedInputException("XZ Index does not match a Block Header");
                                    }
                                }
                            }
                            throw new CorruptedInputException("XZ Index does not match a Block Header");
                        }
                        throw new CorruptedInputException("XZ Index does not match a Block Header");
                    }
                    FilterDecoder[] filterDecoderArr = new FilterDecoder[jArr.length];
                    for (int i7 = 0; i7 < filterDecoderArr.length; i7++) {
                        if (jArr[i7] == 33) {
                            filterDecoderArr[i7] = new LZMA2Decoder(bArr2[i7]);
                        } else if (jArr[i7] == 3) {
                            filterDecoderArr[i7] = new DeltaDecoder(bArr2[i7]);
                        } else if (BCJDecoder.isBCJFilterID(jArr[i7])) {
                            filterDecoderArr[i7] = new BCJDecoder(jArr[i7], bArr2[i7]);
                        } else {
                            throw new UnsupportedOptionsException("Unknown Filter ID " + jArr[i7]);
                        }
                    }
                    RawCoder.validate(filterDecoderArr);
                    if (i2 >= 0) {
                        int i8 = 0;
                        for (FilterDecoder memoryUsage : filterDecoderArr) {
                            i8 += memoryUsage.getMemoryUsage();
                        }
                        if (i8 > i2) {
                            throw new MemoryLimitException(i8, i2);
                        }
                    }
                    this.inCounted = new CountingInputStream(inputStream2);
                    this.filterChain = this.inCounted;
                    for (int length = filterDecoderArr.length - 1; length >= 0; length--) {
                        this.filterChain = filterDecoderArr[length].getInputStream(this.filterChain);
                    }
                } catch (IOException unused) {
                    throw new CorruptedInputException("XZ Block Header is corrupt");
                }
            } else {
                throw new UnsupportedOptionsException("Unsupported options in XZ Block Header");
            }
        } else {
            throw new IndexIndicatorException();
        }
    }

    public int read() {
        if (read(this.tempBuf, 0, 1) == -1) {
            return -1;
        }
        return this.tempBuf[0] & 255;
    }

    public int read(byte[] bArr, int i, int i2) {
        if (this.endReached) {
            return -1;
        }
        int read = this.filterChain.read(bArr, i, i2);
        if (read > 0) {
            this.check.update(bArr, i, read);
            this.uncompressedSize += (long) read;
            long size = this.inCounted.getSize();
            if (size >= 0 && size <= this.compressedSizeLimit) {
                long j = this.uncompressedSize;
                if (j >= 0) {
                    long j2 = this.uncompressedSizeInHeader;
                    if (j2 == -1 || j <= j2) {
                        if (read < i2 || this.uncompressedSize == this.uncompressedSizeInHeader) {
                            if (this.filterChain.read() == -1) {
                                validate();
                                this.endReached = true;
                            } else {
                                throw new CorruptedInputException();
                            }
                        }
                    }
                }
            }
            throw new CorruptedInputException();
        } else if (read == -1) {
            validate();
            this.endReached = true;
        }
        return read;
    }

    private void validate() {
        long size = this.inCounted.getSize();
        long j = this.compressedSizeInHeader;
        if (j == -1 || j == size) {
            long j2 = this.uncompressedSizeInHeader;
            if (j2 == -1 || j2 == this.uncompressedSize) {
                while (true) {
                    long j3 = 1 + size;
                    if ((size & 3) == 0) {
                        byte[] bArr = new byte[this.check.getSize()];
                        this.inData.readFully(bArr);
                        if (!Arrays.equals(this.check.finish(), bArr)) {
                            throw new CorruptedInputException("Integrity check (" + this.check.getName() + ") does not match");
                        }
                        return;
                    } else if (this.inData.readUnsignedByte() == 0) {
                        size = j3;
                    } else {
                        throw new CorruptedInputException();
                    }
                }
            }
        }
        throw new CorruptedInputException();
    }

    public int available() {
        return this.filterChain.available();
    }

    public long getUnpaddedSize() {
        return ((long) this.headerSize) + this.inCounted.getSize() + ((long) this.check.getSize());
    }

    public long getUncompressedSize() {
        return this.uncompressedSize;
    }
}
