package org.tukaani.xz;

abstract class LZMA2Coder implements FilterCoder {
    public static final long FILTER_ID = 33;

    public boolean changesSize() {
        return true;
    }

    public boolean lastOK() {
        return true;
    }

    public boolean nonLastOK() {
        return false;
    }

    LZMA2Coder() {
    }
}
