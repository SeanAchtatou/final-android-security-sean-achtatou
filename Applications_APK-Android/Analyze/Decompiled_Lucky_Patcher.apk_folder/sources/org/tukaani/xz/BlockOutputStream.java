package org.tukaani.xz;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import org.tukaani.xz.check.Check;
import org.tukaani.xz.common.EncoderUtil;

class BlockOutputStream extends FinishableOutputStream {
    private final Check check;
    private final long compressedSizeLimit;
    private FinishableOutputStream filterChain;
    private final int headerSize;
    private final OutputStream out;
    private final CountingOutputStream outCounted;
    private final byte[] tempBuf = new byte[1];
    private long uncompressedSize = 0;

    public BlockOutputStream(OutputStream outputStream, FilterEncoder[] filterEncoderArr, Check check2) {
        this.out = outputStream;
        this.check = check2;
        this.outCounted = new CountingOutputStream(outputStream);
        this.filterChain = this.outCounted;
        for (int length = filterEncoderArr.length - 1; length >= 0; length--) {
            this.filterChain = filterEncoderArr[length].getOutputStream(this.filterChain);
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(0);
        byteArrayOutputStream.write(filterEncoderArr.length - 1);
        for (int i = 0; i < filterEncoderArr.length; i++) {
            EncoderUtil.encodeVLI(byteArrayOutputStream, filterEncoderArr[i].getFilterID());
            byte[] filterProps = filterEncoderArr[i].getFilterProps();
            EncoderUtil.encodeVLI(byteArrayOutputStream, (long) filterProps.length);
            byteArrayOutputStream.write(filterProps);
        }
        while ((byteArrayOutputStream.size() & 3) != 0) {
            byteArrayOutputStream.write(0);
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        this.headerSize = byteArray.length + 4;
        if (this.headerSize <= 1024) {
            byteArray[0] = (byte) (byteArray.length / 4);
            outputStream.write(byteArray);
            EncoderUtil.writeCRC32(outputStream, byteArray);
            this.compressedSizeLimit = (9223372036854775804L - ((long) this.headerSize)) - ((long) check2.getSize());
            return;
        }
        throw new UnsupportedOptionsException();
    }

    public void write(int i) {
        byte[] bArr = this.tempBuf;
        bArr[0] = (byte) i;
        write(bArr, 0, 1);
    }

    public void write(byte[] bArr, int i, int i2) {
        this.filterChain.write(bArr, i, i2);
        this.check.update(bArr, i, i2);
        this.uncompressedSize += (long) i2;
        validate();
    }

    public void flush() {
        this.filterChain.flush();
        validate();
    }

    public void finish() {
        this.filterChain.finish();
        validate();
        for (long size = this.outCounted.getSize(); (3 & size) != 0; size++) {
            this.out.write(0);
        }
        this.out.write(this.check.finish());
    }

    private void validate() {
        long size = this.outCounted.getSize();
        if (size < 0 || size > this.compressedSizeLimit || this.uncompressedSize < 0) {
            throw new XZIOException("XZ Stream has grown too big");
        }
    }

    public long getUnpaddedSize() {
        return ((long) this.headerSize) + this.outCounted.getSize() + ((long) this.check.getSize());
    }

    public long getUncompressedSize() {
        return this.uncompressedSize;
    }
}
