package org.tukaani.xz;

import java.io.InputStream;

public abstract class FilterOptions implements Cloneable {
    public abstract int getDecoderMemoryUsage();

    public abstract int getEncoderMemoryUsage();

    /* access modifiers changed from: package-private */
    public abstract FilterEncoder getFilterEncoder();

    public abstract InputStream getInputStream(InputStream inputStream);

    public abstract FinishableOutputStream getOutputStream(FinishableOutputStream finishableOutputStream);

    public static int getEncoderMemoryUsage(FilterOptions[] filterOptionsArr) {
        int i = 0;
        for (FilterOptions encoderMemoryUsage : filterOptionsArr) {
            i += encoderMemoryUsage.getEncoderMemoryUsage();
        }
        return i;
    }

    public static int getDecoderMemoryUsage(FilterOptions[] filterOptionsArr) {
        int i = 0;
        for (FilterOptions decoderMemoryUsage : filterOptionsArr) {
            i += decoderMemoryUsage.getDecoderMemoryUsage();
        }
        return i;
    }

    FilterOptions() {
    }
}
