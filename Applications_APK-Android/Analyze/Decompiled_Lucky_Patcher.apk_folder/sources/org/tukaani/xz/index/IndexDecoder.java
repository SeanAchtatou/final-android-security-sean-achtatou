package org.tukaani.xz.index;

import org.tukaani.xz.common.StreamFlags;

public class IndexDecoder extends IndexBase {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private long compressedOffset = 0;
    private long largestBlockSize = 0;
    private final int memoryUsage;
    private int recordOffset = 0;
    private final StreamFlags streamFlags;
    private final long streamPadding;
    private final long[] uncompressed;
    private long uncompressedOffset = 0;
    private final long[] unpadded;

    public /* bridge */ /* synthetic */ long getIndexSize() {
        return super.getIndexSize();
    }

    public /* bridge */ /* synthetic */ long getStreamSize() {
        return super.getStreamSize();
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public IndexDecoder(org.tukaani.xz.SeekableInputStream r18, org.tukaani.xz.common.StreamFlags r19, long r20, int r22) {
        /*
            r17 = this;
            r0 = r17
            r1 = r19
            r2 = r22
            org.tukaani.xz.CorruptedInputException r3 = new org.tukaani.xz.CorruptedInputException
            java.lang.String r4 = "XZ Index is corrupt"
            r3.<init>(r4)
            r0.<init>(r3)
            r5 = 0
            r0.largestBlockSize = r5
            r3 = 0
            r0.recordOffset = r3
            r0.compressedOffset = r5
            r0.uncompressedOffset = r5
            r0.streamFlags = r1
            r5 = r20
            r0.streamPadding = r5
            long r5 = r18.position()
            long r7 = r1.backwardSize
            long r5 = r5 + r7
            r7 = 4
            long r5 = r5 - r7
            java.util.zip.CRC32 r7 = new java.util.zip.CRC32
            r7.<init>()
            java.util.zip.CheckedInputStream r8 = new java.util.zip.CheckedInputStream
            r9 = r18
            r8.<init>(r9, r7)
            int r10 = r8.read()
            if (r10 != 0) goto L_0x011f
            long r10 = org.tukaani.xz.common.DecoderUtil.decodeVLI(r8)     // Catch:{ EOFException -> 0x0118 }
            long r12 = r1.backwardSize     // Catch:{ EOFException -> 0x0118 }
            r14 = 2
            long r12 = r12 / r14
            int r1 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r1 >= 0) goto L_0x0111
            r12 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r1 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r1 > 0) goto L_0x0108
            r12 = 16
            long r12 = r12 * r10
            r14 = 1023(0x3ff, double:5.054E-321)
            long r12 = r12 + r14
            r14 = 1024(0x400, double:5.06E-321)
            long r12 = r12 / r14
            int r1 = (int) r12     // Catch:{ EOFException -> 0x0118 }
            int r1 = r1 + 1
            r0.memoryUsage = r1     // Catch:{ EOFException -> 0x0118 }
            if (r2 < 0) goto L_0x006f
            int r1 = r0.memoryUsage     // Catch:{ EOFException -> 0x0118 }
            if (r1 > r2) goto L_0x0067
            goto L_0x006f
        L_0x0067:
            org.tukaani.xz.MemoryLimitException r1 = new org.tukaani.xz.MemoryLimitException     // Catch:{ EOFException -> 0x0118 }
            int r3 = r0.memoryUsage     // Catch:{ EOFException -> 0x0118 }
            r1.<init>(r3, r2)     // Catch:{ EOFException -> 0x0118 }
            throw r1     // Catch:{ EOFException -> 0x0118 }
        L_0x006f:
            int r1 = (int) r10     // Catch:{ EOFException -> 0x0118 }
            long[] r2 = new long[r1]     // Catch:{ EOFException -> 0x0118 }
            r0.unpadded = r2     // Catch:{ EOFException -> 0x0118 }
            long[] r2 = new long[r1]     // Catch:{ EOFException -> 0x0118 }
            r0.uncompressed = r2     // Catch:{ EOFException -> 0x0118 }
            r2 = 0
        L_0x0079:
            if (r1 <= 0) goto L_0x00bf
            long r10 = org.tukaani.xz.common.DecoderUtil.decodeVLI(r8)     // Catch:{ EOFException -> 0x0118 }
            long r12 = org.tukaani.xz.common.DecoderUtil.decodeVLI(r8)     // Catch:{ EOFException -> 0x0118 }
            long r14 = r18.position()     // Catch:{ EOFException -> 0x0118 }
            int r16 = (r14 > r5 ? 1 : (r14 == r5 ? 0 : -1))
            if (r16 > 0) goto L_0x00b2
            long[] r14 = r0.unpadded     // Catch:{ EOFException -> 0x0118 }
            r15 = r4
            long r3 = r0.blocksSum     // Catch:{ EOFException -> 0x00af }
            long r3 = r3 + r10
            r14[r2] = r3     // Catch:{ EOFException -> 0x00af }
            long[] r3 = r0.uncompressed     // Catch:{ EOFException -> 0x00af }
            r19 = r15
            long r14 = r0.uncompressedSum     // Catch:{ EOFException -> 0x00bc }
            long r14 = r14 + r12
            r3[r2] = r14     // Catch:{ EOFException -> 0x00bc }
            int r2 = r2 + 1
            super.add(r10, r12)     // Catch:{ EOFException -> 0x00bc }
            long r3 = r0.largestBlockSize     // Catch:{ EOFException -> 0x00bc }
            int r10 = (r3 > r12 ? 1 : (r3 == r12 ? 0 : -1))
            if (r10 >= 0) goto L_0x00a9
            r0.largestBlockSize = r12     // Catch:{ EOFException -> 0x00bc }
        L_0x00a9:
            int r1 = r1 + -1
            r4 = r19
            r3 = 0
            goto L_0x0079
        L_0x00af:
            r2 = r15
            goto L_0x0119
        L_0x00b2:
            r19 = r4
            org.tukaani.xz.CorruptedInputException r1 = new org.tukaani.xz.CorruptedInputException     // Catch:{ EOFException -> 0x00bc }
            r2 = r19
            r1.<init>(r2)     // Catch:{ EOFException -> 0x0119 }
            throw r1     // Catch:{ EOFException -> 0x0119 }
        L_0x00bc:
            r2 = r19
            goto L_0x0119
        L_0x00bf:
            r2 = r4
            int r1 = r17.getIndexPaddingSize()
            long r3 = r18.position()
            long r10 = (long) r1
            long r3 = r3 + r10
            int r10 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r10 != 0) goto L_0x0102
        L_0x00ce:
            int r3 = r1 + -1
            if (r1 <= 0) goto L_0x00e0
            int r1 = r8.read()
            if (r1 != 0) goto L_0x00da
            r1 = r3
            goto L_0x00ce
        L_0x00da:
            org.tukaani.xz.CorruptedInputException r1 = new org.tukaani.xz.CorruptedInputException
            r1.<init>(r2)
            throw r1
        L_0x00e0:
            long r3 = r7.getValue()
            r1 = 0
        L_0x00e5:
            r5 = 4
            if (r1 >= r5) goto L_0x0101
            int r5 = r1 * 8
            long r5 = r3 >>> r5
            r7 = 255(0xff, double:1.26E-321)
            long r5 = r5 & r7
            int r7 = r18.read()
            long r7 = (long) r7
            int r10 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r10 != 0) goto L_0x00fb
            int r1 = r1 + 1
            goto L_0x00e5
        L_0x00fb:
            org.tukaani.xz.CorruptedInputException r1 = new org.tukaani.xz.CorruptedInputException
            r1.<init>(r2)
            throw r1
        L_0x0101:
            return
        L_0x0102:
            org.tukaani.xz.CorruptedInputException r1 = new org.tukaani.xz.CorruptedInputException
            r1.<init>(r2)
            throw r1
        L_0x0108:
            r2 = r4
            org.tukaani.xz.UnsupportedOptionsException r1 = new org.tukaani.xz.UnsupportedOptionsException     // Catch:{ EOFException -> 0x0119 }
            java.lang.String r3 = "XZ Index has over 2147483647 Records"
            r1.<init>(r3)     // Catch:{ EOFException -> 0x0119 }
            throw r1     // Catch:{ EOFException -> 0x0119 }
        L_0x0111:
            r2 = r4
            org.tukaani.xz.CorruptedInputException r1 = new org.tukaani.xz.CorruptedInputException     // Catch:{ EOFException -> 0x0119 }
            r1.<init>(r2)     // Catch:{ EOFException -> 0x0119 }
            throw r1     // Catch:{ EOFException -> 0x0119 }
        L_0x0118:
            r2 = r4
        L_0x0119:
            org.tukaani.xz.CorruptedInputException r1 = new org.tukaani.xz.CorruptedInputException
            r1.<init>(r2)
            throw r1
        L_0x011f:
            r2 = r4
            org.tukaani.xz.CorruptedInputException r1 = new org.tukaani.xz.CorruptedInputException
            r1.<init>(r2)
            goto L_0x0127
        L_0x0126:
            throw r1
        L_0x0127:
            goto L_0x0126
        */
        throw new UnsupportedOperationException("Method not decompiled: org.tukaani.xz.index.IndexDecoder.<init>(org.tukaani.xz.SeekableInputStream, org.tukaani.xz.common.StreamFlags, long, int):void");
    }

    public void setOffsets(IndexDecoder indexDecoder) {
        this.recordOffset = indexDecoder.recordOffset + ((int) indexDecoder.recordCount);
        this.compressedOffset = indexDecoder.compressedOffset + indexDecoder.getStreamSize() + indexDecoder.streamPadding;
        this.uncompressedOffset = indexDecoder.uncompressedOffset + indexDecoder.uncompressedSum;
    }

    public int getMemoryUsage() {
        return this.memoryUsage;
    }

    public StreamFlags getStreamFlags() {
        return this.streamFlags;
    }

    public int getRecordCount() {
        return (int) this.recordCount;
    }

    public long getUncompressedSize() {
        return this.uncompressedSum;
    }

    public long getLargestBlockSize() {
        return this.largestBlockSize;
    }

    public boolean hasUncompressedOffset(long j) {
        long j2 = this.uncompressedOffset;
        return j >= j2 && j < j2 + this.uncompressedSum;
    }

    public boolean hasRecord(int i) {
        int i2 = this.recordOffset;
        return i >= i2 && ((long) i) < ((long) i2) + this.recordCount;
    }

    public void locateBlock(BlockInfo blockInfo, long j) {
        long j2 = j - this.uncompressedOffset;
        int i = 0;
        int length = this.unpadded.length - 1;
        while (i < length) {
            int i2 = ((length - i) / 2) + i;
            if (this.uncompressed[i2] <= j2) {
                i = i2 + 1;
            } else {
                length = i2;
            }
        }
        setBlockInfo(blockInfo, this.recordOffset + i);
    }

    public void setBlockInfo(BlockInfo blockInfo, int i) {
        blockInfo.index = this;
        blockInfo.blockNumber = i;
        int i2 = i - this.recordOffset;
        if (i2 == 0) {
            blockInfo.compressedOffset = 0;
            blockInfo.uncompressedOffset = 0;
        } else {
            int i3 = i2 - 1;
            blockInfo.compressedOffset = (this.unpadded[i3] + 3) & -4;
            blockInfo.uncompressedOffset = this.uncompressed[i3];
        }
        blockInfo.unpaddedSize = this.unpadded[i2] - blockInfo.compressedOffset;
        blockInfo.uncompressedSize = this.uncompressed[i2] - blockInfo.uncompressedOffset;
        blockInfo.compressedOffset += this.compressedOffset + 12;
        blockInfo.uncompressedOffset += this.uncompressedOffset;
    }
}
