package org.tukaani.xz;

import java.io.FilterInputStream;
import java.io.InputStream;

class CountingInputStream extends FilterInputStream {
    private long size = 0;

    public CountingInputStream(InputStream inputStream) {
        super(inputStream);
    }

    public int read() {
        int read = this.in.read();
        if (read != -1) {
            long j = this.size;
            if (j >= 0) {
                this.size = j + 1;
            }
        }
        return read;
    }

    public int read(byte[] bArr, int i, int i2) {
        int read = this.in.read(bArr, i, i2);
        if (read > 0) {
            long j = this.size;
            if (j >= 0) {
                this.size = j + ((long) read);
            }
        }
        return read;
    }

    public long getSize() {
        return this.size;
    }
}
