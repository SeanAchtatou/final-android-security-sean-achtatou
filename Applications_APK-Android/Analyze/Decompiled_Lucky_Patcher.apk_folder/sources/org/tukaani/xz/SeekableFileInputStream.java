package org.tukaani.xz;

import java.io.File;
import java.io.RandomAccessFile;
import net.lingala.zip4j.util.InternalZipConstants;

public class SeekableFileInputStream extends SeekableInputStream {
    protected RandomAccessFile randomAccessFile;

    public SeekableFileInputStream(File file) {
        this.randomAccessFile = new RandomAccessFile(file, InternalZipConstants.READ_MODE);
    }

    public SeekableFileInputStream(String str) {
        this.randomAccessFile = new RandomAccessFile(str, InternalZipConstants.READ_MODE);
    }

    public SeekableFileInputStream(RandomAccessFile randomAccessFile2) {
        this.randomAccessFile = randomAccessFile2;
    }

    public int read() {
        return this.randomAccessFile.read();
    }

    public int read(byte[] bArr) {
        return this.randomAccessFile.read(bArr);
    }

    public int read(byte[] bArr, int i, int i2) {
        return this.randomAccessFile.read(bArr, i, i2);
    }

    public void close() {
        this.randomAccessFile.close();
    }

    public long length() {
        return this.randomAccessFile.length();
    }

    public long position() {
        return this.randomAccessFile.getFilePointer();
    }

    public void seek(long j) {
        this.randomAccessFile.seek(j);
    }
}
