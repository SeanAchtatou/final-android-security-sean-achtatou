package org.tukaani.xz;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.tukaani.xz.check.Check;
import org.tukaani.xz.common.DecoderUtil;
import org.tukaani.xz.common.StreamFlags;
import org.tukaani.xz.index.IndexHash;

public class SingleXZInputStream extends InputStream {
    private BlockInputStream blockDecoder = null;
    private Check check;
    private boolean endReached = false;
    private IOException exception = null;
    private InputStream in;
    private final IndexHash indexHash = new IndexHash();
    private int memoryLimit;
    private StreamFlags streamHeaderFlags;
    private final byte[] tempBuf = new byte[1];

    public SingleXZInputStream(InputStream inputStream) {
        initialize(inputStream, -1);
    }

    public SingleXZInputStream(InputStream inputStream, int i) {
        initialize(inputStream, i);
    }

    SingleXZInputStream(InputStream inputStream, int i, byte[] bArr) {
        initialize(inputStream, i, bArr);
    }

    private void initialize(InputStream inputStream, int i) {
        byte[] bArr = new byte[12];
        new DataInputStream(inputStream).readFully(bArr);
        initialize(inputStream, i, bArr);
    }

    private void initialize(InputStream inputStream, int i, byte[] bArr) {
        this.in = inputStream;
        this.memoryLimit = i;
        this.streamHeaderFlags = DecoderUtil.decodeStreamHeader(bArr);
        this.check = Check.getInstance(this.streamHeaderFlags.checkType);
    }

    public int getCheckType() {
        return this.streamHeaderFlags.checkType;
    }

    public String getCheckName() {
        return this.check.getName();
    }

    public int read() {
        if (read(this.tempBuf, 0, 1) == -1) {
            return -1;
        }
        return this.tempBuf[0] & 255;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:20|21|22|23|(2:25|47)(1:26)) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0036 */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0045 A[Catch:{ IOException -> 0x006b }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0046 A[Catch:{ IOException -> 0x006b }, ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int read(byte[] r12, int r13, int r14) {
        /*
            r11 = this;
            if (r13 < 0) goto L_0x007c
            if (r14 < 0) goto L_0x007c
            int r0 = r13 + r14
            if (r0 < 0) goto L_0x007c
            int r1 = r12.length
            if (r0 > r1) goto L_0x007c
            r0 = 0
            if (r14 != 0) goto L_0x000f
            return r0
        L_0x000f:
            java.io.InputStream r1 = r11.in
            if (r1 == 0) goto L_0x0074
            java.io.IOException r1 = r11.exception
            if (r1 != 0) goto L_0x0073
            boolean r1 = r11.endReached
            r2 = -1
            if (r1 == 0) goto L_0x001d
            return r2
        L_0x001d:
            if (r14 <= 0) goto L_0x0072
            org.tukaani.xz.BlockInputStream r1 = r11.blockDecoder     // Catch:{ IOException -> 0x006b }
            if (r1 != 0) goto L_0x0048
            org.tukaani.xz.BlockInputStream r1 = new org.tukaani.xz.BlockInputStream     // Catch:{ IndexIndicatorException -> 0x0036 }
            java.io.InputStream r4 = r11.in     // Catch:{ IndexIndicatorException -> 0x0036 }
            org.tukaani.xz.check.Check r5 = r11.check     // Catch:{ IndexIndicatorException -> 0x0036 }
            int r6 = r11.memoryLimit     // Catch:{ IndexIndicatorException -> 0x0036 }
            r7 = -1
            r9 = -1
            r3 = r1
            r3.<init>(r4, r5, r6, r7, r9)     // Catch:{ IndexIndicatorException -> 0x0036 }
            r11.blockDecoder = r1     // Catch:{ IndexIndicatorException -> 0x0036 }
            goto L_0x0048
        L_0x0036:
            org.tukaani.xz.index.IndexHash r12 = r11.indexHash     // Catch:{ IOException -> 0x006b }
            java.io.InputStream r13 = r11.in     // Catch:{ IOException -> 0x006b }
            r12.validate(r13)     // Catch:{ IOException -> 0x006b }
            r11.validateStreamFooter()     // Catch:{ IOException -> 0x006b }
            r12 = 1
            r11.endReached = r12     // Catch:{ IOException -> 0x006b }
            if (r0 <= 0) goto L_0x0046
            goto L_0x0047
        L_0x0046:
            r0 = -1
        L_0x0047:
            return r0
        L_0x0048:
            org.tukaani.xz.BlockInputStream r1 = r11.blockDecoder     // Catch:{ IOException -> 0x006b }
            int r1 = r1.read(r12, r13, r14)     // Catch:{ IOException -> 0x006b }
            if (r1 <= 0) goto L_0x0054
            int r0 = r0 + r1
            int r13 = r13 + r1
            int r14 = r14 - r1
            goto L_0x001d
        L_0x0054:
            if (r1 != r2) goto L_0x001d
            org.tukaani.xz.index.IndexHash r1 = r11.indexHash     // Catch:{ IOException -> 0x006b }
            org.tukaani.xz.BlockInputStream r3 = r11.blockDecoder     // Catch:{ IOException -> 0x006b }
            long r3 = r3.getUnpaddedSize()     // Catch:{ IOException -> 0x006b }
            org.tukaani.xz.BlockInputStream r5 = r11.blockDecoder     // Catch:{ IOException -> 0x006b }
            long r5 = r5.getUncompressedSize()     // Catch:{ IOException -> 0x006b }
            r1.add(r3, r5)     // Catch:{ IOException -> 0x006b }
            r1 = 0
            r11.blockDecoder = r1     // Catch:{ IOException -> 0x006b }
            goto L_0x001d
        L_0x006b:
            r12 = move-exception
            r11.exception = r12
            if (r0 == 0) goto L_0x0071
            goto L_0x0072
        L_0x0071:
            throw r12
        L_0x0072:
            return r0
        L_0x0073:
            throw r1
        L_0x0074:
            org.tukaani.xz.XZIOException r12 = new org.tukaani.xz.XZIOException
            java.lang.String r13 = "Stream closed"
            r12.<init>(r13)
            throw r12
        L_0x007c:
            java.lang.IndexOutOfBoundsException r12 = new java.lang.IndexOutOfBoundsException
            r12.<init>()
            goto L_0x0083
        L_0x0082:
            throw r12
        L_0x0083:
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: org.tukaani.xz.SingleXZInputStream.read(byte[], int, int):int");
    }

    private void validateStreamFooter() {
        byte[] bArr = new byte[12];
        new DataInputStream(this.in).readFully(bArr);
        StreamFlags decodeStreamFooter = DecoderUtil.decodeStreamFooter(bArr);
        if (!DecoderUtil.areStreamFlagsEqual(this.streamHeaderFlags, decodeStreamFooter) || this.indexHash.getIndexSize() != decodeStreamFooter.backwardSize) {
            throw new CorruptedInputException("XZ Stream Footer does not match Stream Header");
        }
    }

    public int available() {
        if (this.in != null) {
            IOException iOException = this.exception;
            if (iOException == null) {
                BlockInputStream blockInputStream = this.blockDecoder;
                if (blockInputStream == null) {
                    return 0;
                }
                return blockInputStream.available();
            }
            throw iOException;
        }
        throw new XZIOException("Stream closed");
    }

    public void close() {
        InputStream inputStream = this.in;
        if (inputStream != null) {
            try {
                inputStream.close();
            } finally {
                this.in = null;
            }
        }
    }
}
