package org.tukaani.xz.lzma;

import org.tukaani.xz.LZMA2Options;
import org.tukaani.xz.lz.LZEncoder;
import org.tukaani.xz.lz.Matches;
import org.tukaani.xz.rangecoder.RangeEncoder;

final class LZMAEncoderNormal extends LZMAEncoder {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static int EXTRA_SIZE_AFTER = 4096;
    private static int EXTRA_SIZE_BEFORE = 4096;
    private static final int OPTS = 4096;
    private Matches matches;
    private final State nextState;
    private int optCur;
    private int optEnd;
    private final Optimum[] opts = new Optimum[4096];
    private final int[] repLens;

    static int getMemoryUsage(int i, int i2, int i3) {
        return LZEncoder.getMemoryUsage(i, Math.max(i2, EXTRA_SIZE_BEFORE), EXTRA_SIZE_AFTER, LZMA2Options.NICE_LEN_MAX, i3) + 256;
    }

    LZMAEncoderNormal(RangeEncoder rangeEncoder, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        super(rangeEncoder, LZEncoder.getInstance(i4, Math.max(i5, EXTRA_SIZE_BEFORE), EXTRA_SIZE_AFTER, i6, LZMA2Options.NICE_LEN_MAX, i7, i8), i, i2, i3, i4, i6);
        this.optCur = 0;
        this.optEnd = 0;
        this.repLens = new int[4];
        this.nextState = new State();
        for (int i9 = 0; i9 < 4096; i9++) {
            this.opts[i9] = new Optimum();
        }
    }

    public void reset() {
        this.optCur = 0;
        this.optEnd = 0;
        super.reset();
    }

    private int convertOpts() {
        int i = this.optCur;
        this.optEnd = i;
        int i2 = this.opts[i].optPrev;
        while (true) {
            Optimum optimum = this.opts[this.optCur];
            if (optimum.prev1IsLiteral) {
                Optimum[] optimumArr = this.opts;
                optimumArr[i2].optPrev = this.optCur;
                optimumArr[i2].backPrev = -1;
                int i3 = i2 - 1;
                this.optCur = i2;
                if (optimum.hasPrev2) {
                    Optimum[] optimumArr2 = this.opts;
                    optimumArr2[i3].optPrev = i3 + 1;
                    optimumArr2[i3].backPrev = optimum.backPrev2;
                    this.optCur = i3;
                    i2 = optimum.optPrev2;
                } else {
                    i2 = i3;
                }
            }
            int i4 = this.opts[i2].optPrev;
            Optimum[] optimumArr3 = this.opts;
            optimumArr3[i2].optPrev = this.optCur;
            this.optCur = i2;
            if (this.optCur <= 0) {
                this.optCur = optimumArr3[0].optPrev;
                this.back = this.opts[this.optCur].backPrev;
                return this.optCur;
            }
            i2 = i4;
        }
    }

    /* access modifiers changed from: package-private */
    public int getNextSymbol() {
        int i;
        int shortRepPrice;
        int i2 = this.optCur;
        if (i2 < this.optEnd) {
            int i3 = this.opts[i2].optPrev;
            int i4 = this.optCur;
            int i5 = i3 - i4;
            this.optCur = this.opts[i4].optPrev;
            this.back = this.opts[this.optCur].backPrev;
            return i5;
        }
        this.optCur = 0;
        this.optEnd = 0;
        this.back = -1;
        if (this.readAhead == -1) {
            this.matches = getMatches();
        }
        int min = Math.min(this.lz.getAvail(), (int) LZMA2Options.NICE_LEN_MAX);
        if (min < 2) {
            return 1;
        }
        int i6 = 0;
        for (int i7 = 0; i7 < 4; i7++) {
            this.repLens[i7] = this.lz.getMatchLen(this.reps[i7], min);
            int[] iArr = this.repLens;
            if (iArr[i7] < 2) {
                iArr[i7] = 0;
            } else if (iArr[i7] > iArr[i6]) {
                i6 = i7;
            }
        }
        if (this.repLens[i6] >= this.niceLen) {
            this.back = i6;
            skip(this.repLens[i6] - 1);
            return this.repLens[i6];
        }
        if (this.matches.count > 0) {
            i = this.matches.len[this.matches.count - 1];
            int i8 = this.matches.dist[this.matches.count - 1];
            if (i >= this.niceLen) {
                this.back = i8 + 4;
                skip(i - 1);
                return i;
            }
        } else {
            i = 0;
        }
        int i9 = this.lz.getByte(0);
        int i10 = this.lz.getByte(this.reps[0] + 1);
        if (i < 2 && i9 != i10 && this.repLens[i6] < 2) {
            return 1;
        }
        int pos = this.lz.getPos();
        int i11 = pos & this.posMask;
        this.opts[1].set1(this.literalEncoder.getPrice(i9, i10, this.lz.getByte(1), pos, this.state), 0, -1);
        int anyMatchPrice = getAnyMatchPrice(this.state, i11);
        int anyRepPrice = getAnyRepPrice(anyMatchPrice, this.state);
        if (i10 == i9 && (shortRepPrice = getShortRepPrice(anyRepPrice, this.state, i11)) < this.opts[1].price) {
            this.opts[1].set1(shortRepPrice, 0, 0);
        }
        this.optEnd = Math.max(i, this.repLens[i6]);
        if (this.optEnd < 2) {
            this.back = this.opts[1].backPrev;
            return 1;
        }
        updatePrices();
        this.opts[0].state.set(this.state);
        System.arraycopy(this.reps, 0, this.opts[0].reps, 0, 4);
        for (int i12 = this.optEnd; i12 >= 2; i12--) {
            this.opts[i12].reset();
        }
        for (int i13 = 0; i13 < 4; i13++) {
            int i14 = this.repLens[i13];
            if (i14 >= 2) {
                int longRepPrice = getLongRepPrice(anyRepPrice, i13, this.state, i11);
                do {
                    int price = this.repLenEncoder.getPrice(i14, i11) + longRepPrice;
                    if (price < this.opts[i14].price) {
                        this.opts[i14].set1(price, 0, i13);
                    }
                    i14--;
                } while (i14 >= 2);
            }
        }
        int max = Math.max(this.repLens[0] + 1, 2);
        if (max <= i) {
            int normalMatchPrice = getNormalMatchPrice(anyMatchPrice, this.state);
            int i15 = 0;
            while (max > this.matches.len[i15]) {
                i15++;
            }
            while (true) {
                int i16 = this.matches.dist[i15];
                int matchAndLenPrice = getMatchAndLenPrice(normalMatchPrice, i16, max, i11);
                if (matchAndLenPrice < this.opts[max].price) {
                    this.opts[max].set1(matchAndLenPrice, 0, i16 + 4);
                }
                if (max == this.matches.len[i15] && (i15 = i15 + 1) == this.matches.count) {
                    break;
                }
                max++;
            }
        }
        int min2 = Math.min(this.lz.getAvail(), 4095);
        while (true) {
            int i17 = this.optCur + 1;
            this.optCur = i17;
            if (i17 >= this.optEnd) {
                break;
            }
            this.matches = getMatches();
            if (this.matches.count > 0 && this.matches.len[this.matches.count - 1] >= this.niceLen) {
                break;
            }
            int i18 = min2 - 1;
            int i19 = pos + 1;
            int i20 = i19 & this.posMask;
            updateOptStateAndReps();
            int anyMatchPrice2 = this.opts[this.optCur].price + getAnyMatchPrice(this.opts[this.optCur].state, i20);
            int anyRepPrice2 = getAnyRepPrice(anyMatchPrice2, this.opts[this.optCur].state);
            calc1BytePrices(i19, i20, i18, anyRepPrice2);
            if (i18 >= 2) {
                int calcLongRepPrices = calcLongRepPrices(i19, i20, i18, anyRepPrice2);
                if (this.matches.count > 0) {
                    calcNormalMatchPrices(i19, i20, i18, anyMatchPrice2, calcLongRepPrices);
                }
            }
            min2 = i18;
            pos = i19;
        }
        return convertOpts();
    }

    private void updateOptStateAndReps() {
        int i;
        int i2 = this.opts[this.optCur].optPrev;
        if (this.opts[this.optCur].prev1IsLiteral) {
            i2--;
            if (this.opts[this.optCur].hasPrev2) {
                State state = this.opts[this.optCur].state;
                Optimum[] optimumArr = this.opts;
                state.set(optimumArr[optimumArr[this.optCur].optPrev2].state);
                if (this.opts[this.optCur].backPrev2 < 4) {
                    this.opts[this.optCur].state.updateLongRep();
                } else {
                    this.opts[this.optCur].state.updateMatch();
                }
            } else {
                this.opts[this.optCur].state.set(this.opts[i2].state);
            }
            this.opts[this.optCur].state.updateLiteral();
        } else {
            this.opts[this.optCur].state.set(this.opts[i2].state);
        }
        int i3 = this.optCur;
        if (i2 == i3 - 1) {
            if (this.opts[i3].backPrev == 0) {
                this.opts[this.optCur].state.updateShortRep();
            } else {
                this.opts[this.optCur].state.updateLiteral();
            }
            System.arraycopy(this.opts[i2].reps, 0, this.opts[this.optCur].reps, 0, 4);
            return;
        }
        if (!this.opts[i3].prev1IsLiteral || !this.opts[this.optCur].hasPrev2) {
            i = this.opts[this.optCur].backPrev;
            if (i < 4) {
                this.opts[this.optCur].state.updateLongRep();
            } else {
                this.opts[this.optCur].state.updateMatch();
            }
        } else {
            i2 = this.opts[this.optCur].optPrev2;
            i = this.opts[this.optCur].backPrev2;
            this.opts[this.optCur].state.updateLongRep();
        }
        int i4 = 1;
        if (i < 4) {
            this.opts[this.optCur].reps[0] = this.opts[i2].reps[i];
            while (i4 <= i) {
                this.opts[this.optCur].reps[i4] = this.opts[i2].reps[i4 - 1];
                i4++;
            }
            while (i4 < 4) {
                this.opts[this.optCur].reps[i4] = this.opts[i2].reps[i4];
                i4++;
            }
            return;
        }
        this.opts[this.optCur].reps[0] = i - 4;
        System.arraycopy(this.opts[i2].reps, 0, this.opts[this.optCur].reps, 1, 3);
    }

    private void calc1BytePrices(int i, int i2, int i3, int i4) {
        boolean z;
        int matchLen;
        int i5;
        int shortRepPrice;
        int i6 = this.lz.getByte(0);
        int i7 = this.lz.getByte(this.opts[this.optCur].reps[0] + 1);
        int price = this.opts[this.optCur].price + this.literalEncoder.getPrice(i6, i7, this.lz.getByte(1), i, this.opts[this.optCur].state);
        if (price < this.opts[this.optCur + 1].price) {
            Optimum[] optimumArr = this.opts;
            int i8 = this.optCur;
            optimumArr[i8 + 1].set1(price, i8, -1);
            z = true;
        } else {
            z = false;
        }
        if (i7 == i6 && ((this.opts[this.optCur + 1].optPrev == (i5 = this.optCur) || this.opts[i5 + 1].backPrev != 0) && (shortRepPrice = getShortRepPrice(i4, this.opts[this.optCur].state, i2)) <= this.opts[this.optCur + 1].price)) {
            Optimum[] optimumArr2 = this.opts;
            int i9 = this.optCur;
            optimumArr2[i9 + 1].set1(shortRepPrice, i9, 0);
            z = true;
        }
        if (!z && i7 != i6 && i3 > 2 && (matchLen = this.lz.getMatchLen(1, this.opts[this.optCur].reps[0], Math.min(this.niceLen, i3 - 1))) >= 2) {
            this.nextState.set(this.opts[this.optCur].state);
            this.nextState.updateLiteral();
            int longRepAndLenPrice = price + getLongRepAndLenPrice(0, matchLen, this.nextState, (i + 1) & this.posMask);
            int i10 = this.optCur + 1 + matchLen;
            while (true) {
                int i11 = this.optEnd;
                if (i11 >= i10) {
                    break;
                }
                Optimum[] optimumArr3 = this.opts;
                int i12 = i11 + 1;
                this.optEnd = i12;
                optimumArr3[i12].reset();
            }
            if (longRepAndLenPrice < this.opts[i10].price) {
                this.opts[i10].set2(longRepAndLenPrice, this.optCur, 0);
            }
        }
    }

    private int calcLongRepPrices(int i, int i2, int i3, int i4) {
        int i5;
        int i6 = i2;
        int i7 = i3;
        int min = Math.min(i7, this.niceLen);
        int i8 = 2;
        for (int i9 = 0; i9 < 4; i9++) {
            int matchLen = this.lz.getMatchLen(this.opts[this.optCur].reps[i9], min);
            if (matchLen >= 2) {
                while (true) {
                    int i10 = this.optEnd;
                    i5 = this.optCur;
                    if (i10 >= i5 + matchLen) {
                        break;
                    }
                    Optimum[] optimumArr = this.opts;
                    int i11 = i10 + 1;
                    this.optEnd = i11;
                    optimumArr[i11].reset();
                }
                int longRepPrice = getLongRepPrice(i4, i9, this.opts[i5].state, i6);
                for (int i12 = matchLen; i12 >= 2; i12--) {
                    int price = this.repLenEncoder.getPrice(i12, i6) + longRepPrice;
                    if (price < this.opts[this.optCur + i12].price) {
                        Optimum[] optimumArr2 = this.opts;
                        int i13 = this.optCur;
                        optimumArr2[i13 + i12].set1(price, i13, i9);
                    }
                }
                if (i9 == 0) {
                    i8 = matchLen + 1;
                }
                int i14 = i8;
                int matchLen2 = this.lz.getMatchLen(matchLen + 1, this.opts[this.optCur].reps[i9], Math.min(this.niceLen, (i7 - matchLen) - 1));
                if (matchLen2 >= 2) {
                    int price2 = longRepPrice + this.repLenEncoder.getPrice(matchLen, i6);
                    this.nextState.set(this.opts[this.optCur].state);
                    this.nextState.updateLongRep();
                    int i15 = i + matchLen;
                    int price3 = price2 + this.literalEncoder.getPrice(this.lz.getByte(matchLen, 0), this.lz.getByte(0), this.lz.getByte(matchLen, 1), i15, this.nextState);
                    this.nextState.updateLiteral();
                    int longRepAndLenPrice = price3 + getLongRepAndLenPrice(0, matchLen2, this.nextState, (i15 + 1) & this.posMask);
                    int i16 = this.optCur + matchLen + 1 + matchLen2;
                    while (true) {
                        int i17 = this.optEnd;
                        if (i17 >= i16) {
                            break;
                        }
                        Optimum[] optimumArr3 = this.opts;
                        int i18 = i17 + 1;
                        this.optEnd = i18;
                        optimumArr3[i18].reset();
                    }
                    if (longRepAndLenPrice < this.opts[i16].price) {
                        this.opts[i16].set3(longRepAndLenPrice, this.optCur, i9, matchLen, 0);
                    }
                }
                i8 = i14;
            }
        }
        return i8;
    }

    private void calcNormalMatchPrices(int i, int i2, int i3, int i4, int i5) {
        int i6 = i3;
        int i7 = i5;
        if (this.matches.len[this.matches.count - 1] > i6) {
            this.matches.count = 0;
            while (this.matches.len[this.matches.count] < i6) {
                this.matches.count++;
            }
            int[] iArr = this.matches.len;
            Matches matches2 = this.matches;
            int i8 = matches2.count;
            matches2.count = i8 + 1;
            iArr[i8] = i6;
        }
        if (this.matches.len[this.matches.count - 1] >= i7) {
            while (this.optEnd < this.optCur + this.matches.len[this.matches.count - 1]) {
                Optimum[] optimumArr = this.opts;
                int i9 = this.optEnd + 1;
                this.optEnd = i9;
                optimumArr[i9].reset();
            }
            int normalMatchPrice = getNormalMatchPrice(i4, this.opts[this.optCur].state);
            int i10 = 0;
            while (i7 > this.matches.len[i10]) {
                i10++;
            }
            while (true) {
                int i11 = this.matches.dist[i10];
                int matchAndLenPrice = getMatchAndLenPrice(normalMatchPrice, i11, i7, i2);
                if (matchAndLenPrice < this.opts[this.optCur + i7].price) {
                    Optimum[] optimumArr2 = this.opts;
                    int i12 = this.optCur;
                    optimumArr2[i12 + i7].set1(matchAndLenPrice, i12, i11 + 4);
                }
                if (i7 == this.matches.len[i10]) {
                    int matchLen = this.lz.getMatchLen(i7 + 1, i11, Math.min(this.niceLen, (i6 - i7) - 1));
                    if (matchLen >= 2) {
                        this.nextState.set(this.opts[this.optCur].state);
                        this.nextState.updateMatch();
                        int i13 = i + i7;
                        int price = matchAndLenPrice + this.literalEncoder.getPrice(this.lz.getByte(i7, 0), this.lz.getByte(0), this.lz.getByte(i7, 1), i13, this.nextState);
                        this.nextState.updateLiteral();
                        int longRepAndLenPrice = price + getLongRepAndLenPrice(0, matchLen, this.nextState, (i13 + 1) & this.posMask);
                        int i14 = this.optCur + i7 + 1 + matchLen;
                        while (true) {
                            int i15 = this.optEnd;
                            if (i15 >= i14) {
                                break;
                            }
                            Optimum[] optimumArr3 = this.opts;
                            int i16 = i15 + 1;
                            this.optEnd = i16;
                            optimumArr3[i16].reset();
                        }
                        if (longRepAndLenPrice < this.opts[i14].price) {
                            this.opts[i14].set3(longRepAndLenPrice, this.optCur, i11 + 4, i7, 0);
                        }
                    }
                    i10++;
                    if (i10 == this.matches.count) {
                        return;
                    }
                }
                i7++;
            }
        }
    }
}
