package org.tukaani.xz.lzma;

import org.tukaani.xz.LZMA2Options;
import org.tukaani.xz.lz.LZEncoder;
import org.tukaani.xz.lz.Matches;

final class LZMAEncoderFast extends LZMAEncoder {
    private static int EXTRA_SIZE_AFTER = 272;
    private static int EXTRA_SIZE_BEFORE = 1;
    private Matches matches = null;

    private boolean changePair(int i, int i2) {
        return i < (i2 >>> 7);
    }

    static int getMemoryUsage(int i, int i2, int i3) {
        return LZEncoder.getMemoryUsage(i, Math.max(i2, EXTRA_SIZE_BEFORE), EXTRA_SIZE_AFTER, LZMA2Options.NICE_LEN_MAX, i3);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    LZMAEncoderFast(org.tukaani.xz.rangecoder.RangeEncoder r15, int r16, int r17, int r18, int r19, int r20, int r21, int r22, int r23) {
        /*
            r14 = this;
            int r0 = org.tukaani.xz.lzma.LZMAEncoderFast.EXTRA_SIZE_BEFORE
            r1 = r20
            int r2 = java.lang.Math.max(r1, r0)
            int r3 = org.tukaani.xz.lzma.LZMAEncoderFast.EXTRA_SIZE_AFTER
            r5 = 273(0x111, float:3.83E-43)
            r1 = r19
            r4 = r21
            r6 = r22
            r7 = r23
            org.tukaani.xz.lz.LZEncoder r8 = org.tukaani.xz.lz.LZEncoder.getInstance(r1, r2, r3, r4, r5, r6, r7)
            r6 = r14
            r7 = r15
            r9 = r16
            r10 = r17
            r11 = r18
            r12 = r19
            r13 = r21
            r6.<init>(r7, r8, r9, r10, r11, r12, r13)
            r0 = 0
            r1 = r14
            r1.matches = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.tukaani.xz.lzma.LZMAEncoderFast.<init>(org.tukaani.xz.rangecoder.RangeEncoder, int, int, int, int, int, int, int, int):void");
    }

    /* access modifiers changed from: package-private */
    public int getNextSymbol() {
        int i;
        int i2;
        int i3;
        if (this.readAhead == -1) {
            this.matches = getMatches();
        }
        this.back = -1;
        int min = Math.min(this.lz.getAvail(), (int) LZMA2Options.NICE_LEN_MAX);
        if (min < 2) {
            return 1;
        }
        int i4 = 0;
        int i5 = 0;
        for (int i6 = 0; i6 < 4; i6++) {
            int matchLen = this.lz.getMatchLen(this.reps[i6], min);
            if (matchLen >= 2) {
                if (matchLen >= this.niceLen) {
                    this.back = i6;
                    skip(matchLen - 1);
                    return matchLen;
                } else if (matchLen > i4) {
                    i5 = i6;
                    i4 = matchLen;
                }
            }
        }
        if (this.matches.count > 0) {
            i2 = this.matches.len[this.matches.count - 1];
            i = this.matches.dist[this.matches.count - 1];
            if (i2 >= this.niceLen) {
                this.back = i + 4;
                skip(i2 - 1);
                return i2;
            }
            while (this.matches.count > 1 && i2 == this.matches.len[this.matches.count - 2] + 1 && changePair(this.matches.dist[this.matches.count - 2], i)) {
                this.matches.count--;
                i2 = this.matches.len[this.matches.count - 1];
                i = this.matches.dist[this.matches.count - 1];
            }
            if (i2 == 2 && i >= 128) {
                i2 = 1;
            }
        } else {
            i2 = 0;
            i = 0;
        }
        if (i4 >= 2 && (i4 + 1 >= i2 || ((i4 + 2 >= i2 && i >= 512) || (i4 + 3 >= i2 && i >= 32768)))) {
            this.back = i5;
            skip(i4 - 1);
            return i4;
        } else if (i2 < 2 || min <= 2) {
            return 1;
        } else {
            this.matches = getMatches();
            if (this.matches.count > 0) {
                int i7 = this.matches.len[this.matches.count - 1];
                int i8 = this.matches.dist[this.matches.count - 1];
                if ((i7 >= i2 && i8 < i) || ((i7 == (i3 = i2 + 1) && !changePair(i, i8)) || i7 > i3 || (i7 + 1 >= i2 && i2 >= 3 && changePair(i8, i)))) {
                    return 1;
                }
            }
            int max = Math.max(i2 - 1, 2);
            for (int i9 = 0; i9 < 4; i9++) {
                if (this.lz.getMatchLen(this.reps[i9], max) == max) {
                    return 1;
                }
            }
            this.back = i + 4;
            skip(i2 - 2);
            return i2;
        }
    }
}
