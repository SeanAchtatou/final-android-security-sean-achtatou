package org.tukaani.xz.lzma;

import org.tukaani.xz.lz.LZDecoder;
import org.tukaani.xz.lzma.LZMACoder;
import org.tukaani.xz.rangecoder.RangeDecoder;

public final class LZMADecoder extends LZMACoder {
    private final LiteralDecoder literalDecoder;
    /* access modifiers changed from: private */
    public final LZDecoder lz;
    private final LengthDecoder matchLenDecoder = new LengthDecoder();
    /* access modifiers changed from: private */
    public final RangeDecoder rc;
    private final LengthDecoder repLenDecoder = new LengthDecoder();

    public LZMADecoder(LZDecoder lZDecoder, RangeDecoder rangeDecoder, int i, int i2, int i3) {
        super(i3);
        this.lz = lZDecoder;
        this.rc = rangeDecoder;
        this.literalDecoder = new LiteralDecoder(i, i2);
        reset();
    }

    public void reset() {
        super.reset();
        this.literalDecoder.reset();
        this.matchLenDecoder.reset();
        this.repLenDecoder.reset();
    }

    public boolean endMarkerDetected() {
        return this.reps[0] == -1;
    }

    public void decode() {
        int i;
        this.lz.repeatPending();
        while (this.lz.hasSpace()) {
            int pos = this.lz.getPos() & this.posMask;
            if (this.rc.decodeBit(this.isMatch[this.state.get()], pos) == 0) {
                this.literalDecoder.decode();
            } else {
                if (this.rc.decodeBit(this.isRep, this.state.get()) == 0) {
                    i = decodeMatch(pos);
                } else {
                    i = decodeRepMatch(pos);
                }
                this.lz.repeat(this.reps[0], i);
            }
        }
        this.rc.normalize();
    }

    private int decodeMatch(int i) {
        this.state.updateMatch();
        this.reps[3] = this.reps[2];
        this.reps[2] = this.reps[1];
        this.reps[1] = this.reps[0];
        int decode = this.matchLenDecoder.decode(i);
        int decodeBitTree = this.rc.decodeBitTree(this.distSlots[getDistState(decode)]);
        if (decodeBitTree < 4) {
            this.reps[0] = decodeBitTree;
        } else {
            int i2 = (decodeBitTree >> 1) - 1;
            this.reps[0] = (2 | (decodeBitTree & 1)) << i2;
            if (decodeBitTree < 14) {
                int[] iArr = this.reps;
                iArr[0] = this.rc.decodeReverseBitTree(this.distSpecial[decodeBitTree - 4]) | iArr[0];
            } else {
                int[] iArr2 = this.reps;
                iArr2[0] = (this.rc.decodeDirectBits(i2 - 4) << 4) | iArr2[0];
                int[] iArr3 = this.reps;
                iArr3[0] = iArr3[0] | this.rc.decodeReverseBitTree(this.distAlign);
            }
        }
        return decode;
    }

    private int decodeRepMatch(int i) {
        int i2;
        if (this.rc.decodeBit(this.isRep0, this.state.get()) != 0) {
            if (this.rc.decodeBit(this.isRep1, this.state.get()) == 0) {
                i2 = this.reps[1];
            } else {
                if (this.rc.decodeBit(this.isRep2, this.state.get()) == 0) {
                    i2 = this.reps[2];
                } else {
                    i2 = this.reps[3];
                    this.reps[3] = this.reps[2];
                }
                this.reps[2] = this.reps[1];
            }
            this.reps[1] = this.reps[0];
            this.reps[0] = i2;
        } else if (this.rc.decodeBit(this.isRep0Long[this.state.get()], i) == 0) {
            this.state.updateShortRep();
            return 1;
        }
        this.state.updateLongRep();
        return this.repLenDecoder.decode(i);
    }

    private class LiteralDecoder extends LZMACoder.LiteralCoder {
        LiteralSubdecoder[] subdecoders;

        LiteralDecoder(int i, int i2) {
            super(i, i2);
            this.subdecoders = new LiteralSubdecoder[(1 << (i + i2))];
            int i3 = 0;
            while (true) {
                LiteralSubdecoder[] literalSubdecoderArr = this.subdecoders;
                if (i3 < literalSubdecoderArr.length) {
                    literalSubdecoderArr[i3] = new LiteralSubdecoder();
                    i3++;
                } else {
                    return;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void reset() {
            int i = 0;
            while (true) {
                LiteralSubdecoder[] literalSubdecoderArr = this.subdecoders;
                if (i < literalSubdecoderArr.length) {
                    literalSubdecoderArr[i].reset();
                    i++;
                } else {
                    return;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void decode() {
            this.subdecoders[getSubcoderIndex(LZMADecoder.this.lz.getByte(0), LZMADecoder.this.lz.getPos())].decode();
        }

        private class LiteralSubdecoder extends LZMACoder.LiteralCoder.LiteralSubcoder {
            private LiteralSubdecoder() {
                super();
            }

            /* access modifiers changed from: package-private */
            public void decode() {
                int i = 1;
                if (LZMADecoder.this.state.isLiteral()) {
                    do {
                        i = LZMADecoder.this.rc.decodeBit(this.probs, i) | (i << 1);
                    } while (i < 256);
                } else {
                    int i2 = LZMADecoder.this.lz.getByte(LZMADecoder.this.reps[0]);
                    int i3 = 256;
                    int i4 = 1;
                    do {
                        i2 <<= 1;
                        int i5 = i2 & i3;
                        int decodeBit = LZMADecoder.this.rc.decodeBit(this.probs, i3 + i5 + i4);
                        i4 = (i4 << 1) | decodeBit;
                        i3 &= (i5 ^ -1) ^ (0 - decodeBit);
                    } while (i4 < 256);
                    i = i4;
                }
                LZMADecoder.this.lz.putByte((byte) i);
                LZMADecoder.this.state.updateLiteral();
            }
        }
    }

    private class LengthDecoder extends LZMACoder.LengthCoder {
        private LengthDecoder() {
            super();
        }

        /* access modifiers changed from: package-private */
        public int decode(int i) {
            if (LZMADecoder.this.rc.decodeBit(this.choice, 0) == 0) {
                return LZMADecoder.this.rc.decodeBitTree(this.low[i]) + 2;
            }
            if (LZMADecoder.this.rc.decodeBit(this.choice, 1) == 0) {
                return LZMADecoder.this.rc.decodeBitTree(this.mid[i]) + 2 + 8;
            }
            return LZMADecoder.this.rc.decodeBitTree(this.high) + 2 + 8 + 8;
        }
    }
}
