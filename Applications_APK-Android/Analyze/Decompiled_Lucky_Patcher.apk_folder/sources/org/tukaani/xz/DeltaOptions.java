package org.tukaani.xz;

import java.io.InputStream;

public class DeltaOptions extends FilterOptions {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    public static final int DISTANCE_MAX = 256;
    public static final int DISTANCE_MIN = 1;
    private int distance = 1;

    public int getDecoderMemoryUsage() {
        return 1;
    }

    public DeltaOptions() {
    }

    public DeltaOptions(int i) {
        setDistance(i);
    }

    public void setDistance(int i) {
        if (i < 1 || i > 256) {
            throw new UnsupportedOptionsException("Delta distance must be in the range [1, 256]: " + i);
        }
        this.distance = i;
    }

    public int getDistance() {
        return this.distance;
    }

    public int getEncoderMemoryUsage() {
        return DeltaOutputStream.getMemoryUsage();
    }

    public FinishableOutputStream getOutputStream(FinishableOutputStream finishableOutputStream) {
        return new DeltaOutputStream(finishableOutputStream, this);
    }

    public InputStream getInputStream(InputStream inputStream) {
        return new DeltaInputStream(inputStream, this.distance);
    }

    /* access modifiers changed from: package-private */
    public FilterEncoder getFilterEncoder() {
        return new DeltaEncoder(this);
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException unused) {
            throw new RuntimeException();
        }
    }
}
