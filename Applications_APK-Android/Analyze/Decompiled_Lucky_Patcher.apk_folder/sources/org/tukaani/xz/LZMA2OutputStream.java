package org.tukaani.xz;

import java.io.DataOutputStream;
import java.io.IOException;
import org.tukaani.xz.lz.LZEncoder;
import org.tukaani.xz.lzma.LZMAEncoder;
import org.tukaani.xz.rangecoder.RangeEncoder;

class LZMA2OutputStream extends FinishableOutputStream {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    static final int COMPRESSED_SIZE_MAX = 65536;
    private boolean dictResetNeeded = true;
    private IOException exception = null;
    private boolean finished = false;
    private final LZEncoder lz;
    private final LZMAEncoder lzma;
    private FinishableOutputStream out;
    private final DataOutputStream outData;
    private int pendingSize = 0;
    private final int props;
    private boolean propsNeeded = true;
    private final RangeEncoder rc;
    private boolean stateResetNeeded = true;
    private final byte[] tempBuf = new byte[1];

    private static int getExtraSizeBefore(int i) {
        if (65536 > i) {
            return 65536 - i;
        }
        return 0;
    }

    static int getMemoryUsage(LZMA2Options lZMA2Options) {
        int dictSize = lZMA2Options.getDictSize();
        return LZMAEncoder.getMemoryUsage(lZMA2Options.getMode(), dictSize, getExtraSizeBefore(dictSize), lZMA2Options.getMatchFinder()) + 70;
    }

    LZMA2OutputStream(FinishableOutputStream finishableOutputStream, LZMA2Options lZMA2Options) {
        if (finishableOutputStream != null) {
            this.out = finishableOutputStream;
            this.outData = new DataOutputStream(finishableOutputStream);
            this.rc = new RangeEncoder(65536);
            int dictSize = lZMA2Options.getDictSize();
            int i = dictSize;
            this.lzma = LZMAEncoder.getInstance(this.rc, lZMA2Options.getLc(), lZMA2Options.getLp(), lZMA2Options.getPb(), lZMA2Options.getMode(), i, getExtraSizeBefore(dictSize), lZMA2Options.getNiceLen(), lZMA2Options.getMatchFinder(), lZMA2Options.getDepthLimit());
            this.lz = this.lzma.getLZEncoder();
            byte[] presetDict = lZMA2Options.getPresetDict();
            if (presetDict != null && presetDict.length > 0) {
                this.lz.setPresetDict(dictSize, presetDict);
                this.dictResetNeeded = false;
            }
            this.props = (((lZMA2Options.getPb() * 5) + lZMA2Options.getLp()) * 9) + lZMA2Options.getLc();
            return;
        }
        throw new NullPointerException();
    }

    public void write(int i) {
        byte[] bArr = this.tempBuf;
        bArr[0] = (byte) i;
        write(bArr, 0, 1);
    }

    public void write(byte[] bArr, int i, int i2) {
        int i3;
        if (i < 0 || i2 < 0 || (i3 = i + i2) < 0 || i3 > bArr.length) {
            throw new IndexOutOfBoundsException();
        }
        IOException iOException = this.exception;
        if (iOException != null) {
            throw iOException;
        } else if (!this.finished) {
            while (i2 > 0) {
                try {
                    int fillWindow = this.lz.fillWindow(bArr, i, i2);
                    i += fillWindow;
                    i2 -= fillWindow;
                    this.pendingSize += fillWindow;
                    if (this.lzma.encodeForLZMA2()) {
                        writeChunk();
                    }
                } catch (IOException e) {
                    this.exception = e;
                    throw e;
                }
            }
        } else {
            throw new XZIOException("Stream finished or closed");
        }
    }

    private void writeChunk() {
        int finish = this.rc.finish();
        int uncompressedSize = this.lzma.getUncompressedSize();
        if (finish + 2 < uncompressedSize) {
            writeLZMA(uncompressedSize, finish);
        } else {
            this.lzma.reset();
            uncompressedSize = this.lzma.getUncompressedSize();
            writeUncompressed(uncompressedSize);
        }
        this.pendingSize -= uncompressedSize;
        this.lzma.resetUncompressedSize();
        this.rc.reset();
    }

    private void writeLZMA(int i, int i2) {
        int i3;
        if (this.propsNeeded) {
            i3 = this.dictResetNeeded ? 224 : 192;
        } else {
            i3 = this.stateResetNeeded ? 160 : 128;
        }
        int i4 = i - 1;
        this.outData.writeByte(i3 | (i4 >>> 16));
        this.outData.writeShort(i4);
        this.outData.writeShort(i2 - 1);
        if (this.propsNeeded) {
            this.outData.writeByte(this.props);
        }
        this.rc.write(this.out);
        this.propsNeeded = false;
        this.stateResetNeeded = false;
        this.dictResetNeeded = false;
    }

    private void writeUncompressed(int i) {
        while (true) {
            int i2 = 1;
            if (i > 0) {
                int min = Math.min(i, 65536);
                DataOutputStream dataOutputStream = this.outData;
                if (!this.dictResetNeeded) {
                    i2 = 2;
                }
                dataOutputStream.writeByte(i2);
                this.outData.writeShort(min - 1);
                this.lz.copyUncompressed(this.out, i, min);
                i -= min;
                this.dictResetNeeded = false;
            } else {
                this.stateResetNeeded = true;
                return;
            }
        }
    }

    private void writeEndMarker() {
        IOException iOException = this.exception;
        if (iOException == null) {
            this.lz.setFinishing();
            while (this.pendingSize > 0) {
                try {
                    this.lzma.encodeForLZMA2();
                    writeChunk();
                } catch (IOException e) {
                    this.exception = e;
                    throw e;
                }
            }
            this.out.write(0);
            this.finished = true;
            return;
        }
        throw iOException;
    }

    public void flush() {
        IOException iOException = this.exception;
        if (iOException != null) {
            throw iOException;
        } else if (!this.finished) {
            try {
                this.lz.setFlushing();
                while (this.pendingSize > 0) {
                    this.lzma.encodeForLZMA2();
                    writeChunk();
                }
                this.out.flush();
            } catch (IOException e) {
                this.exception = e;
                throw e;
            }
        } else {
            throw new XZIOException("Stream finished or closed");
        }
    }

    public void finish() {
        if (!this.finished) {
            writeEndMarker();
            try {
                this.out.finish();
                this.finished = true;
            } catch (IOException e) {
                this.exception = e;
                throw e;
            }
        }
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x000b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void close() {
        /*
            r2 = this;
            org.tukaani.xz.FinishableOutputStream r0 = r2.out
            if (r0 == 0) goto L_0x001b
            boolean r0 = r2.finished
            if (r0 != 0) goto L_0x000b
            r2.writeEndMarker()     // Catch:{ IOException -> 0x000b }
        L_0x000b:
            org.tukaani.xz.FinishableOutputStream r0 = r2.out     // Catch:{ IOException -> 0x0011 }
            r0.close()     // Catch:{ IOException -> 0x0011 }
            goto L_0x0018
        L_0x0011:
            r0 = move-exception
            java.io.IOException r1 = r2.exception
            if (r1 != 0) goto L_0x0018
            r2.exception = r0
        L_0x0018:
            r0 = 0
            r2.out = r0
        L_0x001b:
            java.io.IOException r0 = r2.exception
            if (r0 != 0) goto L_0x0020
            return
        L_0x0020:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.tukaani.xz.LZMA2OutputStream.close():void");
    }
}
