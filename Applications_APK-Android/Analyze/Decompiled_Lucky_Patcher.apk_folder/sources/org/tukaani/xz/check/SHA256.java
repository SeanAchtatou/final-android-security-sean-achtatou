package org.tukaani.xz.check;

import java.security.MessageDigest;

public class SHA256 extends Check {
    private final MessageDigest sha256 = MessageDigest.getInstance("SHA-256");

    public SHA256() {
        this.size = 32;
        this.name = "SHA-256";
    }

    public void update(byte[] bArr, int i, int i2) {
        this.sha256.update(bArr, i, i2);
    }

    public byte[] finish() {
        byte[] digest = this.sha256.digest();
        this.sha256.reset();
        return digest;
    }
}
