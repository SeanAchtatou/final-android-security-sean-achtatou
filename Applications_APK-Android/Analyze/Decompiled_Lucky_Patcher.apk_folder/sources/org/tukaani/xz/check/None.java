package org.tukaani.xz.check;

public class None extends Check {
    public void update(byte[] bArr, int i, int i2) {
    }

    public None() {
        this.size = 0;
        this.name = "None";
    }

    public byte[] finish() {
        return new byte[0];
    }
}
