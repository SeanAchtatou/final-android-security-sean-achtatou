package org.tukaani.xz;

import java.io.DataOutputStream;
import java.io.IOException;
import net.lingala.zip4j.util.InternalZipConstants;

class UncompressedLZMA2OutputStream extends FinishableOutputStream {
    private boolean dictResetNeeded = true;
    private IOException exception = null;
    private boolean finished = false;
    private FinishableOutputStream out;
    private final DataOutputStream outData;
    private final byte[] tempBuf = new byte[1];
    private final byte[] uncompBuf = new byte[InternalZipConstants.MIN_SPLIT_LENGTH];
    private int uncompPos = 0;

    static int getMemoryUsage() {
        return 70;
    }

    UncompressedLZMA2OutputStream(FinishableOutputStream finishableOutputStream) {
        if (finishableOutputStream != null) {
            this.out = finishableOutputStream;
            this.outData = new DataOutputStream(finishableOutputStream);
            return;
        }
        throw new NullPointerException();
    }

    public void write(int i) {
        byte[] bArr = this.tempBuf;
        bArr[0] = (byte) i;
        write(bArr, 0, 1);
    }

    public void write(byte[] bArr, int i, int i2) {
        int i3;
        if (i < 0 || i2 < 0 || (i3 = i + i2) < 0 || i3 > bArr.length) {
            throw new IndexOutOfBoundsException();
        }
        IOException iOException = this.exception;
        if (iOException != null) {
            throw iOException;
        } else if (!this.finished) {
            while (i2 > 0) {
                try {
                    int min = Math.min(this.uncompBuf.length - this.uncompPos, i2);
                    System.arraycopy(bArr, i, this.uncompBuf, this.uncompPos, min);
                    i2 -= min;
                    this.uncompPos += min;
                    if (this.uncompPos == this.uncompBuf.length) {
                        writeChunk();
                    }
                } catch (IOException e) {
                    this.exception = e;
                    throw e;
                }
            }
        } else {
            throw new XZIOException("Stream finished or closed");
        }
    }

    private void writeChunk() {
        this.outData.writeByte(this.dictResetNeeded ? 1 : 2);
        this.outData.writeShort(this.uncompPos - 1);
        this.outData.write(this.uncompBuf, 0, this.uncompPos);
        this.uncompPos = 0;
        this.dictResetNeeded = false;
    }

    private void writeEndMarker() {
        IOException iOException = this.exception;
        if (iOException != null) {
            throw iOException;
        } else if (!this.finished) {
            try {
                if (this.uncompPos > 0) {
                    writeChunk();
                }
                this.out.write(0);
            } catch (IOException e) {
                this.exception = e;
                throw e;
            }
        } else {
            throw new XZIOException("Stream finished or closed");
        }
    }

    public void flush() {
        IOException iOException = this.exception;
        if (iOException != null) {
            throw iOException;
        } else if (!this.finished) {
            try {
                if (this.uncompPos > 0) {
                    writeChunk();
                }
                this.out.flush();
            } catch (IOException e) {
                this.exception = e;
                throw e;
            }
        } else {
            throw new XZIOException("Stream finished or closed");
        }
    }

    public void finish() {
        if (!this.finished) {
            writeEndMarker();
            try {
                this.out.finish();
                this.finished = true;
            } catch (IOException e) {
                this.exception = e;
                throw e;
            }
        }
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x000b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void close() {
        /*
            r2 = this;
            org.tukaani.xz.FinishableOutputStream r0 = r2.out
            if (r0 == 0) goto L_0x001b
            boolean r0 = r2.finished
            if (r0 != 0) goto L_0x000b
            r2.writeEndMarker()     // Catch:{ IOException -> 0x000b }
        L_0x000b:
            org.tukaani.xz.FinishableOutputStream r0 = r2.out     // Catch:{ IOException -> 0x0011 }
            r0.close()     // Catch:{ IOException -> 0x0011 }
            goto L_0x0018
        L_0x0011:
            r0 = move-exception
            java.io.IOException r1 = r2.exception
            if (r1 != 0) goto L_0x0018
            r2.exception = r0
        L_0x0018:
            r0 = 0
            r2.out = r0
        L_0x001b:
            java.io.IOException r0 = r2.exception
            if (r0 != 0) goto L_0x0020
            return
        L_0x0020:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.tukaani.xz.UncompressedLZMA2OutputStream.close():void");
    }
}
