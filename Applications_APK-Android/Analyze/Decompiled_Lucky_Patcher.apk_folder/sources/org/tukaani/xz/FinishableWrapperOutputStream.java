package org.tukaani.xz;

import java.io.OutputStream;

public class FinishableWrapperOutputStream extends FinishableOutputStream {
    protected OutputStream out;

    public FinishableWrapperOutputStream(OutputStream outputStream) {
        this.out = outputStream;
    }

    public void write(int i) {
        this.out.write(i);
    }

    public void write(byte[] bArr) {
        this.out.write(bArr);
    }

    public void write(byte[] bArr, int i, int i2) {
        this.out.write(bArr, i, i2);
    }

    public void flush() {
        this.out.flush();
    }

    public void close() {
        this.out.close();
    }
}
