package org.tukaani.xz;

abstract class BCJOptions extends FilterOptions {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private final int alignment;
    int startOffset = 0;

    BCJOptions(int i) {
        this.alignment = i;
    }

    public void setStartOffset(int i) {
        if (((this.alignment - 1) & i) == 0) {
            this.startOffset = i;
            return;
        }
        throw new UnsupportedOptionsException("Start offset must be a multiple of " + this.alignment);
    }

    public int getStartOffset() {
        return this.startOffset;
    }

    public int getEncoderMemoryUsage() {
        return SimpleOutputStream.getMemoryUsage();
    }

    public int getDecoderMemoryUsage() {
        return SimpleInputStream.getMemoryUsage();
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException unused) {
            throw new RuntimeException();
        }
    }
}
