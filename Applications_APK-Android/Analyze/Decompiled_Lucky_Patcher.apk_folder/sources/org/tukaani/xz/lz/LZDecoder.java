package org.tukaani.xz.lz;

import java.io.DataInputStream;
import org.tukaani.xz.CorruptedInputException;

public final class LZDecoder {
    private final byte[] buf;
    private int full = 0;
    private int limit = 0;
    private int pendingDist = 0;
    private int pendingLen = 0;
    private int pos = 0;
    private int start = 0;

    public LZDecoder(int i, byte[] bArr) {
        this.buf = new byte[i];
        if (bArr != null) {
            this.pos = Math.min(bArr.length, i);
            int i2 = this.pos;
            this.full = i2;
            this.start = i2;
            System.arraycopy(bArr, bArr.length - i2, this.buf, 0, i2);
        }
    }

    public void reset() {
        this.start = 0;
        this.pos = 0;
        this.full = 0;
        this.limit = 0;
        byte[] bArr = this.buf;
        bArr[bArr.length - 1] = 0;
    }

    public void setLimit(int i) {
        byte[] bArr = this.buf;
        int length = bArr.length;
        int i2 = this.pos;
        if (length - i2 <= i) {
            this.limit = bArr.length;
        } else {
            this.limit = i2 + i;
        }
    }

    public boolean hasSpace() {
        return this.pos < this.limit;
    }

    public boolean hasPending() {
        return this.pendingLen > 0;
    }

    public int getPos() {
        return this.pos;
    }

    public int getByte(int i) {
        int i2 = this.pos;
        int i3 = (i2 - i) - 1;
        if (i >= i2) {
            i3 += this.buf.length;
        }
        return this.buf[i3] & 255;
    }

    public void putByte(byte b) {
        byte[] bArr = this.buf;
        int i = this.pos;
        this.pos = i + 1;
        bArr[i] = b;
        int i2 = this.full;
        int i3 = this.pos;
        if (i2 < i3) {
            this.full = i3;
        }
    }

    public void repeat(int i, int i2) {
        int i3;
        if (i < 0 || i >= this.full) {
            throw new CorruptedInputException();
        }
        int min = Math.min(this.limit - this.pos, i2);
        this.pendingLen = i2 - min;
        this.pendingDist = i;
        int i4 = this.pos;
        int i5 = (i4 - i) - 1;
        if (i >= i4) {
            i5 += this.buf.length;
        }
        do {
            byte[] bArr = this.buf;
            int i6 = this.pos;
            this.pos = i6 + 1;
            int i7 = i3 + 1;
            bArr[i6] = bArr[i3];
            i3 = i7 == bArr.length ? 0 : i7;
            min--;
        } while (min > 0);
        int i8 = this.full;
        int i9 = this.pos;
        if (i8 < i9) {
            this.full = i9;
        }
    }

    public void repeatPending() {
        int i = this.pendingLen;
        if (i > 0) {
            repeat(this.pendingDist, i);
        }
    }

    public void copyUncompressed(DataInputStream dataInputStream, int i) {
        int min = Math.min(this.buf.length - this.pos, i);
        dataInputStream.readFully(this.buf, this.pos, min);
        this.pos += min;
        int i2 = this.full;
        int i3 = this.pos;
        if (i2 < i3) {
            this.full = i3;
        }
    }

    public int flush(byte[] bArr, int i) {
        int i2 = this.pos;
        int i3 = i2 - this.start;
        if (i2 == this.buf.length) {
            this.pos = 0;
        }
        System.arraycopy(this.buf, this.start, bArr, i, i3);
        this.start = this.pos;
        return i3;
    }
}
