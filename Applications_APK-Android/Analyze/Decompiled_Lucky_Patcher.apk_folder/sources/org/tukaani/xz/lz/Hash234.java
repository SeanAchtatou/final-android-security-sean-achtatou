package org.tukaani.xz.lz;

final class Hash234 extends CRC32Hash {
    private static final int HASH_2_MASK = 1023;
    private static final int HASH_2_SIZE = 1024;
    private static final int HASH_3_MASK = 65535;
    private static final int HASH_3_SIZE = 65536;
    private final int[] hash2Table = new int[1024];
    private int hash2Value = 0;
    private final int[] hash3Table = new int[65536];
    private int hash3Value = 0;
    private final int hash4Mask;
    private final int[] hash4Table;
    private int hash4Value = 0;

    static int getHash4Size(int i) {
        int i2 = i - 1;
        int i3 = i2 | (i2 >>> 1);
        int i4 = i3 | (i3 >>> 2);
        int i5 = i4 | (i4 >>> 4);
        int i6 = ((i5 | (i5 >>> 8)) >>> 1) | 65535;
        if (i6 > 16777216) {
            i6 >>>= 1;
        }
        return i6 + 1;
    }

    static int getMemoryUsage(int i) {
        return ((getHash4Size(i) + 66560) / 256) + 4;
    }

    Hash234(int i) {
        this.hash4Table = new int[getHash4Size(i)];
        this.hash4Mask = this.hash4Table.length - 1;
    }

    /* access modifiers changed from: package-private */
    public void calcHashes(byte[] bArr, int i) {
        byte b = crcTable[bArr[i] & 255] ^ (bArr[i + 1] & 255);
        this.hash2Value = b & 1023;
        byte b2 = b ^ ((bArr[i + 2] & 255) << 8);
        this.hash3Value = 65535 & b2;
        this.hash4Value = ((crcTable[bArr[i + 3] & 255] << 5) ^ b2) & this.hash4Mask;
    }

    /* access modifiers changed from: package-private */
    public int getHash2Pos() {
        return this.hash2Table[this.hash2Value];
    }

    /* access modifiers changed from: package-private */
    public int getHash3Pos() {
        return this.hash3Table[this.hash3Value];
    }

    /* access modifiers changed from: package-private */
    public int getHash4Pos() {
        return this.hash4Table[this.hash4Value];
    }

    /* access modifiers changed from: package-private */
    public void updateTables(int i) {
        this.hash2Table[this.hash2Value] = i;
        this.hash3Table[this.hash3Value] = i;
        this.hash4Table[this.hash4Value] = i;
    }

    /* access modifiers changed from: package-private */
    public void normalize(int i) {
        LZEncoder.normalize(this.hash2Table, i);
        LZEncoder.normalize(this.hash3Table, i);
        LZEncoder.normalize(this.hash4Table, i);
    }
}
