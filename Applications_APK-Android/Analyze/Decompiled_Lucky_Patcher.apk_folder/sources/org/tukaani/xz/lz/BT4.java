package org.tukaani.xz.lz;

final class BT4 extends LZEncoder {
    private int cyclicPos = -1;
    private final int cyclicSize;
    private final int depthLimit;
    private final Hash234 hash;
    private int lzPos;
    private final Matches matches;
    private final int[] tree;

    static int getMemoryUsage(int i) {
        return Hash234.getMemoryUsage(i) + (i / 128) + 10;
    }

    BT4(int i, int i2, int i3, int i4, int i5, int i6) {
        super(i, i2, i3, i4, i5);
        this.cyclicSize = i + 1;
        this.lzPos = this.cyclicSize;
        this.hash = new Hash234(i);
        this.tree = new int[(this.cyclicSize * 2)];
        this.matches = new Matches(i4 - 1);
        this.depthLimit = i6 <= 0 ? (i4 / 2) + 16 : i6;
    }

    private int movePos() {
        int movePos = movePos(this.niceLen, 4);
        if (movePos != 0) {
            int i = this.lzPos + 1;
            this.lzPos = i;
            if (i == Integer.MAX_VALUE) {
                int i2 = Integer.MAX_VALUE - this.cyclicSize;
                this.hash.normalize(i2);
                normalize(this.tree, i2);
                this.lzPos -= i2;
            }
            int i3 = this.cyclicPos + 1;
            this.cyclicPos = i3;
            if (i3 == this.cyclicSize) {
                this.cyclicPos = 0;
            }
        }
        return movePos;
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x015c  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0168  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.tukaani.xz.lz.Matches getMatches() {
        /*
            r16 = this;
            r0 = r16
            org.tukaani.xz.lz.Matches r1 = r0.matches
            r2 = 0
            r1.count = r2
            int r1 = r0.matchLenMax
            int r3 = r0.niceLen
            int r4 = r16.movePos()
            if (r4 >= r1) goto L_0x001a
            if (r4 != 0) goto L_0x0016
            org.tukaani.xz.lz.Matches r1 = r0.matches
            return r1
        L_0x0016:
            if (r3 <= r4) goto L_0x001b
            r3 = r4
            goto L_0x001b
        L_0x001a:
            r4 = r1
        L_0x001b:
            org.tukaani.xz.lz.Hash234 r1 = r0.hash
            byte[] r5 = r0.buf
            int r6 = r0.readPos
            r1.calcHashes(r5, r6)
            int r1 = r0.lzPos
            org.tukaani.xz.lz.Hash234 r5 = r0.hash
            int r5 = r5.getHash2Pos()
            int r1 = r1 - r5
            int r5 = r0.lzPos
            org.tukaani.xz.lz.Hash234 r6 = r0.hash
            int r6 = r6.getHash3Pos()
            int r5 = r5 - r6
            org.tukaani.xz.lz.Hash234 r6 = r0.hash
            int r6 = r6.getHash4Pos()
            org.tukaani.xz.lz.Hash234 r7 = r0.hash
            int r8 = r0.lzPos
            r7.updateTables(r8)
            int r7 = r0.cyclicSize
            r8 = 2
            r9 = 1
            if (r1 >= r7) goto L_0x006b
            byte[] r7 = r0.buf
            int r10 = r0.readPos
            int r10 = r10 - r1
            byte r7 = r7[r10]
            byte[] r10 = r0.buf
            int r11 = r0.readPos
            byte r10 = r10[r11]
            if (r7 != r10) goto L_0x006b
            org.tukaani.xz.lz.Matches r7 = r0.matches
            int[] r7 = r7.len
            r7[r2] = r8
            org.tukaani.xz.lz.Matches r7 = r0.matches
            int[] r7 = r7.dist
            int r10 = r1 + -1
            r7[r2] = r10
            org.tukaani.xz.lz.Matches r7 = r0.matches
            r7.count = r9
            goto L_0x006c
        L_0x006b:
            r8 = 0
        L_0x006c:
            r7 = 3
            if (r1 == r5) goto L_0x0094
            int r10 = r0.cyclicSize
            if (r5 >= r10) goto L_0x0094
            byte[] r10 = r0.buf
            int r11 = r0.readPos
            int r11 = r11 - r5
            byte r10 = r10[r11]
            byte[] r11 = r0.buf
            int r12 = r0.readPos
            byte r11 = r11[r12]
            if (r10 != r11) goto L_0x0094
            org.tukaani.xz.lz.Matches r1 = r0.matches
            int[] r1 = r1.dist
            org.tukaani.xz.lz.Matches r8 = r0.matches
            int r10 = r8.count
            int r11 = r10 + 1
            r8.count = r11
            int r8 = r5 + -1
            r1[r10] = r8
            r1 = r5
            r8 = 3
        L_0x0094:
            org.tukaani.xz.lz.Matches r5 = r0.matches
            int r5 = r5.count
            if (r5 <= 0) goto L_0x00c3
        L_0x009a:
            if (r8 >= r4) goto L_0x00b0
            byte[] r5 = r0.buf
            int r10 = r0.readPos
            int r10 = r10 + r8
            int r10 = r10 - r1
            byte r5 = r5[r10]
            byte[] r10 = r0.buf
            int r11 = r0.readPos
            int r11 = r11 + r8
            byte r10 = r10[r11]
            if (r5 != r10) goto L_0x00b0
            int r8 = r8 + 1
            goto L_0x009a
        L_0x00b0:
            org.tukaani.xz.lz.Matches r1 = r0.matches
            int[] r1 = r1.len
            org.tukaani.xz.lz.Matches r5 = r0.matches
            int r5 = r5.count
            int r5 = r5 - r9
            r1[r5] = r8
            if (r8 < r3) goto L_0x00c3
            r0.skip(r3, r6)
            org.tukaani.xz.lz.Matches r1 = r0.matches
            return r1
        L_0x00c3:
            if (r8 >= r7) goto L_0x00c6
            goto L_0x00c7
        L_0x00c6:
            r7 = r8
        L_0x00c7:
            int r1 = r0.depthLimit
            int r5 = r0.cyclicPos
            int r8 = r5 << 1
            int r8 = r8 + r9
            int r5 = r5 << r9
            r11 = r7
            r7 = 0
            r10 = 0
        L_0x00d2:
            int r12 = r0.lzPos
            int r12 = r12 - r6
            int r13 = r1 + -1
            if (r1 == 0) goto L_0x0176
            int r1 = r0.cyclicSize
            if (r12 < r1) goto L_0x00df
            goto L_0x0176
        L_0x00df:
            int r14 = r0.cyclicPos
            int r15 = r14 - r12
            if (r12 <= r14) goto L_0x00e6
            goto L_0x00e7
        L_0x00e6:
            r1 = 0
        L_0x00e7:
            int r15 = r15 + r1
            int r1 = r15 << 1
            int r14 = java.lang.Math.min(r7, r10)
            byte[] r15 = r0.buf
            int r2 = r0.readPos
            int r2 = r2 + r14
            int r2 = r2 - r12
            byte r2 = r15[r2]
            byte[] r15 = r0.buf
            int r9 = r0.readPos
            int r9 = r9 + r14
            byte r9 = r15[r9]
            if (r2 != r9) goto L_0x0146
        L_0x00ff:
            r2 = 1
            int r14 = r14 + r2
            if (r14 >= r4) goto L_0x0114
            byte[] r2 = r0.buf
            int r9 = r0.readPos
            int r9 = r9 + r14
            int r9 = r9 - r12
            byte r2 = r2[r9]
            byte[] r9 = r0.buf
            int r15 = r0.readPos
            int r15 = r15 + r14
            byte r9 = r9[r15]
            if (r2 == r9) goto L_0x00ff
        L_0x0114:
            if (r14 <= r11) goto L_0x0146
            org.tukaani.xz.lz.Matches r2 = r0.matches
            int[] r2 = r2.len
            org.tukaani.xz.lz.Matches r9 = r0.matches
            int r9 = r9.count
            r2[r9] = r14
            org.tukaani.xz.lz.Matches r2 = r0.matches
            int[] r2 = r2.dist
            org.tukaani.xz.lz.Matches r9 = r0.matches
            int r9 = r9.count
            int r11 = r12 + -1
            r2[r9] = r11
            org.tukaani.xz.lz.Matches r2 = r0.matches
            int r9 = r2.count
            r15 = 1
            int r9 = r9 + r15
            r2.count = r9
            if (r14 < r3) goto L_0x0144
            int[] r2 = r0.tree
            r3 = r2[r1]
            r2[r5] = r3
            int r1 = r1 + r15
            r1 = r2[r1]
            r2[r8] = r1
            org.tukaani.xz.lz.Matches r1 = r0.matches
            return r1
        L_0x0144:
            r11 = r14
            goto L_0x0147
        L_0x0146:
            r15 = 1
        L_0x0147:
            byte[] r2 = r0.buf
            int r9 = r0.readPos
            int r9 = r9 + r14
            int r9 = r9 - r12
            byte r2 = r2[r9]
            r2 = r2 & 255(0xff, float:3.57E-43)
            byte[] r9 = r0.buf
            int r12 = r0.readPos
            int r12 = r12 + r14
            byte r9 = r9[r12]
            r9 = r9 & 255(0xff, float:3.57E-43)
            if (r2 >= r9) goto L_0x0168
            int[] r2 = r0.tree
            r2[r5] = r6
            int r1 = r1 + 1
            r2 = r2[r1]
            r5 = r1
            r6 = r2
            r10 = r14
            goto L_0x0171
        L_0x0168:
            int[] r2 = r0.tree
            r2[r8] = r6
            r2 = r2[r1]
            r8 = r1
            r6 = r2
            r7 = r14
        L_0x0171:
            r1 = r13
            r2 = 0
            r9 = 1
            goto L_0x00d2
        L_0x0176:
            int[] r1 = r0.tree
            r2 = 0
            r1[r8] = r2
            r1[r5] = r2
            org.tukaani.xz.lz.Matches r1 = r0.matches
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.tukaani.xz.lz.BT4.getMatches():org.tukaani.xz.lz.Matches");
    }

    private void skip(int i, int i2) {
        int i3;
        int i4 = this.depthLimit;
        int i5 = this.cyclicPos;
        int i6 = (i5 << 1) + 1;
        int i7 = i5 << 1;
        int i8 = 0;
        int i9 = 0;
        while (true) {
            int i10 = this.lzPos - i2;
            int i11 = i4 - 1;
            if (i4 == 0 || i10 >= (i3 = this.cyclicSize)) {
                int[] iArr = this.tree;
                iArr[i6] = 0;
                iArr[i7] = 0;
            } else {
                int i12 = this.cyclicPos;
                int i13 = i12 - i10;
                if (i10 <= i12) {
                    i3 = 0;
                }
                int i14 = (i13 + i3) << 1;
                int min = Math.min(i8, i9);
                if (this.buf[(this.readPos + min) - i10] == this.buf[this.readPos + min]) {
                    do {
                        min++;
                        if (min == i) {
                            int[] iArr2 = this.tree;
                            iArr2[i7] = iArr2[i14];
                            iArr2[i6] = iArr2[i14 + 1];
                            return;
                        }
                    } while (this.buf[(this.readPos + min) - i10] == this.buf[this.readPos + min]);
                }
                if ((this.buf[(this.readPos + min) - i10] & 255) < (this.buf[this.readPos + min] & 255)) {
                    int[] iArr3 = this.tree;
                    iArr3[i7] = i2;
                    int i15 = i14 + 1;
                    i2 = iArr3[i15];
                    i7 = i15;
                    i9 = min;
                } else {
                    int[] iArr4 = this.tree;
                    iArr4[i6] = i2;
                    i2 = iArr4[i14];
                    i6 = i14;
                    i8 = min;
                }
                i4 = i11;
            }
        }
        int[] iArr5 = this.tree;
        iArr5[i6] = 0;
        iArr5[i7] = 0;
    }

    public void skip(int i) {
        while (true) {
            int i2 = i - 1;
            if (i > 0) {
                int i3 = this.niceLen;
                int movePos = movePos();
                if (movePos < i3) {
                    if (movePos == 0) {
                        i = i2;
                    } else {
                        i3 = movePos;
                    }
                }
                this.hash.calcHashes(this.buf, this.readPos);
                int hash4Pos = this.hash.getHash4Pos();
                this.hash.updateTables(this.lzPos);
                skip(i3, hash4Pos);
                i = i2;
            } else {
                return;
            }
        }
    }
}
