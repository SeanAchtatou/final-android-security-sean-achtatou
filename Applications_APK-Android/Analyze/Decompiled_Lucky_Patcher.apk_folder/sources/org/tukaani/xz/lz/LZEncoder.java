package org.tukaani.xz.lz;

import java.io.OutputStream;
import org.tukaani.xz.common.Util;

public abstract class LZEncoder {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    public static final int MF_BT4 = 20;
    public static final int MF_HC4 = 4;
    final byte[] buf;
    private boolean finishing = false;
    private final int keepSizeAfter;
    private final int keepSizeBefore;
    final int matchLenMax;
    final int niceLen;
    private int pendingSize = 0;
    private int readLimit = -1;
    int readPos = -1;
    private int writePos = 0;

    public abstract Matches getMatches();

    public abstract void skip(int i);

    static void normalize(int[] iArr, int i) {
        for (int i2 = 0; i2 < iArr.length; i2++) {
            if (iArr[i2] <= i) {
                iArr[i2] = 0;
            } else {
                iArr[i2] = iArr[i2] - i;
            }
        }
    }

    private static int getBufSize(int i, int i2, int i3, int i4) {
        return i2 + i + i3 + i4 + Math.min((i / 2) + 262144, 536870912);
    }

    public static int getMemoryUsage(int i, int i2, int i3, int i4, int i5) {
        int i6;
        int bufSize = (getBufSize(i, i2, i3, i4) / Util.BLOCK_HEADER_SIZE_MAX) + 10;
        if (i5 == 4) {
            i6 = HC4.getMemoryUsage(i);
        } else if (i5 == 20) {
            i6 = BT4.getMemoryUsage(i);
        } else {
            throw new IllegalArgumentException();
        }
        return bufSize + i6;
    }

    public static LZEncoder getInstance(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        if (i6 == 4) {
            return new HC4(i, i2, i3, i4, i5, i7);
        }
        if (i6 == 20) {
            return new BT4(i, i2, i3, i4, i5, i7);
        }
        throw new IllegalArgumentException();
    }

    LZEncoder(int i, int i2, int i3, int i4, int i5) {
        this.buf = new byte[getBufSize(i, i2, i3, i5)];
        this.keepSizeBefore = i2 + i;
        this.keepSizeAfter = i3 + i5;
        this.matchLenMax = i5;
        this.niceLen = i4;
    }

    public void setPresetDict(int i, byte[] bArr) {
        if (bArr != null) {
            int min = Math.min(bArr.length, i);
            System.arraycopy(bArr, bArr.length - min, this.buf, 0, min);
            this.writePos += min;
            skip(min);
        }
    }

    private void moveWindow() {
        int i = ((this.readPos + 1) - this.keepSizeBefore) & -16;
        byte[] bArr = this.buf;
        System.arraycopy(bArr, i, bArr, 0, this.writePos - i);
        this.readPos -= i;
        this.readLimit -= i;
        this.writePos -= i;
    }

    public int fillWindow(byte[] bArr, int i, int i2) {
        if (this.readPos >= this.buf.length - this.keepSizeAfter) {
            moveWindow();
        }
        byte[] bArr2 = this.buf;
        int length = bArr2.length;
        int i3 = this.writePos;
        if (i2 > length - i3) {
            i2 = bArr2.length - i3;
        }
        System.arraycopy(bArr, i, this.buf, this.writePos, i2);
        this.writePos += i2;
        int i4 = this.writePos;
        int i5 = this.keepSizeAfter;
        if (i4 >= i5) {
            this.readLimit = i4 - i5;
        }
        processPendingBytes();
        return i2;
    }

    private void processPendingBytes() {
        int i;
        int i2 = this.pendingSize;
        if (i2 > 0 && (i = this.readPos) < this.readLimit) {
            this.readPos = i - i2;
            this.pendingSize = 0;
            skip(i2);
        }
    }

    public boolean isStarted() {
        return this.readPos != -1;
    }

    public void setFlushing() {
        this.readLimit = this.writePos - 1;
        processPendingBytes();
    }

    public void setFinishing() {
        this.readLimit = this.writePos - 1;
        this.finishing = true;
        processPendingBytes();
    }

    public boolean hasEnoughData(int i) {
        return this.readPos - i < this.readLimit;
    }

    public void copyUncompressed(OutputStream outputStream, int i, int i2) {
        outputStream.write(this.buf, (this.readPos + 1) - i, i2);
    }

    public int getAvail() {
        return this.writePos - this.readPos;
    }

    public int getPos() {
        return this.readPos;
    }

    public int getByte(int i) {
        return this.buf[this.readPos - i] & 255;
    }

    public int getByte(int i, int i2) {
        return this.buf[(this.readPos + i) - i2] & 255;
    }

    public int getMatchLen(int i, int i2) {
        int i3 = (this.readPos - i) - 1;
        int i4 = 0;
        while (i4 < i2) {
            byte[] bArr = this.buf;
            if (bArr[this.readPos + i4] != bArr[i3 + i4]) {
                break;
            }
            i4++;
        }
        return i4;
    }

    public int getMatchLen(int i, int i2, int i3) {
        int i4 = this.readPos + i;
        int i5 = (i4 - i2) - 1;
        int i6 = 0;
        while (i6 < i3) {
            byte[] bArr = this.buf;
            if (bArr[i4 + i6] != bArr[i5 + i6]) {
                break;
            }
            i6++;
        }
        return i6;
    }

    public boolean verifyMatches(Matches matches) {
        int min = Math.min(getAvail(), this.matchLenMax);
        for (int i = 0; i < matches.count; i++) {
            if (getMatchLen(matches.dist[i], min) != matches.len[i]) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public int movePos(int i, int i2) {
        this.readPos++;
        int i3 = this.writePos - this.readPos;
        if (i3 >= i) {
            return i3;
        }
        if (i3 >= i2 && this.finishing) {
            return i3;
        }
        this.pendingSize++;
        return 0;
    }
}
