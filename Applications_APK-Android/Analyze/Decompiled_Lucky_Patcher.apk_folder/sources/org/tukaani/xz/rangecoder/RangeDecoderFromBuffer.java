package org.tukaani.xz.rangecoder;

import java.io.DataInputStream;
import org.tukaani.xz.CorruptedInputException;

public final class RangeDecoderFromBuffer extends RangeDecoder {
    private static final int INIT_SIZE = 5;
    private final byte[] buf;
    private int end = 0;
    private int pos = 0;

    public RangeDecoderFromBuffer(int i) {
        this.buf = new byte[(i - 5)];
    }

    public void prepareInputBuffer(DataInputStream dataInputStream, int i) {
        if (i < 5) {
            throw new CorruptedInputException();
        } else if (dataInputStream.readUnsignedByte() == 0) {
            this.code = dataInputStream.readInt();
            this.range = -1;
            this.pos = 0;
            this.end = i - 5;
            dataInputStream.readFully(this.buf, 0, this.end);
        } else {
            throw new CorruptedInputException();
        }
    }

    public boolean isInBufferOK() {
        return this.pos <= this.end;
    }

    public boolean isFinished() {
        return this.pos == this.end && this.code == 0;
    }

    public void normalize() {
        if ((this.range & -16777216) == 0) {
            try {
                byte[] bArr = this.buf;
                int i = this.pos;
                this.pos = i + 1;
                this.code = (this.code << 8) | (bArr[i] & 255);
                this.range <<= 8;
            } catch (ArrayIndexOutOfBoundsException unused) {
                throw new CorruptedInputException();
            }
        }
    }
}
