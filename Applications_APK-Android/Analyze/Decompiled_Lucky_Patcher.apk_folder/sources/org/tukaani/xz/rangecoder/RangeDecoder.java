package org.tukaani.xz.rangecoder;

public abstract class RangeDecoder extends RangeCoder {
    int code = 0;
    int range = 0;

    public abstract void normalize();

    public int decodeBit(short[] sArr, int i) {
        normalize();
        short s = sArr[i];
        int i2 = this.range;
        int i3 = (i2 >>> 11) * s;
        int i4 = this.code;
        if ((i4 ^ Integer.MIN_VALUE) < (Integer.MIN_VALUE ^ i3)) {
            this.range = i3;
            sArr[i] = (short) (s + ((2048 - s) >>> 5));
            return 0;
        }
        this.range = i2 - i3;
        this.code = i4 - i3;
        sArr[i] = (short) (s - (s >>> 5));
        return 1;
    }

    public int decodeBitTree(short[] sArr) {
        int i = 1;
        do {
            i = decodeBit(sArr, i) | (i << 1);
        } while (i < sArr.length);
        return i - sArr.length;
    }

    public int decodeReverseBitTree(short[] sArr) {
        int i = 1;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int decodeBit = decodeBit(sArr, i);
            i = (i << 1) | decodeBit;
            int i4 = i3 + 1;
            i2 |= decodeBit << i3;
            if (i >= sArr.length) {
                return i2;
            }
            i3 = i4;
        }
    }

    public int decodeDirectBits(int i) {
        int i2 = 0;
        do {
            normalize();
            this.range >>>= 1;
            int i3 = this.code;
            int i4 = this.range;
            int i5 = (i3 - i4) >>> 31;
            this.code = i3 - (i4 & (i5 - 1));
            i2 = (i2 << 1) | (1 - i5);
            i--;
        } while (i != 0);
        return i2;
    }
}
