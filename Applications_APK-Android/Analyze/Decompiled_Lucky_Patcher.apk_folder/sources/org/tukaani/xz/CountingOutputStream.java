package org.tukaani.xz;

import java.io.OutputStream;

class CountingOutputStream extends FinishableOutputStream {
    private final OutputStream out;
    private long size = 0;

    public CountingOutputStream(OutputStream outputStream) {
        this.out = outputStream;
    }

    public void write(int i) {
        this.out.write(i);
        long j = this.size;
        if (j >= 0) {
            this.size = j + 1;
        }
    }

    public void write(byte[] bArr, int i, int i2) {
        this.out.write(bArr, i, i2);
        long j = this.size;
        if (j >= 0) {
            this.size = j + ((long) i2);
        }
    }

    public void flush() {
        this.out.flush();
    }

    public void close() {
        this.out.close();
    }

    public long getSize() {
        return this.size;
    }
}
