package org.ʻ.ʻ.ʿ.ʻ;

import org.ʻ.ʻ.ʾ.ʻ.C1188;
import org.ʻ.ʻ.ʾ.ʻ.C1189;
import org.ʻ.ʻ.ʾ.ʻ.C1190;
import org.ʻ.ʻ.ʾ.ʻ.C1191;
import org.ʻ.ʻ.ʾ.ʻ.C1193;
import org.ʻ.ʻ.ʾ.ʻ.C1194;
import org.ʻ.ʻ.ʾ.ʻ.C1195;
import org.ʻ.ʻ.ʾ.ʻ.C1196;
import org.ʻ.ʼ.C1404;
import org.ʻ.ʼ.C1406;

/* renamed from: org.ʻ.ʻ.ʿ.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: ImmutableDebugItem */
public abstract class C1294 implements C1188 {

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final C1406<C1294, C1188> f7091 = new C1406<C1294, C1188>() {
        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m7163(C1188 r1) {
            return r1 instanceof C1294;
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʼ  reason: contains not printable characters */
        public C1294 m7160(C1188 r1) {
            return C1294.m7158(r1);
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final int f7092;

    public C1294(int i) {
        this.f7092 = i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1294 m7158(C1188 r3) {
        if (r3 instanceof C1294) {
            return (C1294) r3;
        }
        switch (r3.m7005()) {
            case 3:
                return C1301.m7188((C1196) r3);
            case 4:
            default:
                throw new C1404("Invalid debug item type: %d", Integer.valueOf(r3.m7005()));
            case 5:
                return C1295.m7164((C1189) r3);
            case 6:
                return C1299.m7177((C1194) r3);
            case 7:
                return C1298.m7175((C1193) r3);
            case 8:
                return C1296.m7170((C1190) r3);
            case 9:
                return C1300.m7183((C1195) r3);
            case 10:
                return C1297.m7172((C1191) r3);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m7159() {
        return this.f7092;
    }
}
