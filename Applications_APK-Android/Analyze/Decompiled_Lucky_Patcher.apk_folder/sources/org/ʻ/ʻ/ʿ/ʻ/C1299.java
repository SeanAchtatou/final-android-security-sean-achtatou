package org.ʻ.ʻ.ʿ.ʻ;

import org.ʻ.ʻ.ʾ.ʻ.C1194;

/* renamed from: org.ʻ.ʻ.ʿ.ʻ.ˆ  reason: contains not printable characters */
/* compiled from: ImmutableRestartLocal */
public class C1299 extends C1294 implements C1194 {

    /* renamed from: ʼ  reason: contains not printable characters */
    protected final int f7098;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected final String f7099;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected final String f7100;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected final String f7101;

    /* renamed from: ˈ  reason: contains not printable characters */
    public int m7182() {
        return 6;
    }

    public C1299(int i, int i2, String str, String str2, String str3) {
        super(i);
        this.f7098 = i2;
        this.f7099 = str;
        this.f7100 = str2;
        this.f7101 = str3;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1299 m7177(C1194 r7) {
        if (r7 instanceof C1299) {
            return (C1299) r7;
        }
        return new C1299(r7.m7004(), r7.m7011(), r7.m7008(), r7.m7010(), r7.m7009());
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m7180() {
        return this.f7098;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public String m7181() {
        return this.f7099;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m7178() {
        return this.f7100;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m7179() {
        return this.f7101;
    }
}
