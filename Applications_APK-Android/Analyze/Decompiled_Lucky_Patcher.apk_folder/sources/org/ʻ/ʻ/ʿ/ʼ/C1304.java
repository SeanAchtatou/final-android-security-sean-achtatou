package org.ʻ.ʻ.ʿ.ʼ;

import com.google.ʻ.ʼ.C0891;
import java.util.List;
import org.ʻ.ʻ.ʻ.ʻ.C1006;
import org.ʻ.ʻ.ʾ.ʽ.C1261;
import org.ʻ.ʻ.ʿ.ʽ.C1307;

/* renamed from: org.ʻ.ʻ.ʿ.ʼ.ʽ  reason: contains not printable characters */
/* compiled from: ImmutableMethodProtoReference */
public class C1304 extends C1006 implements C1306 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final C0891<String> f7116;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected final String f7117;

    public C1304(Iterable<? extends CharSequence> iterable, String str) {
        this.f7116 = C1307.m7216(iterable);
        this.f7117 = str;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static C1304 m7207(C1261 r2) {
        if (r2 instanceof C1304) {
            return (C1304) r2;
        }
        return new C1304(r2.m7072(), r2.m7073());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public List<? extends CharSequence> m7208() {
        return this.f7116;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m7209() {
        return this.f7117;
    }
}
