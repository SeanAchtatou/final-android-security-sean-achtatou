package org.ʻ.ʻ.ʿ.ʾ;

import org.ʻ.ʻ.ʻ.ʼ.C1016;
import org.ʻ.ʻ.ʾ.ʾ.C1271;

/* renamed from: org.ʻ.ʻ.ʿ.ʾ.ʿ  reason: contains not printable characters */
/* compiled from: ImmutableCharEncodedValue */
public class C1312 extends C1016 implements C1314 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final char f7130;

    public C1312(char c) {
        this.f7130 = c;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1312 m7233(C1271 r1) {
        if (r1 instanceof C1312) {
            return (C1312) r1;
        }
        return new C1312(r1.m7095());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public char m7234() {
        return this.f7130;
    }
}
