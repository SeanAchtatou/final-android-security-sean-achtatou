package org.ʻ.ʻ.ʿ.ʾ;

import com.google.ʻ.ʼ.C0904;
import java.util.Collection;
import org.ʻ.ʻ.ʻ.ʼ.C1012;
import org.ʻ.ʻ.ʾ.C1197;
import org.ʻ.ʻ.ʾ.ʾ.C1267;
import org.ʻ.ʻ.ʿ.C1293;

/* renamed from: org.ʻ.ʻ.ʿ.ʾ.ʻ  reason: contains not printable characters */
/* compiled from: ImmutableAnnotationEncodedValue */
public class C1308 extends C1012 implements C1314 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final String f7123;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected final C0904<? extends C1293> f7124;

    public C1308(String str, Collection<? extends C1197> collection) {
        this.f7123 = str;
        this.f7124 = C1293.m7150(collection);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1308 m7221(C1267 r2) {
        if (r2 instanceof C1308) {
            return (C1308) r2;
        }
        return new C1308(r2.m7086(), r2.m7087());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m7222() {
        return this.f7123;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public C0904<? extends C1293> m7223() {
        return this.f7124;
    }
}
