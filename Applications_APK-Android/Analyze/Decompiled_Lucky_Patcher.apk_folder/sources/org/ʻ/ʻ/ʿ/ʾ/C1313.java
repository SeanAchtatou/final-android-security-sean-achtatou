package org.ʻ.ʻ.ʿ.ʾ;

import org.ʻ.ʻ.ʻ.ʼ.C1017;
import org.ʻ.ʻ.ʾ.ʾ.C1272;

/* renamed from: org.ʻ.ʻ.ʿ.ʾ.ˆ  reason: contains not printable characters */
/* compiled from: ImmutableDoubleEncodedValue */
public class C1313 extends C1017 implements C1314 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final double f7131;

    public C1313(double d) {
        this.f7131 = d;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1313 m7235(C1272 r3) {
        if (r3 instanceof C1313) {
            return (C1313) r3;
        }
        return new C1313(r3.m7097());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public double m7236() {
        return this.f7131;
    }
}
