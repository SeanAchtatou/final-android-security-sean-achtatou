package org.ʻ.ʻ.ʿ.ʼ;

import com.google.ʻ.ʼ.C0891;
import org.ʻ.ʻ.ʻ.ʻ.C1007;
import org.ʻ.ʻ.ʾ.ʽ.C1262;
import org.ʻ.ʻ.ʿ.ʽ.C1307;

/* renamed from: org.ʻ.ʻ.ʿ.ʼ.ʾ  reason: contains not printable characters */
/* compiled from: ImmutableMethodReference */
public class C1305 extends C1007 implements C1306 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final String f7118;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected final String f7119;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected final C0891<String> f7120;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected final String f7121;

    public C1305(String str, String str2, Iterable<? extends CharSequence> iterable, String str3) {
        this.f7118 = str;
        this.f7119 = str2;
        this.f7120 = C1307.m7216(iterable);
        this.f7121 = str3;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static C1305 m7210(C1262 r4) {
        if (r4 instanceof C1305) {
            return (C1305) r4;
        }
        return new C1305(r4.m7075(), r4.m7076(), r4.m7078(), r4.m7077());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m7211() {
        return this.f7118;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m7213() {
        return this.f7119;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0891<String> m7215() {
        return this.f7120;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public String m7214() {
        return this.f7121;
    }
}
