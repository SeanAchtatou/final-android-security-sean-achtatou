package org.ʻ.ʻ.ʿ.ʻ;

import org.ʻ.ʻ.ʻ.ʻ.C1009;
import org.ʻ.ʻ.ʻ.ʻ.C1010;
import org.ʻ.ʻ.ʾ.ʻ.C1196;
import org.ʻ.ʻ.ʾ.ʽ.C1264;
import org.ʻ.ʻ.ʾ.ʽ.C1265;

/* renamed from: org.ʻ.ʻ.ʿ.ʻ.ˉ  reason: contains not printable characters */
/* compiled from: ImmutableStartLocal */
public class C1301 extends C1294 implements C1196 {

    /* renamed from: ʼ  reason: contains not printable characters */
    protected final int f7104;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected final String f7105;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected final String f7106;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected final String f7107;

    /* renamed from: ˈ  reason: contains not printable characters */
    public int m7193() {
        return 3;
    }

    public C1301(int i, int i2, String str, String str2, String str3) {
        super(i);
        this.f7104 = i2;
        this.f7105 = str;
        this.f7106 = str2;
        this.f7107 = str3;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1301 m7188(C1196 r7) {
        if (r7 instanceof C1301) {
            return (C1301) r7;
        }
        return new C1301(r7.m7004(), r7.m7014(), r7.m7010(), r7.m7008(), r7.m7009());
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m7191() {
        return this.f7104;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public C1264 m7194() {
        if (this.f7105 == null) {
            return null;
        }
        return new C1009() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public String m7197() {
                return C1301.this.f7105;
            }
        };
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public C1265 m7195() {
        if (this.f7106 == null) {
            return null;
        }
        return new C1010() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public String m7198() {
                return C1301.this.f7106;
            }
        };
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public C1264 m7196() {
        if (this.f7107 == null) {
            return null;
        }
        return new C1009() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public String m7199() {
                return C1301.this.f7107;
            }
        };
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public String m7192() {
        return this.f7105;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m7189() {
        return this.f7106;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m7190() {
        return this.f7107;
    }
}
