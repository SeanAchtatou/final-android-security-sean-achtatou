package org.ʻ.ʻ.ʿ.ʾ;

import org.ʻ.ʻ.ʻ.ʼ.C1027;
import org.ʻ.ʻ.ʾ.ʾ.C1283;

/* renamed from: org.ʻ.ʻ.ʿ.ʾ.ᴵ  reason: contains not printable characters */
/* compiled from: ImmutableShortEncodedValue */
public class C1325 extends C1027 implements C1314 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final short f7142;

    public C1325(short s) {
        this.f7142 = s;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1325 m7265(C1283 r1) {
        if (r1 instanceof C1325) {
            return (C1325) r1;
        }
        return new C1325(r1.m7117());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public short m7266() {
        return this.f7142;
    }
}
