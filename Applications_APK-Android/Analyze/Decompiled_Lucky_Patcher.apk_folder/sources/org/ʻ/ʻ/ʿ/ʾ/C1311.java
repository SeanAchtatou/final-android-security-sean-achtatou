package org.ʻ.ʻ.ʿ.ʾ;

import org.ʻ.ʻ.ʻ.ʼ.C1015;
import org.ʻ.ʻ.ʾ.ʾ.C1270;

/* renamed from: org.ʻ.ʻ.ʿ.ʾ.ʾ  reason: contains not printable characters */
/* compiled from: ImmutableByteEncodedValue */
public class C1311 extends C1015 implements C1314 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final byte f7129;

    public C1311(byte b) {
        this.f7129 = b;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1311 m7231(C1270 r1) {
        if (r1 instanceof C1311) {
            return (C1311) r1;
        }
        return new C1311(r1.m7093());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public byte m7232() {
        return this.f7129;
    }
}
