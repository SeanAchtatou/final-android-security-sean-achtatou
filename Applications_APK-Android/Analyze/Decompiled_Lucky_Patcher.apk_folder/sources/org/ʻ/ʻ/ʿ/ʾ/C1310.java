package org.ʻ.ʻ.ʿ.ʾ;

import org.ʻ.ʻ.ʻ.ʼ.C1014;
import org.ʻ.ʻ.ʾ.ʾ.C1269;

/* renamed from: org.ʻ.ʻ.ʿ.ʾ.ʽ  reason: contains not printable characters */
/* compiled from: ImmutableBooleanEncodedValue */
public class C1310 extends C1014 implements C1314 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final C1310 f7126 = new C1310(true);

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final C1310 f7127 = new C1310(false);

    /* renamed from: ʽ  reason: contains not printable characters */
    protected final boolean f7128;

    private C1310(boolean z) {
        this.f7128 = z;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1310 m7229(boolean z) {
        return z ? f7126 : f7127;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1310 m7228(C1269 r0) {
        return m7229(r0.m7091());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m7230() {
        return this.f7128;
    }
}
