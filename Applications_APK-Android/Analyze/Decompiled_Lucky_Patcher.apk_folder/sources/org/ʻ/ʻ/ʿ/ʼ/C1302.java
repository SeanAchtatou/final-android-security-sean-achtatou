package org.ʻ.ʻ.ʿ.ʼ;

import org.ʻ.ʻ.ʻ.ʻ.C1004;
import org.ʻ.ʻ.ʾ.ʽ.C1259;

/* renamed from: org.ʻ.ʻ.ʿ.ʼ.ʻ  reason: contains not printable characters */
/* compiled from: ImmutableFieldReference */
public class C1302 extends C1004 implements C1306 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final String f7111;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected final String f7112;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected final String f7113;

    public C1302(String str, String str2, String str3) {
        this.f7111 = str;
        this.f7112 = str2;
        this.f7113 = str3;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static C1302 m7200(C1259 r3) {
        if (r3 instanceof C1302) {
            return (C1302) r3;
        }
        return new C1302(r3.m7067(), r3.m7065(), r3.m7066());
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m7203() {
        return this.f7111;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m7201() {
        return this.f7112;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m7202() {
        return this.f7113;
    }
}
