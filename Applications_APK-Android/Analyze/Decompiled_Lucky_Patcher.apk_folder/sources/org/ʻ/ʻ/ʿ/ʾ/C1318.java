package org.ʻ.ʻ.ʿ.ʾ;

import org.ʻ.ʻ.ʻ.ʼ.C1020;
import org.ʻ.ʻ.ʾ.ʾ.C1276;

/* renamed from: org.ʻ.ʻ.ʿ.ʾ.ˎ  reason: contains not printable characters */
/* compiled from: ImmutableFloatEncodedValue */
public class C1318 extends C1020 implements C1314 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final float f7135;

    public C1318(float f) {
        this.f7135 = f;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1318 m7250(C1276 r1) {
        if (r1 instanceof C1318) {
            return (C1318) r1;
        }
        return new C1318(r1.m7104());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public float m7251() {
        return this.f7135;
    }
}
