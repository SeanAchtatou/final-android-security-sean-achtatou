package org.ʻ.ʻ.ʿ.ʾ;

import com.google.ʻ.ʻ.C0847;
import com.google.ʻ.ʼ.C0891;
import org.ʻ.ʻ.ʾ.ʾ.C1267;
import org.ʻ.ʻ.ʾ.ʾ.C1268;
import org.ʻ.ʻ.ʾ.ʾ.C1269;
import org.ʻ.ʻ.ʾ.ʾ.C1270;
import org.ʻ.ʻ.ʾ.ʾ.C1271;
import org.ʻ.ʻ.ʾ.ʾ.C1272;
import org.ʻ.ʻ.ʾ.ʾ.C1273;
import org.ʻ.ʻ.ʾ.ʾ.C1274;
import org.ʻ.ʻ.ʾ.ʾ.C1275;
import org.ʻ.ʻ.ʾ.ʾ.C1276;
import org.ʻ.ʻ.ʾ.ʾ.C1277;
import org.ʻ.ʻ.ʾ.ʾ.C1278;
import org.ʻ.ʻ.ʾ.ʾ.C1279;
import org.ʻ.ʻ.ʾ.ʾ.C1280;
import org.ʻ.ʻ.ʾ.ʾ.C1281;
import org.ʻ.ʻ.ʾ.ʾ.C1283;
import org.ʻ.ʻ.ʾ.ʾ.C1284;
import org.ʻ.ʻ.ʾ.ʾ.C1285;
import org.ʻ.ʼ.C1404;
import org.ʻ.ʼ.C1406;

/* renamed from: org.ʻ.ʻ.ʿ.ʾ.ˉ  reason: contains not printable characters */
/* compiled from: ImmutableEncodedValueFactory */
public class C1315 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final C1406<C1314, C1273> f7132 = new C1406<C1314, C1273>() {
        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m7243(C1273 r1) {
            return r1 instanceof C1314;
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʼ  reason: contains not printable characters */
        public C1314 m7240(C1273 r1) {
            return C1315.m7239(r1);
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1314 m7239(C1273 r2) {
        int r0 = r2.m7098();
        if (r0 == 0) {
            return C1311.m7231((C1270) r2);
        }
        if (r0 == 6) {
            return C1320.m7254((C1278) r2);
        }
        if (r0 == 2) {
            return C1325.m7265((C1283) r2);
        }
        if (r0 == 3) {
            return C1312.m7233((C1271) r2);
        }
        if (r0 == 4) {
            return C1319.m7252((C1277) r2);
        }
        if (r0 == 16) {
            return C1318.m7250((C1276) r2);
        }
        if (r0 == 17) {
            return C1313.m7235((C1272) r2);
        }
        switch (r0) {
            case 21:
                return C1323.m7262((C1281) r2);
            case 22:
                return C1322.m7259((C1280) r2);
            case 23:
                return C1326.m7267((C1284) r2);
            case 24:
                return C1327.m7269((C1285) r2);
            case 25:
                return C1317.m7247((C1275) r2);
            case 26:
                return C1321.m7256((C1279) r2);
            case 27:
                return C1316.m7244((C1274) r2);
            case 28:
                return C1309.m7225((C1268) r2);
            case 29:
                return C1308.m7221((C1267) r2);
            case 30:
                return C1324.f7141;
            case 31:
                return C1310.m7228((C1269) r2);
            default:
                C0847.m5422(false);
                return null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1273 m7238(String str) {
        char charAt = str.charAt(0);
        if (charAt == 'F') {
            return new C1318(0.0f);
        }
        if (charAt != 'L') {
            if (charAt == 'S') {
                return new C1325(0);
            }
            if (charAt == 'I') {
                return new C1319(0);
            }
            if (charAt == 'J') {
                return new C1320(0);
            }
            if (charAt == 'Z') {
                return C1310.f7127;
            }
            if (charAt != '[') {
                switch (charAt) {
                    case 'B':
                        return new C1311((byte) 0);
                    case 'C':
                        return new C1312(0);
                    case 'D':
                        return new C1313(0.0d);
                    default:
                        throw new C1404("Unrecognized type: %s", str);
                }
            }
        }
        return C1324.f7141;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0891<C1314> m7237(Iterable<? extends C1273> iterable) {
        return f7132.m7802((Iterable) iterable);
    }
}
