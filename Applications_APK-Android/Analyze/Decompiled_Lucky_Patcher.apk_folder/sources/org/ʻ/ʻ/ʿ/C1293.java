package org.ʻ.ʻ.ʿ;

import com.google.ʻ.ʼ.C0904;
import org.ʻ.ʻ.ʻ.C1011;
import org.ʻ.ʻ.ʾ.C1197;
import org.ʻ.ʻ.ʾ.ʾ.C1273;
import org.ʻ.ʻ.ʿ.ʾ.C1314;
import org.ʻ.ʻ.ʿ.ʾ.C1315;
import org.ʻ.ʼ.C1406;

/* renamed from: org.ʻ.ʻ.ʿ.ʻ  reason: contains not printable characters */
/* compiled from: ImmutableAnnotationElement */
public class C1293 extends C1011 {

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final C1406<C1293, C1197> f7088 = new C1406<C1293, C1197>() {
        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m7157(C1197 r1) {
            return r1 instanceof C1293;
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʼ  reason: contains not printable characters */
        public C1293 m7154(C1197 r1) {
            return C1293.m7151(r1);
        }
    };

    /* renamed from: ʼ  reason: contains not printable characters */
    protected final String f7089;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected final C1314 f7090;

    public C1293(String str, C1273 r2) {
        this.f7089 = str;
        this.f7090 = C1315.m7239(r2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static C1293 m7151(C1197 r2) {
        if (r2 instanceof C1293) {
            return (C1293) r2;
        }
        return new C1293(r2.m7019(), r2.m7020());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m7152() {
        return this.f7089;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C1273 m7153() {
        return this.f7090;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0904<C1293> m7150(Iterable<? extends C1197> iterable) {
        return f7088.m7804((Iterable) iterable);
    }
}
