package org.ʻ.ʻ.ʿ.ʾ;

import org.ʻ.ʻ.ʻ.ʼ.C1022;
import org.ʻ.ʻ.ʾ.ʾ.C1278;

/* renamed from: org.ʻ.ʻ.ʿ.ʾ.ˑ  reason: contains not printable characters */
/* compiled from: ImmutableLongEncodedValue */
public class C1320 extends C1022 implements C1314 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final long f7137;

    public C1320(long j) {
        this.f7137 = j;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1320 m7254(C1278 r3) {
        if (r3 instanceof C1320) {
            return (C1320) r3;
        }
        return new C1320(r3.m7108());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public long m7255() {
        return this.f7137;
    }
}
