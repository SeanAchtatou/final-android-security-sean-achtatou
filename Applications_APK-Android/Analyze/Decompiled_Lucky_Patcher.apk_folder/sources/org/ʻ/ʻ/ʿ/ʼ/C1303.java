package org.ʻ.ʻ.ʿ.ʼ;

import org.ʻ.ʻ.ʻ.ʻ.C1005;
import org.ʻ.ʻ.ʾ.ʽ.C1259;
import org.ʻ.ʻ.ʾ.ʽ.C1260;
import org.ʻ.ʻ.ʾ.ʽ.C1262;
import org.ʻ.ʻ.ʾ.ʽ.C1263;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ʿ.ʼ.ʼ  reason: contains not printable characters */
/* compiled from: ImmutableMethodHandleReference */
public class C1303 extends C1005 implements C1306 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final int f7114;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected final C1306 f7115;

    public C1303(int i, C1306 r2) {
        this.f7114 = i;
        this.f7115 = r2;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static C1303 m7204(C1260 r3) {
        C1306 r32;
        if (r3 instanceof C1303) {
            return (C1303) r3;
        }
        int r0 = r3.m7068();
        switch (r0) {
            case 0:
            case 1:
            case 2:
            case 3:
                r32 = C1302.m7200((C1259) r3.m7070());
                break;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                r32 = C1305.m7210((C1262) r3.m7070());
                break;
            default:
                throw new C1404("Invalid method handle type: %d", Integer.valueOf(r0));
        }
        return new C1303(r0, r32);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m7205() {
        return this.f7114;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C1263 m7206() {
        return this.f7115;
    }
}
