package org.ʻ.ʻ;

import org.ʻ.ʻ.ʾ.ʽ.C1258;
import org.ʻ.ʻ.ʾ.ʽ.C1259;
import org.ʻ.ʻ.ʾ.ʽ.C1260;
import org.ʻ.ʻ.ʾ.ʽ.C1261;
import org.ʻ.ʻ.ʾ.ʽ.C1262;
import org.ʻ.ʻ.ʾ.ʽ.C1263;
import org.ʻ.ʻ.ʾ.ʽ.C1264;
import org.ʻ.ʻ.ʾ.ʽ.C1265;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ˆ  reason: contains not printable characters */
/* compiled from: ReferenceType */
public final class C1328 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m7271(C1263 r1) {
        if (r1 instanceof C1264) {
            return 0;
        }
        if (r1 instanceof C1265) {
            return 1;
        }
        if (r1 instanceof C1259) {
            return 2;
        }
        if (r1 instanceof C1262) {
            return 3;
        }
        if (r1 instanceof C1261) {
            return 4;
        }
        if (r1 instanceof C1258) {
            return 5;
        }
        if (r1 instanceof C1260) {
            return 6;
        }
        throw new IllegalStateException("Invalid reference");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m7272(int i) {
        if (i < 0 || i > 4) {
            throw new C1330(i);
        }
    }

    /* renamed from: org.ʻ.ʻ.ˆ$ʻ  reason: contains not printable characters */
    /* compiled from: ReferenceType */
    public static class C1330 extends C1404 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final int f7146;

        public C1330(int i) {
            super("Invalid reference type: %d", Integer.valueOf(i));
            this.f7146 = i;
        }
    }
}
