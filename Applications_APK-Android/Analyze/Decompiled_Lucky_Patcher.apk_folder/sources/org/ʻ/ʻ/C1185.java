package org.ʻ.ʻ;

import com.google.ʻ.ʼ.C0860;
import com.google.ʻ.ʼ.C0866;
import com.google.ʻ.ʼ.C0901;
import com.google.ʻ.ʼ.C0926;
import java.util.ArrayList;
import java.util.List;

/* renamed from: org.ʻ.ʻ.ʾ  reason: contains not printable characters */
/* compiled from: Opcode */
public enum C1185 {
    NOP(0, "nop", 7, C1092.Format10x, 4),
    MOVE(1, "move", 7, C1092.Format12x, 20),
    MOVE_FROM16(2, "move/from16", 7, C1092.Format22x, 20),
    MOVE_16(3, "move/16", 7, C1092.Format32x, 20),
    MOVE_WIDE(4, "move-wide", 7, C1092.Format12x, 52),
    MOVE_WIDE_FROM16(5, "move-wide/from16", 7, C1092.Format22x, 52),
    MOVE_WIDE_16(6, "move-wide/16", 7, C1092.Format32x, 52),
    MOVE_OBJECT(7, "move-object", 7, C1092.Format12x, 20),
    MOVE_OBJECT_FROM16(8, "move-object/from16", 7, C1092.Format22x, 20),
    MOVE_OBJECT_16(9, "move-object/16", 7, C1092.Format32x, 20),
    MOVE_RESULT(10, "move-result", 7, C1092.Format11x, 20),
    MOVE_RESULT_WIDE(11, "move-result-wide", 7, C1092.Format11x, 52),
    MOVE_RESULT_OBJECT(12, "move-result-object", 7, C1092.Format11x, 20),
    MOVE_EXCEPTION(13, "move-exception", 7, C1092.Format11x, 20),
    RETURN_VOID(14, "return-void", 7, C1092.Format10x),
    RETURN(15, "return", 7, C1092.Format11x),
    RETURN_WIDE(16, "return-wide", 7, C1092.Format11x),
    RETURN_OBJECT(17, "return-object", 7, C1092.Format11x),
    CONST_4(18, "const/4", 7, C1092.Format11n, 20),
    CONST_16(19, "const/16", 7, C1092.Format21s, 20),
    CONST(20, "const", 7, C1092.Format31i, 20),
    CONST_HIGH16(21, "const/high16", 7, C1092.Format21ih, 20),
    CONST_WIDE_16(22, "const-wide/16", 7, C1092.Format21s, 52),
    CONST_WIDE_32(23, "const-wide/32", 7, C1092.Format31i, 52),
    CONST_WIDE(24, "const-wide", 7, C1092.Format51l, 52),
    CONST_WIDE_HIGH16(25, "const-wide/high16", 7, C1092.Format21lh, 52),
    CONST_STRING(26, "const-string", 0, C1092.Format21c, 21),
    CONST_STRING_JUMBO(27, "const-string/jumbo", 0, C1092.Format31c, 21),
    CONST_CLASS(28, "const-class", 1, C1092.Format21c, 21),
    MONITOR_ENTER(29, "monitor-enter", 7, C1092.Format11x, 5),
    MONITOR_EXIT(30, "monitor-exit", 7, C1092.Format11x, 5),
    CHECK_CAST(31, "check-cast", 1, C1092.Format21c, 21),
    INSTANCE_OF(32, "instance-of", 1, C1092.Format22c, 21),
    ARRAY_LENGTH(33, "array-length", 7, C1092.Format12x, 21),
    NEW_INSTANCE(34, "new-instance", 1, C1092.Format21c, 21),
    NEW_ARRAY(35, "new-array", 1, C1092.Format22c, 21),
    FILLED_NEW_ARRAY(36, "filled-new-array", 1, C1092.Format35c, 13),
    FILLED_NEW_ARRAY_RANGE(37, "filled-new-array/range", 1, C1092.Format3rc, 13),
    FILL_ARRAY_DATA(38, "fill-array-data", 7, C1092.Format31t, 4),
    THROW(39, "throw", 7, C1092.Format11x, 1),
    GOTO(40, "goto", 7, C1092.Format10t),
    GOTO_16(41, "goto/16", 7, C1092.Format20t),
    GOTO_32(42, "goto/32", 7, C1092.Format30t),
    PACKED_SWITCH(43, "packed-switch", 7, C1092.Format31t, 4),
    SPARSE_SWITCH(44, "sparse-switch", 7, C1092.Format31t, 4),
    CMPL_FLOAT(45, "cmpl-float", 7, C1092.Format23x, 20),
    CMPG_FLOAT(46, "cmpg-float", 7, C1092.Format23x, 20),
    CMPL_DOUBLE(47, "cmpl-double", 7, C1092.Format23x, 20),
    CMPG_DOUBLE(48, "cmpg-double", 7, C1092.Format23x, 20),
    CMP_LONG(49, "cmp-long", 7, C1092.Format23x, 20),
    IF_EQ(50, "if-eq", 7, C1092.Format22t, 4),
    IF_NE(51, "if-ne", 7, C1092.Format22t, 4),
    IF_LT(52, "if-lt", 7, C1092.Format22t, 4),
    IF_GE(53, "if-ge", 7, C1092.Format22t, 4),
    IF_GT(54, "if-gt", 7, C1092.Format22t, 4),
    IF_LE(55, "if-le", 7, C1092.Format22t, 4),
    IF_EQZ(56, "if-eqz", 7, C1092.Format21t, 4),
    IF_NEZ(57, "if-nez", 7, C1092.Format21t, 4),
    IF_LTZ(58, "if-ltz", 7, C1092.Format21t, 4),
    IF_GEZ(59, "if-gez", 7, C1092.Format21t, 4),
    IF_GTZ(60, "if-gtz", 7, C1092.Format21t, 4),
    IF_LEZ(61, "if-lez", 7, C1092.Format21t, 4),
    AGET(68, "aget", 7, C1092.Format23x, 21),
    AGET_WIDE(69, "aget-wide", 7, C1092.Format23x, 53),
    AGET_OBJECT(70, "aget-object", 7, C1092.Format23x, 21),
    AGET_BOOLEAN(71, "aget-boolean", 7, C1092.Format23x, 21),
    AGET_BYTE(72, "aget-byte", 7, C1092.Format23x, 21),
    AGET_CHAR(73, "aget-char", 7, C1092.Format23x, 21),
    AGET_SHORT(74, "aget-short", 7, C1092.Format23x, 21),
    APUT(75, "aput", 7, C1092.Format23x, 5),
    APUT_WIDE(76, "aput-wide", 7, C1092.Format23x, 5),
    APUT_OBJECT(77, "aput-object", 7, C1092.Format23x, 5),
    APUT_BOOLEAN(78, "aput-boolean", 7, C1092.Format23x, 5),
    APUT_BYTE(79, "aput-byte", 7, C1092.Format23x, 5),
    APUT_CHAR(80, "aput-char", 7, C1092.Format23x, 5),
    APUT_SHORT(81, "aput-short", 7, C1092.Format23x, 5),
    IGET(82, "iget", 2, C1092.Format22c, 21),
    IGET_WIDE(83, "iget-wide", 2, C1092.Format22c, 53),
    IGET_OBJECT(84, "iget-object", 2, C1092.Format22c, 21),
    IGET_BOOLEAN(85, "iget-boolean", 2, C1092.Format22c, 21),
    IGET_BYTE(86, "iget-byte", 2, C1092.Format22c, 21),
    IGET_CHAR(87, "iget-char", 2, C1092.Format22c, 21),
    IGET_SHORT(88, "iget-short", 2, C1092.Format22c, 21),
    IPUT(89, "iput", 2, C1092.Format22c, 5),
    IPUT_WIDE(90, "iput-wide", 2, C1092.Format22c, 5),
    IPUT_OBJECT(91, "iput-object", 2, C1092.Format22c, 5),
    IPUT_BOOLEAN(92, "iput-boolean", 2, C1092.Format22c, 5),
    IPUT_BYTE(93, "iput-byte", 2, C1092.Format22c, 5),
    IPUT_CHAR(94, "iput-char", 2, C1092.Format22c, 5),
    IPUT_SHORT(95, "iput-short", 2, C1092.Format22c, 5),
    SGET(96, "sget", 2, C1092.Format21c, 277),
    SGET_WIDE(97, "sget-wide", 2, C1092.Format21c, 309),
    SGET_OBJECT(98, "sget-object", 2, C1092.Format21c, 277),
    SGET_BOOLEAN(99, "sget-boolean", 2, C1092.Format21c, 277),
    SGET_BYTE(100, "sget-byte", 2, C1092.Format21c, 277),
    SGET_CHAR(101, "sget-char", 2, C1092.Format21c, 277),
    SGET_SHORT(102, "sget-short", 2, C1092.Format21c, 277),
    SPUT(103, "sput", 2, C1092.Format21c, 261),
    SPUT_WIDE(104, "sput-wide", 2, C1092.Format21c, 261),
    SPUT_OBJECT(105, "sput-object", 2, C1092.Format21c, 261),
    SPUT_BOOLEAN(106, "sput-boolean", 2, C1092.Format21c, 261),
    SPUT_BYTE(107, "sput-byte", 2, C1092.Format21c, 261),
    SPUT_CHAR(108, "sput-char", 2, C1092.Format21c, 261),
    SPUT_SHORT(109, "sput-short", 2, C1092.Format21c, 261),
    INVOKE_VIRTUAL(110, "invoke-virtual", 3, C1092.Format35c, 13),
    INVOKE_SUPER(111, "invoke-super", 3, C1092.Format35c, 13),
    INVOKE_DIRECT(112, "invoke-direct", 3, C1092.Format35c, 1037),
    INVOKE_STATIC(113, "invoke-static", 3, C1092.Format35c, 13),
    INVOKE_INTERFACE(114, "invoke-interface", 3, C1092.Format35c, 13),
    INVOKE_VIRTUAL_RANGE(116, "invoke-virtual/range", 3, C1092.Format3rc, 13),
    INVOKE_SUPER_RANGE(117, "invoke-super/range", 3, C1092.Format3rc, 13),
    INVOKE_DIRECT_RANGE(118, "invoke-direct/range", 3, C1092.Format3rc, 1037),
    INVOKE_STATIC_RANGE(119, "invoke-static/range", 3, C1092.Format3rc, 13),
    INVOKE_INTERFACE_RANGE(120, "invoke-interface/range", 3, C1092.Format3rc, 13),
    NEG_INT(123, "neg-int", 7, C1092.Format12x, 20),
    NOT_INT(124, "not-int", 7, C1092.Format12x, 20),
    NEG_LONG(125, "neg-long", 7, C1092.Format12x, 52),
    NOT_LONG(126, "not-long", 7, C1092.Format12x, 52),
    NEG_FLOAT(127, "neg-float", 7, C1092.Format12x, 20),
    NEG_DOUBLE(128, "neg-double", 7, C1092.Format12x, 52),
    INT_TO_LONG(129, "int-to-long", 7, C1092.Format12x, 52),
    INT_TO_FLOAT(130, "int-to-float", 7, C1092.Format12x, 20),
    INT_TO_DOUBLE(131, "int-to-double", 7, C1092.Format12x, 52),
    LONG_TO_INT(132, "long-to-int", 7, C1092.Format12x, 20),
    LONG_TO_FLOAT(133, "long-to-float", 7, C1092.Format12x, 20),
    LONG_TO_DOUBLE(134, "long-to-double", 7, C1092.Format12x, 52),
    FLOAT_TO_INT(135, "float-to-int", 7, C1092.Format12x, 20),
    FLOAT_TO_LONG(136, "float-to-long", 7, C1092.Format12x, 52),
    FLOAT_TO_DOUBLE(137, "float-to-double", 7, C1092.Format12x, 52),
    DOUBLE_TO_INT(138, "double-to-int", 7, C1092.Format12x, 20),
    DOUBLE_TO_LONG(139, "double-to-long", 7, C1092.Format12x, 52),
    DOUBLE_TO_FLOAT(140, "double-to-float", 7, C1092.Format12x, 20),
    INT_TO_BYTE(141, "int-to-byte", 7, C1092.Format12x, 20),
    INT_TO_CHAR(142, "int-to-char", 7, C1092.Format12x, 20),
    INT_TO_SHORT(143, "int-to-short", 7, C1092.Format12x, 20),
    ADD_INT(144, "add-int", 7, C1092.Format23x, 20),
    SUB_INT(145, "sub-int", 7, C1092.Format23x, 20),
    MUL_INT(146, "mul-int", 7, C1092.Format23x, 20),
    DIV_INT(147, "div-int", 7, C1092.Format23x, 21),
    REM_INT(148, "rem-int", 7, C1092.Format23x, 21),
    AND_INT(149, "and-int", 7, C1092.Format23x, 20),
    OR_INT(150, "or-int", 7, C1092.Format23x, 20),
    XOR_INT(151, "xor-int", 7, C1092.Format23x, 20),
    SHL_INT(152, "shl-int", 7, C1092.Format23x, 20),
    SHR_INT(153, "shr-int", 7, C1092.Format23x, 20),
    USHR_INT(154, "ushr-int", 7, C1092.Format23x, 20),
    ADD_LONG(155, "add-long", 7, C1092.Format23x, 52),
    SUB_LONG(156, "sub-long", 7, C1092.Format23x, 52),
    MUL_LONG(157, "mul-long", 7, C1092.Format23x, 52),
    DIV_LONG(158, "div-long", 7, C1092.Format23x, 53),
    REM_LONG(159, "rem-long", 7, C1092.Format23x, 53),
    AND_LONG(160, "and-long", 7, C1092.Format23x, 52),
    OR_LONG(161, "or-long", 7, C1092.Format23x, 52),
    XOR_LONG(162, "xor-long", 7, C1092.Format23x, 52),
    SHL_LONG(163, "shl-long", 7, C1092.Format23x, 52),
    SHR_LONG(164, "shr-long", 7, C1092.Format23x, 52),
    USHR_LONG(165, "ushr-long", 7, C1092.Format23x, 52),
    ADD_FLOAT(166, "add-float", 7, C1092.Format23x, 20),
    SUB_FLOAT(167, "sub-float", 7, C1092.Format23x, 20),
    MUL_FLOAT(168, "mul-float", 7, C1092.Format23x, 20),
    DIV_FLOAT(169, "div-float", 7, C1092.Format23x, 20),
    REM_FLOAT(170, "rem-float", 7, C1092.Format23x, 20),
    ADD_DOUBLE(171, "add-double", 7, C1092.Format23x, 52),
    SUB_DOUBLE(172, "sub-double", 7, C1092.Format23x, 52),
    MUL_DOUBLE(173, "mul-double", 7, C1092.Format23x, 52),
    DIV_DOUBLE(174, "div-double", 7, C1092.Format23x, 52),
    REM_DOUBLE(175, "rem-double", 7, C1092.Format23x, 52),
    ADD_INT_2ADDR(176, "add-int/2addr", 7, C1092.Format12x, 20),
    SUB_INT_2ADDR(177, "sub-int/2addr", 7, C1092.Format12x, 20),
    MUL_INT_2ADDR(178, "mul-int/2addr", 7, C1092.Format12x, 20),
    DIV_INT_2ADDR(179, "div-int/2addr", 7, C1092.Format12x, 21),
    REM_INT_2ADDR(180, "rem-int/2addr", 7, C1092.Format12x, 21),
    AND_INT_2ADDR(181, "and-int/2addr", 7, C1092.Format12x, 20),
    OR_INT_2ADDR(182, "or-int/2addr", 7, C1092.Format12x, 20),
    XOR_INT_2ADDR(183, "xor-int/2addr", 7, C1092.Format12x, 20),
    SHL_INT_2ADDR(184, "shl-int/2addr", 7, C1092.Format12x, 20),
    SHR_INT_2ADDR(185, "shr-int/2addr", 7, C1092.Format12x, 20),
    USHR_INT_2ADDR(186, "ushr-int/2addr", 7, C1092.Format12x, 20),
    ADD_LONG_2ADDR(187, "add-long/2addr", 7, C1092.Format12x, 52),
    SUB_LONG_2ADDR(188, "sub-long/2addr", 7, C1092.Format12x, 52),
    MUL_LONG_2ADDR(189, "mul-long/2addr", 7, C1092.Format12x, 52),
    DIV_LONG_2ADDR(190, "div-long/2addr", 7, C1092.Format12x, 53),
    REM_LONG_2ADDR(191, "rem-long/2addr", 7, C1092.Format12x, 53),
    AND_LONG_2ADDR(192, "and-long/2addr", 7, C1092.Format12x, 52),
    OR_LONG_2ADDR(193, "or-long/2addr", 7, C1092.Format12x, 52),
    XOR_LONG_2ADDR(194, "xor-long/2addr", 7, C1092.Format12x, 52),
    SHL_LONG_2ADDR(195, "shl-long/2addr", 7, C1092.Format12x, 52),
    SHR_LONG_2ADDR(196, "shr-long/2addr", 7, C1092.Format12x, 52),
    USHR_LONG_2ADDR(197, "ushr-long/2addr", 7, C1092.Format12x, 52),
    ADD_FLOAT_2ADDR(198, "add-float/2addr", 7, C1092.Format12x, 20),
    SUB_FLOAT_2ADDR(199, "sub-float/2addr", 7, C1092.Format12x, 20),
    MUL_FLOAT_2ADDR(200, "mul-float/2addr", 7, C1092.Format12x, 20),
    DIV_FLOAT_2ADDR(201, "div-float/2addr", 7, C1092.Format12x, 20),
    REM_FLOAT_2ADDR(202, "rem-float/2addr", 7, C1092.Format12x, 20),
    ADD_DOUBLE_2ADDR(203, "add-double/2addr", 7, C1092.Format12x, 52),
    SUB_DOUBLE_2ADDR(204, "sub-double/2addr", 7, C1092.Format12x, 52),
    MUL_DOUBLE_2ADDR(205, "mul-double/2addr", 7, C1092.Format12x, 52),
    DIV_DOUBLE_2ADDR(206, "div-double/2addr", 7, C1092.Format12x, 52),
    REM_DOUBLE_2ADDR(207, "rem-double/2addr", 7, C1092.Format12x, 52),
    ADD_INT_LIT16(208, "add-int/lit16", 7, C1092.Format22s, 20),
    RSUB_INT(209, "rsub-int", 7, C1092.Format22s, 20),
    MUL_INT_LIT16(210, "mul-int/lit16", 7, C1092.Format22s, 20),
    DIV_INT_LIT16(211, "div-int/lit16", 7, C1092.Format22s, 21),
    REM_INT_LIT16(212, "rem-int/lit16", 7, C1092.Format22s, 21),
    AND_INT_LIT16(213, "and-int/lit16", 7, C1092.Format22s, 20),
    OR_INT_LIT16(214, "or-int/lit16", 7, C1092.Format22s, 20),
    XOR_INT_LIT16(215, "xor-int/lit16", 7, C1092.Format22s, 20),
    ADD_INT_LIT8(216, "add-int/lit8", 7, C1092.Format22b, 20),
    RSUB_INT_LIT8(217, "rsub-int/lit8", 7, C1092.Format22b, 20),
    MUL_INT_LIT8(218, "mul-int/lit8", 7, C1092.Format22b, 20),
    DIV_INT_LIT8(219, "div-int/lit8", 7, C1092.Format22b, 21),
    REM_INT_LIT8(220, "rem-int/lit8", 7, C1092.Format22b, 21),
    AND_INT_LIT8(221, "and-int/lit8", 7, C1092.Format22b, 20),
    OR_INT_LIT8(222, "or-int/lit8", 7, C1092.Format22b, 20),
    XOR_INT_LIT8(223, "xor-int/lit8", 7, C1092.Format22b, 20),
    SHL_INT_LIT8(224, "shl-int/lit8", 7, C1092.Format22b, 20),
    SHR_INT_LIT8(225, "shr-int/lit8", 7, C1092.Format22b, 20),
    USHR_INT_LIT8(226, "ushr-int/lit8", 7, C1092.Format22b, 20),
    IGET_VOLATILE(m6992(227, 9), "iget-volatile", 2, C1092.Format22c, 151),
    IPUT_VOLATILE(m6992(228, 9), "iput-volatile", 2, C1092.Format22c, 135),
    SGET_VOLATILE(m6992(229, 9), "sget-volatile", 2, C1092.Format21c, 407),
    SPUT_VOLATILE(m6992(230, 9), "sput-volatile", 2, C1092.Format21c, 391),
    IGET_OBJECT_VOLATILE(m6992(231, 9), "iget-object-volatile", 2, C1092.Format22c, 151),
    IGET_WIDE_VOLATILE(m6992(232, 9), "iget-wide-volatile", 2, C1092.Format22c, 183),
    IPUT_WIDE_VOLATILE(m6992(233, 9), "iput-wide-volatile", 2, C1092.Format22c, 135),
    SGET_WIDE_VOLATILE(m6992(234, 9), "sget-wide-volatile", 2, C1092.Format21c, 439),
    SPUT_WIDE_VOLATILE(m6992(235, 9), "sput-wide-volatile", 2, C1092.Format21c, 391),
    THROW_VERIFICATION_ERROR(m6992(237, 5), "throw-verification-error", 7, C1092.Format20bc, 3),
    EXECUTE_INLINE(m6995(238), "execute-inline", 7, C1092.Format35mi, 15),
    EXECUTE_INLINE_RANGE(m6992(239, 8), "execute-inline/range", 7, C1092.Format3rmi, 15),
    INVOKE_DIRECT_EMPTY(m6996(240, 13), "invoke-direct-empty", 3, C1092.Format35c, 1039),
    INVOKE_OBJECT_INIT_RANGE(m6992(240, 14), "invoke-object-init/range", 3, C1092.Format3rc, 1039),
    RETURN_VOID_BARRIER(m6994(m6992(241, 11), m6999(115, 59)), "return-void-barrier", 7, C1092.Format10x, 2),
    RETURN_VOID_NO_BARRIER(m6998(115, 60), "return-void-no-barrier", 7, C1092.Format10x, 2),
    IGET_QUICK(m6994(m6995(242), m6997(227)), "iget-quick", 7, C1092.Format22cs, 87),
    IGET_WIDE_QUICK(m6994(m6995(243), m6997(228)), "iget-wide-quick", 7, C1092.Format22cs, 119),
    IGET_OBJECT_QUICK(m6994(m6995(244), m6997(229)), "iget-object-quick", 7, C1092.Format22cs, 87),
    IPUT_QUICK(m6994(m6995(245), m6997(230)), "iput-quick", 7, C1092.Format22cs, 71),
    IPUT_WIDE_QUICK(m6994(m6995(246), m6997(231)), "iput-wide-quick", 7, C1092.Format22cs, 71),
    IPUT_OBJECT_QUICK(m6994(m6995(247), m6997(232)), "iput-object-quick", 7, C1092.Format22cs, 71),
    IPUT_BOOLEAN_QUICK(m6997(235), "iput-boolean-quick", 7, C1092.Format22cs, 71),
    IPUT_BYTE_QUICK(m6997(236), "iput-byte-quick", 7, C1092.Format22cs, 71),
    IPUT_CHAR_QUICK(m6997(237), "iput-char-quick", 7, C1092.Format22cs, 71),
    IPUT_SHORT_QUICK(m6997(238), "iput-short-quick", 7, C1092.Format22cs, 71),
    IGET_BOOLEAN_QUICK(m6997(239), "iget-boolean-quick", 7, C1092.Format22cs, 87),
    IGET_BYTE_QUICK(m6997(240), "iget-byte-quick", 7, C1092.Format22cs, 87),
    IGET_CHAR_QUICK(m6997(241), "iget-char-quick", 7, C1092.Format22cs, 87),
    IGET_SHORT_QUICK(m6997(242), "iget-short-quick", 7, C1092.Format22cs, 87),
    INVOKE_VIRTUAL_QUICK(m6994(m6995(248), m6997(233)), "invoke-virtual-quick", 7, C1092.Format35ms, 15),
    INVOKE_VIRTUAL_QUICK_RANGE(m6994(m6995(249), m6997(234)), "invoke-virtual-quick/range", 7, C1092.Format3rms, 15),
    INVOKE_SUPER_QUICK(m6996(250, 25), "invoke-super-quick", 7, C1092.Format35ms, 15),
    INVOKE_SUPER_QUICK_RANGE(m6996(251, 25), "invoke-super-quick/range", 7, C1092.Format3rms, 15),
    IPUT_OBJECT_VOLATILE(m6992(252, 9), "iput-object-volatile", 2, C1092.Format22c, 135),
    SGET_OBJECT_VOLATILE(m6992(253, 9), "sget-object-volatile", 2, C1092.Format21c, 407),
    SPUT_OBJECT_VOLATILE(m6993(254, 9, 19), "sput-object-volatile", 2, C1092.Format21c, 391),
    PACKED_SWITCH_PAYLOAD(256, "packed-switch-payload", 7, C1092.PackedSwitchPayload, 0),
    SPARSE_SWITCH_PAYLOAD(512, "sparse-switch-payload", 7, C1092.SparseSwitchPayload, 0),
    ARRAY_PAYLOAD(768, "array-payload", 7, C1092.ArrayPayload, 0),
    INVOKE_POLYMORPHIC(m6998(250, 87), "invoke-polymorphic", 3, 4, C1092.Format45cc, 13),
    INVOKE_POLYMORPHIC_RANGE(m6998(251, 87), "invoke-polymorphic/range", 3, 4, C1092.Format4rcc, 13),
    INVOKE_CUSTOM(m6998(252, 111), "invoke-custom", 5, C1092.Format35c, 13),
    INVOKE_CUSTOM_RANGE(m6998(253, 111), "invoke-custom/range", 5, C1092.Format3rc, 13),
    CONST_METHOD_HANDLE(m6998(254, 134), "const-method-handle", 6, C1092.Format21c, 21),
    CONST_METHOD_TYPE(m6998(255, 134), "const-method-type", 4, C1092.Format21c, 21);
    

    /* renamed from: ˊﹶ  reason: contains not printable characters */
    public final C0866<Integer, Short> f7073;

    /* renamed from: ˊﾞ  reason: contains not printable characters */
    public final C0866<Integer, Short> f7074;

    /* renamed from: ˋʻ  reason: contains not printable characters */
    public final String f7075;

    /* renamed from: ˋʼ  reason: contains not printable characters */
    public final int f7076;

    /* renamed from: ˋʽ  reason: contains not printable characters */
    public final C1092 f7077;

    /* renamed from: ˋʾ  reason: contains not printable characters */
    public final int f7078;

    /* renamed from: ˋʿ  reason: contains not printable characters */
    public final int f7079;

    private C1185(int i, String str, int i2, C1092 r14) {
        this(r9, r10, i, str, i2, r14, 0);
    }

    private C1185(int i, String str, int i2, C1092 r14, int i3) {
        this(r9, r10, m6991(i), str, i2, r14, i3);
    }

    private C1185(List<C1186> list, String str, int i, C1092 r15, int i2) {
        this(r10, r11, list, str, i, -1, r15, i2);
    }

    private C1185(List<C1186> list, String str, int i, int i2, C1092 r10, int i3) {
        C0901.C0902 r4 = C0901.m5641();
        C0901.C0902 r5 = C0901.m5641();
        for (C1186 next : list) {
            if (!next.f7080.m5467()) {
                r4.m5645(next.f7080, Short.valueOf((short) next.f7082));
            }
            if (!next.f7081.m5467()) {
                r5.m5645(next.f7081, Short.valueOf((short) next.f7082));
            }
        }
        this.f7073 = r4.m5646();
        this.f7074 = r5.m5646();
        this.f7075 = str;
        this.f7076 = i;
        this.f7079 = i2;
        this.f7077 = r10;
        this.f7078 = i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ʾʾ.ʼ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C>
     arg types: [int, int]
     candidates:
      com.google.ʻ.ʼ.ʾʾ.ʼ(com.google.ʻ.ʼ.ˊ<?>, com.google.ʻ.ʼ.ˊ<?>):java.lang.String
      com.google.ʻ.ʼ.ʾʾ.ʼ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C> */
    /* renamed from: ʻ  reason: contains not printable characters */
    private static List<C1186> m6992(int i, int i2) {
        return C0926.m5779(new C1186(C0860.m5458(Integer.valueOf(i2)), C0860.m5459((Comparable) 0, (Comparable) 0), i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ʾʾ.ʼ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C>
     arg types: [int, int]
     candidates:
      com.google.ʻ.ʼ.ʾʾ.ʼ(com.google.ʻ.ʼ.ˊ<?>, com.google.ʻ.ʼ.ˊ<?>):java.lang.String
      com.google.ʻ.ʼ.ʾʾ.ʼ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C> */
    /* renamed from: ʼ  reason: contains not printable characters */
    private static List<C1186> m6996(int i, int i2) {
        return C0926.m5779(new C1186(C0860.m5455((Comparable) Integer.valueOf(i2)), C0860.m5459((Comparable) 0, (Comparable) 0), i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ʾʾ.ʼ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C>
     arg types: [int, int]
     candidates:
      com.google.ʻ.ʼ.ʾʾ.ʼ(com.google.ʻ.ʼ.ˊ<?>, com.google.ʻ.ʼ.ˊ<?>):java.lang.String
      com.google.ʻ.ʼ.ʾʾ.ʼ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C> */
    /* renamed from: ʻ  reason: contains not printable characters */
    private static List<C1186> m6993(int i, int i2, int i3) {
        return C0926.m5779(new C1186(C0860.m5456(Integer.valueOf(i2), Integer.valueOf(i3)), C0860.m5459((Comparable) 0, (Comparable) 0), i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ʾʾ.ʼ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C>
     arg types: [int, int]
     candidates:
      com.google.ʻ.ʼ.ʾʾ.ʼ(com.google.ʻ.ʼ.ˊ<?>, com.google.ʻ.ʼ.ˊ<?>):java.lang.String
      com.google.ʻ.ʼ.ʾʾ.ʼ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C> */
    /* renamed from: ʽ  reason: contains not printable characters */
    private static List<C1186> m6998(int i, int i2) {
        return C0926.m5779(new C1186(C0860.m5459((Comparable) 0, (Comparable) 0), C0860.m5458(Integer.valueOf(i2)), i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ʾʾ.ʼ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C>
     arg types: [int, int]
     candidates:
      com.google.ʻ.ʼ.ʾʾ.ʼ(com.google.ʻ.ʼ.ˊ<?>, com.google.ʻ.ʼ.ˊ<?>):java.lang.String
      com.google.ʻ.ʼ.ʾʾ.ʼ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C> */
    /* renamed from: ʾ  reason: contains not printable characters */
    private static List<C1186> m6999(int i, int i2) {
        return C0926.m5779(new C1186(C0860.m5459((Comparable) 0, (Comparable) 0), C0860.m5455((Comparable) Integer.valueOf(i2)), i));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static List<C1186> m6991(int i) {
        return C0926.m5779(new C1186(C0860.m5462(), C0860.m5462(), i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ʾʾ.ʼ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C>
     arg types: [int, int]
     candidates:
      com.google.ʻ.ʼ.ʾʾ.ʼ(com.google.ʻ.ʼ.ˊ<?>, com.google.ʻ.ʼ.ˊ<?>):java.lang.String
      com.google.ʻ.ʼ.ʾʾ.ʼ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C> */
    /* renamed from: ʼ  reason: contains not printable characters */
    private static List<C1186> m6995(int i) {
        return C0926.m5779(new C1186(C0860.m5462(), C0860.m5459((Comparable) 0, (Comparable) 0), i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ʾʾ.ʼ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C>
     arg types: [int, int]
     candidates:
      com.google.ʻ.ʼ.ʾʾ.ʼ(com.google.ʻ.ʼ.ˊ<?>, com.google.ʻ.ʼ.ˊ<?>):java.lang.String
      com.google.ʻ.ʼ.ʾʾ.ʼ(java.lang.Comparable, java.lang.Comparable):com.google.ʻ.ʼ.ʾʾ<C> */
    /* renamed from: ʽ  reason: contains not printable characters */
    private static List<C1186> m6997(int i) {
        return C0926.m5779(new C1186(C0860.m5459((Comparable) 0, (Comparable) 0), C0860.m5462(), i));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static List<C1186> m6994(List<C1186>... listArr) {
        ArrayList r0 = C0926.m5776();
        for (List<C1186> addAll : listArr) {
            r0.addAll(addAll);
        }
        return r0;
    }

    /* renamed from: org.ʻ.ʻ.ʾ$ʻ  reason: contains not printable characters */
    /* compiled from: Opcode */
    private static class C1186 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public final C0860<Integer> f7080;

        /* renamed from: ʼ  reason: contains not printable characters */
        public final C0860<Integer> f7081;

        /* renamed from: ʽ  reason: contains not printable characters */
        public final int f7082;

        public C1186(C0860<Integer> r1, C0860<Integer> r2, int i) {
            this.f7080 = r1;
            this.f7081 = r2;
            this.f7082 = i;
        }
    }
}
