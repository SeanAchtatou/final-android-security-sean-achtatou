package org.ʻ.ʻ;

/* renamed from: org.ʻ.ʻ.ˉ  reason: contains not printable characters */
/* compiled from: VersionMap */
public class C1399 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m7783(int i) {
        switch (i) {
            case 35:
                return 23;
            case 36:
            default:
                return -1;
            case 37:
                return 25;
            case 38:
                return 27;
            case 39:
                return 28;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int m7784(int i) {
        if (i <= 23) {
            return 35;
        }
        if (i <= 25) {
            return 37;
        }
        return i <= 27 ? 38 : 39;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static int m7785(int i) {
        if (i >= 138) {
            return 28;
        }
        if (i >= 131) {
            return 27;
        }
        if (i >= 124) {
            return 26;
        }
        if (i >= 79) {
            return 24;
        }
        if (i >= 64) {
            return 23;
        }
        if (i >= 45) {
            return 22;
        }
        return i >= 39 ? 21 : 19;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static int m7786(int i) {
        if (i < 19) {
            return -1;
        }
        switch (i) {
            case 19:
            case 20:
                return 7;
            case 21:
                return 39;
            case 22:
                return 45;
            case 23:
                return 64;
            case 24:
            case 25:
                return 79;
            case 26:
                return 124;
            case 27:
                return 131;
            default:
                return 144;
        }
    }
}
