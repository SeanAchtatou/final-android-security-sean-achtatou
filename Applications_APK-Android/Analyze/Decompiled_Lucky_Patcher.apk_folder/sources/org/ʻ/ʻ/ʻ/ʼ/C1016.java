package org.ʻ.ʻ.ʻ.ʼ;

import com.google.ʻ.ʿ.C0934;
import com.google.ʻ.ʿ.C0935;
import org.ʻ.ʻ.ʾ.ʾ.C1271;
import org.ʻ.ʻ.ʾ.ʾ.C1273;

/* renamed from: org.ʻ.ʻ.ʻ.ʼ.ʿ  reason: contains not printable characters */
/* compiled from: BaseCharEncodedValue */
public abstract class C1016 implements C1271 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6310() {
        return 3;
    }

    public int hashCode() {
        return m7095();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C1271) || m7095() != ((C1271) obj).m7095()) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1273 r3) {
        int r0 = C0935.m5810(m6310(), r3.m7098());
        if (r0 != 0) {
            return r0;
        }
        return C0934.m5809(m7095(), ((C1271) r3).m7095());
    }
}
