package org.ʻ.ʻ.ʻ.ʻ;

import org.ʻ.ʻ.ʾ.ʽ.C1264;

/* renamed from: org.ʻ.ʻ.ʻ.ʻ.ˈ  reason: contains not printable characters */
/* compiled from: BaseStringReference */
public abstract class C1009 extends C1008 implements C1264 {
    public int hashCode() {
        return m7080().hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C1264)) {
            return false;
        }
        return m7080().equals(((C1264) obj).m7080());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(CharSequence charSequence) {
        return m7080().compareTo(charSequence.toString());
    }

    public int length() {
        return m7080().length();
    }

    public char charAt(int i) {
        return m7080().charAt(i);
    }

    public CharSequence subSequence(int i, int i2) {
        return m7080().subSequence(i, i2);
    }

    public String toString() {
        return m7080();
    }
}
