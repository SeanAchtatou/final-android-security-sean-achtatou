package org.ʻ.ʻ.ʻ;

import com.google.ʻ.ʻ.C0845;
import com.google.ʻ.ʿ.C0935;
import java.util.Comparator;
import org.ʻ.ʻ.ʻ.ʻ.C1010;
import org.ʻ.ʻ.ʾ.C1286;
import org.ʻ.ʻ.ʾ.ʽ.C1265;

/* renamed from: org.ʻ.ʻ.ʻ.ʽ  reason: contains not printable characters */
/* compiled from: BaseExceptionHandler */
public abstract class C1030 implements C1286 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final Comparator<C1286> f6319 = new Comparator<C1286>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public int compare(C1286 r2, C1286 r3) {
            String r22 = r2.m7124();
            if (r22 == null) {
                return r3.m7124() != null ? 1 : 0;
            }
            if (r3.m7124() == null) {
                return -1;
            }
            return r22.compareTo(r3.m7124());
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    public C1265 m6339() {
        final String r0 = m7124();
        if (r0 == null) {
            return null;
        }
        return new C1010() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public String m6340() {
                return r0;
            }
        };
    }

    public int hashCode() {
        int i;
        String r0 = m7124();
        if (r0 == null) {
            i = 0;
        } else {
            i = r0.hashCode();
        }
        return (i * 31) + m7125();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C1286)) {
            return false;
        }
        C1286 r4 = (C1286) obj;
        if (!C0845.m5413(m7124(), r4.m7124()) || m7125() != r4.m7125()) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1286 r3) {
        String r0 = m7124();
        if (r0 == null) {
            if (r3.m7124() != null) {
                return 1;
            }
        } else if (r3.m7124() == null) {
            return -1;
        } else {
            int compareTo = r0.compareTo(r3.m7124());
            if (compareTo != 0) {
                return compareTo;
            }
        }
        return C0935.m5810(m7125(), r3.m7125());
    }
}
