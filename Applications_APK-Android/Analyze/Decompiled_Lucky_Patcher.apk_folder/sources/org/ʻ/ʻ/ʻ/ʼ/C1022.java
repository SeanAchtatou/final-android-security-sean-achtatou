package org.ʻ.ʻ.ʻ.ʼ;

import com.google.ʻ.ʿ.C0935;
import com.google.ʻ.ʿ.C0936;
import org.ʻ.ʻ.ʾ.ʾ.C1273;
import org.ʻ.ʻ.ʾ.ʾ.C1278;

/* renamed from: org.ʻ.ʻ.ʻ.ʼ.ˎ  reason: contains not printable characters */
/* compiled from: BaseLongEncodedValue */
public abstract class C1022 implements C1278 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6322() {
        return 6;
    }

    public int hashCode() {
        long r0 = m7108();
        return (((int) r0) * 31) + ((int) (r0 >>> 32));
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C1278) || m7108() != ((C1278) obj).m7108()) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1273 r5) {
        int r0 = C0935.m5810(m6322(), r5.m7098());
        if (r0 != 0) {
            return r0;
        }
        return C0936.m5812(m7108(), ((C1278) r5).m7108());
    }
}
