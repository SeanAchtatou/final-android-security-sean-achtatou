package org.ʻ.ʻ.ʻ.ʻ;

import org.ʻ.ʻ.ʾ.ʽ.C1259;
import org.ʻ.ʻ.ˆ.C1338;

/* renamed from: org.ʻ.ʻ.ʻ.ʻ.ʼ  reason: contains not printable characters */
/* compiled from: BaseFieldReference */
public abstract class C1004 extends C1008 implements C1259 {
    public int hashCode() {
        return (((m7067().hashCode() * 31) + m7065().hashCode()) * 31) + m7066().hashCode();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C1259)) {
            return false;
        }
        C1259 r4 = (C1259) obj;
        if (!m7067().equals(r4.m7067()) || !m7065().equals(r4.m7065()) || !m7066().equals(r4.m7066())) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1259 r3) {
        int compareTo = m7067().compareTo(r3.m7067());
        if (compareTo != 0) {
            return compareTo;
        }
        int compareTo2 = m7065().compareTo(r3.m7065());
        if (compareTo2 != 0) {
            return compareTo2;
        }
        return m7066().compareTo(r3.m7066());
    }

    public String toString() {
        return C1338.m7301(this);
    }
}
