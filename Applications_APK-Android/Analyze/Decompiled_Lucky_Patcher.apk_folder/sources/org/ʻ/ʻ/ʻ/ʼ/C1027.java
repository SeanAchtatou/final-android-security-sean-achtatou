package org.ʻ.ʻ.ʻ.ʼ;

import com.google.ʻ.ʿ.C0935;
import com.google.ʻ.ʿ.C0937;
import org.ʻ.ʻ.ʾ.ʾ.C1273;
import org.ʻ.ʻ.ʾ.ʾ.C1283;

/* renamed from: org.ʻ.ʻ.ʻ.ʼ.ٴ  reason: contains not printable characters */
/* compiled from: BaseShortEncodedValue */
public abstract class C1027 implements C1283 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6332() {
        return 2;
    }

    public int hashCode() {
        return m7117();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C1283) || m7117() != ((C1283) obj).m7117()) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1273 r3) {
        int r0 = C0935.m5810(m6332(), r3.m7098());
        if (r0 != 0) {
            return r0;
        }
        return C0937.m5813(m7117(), ((C1283) r3).m7117());
    }
}
