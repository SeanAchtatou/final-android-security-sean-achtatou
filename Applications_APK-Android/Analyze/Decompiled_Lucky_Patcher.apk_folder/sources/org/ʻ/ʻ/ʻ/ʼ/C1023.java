package org.ʻ.ʻ.ʻ.ʼ;

import com.google.ʻ.ʿ.C0935;
import org.ʻ.ʻ.ʾ.ʾ.C1273;
import org.ʻ.ʻ.ʾ.ʾ.C1279;

/* renamed from: org.ʻ.ʻ.ʻ.ʼ.ˏ  reason: contains not printable characters */
/* compiled from: BaseMethodEncodedValue */
public abstract class C1023 implements C1279 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6324() {
        return 26;
    }

    public int hashCode() {
        return m7110().hashCode();
    }

    public boolean equals(Object obj) {
        if (obj instanceof C1279) {
            return m7110().equals(((C1279) obj).m7110());
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1273 r3) {
        int r0 = C0935.m5810(m6324(), r3.m7098());
        if (r0 != 0) {
            return r0;
        }
        return m7110().m7074(((C1279) r3).m7110());
    }
}
