package org.ʻ.ʻ.ʻ.ʼ;

import com.google.ʻ.ʿ.C0935;
import org.ʻ.ʻ.ʾ.ʾ.C1272;
import org.ʻ.ʻ.ʾ.ʾ.C1273;

/* renamed from: org.ʻ.ʻ.ʻ.ʼ.ˆ  reason: contains not printable characters */
/* compiled from: BaseDoubleEncodedValue */
public abstract class C1017 implements C1272 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6312() {
        return 17;
    }

    public int hashCode() {
        long doubleToRawLongBits = Double.doubleToRawLongBits(m7097());
        return (int) (doubleToRawLongBits ^ (doubleToRawLongBits >>> 32));
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C1272) || Double.doubleToRawLongBits(m7097()) != Double.doubleToRawLongBits(((C1272) obj).m7097())) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1273 r5) {
        int r0 = C0935.m5810(m6312(), r5.m7098());
        if (r0 != 0) {
            return r0;
        }
        return Double.compare(m7097(), ((C1272) r5).m7097());
    }
}
