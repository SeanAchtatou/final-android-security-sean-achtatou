package org.ʻ.ʻ.ʻ.ʼ;

import com.google.ʻ.ʿ.C0933;
import com.google.ʻ.ʿ.C0935;
import org.ʻ.ʻ.ʾ.ʾ.C1269;
import org.ʻ.ʻ.ʾ.ʾ.C1273;

/* renamed from: org.ʻ.ʻ.ʻ.ʼ.ʽ  reason: contains not printable characters */
/* compiled from: BaseBooleanEncodedValue */
public abstract class C1014 implements C1269 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6306() {
        return 31;
    }

    public int hashCode() {
        return m7091() ? 1 : 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C1269) || m7091() != ((C1269) obj).m7091()) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1273 r3) {
        int r0 = C0935.m5810(m6306(), r3.m7098());
        if (r0 != 0) {
            return r0;
        }
        return C0933.m5808(m7091(), ((C1269) r3).m7091());
    }
}
