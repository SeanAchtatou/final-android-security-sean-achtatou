package org.ʻ.ʻ.ʻ.ʻ;

import com.google.ʻ.ʼ.C0858;
import org.ʻ.ʻ.ʾ.ʽ.C1262;
import org.ʻ.ʻ.ˆ.C1338;
import org.ʻ.ʼ.C1402;
import org.ʻ.ʼ.C1403;

/* renamed from: org.ʻ.ʻ.ʻ.ʻ.ʿ  reason: contains not printable characters */
/* compiled from: BaseMethodReference */
public abstract class C1007 extends C1008 implements C1262 {
    public int hashCode() {
        return (((((m7075().hashCode() * 31) + m7076().hashCode()) * 31) + m7077().hashCode()) * 31) + m7078().hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C1262)) {
            return false;
        }
        C1262 r4 = (C1262) obj;
        if (!m7075().equals(r4.m7075()) || !m7076().equals(r4.m7076()) || !m7077().equals(r4.m7077()) || !C1402.m7790(m7078(), r4.m7078())) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1262 r3) {
        int compareTo = m7075().compareTo(r3.m7075());
        if (compareTo != 0) {
            return compareTo;
        }
        int compareTo2 = m7076().compareTo(r3.m7076());
        if (compareTo2 != 0) {
            return compareTo2;
        }
        int compareTo3 = m7077().compareTo(r3.m7077());
        if (compareTo3 != 0) {
            return compareTo3;
        }
        return C1403.m7793(C0858.m5448(), m7078(), r3.m7078());
    }

    public String toString() {
        return C1338.m7304(this);
    }
}
