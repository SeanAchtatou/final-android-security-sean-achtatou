package org.ʻ.ʻ.ʻ.ʼ;

import com.google.ʻ.ʿ.C0935;
import org.ʻ.ʻ.ʾ.ʾ.C1273;
import org.ʻ.ʻ.ʾ.ʾ.C1285;

/* renamed from: org.ʻ.ʻ.ʻ.ʼ.ᴵ  reason: contains not printable characters */
/* compiled from: BaseTypeEncodedValue */
public abstract class C1029 implements C1285 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6336() {
        return 24;
    }

    public int hashCode() {
        return m7121().hashCode();
    }

    public boolean equals(Object obj) {
        if (obj instanceof C1285) {
            return m7121().equals(((C1285) obj).m7121());
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1273 r3) {
        int r0 = C0935.m5810(m6336(), r3.m7098());
        if (r0 != 0) {
            return r0;
        }
        return m7121().compareTo(((C1285) r3).m7121());
    }
}
