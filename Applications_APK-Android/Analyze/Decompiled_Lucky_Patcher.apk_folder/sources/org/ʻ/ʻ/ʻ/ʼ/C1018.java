package org.ʻ.ʻ.ʻ.ʼ;

import com.google.ʻ.ʿ.C0935;
import org.ʻ.ʻ.ʾ.ʾ.C1273;
import org.ʻ.ʻ.ʾ.ʾ.C1274;

/* renamed from: org.ʻ.ʻ.ʻ.ʼ.ˈ  reason: contains not printable characters */
/* compiled from: BaseEnumEncodedValue */
public abstract class C1018 implements C1274 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6314() {
        return 27;
    }

    public int hashCode() {
        return m7100().hashCode();
    }

    public boolean equals(Object obj) {
        if (obj instanceof C1274) {
            return m7100().equals(((C1274) obj).m7100());
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1273 r3) {
        int r0 = C0935.m5810(m6314(), r3.m7098());
        if (r0 != 0) {
            return r0;
        }
        return m7100().m7064(((C1274) r3).m7100());
    }
}
