package org.ʻ.ʻ.ʻ.ʻ;

import org.ʻ.ʻ.ʾ.ʽ.C1265;

/* renamed from: org.ʻ.ʻ.ʻ.ʻ.ˉ  reason: contains not printable characters */
/* compiled from: BaseTypeReference */
public abstract class C1010 extends C1008 implements C1265 {
    public int hashCode() {
        return m7082().hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof C1265) {
            return m7082().equals(((C1265) obj).m7082());
        }
        if (obj instanceof CharSequence) {
            return m7082().equals(obj.toString());
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(CharSequence charSequence) {
        return m7082().compareTo(charSequence.toString());
    }

    public int length() {
        return m7082().length();
    }

    public char charAt(int i) {
        return m7082().charAt(i);
    }

    public CharSequence subSequence(int i, int i2) {
        return m7082().subSequence(i, i2);
    }

    public String toString() {
        return m7082();
    }
}
