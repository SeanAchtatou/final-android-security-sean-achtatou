package org.ʻ.ʻ.ʻ.ʼ;

import com.google.ʻ.ʿ.C0935;
import org.ʻ.ʻ.ʾ.ʾ.C1273;
import org.ʻ.ʻ.ʾ.ʾ.C1277;

/* renamed from: org.ʻ.ʻ.ʻ.ʼ.ˋ  reason: contains not printable characters */
/* compiled from: BaseIntEncodedValue */
public abstract class C1021 implements C1277 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6320() {
        return 4;
    }

    public int hashCode() {
        return m7106();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C1277) || m7106() != ((C1277) obj).m7106()) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1273 r3) {
        int r0 = C0935.m5810(m6320(), r3.m7098());
        if (r0 != 0) {
            return r0;
        }
        return C0935.m5810(m7106(), ((C1277) r3).m7106());
    }
}
