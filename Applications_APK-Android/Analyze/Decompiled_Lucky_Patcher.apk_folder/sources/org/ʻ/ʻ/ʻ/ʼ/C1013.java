package org.ʻ.ʻ.ʻ.ʼ;

import com.google.ʻ.ʿ.C0935;
import org.ʻ.ʻ.ʾ.ʾ.C1268;
import org.ʻ.ʻ.ʾ.ʾ.C1273;
import org.ʻ.ʼ.C1403;

/* renamed from: org.ʻ.ʻ.ʻ.ʼ.ʼ  reason: contains not printable characters */
/* compiled from: BaseArrayEncodedValue */
public abstract class C1013 implements C1268 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6304() {
        return 28;
    }

    public int hashCode() {
        return m7089().hashCode();
    }

    public boolean equals(Object obj) {
        if (obj instanceof C1268) {
            return m7089().equals(((C1268) obj).m7089());
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1273 r3) {
        int r0 = C0935.m5810(m6304(), r3.m7098());
        if (r0 != 0) {
            return r0;
        }
        return C1403.m7792(m7089(), ((C1268) r3).m7089());
    }
}
