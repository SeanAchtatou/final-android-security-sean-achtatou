package org.ʻ.ʻ.ʻ.ʻ;

import com.google.ʻ.ʿ.C0935;
import org.ʻ.ʻ.ʾ.ʽ.C1259;
import org.ʻ.ʻ.ʾ.ʽ.C1260;
import org.ʻ.ʻ.ʾ.ʽ.C1262;
import org.ʻ.ʻ.ʾ.ʽ.C1263;

/* renamed from: org.ʻ.ʻ.ʻ.ʻ.ʽ  reason: contains not printable characters */
/* compiled from: BaseMethodHandleReference */
public abstract class C1005 extends C1008 implements C1260 {
    public int hashCode() {
        return (m7068() * 31) + m7070().hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C1260)) {
            return false;
        }
        C1260 r4 = (C1260) obj;
        if (m7068() != r4.m7068() || !m7070().equals(r4.m7070())) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1260 r3) {
        int r0 = C0935.m5810(m7068(), r3.m7068());
        if (r0 != 0) {
            return r0;
        }
        C1263 r02 = m7070();
        if (r02 instanceof C1259) {
            if (!(r3.m7070() instanceof C1259)) {
                return -1;
            }
            return ((C1259) r02).m7064((C1259) r3.m7070());
        } else if (!(r3.m7070() instanceof C1262)) {
            return 1;
        } else {
            return ((C1262) r02).m7074((C1262) r3.m7070());
        }
    }
}
