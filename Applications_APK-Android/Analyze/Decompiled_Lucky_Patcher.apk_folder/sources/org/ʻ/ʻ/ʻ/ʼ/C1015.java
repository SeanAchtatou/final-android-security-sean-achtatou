package org.ʻ.ʻ.ʻ.ʼ;

import com.google.ʻ.ʿ.C0935;
import org.ʻ.ʻ.ʾ.ʾ.C1270;
import org.ʻ.ʻ.ʾ.ʾ.C1273;

/* renamed from: org.ʻ.ʻ.ʻ.ʼ.ʾ  reason: contains not printable characters */
/* compiled from: BaseByteEncodedValue */
public abstract class C1015 implements C1270 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6308() {
        return 0;
    }

    public int hashCode() {
        return m7093();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C1270) || m7093() != ((C1270) obj).m7093()) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1273 r3) {
        int r0 = C0935.m5810(m6308(), r3.m7098());
        if (r0 != 0) {
            return r0;
        }
        return C0935.m5810(m7093(), ((C1270) r3).m7093());
    }
}
