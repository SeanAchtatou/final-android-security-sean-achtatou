package org.ʻ.ʻ.ʻ;

import java.util.Comparator;
import org.ʻ.ʻ.ʾ.C1197;

/* renamed from: org.ʻ.ʻ.ʻ.ʼ  reason: contains not printable characters */
/* compiled from: BaseAnnotationElement */
public abstract class C1011 implements C1197 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final Comparator<C1197> f6318 = new Comparator<C1197>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public int compare(C1197 r1, C1197 r2) {
            return r1.m7019().compareTo(r2.m7019());
        }
    };

    public int hashCode() {
        return (m7019().hashCode() * 31) + m7020().hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C1197)) {
            return false;
        }
        C1197 r4 = (C1197) obj;
        if (!m7019().equals(r4.m7019()) || !m7020().equals(r4.m7020())) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1197 r3) {
        int compareTo = m7019().compareTo(r3.m7019());
        if (compareTo != 0) {
            return compareTo;
        }
        return m7020().compareTo(r3.m7020());
    }
}
