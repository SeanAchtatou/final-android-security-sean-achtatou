package org.ʻ.ʻ.ʻ.ʻ;

import com.google.ʻ.ʼ.C0858;
import org.ʻ.ʻ.ʾ.ʽ.C1261;
import org.ʻ.ʻ.ˆ.C1338;
import org.ʻ.ʼ.C1402;
import org.ʻ.ʼ.C1403;

/* renamed from: org.ʻ.ʻ.ʻ.ʻ.ʾ  reason: contains not printable characters */
/* compiled from: BaseMethodProtoReference */
public abstract class C1006 extends C1008 implements C1261 {
    public int hashCode() {
        return (m7073().hashCode() * 31) + m7072().hashCode();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C1261)) {
            return false;
        }
        C1261 r4 = (C1261) obj;
        if (!m7073().equals(r4.m7073()) || !C1402.m7790(m7072(), r4.m7072())) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1261 r3) {
        int compareTo = m7073().compareTo(r3.m7073());
        if (compareTo != 0) {
            return compareTo;
        }
        return C1403.m7793(C0858.m5448(), m7072(), r3.m7072());
    }

    public String toString() {
        return C1338.m7303(this);
    }
}
