package org.ʻ.ʻ.ʻ;

import java.util.Iterator;
import org.ʻ.ʻ.ʻ.ʻ.C1010;
import org.ʻ.ʻ.ʾ.C1187;
import org.ʻ.ʻ.ʾ.C1197;
import org.ʻ.ʻ.ʾ.C1290;
import org.ʻ.ʻ.ʾ.ʾ.C1268;
import org.ʻ.ʻ.ʾ.ʾ.C1273;
import org.ʻ.ʻ.ʾ.ʾ.C1284;

/* renamed from: org.ʻ.ʻ.ʻ.ʾ  reason: contains not printable characters */
/* compiled from: BaseMethodParameter */
public abstract class C1031 extends C1010 implements C1290 {
    /* renamed from: ʼ  reason: contains not printable characters */
    public String m6342() {
        C1187 r1;
        C1268 r0;
        Iterator<? extends C1187> it = m7141().iterator();
        while (true) {
            if (!it.hasNext()) {
                r1 = null;
                break;
            }
            r1 = (C1187) it.next();
            if (r1.m7002().equals("Ldalvik/annotation/Signature;")) {
                break;
            }
        }
        if (r1 == null) {
            return null;
        }
        Iterator<? extends C1197> it2 = r1.m7003().iterator();
        while (true) {
            if (!it2.hasNext()) {
                r0 = null;
                break;
            }
            C1197 r12 = (C1197) it2.next();
            if (r12.m7019().equals("value")) {
                C1273 r02 = r12.m7020();
                if (r02.m7098() != 28) {
                    return null;
                }
                r0 = (C1268) r02;
            }
        }
        if (r0 == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (C1273 r3 : r0.m7089()) {
            if (r3.m7098() != 23) {
                return null;
            }
            sb.append(((C1284) r3).m7119());
        }
        return sb.toString();
    }
}
