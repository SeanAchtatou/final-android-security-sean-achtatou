package org.ʻ.ʻ.ʻ.ʻ;

import org.ʻ.ʻ.ʾ.ʽ.C1258;

/* renamed from: org.ʻ.ʻ.ʻ.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: BaseCallSiteReference */
public abstract class C1003 extends C1008 implements C1258 {
    public int hashCode() {
        return (((((((m7059().hashCode() * 31) + m7060().hashCode()) * 31) + m7061().hashCode()) * 31) + m7062().hashCode()) * 31) + m7063().hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C1258)) {
            return false;
        }
        C1258 r4 = (C1258) obj;
        if (!m7060().equals(r4.m7060()) || !m7061().equals(r4.m7061()) || !m7062().equals(r4.m7062()) || !m7063().equals(r4.m7063())) {
            return false;
        }
        return true;
    }
}
