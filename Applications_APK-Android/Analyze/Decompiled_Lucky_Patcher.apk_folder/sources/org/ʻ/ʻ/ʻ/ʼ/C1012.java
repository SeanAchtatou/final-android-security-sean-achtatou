package org.ʻ.ʻ.ʻ.ʼ;

import com.google.ʻ.ʿ.C0935;
import org.ʻ.ʻ.ʾ.ʾ.C1267;
import org.ʻ.ʻ.ʾ.ʾ.C1273;
import org.ʻ.ʼ.C1403;

/* renamed from: org.ʻ.ʻ.ʻ.ʼ.ʻ  reason: contains not printable characters */
/* compiled from: BaseAnnotationEncodedValue */
public abstract class C1012 implements C1267 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6302() {
        return 29;
    }

    public int hashCode() {
        return (m7086().hashCode() * 31) + m7087().hashCode();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C1267)) {
            return false;
        }
        C1267 r4 = (C1267) obj;
        if (!m7086().equals(r4.m7086()) || !m7087().equals(r4.m7087())) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1273 r3) {
        int r0 = C0935.m5810(m6302(), r3.m7098());
        if (r0 != 0) {
            return r0;
        }
        C1267 r32 = (C1267) r3;
        int compareTo = m7086().compareTo(r32.m7086());
        if (compareTo != 0) {
            return compareTo;
        }
        return C1403.m7796(m7087(), r32.m7087());
    }
}
