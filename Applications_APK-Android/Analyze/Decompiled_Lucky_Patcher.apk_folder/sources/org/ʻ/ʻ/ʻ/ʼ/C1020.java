package org.ʻ.ʻ.ʻ.ʼ;

import com.google.ʻ.ʿ.C0935;
import org.ʻ.ʻ.ʾ.ʾ.C1273;
import org.ʻ.ʻ.ʾ.ʾ.C1276;

/* renamed from: org.ʻ.ʻ.ʻ.ʼ.ˊ  reason: contains not printable characters */
/* compiled from: BaseFloatEncodedValue */
public abstract class C1020 implements C1276 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6318() {
        return 16;
    }

    public int hashCode() {
        return Float.floatToRawIntBits(m7104());
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C1276) || Float.floatToRawIntBits(m7104()) != Float.floatToRawIntBits(((C1276) obj).m7104())) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1273 r3) {
        int r0 = C0935.m5810(m6318(), r3.m7098());
        if (r0 != 0) {
            return r0;
        }
        return Float.compare(m7104(), ((C1276) r3).m7104());
    }
}
