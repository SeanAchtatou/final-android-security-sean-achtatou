package org.ʻ.ʻ.ʻ;

import com.google.ʻ.ʿ.C0935;
import java.util.Comparator;
import org.ʻ.ʻ.ʾ.C1187;
import org.ʻ.ʼ.C1403;

/* renamed from: org.ʻ.ʻ.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: BaseAnnotation */
public abstract class C1002 implements C1187 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final Comparator<? super C1187> f6317 = new Comparator<C1187>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public int compare(C1187 r1, C1187 r2) {
            return r1.m7002().compareTo(r2.m7002());
        }
    };

    public int hashCode() {
        return (((m7000() * 31) + m7002().hashCode()) * 31) + m7003().hashCode();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C1187)) {
            return false;
        }
        C1187 r4 = (C1187) obj;
        if (m7000() != r4.m7000() || !m7002().equals(r4.m7002()) || !m7003().equals(r4.m7003())) {
            return false;
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int compareTo(C1187 r3) {
        int r0 = C0935.m5810(m7000(), r3.m7000());
        if (r0 != 0) {
            return r0;
        }
        int compareTo = m7002().compareTo(r3.m7002());
        if (compareTo != 0) {
            return compareTo;
        }
        return C1403.m7796(m7003(), r3.m7003());
    }
}
