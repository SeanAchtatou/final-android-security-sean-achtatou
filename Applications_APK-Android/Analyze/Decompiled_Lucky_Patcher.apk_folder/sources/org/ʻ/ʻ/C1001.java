package org.ʻ.ʻ;

import java.util.HashMap;
import net.lingala.zip4j.util.InternalZipConstants;
import org.tukaani.xz.common.Util;

/* renamed from: org.ʻ.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: AccessFlags */
public enum C1001 {
    PUBLIC(1, "public", true, true, true),
    PRIVATE(2, "private", true, true, true),
    PROTECTED(4, "protected", true, true, true),
    STATIC(8, "static", true, true, true),
    FINAL(16, "final", true, true, true),
    SYNCHRONIZED(32, "synchronized", false, true, false),
    VOLATILE(64, "volatile", false, false, true),
    BRIDGE(64, "bridge", false, true, false),
    TRANSIENT(128, "transient", false, false, true),
    VARARGS(128, "varargs", false, true, false),
    NATIVE(256, "native", false, true, false),
    INTERFACE(512, "interface", true, false, false),
    ABSTRACT(Util.BLOCK_HEADER_SIZE_MAX, "abstract", true, true, false),
    STRICTFP(InternalZipConstants.UFT8_NAMES_FLAG, "strictfp", false, true, false),
    SYNTHETIC(4096, "synthetic", true, true, true),
    ANNOTATION(8192, "annotation", true, false, false),
    ENUM(16384, "enum", true, false, true),
    CONSTRUCTOR(InternalZipConstants.MIN_SPLIT_LENGTH, "constructor", false, true, false),
    DECLARED_SYNCHRONIZED(131072, "declared-synchronized", false, true, false);
    

    /* renamed from: ﾞ  reason: contains not printable characters */
    private static final C1001[] f6310 = values();

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private static HashMap<String, C1001> f6311 = new HashMap<>();

    /* renamed from: ᵔ  reason: contains not printable characters */
    private int f6312;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private String f6313;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private boolean f6314;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private boolean f6315;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f6316;

    static {
        for (C1001 r3 : f6310) {
            f6311.put(r3.f6313, r3);
        }
    }

    private C1001(int i, String str, boolean z, boolean z2, boolean z3) {
        this.f6312 = i;
        this.f6313 = str;
        this.f6314 = z;
        this.f6315 = z2;
        this.f6316 = z3;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m6291(int i) {
        return (i & this.f6312) != 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6290() {
        return this.f6312;
    }

    public String toString() {
        return this.f6313;
    }
}
