package org.ʻ.ʻ;

import com.google.ʻ.ʼ.C0866;
import com.google.ʻ.ʼ.C0929;
import java.util.EnumMap;
import java.util.HashMap;

/* renamed from: org.ʻ.ʻ.ʿ  reason: contains not printable characters */
/* compiled from: Opcodes */
public class C1292 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final int f7083;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final int f7084;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final C1185[] f7085 = new C1185[256];

    /* renamed from: ʾ  reason: contains not printable characters */
    private final EnumMap<C1185, Short> f7086;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final HashMap<String, C1185> f7087;

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1292 m7146(int i) {
        int r0 = C1399.m7783(i);
        if (r0 != -1) {
            return new C1292(r0, -1);
        }
        throw new RuntimeException("Unsupported dex version " + i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(org.ʻ.ʻ.ʾ, java.lang.Short):V}
     arg types: [org.ʻ.ʻ.ʾ, java.lang.Short]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(org.ʻ.ʻ.ʾ, java.lang.Short):V}
      ClspMth{java.util.Map.put(org.ʻ.ʻ.ʾ, java.lang.Short):V}
      ClspMth{java.util.EnumMap.put(org.ʻ.ʻ.ʾ, java.lang.Short):V} */
    private C1292(int i, int i2) {
        int i3;
        C0866<Integer, Short> r3;
        if (i >= 21) {
            this.f7083 = i;
            this.f7084 = C1399.m7786(i);
        } else if (i2 < 0 || i2 >= 39) {
            this.f7083 = i;
            this.f7084 = i2;
        } else {
            this.f7083 = C1399.m7785(i2);
            this.f7084 = i2;
        }
        this.f7086 = new EnumMap<>(C1185.class);
        this.f7087 = C0929.m5793();
        if (m7148()) {
            i3 = this.f7084;
        } else {
            i3 = this.f7083;
        }
        for (C1185 r2 : C1185.values()) {
            if (m7148()) {
                r3 = r2.f7074;
            } else {
                r3 = r2.f7073;
            }
            Short sh = (Short) r3.m5478(Integer.valueOf(i3));
            if (sh != null) {
                if (!r2.f7077.f6523) {
                    this.f7085[sh.shortValue()] = r2;
                }
                this.f7086.put(r2, sh);
                this.f7087.put(r2.f7075.toLowerCase(), r2);
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C1185 m7149(int i) {
        if (i == 256) {
            return C1185.PACKED_SWITCH_PAYLOAD;
        }
        if (i == 512) {
            return C1185.SPARSE_SWITCH_PAYLOAD;
        }
        if (i == 768) {
            return C1185.ARRAY_PAYLOAD;
        }
        if (i < 0) {
            return null;
        }
        C1185[] r0 = this.f7085;
        if (i < r0.length) {
            return r0[i];
        }
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Short m7147(C1185 r2) {
        return this.f7086.get(r2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m7148() {
        return this.f7084 != -1;
    }
}
