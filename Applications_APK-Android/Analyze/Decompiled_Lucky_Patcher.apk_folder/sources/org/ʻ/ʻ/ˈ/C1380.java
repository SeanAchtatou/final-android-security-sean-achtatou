package org.ʻ.ʻ.ˈ;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import net.lingala.zip4j.util.InternalZipConstants;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ˈ.ˆ  reason: contains not printable characters */
/* compiled from: DexDataWriter */
public class C1380 extends BufferedOutputStream {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f7214;

    /* renamed from: ʼ  reason: contains not printable characters */
    private byte[] f7215;

    /* renamed from: ʽ  reason: contains not printable characters */
    private byte[] f7216;

    public C1380(OutputStream outputStream, int i) {
        this(outputStream, i, 262144);
    }

    public C1380(OutputStream outputStream, int i, int i2) {
        super(outputStream, i2);
        this.f7215 = new byte[8];
        this.f7216 = new byte[3];
        this.f7214 = i;
    }

    public void write(int i) {
        this.f7214++;
        super.write(i);
    }

    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) {
        this.f7214 += i2;
        super.write(bArr, i, i2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7630(long j) {
        m7625((int) j);
        m7625((int) (j >> 32));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m7621(OutputStream outputStream, int i) {
        outputStream.write(i);
        outputStream.write(i >> 8);
        outputStream.write(i >> 16);
        outputStream.write(i >> 24);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7625(int i) {
        m7621(this, i);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7633(int i) {
        if (i < -32768 || i > 32767) {
            throw new C1404("Short value out of range: %d", Integer.valueOf(i));
        }
        write(i);
        write(i >> 8);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m7636(int i) {
        if (i < 0 || i > 65535) {
            throw new C1404("Unsigned short value out of range: %d", Integer.valueOf(i));
        }
        write(i);
        write(i >> 8);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m7638(int i) {
        if (i < 0 || i > 255) {
            throw new C1404("Unsigned byte value out of range: %d", Integer.valueOf(i));
        } else {
            write(i);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static void m7622(OutputStream outputStream, int i) {
        while ((((long) i) & InternalZipConstants.ZIP_64_LIMIT) > 127) {
            outputStream.write((i & 127) | 128);
            i >>>= 7;
        }
        outputStream.write(i);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m7640(int i) {
        m7622(this, i);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static void m7623(OutputStream outputStream, int i) {
        if (i >= 0) {
            while (i > 63) {
                outputStream.write((i & 127) | 128);
                i >>>= 7;
            }
            outputStream.write(i & 127);
            return;
        }
        while (i < -64) {
            outputStream.write((i & 127) | 128);
            i >>= 7;
        }
        outputStream.write(i & 127);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m7641(int i) {
        m7623(this, i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7628(int i, int i2) {
        write(i | (i2 << 5));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7634(int i, int i2) {
        int i3;
        if (i2 >= 0) {
            i3 = 0;
            while (i2 > 127) {
                this.f7215[i3] = (byte) i2;
                i2 >>= 8;
                i3++;
            }
        } else {
            int i4 = 0;
            while (i2 < -128) {
                this.f7215[i3] = (byte) i2;
                i2 >>= 8;
                i4 = i3 + 1;
            }
        }
        int i5 = i3 + 1;
        this.f7215[i3] = (byte) i2;
        m7628(i, i5 - 1);
        write(this.f7215, 0, i5);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7629(int i, long j) {
        int i2;
        if (j >= 0) {
            i2 = 0;
            while (j > 127) {
                this.f7215[i2] = (byte) ((int) j);
                j >>= 8;
                i2++;
            }
        } else {
            int i3 = 0;
            while (j < -128) {
                this.f7215[i2] = (byte) ((int) j);
                j >>= 8;
                i3 = i2 + 1;
            }
        }
        int i4 = i2 + 1;
        this.f7215[i2] = (byte) ((int) j);
        m7628(i, i4 - 1);
        write(this.f7215, 0, i4);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m7637(int i, int i2) {
        int i3 = i2;
        int i4 = 0;
        while (true) {
            int i5 = i4 + 1;
            this.f7215[i4] = (byte) i3;
            i3 >>>= 8;
            if (i3 == 0) {
                m7628(i, i5 - 1);
                write(this.f7215, 0, i5);
                return;
            }
            i4 = i5;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7627(int i, float f) {
        m7639(i, Float.floatToRawIntBits(f));
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m7639(int i, int i2) {
        int i3 = 3;
        while (true) {
            int i4 = i3 - 1;
            this.f7215[i3] = (byte) ((-16777216 & i2) >>> 24);
            i2 <<= 8;
            if (i2 == 0) {
                int i5 = i4 + 1;
                int i6 = 4 - i5;
                m7628(i, i6 - 1);
                write(this.f7215, i5, i6);
                return;
            }
            i3 = i4;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7626(int i, double d) {
        m7635(i, Double.doubleToRawLongBits(d));
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7635(int i, long j) {
        int i2 = 7;
        while (true) {
            int i3 = i2 - 1;
            this.f7215[i2] = (byte) ((int) ((-72057594037927936L & j) >>> 56));
            j <<= 8;
            if (j == 0) {
                int i4 = i3 + 1;
                int i5 = 8 - i4;
                m7628(i, i5 - 1);
                write(this.f7215, i4, i5);
                return;
            }
            i2 = i3;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7631(String str) {
        int length = str.length();
        if (this.f7215.length <= str.length() * 3) {
            this.f7215 = new byte[(str.length() * 3)];
        }
        byte[] bArr = this.f7215;
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            if (charAt != 0 && charAt < 128) {
                bArr[i] = (byte) charAt;
                i++;
            } else if (charAt < 2048) {
                int i3 = i + 1;
                bArr[i] = (byte) (((charAt >> 6) & 31) | 192);
                i = i3 + 1;
                bArr[i3] = (byte) ((charAt & '?') | 128);
            } else {
                int i4 = i + 1;
                bArr[i] = (byte) (((charAt >> 12) & 15) | 224);
                int i5 = i4 + 1;
                bArr[i4] = (byte) (((charAt >> 6) & 63) | 128);
                bArr[i5] = (byte) ((charAt & '?') | 128);
                i = i5 + 1;
            }
        }
        write(bArr, 0, i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7624() {
        int i = (-m7632()) & 3;
        if (i > 0) {
            write(this.f7216, 0, i);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m7632() {
        return this.f7214;
    }
}
