package org.ʻ.ʻ.ˈ.ʼ;

import com.google.ʻ.ʻ.C0842;
import com.google.ʻ.ʼ.C0858;
import com.google.ʻ.ʼ.C0891;
import com.google.ʻ.ʼ.C0912;
import com.google.ʻ.ʼ.C0918;
import com.google.ʻ.ʼ.C0920;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import org.ʻ.ʻ.ʻ.ʻ.C1010;
import org.ʻ.ʻ.ʾ.C1187;
import org.ʻ.ʻ.ʾ.C1257;
import org.ʻ.ʻ.ʾ.C1287;
import org.ʻ.ʻ.ˈ.ʼ.C1368;

/* renamed from: org.ʻ.ʻ.ˈ.ʼ.י  reason: contains not printable characters */
/* compiled from: PoolClassDef */
class C1362 extends C1010 implements C1257 {

    /* renamed from: ʻ  reason: contains not printable characters */
    final C1257 f7174;

    /* renamed from: ʼ  reason: contains not printable characters */
    final C1368.C1369<List<String>> f7175;

    /* renamed from: ʽ  reason: contains not printable characters */
    final C0912<C1287> f7176;

    /* renamed from: ʾ  reason: contains not printable characters */
    final C0912<C1287> f7177;

    /* renamed from: ʿ  reason: contains not printable characters */
    final C0912<C1363> f7178;

    /* renamed from: ˆ  reason: contains not printable characters */
    final C0912<C1363> f7179;

    /* renamed from: ˈ  reason: contains not printable characters */
    int f7180 = -1;

    /* renamed from: ˉ  reason: contains not printable characters */
    int f7181 = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, com.google.ʻ.ʻ.ʽ):java.lang.Iterable<T>
     arg types: [java.lang.Iterable<? extends org.ʻ.ʻ.ʾ.ˈ>, com.google.ʻ.ʻ.ʽ<org.ʻ.ʻ.ʾ.ˈ, org.ʻ.ʻ.ˈ.ʼ.ـ>]
     candidates:
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, int):java.lang.Iterable<T>
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, java.lang.Iterable):java.lang.Iterable<T>
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, java.lang.Object):T
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, com.google.ʻ.ʻ.ˉ):boolean
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, com.google.ʻ.ʻ.ʽ):java.lang.Iterable<T> */
    C1362(C1257 r3) {
        this.f7174 = r3;
        this.f7175 = new C1368.C1369<>(C0891.m5597((Collection) r3.m7053()));
        this.f7176 = C0912.m5705((Iterable) r3.m7055());
        this.f7177 = C0912.m5705((Iterable) r3.m7056());
        this.f7178 = C0912.m5705(C0918.m5742((Iterable) r3.m7057(), (C0842) C1363.f7184));
        this.f7179 = C0912.m5705(C0918.m5742((Iterable) r3.m7058(), (C0842) C1363.f7184));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m7492() {
        return this.f7174.m7049();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m7494() {
        return this.f7174.m7051();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m7493() {
        return this.f7174.m7050();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public List<String> m7496() {
        return (List) this.f7175.f7189;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public String m7495() {
        return this.f7174.m7052();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public Set<? extends C1187> m7497() {
        return this.f7174.m7054();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public SortedSet<C1287> m7498() {
        return this.f7176;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public SortedSet<C1287> m7499() {
        return this.f7177;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public Collection<C1287> m7504() {
        return new AbstractCollection<C1287>() {
            public Iterator<C1287> iterator() {
                return C0920.m5756(C0891.m5596(C1362.this.f7176.iterator(), C1362.this.f7177.iterator()), C0858.m5447());
            }

            public int size() {
                return C1362.this.f7176.size() + C1362.this.f7177.size();
            }
        };
    }

    /* renamed from: י  reason: contains not printable characters */
    public SortedSet<C1363> m7500() {
        return this.f7178;
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public SortedSet<C1363> m7501() {
        return this.f7179;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public Collection<C1363> m7507() {
        return new AbstractCollection<C1363>() {
            public Iterator<C1363> iterator() {
                return C0920.m5756(C0891.m5596(C1362.this.f7178.iterator(), C1362.this.f7179.iterator()), C0858.m5447());
            }

            public int size() {
                return C1362.this.f7178.size() + C1362.this.f7179.size();
            }
        };
    }
}
