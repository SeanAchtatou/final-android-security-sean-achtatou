package org.ʻ.ʻ.ˈ.ʻ;

import java.io.File;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import net.lingala.zip4j.util.InternalZipConstants;
import org.ʻ.ʼ.C1408;

/* renamed from: org.ʻ.ʻ.ˈ.ʻ.ʾ  reason: contains not printable characters */
/* compiled from: FileDataStore */
public class C1344 implements C1343 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final RandomAccessFile f7151;

    public C1344(File file) {
        this.f7151 = new RandomAccessFile(file, InternalZipConstants.WRITE_MODE);
        this.f7151.setLength(0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public OutputStream m7318(int i) {
        return new C1408(this.f7151, i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7319() {
        this.f7151.close();
    }
}
