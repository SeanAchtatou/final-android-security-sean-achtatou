package org.ʻ.ʻ.ˈ.ʼ;

import java.util.Collection;
import java.util.Map;
import org.ʻ.ʻ.ˈ.C1388;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ˈ.ʼ.ʽ  reason: contains not printable characters */
/* compiled from: BaseIndexPool */
public abstract class C1349<Key> extends C1352<Key, Integer> implements C1388<Key> {
    public C1349(C1356 r1) {
        super(r1);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Collection<? extends Map.Entry<? extends Key, Integer>> m7340() {
        return this.f7157.entrySet();
    }

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public int m7342(Key key) {
        Integer num = (Integer) this.f7157.get(key);
        if (num != null) {
            return num.intValue();
        }
        throw new C1404("Item not found.: %s", m7341(key));
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʾ  reason: contains not printable characters */
    public String m7341(Object obj) {
        return obj.toString();
    }
}
