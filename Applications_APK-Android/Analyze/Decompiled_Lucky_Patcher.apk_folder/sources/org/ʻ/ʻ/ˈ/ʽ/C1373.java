package org.ʻ.ʻ.ˈ.ʽ;

import com.google.ʻ.ʻ.C0842;
import com.google.ʻ.ʻ.C0848;
import com.google.ʻ.ʼ.C0881;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import org.ʻ.ʻ.ʻ.ʼ.C1013;
import org.ʻ.ʻ.ʾ.C1287;
import org.ʻ.ʻ.ʾ.ʾ.C1268;
import org.ʻ.ʻ.ʾ.ʾ.C1273;
import org.ʻ.ʻ.ʿ.ʾ.C1315;
import org.ʻ.ʻ.ˆ.C1334;
import org.ʻ.ʼ.C1400;
import org.ʻ.ʼ.C1403;

/* renamed from: org.ʻ.ʻ.ˈ.ʽ.ʼ  reason: contains not printable characters */
/* compiled from: StaticInitializerUtil */
public class C1373 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final C0848<C1287> f7193 = new C0848<C1287>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m7555(C1287 r1) {
            C1273 r12 = r1.m7128();
            return r12 != null && !C1334.m7276(r12);
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final C0842<C1287, C1273> f7194 = new C0842<C1287, C1273>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public C1273 m7557(C1287 r2) {
            C1273 r0 = r2.m7128();
            return r0 == null ? C1315.m7238(r2.m7126()) : r0;
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1268 m7553(final SortedSet<? extends C1287> sortedSet) {
        final int r0 = C1403.m7791(sortedSet, f7193);
        if (r0 > -1) {
            return new C1013() {
                /* renamed from: ʼ  reason: contains not printable characters */
                public List<? extends C1273> m7554() {
                    return new C1400<C1273>() {
                        public Iterator<C1273> iterator() {
                            return C0881.m5557(sortedSet).m5562(r0 + 1).m5563(C1373.f7194).iterator();
                        }

                        public int size() {
                            return r0 + 1;
                        }
                    };
                }
            };
        }
        return null;
    }
}
