package org.ʻ.ʻ.ˈ.ʼ;

import java.util.Collection;
import java.util.List;
import org.ʻ.ʻ.ʾ.ʽ.C1261;
import org.ʻ.ʻ.ˆ.C1336;
import org.ʻ.ʻ.ˈ.C1395;
import org.ʻ.ʻ.ˈ.ʼ.C1368;

/* renamed from: org.ʻ.ʻ.ˈ.ʼ.ᐧ  reason: contains not printable characters */
/* compiled from: ProtoPool */
public class C1365 extends C1349<C1261> implements C1395<CharSequence, CharSequence, C1261, C1368.C1369<? extends Collection<? extends CharSequence>>> {
    public C1365(C1356 r1) {
        super(r1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7521(C1261 r3) {
        if (((Integer) this.f7157.put(r3, 0)) == null) {
            ((C1366) this.f7156.f7256).m7528(m7520(r3));
            ((C1370) this.f7156.f7246).m7543((CharSequence) r3.m7073());
            ((C1368) this.f7156.f7243).m7538((Collection<? extends CharSequence>) r3.m7072());
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public CharSequence m7520(C1261 r2) {
        return C1336.m7282(r2.m7072(), r2.m7073());
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public CharSequence m7523(C1261 r1) {
        return r1.m7073();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public C1368.C1369<List<? extends CharSequence>> m7525(C1261 r2) {
        return new C1368.C1369<>(r2.m7072());
    }
}
