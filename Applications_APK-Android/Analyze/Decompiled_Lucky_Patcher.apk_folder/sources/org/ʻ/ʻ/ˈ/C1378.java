package org.ʻ.ʻ.ˈ;

import java.lang.CharSequence;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.ʻ.ʻ.ʼ.C1088;
import org.ʻ.ʻ.ʾ.C1286;
import org.ʻ.ʻ.ʾ.C1291;
import org.ʻ.ʻ.ʾ.ʻ.C1188;
import org.ʻ.ʻ.ʾ.ʼ.C1240;

/* renamed from: org.ʻ.ʻ.ˈ.ʾ  reason: contains not printable characters */
/* compiled from: ClassSection */
public interface C1378<StringKey extends CharSequence, TypeKey extends CharSequence, TypeListKey, ClassKey, FieldKey, MethodKey, AnnotationSetKey, EncodedArrayKey> extends C1388<ClassKey> {
    /* renamed from: ʻ  reason: contains not printable characters */
    TypeKey m7576(Object obj);

    /* renamed from: ʻ  reason: contains not printable characters */
    TypeKey m7577(C1286 r1);

    /* renamed from: ʻ  reason: contains not printable characters */
    Collection<? extends ClassKey> m7578();

    /* renamed from: ʻ  reason: contains not printable characters */
    Map.Entry<? extends ClassKey, Integer> m7579(CharSequence charSequence);

    /* renamed from: ʻ  reason: contains not printable characters */
    void m7580(Object obj, int i);

    /* renamed from: ʻ  reason: contains not printable characters */
    void m7581(C1379 r1, C1188 r2);

    /* renamed from: ʼ  reason: contains not printable characters */
    int m7582(Object obj);

    /* renamed from: ʼ  reason: contains not printable characters */
    void m7583(Object obj, int i);

    /* renamed from: ʽ  reason: contains not printable characters */
    TypeKey m7584(Object obj);

    /* renamed from: ʽ  reason: contains not printable characters */
    void m7585(MethodKey methodkey, int i);

    /* renamed from: ʾ  reason: contains not printable characters */
    TypeListKey m7586(Object obj);

    /* renamed from: ʿ  reason: contains not printable characters */
    StringKey m7587(Object obj);

    /* renamed from: ˆ  reason: contains not printable characters */
    EncodedArrayKey m7588(Object obj);

    /* renamed from: ˈ  reason: contains not printable characters */
    Collection<? extends FieldKey> m7589(Object obj);

    /* renamed from: ˉ  reason: contains not printable characters */
    Collection<? extends FieldKey> m7590(Object obj);

    /* renamed from: ˊ  reason: contains not printable characters */
    Collection<? extends FieldKey> m7591(Object obj);

    /* renamed from: ˋ  reason: contains not printable characters */
    Collection<? extends MethodKey> m7592(Object obj);

    /* renamed from: ˎ  reason: contains not printable characters */
    Collection<? extends MethodKey> m7593(Object obj);

    /* renamed from: ˏ  reason: contains not printable characters */
    Collection<? extends MethodKey> m7594(Object obj);

    /* renamed from: ˑ  reason: contains not printable characters */
    int m7595(Object obj);

    /* renamed from: י  reason: contains not printable characters */
    int m7596(Object obj);

    /* renamed from: ـ  reason: contains not printable characters */
    AnnotationSetKey m7597(Object obj);

    /* renamed from: ٴ  reason: contains not printable characters */
    AnnotationSetKey m7598(FieldKey fieldkey);

    /* renamed from: ᐧ  reason: contains not printable characters */
    AnnotationSetKey m7599(MethodKey methodkey);

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    int m7600(MethodKey methodkey);

    /* renamed from: ᴵ  reason: contains not printable characters */
    List<? extends AnnotationSetKey> m7601(MethodKey methodkey);

    /* renamed from: ᵎ  reason: contains not printable characters */
    Iterable<? extends C1188> m7602(MethodKey methodkey);

    /* renamed from: ᵔ  reason: contains not printable characters */
    Iterable<? extends StringKey> m7603(MethodKey methodkey);

    /* renamed from: ᵢ  reason: contains not printable characters */
    int m7604(MethodKey methodkey);

    /* renamed from: ⁱ  reason: contains not printable characters */
    Iterable<? extends C1240> m7605(MethodKey methodkey);

    /* renamed from: ﹳ  reason: contains not printable characters */
    List<? extends C1291<? extends C1286>> m7606(MethodKey methodkey);

    /* renamed from: ﹶ  reason: contains not printable characters */
    C1088 m7607(MethodKey methodkey);

    /* renamed from: ﾞ  reason: contains not printable characters */
    int m7608(ClassKey classkey);

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    int m7609(MethodKey methodkey);
}
