package org.ʻ.ʻ.ˈ.ʽ;

import com.google.ʻ.ʼ.C0926;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.ʻ.ʻ.ʻ.C1032;
import org.ʻ.ʻ.ʾ.C1286;
import org.ʻ.ʻ.ʾ.C1291;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ˈ.ʽ.ʽ  reason: contains not printable characters */
/* compiled from: TryListBuilder */
public class C1374<EH extends C1286> {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final C1376<EH> f7198 = new C1376<>(0, 0);
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public final C1376<EH> f7199 = new C1376<>(0, 0);

    public C1374() {
        C1376<EH> r0 = this.f7198;
        C1376<EH> r1 = this.f7199;
        r0.f7203 = r1;
        r1.f7202 = r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <EH extends C1286> List<C1291<EH>> m7559(List<? extends C1291<? extends EH>> list) {
        C1374 r0 = new C1374();
        for (C1291 r1 : list) {
            int r2 = r1.m7143();
            int r3 = r1.m7144() + r2;
            for (C1286 r4 : r1.m7145()) {
                r0.m7564(r2, r3, r4);
            }
        }
        return r0.m7563();
    }

    /* renamed from: org.ʻ.ʻ.ˈ.ʽ.ʽ$ʽ  reason: contains not printable characters */
    /* compiled from: TryListBuilder */
    private static class C1377<EH extends C1286> {

        /* renamed from: ʻ  reason: contains not printable characters */
        public final C1376<EH> f7207;

        /* renamed from: ʼ  reason: contains not printable characters */
        public final C1376<EH> f7208;

        public C1377(C1376<EH> r1, C1376<EH> r2) {
            this.f7207 = r1;
            this.f7208 = r2;
        }
    }

    /* renamed from: org.ʻ.ʻ.ˈ.ʽ.ʽ$ʻ  reason: contains not printable characters */
    /* compiled from: TryListBuilder */
    public static class C1375 extends C1404 {
        public C1375(String str, Object... objArr) {
            super(str, objArr);
        }
    }

    /* renamed from: org.ʻ.ʻ.ˈ.ʽ.ʽ$ʼ  reason: contains not printable characters */
    /* compiled from: TryListBuilder */
    private static class C1376<EH extends C1286> extends C1032<EH> {

        /* renamed from: ʻ  reason: contains not printable characters */
        public C1376<EH> f7202 = null;

        /* renamed from: ʼ  reason: contains not printable characters */
        public C1376<EH> f7203 = null;

        /* renamed from: ʽ  reason: contains not printable characters */
        public int f7204;

        /* renamed from: ʾ  reason: contains not printable characters */
        public int f7205;

        /* renamed from: ʿ  reason: contains not printable characters */
        public List<EH> f7206 = C0926.m5776();

        public C1376(int i, int i2) {
            this.f7204 = i;
            this.f7205 = i2;
        }

        public C1376(int i, int i2, List<EH> list) {
            this.f7204 = i;
            this.f7205 = i2;
            this.f7206 = C0926.m5777(list);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m7567() {
            return this.f7204;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m7571() {
            return this.f7205 - this.f7204;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public List<EH> m7573() {
            return this.f7206;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C1376<EH> m7568(int i) {
            C1376<EH> r0 = new C1376<>(i, this.f7205, this.f7206);
            this.f7205 = i;
            m7570(r0);
            return r0;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public void m7574() {
            C1376<EH> r0 = this.f7203;
            r0.f7202 = this.f7202;
            this.f7202.f7203 = r0;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public void m7575() {
            C1376<EH> r0 = this.f7203;
            this.f7205 = r0.f7205;
            r0.m7574();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m7570(C1376<EH> r2) {
            C1376<EH> r0 = this.f7203;
            r0.f7202 = r2;
            r2.f7203 = r0;
            r2.f7202 = this;
            this.f7203 = r2;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m7572(C1376<EH> r2) {
            C1376<EH> r0 = this.f7202;
            r0.f7203 = r2;
            r2.f7202 = r0;
            r2.f7203 = this;
            this.f7202 = r2;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m7569(EH eh) {
            for (EH eh2 : this.f7206) {
                String r2 = eh2.m7124();
                String r3 = eh.m7124();
                if (r2 == null) {
                    if (r3 == null) {
                        if (eh2.m7125() != eh.m7125()) {
                            throw new C1375("Multiple overlapping catch all handlers with different handlers", new Object[0]);
                        }
                        return;
                    }
                } else if (r2.equals(r3)) {
                    return;
                }
            }
            this.f7206.add(eh);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private C1377<EH> m7561(int i, int i2) {
        C1376<EH> r0 = this.f7198.f7203;
        while (true) {
            if (r0 == this.f7199) {
                r0 = null;
                break;
            }
            int i3 = r0.f7204;
            int i4 = r0.f7205;
            if (i != i3) {
                if (i > i3 && i < i4) {
                    r0 = r0.m7568(i);
                    break;
                } else if (i >= i3) {
                    r0 = r0.f7203;
                } else if (i2 <= i3) {
                    C1376 r1 = new C1376(i, i2);
                    r0.m7572(r1);
                    return new C1377<>(r1, r1);
                } else {
                    C1376<EH> r2 = new C1376<>(i, i3);
                    r0.m7572(r2);
                    r0 = r2;
                }
            } else {
                break;
            }
        }
        if (r0 == null) {
            C1376 r02 = new C1376(i, i2);
            this.f7199.m7572(r02);
            return new C1377<>(r02, r02);
        }
        C1376<EH> r4 = r0;
        while (true) {
            C1376<EH> r12 = this.f7199;
            if (r4 != r12) {
                int i5 = r4.f7204;
                int i6 = r4.f7205;
                if (i2 == i6) {
                    return new C1377<>(r0, r4);
                }
                if (i2 > i5 && i2 < i6) {
                    r4.m7568(i2);
                    return new C1377<>(r0, r4);
                } else if (i2 <= i5) {
                    C1376 r13 = new C1376(r4.f7202.f7205, i2);
                    r4.m7572(r13);
                    return new C1377<>(r0, r13);
                } else {
                    r4 = r4.f7203;
                }
            } else {
                C1376 r42 = new C1376(r12.f7202.f7205, i2);
                this.f7199.m7572(r42);
                return new C1377<>(r0, r42);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7564(int i, int i2, EH eh) {
        C1377 r5 = m7561(i, i2);
        C1376<EH> r0 = r5.f7207;
        C1376<EH> r52 = r5.f7208;
        do {
            if (r0.f7204 > i) {
                C1376<EH> r1 = new C1376<>(i, r0.f7204);
                r0.m7572(r1);
                r0 = r1;
            }
            r0.m7569((C1286) eh);
            i = r0.f7205;
            r0 = r0.f7203;
        } while (r0.f7202 != r52);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public List<C1291<EH>> m7563() {
        return C0926.m5778(new Iterator<C1291<EH>>() {

            /* renamed from: ʼ  reason: contains not printable characters */
            private C1376<EH> f7201;

            {
                this.f7201 = C1374.this.f7198;
                this.f7201 = m7565();
            }

            /* access modifiers changed from: protected */
            /* renamed from: ʻ  reason: contains not printable characters */
            public C1376<EH> m7565() {
                C1376<EH> r0 = this.f7201.f7203;
                if (r0 == C1374.this.f7199) {
                    return null;
                }
                while (r0.f7203 != C1374.this.f7199 && r0.f7205 == r0.f7203.f7204 && r0.m7573().equals(r0.f7203.m7573())) {
                    r0.m7575();
                }
                return r0;
            }

            public boolean hasNext() {
                return this.f7201 != null;
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public C1291<EH> next() {
                if (hasNext()) {
                    C1376<EH> r0 = this.f7201;
                    this.f7201 = m7565();
                    return r0;
                }
                throw new NoSuchElementException();
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        });
    }
}
