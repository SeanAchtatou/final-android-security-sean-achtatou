package org.ʻ.ʻ.ˈ;

import com.google.ʻ.ʼ.C0858;
import java.util.Collection;
import java.util.Iterator;
import org.ʻ.ʻ.ʻ.C1011;
import org.ʻ.ʻ.ʾ.C1197;
import org.ʻ.ʻ.ʾ.ʽ.C1259;
import org.ʻ.ʻ.ʾ.ʽ.C1260;
import org.ʻ.ʻ.ʾ.ʽ.C1262;

/* renamed from: org.ʻ.ʻ.ˈ.ˊ  reason: contains not printable characters */
/* compiled from: EncodedValueWriter */
public abstract class C1386<StringKey, TypeKey, FieldRefKey extends C1259, MethodRefKey extends C1262, AnnotationElement extends C1197, ProtoRefKey, MethodHandleKey extends C1260, EncodedValue> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C1380 f7263;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final C1396<StringKey, ?> f7264;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final C1398<?, TypeKey, ?> f7265;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final C1387<?, ?, FieldRefKey, ?> f7266;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final C1391<?, ?, ?, MethodRefKey, ?> f7267;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final C1395<?, ?, ProtoRefKey, ?> f7268;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final C1390<MethodHandleKey, ?, ?> f7269;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final C1340<StringKey, TypeKey, ?, AnnotationElement, EncodedValue> f7270;

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m7700(Object obj);

    public C1386(C1380 r1, C1396<StringKey, ?> r2, C1398<?, TypeKey, ?> r3, C1387<?, ?, FieldRefKey, ?> r4, C1391<?, ?, ?, MethodRefKey, ?> r5, C1395<?, ?, ProtoRefKey, ?> r6, C1390<MethodHandleKey, ?, ?> r7, C1340<StringKey, TypeKey, ?, AnnotationElement, EncodedValue> r8) {
        this.f7263 = r1;
        this.f7264 = r2;
        this.f7265 = r3;
        this.f7266 = r4;
        this.f7267 = r5;
        this.f7268 = r6;
        this.f7269 = r7;
        this.f7270 = r8;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7701(TypeKey typekey, Collection<? extends AnnotationElement> collection) {
        this.f7263.m7628(29, 0);
        this.f7263.m7640(this.f7265.m7718(typekey));
        this.f7263.m7640(collection.size());
        Iterator<? extends AnnotationElement> it = C0858.m5446(C1011.f6318).m5451(collection).iterator();
        while (it.hasNext()) {
            C1197 r5 = (C1197) it.next();
            this.f7263.m7640(this.f7264.m7718(this.f7270.m7312(r5)));
            m7700((Object) this.f7270.m7313(r5));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7702(Collection collection) {
        this.f7263.m7628(28, 0);
        this.f7263.m7640(collection.size());
        for (Object r0 : collection) {
            m7700(r0);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7706(boolean z) {
        this.f7263.m7628(31, z ? 1 : 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ʻ.ʻ.ˈ.ˆ.ʼ(int, int):void
     arg types: [int, byte]
     candidates:
      org.ʻ.ʻ.ˈ.ˆ.ʼ(java.io.OutputStream, int):void
      org.ʻ.ʻ.ˈ.ˆ.ʼ(int, long):void
      org.ʻ.ʻ.ˈ.ˆ.ʼ(int, int):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7694(byte b) {
        this.f7263.m7634(0, (int) b);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7695(char c) {
        this.f7263.m7637(3, c);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7696(double d) {
        this.f7263.m7626(17, d);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7703(C1259 r3) {
        this.f7263.m7637(27, this.f7266.m7718(r3));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7709(C1259 r3) {
        this.f7263.m7637(25, this.f7266.m7718(r3));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7697(float f) {
        this.f7263.m7627(16, f);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7698(int i) {
        this.f7263.m7634(4, i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7699(long j) {
        this.f7263.m7629(6, j);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7705(C1262 r3) {
        this.f7263.m7637(26, this.f7267.m7718(r3));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7693() {
        this.f7263.write(30);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7707(int i) {
        this.f7263.m7634(2, i);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7708(Object obj) {
        this.f7263.m7637(23, this.f7264.m7718(obj));
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m7710(TypeKey typekey) {
        this.f7263.m7637(24, this.f7265.m7718(typekey));
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m7711(ProtoRefKey protorefkey) {
        this.f7263.m7637(21, this.f7268.m7718(protorefkey));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7704(C1260 r3) {
        this.f7263.m7637(22, this.f7269.m7718(r3));
    }
}
