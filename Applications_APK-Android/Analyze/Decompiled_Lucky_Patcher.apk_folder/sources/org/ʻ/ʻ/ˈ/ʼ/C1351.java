package org.ʻ.ʻ.ˈ.ʼ;

import java.util.Collection;
import java.util.Map;
import org.ʻ.ʻ.ˈ.C1394;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ˈ.ʼ.ʿ  reason: contains not printable characters */
/* compiled from: BaseOffsetPool */
public abstract class C1351<Key> extends C1352<Key, Integer> implements C1394<Key> {
    public C1351(C1356 r1) {
        super(r1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Collection<? extends Map.Entry<? extends Key, Integer>> m7344() {
        return this.f7157.entrySet();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public int m7345(Key key) {
        Integer num = (Integer) this.f7157.get(key);
        if (num != null) {
            return num.intValue();
        }
        throw new C1404("Item not found.: %s", m7346(key));
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public String m7346(Key key) {
        return key.toString();
    }
}
