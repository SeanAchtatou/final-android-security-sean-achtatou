package org.ʻ.ʻ.ˈ.ʼ;

import com.google.ʻ.ʼ.C0891;
import java.util.Collection;
import java.util.Iterator;
import org.ʻ.ʻ.ˈ.C1397;

/* renamed from: org.ʻ.ʻ.ˈ.ʼ.ᵔ  reason: contains not printable characters */
/* compiled from: TypeListPool */
public class C1368 extends C1350<C1369<? extends Collection<? extends CharSequence>>> implements C1397<CharSequence, C1369<? extends Collection<? extends CharSequence>>> {
    /* renamed from: ʻ  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Collection m7536(Object obj) {
        return m7537((C1369<? extends Collection<? extends CharSequence>>) ((C1369) obj));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ int m7539(Object obj) {
        return m7540((C1369<? extends Collection<? extends CharSequence>>) ((C1369) obj));
    }

    public C1368(C1356 r1) {
        super(r1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7538(Collection<? extends CharSequence> collection) {
        if (collection.size() > 0) {
            if (((Integer) this.f7157.put(new C1369(collection), 0)) == null) {
                for (CharSequence r0 : collection) {
                    ((C1370) this.f7156.f7246).m7543(r0);
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Collection<? extends CharSequence> m7537(C1369<? extends Collection<? extends CharSequence>> r1) {
        if (r1 == null) {
            return C0891.m5603();
        }
        return r1.f7189;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m7540(C1369<? extends Collection<? extends CharSequence>> r2) {
        if (r2 == null || r2.f7189.size() == 0) {
            return 0;
        }
        return super.m7343((Object) r2);
    }

    /* renamed from: org.ʻ.ʻ.ˈ.ʼ.ᵔ$ʻ  reason: contains not printable characters */
    /* compiled from: TypeListPool */
    public static class C1369<TypeCollection extends Collection<? extends CharSequence>> implements Comparable<C1369<? extends Collection<? extends CharSequence>>> {

        /* renamed from: ʻ  reason: contains not printable characters */
        TypeCollection f7189;

        public C1369(TypeCollection typecollection) {
            this.f7189 = typecollection;
        }

        public int hashCode() {
            int i = 1;
            for (CharSequence charSequence : this.f7189) {
                i = (i * 31) + charSequence.toString().hashCode();
            }
            return i;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C1369)) {
                return false;
            }
            C1369 r5 = (C1369) obj;
            if (this.f7189.size() != r5.f7189.size()) {
                return false;
            }
            Iterator it = r5.f7189.iterator();
            for (CharSequence charSequence : this.f7189) {
                if (!charSequence.toString().equals(((CharSequence) it.next()).toString())) {
                    return false;
                }
            }
            return true;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (CharSequence charSequence : this.f7189) {
                sb.append(charSequence.toString());
            }
            return sb.toString();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int compareTo(C1369<? extends Collection<? extends CharSequence>> r4) {
            Iterator it = r4.f7189.iterator();
            for (CharSequence charSequence : this.f7189) {
                if (!it.hasNext()) {
                    return 1;
                }
                int compareTo = charSequence.toString().compareTo(((CharSequence) it.next()).toString());
                if (compareTo != 0) {
                    return compareTo;
                }
            }
            return it.hasNext() ? -1 : 0;
        }
    }
}
