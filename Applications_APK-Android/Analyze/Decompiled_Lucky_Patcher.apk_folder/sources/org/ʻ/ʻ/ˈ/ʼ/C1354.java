package org.ʻ.ʻ.ˈ.ʼ;

import com.google.ʻ.ʻ.C0842;
import com.google.ʻ.ʻ.C0848;
import com.google.ʻ.ʼ.C0858;
import com.google.ʻ.ʼ.C0881;
import com.google.ʻ.ʼ.C0891;
import com.google.ʻ.ʼ.C0918;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.ʻ.ʻ.ʼ.C1088;
import org.ʻ.ʻ.ʾ.C1187;
import org.ʻ.ʻ.ʾ.C1257;
import org.ʻ.ʻ.ʾ.C1286;
import org.ʻ.ʻ.ʾ.C1287;
import org.ʻ.ʻ.ʾ.C1288;
import org.ʻ.ʻ.ʾ.C1289;
import org.ʻ.ʻ.ʾ.C1290;
import org.ʻ.ʻ.ʾ.C1291;
import org.ʻ.ʻ.ʾ.ʻ.C1188;
import org.ʻ.ʻ.ʾ.ʻ.C1189;
import org.ʻ.ʻ.ʾ.ʻ.C1191;
import org.ʻ.ʻ.ʾ.ʻ.C1194;
import org.ʻ.ʻ.ʾ.ʻ.C1195;
import org.ʻ.ʻ.ʾ.ʻ.C1196;
import org.ʻ.ʻ.ʾ.ʼ.C1240;
import org.ʻ.ʻ.ʾ.ʼ.C1247;
import org.ʻ.ʻ.ʾ.ʽ.C1258;
import org.ʻ.ʻ.ʾ.ʽ.C1259;
import org.ʻ.ʻ.ʾ.ʽ.C1262;
import org.ʻ.ʻ.ʾ.ʽ.C1263;
import org.ʻ.ʻ.ʾ.ʽ.C1264;
import org.ʻ.ʻ.ʾ.ʽ.C1265;
import org.ʻ.ʻ.ʾ.ʾ.C1268;
import org.ʻ.ʻ.ʾ.ʾ.C1273;
import org.ʻ.ʻ.ˆ.C1338;
import org.ʻ.ʻ.ˈ.C1378;
import org.ʻ.ʻ.ˈ.C1379;
import org.ʻ.ʻ.ˈ.ʼ.C1368;
import org.ʻ.ʻ.ˈ.ʽ.C1373;
import org.ʻ.ʼ.C1400;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ˈ.ʼ.ˉ  reason: contains not printable characters */
/* compiled from: ClassPool */
public class C1354 extends C1352<String, C1362> implements C1378<CharSequence, CharSequence, C1368.C1369<? extends Collection<? extends CharSequence>>, C1362, C1287, C1363, Set<? extends C1187>, C1268> {

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final C0848<C1290> f7159 = new C0848<C1290>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m7425(C1290 r1) {
            return r1.m7141().size() > 0;
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public static final C0842<C1290, Set<? extends C1187>> f7160 = new C0842<C1290, Set<? extends C1187>>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public Set<? extends C1187> m7427(C1290 r1) {
            return r1.m7141();
        }
    };

    /* renamed from: ʽ  reason: contains not printable characters */
    private C0891<C1362> f7161 = null;

    public C1354(C1356 r1) {
        super(r1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ʻ.ʻ.ˆ.ˈ.ʻ(org.ʻ.ʻ.ʾ.ʽ.ʿ, boolean):java.lang.String
     arg types: [org.ʻ.ʻ.ˈ.ʼ.ـ, int]
     candidates:
      org.ʻ.ʻ.ˆ.ˈ.ʻ(org.ʻ.ʻ.ʾ.ʽ.ʼ, boolean):java.lang.String
      org.ʻ.ʻ.ˆ.ˈ.ʻ(java.io.Writer, org.ʻ.ʻ.ʾ.ʽ.ʾ):void
      org.ʻ.ʻ.ˆ.ˈ.ʻ(org.ʻ.ʻ.ʾ.ʽ.ʿ, boolean):java.lang.String */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7362(C1257 r9) {
        C1362 r0 = new C1362(r9);
        if (((C1362) this.f7157.put(r0.m7492(), r0)) == null) {
            ((C1370) this.f7156.f7246).m7543((CharSequence) r0.m7492());
            ((C1370) this.f7156.f7246).m7545((CharSequence) r0.m7493());
            ((C1368) this.f7156.f7243).m7538((Collection<? extends CharSequence>) r0.m7496());
            ((C1366) this.f7156.f7256).m7530(r0.m7495());
            HashSet hashSet = new HashSet();
            for (C1287 next : r0.m7504()) {
                String r6 = C1338.m7307(next);
                if (hashSet.add(r6)) {
                    ((C1359) this.f7156.f7219).m7473((C1259) next);
                    C1273 r5 = next.m7128();
                    if (r5 != null) {
                        this.f7156.m7438(r5);
                    }
                    ((C1348) this.f7156.f7233).m7338(next.m7129());
                    C1268 r4 = m7384(r0);
                    if (r4 != null) {
                        ((C1358) this.f7156.f7231).m7466(r4);
                    }
                } else {
                    throw new C1404("Multiple definitions for field %s->%s", r0.m7492(), r6);
                }
            }
            HashSet hashSet2 = new HashSet();
            for (C1363 next2 : r0.m7507()) {
                String r62 = C1338.m7305((C1262) next2, true);
                if (hashSet2.add(r62)) {
                    ((C1361) this.f7156.f7223).m7489((C1262) next2);
                    m7351((C1288) next2);
                    m7352((C1288) next2);
                    ((C1348) this.f7156.f7233).m7338(next2.m7514());
                    for (C1290 r63 : next2.m7512()) {
                        ((C1348) this.f7156.f7233).m7338(r63.m7141());
                    }
                } else {
                    throw new C1404("Multiple definitions for method %s->%s", r0.m7492(), r62);
                }
            }
            ((C1348) this.f7156.f7233).m7338(r0.m7497());
            return;
        }
        throw new C1404("Class %s has already been interned", r0.m7492());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m7351(C1288 r9) {
        C1289 r0 = r9.m7136();
        if (r0 != null) {
            boolean z = false;
            for (C1240 r4 : r0.m7140()) {
                if (r4 instanceof C1247) {
                    C1263 r5 = ((C1247) r4).m7038();
                    int i = r4.m7034().f7076;
                    if (i == 0) {
                        ((C1366) this.f7156.f7256).m7528((CharSequence) ((C1264) r5));
                    } else if (i == 1) {
                        ((C1370) this.f7156.f7246).m7543((CharSequence) ((C1265) r5));
                    } else if (i == 2) {
                        ((C1359) this.f7156.f7219).m7473((C1259) r5);
                    } else if (i == 3) {
                        ((C1361) this.f7156.f7223).m7489((C1262) r5);
                    } else if (i == 5) {
                        ((C1353) this.f7156.f7227).m7349((C1258) r5);
                    } else {
                        throw new C1404("Unrecognized reference type: %d", Integer.valueOf(r4.m7034().f7076));
                    }
                }
                z = true;
            }
            List<? extends C1291<? extends C1286>> r1 = r0.m7138();
            if (z || r1.size() <= 0) {
                for (C1291 r02 : r0.m7138()) {
                    for (C1286 r12 : r02.m7145()) {
                        ((C1370) this.f7156.f7246).m7545((CharSequence) r12.m7124());
                    }
                }
                return;
            }
            throw new C1404("Method %s has no instructions, but has try blocks.", C1338.m7304(r9));
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m7352(C1288 r4) {
        for (C1290 r1 : r4.m7134()) {
            String r12 = r1.m7142();
            if (r12 != null) {
                ((C1366) this.f7156.f7256).m7528((CharSequence) r12);
            }
        }
        C1289 r42 = r4.m7136();
        if (r42 != null) {
            for (C1188 r0 : r42.m7139()) {
                int r13 = r0.m7005();
                if (r13 == 3) {
                    C1196 r02 = (C1196) r0;
                    ((C1366) this.f7156.f7256).m7530(r02.m7010());
                    ((C1370) this.f7156.f7246).m7545((CharSequence) r02.m7008());
                    ((C1366) this.f7156.f7256).m7530(r02.m7009());
                } else if (r13 == 9) {
                    ((C1366) this.f7156.f7256).m7530(((C1195) r0).m7012());
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Collection<? extends C1362> m7359() {
        if (this.f7161 == null) {
            this.f7161 = C0858.m5447().m5451(this.f7157.values());
        }
        return this.f7161;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0004, code lost:
        r3 = (org.ʻ.ʻ.ˈ.ʼ.C1362) r2.f7157.get(r3.toString());
     */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Map.Entry<? extends org.ʻ.ʻ.ˈ.ʼ.C1362, java.lang.Integer> m7360(java.lang.CharSequence r3) {
        /*
            r2 = this;
            r0 = 0
            if (r3 != 0) goto L_0x0004
            return r0
        L_0x0004:
            java.util.Map r1 = r2.f7157
            java.lang.String r3 = r3.toString()
            java.lang.Object r3 = r1.get(r3)
            org.ʻ.ʻ.ˈ.ʼ.י r3 = (org.ʻ.ʻ.ˈ.ʼ.C1362) r3
            if (r3 != 0) goto L_0x0013
            return r0
        L_0x0013:
            org.ʻ.ʻ.ˈ.ʼ.ˉ$1 r0 = new org.ʻ.ʻ.ˈ.ʼ.ˉ$1
            r0.<init>(r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.ʼ.C1354.m7360(java.lang.CharSequence):java.util.Map$Entry");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public CharSequence m7356(C1362 r1) {
        return r1.m7492();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m7366(C1362 r1) {
        return r1.m7494();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public CharSequence m7373(C1362 r1) {
        return r1.m7493();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public C1368.C1369<List<String>> m7378(C1362 r1) {
        return r1.f7175;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public CharSequence m7380(C1362 r1) {
        return r1.m7495();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public C1268 m7384(C1362 r1) {
        return C1373.m7553(r1.m7498());
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public Collection<? extends C1287> m7387(C1362 r1) {
        return r1.m7498();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public Collection<? extends C1287> m7389(C1362 r1) {
        return r1.m7499();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public Collection<? extends C1287> m7392(C1362 r1) {
        return r1.m7504();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public Collection<C1363> m7396(C1362 r1) {
        return r1.m7500();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public Collection<C1363> m7399(C1362 r1) {
        return r1.m7501();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public Collection<? extends C1363> m7401(C1362 r1) {
        return r1.m7507();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m7403(C1287 r1) {
        return r1.m7127();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m7405(C1363 r1) {
        return r1.m7509();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public Set<? extends C1187> m7408(C1362 r2) {
        Set<? extends C1187> r22 = r2.m7497();
        if (r22.size() == 0) {
            return null;
        }
        return r22;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Set<? extends C1187> m7409(C1287 r2) {
        Set<? extends C1187> r22 = r2.m7129();
        if (r22.size() == 0) {
            return null;
        }
        return r22;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Set<? extends C1187> m7410(C1363 r2) {
        Set<? extends C1187> r22 = r2.m7514();
        if (r22.size() == 0) {
            return null;
        }
        return r22;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, com.google.ʻ.ʻ.ˉ):boolean
     arg types: [java.util.List<? extends org.ʻ.ʻ.ʾ.ˊ>, com.google.ʻ.ʻ.ˉ<org.ʻ.ʻ.ʾ.ˊ>]
     candidates:
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, int):java.lang.Iterable<T>
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, com.google.ʻ.ʻ.ʽ):java.lang.Iterable<T>
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, java.lang.Iterable):java.lang.Iterable<T>
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, java.lang.Object):T
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, com.google.ʻ.ʻ.ˉ):boolean */
    /* renamed from: ʽ  reason: contains not printable characters */
    public List<? extends Set<? extends C1187>> m7412(C1363 r2) {
        final List<? extends C1290> r22 = r2.m7512();
        if (C0918.m5745((Iterable) r22, (C0848) f7159)) {
            return new C1400<Set<? extends C1187>>() {
                public Iterator<Set<? extends C1187>> iterator() {
                    return C0881.m5557(r22).m5563(C1354.f7160).iterator();
                }

                public int size() {
                    return r22.size();
                }
            };
        }
        return null;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public Iterable<? extends C1188> m7414(C1363 r1) {
        C1289 r12 = r1.m7515();
        if (r12 != null) {
            return r12.m7139();
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, com.google.ʻ.ʻ.ʽ):java.lang.Iterable<T>
     arg types: [java.util.List<? extends org.ʻ.ʻ.ʾ.ˊ>, org.ʻ.ʻ.ˈ.ʼ.ˉ$5]
     candidates:
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, int):java.lang.Iterable<T>
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, java.lang.Iterable):java.lang.Iterable<T>
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, java.lang.Object):T
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, com.google.ʻ.ʻ.ˉ):boolean
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, com.google.ʻ.ʻ.ʽ):java.lang.Iterable<T> */
    /* renamed from: ʿ  reason: contains not printable characters */
    public Iterable<CharSequence> m7415(C1363 r2) {
        return C0918.m5742((Iterable) r2.m7512(), (C0842) new C0842<C1290, CharSequence>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public CharSequence m7430(C1290 r1) {
                return r1.m7142();
            }
        });
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public int m7416(C1363 r1) {
        C1289 r12 = r1.m7515();
        if (r12 != null) {
            return r12.m7137();
        }
        return 0;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public Iterable<? extends C1240> m7417(C1363 r1) {
        C1289 r12 = r1.m7515();
        if (r12 != null) {
            return r12.m7140();
        }
        return null;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public List<? extends C1291<? extends C1286>> m7418(C1363 r1) {
        C1289 r12 = r1.m7515();
        if (r12 != null) {
            return r12.m7138();
        }
        return C0891.m5603();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public CharSequence m7357(C1286 r1) {
        return r1.m7124();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public C1088 m7419(C1363 r2) {
        return new C1088(r2.m7515());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7361(C1362 r1, int i) {
        r1.f7181 = i;
    }

    /* renamed from: י  reason: contains not printable characters */
    public int m7420(C1362 r1) {
        return r1.f7181;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7371(C1363 r1, int i) {
        r1.f7185 = i;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public int m7421(C1363 r1) {
        return r1.f7185;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7376(C1363 r1, int i) {
        r1.f7186 = i;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public int m7411(C1363 r1) {
        return r1.f7186;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7365(C1379<CharSequence, CharSequence> r7, C1188 r8) {
        switch (r8.m7005()) {
            case 3:
                C1196 r82 = (C1196) r8;
                r7.m7615(r82.m7004(), r82.m7014(), r82.m7010(), r82.m7008(), r82.m7009());
                return;
            case 5:
                C1189 r83 = (C1189) r8;
                r7.m7614(r83.m7004(), r83.m7006());
                return;
            case 6:
                C1194 r84 = (C1194) r8;
                r7.m7618(r84.m7004(), r84.m7011());
                return;
            case 7:
                r7.m7617(r8.m7004());
                return;
            case 8:
                r7.m7619(r8.m7004());
                return;
            case 9:
                C1195 r0 = (C1195) r8;
                r7.m7616(r0.m7004(), r0.m7012());
                break;
            case 10:
                C1191 r85 = (C1191) r8;
                r7.m7620(r85.m7004(), r85.m7007());
                return;
        }
        throw new C1404("Unexpected debug item type: %d", Integer.valueOf(r8.m7005()));
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public int m7413(C1362 r1) {
        return r1.f7180;
    }

    /* renamed from: org.ʻ.ʻ.ˈ.ʼ.ˉ$ʻ  reason: contains not printable characters */
    /* compiled from: ClassPool */
    class C1355 implements Map.Entry<C1362, Integer> {

        /* renamed from: ʼ  reason: contains not printable characters */
        private final C1362 f7171;

        public C1355(C1362 r2) {
            this.f7171 = r2;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C1362 getKey() {
            return this.f7171;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public Integer getValue() {
            return Integer.valueOf(this.f7171.f7180);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Integer setValue(Integer num) {
            int i = this.f7171.f7180;
            this.f7171.f7180 = num.intValue();
            return Integer.valueOf(i);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Collection<? extends Map.Entry<C1362, Integer>> m7368() {
        return new AbstractCollection<Map.Entry<C1362, Integer>>() {
            public Iterator<Map.Entry<C1362, Integer>> iterator() {
                return new Iterator<Map.Entry<C1362, Integer>>() {

                    /* renamed from: ʻ  reason: contains not printable characters */
                    Iterator<C1362> f7168 = C1354.this.f7157.values().iterator();

                    public boolean hasNext() {
                        return this.f7168.hasNext();
                    }

                    /* renamed from: ʻ  reason: contains not printable characters */
                    public Map.Entry<C1362, Integer> next() {
                        return new C1355(this.f7168.next());
                    }

                    public void remove() {
                        throw new UnsupportedOperationException();
                    }
                };
            }

            public int size() {
                return C1354.this.f7157.size();
            }
        };
    }
}
