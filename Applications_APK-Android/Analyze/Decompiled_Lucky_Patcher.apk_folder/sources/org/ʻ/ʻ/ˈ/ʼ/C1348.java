package org.ʻ.ʻ.ˈ.ʼ;

import java.util.Collection;
import java.util.Set;
import org.ʻ.ʻ.ʾ.C1187;
import org.ʻ.ʻ.ˈ.C1346;

/* renamed from: org.ʻ.ʻ.ˈ.ʼ.ʼ  reason: contains not printable characters */
/* compiled from: AnnotationSetPool */
public class C1348 extends C1350<Set<? extends C1187>> implements C1346<C1187, Set<? extends C1187>> {
    /* renamed from: ʼ  reason: contains not printable characters */
    public Collection<? extends C1187> m7339(Set<? extends C1187> set) {
        return set;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public /* synthetic */ Collection m7337(Object obj) {
        return m7339((Set<? extends C1187>) ((Set) obj));
    }

    public C1348(C1356 r1) {
        super(r1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7338(Set<? extends C1187> set) {
        if (set.size() > 0 && ((Integer) this.f7157.put(set, 0)) == null) {
            for (C1187 r0 : set) {
                ((C1347) this.f7156.f7229).m7328(r0);
            }
        }
    }
}
