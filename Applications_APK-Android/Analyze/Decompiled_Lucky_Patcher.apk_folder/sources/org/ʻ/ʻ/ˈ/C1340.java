package org.ʻ.ʻ.ˈ;

import java.util.Collection;

/* renamed from: org.ʻ.ʻ.ˈ.ʻ  reason: contains not printable characters */
/* compiled from: AnnotationSection */
public interface C1340<StringKey, TypeKey, AnnotationKey, AnnotationElement, EncodedValue> extends C1394<AnnotationKey> {
    /* renamed from: ʻ  reason: contains not printable characters */
    int m7309(AnnotationKey annotationkey);

    /* renamed from: ʼ  reason: contains not printable characters */
    TypeKey m7310(AnnotationKey annotationkey);

    /* renamed from: ʽ  reason: contains not printable characters */
    Collection<? extends AnnotationElement> m7311(AnnotationKey annotationkey);

    /* renamed from: ʾ  reason: contains not printable characters */
    StringKey m7312(AnnotationElement annotationelement);

    /* renamed from: ʿ  reason: contains not printable characters */
    EncodedValue m7313(AnnotationElement annotationelement);
}
