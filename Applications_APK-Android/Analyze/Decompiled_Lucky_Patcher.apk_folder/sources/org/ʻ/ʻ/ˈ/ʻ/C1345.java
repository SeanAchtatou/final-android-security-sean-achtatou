package org.ʻ.ʻ.ˈ.ʻ;

import com.google.ʻ.ʼ.C0926;
import java.io.OutputStream;
import java.util.List;

/* renamed from: org.ʻ.ʻ.ˈ.ʻ.ʿ  reason: contains not printable characters */
/* compiled from: MemoryDeferredOutputStream */
public class C1345 extends C1341 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final List<byte[]> f7152;

    /* renamed from: ʼ  reason: contains not printable characters */
    private byte[] f7153;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f7154;

    public C1345() {
        this(16384);
    }

    public C1345(int i) {
        this.f7152 = C0926.m5776();
        this.f7153 = new byte[i];
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7323(OutputStream outputStream) {
        for (byte[] write : this.f7152) {
            outputStream.write(write);
        }
        int i = this.f7154;
        if (i > 0) {
            outputStream.write(this.f7153, 0, i);
        }
        this.f7152.clear();
        this.f7154 = 0;
    }

    public void write(int i) {
        if (m7322() == 0) {
            this.f7152.add(this.f7153);
            this.f7153 = new byte[this.f7153.length];
            this.f7154 = 0;
        }
        byte[] bArr = this.f7153;
        int i2 = this.f7154;
        this.f7154 = i2 + 1;
        bArr[i2] = (byte) i;
    }

    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) {
        int r2 = m7322();
        int i3 = 0;
        while (true) {
            int i4 = i2 - i3;
            if (i4 > 0) {
                int min = Math.min(r2, i4);
                System.arraycopy(bArr, i + i3, this.f7153, this.f7154, min);
                i3 += min;
                this.f7154 += min;
                r2 = m7322();
                if (r2 == 0) {
                    this.f7152.add(this.f7153);
                    this.f7153 = new byte[this.f7153.length];
                    this.f7154 = 0;
                    r2 = this.f7153.length;
                }
            } else {
                return;
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int m7322() {
        return this.f7153.length - this.f7154;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1342 m7320() {
        return m7321(16384);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1342 m7321(final int i) {
        return new C1342() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C1341 m7324() {
                return new C1345(i);
            }
        };
    }
}
