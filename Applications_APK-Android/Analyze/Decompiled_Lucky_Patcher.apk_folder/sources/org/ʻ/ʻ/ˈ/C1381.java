package org.ʻ.ʻ.ˈ;

import com.google.ʻ.ʼ.C0926;
import java.lang.CharSequence;
import java.lang.Comparable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import org.ʻ.ʻ.C1292;
import org.ʻ.ʻ.ʽ.ʼ.C1134;
import org.ʻ.ʻ.ʾ.C1187;
import org.ʻ.ʻ.ʾ.C1197;
import org.ʻ.ʻ.ʾ.ʽ.C1258;
import org.ʻ.ʻ.ʾ.ʽ.C1259;
import org.ʻ.ʻ.ʾ.ʽ.C1260;
import org.ʻ.ʻ.ʾ.ʽ.C1261;
import org.ʻ.ʻ.ʾ.ʽ.C1262;
import org.ʻ.ʻ.ʾ.ʽ.C1264;
import org.ʻ.ʻ.ʾ.ʽ.C1265;
import org.ʻ.ʻ.ˈ.C1340;
import org.ʻ.ʻ.ˈ.C1346;
import org.ʻ.ʻ.ˈ.C1371;
import org.ʻ.ʻ.ˈ.C1378;
import org.ʻ.ʻ.ˈ.C1385;
import org.ʻ.ʻ.ˈ.C1387;
import org.ʻ.ʻ.ˈ.C1390;
import org.ʻ.ʻ.ˈ.C1391;
import org.ʻ.ʻ.ˈ.C1395;
import org.ʻ.ʻ.ˈ.C1396;
import org.ʻ.ʻ.ˈ.C1397;
import org.ʻ.ʻ.ˈ.C1398;
import org.ʻ.ʻ.ˈ.ʻ.C1342;
import org.ʻ.ʻ.ˈ.ʻ.C1343;
import org.ʻ.ʻ.ˈ.ʻ.C1345;

/* renamed from: org.ʻ.ʻ.ˈ.ˈ  reason: contains not printable characters */
/* compiled from: DexWriter */
public abstract class C1381<StringKey extends CharSequence, StringRef extends C1264, TypeKey extends CharSequence, TypeRef extends C1265, ProtoRefKey extends C1261, FieldRefKey extends C1259, MethodRefKey extends C1262, ClassKey extends Comparable<? super ClassKey>, CallSiteKey extends C1258, MethodHandleKey extends C1260, AnnotationKey extends C1187, AnnotationSetKey, TypeListKey, FieldKey, MethodKey, EncodedArrayKey, EncodedValue, AnnotationElement extends C1197, StringSectionType extends C1396<StringKey, StringRef>, TypeSectionType extends C1398<StringKey, TypeKey, TypeRef>, ProtoSectionType extends C1395<StringKey, TypeKey, ProtoRefKey, TypeListKey>, FieldSectionType extends C1387<StringKey, TypeKey, FieldRefKey, FieldKey>, MethodSectionType extends C1391<StringKey, TypeKey, ProtoRefKey, MethodRefKey, MethodKey>, ClassSectionType extends C1378<StringKey, TypeKey, TypeListKey, ClassKey, FieldKey, MethodKey, AnnotationSetKey, EncodedArrayKey>, CallSiteSectionType extends C1371<CallSiteKey, EncodedArrayKey>, MethodHandleSectionType extends C1390<MethodHandleKey, FieldRefKey, MethodRefKey>, TypeListSectionType extends C1397<TypeKey, TypeListKey>, AnnotationSectionType extends C1340<StringKey, TypeKey, AnnotationKey, AnnotationElement, EncodedValue>, AnnotationSetSectionType extends C1346<AnnotationKey, AnnotationSetKey>, EncodedArraySectionType extends C1385<EncodedArrayKey, EncodedValue>> {

    /* renamed from: ˏˏ  reason: contains not printable characters */
    private static Comparator<Map.Entry> f7217 = new Comparator<Map.Entry>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public int compare(Map.Entry entry, Map.Entry entry2) {
            return entry.getKey().toString().compareTo(entry2.getKey().toString());
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final C1292 f7218;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public final FieldSectionType f7219;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected int f7220 = 0;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    public final ClassSectionType f7221;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected int f7222 = 0;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public final MethodSectionType f7223;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected int f7224 = 0;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    public final MethodHandleSectionType f7225;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected int f7226 = 0;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    public final CallSiteSectionType f7227;

    /* renamed from: ˆ  reason: contains not printable characters */
    protected int f7228 = 0;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    public final AnnotationSectionType f7229;

    /* renamed from: ˈ  reason: contains not printable characters */
    protected int f7230 = 0;

    /* renamed from: ˈˈ  reason: contains not printable characters */
    public final EncodedArraySectionType f7231;

    /* renamed from: ˉ  reason: contains not printable characters */
    protected int f7232 = 0;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    public final AnnotationSetSectionType f7233;

    /* renamed from: ˊ  reason: contains not printable characters */
    protected int f7234 = 0;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private Comparator<Map.Entry<? extends CallSiteKey, Integer>> f7235 = new Comparator<Map.Entry<? extends CallSiteKey, Integer>>() {
        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: EncodedArraySectionType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        /* renamed from: ʻ  reason: contains not printable characters */
        public int compare(java.util.Map.Entry<? extends CallSiteKey, java.lang.Integer> r3, java.util.Map.Entry<? extends CallSiteKey, java.lang.Integer> r4) {
            /*
                r2 = this;
                org.ʻ.ʻ.ˈ.ˈ r0 = org.ʻ.ʻ.ˈ.C1381.this
                EncodedArraySectionType r0 = r0.f7231
                org.ʻ.ʻ.ˈ.ˈ r1 = org.ʻ.ʻ.ˈ.C1381.this
                CallSiteSectionType r1 = r1.f7227
                java.lang.Object r3 = r3.getKey()
                org.ʻ.ʻ.ʾ.ʽ.ʻ r3 = (org.ʻ.ʻ.ʾ.ʽ.C1258) r3
                java.lang.Object r3 = r1.m7547(r3)
                int r3 = r0.m7773(r3)
                org.ʻ.ʻ.ˈ.ˈ r0 = org.ʻ.ʻ.ˈ.C1381.this
                EncodedArraySectionType r0 = r0.f7231
                org.ʻ.ʻ.ˈ.ˈ r1 = org.ʻ.ʻ.ˈ.C1381.this
                CallSiteSectionType r1 = r1.f7227
                java.lang.Object r4 = r4.getKey()
                org.ʻ.ʻ.ʾ.ʽ.ʻ r4 = (org.ʻ.ʻ.ʾ.ʽ.C1258) r4
                java.lang.Object r4 = r1.m7547(r4)
                int r4 = r0.m7773(r4)
                int r3 = com.google.ʻ.ʿ.C0935.m5810(r3, r4)
                return r3
            */
            throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.AnonymousClass1.compare(java.util.Map$Entry, java.util.Map$Entry):int");
        }
    };

    /* renamed from: ˋ  reason: contains not printable characters */
    protected int f7236 = 0;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private final C1388<?>[] f7237;

    /* renamed from: ˎ  reason: contains not printable characters */
    protected int f7238 = 0;

    /* renamed from: ˏ  reason: contains not printable characters */
    protected int f7239 = 0;

    /* renamed from: ˑ  reason: contains not printable characters */
    protected int f7240 = 0;

    /* renamed from: י  reason: contains not printable characters */
    protected int f7241 = 0;

    /* renamed from: ـ  reason: contains not printable characters */
    protected int f7242 = 0;

    /* renamed from: ــ  reason: contains not printable characters */
    public final TypeListSectionType f7243;

    /* renamed from: ٴ  reason: contains not printable characters */
    protected int f7244 = 0;

    /* renamed from: ᐧ  reason: contains not printable characters */
    protected int f7245 = 0;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public final TypeSectionType f7246;

    /* renamed from: ᴵ  reason: contains not printable characters */
    protected int f7247 = 0;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public final ProtoSectionType f7248;

    /* renamed from: ᵎ  reason: contains not printable characters */
    protected int f7249 = 0;

    /* renamed from: ᵔ  reason: contains not printable characters */
    protected int f7250 = 0;

    /* renamed from: ᵢ  reason: contains not printable characters */
    protected int f7251 = 0;

    /* renamed from: ⁱ  reason: contains not printable characters */
    protected int f7252 = 0;

    /* renamed from: ﹳ  reason: contains not printable characters */
    protected int f7253 = 0;

    /* renamed from: ﹶ  reason: contains not printable characters */
    protected int f7254 = 0;

    /* renamed from: ﾞ  reason: contains not printable characters */
    protected int f7255 = 0;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public final StringSectionType f7256;

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract C1381<StringKey, StringRef, TypeKey, TypeRef, ProtoRefKey, FieldRefKey, MethodRefKey, ClassKey, CallSiteKey, MethodHandleKey, AnnotationKey, AnnotationSetKey, TypeListKey, FieldKey, MethodKey, EncodedArrayKey, EncodedValue, AnnotationElement, StringSectionType, TypeSectionType, ProtoSectionType, FieldSectionType, MethodSectionType, ClassSectionType, CallSiteSectionType, MethodHandleSectionType, TypeListSectionType, AnnotationSectionType, AnnotationSetSectionType, EncodedArraySectionType>.ʽ m7672();

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m7675(C1381<StringKey, StringRef, TypeKey, TypeRef, ProtoRefKey, FieldRefKey, MethodRefKey, ClassKey, CallSiteKey, MethodHandleKey, AnnotationKey, AnnotationSetKey, TypeListKey, FieldKey, MethodKey, EncodedArrayKey, EncodedValue, AnnotationElement, StringSectionType, TypeSectionType, ProtoSectionType, FieldSectionType, MethodSectionType, ClassSectionType, CallSiteSectionType, MethodHandleSectionType, TypeListSectionType, AnnotationSectionType, AnnotationSetSectionType, EncodedArraySectionType>.ʼ r1, Object obj);

    protected C1381(C1292 r3) {
        this.f7218 = r3;
        C1381<StringKey, StringRef, TypeKey, TypeRef, ProtoRefKey, FieldRefKey, MethodRefKey, ClassKey, CallSiteKey, MethodHandleKey, AnnotationKey, AnnotationSetKey, TypeListKey, FieldKey, MethodKey, EncodedArrayKey, EncodedValue, AnnotationElement, StringSectionType, TypeSectionType, ProtoSectionType, FieldSectionType, MethodSectionType, ClassSectionType, CallSiteSectionType, MethodHandleSectionType, TypeListSectionType, AnnotationSectionType, AnnotationSetSectionType, EncodedArraySectionType>.ʽ r32 = m7672();
        this.f7256 = r32.m7680();
        this.f7246 = r32.m7681();
        this.f7248 = r32.m7682();
        this.f7219 = r32.m7683();
        this.f7223 = r32.m7684();
        this.f7221 = r32.m7685();
        this.f7227 = r32.m7686();
        this.f7225 = r32.m7687();
        this.f7243 = r32.m7688();
        this.f7229 = r32.m7689();
        this.f7233 = r32.m7690();
        this.f7231 = r32.m7691();
        this.f7237 = new C1388[]{this.f7246, this.f7248, this.f7219, this.f7223, this.f7227, this.f7225};
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static <T extends Comparable<? super T>> Comparator<Map.Entry<? extends T, ?>> m7653() {
        return new Comparator<Map.Entry<? extends T, ?>>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public int compare(Map.Entry<? extends T, ?> entry, Map.Entry<? extends T, ?> entry2) {
                return ((Comparable) entry.getKey()).compareTo(entry2.getKey());
            }
        };
    }

    /* renamed from: org.ʻ.ʻ.ˈ.ˈ$ʼ  reason: contains not printable characters */
    /* compiled from: DexWriter */
    protected class C1383 extends C1386<StringKey, TypeKey, FieldRefKey, MethodRefKey, AnnotationElement, ProtoRefKey, MethodHandleKey, EncodedValue> {
        private C1383(C1380 r11) {
            super(r11, C1381.this.f7256, C1381.this.f7246, C1381.this.f7219, C1381.this.f7223, C1381.this.f7248, C1381.this.f7225, C1381.this.f7229);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m7679(Object obj) {
            C1381.this.m7675(this, obj);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private int m7658() {
        return (this.f7256.m7717() * 4) + 112 + (this.f7246.m7717() * 4) + (this.f7248.m7717() * 12) + (this.f7219.m7717() * 8) + (this.f7223.m7717() * 8) + (this.f7221.m7717() * 32) + (this.f7227.m7717() * 4) + (this.f7225.m7717() * 8);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7673(C1343 r2) {
        m7674(r2, C1345.m7320());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7674(C1343 r7, C1342 r8) {
        try {
            int r0 = m7658();
            C1380 r1 = m7645(r7, 0);
            C1380 r2 = m7645(r7, 112);
            C1380 r3 = m7645(r7, r0);
            try {
                m7652(r2, r3);
                m7647(r2);
                m7665(r3);
                m7654(r2);
                m7659(r2);
                m7661(r2);
                r3 = m7645(r7, r2.m7632() + (this.f7221.m7717() * 32) + (this.f7227.m7717() * 4));
                m7664(r3);
                r3.close();
                m7666(r3);
                r3 = m7645(r7, r2.m7632() + (this.f7221.m7717() * 32));
                m7662(r3);
                r3.close();
                m7667(r3);
                m7668(r3);
                m7669(r3);
                m7670(r3);
                m7651(r3, r8.m7315());
                m7657(r2, r3);
                m7671(r3);
                m7648(r1, r0, r3.m7632());
                r1.close();
                r2.close();
            } catch (Throwable th) {
                r1.close();
                r2.close();
                throw th;
            } finally {
                r3.close();
            }
        } finally {
            r7.m7317();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static C1380 m7645(C1343 r1, int i) {
        return new C1380(r1.m7316(i), i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m7652(C1380 r6, C1380 r7) {
        this.f7220 = r6.m7632();
        this.f7236 = r7.m7632();
        ArrayList<Map.Entry> r0 = C0926.m5777(this.f7256.m7716());
        Collections.sort(r0, f7217);
        int i = 0;
        for (Map.Entry entry : r0) {
            int i2 = i + 1;
            entry.setValue(Integer.valueOf(i));
            r6.m7625(r7.m7632());
            String charSequence = ((CharSequence) entry.getKey()).toString();
            r7.m7640(charSequence.length());
            r7.m7631(charSequence);
            r7.write(0);
            i = i2;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: StringSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m7647(org.ʻ.ʻ.ˈ.C1380 r6) {
        /*
            r5 = this;
            int r0 = r6.m7632()
            r5.f7222 = r0
            TypeSectionType r0 = r5.f7246
            java.util.Collection r0 = r0.m7716()
            java.util.ArrayList r0 = com.google.ʻ.ʼ.C0926.m5777(r0)
            java.util.Comparator<java.util.Map$Entry> r1 = org.ʻ.ʻ.ˈ.C1381.f7217
            java.util.Collections.sort(r0, r1)
            java.util.Iterator r0 = r0.iterator()
            r1 = 0
        L_0x001a:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x0044
            java.lang.Object r2 = r0.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            int r3 = r1 + 1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r2.setValue(r1)
            StringSectionType r1 = r5.f7256
            TypeSectionType r4 = r5.f7246
            java.lang.Object r2 = r2.getKey()
            java.lang.Object r2 = r4.m7782(r2)
            int r1 = r1.m7718(r2)
            r6.m7625(r1)
            r1 = r3
            goto L_0x001a
        L_0x0044:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7647(org.ʻ.ʻ.ˈ.ˆ):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: StringSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ʼ  reason: contains not printable characters */
    private void m7654(org.ʻ.ʻ.ˈ.C1380 r6) {
        /*
            r5 = this;
            int r0 = r6.m7632()
            r5.f7224 = r0
            ProtoSectionType r0 = r5.f7248
            java.util.Collection r0 = r0.m7716()
            java.util.ArrayList r0 = com.google.ʻ.ʼ.C0926.m5777(r0)
            java.util.Comparator r1 = m7653()
            java.util.Collections.sort(r0, r1)
            java.util.Iterator r0 = r0.iterator()
            r1 = 0
        L_0x001c:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x0066
            java.lang.Object r2 = r0.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            int r3 = r1 + 1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r2.setValue(r1)
            java.lang.Object r1 = r2.getKey()
            org.ʻ.ʻ.ʾ.ʽ.ʾ r1 = (org.ʻ.ʻ.ʾ.ʽ.C1261) r1
            StringSectionType r2 = r5.f7256
            ProtoSectionType r4 = r5.f7248
            java.lang.Object r4 = r4.m7774(r1)
            int r2 = r2.m7718(r4)
            r6.m7625(r2)
            TypeSectionType r2 = r5.f7246
            ProtoSectionType r4 = r5.f7248
            java.lang.Object r4 = r4.m7775(r1)
            int r2 = r2.m7718(r4)
            r6.m7625(r2)
            TypeListSectionType r2 = r5.f7243
            ProtoSectionType r4 = r5.f7248
            java.lang.Object r1 = r4.m7776(r1)
            int r1 = r2.m7780(r1)
            r6.m7625(r1)
            r1 = r3
            goto L_0x001c
        L_0x0066:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7654(org.ʻ.ʻ.ˈ.ˆ):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: TypeSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ʽ  reason: contains not printable characters */
    private void m7659(org.ʻ.ʻ.ˈ.C1380 r6) {
        /*
            r5 = this;
            int r0 = r6.m7632()
            r5.f7226 = r0
            FieldSectionType r0 = r5.f7219
            java.util.Collection r0 = r0.m7716()
            java.util.ArrayList r0 = com.google.ʻ.ʼ.C0926.m5777(r0)
            java.util.Comparator r1 = m7653()
            java.util.Collections.sort(r0, r1)
            java.util.Iterator r0 = r0.iterator()
            r1 = 0
        L_0x001c:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x0066
            java.lang.Object r2 = r0.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            int r3 = r1 + 1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r2.setValue(r1)
            java.lang.Object r1 = r2.getKey()
            org.ʻ.ʻ.ʾ.ʽ.ʼ r1 = (org.ʻ.ʻ.ʾ.ʽ.C1259) r1
            TypeSectionType r2 = r5.f7246
            FieldSectionType r4 = r5.f7219
            java.lang.Object r4 = r4.m7713(r1)
            int r2 = r2.m7718(r4)
            r6.m7636(r2)
            TypeSectionType r2 = r5.f7246
            FieldSectionType r4 = r5.f7219
            java.lang.Object r4 = r4.m7714(r1)
            int r2 = r2.m7718(r4)
            r6.m7636(r2)
            StringSectionType r2 = r5.f7256
            FieldSectionType r4 = r5.f7219
            java.lang.Object r1 = r4.m7715(r1)
            int r1 = r2.m7718(r1)
            r6.m7625(r1)
            r1 = r3
            goto L_0x001c
        L_0x0066:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7659(org.ʻ.ʻ.ˈ.ˆ):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: TypeSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ʾ  reason: contains not printable characters */
    private void m7661(org.ʻ.ʻ.ˈ.C1380 r6) {
        /*
            r5 = this;
            int r0 = r6.m7632()
            r5.f7228 = r0
            MethodSectionType r0 = r5.f7223
            java.util.Collection r0 = r0.m7716()
            java.util.ArrayList r0 = com.google.ʻ.ʼ.C0926.m5777(r0)
            java.util.Comparator r1 = m7653()
            java.util.Collections.sort(r0, r1)
            java.util.Iterator r0 = r0.iterator()
            r1 = 0
        L_0x001c:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x0066
            java.lang.Object r2 = r0.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            int r3 = r1 + 1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r2.setValue(r1)
            java.lang.Object r1 = r2.getKey()
            org.ʻ.ʻ.ʾ.ʽ.ʿ r1 = (org.ʻ.ʻ.ʾ.ʽ.C1262) r1
            TypeSectionType r2 = r5.f7246
            MethodSectionType r4 = r5.f7223
            java.lang.Object r4 = r4.m7764(r1)
            int r2 = r2.m7718(r4)
            r6.m7636(r2)
            ProtoSectionType r2 = r5.f7248
            MethodSectionType r4 = r5.f7223
            org.ʻ.ʻ.ʾ.ʽ.ʾ r4 = r4.m7767(r1)
            int r2 = r2.m7718(r4)
            r6.m7636(r2)
            StringSectionType r2 = r5.f7256
            MethodSectionType r4 = r5.f7223
            java.lang.Object r1 = r4.m7769(r1)
            int r1 = r2.m7718(r1)
            r6.m7625(r1)
            r1 = r3
            goto L_0x001c
        L_0x0066:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7661(org.ʻ.ʻ.ˈ.ˆ):void");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m7657(C1380 r4, C1380 r5) {
        this.f7230 = r4.m7632();
        this.f7238 = r5.m7632();
        ArrayList<Map.Entry> r0 = C0926.m5777(this.f7221.m7716());
        Collections.sort(r0, m7653());
        int i = 0;
        for (Map.Entry r2 : r0) {
            i = m7644(r4, r5, i, r2);
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ClassSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ʻ  reason: contains not printable characters */
    private int m7644(org.ʻ.ʻ.ˈ.C1380 r9, org.ʻ.ʻ.ˈ.C1380 r10, int r11, java.util.Map.Entry<? extends ClassKey, java.lang.Integer> r12) {
        /*
            r8 = this;
            if (r12 != 0) goto L_0x0003
            return r11
        L_0x0003:
            java.lang.Object r0 = r12.getValue()
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            r1 = -1
            if (r0 == r1) goto L_0x0011
            return r11
        L_0x0011:
            java.lang.Object r0 = r12.getKey()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            r1 = 0
            java.lang.Integer r2 = java.lang.Integer.valueOf(r1)
            r12.setValue(r2)
            ClassSectionType r2 = r8.f7221
            java.lang.CharSequence r3 = r2.m7584(r0)
            java.util.Map$Entry r2 = r2.m7579(r3)
            int r11 = r8.m7644(r9, r10, r11, r2)
            TypeListSectionType r2 = r8.f7243
            ClassSectionType r3 = r8.f7221
            java.lang.Object r3 = r3.m7586(r0)
            java.util.Collection r2 = r2.m7779(r3)
            java.util.Iterator r2 = r2.iterator()
        L_0x003d:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0054
            java.lang.Object r3 = r2.next()
            java.lang.CharSequence r3 = (java.lang.CharSequence) r3
            ClassSectionType r4 = r8.f7221
            java.util.Map$Entry r3 = r4.m7579(r3)
            int r11 = r8.m7644(r9, r10, r11, r3)
            goto L_0x003d
        L_0x0054:
            int r2 = r11 + 1
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)
            r12.setValue(r11)
            TypeSectionType r11 = r8.f7246
            ClassSectionType r12 = r8.f7221
            java.lang.CharSequence r12 = r12.m7576(r0)
            int r11 = r11.m7718(r12)
            r9.m7625(r11)
            ClassSectionType r11 = r8.f7221
            int r11 = r11.m7582(r0)
            r9.m7625(r11)
            TypeSectionType r11 = r8.f7246
            ClassSectionType r12 = r8.f7221
            java.lang.CharSequence r12 = r12.m7584(r0)
            int r11 = r11.m7770(r12)
            r9.m7625(r11)
            TypeListSectionType r11 = r8.f7243
            ClassSectionType r12 = r8.f7221
            java.lang.Object r12 = r12.m7586(r0)
            int r11 = r11.m7780(r12)
            r9.m7625(r11)
            StringSectionType r11 = r8.f7256
            ClassSectionType r12 = r8.f7221
            java.lang.CharSequence r12 = r12.m7587(r0)
            int r11 = r11.m7770(r12)
            r9.m7625(r11)
            ClassSectionType r11 = r8.f7221
            int r11 = r11.m7608(r0)
            r9.m7625(r11)
            ClassSectionType r11 = r8.f7221
            java.util.Collection r11 = r11.m7589(r0)
            ClassSectionType r12 = r8.f7221
            java.util.Collection r12 = r12.m7590(r0)
            ClassSectionType r3 = r8.f7221
            java.util.Collection r3 = r3.m7592(r0)
            ClassSectionType r4 = r8.f7221
            java.util.Collection r4 = r4.m7593(r0)
            int r5 = r11.size()
            r6 = 1
            if (r5 > 0) goto L_0x00df
            int r5 = r12.size()
            if (r5 > 0) goto L_0x00df
            int r5 = r3.size()
            if (r5 > 0) goto L_0x00df
            int r5 = r4.size()
            if (r5 <= 0) goto L_0x00dd
            goto L_0x00df
        L_0x00dd:
            r5 = 0
            goto L_0x00e0
        L_0x00df:
            r5 = 1
        L_0x00e0:
            if (r5 == 0) goto L_0x00ea
            int r7 = r10.m7632()
            r9.m7625(r7)
            goto L_0x00ed
        L_0x00ea:
            r9.m7625(r1)
        L_0x00ed:
            ClassSectionType r7 = r8.f7221
            java.lang.Object r0 = r7.m7588(r0)
            if (r0 == 0) goto L_0x00ff
            EncodedArraySectionType r1 = r8.f7231
            int r0 = r1.m7773(r0)
            r9.m7625(r0)
            goto L_0x0102
        L_0x00ff:
            r9.m7625(r1)
        L_0x0102:
            if (r5 == 0) goto L_0x0131
            int r9 = r8.f7255
            int r9 = r9 + r6
            r8.f7255 = r9
            int r9 = r11.size()
            r10.m7640(r9)
            int r9 = r12.size()
            r10.m7640(r9)
            int r9 = r3.size()
            r10.m7640(r9)
            int r9 = r4.size()
            r10.m7640(r9)
            r8.m7650(r10, r11)
            r8.m7650(r10, r12)
            r8.m7656(r10, r3)
            r8.m7656(r10, r4)
        L_0x0131:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7644(org.ʻ.ʻ.ˈ.ˆ, org.ʻ.ʻ.ˈ.ˆ, int, java.util.Map$Entry):int");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: EncodedArraySectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ʿ  reason: contains not printable characters */
    private void m7662(org.ʻ.ʻ.ˈ.C1380 r6) {
        /*
            r5 = this;
            int r0 = r6.m7632()
            r5.f7232 = r0
            CallSiteSectionType r0 = r5.f7227
            java.util.Collection r0 = r0.m7716()
            java.util.ArrayList r0 = com.google.ʻ.ʼ.C0926.m5777(r0)
            java.util.Comparator<java.util.Map$Entry<? extends CallSiteKey, java.lang.Integer>> r1 = r5.f7235
            java.util.Collections.sort(r0, r1)
            java.util.Iterator r0 = r0.iterator()
            r1 = 0
        L_0x001a:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x0046
            java.lang.Object r2 = r0.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            int r3 = r1 + 1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r2.setValue(r1)
            EncodedArraySectionType r1 = r5.f7231
            CallSiteSectionType r4 = r5.f7227
            java.lang.Object r2 = r2.getKey()
            org.ʻ.ʻ.ʾ.ʽ.ʻ r2 = (org.ʻ.ʻ.ʾ.ʽ.C1258) r2
            java.lang.Object r2 = r4.m7547(r2)
            int r1 = r1.m7773(r2)
            r6.m7625(r1)
            r1 = r3
            goto L_0x001a
        L_0x0046:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7662(org.ʻ.ʻ.ˈ.ˆ):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MethodSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ˆ  reason: contains not printable characters */
    private void m7664(org.ʻ.ʻ.ˈ.C1380 r7) {
        /*
            r6 = this;
            int r0 = r7.m7632()
            r6.f7234 = r0
            MethodHandleSectionType r0 = r6.f7225
            java.util.Collection r0 = r0.m7716()
            java.util.Iterator r0 = r0.iterator()
            r1 = 0
            r2 = 0
        L_0x0012:
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto L_0x0074
            java.lang.Object r3 = r0.next()
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            int r4 = r2 + 1
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r3.setValue(r2)
            java.lang.Object r2 = r3.getKey()
            org.ʻ.ʻ.ʾ.ʽ.ʽ r2 = (org.ʻ.ʻ.ʾ.ʽ.C1260) r2
            int r3 = r2.m7068()
            r7.m7636(r3)
            r7.m7636(r1)
            int r3 = r2.m7068()
            switch(r3) {
                case 0: goto L_0x0060;
                case 1: goto L_0x0060;
                case 2: goto L_0x0060;
                case 3: goto L_0x0060;
                case 4: goto L_0x0053;
                case 5: goto L_0x0053;
                case 6: goto L_0x0053;
                case 7: goto L_0x0053;
                case 8: goto L_0x0053;
                default: goto L_0x003e;
            }
        L_0x003e:
            org.ʻ.ʼ.ʿ r7 = new org.ʻ.ʼ.ʿ
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]
            int r2 = r2.m7068()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r0[r1] = r2
            java.lang.String r1 = "Invalid method handle type: %d"
            r7.<init>(r1, r0)
            throw r7
        L_0x0053:
            MethodSectionType r3 = r6.f7223
            MethodHandleSectionType r5 = r6.f7225
            org.ʻ.ʻ.ʾ.ʽ.ʿ r2 = r5.m7763(r2)
            int r2 = r3.m7718(r2)
            goto L_0x006c
        L_0x0060:
            FieldSectionType r3 = r6.f7219
            MethodHandleSectionType r5 = r6.f7225
            org.ʻ.ʻ.ʾ.ʽ.ʼ r2 = r5.m7762(r2)
            int r2 = r3.m7718(r2)
        L_0x006c:
            r7.m7636(r2)
            r7.m7636(r1)
            r2 = r4
            goto L_0x0012
        L_0x0074:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7664(org.ʻ.ʻ.ˈ.ˆ):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: FieldSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m7650(org.ʻ.ʻ.ˈ.C1380 r4, java.util.Collection r5) {
        /*
            r3 = this;
            java.util.Iterator r5 = r5.iterator()
            r0 = 0
        L_0x0005:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x0025
            java.lang.Object r1 = r5.next()
            FieldSectionType r2 = r3.f7219
            int r2 = r2.m7712(r1)
            int r0 = r2 - r0
            r4.m7640(r0)
            ClassSectionType r0 = r3.f7221
            int r0 = r0.m7595(r1)
            r4.m7640(r0)
            r0 = r2
            goto L_0x0005
        L_0x0025:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7650(org.ʻ.ʻ.ˈ.ˆ, java.util.Collection):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MethodSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ʼ  reason: contains not printable characters */
    private void m7656(org.ʻ.ʻ.ˈ.C1380 r4, java.util.Collection<? extends MethodKey> r5) {
        /*
            r3 = this;
            java.util.Iterator r5 = r5.iterator()
            r0 = 0
        L_0x0005:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x002e
            java.lang.Object r1 = r5.next()
            MethodSectionType r2 = r3.f7223
            int r2 = r2.m7768(r1)
            int r0 = r2 - r0
            r4.m7640(r0)
            ClassSectionType r0 = r3.f7221
            int r0 = r0.m7596(r1)
            r4.m7640(r0)
            ClassSectionType r0 = r3.f7221
            int r0 = r0.m7600(r1)
            r4.m7640(r0)
            r0 = r2
            goto L_0x0005
        L_0x002e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7656(org.ʻ.ʻ.ˈ.ˆ, java.util.Collection):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: TypeListSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ˈ  reason: contains not printable characters */
    private void m7665(org.ʻ.ʻ.ˈ.C1380 r5) {
        /*
            r4 = this;
            r5.m7624()
            int r0 = r5.m7632()
            r4.f7239 = r0
            TypeListSectionType r0 = r4.f7243
            java.util.Collection r0 = r0.m7772()
            java.util.Iterator r0 = r0.iterator()
        L_0x0013:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0058
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            r5.m7624()
            int r2 = r5.m7632()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.setValue(r2)
            TypeListSectionType r2 = r4.f7243
            java.lang.Object r1 = r1.getKey()
            java.util.Collection r1 = r2.m7779(r1)
            int r2 = r1.size()
            r5.m7625(r2)
            java.util.Iterator r1 = r1.iterator()
        L_0x0042:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0013
            java.lang.Object r2 = r1.next()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            TypeSectionType r3 = r4.f7246
            int r2 = r3.m7718(r2)
            r5.m7636(r2)
            goto L_0x0042
        L_0x0058:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7665(org.ʻ.ʻ.ˈ.ˆ):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: EncodedArraySectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ˉ  reason: contains not printable characters */
    private void m7666(org.ʻ.ʻ.ˈ.C1380 r5) {
        /*
            r4 = this;
            org.ʻ.ʻ.ˈ.ˈ$ʼ r0 = new org.ʻ.ʻ.ˈ.ˈ$ʼ
            r1 = 0
            r0.<init>(r5)
            int r1 = r5.m7632()
            r4.f7240 = r1
            EncodedArraySectionType r1 = r4.f7231
            java.util.Collection r1 = r1.m7772()
            java.util.Iterator r1 = r1.iterator()
        L_0x0016:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0050
            java.lang.Object r2 = r1.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            int r3 = r5.m7632()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2.setValue(r3)
            EncodedArraySectionType r3 = r4.f7231
            java.lang.Object r2 = r2.getKey()
            java.util.List r2 = r3.m7692(r2)
            int r3 = r2.size()
            r5.m7640(r3)
            java.util.Iterator r2 = r2.iterator()
        L_0x0042:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0016
            java.lang.Object r3 = r2.next()
            r4.m7675(r0, r3)
            goto L_0x0042
        L_0x0050:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7666(org.ʻ.ʻ.ˈ.ˆ):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: AnnotationSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ˊ  reason: contains not printable characters */
    private void m7667(org.ʻ.ʻ.ˈ.C1380 r7) {
        /*
            r6 = this;
            org.ʻ.ʻ.ˈ.ˈ$ʼ r0 = new org.ʻ.ʻ.ˈ.ˈ$ʼ
            r1 = 0
            r0.<init>(r7)
            int r1 = r7.m7632()
            r6.f7241 = r1
            AnnotationSectionType r1 = r6.f7229
            java.util.Collection r1 = r1.m7772()
            java.util.Iterator r1 = r1.iterator()
        L_0x0016:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x008b
            java.lang.Object r2 = r1.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            int r3 = r7.m7632()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2.setValue(r3)
            java.lang.Object r2 = r2.getKey()
            org.ʻ.ʻ.ʾ.ʻ r2 = (org.ʻ.ʻ.ʾ.C1187) r2
            AnnotationSectionType r3 = r6.f7229
            int r3 = r3.m7309(r2)
            r7.m7638(r3)
            TypeSectionType r3 = r6.f7246
            AnnotationSectionType r4 = r6.f7229
            java.lang.Object r4 = r4.m7310(r2)
            int r3 = r3.m7718(r4)
            r7.m7640(r3)
            java.util.Comparator<org.ʻ.ʻ.ʾ.ʼ> r3 = org.ʻ.ʻ.ʻ.C1011.f6318
            com.google.ʻ.ʼ.ʽʽ r3 = com.google.ʻ.ʼ.C0858.m5446(r3)
            AnnotationSectionType r4 = r6.f7229
            java.util.Collection r2 = r4.m7311(r2)
            com.google.ʻ.ʼ.י r2 = r3.m5451(r2)
            int r3 = r2.size()
            r7.m7640(r3)
            java.util.Iterator r2 = r2.iterator()
        L_0x0066:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0016
            java.lang.Object r3 = r2.next()
            org.ʻ.ʻ.ʾ.ʼ r3 = (org.ʻ.ʻ.ʾ.C1197) r3
            StringSectionType r4 = r6.f7256
            AnnotationSectionType r5 = r6.f7229
            java.lang.Object r5 = r5.m7312(r3)
            int r4 = r4.m7718(r5)
            r7.m7640(r4)
            AnnotationSectionType r4 = r6.f7229
            java.lang.Object r3 = r4.m7313(r3)
            r6.m7675(r0, r3)
            goto L_0x0066
        L_0x008b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7667(org.ʻ.ʻ.ˈ.ˆ):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: AnnotationSetSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ˋ  reason: contains not printable characters */
    private void m7668(org.ʻ.ʻ.ˈ.C1380 r6) {
        /*
            r5 = this;
            r6.m7624()
            int r0 = r6.m7632()
            r5.f7242 = r0
            boolean r0 = r5.m7663()
            if (r0 == 0) goto L_0x0013
            r0 = 0
            r6.m7625(r0)
        L_0x0013:
            AnnotationSetSectionType r0 = r5.f7233
            java.util.Collection r0 = r0.m7772()
            java.util.Iterator r0 = r0.iterator()
        L_0x001d:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x006c
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.util.Comparator<? super org.ʻ.ʻ.ʾ.ʻ> r2 = org.ʻ.ʻ.ʻ.C1002.f6317
            com.google.ʻ.ʼ.ʽʽ r2 = com.google.ʻ.ʼ.C0858.m5446(r2)
            AnnotationSetSectionType r3 = r5.f7233
            java.lang.Object r4 = r1.getKey()
            java.util.Collection r3 = r3.m7325(r4)
            com.google.ʻ.ʼ.י r2 = r2.m5451(r3)
            r6.m7624()
            int r3 = r6.m7632()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r1.setValue(r3)
            int r1 = r2.size()
            r6.m7625(r1)
            java.util.Iterator r1 = r2.iterator()
        L_0x0056:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x001d
            java.lang.Object r2 = r1.next()
            org.ʻ.ʻ.ʾ.ʻ r2 = (org.ʻ.ʻ.ʾ.C1187) r2
            AnnotationSectionType r3 = r5.f7229
            int r2 = r3.m7773(r2)
            r6.m7625(r2)
            goto L_0x0056
        L_0x006c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7668(org.ʻ.ʻ.ˈ.ˆ):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ClassSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ˎ  reason: contains not printable characters */
    private void m7669(org.ʻ.ʻ.ˈ.C1380 r8) {
        /*
            r7 = this;
            r8.m7624()
            int r0 = r8.m7632()
            r7.f7244 = r0
            java.util.HashMap r0 = com.google.ʻ.ʼ.C0929.m5793()
            ClassSectionType r1 = r7.f7221
            java.util.Collection r1 = r1.m7578()
            java.util.Iterator r1 = r1.iterator()
        L_0x0017:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x00a6
            java.lang.Object r2 = r1.next()
            java.lang.Comparable r2 = (java.lang.Comparable) r2
            ClassSectionType r3 = r7.f7221
            java.util.Collection r2 = r3.m7594(r2)
            java.util.Iterator r2 = r2.iterator()
        L_0x002d:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0017
            java.lang.Object r3 = r2.next()
            ClassSectionType r4 = r7.f7221
            java.util.List r4 = r4.m7601(r3)
            if (r4 == 0) goto L_0x002d
            java.lang.Object r5 = r0.get(r4)
            java.lang.Integer r5 = (java.lang.Integer) r5
            if (r5 == 0) goto L_0x0051
            ClassSectionType r4 = r7.f7221
            int r5 = r5.intValue()
            r4.m7583(r3, r5)
            goto L_0x002d
        L_0x0051:
            r8.m7624()
            int r5 = r8.m7632()
            ClassSectionType r6 = r7.f7221
            r6.m7583(r3, r5)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r5)
            r0.put(r4, r3)
            int r3 = r7.f7251
            int r3 = r3 + 1
            r7.f7251 = r3
            int r3 = r4.size()
            r8.m7625(r3)
            java.util.Iterator r3 = r4.iterator()
        L_0x0075:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x002d
            java.lang.Object r4 = r3.next()
            AnnotationSetSectionType r5 = r7.f7233
            java.util.Collection r5 = r5.m7325(r4)
            int r5 = r5.size()
            if (r5 <= 0) goto L_0x0095
            AnnotationSetSectionType r5 = r7.f7233
            int r4 = r5.m7773(r4)
            r8.m7625(r4)
            goto L_0x0075
        L_0x0095:
            boolean r4 = r7.m7663()
            if (r4 == 0) goto L_0x00a1
            int r4 = r7.f7242
            r8.m7625(r4)
            goto L_0x0075
        L_0x00a1:
            r4 = 0
            r8.m7625(r4)
            goto L_0x0075
        L_0x00a6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7669(org.ʻ.ʻ.ˈ.ˆ):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ClassSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ˏ  reason: contains not printable characters */
    private void m7670(org.ʻ.ʻ.ˈ.C1380 r13) {
        /*
            r12 = this;
            r13.m7624()
            int r0 = r13.m7632()
            r12.f7245 = r0
            java.util.HashMap r0 = com.google.ʻ.ʼ.C0929.m5793()
            r1 = 65536(0x10000, float:9.18355E-41)
            java.nio.ByteBuffer r1 = java.nio.ByteBuffer.allocate(r1)
            java.nio.ByteOrder r2 = java.nio.ByteOrder.LITTLE_ENDIAN
            r1.order(r2)
            ClassSectionType r2 = r12.f7221
            java.util.Collection r2 = r2.m7578()
            java.util.Iterator r2 = r2.iterator()
        L_0x0022:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0132
            java.lang.Object r3 = r2.next()
            java.lang.Comparable r3 = (java.lang.Comparable) r3
            ClassSectionType r4 = r12.f7221
            java.util.Collection r4 = r4.m7591(r3)
            ClassSectionType r5 = r12.f7221
            java.util.Collection r5 = r5.m7594(r3)
            int r6 = r4.size()
            int r6 = r6 * 8
            int r7 = r5.size()
            int r7 = r7 * 16
            int r6 = r6 + r7
            int r7 = r1.capacity()
            if (r6 <= r7) goto L_0x0056
            java.nio.ByteBuffer r1 = java.nio.ByteBuffer.allocate(r6)
            java.nio.ByteOrder r6 = java.nio.ByteOrder.LITTLE_ENDIAN
            r1.order(r6)
        L_0x0056:
            r1.clear()
            java.util.Iterator r4 = r4.iterator()
            r6 = 0
            r7 = 0
        L_0x005f:
            boolean r8 = r4.hasNext()
            if (r8 == 0) goto L_0x0086
            java.lang.Object r8 = r4.next()
            ClassSectionType r9 = r12.f7221
            java.lang.Object r9 = r9.m7598(r8)
            if (r9 == 0) goto L_0x005f
            int r7 = r7 + 1
            FieldSectionType r10 = r12.f7219
            int r8 = r10.m7712(r8)
            r1.putInt(r8)
            AnnotationSetSectionType r8 = r12.f7233
            int r8 = r8.m7773(r9)
            r1.putInt(r8)
            goto L_0x005f
        L_0x0086:
            java.util.Iterator r4 = r5.iterator()
            r8 = 0
        L_0x008b:
            boolean r9 = r4.hasNext()
            if (r9 == 0) goto L_0x00b2
            java.lang.Object r9 = r4.next()
            ClassSectionType r10 = r12.f7221
            java.lang.Object r10 = r10.m7599(r9)
            if (r10 == 0) goto L_0x008b
            int r8 = r8 + 1
            MethodSectionType r11 = r12.f7223
            int r9 = r11.m7768(r9)
            r1.putInt(r9)
            AnnotationSetSectionType r9 = r12.f7233
            int r9 = r9.m7773(r10)
            r1.putInt(r9)
            goto L_0x008b
        L_0x00b2:
            java.util.Iterator r4 = r5.iterator()
            r5 = 0
        L_0x00b7:
            boolean r9 = r4.hasNext()
            if (r9 == 0) goto L_0x00d8
            java.lang.Object r9 = r4.next()
            ClassSectionType r10 = r12.f7221
            int r10 = r10.m7609(r9)
            if (r10 == 0) goto L_0x00b7
            int r5 = r5 + 1
            MethodSectionType r11 = r12.f7223
            int r9 = r11.m7768(r9)
            r1.putInt(r9)
            r1.putInt(r10)
            goto L_0x00b7
        L_0x00d8:
            ClassSectionType r4 = r12.f7221
            java.lang.Object r4 = r4.m7597(r3)
            if (r7 != 0) goto L_0x0104
            if (r8 != 0) goto L_0x0104
            if (r5 != 0) goto L_0x0104
            if (r4 == 0) goto L_0x0022
            java.lang.Object r9 = r0.get(r4)
            java.lang.Integer r9 = (java.lang.Integer) r9
            if (r9 == 0) goto L_0x00f9
            ClassSectionType r4 = r12.f7221
            int r5 = r9.intValue()
            r4.m7580(r3, r5)
            goto L_0x0022
        L_0x00f9:
            int r9 = r13.m7632()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r0.put(r4, r9)
        L_0x0104:
            int r9 = r12.f7252
            int r9 = r9 + 1
            r12.f7252 = r9
            ClassSectionType r9 = r12.f7221
            int r10 = r13.m7632()
            r9.m7580(r3, r10)
            AnnotationSetSectionType r3 = r12.f7233
            int r3 = r3.m7771(r4)
            r13.m7625(r3)
            r13.m7625(r7)
            r13.m7625(r8)
            r13.m7625(r5)
            byte[] r3 = r1.array()
            int r4 = r1.position()
            r13.write(r3, r6, r4)
            goto L_0x0022
        L_0x0132:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7670(org.ʻ.ʻ.ˈ.ˆ):void");
    }

    /* renamed from: org.ʻ.ʻ.ˈ.ˈ$ʻ  reason: contains not printable characters */
    /* compiled from: DexWriter */
    private static class C1382<MethodKey> {

        /* renamed from: ʻ  reason: contains not printable characters */
        MethodKey f7259;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f7260;

        private C1382(MethodKey methodkey, int i) {
            this.f7260 = i;
            this.f7259 = methodkey;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, java.lang.Iterable):java.lang.Iterable<T>
     arg types: [java.util.Collection, java.util.Collection]
     candidates:
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, int):java.lang.Iterable<T>
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, com.google.ʻ.ʻ.ʽ):java.lang.Iterable<T>
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, java.lang.Object):T
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, com.google.ʻ.ʻ.ˉ):boolean
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, java.lang.Iterable):java.lang.Iterable<T> */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ClassSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00db  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00e4 A[SYNTHETIC] */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m7651(org.ʻ.ʻ.ˈ.C1380 r19, org.ʻ.ʻ.ˈ.ʻ.C1341 r20) {
        /*
            r18 = this;
            r8 = r18
            r0 = r19
            r9 = r20
            java.io.ByteArrayOutputStream r10 = new java.io.ByteArrayOutputStream
            r10.<init>()
            int r1 = r19.m7632()
            r8.f7247 = r1
            org.ʻ.ʻ.ˈ.ʿ r11 = new org.ʻ.ʻ.ˈ.ʿ
            StringSectionType r1 = r8.f7256
            TypeSectionType r2 = r8.f7246
            r11.<init>(r1, r2, r0)
            org.ʻ.ʻ.ˈ.ˆ r12 = new org.ʻ.ʻ.ˈ.ˆ
            r13 = 0
            r12.<init>(r9, r13)
            java.util.ArrayList r14 = com.google.ʻ.ʼ.C0926.m5776()
            ClassSectionType r1 = r8.f7221
            java.util.Collection r1 = r1.m7578()
            java.util.Iterator r15 = r1.iterator()
        L_0x002e:
            boolean r1 = r15.hasNext()
            if (r1 == 0) goto L_0x00ff
            java.lang.Object r1 = r15.next()
            java.lang.Comparable r1 = (java.lang.Comparable) r1
            ClassSectionType r2 = r8.f7221
            java.util.Collection r2 = r2.m7592(r1)
            ClassSectionType r3 = r8.f7221
            java.util.Collection r1 = r3.m7593(r1)
            java.lang.Iterable r1 = com.google.ʻ.ʼ.C0918.m5743(r2, r1)
            java.util.Iterator r16 = r1.iterator()
        L_0x004e:
            boolean r1 = r16.hasNext()
            if (r1 == 0) goto L_0x002e
            java.lang.Object r7 = r16.next()
            ClassSectionType r1 = r8.f7221
            java.util.List r1 = r1.m7606(r7)
            ClassSectionType r2 = r8.f7221
            java.lang.Iterable r2 = r2.m7605(r7)
            ClassSectionType r3 = r8.f7221
            java.lang.Iterable r3 = r3.m7602(r7)
            if (r2 == 0) goto L_0x00bd
            StringSectionType r4 = r8.f7256
            boolean r4 = r4.m7778()
            if (r4 == 0) goto L_0x00bd
            java.util.Iterator r4 = r2.iterator()
        L_0x0078:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x00a2
            java.lang.Object r5 = r4.next()
            org.ʻ.ʻ.ʾ.ʼ.ˆ r5 = (org.ʻ.ʻ.ʾ.ʼ.C1240) r5
            org.ʻ.ʻ.ʾ r6 = r5.m7034()
            org.ʻ.ʻ.ʾ r13 = org.ʻ.ʻ.C1185.CONST_STRING
            if (r6 != r13) goto L_0x00a0
            StringSectionType r6 = r8.f7256
            org.ʻ.ʻ.ʾ.ʼ.ˑ r5 = (org.ʻ.ʻ.ʾ.ʼ.C1247) r5
            org.ʻ.ʻ.ʾ.ʽ.ˆ r5 = r5.m7038()
            org.ʻ.ʻ.ʾ.ʽ.ˈ r5 = (org.ʻ.ʻ.ʾ.ʽ.C1264) r5
            int r5 = r6.m7777(r5)
            r6 = 65536(0x10000, float:9.18355E-41)
            if (r5 < r6) goto L_0x00a0
            r4 = 1
            goto L_0x00a3
        L_0x00a0:
            r13 = 0
            goto L_0x0078
        L_0x00a2:
            r4 = 0
        L_0x00a3:
            if (r4 == 0) goto L_0x00bd
            ClassSectionType r1 = r8.f7221
            org.ʻ.ʻ.ʼ.ˑ r1 = r1.m7607(r7)
            r8.m7646(r1)
            java.util.List r2 = r1.m6567()
            java.util.List r3 = r1.m6565()
            java.lang.Iterable r1 = r1.m6566()
            r6 = r2
            r5 = r3
            goto L_0x00c0
        L_0x00bd:
            r5 = r1
            r6 = r2
            r1 = r3
        L_0x00c0:
            ClassSectionType r2 = r8.f7221
            java.lang.Iterable r2 = r2.m7603(r7)
            int r13 = r8.m7643(r0, r11, r2, r1)
            r1 = r18
            r2 = r12
            r3 = r10
            r4 = r7
            r17 = r10
            r10 = 1
            r10 = r7
            r7 = r13
            int r1 = r1.m7642(r2, r3, r4, r5, r6, r7)     // Catch:{ RuntimeException -> 0x00e9 }
            r2 = -1
            if (r1 == r2) goto L_0x00e4
            org.ʻ.ʻ.ˈ.ˈ$ʻ r2 = new org.ʻ.ʻ.ˈ.ˈ$ʻ
            r3 = 0
            r2.<init>(r10, r1)
            r14.add(r2)
        L_0x00e4:
            r10 = r17
            r13 = 0
            goto L_0x004e
        L_0x00e9:
            r0 = move-exception
            r1 = r0
            org.ʻ.ʼ.ʿ r0 = new org.ʻ.ʼ.ʿ
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            MethodSectionType r3 = r8.f7223
            org.ʻ.ʻ.ʾ.ʽ.ʿ r3 = r3.m7765(r10)
            r4 = 0
            r2[r4] = r3
            java.lang.String r3 = "Exception occurred while writing code_item for method %s"
            r0.<init>(r1, r3, r2)
            throw r0
        L_0x00ff:
            r19.m7624()
            int r1 = r19.m7632()
            r8.f7249 = r1
            r12.close()
            r9.m7314(r0)
            r20.close()
            java.util.Iterator r0 = r14.iterator()
        L_0x0115:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x012e
            java.lang.Object r1 = r0.next()
            org.ʻ.ʻ.ˈ.ˈ$ʻ r1 = (org.ʻ.ʻ.ˈ.C1381.C1382) r1
            ClassSectionType r2 = r8.f7221
            MethodKey r3 = r1.f7259
            int r4 = r8.f7249
            int r1 = r1.f7260
            int r4 = r4 + r1
            r2.m7585(r3, r4)
            goto L_0x0115
        L_0x012e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7651(org.ʻ.ʻ.ˈ.ˆ, org.ʻ.ʻ.ˈ.ʻ.ʻ):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: StringSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m7646(org.ʻ.ʻ.ʼ.C1088 r7) {
        /*
            r6 = this;
            java.util.List r0 = r7.m6567()
            r1 = 0
        L_0x0005:
            int r2 = r0.size()
            if (r1 >= r2) goto L_0x0043
            java.lang.Object r2 = r0.get(r1)
            org.ʻ.ʻ.ʾ.ʼ.ˆ r2 = (org.ʻ.ʻ.ʾ.ʼ.C1240) r2
            org.ʻ.ʻ.ʾ r3 = r2.m7034()
            org.ʻ.ʻ.ʾ r4 = org.ʻ.ʻ.C1185.CONST_STRING
            if (r3 != r4) goto L_0x0040
            StringSectionType r3 = r6.f7256
            r4 = r2
            org.ʻ.ʻ.ʾ.ʼ.ˑ r4 = (org.ʻ.ʻ.ʾ.ʼ.C1247) r4
            org.ʻ.ʻ.ʾ.ʽ.ˆ r5 = r4.m7038()
            org.ʻ.ʻ.ʾ.ʽ.ˈ r5 = (org.ʻ.ʻ.ʾ.ʽ.C1264) r5
            int r3 = r3.m7777(r5)
            r5 = 65536(0x10000, float:9.18355E-41)
            if (r3 < r5) goto L_0x0040
            org.ʻ.ʻ.ʼ.ʼ.ⁱ r3 = new org.ʻ.ʻ.ʼ.ʼ.ⁱ
            org.ʻ.ʻ.ʾ r5 = org.ʻ.ʻ.C1185.CONST_STRING_JUMBO
            org.ʻ.ʻ.ʾ.ʼ.ˎ r2 = (org.ʻ.ʻ.ʾ.ʼ.C1245) r2
            int r2 = r2.a_()
            org.ʻ.ʻ.ʾ.ʽ.ˆ r4 = r4.m7038()
            r3.<init>(r5, r2, r4)
            r7.m6564(r1, r3)
        L_0x0040:
            int r1 = r1 + 1
            goto L_0x0005
        L_0x0043:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7646(org.ʻ.ʻ.ʼ.ˑ):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: StringSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0081  */
    /* renamed from: ʻ  reason: contains not printable characters */
    private int m7643(org.ʻ.ʻ.ˈ.C1380 r8, org.ʻ.ʻ.ˈ.C1379<StringKey, TypeKey> r9, java.lang.Iterable<? extends StringKey> r10, java.lang.Iterable<? extends org.ʻ.ʻ.ʾ.ʻ.C1188> r11) {
        /*
            r7 = this;
            r0 = -1
            r1 = 0
            if (r10 == 0) goto L_0x0020
            int r2 = com.google.ʻ.ʼ.C0918.m5739(r10)
            java.util.Iterator r3 = r10.iterator()
            r4 = -1
            r5 = 0
        L_0x000e:
            boolean r6 = r3.hasNext()
            if (r6 == 0) goto L_0x0022
            java.lang.Object r6 = r3.next()
            java.lang.CharSequence r6 = (java.lang.CharSequence) r6
            if (r6 == 0) goto L_0x001d
            r4 = r5
        L_0x001d:
            int r5 = r5 + 1
            goto L_0x000e
        L_0x0020:
            r2 = 0
            r4 = -1
        L_0x0022:
            if (r4 != r0) goto L_0x002d
            if (r11 == 0) goto L_0x002c
            boolean r0 = com.google.ʻ.ʼ.C0918.m5749(r11)
            if (r0 == 0) goto L_0x002d
        L_0x002c:
            return r1
        L_0x002d:
            int r0 = r7.f7253
            int r0 = r0 + 1
            r7.f7253 = r0
            int r0 = r8.m7632()
            if (r11 == 0) goto L_0x0054
            java.util.Iterator r3 = r11.iterator()
        L_0x003d:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0054
            java.lang.Object r4 = r3.next()
            org.ʻ.ʻ.ʾ.ʻ.ʻ r4 = (org.ʻ.ʻ.ʾ.ʻ.C1188) r4
            boolean r5 = r4 instanceof org.ʻ.ʻ.ʾ.ʻ.C1191
            if (r5 == 0) goto L_0x003d
            org.ʻ.ʻ.ʾ.ʻ.ʾ r4 = (org.ʻ.ʻ.ʾ.ʻ.C1191) r4
            int r3 = r4.m7007()
            goto L_0x0055
        L_0x0054:
            r3 = 0
        L_0x0055:
            r8.m7640(r3)
            r8.m7640(r2)
            if (r10 == 0) goto L_0x007f
            java.util.Iterator r10 = r10.iterator()
            r4 = 0
        L_0x0062:
            boolean r5 = r10.hasNext()
            if (r5 == 0) goto L_0x007f
            java.lang.Object r5 = r10.next()
            java.lang.CharSequence r5 = (java.lang.CharSequence) r5
            if (r4 != r2) goto L_0x0071
            goto L_0x007f
        L_0x0071:
            int r4 = r4 + 1
            StringSectionType r6 = r7.f7256
            int r5 = r6.m7770(r5)
            int r5 = r5 + 1
            r8.m7640(r5)
            goto L_0x0062
        L_0x007f:
            if (r11 == 0) goto L_0x009a
            r9.m7613(r3)
            java.util.Iterator r10 = r11.iterator()
        L_0x0088:
            boolean r11 = r10.hasNext()
            if (r11 == 0) goto L_0x009a
            java.lang.Object r11 = r10.next()
            org.ʻ.ʻ.ʾ.ʻ.ʻ r11 = (org.ʻ.ʻ.ʾ.ʻ.C1188) r11
            ClassSectionType r2 = r7.f7221
            r2.m7581(r9, r11)
            goto L_0x0088
        L_0x009a:
            r8.write(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7643(org.ʻ.ʻ.ˈ.ˆ, org.ʻ.ʻ.ˈ.ʿ, java.lang.Iterable, java.lang.Iterable):int");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ClassSectionType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ʻ  reason: contains not printable characters */
    private int m7642(org.ʻ.ʻ.ˈ.C1380 r18, java.io.ByteArrayOutputStream r19, MethodKey r20, java.util.List<? extends org.ʻ.ʻ.ʾ.C1291<? extends org.ʻ.ʻ.ʾ.C1286>> r21, java.lang.Iterable<? extends org.ʻ.ʻ.ʾ.ʼ.C1240> r22, int r23) {
        /*
            r17 = this;
            r1 = r17
            r0 = r18
            r11 = r19
            r2 = r20
            r3 = r23
            if (r22 != 0) goto L_0x0010
            if (r3 != 0) goto L_0x0010
            r0 = -1
            return r0
        L_0x0010:
            int r4 = r1.f7254
            r12 = 1
            int r4 = r4 + r12
            r1.f7254 = r4
            r18.m7624()
            int r13 = r18.m7632()
            ClassSectionType r4 = r1.f7221
            int r4 = r4.m7604(r2)
            r0.m7636(r4)
            org.ʻ.ʻ.ʻ r4 = org.ʻ.ʻ.C1001.STATIC
            ClassSectionType r5 = r1.f7221
            int r5 = r5.m7596(r2)
            boolean r4 = r4.m6291(r5)
            TypeListSectionType r5 = r1.f7243
            ProtoSectionType r6 = r1.f7248
            MethodSectionType r7 = r1.f7223
            org.ʻ.ʻ.ʾ.ʽ.ʾ r2 = r7.m7766(r2)
            java.lang.Object r2 = r6.m7776(r2)
            java.util.Collection r2 = r5.m7779(r2)
            int r2 = org.ʻ.ʻ.ˆ.C1336.m7280(r2, r4)
            r0.m7636(r2)
            if (r22 == 0) goto L_0x0314
            java.util.List r15 = org.ʻ.ʻ.ˈ.ʽ.C1374.m7559(r21)
            java.util.Iterator r2 = r22.iterator()
            r4 = 0
            r10 = 0
        L_0x0057:
            boolean r5 = r2.hasNext()
            if (r5 == 0) goto L_0x0097
            java.lang.Object r5 = r2.next()
            org.ʻ.ʻ.ʾ.ʼ.ˆ r5 = (org.ʻ.ʻ.ʾ.ʼ.C1240) r5
            int r6 = r5.m7035()
            int r10 = r10 + r6
            org.ʻ.ʻ.ʾ r6 = r5.m7034()
            int r6 = r6.f7076
            r7 = 3
            if (r6 != r7) goto L_0x0057
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ˑ r6 = (org.ʻ.ʻ.ʾ.ʼ.C1247) r6
            org.ʻ.ʻ.ʾ.ʽ.ˆ r6 = r6.m7038()
            org.ʻ.ʻ.ʾ.ʽ.ʿ r6 = (org.ʻ.ʻ.ʾ.ʽ.C1262) r6
            org.ʻ.ʻ.ʾ r7 = r5.m7034()
            boolean r8 = org.ʻ.ʻ.ˆ.C1335.m7278(r7)
            if (r8 == 0) goto L_0x008b
            org.ʻ.ʻ.ʾ.ʼ.ᵎ r5 = (org.ʻ.ʻ.ʾ.ʼ.C1253) r5
            int r5 = r5.m7045()
            goto L_0x0093
        L_0x008b:
            boolean r5 = org.ʻ.ʻ.ˆ.C1335.m7277(r7)
            int r5 = org.ʻ.ʻ.ˆ.C1336.m7281(r6, r5)
        L_0x0093:
            if (r5 <= r4) goto L_0x0057
            r4 = r5
            goto L_0x0057
        L_0x0097:
            r0.m7636(r4)
            int r2 = r15.size()
            r0.m7636(r2)
            r0.m7625(r3)
            org.ʻ.ʻ.ʿ r2 = r1.f7218
            StringSectionType r4 = r1.f7256
            TypeSectionType r5 = r1.f7246
            FieldSectionType r6 = r1.f7219
            MethodSectionType r7 = r1.f7223
            ProtoSectionType r8 = r1.f7248
            MethodHandleSectionType r9 = r1.f7225
            CallSiteSectionType r3 = r1.f7227
            r16 = r3
            r3 = r18
            r14 = r10
            r10 = r16
            org.ʻ.ʻ.ˈ.ˏ r2 = org.ʻ.ʻ.ˈ.C1389.m7723(r2, r3, r4, r5, r6, r7, r8, r9, r10)
            r0.m7625(r14)
            java.util.Iterator r3 = r22.iterator()
            r4 = 0
        L_0x00c7:
            boolean r5 = r3.hasNext()
            if (r5 == 0) goto L_0x0222
            java.lang.Object r5 = r3.next()
            org.ʻ.ʻ.ʾ.ʼ.ˆ r5 = (org.ʻ.ʻ.ʾ.ʼ.C1240) r5
            int[] r6 = org.ʻ.ʻ.ˈ.C1381.AnonymousClass4.f7258     // Catch:{ RuntimeException -> 0x0210 }
            org.ʻ.ʻ.ʾ r7 = r5.m7034()     // Catch:{ RuntimeException -> 0x0210 }
            org.ʻ.ʻ.ʽ r7 = r7.f7077     // Catch:{ RuntimeException -> 0x0210 }
            int r7 = r7.ordinal()     // Catch:{ RuntimeException -> 0x0210 }
            r6 = r6[r7]     // Catch:{ RuntimeException -> 0x0210 }
            switch(r6) {
                case 1: goto L_0x01f2;
                case 2: goto L_0x01eb;
                case 3: goto L_0x01e4;
                case 4: goto L_0x01dd;
                case 5: goto L_0x01d6;
                case 6: goto L_0x01cf;
                case 7: goto L_0x01c8;
                case 8: goto L_0x01c1;
                case 9: goto L_0x01ba;
                case 10: goto L_0x01b3;
                case 11: goto L_0x01ac;
                case 12: goto L_0x01a5;
                case 13: goto L_0x019e;
                case 14: goto L_0x0197;
                case 15: goto L_0x0190;
                case 16: goto L_0x0188;
                case 17: goto L_0x0180;
                case 18: goto L_0x0178;
                case 19: goto L_0x0170;
                case 20: goto L_0x0168;
                case 21: goto L_0x0160;
                case 22: goto L_0x0158;
                case 23: goto L_0x0150;
                case 24: goto L_0x0148;
                case 25: goto L_0x0140;
                case 26: goto L_0x0138;
                case 27: goto L_0x0130;
                case 28: goto L_0x0128;
                case 29: goto L_0x0120;
                case 30: goto L_0x0118;
                case 31: goto L_0x0110;
                case 32: goto L_0x0108;
                case 33: goto L_0x0100;
                case 34: goto L_0x00f8;
                case 35: goto L_0x00f0;
                case 36: goto L_0x00e8;
                default: goto L_0x00e4;
            }     // Catch:{ RuntimeException -> 0x0210 }
        L_0x00e4:
            org.ʻ.ʼ.ʿ r0 = new org.ʻ.ʼ.ʿ     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01ff
        L_0x00e8:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ˉˉ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1214) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7739(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x00f0:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ˆˆ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1210) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7736(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x00f8:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ʻ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1199) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7725(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0100:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ــ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1222) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7747(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0108:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ʾʾ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1206) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7732(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0110:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ʿʿ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1208) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7734(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0118:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ʼʼ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1202) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7728(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0120:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ʽʽ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1204) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7730(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0128:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ʻʻ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1200) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7726(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0130:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ᴵᴵ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1227) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7752(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0138:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ᐧᐧ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1225) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7750(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0140:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ﾞﾞ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1235) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7760(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0148:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ﾞ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1234) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7759(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0150:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ﹶ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1233) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7758(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0158:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ﹳ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1232) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7757(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0160:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ⁱ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1231) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7756(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0168:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ᵢ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1230) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7755(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0170:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ᵔ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1229) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7754(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0178:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ᵎ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1228) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7753(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0180:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ᴵ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1226) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7751(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0188:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ᐧ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1224) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7749(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0190:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ٴ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1223) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7748(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x0197:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ـ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1221) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7746(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x019e:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.י r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1220) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7745(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x01a5:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ˑ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1219) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7744(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x01ac:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ˏ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1218) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7743(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x01b3:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ˎ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1217) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7742(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x01ba:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ˋ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1216) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7741(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x01c1:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ˊ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1215) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7740(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x01c8:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ˉ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1213) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7738(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x01cf:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ˈ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1211) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7737(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x01d6:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ˆ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1209) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7735(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x01dd:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ʿ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1207) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7733(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x01e4:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ʾ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1205) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7731(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x01eb:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ʽ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1203) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7729(r6)     // Catch:{ RuntimeException -> 0x0210 }
            goto L_0x01f8
        L_0x01f2:
            r6 = r5
            org.ʻ.ʻ.ʾ.ʼ.ʻ.ʼ r6 = (org.ʻ.ʻ.ʾ.ʼ.ʻ.C1201) r6     // Catch:{ RuntimeException -> 0x0210 }
            r2.m7727(r6)     // Catch:{ RuntimeException -> 0x0210 }
        L_0x01f8:
            int r5 = r5.m7035()
            int r4 = r4 + r5
            goto L_0x00c7
        L_0x01ff:
            java.lang.String r2 = "Unsupported instruction format: %s"
            java.lang.Object[] r3 = new java.lang.Object[r12]     // Catch:{ RuntimeException -> 0x0210 }
            org.ʻ.ʻ.ʾ r5 = r5.m7034()     // Catch:{ RuntimeException -> 0x0210 }
            org.ʻ.ʻ.ʽ r5 = r5.f7077     // Catch:{ RuntimeException -> 0x0210 }
            r6 = 0
            r3[r6] = r5     // Catch:{ RuntimeException -> 0x0210 }
            r0.<init>(r2, r3)     // Catch:{ RuntimeException -> 0x0210 }
            throw r0     // Catch:{ RuntimeException -> 0x0210 }
        L_0x0210:
            r0 = move-exception
            org.ʻ.ʼ.ʿ r2 = new org.ʻ.ʼ.ʿ
            java.lang.Object[] r3 = new java.lang.Object[r12]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r5 = 0
            r3[r5] = r4
            java.lang.String r4 = "Error while writing instruction at code offset 0x%x"
            r2.<init>(r0, r4, r3)
            throw r2
        L_0x0222:
            int r2 = r15.size()
            if (r2 <= 0) goto L_0x0321
            r18.m7624()
            java.util.HashMap r2 = com.google.ʻ.ʼ.C0929.m5793()
            java.util.Iterator r3 = r15.iterator()
        L_0x0233:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x024c
            java.lang.Object r4 = r3.next()
            org.ʻ.ʻ.ʾ.ˋ r4 = (org.ʻ.ʻ.ʾ.C1291) r4
            java.util.List r4 = r4.m7145()
            r5 = 0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r5)
            r2.put(r4, r6)
            goto L_0x0233
        L_0x024c:
            int r3 = r2.size()
            org.ʻ.ʻ.ˈ.C1380.m7622(r11, r3)
            java.util.Iterator r3 = r15.iterator()
        L_0x0257:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0307
            java.lang.Object r4 = r3.next()
            org.ʻ.ʻ.ʾ.ˋ r4 = (org.ʻ.ʻ.ʾ.C1291) r4
            int r5 = r4.m7143()
            int r6 = r4.m7144()
            int r6 = r6 + r5
            int r6 = r6 - r5
            r0.m7625(r5)
            r0.m7636(r6)
            java.util.List r5 = r4.m7145()
            int r5 = r5.size()
            if (r5 == 0) goto L_0x02fc
            java.util.List r5 = r4.m7145()
            java.lang.Object r5 = r2.get(r5)
            java.lang.Integer r5 = (java.lang.Integer) r5
            int r6 = r5.intValue()
            if (r6 == 0) goto L_0x0295
            int r4 = r5.intValue()
            r0.m7636(r4)
            goto L_0x0257
        L_0x0295:
            int r5 = r19.size()
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            int r6 = r5.intValue()
            r0.m7636(r6)
            java.util.List r6 = r4.m7145()
            r2.put(r6, r5)
            java.util.List r5 = r4.m7145()
            int r5 = r5.size()
            java.util.List r6 = r4.m7145()
            int r7 = r5 + -1
            java.lang.Object r6 = r6.get(r7)
            org.ʻ.ʻ.ʾ.ʿ r6 = (org.ʻ.ʻ.ʾ.C1286) r6
            java.lang.String r6 = r6.m7124()
            if (r6 != 0) goto L_0x02c8
            int r5 = r5 * -1
            int r5 = r5 + r12
        L_0x02c8:
            org.ʻ.ʻ.ˈ.C1380.m7623(r11, r5)
            java.util.List r4 = r4.m7145()
            java.util.Iterator r4 = r4.iterator()
        L_0x02d3:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x0257
            java.lang.Object r5 = r4.next()
            org.ʻ.ʻ.ʾ.ʿ r5 = (org.ʻ.ʻ.ʾ.C1286) r5
            ClassSectionType r6 = r1.f7221
            java.lang.CharSequence r6 = r6.m7577(r5)
            int r5 = r5.m7125()
            if (r6 == 0) goto L_0x02f8
            TypeSectionType r7 = r1.f7246
            int r6 = r7.m7718(r6)
            org.ʻ.ʻ.ˈ.C1380.m7622(r11, r6)
            org.ʻ.ʻ.ˈ.C1380.m7622(r11, r5)
            goto L_0x02d3
        L_0x02f8:
            org.ʻ.ʻ.ˈ.C1380.m7622(r11, r5)
            goto L_0x02d3
        L_0x02fc:
            org.ʻ.ʼ.ʿ r0 = new org.ʻ.ʼ.ʿ
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.String r3 = "No exception handlers for the try block!"
            r0.<init>(r3, r2)
            throw r0
        L_0x0307:
            int r2 = r19.size()
            if (r2 <= 0) goto L_0x0321
            r11.writeTo(r0)
            r19.reset()
            goto L_0x0321
        L_0x0314:
            r2 = 0
            r0.m7636(r2)
            r0.m7636(r2)
            r0.m7625(r3)
            r0.m7625(r2)
        L_0x0321:
            return r13
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˈ.C1381.m7642(org.ʻ.ʻ.ˈ.ˆ, java.io.ByteArrayOutputStream, java.lang.Object, java.util.List, java.lang.Iterable, int):int");
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private int m7660() {
        int i = this.f7256.m7716().size() > 0 ? 3 : 1;
        if (this.f7246.m7716().size() > 0) {
            i++;
        }
        if (this.f7248.m7716().size() > 0) {
            i++;
        }
        if (this.f7219.m7716().size() > 0) {
            i++;
        }
        if (this.f7223.m7716().size() > 0) {
            i++;
        }
        if (this.f7227.m7716().size() > 0) {
            i++;
        }
        if (this.f7225.m7716().size() > 0) {
            i++;
        }
        if (this.f7243.m7772().size() > 0) {
            i++;
        }
        if (this.f7231.m7772().size() > 0) {
            i++;
        }
        if (this.f7229.m7772().size() > 0) {
            i++;
        }
        if (this.f7233.m7772().size() > 0 || m7663()) {
            i++;
        }
        if (this.f7251 > 0) {
            i++;
        }
        if (this.f7252 > 0) {
            i++;
        }
        if (this.f7253 > 0) {
            i++;
        }
        if (this.f7254 > 0) {
            i++;
        }
        if (this.f7221.m7716().size() > 0) {
            i++;
        }
        if (this.f7255 > 0) {
            i++;
        }
        return i + 1;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m7671(C1380 r5) {
        r5.m7624();
        this.f7250 = r5.m7632();
        r5.m7625(m7660());
        m7649(r5, 0, 1, 0);
        m7649(r5, 1, this.f7256.m7716().size(), this.f7220);
        m7649(r5, 2, this.f7246.m7716().size(), this.f7222);
        m7649(r5, 3, this.f7248.m7716().size(), this.f7224);
        m7649(r5, 4, this.f7219.m7716().size(), this.f7226);
        m7649(r5, 5, this.f7223.m7716().size(), this.f7228);
        m7649(r5, 6, this.f7221.m7716().size(), this.f7230);
        m7649(r5, 7, this.f7227.m7716().size(), this.f7232);
        m7649(r5, 8, this.f7225.m7716().size(), this.f7234);
        m7649(r5, 8194, this.f7256.m7716().size(), this.f7236);
        m7649(r5, 4097, this.f7243.m7772().size(), this.f7239);
        m7649(r5, 8197, this.f7231.m7772().size(), this.f7240);
        m7649(r5, 8196, this.f7229.m7772().size(), this.f7241);
        m7649(r5, 4099, this.f7233.m7772().size() + (m7663() ? 1 : 0), this.f7242);
        m7649(r5, 4098, this.f7251, this.f7244);
        m7649(r5, 8198, this.f7252, this.f7245);
        m7649(r5, 8195, this.f7253, this.f7247);
        m7649(r5, 8193, this.f7254, this.f7249);
        m7649(r5, 8192, this.f7255, this.f7238);
        m7649(r5, 4096, 1, this.f7250);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m7649(C1380 r1, int i, int i2, int i3) {
        if (i2 > 0) {
            r1.m7636(i);
            r1.m7636(0);
            r1.m7625(i2);
            r1.m7625(i3);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m7648(C1380 r3, int i, int i2) {
        r3.write(C1134.m6689(this.f7218.f7083));
        r3.m7625(0);
        r3.write(new byte[20]);
        r3.m7625(i2);
        r3.m7625(112);
        r3.m7625(305419896);
        r3.m7625(0);
        r3.m7625(0);
        r3.m7625(this.f7250);
        m7655(r3, this.f7256.m7716().size(), this.f7220);
        m7655(r3, this.f7246.m7716().size(), this.f7222);
        m7655(r3, this.f7248.m7716().size(), this.f7224);
        m7655(r3, this.f7219.m7716().size(), this.f7226);
        m7655(r3, this.f7223.m7716().size(), this.f7228);
        m7655(r3, this.f7221.m7716().size(), this.f7230);
        r3.m7625(i2 - i);
        r3.m7625(i);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m7655(C1380 r1, int i, int i2) {
        r1.m7625(i);
        if (i > 0) {
            r1.m7625(i2);
        } else {
            r1.m7625(0);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean m7663() {
        return this.f7218.f7083 < 17;
    }

    /* renamed from: org.ʻ.ʻ.ˈ.ˈ$ʽ  reason: contains not printable characters */
    /* compiled from: DexWriter */
    public abstract class C1384 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract StringSectionType m7680();

        /* renamed from: ʼ  reason: contains not printable characters */
        public abstract TypeSectionType m7681();

        /* renamed from: ʽ  reason: contains not printable characters */
        public abstract ProtoSectionType m7682();

        /* renamed from: ʾ  reason: contains not printable characters */
        public abstract FieldSectionType m7683();

        /* renamed from: ʿ  reason: contains not printable characters */
        public abstract MethodSectionType m7684();

        /* renamed from: ˆ  reason: contains not printable characters */
        public abstract ClassSectionType m7685();

        /* renamed from: ˈ  reason: contains not printable characters */
        public abstract CallSiteSectionType m7686();

        /* renamed from: ˉ  reason: contains not printable characters */
        public abstract MethodHandleSectionType m7687();

        /* renamed from: ˊ  reason: contains not printable characters */
        public abstract TypeListSectionType m7688();

        /* renamed from: ˋ  reason: contains not printable characters */
        public abstract AnnotationSectionType m7689();

        /* renamed from: ˎ  reason: contains not printable characters */
        public abstract AnnotationSetSectionType m7690();

        /* renamed from: ˏ  reason: contains not printable characters */
        public abstract EncodedArraySectionType m7691();

        public C1384() {
        }
    }
}
