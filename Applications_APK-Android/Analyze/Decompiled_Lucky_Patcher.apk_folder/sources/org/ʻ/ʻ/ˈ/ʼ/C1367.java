package org.ʻ.ʻ.ˈ.ʼ;

import java.util.Collection;
import java.util.Map;
import org.ʻ.ʻ.ˈ.C1392;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ˈ.ʼ.ᵎ  reason: contains not printable characters */
/* compiled from: StringTypeBasePool */
public abstract class C1367 extends C1352<String, Integer> implements C1392<CharSequence> {
    public C1367(C1356 r1) {
        super(r1);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Collection<Map.Entry<String, Integer>> m7532() {
        return this.f7157.entrySet();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m7535(CharSequence charSequence) {
        Integer num = (Integer) this.f7157.get(charSequence.toString());
        if (num != null) {
            return num.intValue();
        }
        throw new C1404("Item not found.: %s", charSequence.toString());
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m7531(CharSequence charSequence) {
        if (charSequence == null) {
            return -1;
        }
        return m7535(charSequence);
    }
}
