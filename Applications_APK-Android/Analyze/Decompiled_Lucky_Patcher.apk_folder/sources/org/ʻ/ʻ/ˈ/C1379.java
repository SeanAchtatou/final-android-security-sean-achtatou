package org.ʻ.ʻ.ˈ;

import java.lang.CharSequence;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ˈ.ʿ  reason: contains not printable characters */
/* compiled from: DebugWriter */
public class C1379<StringKey extends CharSequence, TypeKey extends CharSequence> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C1396<StringKey, ?> f7209;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final C1398<StringKey, TypeKey, ?> f7210;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final C1380 f7211;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f7212;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f7213;

    C1379(C1396<StringKey, ?> r1, C1398<StringKey, TypeKey, ?> r2, C1380 r3) {
        this.f7209 = r1;
        this.f7210 = r2;
        this.f7211 = r3;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7613(int i) {
        this.f7212 = 0;
        this.f7213 = i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7615(int i, int i2, StringKey stringkey, TypeKey typekey, StringKey stringkey2) {
        int r4 = this.f7209.m7770((Object) stringkey);
        int r5 = this.f7210.m7770((Object) typekey);
        int r6 = this.f7209.m7770((Object) stringkey2);
        m7610(i);
        if (r6 == -1) {
            this.f7211.write(3);
            this.f7211.m7640(i2);
            this.f7211.m7640(r4 + 1);
            this.f7211.m7640(r5 + 1);
            return;
        }
        this.f7211.write(4);
        this.f7211.m7640(i2);
        this.f7211.m7640(r4 + 1);
        this.f7211.m7640(r5 + 1);
        this.f7211.m7640(r6 + 1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7614(int i, int i2) {
        m7610(i);
        this.f7211.write(5);
        this.f7211.m7640(i2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7618(int i, int i2) {
        m7610(i);
        this.f7211.write(6);
        this.f7211.m7640(i2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7617(int i) {
        m7610(i);
        this.f7211.write(7);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m7619(int i) {
        m7610(i);
        this.f7211.write(8);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m7620(int i, int i2) {
        int i3 = i2 - this.f7213;
        int i4 = i - this.f7212;
        if (i4 >= 0) {
            if (i3 < -4 || i3 > 10) {
                m7612(i2);
                i3 = 0;
            }
            if ((i3 < 2 && i4 > 16) || (i3 > 1 && i4 > 15)) {
                m7610(i);
                i4 = 0;
            }
            m7611(i3, i4);
            return;
        }
        throw new C1404("debug info items must have non-decreasing code addresses", new Object[0]);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7616(int i, CharSequence charSequence) {
        m7610(i);
        this.f7211.write(9);
        this.f7211.m7640(this.f7209.m7770(charSequence) + 1);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m7610(int i) {
        int i2 = i - this.f7212;
        if (i2 > 0) {
            this.f7211.write(1);
            this.f7211.m7640(i2);
            this.f7212 = i;
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m7612(int i) {
        int i2 = i - this.f7213;
        if (i2 != 0) {
            this.f7211.write(2);
            this.f7211.m7641(i2);
            this.f7213 = i;
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m7611(int i, int i2) {
        this.f7211.write((byte) ((i2 * 15) + 10 + i + 4));
        this.f7213 += i;
        this.f7212 += i2;
    }
}
