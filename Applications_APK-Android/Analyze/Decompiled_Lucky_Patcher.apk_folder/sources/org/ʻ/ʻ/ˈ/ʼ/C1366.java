package org.ʻ.ʻ.ˈ.ʼ;

import org.ʻ.ʻ.ʾ.ʽ.C1264;
import org.ʻ.ʻ.ˈ.C1396;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ˈ.ʼ.ᴵ  reason: contains not printable characters */
/* compiled from: StringPool */
public class C1366 extends C1367 implements C1396<CharSequence, C1264> {
    public C1366(C1356 r1) {
        super(r1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7528(CharSequence charSequence) {
        this.f7157.put(charSequence.toString(), 0);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7530(CharSequence charSequence) {
        if (charSequence != null) {
            m7528(charSequence);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m7527(C1264 r4) {
        Integer num = (Integer) this.f7157.get(r4.toString());
        if (num != null) {
            return num.intValue();
        }
        throw new C1404("Item not found.: %s", r4.toString());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m7529() {
        return this.f7157.size() > 65536;
    }
}
