package org.ʻ.ʻ.ʽ.ʿ;

import org.ʻ.ʻ.ʽ.C1163;
import org.ʻ.ʻ.ʽ.C1184;
import org.ʻ.ʻ.ʾ.ʾ.C1273;
import org.ʻ.ʻ.ʿ.ʾ.C1310;
import org.ʻ.ʻ.ʿ.ʾ.C1311;
import org.ʻ.ʻ.ʿ.ʾ.C1312;
import org.ʻ.ʻ.ʿ.ʾ.C1313;
import org.ʻ.ʻ.ʿ.ʾ.C1318;
import org.ʻ.ʻ.ʿ.ʾ.C1319;
import org.ʻ.ʻ.ʿ.ʾ.C1320;
import org.ʻ.ʻ.ʿ.ʾ.C1324;
import org.ʻ.ʻ.ʿ.ʾ.C1325;
import org.ʻ.ʻ.ˆ.C1337;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ʽ.ʿ.ʽ  reason: contains not printable characters */
/* compiled from: DexBackedEncodedValue */
public abstract class C1168 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1273 m6903(C1163 r8, C1184 r9) {
        int r0 = r9.m6972();
        try {
            int r3 = r9.m6988();
            int i = r3 & 31;
            int i2 = r3 >>> 5;
            if (i == 0) {
                C1337.m7290(i2, 0);
                return new C1311((byte) r9.m6989());
            } else if (i == 6) {
                C1337.m7290(i2, 7);
                return new C1320(r9.m6985(i2 + 1));
            } else if (i == 2) {
                C1337.m7290(i2, 1);
                return new C1325((short) r9.m6977(i2 + 1));
            } else if (i == 3) {
                C1337.m7290(i2, 1);
                return new C1312((char) r9.m6979(i2 + 1));
            } else if (i == 4) {
                C1337.m7290(i2, 3);
                return new C1319(r9.m6977(i2 + 1));
            } else if (i == 16) {
                C1337.m7290(i2, 3);
                return new C1318(Float.intBitsToFloat(r9.m6981(i2 + 1)));
            } else if (i != 17) {
                switch (i) {
                    case 21:
                        C1337.m7290(i2, 3);
                        return new C1174(r8, r9, i2);
                    case 22:
                        C1337.m7290(i2, 3);
                        return new C1173(r8, r9, i2);
                    case 23:
                        C1337.m7290(i2, 3);
                        return new C1175(r8, r9, i2);
                    case 24:
                        C1337.m7290(i2, 3);
                        return new C1176(r8, r9, i2);
                    case 25:
                        C1337.m7290(i2, 3);
                        return new C1171(r8, r9, i2);
                    case 26:
                        C1337.m7290(i2, 3);
                        return new C1172(r8, r9, i2);
                    case 27:
                        C1337.m7290(i2, 3);
                        return new C1170(r8, r9, i2);
                    case 28:
                        C1337.m7290(i2, 0);
                        return new C1167(r8, r9);
                    case 29:
                        C1337.m7290(i2, 0);
                        return new C1165(r8, r9);
                    case 30:
                        C1337.m7290(i2, 0);
                        return C1324.f7141;
                    case 31:
                        C1337.m7290(i2, 1);
                        return C1310.m7229(i2 == 1);
                    default:
                        throw new C1404("Invalid encoded_value type: 0x%x", Integer.valueOf(i));
                }
            } else {
                C1337.m7290(i2, 7);
                return new C1313(Double.longBitsToDouble(r9.m6982(i2 + 1)));
            }
        } catch (Exception e) {
            throw C1404.m7798(e, "Error while reading encoded value at offset 0x%x", Integer.valueOf(r0));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m6904(C1184 r6) {
        int r0 = r6.m6972();
        try {
            int r3 = r6.m6988();
            int i = r3 & 31;
            if (i != 0) {
                if (!(i == 6 || i == 2 || i == 3 || i == 4 || i == 16 || i == 17)) {
                    switch (i) {
                        case 21:
                        case 22:
                        case 23:
                        case 24:
                        case 25:
                        case 26:
                        case 27:
                            break;
                        case 28:
                            C1167.m6898(r6);
                            return;
                        case 29:
                            C1165.m6892(r6);
                            return;
                        case 30:
                        case 31:
                            return;
                        default:
                            throw new C1404("Invalid encoded_value type: 0x%x", Integer.valueOf(i));
                    }
                }
                r6.m6975((r3 >>> 5) + 1);
                return;
            }
            r6.m6990();
        } catch (Exception e) {
            throw C1404.m7798(e, "Error while skipping encoded value at offset 0x%x", Integer.valueOf(r0));
        }
    }
}
