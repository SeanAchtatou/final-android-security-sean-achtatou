package org.ʻ.ʻ.ʽ;

import com.google.ʻ.ʼ.C0891;
import com.google.ʻ.ʼ.C0904;
import com.google.ʻ.ʼ.C0918;
import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.ʻ.ʻ.ʻ.ʻ.C1010;
import org.ʻ.ʻ.ʽ.ʾ.C1146;
import org.ʻ.ʻ.ʽ.ʾ.C1153;
import org.ʻ.ʻ.ʽ.ʾ.C1161;
import org.ʻ.ʻ.ʾ.C1257;
import org.ʻ.ʻ.ʾ.ʽ.C1259;
import org.ʻ.ʻ.ʾ.ʽ.C1262;
import org.ʻ.ʻ.ʿ.ʼ.C1302;
import org.ʻ.ʻ.ʿ.ʼ.C1305;

/* renamed from: org.ʻ.ʻ.ʽ.ʾ  reason: contains not printable characters */
/* compiled from: DexBackedClassDef */
public class C1145 extends C1010 implements C1257 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final C1163 f6590;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f6591;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f6592;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public int f6593 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public int f6594 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ˆ  reason: contains not printable characters */
    public int f6595 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public final int f6596;
    /* access modifiers changed from: private */

    /* renamed from: ˉ  reason: contains not printable characters */
    public final int f6597;
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public final int f6598;
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public final int f6599;

    /* renamed from: ˎ  reason: contains not printable characters */
    private C1146 f6600;

    public C1145(C1163 r3, int i) {
        this.f6590 = r3;
        this.f6591 = i;
        int r4 = r3.m6854().m6962(i + 24);
        if (r4 == 0) {
            this.f6592 = -1;
            this.f6596 = 0;
            this.f6597 = 0;
            this.f6598 = 0;
            this.f6599 = 0;
            return;
        }
        C1184 r32 = r3.m6855().m6970(r4);
        this.f6596 = r32.m6976();
        this.f6597 = r32.m6976();
        this.f6598 = r32.m6976();
        this.f6599 = r32.m6976();
        this.f6592 = r32.m6972();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m6738() {
        return (String) this.f6590.m6860().get(this.f6590.m6854().m6962(this.f6591 + 0));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m6740() {
        return this.f6590.m6860().m6905(this.f6590.m6854().m6963(this.f6591 + 8));
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6741() {
        return this.f6590.m6854().m6962(this.f6591 + 4);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public String m6744() {
        return this.f6590.m6859().m6905(this.f6590.m6854().m6963(this.f6591 + 16));
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public List<String> m6745() {
        final int r0 = this.f6590.m6854().m6962(this.f6591 + 12);
        if (r0 <= 0) {
            return C0891.m5603();
        }
        final int r1 = this.f6590.m6855().m6962(r0);
        return new AbstractList<String>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public String get(int i) {
                return (String) C1145.this.f6590.m6860().get(C1145.this.f6590.m6855().m6964(r0 + 4 + (i * 2)));
            }

            public int size() {
                return r1;
            }
        };
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public Set<? extends C1093> m6746() {
        return m6733().m6764();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public Iterable<? extends C1178> m6747() {
        return m6737(true);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Iterable<? extends C1178> m6737(boolean z) {
        if (this.f6596 > 0) {
            C1184 r0 = this.f6590.m6855().m6970(this.f6592);
            final C1146 r3 = m6733();
            final int r4 = this.f6590.m6854().m6962(this.f6591 + 28);
            final int r5 = r0.m6972();
            final boolean z2 = z;
            return new Iterable<C1178>() {
                public Iterator<C1178> iterator() {
                    final C1146.C1147 r6 = r3.m6765();
                    final C1153 r5 = C1153.m6803(C1145.this.f6590, r4);
                    return new C1161<C1178>(C1145.this.f6590.m6855(), r5) {

                        /* renamed from: ʾ  reason: contains not printable characters */
                        private int f6612;

                        /* renamed from: ʿ  reason: contains not printable characters */
                        private C1259 f6613;

                        /* renamed from: ˆ  reason: contains not printable characters */
                        private int f6614;

                        /* access modifiers changed from: protected */
                        /* renamed from: ʻ  reason: contains not printable characters */
                        public C1178 m6754(C1184 r9) {
                            C1178 r0;
                            C1259 r02;
                            C1302 r1;
                            do {
                                int i = this.f6612 + 1;
                                this.f6612 = i;
                                if (i > C1145.this.f6596) {
                                    int unused = C1145.this.f6593 = r9.m6972();
                                    return (C1178) m5444();
                                }
                                r0 = new C1178(C1145.this.f6590, r9, C1145.this, this.f6614, r5, r6);
                                r02 = this.f6613;
                                r1 = C1302.m7200(r0);
                                this.f6613 = r1;
                                this.f6614 = r0.f6764;
                                if (!z2 || r02 == null) {
                                    return r0;
                                }
                            } while (r02.equals(r1));
                            return r0;
                        }
                    };
                }
            };
        }
        this.f6593 = this.f6592;
        return C0904.m5655();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public Iterable<? extends C1178> m6748() {
        return m6739(true);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Iterable<? extends C1178> m6739(final boolean z) {
        if (this.f6597 > 0) {
            C1184 r0 = this.f6590.m6855().m6970(m6734());
            final C1146 r1 = m6733();
            final int r02 = r0.m6972();
            return new Iterable<C1178>() {
                public Iterator<C1178> iterator() {
                    final C1146.C1147 r0 = r1.m6765();
                    return new C1161<C1178>(C1145.this.f6590.m6855(), r02) {

                        /* renamed from: ʽ  reason: contains not printable characters */
                        private int f6621;

                        /* renamed from: ʾ  reason: contains not printable characters */
                        private C1259 f6622;

                        /* renamed from: ʿ  reason: contains not printable characters */
                        private int f6623;

                        /* access modifiers changed from: protected */
                        /* renamed from: ʻ  reason: contains not printable characters */
                        public C1178 m6756(C1184 r8) {
                            C1178 r0;
                            C1259 r02;
                            C1302 r1;
                            do {
                                int i = this.f6621 + 1;
                                this.f6621 = i;
                                if (i > C1145.this.f6597) {
                                    int unused = C1145.this.f6594 = r8.m6972();
                                    return (C1178) m5444();
                                }
                                r0 = new C1178(C1145.this.f6590, r8, C1145.this, this.f6623, r0);
                                r02 = this.f6622;
                                r1 = C1302.m7200(r0);
                                this.f6622 = r1;
                                this.f6623 = r0.f6764;
                                if (!z || r02 == null) {
                                    return r0;
                                }
                            } while (r02.equals(r1));
                            return r0;
                        }
                    };
                }
            };
        }
        int i = this.f6593;
        if (i > 0) {
            this.f6594 = i;
        }
        return C0904.m5655();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public Iterable<? extends C1179> m6749() {
        return m6742(true);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Iterable<? extends C1179> m6742(final boolean z) {
        if (this.f6598 > 0) {
            C1184 r0 = this.f6590.m6855().m6970(m6735());
            final C1146 r1 = m6733();
            final int r02 = r0.m6972();
            return new Iterable<C1179>() {
                public Iterator<C1179> iterator() {
                    final C1146.C1147 r5 = r1.m6766();
                    final C1146.C1147 r6 = r1.m6767();
                    return new C1161<C1179>(C1145.this.f6590.m6855(), r02) {

                        /* renamed from: ʾ  reason: contains not printable characters */
                        private int f6631;

                        /* renamed from: ʿ  reason: contains not printable characters */
                        private C1262 f6632;

                        /* renamed from: ˆ  reason: contains not printable characters */
                        private int f6633;

                        /* access modifiers changed from: protected */
                        /* renamed from: ʻ  reason: contains not printable characters */
                        public C1179 m6758(C1184 r9) {
                            C1179 r0;
                            C1262 r02;
                            C1305 r1;
                            do {
                                int i = this.f6631 + 1;
                                this.f6631 = i;
                                if (i > C1145.this.f6598) {
                                    int unused = C1145.this.f6595 = r9.m6972();
                                    return (C1179) m5444();
                                }
                                r0 = new C1179(C1145.this.f6590, r9, C1145.this, this.f6633, r5, r6);
                                r02 = this.f6632;
                                r1 = C1305.m7210(r0);
                                this.f6632 = r1;
                                this.f6633 = r0.f6771;
                                if (!z || r02 == null) {
                                    return r0;
                                }
                            } while (r02.equals(r1));
                            return r0;
                        }
                    };
                }
            };
        }
        int i = this.f6594;
        if (i > 0) {
            this.f6595 = i;
        }
        return C0904.m5655();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public Iterable<? extends C1179> m6743(final boolean z) {
        if (this.f6599 <= 0) {
            return C0904.m5655();
        }
        C1184 r0 = this.f6590.m6855().m6970(m6736());
        final C1146 r1 = m6733();
        final int r02 = r0.m6972();
        return new Iterable<C1179>() {

            /* renamed from: ʻ  reason: contains not printable characters */
            final C1146.C1147 f6634 = r1.m6766();

            /* renamed from: ʼ  reason: contains not printable characters */
            final C1146.C1147 f6635 = r1.m6767();

            public Iterator<C1179> iterator() {
                return new C1161<C1179>(C1145.this.f6590.m6855(), r02) {

                    /* renamed from: ʼ  reason: contains not printable characters */
                    private int f6641;

                    /* renamed from: ʽ  reason: contains not printable characters */
                    private C1262 f6642;

                    /* renamed from: ʾ  reason: contains not printable characters */
                    private int f6643;

                    /* access modifiers changed from: protected */
                    /* renamed from: ʻ  reason: contains not printable characters */
                    public C1179 m6760(C1184 r9) {
                        C1179 r0;
                        C1262 r02;
                        C1305 r1;
                        do {
                            int i = this.f6641 + 1;
                            this.f6641 = i;
                            if (i > C1145.this.f6599) {
                                return (C1179) m5444();
                            }
                            r0 = new C1179(C1145.this.f6590, r9, C1145.this, this.f6643, AnonymousClass5.this.f6634, AnonymousClass5.this.f6635);
                            r02 = this.f6642;
                            r1 = C1305.m7210(r0);
                            this.f6642 = r1;
                            this.f6643 = r0.f6771;
                            if (!z || r02 == null) {
                                return r0;
                            }
                        } while (r02.equals(r1));
                        return r0;
                    }
                };
            }
        };
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public Iterable<? extends C1179> m6750() {
        return m6743(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, java.lang.Iterable):java.lang.Iterable<T>
     arg types: [java.lang.Iterable<? extends org.ʻ.ʻ.ʽ.ˉ>, java.lang.Iterable<? extends org.ʻ.ʻ.ʽ.ˉ>]
     candidates:
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, int):java.lang.Iterable<T>
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, com.google.ʻ.ʻ.ʽ):java.lang.Iterable<T>
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, java.lang.Object):T
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, com.google.ʻ.ʻ.ˉ):boolean
      com.google.ʻ.ʼ.ﹳ.ʻ(java.lang.Iterable, java.lang.Iterable):java.lang.Iterable<T> */
    /* renamed from: ˎ  reason: contains not printable characters */
    public Iterable<? extends C1179> m6751() {
        return C0918.m5743((Iterable) m6749(), (Iterable) m6750());
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private C1146 m6733() {
        if (this.f6600 == null) {
            this.f6600 = C1146.m6761(this.f6590, this.f6590.m6854().m6962(this.f6591 + 20));
        }
        return this.f6600;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private int m6734() {
        int i = this.f6593;
        if (i > 0) {
            return i;
        }
        C1184 r0 = this.f6590.m6855().m6970(this.f6592);
        C1178.m6913(r0, this.f6596);
        this.f6593 = r0.m6972();
        return this.f6593;
    }

    /* renamed from: י  reason: contains not printable characters */
    private int m6735() {
        int i = this.f6594;
        if (i > 0) {
            return i;
        }
        C1184 r0 = this.f6590.m6855().m6970(m6734());
        C1178.m6913(r0, this.f6597);
        this.f6594 = r0.m6972();
        return this.f6594;
    }

    /* renamed from: ـ  reason: contains not printable characters */
    private int m6736() {
        int i = this.f6595;
        if (i > 0) {
            return i;
        }
        C1184 r0 = this.f6590.m6855().m6970(m6735());
        C1179.m6921(r0, this.f6598);
        this.f6595 = r0.m6972();
        return this.f6595;
    }
}
