package org.ʻ.ʻ.ʽ.ʾ;

import com.google.ʻ.ʼ.C0854;
import java.util.Iterator;
import org.ʻ.ʻ.ʽ.C1183;
import org.ʻ.ʻ.ʽ.C1184;

/* renamed from: org.ʻ.ʻ.ʽ.ʾ.ˋ  reason: contains not printable characters */
/* compiled from: VariableSizeLookaheadIterator */
public abstract class C1161<T> extends C0854<T> implements Iterator<T> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C1184 f6696;

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract T m6829(C1184 r1);

    protected C1161(C1183 r1, int i) {
        this.f6696 = r1.m6970(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public T m6828() {
        return m6829(this.f6696);
    }
}
