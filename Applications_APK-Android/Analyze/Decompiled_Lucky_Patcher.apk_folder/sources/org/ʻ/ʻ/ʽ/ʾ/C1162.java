package org.ʻ.ʻ.ʽ.ʾ;

import java.util.AbstractSet;
import org.ʻ.ʻ.ʽ.C1183;
import org.ʻ.ʻ.ʽ.C1184;

/* renamed from: org.ʻ.ʻ.ʽ.ʾ.ˎ  reason: contains not printable characters */
/* compiled from: VariableSizeSet */
public abstract class C1162<T> extends AbstractSet<T> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C1183 f6697;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f6698;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f6699;

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract T m6831(C1184 r1, int i);

    public C1162(C1183 r1, int i, int i2) {
        this.f6697 = r1;
        this.f6698 = i;
        this.f6699 = i2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C1158<T> iterator() {
        return new C1158<T>(this.f6697, this.f6698, this.f6699) {
            /* access modifiers changed from: protected */
            /* renamed from: ʼ  reason: contains not printable characters */
            public T m6832(C1184 r2, int i) {
                return C1162.this.m6831(r2, i);
            }
        };
    }

    public int size() {
        return this.f6699;
    }
}
