package org.ʻ.ʻ.ʽ.ʽ;

import com.google.ʻ.ʼ.C0926;
import java.util.ArrayList;
import java.util.List;
import org.ʻ.ʻ.ʻ.ʻ.C1003;
import org.ʻ.ʻ.ʽ.C1163;
import org.ʻ.ʻ.ʽ.ʾ.C1153;
import org.ʻ.ʻ.ʾ.ʽ.C1260;
import org.ʻ.ʻ.ʾ.ʽ.C1261;
import org.ʻ.ʻ.ʾ.ʾ.C1273;
import org.ʻ.ʻ.ʾ.ʾ.C1280;
import org.ʻ.ʻ.ʾ.ʾ.C1281;
import org.ʻ.ʻ.ʾ.ʾ.C1284;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ʽ.ʽ.ʻ  reason: contains not printable characters */
/* compiled from: DexBackedCallSiteReference */
public class C1137 extends C1003 {

    /* renamed from: ʾ  reason: contains not printable characters */
    static final /* synthetic */ boolean f6565 = (!C1137.class.desiredAssertionStatus());

    /* renamed from: ʻ  reason: contains not printable characters */
    public final C1163 f6566;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final int f6567;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final int f6568;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f6569 = -1;

    public C1137(C1163 r2, int i) {
        this.f6566 = r2;
        this.f6567 = i;
        this.f6568 = r2.m6865().m6891(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m6702() {
        return String.format("call_site_%d", Integer.valueOf(this.f6567));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C1260 m6703() {
        if (m6700().m6807() >= 3) {
            C1273 r0 = m6700().m6804();
            if (!f6565 && r0 == null) {
                throw new AssertionError();
            } else if (r0.m7098() == 22) {
                return ((C1280) r0).m7112();
            } else {
                throw new C1404("Invalid encoded value type (%d) for the first item in call site %d", Integer.valueOf(r0.m7098()), Integer.valueOf(this.f6567));
            }
        } else {
            throw new C1404("Invalid call site item: must contain at least 3 entries.", new Object[0]);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m6704() {
        C1153 r0 = m6700();
        if (r0.m6807() >= 3) {
            r0.m6805();
            C1273 r02 = r0.m6804();
            if (!f6565 && r02 == null) {
                throw new AssertionError();
            } else if (r02.m7098() == 23) {
                return ((C1284) r02).m7119();
            } else {
                throw new C1404("Invalid encoded value type (%d) for the second item in call site %d", Integer.valueOf(r02.m7098()), Integer.valueOf(this.f6567));
            }
        } else {
            throw new C1404("Invalid call site item: must contain at least 3 entries.", new Object[0]);
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public C1261 m6705() {
        C1153 r0 = m6700();
        if (r0.m6807() >= 3) {
            r0.m6805();
            r0.m6805();
            C1273 r02 = r0.m6804();
            if (!f6565 && r02 == null) {
                throw new AssertionError();
            } else if (r02.m7098() == 21) {
                return ((C1281) r02).m7114();
            } else {
                throw new C1404("Invalid encoded value type (%d) for the second item in call site %d", Integer.valueOf(r02.m7098()), Integer.valueOf(this.f6567));
            }
        } else {
            throw new C1404("Invalid call site item: must contain at least 3 entries.", new Object[0]);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public List<? extends C1273> m6706() {
        ArrayList r0 = C0926.m5776();
        C1153 r1 = m6700();
        if (r1.m6807() < 3) {
            throw new C1404("Invalid call site item: must contain at least 3 entries.", new Object[0]);
        } else if (r1.m6807() == 3) {
            return r0;
        } else {
            r1.m6805();
            r1.m6805();
            r1.m6805();
            for (C1273 r2 = r1.m6804(); r2 != null; r2 = r1.m6804()) {
                r0.add(r2);
            }
            return r0;
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private C1153 m6700() {
        return C1153.m6803(this.f6566, m6701());
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private int m6701() {
        if (this.f6569 < 0) {
            this.f6569 = this.f6566.m6854().m6962(this.f6568);
        }
        return this.f6569;
    }
}
