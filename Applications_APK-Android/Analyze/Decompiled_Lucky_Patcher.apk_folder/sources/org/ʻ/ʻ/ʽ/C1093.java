package org.ʻ.ʻ.ʽ;

import java.util.Set;
import org.ʻ.ʻ.ʻ.C1002;
import org.ʻ.ʻ.ʽ.ʾ.C1162;

/* renamed from: org.ʻ.ʻ.ʽ.ʻ  reason: contains not printable characters */
/* compiled from: DexBackedAnnotation */
public class C1093 extends C1002 {

    /* renamed from: ʼ  reason: contains not printable characters */
    public final C1163 f6525;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final int f6526;

    /* renamed from: ʾ  reason: contains not printable characters */
    public final int f6527;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final int f6528;

    public C1093(C1163 r1, int i) {
        this.f6525 = r1;
        C1184 r12 = r1.m6855().m6970(i);
        this.f6526 = r12.m6988();
        this.f6527 = r12.m6976();
        this.f6528 = r12.m6972();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6573() {
        return this.f6526;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m6574() {
        return (String) this.f6525.m6860().get(this.f6527);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Set<? extends C1133> m6575() {
        C1184 r0 = this.f6525.m6855().m6970(this.f6528);
        return new C1162<C1133>(this.f6525.m6855(), r0.m6972(), r0.m6976()) {
            /* access modifiers changed from: protected */
            /* renamed from: ʻ  reason: contains not printable characters */
            public C1133 m6577(C1184 r2, int i) {
                return new C1133(C1093.this.f6525, r2);
            }
        };
    }
}
