package org.ʻ.ʻ.ʽ.ʾ;

import com.google.ʻ.ʼ.C0891;
import com.google.ʻ.ʼ.C0904;
import java.util.List;
import java.util.Set;
import org.ʻ.ʻ.ʽ.C1093;
import org.ʻ.ʻ.ʽ.C1163;

/* renamed from: org.ʻ.ʻ.ʽ.ʾ.ʻ  reason: contains not printable characters */
/* compiled from: AnnotationsDirectory */
public abstract class C1146 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final C1146 f6644 = new C1146() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public Set<? extends C1093> m6768() {
            return C0904.m5655();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public C1147 m6769() {
            return C1147.f6651;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public C1147 m6770() {
            return C1147.f6651;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public C1147 m6771() {
            return C1147.f6651;
        }
    };

    /* renamed from: org.ʻ.ʻ.ʽ.ʾ.ʻ$ʻ  reason: contains not printable characters */
    /* compiled from: AnnotationsDirectory */
    public interface C1147 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public static final C1147 f6651 = new C1147() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public int m6777(int i) {
                return 0;
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        int m6776(int i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract Set<? extends C1093> m6764();

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract C1147 m6765();

    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract C1147 m6766();

    /* renamed from: ʾ  reason: contains not printable characters */
    public abstract C1147 m6767();

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1146 m6761(C1163 r1, int i) {
        if (i == 0) {
            return f6644;
        }
        return new C1148(r1, i);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static Set<? extends C1093> m6762(final C1163 r2, final int i) {
        if (i == 0) {
            return C0904.m5655();
        }
        final int r0 = r2.m6855().m6962(i);
        return new C1156<C1093>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C1093 m6773(int i) {
                return new C1093(r2, r2.m6855().m6962(i + 4 + (i * 4)));
            }

            public int size() {
                return r0;
            }
        };
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static List<Set<? extends C1093>> m6763(final C1163 r2, final int i) {
        if (i <= 0) {
            return C0891.m5603();
        }
        final int r0 = r2.m6855().m6962(i);
        return new C1155<Set<? extends C1093>>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public Set<? extends C1093> m6775(int i) {
                return C1146.m6762(r2, r2.m6855().m6962(i + 4 + (i * 4)));
            }

            public int size() {
                return r0;
            }
        };
    }

    /* renamed from: org.ʻ.ʻ.ʽ.ʾ.ʻ$ʼ  reason: contains not printable characters */
    /* compiled from: AnnotationsDirectory */
    private static class C1148 extends C1146 {

        /* renamed from: ʼ  reason: contains not printable characters */
        public final C1163 f6652;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final int f6653;

        public C1148(C1163 r1, int i) {
            this.f6652 = r1;
            this.f6653 = i;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public int m6782() {
            return this.f6652.m6855().m6962(this.f6653 + 4);
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public int m6783() {
            return this.f6652.m6855().m6962(this.f6653 + 8);
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public int m6784() {
            return this.f6652.m6855().m6962(this.f6653 + 12);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Set<? extends C1093> m6778() {
            C1163 r0 = this.f6652;
            return m6762(r0, r0.m6855().m6962(this.f6653));
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public C1147 m6779() {
            int r0 = m6782();
            if (r0 == 0) {
                return C1147.f6651;
            }
            return new C1149(this.f6653 + 16, r0);
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public C1147 m6780() {
            int r0 = m6783();
            if (r0 == 0) {
                return C1147.f6651;
            }
            return new C1149(this.f6653 + 16 + (m6782() * 8), r0);
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public C1147 m6781() {
            int r0 = m6784();
            if (r0 == 0) {
                return C1147.f6651;
            }
            return new C1149(this.f6653 + 16 + (m6782() * 8) + (m6783() * 8), r0);
        }

        /* renamed from: org.ʻ.ʻ.ʽ.ʾ.ʻ$ʼ$ʻ  reason: contains not printable characters */
        /* compiled from: AnnotationsDirectory */
        private class C1149 implements C1147 {

            /* renamed from: ʽ  reason: contains not printable characters */
            private final int f6655;

            /* renamed from: ʾ  reason: contains not printable characters */
            private final int f6656;

            /* renamed from: ʿ  reason: contains not printable characters */
            private int f6657 = 0;

            /* renamed from: ˆ  reason: contains not printable characters */
            private int f6658;

            public C1149(int i, int i2) {
                this.f6655 = i;
                this.f6656 = i2;
                this.f6658 = C1148.this.f6652.m6855().m6962(i);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public int m6785(int i) {
                while (this.f6658 < i) {
                    int i2 = this.f6657;
                    if (i2 + 1 >= this.f6656) {
                        break;
                    }
                    this.f6657 = i2 + 1;
                    this.f6658 = C1148.this.f6652.m6855().m6962(this.f6655 + (this.f6657 * 8));
                }
                if (this.f6658 == i) {
                    return C1148.this.f6652.m6855().m6962(this.f6655 + (this.f6657 * 8) + 4);
                }
                return 0;
            }
        }
    }
}
