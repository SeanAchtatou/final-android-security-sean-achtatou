package org.ʻ.ʻ.ʽ;

import com.google.ʻ.ʼ.C0891;
import com.google.ʻ.ʼ.C0904;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.ʻ.ʻ.ʻ.ʻ.C1007;
import org.ʻ.ʻ.ʽ.ʾ.C1146;
import org.ʻ.ʻ.ʽ.ʾ.C1155;
import org.ʻ.ʻ.ʽ.ʾ.C1157;
import org.ʻ.ʻ.ʾ.C1187;
import org.ʻ.ʻ.ʾ.C1288;
import org.ʻ.ʻ.ʾ.C1290;
import org.ʻ.ʼ.C1400;

/* renamed from: org.ʻ.ʻ.ʽ.ˉ  reason: contains not printable characters */
/* compiled from: DexBackedMethod */
public class C1179 extends C1007 implements C1288 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final C1163 f6768;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final C1145 f6769;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final int f6770;

    /* renamed from: ʾ  reason: contains not printable characters */
    public final int f6771;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final int f6772;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final int f6773;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final int f6774;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final int f6775;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f6776;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f6777;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f6778 = -1;

    public C1179(C1163 r2, C1184 r3, C1145 r4, int i, C1146.C1147 r6, C1146.C1147 r7) {
        this.f6768 = r2;
        this.f6769 = r4;
        this.f6775 = r3.m6972();
        this.f6771 = r3.m6978() + i;
        this.f6770 = r3.m6976();
        this.f6772 = r3.m6976();
        this.f6774 = r6.m6776(this.f6771);
        this.f6773 = r7.m6776(this.f6771);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m6925() {
        return this.f6769.m6738();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m6926() {
        return this.f6770;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m6927() {
        return (String) this.f6768.m6859().get(this.f6768.m6854().m6962(m6922() + 4));
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public String m6928() {
        return (String) this.f6768.m6860().get(this.f6768.m6854().m6962(m6923() + 4));
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public List<? extends C1290> m6929() {
        if (m6924() <= 0) {
            return C0891.m5603();
        }
        final List<String> r0 = m6932();
        return new C1400<C1290>() {
            public Iterator<C1290> iterator() {
                return new C1157(r0, C1179.this.m6930(), C1179.this.m6931());
            }

            public int size() {
                return r0.size();
            }
        };
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public List<? extends Set<? extends C1093>> m6930() {
        return C1146.m6763(this.f6768, this.f6773);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public Iterator<String> m6931() {
        C1180 r0 = m6935();
        if (r0 != null) {
            return r0.m6940(null);
        }
        return C0904.m5655().iterator();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public List<String> m6932() {
        int r0 = m6924();
        if (r0 <= 0) {
            return C0891.m5603();
        }
        final int r1 = this.f6768.m6855().m6962(r0 + 0);
        final int i = r0 + 4;
        return new C1155<String>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public String m6937(int i) {
                return (String) C1179.this.f6768.m6860().get(C1179.this.f6768.m6855().m6964(i + (i * 2)));
            }

            public int size() {
                return r1;
            }
        };
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public Set<? extends C1187> m6933() {
        return C1146.m6762(this.f6768, this.f6774);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public C1180 m6935() {
        int i = this.f6772;
        if (i <= 0) {
            return null;
        }
        C1163 r1 = this.f6768;
        return r1.m6851(r1, this, i);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private int m6922() {
        if (this.f6776 == 0) {
            this.f6776 = this.f6768.m6862().m6891(this.f6771);
        }
        return this.f6776;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private int m6923() {
        if (this.f6777 == 0) {
            this.f6777 = this.f6768.m6863().m6891(this.f6768.m6854().m6964(m6922() + 2));
        }
        return this.f6777;
    }

    /* renamed from: י  reason: contains not printable characters */
    private int m6924() {
        if (this.f6778 == -1) {
            this.f6778 = this.f6768.m6854().m6962(m6923() + 8);
        }
        return this.f6778;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m6921(C1184 r1, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            r1.m6983();
            r1.m6983();
            r1.m6983();
        }
    }
}
