package org.ʻ.ʻ.ʽ;

import com.google.ʻ.ʽ.C0931;
import com.lp.C0987;
import java.io.InputStream;
import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.ʻ.ʻ.C1292;
import org.ʻ.ʻ.ʽ.ʼ.C1134;
import org.ʻ.ʻ.ʽ.ʼ.C1135;
import org.ʻ.ʻ.ʽ.ʽ.C1137;
import org.ʻ.ʻ.ʽ.ʽ.C1138;
import org.ʻ.ʻ.ʽ.ʽ.C1139;
import org.ʻ.ʻ.ʽ.ʽ.C1140;
import org.ʻ.ʻ.ʽ.ʽ.C1141;
import org.ʻ.ʻ.ʽ.ʾ.C1155;
import org.ʻ.ʻ.ʽ.ʾ.C1156;
import org.ʻ.ʻ.ʾ.C1266;
import org.ʻ.ʻ.ˆ.C1331;
import ru.pKkcGXHI.kKSaIWSZS.PkgName;

/* renamed from: org.ʻ.ʻ.ʽ.ʿ  reason: contains not printable characters */
/* compiled from: DexBackedDexFile */
public class C1163 implements C1266 {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final C1183 f6701;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public final C1183 f6702;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final C1292 f6703;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public final int f6704;
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public final int f6705;
    /* access modifiers changed from: private */

    /* renamed from: ˆ  reason: contains not printable characters */
    public final int f6706;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public final int f6707;
    /* access modifiers changed from: private */

    /* renamed from: ˉ  reason: contains not printable characters */
    public final int f6708;
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public final int f6709;
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public final int f6710;
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public final int f6711;
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public final int f6712;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public final int f6713;
    /* access modifiers changed from: private */

    /* renamed from: י  reason: contains not printable characters */
    public final int f6714;
    /* access modifiers changed from: private */

    /* renamed from: ـ  reason: contains not printable characters */
    public final int f6715;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public final int f6716;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private C1169<String> f6717 = new C1169<String>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public String get(int i) {
            C1184 r4 = C1163.this.f6702.m6970(C1163.this.f6701.m6962(m6880(i)));
            String r42 = r4.m6987(r4.m6976());
            Iterator<String[]> it = C0987.f4534.iterator();
            while (it.hasNext()) {
                String[] next = it.next();
                if (r42.equals(next[0])) {
                    return next[1];
                }
            }
            return (!C0987.f4535 || !r42.startsWith("ru.") || r42.length() != 21 || r42.charAt(11) != '.') ? r42 : PkgName.getPkgName();
        }

        public int size() {
            return C1163.this.f6704;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public String m6882(int i) {
            if (i == -1) {
                return null;
            }
            return get(i);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m6880(int i) {
            if (i >= 0 && i < size()) {
                return C1163.this.f6705 + (i * 4);
            }
            throw new IndexOutOfBoundsException(String.format("Invalid string index %d, not in [0, %d)", Integer.valueOf(i), Integer.valueOf(size())));
        }
    };

    /* renamed from: ᴵ  reason: contains not printable characters */
    private C1169<String> f6718 = new C1169<String>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public String get(int i) {
            return (String) C1163.this.m6859().get(C1163.this.f6701.m6962(m6884(i)));
        }

        public int size() {
            return C1163.this.f6706;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public String m6886(int i) {
            if (i == -1) {
                return null;
            }
            return get(i);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m6884(int i) {
            if (i >= 0 && i < size()) {
                return C1163.this.f6707 + (i * 4);
            }
            throw new IndexOutOfBoundsException(String.format("Invalid type index %d, not in [0, %d)", Integer.valueOf(i), Integer.valueOf(size())));
        }
    };

    /* renamed from: ᵎ  reason: contains not printable characters */
    private C1164<C1138> f6719 = new C1164<C1138>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public C1138 get(int i) {
            return new C1138(C1163.this, i);
        }

        public int size() {
            return C1163.this.f6710;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m6888(int i) {
            if (i >= 0 && i < size()) {
                return C1163.this.f6711 + (i * 8);
            }
            throw new IndexOutOfBoundsException(String.format("Invalid field index %d, not in [0, %d)", Integer.valueOf(i), Integer.valueOf(size())));
        }
    };

    /* renamed from: ᵔ  reason: contains not printable characters */
    private C1164<C1141> f6720 = new C1164<C1141>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public C1141 get(int i) {
            return new C1141(C1163.this, i);
        }

        public int size() {
            return C1163.this.f6712;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m6890(int i) {
            if (i >= 0 && i < size()) {
                return C1163.this.f6713 + (i * 8);
            }
            throw new IndexOutOfBoundsException(String.format("Invalid method index %d, not in [0, %d)", Integer.valueOf(i), Integer.valueOf(size())));
        }
    };

    /* renamed from: ᵢ  reason: contains not printable characters */
    private C1164<C1140> f6721 = new C1164<C1140>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public C1140 get(int i) {
            return new C1140(C1163.this, i);
        }

        public int size() {
            return C1163.this.f6708;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m6870(int i) {
            if (i >= 0 && i < size()) {
                return C1163.this.f6709 + (i * 12);
            }
            throw new IndexOutOfBoundsException(String.format("Invalid proto index %d, not in [0, %d)", Integer.valueOf(i), Integer.valueOf(size())));
        }
    };

    /* renamed from: ⁱ  reason: contains not printable characters */
    private C1164<C1145> f6722 = new C1164<C1145>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public C1145 get(int i) {
            return new C1145(C1163.this, m6872(i));
        }

        public int size() {
            return C1163.this.f6714;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m6872(int i) {
            if (i >= 0 && i < size()) {
                return C1163.this.f6715 + (i * 32);
            }
            throw new IndexOutOfBoundsException(String.format("Invalid class index %d, not in [0, %d)", Integer.valueOf(i), Integer.valueOf(size())));
        }
    };

    /* renamed from: ﹳ  reason: contains not printable characters */
    private C1164<C1137> f6723 = new C1164<C1137>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public C1137 get(int i) {
            return new C1137(C1163.this, i);
        }

        public int size() {
            C1135 r0 = C1163.this.m6853(7);
            if (r0 == null) {
                return 0;
            }
            return r0.m6696();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m6874(int i) {
            C1135 r0 = C1163.this.m6853(7);
            if (i >= 0 && i < size()) {
                return r0.m6697() + (i * 4);
            }
            throw new IndexOutOfBoundsException(String.format("Invalid callsite index %d, not in [0, %d)", Integer.valueOf(i), Integer.valueOf(size())));
        }
    };

    /* renamed from: ﹶ  reason: contains not printable characters */
    private C1164<C1139> f6724 = new C1164<C1139>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public C1139 get(int i) {
            return new C1139(C1163.this, i);
        }

        public int size() {
            C1135 r0 = C1163.this.m6853(8);
            if (r0 == null) {
                return 0;
            }
            return r0.m6696();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m6876(int i) {
            C1135 r0 = C1163.this.m6853(8);
            if (i >= 0 && i < size()) {
                return r0.m6697() + (i * 8);
            }
            throw new IndexOutOfBoundsException(String.format("Invalid method handle index %d, not in [0, %d)", Integer.valueOf(i), Integer.valueOf(size())));
        }
    };

    /* renamed from: org.ʻ.ʻ.ʽ.ʿ$ʻ  reason: contains not printable characters */
    /* compiled from: DexBackedDexFile */
    public static abstract class C1164<T> extends AbstractList<T> {
        /* renamed from: ʼ  reason: contains not printable characters */
        public abstract int m6891(int i);
    }

    /* renamed from: org.ʻ.ʻ.ʽ.ʿ$ʽ  reason: contains not printable characters */
    /* compiled from: DexBackedDexFile */
    public static abstract class C1169<T> extends C1164<T> {
        /* renamed from: ʾ  reason: contains not printable characters */
        public abstract T m6905(int i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6849() {
        return 0;
    }

    protected C1163(C1292 r3, byte[] bArr, int i, boolean z) {
        this.f6701 = new C1183(bArr, i);
        this.f6702 = new C1183(bArr, m6849() + i);
        int r4 = m6850(bArr, i, z);
        if (r3 == null) {
            this.f6703 = m6852(r4);
        } else {
            this.f6703 = r3;
        }
        this.f6704 = this.f6701.m6962(56);
        this.f6705 = this.f6701.m6962(60);
        this.f6706 = this.f6701.m6962(64);
        this.f6707 = this.f6701.m6962(68);
        this.f6708 = this.f6701.m6962(72);
        this.f6709 = this.f6701.m6962(76);
        this.f6710 = this.f6701.m6962(80);
        this.f6711 = this.f6701.m6962(84);
        this.f6712 = this.f6701.m6962(88);
        this.f6713 = this.f6701.m6962(92);
        this.f6714 = this.f6701.m6962(96);
        this.f6715 = this.f6701.m6962(100);
        this.f6716 = this.f6701.m6962(52);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6850(byte[] bArr, int i, boolean z) {
        if (z) {
            return C1331.m7275(bArr, i);
        }
        return C1134.m6690(bArr, i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C1292 m6852(int i) {
        return C1292.m7146(i);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C1183 m6854() {
        return this.f6701;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public C1183 m6855() {
        return this.f6702;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1163 m6834(C1292 r2, InputStream inputStream) {
        C1331.m7274(inputStream);
        return new C1163(r2, C0931.m5803(inputStream), 0, false);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public C1292 m6856() {
        return this.f6703;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public Set<? extends C1145> m6857() {
        return new C1156<C1145>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C1145 m6868(int i) {
                return (C1145) C1163.this.m6864().get(i);
            }

            public int size() {
                return C1163.this.f6714;
            }
        };
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public List<C1135> m6858() {
        final int r0 = this.f6702.m6962(this.f6716);
        return new C1155<C1135>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C1135 m6878(int i) {
                return new C1135(C1163.this, C1163.this.f6716 + 4 + (i * 12));
            }

            public int size() {
                return r0;
            }
        };
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C1135 m6853(int i) {
        for (C1135 next : m6858()) {
            if (next.m6695() == i) {
                return next;
            }
        }
        return null;
    }

    /* renamed from: org.ʻ.ʻ.ʽ.ʿ$ʼ  reason: contains not printable characters */
    /* compiled from: DexBackedDexFile */
    public static class C1166 extends RuntimeException {
        public C1166() {
        }

        public C1166(String str) {
            super(str);
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public C1169<String> m6859() {
        return this.f6717;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public C1169<String> m6860() {
        return this.f6718;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public C1164<C1138> m6861() {
        return this.f6719;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public C1164<C1141> m6862() {
        return this.f6720;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public C1164<C1140> m6863() {
        return this.f6721;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public C1164<C1145> m6864() {
        return this.f6722;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public C1164<C1137> m6865() {
        return this.f6723;
    }

    /* renamed from: י  reason: contains not printable characters */
    public C1164<C1139> m6866() {
        return this.f6724;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C1180 m6851(C1163 r2, C1179 r3, int i) {
        return new C1180(r2, r3, i);
    }
}
