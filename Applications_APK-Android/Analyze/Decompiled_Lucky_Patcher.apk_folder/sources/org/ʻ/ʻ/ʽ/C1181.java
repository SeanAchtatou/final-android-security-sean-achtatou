package org.ʻ.ʻ.ʽ;

import java.util.List;
import org.ʻ.ʻ.ʻ.C1032;
import org.ʻ.ʻ.ʽ.ʾ.C1159;

/* renamed from: org.ʻ.ʻ.ʽ.ˋ  reason: contains not printable characters */
/* compiled from: DexBackedTryBlock */
public class C1181 extends C1032<C1177> {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final C1163 f6795;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f6796;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f6797;

    public C1181(C1163 r1, int i, int i2) {
        this.f6795 = r1;
        this.f6796 = i;
        this.f6797 = i2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6952() {
        return this.f6795.m6855().m6962(this.f6796 + 0);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m6953() {
        return this.f6795.m6855().m6964(this.f6796 + 4);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public List<? extends C1177> m6954() {
        C1184 r0 = this.f6795.m6855().m6970(this.f6797 + this.f6795.m6855().m6964(this.f6796 + 6));
        int r1 = r0.m6974();
        if (r1 > 0) {
            return new C1159<C1182>(this.f6795.m6855(), r0.m6972(), r1) {
                /* access modifiers changed from: protected */
                /* renamed from: ʻ  reason: contains not printable characters */
                public C1182 m6956(C1184 r2, int i) {
                    return new C1182(C1181.this.f6795, r2);
                }
            };
        }
        final int i = (r1 * -1) + 1;
        return new C1159<C1177>(this.f6795.m6855(), r0.m6972(), i) {
            /* access modifiers changed from: protected */
            /* renamed from: ʻ  reason: contains not printable characters */
            public C1177 m6958(C1184 r2, int i) {
                if (i == i - 1) {
                    return new C1136(r2);
                }
                return new C1182(C1181.this.f6795, r2);
            }
        };
    }
}
