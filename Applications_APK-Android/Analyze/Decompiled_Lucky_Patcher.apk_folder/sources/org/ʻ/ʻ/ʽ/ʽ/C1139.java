package org.ʻ.ʻ.ʽ.ʽ;

import org.ʻ.ʻ.ʻ.ʻ.C1005;
import org.ʻ.ʻ.ʽ.C1163;
import org.ʻ.ʻ.ʾ.ʽ.C1263;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ʽ.ʽ.ʽ  reason: contains not printable characters */
/* compiled from: DexBackedMethodHandleReference */
public class C1139 extends C1005 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final C1163 f6572;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final int f6573;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final int f6574;

    public C1139(C1163 r1, int i) {
        this.f6572 = r1;
        this.f6573 = i;
        this.f6574 = r1.m6866().m6891(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6710() {
        return this.f6572.m6854().m6964(this.f6574 + 0);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C1263 m6711() {
        int r0 = this.f6572.m6854().m6964(this.f6574 + 4);
        switch (m6710()) {
            case 0:
            case 1:
            case 2:
            case 3:
                return new C1138(this.f6572, r0);
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                return new C1141(this.f6572, r0);
            default:
                throw new C1404("Invalid method handle type: %d", Integer.valueOf(m6710()));
        }
    }
}
