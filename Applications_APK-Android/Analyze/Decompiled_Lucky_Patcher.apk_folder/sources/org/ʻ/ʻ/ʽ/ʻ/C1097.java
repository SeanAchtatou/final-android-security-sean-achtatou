package org.ʻ.ʻ.ʽ.ʻ;

import org.ʻ.ʻ.C1185;
import org.ʻ.ʻ.ʽ.C1163;
import org.ʻ.ʻ.ʽ.C1184;
import org.ʻ.ʻ.ʾ.ʼ.C1240;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ʽ.ʻ.ʼ  reason: contains not printable characters */
/* compiled from: DexBackedInstruction */
public abstract class C1097 implements C1240 {

    /* renamed from: ʾ  reason: contains not printable characters */
    public final C1163 f6542;

    /* renamed from: ʿ  reason: contains not printable characters */
    public final C1185 f6543;

    /* renamed from: ˆ  reason: contains not printable characters */
    public final int f6544;

    public C1097(C1163 r1, C1185 r2, int i) {
        this.f6542 = r1;
        this.f6543 = r2;
        this.f6544 = i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C1185 m6598() {
        return this.f6543;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6599() {
        return this.f6543.f7077.f6524 / 2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1240 m6597(C1163 r3, C1184 r4) {
        int r0 = r4.m6986();
        if (r0 == 0) {
            r0 = r4.m6984();
        }
        C1097 r32 = m6596(r3, r3.m6856().m7149(r0), ((r4.m6972() + r4.f6806.m6961()) - r3.m6854().m6961()) - r3.m6849());
        r4.m6975(r32.m7035() * 2);
        return r32;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static C1097 m6596(C1163 r2, C1185 r3, int i) {
        if (r3 == null) {
            return new C1113(r2, i);
        }
        switch (r3.f7077) {
            case Format10t:
                return new C1099(r2, r3, i);
            case Format10x:
                return new C1101(r2, r3, i);
            case Format11n:
                return new C1103(r2, r3, i);
            case Format11x:
                return new C1105(r2, r3, i);
            case Format12x:
                return new C1107(r2, r3, i);
            case Format20bc:
                return new C1109(r2, r3, i);
            case Format20t:
                return new C1111(r2, r3, i);
            case Format21c:
                return new C1112(r2, r3, i);
            case Format21ih:
                return new C1114(r2, r3, i);
            case Format21lh:
                return new C1115(r2, r3, i);
            case Format21s:
                return new C1116(r2, r3, i);
            case Format21t:
                return new C1117(r2, r3, i);
            case Format22b:
                return new C1118(r2, r3, i);
            case Format22c:
                return new C1120(r2, r3, i);
            case Format22cs:
                return new C1121(r2, r3, i);
            case Format22s:
                return new C1123(r2, r3, i);
            case Format22t:
                return new C1125(r2, r3, i);
            case Format22x:
                return new C1126(r2, r3, i);
            case Format23x:
                return new C1127(r2, r3, i);
            case Format30t:
                return new C1128(r2, r3, i);
            case Format31c:
                return new C1129(r2, r3, i);
            case Format31i:
                return new C1130(r2, r3, i);
            case Format31t:
                return new C1131(r2, r3, i);
            case Format32x:
                return new C1132(r2, r3, i);
            case Format35c:
                return new C1122(r2, r3, i);
            case Format35ms:
                return new C1096(r2, r3, i);
            case Format35mi:
                return new C1124(r2, r3, i);
            case Format3rc:
                return new C1100(r2, r3, i);
            case Format3rmi:
                return new C1098(r2, r3, i);
            case Format3rms:
                return new C1104(r2, r3, i);
            case Format45cc:
                return new C1102(r2, r3, i);
            case Format4rcc:
                return new C1119(r2, r3, i);
            case Format51l:
                return new C1106(r2, r3, i);
            case PackedSwitchPayload:
                return new C1110(r2, i);
            case SparseSwitchPayload:
                return new C1108(r2, i);
            case ArrayPayload:
                return new C1094(r2, i);
            default:
                throw new C1404("Unexpected opcode format: %s", r3.f7077.toString());
        }
    }
}
