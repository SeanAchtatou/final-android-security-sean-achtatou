package org.ʻ.ʻ.ʽ;

import com.google.ʻ.ʼ.C0891;
import java.util.Iterator;
import java.util.List;
import org.ʻ.ʻ.ʽ.ʻ.C1097;
import org.ʻ.ʻ.ʽ.ʾ.C1150;
import org.ʻ.ʻ.ʽ.ʾ.C1155;
import org.ʻ.ʻ.ʽ.ʾ.C1161;
import org.ʻ.ʻ.ʾ.C1289;
import org.ʻ.ʻ.ʾ.ʻ.C1188;
import org.ʻ.ʻ.ʾ.ʼ.C1240;
import org.ʻ.ʻ.ˆ.C1329;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ʽ.ˊ  reason: contains not printable characters */
/* compiled from: DexBackedMethodImplementation */
public class C1180 implements C1289 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final C1163 f6784;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final C1179 f6785;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final int f6786;

    protected C1180(C1163 r1, C1179 r2, int i) {
        this.f6784 = r1;
        this.f6785 = r2;
        this.f6786 = i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6939() {
        return this.f6784.m6855().m6964(this.f6786);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m6941() {
        return this.f6784.m6855().m6962(this.f6786 + 12);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˆ  reason: contains not printable characters */
    public int m6945() {
        return this.f6786 + 16;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public Iterable<? extends C1240> m6944() {
        int r0 = m6941();
        final int r1 = m6945();
        final int i = (r0 * 2) + r1;
        return new Iterable<C1240>() {
            public Iterator<C1240> iterator() {
                return new C1161<C1240>(C1180.this.f6784.m6855(), r1) {
                    /* access modifiers changed from: protected */
                    /* renamed from: ʻ  reason: contains not printable characters */
                    public C1240 m6949(C1184 r4) {
                        if (r4.m6972() >= i) {
                            return (C1240) m5444();
                        }
                        C1240 r0 = C1097.m6597(C1180.this.f6784, r4);
                        int r42 = r4.m6972();
                        if (r42 <= i && r42 >= 0) {
                            return r0;
                        }
                        throw new C1404("The last instruction in method %s is truncated", C1180.this.f6785);
                    }
                };
            }
        };
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public int m6946() {
        return this.f6784.m6855().m6964(this.f6786 + 6);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public List<? extends C1181> m6942() {
        final int r0 = m6946();
        if (r0 <= 0) {
            return C0891.m5603();
        }
        final int r1 = C1329.m7273(m6945() + (m6941() * 2), 4);
        final int i = (r0 * 8) + r1;
        return new C1155<C1181>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C1181 m6951(int i) {
                return new C1181(C1180.this.f6784, r1 + (i * 8), i);
            }

            public int size() {
                return r0;
            }
        };
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˉ  reason: contains not printable characters */
    public int m6947() {
        return this.f6784.m6855().m6967(this.f6786 + 8);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private C1150 m6938() {
        int r0 = m6947();
        if (r0 == -1 || r0 == 0) {
            return C1150.m6786(this.f6784, 0, this);
        }
        if (r0 < 0) {
            System.err.println(String.format("%s: Invalid debug offset", this.f6785));
            return C1150.m6786(this.f6784, 0, this);
        } else if (this.f6784.m6849() + r0 < this.f6784.m6854().f6804.length) {
            return C1150.m6786(this.f6784, r0, this);
        } else {
            System.err.println(String.format("%s: Invalid debug offset", this.f6785));
            return C1150.m6786(this.f6784, 0, this);
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public Iterable<? extends C1188> m6943() {
        return m6938();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Iterator<String> m6940(C1184 r2) {
        return m6938().m6787(r2);
    }
}
