package org.ʻ.ʻ.ʽ;

import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ʽ.ˏ  reason: contains not printable characters */
/* compiled from: DexBuffer */
public class C1183 {

    /* renamed from: ʻ  reason: contains not printable characters */
    final byte[] f6804;

    /* renamed from: ʼ  reason: contains not printable characters */
    final int f6805;

    public C1183(byte[] bArr) {
        this(bArr, 0);
    }

    public C1183(byte[] bArr, int i) {
        this.f6804 = bArr;
        this.f6805 = i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6962(int i) {
        byte[] bArr = this.f6804;
        int i2 = i + this.f6805;
        byte b = (bArr[i2 + 3] << 24) | (bArr[i2] & 255) | ((bArr[i2 + 1] & 255) << 8) | ((bArr[i2 + 2] & 255) << 16);
        if (b >= 0) {
            return b;
        }
        throw new C1404("Encountered small uint that is out of range at offset 0x%x", Integer.valueOf(i2));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m6963(int i) {
        byte[] bArr = this.f6804;
        int i2 = i + this.f6805;
        byte b = (bArr[i2 + 3] << 24) | (bArr[i2] & 255) | ((bArr[i2 + 1] & 255) << 8) | ((bArr[i2 + 2] & 255) << 16);
        if (b >= -1) {
            return b;
        }
        throw new C1404("Encountered optional uint that is out of range at offset 0x%x", Integer.valueOf(i2));
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6964(int i) {
        byte[] bArr = this.f6804;
        int i2 = i + this.f6805;
        return ((bArr[i2 + 1] & 255) << 8) | (bArr[i2] & 255);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m6965(int i) {
        return this.f6804[i + this.f6805] & 255;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public long m6966(int i) {
        byte[] bArr = this.f6804;
        int i2 = i + this.f6805;
        return ((long) ((bArr[i2] & 255) | ((bArr[i2 + 1] & 255) << 8) | ((bArr[i2 + 2] & 255) << 16))) | ((((long) bArr[i2 + 3]) & 255) << 24) | ((((long) bArr[i2 + 4]) & 255) << 32) | ((((long) bArr[i2 + 5]) & 255) << 40) | ((((long) bArr[i2 + 6]) & 255) << 48) | (((long) bArr[i2 + 7]) << 56);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public int m6967(int i) {
        byte[] bArr = this.f6804;
        int i2 = i + this.f6805;
        return (bArr[i2 + 3] << 24) | (bArr[i2] & 255) | ((bArr[i2 + 1] & 255) << 8) | ((bArr[i2 + 2] & 255) << 16);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public int m6968(int i) {
        byte[] bArr = this.f6804;
        int i2 = i + this.f6805;
        return (bArr[i2 + 1] << 8) | (bArr[i2] & 255);
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public int m6969(int i) {
        return this.f6804[this.f6805 + i];
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public C1184 m6970(int i) {
        return new C1184(this, i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6961() {
        return this.f6805;
    }
}
