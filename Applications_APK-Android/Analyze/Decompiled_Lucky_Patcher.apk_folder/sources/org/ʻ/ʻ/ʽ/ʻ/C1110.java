package org.ʻ.ʻ.ʽ.ʻ;

import java.util.List;
import org.ʻ.ʻ.C1185;
import org.ʻ.ʻ.ʽ.C1163;
import org.ʻ.ʻ.ʽ.ʾ.C1155;
import org.ʻ.ʻ.ʾ.ʼ.C1249;
import org.ʻ.ʻ.ʾ.ʼ.ʻ.C1210;

/* renamed from: org.ʻ.ʻ.ʽ.ʻ.ˉˉ  reason: contains not printable characters */
/* compiled from: DexBackedPackedSwitchPayload */
public class C1110 extends C1097 implements C1210 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final int f6553;

    public C1110(C1163 r2, int i) {
        super(r2, C1185.PACKED_SWITCH_PAYLOAD, i);
        this.f6553 = r2.m6855().m6964(i + 2);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public List<? extends C1249> m6634() {
        final int r0 = this.f6542.m6855().m6967(this.f6544 + 4);
        return new C1155<C1249>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C1249 m6636(final int i) {
                return new C1249() {
                    /* renamed from: ʻ  reason: contains not printable characters */
                    public int m6637() {
                        return r0 + i;
                    }

                    /* renamed from: ʼ  reason: contains not printable characters */
                    public int m6638() {
                        return C1110.this.f6542.m6855().m6967(C1110.this.f6544 + 8 + (i * 4));
                    }
                };
            }

            public int size() {
                return C1110.this.f6553;
            }
        };
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6633() {
        return (this.f6553 * 2) + 4;
    }
}
