package org.ʻ.ʻ.ʽ.ʾ;

import java.util.AbstractSequentialList;
import org.ʻ.ʻ.ʽ.C1183;
import org.ʻ.ʻ.ʽ.C1184;

/* renamed from: org.ʻ.ʻ.ʽ.ʾ.ˉ  reason: contains not printable characters */
/* compiled from: VariableSizeList */
public abstract class C1159<T> extends AbstractSequentialList<T> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C1183 f6688;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f6689;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f6690;

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract T m6825(C1184 r1, int i);

    public C1159(C1183 r1, int i, int i2) {
        this.f6688 = r1;
        this.f6689 = i;
        this.f6690 = i2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C1160<T> listIterator() {
        return listIterator(0);
    }

    public int size() {
        return this.f6690;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C1160<T> listIterator(int i) {
        AnonymousClass1 r0 = new C1160<T>(this.f6688, this.f6689, this.f6690) {
            /* access modifiers changed from: protected */
            /* renamed from: ʻ  reason: contains not printable characters */
            public T m6826(C1184 r2, int i) {
                return C1159.this.m6825(r2, i);
            }
        };
        for (int i2 = 0; i2 < i; i2++) {
            r0.next();
        }
        return r0;
    }
}
