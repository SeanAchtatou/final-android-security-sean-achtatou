package org.ʻ.ʻ.ʽ.ʼ;

import org.ʻ.ʻ.C1399;
import org.ʻ.ʻ.ʽ.C1183;

/* renamed from: org.ʻ.ʻ.ʽ.ʼ.ʻ  reason: contains not printable characters */
/* compiled from: HeaderItem */
public class C1134 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final byte[] f6561 = {100, 101, 120, 10, 0, 0, 0, 0};

    /* renamed from: ʻ  reason: contains not printable characters */
    public static byte[] m6689(int i) {
        return m6691(C1399.m7784(i));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static byte[] m6691(int i) {
        byte[] bArr = (byte[]) f6561.clone();
        if (i < 0 || i > 999) {
            throw new IllegalArgumentException("dexVersion must be within [0, 999]");
        }
        for (int i2 = 6; i2 >= 4; i2--) {
            bArr[i2] = (byte) ((i % 10) + 48);
            i /= 10;
        }
        return bArr;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m6688(byte[] bArr, int i) {
        if (bArr.length - i < 8) {
            return false;
        }
        int i2 = 0;
        while (true) {
            if (i2 >= 4) {
                for (int i3 = 4; i3 < 7; i3++) {
                    int i4 = i + i3;
                    if (bArr[i4] < 48 || bArr[i4] > 57) {
                        return false;
                    }
                }
                if (bArr[i + 7] != f6561[7]) {
                    return false;
                }
                return true;
            } else if (bArr[i + i2] != f6561[i2]) {
                return false;
            } else {
                i2++;
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int m6690(byte[] bArr, int i) {
        if (!m6688(bArr, i)) {
            return -1;
        }
        return m6694(bArr, i);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private static int m6694(byte[] bArr, int i) {
        return ((bArr[i + 4] - 48) * 100) + ((bArr[i + 5] - 48) * 10) + (bArr[i + 6] - 48);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static boolean m6693(int i) {
        return C1399.m7783(i) != -1;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static int m6692(byte[] bArr, int i) {
        return new C1183(bArr).m6967(i + 40);
    }
}
