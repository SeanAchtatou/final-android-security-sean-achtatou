package org.ʻ.ʻ.ʽ.ʾ;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: org.ʻ.ʻ.ʽ.ʾ.ʿ  reason: contains not printable characters */
/* compiled from: FixedSizeSet */
public abstract class C1156<T> extends AbstractSet<T> {
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract T m6817(int i);

    public Iterator<T> iterator() {
        return new Iterator<T>() {

            /* renamed from: ʻ  reason: contains not printable characters */
            int f6676 = 0;

            public boolean hasNext() {
                return this.f6676 < C1156.this.size();
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }

            public T next() {
                if (hasNext()) {
                    C1156 r0 = C1156.this;
                    int i = this.f6676;
                    this.f6676 = i + 1;
                    return r0.m6817(i);
                }
                throw new NoSuchElementException();
            }
        };
    }
}
