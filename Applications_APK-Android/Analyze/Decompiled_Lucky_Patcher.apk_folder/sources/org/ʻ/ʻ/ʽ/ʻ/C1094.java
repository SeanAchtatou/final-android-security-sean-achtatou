package org.ʻ.ʻ.ʽ.ʻ;

import com.google.ʻ.ʼ.C0891;
import java.util.List;
import org.ʻ.ʻ.C1185;
import org.ʻ.ʻ.ʽ.C1163;
import org.ʻ.ʻ.ʽ.ʾ.C1155;
import org.ʻ.ʻ.ʾ.ʼ.ʻ.C1199;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ʽ.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: DexBackedArrayPayload */
public class C1094 extends C1097 implements C1199 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final C1185 f6530 = C1185.ARRAY_PAYLOAD;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final int f6531;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final int f6532;

    public C1094(C1163 r5, int i) {
        super(r5, f6530, i);
        int r0 = r5.m6855().m6964(i + 2);
        if (r0 == 0) {
            this.f6531 = 1;
            this.f6532 = 0;
            return;
        }
        this.f6531 = r0;
        this.f6532 = r5.m6855().m6962(i + 4);
        if (((long) this.f6531) * ((long) this.f6532) > 2147483647L) {
            throw new C1404("Invalid array-payload instruction: element width*count overflows", new Object[0]);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m6579() {
        return this.f6531;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public List<Number> m6580() {
        final int i = this.f6544 + 8;
        if (this.f6532 == 0) {
            return C0891.m5603();
        }
        int i2 = this.f6531;
        if (i2 == 1) {
            return new C1095() {
                /* renamed from: ʻ  reason: contains not printable characters */
                public Number m6582(int i) {
                    return Integer.valueOf(C1094.this.f6542.m6855().m6969(i + i));
                }
            };
        }
        if (i2 == 2) {
            return new C1095() {
                /* renamed from: ʻ  reason: contains not printable characters */
                public Number m6584(int i) {
                    return Integer.valueOf(C1094.this.f6542.m6855().m6968(i + (i * 2)));
                }
            };
        }
        if (i2 == 4) {
            return new C1095() {
                /* renamed from: ʻ  reason: contains not printable characters */
                public Number m6586(int i) {
                    return Integer.valueOf(C1094.this.f6542.m6855().m6967(i + (i * 4)));
                }
            };
        }
        if (i2 == 8) {
            return new C1095() {
                /* renamed from: ʻ  reason: contains not printable characters */
                public Number m6588(int i) {
                    return Long.valueOf(C1094.this.f6542.m6855().m6966(i + (i * 8)));
                }
            };
        }
        throw new C1404("Invalid element width: %d", Integer.valueOf(i2));
    }

    /* renamed from: org.ʻ.ʻ.ʽ.ʻ.ʻ$ʻ  reason: contains not printable characters */
    /* compiled from: DexBackedArrayPayload */
    abstract class C1095 extends C1155<Number> {
        C1095() {
        }

        public int size() {
            return C1094.this.f6532;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6578() {
        return (((this.f6531 * this.f6532) + 1) / 2) + 4;
    }
}
