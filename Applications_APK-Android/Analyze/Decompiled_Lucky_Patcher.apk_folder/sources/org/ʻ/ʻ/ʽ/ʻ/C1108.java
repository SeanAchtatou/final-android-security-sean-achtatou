package org.ʻ.ʻ.ʽ.ʻ;

import java.util.List;
import org.ʻ.ʻ.C1185;
import org.ʻ.ʻ.ʽ.C1163;
import org.ʻ.ʻ.ʽ.ʾ.C1155;
import org.ʻ.ʻ.ʾ.ʼ.C1249;
import org.ʻ.ʻ.ʾ.ʼ.ʻ.C1214;

/* renamed from: org.ʻ.ʻ.ʽ.ʻ.ˈˈ  reason: contains not printable characters */
/* compiled from: DexBackedSparseSwitchPayload */
public class C1108 extends C1097 implements C1214 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final int f6546;

    public C1108(C1163 r2, int i) {
        super(r2, C1185.SPARSE_SWITCH_PAYLOAD, i);
        this.f6546 = r2.m6855().m6964(i + 2);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public List<? extends C1249> m6625() {
        return new C1155<C1249>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C1249 m6627(final int i) {
                return new C1249() {
                    /* renamed from: ʻ  reason: contains not printable characters */
                    public int m6628() {
                        return C1108.this.f6542.m6855().m6967(C1108.this.f6544 + 4 + (i * 4));
                    }

                    /* renamed from: ʼ  reason: contains not printable characters */
                    public int m6629() {
                        return C1108.this.f6542.m6855().m6967(C1108.this.f6544 + 4 + (C1108.this.f6546 * 4) + (i * 4));
                    }
                };
            }

            public int size() {
                return C1108.this.f6546;
            }
        };
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6624() {
        return (this.f6546 * 4) + 2;
    }
}
