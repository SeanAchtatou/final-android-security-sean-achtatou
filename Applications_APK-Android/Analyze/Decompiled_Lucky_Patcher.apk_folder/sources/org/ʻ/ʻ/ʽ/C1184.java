package org.ʻ.ʻ.ʽ;

import org.ʻ.ʻ.ʽ.C1183;
import org.ʻ.ʼ.C1404;
import org.ʻ.ʼ.C1409;

/* renamed from: org.ʻ.ʻ.ʽ.ˑ  reason: contains not printable characters */
/* compiled from: DexReader */
public class C1184<T extends C1183> {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final T f6806;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f6807;

    public C1184(T t, int i) {
        this.f6806 = t;
        this.f6807 = i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m6972() {
        return this.f6807;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m6973(int i) {
        this.f6807 = i;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m6974() {
        int i;
        int i2 = this.f6806.f6805 + this.f6807;
        byte[] bArr = this.f6806.f6804;
        int i3 = i2 + 1;
        byte b = bArr[i2] & 255;
        if (b <= Byte.MAX_VALUE) {
            i = (b << 25) >> 25;
        } else {
            int i4 = i3 + 1;
            byte b2 = bArr[i3] & 255;
            byte b3 = (b & Byte.MAX_VALUE) | ((b2 & Byte.MAX_VALUE) << 7);
            if (b2 <= Byte.MAX_VALUE) {
                i = (b3 << 18) >> 18;
            } else {
                i3 = i4 + 1;
                byte b4 = bArr[i4] & 255;
                byte b5 = b3 | ((b4 & Byte.MAX_VALUE) << 14);
                if (b4 <= Byte.MAX_VALUE) {
                    i = (b5 << 11) >> 11;
                } else {
                    i4 = i3 + 1;
                    byte b6 = bArr[i3] & 255;
                    byte b7 = b5 | ((b6 & Byte.MAX_VALUE) << 21);
                    if (b6 <= Byte.MAX_VALUE) {
                        i = (b7 << 4) >> 4;
                    } else {
                        i3 = i4 + 1;
                        byte b8 = bArr[i4] & 255;
                        if (b8 <= Byte.MAX_VALUE) {
                            i = b7 | (b8 << 28);
                        } else {
                            throw new C1404("Invalid sleb128 integer encountered at offset 0x%x", Integer.valueOf(this.f6807));
                        }
                    }
                }
            }
            i3 = i4;
        }
        this.f6807 = i3 - this.f6806.f6805;
        return i;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6976() {
        return m6971(false);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m6971(boolean z) {
        int i = this.f6806.f6805 + this.f6807;
        byte[] bArr = this.f6806.f6804;
        int i2 = i + 1;
        byte b = bArr[i] & 255;
        if (b > Byte.MAX_VALUE) {
            int i3 = i2 + 1;
            byte b2 = bArr[i2] & 255;
            b = (b & Byte.MAX_VALUE) | ((b2 & Byte.MAX_VALUE) << 7);
            if (b2 > Byte.MAX_VALUE) {
                i2 = i3 + 1;
                byte b3 = bArr[i3] & 255;
                b |= (b3 & Byte.MAX_VALUE) << 14;
                if (b3 > Byte.MAX_VALUE) {
                    i3 = i2 + 1;
                    byte b4 = bArr[i2] & 255;
                    b |= (b4 & Byte.MAX_VALUE) << 21;
                    if (b4 > Byte.MAX_VALUE) {
                        i2 = i3 + 1;
                        byte b5 = bArr[i3];
                        if (b5 < 0) {
                            throw new C1404("Invalid uleb128 integer encountered at offset 0x%x", Integer.valueOf(this.f6807));
                        } else if ((b5 & 15) <= 7 || z) {
                            b |= b5 << 28;
                        } else {
                            throw new C1404("Encountered valid uleb128 that is out of range at offset 0x%x", Integer.valueOf(this.f6807));
                        }
                    }
                }
            }
            i2 = i3;
        }
        this.f6807 = i2 - this.f6806.f6805;
        return b;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m6978() {
        return m6971(true);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m6980() {
        int i = this.f6806.f6805 + this.f6807;
        byte[] bArr = this.f6806.f6804;
        int i2 = i + 1;
        byte b = bArr[i] & 255;
        if (b > Byte.MAX_VALUE) {
            int i3 = i2 + 1;
            byte b2 = bArr[i2] & 255;
            b = (b & Byte.MAX_VALUE) | ((b2 & Byte.MAX_VALUE) << 7);
            if (b2 > Byte.MAX_VALUE) {
                i2 = i3 + 1;
                byte b3 = bArr[i3] & 255;
                b |= (b3 & Byte.MAX_VALUE) << 14;
                if (b3 > Byte.MAX_VALUE) {
                    i3 = i2 + 1;
                    byte b4 = bArr[i2] & 255;
                    b |= (b4 & Byte.MAX_VALUE) << 21;
                    if (b4 > Byte.MAX_VALUE) {
                        i2 = i3 + 1;
                        byte b5 = bArr[i3];
                        if (b5 >= 0) {
                            b |= b5 << 28;
                        } else {
                            throw new C1404("Invalid uleb128 integer encountered at offset 0x%x", Integer.valueOf(this.f6807));
                        }
                    }
                }
            }
            i2 = i3;
        }
        this.f6807 = i2 - this.f6806.f6805;
        return b;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m6983() {
        int i = this.f6806.f6805 + this.f6807;
        byte[] bArr = this.f6806.f6804;
        int i2 = i + 1;
        if (bArr[i] < 0) {
            int i3 = i2 + 1;
            if (bArr[i2] < 0) {
                i2 = i3 + 1;
                if (bArr[i3] < 0) {
                    i3 = i2 + 1;
                    if (bArr[i2] < 0) {
                        i2 = i3 + 1;
                        if (bArr[i3] < 0) {
                            throw new C1404("Invalid uleb128 integer encountered at offset 0x%x", Integer.valueOf(this.f6807));
                        }
                    }
                }
            }
            i2 = i3;
        }
        this.f6807 = i2 - this.f6806.f6805;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ˈ  reason: contains not printable characters */
    public int m6984() {
        /*
            r2 = this;
            T r0 = r2.f6806
            int r1 = r2.f6807
            int r0 = r0.m6964(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ʽ.C1184.m6984():int");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ˉ  reason: contains not printable characters */
    public int m6986() {
        /*
            r2 = this;
            T r0 = r2.f6806
            int r1 = r2.f6807
            int r0 = r0.m6965(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ʽ.C1184.m6986():int");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ˊ  reason: contains not printable characters */
    public int m6988() {
        /*
            r2 = this;
            int r0 = r2.f6807
            T r1 = r2.f6806
            int r1 = r1.m6965(r0)
            int r0 = r0 + 1
            r2.f6807 = r0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ʽ.C1184.m6988():int");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ˋ  reason: contains not printable characters */
    public int m6989() {
        /*
            r2 = this;
            int r0 = r2.f6807
            T r1 = r2.f6806
            int r1 = r1.m6969(r0)
            int r0 = r0 + 1
            r2.f6807 = r0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ʽ.C1184.m6989():int");
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public void m6990() {
        this.f6807++;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m6975(int i) {
        this.f6807 += i;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6977(int i) {
        byte b;
        byte b2;
        int i2;
        int i3 = this.f6806.f6805 + this.f6807;
        byte[] bArr = this.f6806.f6804;
        if (i != 1) {
            if (i == 2) {
                b2 = bArr[i3] & 255;
                i2 = bArr[i3 + 1] << 8;
            } else if (i == 3) {
                b2 = (bArr[i3] & 255) | ((bArr[i3 + 1] & 255) << 8);
                i2 = bArr[i3 + 2] << 16;
            } else if (i == 4) {
                b2 = (bArr[i3] & 255) | ((bArr[i3 + 1] & 255) << 8) | ((bArr[i3 + 2] & 255) << 16);
                i2 = bArr[i3 + 3] << 24;
            } else {
                throw new C1404("Invalid size %d for sized int at offset 0x%x", Integer.valueOf(i), Integer.valueOf(this.f6807));
            }
            b = i2 | b2;
        } else {
            b = bArr[i3];
        }
        this.f6807 = (i3 + i) - this.f6806.f6805;
        return b;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m6979(int i) {
        int i2 = this.f6806.f6805 + this.f6807;
        byte[] bArr = this.f6806.f6804;
        int i3 = 0;
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i == 4) {
                        byte b = bArr[i2 + 3];
                        if (b >= 0) {
                            i3 = b << 24;
                        } else {
                            throw new C1404("Encountered valid sized uint that is out of range at offset 0x%x", Integer.valueOf(this.f6807));
                        }
                    } else {
                        throw new C1404("Invalid size %d for sized uint at offset 0x%x", Integer.valueOf(i), Integer.valueOf(this.f6807));
                    }
                }
                i3 |= (bArr[i2 + 2] & 255) << 16;
            }
            i3 |= (bArr[i2 + 1] & 255) << 8;
        }
        byte b2 = (bArr[i2] & 255) | i3;
        this.f6807 = (i2 + i) - this.f6806.f6805;
        return b2;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m6981(int i) {
        int i2;
        int i3;
        byte b;
        int i4 = this.f6806.f6805 + this.f6807;
        byte[] bArr = this.f6806.f6804;
        if (i != 1) {
            if (i == 2) {
                i3 = (bArr[i4] & 255) << 16;
                b = bArr[i4 + 1];
            } else if (i == 3) {
                i3 = ((bArr[i4] & 255) << 8) | ((bArr[i4 + 1] & 255) << 16);
                b = bArr[i4 + 2];
            } else if (i == 4) {
                i3 = (bArr[i4] & 255) | ((bArr[i4 + 1] & 255) << 8) | ((bArr[i4 + 2] & 255) << 16);
                b = bArr[i4 + 3];
            } else {
                throw new C1404("Invalid size %d for sized, right extended int at offset 0x%x", Integer.valueOf(i), Integer.valueOf(this.f6807));
            }
            i2 = (b << 24) | i3;
        } else {
            i2 = bArr[i4] << 24;
        }
        this.f6807 = (i4 + i) - this.f6806.f6805;
        return i2;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public long m6982(int i) {
        long j;
        long j2;
        byte b;
        int i2 = this.f6806.f6805 + this.f6807;
        byte[] bArr = this.f6806.f6804;
        switch (i) {
            case 1:
                j = ((long) bArr[i2]) << 56;
                break;
            case 2:
                j2 = (((long) bArr[i2]) & 255) << 48;
                b = bArr[i2 + 1];
                j = j2 | (((long) b) << 56);
                break;
            case 3:
                j2 = ((((long) bArr[i2]) & 255) << 40) | ((255 & ((long) bArr[i2 + 1])) << 48);
                b = bArr[i2 + 2];
                j = j2 | (((long) b) << 56);
                break;
            case 4:
                j2 = ((((long) bArr[i2]) & 255) << 32) | ((((long) bArr[i2 + 1]) & 255) << 40) | ((255 & ((long) bArr[i2 + 2])) << 48);
                b = bArr[i2 + 3];
                j = j2 | (((long) b) << 56);
                break;
            case 5:
                j2 = ((((long) bArr[i2 + 1]) & 255) << 32) | ((((long) bArr[i2]) & 255) << 24) | ((((long) bArr[i2 + 2]) & 255) << 40) | ((255 & ((long) bArr[i2 + 3])) << 48);
                b = bArr[i2 + 4];
                j = j2 | (((long) b) << 56);
                break;
            case 6:
                j2 = ((((long) bArr[i2 + 2]) & 255) << 32) | ((long) ((bArr[i2] & 255) << 16)) | ((((long) bArr[i2 + 1]) & 255) << 24) | ((((long) bArr[i2 + 3]) & 255) << 40) | ((255 & ((long) bArr[i2 + 4])) << 48);
                b = bArr[i2 + 5];
                j = j2 | (((long) b) << 56);
                break;
            case 7:
                j2 = ((((long) bArr[i2 + 3]) & 255) << 32) | ((long) (((bArr[i2] & 255) << 8) | ((bArr[i2 + 1] & 255) << 16))) | ((((long) bArr[i2 + 2]) & 255) << 24) | ((((long) bArr[i2 + 4]) & 255) << 40) | ((255 & ((long) bArr[i2 + 5])) << 48);
                b = bArr[i2 + 6];
                j = j2 | (((long) b) << 56);
                break;
            case 8:
                j2 = ((((long) bArr[i2 + 4]) & 255) << 32) | ((long) ((bArr[i2] & 255) | ((bArr[i2 + 1] & 255) << 8) | ((bArr[i2 + 2] & 255) << 16))) | ((((long) bArr[i2 + 3]) & 255) << 24) | ((((long) bArr[i2 + 5]) & 255) << 40) | ((255 & ((long) bArr[i2 + 6])) << 48);
                b = bArr[i2 + 7];
                j = j2 | (((long) b) << 56);
                break;
            default:
                throw new C1404("Invalid size %d for sized, right extended long at offset 0x%x", Integer.valueOf(i), Integer.valueOf(this.f6807));
        }
        this.f6807 = (i2 + i) - this.f6806.f6805;
        return j;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public long m6985(int i) {
        long j;
        byte b;
        byte b2;
        int i2;
        long j2;
        long j3;
        long j4;
        long j5;
        int i3 = this.f6806.f6805 + this.f6807;
        byte[] bArr = this.f6806.f6804;
        switch (i) {
            case 1:
                b = bArr[i3];
                j = (long) b;
                break;
            case 2:
                b2 = bArr[i3] & 255;
                i2 = bArr[i3 + 1] << 8;
                b = i2 | b2;
                j = (long) b;
                break;
            case 3:
                b2 = (bArr[i3] & 255) | ((bArr[i3 + 1] & 255) << 8);
                i2 = bArr[i3 + 2] << 16;
                b = i2 | b2;
                j = (long) b;
                break;
            case 4:
                j3 = (long) ((bArr[i3] & 255) | ((bArr[i3 + 1] & 255) << 8) | ((bArr[i3 + 2] & 255) << 16));
                j2 = ((long) bArr[i3 + 3]) << 24;
                j = j3 | j2;
                break;
            case 5:
                j3 = ((long) ((bArr[i3] & 255) | ((bArr[i3 + 1] & 255) << 8) | ((bArr[i3 + 2] & 255) << 16))) | ((255 & ((long) bArr[i3 + 3])) << 24);
                j2 = ((long) bArr[i3 + 4]) << 32;
                j = j3 | j2;
                break;
            case 6:
                j4 = ((255 & ((long) bArr[i3 + 4])) << 32) | ((long) ((bArr[i3] & 255) | ((bArr[i3 + 1] & 255) << 8) | ((bArr[i3 + 2] & 255) << 16))) | ((((long) bArr[i3 + 3]) & 255) << 24);
                j5 = ((long) bArr[i3 + 5]) << 40;
                j = j5 | j4;
                break;
            case 7:
                j4 = ((((long) bArr[i3 + 4]) & 255) << 32) | ((long) ((bArr[i3] & 255) | ((bArr[i3 + 1] & 255) << 8) | ((bArr[i3 + 2] & 255) << 16))) | ((((long) bArr[i3 + 3]) & 255) << 24) | ((255 & ((long) bArr[i3 + 5])) << 40);
                j5 = ((long) bArr[i3 + 6]) << 48;
                j = j5 | j4;
                break;
            case 8:
                j3 = ((255 & ((long) bArr[i3 + 6])) << 48) | ((((long) bArr[i3 + 4]) & 255) << 32) | ((long) ((bArr[i3] & 255) | ((bArr[i3 + 1] & 255) << 8) | ((bArr[i3 + 2] & 255) << 16))) | ((((long) bArr[i3 + 3]) & 255) << 24) | ((((long) bArr[i3 + 5]) & 255) << 40);
                j2 = ((long) bArr[i3 + 7]) << 56;
                j = j3 | j2;
                break;
            default:
                throw new C1404("Invalid size %d for sized long at offset 0x%x", Integer.valueOf(i), Integer.valueOf(this.f6807));
        }
        this.f6807 = (i3 + i) - this.f6806.f6805;
        return j;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public String m6987(int i) {
        int[] iArr = new int[1];
        String r5 = C1409.m7810(this.f6806.f6804, this.f6806.f6805 + this.f6807, i, iArr);
        this.f6807 += iArr[0];
        return r5;
    }
}
