package org.ʻ.ʻ.ʽ.ʾ;

import org.ʻ.ʻ.ʽ.C1163;
import org.ʻ.ʻ.ʽ.C1184;
import org.ʻ.ʻ.ʽ.ʿ.C1168;
import org.ʻ.ʻ.ʾ.ʾ.C1273;

/* renamed from: org.ʻ.ʻ.ʽ.ʾ.ʽ  reason: contains not printable characters */
/* compiled from: EncodedArrayItemIterator */
public abstract class C1153 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final C1153 f6671 = new C1153() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public C1273 m6808() {
            return null;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m6809() {
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public int m6810() {
            return 0;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public int m6811() {
            return 0;
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract C1273 m6804();

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract void m6805();

    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract int m6806();

    /* renamed from: ʾ  reason: contains not printable characters */
    public abstract int m6807();

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1153 m6803(C1163 r1, int i) {
        if (i == 0) {
            return f6671;
        }
        return new C1154(r1, i);
    }

    /* renamed from: org.ʻ.ʻ.ʽ.ʾ.ʽ$ʻ  reason: contains not printable characters */
    /* compiled from: EncodedArrayItemIterator */
    private static class C1154 extends C1153 {

        /* renamed from: ʼ  reason: contains not printable characters */
        private final C1184 f6672;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final C1163 f6673;

        /* renamed from: ʾ  reason: contains not printable characters */
        private final int f6674;

        /* renamed from: ʿ  reason: contains not printable characters */
        private int f6675 = 0;

        public C1154(C1163 r2, int i) {
            this.f6673 = r2;
            this.f6672 = r2.m6855().m6970(i);
            this.f6674 = this.f6672.m6976();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C1273 m6812() {
            int i = this.f6675;
            if (i >= this.f6674) {
                return null;
            }
            this.f6675 = i + 1;
            return C1168.m6903(this.f6673, this.f6672);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m6813() {
            int i = this.f6675;
            if (i < this.f6674) {
                this.f6675 = i + 1;
                C1168.m6904(this.f6672);
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public int m6814() {
            return this.f6672.m6972();
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public int m6815() {
            return this.f6674;
        }
    }
}
