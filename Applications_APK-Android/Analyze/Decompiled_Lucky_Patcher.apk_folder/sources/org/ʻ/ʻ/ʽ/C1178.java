package org.ʻ.ʻ.ʽ;

import java.util.Set;
import org.ʻ.ʻ.ʻ.ʻ.C1004;
import org.ʻ.ʻ.ʽ.ʾ.C1146;
import org.ʻ.ʻ.ʽ.ʾ.C1153;
import org.ʻ.ʻ.ʾ.C1257;
import org.ʻ.ʻ.ʾ.C1287;
import org.ʻ.ʻ.ʾ.ʾ.C1273;

/* renamed from: org.ʻ.ʻ.ʽ.ˈ  reason: contains not printable characters */
/* compiled from: DexBackedField */
public class C1178 extends C1004 implements C1287 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final C1163 f6759;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final C1257 f6760;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final int f6761;

    /* renamed from: ʾ  reason: contains not printable characters */
    public final C1273 f6762;

    /* renamed from: ʿ  reason: contains not printable characters */
    public final int f6763;

    /* renamed from: ˆ  reason: contains not printable characters */
    public final int f6764;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final int f6765;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final int f6766;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f6767;

    public C1178(C1163 r1, C1184 r2, C1145 r3, int i, C1153 r5, C1146.C1147 r6) {
        this.f6759 = r1;
        this.f6760 = r3;
        this.f6765 = r2.m6972();
        this.f6764 = r2.m6978() + i;
        this.f6761 = r2.m6976();
        this.f6763 = r6.m6776(this.f6764);
        this.f6766 = r5.m6806();
        this.f6762 = r5.m6804();
    }

    public C1178(C1163 r1, C1184 r2, C1145 r3, int i, C1146.C1147 r5) {
        this.f6759 = r1;
        this.f6760 = r3;
        this.f6765 = r2.m6972();
        this.f6764 = r2.m6978() + i;
        this.f6761 = r2.m6976();
        this.f6763 = r5.m6776(this.f6764);
        this.f6766 = 0;
        this.f6762 = null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m6915() {
        return (String) this.f6759.m6859().get(this.f6759.m6854().m6962(m6914() + 4));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m6916() {
        return (String) this.f6759.m6860().get(this.f6759.m6854().m6964(m6914() + 2));
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m6917() {
        return this.f6760.m7049();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m6918() {
        return this.f6761;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public C1273 m6919() {
        return this.f6762;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public Set<? extends C1093> m6920() {
        return C1146.m6762(this.f6759, this.f6763);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m6913(C1184 r1, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            r1.m6983();
            r1.m6983();
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private int m6914() {
        if (this.f6767 == 0) {
            this.f6767 = this.f6759.m6861().m6891(this.f6764);
        }
        return this.f6767;
    }
}
