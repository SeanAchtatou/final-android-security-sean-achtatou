package org.ʻ.ʻ.ʽ.ʾ;

import java.util.Iterator;
import java.util.NoSuchElementException;
import org.ʻ.ʻ.ʽ.C1183;
import org.ʻ.ʻ.ʽ.C1184;

/* renamed from: org.ʻ.ʻ.ʽ.ʾ.ˈ  reason: contains not printable characters */
/* compiled from: VariableSizeIterator */
public abstract class C1158<T> implements Iterator<T> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C1184 f6685;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected final int f6686;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f6687;

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract T m6822(C1184 r1, int i);

    protected C1158(C1183 r1, int i, int i2) {
        this.f6685 = r1.m6970(i);
        this.f6686 = i2;
    }

    protected C1158(C1184 r1, int i) {
        this.f6685 = r1;
        this.f6686 = i;
    }

    public boolean hasNext() {
        return this.f6687 < this.f6686;
    }

    public T next() {
        int i = this.f6687;
        if (i < this.f6686) {
            C1184 r1 = this.f6685;
            this.f6687 = i + 1;
            return m6822(r1, i);
        }
        throw new NoSuchElementException();
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
