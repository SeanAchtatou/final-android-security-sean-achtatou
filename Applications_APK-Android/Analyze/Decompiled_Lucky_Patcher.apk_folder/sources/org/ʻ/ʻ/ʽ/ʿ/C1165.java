package org.ʻ.ʻ.ʽ.ʿ;

import java.util.Set;
import org.ʻ.ʻ.ʻ.ʼ.C1012;
import org.ʻ.ʻ.ʽ.C1133;
import org.ʻ.ʻ.ʽ.C1163;
import org.ʻ.ʻ.ʽ.C1184;
import org.ʻ.ʻ.ʽ.ʾ.C1162;
import org.ʻ.ʻ.ʾ.ʾ.C1267;

/* renamed from: org.ʻ.ʻ.ʽ.ʿ.ʻ  reason: contains not printable characters */
/* compiled from: DexBackedAnnotationEncodedValue */
public class C1165 extends C1012 implements C1267 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final C1163 f6736;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String f6737;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f6738;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final int f6739;

    public C1165(C1163 r2, C1184 r3) {
        this.f6736 = r2;
        this.f6737 = (String) r2.m6860().get(r3.m6976());
        this.f6738 = r3.m6976();
        this.f6739 = r3.m6972();
        m6893(r3, this.f6738);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m6892(C1184 r1) {
        r1.m6983();
        m6893(r1, r1.m6976());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m6893(C1184 r1, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            r1.m6983();
            C1168.m6904(r1);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m6894() {
        return this.f6737;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Set<? extends C1133> m6895() {
        return new C1162<C1133>(this.f6736.m6855(), this.f6739, this.f6738) {
            /* access modifiers changed from: protected */
            /* renamed from: ʻ  reason: contains not printable characters */
            public C1133 m6897(C1184 r2, int i) {
                return new C1133(C1165.this.f6736, r2);
            }
        };
    }
}
