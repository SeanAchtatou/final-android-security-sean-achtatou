package org.ʻ.ʻ.ʽ.ʾ;

import java.util.AbstractList;

/* renamed from: org.ʻ.ʻ.ʽ.ʾ.ʾ  reason: contains not printable characters */
/* compiled from: FixedSizeList */
public abstract class C1155<T> extends AbstractList<T> {
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract T m6816(int i);

    public T get(int i) {
        if (i >= 0 && i < size()) {
            return m6816(i);
        }
        throw new IndexOutOfBoundsException();
    }
}
