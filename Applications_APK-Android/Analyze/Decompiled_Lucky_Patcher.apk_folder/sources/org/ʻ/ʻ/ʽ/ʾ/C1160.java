package org.ʻ.ʻ.ʽ.ʾ;

import java.util.ListIterator;
import java.util.NoSuchElementException;
import org.ʻ.ʻ.ʽ.C1183;
import org.ʻ.ʻ.ʽ.C1184;

/* renamed from: org.ʻ.ʻ.ʽ.ʾ.ˊ  reason: contains not printable characters */
/* compiled from: VariableSizeListIterator */
public abstract class C1160<T> implements ListIterator<T> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private C1184 f6692;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected final int f6693;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f6694;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f6695;

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract T m6827(C1184 r1, int i);

    protected C1160(C1183 r1, int i, int i2) {
        this.f6692 = r1.m6970(i);
        this.f6694 = i;
        this.f6693 = i2;
    }

    public boolean hasNext() {
        return this.f6695 < this.f6693;
    }

    public T next() {
        int i = this.f6695;
        if (i < this.f6693) {
            C1184 r1 = this.f6692;
            this.f6695 = i + 1;
            return m6827(r1, i);
        }
        throw new NoSuchElementException();
    }

    public boolean hasPrevious() {
        return this.f6695 > 0;
    }

    public T previous() {
        int i = this.f6695 - 1;
        this.f6692.m6973(this.f6694);
        this.f6695 = 0;
        while (true) {
            int i2 = this.f6695;
            if (i2 < i) {
                C1184 r2 = this.f6692;
                this.f6695 = i2 + 1;
                m6827(r2, i2);
            } else {
                C1184 r0 = this.f6692;
                this.f6695 = i2 + 1;
                return m6827(r0, i2);
            }
        }
    }

    public int nextIndex() {
        return this.f6695;
    }

    public int previousIndex() {
        return this.f6695 - 1;
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

    public void set(T t) {
        throw new UnsupportedOperationException();
    }

    public void add(T t) {
        throw new UnsupportedOperationException();
    }
}
