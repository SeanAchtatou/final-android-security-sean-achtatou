package org.ʻ.ʻ.ʽ.ʾ;

import com.google.ʻ.ʼ.C0904;
import java.util.Arrays;
import java.util.Iterator;
import org.ʻ.ʻ.C1001;
import org.ʻ.ʻ.ʽ.C1163;
import org.ʻ.ʻ.ʽ.C1179;
import org.ʻ.ʻ.ʽ.C1180;
import org.ʻ.ʻ.ʽ.C1184;
import org.ʻ.ʻ.ʾ.ʻ.C1188;
import org.ʻ.ʻ.ʾ.ʻ.C1192;

/* renamed from: org.ʻ.ʻ.ʽ.ʾ.ʼ  reason: contains not printable characters */
/* compiled from: DebugInfo */
public abstract class C1150 implements Iterable<C1188> {
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract Iterator<String> m6787(C1184 r1);

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1150 m6786(C1163 r1, int i, C1180 r3) {
        if (i == 0) {
            return C1152.f6670;
        }
        return new C1151(r1, i, r3);
    }

    /* renamed from: org.ʻ.ʻ.ʽ.ʾ.ʼ$ʼ  reason: contains not printable characters */
    /* compiled from: DebugInfo */
    private static class C1152 extends C1150 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public static final C1152 f6670 = new C1152();

        private C1152() {
        }

        public Iterator<C1188> iterator() {
            return C0904.m5655().iterator();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Iterator<String> m6802(C1184 r1) {
            return C0904.m5655().iterator();
        }
    }

    /* renamed from: org.ʻ.ʻ.ʽ.ʾ.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: DebugInfo */
    private static class C1151 extends C1150 {
        /* access modifiers changed from: private */

        /* renamed from: ʾ  reason: contains not printable characters */
        public static final C1192 f6659 = new C1192() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public String m6792() {
                return null;
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public String m6793() {
                return null;
            }

            /* renamed from: ˆ  reason: contains not printable characters */
            public String m6794() {
                return null;
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        public final C1163 f6660;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final int f6661;
        /* access modifiers changed from: private */

        /* renamed from: ʽ  reason: contains not printable characters */
        public final C1180 f6662;

        public C1151(C1163 r1, int i, C1180 r3) {
            this.f6660 = r1;
            this.f6661 = i;
            this.f6662 = r3;
        }

        public Iterator<C1188> iterator() {
            C1192 r3;
            String r4;
            C1184 r0 = this.f6660.m6855().m6970(this.f6661);
            final int r5 = r0.m6980();
            int r1 = this.f6662.m6939();
            final C1192[] r6 = new C1192[r1];
            Arrays.fill(r6, f6659);
            C1179 r2 = this.f6662.f6785;
            C1157 r32 = new C1157(r2.m6932(), r2.m6930(), m6790(r0));
            int i = 0;
            if (!C1001.STATIC.m6291(this.f6662.f6785.m6926())) {
                r6[0] = new C1192() {
                    /* renamed from: ʼ  reason: contains not printable characters */
                    public String m6796() {
                        return null;
                    }

                    /* renamed from: ˆ  reason: contains not printable characters */
                    public String m6797() {
                        return "this";
                    }

                    /* renamed from: ʻ  reason: contains not printable characters */
                    public String m6795() {
                        return C1151.this.f6662.f6785.m6925();
                    }
                };
                i = 1;
            }
            while (r32.hasNext()) {
                r6[i] = (C1192) r32.next();
                i++;
            }
            if (i < r1) {
                int i2 = r1 - 1;
                while (true) {
                    i--;
                    if (i <= -1 || ((r4 = (r3 = r6[i]).m7008()) != null && ((r4.equals("J") || r4.equals("D")) && i2 - 1 == i))) {
                        break;
                    }
                    r6[i2] = r3;
                    r6[i] = f6659;
                    i2--;
                }
            }
            return new C1161<C1188>(this.f6660.m6855(), r0.m6972()) {

                /* renamed from: ʾ  reason: contains not printable characters */
                private int f6667 = 0;

                /* renamed from: ʿ  reason: contains not printable characters */
                private int f6668 = r5;

                /* access modifiers changed from: protected */
                /* JADX WARNING: Removed duplicated region for block: B:18:0x0078  */
                /* JADX WARNING: Removed duplicated region for block: B:30:0x0098  */
                /* JADX WARNING: Removed duplicated region for block: B:31:0x009d  */
                /* JADX WARNING: Removed duplicated region for block: B:34:0x00b5  */
                /* renamed from: ʻ  reason: contains not printable characters */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public org.ʻ.ʻ.ʾ.ʻ.C1188 m6799(org.ʻ.ʻ.ʽ.C1184 r15) {
                    /*
                        r14 = this;
                    L_0x0000:
                        int r0 = r15.m6988()
                        r1 = 1
                        switch(r0) {
                            case 0: goto L_0x0163;
                            case 1: goto L_0x0158;
                            case 2: goto L_0x014d;
                            case 3: goto L_0x010d;
                            case 4: goto L_0x00ba;
                            case 5: goto L_0x0080;
                            case 6: goto L_0x004f;
                            case 7: goto L_0x0047;
                            case 8: goto L_0x003f;
                            case 9: goto L_0x0024;
                            default: goto L_0x0008;
                        }
                    L_0x0008:
                        int r0 = r0 + -10
                        int r15 = r14.f6667
                        int r1 = r0 / 15
                        int r15 = r15 + r1
                        r14.f6667 = r15
                        int r15 = r14.f6668
                        int r0 = r0 % 15
                        int r0 = r0 + -4
                        int r15 = r15 + r0
                        r14.f6668 = r15
                        org.ʻ.ʻ.ʿ.ʻ.ʾ r15 = new org.ʻ.ʻ.ʿ.ʻ.ʾ
                        int r0 = r14.f6667
                        int r1 = r14.f6668
                        r15.<init>(r0, r1)
                        return r15
                    L_0x0024:
                        org.ʻ.ʻ.ʽ.ʾ.ʼ$ʻ r0 = org.ʻ.ʻ.ʽ.ʾ.C1150.C1151.this
                        org.ʻ.ʻ.ʽ.ʿ r0 = r0.f6660
                        org.ʻ.ʻ.ʽ.ʿ$ʽ r0 = r0.m6859()
                        int r15 = r15.m6976()
                        int r15 = r15 - r1
                        java.lang.Object r15 = r0.m6905(r15)
                        java.lang.String r15 = (java.lang.String) r15
                        org.ʻ.ʻ.ʿ.ʻ.ˈ r0 = new org.ʻ.ʻ.ʿ.ʻ.ˈ
                        int r1 = r14.f6667
                        r0.<init>(r1, r15)
                        return r0
                    L_0x003f:
                        org.ʻ.ʻ.ʿ.ʻ.ʽ r15 = new org.ʻ.ʻ.ʿ.ʻ.ʽ
                        int r0 = r14.f6667
                        r15.<init>(r0)
                        return r15
                    L_0x0047:
                        org.ʻ.ʻ.ʿ.ʻ.ʿ r15 = new org.ʻ.ʻ.ʿ.ʻ.ʿ
                        int r0 = r14.f6667
                        r15.<init>(r0)
                        return r15
                    L_0x004f:
                        int r15 = r15.m6976()
                        if (r15 < 0) goto L_0x005d
                        org.ʻ.ʻ.ʾ.ʻ.ʿ[] r0 = r6
                        int r1 = r0.length
                        if (r15 >= r1) goto L_0x005d
                        r0 = r0[r15]
                        goto L_0x0061
                    L_0x005d:
                        org.ʻ.ʻ.ʾ.ʻ.ʿ r0 = org.ʻ.ʻ.ʽ.ʾ.C1150.C1151.f6659
                    L_0x0061:
                        org.ʻ.ʻ.ʿ.ʻ.ˆ r7 = new org.ʻ.ʻ.ʿ.ʻ.ˆ
                        int r2 = r14.f6667
                        java.lang.String r4 = r0.m7010()
                        java.lang.String r5 = r0.m7008()
                        java.lang.String r6 = r0.m7009()
                        r1 = r7
                        r3 = r15
                        r1.<init>(r2, r3, r4, r5, r6)
                        if (r15 < 0) goto L_0x007f
                        org.ʻ.ʻ.ʾ.ʻ.ʿ[] r0 = r6
                        int r1 = r0.length
                        if (r15 >= r1) goto L_0x007f
                        r0[r15] = r7
                    L_0x007f:
                        return r7
                    L_0x0080:
                        int r15 = r15.m6976()
                        r0 = 0
                        if (r15 < 0) goto L_0x008f
                        org.ʻ.ʻ.ʾ.ʻ.ʿ[] r2 = r6
                        int r3 = r2.length
                        if (r15 >= r3) goto L_0x008f
                        r2 = r2[r15]
                        goto L_0x0094
                    L_0x008f:
                        org.ʻ.ʻ.ʾ.ʻ.ʿ r2 = org.ʻ.ʻ.ʽ.ʾ.C1150.C1151.f6659
                        r1 = 0
                    L_0x0094:
                        boolean r3 = r2 instanceof org.ʻ.ʻ.ʾ.ʻ.C1189
                        if (r3 == 0) goto L_0x009d
                        org.ʻ.ʻ.ʾ.ʻ.ʿ r2 = org.ʻ.ʻ.ʽ.ʾ.C1150.C1151.f6659
                        goto L_0x009e
                    L_0x009d:
                        r0 = r1
                    L_0x009e:
                        org.ʻ.ʻ.ʿ.ʻ.ʼ r1 = new org.ʻ.ʻ.ʿ.ʻ.ʼ
                        int r9 = r14.f6667
                        java.lang.String r11 = r2.m7010()
                        java.lang.String r12 = r2.m7008()
                        java.lang.String r13 = r2.m7009()
                        r8 = r1
                        r10 = r15
                        r8.<init>(r9, r10, r11, r12, r13)
                        if (r0 == 0) goto L_0x00b9
                        org.ʻ.ʻ.ʾ.ʻ.ʿ[] r0 = r6
                        r0[r15] = r1
                    L_0x00b9:
                        return r1
                    L_0x00ba:
                        int r0 = r15.m6976()
                        org.ʻ.ʻ.ʽ.ʾ.ʼ$ʻ r2 = org.ʻ.ʻ.ʽ.ʾ.C1150.C1151.this
                        org.ʻ.ʻ.ʽ.ʿ r2 = r2.f6660
                        org.ʻ.ʻ.ʽ.ʿ$ʽ r2 = r2.m6859()
                        int r3 = r15.m6976()
                        int r3 = r3 - r1
                        java.lang.Object r2 = r2.m6905(r3)
                        r5 = r2
                        java.lang.String r5 = (java.lang.String) r5
                        org.ʻ.ʻ.ʽ.ʾ.ʼ$ʻ r2 = org.ʻ.ʻ.ʽ.ʾ.C1150.C1151.this
                        org.ʻ.ʻ.ʽ.ʿ r2 = r2.f6660
                        org.ʻ.ʻ.ʽ.ʿ$ʽ r2 = r2.m6860()
                        int r3 = r15.m6976()
                        int r3 = r3 - r1
                        java.lang.Object r2 = r2.m6905(r3)
                        r6 = r2
                        java.lang.String r6 = (java.lang.String) r6
                        org.ʻ.ʻ.ʽ.ʾ.ʼ$ʻ r2 = org.ʻ.ʻ.ʽ.ʾ.C1150.C1151.this
                        org.ʻ.ʻ.ʽ.ʿ r2 = r2.f6660
                        org.ʻ.ʻ.ʽ.ʿ$ʽ r2 = r2.m6859()
                        int r15 = r15.m6976()
                        int r15 = r15 - r1
                        java.lang.Object r15 = r2.m6905(r15)
                        r7 = r15
                        java.lang.String r7 = (java.lang.String) r7
                        org.ʻ.ʻ.ʿ.ʻ.ˉ r15 = new org.ʻ.ʻ.ʿ.ʻ.ˉ
                        int r3 = r14.f6667
                        r2 = r15
                        r4 = r0
                        r2.<init>(r3, r4, r5, r6, r7)
                        if (r0 < 0) goto L_0x010c
                        org.ʻ.ʻ.ʾ.ʻ.ʿ[] r1 = r6
                        int r2 = r1.length
                        if (r0 >= r2) goto L_0x010c
                        r1[r0] = r15
                    L_0x010c:
                        return r15
                    L_0x010d:
                        int r0 = r15.m6976()
                        org.ʻ.ʻ.ʽ.ʾ.ʼ$ʻ r2 = org.ʻ.ʻ.ʽ.ʾ.C1150.C1151.this
                        org.ʻ.ʻ.ʽ.ʿ r2 = r2.f6660
                        org.ʻ.ʻ.ʽ.ʿ$ʽ r2 = r2.m6859()
                        int r3 = r15.m6976()
                        int r3 = r3 - r1
                        java.lang.Object r2 = r2.m6905(r3)
                        r6 = r2
                        java.lang.String r6 = (java.lang.String) r6
                        org.ʻ.ʻ.ʽ.ʾ.ʼ$ʻ r2 = org.ʻ.ʻ.ʽ.ʾ.C1150.C1151.this
                        org.ʻ.ʻ.ʽ.ʿ r2 = r2.f6660
                        org.ʻ.ʻ.ʽ.ʿ$ʽ r2 = r2.m6860()
                        int r15 = r15.m6976()
                        int r15 = r15 - r1
                        java.lang.Object r15 = r2.m6905(r15)
                        r7 = r15
                        java.lang.String r7 = (java.lang.String) r7
                        org.ʻ.ʻ.ʿ.ʻ.ˉ r15 = new org.ʻ.ʻ.ʿ.ʻ.ˉ
                        int r4 = r14.f6667
                        r8 = 0
                        r3 = r15
                        r5 = r0
                        r3.<init>(r4, r5, r6, r7, r8)
                        if (r0 < 0) goto L_0x014c
                        org.ʻ.ʻ.ʾ.ʻ.ʿ[] r1 = r6
                        int r2 = r1.length
                        if (r0 >= r2) goto L_0x014c
                        r1[r0] = r15
                    L_0x014c:
                        return r15
                    L_0x014d:
                        int r0 = r15.m6974()
                        int r1 = r14.f6668
                        int r1 = r1 + r0
                        r14.f6668 = r1
                        goto L_0x0000
                    L_0x0158:
                        int r0 = r15.m6976()
                        int r1 = r14.f6667
                        int r1 = r1 + r0
                        r14.f6667 = r1
                        goto L_0x0000
                    L_0x0163:
                        java.lang.Object r15 = r14.m5444()
                        org.ʻ.ʻ.ʾ.ʻ.ʻ r15 = (org.ʻ.ʻ.ʾ.ʻ.C1188) r15
                        return r15
                    */
                    throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ʽ.ʾ.C1150.C1151.AnonymousClass3.m6799(org.ʻ.ʻ.ʽ.ˑ):org.ʻ.ʻ.ʾ.ʻ.ʻ");
                }
            };
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public C1158<String> m6790(C1184 r3) {
            if (r3 == null) {
                r3 = this.f6660.m6855().m6970(this.f6661);
                r3.m6983();
            }
            return new C1158<String>(r3, r3.m6976()) {
                /* access modifiers changed from: protected */
                /* renamed from: ʻ  reason: contains not printable characters */
                public String m6801(C1184 r1, int i) {
                    return C1151.this.f6660.m6859().m6905(r1.m6976() - 1);
                }
            };
        }
    }
}
