package org.ʻ.ʻ.ʽ.ʾ;

import com.google.ʻ.ʼ.C0904;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.ʻ.ʻ.ʻ.C1031;
import org.ʻ.ʻ.ʾ.C1187;
import org.ʻ.ʻ.ʾ.C1290;

/* renamed from: org.ʻ.ʻ.ʽ.ʾ.ˆ  reason: contains not printable characters */
/* compiled from: ParameterIterator */
public class C1157 implements Iterator<C1290> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Iterator<? extends CharSequence> f6678;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Iterator<? extends Set<? extends C1187>> f6679;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Iterator<String> f6680;

    public C1157(List<? extends CharSequence> list, List<? extends Set<? extends C1187>> list2, Iterator<String> it) {
        this.f6678 = list.iterator();
        this.f6679 = list2.iterator();
        this.f6680 = it;
    }

    public boolean hasNext() {
        return this.f6678.hasNext();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C1290 next() {
        final Set set;
        final String charSequence = ((CharSequence) this.f6678.next()).toString();
        if (this.f6679.hasNext()) {
            set = (Set) this.f6679.next();
        } else {
            set = C0904.m5655();
        }
        final String next = this.f6680.hasNext() ? this.f6680.next() : null;
        return new C1031() {
            /* renamed from: ʽ  reason: contains not printable characters */
            public Set<? extends C1187> m6820() {
                return set;
            }

            /* renamed from: ˆ  reason: contains not printable characters */
            public String m6821() {
                return next;
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public String m6819() {
                return charSequence;
            }
        };
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
