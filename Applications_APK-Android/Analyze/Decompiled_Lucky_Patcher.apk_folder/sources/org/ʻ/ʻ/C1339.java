package org.ʻ.ʻ;

import com.google.ʻ.ʼ.C0929;
import java.util.HashMap;

/* renamed from: org.ʻ.ʻ.ˈ  reason: contains not printable characters */
/* compiled from: VerificationError */
public class C1339 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final HashMap<String, Integer> f7150 = C0929.m5793();

    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m7308(int i) {
        return i > 0 && i < 10;
    }

    static {
        f7150.put("generic-error", 1);
        f7150.put("no-such-class", 2);
        f7150.put("no-such-field", 3);
        f7150.put("no-such-method", 4);
        f7150.put("illegal-class-access", 5);
        f7150.put("illegal-field-access", 6);
        f7150.put("illegal-method-access", 7);
        f7150.put("class-change-error", 8);
        f7150.put("instantiation-error", 9);
    }
}
