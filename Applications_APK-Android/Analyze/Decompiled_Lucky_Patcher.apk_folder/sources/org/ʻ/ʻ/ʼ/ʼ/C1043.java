package org.ʻ.ʻ.ʼ.ʼ;

import com.google.ʻ.ʼ.C0891;
import java.util.List;
import org.ʻ.ʻ.C1092;
import org.ʻ.ʻ.C1185;
import org.ʻ.ʻ.ʼ.C1078;
import org.ʻ.ʻ.ʾ.ʼ.ʻ.C1199;

/* renamed from: org.ʻ.ʻ.ʼ.ʼ.ʻ  reason: contains not printable characters */
/* compiled from: BuilderArrayPayload */
public class C1043 extends C1078 implements C1199 {

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final C1185 f6332 = C1185.ARRAY_PAYLOAD;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected final int f6333;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected final List<Number> f6334;

    public C1043(int i, List<Number> list) {
        super(f6332);
        this.f6333 = i;
        this.f6334 = list == null ? C0891.m5603() : list;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m6379() {
        return this.f6333;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public List<Number> m6380() {
        return this.f6334;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6378() {
        return (((this.f6333 * this.f6334.size()) + 1) / 2) + 4;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C1092 m6377() {
        return f6332.f7077;
    }
}
