package org.ʻ.ʻ.ʼ.ʼ;

import org.ʻ.ʻ.C1092;
import org.ʻ.ʻ.C1185;
import org.ʻ.ʻ.ʼ.C1078;
import org.ʻ.ʻ.ʾ.ʼ.ʻ.C1227;
import org.ʻ.ʻ.ˆ.C1337;

/* renamed from: org.ʻ.ʻ.ʼ.ʼ.ᴵᴵ  reason: contains not printable characters */
/* compiled from: BuilderInstruction35ms */
public class C1069 extends C1078 implements C1227 {

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final C1092 f6414 = C1092.Format35ms;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected final int f6415;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected final int f6416;

    /* renamed from: ˆ  reason: contains not printable characters */
    protected final int f6417;

    /* renamed from: ˈ  reason: contains not printable characters */
    protected final int f6418;

    /* renamed from: ˉ  reason: contains not printable characters */
    protected final int f6419;

    /* renamed from: ˊ  reason: contains not printable characters */
    protected final int f6420;

    /* renamed from: ˋ  reason: contains not printable characters */
    protected final int f6421;

    public C1069(C1185 r1, int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        super(r1);
        this.f6415 = C1337.m7298(i);
        int i8 = 0;
        this.f6416 = i > 0 ? C1337.m7288(i2) : 0;
        this.f6417 = i > 1 ? C1337.m7288(i3) : 0;
        this.f6418 = i > 2 ? C1337.m7288(i4) : 0;
        this.f6419 = i > 3 ? C1337.m7288(i5) : 0;
        this.f6420 = i > 4 ? C1337.m7288(i6) : i8;
        this.f6421 = i7;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m6453() {
        return this.f6415;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public int m6455() {
        return this.f6416;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public int m6456() {
        return this.f6417;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public int m6457() {
        return this.f6418;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public int m6458() {
        return this.f6419;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public int m6459() {
        return this.f6420;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public int m6454() {
        return this.f6421;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C1092 m6452() {
        return f6414;
    }
}
