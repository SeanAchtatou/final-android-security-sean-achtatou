package org.ʻ.ʻ.ʼ;

import com.google.ʻ.ʼ.C0891;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.ʻ.ʻ.ʼ.C1082;

/* renamed from: org.ʻ.ʻ.ʼ.ˋ  reason: contains not printable characters */
/* compiled from: LocatedItems */
public abstract class C1085<T extends C1082> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private List<T> f6457 = null;

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract String m6501();

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public List<T> m6500() {
        List<T> list = this.f6457;
        return list == null ? C0891.m5603() : list;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Set<T> m6502(final C1087 r2) {
        return new AbstractSet<T>() {
            public Iterator<T> iterator() {
                final Iterator it = C1085.this.m6500().iterator();
                return new Iterator<T>() {

                    /* renamed from: ʽ  reason: contains not printable characters */
                    private T f6462 = null;

                    public boolean hasNext() {
                        return it.hasNext();
                    }

                    /* renamed from: ʻ  reason: contains not printable characters */
                    public T next() {
                        this.f6462 = (C1082) it.next();
                        return this.f6462;
                    }

                    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
                        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                        */
                    public void remove() {
                        /*
                            r2 = this;
                            T r0 = r2.f6462
                            if (r0 == 0) goto L_0x0008
                            r1 = 0
                            r0.m6492(r1)
                        L_0x0008:
                            java.util.Iterator r0 = r0
                            r0.remove()
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ʼ.C1085.AnonymousClass1.AnonymousClass1.remove():void");
                    }
                };
            }

            public int size() {
                return C1085.this.m6500().size();
            }

            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            /* renamed from: ʻ  reason: contains not printable characters */
            public boolean add(T r2) {
                /*
                    r1 = this;
                    boolean r0 = r2.m6493()
                    if (r0 != 0) goto L_0x0012
                    org.ʻ.ʻ.ʼ.ˏ r0 = r2
                    r2.m6492(r0)
                    org.ʻ.ʻ.ʼ.ˋ r0 = org.ʻ.ʻ.ʼ.C1085.this
                    r0.m6498((org.ʻ.ʻ.ʼ.C1083) r2)
                    r2 = 1
                    return r2
                L_0x0012:
                    java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
                    org.ʻ.ʻ.ʼ.ˋ r0 = org.ʻ.ʻ.ʼ.C1085.this
                    java.lang.String r0 = r0.m6501()
                    r2.<init>(r0)
                    throw r2
                */
                throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ʼ.C1085.AnonymousClass1.add(org.ʻ.ʻ.ʼ.ˈ):boolean");
            }
        };
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m6498(C1083 r3) {
        if (this.f6457 == null) {
            this.f6457 = new ArrayList(1);
        }
        this.f6457.add(r3);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m6503(C1087 r3, C1085<C1083> r4) {
        List<T> list;
        if (r4 != this && (list = this.f6457) != null) {
            for (T r1 : list) {
                r1.m6492(r3);
            }
            List<T> list2 = this.f6457;
            list2.addAll(r4.m6500());
            r4.f6457 = list2;
            this.f6457 = null;
        }
    }
}
