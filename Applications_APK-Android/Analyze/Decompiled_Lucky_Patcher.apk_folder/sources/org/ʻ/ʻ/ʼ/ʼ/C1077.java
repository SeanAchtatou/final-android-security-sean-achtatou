package org.ʻ.ʻ.ʼ.ʼ;

import org.ʻ.ʻ.C1092;
import org.ʻ.ʻ.C1185;
import org.ʻ.ʻ.ʼ.C1078;
import org.ʻ.ʻ.ʾ.ʼ.ʻ.C1235;
import org.ʻ.ʻ.ʾ.ʽ.C1263;
import org.ʻ.ʻ.ˆ.C1337;

/* renamed from: org.ʻ.ʻ.ʼ.ʼ.ﾞﾞ  reason: contains not printable characters */
/* compiled from: BuilderInstruction35c */
public class C1077 extends C1078 implements C1235 {

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final C1092 f6441 = C1092.Format35c;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected final int f6442;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected final int f6443;

    /* renamed from: ˆ  reason: contains not printable characters */
    protected final int f6444;

    /* renamed from: ˈ  reason: contains not printable characters */
    protected final int f6445;

    /* renamed from: ˉ  reason: contains not printable characters */
    protected final int f6446;

    /* renamed from: ˊ  reason: contains not printable characters */
    protected final int f6447;

    /* renamed from: ˋ  reason: contains not printable characters */
    protected final C1263 f6448;

    public C1077(C1185 r1, int i, int i2, int i3, int i4, int i5, int i6, C1263 r8) {
        super(r1);
        this.f6442 = C1337.m7298(i);
        int i7 = 0;
        this.f6443 = i > 0 ? C1337.m7288(i2) : 0;
        this.f6444 = i > 1 ? C1337.m7288(i3) : 0;
        this.f6445 = i > 2 ? C1337.m7288(i4) : 0;
        this.f6446 = i > 3 ? C1337.m7288(i5) : 0;
        this.f6447 = i > 4 ? C1337.m7288(i6) : i7;
        this.f6448 = r8;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m6473() {
        return this.f6442;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public int m6476() {
        return this.f6443;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public int m6477() {
        return this.f6444;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public int m6478() {
        return this.f6445;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public int m6479() {
        return this.f6446;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public int m6480() {
        return this.f6447;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public C1263 m6474() {
        return this.f6448;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public int m6475() {
        return this.f6449.f7076;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C1092 m6472() {
        return f6441;
    }
}
