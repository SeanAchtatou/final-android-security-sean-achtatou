package org.ʻ.ʻ.ʼ;

import org.ʻ.ʻ.C1092;
import org.ʻ.ʻ.C1185;
import org.ʻ.ʻ.ʾ.ʼ.C1240;
import org.ʻ.ʻ.ˆ.C1337;

/* renamed from: org.ʻ.ʻ.ʼ.ʽ  reason: contains not printable characters */
/* compiled from: BuilderInstruction */
public abstract class C1078 implements C1240 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final C1185 f6449;

    /* renamed from: ʼ  reason: contains not printable characters */
    C1087 f6450;

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract C1092 m6482();

    protected C1078(C1185 r2) {
        C1337.m7291(r2, m6482());
        this.f6449 = r2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C1185 m6481() {
        return this.f6449;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6483() {
        return m6482().f6524 / 2;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public C1087 m6484() {
        C1087 r0 = this.f6450;
        if (r0 != null) {
            return r0;
        }
        throw new IllegalStateException("Cannot get the location of an instruction that hasn't been added to a method.");
    }
}
