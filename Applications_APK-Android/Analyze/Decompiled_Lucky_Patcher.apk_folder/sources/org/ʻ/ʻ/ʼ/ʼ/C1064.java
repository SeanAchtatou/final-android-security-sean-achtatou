package org.ʻ.ʻ.ʼ.ʼ;

import com.google.ʻ.ʻ.C0842;
import com.google.ʻ.ʼ.C0891;
import com.google.ʻ.ʼ.C0926;
import java.util.List;
import org.ʻ.ʻ.C1092;
import org.ʻ.ʻ.C1185;
import org.ʻ.ʻ.ʼ.C1080;
import org.ʻ.ʻ.ʼ.C1091;
import org.ʻ.ʻ.ʾ.ʼ.ʻ.C1214;

/* renamed from: org.ʻ.ʻ.ʼ.ʼ.ــ  reason: contains not printable characters */
/* compiled from: BuilderSparseSwitchPayload */
public class C1064 extends C1080 implements C1214 {

    /* renamed from: ʾ  reason: contains not printable characters */
    public static final C1185 f6391 = C1185.SPARSE_SWITCH_PAYLOAD;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected final List<C1054> f6392;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ʻ.ʼ.ﾞ.ʻ(java.util.List, com.google.ʻ.ʻ.ʽ):java.util.List<T>
     arg types: [java.util.List<? extends org.ʻ.ʻ.ʼ.י>, org.ʻ.ʻ.ʼ.ʼ.ــ$1]
     candidates:
      com.google.ʻ.ʼ.ﾞ.ʻ(java.util.List<?>, java.lang.Object):boolean
      com.google.ʻ.ʼ.ﾞ.ʻ(java.util.List, com.google.ʻ.ʻ.ʽ):java.util.List<T> */
    public C1064(List<? extends C1091> list) {
        super(f6391);
        if (list == null) {
            this.f6392 = C0891.m5603();
        } else {
            this.f6392 = C0926.m5780((List) list, (C0842) new C0842<C1091, C1054>() {

                /* renamed from: ʻ  reason: contains not printable characters */
                static final /* synthetic */ boolean f6393 = (!C1064.class.desiredAssertionStatus());

                /* renamed from: ʻ  reason: contains not printable characters */
                public C1054 m6436(C1091 r4) {
                    if (f6393 || r4 != null) {
                        return new C1054(C1064.this, r4.f6483, r4.f6484);
                    }
                    throw new AssertionError();
                }
            });
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public List<C1054> m6435() {
        return this.f6392;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6434() {
        return (this.f6392.size() * 4) + 2;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C1092 m6433() {
        return f6391.f7077;
    }
}
