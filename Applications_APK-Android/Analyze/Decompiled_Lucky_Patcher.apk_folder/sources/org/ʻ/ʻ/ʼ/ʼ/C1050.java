package org.ʻ.ʻ.ʼ.ʼ;

import com.google.ʻ.ʼ.C0891;
import com.google.ʻ.ʼ.C0926;
import java.util.List;
import org.ʻ.ʻ.C1092;
import org.ʻ.ʻ.C1185;
import org.ʻ.ʻ.ʼ.C1080;
import org.ʻ.ʻ.ʼ.C1083;
import org.ʻ.ʻ.ʾ.ʼ.ʻ.C1210;

/* renamed from: org.ʻ.ʻ.ʼ.ʼ.ʾʾ  reason: contains not printable characters */
/* compiled from: BuilderPackedSwitchPayload */
public class C1050 extends C1080 implements C1210 {

    /* renamed from: ʾ  reason: contains not printable characters */
    public static final C1185 f6352 = C1185.PACKED_SWITCH_PAYLOAD;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected final List<C1054> f6353;

    public C1050(int i, List<? extends C1083> list) {
        super(f6352);
        if (list == null) {
            this.f6353 = C0891.m5603();
            return;
        }
        this.f6353 = C0926.m5776();
        for (C1083 r0 : list) {
            this.f6353.add(new C1054(this, i, r0));
            i++;
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public List<C1054> m6401() {
        return this.f6353;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6400() {
        return (this.f6353.size() * 2) + 4;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C1092 m6399() {
        return f6352.f7077;
    }
}
