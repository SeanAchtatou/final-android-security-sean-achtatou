package org.ʻ.ʻ.ʼ;

import org.ʻ.ʻ.C1185;
import org.ʻ.ʻ.ʾ.ʼ.C1244;
import org.ʻ.ʼ.C1404;

/* renamed from: org.ʻ.ʻ.ʼ.ʾ  reason: contains not printable characters */
/* compiled from: BuilderOffsetInstruction */
public abstract class C1079 extends C1078 implements C1244 {

    /* renamed from: ʽ  reason: contains not printable characters */
    protected final C1083 f6451;

    public C1079(C1185 r1, C1083 r2) {
        super(r1);
        this.f6451 = r2;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m6485() {
        int r0 = m6486();
        if (m6483() == 1) {
            if (r0 < -128 || r0 > 127) {
                throw new C1404("Invalid instruction offset: %d. Offset must be in [-128, 127]", Integer.valueOf(r0));
            }
        } else if (m6483() == 2 && (r0 < -32768 || r0 > 32767)) {
            throw new C1404("Invalid instruction offset: %d. Offset must be in [-32768, 32767]", Integer.valueOf(r0));
        }
        return r0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public int m6486() {
        return this.f6451.m6494() - m6484().m6509();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public C1083 m6487() {
        return this.f6451;
    }
}
