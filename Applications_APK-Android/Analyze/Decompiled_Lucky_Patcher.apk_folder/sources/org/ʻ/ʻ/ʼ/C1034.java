package org.ʻ.ʻ.ʼ;

import org.ʻ.ʻ.ʾ.ʻ.C1188;

/* renamed from: org.ʻ.ʻ.ʼ.ʻ  reason: contains not printable characters */
/* compiled from: BuilderDebugItem */
public abstract class C1034 extends C1082 implements C1188 {
    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6344() {
        if (this.f6456 != null) {
            return this.f6456.m6509();
        }
        throw new IllegalStateException("Cannot get the address of a BuilderDebugItem that isn't associated with a method.");
    }
}
