package org.ʻ.ʻ.ʼ;

import org.ʻ.ʻ.ʻ.C1030;
import org.ʻ.ʻ.ʾ.ʽ.C1265;

/* renamed from: org.ʻ.ʻ.ʼ.ʼ  reason: contains not printable characters */
/* compiled from: BuilderExceptionHandler */
public abstract class C1042 extends C1030 {

    /* renamed from: ʼ  reason: contains not printable characters */
    protected final C1083 f6330;

    private C1042(C1083 r1) {
        this.f6330 = r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static C1042 m6371(final C1265 r1, C1083 r2) {
        if (r1 == null) {
            return m6370(r2);
        }
        return new C1042(r2) {
            /* renamed from: ʼ  reason: contains not printable characters */
            public String m6373() {
                return r1.m7082();
            }

            /* renamed from: ʽ  reason: contains not printable characters */
            public int m6374() {
                return this.f6330.m6494();
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C1265 m6372() {
                return r1;
            }
        };
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static C1042 m6370(C1083 r1) {
        return new C1042(r1) {
            /* renamed from: ʼ  reason: contains not printable characters */
            public String m6375() {
                return null;
            }

            /* renamed from: ʽ  reason: contains not printable characters */
            public int m6376() {
                return this.f6330.m6494();
            }
        };
    }
}
