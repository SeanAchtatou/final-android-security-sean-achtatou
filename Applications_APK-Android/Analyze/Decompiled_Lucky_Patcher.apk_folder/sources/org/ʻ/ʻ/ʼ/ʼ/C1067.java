package org.ʻ.ʻ.ʼ.ʼ;

import org.ʻ.ʻ.C1092;
import org.ʻ.ʻ.C1185;
import org.ʻ.ʻ.ʼ.C1078;
import org.ʻ.ʻ.ʾ.ʼ.ʻ.C1225;
import org.ʻ.ʻ.ˆ.C1337;

/* renamed from: org.ʻ.ʻ.ʼ.ʼ.ᐧᐧ  reason: contains not printable characters */
/* compiled from: BuilderInstruction35mi */
public class C1067 extends C1078 implements C1225 {

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final C1092 f6403 = C1092.Format35mi;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected final int f6404;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected final int f6405;

    /* renamed from: ˆ  reason: contains not printable characters */
    protected final int f6406;

    /* renamed from: ˈ  reason: contains not printable characters */
    protected final int f6407;

    /* renamed from: ˉ  reason: contains not printable characters */
    protected final int f6408;

    /* renamed from: ˊ  reason: contains not printable characters */
    protected final int f6409;

    /* renamed from: ˋ  reason: contains not printable characters */
    protected final int f6410;

    public C1067(C1185 r1, int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        super(r1);
        this.f6404 = C1337.m7298(i);
        int i8 = 0;
        this.f6405 = i > 0 ? C1337.m7288(i2) : 0;
        this.f6406 = i > 1 ? C1337.m7288(i3) : 0;
        this.f6407 = i > 2 ? C1337.m7288(i4) : 0;
        this.f6408 = i > 3 ? C1337.m7288(i5) : 0;
        this.f6409 = i > 4 ? C1337.m7288(i6) : i8;
        this.f6410 = i7;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m6444() {
        return this.f6404;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public int m6446() {
        return this.f6405;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public int m6447() {
        return this.f6406;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public int m6448() {
        return this.f6407;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public int m6449() {
        return this.f6408;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public int m6450() {
        return this.f6409;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public int m6445() {
        return this.f6410;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C1092 m6443() {
        return f6403;
    }
}
