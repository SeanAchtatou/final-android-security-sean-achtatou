package org.ʻ.ʻ.ˆ;

import org.ʻ.ʻ.C1092;
import org.ʻ.ʻ.C1185;
import org.ʻ.ʻ.C1339;

/* renamed from: org.ʻ.ʻ.ˆ.ˆ  reason: contains not printable characters */
/* compiled from: Preconditions */
public class C1337 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m7291(C1185 r3, C1092 r4) {
        if (r3.f7077 != r4) {
            throw new IllegalArgumentException(String.format("Invalid opcode %s for %s", r3.f7075, r4.name()));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m7288(int i) {
        if ((i & -16) == 0) {
            return i;
        }
        throw new IllegalArgumentException(String.format("Invalid register: v%d. Must be between v0 and v15, inclusive.", Integer.valueOf(i)));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int m7292(int i) {
        if ((i & -256) == 0) {
            return i;
        }
        throw new IllegalArgumentException(String.format("Invalid register: v%d. Must be between v0 and v255, inclusive.", Integer.valueOf(i)));
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static int m7293(int i) {
        if ((-65536 & i) == 0) {
            return i;
        }
        throw new IllegalArgumentException(String.format("Invalid register: v%d. Must be between v0 and v65535, inclusive.", Integer.valueOf(i)));
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static int m7294(int i) {
        if (i >= -8 && i <= 7) {
            return i;
        }
        throw new IllegalArgumentException(String.format("Invalid literal value: %d. Must be between -8 and 7, inclusive.", Integer.valueOf(i)));
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public static int m7295(int i) {
        if (i >= -128 && i <= 127) {
            return i;
        }
        throw new IllegalArgumentException(String.format("Invalid literal value: %d. Must be between -128 and 127, inclusive.", Integer.valueOf(i)));
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public static int m7296(int i) {
        if (i >= -32768 && i <= 32767) {
            return i;
        }
        throw new IllegalArgumentException(String.format("Invalid literal value: %d. Must be between -32768 and 32767, inclusive.", Integer.valueOf(i)));
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public static int m7297(int i) {
        if ((65535 & i) == 0) {
            return i;
        }
        throw new IllegalArgumentException(String.format("Invalid literal value: %d. Low 16 bits must be zeroed out.", Integer.valueOf(i)));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static long m7289(long j) {
        if ((281474976710655L & j) == 0) {
            return j;
        }
        throw new IllegalArgumentException(String.format("Invalid literal value: %d. Low 48 bits must be zeroed out.", Long.valueOf(j)));
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public static int m7298(int i) {
        if (i >= 0 && i <= 5) {
            return i;
        }
        throw new IllegalArgumentException(String.format("Invalid register count: %d. Must be between 0 and 5, inclusive.", Integer.valueOf(i)));
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static int m7299(int i) {
        if ((i & -256) == 0) {
            return i;
        }
        throw new IllegalArgumentException(String.format("Invalid register count: %d. Must be between 0 and 255, inclusive.", Integer.valueOf(i)));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m7290(int i, int i2) {
        if (i <= i2) {
            return;
        }
        if (i2 == 0) {
            throw new IllegalArgumentException(String.format("Invalid value_arg value %d for an encoded_value. Expecting 0", Integer.valueOf(i)));
        } else {
            throw new IllegalArgumentException(String.format("Invalid value_arg value %d for an encoded_value. Expecting 0..%d, inclusive", Integer.valueOf(i), Integer.valueOf(i2)));
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public static int m7300(int i) {
        if (C1339.m7308(i)) {
            return i;
        }
        throw new IllegalArgumentException(String.format("Invalid verification error value: %d. Must be between 1 and 9, inclusive", Integer.valueOf(i)));
    }
}
