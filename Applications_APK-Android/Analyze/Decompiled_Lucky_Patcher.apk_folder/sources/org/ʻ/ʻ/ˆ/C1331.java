package org.ʻ.ʻ.ˆ;

import org.ʻ.ʻ.ʽ.C1163;
import org.ʻ.ʻ.ʽ.ʼ.C1134;

/* renamed from: org.ʻ.ʻ.ˆ.ʼ  reason: contains not printable characters */
/* compiled from: DexUtil */
public class C1331 {
    /* JADX WARNING: Can't wrap try/catch for region: R(3:8|9|10) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0022, code lost:
        throw new org.ʻ.ʻ.ʽ.C1163.C1166("File is too short");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0023, code lost:
        r2.reset();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0026, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0019, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x001b */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int m7274(java.io.InputStream r2) {
        /*
            boolean r0 = r2.markSupported()
            if (r0 == 0) goto L_0x0027
            r0 = 44
            r2.mark(r0)
            byte[] r0 = new byte[r0]
            com.google.ʻ.ʽ.C0931.m5801(r2, r0)     // Catch:{ EOFException -> 0x001b }
            r2.reset()
            r2 = 0
            int r2 = m7275(r0, r2)
            return r2
        L_0x0019:
            r0 = move-exception
            goto L_0x0023
        L_0x001b:
            org.ʻ.ʻ.ʽ.ʿ$ʼ r0 = new org.ʻ.ʻ.ʽ.ʿ$ʼ     // Catch:{ all -> 0x0019 }
            java.lang.String r1 = "File is too short"
            r0.<init>(r1)     // Catch:{ all -> 0x0019 }
            throw r0     // Catch:{ all -> 0x0019 }
        L_0x0023:
            r2.reset()
            throw r0
        L_0x0027:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "InputStream must support mark"
            r2.<init>(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʻ.ˆ.C1331.m7274(java.io.InputStream):int");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m7275(byte[] bArr, int i) {
        int r0 = C1134.m6690(bArr, i);
        if (r0 == -1) {
            StringBuilder sb = new StringBuilder("Not a valid dex magic value:");
            for (int i2 = 0; i2 < 8; i2++) {
                sb.append(String.format(" %02x", Byte.valueOf(bArr[i2])));
            }
            throw new C1163.C1166(sb.toString());
        } else if (C1134.m6693(r0)) {
            int r5 = C1134.m6692(bArr, i);
            if (r5 == 2018915346) {
                throw new C1333("Big endian dex files are not supported");
            } else if (r5 == 305419896) {
                return r0;
            } else {
                throw new C1332(String.format("Invalid endian tag: 0x%x", Integer.valueOf(r5)));
            }
        } else {
            throw new C1333(String.format("Dex version %03d is not supported", Integer.valueOf(r0)));
        }
    }

    /* renamed from: org.ʻ.ʻ.ˆ.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: DexUtil */
    public static class C1332 extends RuntimeException {
        public C1332() {
        }

        public C1332(String str) {
            super(str);
        }
    }

    /* renamed from: org.ʻ.ʻ.ˆ.ʼ$ʼ  reason: contains not printable characters */
    /* compiled from: DexUtil */
    public static class C1333 extends RuntimeException {
        public C1333() {
        }

        public C1333(String str) {
            super(str);
        }
    }
}
