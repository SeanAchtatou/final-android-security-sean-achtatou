package org.ʻ.ʻ.ˆ;

import com.google.ʻ.ʻ.C0848;
import java.util.Collection;
import org.ʻ.ʻ.C1001;
import org.ʻ.ʻ.ʾ.C1288;
import org.ʻ.ʻ.ʾ.ʽ.C1262;

/* renamed from: org.ʻ.ʻ.ˆ.ʿ  reason: contains not printable characters */
/* compiled from: MethodUtil */
public final class C1336 {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0848<C1288> f7147 = new C0848<C1288>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m7284(C1288 r1) {
            return r1 != null && C1336.m7283(r1);
        }
    };

    /* renamed from: ʼ  reason: contains not printable characters */
    public static C0848<C1288> f7148 = new C0848<C1288>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m7286(C1288 r1) {
            return r1 != null && !C1336.m7283(r1);
        }
    };

    /* renamed from: ʽ  reason: contains not printable characters */
    private static int f7149 = ((C1001.STATIC.m6290() | C1001.PRIVATE.m6290()) | C1001.CONSTRUCTOR.m6290());

    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m7283(C1288 r1) {
        return (r1.m7131() & f7149) != 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m7281(C1262 r0, boolean z) {
        return m7280(r0.m7078(), z);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m7280(Collection<? extends CharSequence> collection, boolean z) {
        int i = 0;
        for (CharSequence charAt : collection) {
            char charAt2 = charAt.charAt(0);
            i = (charAt2 == 'J' || charAt2 == 'D') ? i + 2 : i + 1;
        }
        return !z ? i + 1 : i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static char m7279(CharSequence charSequence) {
        if (charSequence.length() > 1) {
            return 'L';
        }
        return charSequence.charAt(0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m7282(Collection<? extends CharSequence> collection, String str) {
        StringBuilder sb = new StringBuilder(collection.size() + 1);
        sb.append(m7279(str));
        for (CharSequence r3 : collection) {
            sb.append(m7279(r3));
        }
        return sb.toString();
    }
}
