package org.ʻ.ʻ.ˆ;

/* renamed from: org.ʻ.ʻ.ˆ.ʻ  reason: contains not printable characters */
/* compiled from: AlignmentUtils */
public abstract class C1329 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final /* synthetic */ boolean f7145 = (!C1329.class.desiredAssertionStatus());

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m7273(int i, int i2) {
        int i3 = i2 - 1;
        if (f7145 || (i2 >= 0 && (i2 & i3) == 0)) {
            return (i + i3) & (i3 ^ -1);
        }
        throw new AssertionError();
    }
}
