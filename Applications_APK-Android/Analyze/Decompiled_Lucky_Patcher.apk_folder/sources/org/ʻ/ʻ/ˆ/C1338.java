package org.ʻ.ʻ.ˆ;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import org.ʻ.ʻ.ʾ.ʽ.C1259;
import org.ʻ.ʻ.ʾ.ʽ.C1261;
import org.ʻ.ʻ.ʾ.ʽ.C1262;

/* renamed from: org.ʻ.ʻ.ˆ.ˈ  reason: contains not printable characters */
/* compiled from: ReferenceUtil */
public final class C1338 {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ʻ.ʻ.ˆ.ˈ.ʻ(org.ʻ.ʻ.ʾ.ʽ.ʿ, boolean):java.lang.String
     arg types: [org.ʻ.ʻ.ʾ.ʽ.ʿ, int]
     candidates:
      org.ʻ.ʻ.ˆ.ˈ.ʻ(org.ʻ.ʻ.ʾ.ʽ.ʼ, boolean):java.lang.String
      org.ʻ.ʻ.ˆ.ˈ.ʻ(java.io.Writer, org.ʻ.ʻ.ʾ.ʽ.ʾ):void
      org.ʻ.ʻ.ˆ.ˈ.ʻ(org.ʻ.ʻ.ʾ.ʽ.ʿ, boolean):java.lang.String */
    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m7304(C1262 r1) {
        return m7305(r1, false);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m7305(C1262 r2, boolean z) {
        StringBuilder sb = new StringBuilder();
        if (!z) {
            sb.append(r2.m7075());
            sb.append("->");
        }
        sb.append(r2.m7076());
        sb.append('(');
        for (CharSequence append : r2.m7078()) {
            sb.append(append);
        }
        sb.append(')');
        sb.append(r2.m7077());
        return sb.toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m7303(C1261 r1) {
        StringWriter stringWriter = new StringWriter();
        try {
            m7306(stringWriter, r1);
            return stringWriter.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m7306(Writer writer, C1261 r3) {
        writer.write(40);
        for (CharSequence charSequence : r3.m7072()) {
            writer.write(charSequence.toString());
        }
        writer.write(41);
        writer.write(r3.m7073());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ʻ.ʻ.ˆ.ˈ.ʻ(org.ʻ.ʻ.ʾ.ʽ.ʼ, boolean):java.lang.String
     arg types: [org.ʻ.ʻ.ʾ.ʽ.ʼ, int]
     candidates:
      org.ʻ.ʻ.ˆ.ˈ.ʻ(org.ʻ.ʻ.ʾ.ʽ.ʿ, boolean):java.lang.String
      org.ʻ.ʻ.ˆ.ˈ.ʻ(java.io.Writer, org.ʻ.ʻ.ʾ.ʽ.ʾ):void
      org.ʻ.ʻ.ˆ.ˈ.ʻ(org.ʻ.ʻ.ʾ.ʽ.ʼ, boolean):java.lang.String */
    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m7301(C1259 r1) {
        return m7302(r1, false);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m7302(C1259 r1, boolean z) {
        StringBuilder sb = new StringBuilder();
        if (!z) {
            sb.append(r1.m7067());
            sb.append("->");
        }
        sb.append(r1.m7065());
        sb.append(':');
        sb.append(r1.m7066());
        return sb.toString();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static String m7307(C1259 r2) {
        return r2.m7065() + ':' + r2.m7066();
    }
}
