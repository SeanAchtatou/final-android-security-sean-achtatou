package org.ʻ.ʼ;

import com.google.ʻ.ʼ.C0891;
import com.google.ʻ.ʼ.C0904;
import java.util.Iterator;

/* renamed from: org.ʻ.ʼ.ˈ  reason: contains not printable characters */
/* compiled from: ImmutableConverter */
public abstract class C1406<ImmutableItem, Item> {
    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract ImmutableItem m7803(Object obj);

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract boolean m7805(Object obj);

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0891<ImmutableItem> m7802(Iterable iterable) {
        if (iterable == null) {
            return C0891.m5603();
        }
        boolean z = true;
        if (iterable instanceof C0891) {
            Iterator it = iterable.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (!m7805(it.next())) {
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
        }
        if (!z) {
            return (C0891) iterable;
        }
        final Iterator it2 = iterable.iterator();
        return C0891.m5599((Iterator) new Iterator<ImmutableItem>() {
            public boolean hasNext() {
                return it2.hasNext();
            }

            public ImmutableItem next() {
                return C1406.this.m7803(it2.next());
            }

            public void remove() {
                it2.remove();
            }
        });
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0904<ImmutableItem> m7804(Iterable iterable) {
        if (iterable == null) {
            return C0904.m5655();
        }
        boolean z = true;
        if (iterable instanceof C0904) {
            Iterator it = iterable.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (!m7805(it.next())) {
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
        }
        if (!z) {
            return (C0904) iterable;
        }
        final Iterator it2 = iterable.iterator();
        return C0904.m5651((Iterator) new Iterator<ImmutableItem>() {
            public boolean hasNext() {
                return it2.hasNext();
            }

            public ImmutableItem next() {
                return C1406.this.m7803(it2.next());
            }

            public void remove() {
                it2.remove();
            }
        });
    }
}
