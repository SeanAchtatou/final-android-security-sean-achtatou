package org.ʻ.ʼ;

/* renamed from: org.ʻ.ʼ.ˉ  reason: contains not printable characters */
/* compiled from: NibbleUtils */
public abstract class C1407 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m7806(int i) {
        return (i << 24) >> 28;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int m7807(int i) {
        return (i & 240) >>> 4;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static int m7808(int i) {
        return i & 15;
    }
}
