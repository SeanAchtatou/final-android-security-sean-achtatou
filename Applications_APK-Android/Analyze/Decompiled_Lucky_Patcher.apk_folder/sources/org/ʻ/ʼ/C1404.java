package org.ʻ.ʼ;

import java.io.PrintStream;
import java.io.PrintWriter;

/* renamed from: org.ʻ.ʼ.ʿ  reason: contains not printable characters */
/* compiled from: ExceptionWithContext */
public class C1404 extends RuntimeException {

    /* renamed from: ʻ  reason: contains not printable characters */
    private StringBuffer f7288;

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1404 m7798(Throwable th, String str, Object... objArr) {
        C1404 r1;
        if (th instanceof C1404) {
            r1 = (C1404) th;
        } else {
            r1 = new C1404(th);
        }
        r1.m7799(String.format(str, objArr));
        return r1;
    }

    public C1404(String str, Object... objArr) {
        this(null, str, objArr);
    }

    public C1404(Throwable th) {
        this(th, null, new Object[0]);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C1404(java.lang.Throwable r2, java.lang.String r3, java.lang.Object... r4) {
        /*
            r1 = this;
            if (r3 == 0) goto L_0x0007
            java.lang.String r3 = m7797(r3, r4)
            goto L_0x000f
        L_0x0007:
            if (r2 == 0) goto L_0x000e
            java.lang.String r3 = r2.getMessage()
            goto L_0x000f
        L_0x000e:
            r3 = 0
        L_0x000f:
            r1.<init>(r3, r2)
            boolean r3 = r2 instanceof org.ʻ.ʼ.C1404
            r4 = 200(0xc8, float:2.8E-43)
            if (r3 == 0) goto L_0x0032
            org.ʻ.ʼ.ʿ r2 = (org.ʻ.ʼ.C1404) r2
            java.lang.StringBuffer r2 = r2.f7288
            java.lang.String r2 = r2.toString()
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            int r0 = r2.length()
            int r0 = r0 + r4
            r3.<init>(r0)
            r1.f7288 = r3
            java.lang.StringBuffer r3 = r1.f7288
            r3.append(r2)
            goto L_0x0039
        L_0x0032:
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>(r4)
            r1.f7288 = r2
        L_0x0039:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ʻ.ʼ.C1404.<init>(java.lang.Throwable, java.lang.String, java.lang.Object[]):void");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static String m7797(String str, Object... objArr) {
        if (str == null) {
            return null;
        }
        return String.format(str, objArr);
    }

    public void printStackTrace(PrintStream printStream) {
        super.printStackTrace(printStream);
        printStream.println(this.f7288);
    }

    public void printStackTrace(PrintWriter printWriter) {
        super.printStackTrace(printWriter);
        printWriter.println(this.f7288);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7799(String str) {
        if (str != null) {
            this.f7288.append(str);
            if (!str.endsWith("\n")) {
                this.f7288.append(10);
                return;
            }
            return;
        }
        throw new NullPointerException("str == null");
    }
}
