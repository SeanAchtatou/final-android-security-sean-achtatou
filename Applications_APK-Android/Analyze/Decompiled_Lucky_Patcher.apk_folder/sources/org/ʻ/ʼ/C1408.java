package org.ʻ.ʼ;

import java.io.OutputStream;
import java.io.RandomAccessFile;

/* renamed from: org.ʻ.ʼ.ˊ  reason: contains not printable characters */
/* compiled from: RandomAccessFileOutputStream */
public class C1408 extends OutputStream {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f7293;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final RandomAccessFile f7294;

    public C1408(RandomAccessFile randomAccessFile, int i) {
        this.f7293 = i;
        this.f7294 = randomAccessFile;
    }

    public void write(int i) {
        this.f7294.seek((long) this.f7293);
        this.f7293++;
        this.f7294.write(i);
    }

    public void write(byte[] bArr) {
        this.f7294.seek((long) this.f7293);
        this.f7293 += bArr.length;
        this.f7294.write(bArr);
    }

    public void write(byte[] bArr, int i, int i2) {
        this.f7294.seek((long) this.f7293);
        this.f7293 += i2;
        this.f7294.write(bArr, i, i2);
    }
}
