package org.ʻ.ʼ;

/* renamed from: org.ʻ.ʼ.ˆ  reason: contains not printable characters */
/* compiled from: Hex */
public final class C1405 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m7800(int i) {
        char[] cArr = new char[8];
        for (int i2 = 0; i2 < 8; i2++) {
            cArr[7 - i2] = Character.forDigit(i & 15, 16);
            i >>= 4;
        }
        return new String(cArr);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static String m7801(int i) {
        char[] cArr = new char[2];
        for (int i2 = 0; i2 < 2; i2++) {
            cArr[1 - i2] = Character.forDigit(i & 15, 16);
            i >>= 4;
        }
        return new String(cArr);
    }
}
