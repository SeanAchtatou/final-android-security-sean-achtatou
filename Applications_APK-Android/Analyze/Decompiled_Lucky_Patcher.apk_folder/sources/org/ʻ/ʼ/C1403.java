package org.ʻ.ʼ;

import com.google.ʻ.ʻ.C0848;
import com.google.ʻ.ʼ.C0858;
import com.google.ʻ.ʼ.C0912;
import com.google.ʻ.ʿ.C0935;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.SortedSet;

/* renamed from: org.ʻ.ʼ.ʾ  reason: contains not printable characters */
/* compiled from: CollectionUtils */
public class C1403 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> int m7791(Iterable<T> iterable, C0848<? super T> r4) {
        int i = 0;
        int i2 = -1;
        for (T r2 : iterable) {
            if (r4.m5433(r2)) {
                i2 = i;
            }
            i++;
        }
        return i2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T extends Comparable<? super T>> int m7792(Collection<? extends T> collection, Collection<? extends T> collection2) {
        int r0 = C0935.m5810(collection.size(), collection2.size());
        if (r0 != 0) {
            return r0;
        }
        Iterator<? extends T> it = collection2.iterator();
        Iterator<? extends T> it2 = collection.iterator();
        while (it2.hasNext()) {
            int compareTo = ((Comparable) it2.next()).compareTo(it.next());
            if (compareTo != 0) {
                return compareTo;
            }
        }
        return 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> int m7793(Comparator<? super T> comparator, Iterable<? extends T> iterable, Iterable<? extends T> iterable2) {
        Iterator<? extends T> it = iterable2.iterator();
        for (Object compare : iterable) {
            try {
                int compare2 = comparator.compare(compare, it.next());
                if (compare2 != 0) {
                    return compare2;
                }
            } catch (NoSuchElementException unused) {
                return 1;
            }
        }
        return it.hasNext() ? -1 : 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static <T> boolean m7795(Iterable<? extends T> iterable) {
        if (!(iterable instanceof SortedSet)) {
            return false;
        }
        Comparator comparator = ((SortedSet) iterable).comparator();
        if (comparator == null || comparator.equals(C0858.m5447())) {
            return true;
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static <T> SortedSet<? extends T> m7794(Collection<? extends T> collection) {
        if (m7795((Iterable) collection)) {
            return (SortedSet) collection;
        }
        return C0912.m5706((Collection) collection);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static <T extends Comparable<T>> int m7796(Collection<? extends T> collection, Collection<? extends T> collection2) {
        int r0 = C0935.m5810(collection.size(), collection2.size());
        if (r0 != 0) {
            return r0;
        }
        m7794((Collection) collection);
        m7794((Collection) collection2);
        Iterator<? extends T> it = collection2.iterator();
        Iterator<? extends T> it2 = collection.iterator();
        while (it2.hasNext()) {
            int compareTo = ((Comparable) it2.next()).compareTo(it.next());
            if (compareTo != 0) {
                return compareTo;
            }
        }
        return 0;
    }
}
