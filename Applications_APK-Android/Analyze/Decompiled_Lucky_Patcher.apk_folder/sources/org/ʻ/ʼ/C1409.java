package org.ʻ.ʼ;

/* renamed from: org.ʻ.ʼ.ˋ  reason: contains not printable characters */
/* compiled from: Utf8Utils */
public final class C1409 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final ThreadLocal<char[]> f7295 = new ThreadLocal<char[]>() {
        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public char[] initialValue() {
            return new char[256];
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m7810(byte[] bArr, int i, int i2, int[] iArr) {
        char c;
        char[] cArr = f7295.get();
        if (cArr == null || cArr.length < i2) {
            cArr = new char[i2];
            f7295.set(cArr);
        }
        int i3 = i;
        int i4 = 0;
        while (i2 > 0) {
            byte b = bArr[i3] & 255;
            switch (b >> 4) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    if (b != 0) {
                        c = (char) b;
                        i3++;
                        break;
                    } else {
                        return m7809(b, i3);
                    }
                case 8:
                case 9:
                case 10:
                case 11:
                default:
                    return m7809(b, i3);
                case 12:
                case 13:
                    int i5 = i3 + 1;
                    byte b2 = bArr[i5] & 255;
                    if ((b2 & 192) != 128) {
                        return m7809(b2, i5);
                    }
                    byte b3 = ((b & 31) << 6) | (b2 & 63);
                    if (b3 == 0 || b3 >= 128) {
                        c = (char) b3;
                        i3 += 2;
                        break;
                    } else {
                        return m7809(b2, i5);
                    }
                case 14:
                    int i6 = i3 + 1;
                    byte b4 = bArr[i6] & 255;
                    if ((b4 & 192) == 128) {
                        int i7 = i3 + 2;
                        byte b5 = bArr[i7] & 255;
                        if ((b5 & 192) == 128) {
                            byte b6 = ((b & 15) << 12) | ((b4 & 63) << 6) | (b5 & 63);
                            if (b6 >= 2048) {
                                c = (char) b6;
                                i3 += 3;
                                break;
                            } else {
                                return m7809(b5, i7);
                            }
                        } else {
                            return m7809(b5, i7);
                        }
                    } else {
                        return m7809(b4, i6);
                    }
            }
            cArr[i4] = c;
            i4++;
            i2--;
        }
        if (iArr != null && iArr.length > 0) {
            int i8 = i3 - i;
            iArr[0] = i8;
            iArr[0] = i8;
        }
        return new String(cArr, 0, i4);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static String m7809(int i, int i2) {
        throw new IllegalArgumentException("bad utf-8 byte " + C1405.m7801(i) + " at offset " + C1405.m7800(i2));
    }
}
