package org.ʻ.ʼ;

import java.util.AbstractSequentialList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/* renamed from: org.ʻ.ʼ.ʻ  reason: contains not printable characters */
/* compiled from: AbstractForwardSequentialList */
public abstract class C1400<T> extends AbstractSequentialList<T> {
    public abstract Iterator<T> iterator();

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Iterator<T> m7787(int i) {
        if (i >= 0) {
            Iterator<T> it = iterator();
            for (int i2 = 0; i2 < i; i2++) {
                it.next();
            }
            return it;
        }
        throw new NoSuchElementException();
    }

    public ListIterator<T> listIterator(final int i) {
        try {
            final Iterator r0 = m7787(i);
            return new C1401<T>() {

                /* renamed from: ʾ  reason: contains not printable characters */
                private int f7285 = (i - 1);

                /* renamed from: ʿ  reason: contains not printable characters */
                private Iterator<T> f7286 = r0;

                /* renamed from: ʻ  reason: contains not printable characters */
                private Iterator<T> m7789() {
                    if (this.f7286 == null) {
                        try {
                            this.f7286 = C1400.this.m7787(this.f7285 + 1);
                        } catch (IndexOutOfBoundsException unused) {
                            throw new NoSuchElementException();
                        }
                    }
                    return this.f7286;
                }

                public boolean hasNext() {
                    return m7789().hasNext();
                }

                public boolean hasPrevious() {
                    return this.f7285 >= 0;
                }

                public T next() {
                    T next = m7789().next();
                    this.f7285++;
                    return next;
                }

                public int nextIndex() {
                    return this.f7285 + 1;
                }

                public T previous() {
                    this.f7286 = null;
                    try {
                        C1400 r0 = C1400.this;
                        int i = this.f7285;
                        this.f7285 = i - 1;
                        return r0.m7787(i).next();
                    } catch (IndexOutOfBoundsException unused) {
                        throw new NoSuchElementException();
                    }
                }

                public int previousIndex() {
                    return this.f7285;
                }
            };
        } catch (NoSuchElementException unused) {
            throw new IndexOutOfBoundsException();
        }
    }

    public ListIterator<T> listIterator() {
        return listIterator(0);
    }
}
