package ʻ.ʻ;

import java.nio.charset.Charset;
import java.util.zip.CRC32;
import net.lingala.zip4j.util.InternalZipConstants;

/* renamed from: ʻ.ʻ.ˈ  reason: contains not printable characters */
/* compiled from: ZipEntry */
public class C1416 implements Cloneable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f7341 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f7342;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f7343;

    /* renamed from: ʾ  reason: contains not printable characters */
    private long f7344;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f7345;

    /* renamed from: ˆ  reason: contains not printable characters */
    private long f7346;

    /* renamed from: ˈ  reason: contains not printable characters */
    private long f7347;

    /* renamed from: ˉ  reason: contains not printable characters */
    private String f7348;

    /* renamed from: ˊ  reason: contains not printable characters */
    private String f7349;

    /* renamed from: ˋ  reason: contains not printable characters */
    private byte[] f7350;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f7351 = 0;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f7352 = 0;

    /* renamed from: ˑ  reason: contains not printable characters */
    private long f7353;

    /* renamed from: י  reason: contains not printable characters */
    private long f7354;

    /* renamed from: ـ  reason: contains not printable characters */
    private byte[] f7355;

    /* renamed from: ٴ  reason: contains not printable characters */
    private byte[] f7356;

    C1416() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m7840() {
        return this.f7341;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7841(int i) {
        this.f7341 = i;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m7846() {
        return this.f7342;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7847(int i) {
        this.f7342 = i;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m7851() {
        return this.f7343;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m7852(int i) {
        this.f7343 = i;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m7857() {
        return (this.f7342 & 1) != 0;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public long m7858() {
        return this.f7344;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7842(long j) {
        this.f7344 = j;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public int m7861() {
        return this.f7345;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m7855(int i) {
        this.f7345 = i;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public long m7863() {
        return this.f7346;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7848(long j) {
        this.f7346 = j;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public long m7864() {
        return this.f7347;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m7853(long j) {
        this.f7347 = j;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean m7865() {
        return m7866().endsWith(InternalZipConstants.ZIP_FILE_SEPARATOR);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public String m7866() {
        return this.f7348;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7843(String str) {
        if (str == null) {
            throw new NullPointerException();
        } else if (!str.isEmpty()) {
            if (m7840() == 0 && !str.contains(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                str = str.replace('\\', '/');
            }
            this.f7348 = str;
        } else {
            throw new IllegalArgumentException("Name can not be empty");
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public String m7867() {
        return this.f7349;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7849(String str) {
        this.f7349 = str;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public byte[] m7868() {
        return this.f7350;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7845(byte[] bArr) {
        this.f7350 = bArr;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public int m7869() {
        return this.f7351;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m7859(int i) {
        this.f7351 = i;
    }

    /* renamed from: י  reason: contains not printable characters */
    public int m7870() {
        return this.f7352;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m7862(int i) {
        this.f7352 = i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ـ  reason: contains not printable characters */
    public long m7871() {
        return this.f7353;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m7856(long j) {
        this.f7353 = j;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public long m7872() {
        return this.f7354;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m7860(long j) {
        this.f7354 = j;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7844(Charset charset, boolean z) {
        byte[] bArr = this.f7350;
        if (bArr == null || bArr.length == 0) {
            z = false;
        }
        String r1 = z ? m7839(C1414.m7829(28789, this.f7350), this.f7355) : null;
        if (r1 == null) {
            r1 = new String(this.f7355, charset);
        }
        m7843(r1);
        this.f7355 = null;
        if (this.f7356 != null) {
            String r5 = z ? m7839(C1414.m7829(25461, this.f7350), this.f7356) : null;
            if (r5 == null) {
                r5 = new String(this.f7356, charset);
            }
            m7849(r5);
            this.f7356 = null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private String m7839(C1414 r7, byte[] bArr) {
        byte[] r1;
        int length;
        if (r7 == null || bArr == null || (length = (r1 = r7.m7832()).length) < 5 || r7.m7835(0) != 1) {
            return null;
        }
        int r72 = r7.m7836(1);
        CRC32 crc32 = new CRC32();
        crc32.update(bArr);
        if (r72 != ((int) crc32.getValue())) {
            return null;
        }
        return new String(r1, 5, length - 5, C1413.f7337);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7850(byte[] bArr) {
        this.f7355 = bArr;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m7854(byte[] bArr) {
        this.f7356 = bArr;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean m7873() {
        return (m7846() & InternalZipConstants.UFT8_NAMES_FLAG) != 0;
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public C1416 clone() {
        try {
            return (C1416) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
