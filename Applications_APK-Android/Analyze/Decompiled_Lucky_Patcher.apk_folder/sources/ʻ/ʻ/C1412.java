package ʻ.ʻ;

import java.nio.charset.Charset;

/* renamed from: ʻ.ʻ.ʽ  reason: contains not printable characters */
/* compiled from: CenterFileHeader */
class C1412 {

    /* renamed from: ʻ  reason: contains not printable characters */
    int f7320 = 10;

    /* renamed from: ʼ  reason: contains not printable characters */
    int f7321 = 10;

    /* renamed from: ʽ  reason: contains not printable characters */
    int f7322;

    /* renamed from: ʾ  reason: contains not printable characters */
    int f7323;

    /* renamed from: ʿ  reason: contains not printable characters */
    int f7324;

    /* renamed from: ˆ  reason: contains not printable characters */
    int f7325;

    /* renamed from: ˈ  reason: contains not printable characters */
    int f7326;

    /* renamed from: ˉ  reason: contains not printable characters */
    int f7327;

    /* renamed from: ˊ  reason: contains not printable characters */
    byte[] f7328;

    /* renamed from: ˋ  reason: contains not printable characters */
    byte[] f7329;

    /* renamed from: ˎ  reason: contains not printable characters */
    byte[] f7330;

    /* renamed from: ˏ  reason: contains not printable characters */
    int f7331;

    /* renamed from: ˑ  reason: contains not printable characters */
    int f7332;

    /* renamed from: י  reason: contains not printable characters */
    int f7333;

    /* renamed from: ـ  reason: contains not printable characters */
    long f7334;

    /* renamed from: ٴ  reason: contains not printable characters */
    boolean f7335;

    /* renamed from: ᐧ  reason: contains not printable characters */
    boolean f7336;

    C1412(C1416 r3, Charset charset) {
        if (r3.m7857()) {
            this.f7320 = 20;
            this.f7321 = 20;
        }
        this.f7328 = r3.m7866().getBytes(charset);
        this.f7336 = charset.name().equalsIgnoreCase("UTF-8");
        this.f7324 = (int) C1419.m7903(r3.m7858());
        this.f7329 = r3.m7868() == null ? new byte[0] : r3.m7868();
        this.f7330 = r3.m7867() == null ? new byte[0] : r3.m7867().getBytes(charset);
        this.f7332 = r3.m7869();
        this.f7333 = r3.m7870();
        this.f7335 = r3.m7865();
    }
}
