package ʻ.ʻ;

import java.util.Calendar;

/* renamed from: ʻ.ʻ.ˋ  reason: contains not printable characters */
/* compiled from: ZipUtil */
public class C1419 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static long m7901(long j) {
        Calendar instance = Calendar.getInstance();
        instance.set(1, ((int) ((j >> 25) & 127)) + 1980);
        instance.set(2, ((int) ((j >> 21) & 15)) - 1);
        instance.set(5, ((int) (j >> 16)) & 31);
        instance.set(11, ((int) (j >> 11)) & 31);
        instance.set(12, ((int) (j >> 5)) & 63);
        instance.set(13, ((int) (j << 1)) & 62);
        return instance.getTime().getTime();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static long m7903(long j) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(j);
        int i = instance.get(1);
        if (i < 1980) {
            return 2162688;
        }
        return (long) ((instance.get(13) >> 1) | ((i - 1980) << 25) | ((instance.get(2) + 1) << 21) | (instance.get(5) << 16) | (instance.get(11) << 11) | (instance.get(12) << 5));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m7900(byte[] bArr, int i) {
        return bArr[i] & 255;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int m7902(byte[] bArr, int i) {
        return ((bArr[i + 1] & 255) << 8) | (bArr[i] & 255);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static int m7904(byte[] bArr, int i) {
        return ((((bArr[i + 3] & 255) << 8) | (bArr[i + 2] & 255)) << 16) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8);
    }
}
