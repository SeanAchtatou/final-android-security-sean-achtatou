package ʻ.ʻ;

/* renamed from: ʻ.ʻ.ʿ  reason: contains not printable characters */
/* compiled from: ExtraDataRecord */
public class C1414 implements Cloneable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f7338;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f7339;

    /* renamed from: ʽ  reason: contains not printable characters */
    private byte[] f7340;

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C1414 m7829(int i, byte[] bArr) {
        int i2 = 0;
        while (bArr.length - i2 >= 4) {
            int r2 = C1419.m7902(bArr, i2);
            int r4 = C1419.m7902(bArr, i2 + 2);
            int i3 = i2 + 4;
            if (r4 < 0 || r4 > bArr.length - i3) {
                break;
            } else if (r2 != i) {
                i2 = i3 + r4;
            } else {
                byte[] bArr2 = new byte[r4];
                System.arraycopy(bArr, i3, bArr2, 0, r4);
                C1414 r7 = new C1414();
                r7.m7830(i);
                r7.m7834(r4);
                r7.m7831(bArr2);
                return r7;
            }
        }
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7830(int i) {
        this.f7338 = i;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7834(int i) {
        this.f7339 = i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public byte[] m7832() {
        return this.f7340;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7831(byte[] bArr) {
        this.f7340 = bArr;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m7835(int i) {
        return C1419.m7900(this.f7340, i);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m7836(int i) {
        return C1419.m7904(this.f7340, i);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public C1414 clone() {
        return (C1414) super.clone();
    }
}
