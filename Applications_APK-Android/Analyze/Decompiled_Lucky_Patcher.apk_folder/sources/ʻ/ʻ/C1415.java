package ʻ.ʻ;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/* renamed from: ʻ.ʻ.ˆ  reason: contains not printable characters */
/* compiled from: ZipAlign */
public class C1415 {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m7838(File file, File file2) {
        C1418 r2;
        C1417 r1;
        C1417 r0 = null;
        try {
            r1 = new C1417(file);
            try {
                r2 = new C1418(file2);
            } catch (Throwable th) {
                th = th;
                r2 = null;
                m7837(r1);
                m7837(r2);
                throw th;
            }
            try {
                r2.m7898();
                Iterator<C1416> it = r1.m7885().iterator();
                while (it.hasNext()) {
                    r2.m7899(it.next(), r1);
                }
                m7837(r1);
            } catch (Throwable th2) {
                th = th2;
                m7837(r1);
                m7837(r2);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            r2 = null;
            r1 = null;
            m7837(r1);
            m7837(r2);
            throw th;
        }
        m7837(r2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m7837(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
