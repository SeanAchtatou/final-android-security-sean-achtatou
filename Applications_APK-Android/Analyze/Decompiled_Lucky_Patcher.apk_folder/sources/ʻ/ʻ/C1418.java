package ʻ.ʻ;

import java.io.Closeable;
import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import net.lingala.zip4j.util.InternalZipConstants;

/* renamed from: ʻ.ʻ.ˊ  reason: contains not printable characters */
/* compiled from: ZipMaker */
public class C1418 implements Closeable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C1411 f7361;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ArrayList<C1412> f7362 = new ArrayList<>();

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f7363;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final byte[] f7364 = new byte[4096];

    public C1418(File file) {
        if (file.exists()) {
            file.delete();
        }
        this.f7361 = new C1411(new RandomAccessFile(file, InternalZipConstants.WRITE_MODE));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7898() {
        this.f7363 = true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7899(C1416 r4, C1417 r5) {
        m7890(r4);
        InputStream r42 = r5.m7884(r4);
        byte[] bArr = this.f7364;
        while (true) {
            int read = r42.read(bArr);
            if (read != -1) {
                this.f7361.m7820(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m7890(C1416 r4) {
        C1412 r0 = new C1412(r4, C1413.f7337);
        if (r4.m7857()) {
            r0.f7322 |= 1;
        }
        if (r0.f7336) {
            r0.f7322 |= InternalZipConstants.UFT8_NAMES_FLAG;
        }
        r0.f7323 = r4.m7851();
        r0.f7325 = r4.m7861();
        r0.f7326 = (int) r4.m7863();
        r0.f7327 = (int) r4.m7864();
        r0.f7334 = m7894();
        this.f7362.add(r0);
        m7888(r0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m7888(C1412 r5) {
        m7895(67324752);
        m7886(r5.f7321);
        m7886(r5.f7322);
        m7886(r5.f7323);
        m7895(r5.f7324);
        m7895(r5.f7325);
        m7895(r5.f7326);
        m7895(r5.f7327);
        m7886(r5.f7328.length);
        if (this.f7363 && r5.f7323 == 0) {
            m7889(r5, m7894() + 2 + ((long) r5.f7328.length));
        }
        m7886(r5.f7329.length);
        m7897(r5.f7328);
        m7897(r5.f7329);
    }

    public void close() {
        if (!this.f7361.m7828()) {
            long r0 = m7894();
            Iterator<C1412> it = this.f7362.iterator();
            while (it.hasNext()) {
                m7896(it.next());
            }
            m7887(m7894() - r0, r0);
            this.f7361.m7827();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m7889(C1412 r4, long j) {
        int i = new String(r4.f7328, C1413.f7337).endsWith(".so") ? 4096 : 4;
        if (!m7891(((long) r4.f7329.length) + j, i)) {
            if (m7892(r4.f7329)) {
                r4.f7329 = new byte[m7893(j, i)];
            } else {
                r4.f7329 = Arrays.copyOf(r4.f7329, r4.f7329.length + m7893(j + ((long) r4.f7329.length), i));
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean m7891(long j, int i) {
        return j % ((long) i) == 0;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static int m7893(long j, int i) {
        long j2 = (long) i;
        return ((int) (j2 - (j % j2))) % i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean m7892(byte[] bArr) {
        for (byte b : bArr) {
            if (b != 0) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m7896(C1412 r3) {
        m7895(33639248);
        m7886(r3.f7320);
        m7886(r3.f7321);
        m7886(r3.f7322);
        m7886(r3.f7323);
        m7895(r3.f7324);
        m7895(r3.f7325);
        m7895(r3.f7326);
        m7895(r3.f7327);
        m7886(r3.f7328.length);
        m7886(r3.f7329.length);
        m7886(r3.f7330.length);
        m7886(r3.f7331);
        m7886(r3.f7332);
        m7895(r3.f7333);
        m7895((int) r3.f7334);
        m7897(r3.f7328);
        m7897(r3.f7329);
        m7897(r3.f7330);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m7887(long j, long j2) {
        byte[] bArr = new byte[0];
        m7895(101010256);
        m7886(0);
        m7886(0);
        m7886(this.f7362.size());
        m7886(this.f7362.size());
        m7895((int) j);
        m7895((int) j2);
        m7886(bArr.length);
        m7897(bArr);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private long m7894() {
        return this.f7361.m7824();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m7897(byte[] bArr) {
        if (bArr.length > 0) {
            this.f7361.m7819(bArr);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m7886(int i) {
        this.f7361.m7817(i & 255);
        this.f7361.m7817((i >>> 8) & 255);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m7895(int i) {
        this.f7361.m7817(i & 255);
        this.f7361.m7817((i >>> 8) & 255);
        this.f7361.m7817((i >>> 16) & 255);
        this.f7361.m7817((i >>> 24) & 255);
    }
}
