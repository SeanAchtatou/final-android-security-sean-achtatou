package ʻ.ʻ;

import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipConstants;

/* renamed from: ʻ.ʻ.ˉ  reason: contains not printable characters */
/* compiled from: ZipFile */
public class C1417 implements Closeable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C1411 f7357;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Map<String, C1416> f7358;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f7359;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f7360;

    public C1417(File file) {
        this(new RandomAccessFile(file, InternalZipConstants.READ_MODE));
    }

    public C1417(RandomAccessFile randomAccessFile) {
        this.f7358 = new LinkedHashMap();
        this.f7360 = false;
        this.f7357 = new C1411(randomAccessFile);
        this.f7359 = true;
        m7877();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public ArrayList<C1416> m7885() {
        return new ArrayList<>(this.f7358.values());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ʻ.ʻ.ˈ.ʻ(java.nio.charset.Charset, boolean):void
     arg types: [java.nio.charset.Charset, int]
     candidates:
      ʻ.ʻ.ˈ.ʻ(ʻ.ʻ.ʿ, byte[]):java.lang.String
      ʻ.ʻ.ˈ.ʻ(java.nio.charset.Charset, boolean):void */
    /* renamed from: ʼ  reason: contains not printable characters */
    private void m7877() {
        ArrayList<C1416> arrayList = new ArrayList<>();
        m7879();
        while (m7881() == 33639248) {
            C1416 r1 = new C1416();
            r1.m7841((m7882() >> 8) & 15);
            m7882();
            r1.m7847(m7882());
            r1.m7852(m7882());
            r1.m7842(C1419.m7901(m7883()));
            r1.m7855(m7881());
            r1.m7848(m7883());
            r1.m7853(m7883());
            int r2 = m7882();
            int r3 = m7882();
            int r4 = m7882();
            m7882();
            r1.m7859(m7882());
            r1.m7862(m7881());
            r1.m7856(m7883());
            r1.m7850(m7878(r2));
            if (r3 > 0) {
                m7875(r3);
            }
            if (r4 > 0) {
                r1.m7854(m7878(r4));
            }
            arrayList.add(r1);
        }
        Charset charset = C1413.f7337;
        Charset charset2 = C1413.f7337;
        this.f7358.clear();
        for (C1416 r42 : arrayList) {
            long r5 = r42.m7871() + 26;
            m7876(r5);
            int r7 = m7882();
            int r8 = m7882();
            m7875(r7);
            r42.m7845(m7878(r8));
            r42.m7860(r5 + 2 + 2 + ((long) r7) + ((long) r8));
            int i = 0;
            if (r42.m7873()) {
                r42.m7844(charset2, false);
            } else {
                r42.m7844(charset, true);
            }
            if (r42.m7857() && this.f7359) {
                r42.m7847(r42.m7846() & -2);
                int r52 = r42.m7851();
                if (!(r52 == 8 || r52 == 0)) {
                    if (r42.m7864() != r42.m7863()) {
                        i = 8;
                    }
                    r42.m7852(i);
                }
            }
            this.f7358.put(r42.m7866(), r42);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* renamed from: ʽ  reason: contains not printable characters */
    private void m7879() {
        boolean z;
        long r0 = m7880();
        long j = r0 - 22;
        long max = Math.max(0L, r0 - 65557);
        while (true) {
            if (j < max) {
                z = false;
                break;
            }
            m7876(j);
            if (m7881() == 101010256) {
                z = true;
                break;
            }
            j--;
        }
        if (z) {
            m7876(j + 16);
            m7876(m7883());
            return;
        }
        throw new IOException("Archive is not a ZIP archive");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public InputStream m7884(C1416 r8) {
        return new C1410(this.f7357, r8.m7872(), r8.m7863());
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private long m7880() {
        return this.f7357.m7822();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m7876(long j) {
        this.f7357.m7818(j);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m7875(int i) {
        if (i >= 0) {
            long r0 = this.f7357.m7824() + ((long) i);
            if (r0 <= this.f7357.m7822()) {
                this.f7357.m7818(r0);
                return;
            }
            throw new EOFException();
        }
        throw new IOException("Skip " + i);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private byte[] m7878(int i) {
        byte[] bArr = new byte[i];
        this.f7357.m7823(bArr);
        return bArr;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private int m7881() {
        int r0 = this.f7357.m7816();
        int r1 = this.f7357.m7816();
        int r2 = this.f7357.m7816();
        int r3 = this.f7357.m7816();
        if ((r0 | r1 | r2 | r3) >= 0) {
            return r0 | (r1 << 8) | (r2 << 16) | (r3 << 24);
        }
        throw new EOFException();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private int m7882() {
        int r0 = this.f7357.m7816();
        int r1 = this.f7357.m7816();
        if ((r0 | r1) >= 0) {
            return r0 | (r1 << 8);
        }
        throw new EOFException();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private long m7883() {
        return ((long) m7881()) & InternalZipConstants.ZIP_64_LIMIT;
    }

    public void close() {
        if (!this.f7360) {
            this.f7357.m7827();
            this.f7358.clear();
            this.f7360 = true;
        }
    }
}
