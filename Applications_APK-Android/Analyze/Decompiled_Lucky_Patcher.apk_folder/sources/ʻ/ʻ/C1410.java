package ʻ.ʻ;

import java.io.InputStream;

/* renamed from: ʻ.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: BridgeInputStream */
public class C1410 extends InputStream {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C1411 f7307;

    /* renamed from: ʼ  reason: contains not printable characters */
    private long f7308;

    /* renamed from: ʽ  reason: contains not printable characters */
    private long f7309;

    public C1410(C1411 r1, long j, long j2) {
        this.f7307 = r1;
        this.f7308 = j2;
        this.f7309 = j;
    }

    public int read() {
        int r1;
        long j = this.f7308;
        this.f7308 = j - 1;
        if (j <= 0) {
            return -1;
        }
        synchronized (this.f7307) {
            C1411 r12 = this.f7307;
            long j2 = this.f7309;
            this.f7309 = 1 + j2;
            r12.m7818(j2);
            r1 = this.f7307.m7816();
        }
        return r1;
    }

    public int available() {
        return (int) (this.f7308 & 2147483647L);
    }

    public int read(byte[] bArr, int i, int i2) {
        int r6;
        long j = this.f7308;
        if (j <= 0) {
            return -1;
        }
        if (i2 <= 0) {
            return 0;
        }
        if (((long) i2) > j) {
            i2 = (int) j;
        }
        synchronized (this.f7307) {
            this.f7307.m7818(this.f7309);
            r6 = this.f7307.m7821(bArr, i, i2);
        }
        if (r6 > 0) {
            long j2 = (long) r6;
            this.f7309 += j2;
            this.f7308 -= j2;
        }
        return r6;
    }
}
