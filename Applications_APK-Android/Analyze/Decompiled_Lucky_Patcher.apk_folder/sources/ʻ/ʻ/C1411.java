package ʻ.ʻ;

import java.io.EOFException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import net.lingala.zip4j.util.InternalZipConstants;

/* renamed from: ʻ.ʻ.ʼ  reason: contains not printable characters */
/* compiled from: BufferedRandomAccessFile */
public final class C1411 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f7310;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f7311;

    /* renamed from: ʽ  reason: contains not printable characters */
    private long f7312;

    /* renamed from: ʾ  reason: contains not printable characters */
    private long f7313;

    /* renamed from: ʿ  reason: contains not printable characters */
    private long f7314;

    /* renamed from: ˆ  reason: contains not printable characters */
    private byte[] f7315;

    /* renamed from: ˈ  reason: contains not printable characters */
    private long f7316;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f7317;

    /* renamed from: ˊ  reason: contains not printable characters */
    private long f7318;

    /* renamed from: ˋ  reason: contains not printable characters */
    private RandomAccessFile f7319;

    C1411(RandomAccessFile randomAccessFile) {
        this.f7319 = randomAccessFile;
        m7813();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private void m7813() {
        this.f7311 = false;
        this.f7310 = false;
        this.f7314 = 0;
        this.f7312 = 0;
        this.f7313 = 0;
        this.f7315 = new byte[InternalZipConstants.MIN_SPLIT_LENGTH];
        this.f7316 = 65536;
        this.f7317 = false;
        this.f7318 = 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7817(int i) {
        long j = this.f7312;
        long j2 = this.f7314;
        if (j >= j2) {
            if (!this.f7317 || j2 >= this.f7316) {
                m7818(this.f7312);
                long j3 = this.f7312;
                long j4 = this.f7314;
                if (j3 == j4) {
                    this.f7314 = j4 + 1;
                }
            } else {
                this.f7314 = j2 + 1;
            }
        }
        byte[] bArr = this.f7315;
        long j5 = this.f7312;
        bArr[(int) (j5 - this.f7313)] = (byte) i;
        this.f7312 = j5 + 1;
        this.f7310 = true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7819(byte[] bArr) {
        m7820(bArr, 0, bArr.length);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7820(byte[] bArr, int i, int i2) {
        while (i2 > 0) {
            int r0 = m7812(bArr, i, i2);
            i += r0;
            i2 -= r0;
            this.f7310 = true;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m7816() {
        long j = this.f7312;
        if (j >= this.f7314) {
            if (this.f7317) {
                return -1;
            }
            m7818(j);
            if (this.f7312 == this.f7314) {
                return -1;
            }
        }
        byte[] bArr = this.f7315;
        long j2 = this.f7312;
        byte b = bArr[(int) (j2 - this.f7313)];
        this.f7312 = j2 + 1;
        return b & 255;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m7821(byte[] bArr, int i, int i2) {
        long j = this.f7312;
        if (j >= this.f7314) {
            if (this.f7317) {
                return -1;
            }
            m7818(j);
            if (this.f7312 == this.f7314) {
                return -1;
            }
        }
        int min = Math.min(i2, (int) (this.f7314 - this.f7312));
        System.arraycopy(this.f7315, (int) (this.f7312 - this.f7313), bArr, i, min);
        this.f7312 += (long) min;
        return min;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m7823(byte[] bArr) {
        m7825(bArr, 0, bArr.length);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m7825(byte[] bArr, int i, int i2) {
        int i3 = 0;
        do {
            int r1 = m7821(bArr, i + i3, i2 - i3);
            if (r1 >= 0) {
                i3 += r1;
            } else {
                throw new EOFException();
            }
        } while (i3 < i2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public long m7822() {
        return Math.max(this.f7312, this.f7319.length());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7818(long j) {
        if (j >= this.f7314 || j < this.f7313) {
            m7814();
            this.f7313 = -65536 & j;
            long j2 = this.f7313;
            this.f7316 = ((long) this.f7315.length) + j2;
            if (this.f7318 != j2) {
                this.f7319.seek(j2);
                this.f7318 = this.f7313;
            }
            this.f7314 = this.f7313 + ((long) m7815());
        } else if (j < this.f7312) {
            m7814();
        }
        this.f7312 = j;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public long m7824() {
        return this.f7312;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m7826() {
        m7814();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m7827() {
        m7826();
        this.f7311 = true;
        this.f7319.close();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m7828() {
        return this.f7311;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private void m7814() {
        if (this.f7310) {
            long j = this.f7318;
            long j2 = this.f7313;
            if (j != j2) {
                this.f7319.seek(j2);
            }
            this.f7319.write(this.f7315, 0, (int) (this.f7312 - this.f7313));
            this.f7318 = this.f7312;
            this.f7310 = false;
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private int m7815() {
        int length = this.f7315.length;
        boolean z = false;
        int i = 0;
        while (length > 0) {
            int read = this.f7319.read(this.f7315, i, length);
            if (read < 0) {
                break;
            }
            i += read;
            length -= read;
        }
        if (i < this.f7315.length) {
            z = true;
        }
        this.f7317 = z;
        if (z) {
            byte[] bArr = this.f7315;
            Arrays.fill(bArr, i, bArr.length, (byte) -1);
        }
        this.f7318 += (long) i;
        return i;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private int m7812(byte[] bArr, int i, int i2) {
        long j = this.f7312;
        long j2 = this.f7314;
        if (j >= j2) {
            if (this.f7317) {
                long j3 = this.f7316;
                if (j2 < j3) {
                    this.f7314 = j3;
                }
            }
            m7818(this.f7312);
            if (this.f7312 == this.f7314) {
                this.f7314 = this.f7316;
            }
        }
        int min = Math.min(i2, (int) (this.f7314 - this.f7312));
        System.arraycopy(bArr, i, this.f7315, (int) (this.f7312 - this.f7313), min);
        this.f7312 += (long) min;
        return min;
    }
}
