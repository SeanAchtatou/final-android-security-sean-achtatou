package com.iab.omid.library.adcolony.adsession;

import com.iab.omid.library.adcolony.d.e;

public final class AdEvents {
    private final a a;

    private AdEvents(a aVar) {
        this.a = aVar;
    }

    public static AdEvents createAdEvents(AdSession adSession) {
        a aVar = (a) adSession;
        e.a(adSession, "AdSession is null");
        e.d(aVar);
        e.b(aVar);
        AdEvents adEvents = new AdEvents(aVar);
        aVar.getAdSessionStatePublisher().a(adEvents);
        return adEvents;
    }

    public void impressionOccurred() {
        e.b(this.a);
        e.f(this.a);
        if (!this.a.d()) {
            try {
                this.a.start();
            } catch (Exception unused) {
            }
        }
        if (this.a.d()) {
            this.a.b();
        }
    }
}
