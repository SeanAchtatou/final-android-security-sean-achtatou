package com.iab.omid.library.adcolony.adsession;

public enum ErrorType {
    GENERIC("generic"),
    VIDEO("video");
    
    private final String a;

    private ErrorType(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
