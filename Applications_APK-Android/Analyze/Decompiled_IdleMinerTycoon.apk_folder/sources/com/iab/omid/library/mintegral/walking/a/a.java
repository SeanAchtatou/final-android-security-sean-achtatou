package com.iab.omid.library.mintegral.walking.a;

import com.iab.omid.library.mintegral.walking.a.b;
import java.util.HashSet;
import org.json.JSONObject;

public abstract class a extends b {
    protected final HashSet<String> a;
    protected final JSONObject b;
    protected final double c;

    public a(b.C0048b bVar, HashSet<String> hashSet, JSONObject jSONObject, double d) {
        super(bVar);
        this.a = new HashSet<>(hashSet);
        this.b = jSONObject;
        this.c = d;
    }
}
