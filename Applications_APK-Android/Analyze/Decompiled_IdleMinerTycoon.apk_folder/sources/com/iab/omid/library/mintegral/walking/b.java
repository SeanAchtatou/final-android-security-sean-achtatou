package com.iab.omid.library.mintegral.walking;

import android.support.annotation.VisibleForTesting;
import com.iab.omid.library.mintegral.walking.a.b;
import com.iab.omid.library.mintegral.walking.a.c;
import com.iab.omid.library.mintegral.walking.a.d;
import com.iab.omid.library.mintegral.walking.a.e;
import com.iab.omid.library.mintegral.walking.a.f;
import java.util.HashSet;
import org.json.JSONObject;

public class b implements b.C0048b {
    private JSONObject a;
    private final c b;

    public b(c cVar) {
        this.b = cVar;
    }

    public void a() {
        this.b.b(new d(this));
    }

    @VisibleForTesting
    public void a(JSONObject jSONObject) {
        this.a = jSONObject;
    }

    public void a(JSONObject jSONObject, HashSet<String> hashSet, double d) {
        this.b.b(new f(this, hashSet, jSONObject, d));
    }

    @VisibleForTesting
    public JSONObject b() {
        return this.a;
    }

    public void b(JSONObject jSONObject, HashSet<String> hashSet, double d) {
        this.b.b(new e(this, hashSet, jSONObject, d));
    }
}
