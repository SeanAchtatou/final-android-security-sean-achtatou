package com.iab.omid.library.adcolony.publisher;

import android.webkit.WebView;
import com.iab.omid.library.adcolony.adsession.AdEvents;
import com.iab.omid.library.adcolony.adsession.AdSessionConfiguration;
import com.iab.omid.library.adcolony.adsession.AdSessionContext;
import com.iab.omid.library.adcolony.adsession.ErrorType;
import com.iab.omid.library.adcolony.adsession.VerificationScriptResource;
import com.iab.omid.library.adcolony.adsession.video.VideoEvents;
import com.iab.omid.library.adcolony.b.c;
import com.iab.omid.library.adcolony.b.d;
import com.iab.omid.library.adcolony.e.b;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class AdSessionStatePublisher {
    private b a = new b(null);
    private AdEvents b;
    private VideoEvents c;
    private a d;
    private long e;

    enum a {
        AD_STATE_IDLE,
        AD_STATE_VISIBLE,
        AD_STATE_NOTVISIBLE
    }

    public AdSessionStatePublisher() {
        h();
    }

    public void a() {
    }

    public void a(float f) {
        d.a().a(getWebView(), f);
    }

    /* access modifiers changed from: package-private */
    public void a(WebView webView) {
        this.a = new b(webView);
    }

    public void a(AdEvents adEvents) {
        this.b = adEvents;
    }

    public void a(AdSessionConfiguration adSessionConfiguration) {
        d.a().a(getWebView(), adSessionConfiguration.toJsonObject());
    }

    public void a(ErrorType errorType, String str) {
        d.a().a(getWebView(), errorType, str);
    }

    public void a(com.iab.omid.library.adcolony.adsession.a aVar, AdSessionContext adSessionContext) {
        String adSessionId = aVar.getAdSessionId();
        JSONObject jSONObject = new JSONObject();
        com.iab.omid.library.adcolony.d.b.a(jSONObject, "environment", "app");
        com.iab.omid.library.adcolony.d.b.a(jSONObject, "adSessionType", adSessionContext.getAdSessionContextType());
        com.iab.omid.library.adcolony.d.b.a(jSONObject, "deviceInfo", com.iab.omid.library.adcolony.d.a.d());
        JSONArray jSONArray = new JSONArray();
        jSONArray.put("clid");
        jSONArray.put("vlid");
        com.iab.omid.library.adcolony.d.b.a(jSONObject, "supports", jSONArray);
        JSONObject jSONObject2 = new JSONObject();
        com.iab.omid.library.adcolony.d.b.a(jSONObject2, "partnerName", adSessionContext.getPartner().getName());
        com.iab.omid.library.adcolony.d.b.a(jSONObject2, "partnerVersion", adSessionContext.getPartner().getVersion());
        com.iab.omid.library.adcolony.d.b.a(jSONObject, "omidNativeInfo", jSONObject2);
        JSONObject jSONObject3 = new JSONObject();
        com.iab.omid.library.adcolony.d.b.a(jSONObject3, "libraryVersion", "1.2.20-Adcolony");
        com.iab.omid.library.adcolony.d.b.a(jSONObject3, "appId", c.a().b().getApplicationContext().getPackageName());
        com.iab.omid.library.adcolony.d.b.a(jSONObject, "app", jSONObject3);
        if (adSessionContext.getCustomReferenceData() != null) {
            com.iab.omid.library.adcolony.d.b.a(jSONObject, "customReferenceData", adSessionContext.getCustomReferenceData());
        }
        JSONObject jSONObject4 = new JSONObject();
        for (VerificationScriptResource next : adSessionContext.getVerificationScriptResources()) {
            com.iab.omid.library.adcolony.d.b.a(jSONObject4, next.getVendorKey(), next.getVerificationParameters());
        }
        d.a().a(getWebView(), adSessionId, jSONObject, jSONObject4);
    }

    public void a(VideoEvents videoEvents) {
        this.c = videoEvents;
    }

    public void a(String str) {
        d.a().a(getWebView(), str, (JSONObject) null);
    }

    public void a(String str, long j) {
        if (j >= this.e) {
            this.d = a.AD_STATE_VISIBLE;
            d.a().c(getWebView(), str);
        }
    }

    public void a(String str, JSONObject jSONObject) {
        d.a().a(getWebView(), str, jSONObject);
    }

    public void a(boolean z) {
        if (e()) {
            d.a().d(getWebView(), z ? "foregrounded" : "backgrounded");
        }
    }

    public void b() {
        this.a.clear();
    }

    public void b(String str, long j) {
        if (j >= this.e && this.d != a.AD_STATE_NOTVISIBLE) {
            this.d = a.AD_STATE_NOTVISIBLE;
            d.a().c(getWebView(), str);
        }
    }

    public AdEvents c() {
        return this.b;
    }

    public VideoEvents d() {
        return this.c;
    }

    public boolean e() {
        return this.a.get() != null;
    }

    public void f() {
        d.a().a(getWebView());
    }

    public void g() {
        d.a().b(getWebView());
    }

    public WebView getWebView() {
        return (WebView) this.a.get();
    }

    public void h() {
        this.e = com.iab.omid.library.adcolony.d.d.a();
        this.d = a.AD_STATE_IDLE;
    }
}
