package com.iab.omid.library.mintegral.adsession.video;

import com.applovin.mediation.unity.BuildConfig;
import com.vungle.warren.model.Advertisement;

public enum Position {
    PREROLL("preroll"),
    MIDROLL("midroll"),
    POSTROLL(Advertisement.KEY_POSTROLL),
    STANDALONE(BuildConfig.FLAVOR);
    
    private final String position;

    private Position(String str) {
        this.position = str;
    }

    public String toString() {
        return this.position;
    }
}
