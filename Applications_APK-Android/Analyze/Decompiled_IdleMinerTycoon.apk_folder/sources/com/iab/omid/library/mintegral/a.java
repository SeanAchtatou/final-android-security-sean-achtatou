package com.iab.omid.library.mintegral;

import android.content.Context;
import com.iab.omid.library.mintegral.b.b;
import com.iab.omid.library.mintegral.b.c;
import com.iab.omid.library.mintegral.d.e;

public class a {
    private boolean a;

    private void b(Context context) {
        e.a(context, "Application Context cannot be null");
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return "1.2.15-Mintegral";
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        b(context);
        if (!b()) {
            a(true);
            com.iab.omid.library.mintegral.b.e.a().a(context);
            b.a().a(context);
            com.iab.omid.library.mintegral.d.b.a(context);
            c.a().a(context);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.a = z;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str) {
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.a;
    }
}
