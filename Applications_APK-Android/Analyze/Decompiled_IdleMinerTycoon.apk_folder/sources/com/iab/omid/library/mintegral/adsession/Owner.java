package com.iab.omid.library.mintegral.adsession;

import com.ironsource.sdk.constants.Constants;

public enum Owner {
    NATIVE("native"),
    JAVASCRIPT("javascript"),
    NONE(Constants.ParametersKeys.ORIENTATION_NONE);
    
    private final String owner;

    private Owner(String str) {
        this.owner = str;
    }

    public String toString() {
        return this.owner;
    }
}
