package com.iab.omid.library.mintegral.adsession;

import android.webkit.WebView;
import com.iab.omid.library.mintegral.d.e;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class AdSessionContext {
    private final AdSessionContextType adSessionContextType;
    private final String customReferenceData;
    private final String omidJsScriptContent;
    private final Partner partner;
    private final List<VerificationScriptResource> verificationScriptResources = new ArrayList();
    private final WebView webView;

    private AdSessionContext(Partner partner2, WebView webView2, String str, List<VerificationScriptResource> list, String str2) {
        AdSessionContextType adSessionContextType2;
        this.partner = partner2;
        this.webView = webView2;
        this.omidJsScriptContent = str;
        if (list != null) {
            this.verificationScriptResources.addAll(list);
            adSessionContextType2 = AdSessionContextType.NATIVE;
        } else {
            adSessionContextType2 = AdSessionContextType.HTML;
        }
        this.adSessionContextType = adSessionContextType2;
        this.customReferenceData = str2;
    }

    public static AdSessionContext createHtmlAdSessionContext(Partner partner2, WebView webView2, String str) {
        e.a(partner2, "Partner is null");
        e.a(webView2, "WebView is null");
        if (str != null) {
            e.a(str, 256, "CustomReferenceData is greater than 256 characters");
        }
        return new AdSessionContext(partner2, webView2, null, null, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.iab.omid.library.mintegral.d.e.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.iab.omid.library.mintegral.d.e.a(java.lang.String, java.lang.String):void
      com.iab.omid.library.mintegral.d.e.a(java.lang.Object, java.lang.String):void */
    public static AdSessionContext createNativeAdSessionContext(Partner partner2, String str, List<VerificationScriptResource> list, String str2) {
        e.a(partner2, "Partner is null");
        e.a((Object) str, "OM SDK JS script content is null");
        e.a(list, "VerificationScriptResources is null");
        if (str2 != null) {
            e.a(str2, 256, "CustomReferenceData is greater than 256 characters");
        }
        return new AdSessionContext(partner2, null, str, list, str2);
    }

    public AdSessionContextType getAdSessionContextType() {
        return this.adSessionContextType;
    }

    public String getCustomReferenceData() {
        return this.customReferenceData;
    }

    public String getOmidJsScriptContent() {
        return this.omidJsScriptContent;
    }

    public Partner getPartner() {
        return this.partner;
    }

    public List<VerificationScriptResource> getVerificationScriptResources() {
        return Collections.unmodifiableList(this.verificationScriptResources);
    }

    public WebView getWebView() {
        return this.webView;
    }
}
