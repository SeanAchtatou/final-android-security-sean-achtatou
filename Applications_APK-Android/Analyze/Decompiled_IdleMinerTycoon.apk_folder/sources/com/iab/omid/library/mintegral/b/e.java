package com.iab.omid.library.mintegral.b;

import android.content.Context;
import android.os.Handler;
import com.iab.omid.library.mintegral.a.c;
import com.iab.omid.library.mintegral.a.d;
import com.iab.omid.library.mintegral.adsession.a;
import com.iab.omid.library.mintegral.b.b;
import com.iab.omid.library.mintegral.walking.TreeWalker;

public class e implements c, b.a {
    private static e a;
    private float b = 0.0f;
    private final com.iab.omid.library.mintegral.a.e c;
    private final com.iab.omid.library.mintegral.a.b d;
    private d e;
    private a f;

    public e(com.iab.omid.library.mintegral.a.e eVar, com.iab.omid.library.mintegral.a.b bVar) {
        this.c = eVar;
        this.d = bVar;
    }

    public static e a() {
        if (a == null) {
            a = new e(new com.iab.omid.library.mintegral.a.e(), new com.iab.omid.library.mintegral.a.b());
        }
        return a;
    }

    private a e() {
        if (this.f == null) {
            this.f = a.a();
        }
        return this.f;
    }

    public void a(float f2) {
        this.b = f2;
        for (a adSessionStatePublisher : e().c()) {
            adSessionStatePublisher.getAdSessionStatePublisher().a(f2);
        }
    }

    public void a(Context context) {
        this.e = this.c.a(new Handler(), context, this.d.a(), this);
    }

    public void a(boolean z) {
        if (z) {
            TreeWalker.getInstance().a();
        } else {
            TreeWalker.getInstance().c();
        }
    }

    public void b() {
        b.a().a(this);
        b.a().b();
        if (b.a().d()) {
            TreeWalker.getInstance().a();
        }
        this.e.a();
    }

    public void c() {
        TreeWalker.getInstance().b();
        b.a().c();
        this.e.b();
    }

    public float d() {
        return this.b;
    }
}
