package com.iab.omid.library.adcolony.adsession.video;

public enum PlayerState {
    MINIMIZED("minimized"),
    COLLAPSED("collapsed"),
    NORMAL("normal"),
    EXPANDED("expanded"),
    FULLSCREEN("fullscreen");
    
    private final String a;

    private PlayerState(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
