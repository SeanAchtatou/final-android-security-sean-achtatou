package com.iab.omid.library.adcolony.adsession;

import com.tapjoy.TJAdUnitConstants;

public enum AdSessionContextType {
    HTML(TJAdUnitConstants.String.HTML),
    NATIVE("native");
    
    private final String a;

    private AdSessionContextType(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
