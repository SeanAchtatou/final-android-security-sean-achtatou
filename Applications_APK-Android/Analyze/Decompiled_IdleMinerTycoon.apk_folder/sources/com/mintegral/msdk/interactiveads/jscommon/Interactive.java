package com.mintegral.msdk.interactiveads.jscommon;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Base64;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.d.b;
import com.mintegral.msdk.interactiveads.activity.InteractiveShowActivity;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.mtgjscommon.windvane.i;
import com.tapjoy.TapjoyConstants;
import im.getsocial.sdk.consts.LanguageCodes;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Interactive extends i {
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void init(final java.lang.Object r6, java.lang.String r7) {
        /*
            r5 = this;
            java.lang.String r0 = "Interactive"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "INIT:"
            r1.<init>(r2)
            r1.append(r7)
            java.lang.String r1 = r1.toString()
            com.mintegral.msdk.base.utils.g.a(r0, r1)
            org.json.JSONArray r0 = new org.json.JSONArray
            r0.<init>()
            boolean r1 = android.text.TextUtils.isEmpty(r7)
            if (r1 == 0) goto L_0x0025
            java.lang.String r1 = "Interactive"
            java.lang.String r2 = "init params is null or empty"
            com.mintegral.msdk.base.utils.g.d(r1, r2)
        L_0x0025:
            r1 = 1
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x003b }
            r2.<init>(r7)     // Catch:{ JSONException -> 0x003b }
            java.lang.String r3 = "pageNo"
            int r3 = r2.optInt(r3)     // Catch:{ JSONException -> 0x003b }
            java.lang.String r4 = "exclude_ids"
            org.json.JSONArray r2 = r2.optJSONArray(r4)     // Catch:{ JSONException -> 0x0039 }
            r0 = r2
            goto L_0x0040
        L_0x0039:
            r2 = move-exception
            goto L_0x003d
        L_0x003b:
            r2 = move-exception
            r3 = 1
        L_0x003d:
            r2.printStackTrace()
        L_0x0040:
            if (r6 == 0) goto L_0x00ac
            r2 = r6
            com.mintegral.msdk.mtgjscommon.windvane.a r2 = (com.mintegral.msdk.mtgjscommon.windvane.a) r2
            com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView r4 = r2.a
            java.lang.Object r4 = r4.getObject()
            boolean r4 = r4 instanceof com.mintegral.msdk.interactiveads.jscommon.a
            if (r4 == 0) goto L_0x00ac
            com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView r2 = r2.a
            java.lang.Object r2 = r2.getObject()
            com.mintegral.msdk.interactiveads.jscommon.a r2 = (com.mintegral.msdk.interactiveads.jscommon.a) r2
            if (r3 != r1) goto L_0x0080
            java.lang.String r7 = r2.b()
            java.lang.String r0 = "Interactive"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "INIT json:"
            r1.<init>(r2)
            r1.append(r7)
            java.lang.String r1 = r1.toString()
            com.mintegral.msdk.base.utils.g.a(r0, r1)
            byte[] r7 = r7.getBytes()
            r0 = 2
            java.lang.String r7 = android.util.Base64.encodeToString(r7, r0)
            com.mintegral.msdk.mtgjscommon.windvane.g.a()
            com.mintegral.msdk.mtgjscommon.windvane.g.a(r6, r7)
            return
        L_0x0080:
            if (r3 <= r1) goto L_0x0099
            if (r2 == 0) goto L_0x00ac
            android.app.Activity r7 = r2.d()
            if (r7 == 0) goto L_0x0098
            boolean r1 = r7 instanceof com.mintegral.msdk.interactiveads.activity.InteractiveShowActivity
            if (r1 == 0) goto L_0x0098
            com.mintegral.msdk.interactiveads.activity.InteractiveShowActivity r7 = (com.mintegral.msdk.interactiveads.activity.InteractiveShowActivity) r7
            com.mintegral.msdk.interactiveads.jscommon.Interactive$1 r1 = new com.mintegral.msdk.interactiveads.jscommon.Interactive$1
            r1.<init>(r2, r6)
            r7.webviewLoadMore(r3, r0, r1)
        L_0x0098:
            return
        L_0x0099:
            java.lang.String r6 = "Interactive"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "INIT error:"
            r0.<init>(r1)
            r0.append(r7)
            java.lang.String r7 = r0.toString()
            com.mintegral.msdk.base.utils.g.a(r6, r7)
        L_0x00ac:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.interactiveads.jscommon.Interactive.init(java.lang.Object, java.lang.String):void");
    }

    public void readyStatus(Object obj, String str) {
        try {
            g.a("Interactive", "readyStatus:" + str);
            if (obj != null) {
                com.mintegral.msdk.mtgjscommon.windvane.a aVar = (com.mintegral.msdk.mtgjscommon.windvane.a) obj;
                if (aVar.a.getObject() instanceof a) {
                    int optInt = new JSONObject(str).optInt("isReady", 1);
                    ((a) aVar.a.getObject()).a(optInt);
                    if (aVar.a instanceof WindVaneWebView) {
                        WindVaneWebView windVaneWebView = aVar.a;
                        if (windVaneWebView.getWebViewListener() != null) {
                            windVaneWebView.getWebViewListener().a(optInt);
                        }
                    }
                }
            }
        } catch (Throwable th) {
            g.c("Interactive", th.getMessage(), th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, ?[OBJECT, ARRAY], java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    public void reportUrls(Object obj, String str) {
        g.a("Interactive", "reportUrls:" + str);
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONArray jSONArray = new JSONArray(str);
                for (int i = 0; i < jSONArray.length(); i++) {
                    com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), (CampaignEx) null, "", jSONArray.getJSONObject(i).optString("url"), false, true);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendImpressions(Object obj, String str) {
        InteractiveShowActivity a2;
        g.a("Interactive", "sendImpressions:" + str);
        try {
            if (!TextUtils.isEmpty(str) && (a2 = a(obj)) != null) {
                a2.sendImpressions(new JSONArray(str));
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void triggerCloseBtn(Object obj, String str) {
        g.a("Interactive", "triggerCloseBtn:" + str);
        InteractiveShowActivity a2 = a(obj);
        if (a2 != null) {
            try {
                switch (new JSONObject(str).optInt("type")) {
                    case 1:
                        a2.onInteractivePlayingComplete(true);
                        break;
                    case 2:
                        a2.onInteractivePlayingComplete(false);
                        break;
                    default:
                        a2.onInteractivePlayingComplete(true);
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (InteractiveShowActivity.interactiveStatusListener != null) {
                InteractiveShowActivity.interactiveStatusListener.e();
            }
            a2.tryReportADTrackingImpression();
            a2.tryReportPlayableClosed();
            a2.finish();
        }
    }

    public void toggleCloseBtn(Object obj, String str) {
        g.a("Interactive", "toggleCloseBtn:" + str);
        if (!TextUtils.isEmpty(str)) {
            try {
                int optInt = new JSONObject(str).optInt("state");
                InteractiveShowActivity a2 = a(obj);
                switch (optInt) {
                    case 1:
                        if (a2 != null) {
                            a2.setCloseButtonVisible(true);
                            a2.isADShowing = true;
                            return;
                        }
                        return;
                    case 2:
                        if (a2 != null) {
                            a2.setCloseButtonVisible(false);
                            a2.isADShowing = false;
                            return;
                        }
                        return;
                    default:
                        return;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void getEndScreenInfo(Object obj, String str) {
        if (obj != null) {
            try {
                com.mintegral.msdk.mtgjscommon.windvane.a aVar = (com.mintegral.msdk.mtgjscommon.windvane.a) obj;
                if (aVar.a.getObject() instanceof a) {
                    a aVar2 = (a) aVar.a.getObject();
                    List arrayList = new ArrayList();
                    String str2 = "";
                    JSONObject jSONObject = new JSONObject();
                    if (aVar2 != null) {
                        arrayList = aVar2.c();
                        str2 = aVar2.a();
                        jSONObject = aVar2.e();
                    }
                    String a2 = a(arrayList, str2, "MAL_10.1.51,3.0.1", jSONObject);
                    String encodeToString = !TextUtils.isEmpty(a2) ? Base64.encodeToString(a2.getBytes(), 2) : "";
                    com.mintegral.msdk.mtgjscommon.windvane.g.a();
                    com.mintegral.msdk.mtgjscommon.windvane.g.a(aVar, encodeToString);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public void click(Object obj, String str) {
        InteractiveShowActivity a2;
        g.a("Interactive", "click:" + str);
        try {
            if (!TextUtils.isEmpty(str) && (a2 = a(obj)) != null) {
                JSONObject jSONObject = new JSONObject(str);
                a2.click(jSONObject.optInt("type"), CampaignEx.parseShortCutsCampaign(jSONObject.getJSONObject(LanguageCodes.PORTUGUESE)));
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void handlerPlayableException(Object obj, String str) {
        try {
            InteractiveShowActivity a2 = a(obj);
            if (a2 != null && !TextUtils.isEmpty(str)) {
                String optString = new JSONObject(str).optString(NotificationCompat.CATEGORY_MESSAGE);
                g.a("Interactive", "handlerPlayableException,msg:" + str);
                a2.handlerPlayableException(optString);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void showAlertView(Object obj, String str) {
        InteractiveShowActivity a2;
        g.a("Interactive", "showAlertView:" + str);
        try {
            if (!TextUtils.isEmpty(str) && (a2 = a(obj)) != null) {
                a2.showAlertView();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private static InteractiveShowActivity a(Object obj) {
        a aVar;
        Activity d;
        if (obj != null) {
            com.mintegral.msdk.mtgjscommon.windvane.a aVar2 = (com.mintegral.msdk.mtgjscommon.windvane.a) obj;
            if ((aVar2.a.getObject() instanceof a) && (aVar = (a) aVar2.a.getObject()) != null && (d = aVar.d()) != null && (d instanceof InteractiveShowActivity)) {
                return (InteractiveShowActivity) d;
            }
        }
        return null;
    }

    public void install(Object obj, String str) {
        InteractiveShowActivity a2;
        g.a("Interactive", "install:" + str);
        try {
            if (!TextUtils.isEmpty(str) && (a2 = a(obj)) != null) {
                a2.clickPlayer();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private static String a(List<CampaignEx> list, String str, String str2, JSONObject jSONObject) {
        if (list == null) {
            return null;
        }
        try {
            if (list.size() <= 0) {
                return null;
            }
            JSONArray parseCamplistToJson = CampaignEx.parseCamplistToJson(list);
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("campaignList", parseCamplistToJson);
            jSONObject2.put(MIntegralConstans.PROPERTIES_UNIT_ID, str);
            jSONObject2.put("sdk_info", str2);
            jSONObject2.put("unitSetting", jSONObject);
            if (b.a() != null) {
                b.a();
                String c = b.c(com.mintegral.msdk.base.controller.a.d().j());
                if (!TextUtils.isEmpty(c)) {
                    jSONObject2.put("appSetting", new JSONObject(c));
                }
            }
            jSONObject2.put("device", new a(com.mintegral.msdk.base.controller.a.d().h()).a());
            return jSONObject2.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static final class a {
        public String a = c.d();
        public String b = c.i();
        public String c = "android";
        public String d;
        public String e;
        public String f;
        public String g;
        public String h;
        public String i;
        public String j;
        public String k;
        public String l;
        public String m;
        public String n;
        public String o;
        public String p;

        public a(Context context) {
            this.d = c.c(context);
            this.e = c.f(context);
            this.f = c.k();
            int s = c.s(context);
            this.h = String.valueOf(s);
            this.i = c.a(context, s);
            this.j = c.r(context);
            this.k = com.mintegral.msdk.base.controller.a.d().k();
            this.l = com.mintegral.msdk.base.controller.a.d().j();
            this.m = String.valueOf(k.i(context));
            this.n = String.valueOf(k.h(context));
            this.p = String.valueOf(k.c(context));
            if (context.getResources().getConfiguration().orientation == 2) {
                this.o = "landscape";
            } else {
                this.o = "portrait";
            }
        }

        public final JSONObject a() {
            JSONObject jSONObject = new JSONObject();
            try {
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                    jSONObject.put("device", this.a);
                    jSONObject.put("system_version", this.b);
                    jSONObject.put("network_type", this.h);
                    jSONObject.put("network_type_str", this.i);
                    jSONObject.put("device_ua", this.j);
                }
                jSONObject.put("plantform", this.c);
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITYIMEIMAC)) {
                    jSONObject.put("device_imei", this.d);
                }
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_ANDROID_ID)) {
                    jSONObject.put(TapjoyConstants.TJC_ANDROID_ID, this.e);
                }
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
                    jSONObject.put("google_ad_id", this.f);
                    jSONObject.put("oaid", this.g);
                }
                jSONObject.put("appkey", this.k);
                jSONObject.put("appId", this.l);
                jSONObject.put("screen_width", this.m);
                jSONObject.put("screen_height", this.n);
                jSONObject.put("orientation", this.o);
                jSONObject.put("scale", this.p);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            return jSONObject;
        }
    }
}
