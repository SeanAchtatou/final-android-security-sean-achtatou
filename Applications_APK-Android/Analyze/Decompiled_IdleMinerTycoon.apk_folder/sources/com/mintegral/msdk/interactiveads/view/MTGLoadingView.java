package com.mintegral.msdk.interactiveads.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.mintegral.msdk.base.utils.p;

public class MTGLoadingView extends FrameLayout {
    private MTGCircleView a;
    private MTGCircleView b;
    private MTGCircleView c;

    public MTGLoadingView(Context context, AttributeSet attributeSet) {
        super(context);
        View inflate = View.inflate(context, p.a(context, "mintegral_loading_view", "layout"), this);
        if (inflate != null) {
            this.a = (MTGCircleView) inflate.findViewById(p.a(context, "mintegral_left", "id"));
            this.b = (MTGCircleView) inflate.findViewById(p.a(context, "mintegral_middle", "id"));
            this.c = (MTGCircleView) inflate.findViewById(p.a(context, "mintegral_right", "id"));
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        if (i == 0) {
            if (this.a != null) {
                this.a.startAnimationDelay(0);
            }
            if (this.b != null) {
                this.b.startAnimationDelay(200);
            }
            if (this.c != null) {
                this.c.startAnimationDelay(400);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.a != null) {
            this.a.clearAnimation();
            this.a = null;
        }
        if (this.b != null) {
            this.b.clearAnimation();
            this.b = null;
        }
        if (this.c != null) {
            this.c.clearAnimation();
            this.c = null;
        }
    }
}
