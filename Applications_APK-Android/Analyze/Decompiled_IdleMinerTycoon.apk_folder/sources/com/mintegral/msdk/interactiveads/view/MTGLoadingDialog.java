package com.mintegral.msdk.interactiveads.view;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.p;
import com.mintegral.msdk.interactiveads.d.c;

public class MTGLoadingDialog extends AlertDialog {
    /* access modifiers changed from: private */
    public c a;

    public MTGLoadingDialog(Context context) {
        super(context);
    }

    public MTGLoadingDialog(Context context, c cVar) {
        super(context);
        this.a = cVar;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        getWindow().setFlags(1024, 1024);
        super.onCreate(bundle);
        setContentView(p.a(getContext(), "mintegral_loading_dialog", "layout"));
        getWindow().setBackgroundDrawable(new ColorDrawable(-1));
        getWindow().setLayout(-1, -1);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        ImageView imageView = (ImageView) findViewById(p.a(getContext(), "mintegral_close", "id"));
        if (imageView != null) {
            imageView.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    if (MTGLoadingDialog.this.a != null) {
                        MTGLoadingDialog.this.a.a();
                    } else {
                        g.a("MTGLoadingDialog", "MTGLoadingDialog LoadingDialogCloseListener is null");
                    }
                    MTGLoadingDialog.this.dismiss();
                }
            });
        }
    }
}
