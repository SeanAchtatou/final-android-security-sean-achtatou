package com.mintegral.msdk.interactiveads.c;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.Toast;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.d.c;
import com.mintegral.msdk.d.d;
import com.mintegral.msdk.interactiveads.activity.InteractiveShowActivity;
import com.mintegral.msdk.interactiveads.d.b;
import com.mintegral.msdk.interactiveads.out.InteractiveAdsListener;
import com.mintegral.msdk.interactiveads.view.MTGEntrancePictureView;
import com.mintegral.msdk.videocommon.download.j;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;

/* compiled from: InteractiveController */
public final class a implements b {
    public static String a = null;
    public static String c = null;
    public static boolean f = false;
    private static Map<String, Integer> p = new HashMap();
    public InteractiveAdsListener b;
    public int d = 0;
    public com.mintegral.msdk.interactiveads.a.a e;
    /* access modifiers changed from: private */
    public String g = "InteractiveController";
    private d h;
    /* access modifiers changed from: private */
    public Context i;
    private ViewGroup j;
    private String k;
    private MTGEntrancePictureView l;
    private boolean m = false;
    private Handler n;
    private CampaignEx o;

    public a() {
        try {
            this.n = new Handler(Looper.getMainLooper()) {
                /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v8, resolved type: java.lang.Object} */
                /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v8, resolved type: java.lang.String} */
                /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v14, resolved type: java.lang.Object} */
                /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v17, resolved type: java.lang.String} */
                /* JADX WARNING: Multi-variable type inference failed */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final void handleMessage(android.os.Message r4) {
                    /*
                        r3 = this;
                        if (r4 != 0) goto L_0x0003
                        return
                    L_0x0003:
                        int r0 = r4.what
                        switch(r0) {
                            case 1: goto L_0x00af;
                            case 2: goto L_0x0065;
                            case 3: goto L_0x0008;
                            case 4: goto L_0x0027;
                            case 5: goto L_0x0008;
                            case 6: goto L_0x0019;
                            case 7: goto L_0x000a;
                            default: goto L_0x0008;
                        }
                    L_0x0008:
                        goto L_0x00b0
                    L_0x000a:
                        com.mintegral.msdk.interactiveads.c.a r4 = com.mintegral.msdk.interactiveads.c.a.this
                        com.mintegral.msdk.interactiveads.out.InteractiveAdsListener r4 = r4.b
                        if (r4 == 0) goto L_0x00b0
                        com.mintegral.msdk.interactiveads.c.a r4 = com.mintegral.msdk.interactiveads.c.a.this
                        com.mintegral.msdk.interactiveads.out.InteractiveAdsListener r4 = r4.b
                        r4.onInteractiveClosed()
                        goto L_0x00b0
                    L_0x0019:
                        com.mintegral.msdk.interactiveads.c.a r4 = com.mintegral.msdk.interactiveads.c.a.this
                        com.mintegral.msdk.interactiveads.out.InteractiveAdsListener r4 = r4.b
                        if (r4 == 0) goto L_0x00b0
                        com.mintegral.msdk.interactiveads.c.a r4 = com.mintegral.msdk.interactiveads.c.a.this
                        com.mintegral.msdk.interactiveads.out.InteractiveAdsListener r4 = r4.b
                        r4.onInteractiveAdClick()
                        return
                    L_0x0027:
                        com.mintegral.msdk.interactiveads.c.a r0 = com.mintegral.msdk.interactiveads.c.a.this
                        com.mintegral.msdk.interactiveads.out.InteractiveAdsListener r0 = r0.b
                        if (r0 == 0) goto L_0x00b0
                        java.lang.String r0 = ""
                        java.lang.Object r1 = r4.obj
                        if (r1 == 0) goto L_0x003e
                        java.lang.Object r1 = r4.obj
                        boolean r1 = r1 instanceof java.lang.String
                        if (r1 == 0) goto L_0x003e
                        java.lang.Object r4 = r4.obj
                        r0 = r4
                        java.lang.String r0 = (java.lang.String) r0
                    L_0x003e:
                        boolean r4 = android.text.TextUtils.isEmpty(r0)
                        if (r4 == 0) goto L_0x0046
                        java.lang.String r0 = "can't show because unknow error"
                    L_0x0046:
                        com.mintegral.msdk.interactiveads.c.a r4 = com.mintegral.msdk.interactiveads.c.a.this
                        com.mintegral.msdk.interactiveads.out.InteractiveAdsListener r4 = r4.b
                        r4.onInteractiveShowFail(r0)
                        com.mintegral.msdk.interactiveads.c.a r4 = com.mintegral.msdk.interactiveads.c.a.this
                        java.lang.String r4 = r4.g
                        java.lang.StringBuilder r1 = new java.lang.StringBuilder
                        java.lang.String r2 = "handler 数据show失败:"
                        r1.<init>(r2)
                        r1.append(r0)
                        java.lang.String r0 = r1.toString()
                        com.mintegral.msdk.base.utils.g.b(r4, r0)
                        return
                    L_0x0065:
                        com.mintegral.msdk.interactiveads.c.a r0 = com.mintegral.msdk.interactiveads.c.a.this
                        com.mintegral.msdk.interactiveads.out.InteractiveAdsListener r0 = r0.b
                        if (r0 == 0) goto L_0x00b0
                        java.lang.String r0 = ""
                        java.lang.Object r1 = r4.obj
                        if (r1 == 0) goto L_0x007c
                        java.lang.Object r1 = r4.obj
                        boolean r1 = r1 instanceof java.lang.String
                        if (r1 == 0) goto L_0x007c
                        java.lang.Object r4 = r4.obj
                        r0 = r4
                        java.lang.String r0 = (java.lang.String) r0
                    L_0x007c:
                        boolean r4 = android.text.TextUtils.isEmpty(r0)
                        if (r4 == 0) goto L_0x0084
                        java.lang.String r0 = "can't show because unknow error"
                    L_0x0084:
                        com.mintegral.msdk.interactiveads.c.a r4 = com.mintegral.msdk.interactiveads.c.a.this
                        android.content.Context r4 = r4.i
                        java.lang.String r1 = com.mintegral.msdk.interactiveads.c.a.a
                        r2 = 0
                        com.mintegral.msdk.interactiveads.f.a.a(r4, r0, r1, r2)
                        com.mintegral.msdk.interactiveads.c.a r4 = com.mintegral.msdk.interactiveads.c.a.this
                        com.mintegral.msdk.interactiveads.out.InteractiveAdsListener r4 = r4.b
                        r4.onInteractiveLoadFail(r0)
                        com.mintegral.msdk.interactiveads.c.a r4 = com.mintegral.msdk.interactiveads.c.a.this
                        java.lang.String r4 = r4.g
                        java.lang.StringBuilder r1 = new java.lang.StringBuilder
                        java.lang.String r2 = "handler 数据load失败:"
                        r1.<init>(r2)
                        r1.append(r0)
                        java.lang.String r0 = r1.toString()
                        com.mintegral.msdk.base.utils.g.b(r4, r0)
                        return
                    L_0x00af:
                        return
                    L_0x00b0:
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.interactiveads.c.a.AnonymousClass1.handleMessage(android.os.Message):void");
                }
            };
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void a(String str, int i2) {
        try {
            if (p != null && s.b(str)) {
                p.put(str, Integer.valueOf(i2));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static int a(String str) {
        Integer num;
        try {
            if (!s.b(str) || p == null || !p.containsKey(str) || (num = p.get(str)) == null) {
                return 0;
            }
            return num.intValue();
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public final boolean a(Context context, Map<String, Object> map) {
        try {
            this.m = false;
            if (map == null) {
                g.c(this.g, "init error params==null");
                return false;
            } else if (context == null) {
                g.c(this.g, "init context ==null");
                return false;
            } else {
                if (map.containsKey(MIntegralConstans.PROPERTIES_UNIT_ID)) {
                    if (map.get(MIntegralConstans.PROPERTIES_UNIT_ID) instanceof String) {
                        String str = (String) map.get(MIntegralConstans.PROPERTIES_UNIT_ID);
                        a = str;
                        if (!TextUtils.isEmpty(str)) {
                            com.mintegral.msdk.base.a.a.a.a().a("interactive_unitid", a);
                        }
                        this.i = context;
                        this.m = true;
                        j.a().b();
                        com.mintegral.msdk.interactiveads.f.a.a(context, a);
                        return this.m;
                    }
                }
                g.c(this.g, "init error,make sure you have unitid");
                return false;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            this.m = false;
        }
    }

    public final boolean a(Context context, Map<String, Object> map, ViewGroup viewGroup, String str) {
        this.m = a(context, map);
        if (!(viewGroup == null || str == null)) {
            try {
                this.j = viewGroup;
                this.k = str;
                b();
            } catch (Exception e2) {
                e2.printStackTrace();
                this.m = false;
            }
        }
        return this.m;
    }

    public final void a(ViewGroup viewGroup) {
        this.j = viewGroup;
    }

    public final void b(String str) {
        this.k = str;
    }

    public final void a() {
        try {
            if (this.i == null) {
                e("context is null");
                g.b(this.g, "load context is null");
            } else if (TextUtils.isEmpty(a)) {
                e("unitid is null");
                g.b(this.g, "load unitid is null");
            } else if (!this.m) {
                e("init error");
                g.b(this.g, "load init error");
            } else {
                try {
                    new c().a(this.i, com.mintegral.msdk.base.controller.a.d().j(), com.mintegral.msdk.base.controller.a.d().k(), com.mintegral.msdk.base.a.a.a.a().a("interactive_unitid"));
                } catch (Exception e2) {
                    try {
                        e2.printStackTrace();
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                }
                com.mintegral.msdk.d.b.a();
                this.h = com.mintegral.msdk.d.b.c(com.mintegral.msdk.base.controller.a.d().j(), a);
                if (this.h == null) {
                    this.h = d.f(a);
                    g.b(this.g, "获取默认的unitsetting");
                }
                try {
                    if (this.e == null || !a.equals(this.e.b())) {
                        this.e = new com.mintegral.msdk.interactiveads.a.a(this.i, a);
                    }
                    this.e.a(this);
                    this.e.c();
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
            }
        } catch (Exception e5) {
            e5.printStackTrace();
            e("can't show because unknow error");
        }
    }

    public final void b() {
        if (this.i == null) {
            g.b(this.g, "refreshEntranceView error mContext == null");
        } else if (this.j == null) {
            g.b(this.g, "refreshEntranceView error container == null");
        } else if (this.k == null) {
            g.b(this.g, "refreshEntranceView error imageUrl == null");
        } else {
            if (this.l == null && this.i != null) {
                this.l = new MTGEntrancePictureView(this.i, null);
                InteractiveShowActivity.interactiveStatusListener = this;
            }
            this.l.setUnitId(a);
            this.l.refreshUI(this.i, this.k, this.o);
            this.j.removeAllViews();
            this.j.addView(this.l);
        }
    }

    public final void c() {
        f = true;
        if (com.mintegral.msdk.interactiveads.b.a.e) {
            Toast.makeText(this.i, "current page is showing", 0).show();
        } else if (this.o != null) {
            Intent intent = new Intent(this.i, InteractiveShowActivity.class);
            intent.putExtra("campaign", this.o);
            intent.putExtra("unitId", a);
            InteractiveShowActivity.interactiveStatusListener = this;
            this.i.startActivity(intent);
        } else {
            Intent intent2 = new Intent(this.i, InteractiveShowActivity.class);
            intent2.putExtra("unitId", a);
            InteractiveShowActivity.interactiveStatusListener = this;
            this.i.startActivity(intent2);
        }
    }

    private void e(String str) {
        try {
            if (this.n != null) {
                Message obtain = Message.obtain();
                obtain.obj = str;
                obtain.what = 2;
                this.n.sendMessage(obtain);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void a(List<CampaignEx> list) {
        if (list != null && list.size() > 0) {
            CampaignEx campaignEx = list.get(0);
            if (!TextUtils.isEmpty(campaignEx.getKeyIaUrl())) {
                if (this.j != null) {
                    String keyIaIcon = campaignEx.getKeyIaIcon();
                    if (this.i == null) {
                        g.b(this.g, "refreshEntranceView error mContext == null");
                    } else if (keyIaIcon == null) {
                        g.b(this.g, "refreshEntranceView error imageUrl == null");
                    } else {
                        this.k = keyIaIcon;
                        if (this.l != null) {
                            this.l.setUnitId(a);
                            this.l.refreshUI(this.i, keyIaIcon, campaignEx);
                        } else {
                            g.b(this.g, "refreshEntranceView failed entrancePictureView == null");
                        }
                    }
                    com.mintegral.msdk.interactiveads.f.a.a(this.i, campaignEx, 1, a);
                }
                if (this.b != null) {
                    this.b.onInteractivelLoadSuccess(campaignEx.getKeyIaRst());
                }
            } else if (this.b != null) {
                this.b.onInteractiveLoadFail("data is null");
                com.mintegral.msdk.interactiveads.f.a.a(this.i, "data is null", a, campaignEx);
            }
        } else if (this.b != null) {
            this.b.onInteractiveLoadFail("data is null");
            com.mintegral.msdk.interactiveads.f.a.a(this.i, "data is null", a, (CampaignEx) null);
        }
    }

    public final void a(CampaignEx campaignEx) {
        if (this.b != null) {
            this.b.onInterActiveMaterialLoadSuccess();
        }
        this.o = campaignEx;
        this.d = 1;
    }

    public final void d() {
        if (this.b != null) {
            this.b.onInteractiveShowSuccess();
        }
    }

    public final void c(String str) {
        this.d = -1;
        if (this.b != null) {
            this.b.onInteractiveShowFail(str);
        }
    }

    public final void d(String str) {
        if (this.b != null) {
            this.b.onInteractiveLoadFail(str);
        }
    }

    public final void a(JSONArray jSONArray, com.mintegral.msdk.interactiveads.d.d dVar) {
        com.mintegral.msdk.interactiveads.a.c.a(this.i).a = jSONArray;
        com.mintegral.msdk.interactiveads.a.c.a(this.i).a(true);
        p.put(a, 0);
        if (this.e == null) {
            this.e = new com.mintegral.msdk.interactiveads.a.a(this.i, a);
        }
        this.e.a(this);
        this.e.a("loadmore", dVar);
    }

    public final void a(int i2) {
        g.d("InteractiveController", "InteractiveController ZIP campaignTag.equals" + i2);
    }

    public final void e() {
        if (this.b != null) {
            this.b.onInteractiveClosed();
        }
    }

    public final void f() {
        if (this.b != null) {
            this.b.onInteractiveAdClick();
        }
    }

    public final void a(boolean z) {
        if (this.b != null) {
            this.b.onInteractivePlayingComplete(z);
        }
    }
}
