package com.mintegral.msdk.interactiveads.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.mintegral.msdk.base.utils.p;

public class MTGCircleView extends View {
    private Paint a = new Paint();
    private float b = 20.0f;
    private float c = 0.0f;
    private float d = 0.0f;
    private float e = 0.0f;
    private float f = 0.0f;
    /* access modifiers changed from: private */
    public Animation g;
    private Runnable h;

    public MTGCircleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a.setColor(-1);
        this.a.setAntiAlias(true);
        this.a.setDither(true);
        this.g = AnimationUtils.loadAnimation(context, p.a(context, "mintegral_anim_scale", "anim"));
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.e == 0.0f && this.f == 0.0f) {
            this.e = (float) getMeasuredWidth();
            this.f = (float) getMeasuredHeight();
            this.c = ((float) getLeft()) + (this.e / 2.0f);
            this.d = ((float) getTop()) + (this.f / 2.0f);
            this.b = this.e / 2.0f;
        }
    }

    public void startAnimationDelay(long j) {
        if (this.g != null) {
            if (this.h == null) {
                this.h = new Runnable() {
                    public final void run() {
                        MTGCircleView.this.g.startNow();
                    }
                };
            }
            this.g.reset();
            this.g.setStartOffset(200);
            setAnimation(this.g);
            postDelayed(this.h, j);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.a != null) {
            canvas.drawCircle(this.c, this.d, this.b, this.a);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.h != null) {
            removeCallbacks(this.h);
            this.h = null;
        }
        if (this.a != null) {
            this.a = null;
        }
        if (this.g != null) {
            this.g = null;
        }
    }
}
