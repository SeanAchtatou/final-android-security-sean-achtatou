package com.mintegral.msdk.interactiveads.out;

import android.content.Context;
import android.view.ViewGroup;
import com.mintegral.msdk.interactiveads.c.a;
import java.util.Map;

public class MTGInteractiveHandler {
    private static final String TAG = "MTGInteractiveHandler";
    private a mController;

    public MTGInteractiveHandler(Context context, Map<String, Object> map) {
        if (this.mController == null) {
            this.mController = new a();
        }
        this.mController.a(context, map);
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context);
        }
    }

    public MTGInteractiveHandler(Context context, Map<String, Object> map, ViewGroup viewGroup, String str) {
        if (this.mController == null) {
            this.mController = new a();
        }
        this.mController.a(context, map, viewGroup, str);
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context);
        }
    }

    public void setEntranceView(ViewGroup viewGroup, String str) {
        if (this.mController != null && str != null) {
            this.mController.a(viewGroup);
            this.mController.b(str);
            this.mController.b();
        }
    }

    public void load() {
        try {
            if (this.mController != null) {
                this.mController.a();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void show() {
        try {
            this.mController.c();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int isReady() {
        if (this.mController == null) {
            return -1;
        }
        a aVar = this.mController;
        if ((aVar.e == null || !aVar.e.a()) && aVar.e != null) {
            return aVar.d;
        }
        return 0;
    }

    public void setInteractiveAdsListener(InteractiveAdsListener interactiveAdsListener) {
        try {
            if (this.mController != null && interactiveAdsListener != null) {
                this.mController.b = interactiveAdsListener;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
