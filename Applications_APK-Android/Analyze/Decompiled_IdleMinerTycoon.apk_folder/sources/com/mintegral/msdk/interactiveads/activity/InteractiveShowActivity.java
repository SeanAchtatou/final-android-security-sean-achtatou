package com.mintegral.msdk.interactiveads.activity;

import android.app.Activity;
import android.content.Context;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.f;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.k;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.e;
import com.mintegral.msdk.base.entity.q;
import com.mintegral.msdk.base.utils.p;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.d.d;
import com.mintegral.msdk.interactiveads.d.b;
import com.mintegral.msdk.interactiveads.d.c;
import com.mintegral.msdk.interactiveads.view.MTGLoadingDialog;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.mtgjscommon.windvane.g;
import com.mintegral.msdk.videocommon.a;
import com.mintegral.msdk.videocommon.dialog.MTGAlertDialog;
import com.mintegral.msdk.videocommon.download.j;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONObject;

public class InteractiveShowActivity extends Activity implements com.mintegral.msdk.interactiveads.d.a {
    public static final int SHOW_INIT = 0;
    public static final int SHOW_NOT_ZIP = 1;
    public static String TAG = "InteractiveShowActivity";
    public static b interactiveStatusListener = null;
    /* access modifiers changed from: private */
    public static long o = 30000;
    FrameLayout a;
    FrameLayout.LayoutParams b;
    CampaignEx c;
    WindVaneWebView d;
    View e;
    ImageView f;
    Handler g = new Handler() {
        public final void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 0:
                    InteractiveShowActivity.this.init();
                    return;
                case 1:
                    List list = (List) message.obj;
                    InteractiveShowActivity.this.interactiveAdsAdapter.a(list, ((CampaignEx) list.get(0)).getKeyIaUrl(), true, true);
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public String h = "";
    private MTGLoadingDialog i;
    public com.mintegral.msdk.interactiveads.a.a interactiveAdsAdapter;
    public boolean isADShowing = false;
    private c j;
    private MTGAlertDialog k;
    private com.mintegral.msdk.videocommon.dialog.a l;
    private boolean m = false;
    private List<CampaignEx> n = new ArrayList(10);
    private CountDownTimer p = null;
    /* access modifiers changed from: private */
    public boolean q = false;
    /* access modifiers changed from: private */
    public boolean r = false;
    /* access modifiers changed from: private */
    public boolean s = false;
    private CountDownTimer t = null;

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        return i2 == 4;
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.t != null) {
            this.t.cancel();
            this.t = null;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.p != null) {
            this.p.cancel();
            this.p = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        com.mintegral.msdk.interactiveads.b.a.f.put(TAG, this);
        this.c = (CampaignEx) getIntent().getSerializableExtra("campaign");
        this.h = getIntent().getStringExtra("unitId");
        if (this.interactiveAdsAdapter == null) {
            this.interactiveAdsAdapter = new com.mintegral.msdk.interactiveads.a.a(this, this.h);
        }
        this.interactiveAdsAdapter.a(this);
        com.mintegral.msdk.interactiveads.b.a.e = true;
        this.a = new FrameLayout(this);
        this.b = new FrameLayout.LayoutParams(-1, -1);
        this.a.setBackgroundColor(-1);
        setContentView(this.a);
        String j2 = com.mintegral.msdk.base.controller.a.d().j();
        if (this.h == null) {
            this.h = com.mintegral.msdk.base.a.a.a.a().a("interactive_unitid");
        }
        com.mintegral.msdk.d.b.a();
        d c2 = com.mintegral.msdk.d.b.c(j2, this.h);
        if (c2 != null) {
            o = (long) (c2.o() * 1000);
        }
        this.e = LayoutInflater.from(this).inflate(p.a(this, "mintegral_close_imageview_layout", "layout"), (ViewGroup) null);
        if (this.e != null) {
            this.f = (ImageView) this.e.findViewById(p.a(this, "mintegral_close_imageview", "id"));
        }
        if (this.f != null) {
            this.f.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    if (InteractiveShowActivity.interactiveStatusListener != null) {
                        InteractiveShowActivity.interactiveStatusListener.e();
                    }
                    InteractiveShowActivity.this.onInteractivePlayingComplete(true);
                    if (InteractiveShowActivity.this.isADShowing) {
                        InteractiveShowActivity.this.tryReportADTrackingImpression();
                    }
                    InteractiveShowActivity.this.tryReportPlayableClosed();
                    InteractiveShowActivity.this.finish();
                }
            });
        }
        showLoadingDialog();
        if (this.p == null) {
            this.q = false;
            this.s = false;
            com.mintegral.msdk.videocommon.e.b.a();
            com.mintegral.msdk.videocommon.e.c a2 = com.mintegral.msdk.videocommon.e.b.a(com.mintegral.msdk.base.controller.a.d().j(), this.h);
            if (a2 != null && a2.l() > 0) {
                long l2 = (long) (a2.l() * 1000);
                if (l2 < o) {
                    o = l2;
                    this.s = true;
                }
            }
            this.p = new CountDownTimer(o) {
                public final void onTick(long j) {
                }

                public final void onFinish() {
                    if (InteractiveShowActivity.interactiveStatusListener != null) {
                        InteractiveShowActivity.interactiveStatusListener.c("showing is timeout");
                        new a(InteractiveShowActivity.this.c, false, InteractiveShowActivity.this, Constants.ParametersKeys.FAILED).start();
                    }
                    boolean unused = InteractiveShowActivity.this.q = true;
                    InteractiveShowActivity.this.init();
                    if (InteractiveShowActivity.this.c != null) {
                        q qVar = new q();
                        qVar.k(InteractiveShowActivity.this.c.getRequestIdNotice());
                        qVar.m(InteractiveShowActivity.this.c.getId());
                        if (InteractiveShowActivity.this.s) {
                            qVar.c(2);
                        } else {
                            qVar.c(3);
                        }
                        qVar.p(String.valueOf(InteractiveShowActivity.o));
                        qVar.f(InteractiveShowActivity.this.c.getKeyIaUrl());
                        String str = "2";
                        if (s.b(InteractiveShowActivity.this.c.getKeyIaUrl()) && InteractiveShowActivity.this.c.getKeyIaUrl().contains(".zip")) {
                            str = "1";
                        }
                        qVar.g(str);
                        qVar.o("time out");
                        qVar.a(String.valueOf(InteractiveShowActivity.this.c.getKeyIaRst()));
                        qVar.h("4");
                        qVar.a(InteractiveShowActivity.this.c.isMraid() ? q.a : q.b);
                        com.mintegral.msdk.base.common.e.a.c(qVar, InteractiveShowActivity.this.getApplicationContext(), InteractiveShowActivity.this.h);
                    }
                }
            }.start();
        } else {
            this.p.cancel();
            this.p.start();
        }
        this.h = getIntent().getStringExtra("unitId");
        new Thread(new Runnable() {
            public final void run() {
                List<CampaignEx> a2 = com.mintegral.msdk.interactiveads.a.b.a(InteractiveShowActivity.this).a(InteractiveShowActivity.this.h);
                List<CampaignEx> b = com.mintegral.msdk.interactiveads.a.b.a(InteractiveShowActivity.this).b(InteractiveShowActivity.this.h);
                if (a2.size() > 0) {
                    if (InteractiveShowActivity.a(a2)) {
                        com.mintegral.msdk.interactiveads.a.b.a(InteractiveShowActivity.this);
                        if (com.mintegral.msdk.interactiveads.a.b.a(a2)) {
                            InteractiveShowActivity.this.c = a2.get(0);
                            if (a2.get(0).getKeyIaUrl().contains("zip")) {
                                InteractiveShowActivity.this.interactiveAdsAdapter.a(a2, a2.get(0).getKeyIaUrl(), InteractiveShowActivity.this.h, null, true);
                            } else if (!com.mintegral.msdk.interactiveads.b.a.d) {
                                Message obtain = Message.obtain();
                                obtain.obj = a2;
                                obtain.what = 1;
                                InteractiveShowActivity.this.g.sendMessage(obtain);
                            }
                        }
                    }
                } else if (b.size() <= 0 || !InteractiveShowActivity.a(b)) {
                    InteractiveShowActivity.this.g.sendEmptyMessage(0);
                } else {
                    com.mintegral.msdk.interactiveads.a.b.a(InteractiveShowActivity.this);
                    if (com.mintegral.msdk.interactiveads.a.b.a(b)) {
                        InteractiveShowActivity.this.c = b.get(0);
                        if (b.get(0).getKeyIaUrl().contains("zip")) {
                            InteractiveShowActivity.this.interactiveAdsAdapter.a(b, b.get(0).getKeyIaUrl(), InteractiveShowActivity.this.h, null, true);
                        } else if (!com.mintegral.msdk.interactiveads.b.a.d) {
                            Message obtain2 = Message.obtain();
                            obtain2.obj = b;
                            obtain2.what = 1;
                            InteractiveShowActivity.this.g.sendMessage(obtain2);
                        }
                    }
                }
            }
        }).start();
    }

    public void setCloseButtonVisible(boolean z) {
        if (this.e == null) {
            return;
        }
        if (z) {
            this.e.setVisibility(0);
        } else {
            this.e.setVisibility(4);
        }
    }

    public void tryReportADTrackingImpression() {
        try {
            if (this.c != null && this.c.getKeyIaRst() == 2 && this.n != null && this.n.size() > 0) {
                for (CampaignEx reportADTrackingImpression : this.n) {
                    reportADTrackingImpression(com.mintegral.msdk.base.controller.a.d().h(), reportADTrackingImpression, this.h);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    public void reportADTrackingImpression(Context context, CampaignEx campaignEx, String str) {
        if (campaignEx != null) {
            try {
                if (campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().n() != null) {
                    for (String str2 : campaignEx.getNativeVideoTracking().n()) {
                        if (!TextUtils.isEmpty(str2)) {
                            com.mintegral.msdk.click.a.a(context, campaignEx, str, str2, false, false);
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    public void reportADTrackingClick(Context context, CampaignEx campaignEx, String str) {
        if (campaignEx != null) {
            try {
                if (campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().j() != null) {
                    for (String str2 : campaignEx.getNativeVideoTracking().j()) {
                        if (!TextUtils.isEmpty(str2)) {
                            com.mintegral.msdk.click.a.a(context, campaignEx, str, str2, false, false);
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void tryReportPlayableClosed() {
        if (this.m && this.c != null && this.c.getKeyIaRst() == 1) {
            q qVar = new q("2000061", this.c.getId(), this.c.getRequestId(), this.h, com.mintegral.msdk.base.utils.c.s(com.mintegral.msdk.base.controller.a.d().h()));
            qVar.a(this.c.isMraid() ? q.a : q.b);
            com.mintegral.msdk.base.common.e.a.d(qVar, this, this.h);
        }
    }

    public void reportPlayableExp(CampaignEx campaignEx, int i2, String str) {
        if (campaignEx != null && campaignEx.getKeyIaRst() == 1) {
            com.mintegral.msdk.base.common.e.a.e(new q("2000062", campaignEx.getId(), campaignEx.getRequestId(), this.h, com.mintegral.msdk.base.utils.c.s(com.mintegral.msdk.base.controller.a.d().h()), i2, str), this, this.h);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    /* access modifiers changed from: protected */
    public void init() {
        try {
            if (this.c != null) {
                this.d = a(this.c);
                String keyIaUrl = this.c.getKeyIaUrl();
                boolean z = !TextUtils.isEmpty(keyIaUrl) && keyIaUrl.contains("forcevert=1") && Build.VERSION.SDK_INT < 14;
                boolean z2 = !TextUtils.isEmpty(keyIaUrl) && keyIaUrl.contains("anpad=1") && com.mintegral.msdk.base.utils.c.u(this);
                if (z || z2) {
                    setRequestedOrientation(1);
                }
                if (this.c.getKeyIaOri() == 2) {
                    setRequestedOrientation(0);
                } else if (this.c.getKeyIaOri() == 1) {
                    setRequestedOrientation(1);
                }
            }
            String j2 = com.mintegral.msdk.base.controller.a.d().j();
            if (this.h == null) {
                this.h = com.mintegral.msdk.base.a.a.a.a().a("interactive_unitid");
            }
            com.mintegral.msdk.d.b.a();
            d c2 = com.mintegral.msdk.d.b.c(j2, this.h);
            this.m = false;
            if (this.d == null || this.q) {
                this.m = false;
                if (c2 == null || TextUtils.isEmpty(c2.e())) {
                    if (!com.mintegral.msdk.interactiveads.b.a.c) {
                        dismissLoadingDialog();
                        e();
                        d();
                        if (interactiveStatusListener != null && !this.q) {
                            interactiveStatusListener.c("failed:no init data");
                            new a(this.c, false, this, Constants.ParametersKeys.FAILED).start();
                            c();
                        }
                    }
                    reportPlayableExp(this.c, 3, "webview null, unitSetting or noAdsUrl null");
                    return;
                }
                this.r = false;
                if (this.t == null) {
                    this.t = new CountDownTimer() {
                        public final void onTick(long j) {
                        }

                        public final void onFinish() {
                            boolean unused = InteractiveShowActivity.this.r = true;
                            InteractiveShowActivity.this.dismissLoadingDialog();
                            InteractiveShowActivity.this.e();
                            InteractiveShowActivity.this.d();
                        }
                    }.start();
                } else {
                    this.t.cancel();
                    this.t.start();
                }
                if (!com.mintegral.msdk.interactiveads.b.a.c) {
                    this.d = new WindVaneWebView(this);
                    this.a.removeAllViews();
                    this.a.addView(this.d, this.b);
                    this.a.addView(this.e);
                    com.mintegral.msdk.interactiveads.jscommon.a aVar = new com.mintegral.msdk.interactiveads.jscommon.a(this, null);
                    aVar.a(this);
                    this.d.setObject(aVar);
                    this.d.loadUrl(c2.e());
                    this.d.setWebViewListener(new com.mintegral.msdk.mtgjscommon.windvane.c() {
                        public final void a() {
                        }

                        public final boolean b() {
                            return false;
                        }

                        public final void c() {
                        }

                        public final void a(WebView webView, int i, String str, String str2) {
                            InteractiveShowActivity.this.b();
                            InteractiveShowActivity.this.c();
                            InteractiveShowActivity.this.dismissLoadingDialog();
                            InteractiveShowActivity.this.a.removeAllViews();
                            InteractiveShowActivity.this.e();
                            InteractiveShowActivity.this.d();
                            if (InteractiveShowActivity.interactiveStatusListener != null && !InteractiveShowActivity.this.q) {
                                InteractiveShowActivity.interactiveStatusListener.c("show failed");
                            }
                        }

                        public final void a(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
                            InteractiveShowActivity.this.b();
                            InteractiveShowActivity.this.c();
                            InteractiveShowActivity.this.dismissLoadingDialog();
                            InteractiveShowActivity.this.a.removeAllViews();
                            InteractiveShowActivity.this.e();
                            InteractiveShowActivity.this.d();
                            if (InteractiveShowActivity.interactiveStatusListener != null && !InteractiveShowActivity.this.q) {
                                InteractiveShowActivity.interactiveStatusListener.c("show failed");
                            }
                        }

                        public final void a(WebView webView, String str) {
                            if (!InteractiveShowActivity.this.r) {
                                InteractiveShowActivity.this.b(InteractiveShowActivity.this.d);
                            }
                        }

                        public final void a(int i) {
                            if (i == 1) {
                                InteractiveShowActivity.this.b();
                                InteractiveShowActivity.this.dismissLoadingDialog();
                            }
                        }
                    });
                    reportPlayableExp(this.c, 3, "webview null, load noAdsUrl");
                    return;
                }
                return;
            }
            dismissLoadingDialog();
            Object object = this.d.getObject();
            if (object != null && (object instanceof com.mintegral.msdk.interactiveads.jscommon.a)) {
                com.mintegral.msdk.interactiveads.jscommon.a aVar2 = (com.mintegral.msdk.interactiveads.jscommon.a) object;
                aVar2.a(this);
                this.d.setObject(aVar2);
            }
            this.a.removeAllViews();
            this.a.addView(this.d, this.b);
            this.a.addView(this.e);
            b(this.d);
            this.m = true;
            com.mintegral.msdk.interactiveads.a.c.a(this);
            com.mintegral.msdk.interactiveads.a.c.i();
            if (this.c != null) {
                String impressionURL = this.c.getImpressionURL();
                if (this.c.getKeyIaRst() == 1 && !TextUtils.isEmpty(impressionURL)) {
                    com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), this.c, this.h, impressionURL, false, true);
                    if (!TextUtils.isEmpty(this.c.getOnlyImpressionURL())) {
                        com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), this.c, this.h, this.c.getOnlyImpressionURL(), false, true);
                    }
                }
                if (this.c.getKeyIaRst() == 1) {
                    reportADTrackingImpression(com.mintegral.msdk.base.controller.a.d().h(), this.c, this.h);
                }
            }
            if (c2 != null) {
                if (Double.valueOf(new Random().nextDouble()).doubleValue() <= c2.d()) {
                    if (interactiveStatusListener != null) {
                        interactiveStatusListener.d();
                        new a(this.c, true, this, "").start();
                    }
                } else if (interactiveStatusListener != null) {
                    interactiveStatusListener.c(Constants.ParametersKeys.FAILED);
                    new a(this.c, false, this, Constants.ParametersKeys.FAILED).start();
                }
            } else if (interactiveStatusListener != null) {
                interactiveStatusListener.d();
                new a(this.c, true, this, "").start();
            }
        } catch (Throwable th) {
            this.m = false;
            reportPlayableExp(this.c, 3, th.getMessage());
            th.printStackTrace();
            finish();
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        this.a.removeView(this.e);
        this.a.addView(this.e);
        setCloseButtonVisible(true);
    }

    /* access modifiers changed from: private */
    public void e() {
        TextView textView = new TextView(this);
        textView.setText("Check your connection,and try again.");
        textView.setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
        textView.setGravity(17);
        this.a.addView(textView, this.b);
    }

    public void webviewLoadMore(int i2, JSONArray jSONArray, com.mintegral.msdk.interactiveads.d.d dVar) {
        if (interactiveStatusListener != null) {
            interactiveStatusListener.a(jSONArray, dVar);
        }
    }

    private static WindVaneWebView a(CampaignEx campaignEx) {
        try {
            a.C0069a a2 = com.mintegral.msdk.videocommon.a.a(288, campaignEx);
            if (a2 == null || !a2.b()) {
                return null;
            }
            com.mintegral.msdk.videocommon.a.b(288, campaignEx);
            return a2.a();
        } catch (Exception e2) {
            if (!MIntegralConstans.DEBUG) {
                return null;
            }
            e2.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void b(WindVaneWebView windVaneWebView) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("adtype", "Interactive");
            String encodeToString = Base64.encodeToString(jSONObject.toString().getBytes(), 2);
            g.a();
            g.a(windVaneWebView, "webviewshow", encodeToString);
            q qVar = new q();
            qVar.k(this.c.getRequestIdNotice());
            qVar.m(this.c.getId());
            qVar.a(this.c.isMraid() ? q.a : q.b);
            com.mintegral.msdk.base.common.e.a.b(qVar, getApplicationContext(), this.h);
        } catch (Exception e2) {
            reportPlayableExp(this.c, 3, e2.getMessage());
            com.mintegral.msdk.base.utils.g.d(TAG, e2.getMessage());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    public void onInteractivePlayingComplete(boolean z) {
        if (!(this.c == null || this.c.getKeyIaRst() != 1 || interactiveStatusListener == null)) {
            interactiveStatusListener.a(z);
        }
        try {
            if (this.c != null && this.c.getKeyIaRst() == 1) {
                int i2 = 0;
                if (z) {
                    Context h2 = com.mintegral.msdk.base.controller.a.d().h();
                    CampaignEx campaignEx = this.c;
                    String str = this.h;
                    if (campaignEx != null) {
                        try {
                            if (campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().b() != null) {
                                String[] b2 = campaignEx.getNativeVideoTracking().b();
                                int length = b2.length;
                                while (i2 < length) {
                                    String str2 = b2[i2];
                                    if (!TextUtils.isEmpty(str2)) {
                                        com.mintegral.msdk.click.a.a(h2, campaignEx, str, str2, false, false);
                                    }
                                    i2++;
                                }
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                } else {
                    Context h3 = com.mintegral.msdk.base.controller.a.d().h();
                    CampaignEx campaignEx2 = this.c;
                    String str3 = this.h;
                    if (campaignEx2 != null) {
                        try {
                            if (campaignEx2.getNativeVideoTracking() != null && campaignEx2.getNativeVideoTracking().a() != null) {
                                String[] a2 = campaignEx2.getNativeVideoTracking().a();
                                int length2 = a2.length;
                                while (i2 < length2) {
                                    String str4 = a2[i2];
                                    if (!TextUtils.isEmpty(str4)) {
                                        com.mintegral.msdk.click.a.a(h3, campaignEx2, str3, str4, false, false);
                                    }
                                    i2++;
                                }
                            }
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                    }
                }
            }
        } catch (Exception e4) {
            e4.printStackTrace();
        }
    }

    public void showAlertView() {
        if (this.l == null) {
            this.l = new com.mintegral.msdk.videocommon.dialog.a() {
                public final void a() {
                    if (InteractiveShowActivity.this.d != null) {
                        InteractiveShowActivity.a(InteractiveShowActivity.this.d);
                    }
                }

                public final void b() {
                    InteractiveShowActivity.this.onInteractivePlayingComplete(false);
                    if (InteractiveShowActivity.interactiveStatusListener != null) {
                        InteractiveShowActivity.interactiveStatusListener.e();
                    }
                    InteractiveShowActivity.this.tryReportADTrackingImpression();
                    InteractiveShowActivity.this.tryReportPlayableClosed();
                    InteractiveShowActivity.this.finish();
                }
            };
        }
        if (this.k == null) {
            this.k = new MTGAlertDialog(this, this.l);
        }
        this.k.makePlayableAlertView();
        this.k.show();
    }

    public void finish() {
        super.finish();
        com.mintegral.msdk.interactiveads.b.a.e = false;
        this.interactiveAdsAdapter = null;
        com.mintegral.msdk.interactiveads.c.a.f = false;
        com.mintegral.msdk.interactiveads.b.a.f.clear();
        com.mintegral.msdk.videocommon.a.b(288);
        try {
            if (!(this.a == null || this.d == null)) {
                this.a.removeView(this.d);
                this.d.removeViewInLayout(this.a);
                if (this.e != null) {
                    this.a.removeView(this.e);
                }
            }
            if (this.i != null) {
                this.i = null;
            }
            if (this.j != null) {
                this.j = null;
            }
            com.mintegral.msdk.interactiveads.a.c.a(this).a(false);
            if (this.k != null) {
                this.k.clear();
                this.k = null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        c();
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    public void sendImpressions(JSONArray jSONArray) {
        String str = TAG;
        com.mintegral.msdk.base.utils.g.a(str, "sendImpressions JSONArray:" + jSONArray.toString());
        List<CampaignEx> a2 = f.a(i.a(com.mintegral.msdk.base.controller.a.d().h())).a(jSONArray, this.h);
        if (a2 != null && a2.size() > 0) {
            for (CampaignEx next : a2) {
                String impressionURL = next.getImpressionURL();
                String str2 = TAG;
                com.mintegral.msdk.base.utils.g.a(str2, "sendImpressions  impressionUrl:" + impressionURL);
                if (!TextUtils.isEmpty(impressionURL)) {
                    com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), next, this.h, impressionURL, false, true);
                }
                com.mintegral.msdk.base.common.a.c.a(this.h, next, "interactive");
            }
        }
        this.n.clear();
        this.n.addAll(a2);
    }

    public void click(int i2, CampaignEx campaignEx) {
        if (campaignEx != null) {
            String str = TAG;
            com.mintegral.msdk.base.utils.g.a(str, "click TYPE:" + i2 + "-unit id:" + this.h);
            new com.mintegral.msdk.click.a(com.mintegral.msdk.base.controller.a.d().h(), this.h).b(campaignEx);
            reportADTrackingClick(com.mintegral.msdk.base.controller.a.d().h(), campaignEx, this.h);
            if (interactiveStatusListener != null) {
                interactiveStatusListener.f();
            }
            k.a(i.a(this)).a(new e(Long.parseLong(campaignEx.getId()), Long.valueOf(System.currentTimeMillis()).longValue(), campaignEx.getKeyIaRst(), 1));
        }
    }

    public void handlerPlayableException(String str) {
        com.mintegral.msdk.base.utils.g.d(TAG, "===========handlerPlayableException");
        if (interactiveStatusListener != null) {
            interactiveStatusListener.c("Playable Render Exception");
            new a(this.c, false, this, Constants.ParametersKeys.FAILED).start();
        }
        if (!this.q) {
            this.q = true;
            init();
            if (this.c != null) {
                q qVar = new q();
                qVar.k(this.c.getRequestIdNotice());
                qVar.m(this.c.getId());
                qVar.o(str);
                com.mintegral.msdk.base.common.e.a.f(qVar, getApplicationContext(), this.h);
            }
        }
    }

    public void clickPlayer() {
        if (this.c != null) {
            new com.mintegral.msdk.click.a(com.mintegral.msdk.base.controller.a.d().h(), this.h).b(this.c);
            reportADTrackingClick(com.mintegral.msdk.base.controller.a.d().h(), this.c, this.h);
            if (interactiveStatusListener != null) {
                interactiveStatusListener.f();
            }
            k.a(i.a(this)).a(new e(Long.parseLong(this.c.getId()), Long.valueOf(System.currentTimeMillis()).longValue(), this.c.getKeyIaRst(), 1));
        }
    }

    public void showLoadingDialog() {
        if (this.j == null) {
            this.j = new c() {
                public final void a() {
                    if (InteractiveShowActivity.interactiveStatusListener != null) {
                        InteractiveShowActivity.interactiveStatusListener.c("closed user cancel");
                        new a(InteractiveShowActivity.this.c, false, InteractiveShowActivity.this, Constants.ParametersKeys.FAILED).start();
                        InteractiveShowActivity.interactiveStatusListener.e();
                    }
                    InteractiveShowActivity.this.finish();
                }
            };
        }
        if (this.i == null) {
            this.i = new MTGLoadingDialog(this, this.j);
        }
        this.i.show();
    }

    public void dismissLoadingDialog() {
        if (this.i != null && this.i.isShowing()) {
            this.i.dismiss();
            this.j = null;
        }
    }

    public void interactiveAdsMateriaShowSuccessful(CampaignEx campaignEx, boolean z) {
        com.mintegral.msdk.interactiveads.c.a.f = false;
        this.c = campaignEx;
        com.mintegral.msdk.interactiveads.b.a.f.clear();
        if (interactiveStatusListener != null && this.c != null && !TextUtils.isEmpty(this.c.getInteractiveCache()) && this.c.getInteractiveCache().equals("onelevel") && !z) {
            interactiveStatusListener.a(campaignEx);
        }
        if (!this.q) {
            c();
            init();
        }
    }

    class a extends Thread {
        private CampaignEx b;
        private boolean c;
        private Context d;
        private String e;

        public a(CampaignEx campaignEx, boolean z, Context context, String str) {
            this.b = campaignEx;
            this.c = z;
            this.d = context;
            this.e = str;
        }

        public final void run() {
            super.run();
            if (this.b != null) {
                try {
                    q qVar = new q();
                    qVar.n("2000054");
                    qVar.a(String.valueOf(this.b.getKeyIaRst()));
                    qVar.l(InteractiveShowActivity.this.h);
                    qVar.b(com.mintegral.msdk.base.utils.c.k());
                    qVar.h("4");
                    int i = 1;
                    if (this.b.getKeyIaRst() == 1) {
                        qVar.m(this.b.getId());
                        qVar.k(this.b.getRequestIdNotice());
                    } else {
                        qVar.k(this.b.getRequestId());
                    }
                    if (!this.c) {
                        i = 2;
                    }
                    qVar.c(i);
                    qVar.o(this.c ? "" : this.e);
                    qVar.b(com.mintegral.msdk.base.utils.c.s(InteractiveShowActivity.this));
                    qVar.c(this.b.getKeyIaUrl());
                    w.a(i.a(this.d)).a(qVar);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    static /* synthetic */ boolean a(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            CampaignEx campaignEx = (CampaignEx) it.next();
            if (campaignEx.getIsDownLoadZip() == 1) {
                if (!TextUtils.isEmpty(j.a().a(campaignEx.getKeyIaUrl()))) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    static /* synthetic */ void a(WindVaneWebView windVaneWebView) {
        try {
            g.a();
            g.a(windVaneWebView, "continuePlay", "");
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
