package com.mintegral.msdk.interactiveads.out;

public interface InteractiveAdsListener {
    void onInterActiveMaterialLoadSuccess();

    void onInteractiveAdClick();

    void onInteractiveClosed();

    void onInteractiveLoadFail(String str);

    void onInteractivePlayingComplete(boolean z);

    void onInteractiveShowFail(String str);

    void onInteractiveShowSuccess();

    void onInteractivelLoadSuccess(int i);
}
