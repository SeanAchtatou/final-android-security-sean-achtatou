package com.mintegral.msdk.interactiveads.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.f;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.k;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.mintegral.msdk.base.entity.e;
import com.mintegral.msdk.base.entity.h;
import com.mintegral.msdk.base.entity.q;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.n;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.interactiveads.activity.InteractiveShowActivity;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.videocommon.a;
import com.mintegral.msdk.videocommon.download.g;
import com.mintegral.msdk.videocommon.download.j;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: InteractiveAdsAdapter */
public final class a {
    public static List<CampaignEx> a;
    /* access modifiers changed from: private */
    public Context b;
    /* access modifiers changed from: private */
    public String c;
    private int d;
    private com.mintegral.msdk.interactiveads.d.a e;
    /* access modifiers changed from: private */
    public com.mintegral.msdk.interactiveads.d.b f;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public long i = 0;
    private CountDownTimer j = null;
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public boolean l = false;
    /* access modifiers changed from: private */
    public Handler m = new Handler(Looper.getMainLooper()) {
        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public final void handleMessage(Message message) {
            try {
                int i = message.what;
                if (i != 16) {
                    switch (i) {
                        case 3:
                            if (a.this.f != null && a.a != null) {
                                String str = (String) message.obj;
                                if (!str.equals("loadmore") && !str.equals("twolevel")) {
                                    a.this.f.a(a.a);
                                    return;
                                }
                                return;
                            }
                            return;
                        case 4:
                            com.mintegral.msdk.interactiveads.f.a.a(a.this.b, Constants.ParametersKeys.FAILED, a.this.c, (CampaignEx) null);
                            a.this.f.d("no data");
                            return;
                        case 5:
                            try {
                                com.mintegral.msdk.interactiveads.e.a aVar = new com.mintegral.msdk.interactiveads.e.a(a.this.b);
                                HashMap hashMap = (HashMap) message.obj;
                                final com.mintegral.msdk.interactiveads.d.d dVar = (com.mintegral.msdk.interactiveads.d.d) hashMap.get("loadmore");
                                l lVar = (l) hashMap.get("param");
                                if (dVar == null) {
                                    c.a(a.this.b);
                                    c.g();
                                }
                                final String str2 = (String) hashMap.get("tag");
                                AnonymousClass1 r3 = new com.mintegral.msdk.interactiveads.e.b() {
                                    public final void a(CampaignUnit campaignUnit) {
                                        try {
                                            if (dVar == null) {
                                                c.a(a.this.b);
                                                c.h();
                                            }
                                            g.d("InteractiveAdsAdapter", "InteractiveLoad succesfull");
                                            if (!a.this.k) {
                                                a.this.a(campaignUnit, str2, dVar);
                                            }
                                            boolean unused = a.this.h = false;
                                            com.mintegral.msdk.interactiveads.b.a.c = false;
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    public final void g() {
                                        g.d("InteractiveAdsAdapter", "InteractiveLoad faliled");
                                        try {
                                            if (dVar == null && !a.this.k && str2.equals("onelevel")) {
                                                a.this.f.d("faliled");
                                                com.mintegral.msdk.interactiveads.f.a.a(a.this.b, Constants.ParametersKeys.FAILED, a.this.c, (CampaignEx) null);
                                            }
                                            a.i(a.this);
                                            boolean unused = a.this.h = false;
                                            com.mintegral.msdk.interactiveads.b.a.c = false;
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                };
                                r3.d = a.this.c;
                                aVar.a(com.mintegral.msdk.base.common.a.k, lVar, r3);
                                return;
                            } catch (Exception e) {
                                e.printStackTrace();
                                return;
                            }
                        case 6:
                            try {
                                Object[] objArr = (Object[]) message.obj;
                                a.a(a.this, (List) objArr[0], (String) objArr[1]);
                                a.a(a.this, (CampaignUnit) objArr[2]);
                                return;
                            } catch (Exception e2) {
                                e2.printStackTrace();
                                return;
                            }
                        default:
                            switch (i) {
                                case 8:
                                    return;
                                case 9:
                                    try {
                                        g.d("InteractiveAdsAdapter", "经过preload");
                                        int i2 = message.what;
                                        Object[] objArr2 = (Object[]) message.obj;
                                        List list = (List) objArr2[0];
                                        String str3 = (String) objArr2[1];
                                        boolean booleanValue = ((Boolean) objArr2[4]).booleanValue();
                                        if (list != null && !TextUtils.isEmpty(str3)) {
                                            a.this.a(list, str3, true, booleanValue);
                                            return;
                                        }
                                        return;
                                    } catch (Exception e3) {
                                        e3.printStackTrace();
                                        return;
                                    }
                                default:
                                    return;
                            }
                    }
                    e.printStackTrace();
                }
                try {
                    g.d("InteractiveAdsAdapter", "经过preload");
                    int i3 = message.what;
                    Object[] objArr3 = (Object[]) message.obj;
                    List list2 = (List) objArr3[0];
                    String str4 = (String) objArr3[1];
                    boolean booleanValue2 = ((Boolean) objArr3[4]).booleanValue();
                    if (list2 != null && !TextUtils.isEmpty(str4)) {
                        a.this.a(list2, str4, false, booleanValue2);
                    }
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
            } catch (Exception e5) {
                e5.printStackTrace();
            }
        }
    };

    public final void a(com.mintegral.msdk.interactiveads.d.a aVar) {
        this.e = aVar;
    }

    public final void a(com.mintegral.msdk.interactiveads.d.b bVar) {
        this.f = bVar;
    }

    public final boolean a() {
        return this.g;
    }

    public a(Context context, String str) {
        this.b = context;
        this.c = str;
        this.g = false;
    }

    public final String b() {
        return this.c;
    }

    public final void c() {
        if (this.h) {
            this.f.d("current data is loading");
            com.mintegral.msdk.interactiveads.f.a.a(this.b, "current data is loading", this.c, (CampaignEx) null);
            return;
        }
        com.mintegral.msdk.interactiveads.b.a.c = true;
        this.h = true;
        this.k = false;
        this.l = false;
        if (this.j == null) {
            this.j = new CountDownTimer() {
                public final void onTick(long j) {
                }

                public final void onFinish() {
                    boolean unused = a.this.k = true;
                    if (a.this.h) {
                        a.this.f.d("loading is timeout");
                        com.mintegral.msdk.interactiveads.f.a.a(a.this.b, "loading is timeout", a.this.c, (CampaignEx) null);
                    }
                    if (!a.this.h && !a.this.l) {
                        a.this.f.d(" downloading zip is timeout");
                        com.mintegral.msdk.interactiveads.f.a.a(a.this.b, "loading is timeout", a.this.c, (CampaignEx) null);
                    }
                }
            }.start();
        } else {
            this.j.cancel();
            this.j.start();
        }
        f a2 = f.a(i.a(this.b));
        List<CampaignEx> b2 = b.a(this.b).b(this.c);
        g.d("InteractiveAdsAdapter", "二级缓存size---》" + b2.size());
        if (b2.size() <= 0) {
            List<CampaignEx> a3 = b.a(this.b).a(this.c);
            if (a3.size() > 0) {
                b.a(this.b);
                if (b.a(a3)) {
                    this.f.a(a3);
                    a("twolevel", (com.mintegral.msdk.interactiveads.d.d) null);
                    a(a3, a3.get(0).getKeyIaUrl(), this.c, null, false);
                    g.d("InteractiveAdsAdapter", "InteractiveLoad 6");
                    return;
                }
                a2.b(this.c, a3.get(0).getKeyIaRst());
                a("onelevel", (com.mintegral.msdk.interactiveads.d.d) null);
                g.d("InteractiveAdsAdapter", "InteractiveLoad 7");
                return;
            }
            a("onelevel", (com.mintegral.msdk.interactiveads.d.d) null);
            g.d("InteractiveAdsAdapter", "InteractiveLoad 8");
        } else if (b(b2)) {
            List<CampaignEx> a4 = b.a(this.b).a(this.c);
            if (a4.size() > 0) {
                a2.b(this.c, a4.get(0).getKeyIaRst());
                a(b2);
                a2.b(b2, this.c);
                this.f.a(b2);
                a("twolevel", (com.mintegral.msdk.interactiveads.d.d) null);
                a(b2, b2.get(0).getKeyIaUrl(), this.c, null, false);
                g.d("InteractiveAdsAdapter", "InteractiveLoad 1");
                return;
            }
            a(b2);
            a2.b(b2, this.c);
            this.f.a(b2);
            a("twolevel", (com.mintegral.msdk.interactiveads.d.d) null);
            a(b2, b2.get(0).getKeyIaUrl(), this.c, null, false);
            g.d("InteractiveAdsAdapter", "InteractiveLoad 2");
        } else {
            a2.b(this.c, "twolevel");
            List<CampaignEx> a5 = b.a(this.b).a(this.c);
            if (a5.size() > 0) {
                b.a(this.b);
                if (b.a(a5)) {
                    this.f.a(a5);
                    a("twolevel", (com.mintegral.msdk.interactiveads.d.d) null);
                    a(a5, a5.get(0).getKeyIaUrl(), this.c, null, false);
                    g.d("InteractiveAdsAdapter", "InteractiveLoad 3");
                    return;
                }
                a("onelevel", (com.mintegral.msdk.interactiveads.d.d) null);
                a2.b(this.c, a5.get(0).getKeyIaRst());
                g.d("InteractiveAdsAdapter", "InteractiveLoad 4");
                return;
            }
            a("onelevel", (com.mintegral.msdk.interactiveads.d.d) null);
            g.d("InteractiveAdsAdapter", "InteractiveLoad 5");
        }
    }

    private static void a(List<CampaignEx> list) {
        for (CampaignEx interactiveCache : list) {
            interactiveCache.setInteractiveCache("onelevel");
        }
    }

    private static boolean b(List<CampaignEx> list) {
        for (CampaignEx next : list) {
            if (next.getIsDownLoadZip() == 1) {
                if (TextUtils.isEmpty(j.a().a(next.getKeyIaUrl()))) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    public final synchronized void a(List<CampaignEx> list, String str, boolean z, boolean z2) {
        List<CampaignEx> list2 = list;
        String str2 = str;
        synchronized (this) {
            try {
                g.d("InteractiveAdsAdapter", "InteractiveLoad 9");
                g.d("InteractiveAdsAdapter", "InteractiveLoad 001" + str2);
                g.d("InteractiveAdsAdapter", "InteractiveLoad campaigns" + list.size());
                a.C0069a aVar = new a.C0069a();
                WindVaneWebView windVaneWebView = new WindVaneWebView(com.mintegral.msdk.base.controller.a.d().h());
                aVar.a(windVaneWebView);
                com.mintegral.msdk.interactiveads.jscommon.a aVar2 = new com.mintegral.msdk.interactiveads.jscommon.a(null, list2);
                aVar2.a(this.c);
                com.mintegral.msdk.d.b.a();
                aVar2.a(com.mintegral.msdk.d.b.c(com.mintegral.msdk.base.controller.a.d().j(), this.c));
                com.mintegral.msdk.interactiveads.b.a.d = true;
                windVaneWebView.setWebViewListener(new d(aVar, list2.get(0), this, this.f, this.e, z, z2));
                g.d("InteractiveAdsAdapter", "InteractiveLoad jsoncomm" + aVar2.hashCode());
                windVaneWebView.setObject(aVar2);
                g.d("InteractiveAdsAdapter", "InteractiveLoad jsoncommafter" + windVaneWebView.getObject().hashCode());
                windVaneWebView.loadUrl(str2);
            } catch (Exception e2) {
                if (list2 != null) {
                    if (list.size() > 0) {
                        a(list2.get(0), e2.getMessage());
                    }
                }
                g.d("InteractiveAdsAdapter", "InteractiveLoad printStackTrace");
                e2.printStackTrace();
            }
        }
    }

    /* compiled from: InteractiveAdsAdapter */
    public class d extends com.mintegral.msdk.mtgjscommon.b.a {
        com.mintegral.msdk.interactiveads.d.b a;
        com.mintegral.msdk.interactiveads.d.a b;
        boolean c;
        long d = 0;
        private a f;
        private a.C0069a g;
        private CampaignEx h;
        private boolean i;
        private boolean j;
        private boolean k;

        public d(a.C0069a aVar, CampaignEx campaignEx, a aVar2, com.mintegral.msdk.interactiveads.d.b bVar, com.mintegral.msdk.interactiveads.d.a aVar3, boolean z, boolean z2) {
            this.g = aVar;
            if (aVar2 != null) {
                this.f = aVar2;
            }
            this.h = campaignEx;
            this.a = bVar;
            this.b = aVar3;
            this.k = z;
            this.c = z2;
            this.d = System.currentTimeMillis();
            g.d("InteractiveAdsAdapter", "InteractiveLoad 99" + aVar.hashCode());
        }

        public final void a(int i2) {
            g.d("InteractiveAdsAdapter", "InteractiveLoad 13");
            com.mintegral.msdk.interactiveads.b.a.d = false;
            if (!this.j) {
                if (this.g != null) {
                    this.g.c();
                }
                this.j = true;
            }
        }

        public final void a(WebView webView, String str) {
            Context h2;
            super.a(webView, str);
            try {
                com.mintegral.msdk.interactiveads.b.a.d = false;
                if (!this.i) {
                    boolean unused = a.this.l = true;
                    com.mintegral.msdk.mtgjscommon.windvane.g.a();
                    com.mintegral.msdk.mtgjscommon.windvane.g.a(webView, "onJSBridgeConnected", "");
                    this.i = true;
                    if (this.g != null) {
                        this.g.c();
                    }
                    g.d("InteractiveAdsAdapter", "InteractiveLoad 11" + this.g.hashCode());
                    com.mintegral.msdk.videocommon.a.b(288);
                    com.mintegral.msdk.videocommon.a.b(288, this.h.getKeyIaUrl(), this.g);
                    if (this.k && (h2 = com.mintegral.msdk.base.controller.a.d().h()) != null && this.h != null && !TextUtils.isEmpty(this.h.getKeyIaUrl())) {
                        f.a(i.a(h2)).e(this.h.getKeyIaUrl());
                    }
                    if (com.mintegral.msdk.interactiveads.c.a.f) {
                        if (this.b != null) {
                            g.d("InteractiveAdsAdapter", "InteractiveLoad 111---->" + this.g.hashCode());
                            if (!a.this.k) {
                                this.b.interactiveAdsMateriaShowSuccessful(this.h, this.c);
                                b(1);
                                return;
                            }
                            return;
                        }
                        g.d("InteractiveAdsAdapter", "InteractiveLoad 222---->" + this.g.hashCode());
                        InteractiveShowActivity interactiveShowActivity = (InteractiveShowActivity) com.mintegral.msdk.interactiveads.b.a.f.get(InteractiveShowActivity.TAG);
                        if (interactiveShowActivity != null) {
                            g.d("InteractiveAdsAdapter", "InteractiveLoad 333---->" + this.g.hashCode());
                            if (!a.this.k) {
                                interactiveShowActivity.interactiveAdsMateriaShowSuccessful(this.h, this.c);
                                b(1);
                            }
                        }
                    } else if (this.h.getInteractiveCache().equals("onelevel")) {
                        g.d("InteractiveAdsAdapter", "InteractiveLoad 12");
                        if (this.a != null && !a.this.k) {
                            a.i(a.this);
                            this.a.a(this.h);
                            com.mintegral.msdk.interactiveads.f.a.a(a.this.b, this.h, a.this.c);
                            b(1);
                        }
                    }
                }
            } catch (Exception e2) {
                a.this.a(this.h, e2.getMessage());
                g.a("WindVaneWebView", e2.getMessage());
            }
        }

        public final void a(WebView webView, int i2, String str, String str2) {
            super.a(webView, i2, str, str2);
            try {
                g.d("InteractiveAdsAdapter", "InteractiveLoad 10");
                g.d("InteractiveAdsAdapter", "TempalteWindVaneWebviewClient tempalte load failed");
                a aVar = a.this;
                CampaignEx campaignEx = this.h;
                aVar.a(campaignEx, "webview loadUrl onReceivedError errorCode " + str);
                if (this.f != null) {
                    synchronized (this.f) {
                        g.d("InteractiveAdsAdapter", "TempalteWindVaneWebviewClient tempalte load callback failed");
                        this.f = null;
                        if (!TextUtils.isEmpty(this.h.getInteractiveCache()) && !this.h.getInteractiveCache().equals("twolevel")) {
                            this.a.d(str);
                            com.mintegral.msdk.interactiveads.f.a.a(a.this.b, str, a.this.c, this.h);
                            b(3);
                        }
                    }
                }
            } catch (Throwable th) {
                a.this.a(this.h, th.getMessage());
                g.d("InteractiveAdsAdapter", "InteractiveLoad 999");
                g.c("WindVaneWebView", th.getMessage(), th);
            }
        }

        public final void a(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            super.a(webView, sslErrorHandler, sslError);
            a aVar = a.this;
            CampaignEx campaignEx = this.h;
            aVar.a(campaignEx, "webview loadUrl SslError " + sslError);
        }

        private void b(int i2) {
            if (this.h != null) {
                q qVar = new q();
                qVar.k(this.h.getRequestIdNotice());
                qVar.m(this.h.getId());
                qVar.c(i2);
                qVar.p(String.valueOf(System.currentTimeMillis() - this.d));
                qVar.f(this.h.getKeyIaUrl());
                String str = "2";
                if (s.b(this.h.getKeyIaUrl()) && this.h.getKeyIaUrl().contains(".zip")) {
                    str = "1";
                }
                qVar.o("");
                qVar.g(str);
                qVar.h(String.valueOf(this.h.getAdType()));
                qVar.a(String.valueOf(this.h.getKeyIaRst()));
                qVar.h("4");
                qVar.a(this.h.isMraid() ? q.a : q.b);
                com.mintegral.msdk.base.common.e.a.c(qVar, a.this.b, a.this.c);
            }
        }
    }

    public final void a(final String str, final com.mintegral.msdk.interactiveads.d.d dVar) {
        try {
            new Thread(new Runnable() {
                public final void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    l d = a.this.d();
                    Message obtain = Message.obtain();
                    HashMap hashMap = new HashMap();
                    hashMap.put("param", d);
                    hashMap.put("tag", str);
                    hashMap.put("loadmore", dVar);
                    obtain.obj = hashMap;
                    obtain.what = 5;
                    a.this.m.sendMessage(obtain);
                }
            }).start();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void e() {
        if (this.m != null) {
            this.m.sendEmptyMessage(4);
        }
    }

    public final void a(final CampaignUnit campaignUnit, final String str, com.mintegral.msdk.interactiveads.d.d dVar) {
        final List<CampaignEx> a2 = a(campaignUnit);
        a = a2;
        if (a2.size() > 0 && dVar != null) {
            dVar.a(a);
        }
        if (a2.size() > 0) {
            g.b("InteractiveAdsAdapter", "onload load成功 size:" + a2.size());
            if (!TextUtils.isEmpty(a2.get(0).getKeyIaUrl())) {
                if (this.m != null) {
                    Message obtain = Message.obtain();
                    obtain.what = 3;
                    obtain.obj = str;
                    this.m.sendMessage(obtain);
                }
                if (campaignUnit != null) {
                    String sessionId = campaignUnit.getSessionId();
                    if (s.b(sessionId)) {
                        g.b("InteractiveAdsAdapter", "onload sessionId:" + sessionId);
                        com.mintegral.msdk.interactiveads.c.a.c = sessionId;
                    }
                }
                if (campaignUnit.getIa_rst() == 1) {
                    try {
                        if (a2.size() > 0) {
                            this.d += a2.size();
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                c.a(this.b);
                com.mintegral.msdk.d.d a3 = c.a();
                if (a3 != null && this.d > a3.z()) {
                    this.d = 0;
                }
                com.mintegral.msdk.interactiveads.c.a.a(this.c, this.d);
                if (dVar == null) {
                    new Thread(new Runnable() {
                        public final void run() {
                            g.b("InteractiveAdsAdapter", "在子线程处理业务逻辑 开始");
                            if (a2 != null && a2.size() > 0) {
                                g.b("InteractiveAdsAdapter", "onload 把广告存在本地 size:" + a2.size());
                                a.this.a(a.this.c, a2, str);
                                Message obtain = Message.obtain();
                                obtain.obj = new Object[]{a2, str, campaignUnit};
                                obtain.what = 6;
                                a.this.m.sendMessage(obtain);
                            }
                            m.a(i.a(a.this.b)).d();
                            if (!(campaignUnit == null || campaignUnit.getAds() == null || campaignUnit.getAds().size() <= 0)) {
                                a.a(a.this, campaignUnit.getAds());
                            }
                            g.b("InteractiveAdsAdapter", "在子线程处理业务逻辑 完成");
                        }
                    }).start();
                }
            } else if (str.equals("onelevel")) {
                e();
            }
        } else if (str.equals("onelevel")) {
            e();
        }
    }

    /* compiled from: InteractiveAdsAdapter */
    private final class c implements g.c {
        Context a;
        String b;
        private List<CampaignEx> d;
        private String e;
        private com.mintegral.msdk.d.d f;

        public c(List<CampaignEx> list, Context context, String str, com.mintegral.msdk.d.d dVar, String str2) {
            this.e = str;
            this.d = list;
            this.f = dVar;
            this.a = context;
            this.b = str2;
        }

        public final void a(String str) {
            com.mintegral.msdk.base.utils.g.d("InteractiveAdsAdapter", "InteractiveZipDownloadListener ZIP successful:" + this.b + str);
            f.a(i.a(this.a)).e(this.d.get(0).getKeyIaUrl());
            if (this.b.equals("onelevel")) {
                com.mintegral.msdk.base.utils.g.d("InteractiveAdsAdapter", "InteractiveZipDownloadListener ZIP campaignTag.equals");
                a.this.a(this.d, str, this.e, this.f, false);
                a.this.f.a(1);
            }
            boolean unused = a.this.g = false;
            CampaignEx campaignEx = this.d.get(0);
            long currentTimeMillis = System.currentTimeMillis() - a.this.i;
            if (campaignEx != null) {
                new b(campaignEx, currentTimeMillis, true, this.a, "").start();
            }
        }

        public final void a(String str, String str2) {
            com.mintegral.msdk.base.utils.g.d("InteractiveAdsAdapter", "InteractiveZipDownloadListener ZIP failed:" + str2);
            if (this.b.equals("onelevel")) {
                a.this.f.c(str);
                a.this.f.a(0);
                com.mintegral.msdk.interactiveads.f.a.a(a.this.b, "errorMsg", a.this.c, this.d.get(0));
            }
            boolean unused = a.this.g = false;
            CampaignEx campaignEx = this.d.get(0);
            long currentTimeMillis = System.currentTimeMillis() - a.this.i;
            if (campaignEx != null) {
                new b(campaignEx, currentTimeMillis, false, this.a, str).start();
            }
        }
    }

    /* compiled from: InteractiveAdsAdapter */
    class b extends Thread {
        private CampaignEx b;
        private long c;
        private boolean d;
        private Context e;
        private String f;

        public b(CampaignEx campaignEx, long j, boolean z, Context context, String str) {
            this.b = campaignEx;
            this.c = j;
            this.d = z;
            this.e = context;
            this.f = str;
        }

        public final void run() {
            String campaignUnitId;
            super.run();
            if (this.b != null) {
                try {
                    if (this.b.getCampaignUnitId() == null) {
                        campaignUnitId = a.this.c;
                    } else {
                        campaignUnitId = this.b.getCampaignUnitId();
                    }
                    q qVar = new q("2000043", this.d ? 1 : 3, String.valueOf(this.c), this.b.getendcard_url(), this.b.getId(), campaignUnitId, this.d ? "" : this.f, String.valueOf(this.b.getKeyIaRst()));
                    if (this.b.getKeyIaRst() == 1) {
                        qVar.m(this.b.getId());
                        qVar.k(this.b.getRequestIdNotice());
                    } else {
                        qVar.m("");
                        qVar.k(this.b.getRequestId());
                    }
                    qVar.h("4");
                    qVar.a(String.valueOf(this.b.getKeyIaRst()));
                    qVar.f(this.b.getKeyIaUrl());
                    qVar.b(com.mintegral.msdk.base.utils.c.s(this.e));
                    com.mintegral.msdk.base.common.e.a.a(qVar, this.e, a.this.c);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public final void a(List<CampaignEx> list, String str, String str2, com.mintegral.msdk.d.d dVar, boolean z) {
        int i2;
        try {
            if (!TextUtils.isEmpty(str)) {
                Object[] objArr = new Object[5];
                if (str.contains("zip")) {
                    str = j.a().a(str);
                    i2 = 16;
                } else {
                    i2 = 9;
                }
                Message obtain = Message.obtain();
                obtain.what = i2;
                objArr[0] = list;
                objArr[1] = str;
                objArr[2] = str2;
                objArr[3] = dVar;
                objArr[4] = Boolean.valueOf(z);
                obtain.obj = objArr;
                this.m.sendMessage(obtain);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* renamed from: com.mintegral.msdk.interactiveads.a.a$a  reason: collision with other inner class name */
    /* compiled from: InteractiveAdsAdapter */
    private static final class C0058a implements com.mintegral.msdk.base.common.c.c {
        private a a;
        private CampaignUnit b;
        private String c;

        public final void onFailedLoad(String str, String str2) {
        }

        public final void onSuccessLoad(Bitmap bitmap, String str) {
        }

        public C0058a(a aVar, CampaignUnit campaignUnit, String str) {
            if (aVar != null) {
                this.a = aVar;
            }
            this.b = campaignUnit;
            this.c = str;
        }
    }

    public final void a(CampaignEx campaignEx, String str) {
        if (campaignEx != null && campaignEx.getKeyIaRst() == 1) {
            com.mintegral.msdk.base.common.e.a.e(new q("2000062", campaignEx.getId(), campaignEx.getRequestId(), this.c, com.mintegral.msdk.base.utils.c.s(com.mintegral.msdk.base.controller.a.d().h()), 1, str), this.b, this.c);
        }
    }

    public final void a(String str, List<CampaignEx> list, String str2) {
        f a2;
        try {
            if (!TextUtils.isEmpty(str) && list != null && list.size() > 0) {
                for (CampaignEx next : list) {
                    if (str2.equals("twolevel")) {
                        next.setInteractiveCache("twolevel");
                    } else if (str2.equals("onelevel")) {
                        next.setInteractiveCache("onelevel");
                    }
                    if (next != null) {
                        try {
                            if (!TextUtils.isEmpty(str) && (a2 = f.a(i.a(this.b))) != null) {
                                a2.a(next, str);
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                    k.a(i.a(this.b)).a(new e(Long.parseLong(next.getId()), System.currentTimeMillis(), next.getKeyIaRst(), 0));
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private List<CampaignEx> a(CampaignUnit campaignUnit) {
        ArrayList arrayList = new ArrayList();
        if (campaignUnit != null) {
            try {
                if (campaignUnit.getAds() != null && campaignUnit.getAds().size() > 0) {
                    ArrayList<CampaignEx> ads = campaignUnit.getAds();
                    for (int i2 = 0; i2 < ads.size(); i2++) {
                        CampaignEx campaignEx = ads.get(i2);
                        if (!(campaignEx == null || campaignEx.getOfferType() == 99 || (campaignEx.getWtick() != 1 && com.mintegral.msdk.base.utils.k.a(this.b, campaignEx.getPackageName()) && !com.mintegral.msdk.base.utils.k.a(campaignEx) && !com.mintegral.msdk.base.utils.k.b(campaignEx)))) {
                            campaignEx.setKeyIaIcon(campaignUnit.getIa_icon());
                            campaignEx.setKeyIaUrl(campaignUnit.getIa_url());
                            campaignEx.setKeyIaRst(campaignUnit.getIa_rst());
                            campaignEx.setKeyIaOri(campaignUnit.getIa_ori());
                            campaignEx.setAdType(campaignUnit.getAdType());
                            arrayList.add(campaignEx);
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return arrayList;
    }

    private int f() {
        try {
            int a2 = s.b(this.c) ? com.mintegral.msdk.interactiveads.c.a.a(this.c) : 0;
            c.a(this.b);
            if (c.a() == null) {
                return 0;
            }
            return a2;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public final l d() {
        int i2;
        String j2 = com.mintegral.msdk.base.controller.a.d().j();
        String md5 = CommonMD5.getMD5(com.mintegral.msdk.base.controller.a.d().j() + com.mintegral.msdk.base.controller.a.d().k());
        l lVar = new l();
        n.a(lVar, "app_id", j2);
        n.a(lVar, MIntegralConstans.PROPERTIES_UNIT_ID, this.c);
        n.a(lVar, "sign", md5);
        n.a(lVar, "ad_num", "1");
        n.a(lVar, CampaignEx.JSON_KEY_AD_SOURCE_ID, "1");
        n.a(lVar, "ad_type", "288");
        n.a(lVar, "display_info", com.mintegral.msdk.base.common.a.c.a(this.c, "interactive"));
        n.a(lVar, "exclude_ids", c.a(this.b).b());
        n.a(lVar, "keyword", "");
        n.a(lVar, "offset", String.valueOf(f()));
        n.a(lVar, "only_impression", "1");
        n.a(lVar, "orientation", String.valueOf(com.mintegral.msdk.base.utils.c.i(this.b)));
        n.a(lVar, "ping_mode", "1");
        n.a(lVar, "tnum", "1");
        n.a(lVar, "ia_c_ads", c.a(this.b).b(this.c));
        List<CampaignEx> a2 = c.a(this.b).a(this.c);
        int i3 = 0;
        if (a2 != null) {
            i2 = a2.get(0).getKeyIaRst();
        } else {
            i2 = 0;
        }
        n.a(lVar, "ia_c_rst", String.valueOf(i2));
        n.a(lVar, "ia_dp_rst", c.a(this.b).c());
        c.a(this.b);
        n.a(lVar, "ia_req_a", String.valueOf(c.d()));
        c.a(this.b);
        n.a(lVar, "ia_req_b", String.valueOf(c.e()));
        if (c.a(this.b).f()) {
            i3 = 2;
        }
        n.a(lVar, "ia_rst", String.valueOf(i3));
        n.a(lVar, "ia_lm", String.valueOf(c.a(this.b).f() ? 1 : 0));
        return lVar;
    }

    static /* synthetic */ void i(a aVar) {
        if (aVar.j != null) {
            aVar.j.cancel();
            aVar.j = null;
        }
    }

    static /* synthetic */ void a(a aVar, List list, String str) {
        if (list != null && list.size() > 0) {
            CampaignEx campaignEx = (CampaignEx) list.get(0);
            String keyIaUrl = campaignEx.getKeyIaUrl();
            if (TextUtils.isEmpty(keyIaUrl)) {
                return;
            }
            if (!keyIaUrl.contains(".zip") || !keyIaUrl.contains("md5filename")) {
                if (str.equals("onelevel")) {
                    com.mintegral.msdk.base.utils.g.d("InteractiveAdsAdapter", "InteractiveZipDownloadListener ZIP campaignTag.equals");
                    aVar.a(list, keyIaUrl, true, false);
                    aVar.f.a(1);
                } else {
                    f.a(i.a(aVar.b)).e(campaignEx.getKeyIaUrl());
                }
                aVar.g = false;
                return;
            }
            String j2 = com.mintegral.msdk.base.controller.a.d().j();
            com.mintegral.msdk.d.b.a();
            com.mintegral.msdk.d.d c2 = com.mintegral.msdk.d.b.c(j2, aVar.c);
            if (str.equals("onelevel")) {
                aVar.g = true;
            }
            aVar.i = System.currentTimeMillis();
            com.mintegral.msdk.videocommon.download.g.a().a(keyIaUrl, new c(list, aVar.b, aVar.c, c2, str));
        }
    }

    static /* synthetic */ void a(a aVar, CampaignUnit campaignUnit) {
        if (campaignUnit != null && !TextUtils.isEmpty(campaignUnit.getIa_icon())) {
            com.mintegral.msdk.base.common.c.b.a(com.mintegral.msdk.base.controller.a.d().h()).a(campaignUnit.getIa_icon(), new C0058a(aVar, campaignUnit, aVar.c));
        }
    }

    static /* synthetic */ void a(a aVar, List list) {
        com.mintegral.msdk.base.utils.g.b("InteractiveAdsAdapter", "onload 开始 更新本机已安装广告列表");
        if (aVar.b == null || list == null || list.size() == 0) {
            com.mintegral.msdk.base.utils.g.b("InteractiveAdsAdapter", "onload 列表为空 不做更新本机已安装广告列表");
            return;
        }
        m a2 = m.a(i.a(aVar.b));
        boolean z = false;
        for (int i2 = 0; i2 < list.size(); i2++) {
            CampaignEx campaignEx = (CampaignEx) list.get(i2);
            if (campaignEx != null) {
                if (com.mintegral.msdk.base.utils.k.a(aVar.b, campaignEx.getPackageName())) {
                    if (com.mintegral.msdk.base.controller.a.c() != null) {
                        com.mintegral.msdk.base.controller.a.c().add(new h(campaignEx.getId(), campaignEx.getPackageName()));
                        z = true;
                    }
                } else if (a2 != null && !a2.a(campaignEx.getId())) {
                    com.mintegral.msdk.base.entity.g gVar = new com.mintegral.msdk.base.entity.g();
                    gVar.a(campaignEx.getId());
                    gVar.a(campaignEx.getFca());
                    gVar.b(campaignEx.getFcb());
                    gVar.g();
                    gVar.e();
                    gVar.a(System.currentTimeMillis());
                    a2.a(gVar);
                }
            }
        }
        if (z) {
            com.mintegral.msdk.base.utils.g.b("InteractiveAdsAdapter", "更新安装列表");
            com.mintegral.msdk.base.controller.a.d().f();
        }
    }
}
