package com.mintegral.msdk.interactiveads.a;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.b.f;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.videocommon.download.j;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/* compiled from: LoadLogicManager */
public class b {
    private static volatile b a;
    private WeakReference<Context> b;

    private b(Context context) {
        this.b = new WeakReference<>(context);
    }

    public static b a(Context context) {
        if (a == null) {
            synchronized (b.class) {
                if (a == null) {
                    a = new b(context);
                }
            }
        }
        return a;
    }

    public final List<CampaignEx> a(String str) {
        List<CampaignEx> c = c(str);
        ArrayList arrayList = new ArrayList();
        if (c != null && c.size() > 0) {
            for (CampaignEx next : c) {
                if (next.getInteractiveCache().equals("onelevel")) {
                    arrayList.add(next);
                }
            }
        }
        return arrayList;
    }

    public static boolean a(List<CampaignEx> list) {
        for (CampaignEx next : list) {
            boolean z = next.getIsDownLoadZip() == 1;
            boolean isEmpty = TextUtils.isEmpty(j.a().a(next.getKeyIaUrl()));
            if (z) {
                if (isEmpty) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    public final List<CampaignEx> b(String str) {
        List<CampaignEx> c = c(str);
        ArrayList arrayList = new ArrayList();
        if (c != null && c.size() > 0) {
            for (CampaignEx next : c) {
                if (!TextUtils.isEmpty(next.getInteractiveCache()) && next.getInteractiveCache().equals("twolevel")) {
                    arrayList.add(next);
                }
            }
        }
        return arrayList;
    }

    private List<CampaignEx> c(String str) {
        List<CampaignEx> h;
        ArrayList arrayList = null;
        try {
            if (TextUtils.isEmpty(str) || (h = f.a(i.a(this.b.get())).h(str)) == null) {
                return null;
            }
            ArrayList arrayList2 = new ArrayList();
            try {
                for (CampaignEx next : h) {
                    if (next != null) {
                        arrayList2.add(next);
                    }
                }
                return arrayList2;
            } catch (Exception e) {
                e = e;
                arrayList = arrayList2;
                e.printStackTrace();
                return arrayList;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return arrayList;
        }
    }
}
