package com.mintegral.msdk.interactiveads.f;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.common.e.c;
import com.mintegral.msdk.base.common.e.d.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.q;
import com.mintegral.msdk.base.utils.g;
import java.util.List;

/* compiled from: ReportUtils */
public final class a {
    /* access modifiers changed from: private */
    public static String a = "ReportUtils";

    public static void a(Context context, String str) {
        if (context != null) {
            try {
                w a2 = w.a(i.a(context));
                if (!TextUtils.isEmpty(str) && a2 != null && a2.c() > 0) {
                    List<q> a3 = a2.a("2000054");
                    String d = q.d(a2.a("2000043"));
                    String a4 = q.a(a3);
                    StringBuilder sb = new StringBuilder();
                    if (!TextUtils.isEmpty(a4)) {
                        sb.append(a4);
                    }
                    if (!TextUtils.isEmpty(d)) {
                        sb.append(d);
                    }
                    if (!TextUtils.isEmpty(sb.toString())) {
                        a(context, sb.toString(), str);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void a(Context context, String str, String str2) {
        if (context != null && !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            try {
                com.mintegral.msdk.base.common.e.d.a aVar = new com.mintegral.msdk.base.common.e.d.a(context);
                aVar.c();
                aVar.b(com.mintegral.msdk.base.common.a.f, c.a(str, context, str2), new b() {
                    public final void b(String str) {
                        g.d(a.a, str);
                    }

                    public final void a(String str) {
                        g.d(a.a, str);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                g.d(a, e.getMessage());
            }
        }
    }

    public static void a(Context context, CampaignEx campaignEx, int i, String str) {
        if (context != null) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000055&");
                    if (campaignEx != null && campaignEx.getKeyIaRst() == 1) {
                        stringBuffer.append("cid=" + campaignEx.getId() + Constants.RequestParameters.AMPERSAND);
                    }
                    stringBuffer.append("network_type=" + com.mintegral.msdk.base.utils.c.s(context) + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("unit_id=" + str + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("action=" + i + Constants.RequestParameters.AMPERSAND);
                    if (campaignEx != null) {
                        if (campaignEx.getKeyIaRst() == 1) {
                            stringBuffer.append("rid_n=" + campaignEx.getRequestIdNotice() + Constants.RequestParameters.AMPERSAND);
                        } else if (campaignEx.getKeyIaRst() == 2) {
                            stringBuffer.append("rid_n=" + campaignEx.getRequestId() + Constants.RequestParameters.AMPERSAND);
                        }
                    }
                    stringBuffer.append("ad_type=4");
                    a(context, stringBuffer.toString(), str);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static void a(Context context, CampaignEx campaignEx, String str) {
        if (context != null) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000048&");
                    if (campaignEx != null && campaignEx.getKeyIaRst() == 1) {
                        stringBuffer.append("cid=" + campaignEx.getId() + Constants.RequestParameters.AMPERSAND);
                    }
                    stringBuffer.append("network_type=" + com.mintegral.msdk.base.utils.c.s(context) + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("unit_id=" + str + Constants.RequestParameters.AMPERSAND);
                    if (campaignEx != null) {
                        if (campaignEx.getKeyIaRst() == 1) {
                            stringBuffer.append("rid_n=" + campaignEx.getRequestIdNotice());
                        } else {
                            stringBuffer.append("rid_n=" + campaignEx.getRequestId());
                        }
                    }
                    String stringBuffer2 = stringBuffer.toString();
                    if (context != null && !TextUtils.isEmpty(stringBuffer2) && !TextUtils.isEmpty(str)) {
                        com.mintegral.msdk.base.common.e.d.a aVar = new com.mintegral.msdk.base.common.e.d.a(context);
                        aVar.c();
                        aVar.b(com.mintegral.msdk.base.common.a.f, c.a(stringBuffer2, context, str), new b() {
                            public final void b(String str) {
                                g.d(a.a, str);
                            }

                            public final void a(String str) {
                                g.d(a.a, str);
                            }
                        });
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                g.d(a, e.getMessage());
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static void a(Context context, String str, String str2, CampaignEx campaignEx) {
        if (context != null) {
            try {
                if ((!TextUtils.isEmpty(str2)) && (!TextUtils.isEmpty(str))) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000047&");
                    stringBuffer.append("network_type=" + com.mintegral.msdk.base.utils.c.s(context) + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("unit_id=" + str2 + Constants.RequestParameters.AMPERSAND);
                    if (campaignEx != null) {
                        if (campaignEx.getKeyIaRst() == 1) {
                            stringBuffer.append("rid_n=" + campaignEx.getRequestIdNotice() + Constants.RequestParameters.AMPERSAND);
                        } else if (campaignEx.getKeyIaRst() == 2) {
                            stringBuffer.append("rid_n=" + campaignEx.getRequestId() + Constants.RequestParameters.AMPERSAND);
                        }
                        stringBuffer.append("cid=" + campaignEx.getId() + Constants.RequestParameters.AMPERSAND);
                    }
                    if (!TextUtils.isEmpty(com.mintegral.msdk.base.common.a.D)) {
                        stringBuffer.append("sys_id=" + com.mintegral.msdk.base.common.a.D + Constants.RequestParameters.AMPERSAND);
                    }
                    if (!TextUtils.isEmpty(com.mintegral.msdk.base.common.a.E)) {
                        stringBuffer.append("bkup_id=" + com.mintegral.msdk.base.common.a.E + Constants.RequestParameters.AMPERSAND);
                    }
                    stringBuffer.append("reason=" + str);
                    String stringBuffer2 = stringBuffer.toString();
                    if (context != null && !TextUtils.isEmpty(stringBuffer2)) {
                        com.mintegral.msdk.base.common.e.d.a aVar = new com.mintegral.msdk.base.common.e.d.a(context);
                        aVar.c();
                        aVar.b(com.mintegral.msdk.base.common.a.f, c.b(stringBuffer2, context), new b() {
                            public final void b(String str) {
                                g.d(a.a, str);
                            }

                            public final void a(String str) {
                                g.d(a.a, str);
                            }
                        });
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                g.d(a, e.getMessage());
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }
}
