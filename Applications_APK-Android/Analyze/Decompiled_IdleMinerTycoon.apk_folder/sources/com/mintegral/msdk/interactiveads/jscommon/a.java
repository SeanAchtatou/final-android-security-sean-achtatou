package com.mintegral.msdk.interactiveads.jscommon;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.d.b;
import com.mintegral.msdk.d.d;
import com.tapjoy.TapjoyConstants;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: JSCommon */
public final class a {
    protected String a;
    protected d b = null;
    private String c = "JSCommon";
    private Activity d;
    private List<CampaignEx> e;
    private int f;
    private int g = 2;

    public a(Activity activity, List<CampaignEx> list) {
        this.d = activity;
        this.e = list;
    }

    public final void a(d dVar) {
        this.b = dVar;
    }

    public final void a(int i) {
        this.f = i;
    }

    public final void a(String str) {
        String str2 = this.c;
        g.a(str2, "setUnitId:" + str);
        this.a = str;
    }

    public final String a() {
        return this.a;
    }

    public final String b() {
        JSONObject jSONObject = new JSONObject();
        C0059a aVar = new C0059a(com.mintegral.msdk.base.controller.a.d().h());
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("playVideoMute", this.g);
            jSONObject.put("sdkSetting", jSONObject2);
            jSONObject.put("device", aVar.a());
            jSONObject.put("campaignList", CampaignEx.parseCamplistToJson(this.e));
            jSONObject.put("unitSetting", e());
            String j = com.mintegral.msdk.base.controller.a.d().j();
            b.a();
            String c2 = b.c(j);
            if (!TextUtils.isEmpty(c2)) {
                jSONObject.put("appSetting", new JSONObject(c2));
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return jSONObject.toString();
    }

    public final List<CampaignEx> c() {
        return this.e;
    }

    public final void a(List<CampaignEx> list) {
        this.e = list;
    }

    public final Activity d() {
        return this.d;
    }

    public final void a(Activity activity) {
        this.d = activity;
    }

    public final JSONObject e() {
        if (this.b != null) {
            return this.b.x();
        }
        return new JSONObject();
    }

    /* renamed from: com.mintegral.msdk.interactiveads.jscommon.a$a  reason: collision with other inner class name */
    /* compiled from: JSCommon */
    private static final class C0059a {
        public String a = c.d();
        public String b = c.i();
        public String c = "android";
        public String d;
        public String e;
        public String f;
        public String g;
        public String h;
        public String i;
        public String j;
        public String k;
        public String l;
        public String m;
        public String n;
        public String o;
        public String p;

        public C0059a(Context context) {
            this.d = c.c(context);
            this.e = c.f(context);
            this.f = c.k();
            int s = c.s(context);
            this.h = String.valueOf(s);
            this.i = c.a(context, s);
            this.j = c.r(context);
            this.k = com.mintegral.msdk.base.controller.a.d().k();
            this.l = com.mintegral.msdk.base.controller.a.d().j();
            this.m = String.valueOf(k.i(context));
            this.n = String.valueOf(k.h(context));
            this.p = String.valueOf(k.c(context));
            if (context.getResources().getConfiguration().orientation == 2) {
                this.o = "landscape";
            } else {
                this.o = "portrait";
            }
            this.g = c.b(context);
        }

        public final JSONObject a() {
            JSONObject jSONObject = new JSONObject();
            try {
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                    jSONObject.put("device", this.a);
                    jSONObject.put("system_version", this.b);
                    jSONObject.put("network_type", this.h);
                    jSONObject.put("network_type_str", this.i);
                    jSONObject.put("device_ua", this.j);
                }
                jSONObject.put("plantform", this.c);
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITYIMEIMAC)) {
                    jSONObject.put("device_imei", this.d);
                }
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_ANDROID_ID)) {
                    jSONObject.put(TapjoyConstants.TJC_ANDROID_ID, this.e);
                }
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
                    jSONObject.put("google_ad_id", this.f);
                    jSONObject.put("oaid", this.g);
                }
                jSONObject.put("appkey", this.k);
                jSONObject.put("appId", this.l);
                jSONObject.put("screen_width", this.m);
                jSONObject.put("screen_height", this.n);
                jSONObject.put("orientation", this.o);
                jSONObject.put("scale", this.p);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            return jSONObject;
        }
    }
}
