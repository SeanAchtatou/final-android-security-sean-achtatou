package com.mintegral.msdk.interactiveads.view;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.interactiveads.activity.InteractiveShowActivity;
import com.mintegral.msdk.interactiveads.activity.InteractiveWebView;
import com.mintegral.msdk.interactiveads.f.a;

public class MTGEntrancePictureView extends LinearLayout {
    /* access modifiers changed from: private */
    public String a = "";

    public void setUnitId(String str) {
        this.a = str;
    }

    public MTGEntrancePictureView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
    }

    public void refreshUI(final Context context, String str, final CampaignEx campaignEx) {
        if (getChildCount() > 0) {
            removeAllViews();
        }
        if (!TextUtils.isEmpty(str)) {
            InteractiveWebView interactiveWebView = new InteractiveWebView(context);
            interactiveWebView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
            interactiveWebView.loadDataWithBaseURL(null, "<!DOCTYPE html><html lang=\"en\"><head>  <meta charset=\"UTF-8\">  <meta name=\"viewport\" content=\"width=100, initial-scale=1.0\"><meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">  <title>Document</title>  <style>  *{    margin: 0;    padding: 0;  }  html, body{    width: 100%;    height: 100%;  }  body{    background-image: url('gifUrl');    background-position: center;    background-size: contain;    background-repeat: no-repeat;  }  </style></head><body></body></html>".replace("gifUrl", str), "text/html", "utf-8", null);
            addView(interactiveWebView);
            setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    if (MTGEntrancePictureView.this.a == null) {
                        g.a("MTGEntrancePictureView", "InteractiveShowActivity unitId == null");
                        return;
                    }
                    a.a(context, campaignEx, 2, MTGEntrancePictureView.this.a);
                    Intent intent = new Intent(context, InteractiveShowActivity.class);
                    com.mintegral.msdk.interactiveads.c.a.f = true;
                    intent.putExtra("unitId", MTGEntrancePictureView.this.a);
                    intent.putExtra("campaign", campaignEx);
                    context.startActivity(intent);
                }
            });
            return;
        }
        g.d("MTGEntrancePictureView", "MTGEntrancePictureView update failed, imageUrl == null ");
    }
}
