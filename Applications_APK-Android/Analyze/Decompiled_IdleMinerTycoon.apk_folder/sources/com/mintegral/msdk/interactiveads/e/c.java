package com.mintegral.msdk.interactiveads.e;

import android.support.v4.app.NotificationCompat;
import com.mintegral.msdk.base.common.net.a.a;
import com.mintegral.msdk.base.entity.CampaignUnit;
import org.json.JSONObject;

/* compiled from: InteractiveResponseHandler */
public abstract class c extends a {
    public abstract void b(CampaignUnit campaignUnit);

    public abstract void h();

    public final /* synthetic */ void a(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        if (1 == jSONObject.optInt("status")) {
            a(System.currentTimeMillis());
            CampaignUnit parseCampaignUnit = CampaignUnit.parseCampaignUnit(jSONObject.optJSONObject("data"));
            if (parseCampaignUnit == null || parseCampaignUnit.getAds() == null || parseCampaignUnit.getAds().size() <= 0) {
                jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE);
                h();
                return;
            }
            b(parseCampaignUnit);
            a(parseCampaignUnit.getAds().size());
            return;
        }
        jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE);
        h();
    }

    public final void a(String str) {
        h();
    }
}
