package com.mintegral.msdk.interactiveads;

public final class R {
    private R() {
    }

    public static final class anim {
        public static final int mintegral_anim_scale = 2130771988;

        private anim() {
        }
    }

    public static final class drawable {
        public static final int mintegral_close = 2131165395;
        public static final int mintegral_close_background = 2131165396;
        public static final int mintegral_loading_bg = 2131165418;

        private drawable() {
        }
    }

    public static final class id {
        public static final int mintegral_close = 2131230953;
        public static final int mintegral_close_imageview = 2131230954;
        public static final int mintegral_left = 2131230969;
        public static final int mintegral_middle = 2131230971;
        public static final int mintegral_right = 2131230975;
        public static final int mintegral_video_templete_container = 2131230997;
        public static final int mintegral_video_templete_progressbar = 2131230998;
        public static final int mintegral_video_templete_videoview = 2131230999;
        public static final int mintegral_video_templete_webview_parent = 2131231000;

        private id() {
        }
    }

    public static final class layout {
        public static final int mintegral_close_imageview_layout = 2131427443;
        public static final int mintegral_loading_dialog = 2131427446;
        public static final int mintegral_loading_view = 2131427447;
        public static final int mintegral_reward_activity_video_templete = 2131427449;

        private layout() {
        }
    }
}
