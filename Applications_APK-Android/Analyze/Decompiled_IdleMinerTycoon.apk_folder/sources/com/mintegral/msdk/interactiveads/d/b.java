package com.mintegral.msdk.interactiveads.d;

import com.mintegral.msdk.base.entity.CampaignEx;
import java.util.List;
import org.json.JSONArray;

/* compiled from: InteractiveStatusListener */
public interface b {
    void a(int i);

    void a(CampaignEx campaignEx);

    void a(List<CampaignEx> list);

    void a(JSONArray jSONArray, d dVar);

    void a(boolean z);

    void c(String str);

    void d();

    void d(String str);

    void e();

    void f();
}
