package com.mintegral.msdk.interactiveads.a;

import android.content.Context;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.b.f;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.d.b;
import com.mintegral.msdk.d.d;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

/* compiled from: PrepareParamManager */
public class c {
    private static volatile c b;
    public JSONArray a;
    private WeakReference<Context> c;
    private boolean d;

    private c(Context context) {
        this.c = new WeakReference<>(context);
    }

    public static c a(Context context) {
        if (b == null) {
            synchronized (c.class) {
                if (b == null) {
                    b = new c(context);
                }
            }
        }
        return b;
    }

    public static d a() {
        String j = a.d().j();
        String a2 = com.mintegral.msdk.base.a.a.a.a().a("interactive_unitid");
        b.a();
        return b.c(j, a2);
    }

    public final List<CampaignEx> a(String str) {
        List<CampaignEx> h;
        ArrayList arrayList = null;
        try {
            if (TextUtils.isEmpty(str) || (h = f.a(i.a(this.c.get())).h(str)) == null) {
                return null;
            }
            ArrayList arrayList2 = new ArrayList();
            try {
                for (CampaignEx next : h) {
                    if (next != null) {
                        arrayList2.add(next);
                    }
                }
                return arrayList2;
            } catch (Exception e) {
                e = e;
                arrayList = arrayList2;
                e.printStackTrace();
                return arrayList;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return arrayList;
        }
    }

    public final String b() {
        long[] c2;
        String str = "";
        try {
            b.a();
            com.mintegral.msdk.d.a b2 = b.b(a.d().j());
            JSONArray jSONArray = new JSONArray();
            if (!(b2 == null || b2.aK() != 1 || (c2 = m.a(i.a(a.d().h())).c()) == null)) {
                for (long put : c2) {
                    jSONArray.put(put);
                }
            }
            if (jSONArray.length() > 0) {
                str = k.a(jSONArray);
            }
            List<String> a2 = a(this.a);
            if (a2 == null) {
                return str;
            }
            String[] strArr = null;
            if (str != null && str.length() > 0) {
                int length = str.length();
                int indexOf = str.indexOf(Constants.RequestParameters.LEFT_BRACKETS) + 1;
                int indexOf2 = str.indexOf(Constants.RequestParameters.RIGHT_BRACKETS);
                if (indexOf <= length && indexOf2 <= length && indexOf <= indexOf2) {
                    str = str.subSequence(indexOf, indexOf2).toString();
                }
                strArr = str.split(",");
            }
            if (strArr != null && strArr.length > 0) {
                for (int i = 0; i < strArr.length; i++) {
                    a2.add(i, strArr[i]);
                }
            }
            return a2.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return str;
        }
    }

    public final String b(String str) {
        ArrayList arrayList = new ArrayList();
        List<CampaignEx> a2 = a(str);
        if (a2 != null) {
            for (CampaignEx id : a2) {
                arrayList.add(id.getId());
            }
        }
        return arrayList.toString();
    }

    public final String c() {
        try {
            return com.mintegral.msdk.base.b.k.a(i.a(this.c.get())).c();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static int d() {
        String a2 = com.mintegral.msdk.base.a.a.a.a().a(com.mintegral.msdk.interactiveads.b.a.a);
        if (a2 == null || a2.equals("")) {
            return 0;
        }
        return Integer.parseInt(com.mintegral.msdk.base.a.a.a.a().a(com.mintegral.msdk.interactiveads.b.a.a));
    }

    public static int e() {
        String a2 = com.mintegral.msdk.base.a.a.a.a().a(com.mintegral.msdk.interactiveads.b.a.b);
        if (a2 == null || a2.equals("")) {
            return 0;
        }
        return Integer.parseInt(com.mintegral.msdk.base.a.a.a.a().a(com.mintegral.msdk.interactiveads.b.a.b));
    }

    public final boolean f() {
        return this.d;
    }

    public final void a(boolean z) {
        this.d = z;
    }

    public static void g() {
        String a2 = com.mintegral.msdk.base.a.a.a.a().a(com.mintegral.msdk.interactiveads.b.a.a);
        com.mintegral.msdk.base.a.a.a.a().a(com.mintegral.msdk.interactiveads.b.a.a, String.valueOf(((a2 == null || a2.equals("")) ? 0 : Integer.parseInt(com.mintegral.msdk.base.a.a.a.a().a(com.mintegral.msdk.interactiveads.b.a.a))) + 1));
    }

    public static void h() {
        String a2 = com.mintegral.msdk.base.a.a.a.a().a(com.mintegral.msdk.interactiveads.b.a.b);
        com.mintegral.msdk.base.a.a.a.a().a(com.mintegral.msdk.interactiveads.b.a.b, String.valueOf(((a2 == null || a2.equals("")) ? 0 : Integer.parseInt(com.mintegral.msdk.base.a.a.a.a().a(com.mintegral.msdk.interactiveads.b.a.b))) + 1));
    }

    public static void i() {
        com.mintegral.msdk.base.a.a.a.a().a(com.mintegral.msdk.interactiveads.b.a.a, AppEventsConstants.EVENT_PARAM_VALUE_NO);
        com.mintegral.msdk.base.a.a.a.a().a(com.mintegral.msdk.interactiveads.b.a.b, AppEventsConstants.EVENT_PARAM_VALUE_NO);
    }

    private static List<String> a(JSONArray jSONArray) {
        if (jSONArray == null) {
            return null;
        }
        int length = jSONArray.length();
        ArrayList arrayList = new ArrayList(length);
        for (int i = 0; i < length; i++) {
            try {
                arrayList.add((String) jSONArray.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return arrayList;
    }
}
