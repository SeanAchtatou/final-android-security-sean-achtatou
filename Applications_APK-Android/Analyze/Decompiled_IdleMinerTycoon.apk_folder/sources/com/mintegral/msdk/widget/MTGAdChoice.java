package com.mintegral.msdk.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.mintegral.msdk.base.utils.j;

public class MTGAdChoice extends MTGImageView {
    /* access modifiers changed from: private */
    public static String a = "MTGAdChoice";
    private String b = "";
    private String c = "";
    private String d = "";
    private Context e;

    public MTGAdChoice(Context context) {
        super(context);
        this.e = context;
        b();
    }

    public MTGAdChoice(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.e = context;
        b();
    }

    public MTGAdChoice(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.e = context;
        b();
    }

    private void b() {
        setScaleType(ImageView.ScaleType.FIT_CENTER);
        setClickable(true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0043  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setCampaign(com.mintegral.msdk.out.Campaign r4) {
        /*
            r3 = this;
            boolean r0 = r4 instanceof com.mintegral.msdk.base.entity.CampaignEx
            if (r0 == 0) goto L_0x0094
            com.mintegral.msdk.base.entity.CampaignEx r4 = (com.mintegral.msdk.base.entity.CampaignEx) r4
            r0 = 1
            r1 = 0
            if (r4 == 0) goto L_0x0040
            com.mintegral.msdk.base.entity.CampaignEx$a r2 = r4.getAdchoice()
            if (r2 == 0) goto L_0x0040
            com.mintegral.msdk.base.entity.CampaignEx$a r2 = r4.getAdchoice()
            java.lang.String r2 = r2.e()
            r3.b = r2
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x0040
            com.mintegral.msdk.base.entity.CampaignEx$a r2 = r4.getAdchoice()
            java.lang.String r2 = r2.d()
            r3.d = r2
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x0040
            com.mintegral.msdk.base.entity.CampaignEx$a r4 = r4.getAdchoice()
            java.lang.String r4 = r4.f()
            boolean r4 = android.text.TextUtils.isEmpty(r4)
            if (r4 != 0) goto L_0x0040
            r4 = 1
            goto L_0x0041
        L_0x0040:
            r4 = 0
        L_0x0041:
            if (r4 != 0) goto L_0x0079
            com.mintegral.msdk.d.b.a()
            com.mintegral.msdk.base.controller.a r4 = com.mintegral.msdk.base.controller.a.d()
            java.lang.String r4 = r4.j()
            com.mintegral.msdk.d.a r4 = com.mintegral.msdk.d.b.b(r4)
            if (r4 == 0) goto L_0x0078
            java.lang.String r2 = r4.bc()
            r3.b = r2
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x0078
            java.lang.String r2 = r4.be()
            r3.d = r2
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x0078
            java.lang.String r4 = r4.bd()
            boolean r4 = android.text.TextUtils.isEmpty(r4)
            if (r4 != 0) goto L_0x0078
            r4 = 1
            goto L_0x0079
        L_0x0078:
            r4 = 0
        L_0x0079:
            java.lang.String r0 = r3.b
            r3.setImageUrl(r0)
            if (r4 == 0) goto L_0x0094
            android.content.Context r4 = r3.e
            if (r4 == 0) goto L_0x0094
            android.content.Context r4 = r3.e
            com.mintegral.msdk.base.common.c.b r4 = com.mintegral.msdk.base.common.c.b.a(r4)
            java.lang.String r0 = r3.b
            com.mintegral.msdk.widget.MTGAdChoice$1 r1 = new com.mintegral.msdk.widget.MTGAdChoice$1
            r1.<init>()
            r4.a(r0, r1)
        L_0x0094:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.widget.MTGAdChoice.setCampaign(com.mintegral.msdk.out.Campaign):void");
    }

    public boolean performClick() {
        if (TextUtils.isEmpty(this.d)) {
            return true;
        }
        j.b(this.e, this.d, null);
        return true;
    }
}
