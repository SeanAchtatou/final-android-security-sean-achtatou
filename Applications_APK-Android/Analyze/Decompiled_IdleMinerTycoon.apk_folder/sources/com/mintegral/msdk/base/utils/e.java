package com.mintegral.msdk.base.utils;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.b.c;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.d.b;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Comparator;

/* compiled from: CommonFileUtil */
public final class e {
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0038 A[SYNTHETIC, Splitter:B:24:0x0038] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x003e A[SYNTHETIC, Splitter:B:28:0x003e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static long c(java.io.File r4) throws java.lang.Exception {
        /*
            r0 = 0
            r2 = 0
            boolean r3 = r4.exists()     // Catch:{ Exception -> 0x0032 }
            if (r3 == 0) goto L_0x001b
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0032 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0032 }
            int r4 = r3.available()     // Catch:{ Exception -> 0x0018, all -> 0x0015 }
            long r0 = (long) r4
            r2 = r3
            goto L_0x0025
        L_0x0015:
            r4 = move-exception
            r2 = r3
            goto L_0x003c
        L_0x0018:
            r4 = move-exception
            r2 = r3
            goto L_0x0033
        L_0x001b:
            r4.createNewFile()     // Catch:{ Exception -> 0x0032 }
            java.lang.String r4 = "获取文件大小"
            java.lang.String r3 = "文件不存在!"
            com.mintegral.msdk.base.utils.g.d(r4, r3)     // Catch:{ Exception -> 0x0032 }
        L_0x0025:
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ Exception -> 0x002b }
            goto L_0x003b
        L_0x002b:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x003b
        L_0x0030:
            r4 = move-exception
            goto L_0x003c
        L_0x0032:
            r4 = move-exception
        L_0x0033:
            r4.printStackTrace()     // Catch:{ all -> 0x0030 }
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ Exception -> 0x002b }
        L_0x003b:
            return r0
        L_0x003c:
            if (r2 == 0) goto L_0x0046
            r2.close()     // Catch:{ Exception -> 0x0042 }
            goto L_0x0046
        L_0x0042:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0046:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.utils.e.c(java.io.File):long");
    }

    private static long d(File file) throws Exception {
        long j;
        File[] listFiles = file.listFiles();
        long j2 = 0;
        if (listFiles != null) {
            for (int i = 0; i < listFiles.length; i++) {
                if (listFiles[i].isDirectory()) {
                    j = d(listFiles[i]);
                } else {
                    j = c(listFiles[i]);
                }
                j2 += j;
            }
        }
        return j2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0040, code lost:
        if (r1 == null) goto L_0x0043;
     */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x004e A[SYNTHETIC, Splitter:B:30:0x004e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.io.File r4) {
        /*
            r0 = 0
            if (r4 != 0) goto L_0x0004
            return r0
        L_0x0004:
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x003a, all -> 0x0037 }
            java.io.FileReader r2 = new java.io.FileReader     // Catch:{ IOException -> 0x003a, all -> 0x0037 }
            r2.<init>(r4)     // Catch:{ IOException -> 0x003a, all -> 0x0037 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x003a, all -> 0x0037 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x0034 }
            r4.<init>()     // Catch:{ IOException -> 0x0034 }
        L_0x0013:
            java.lang.String r2 = r1.readLine()     // Catch:{ IOException -> 0x0032 }
            if (r2 == 0) goto L_0x002e
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0032 }
            r3.<init>()     // Catch:{ IOException -> 0x0032 }
            r3.append(r2)     // Catch:{ IOException -> 0x0032 }
            java.lang.String r2 = "\n"
            r3.append(r2)     // Catch:{ IOException -> 0x0032 }
            java.lang.String r2 = r3.toString()     // Catch:{ IOException -> 0x0032 }
            r4.append(r2)     // Catch:{ IOException -> 0x0032 }
            goto L_0x0013
        L_0x002e:
            r1.close()     // Catch:{ IOException -> 0x0043 }
            goto L_0x0043
        L_0x0032:
            r2 = move-exception
            goto L_0x003d
        L_0x0034:
            r2 = move-exception
            r4 = r0
            goto L_0x003d
        L_0x0037:
            r4 = move-exception
            r1 = r0
            goto L_0x004c
        L_0x003a:
            r2 = move-exception
            r4 = r0
            r1 = r4
        L_0x003d:
            r2.printStackTrace()     // Catch:{ all -> 0x004b }
            if (r1 == 0) goto L_0x0043
            goto L_0x002e
        L_0x0043:
            if (r4 == 0) goto L_0x004a
            java.lang.String r4 = r4.toString()
            return r4
        L_0x004a:
            return r0
        L_0x004b:
            r4 = move-exception
        L_0x004c:
            if (r1 == 0) goto L_0x0051
            r1.close()     // Catch:{ IOException -> 0x0051 }
        L_0x0051:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.utils.e.a(java.io.File):java.lang.String");
    }

    public static File[] b(String str) {
        try {
            File file = new File(str);
            if (file.exists()) {
                return file.listFiles();
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }

    public static void a() {
        new Thread(new Runnable() {
            public final void run() {
                try {
                    if (a.d().h() != null) {
                        String b = com.mintegral.msdk.base.common.b.e.b(c.MINTEGRAL_VC);
                        try {
                            File file = new File(b);
                            if (file.exists() && file.isDirectory()) {
                                for (File file2 : e.b(b)) {
                                    if (file2.exists() && file2.isFile()) {
                                        file2.delete();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e2) {
                    if (MIntegralConstans.DEBUG) {
                        e2.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public static void c(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                File file = new File(str);
                if (file.exists() && file.isDirectory()) {
                    long currentTimeMillis = System.currentTimeMillis();
                    File[] listFiles = file.listFiles();
                    if (listFiles != null) {
                        for (File file2 : listFiles) {
                            if (file2.lastModified() + 259200000 < currentTimeMillis) {
                                b(file2);
                            }
                        }
                    }
                }
            }
        } catch (Exception unused) {
        }
    }

    public static void b() {
        new Thread(new Runnable() {
            public final void run() {
                try {
                    if (a.d().h() != null) {
                        e.d(com.mintegral.msdk.base.common.b.e.b(c.MINTEGRAL_VC));
                    }
                    b.a();
                    com.mintegral.msdk.d.a b = b.b(a.d().j());
                    if (b == null) {
                        b.a();
                        b = b.b();
                    }
                    long currentTimeMillis = System.currentTimeMillis() - ((long) (b.ao() * 1000));
                    try {
                        for (File file : new File(com.mintegral.msdk.base.common.b.e.b(c.MINTEGRAL_VC)).listFiles(new FileFilter() {
                            public final boolean accept(File file) {
                                return !file.isHidden();
                            }
                        })) {
                            if (file.lastModified() < currentTimeMillis && file.exists() && file.isFile()) {
                                file.delete();
                            }
                        }
                    } catch (Throwable th) {
                        g.c("CommonFileUtil", th.getMessage(), th);
                    }
                } catch (Exception e) {
                    if (MIntegralConstans.DEBUG) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public static String b(File file) {
        try {
            if (file.isFile()) {
                file.delete();
                return "";
            } else if (!file.isDirectory()) {
                return "";
            } else {
                File[] listFiles = file.listFiles();
                if (listFiles != null) {
                    if (listFiles.length != 0) {
                        for (File b : listFiles) {
                            b(b);
                        }
                        file.delete();
                        return "";
                    }
                }
                file.delete();
                return "";
            }
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    private static void e(String str) {
        try {
            File[] listFiles = new File(str).listFiles(new FileFilter() {
                public final boolean accept(File file) {
                    return !file.isHidden();
                }
            });
            Arrays.sort(listFiles, new Comparator<File>() {
                public final boolean equals(Object obj) {
                    return true;
                }

                public final /* synthetic */ int compare(Object obj, Object obj2) {
                    long lastModified = ((File) obj).lastModified() - ((File) obj2).lastModified();
                    if (lastModified > 0) {
                        return 1;
                    }
                    return lastModified == 0 ? 0 : -1;
                }
            });
            int length = (listFiles.length - 1) / 2;
            for (int i = 0; i < length; i++) {
                File file = listFiles[i];
                if (file.exists() && file.isFile()) {
                    file.delete();
                }
            }
        } catch (Exception unused) {
            g.d("CommonFileUtil", "del memory failed");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0034 A[SYNTHETIC, Splitter:B:23:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0040 A[SYNTHETIC, Splitter:B:28:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(byte[] r2, java.io.File r3) {
        /*
            r0 = 0
            java.io.File r1 = r3.getParentFile()     // Catch:{ Exception -> 0x002e }
            if (r1 == 0) goto L_0x0014
            boolean r1 = r3.exists()     // Catch:{ Exception -> 0x002e }
            if (r1 != 0) goto L_0x0014
            java.io.File r1 = r3.getParentFile()     // Catch:{ Exception -> 0x002e }
            r1.mkdirs()     // Catch:{ Exception -> 0x002e }
        L_0x0014:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x002e }
            r1.<init>(r3)     // Catch:{ Exception -> 0x002e }
            r1.write(r2)     // Catch:{ Exception -> 0x0029, all -> 0x0026 }
            r1.close()     // Catch:{ IOException -> 0x0020 }
            goto L_0x0024
        L_0x0020:
            r2 = move-exception
            r2.printStackTrace()
        L_0x0024:
            r2 = 1
            return r2
        L_0x0026:
            r2 = move-exception
            r0 = r1
            goto L_0x003e
        L_0x0029:
            r2 = move-exception
            r0 = r1
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            goto L_0x003e
        L_0x002e:
            r2 = move-exception
        L_0x002f:
            r2.printStackTrace()     // Catch:{ all -> 0x002c }
            if (r0 == 0) goto L_0x003c
            r0.close()     // Catch:{ IOException -> 0x0038 }
            goto L_0x003c
        L_0x0038:
            r2 = move-exception
            r2.printStackTrace()
        L_0x003c:
            r2 = 0
            return r2
        L_0x003e:
            if (r0 == 0) goto L_0x0048
            r0.close()     // Catch:{ IOException -> 0x0044 }
            goto L_0x0048
        L_0x0044:
            r3 = move-exception
            r3.printStackTrace()
        L_0x0048:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.utils.e.a(byte[], java.io.File):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:76:0x00dc A[Catch:{ all -> 0x00f8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x00e5 A[SYNTHETIC, Splitter:B:79:0x00e5] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x00ef A[SYNTHETIC, Splitter:B:84:0x00ef] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x00fb A[SYNTHETIC, Splitter:B:91:0x00fb] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0105 A[SYNTHETIC, Splitter:B:96:0x0105] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r8, java.lang.String r9) {
        /*
            java.lang.String r0 = ""
            if (r8 == 0) goto L_0x010e
            if (r9 != 0) goto L_0x0008
            goto L_0x010e
        L_0x0008:
            java.lang.String r1 = "/"
            boolean r1 = r9.endsWith(r1)
            if (r1 != 0) goto L_0x0021
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r9)
            java.lang.String r9 = "/"
            r1.append(r9)
            java.lang.String r9 = r1.toString()
        L_0x0021:
            java.io.File r1 = new java.io.File
            r1.<init>(r8)
            boolean r8 = r1.exists()
            if (r8 != 0) goto L_0x002f
            java.lang.String r8 = "unzip file not exists"
            return r8
        L_0x002f:
            r8 = 0
            java.util.zip.ZipFile r2 = new java.util.zip.ZipFile     // Catch:{ IOException -> 0x00d6, all -> 0x00d3 }
            r2.<init>(r1)     // Catch:{ IOException -> 0x00d6, all -> 0x00d3 }
            java.util.Enumeration r1 = r2.entries()     // Catch:{ IOException -> 0x00d6, all -> 0x00d3 }
            r3 = r8
        L_0x003a:
            boolean r4 = r1.hasMoreElements()     // Catch:{ IOException -> 0x00d1 }
            if (r4 == 0) goto L_0x00b9
            java.lang.Object r4 = r1.nextElement()     // Catch:{ IOException -> 0x00d1 }
            java.util.zip.ZipEntry r4 = (java.util.zip.ZipEntry) r4     // Catch:{ IOException -> 0x00d1 }
            if (r4 != 0) goto L_0x005f
            java.lang.String r9 = "unzip zipEntry is null"
            if (r8 == 0) goto L_0x0054
            r8.close()     // Catch:{ IOException -> 0x0050 }
            goto L_0x0054
        L_0x0050:
            r8 = move-exception
            r8.printStackTrace()
        L_0x0054:
            if (r3 == 0) goto L_0x005e
            r3.close()     // Catch:{ IOException -> 0x005a }
            goto L_0x005e
        L_0x005a:
            r8 = move-exception
            r8.printStackTrace()
        L_0x005e:
            return r9
        L_0x005f:
            java.io.File r5 = new java.io.File     // Catch:{ IOException -> 0x00d1 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00d1 }
            r6.<init>()     // Catch:{ IOException -> 0x00d1 }
            r6.append(r9)     // Catch:{ IOException -> 0x00d1 }
            java.lang.String r7 = r4.getName()     // Catch:{ IOException -> 0x00d1 }
            r6.append(r7)     // Catch:{ IOException -> 0x00d1 }
            java.lang.String r6 = r6.toString()     // Catch:{ IOException -> 0x00d1 }
            r5.<init>(r6)     // Catch:{ IOException -> 0x00d1 }
            boolean r6 = r4.isDirectory()     // Catch:{ IOException -> 0x00d1 }
            if (r6 == 0) goto L_0x0081
            r5.mkdirs()     // Catch:{ IOException -> 0x00d1 }
            goto L_0x003a
        L_0x0081:
            java.io.File r6 = r5.getParentFile()     // Catch:{ IOException -> 0x00d1 }
            boolean r6 = r6.exists()     // Catch:{ IOException -> 0x00d1 }
            if (r6 != 0) goto L_0x0092
            java.io.File r6 = r5.getParentFile()     // Catch:{ IOException -> 0x00d1 }
            r6.mkdirs()     // Catch:{ IOException -> 0x00d1 }
        L_0x0092:
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00d1 }
            r6.<init>(r5)     // Catch:{ IOException -> 0x00d1 }
            java.io.InputStream r3 = r2.getInputStream(r4)     // Catch:{ IOException -> 0x00b6, all -> 0x00b3 }
            r8 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r8]     // Catch:{ IOException -> 0x00b0, all -> 0x00ad }
        L_0x009f:
            r5 = 0
            int r7 = r3.read(r4, r5, r8)     // Catch:{ IOException -> 0x00b0, all -> 0x00ad }
            if (r7 <= 0) goto L_0x00aa
            r6.write(r4, r5, r7)     // Catch:{ IOException -> 0x00b0, all -> 0x00ad }
            goto L_0x009f
        L_0x00aa:
            r8 = r3
            r3 = r6
            goto L_0x003a
        L_0x00ad:
            r9 = move-exception
            r8 = r3
            goto L_0x00b4
        L_0x00b0:
            r9 = move-exception
            r8 = r3
            goto L_0x00b7
        L_0x00b3:
            r9 = move-exception
        L_0x00b4:
            r3 = r6
            goto L_0x00f9
        L_0x00b6:
            r9 = move-exception
        L_0x00b7:
            r3 = r6
            goto L_0x00d8
        L_0x00b9:
            r2.close()     // Catch:{ IOException -> 0x00d1 }
            if (r8 == 0) goto L_0x00c6
            r8.close()     // Catch:{ IOException -> 0x00c2 }
            goto L_0x00c6
        L_0x00c2:
            r8 = move-exception
            r8.printStackTrace()
        L_0x00c6:
            if (r3 == 0) goto L_0x00d0
            r3.close()     // Catch:{ IOException -> 0x00cc }
            goto L_0x00d0
        L_0x00cc:
            r8 = move-exception
            r8.printStackTrace()
        L_0x00d0:
            return r0
        L_0x00d1:
            r9 = move-exception
            goto L_0x00d8
        L_0x00d3:
            r9 = move-exception
            r3 = r8
            goto L_0x00f9
        L_0x00d6:
            r9 = move-exception
            r3 = r8
        L_0x00d8:
            boolean r0 = com.mintegral.msdk.MIntegralConstans.DEBUG     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x00df
            r9.printStackTrace()     // Catch:{ all -> 0x00f8 }
        L_0x00df:
            java.lang.String r9 = r9.getMessage()     // Catch:{ all -> 0x00f8 }
            if (r8 == 0) goto L_0x00ed
            r8.close()     // Catch:{ IOException -> 0x00e9 }
            goto L_0x00ed
        L_0x00e9:
            r8 = move-exception
            r8.printStackTrace()
        L_0x00ed:
            if (r3 == 0) goto L_0x00f7
            r3.close()     // Catch:{ IOException -> 0x00f3 }
            goto L_0x00f7
        L_0x00f3:
            r8 = move-exception
            r8.printStackTrace()
        L_0x00f7:
            return r9
        L_0x00f8:
            r9 = move-exception
        L_0x00f9:
            if (r8 == 0) goto L_0x0103
            r8.close()     // Catch:{ IOException -> 0x00ff }
            goto L_0x0103
        L_0x00ff:
            r8 = move-exception
            r8.printStackTrace()
        L_0x0103:
            if (r3 == 0) goto L_0x010d
            r3.close()     // Catch:{ IOException -> 0x0109 }
            goto L_0x010d
        L_0x0109:
            r8 = move-exception
            r8.printStackTrace()
        L_0x010d:
            throw r9
        L_0x010e:
            java.lang.String r8 = "unzip srcFile or destDir is null "
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.utils.e.a(java.lang.String, java.lang.String):java.lang.String");
    }

    private static boolean f(String str) {
        try {
            Class.forName("android.os.FileUtils").getMethod("setPermissions", String.class, Integer.TYPE, Integer.TYPE, Integer.TYPE).invoke(null, str, Integer.valueOf((int) IronSourceError.ERROR_CODE_KEY_NOT_SET), -1, -1);
            return true;
        } catch (ClassNotFoundException e) {
            g.a("CommonFileUtil", "error when set permissions:", e);
            return false;
        } catch (NoSuchMethodException e2) {
            g.a("CommonFileUtil", "error when set permissions:", e2);
            return false;
        } catch (IllegalArgumentException e3) {
            g.a("CommonFileUtil", "error when set permissions:", e3);
            return false;
        } catch (IllegalAccessException e4) {
            g.a("CommonFileUtil", "error when set permissions:", e4);
            return false;
        } catch (InvocationTargetException e5) {
            g.a("CommonFileUtil", "error when set permissions:", e5);
            return false;
        }
    }

    public static boolean a(String str) {
        if (str == null || str.trim().length() == 0) {
            return false;
        }
        File file = new File(str);
        return file.exists() && file.isFile();
    }

    public static File a(String str, Context context, boolean[] zArr) throws IOException {
        if (Environment.getExternalStorageState().equals("mounted")) {
            File file = new File(com.mintegral.msdk.base.common.b.e.b(c.MINTEGRAL_700_APK) + "/download/.mtg" + str);
            file.mkdirs();
            if (file.exists()) {
                zArr[0] = true;
                return file;
            }
        }
        String absolutePath = context.getCacheDir().getAbsolutePath();
        new File(absolutePath).mkdir();
        f(absolutePath);
        String str2 = absolutePath + "/mtgdownload";
        new File(str2).mkdir();
        f(str2);
        File file2 = new File(str2);
        zArr[0] = false;
        return file2;
    }

    static /* synthetic */ void d(String str) {
        try {
            if (d(new File(str)) > 52428800) {
                e(str);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Throwable unused) {
            g.d("CommonFileUtil", "clean memory failed");
        }
    }
}
