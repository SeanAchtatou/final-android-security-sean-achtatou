package com.mintegral.msdk.base.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import com.appsflyer.share.Constants;
import com.google.android.gms.drive.DriveFile;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.s;
import com.mintegral.msdk.base.common.b.c;
import com.mintegral.msdk.base.common.b.e;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.d.b;
import com.mintegral.msdk.mtgbid.common.BidResponsedEx;
import com.mintegral.msdk.out.NativeListener;
import java.io.File;
import java.util.Iterator;
import java.util.List;

/* compiled from: CommonSDKUtil */
public final class j {
    public static boolean a = false;

    /* compiled from: CommonSDKUtil */
    public static class a {
        private static Intent a() {
            return new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.package.name"));
        }

        private static List<ResolveInfo> a(Context context) {
            try {
                return context.getPackageManager().queryIntentActivities(a(), 0);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public static boolean a(String str) {
            return b(str) || c(str);
        }

        public static boolean b(String str) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    return Uri.parse(str).getScheme().equals("market");
                }
                return false;
            } catch (Throwable th) {
                g.d("SDKUtil", Log.getStackTraceString(th));
                return false;
            }
        }

        private static boolean c(String str) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    Uri parse = Uri.parse(str);
                    if (parse.getHost().equals("play.google.com") || parse.getHost().equals("market.android.com")) {
                        return true;
                    }
                    return false;
                }
            } catch (Throwable th) {
                g.d("SDKUtil", Log.getStackTraceString(th));
            }
            return false;
        }

        public static boolean a(Context context, String str, NativeListener.NativeTrackingListener nativeTrackingListener) {
            try {
                g.d("SDKUtil", "try google play: url = " + str);
                List<ResolveInfo> a = a(context);
                if (a != null) {
                    if (a.size() > 0) {
                        if (!b(str)) {
                            if (c(str)) {
                                str = "market://" + str.substring(str.indexOf("details?id="));
                            } else {
                                str = null;
                            }
                        }
                        if (TextUtils.isEmpty(str)) {
                            return false;
                        }
                        Intent a2 = a();
                        a2.setData(Uri.parse(str));
                        a2.addFlags(DriveFile.MODE_READ_ONLY);
                        Iterator<ResolveInfo> it = a.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                if (it.next().activityInfo.packageName.equals("com.android.vending")) {
                                    a2.setPackage("com.android.vending");
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        g.b("SDKUtil", "open google play: details = " + str);
                        context.startActivity(a2);
                        j.a(nativeTrackingListener);
                        return true;
                    }
                }
                return false;
            } catch (Throwable th) {
                g.d("SDKUtil", Log.getStackTraceString(th));
                return false;
            }
        }
    }

    public static void a(NativeListener.NativeTrackingListener nativeTrackingListener) {
        if (nativeTrackingListener instanceof NativeListener.TrackingExListener) {
            ((NativeListener.TrackingExListener) nativeTrackingListener).onLeaveApp();
        }
    }

    public static void a(Context context, String str, NativeListener.NativeTrackingListener nativeTrackingListener) {
        try {
            g.b("SDKUtil", "gotoGoogle = " + str);
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.addFlags(DriveFile.MODE_READ_ONLY);
            boolean z = false;
            List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
            if (queryIntentActivities.size() > 0) {
                z = true;
            }
            if (str.startsWith("market://")) {
                if (!z) {
                    g.b("SDKUtil", "gotoGoogle = openurl");
                    b(context, "https://play.google.com/store/apps/details?id=" + str.replace("market://details?id=", ""), nativeTrackingListener);
                    return;
                }
                Iterator<ResolveInfo> it = queryIntentActivities.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (it.next().activityInfo.packageName.equals("com.android.vending")) {
                            intent.setClassName("com.android.vending", "com.android.vending.AssetBrowserActivity");
                            break;
                        }
                    } else {
                        break;
                    }
                }
                g.b("SDKUtil", "gotoGoogle = startActivity");
                try {
                    context.startActivity(intent);
                    a(nativeTrackingListener);
                } catch (Exception e) {
                    g.c("SDKUtil", "start intent", e);
                    g.b("SDKUtil", "gotoGoogle = openurl");
                    b(context, "https://play.google.com/store/apps/details?id=" + str.replace("market://details?id=", ""), nativeTrackingListener);
                }
            } else if (str.startsWith("https://play.google.com/")) {
                g.b("SDKUtil", "gotoGoogle = replace url");
                a(context, "market://details?id=" + str.replace("https://play.google.com/store/apps/details?id=", ""), nativeTrackingListener);
            }
        } catch (Exception e2) {
            g.d("SDKUtil", "gotoGoogle = error");
            g.c("SDKUtil", "Exception", e2);
        }
    }

    public static void a(Context context, String str, CampaignEx campaignEx, NativeListener.NativeTrackingListener nativeTrackingListener) {
        if (context != null) {
            if (a) {
                b(context, str, nativeTrackingListener);
                return;
            }
            try {
                Class.forName("com.mintegral.msdk.activity.MTGCommonActivity");
                Intent intent = new Intent(context, Class.forName("com.mintegral.msdk.activity.MTGCommonActivity"));
                if (!TextUtils.isEmpty(str)) {
                    if (a.b(str)) {
                        String replace = str.replace("market://details?id=", "");
                        try {
                            str = "https://play.google.com/store/apps/details?id=" + replace;
                        } catch (Exception e) {
                            e = e;
                            str = replace;
                            b(context, str, nativeTrackingListener);
                            g.c("MTGCommonActivity", "", e);
                        }
                    }
                    g.b("SDKUtil", "openInnerBrowserUrl = openurl");
                    intent.putExtra("url", str);
                    g.b("url", "webview url = " + str);
                    intent.setFlags(DriveFile.MODE_READ_ONLY);
                    intent.putExtra("mvcommon", campaignEx);
                    context.startActivity(intent);
                }
            } catch (Exception e2) {
                e = e2;
                b(context, str, nativeTrackingListener);
                g.c("MTGCommonActivity", "", e);
            }
        }
    }

    public static void b(Context context, String str, NativeListener.NativeTrackingListener nativeTrackingListener) {
        if (str != null && context != null) {
            try {
                if (a.b(str)) {
                    String replace = str.replace("market://details?id=", "");
                    try {
                        str = "https://play.google.com/store/apps/details?id=" + replace;
                    } catch (Exception e) {
                        String str2 = replace;
                        e = e;
                        str = str2;
                        g.d("SDKUtil", "openBrowserUrl = error");
                        e.printStackTrace();
                        try {
                            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                            intent.addFlags(268468224);
                            context.startActivity(intent);
                            a(nativeTrackingListener);
                        } catch (Exception e2) {
                            g.d("SDKUtil", "openBrowserUrl = error2");
                            e2.printStackTrace();
                            return;
                        }
                    }
                }
                g.b("SDKUtil", "openBrowserUrl = openurl");
                Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent2.addFlags(DriveFile.MODE_READ_ONLY);
                ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent2, 65536);
                if (resolveActivity != null) {
                    intent2.setClassName(resolveActivity.activityInfo.packageName, resolveActivity.activityInfo.name);
                }
                context.startActivity(intent2);
                a(nativeTrackingListener);
            } catch (Exception e3) {
                e = e3;
                g.d("SDKUtil", "openBrowserUrl = error");
                e.printStackTrace();
                Intent intent3 = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent3.addFlags(268468224);
                context.startActivity(intent3);
                a(nativeTrackingListener);
            }
        }
    }

    public static String a(String str) {
        String str2;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        File a2 = e.a(c.MINTEGRAL_700_IMG);
        if (TextUtils.isEmpty(str)) {
            str2 = "";
        } else if (str.lastIndexOf(Constants.URL_PATH_DELIMITER) == -1) {
            StringBuilder sb = new StringBuilder();
            sb.append(str.hashCode());
            str2 = sb.toString();
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str.hashCode() + str.substring(str.lastIndexOf(Constants.URL_PATH_DELIMITER) + 1).hashCode());
            str2 = sb2.toString();
        }
        return new File(a2, str2).getAbsolutePath();
    }

    public static void a(final String str, Context context) {
        AnonymousClass1 r0 = new com.mintegral.msdk.base.common.f.a() {
            public final void b() {
            }

            public final void a() {
                try {
                    b.a();
                    com.mintegral.msdk.d.a b = b.b(str);
                    if (b == null) {
                        b.a();
                        b = b.b();
                    }
                    s.a(i.a(com.mintegral.msdk.base.controller.a.d().h())).a(Long.valueOf(b.aa()));
                    com.mintegral.msdk.base.b.b.a(i.a(com.mintegral.msdk.base.controller.a.d().h())).c();
                    com.mintegral.msdk.base.b.c.a(i.a(com.mintegral.msdk.base.controller.a.d().h())).a(Long.valueOf(b.Y()));
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        };
        if (context != null) {
            new com.mintegral.msdk.base.common.f.b(context).b(r0);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:28|29|30|31|32) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0107 */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004a A[Catch:{ Throwable -> 0x01f6, Throwable -> 0x0246 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x010d A[Catch:{ Throwable -> 0x01f6, Throwable -> 0x0246 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0242 A[Catch:{ Throwable -> 0x01f6, Throwable -> 0x0246 }] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002f A[Catch:{ Throwable -> 0x01f6, Throwable -> 0x0246 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r11, final java.lang.String r12, final com.mintegral.msdk.base.entity.CampaignEx r13) {
        /*
            r0 = -1
            if (r13 == 0) goto L_0x0012
            java.lang.String r1 = r13.getPackageName()     // Catch:{ Throwable -> 0x0246 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Throwable -> 0x0246 }
            if (r1 != 0) goto L_0x0012
            java.lang.String r1 = r13.getPackageName()     // Catch:{ Throwable -> 0x0246 }
            goto L_0x0013
        L_0x0012:
            r1 = r12
        L_0x0013:
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0246 }
            android.content.Context r2 = r2.h()     // Catch:{ Throwable -> 0x0246 }
            java.lang.String r3 = ""
            java.lang.Object r2 = com.mintegral.msdk.base.utils.r.b(r2, r1, r3)     // Catch:{ Throwable -> 0x0246 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0246 }
            boolean r3 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Throwable -> 0x0246 }
            r4 = 0
            r6 = 1
            r7 = 0
            if (r3 != 0) goto L_0x004a
            java.io.File r3 = new java.io.File     // Catch:{ Throwable -> 0x0246 }
            r3.<init>(r2)     // Catch:{ Throwable -> 0x0246 }
            boolean r2 = r3.exists()     // Catch:{ Throwable -> 0x0246 }
            if (r2 == 0) goto L_0x00f6
            com.mintegral.msdk.base.controller.a r11 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0246 }
            android.content.Context r11 = r11.h()     // Catch:{ Throwable -> 0x0246 }
            android.net.Uri r2 = android.net.Uri.fromFile(r3)     // Catch:{ Throwable -> 0x0246 }
            com.mintegral.msdk.click.b.a(r11, r2, r12, r1)     // Catch:{ Throwable -> 0x0246 }
            return
        L_0x004a:
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0246 }
            android.content.Context r2 = r2.h()     // Catch:{ Throwable -> 0x0246 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0246 }
            r3.<init>()     // Catch:{ Throwable -> 0x0246 }
            r3.append(r1)     // Catch:{ Throwable -> 0x0246 }
            java.lang.String r8 = "process"
            r3.append(r8)     // Catch:{ Throwable -> 0x0246 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x0246 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r7)     // Catch:{ Throwable -> 0x0246 }
            java.lang.Object r2 = com.mintegral.msdk.base.utils.r.b(r2, r3, r8)     // Catch:{ Throwable -> 0x0246 }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Throwable -> 0x0246 }
            int r2 = r2.intValue()     // Catch:{ Throwable -> 0x0246 }
            int r3 = android.os.Process.myPid()     // Catch:{ Throwable -> 0x0246 }
            if (r2 == 0) goto L_0x00f6
            if (r2 != r3) goto L_0x00f6
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0246 }
            android.content.Context r2 = r2.h()     // Catch:{ Throwable -> 0x0246 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0246 }
            r3.<init>()     // Catch:{ Throwable -> 0x0246 }
            r3.append(r1)     // Catch:{ Throwable -> 0x0246 }
            java.lang.String r8 = "isDowning"
            r3.append(r8)     // Catch:{ Throwable -> 0x0246 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x0246 }
            java.lang.Long r8 = java.lang.Long.valueOf(r4)     // Catch:{ Throwable -> 0x0246 }
            java.lang.Object r2 = com.mintegral.msdk.base.utils.r.b(r2, r3, r8)     // Catch:{ Throwable -> 0x0246 }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ Throwable -> 0x0246 }
            long r2 = r2.longValue()     // Catch:{ Throwable -> 0x0246 }
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0246 }
            r10 = 0
            long r8 = r8 - r2
            int r10 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r10 == 0) goto L_0x00f6
            r2 = 36000000(0x2255100, double:1.77863633E-316)
            int r10 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r10 >= 0) goto L_0x00f6
            com.mintegral.msdk.base.controller.a r11 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0246 }
            android.content.Context r11 = r11.h()     // Catch:{ Throwable -> 0x0246 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0246 }
            r2.<init>()     // Catch:{ Throwable -> 0x0246 }
            r2.append(r1)     // Catch:{ Throwable -> 0x0246 }
            java.lang.String r1 = "downloadType"
            r2.append(r1)     // Catch:{ Throwable -> 0x0246 }
            java.lang.String r1 = r2.toString()     // Catch:{ Throwable -> 0x0246 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)     // Catch:{ Throwable -> 0x0246 }
            java.lang.Object r11 = com.mintegral.msdk.base.utils.r.b(r11, r1, r2)     // Catch:{ Throwable -> 0x0246 }
            java.lang.Integer r11 = (java.lang.Integer) r11     // Catch:{ Throwable -> 0x0246 }
            int r11 = r11.intValue()     // Catch:{ Throwable -> 0x0246 }
            if (r11 != r6) goto L_0x00e8
            com.mintegral.msdk.base.controller.a r11 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0246 }
            android.content.Context r11 = r11.h()     // Catch:{ Throwable -> 0x0246 }
            java.lang.String r1 = "downloading"
            com.mintegral.msdk.click.a.b(r13, r11, r1)     // Catch:{ Throwable -> 0x0246 }
            return
        L_0x00e8:
            com.mintegral.msdk.base.controller.a r11 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0246 }
            android.content.Context r11 = r11.h()     // Catch:{ Throwable -> 0x0246 }
            java.lang.String r1 = "downloading"
            com.mintegral.msdk.click.a.a(r13, r11, r1)     // Catch:{ Throwable -> 0x0246 }
            return
        L_0x00f6:
            int r1 = com.mintegral.msdk.base.utils.t.a     // Catch:{ Throwable -> 0x0246 }
            if (r1 != r0) goto L_0x0109
            java.lang.String r1 = "com.mintegral.msdk.mtgdownload.b"
            java.lang.Class.forName(r1)     // Catch:{ ClassNotFoundException -> 0x0107 }
            java.lang.String r1 = "com.mintegral.msdk.mtgdownload.g"
            java.lang.Class.forName(r1)     // Catch:{ ClassNotFoundException -> 0x0107 }
            com.mintegral.msdk.base.utils.t.a = r6     // Catch:{ ClassNotFoundException -> 0x0107 }
            goto L_0x0109
        L_0x0107:
            com.mintegral.msdk.base.utils.t.a = r7     // Catch:{ Throwable -> 0x0246 }
        L_0x0109:
            int r1 = com.mintegral.msdk.base.utils.t.a     // Catch:{ Throwable -> 0x0246 }
            if (r1 != r6) goto L_0x0242
            if (r13 == 0) goto L_0x011e
            java.lang.String r1 = r13.getPackageName()     // Catch:{ Throwable -> 0x0246 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Throwable -> 0x0246 }
            if (r1 != 0) goto L_0x011e
            java.lang.String r1 = r13.getPackageName()     // Catch:{ Throwable -> 0x0246 }
            goto L_0x011f
        L_0x011e:
            r1 = r12
        L_0x011f:
            a(r12, r6, r13)     // Catch:{ Throwable -> 0x01f6 }
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x01f6 }
            android.content.Context r2 = r2.h()     // Catch:{ Throwable -> 0x01f6 }
            boolean r3 = com.mintegral.msdk.base.utils.t.a(r2)     // Catch:{ Throwable -> 0x01f6 }
            boolean r8 = com.mintegral.msdk.base.utils.t.c(r2)     // Catch:{ Throwable -> 0x01f6 }
            boolean r9 = com.mintegral.msdk.base.utils.t.b(r2)     // Catch:{ Throwable -> 0x01f6 }
            if (r9 != 0) goto L_0x013c
            com.mintegral.msdk.click.b.a(r2, r12, r1)     // Catch:{ Throwable -> 0x01f6 }
            return
        L_0x013c:
            if (r8 != 0) goto L_0x0142
            a(r13, r12)     // Catch:{ Throwable -> 0x01f6 }
            return
        L_0x0142:
            if (r3 != 0) goto L_0x0148
            a(r13, r12)     // Catch:{ Throwable -> 0x01f6 }
            return
        L_0x0148:
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x01f6 }
            android.content.Context r2 = r2.h()     // Catch:{ Throwable -> 0x01f6 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01f6 }
            r3.<init>()     // Catch:{ Throwable -> 0x01f6 }
            r3.append(r1)     // Catch:{ Throwable -> 0x01f6 }
            java.lang.String r8 = "isDowning"
            r3.append(r8)     // Catch:{ Throwable -> 0x01f6 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x01f6 }
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x01f6 }
            java.lang.Long r8 = java.lang.Long.valueOf(r8)     // Catch:{ Throwable -> 0x01f6 }
            com.mintegral.msdk.base.utils.r.a(r2, r3, r8)     // Catch:{ Throwable -> 0x01f6 }
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x01f6 }
            android.content.Context r2 = r2.h()     // Catch:{ Throwable -> 0x01f6 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01f6 }
            r3.<init>()     // Catch:{ Throwable -> 0x01f6 }
            r3.append(r1)     // Catch:{ Throwable -> 0x01f6 }
            java.lang.String r8 = "process"
            r3.append(r8)     // Catch:{ Throwable -> 0x01f6 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x01f6 }
            int r8 = android.os.Process.myPid()     // Catch:{ Throwable -> 0x01f6 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Throwable -> 0x01f6 }
            com.mintegral.msdk.base.utils.r.a(r2, r3, r8)     // Catch:{ Throwable -> 0x01f6 }
            java.lang.String r2 = "com.mintegral.msdk.mtgdownload.g"
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Throwable -> 0x01f6 }
            r3 = 2
            java.lang.Class[] r8 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x01f6 }
            java.lang.Class<android.content.Context> r9 = android.content.Context.class
            r8[r7] = r9     // Catch:{ Throwable -> 0x01f6 }
            java.lang.Class<java.lang.String> r9 = java.lang.String.class
            r8[r6] = r9     // Catch:{ Throwable -> 0x01f6 }
            java.lang.reflect.Constructor r8 = r2.getConstructor(r8)     // Catch:{ Throwable -> 0x01f6 }
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x01f6 }
            com.mintegral.msdk.base.controller.a r9 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x01f6 }
            android.content.Context r9 = r9.h()     // Catch:{ Throwable -> 0x01f6 }
            r3[r7] = r9     // Catch:{ Throwable -> 0x01f6 }
            r3[r6] = r12     // Catch:{ Throwable -> 0x01f6 }
            java.lang.Object r3 = r8.newInstance(r3)     // Catch:{ Throwable -> 0x01f6 }
            boolean r8 = android.text.TextUtils.isEmpty(r11)     // Catch:{ Throwable -> 0x01f6 }
            if (r8 != 0) goto L_0x01d0
            java.lang.String r8 = "setTitle"
            java.lang.Class[] r9 = new java.lang.Class[r6]     // Catch:{ Throwable -> 0x01f6 }
            java.lang.Class<java.lang.String> r10 = java.lang.String.class
            r9[r7] = r10     // Catch:{ Throwable -> 0x01f6 }
            java.lang.reflect.Method r8 = r2.getMethod(r8, r9)     // Catch:{ Throwable -> 0x01f6 }
            java.lang.Object[] r9 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x01f6 }
            r9[r7] = r11     // Catch:{ Throwable -> 0x01f6 }
            r8.invoke(r3, r9)     // Catch:{ Throwable -> 0x01f6 }
        L_0x01d0:
            java.lang.String r11 = "setDownloadListener"
            java.lang.Class[] r8 = new java.lang.Class[r6]     // Catch:{ Throwable -> 0x01f6 }
            java.lang.Class<com.mintegral.msdk.out.IDownloadListener> r9 = com.mintegral.msdk.out.IDownloadListener.class
            r8[r7] = r9     // Catch:{ Throwable -> 0x01f6 }
            java.lang.reflect.Method r11 = r2.getMethod(r11, r8)     // Catch:{ Throwable -> 0x01f6 }
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x01f6 }
            com.mintegral.msdk.base.utils.j$2 r8 = new com.mintegral.msdk.base.utils.j$2     // Catch:{ Throwable -> 0x01f6 }
            r8.<init>(r13, r1, r12)     // Catch:{ Throwable -> 0x01f6 }
            r6[r7] = r8     // Catch:{ Throwable -> 0x01f6 }
            r11.invoke(r3, r6)     // Catch:{ Throwable -> 0x01f6 }
            java.lang.String r11 = "start"
            java.lang.Class[] r6 = new java.lang.Class[r7]     // Catch:{ Throwable -> 0x01f6 }
            java.lang.reflect.Method r11 = r2.getMethod(r11, r6)     // Catch:{ Throwable -> 0x01f6 }
            java.lang.Object[] r2 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x01f6 }
            r11.invoke(r3, r2)     // Catch:{ Throwable -> 0x01f6 }
            return
        L_0x01f6:
            r11 = move-exception
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0246 }
            android.content.Context r2 = r2.h()     // Catch:{ Throwable -> 0x0246 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0246 }
            r3.<init>()     // Catch:{ Throwable -> 0x0246 }
            r3.append(r1)     // Catch:{ Throwable -> 0x0246 }
            java.lang.String r6 = "isDowning"
            r3.append(r6)     // Catch:{ Throwable -> 0x0246 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x0246 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ Throwable -> 0x0246 }
            com.mintegral.msdk.base.utils.r.a(r2, r3, r4)     // Catch:{ Throwable -> 0x0246 }
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0246 }
            android.content.Context r2 = r2.h()     // Catch:{ Throwable -> 0x0246 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0246 }
            r3.<init>()     // Catch:{ Throwable -> 0x0246 }
            r3.append(r1)     // Catch:{ Throwable -> 0x0246 }
            java.lang.String r1 = "process"
            r3.append(r1)     // Catch:{ Throwable -> 0x0246 }
            java.lang.String r1 = r3.toString()     // Catch:{ Throwable -> 0x0246 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r7)     // Catch:{ Throwable -> 0x0246 }
            com.mintegral.msdk.base.utils.r.a(r2, r1, r3)     // Catch:{ Throwable -> 0x0246 }
            boolean r1 = com.mintegral.msdk.MIntegralConstans.DEBUG     // Catch:{ Throwable -> 0x0246 }
            if (r1 == 0) goto L_0x023e
            r11.printStackTrace()     // Catch:{ Throwable -> 0x0246 }
        L_0x023e:
            a(r13, r12)     // Catch:{ Throwable -> 0x0246 }
            return
        L_0x0242:
            a(r13, r12)     // Catch:{ Throwable -> 0x0246 }
            return
        L_0x0246:
            com.mintegral.msdk.base.utils.t.a = r0
            java.lang.String r11 = "downloadapk"
            java.lang.String r0 = "can't find download jar, use simple method"
            com.mintegral.msdk.base.utils.g.b(r11, r0)
            a(r13, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.utils.j.a(java.lang.String, java.lang.String, com.mintegral.msdk.base.entity.CampaignEx):void");
    }

    public static void a(String str, int i, CampaignEx campaignEx) {
        if (campaignEx != null && !TextUtils.isEmpty(campaignEx.getPackageName())) {
            str = campaignEx.getPackageName();
        }
        Context h = com.mintegral.msdk.base.controller.a.d().h();
        r.a(h, str + "downloadType", Integer.valueOf(i));
        if (campaignEx != null) {
            Context h2 = com.mintegral.msdk.base.controller.a.d().h();
            r.a(h2, str + "linkType", Integer.valueOf(campaignEx.getLinkType()));
            Context h3 = com.mintegral.msdk.base.controller.a.d().h();
            r.a(h3, str + "rid", campaignEx.getRequestIdNotice());
            Context h4 = com.mintegral.msdk.base.controller.a.d().h();
            r.a(h4, str + BidResponsedEx.KEY_CID, campaignEx.getId());
            return;
        }
        Context h5 = com.mintegral.msdk.base.controller.a.d().h();
        r.a(h5, str + "linkType", -1);
        Context h6 = com.mintegral.msdk.base.controller.a.d().h();
        r.a(h6, str + "rid", "");
        Context h7 = com.mintegral.msdk.base.controller.a.d().h();
        r.a(h7, str + BidResponsedEx.KEY_CID, "");
    }

    private static void a(final CampaignEx campaignEx, final String str) {
        String packageName = (campaignEx == null || TextUtils.isEmpty(campaignEx.getPackageName())) ? str : campaignEx.getPackageName();
        try {
            a(str, 2, campaignEx);
            Context h = com.mintegral.msdk.base.controller.a.d().h();
            if (!t.b(h)) {
                com.mintegral.msdk.click.b.a(h, str, packageName);
                return;
            }
            Context h2 = com.mintegral.msdk.base.controller.a.d().h();
            r.a(h2, packageName + "isDowning", Long.valueOf(System.currentTimeMillis()));
            Context h3 = com.mintegral.msdk.base.controller.a.d().h();
            r.a(h3, packageName + "process", Integer.valueOf(Process.myPid()));
            new Thread(new Runnable() {
                final /* synthetic */ boolean c = true;

                public final void run() {
                    j.a(com.mintegral.msdk.base.controller.a.d().h(), campaignEx, str, this.c);
                }
            }).start();
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x012a A[Catch:{ all -> 0x0146 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0132 A[SYNTHETIC, Splitter:B:53:0x0132] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x013c A[SYNTHETIC, Splitter:B:58:0x013c] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x014a A[SYNTHETIC, Splitter:B:66:0x014a] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0154 A[SYNTHETIC, Splitter:B:71:0x0154] */
    /* JADX WARNING: Removed duplicated region for block: B:79:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r10, com.mintegral.msdk.base.entity.CampaignEx r11, java.lang.String r12, boolean r13) {
        /*
            if (r11 == 0) goto L_0x0011
            java.lang.String r0 = r11.getPackageName()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0011
            java.lang.String r0 = r11.getPackageName()
            goto L_0x0012
        L_0x0011:
            r0 = r12
        L_0x0012:
            r1 = 1
            r2 = 0
            boolean[] r3 = new boolean[r1]     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            java.lang.String r4 = "/apk"
            java.io.File r3 = com.mintegral.msdk.base.utils.e.a(r4, r10, r3)     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            java.lang.String r4 = com.mintegral.msdk.click.b.a(r12)     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            r5.<init>()     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            r5.append(r4)     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            java.lang.String r4 = ".apk"
            r5.append(r4)     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            java.lang.String r4 = r5.toString()     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            java.io.File r5 = new java.io.File     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            r5.<init>(r3, r4)     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            boolean r3 = r5.exists()     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            if (r3 == 0) goto L_0x003f
            r5.delete()     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
        L_0x003f:
            if (r13 == 0) goto L_0x0047
            java.lang.String r3 = "start"
            com.mintegral.msdk.click.a.a(r11, r10, r3)     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            goto L_0x004c
        L_0x0047:
            java.lang.String r3 = "shortcuts_start"
            com.mintegral.msdk.click.a.a(r11, r10, r3)     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
        L_0x004c:
            java.net.URL r3 = new java.net.URL     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            r3.<init>(r12)     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            java.net.URLConnection r3 = r3.openConnection()     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            r4 = 8000(0x1f40, float:1.121E-41)
            r3.setConnectTimeout(r4)     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            int r4 = r3.getContentLength()     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            java.io.InputStream r3 = r3.getInputStream()     // Catch:{ Throwable -> 0x0124, all -> 0x0120 }
            r6 = 1024(0x400, float:1.435E-42)
            byte[] r6 = new byte[r6]     // Catch:{ Throwable -> 0x011e }
            java.io.FileOutputStream r7 = new java.io.FileOutputStream     // Catch:{ Throwable -> 0x011e }
            r7.<init>(r5, r1)     // Catch:{ Throwable -> 0x011e }
            r1 = 0
            r2 = 0
        L_0x006d:
            int r8 = r3.read(r6)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            r9 = -1
            if (r8 == r9) goto L_0x0079
            r7.write(r6, r1, r8)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            int r2 = r2 + r8
            goto L_0x006d
        L_0x0079:
            if (r2 != r4) goto L_0x0105
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            android.content.Context r2 = r2.h()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            r4.<init>()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            r4.append(r0)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            java.lang.String r6 = "isDowning"
            r4.append(r6)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            r8 = 0
            java.lang.Long r6 = java.lang.Long.valueOf(r8)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            com.mintegral.msdk.base.utils.r.a(r2, r4, r6)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            android.content.Context r2 = r2.h()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            r4.<init>()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            r4.append(r0)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            java.lang.String r6 = "process"
            r4.append(r6)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            com.mintegral.msdk.base.utils.r.a(r2, r4, r1)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            java.lang.String r1 = "end"
            com.mintegral.msdk.click.a.a(r11, r10, r1)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            android.content.Context r1 = r1.h()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            com.mintegral.msdk.base.b.i r1 = com.mintegral.msdk.base.b.i.a(r1)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            com.mintegral.msdk.base.b.g r1 = com.mintegral.msdk.base.b.g.b(r1)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            r1.a(r11)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            boolean r11 = r5.exists()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            if (r11 == 0) goto L_0x00f4
            if (r13 == 0) goto L_0x00f4
            android.net.Uri r11 = android.net.Uri.fromFile(r5)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            com.mintegral.msdk.click.b.a(r10, r11, r12, r0)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            com.mintegral.msdk.base.controller.a r11 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            android.content.Context r11 = r11.h()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            java.lang.String r13 = r5.getAbsolutePath()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            com.mintegral.msdk.base.utils.r.a(r11, r0, r13)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            goto L_0x0105
        L_0x00f4:
            if (r13 != 0) goto L_0x0105
            com.mintegral.msdk.base.controller.a r11 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            android.content.Context r11 = r11.h()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            java.lang.String r13 = r5.getAbsolutePath()     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
            com.mintegral.msdk.base.utils.r.a(r11, r0, r13)     // Catch:{ Throwable -> 0x011b, all -> 0x0119 }
        L_0x0105:
            r7.close()     // Catch:{ IOException -> 0x0109 }
            goto L_0x010d
        L_0x0109:
            r10 = move-exception
            r10.printStackTrace()
        L_0x010d:
            if (r3 == 0) goto L_0x0118
            r3.close()     // Catch:{ IOException -> 0x0113 }
            goto L_0x0118
        L_0x0113:
            r10 = move-exception
            r10.printStackTrace()
            return
        L_0x0118:
            return
        L_0x0119:
            r10 = move-exception
            goto L_0x0148
        L_0x011b:
            r11 = move-exception
            r2 = r7
            goto L_0x0126
        L_0x011e:
            r11 = move-exception
            goto L_0x0126
        L_0x0120:
            r10 = move-exception
            r3 = r2
            r7 = r3
            goto L_0x0148
        L_0x0124:
            r11 = move-exception
            r3 = r2
        L_0x0126:
            boolean r13 = com.mintegral.msdk.MIntegralConstans.DEBUG     // Catch:{ all -> 0x0146 }
            if (r13 == 0) goto L_0x012d
            r11.printStackTrace()     // Catch:{ all -> 0x0146 }
        L_0x012d:
            com.mintegral.msdk.click.b.a(r10, r12, r0)     // Catch:{ all -> 0x0146 }
            if (r2 == 0) goto L_0x013a
            r2.close()     // Catch:{ IOException -> 0x0136 }
            goto L_0x013a
        L_0x0136:
            r10 = move-exception
            r10.printStackTrace()
        L_0x013a:
            if (r3 == 0) goto L_0x0145
            r3.close()     // Catch:{ IOException -> 0x0140 }
            goto L_0x0145
        L_0x0140:
            r10 = move-exception
            r10.printStackTrace()
            return
        L_0x0145:
            return
        L_0x0146:
            r10 = move-exception
            r7 = r2
        L_0x0148:
            if (r7 == 0) goto L_0x0152
            r7.close()     // Catch:{ IOException -> 0x014e }
            goto L_0x0152
        L_0x014e:
            r11 = move-exception
            r11.printStackTrace()
        L_0x0152:
            if (r3 == 0) goto L_0x015c
            r3.close()     // Catch:{ IOException -> 0x0158 }
            goto L_0x015c
        L_0x0158:
            r11 = move-exception
            r11.printStackTrace()
        L_0x015c:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.utils.j.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, boolean):void");
    }
}
