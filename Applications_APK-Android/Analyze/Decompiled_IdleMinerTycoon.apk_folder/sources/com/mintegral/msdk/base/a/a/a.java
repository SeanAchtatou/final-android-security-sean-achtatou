package com.mintegral.msdk.base.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.mintegral.msdk.base.utils.g;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* compiled from: SharedPerferenceManager */
public class a {
    public static final String a = "a";
    private static a c;
    SharedPreferences b;

    private a() {
    }

    public static a a() {
        if (c == null) {
            synchronized (a.class) {
                if (c == null) {
                    c = new a();
                }
            }
        }
        return c;
    }

    public final void a(String str, String str2) {
        try {
            Context h = com.mintegral.msdk.base.controller.a.d().h();
            if (h == null) {
                g.d(a, "context is null in put");
                return;
            }
            if (this.b == null && h != null) {
                this.b = h.getSharedPreferences("mintegral", 0);
            }
            SharedPreferences.Editor edit = this.b.edit();
            edit.putString(str, str2);
            edit.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final String a(String str) {
        try {
            Context h = com.mintegral.msdk.base.controller.a.d().h();
            if (h == null) {
                g.d(a, "context is null in get");
                return null;
            }
            if (this.b == null && h != null) {
                this.b = h.getSharedPreferences("mintegral", 0);
            }
            return this.b.getString(str, "");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final List<String> b() {
        ArrayList arrayList = new ArrayList();
        Context h = com.mintegral.msdk.base.controller.a.d().h();
        if (h == null) {
            g.d(a, "context is null in get");
            return null;
        }
        if (this.b == null && h != null) {
            this.b = h.getSharedPreferences("mintegral", 0);
        }
        for (Map.Entry<String, ?> key : this.b.getAll().entrySet()) {
            arrayList.add(key.getKey());
        }
        return arrayList;
    }
}
