package com.mintegral.msdk.base.b;

import com.mintegral.msdk.out.Campaign;

/* compiled from: CTtimeDao */
public class c extends a<Campaign> {
    private static final String b = "com.mintegral.msdk.base.b.c";
    private static c c;

    private c(h hVar) {
        super(hVar);
    }

    public static c a(h hVar) {
        if (c == null) {
            synchronized (c.class) {
                if (c == null) {
                    c = new c(hVar);
                }
            }
        }
        return c;
    }

    public final synchronized void a(Long l) {
        try {
            String str = "t_time<" + (System.currentTimeMillis() - (l.longValue() * 1000));
            if (b() != null) {
                b().delete("c_t_time", str, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
