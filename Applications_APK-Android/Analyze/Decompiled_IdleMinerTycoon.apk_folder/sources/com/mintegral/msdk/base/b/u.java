package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.i;

/* compiled from: UnitIDDao */
public class u extends a<i> {
    private static u b;

    private u(h hVar) {
        super(hVar);
    }

    public static u a(h hVar) {
        if (b == null) {
            synchronized (u.class) {
                if (b == null) {
                    b = new u(hVar);
                }
            }
        }
        return b;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x005a A[SYNTHETIC, Splitter:B:29:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0063 A[SYNTHETIC, Splitter:B:36:0x0063] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<java.lang.String> a(int r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0067 }
            java.lang.String r1 = "select * from unit_id WHERE ad_type = "
            r0.<init>(r1)     // Catch:{ all -> 0x0067 }
            r0.append(r5)     // Catch:{ all -> 0x0067 }
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x0067 }
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r4.a()     // Catch:{ Exception -> 0x0051, all -> 0x004c }
            android.database.Cursor r5 = r1.rawQuery(r5, r0)     // Catch:{ Exception -> 0x0051, all -> 0x004c }
            if (r5 == 0) goto L_0x0046
            int r1 = r5.getCount()     // Catch:{ Exception -> 0x0041 }
            if (r1 <= 0) goto L_0x0046
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x0041 }
            int r2 = r5.getCount()     // Catch:{ Exception -> 0x0041 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0041 }
        L_0x0029:
            boolean r0 = r5.moveToNext()     // Catch:{ Exception -> 0x003f }
            if (r0 == 0) goto L_0x003d
            java.lang.String r0 = "unitId"
            int r0 = r5.getColumnIndex(r0)     // Catch:{ Exception -> 0x003f }
            java.lang.String r0 = r5.getString(r0)     // Catch:{ Exception -> 0x003f }
            r1.add(r0)     // Catch:{ Exception -> 0x003f }
            goto L_0x0029
        L_0x003d:
            r0 = r1
            goto L_0x0046
        L_0x003f:
            r0 = move-exception
            goto L_0x0055
        L_0x0041:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x0055
        L_0x0046:
            if (r5 == 0) goto L_0x005e
            r5.close()     // Catch:{ all -> 0x0067 }
            goto L_0x005e
        L_0x004c:
            r5 = move-exception
            r3 = r0
            r0 = r5
            r5 = r3
            goto L_0x0061
        L_0x0051:
            r5 = move-exception
            r1 = r0
            r0 = r5
            r5 = r1
        L_0x0055:
            r0.printStackTrace()     // Catch:{ all -> 0x0060 }
            if (r5 == 0) goto L_0x005d
            r5.close()     // Catch:{ all -> 0x0067 }
        L_0x005d:
            r0 = r1
        L_0x005e:
            monitor-exit(r4)
            return r0
        L_0x0060:
            r0 = move-exception
        L_0x0061:
            if (r5 == 0) goto L_0x0066
            r5.close()     // Catch:{ all -> 0x0067 }
        L_0x0066:
            throw r0     // Catch:{ all -> 0x0067 }
        L_0x0067:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.u.a(int):java.util.List");
    }

    public final synchronized void a(String str) {
        if (b() != null) {
            b().delete(MIntegralConstans.PROPERTIES_UNIT_ID, "unitId = ?", new String[]{str});
        }
    }

    public final synchronized void a(String str, int i) {
        if (b() != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("unitId", str);
            contentValues.put("ad_type", Integer.valueOf(i));
            b().insert(MIntegralConstans.PROPERTIES_UNIT_ID, null, contentValues);
        }
    }
}
