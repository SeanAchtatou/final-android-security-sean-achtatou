package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.text.TextUtils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.q;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.s;
import com.vungle.warren.model.AdvertisementDBAdapter;
import java.net.URLEncoder;

/* compiled from: VideoReportDataDao */
public class w extends a<q> {
    private static final String b = "com.mintegral.msdk.base.b.w";
    private static w c;

    private w(h hVar) {
        super(hVar);
    }

    public static w a(h hVar) {
        if (c == null) {
            synchronized (w.class) {
                if (c == null) {
                    c = new w(hVar);
                }
            }
        }
        return c;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:103:0x02e6, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x02e7, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x02e8, code lost:
        r3 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x02ea, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x02eb, code lost:
        r3 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x0310, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x01f0, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x02d7, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x02e7 A[ExcHandler: all (th java.lang.Throwable), PHI: r13 
      PHI: (r13v2 android.database.Cursor) = (r13v0 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor) binds: [B:8:0x0021, B:22:0x0121, B:29:0x0150, B:36:0x016b, B:42:0x017b, B:50:0x0194, B:46:0x0183, B:47:?, B:39:0x0171, B:40:?, B:32:0x0156, B:33:?, B:25:0x0127, B:26:?, B:16:0x00fc, B:17:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:8:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0308 A[SYNTHETIC, Splitter:B:120:0x0308] */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0310 A[Catch:{ all -> 0x030c }] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x02d7 A[ExcHandler: all (th java.lang.Throwable), PHI: r28 
      PHI: (r28v0 android.database.Cursor) = (r28v1 android.database.Cursor), (r28v6 android.database.Cursor) binds: [B:71:0x020e, B:57:0x01ca] A[DONT_GENERATE, DONT_INLINE], Splitter:B:57:0x01ca] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.mintegral.msdk.base.entity.q> a(java.lang.String r33) {
        /*
            r32 = this;
            r0 = r33
            monitor-enter(r32)
            boolean r1 = android.text.TextUtils.isEmpty(r33)     // Catch:{ all -> 0x0317 }
            if (r1 != 0) goto L_0x0314
            android.database.sqlite.SQLiteDatabase r2 = r32.a()     // Catch:{ Exception -> 0x02f8, all -> 0x02f5 }
            java.lang.String r3 = "reward_report"
            r4 = 0
            java.lang.String r5 = "key=?"
            r11 = 1
            java.lang.String[] r6 = new java.lang.String[r11]     // Catch:{ Exception -> 0x02f8, all -> 0x02f5 }
            r12 = 0
            r6[r12] = r0     // Catch:{ Exception -> 0x02f8, all -> 0x02f5 }
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r13 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x02f8, all -> 0x02f5 }
            if (r13 == 0) goto L_0x02ee
            int r1 = r13.getCount()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            if (r1 <= 0) goto L_0x02ee
            java.util.ArrayList r14 = new java.util.ArrayList     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r14.<init>()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
        L_0x002c:
            boolean r1 = r13.moveToNext()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            if (r1 == 0) goto L_0x02de
            java.lang.String r1 = "key"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = r13.getString(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r1 = "networkType"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            int r4 = r13.getInt(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r1 = "network_str"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r7 = r13.getString(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r1 = "isCompleteView"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r13.getInt(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r1 = "watchedMillis"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r13.getInt(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r1 = "videoLength"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            int r18 = r13.getInt(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r1 = "offerUrl"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r5 = r13.getString(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r1 = "reason"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r8 = r13.getString(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r1 = "result"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            int r6 = r13.getInt(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r1 = "duration"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r21 = r13.getString(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r1 = "videoSize"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            int r22 = r13.getInt(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r1 = "campaignId"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r9 = r13.getString(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r1 = "video_url"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r1 = r13.getString(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r2 = "unitId"
            int r2 = r13.getColumnIndex(r2)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r2 = r13.getString(r2)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r10 = "rid"
            int r10 = r13.getColumnIndex(r10)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r10 = r13.getString(r10)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r12 = "ad_type"
            int r12 = r13.getColumnIndex(r12)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r12 = r13.getString(r12)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r11 = "resource_type"
            int r11 = r13.getColumnIndex(r11)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r11 = r13.getString(r11)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r24 = r2
            java.lang.String r2 = "device_id"
            int r2 = r13.getColumnIndex(r2)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r2 = r13.getString(r2)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r25 = r2
            java.lang.String r2 = "creative"
            int r2 = r13.getColumnIndex(r2)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r2 = r13.getString(r2)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r26 = r2
            java.lang.String r2 = "2000021"
            boolean r2 = r0.equals(r2)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            if (r2 == 0) goto L_0x011b
            com.mintegral.msdk.base.entity.q r11 = new com.mintegral.msdk.base.entity.q     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r12 = r24
            r2 = r11
            r6 = r8
            r2.<init>(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r11.m(r9)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r11.e(r1)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r11.k(r10)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r11.l(r12)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r1 = r11
            r3 = r13
            r27 = r14
            goto L_0x02a3
        L_0x0117:
            r0 = move-exception
            r10 = r13
            goto L_0x02fa
        L_0x011b:
            r27 = r14
            r2 = r24
            java.lang.String r14 = "2000022"
            boolean r14 = r0.equals(r14)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            if (r14 == 0) goto L_0x014e
            com.mintegral.msdk.base.entity.q r11 = new com.mintegral.msdk.base.entity.q     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r15 = r11
            r16 = r3
            r17 = r4
            r19 = r5
            r20 = r6
            r23 = r7
            r15.<init>(r16, r17, r18, r19, r20, r21, r22, r23)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r11.m(r9)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r11.e(r1)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r11.k(r10)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r11.l(r2)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r11.o(r8)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r11.k(r10)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r11.h(r12)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r1 = r11
            goto L_0x0166
        L_0x014e:
            java.lang.String r1 = "2000025"
            boolean r1 = r0.equals(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            if (r1 == 0) goto L_0x0169
            com.mintegral.msdk.base.entity.q r1 = new com.mintegral.msdk.base.entity.q     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r15 = r1
            r16 = r3
            r17 = r4
            r19 = r5
            r20 = r6
            r23 = r7
            r15.<init>(r16, r17, r18, r19, r20, r21, r22, r23)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
        L_0x0166:
            r3 = r13
            goto L_0x02a3
        L_0x0169:
            java.lang.String r1 = "2000024"
            boolean r1 = r0.equals(r1)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            if (r1 == 0) goto L_0x0179
            com.mintegral.msdk.base.entity.q r1 = new com.mintegral.msdk.base.entity.q     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r2 = r1
            r6 = r8
            r2.<init>(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            goto L_0x0166
        L_0x0179:
            java.lang.String r1 = "2000039"
            boolean r1 = r1.equals(r0)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            if (r1 == 0) goto L_0x0192
            java.lang.String r1 = "h5_click_data"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            java.lang.String r1 = r13.getString(r1)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            com.mintegral.msdk.base.entity.q r10 = new com.mintegral.msdk.base.entity.q     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r10.<init>(r1)     // Catch:{ Exception -> 0x0117, all -> 0x02e7 }
            r1 = r10
            goto L_0x0166
        L_0x0192:
            java.lang.String r1 = "2000043"
            boolean r1 = r1.equals(r0)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            if (r1 == 0) goto L_0x0200
            java.lang.String r1 = "type"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x01f9, all -> 0x01f2 }
            java.lang.String r14 = r13.getString(r1)     // Catch:{ Exception -> 0x01f9, all -> 0x01f2 }
            java.lang.String r1 = "endcard_url"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x01f9, all -> 0x01f2 }
            java.lang.String r5 = r13.getString(r1)     // Catch:{ Exception -> 0x01f9, all -> 0x01f2 }
            com.mintegral.msdk.base.entity.q r7 = new com.mintegral.msdk.base.entity.q     // Catch:{ Exception -> 0x01f9, all -> 0x01f2 }
            r1 = r7
            r4 = r2
            r28 = r13
            r3 = r25
            r13 = r26
            r2 = r33
            r0 = r3
            r3 = r6
            r6 = r4
            r4 = r21
            r29 = r6
            r6 = r9
            r30 = r13
            r13 = r7
            r7 = r29
            r31 = r9
            r9 = r14
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x01f0, all -> 0x02d7 }
            r13.k(r10)     // Catch:{ Exception -> 0x01f0, all -> 0x02d7 }
            boolean r1 = android.text.TextUtils.isEmpty(r31)     // Catch:{ Exception -> 0x01f0, all -> 0x02d7 }
            if (r1 != 0) goto L_0x01db
            r1 = r31
            r13.m(r1)     // Catch:{ Exception -> 0x01f0, all -> 0x02d7 }
        L_0x01db:
            r13.h(r12)     // Catch:{ Exception -> 0x01f0, all -> 0x02d7 }
            r13.a(r11)     // Catch:{ Exception -> 0x01f0, all -> 0x02d7 }
            r13.b(r0)     // Catch:{ Exception -> 0x01f0, all -> 0x02d7 }
            r2 = r30
            r13.c(r2)     // Catch:{ Exception -> 0x01f0, all -> 0x02d7 }
            r1 = r13
            r3 = r28
            r0 = r33
            goto L_0x02a3
        L_0x01f0:
            r0 = move-exception
            goto L_0x01fc
        L_0x01f2:
            r0 = move-exception
            r28 = r13
        L_0x01f5:
            r3 = r28
            goto L_0x030e
        L_0x01f9:
            r0 = move-exception
            r28 = r13
        L_0x01fc:
            r10 = r28
            goto L_0x02fa
        L_0x0200:
            r29 = r2
            r1 = r9
            r28 = r13
            r0 = r25
            r2 = r26
            java.lang.String r3 = "2000045"
            r5 = r0
            r0 = r33
            boolean r3 = r3.equals(r0)     // Catch:{ Exception -> 0x02da, all -> 0x02d7 }
            if (r3 == 0) goto L_0x0241
            com.mintegral.msdk.base.entity.q r2 = new com.mintegral.msdk.base.entity.q     // Catch:{ Exception -> 0x02da, all -> 0x02d7 }
            r2.<init>()     // Catch:{ Exception -> 0x02da, all -> 0x02d7 }
            r2.n(r0)     // Catch:{ Exception -> 0x02da, all -> 0x02d7 }
            r2.b(r4)     // Catch:{ Exception -> 0x02da, all -> 0x02d7 }
            r2.c(r6)     // Catch:{ Exception -> 0x02da, all -> 0x02d7 }
            r2.m(r1)     // Catch:{ Exception -> 0x02da, all -> 0x02d7 }
            java.lang.String r1 = "template_url"
            r3 = r28
            int r1 = r3.getColumnIndex(r1)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            java.lang.String r1 = r3.getString(r1)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r2.i(r1)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r2.o(r8)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r2.k(r10)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r7 = r29
            r2.l(r7)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
        L_0x023f:
            r1 = r2
            goto L_0x02a3
        L_0x0241:
            r3 = r28
            r7 = r29
            java.lang.String r9 = "2000044"
            boolean r9 = r9.equals(r0)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            if (r9 == 0) goto L_0x0272
            com.mintegral.msdk.base.entity.q r2 = new com.mintegral.msdk.base.entity.q     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r2.<init>()     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r2.n(r0)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r2.b(r4)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r2.m(r1)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            java.lang.String r1 = "image_url"
            int r1 = r3.getColumnIndex(r1)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            java.lang.String r1 = r3.getString(r1)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r2.d(r1)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r2.o(r8)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r2.k(r10)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r2.l(r7)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            goto L_0x023f
        L_0x0272:
            java.lang.String r9 = "2000054"
            boolean r9 = r9.equals(r0)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            if (r9 == 0) goto L_0x02a2
            com.mintegral.msdk.base.entity.q r9 = new com.mintegral.msdk.base.entity.q     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r9.<init>()     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r9.n(r0)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r9.a(r11)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r9.l(r7)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r9.b(r5)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r9.h(r12)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r9.m(r1)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r9.k(r10)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r9.c(r6)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r9.o(r8)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r9.b(r4)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r9.c(r2)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r1 = r9
            goto L_0x02a3
        L_0x02a2:
            r1 = 0
        L_0x02a3:
            if (r1 == 0) goto L_0x02b0
            r2 = r27
            r2.add(r1)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            goto L_0x02b2
        L_0x02ab:
            r0 = move-exception
            goto L_0x030e
        L_0x02ae:
            r0 = move-exception
            goto L_0x02ec
        L_0x02b0:
            r2 = r27
        L_0x02b2:
            java.lang.String r1 = "id"
            int r1 = r3.getColumnIndex(r1)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            int r1 = r3.getInt(r1)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            android.database.sqlite.SQLiteDatabase r4 = r32.b()     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            java.lang.String r5 = "reward_report"
            java.lang.String r6 = "id = ?"
            r7 = 1
            java.lang.String[] r8 = new java.lang.String[r7]     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r9 = 0
            r8[r9] = r1     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r4.delete(r5, r6, r8)     // Catch:{ Exception -> 0x02ae, all -> 0x02ab }
            r14 = r2
            r13 = r3
            r11 = 1
            r12 = 0
            goto L_0x002c
        L_0x02d7:
            r0 = move-exception
            goto L_0x01f5
        L_0x02da:
            r0 = move-exception
            r3 = r28
            goto L_0x02ec
        L_0x02de:
            r3 = r13
            r2 = r14
            if (r3 == 0) goto L_0x02e5
            r3.close()     // Catch:{ all -> 0x0317 }
        L_0x02e5:
            monitor-exit(r32)
            return r2
        L_0x02e7:
            r0 = move-exception
            r3 = r13
            goto L_0x030e
        L_0x02ea:
            r0 = move-exception
            r3 = r13
        L_0x02ec:
            r10 = r3
            goto L_0x02fa
        L_0x02ee:
            r3 = r13
            if (r3 == 0) goto L_0x0314
            r3.close()     // Catch:{ all -> 0x0317 }
            goto L_0x0314
        L_0x02f5:
            r0 = move-exception
            r3 = 0
            goto L_0x030e
        L_0x02f8:
            r0 = move-exception
            r10 = 0
        L_0x02fa:
            r0.printStackTrace()     // Catch:{ all -> 0x030c }
            java.lang.String r1 = com.mintegral.msdk.base.b.w.b     // Catch:{ all -> 0x030c }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x030c }
            com.mintegral.msdk.base.utils.g.d(r1, r0)     // Catch:{ all -> 0x030c }
            if (r10 == 0) goto L_0x0314
            r10.close()     // Catch:{ all -> 0x0317 }
            goto L_0x0314
        L_0x030c:
            r0 = move-exception
            r3 = r10
        L_0x030e:
            if (r3 == 0) goto L_0x0313
            r3.close()     // Catch:{ all -> 0x0317 }
        L_0x0313:
            throw r0     // Catch:{ all -> 0x0317 }
        L_0x0314:
            monitor-exit(r32)
            r1 = 0
            return r1
        L_0x0317:
            r0 = move-exception
            monitor-exit(r32)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.w.a(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0032 A[SYNTHETIC, Splitter:B:22:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0039 A[SYNTHETIC, Splitter:B:27:0x0039] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int c() {
        /*
            r5 = this;
            monitor-enter(r5)
            r0 = 0
            r1 = 0
            java.lang.String r2 = "select count(*) from reward_report"
            android.database.sqlite.SQLiteDatabase r3 = r5.a()     // Catch:{ Exception -> 0x002c }
            android.database.Cursor r2 = r3.rawQuery(r2, r0)     // Catch:{ Exception -> 0x002c }
            if (r2 == 0) goto L_0x0024
            boolean r0 = r2.moveToFirst()     // Catch:{ Exception -> 0x001f, all -> 0x001b }
            if (r0 == 0) goto L_0x0024
            int r0 = r2.getInt(r1)     // Catch:{ Exception -> 0x001f, all -> 0x001b }
            r1 = r0
            goto L_0x0024
        L_0x001b:
            r0 = move-exception
            r1 = r0
            r0 = r2
            goto L_0x0037
        L_0x001f:
            r0 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x002d
        L_0x0024:
            if (r2 == 0) goto L_0x0035
            r2.close()     // Catch:{ all -> 0x003d }
            goto L_0x0035
        L_0x002a:
            r1 = move-exception
            goto L_0x0037
        L_0x002c:
            r2 = move-exception
        L_0x002d:
            r2.printStackTrace()     // Catch:{ all -> 0x002a }
            if (r0 == 0) goto L_0x0035
            r0.close()     // Catch:{ all -> 0x003d }
        L_0x0035:
            monitor-exit(r5)
            return r1
        L_0x0037:
            if (r0 == 0) goto L_0x003f
            r0.close()     // Catch:{ all -> 0x003d }
            goto L_0x003f
        L_0x003d:
            r0 = move-exception
            goto L_0x0040
        L_0x003f:
            throw r1     // Catch:{ all -> 0x003d }
        L_0x0040:
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.w.c():int");
    }

    public final synchronized long a(q qVar) {
        if (qVar != null) {
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("key", qVar.m());
                contentValues.put("networkType", Integer.valueOf(qVar.t()));
                contentValues.put("network_str", qVar.u());
                contentValues.put("isCompleteView", Integer.valueOf(qVar.v()));
                contentValues.put("watchedMillis", Integer.valueOf(qVar.n()));
                contentValues.put("videoLength", Integer.valueOf(qVar.o()));
                if (!TextUtils.isEmpty(qVar.p())) {
                    contentValues.put("offerUrl", URLEncoder.encode(qVar.p(), "utf-8"));
                }
                if (!TextUtils.isEmpty(qVar.q())) {
                    contentValues.put(IronSourceConstants.EVENTS_ERROR_REASON, URLEncoder.encode(qVar.q(), "utf-8"));
                }
                contentValues.put(IronSourceConstants.EVENTS_RESULT, Integer.valueOf(qVar.w()));
                contentValues.put(IronSourceConstants.EVENTS_DURATION, qVar.r());
                contentValues.put("videoSize", Integer.valueOf(qVar.s()));
                contentValues.put("type", qVar.g());
                String f = qVar.f();
                if (!TextUtils.isEmpty(f)) {
                    contentValues.put(CampaignEx.ENDCARD_URL, URLEncoder.encode(f, "utf-8"));
                }
                String e = qVar.e();
                if (!TextUtils.isEmpty(e)) {
                    contentValues.put("video_url", URLEncoder.encode(e, "utf-8"));
                }
                String j = qVar.j();
                if (!TextUtils.isEmpty(j)) {
                    contentValues.put("rid", URLEncoder.encode(j, "utf-8"));
                }
                String i = qVar.i();
                if (!TextUtils.isEmpty(i)) {
                    contentValues.put(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_TEMPLATE_URL, URLEncoder.encode(i, "utf-8"));
                }
                String d = qVar.d();
                if (!TextUtils.isEmpty(d)) {
                    contentValues.put("image_url", URLEncoder.encode(d, "utf-8"));
                }
                String h = qVar.h();
                if (!TextUtils.isEmpty(h)) {
                    contentValues.put("ad_type", URLEncoder.encode(h, "utf-8"));
                }
                contentValues.put("unitId", qVar.k());
                contentValues.put(Constants.RequestParameters.CAMPAIGN_ID, qVar.l());
                if ("2000039".equals(qVar.m())) {
                    String g = q.g(qVar);
                    if (s.b(g)) {
                        contentValues.put("h5_click_data", g);
                    }
                }
                String a = qVar.a();
                if (!TextUtils.isEmpty(a)) {
                    contentValues.put("resource_type", URLEncoder.encode(a, "utf-8"));
                }
                String b2 = qVar.b();
                if (!TextUtils.isEmpty(b2)) {
                    contentValues.put("device_id", URLEncoder.encode(b2, "utf-8"));
                }
                String c2 = qVar.c();
                if (!TextUtils.isEmpty(c2)) {
                    contentValues.put("creative", URLEncoder.encode(c2, "utf-8"));
                }
                return b().insert("reward_report", null, contentValues);
            } catch (Exception e2) {
                e2.printStackTrace();
                g.d(b, e2.getMessage());
            }
        }
        return -1;
    }
}
