package com.mintegral.msdk.base.entity;

/* compiled from: LoadTime */
public final class i {
    private int a;
    private String b;
    private int c;
    private String d;
    private int e;
    private int f;
    private int g;
    private String h;
    private int i;

    public final int a() {
        return this.i;
    }

    public final void a(int i2) {
        this.i = i2;
    }

    public i(int i2, String str, int i3, String str2, int i4, int i5, int i6) {
        this.a = i2;
        this.b = str;
        this.c = i3;
        this.d = str2;
        this.e = i4;
        this.f = i5;
        this.g = i6;
    }

    public i() {
    }

    public final int b() {
        return this.a;
    }

    public final void c() {
        this.a = 1;
    }

    public final String d() {
        return this.b;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final int e() {
        return this.c;
    }

    public final void b(int i2) {
        this.c = i2;
    }

    public final String f() {
        return this.d;
    }

    public final void b(String str) {
        this.d = str;
    }

    public final int g() {
        return this.e;
    }

    public final void c(int i2) {
        this.e = i2;
    }

    public final int h() {
        return this.f;
    }

    public final void i() {
        this.f = 15000;
    }

    public final int j() {
        return this.g;
    }

    public final void d(int i2) {
        this.g = i2;
    }

    public final String k() {
        return this.h;
    }

    public final void c(String str) {
        this.h = str;
    }
}
