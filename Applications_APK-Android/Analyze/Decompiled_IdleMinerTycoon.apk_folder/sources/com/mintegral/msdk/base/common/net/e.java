package com.mintegral.msdk.base.common.net;

import com.mintegral.msdk.base.utils.g;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicHeader;

/* compiled from: CommonHttpMultipartEntity */
final class e implements HttpEntity {
    /* access modifiers changed from: private */
    public static final byte[] a = "\r\n".getBytes();
    /* access modifiers changed from: private */
    public static final byte[] b = "Content-Transfer-Encoding: 8bit\r\n".getBytes();
    private String c = UUID.randomUUID().toString();
    /* access modifiers changed from: private */
    public byte[] d = ("--" + this.c + "\r\n").getBytes();
    private byte[] e = ("--" + this.c + "--\r\n").getBytes();
    private ByteArrayOutputStream f = new ByteArrayOutputStream();
    private List<a> g = new ArrayList();
    private g h;
    private int i;
    private int j;

    public final void consumeContent() throws IOException {
    }

    public final Header getContentEncoding() {
        return null;
    }

    public final boolean isChunked() {
        return false;
    }

    public final boolean isRepeatable() {
        return true;
    }

    public final boolean isStreaming() {
        return false;
    }

    e(g gVar) {
        this.h = gVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, File file, String str2, String str3) {
        this.g.add(new a(str, file, str2, str3));
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, String str2, InputStream inputStream, String str3) throws IOException {
        this.f.write(this.d);
        this.f.write(c(str, str2));
        this.f.write(b(str3));
        this.f.write(b);
        this.f.write(a);
        byte[] bArr = new byte[4096];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                this.f.write(bArr, 0, read);
            } else {
                this.f.write(a);
                this.f.flush();
                try {
                    inputStream.close();
                    return;
                } catch (IOException e2) {
                    g.c("HttpMultipartEntity", "Cannot close input stream", e2);
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static byte[] b(String str) {
        return ("Content-Type: " + str + "\r\n").getBytes();
    }

    /* access modifiers changed from: private */
    public static byte[] c(String str, String str2) {
        return ("Content-Disposition: form-data; name=\"" + str + "\"; filename=\"" + str2 + "\"\r\n").getBytes();
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        this.i += i2;
        if (this.h != null) {
            this.h.a(this.i, this.j);
        }
    }

    public final long getContentLength() {
        long size = (long) this.f.size();
        for (a a2 : this.g) {
            long a3 = a2.a();
            if (a3 < 0) {
                g.d("HttpMultipartEntity", "get FileParam length failed.");
                return -1;
            }
            size += a3;
        }
        return size + ((long) this.e.length);
    }

    public final Header getContentType() {
        return new BasicHeader(HttpRequest.HEADER_CONTENT_TYPE, "multipart/form-data; boundary=" + this.c);
    }

    public final void writeTo(OutputStream outputStream) throws IOException {
        this.i = 0;
        this.j = (int) getContentLength();
        this.f.writeTo(outputStream);
        a(this.f.size());
        for (a a2 : this.g) {
            a2.a(outputStream);
        }
        outputStream.write(this.e);
        a(this.e.length);
    }

    public final InputStream getContent() throws IOException {
        return new ByteArrayInputStream(toString().getBytes());
    }

    /* compiled from: CommonHttpMultipartEntity */
    private class a {
        private byte[] b;
        private File c;

        public a(String str, File file, String str2, String str3) {
            this.b = a(str, str2, str3);
            this.c = file;
        }

        private byte[] a(String str, String str2, String str3) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                byteArrayOutputStream.write(e.this.d);
                byteArrayOutputStream.write(e.c(str, str2));
                byteArrayOutputStream.write(e.b(str3));
                byteArrayOutputStream.write(e.b);
                byteArrayOutputStream.write(e.a);
            } catch (IOException e) {
                g.c("HttpMultipartEntity", "FileParam createHeader to RequestParamBufferStream exception", e);
            }
            return byteArrayOutputStream.toByteArray();
        }

        public final long a() {
            return ((long) this.b.length) + this.c.length() + ((long) e.a.length);
        }

        /* JADX WARNING: Removed duplicated region for block: B:25:0x0059 A[SYNTHETIC, Splitter:B:25:0x0059] */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x005f A[SYNTHETIC, Splitter:B:28:0x005f] */
        /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a(java.io.OutputStream r5) throws java.io.IOException {
            /*
                r4 = this;
                byte[] r0 = r4.b
                r5.write(r0)
                com.mintegral.msdk.base.common.net.e r0 = com.mintegral.msdk.base.common.net.e.this
                byte[] r1 = r4.b
                int r1 = r1.length
                r0.a(r1)
                r0 = 0
                java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0053 }
                java.io.File r2 = r4.c     // Catch:{ Throwable -> 0x0053 }
                r1.<init>(r2)     // Catch:{ Throwable -> 0x0053 }
                r0 = 4096(0x1000, float:5.74E-42)
                byte[] r0 = new byte[r0]     // Catch:{ Throwable -> 0x004d, all -> 0x004b }
            L_0x0019:
                int r2 = r1.read(r0)     // Catch:{ Throwable -> 0x004d, all -> 0x004b }
                r3 = -1
                if (r2 == r3) goto L_0x002a
                r3 = 0
                r5.write(r0, r3, r2)     // Catch:{ Throwable -> 0x004d, all -> 0x004b }
                com.mintegral.msdk.base.common.net.e r3 = com.mintegral.msdk.base.common.net.e.this     // Catch:{ Throwable -> 0x004d, all -> 0x004b }
                r3.a(r2)     // Catch:{ Throwable -> 0x004d, all -> 0x004b }
                goto L_0x0019
            L_0x002a:
                byte[] r0 = com.mintegral.msdk.base.common.net.e.a     // Catch:{ Throwable -> 0x004d, all -> 0x004b }
                r5.write(r0)     // Catch:{ Throwable -> 0x004d, all -> 0x004b }
                com.mintegral.msdk.base.common.net.e r0 = com.mintegral.msdk.base.common.net.e.this     // Catch:{ Throwable -> 0x004d, all -> 0x004b }
                byte[] r2 = com.mintegral.msdk.base.common.net.e.a     // Catch:{ Throwable -> 0x004d, all -> 0x004b }
                int r2 = r2.length     // Catch:{ Throwable -> 0x004d, all -> 0x004b }
                r0.a(r2)     // Catch:{ Throwable -> 0x004d, all -> 0x004b }
                r5.flush()     // Catch:{ Throwable -> 0x004d, all -> 0x004b }
                r1.close()     // Catch:{ IOException -> 0x0042 }
                return
            L_0x0042:
                r5 = move-exception
                java.lang.String r0 = "HttpMultipartEntity"
                java.lang.String r1 = "Cannot close input stream"
                com.mintegral.msdk.base.utils.g.c(r0, r1, r5)
                return
            L_0x004b:
                r5 = move-exception
                goto L_0x005d
            L_0x004d:
                r5 = move-exception
                r0 = r1
                goto L_0x0054
            L_0x0050:
                r5 = move-exception
                r1 = r0
                goto L_0x005d
            L_0x0053:
                r5 = move-exception
            L_0x0054:
                r5.printStackTrace()     // Catch:{ all -> 0x0050 }
                if (r0 == 0) goto L_0x005c
                r0.close()     // Catch:{ IOException -> 0x0042 }
            L_0x005c:
                return
            L_0x005d:
                if (r1 == 0) goto L_0x006b
                r1.close()     // Catch:{ IOException -> 0x0063 }
                goto L_0x006b
            L_0x0063:
                r0 = move-exception
                java.lang.String r1 = "HttpMultipartEntity"
                java.lang.String r2 = "Cannot close input stream"
                com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
            L_0x006b:
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.common.net.e.a.a(java.io.OutputStream):void");
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, String str2) {
        try {
            this.f.write(this.d);
            ByteArrayOutputStream byteArrayOutputStream = this.f;
            byteArrayOutputStream.write(("Content-Disposition: form-data; name=\"" + str + "\"\r\n").getBytes());
            this.f.write(b("text/plain; charset=UTF-8"));
            this.f.write(a);
            this.f.write(str2.getBytes());
            this.f.write(a);
        } catch (IOException e2) {
            g.c("HttpMultipartEntity", "addParam to RequestParamBufferStream exception", e2);
        }
    }
}
