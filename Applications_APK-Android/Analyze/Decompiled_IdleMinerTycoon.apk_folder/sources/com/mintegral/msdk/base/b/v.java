package com.mintegral.msdk.base.b;

import android.database.Cursor;
import com.mintegral.msdk.out.Campaign;

/* compiled from: VideoDao */
public class v extends a<Campaign> {
    private static v b;

    private v(h hVar) {
        super(hVar);
    }

    public static v a(h hVar) {
        if (b == null) {
            synchronized (v.class) {
                if (b == null) {
                    b = new v(hVar);
                }
            }
        }
        return b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x008e, code lost:
        if (r7 != null) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x009f, code lost:
        if (r7 != null) goto L_0x0090;
     */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x009a A[Catch:{ Exception -> 0x009e, all -> 0x0094 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.mintegral.msdk.base.entity.p a(java.lang.String r7, java.lang.String r8) {
        /*
            r6 = this;
            monitor-enter(r6)
            com.mintegral.msdk.base.entity.p r0 = new com.mintegral.msdk.base.entity.p     // Catch:{ all -> 0x00a4 }
            r0.<init>()     // Catch:{ all -> 0x00a4 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a4 }
            java.lang.String r2 = " WHERE video_url = '"
            r1.<init>(r2)     // Catch:{ all -> 0x00a4 }
            r1.append(r7)     // Catch:{ all -> 0x00a4 }
            java.lang.String r7 = "' AND ad_bid_token = '"
            r1.append(r7)     // Catch:{ all -> 0x00a4 }
            r1.append(r8)     // Catch:{ all -> 0x00a4 }
            java.lang.String r7 = "'"
            r1.append(r7)     // Catch:{ all -> 0x00a4 }
            java.lang.String r7 = r1.toString()     // Catch:{ all -> 0x00a4 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a4 }
            java.lang.String r1 = "SELECT * FROM video"
            r8.<init>(r1)     // Catch:{ all -> 0x00a4 }
            r8.append(r7)     // Catch:{ all -> 0x00a4 }
            java.lang.String r7 = r8.toString()     // Catch:{ all -> 0x00a4 }
            r8 = 0
            android.database.sqlite.SQLiteDatabase r1 = r6.a()     // Catch:{ Exception -> 0x009e, all -> 0x0094 }
            android.database.Cursor r7 = r1.rawQuery(r7, r8)     // Catch:{ Exception -> 0x009e, all -> 0x0094 }
            if (r7 == 0) goto L_0x008e
            int r8 = r7.getCount()     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            if (r8 <= 0) goto L_0x008e
        L_0x0040:
            boolean r8 = r7.moveToNext()     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            if (r8 == 0) goto L_0x008e
            java.lang.String r8 = "video_url"
            int r8 = r7.getColumnIndex(r8)     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            java.lang.String r8 = r7.getString(r8)     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            r0.a(r8)     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            java.lang.String r8 = "video_state"
            int r8 = r7.getColumnIndex(r8)     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            int r8 = r7.getInt(r8)     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            r0.b(r8)     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            java.lang.String r8 = "pregeress_size"
            int r8 = r7.getColumnIndex(r8)     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            long r1 = r7.getLong(r8)     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            r0.b(r1)     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            java.lang.String r8 = "total_size"
            int r8 = r7.getColumnIndex(r8)     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            int r8 = r7.getInt(r8)     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            r0.a(r8)     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            java.lang.String r8 = "video_download_start"
            int r8 = r7.getColumnIndex(r8)     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            long r1 = r7.getLong(r8)     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            r3 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 * r3
            r0.a(r1)     // Catch:{ Exception -> 0x009f, all -> 0x008c }
            goto L_0x0040
        L_0x008c:
            r8 = move-exception
            goto L_0x0098
        L_0x008e:
            if (r7 == 0) goto L_0x00a2
        L_0x0090:
            r7.close()     // Catch:{ all -> 0x00a4 }
            goto L_0x00a2
        L_0x0094:
            r7 = move-exception
            r5 = r8
            r8 = r7
            r7 = r5
        L_0x0098:
            if (r7 == 0) goto L_0x009d
            r7.close()     // Catch:{ all -> 0x00a4 }
        L_0x009d:
            throw r8     // Catch:{ all -> 0x00a4 }
        L_0x009e:
            r7 = r8
        L_0x009f:
            if (r7 == 0) goto L_0x00a2
            goto L_0x0090
        L_0x00a2:
            monitor-exit(r6)
            return r0
        L_0x00a4:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.v.a(java.lang.String, java.lang.String):com.mintegral.msdk.base.entity.p");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.lang.String r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            java.lang.String r1 = "video_url = '"
            r0.<init>(r1)     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            r0.append(r4)     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            java.lang.String r4 = "'"
            r0.append(r4)     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            java.lang.String r4 = r0.toString()     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            android.database.sqlite.SQLiteDatabase r0 = r3.b()     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            if (r0 != 0) goto L_0x001c
            monitor-exit(r3)
            return
        L_0x001c:
            android.database.sqlite.SQLiteDatabase r0 = r3.b()     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            java.lang.String r1 = "video"
            r2 = 0
            r0.delete(r1, r4, r2)     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            monitor-exit(r3)
            return
        L_0x0028:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        L_0x002b:
            monitor-exit(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.v.a(java.lang.String):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x017a, code lost:
        return -1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized long a(com.mintegral.msdk.base.entity.CampaignEx r10, long r11) {
        /*
            r9 = this;
            monitor-enter(r9)
            r0 = 0
            if (r10 != 0) goto L_0x0007
            monitor-exit(r9)
            return r0
        L_0x0007:
            r2 = -1
            android.database.sqlite.SQLiteDatabase r4 = r9.b()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            if (r4 != 0) goto L_0x0011
            monitor-exit(r9)
            return r2
        L_0x0011:
            android.content.ContentValues r4 = new android.content.ContentValues     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.<init>()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "id"
            java.lang.String r6 = r10.getId()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "package_name"
            java.lang.String r6 = r10.getPackageName()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "app_name"
            java.lang.String r6 = r10.getAppName()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "app_desc"
            java.lang.String r6 = r10.getAppDesc()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "app_size"
            java.lang.String r6 = r10.getSize()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "image_size"
            java.lang.String r6 = r10.getImageSize()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "icon_url"
            java.lang.String r6 = r10.getIconUrl()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "image_url"
            java.lang.String r6 = r10.getImageUrl()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "impression_url"
            java.lang.String r6 = r10.getImpressionURL()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "notice_url"
            java.lang.String r6 = r10.getNoticeUrl()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "download_url"
            java.lang.String r6 = r10.getClickURL()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "only_impression"
            java.lang.String r6 = r10.getOnlyImpressionURL()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "ts"
            long r6 = r10.getTimestamp()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "template"
            int r6 = r10.getTemplate()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "click_mode"
            java.lang.String r6 = r10.getClick_mode()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "landing_type"
            java.lang.String r6 = r10.getLandingType()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "link_type"
            int r6 = r10.getLinkType()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "star"
            double r6 = r10.getRating()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.Double r6 = java.lang.Double.valueOf(r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "cti"
            int r6 = r10.getClickInterval()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "cpti"
            int r6 = r10.getPreClickInterval()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "preclick"
            boolean r6 = r10.isPreClick()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "level"
            int r6 = r10.getCacheLevel()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "adSource"
            int r6 = r10.getType()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "ad_call"
            java.lang.String r6 = r10.getAdCall()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "fc_a"
            int r6 = r10.getFca()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "ad_url_list"
            java.lang.String r6 = r10.getAd_url_list()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "video_url"
            java.lang.String r6 = r10.getVideoUrlEncode()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r5 = "total_size"
            java.lang.Long r11 = java.lang.Long.valueOf(r11)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r5, r11)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r11 = "video_state"
            r12 = 0
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r11, r12)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r11 = "video_download_start"
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r7 = 1000(0x3e8, double:4.94E-321)
            long r5 = r5 / r7
            java.lang.Long r12 = java.lang.Long.valueOf(r5)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r11, r12)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r11 = "ad_bid_token"
            java.lang.String r12 = r10.getBidToken()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            r4.put(r11, r12)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r10 = r10.getVideoUrlEncode()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            boolean r10 = r9.b(r10)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            if (r10 == 0) goto L_0x0169
            monitor-exit(r9)
            return r0
        L_0x0169:
            android.database.sqlite.SQLiteDatabase r10 = r9.b()     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            java.lang.String r11 = "video"
            r12 = 0
            long r10 = r10.insert(r11, r12, r4)     // Catch:{ Exception -> 0x0179, all -> 0x0176 }
            monitor-exit(r9)
            return r10
        L_0x0176:
            r10 = move-exception
            monitor-exit(r9)
            throw r10
        L_0x0179:
            monitor-exit(r9)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.v.a(com.mintegral.msdk.base.entity.CampaignEx, long):long");
    }

    private synchronized boolean b(String str) {
        Cursor rawQuery = a().rawQuery("SELECT id FROM video WHERE video_url = '" + str + "'", null);
        if (rawQuery == null || rawQuery.getCount() <= 0) {
            if (rawQuery != null) {
                rawQuery.close();
            }
            return false;
        }
        rawQuery.close();
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x004c, code lost:
        r0 = r4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final long a(java.lang.String r4, long r5, int r7) {
        /*
            r3 = this;
            r0 = -1
            android.database.sqlite.SQLiteDatabase r1 = r3.b()     // Catch:{ Exception -> 0x0054 }
            if (r1 != 0) goto L_0x000a
            r4 = -1
            return r4
        L_0x000a:
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ Exception -> 0x0054 }
            r1.<init>()     // Catch:{ Exception -> 0x0054 }
            java.lang.String r2 = "pregeress_size"
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ Exception -> 0x0054 }
            r1.put(r2, r5)     // Catch:{ Exception -> 0x0054 }
            java.lang.String r5 = "video_state"
            java.lang.Integer r6 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0054 }
            r1.put(r5, r6)     // Catch:{ Exception -> 0x0054 }
            boolean r5 = r3.b(r4)     // Catch:{ Exception -> 0x0054 }
            if (r5 == 0) goto L_0x0054
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0054 }
            java.lang.String r6 = "video_url = '"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0054 }
            r5.append(r4)     // Catch:{ Exception -> 0x0054 }
            java.lang.String r4 = "'"
            r5.append(r4)     // Catch:{ Exception -> 0x0054 }
            java.lang.String r4 = r5.toString()     // Catch:{ Exception -> 0x0054 }
            java.lang.Object r5 = new java.lang.Object     // Catch:{ Exception -> 0x0054 }
            r5.<init>()     // Catch:{ Exception -> 0x0054 }
            monitor-enter(r5)     // Catch:{ Exception -> 0x0054 }
            android.database.sqlite.SQLiteDatabase r6 = r3.b()     // Catch:{ all -> 0x0051 }
            java.lang.String r7 = "video"
            r2 = 0
            int r4 = r6.update(r7, r1, r4, r2)     // Catch:{ all -> 0x0051 }
            monitor-exit(r5)     // Catch:{ all -> 0x004e }
            r0 = r4
            goto L_0x0054
        L_0x004e:
            r6 = move-exception
            r0 = r4
            goto L_0x0052
        L_0x0051:
            r6 = move-exception
        L_0x0052:
            monitor-exit(r5)     // Catch:{ all -> 0x0051 }
            throw r6     // Catch:{ Exception -> 0x0054 }
        L_0x0054:
            long r4 = (long) r0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.v.a(java.lang.String, long, int):long");
    }
}
