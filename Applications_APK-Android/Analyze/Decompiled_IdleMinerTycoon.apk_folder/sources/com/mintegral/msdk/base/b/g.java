package com.mintegral.msdk.base.b;

/* compiled from: CampaignReportDao */
public class g extends f {
    private static final String b = "com.mintegral.msdk.base.b.g";
    private static g c;

    private g(h hVar) {
        super(hVar);
    }

    public static g b(h hVar) {
        if (c == null) {
            synchronized (g.class) {
                if (c == null) {
                    c = new g(hVar);
                }
            }
        }
        return c;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x02eb, code lost:
        return -1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized long a(com.mintegral.msdk.base.entity.CampaignEx r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            if (r7 != 0) goto L_0x0007
            r0 = 0
            monitor-exit(r6)
            return r0
        L_0x0007:
            r0 = -1
            android.database.sqlite.SQLiteDatabase r2 = r6.b()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            if (r2 != 0) goto L_0x0011
            monitor-exit(r6)
            return r0
        L_0x0011:
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.<init>()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "id"
            java.lang.String r4 = r7.getId()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "unitid"
            java.lang.String r4 = r7.getCampaignUnitId()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "tab"
            int r4 = r7.getTab()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "package_name"
            java.lang.String r4 = r7.getPackageName()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "app_name"
            java.lang.String r4 = r7.getAppName()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "app_desc"
            java.lang.String r4 = r7.getAppDesc()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "app_size"
            java.lang.String r4 = r7.getSize()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "image_size"
            java.lang.String r4 = r7.getImageSize()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "icon_url"
            java.lang.String r4 = r7.getIconUrl()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "image_url"
            java.lang.String r4 = r7.getImageUrl()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "impression_url"
            java.lang.String r4 = r7.getImpressionURL()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "notice_url"
            java.lang.String r4 = r7.getNoticeUrl()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "download_url"
            java.lang.String r4 = r7.getClickURL()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "wtick"
            int r4 = r7.getWtick()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "only_impression"
            java.lang.String r4 = r7.getOnlyImpressionURL()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "ts"
            long r4 = r7.getTimestamp()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "template"
            int r4 = r7.getTemplate()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "click_mode"
            java.lang.String r4 = r7.getClick_mode()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "landing_type"
            java.lang.String r4 = r7.getLandingType()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "link_type"
            int r4 = r7.getLinkType()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "star"
            double r4 = r7.getRating()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Double r4 = java.lang.Double.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "cti"
            int r4 = r7.getClickInterval()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "cpti"
            int r4 = r7.getPreClickInterval()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "preclick"
            boolean r4 = r7.isPreClick()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "level"
            int r4 = r7.getCacheLevel()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "adSource"
            int r4 = r7.getType()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "ad_call"
            java.lang.String r4 = r7.getAdCall()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "fc_a"
            int r4 = r7.getFca()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "ad_url_list"
            java.lang.String r4 = r7.getAd_url_list()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "video_url"
            java.lang.String r4 = r7.getVideoUrlEncode()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "video_size"
            int r4 = r7.getVideoSize()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "video_length"
            int r4 = r7.getVideoLength()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "video_resolution"
            java.lang.String r4 = r7.getVideoResolution()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "endcard_click_result"
            int r4 = r7.getEndcard_click_result()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "watch_mile"
            int r4 = r7.getWatchMile()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "advImp"
            java.lang.String r4 = r7.getAdvImp()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "bty"
            int r4 = r7.getBty()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "t_imp"
            int r4 = r7.getTImp()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "guidelines"
            java.lang.String r4 = r7.getGuidelines()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "offer_type"
            int r4 = r7.getOfferType()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "html_url"
            java.lang.String r4 = r7.getHtmlUrl()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "end_screen_url"
            java.lang.String r4 = r7.getEndScreenUrl()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "reward_amount"
            int r4 = r7.getRewardAmount()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "reward_name"
            java.lang.String r4 = r7.getRewardName()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "reward_play_status"
            int r4 = r7.getRewardPlayStatus()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "adv_id"
            java.lang.String r4 = r7.getAdvId()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "ttc_ct2"
            int r4 = r7.getTtc_ct2()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            int r4 = r4 * 1000
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "ttc_type"
            int r4 = r7.getTtc_type()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "retarget"
            int r4 = r7.getRetarget_offer()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "native_ad_tracking"
            java.lang.String r4 = r7.getNativeVideoTrackingString()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "playable_ads_without_video"
            int r4 = r7.getPlayable_ads_without_video()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "endcard_url"
            java.lang.String r4 = r7.getendcard_url()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "video_end_type"
            int r4 = r7.getVideo_end_type()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "loopback"
            java.lang.String r4 = r7.getLoopbackString()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "md5_file"
            java.lang.String r4 = r7.getVideoMD5Value()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "nv_t2"
            int r4 = r7.getNvT2()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "gif_url"
            java.lang.String r4 = r7.getGifUrl()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            com.mintegral.msdk.base.entity.CampaignEx$c r3 = r7.getRewardTemplateMode()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            if (r3 == 0) goto L_0x0298
            java.lang.String r3 = "reward_teamplate"
            com.mintegral.msdk.base.entity.CampaignEx$c r4 = r7.getRewardTemplateMode()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r4 = r4.a()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
        L_0x0298:
            java.lang.String r3 = "c_coi"
            int r4 = r7.getClickTimeOutInterval()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "c_ua"
            int r4 = r7.getcUA()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "imp_ua"
            int r4 = r7.getImpUA()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "gh_id"
            java.lang.String r4 = r7.getGhId()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "gh_path"
            java.lang.String r4 = r7.getGhPath()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "bind_id"
            java.lang.String r7 = r7.getBindId()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            r2.put(r3, r7)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            android.database.sqlite.SQLiteDatabase r7 = r6.b()     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            java.lang.String r3 = "report_campaign"
            r4 = 0
            long r2 = r7.insert(r3, r4, r2)     // Catch:{ Exception -> 0x02ea, all -> 0x02e7 }
            monitor-exit(r6)
            return r2
        L_0x02e7:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        L_0x02ea:
            monitor-exit(r6)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.g.a(com.mintegral.msdk.base.entity.CampaignEx):long");
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x03c8  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x03d0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.mintegral.msdk.base.entity.CampaignEx l(java.lang.String r6) {
        /*
            r5 = this;
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03bf, all -> 0x03ba }
            java.lang.String r2 = "SELECT * FROM report_campaign where package_name ='"
            r1.<init>(r2)     // Catch:{ Exception -> 0x03bf, all -> 0x03ba }
            r1.append(r6)     // Catch:{ Exception -> 0x03bf, all -> 0x03ba }
            java.lang.String r6 = "'"
            r1.append(r6)     // Catch:{ Exception -> 0x03bf, all -> 0x03ba }
            java.lang.String r6 = r1.toString()     // Catch:{ Exception -> 0x03bf, all -> 0x03ba }
            android.database.sqlite.SQLiteDatabase r1 = r5.a()     // Catch:{ Exception -> 0x03bf, all -> 0x03ba }
            android.database.Cursor r6 = r1.rawQuery(r6, r0)     // Catch:{ Exception -> 0x03bf, all -> 0x03ba }
            if (r6 == 0) goto L_0x03b4
            int r1 = r6.getCount()     // Catch:{ Exception -> 0x03af }
            if (r1 <= 0) goto L_0x03b4
            boolean r1 = r6.moveToNext()     // Catch:{ Exception -> 0x03af }
            if (r1 == 0) goto L_0x03b4
            com.mintegral.msdk.base.entity.CampaignEx r1 = new com.mintegral.msdk.base.entity.CampaignEx     // Catch:{ Exception -> 0x03af }
            r1.<init>()     // Catch:{ Exception -> 0x03af }
            java.lang.String r0 = "id"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setId(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "tab"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setTab(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "package_name"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setPackageName(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "app_name"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setAppName(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "app_desc"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setAppDesc(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "app_size"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setSize(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "image_size"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setImageSize(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "icon_url"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setIconUrl(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "image_url"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setImageUrl(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "impression_url"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setImpressionURL(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "notice_url"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setNoticeUrl(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "download_url"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setClickURL(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "wtick"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setWtick(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "only_impression"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setOnlyImpressionURL(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "preclick"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r2 = 1
            if (r0 != r2) goto L_0x00f3
            goto L_0x00f4
        L_0x00f3:
            r2 = 0
        L_0x00f4:
            r1.setPreClick(r2)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "template"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setTemplate(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "landing_type"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setLandingType(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "link_type"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setLinkType(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "click_mode"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setClick_mode(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "star"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            double r2 = java.lang.Double.parseDouble(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setRating(r2)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "cti"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setClickInterval(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "cpti"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setPreClickInterval(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "ts"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            long r2 = r6.getLong(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setTimestamp(r2)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "level"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setCacheLevel(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "ad_call"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setAdCall(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "fc_a"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setFca(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "ad_url_list"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setAd_url_list(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "video_length"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setVideoLength(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "video_size"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setVideoSize(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "video_resolution"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setVideoResolution(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "endcard_click_result"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setEndcard_click_result(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "video_url"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setVideoUrlEncode(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "watch_mile"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setWatchMile(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "t_imp"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setTImp(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "bty"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setBty(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "advImp"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setAdvImp(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "guidelines"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setGuidelines(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "offer_type"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setOfferType(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "html_url"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setHtmlUrl(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "guidelines"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setGuidelines(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "html_url"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setHtmlUrl(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "end_screen_url"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setEndScreenUrl(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "reward_name"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setRewardName(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "reward_amount"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setRewardAmount(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "reward_play_status"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setRewardPlayStatus(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "adv_id"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setAdvId(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "ttc_ct2"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setTtc_ct2(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "ttc_type"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setTtc_type(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "retarget"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setRetarget_offer(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "unitid"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setCampaignUnitId(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "native_ad_tracking"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            com.mintegral.msdk.base.entity.j r0 = com.mintegral.msdk.base.entity.CampaignEx.TrackingStr2Object(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setNativeVideoTracking(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "native_ad_tracking"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setNativeVideoTrackingString(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "video_end_type"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setVideo_end_type(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "endcard_url"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setendcard_url(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "playable_ads_without_video"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setPlayable_ads_without_video(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "loopback"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setLoopbackString(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "loopback"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            java.util.Map r0 = com.mintegral.msdk.base.entity.CampaignEx.loopbackStrToMap(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setLoopbackMap(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "reward_teamplate"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            com.mintegral.msdk.base.entity.CampaignEx$c r0 = com.mintegral.msdk.base.entity.CampaignEx.c.a(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setRewardTemplateMode(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "md5_file"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setVideoMD5Value(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "gif_url"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setGifUrl(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "nv_t2"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setNvT2(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "c_coi"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setClickTimeOutInterval(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "c_ua"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setcUA(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "imp_ua"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            int r0 = r6.getInt(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setImpUA(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "gh_id"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setGhId(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "gh_path"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setGhPath(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = "bind_id"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x03ad }
            r1.setBindId(r0)     // Catch:{ Exception -> 0x03ad }
            r0 = r1
            goto L_0x03b4
        L_0x03ad:
            r0 = move-exception
            goto L_0x03c3
        L_0x03af:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x03c3
        L_0x03b4:
            if (r6 == 0) goto L_0x03cc
            r6.close()
            goto L_0x03cc
        L_0x03ba:
            r6 = move-exception
            r4 = r0
            r0 = r6
            r6 = r4
            goto L_0x03ce
        L_0x03bf:
            r6 = move-exception
            r1 = r0
            r0 = r6
            r6 = r1
        L_0x03c3:
            r0.printStackTrace()     // Catch:{ all -> 0x03cd }
            if (r6 == 0) goto L_0x03cb
            r6.close()
        L_0x03cb:
            r0 = r1
        L_0x03cc:
            return r0
        L_0x03cd:
            r0 = move-exception
        L_0x03ce:
            if (r6 == 0) goto L_0x03d3
            r6.close()
        L_0x03d3:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.g.l(java.lang.String):com.mintegral.msdk.base.entity.CampaignEx");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void m(java.lang.String r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            java.lang.String r1 = "package_name = '"
            r0.<init>(r1)     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            r0.append(r4)     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            java.lang.String r4 = "'"
            r0.append(r4)     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            java.lang.String r4 = r0.toString()     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            android.database.sqlite.SQLiteDatabase r0 = r3.b()     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            if (r0 != 0) goto L_0x001c
            monitor-exit(r3)
            return
        L_0x001c:
            android.database.sqlite.SQLiteDatabase r0 = r3.b()     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            java.lang.String r1 = "report_campaign"
            r2 = 0
            r0.delete(r1, r4, r2)     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            monitor-exit(r3)
            return
        L_0x0028:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        L_0x002b:
            monitor-exit(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.g.m(java.lang.String):void");
    }
}
