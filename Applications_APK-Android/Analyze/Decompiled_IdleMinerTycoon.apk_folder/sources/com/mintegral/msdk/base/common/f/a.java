package com.mintegral.msdk.base.common.f;

/* compiled from: CommonTask */
public abstract class a implements Runnable {
    public static long h;
    public int i = C0050a.a;
    public b j;

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    /* renamed from: com.mintegral.msdk.base.common.f.a$a  reason: collision with other inner class name */
    /* compiled from: CommonTask */
    public static final class C0050a extends Enum<C0050a> {
        public static final int a = 1;
        public static final int b = 2;
        public static final int c = 3;
        public static final int d = 4;
        public static final int e = 5;
        private static final /* synthetic */ int[] f = {a, b, c, d, e};
    }

    /* compiled from: CommonTask */
    public interface b {
        void a(int i);
    }

    public abstract void a();

    public abstract void b();

    public final void run() {
        try {
            if (this.i == C0050a.a) {
                a(C0050a.b);
                a();
                a(C0050a.e);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public a() {
        h++;
    }

    public static long c() {
        return h;
    }

    public final void d() {
        if (this.i != C0050a.d) {
            a(C0050a.d);
            b();
        }
    }

    private void a(int i2) {
        this.i = i2;
        if (this.j != null) {
            this.j.a(i2);
        }
    }
}
