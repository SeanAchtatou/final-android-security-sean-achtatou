package com.mintegral.msdk.base.common.e.c;

import android.content.Context;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.r;
import com.mintegral.msdk.base.entity.o;
import java.util.List;

/* compiled from: LogFileController */
public class a {
    public static final String a = "a";
    private static a b;

    private a() {
    }

    public static a a() {
        if (b == null) {
            synchronized (a.class) {
                if (b == null) {
                    b = new a();
                }
            }
        }
        return b;
    }

    public static List<o> a(Context context) {
        if (context != null && r.a(i.a(context)).c() > 0) {
            return r.a(i.a(context)).e();
        }
        return null;
    }
}
