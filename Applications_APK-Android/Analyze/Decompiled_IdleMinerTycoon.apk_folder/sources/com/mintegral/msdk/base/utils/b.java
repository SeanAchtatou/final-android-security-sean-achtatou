package com.mintegral.msdk.base.utils;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import com.mintegral.msdk.base.controller.a;
import java.io.File;

/* compiled from: CommonBatteryStatusUtil */
public final class b {
    private static int a = -1;
    private static int b = -1;
    private static int c = -1;
    private static boolean d = false;
    private static int e = 0;
    private static boolean f = false;
    private static boolean g = false;

    public static int a() {
        try {
            Intent registerReceiver = a.d().h().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int[] iArr = new int[2];
            if (registerReceiver != null) {
                iArr[0] = registerReceiver.getIntExtra("level", 50);
                iArr[1] = registerReceiver.getIntExtra("plugged", 0);
                a(iArr[1] != 0);
                iArr[0] = a(iArr[0], iArr[1]);
                a(iArr[0]);
            } else {
                iArr[0] = 50;
                iArr[1] = 0;
            }
            return iArr[0];
        } catch (Exception unused) {
            if (a != -1) {
                return a;
            }
            return 50;
        }
    }

    private static int a(int i, int i2) {
        String str = Build.MODEL;
        if (str.equalsIgnoreCase("SCH-i909") || str.equalsIgnoreCase("SCH-I535") || str.equalsIgnoreCase("SCH-W899")) {
            if (i > 100) {
                i /= 10;
            }
        } else if (!str.trim().toUpperCase().contains("XT702") && !str.trim().toUpperCase().contains("XT907") && !str.trim().toUpperCase().contains("XT1058") && !str.trim().toUpperCase().contains("XT1080") && Build.MANUFACTURER.equalsIgnoreCase("motorola")) {
            File file = new File("/sys/class/power_supply/battery/charge_counter");
            int parseInt = file.exists() ? Integer.parseInt(a(file)) : 0;
            if (parseInt <= 0) {
                File file2 = new File("/sys/class/power_supply/battery/capacity");
                if (file2.exists()) {
                    Integer.parseInt(a(file2));
                }
            }
            if (parseInt <= 100 && parseInt > 0 && i % 10 == 0) {
                i = parseInt;
            }
        }
        if (i <= 100) {
            return i;
        }
        if (i2 != 0 && i < 110) {
            return 100;
        }
        do {
            i /= 10;
        } while (i > 100);
        return i;
    }

    public static int b() {
        int i;
        try {
            i = a.d().h().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED")).getIntExtra("status", 1);
        } catch (Throwable th) {
            g.c("BatteryStatusUtil", th.getMessage(), th);
            i = 0;
        }
        if (i == 2 || i == 5) {
            return 1;
        }
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x002a A[SYNTHETIC, Splitter:B:16:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0030 A[SYNTHETIC, Splitter:B:22:0x0030] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x003d A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(java.io.File r4) {
        /*
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r1 = 1024(0x400, float:1.435E-42)
            char[] r1 = new char[r1]
            r2 = 0
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x002e, all -> 0x0026 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x002e, all -> 0x0026 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0024, all -> 0x0022 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x0024, all -> 0x0022 }
        L_0x0014:
            int r2 = r4.read(r1)     // Catch:{ IOException -> 0x0024, all -> 0x0022 }
            if (r2 < 0) goto L_0x001e
            a(r2, r1, r0)     // Catch:{ IOException -> 0x0024, all -> 0x0022 }
            goto L_0x0014
        L_0x001e:
            r3.close()     // Catch:{ IOException -> 0x0033 }
            goto L_0x0033
        L_0x0022:
            r4 = move-exception
            goto L_0x0028
        L_0x0024:
            r2 = r3
            goto L_0x002e
        L_0x0026:
            r4 = move-exception
            r3 = r2
        L_0x0028:
            if (r3 == 0) goto L_0x002d
            r3.close()     // Catch:{ IOException -> 0x002d }
        L_0x002d:
            throw r4
        L_0x002e:
            if (r2 == 0) goto L_0x0033
            r2.close()     // Catch:{ IOException -> 0x0033 }
        L_0x0033:
            java.lang.String r4 = r0.toString()
            boolean r0 = android.text.TextUtils.isEmpty(r4)
            if (r0 == 0) goto L_0x003f
            java.lang.String r4 = "0"
        L_0x003f:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.utils.b.a(java.io.File):java.lang.String");
    }

    private static void a(int i, char[] cArr, StringBuilder sb) {
        for (int i2 = 0; i2 < i; i2++) {
            if (!(cArr[i2] == 10 || cArr[i2] == 13)) {
                sb.append(cArr[i2]);
            }
        }
    }

    private static synchronized void a(int i) {
        synchronized (b.class) {
            e = i;
        }
    }

    private static synchronized void a(boolean z) {
        synchronized (b.class) {
            f = z;
        }
    }
}
