package com.mintegral.msdk.base.common.b;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.tapjoy.TJAdUnitConstants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: MIntegralDirContext */
public final class d extends b {
    public d(String str) {
        super(str);
    }

    /* access modifiers changed from: protected */
    public final List<a> b() {
        ArrayList arrayList = new ArrayList();
        a(arrayList, c.AD_MOVIES, "Movies").a(c.MINTEGRAL_VC, ".Mintegral_VC");
        a a = a(arrayList, c.AD_MINTEGRAL_700, ".mintegral700");
        a.a(c.MINTEGRAL_700_IMG, "img");
        a.a(c.MINTEGRAL_700_RES, "res");
        a.a(c.MINTEGRAL_700_HTML, TJAdUnitConstants.String.HTML);
        a.a(c.MINTEGRAL_700_APK, "apk");
        a(arrayList, c.MINTEGRAL_CRASH_INFO, "crashinfo");
        a(arrayList, c.MINTEGRAL_OTHER, FacebookRequestErrorClassification.KEY_OTHER);
        return arrayList;
    }
}
