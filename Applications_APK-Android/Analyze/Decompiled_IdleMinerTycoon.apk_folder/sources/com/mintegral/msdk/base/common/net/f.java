package com.mintegral.msdk.base.common.net;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.utils.g;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;

/* compiled from: CommonHttpRequest */
public final class f extends com.mintegral.msdk.base.common.f.a {
    private int a;
    private HttpRequestBase b;
    private g c;
    private k d;
    private Object e = new Object();
    private Thread f;
    private boolean g;
    private h k;
    private WeakReference<Context> l;

    f(Context context, HttpRequestBase httpRequestBase, g gVar, int i, int i2, int i3, boolean z) {
        this.l = new WeakReference<>(context);
        this.k = h.a(context.getApplicationContext());
        this.k.a();
        this.b = httpRequestBase;
        this.c = gVar;
        this.a = i;
        this.d = new k(i2, i3);
        this.g = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01b3, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x0217, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x0218, code lost:
        r20 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x021c, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x021d, code lost:
        r20 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007b, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x007c, code lost:
        r6 = r8;
        r20 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0125, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0129, code lost:
        r0 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:79:0x0141 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:96:0x017f */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x019a A[Catch:{ Exception -> 0x01b5, OutOfMemoryError -> 0x01b3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x01b3 A[ExcHandler: OutOfMemoryError (e java.lang.OutOfMemoryError), PHI: r11 
      PHI: (r11v17 java.lang.Object) = (r11v18 java.lang.Object), (r11v20 java.lang.Object), (r11v20 java.lang.Object), (r11v20 java.lang.Object) binds: [B:96:0x017f, B:87:0x015b, B:92:0x016f, B:93:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:87:0x015b] */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x01d9  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0206  */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x020f  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x0217 A[ExcHandler: all (th java.lang.Throwable), PHI: r14 
      PHI: (r14v16 java.lang.String) = (r14v0 java.lang.String), (r14v0 java.lang.String), (r14v17 java.lang.String), (r14v17 java.lang.String), (r14v17 java.lang.String), (r14v0 java.lang.String) binds: [B:14:0x0071, B:15:?, B:45:0x00ce, B:27:0x0092, B:28:?, B:22:0x0084] A[DONT_GENERATE, DONT_INLINE], Splitter:B:14:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x021c A[ExcHandler: OutOfMemoryError (e java.lang.OutOfMemoryError), PHI: r14 
      PHI: (r14v15 java.lang.String) = (r14v0 java.lang.String), (r14v0 java.lang.String), (r14v17 java.lang.String), (r14v17 java.lang.String), (r14v17 java.lang.String), (r14v0 java.lang.String) binds: [B:14:0x0071, B:15:?, B:45:0x00ce, B:27:0x0092, B:28:?, B:22:0x0084] A[DONT_GENERATE, DONT_INLINE], Splitter:B:14:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x025e  */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x0267  */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x027d  */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x0289  */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x02a5 A[SYNTHETIC, Splitter:B:186:0x02a5] */
    /* JADX WARNING: Removed duplicated region for block: B:206:0x0317  */
    /* JADX WARNING: Removed duplicated region for block: B:208:0x031c A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x0320  */
    /* JADX WARNING: Removed duplicated region for block: B:217:0x033c  */
    /* JADX WARNING: Removed duplicated region for block: B:231:0x0362  */
    /* JADX WARNING: Removed duplicated region for block: B:235:0x036b  */
    /* JADX WARNING: Removed duplicated region for block: B:242:0x0388  */
    /* JADX WARNING: Removed duplicated region for block: B:246:0x0396  */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x039a  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0125 A[ExcHandler: OutOfMemoryError (e java.lang.OutOfMemoryError), Splitter:B:57:0x0107] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0195 A[Catch:{ Exception -> 0x01b5, OutOfMemoryError -> 0x01b3 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.mintegral.msdk.base.common.net.f.d e() {
        /*
            r22 = this;
            r1 = r22
            com.mintegral.msdk.base.common.net.f$a r2 = new com.mintegral.msdk.base.common.net.f$a
            r2.<init>()
            long r3 = java.lang.System.nanoTime()
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r1.f = r0
            com.mintegral.msdk.base.common.net.f$d r5 = new com.mintegral.msdk.base.common.net.f$d
            r5.<init>()
            boolean r0 = r22.f()
            r6 = -2
            if (r0 != 0) goto L_0x0020
            r5.a = r6
            return r5
        L_0x0020:
            java.lang.ref.WeakReference<android.content.Context> r0 = r1.l
            java.lang.Object r0 = r0.get()
            if (r0 == 0) goto L_0x002d
            com.mintegral.msdk.base.common.net.g r0 = r1.c
            r0.b()
        L_0x002d:
            java.lang.String r0 = "CommonHttpRequest"
            java.lang.String r7 = "request is started"
            com.mintegral.msdk.base.utils.g.a(r0, r7)
            com.mintegral.msdk.base.common.net.k r0 = r1.d
            r0.a()
            r9 = 0
            r12 = r3
            r10 = 1
            r11 = 0
        L_0x003d:
            if (r10 == 0) goto L_0x0390
            java.lang.String r14 = ""
            com.mintegral.msdk.base.common.net.i r15 = new com.mintegral.msdk.base.common.net.i     // Catch:{ Exception -> 0x0291, OutOfMemoryError -> 0x0243, all -> 0x023c }
            org.apache.http.client.methods.HttpRequestBase r0 = r1.b     // Catch:{ Exception -> 0x0291, OutOfMemoryError -> 0x0243, all -> 0x023c }
            java.net.URI r0 = r0.getURI()     // Catch:{ Exception -> 0x0291, OutOfMemoryError -> 0x0243, all -> 0x023c }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0291, OutOfMemoryError -> 0x0243, all -> 0x023c }
            int r7 = r1.a     // Catch:{ Exception -> 0x0291, OutOfMemoryError -> 0x0243, all -> 0x023c }
            com.mintegral.msdk.base.common.net.h r8 = r1.k     // Catch:{ Exception -> 0x0291, OutOfMemoryError -> 0x0243, all -> 0x023c }
            com.mintegral.msdk.base.common.net.j r8 = r8.c()     // Catch:{ Exception -> 0x0291, OutOfMemoryError -> 0x0243, all -> 0x023c }
            r15.<init>(r0, r7, r8)     // Catch:{ Exception -> 0x0291, OutOfMemoryError -> 0x0243, all -> 0x023c }
            org.apache.http.client.methods.HttpRequestBase r0 = r1.b     // Catch:{ Exception -> 0x0235, OutOfMemoryError -> 0x022f, all -> 0x0229 }
            java.net.URI r0 = r0.getURI()     // Catch:{ Exception -> 0x0235, OutOfMemoryError -> 0x022f, all -> 0x0229 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0235, OutOfMemoryError -> 0x022f, all -> 0x0229 }
            r2.a = r0     // Catch:{ Exception -> 0x0235, OutOfMemoryError -> 0x022f, all -> 0x0229 }
            com.mintegral.msdk.base.common.net.f$b r0 = new com.mintegral.msdk.base.common.net.f$b     // Catch:{ Exception -> 0x0235, OutOfMemoryError -> 0x022f, all -> 0x0229 }
            r0.<init>(r1, r9)     // Catch:{ Exception -> 0x0235, OutOfMemoryError -> 0x022f, all -> 0x0229 }
            r15.addRequestInterceptor(r0)     // Catch:{ Exception -> 0x0235, OutOfMemoryError -> 0x022f, all -> 0x0229 }
            com.mintegral.msdk.base.common.net.f$c r8 = new com.mintegral.msdk.base.common.net.f$c     // Catch:{ Exception -> 0x0235, OutOfMemoryError -> 0x022f, all -> 0x0229 }
            r8.<init>(r1, r9)     // Catch:{ Exception -> 0x0235, OutOfMemoryError -> 0x022f, all -> 0x0229 }
            r15.addResponseInterceptor(r8)     // Catch:{ Exception -> 0x0221, OutOfMemoryError -> 0x021c, all -> 0x0217 }
            org.apache.http.client.methods.HttpRequestBase r0 = r1.b     // Catch:{ Throwable -> 0x0083 }
            org.apache.http.HttpResponse r0 = r15.execute(r0)     // Catch:{ Throwable -> 0x0083 }
            goto L_0x008a
        L_0x007b:
            r0 = move-exception
            r6 = r8
            r20 = r10
        L_0x007f:
            r8 = r15
            r7 = 0
            goto L_0x0297
        L_0x0083:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()     // Catch:{ Exception -> 0x0221, OutOfMemoryError -> 0x021c, all -> 0x0217 }
            r14 = r0
            r0 = 0
        L_0x008a:
            if (r0 == 0) goto L_0x01c6
            boolean r7 = r22.f()     // Catch:{ Exception -> 0x0221, OutOfMemoryError -> 0x021c, all -> 0x0217 }
            if (r7 != 0) goto L_0x00ce
            r5.a = r6     // Catch:{ Exception -> 0x007b, OutOfMemoryError -> 0x021c, all -> 0x0217 }
            com.mintegral.msdk.base.common.net.f$c$a r0 = r8.a()
            if (r0 == 0) goto L_0x00a1
            com.mintegral.msdk.base.common.net.f$c$a r0 = r8.a()
            r0.a()
        L_0x00a1:
            r15.a()
            if (r10 == 0) goto L_0x00a8
            if (r11 != 0) goto L_0x0390
        L_0x00a8:
            if (r10 != 0) goto L_0x00be
            boolean r0 = r22.f()
            if (r0 == 0) goto L_0x00be
            java.lang.ref.WeakReference<android.content.Context> r0 = r1.l
            java.lang.Object r0 = r0.get()
            if (r0 == 0) goto L_0x00be
            com.mintegral.msdk.base.common.net.g r0 = r1.c
            r0.c(r14)
            goto L_0x00cd
        L_0x00be:
            java.lang.ref.WeakReference<android.content.Context> r0 = r1.l
            java.lang.Object r0 = r0.get()
            if (r0 != 0) goto L_0x00cd
            com.mintegral.msdk.base.common.net.g r0 = r1.c
            java.lang.String r2 = "http content is null"
            r0.c(r2)
        L_0x00cd:
            return r5
        L_0x00ce:
            org.apache.http.StatusLine r7 = r0.getStatusLine()     // Catch:{ Exception -> 0x0221, OutOfMemoryError -> 0x021c, all -> 0x0217 }
            int r7 = r7.getStatusCode()     // Catch:{ Exception -> 0x0221, OutOfMemoryError -> 0x021c, all -> 0x0217 }
            long r16 = java.lang.System.nanoTime()     // Catch:{ Exception -> 0x0221, OutOfMemoryError -> 0x021c, all -> 0x0217 }
            r18 = 0
            long r16 = r16 - r12
            r18 = 1000000(0xf4240, double:4.940656E-318)
            r20 = r10
            long r9 = r16 / r18
            r2.b = r9     // Catch:{ Exception -> 0x01c4, OutOfMemoryError -> 0x01c2 }
            long r9 = java.lang.System.nanoTime()     // Catch:{ Exception -> 0x01c4, OutOfMemoryError -> 0x01c2 }
            java.lang.String r12 = "CommonHttpRequest"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01bc, OutOfMemoryError -> 0x01b7 }
            java.lang.String r6 = "request status code "
            r13.<init>(r6)     // Catch:{ Exception -> 0x01bc, OutOfMemoryError -> 0x01b7 }
            r13.append(r7)     // Catch:{ Exception -> 0x01bc, OutOfMemoryError -> 0x01b7 }
            java.lang.String r6 = r13.toString()     // Catch:{ Exception -> 0x01bc, OutOfMemoryError -> 0x01b7 }
            com.mintegral.msdk.base.utils.g.a(r12, r6)     // Catch:{ Exception -> 0x01bc, OutOfMemoryError -> 0x01b7 }
            r6 = 200(0xc8, float:2.8E-43)
            if (r7 == r6) goto L_0x012e
            r6 = 206(0xce, float:2.89E-43)
            if (r7 != r6) goto L_0x0107
            goto L_0x012e
        L_0x0107:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0129, OutOfMemoryError -> 0x0125 }
            java.lang.String r6 = "http code error:"
            r0.<init>(r6)     // Catch:{ Exception -> 0x0129, OutOfMemoryError -> 0x0125 }
            r0.append(r7)     // Catch:{ Exception -> 0x0129, OutOfMemoryError -> 0x0125 }
            java.lang.String r6 = r0.toString()     // Catch:{ Exception -> 0x0129, OutOfMemoryError -> 0x0125 }
            java.net.ConnectException r0 = new java.net.ConnectException     // Catch:{ Exception -> 0x0122, OutOfMemoryError -> 0x011f, all -> 0x011b }
            r0.<init>()     // Catch:{ Exception -> 0x0122, OutOfMemoryError -> 0x011f, all -> 0x011b }
            throw r0     // Catch:{ Exception -> 0x0122, OutOfMemoryError -> 0x011f, all -> 0x011b }
        L_0x011b:
            r0 = move-exception
            r14 = r6
            goto L_0x0351
        L_0x011f:
            r0 = move-exception
            r14 = r6
            goto L_0x0126
        L_0x0122:
            r0 = move-exception
            r14 = r6
            goto L_0x012a
        L_0x0125:
            r0 = move-exception
        L_0x0126:
            r12 = r9
            goto L_0x021f
        L_0x0129:
            r0 = move-exception
        L_0x012a:
            r6 = r8
            r12 = r9
            goto L_0x007f
        L_0x012e:
            org.apache.http.Header[] r6 = r0.getAllHeaders()     // Catch:{ Exception -> 0x01bc, OutOfMemoryError -> 0x01b7 }
            org.apache.http.HttpEntity r7 = r0.getEntity()     // Catch:{ Exception -> 0x01bc, OutOfMemoryError -> 0x01b7 }
            if (r7 == 0) goto L_0x017d
            r5.b = r6     // Catch:{ Exception -> 0x01bc, OutOfMemoryError -> 0x01b7 }
            com.mintegral.msdk.base.common.net.g r12 = r1.c     // Catch:{ Exception -> 0x0141, OutOfMemoryError -> 0x0125 }
            java.lang.Object r12 = r12.b(r7)     // Catch:{ Exception -> 0x0141, OutOfMemoryError -> 0x0125 }
            r11 = r12
        L_0x0141:
            long r12 = r7.getContentLength()     // Catch:{ Exception -> 0x01bc, OutOfMemoryError -> 0x01b7 }
            r2.c = r12     // Catch:{ Exception -> 0x01bc, OutOfMemoryError -> 0x01b7 }
            long r12 = r2.c     // Catch:{ Exception -> 0x01bc, OutOfMemoryError -> 0x01b7 }
            r16 = 0
            int r7 = (r12 > r16 ? 1 : (r12 == r16 ? 0 : -1))
            if (r7 >= 0) goto L_0x017b
            java.lang.String r7 = "Content-Length"
            org.apache.http.Header[] r0 = r0.getHeaders(r7)     // Catch:{ Exception -> 0x01bc, OutOfMemoryError -> 0x01b7 }
            if (r0 == 0) goto L_0x017b
            int r7 = r0.length     // Catch:{ Exception -> 0x01bc, OutOfMemoryError -> 0x01b7 }
            if (r7 <= 0) goto L_0x017b
            r7 = 0
            r0 = r0[r7]     // Catch:{ Exception -> 0x01b5, OutOfMemoryError -> 0x01b3 }
            java.lang.String r12 = r0.getValue()     // Catch:{ Exception -> 0x01b5, OutOfMemoryError -> 0x01b3 }
            boolean r13 = android.text.TextUtils.isEmpty(r12)     // Catch:{ Exception -> 0x01b5, OutOfMemoryError -> 0x01b3 }
            if (r13 != 0) goto L_0x017f
            java.lang.String r13 = "[0-9]+"
            boolean r12 = r12.matches(r13)     // Catch:{ Exception -> 0x01b5, OutOfMemoryError -> 0x01b3 }
            if (r12 == 0) goto L_0x017f
            java.lang.String r0 = r0.getValue()     // Catch:{ Exception -> 0x017f, OutOfMemoryError -> 0x01b3 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x017f, OutOfMemoryError -> 0x01b3 }
            long r12 = (long) r0     // Catch:{ Exception -> 0x017f, OutOfMemoryError -> 0x01b3 }
            r2.c = r12     // Catch:{ Exception -> 0x017f, OutOfMemoryError -> 0x01b3 }
            goto L_0x017f
        L_0x017b:
            r7 = 0
            goto L_0x017f
        L_0x017d:
            r7 = 0
            r11 = 0
        L_0x017f:
            long r12 = java.lang.System.nanoTime()     // Catch:{ Exception -> 0x01b5, OutOfMemoryError -> 0x01b3 }
            r0 = 0
            long r12 = r12 - r9
            long r12 = r12 / r18
            r2.d = r12     // Catch:{ Exception -> 0x01b5, OutOfMemoryError -> 0x01b3 }
            long r12 = java.lang.System.nanoTime()     // Catch:{ Exception -> 0x01b5, OutOfMemoryError -> 0x01b3 }
            r0 = 0
            long r12 = r12 - r3
            long r12 = r12 / r18
            r2.e = r12     // Catch:{ Exception -> 0x01b5, OutOfMemoryError -> 0x01b3 }
            if (r11 != 0) goto L_0x019a
            java.lang.String r0 = "http contentResult is null"
            r14 = r0
            r12 = r9
            goto L_0x01d2
        L_0x019a:
            java.lang.ref.WeakReference<android.content.Context> r0 = r1.l     // Catch:{ Exception -> 0x01b5, OutOfMemoryError -> 0x01b3 }
            java.lang.Object r0 = r0.get()     // Catch:{ Exception -> 0x01b5, OutOfMemoryError -> 0x01b3 }
            if (r0 == 0) goto L_0x01a8
            com.mintegral.msdk.base.common.net.g r0 = r1.c     // Catch:{ Exception -> 0x01b5, OutOfMemoryError -> 0x01b3 }
            r0.a(r6, r11)     // Catch:{ Exception -> 0x01b5, OutOfMemoryError -> 0x01b3 }
            goto L_0x01af
        L_0x01a8:
            com.mintegral.msdk.base.common.net.g r0 = r1.c     // Catch:{ Exception -> 0x01b5, OutOfMemoryError -> 0x01b3 }
            java.lang.String r6 = "contentResult and context are null"
            r0.c(r6)     // Catch:{ Exception -> 0x01b5, OutOfMemoryError -> 0x01b3 }
        L_0x01af:
            r12 = r9
            r10 = r20
            goto L_0x01d3
        L_0x01b3:
            r0 = move-exception
            goto L_0x01b9
        L_0x01b5:
            r0 = move-exception
            goto L_0x01be
        L_0x01b7:
            r0 = move-exception
            r7 = 0
        L_0x01b9:
            r12 = r9
            goto L_0x0249
        L_0x01bc:
            r0 = move-exception
            r7 = 0
        L_0x01be:
            r6 = r8
            r12 = r9
            goto L_0x0226
        L_0x01c2:
            r0 = move-exception
            goto L_0x021f
        L_0x01c4:
            r0 = move-exception
            goto L_0x0224
        L_0x01c6:
            r20 = r10
            r7 = 0
            boolean r0 = android.text.TextUtils.isEmpty(r14)     // Catch:{ Exception -> 0x0215, OutOfMemoryError -> 0x0213 }
            if (r0 == 0) goto L_0x01d2
            java.lang.String r0 = "http respon is null"
            r14 = r0
        L_0x01d2:
            r10 = 0
        L_0x01d3:
            com.mintegral.msdk.base.common.net.f$c$a r0 = r8.a()
            if (r0 == 0) goto L_0x01e0
            com.mintegral.msdk.base.common.net.f$c$a r0 = r8.a()
            r0.a()
        L_0x01e0:
            r15.a()
            if (r10 == 0) goto L_0x01e7
            if (r11 != 0) goto L_0x0390
        L_0x01e7:
            if (r10 != 0) goto L_0x01fe
            boolean r0 = r22.f()
            if (r0 == 0) goto L_0x01fe
            java.lang.ref.WeakReference<android.content.Context> r0 = r1.l
            java.lang.Object r0 = r0.get()
            if (r0 == 0) goto L_0x01fe
            com.mintegral.msdk.base.common.net.g r0 = r1.c
            r0.c(r14)
            goto L_0x0346
        L_0x01fe:
            java.lang.ref.WeakReference<android.content.Context> r0 = r1.l
            java.lang.Object r0 = r0.get()
            if (r0 != 0) goto L_0x020f
            com.mintegral.msdk.base.common.net.g r0 = r1.c
            java.lang.String r6 = "http content is null"
            r0.c(r6)
            goto L_0x0346
        L_0x020f:
            r21 = r2
            goto L_0x0344
        L_0x0213:
            r0 = move-exception
            goto L_0x0249
        L_0x0215:
            r0 = move-exception
            goto L_0x0225
        L_0x0217:
            r0 = move-exception
            r20 = r10
            goto L_0x0351
        L_0x021c:
            r0 = move-exception
            r20 = r10
        L_0x021f:
            r7 = 0
            goto L_0x0249
        L_0x0221:
            r0 = move-exception
            r20 = r10
        L_0x0224:
            r7 = 0
        L_0x0225:
            r6 = r8
        L_0x0226:
            r8 = r15
            goto L_0x0297
        L_0x0229:
            r0 = move-exception
            r20 = r10
            r8 = 0
            goto L_0x0351
        L_0x022f:
            r0 = move-exception
            r20 = r10
            r7 = 0
            r8 = 0
            goto L_0x0249
        L_0x0235:
            r0 = move-exception
            r20 = r10
            r7 = 0
            r8 = r15
            r6 = 0
            goto L_0x0297
        L_0x023c:
            r0 = move-exception
            r20 = r10
            r8 = 0
            r15 = 0
            goto L_0x0351
        L_0x0243:
            r0 = move-exception
            r20 = r10
            r7 = 0
            r8 = 0
            r15 = 0
        L_0x0249:
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x028e }
            if (r8 == 0) goto L_0x025c
            com.mintegral.msdk.base.common.net.f$c$a r6 = r8.a()
            if (r6 == 0) goto L_0x025c
            com.mintegral.msdk.base.common.net.f$c$a r6 = r8.a()
            r6.a()
        L_0x025c:
            if (r15 == 0) goto L_0x0261
            r15.a()
        L_0x0261:
            boolean r6 = r22.f()
            if (r6 == 0) goto L_0x0275
            java.lang.ref.WeakReference<android.content.Context> r6 = r1.l
            java.lang.Object r6 = r6.get()
            if (r6 == 0) goto L_0x0275
            com.mintegral.msdk.base.common.net.g r6 = r1.c
            r6.c(r0)
            goto L_0x0284
        L_0x0275:
            java.lang.ref.WeakReference<android.content.Context> r0 = r1.l
            java.lang.Object r0 = r0.get()
            if (r0 != 0) goto L_0x0289
            com.mintegral.msdk.base.common.net.g r0 = r1.c
            java.lang.String r6 = "http content is null"
            r0.c(r6)
        L_0x0284:
            r6 = -2
            r9 = 0
            r10 = 0
            goto L_0x003d
        L_0x0289:
            r21 = r2
            r10 = 0
            goto L_0x0344
        L_0x028e:
            r0 = move-exception
            goto L_0x0351
        L_0x0291:
            r0 = move-exception
            r20 = r10
            r7 = 0
            r6 = 0
            r8 = 0
        L_0x0297:
            com.mintegral.msdk.base.common.net.k r9 = r1.d     // Catch:{ all -> 0x034e }
            com.mintegral.msdk.base.common.net.k$a r9 = r9.a(r0)     // Catch:{ all -> 0x034e }
            java.lang.String r10 = r0.getMessage()     // Catch:{ all -> 0x034e }
            boolean r14 = r9.b     // Catch:{ all -> 0x034a }
            if (r14 != 0) goto L_0x02ed
            boolean r15 = r1.g     // Catch:{ all -> 0x02e5 }
            if (r15 == 0) goto L_0x02ed
            org.apache.http.client.methods.HttpRequestBase r15 = r1.b     // Catch:{ all -> 0x02e5 }
            java.net.URI r15 = r15.getURI()     // Catch:{ all -> 0x02e5 }
            java.lang.String r15 = r15.toString()     // Catch:{ all -> 0x02e5 }
            int r15 = com.mintegral.msdk.base.common.net.i.a(r15)     // Catch:{ all -> 0x02e5 }
            int r7 = com.mintegral.msdk.base.common.net.i.a.b     // Catch:{ all -> 0x02e5 }
            if (r15 != r7) goto L_0x02ed
            org.apache.http.client.methods.HttpRequestBase r7 = r1.b     // Catch:{ all -> 0x02e5 }
            java.net.URI r7 = r7.getURI()     // Catch:{ all -> 0x02e5 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x02e5 }
            r15 = 5
            java.lang.String r7 = r7.substring(r15)     // Catch:{ all -> 0x02e5 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ all -> 0x02e5 }
            r21 = r2
            java.lang.String r2 = "http"
            r15.<init>(r2)     // Catch:{ all -> 0x02e5 }
            r15.append(r7)     // Catch:{ all -> 0x02e5 }
            java.lang.String r2 = r15.toString()     // Catch:{ all -> 0x02e5 }
            org.apache.http.client.methods.HttpRequestBase r7 = r1.b     // Catch:{ all -> 0x02e5 }
            java.net.URI r2 = java.net.URI.create(r2)     // Catch:{ all -> 0x02e5 }
            r7.setURI(r2)     // Catch:{ all -> 0x02e5 }
            r14 = 1
            goto L_0x02ef
        L_0x02e5:
            r0 = move-exception
            r15 = r8
            r20 = r14
            r8 = r6
            r14 = r10
            goto L_0x0351
        L_0x02ed:
            r21 = r2
        L_0x02ef:
            if (r14 == 0) goto L_0x0306
            boolean r2 = r22.f()     // Catch:{ all -> 0x02e5 }
            if (r2 == 0) goto L_0x0306
            java.lang.ref.WeakReference<android.content.Context> r2 = r1.l     // Catch:{ all -> 0x02e5 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x02e5 }
            if (r2 == 0) goto L_0x0306
            com.mintegral.msdk.base.common.net.g r2 = r1.c     // Catch:{ all -> 0x02e5 }
            int r7 = r9.c     // Catch:{ all -> 0x02e5 }
            r2.a(r7, r0)     // Catch:{ all -> 0x02e5 }
        L_0x0306:
            if (r6 == 0) goto L_0x0315
            com.mintegral.msdk.base.common.net.f$c$a r0 = r6.a()
            if (r0 == 0) goto L_0x0315
            com.mintegral.msdk.base.common.net.f$c$a r0 = r6.a()
            r0.a()
        L_0x0315:
            if (r8 == 0) goto L_0x031a
            r8.a()
        L_0x031a:
            if (r14 == 0) goto L_0x031e
            if (r11 != 0) goto L_0x0390
        L_0x031e:
            if (r14 != 0) goto L_0x0334
            boolean r0 = r22.f()
            if (r0 == 0) goto L_0x0334
            java.lang.ref.WeakReference<android.content.Context> r0 = r1.l
            java.lang.Object r0 = r0.get()
            if (r0 == 0) goto L_0x0334
            com.mintegral.msdk.base.common.net.g r0 = r1.c
            r0.c(r10)
            goto L_0x0343
        L_0x0334:
            java.lang.ref.WeakReference<android.content.Context> r0 = r1.l
            java.lang.Object r0 = r0.get()
            if (r0 != 0) goto L_0x0343
            com.mintegral.msdk.base.common.net.g r0 = r1.c
            java.lang.String r2 = "http content is null"
            r0.c(r2)
        L_0x0343:
            r10 = r14
        L_0x0344:
            r2 = r21
        L_0x0346:
            r6 = -2
            r9 = 0
            goto L_0x003d
        L_0x034a:
            r0 = move-exception
            r15 = r8
            r14 = r10
            goto L_0x0350
        L_0x034e:
            r0 = move-exception
            r15 = r8
        L_0x0350:
            r8 = r6
        L_0x0351:
            if (r8 == 0) goto L_0x0360
            com.mintegral.msdk.base.common.net.f$c$a r2 = r8.a()
            if (r2 == 0) goto L_0x0360
            com.mintegral.msdk.base.common.net.f$c$a r2 = r8.a()
            r2.a()
        L_0x0360:
            if (r15 == 0) goto L_0x0365
            r15.a()
        L_0x0365:
            if (r20 == 0) goto L_0x0369
            if (r11 != 0) goto L_0x0390
        L_0x0369:
            if (r20 != 0) goto L_0x0380
            boolean r2 = r22.f()
            if (r2 == 0) goto L_0x0380
            java.lang.ref.WeakReference<android.content.Context> r2 = r1.l
            java.lang.Object r2 = r2.get()
            if (r2 != 0) goto L_0x037a
            goto L_0x0380
        L_0x037a:
            com.mintegral.msdk.base.common.net.g r2 = r1.c
            r2.c(r14)
            goto L_0x038f
        L_0x0380:
            java.lang.ref.WeakReference<android.content.Context> r2 = r1.l
            java.lang.Object r2 = r2.get()
            if (r2 != 0) goto L_0x038f
            com.mintegral.msdk.base.common.net.g r2 = r1.c
            java.lang.String r3 = "http content is null"
            r2.c(r3)
        L_0x038f:
            throw r0
        L_0x0390:
            boolean r0 = r22.f()
            if (r0 != 0) goto L_0x039a
            r2 = -2
            r5.a = r2
            return r5
        L_0x039a:
            java.lang.ref.WeakReference<android.content.Context> r0 = r1.l
            java.lang.Object r0 = r0.get()
            if (r0 == 0) goto L_0x03a7
            com.mintegral.msdk.base.common.net.g r0 = r1.c
            r0.c()
        L_0x03a7:
            java.lang.String r0 = "CommonHttpRequest"
            java.lang.String r2 = "request is finished"
            com.mintegral.msdk.base.utils.g.a(r0, r2)
            r5.c = r11
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.common.net.f.e():com.mintegral.msdk.base.common.net.f$d");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:2:0x0003 */
    /* JADX WARNING: Removed duplicated region for block: B:2:0x0003 A[LOOP:0: B:2:0x0003->B:17:0x0003, LOOP_START, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean f() {
        /*
            r3 = this;
            java.lang.Object r0 = r3.e
            monitor-enter(r0)
        L_0x0003:
            int r1 = r3.i     // Catch:{ all -> 0x001a }
            int r2 = com.mintegral.msdk.base.common.f.a.C0050a.c     // Catch:{ all -> 0x001a }
            if (r1 != r2) goto L_0x000f
            java.lang.Object r1 = r3.e     // Catch:{ InterruptedException -> 0x0003 }
            r1.wait()     // Catch:{ InterruptedException -> 0x0003 }
            goto L_0x0003
        L_0x000f:
            monitor-exit(r0)     // Catch:{ all -> 0x001a }
            int r0 = r3.i
            int r1 = com.mintegral.msdk.base.common.f.a.C0050a.b
            if (r0 != r1) goto L_0x0018
            r0 = 1
            return r0
        L_0x0018:
            r0 = 0
            return r0
        L_0x001a:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x001a }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.common.net.f.f():boolean");
    }

    /* compiled from: CommonHttpRequest */
    private class b implements HttpRequestInterceptor {
        private b() {
        }

        /* synthetic */ b(f fVar, byte b) {
            this();
        }

        public final void process(HttpRequest httpRequest, HttpContext httpContext) {
            if (!httpRequest.containsHeader(io.fabric.sdk.android.services.network.HttpRequest.HEADER_ACCEPT_ENCODING)) {
                httpRequest.addHeader(io.fabric.sdk.android.services.network.HttpRequest.HEADER_ACCEPT_ENCODING, io.fabric.sdk.android.services.network.HttpRequest.ENCODING_GZIP);
            }
        }
    }

    /* compiled from: CommonHttpRequest */
    private class c implements HttpResponseInterceptor {
        private a b;

        private c() {
            this.b = null;
        }

        /* synthetic */ c(f fVar, byte b2) {
            this();
        }

        public final void process(HttpResponse httpResponse, HttpContext httpContext) {
            Header contentEncoding;
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null && (contentEncoding = entity.getContentEncoding()) != null) {
                for (HeaderElement name : contentEncoding.getElements()) {
                    if (name.getName().equalsIgnoreCase(io.fabric.sdk.android.services.network.HttpRequest.ENCODING_GZIP)) {
                        if (this.b == null) {
                            this.b = new a(entity);
                        }
                        httpResponse.setEntity(this.b);
                        return;
                    }
                }
            }
        }

        public final a a() {
            return this.b;
        }

        /* compiled from: CommonHttpRequest */
        private class a extends HttpEntityWrapper {
            private GZIPInputStream b = null;

            public final long getContentLength() {
                return -1;
            }

            public a(HttpEntity httpEntity) {
                super(httpEntity);
            }

            public final InputStream getContent() throws IOException {
                if (this.b == null) {
                    this.b = new GZIPInputStream(this.wrappedEntity.getContent());
                }
                return this.b;
            }

            public final void a() {
                try {
                    this.b.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /* compiled from: CommonHttpRequest */
    static class d {
        int a;
        Header[] b;
        Object c;

        d() {
        }
    }

    public final void a() {
        try {
            e();
        } catch (Exception e2) {
            g.c("CommonHttpRequest", "unknow exception", e2);
        }
    }

    public final void b() {
        g.a("CommonHttpRequest", "request is canceled");
        if (this.l.get() != null) {
            this.c.d();
        }
        if (this.f != null) {
            this.f.interrupt();
        }
        synchronized (this.e) {
            this.e.notifyAll();
        }
    }

    /* compiled from: CommonHttpRequest */
    public static class a {
        private static final SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());
        String a = "";
        long b = 0;
        long c = 0;
        long d = 0;
        long e = 0;

        public final String toString() {
            try {
                if (!TextUtils.isEmpty(this.a)) {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("url", this.a);
                    jSONObject.put("first_data_cost", this.b);
                    jSONObject.put("total_data", this.c);
                    jSONObject.put("read_cost", this.d);
                    jSONObject.put("total_cost", this.e);
                    jSONObject.put("record_time", f.format(Calendar.getInstance().getTime()));
                    return jSONObject.toString();
                }
            } catch (Exception unused) {
            }
            return super.toString();
        }
    }
}
