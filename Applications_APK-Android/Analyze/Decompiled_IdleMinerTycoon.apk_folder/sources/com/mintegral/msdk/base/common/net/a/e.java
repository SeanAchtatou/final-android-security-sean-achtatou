package com.mintegral.msdk.base.common.net.a;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.helpshift.support.constants.FaqsColumns;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.controller.authoritycontroller.a;
import com.mintegral.msdk.base.utils.b;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.i;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.mtgbid.common.BidResponsedEx;
import com.tapjoy.TapjoyConstants;
import org.json.JSONObject;

/* compiled from: CommonRequestParamsForAdd */
public final class e {
    private static String a = "";
    private static String b = "";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.base.common.net.a.e.a(com.mintegral.msdk.base.common.net.l, boolean):void
     arg types: [com.mintegral.msdk.base.common.net.l, int]
     candidates:
      com.mintegral.msdk.base.common.net.a.e.a(com.mintegral.msdk.base.common.net.l, android.content.Context):void
      com.mintegral.msdk.base.common.net.a.e.a(com.mintegral.msdk.base.common.net.l, boolean):void */
    public static void a(l lVar, Context context) {
        a.a();
        if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            StringBuilder sb = new StringBuilder();
            sb.append(i.c());
            lVar.a("cache1", sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append(i.a());
            lVar.a("cache2", sb2.toString());
            StringBuilder sb3 = new StringBuilder();
            sb3.append(b.a());
            lVar.a("power_rate", sb3.toString());
            StringBuilder sb4 = new StringBuilder();
            sb4.append(b.b());
            lVar.a("charging", sb4.toString());
            StringBuilder sb5 = new StringBuilder();
            sb5.append(k.a(context, "com.tencent.mm"));
            lVar.a("has_wx", sb5.toString());
        }
        lVar.a("pkg_source", c.a(c.q(context), context));
        if (Build.VERSION.SDK_INT > 18) {
            lVar.a("http_req", "2");
        }
        a(lVar, true);
        e(lVar);
        c(lVar);
    }

    public static void a(l lVar) {
        lVar.a("api_version", "1.8");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.base.common.net.a.e.a(com.mintegral.msdk.base.common.net.l, boolean):void
     arg types: [com.mintegral.msdk.base.common.net.l, int]
     candidates:
      com.mintegral.msdk.base.common.net.a.e.a(com.mintegral.msdk.base.common.net.l, android.content.Context):void
      com.mintegral.msdk.base.common.net.a.e.a(com.mintegral.msdk.base.common.net.l, boolean):void */
    public static void b(l lVar) {
        a(lVar, false);
        e(lVar);
        c(lVar);
    }

    public static void c(l lVar) {
        try {
            if (TextUtils.isEmpty(com.mintegral.msdk.base.common.a.D)) {
                com.mintegral.msdk.base.common.a.D = com.mintegral.msdk.base.a.a.a.a().a("sys_id");
            }
            if (!TextUtils.isEmpty(com.mintegral.msdk.base.common.a.D)) {
                lVar.a("sys_id", com.mintegral.msdk.base.common.a.D);
            }
            if (TextUtils.isEmpty(com.mintegral.msdk.base.common.a.E)) {
                com.mintegral.msdk.base.common.a.E = com.mintegral.msdk.base.a.a.a.a().a("bkup_id");
            }
            if (!TextUtils.isEmpty(com.mintegral.msdk.base.common.a.E)) {
                lVar.a("bkup_id", com.mintegral.msdk.base.common.a.E);
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    private static void e(l lVar) {
        int g = c.g();
        if (g != -1) {
            lVar.a("unknown_source", String.valueOf(g));
        }
    }

    private static void a(l lVar, boolean z) {
        Context h = com.mintegral.msdk.base.controller.a.d().h();
        com.mintegral.msdk.d.b.a();
        com.mintegral.msdk.d.a b2 = com.mintegral.msdk.d.b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b2 != null) {
            JSONObject jSONObject = new JSONObject();
            try {
                if (b2.as() == 1) {
                    if (c.c(h) != null) {
                        a.a();
                        if (a.a(MIntegralConstans.AUTHORITYIMEIMAC)) {
                            jSONObject.put("imei", c.c(h));
                        }
                    }
                    if (c.j(h) != null) {
                        a.a();
                        if (a.a(MIntegralConstans.AUTHORITYIMEIMAC)) {
                            jSONObject.put("mac", c.j(h));
                        }
                    }
                    if (c.d(h) != null && z) {
                        a.a();
                        if (a.a(MIntegralConstans.AUTHORITYIMEIMAC)) {
                            jSONObject.put("imsi", c.d(h));
                        }
                    }
                }
                if (b2.o() == 1 && z && c.b(h) != null) {
                    a.a();
                    if (a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
                        jSONObject.put("oaid", c.b(h));
                    }
                }
                if (b2.au() == 1 && c.f(h) != null) {
                    a.a();
                    if (a.a(MIntegralConstans.AUTHORITY_ANDROID_ID)) {
                        jSONObject.put(TapjoyConstants.TJC_ANDROID_ID, c.f(h));
                    }
                }
                if (b2.aN() == 1) {
                    a.a();
                    a.a(MIntegralConstans.AUTHORITY_GPS);
                }
                a.a();
                if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(c.n());
                    jSONObject.put(BidResponsedEx.KEY_CID, sb.toString());
                }
                if (!TextUtils.isEmpty(jSONObject.toString())) {
                    if (!jSONObject.equals(a)) {
                        b = com.mintegral.msdk.base.utils.a.b(jSONObject.toString());
                    }
                    if (!TextUtils.isEmpty(b)) {
                        lVar.a("dvi", b);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void d(l lVar) {
        if (lVar != null) {
            a.a();
            if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                lVar.a("model");
                lVar.a("brand");
                lVar.a("screen_size");
                lVar.a("cache1");
                lVar.a("cache2");
                lVar.a("power_rate");
                lVar.a("charging");
                lVar.a("sub_ip");
                lVar.a("network_type");
                lVar.a("useragent");
                lVar.a("ua");
                lVar.a(FaqsColumns.LANGUAGE);
                lVar.a(TapjoyConstants.TJC_DEVICE_TIMEZONE);
                lVar.a("network_str");
                lVar.a(Constants.RequestParameters.NETWORK_MNC);
                lVar.a(Constants.RequestParameters.NETWORK_MCC);
                lVar.a(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME);
                lVar.a("gp_version");
            }
            a.a();
            if (!a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
                lVar.a("gaid");
                lVar.a("oaid");
            }
        }
    }
}
