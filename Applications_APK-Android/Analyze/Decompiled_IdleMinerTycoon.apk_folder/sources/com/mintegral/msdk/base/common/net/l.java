package com.mintegral.msdk.base.common.net;

import android.text.TextUtils;
import com.mintegral.msdk.base.utils.g;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

/* compiled from: CommonRequestParams */
public final class l {
    private Map<String, String> a = new LinkedHashMap();
    private Map<String, b> b = new LinkedHashMap();
    private Map<String, a> c = new LinkedHashMap();
    private String d = "UTF-8";

    /* compiled from: CommonRequestParams */
    private static class a {
        File a;
        String b;
        String c;
    }

    /* compiled from: CommonRequestParams */
    private static class b {
        InputStream a;
        String b;
        String c;
    }

    public final void a(String str, String str2) {
        if (str2 == null) {
            g.d("RequestParams", "不能向接口传递null");
        }
        if (!TextUtils.isEmpty(str) && str2 != null) {
            this.a.put(str, str2);
        }
    }

    public final void a(String str) {
        this.a.remove(str);
        this.b.remove(str);
        this.c.remove(str);
    }

    public final String a() {
        return this.d;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(28);
        for (Map.Entry next : this.a.entrySet()) {
            if (sb.length() > 0) {
                sb.append('&');
            }
            sb.append((String) next.getKey());
            sb.append('=');
            sb.append((String) next.getValue());
        }
        for (Map.Entry next2 : this.b.entrySet()) {
            if (sb.length() > 0) {
                sb.append('&');
            }
            sb.append((String) next2.getKey());
            sb.append('=');
            sb.append("STREAM");
            if (!TextUtils.isEmpty(((b) next2.getValue()).b)) {
                sb.append("_NAME_" + ((b) next2.getValue()).b);
            }
        }
        for (Map.Entry next3 : this.c.entrySet()) {
            if (sb.length() > 0) {
                sb.append('&');
            }
            sb.append((String) next3.getKey());
            sb.append('=');
            sb.append("FILE_NAME_" + ((a) next3.getValue()).a.getName());
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public final HttpEntity a(g gVar) throws IOException {
        if (this.b.isEmpty() && this.c.isEmpty()) {
            return c();
        }
        e eVar = new e(gVar);
        for (Map.Entry next : this.a.entrySet()) {
            eVar.a((String) next.getKey(), (String) next.getValue());
        }
        for (Map.Entry next2 : this.b.entrySet()) {
            b bVar = (b) next2.getValue();
            if (bVar.a != null) {
                eVar.a((String) next2.getKey(), bVar.b, bVar.a, bVar.c);
            }
        }
        for (Map.Entry next3 : this.c.entrySet()) {
            a aVar = (a) next3.getValue();
            eVar.a((String) next3.getKey(), aVar.a, aVar.b, aVar.c);
        }
        return eVar;
    }

    public final String b() {
        try {
            return URLEncodedUtils.format(d(), this.d).replace("+", "%20");
        } catch (Throwable unused) {
            g.d("RequestParams", "URLEncodedUtils failed");
            return "";
        }
    }

    private HttpEntity c() {
        try {
            return new UrlEncodedFormEntity(d(), this.d);
        } catch (UnsupportedEncodingException e) {
            g.b("RequestParams", "createFormEntity failed", e);
            return null;
        }
    }

    private List<BasicNameValuePair> d() {
        LinkedList linkedList = new LinkedList();
        for (Map.Entry next : this.a.entrySet()) {
            linkedList.add(new BasicNameValuePair((String) next.getKey(), (String) next.getValue()));
        }
        return linkedList;
    }
}
