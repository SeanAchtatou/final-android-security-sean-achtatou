package com.mintegral.msdk.base.entity;

import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.controller.authoritycontroller.a;
import com.mintegral.msdk.base.utils.g;
import java.net.URLEncoder;

/* compiled from: DevDataEntity */
public final class d {
    public static String a(int i, String str) {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            a.a();
            if (!a.a(MIntegralConstans.AUTHORITY_APPLIST)) {
                return null;
            }
            stringBuffer.append("app_num=" + i);
            if (str == null) {
                str = "";
            }
            stringBuffer.append("&install_id=" + str);
            String b = com.mintegral.msdk.base.utils.a.b(stringBuffer.toString());
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("key=2000032");
            stringBuffer2.append("&content=" + URLEncoder.encode(b, "utf-8"));
            String stringBuffer3 = stringBuffer2.toString();
            g.d("DevDataEntity", "devDataStr:" + stringBuffer3);
            return stringBuffer3;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
