package com.mintegral.msdk.base.entity;

import android.net.Uri;
import android.text.TextUtils;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.click.CommonJumpLoader;
import com.mintegral.msdk.out.Campaign;
import com.mintegral.msdk.system.NoProGuard;
import com.vungle.warren.model.AdvertisementDBAdapter;
import java.io.Serializable;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CampaignEx extends Campaign implements NoProGuard, Serializable {
    public static final int CAMPAIN_NV_T2_VALUE_3 = 3;
    public static final int CAMPAIN_NV_T2_VALUE_4 = 4;
    public static final String CLICKMODE_ON = "5";
    public static final int CLICK_TIMEOUT_INTERVAL_DEFAULT_VALUE = 2;
    public static final int C_UA_DEFAULT_VALUE = 1;
    public static final String ENDCARD_URL = "endcard_url";
    public static final int FLAG_DEFAULT_SPARE_OFFER = -1;
    public static final int FLAG_IS_SPARE_OFFER = 1;
    public static final int FLAG_NOT_SPARE_OFFER = 0;
    public static final int IMP_UA_DEFAULT_VALUE = 1;
    public static final String JSON_AD_IMP_KEY = "sec";
    public static final String JSON_AD_IMP_VALUE = "url";
    public static final String JSON_KEY_ADVIMP = "adv_imp";
    public static final String JSON_KEY_ADV_ID = "adv_id";
    public static final String JSON_KEY_AD_AKS = "aks";
    public static final String JSON_KEY_AD_AL = "al";
    public static final String JSON_KEY_AD_K = "k";
    public static final String JSON_KEY_AD_MP = "mp";
    public static final String JSON_KEY_AD_Q = "q";
    public static final String JSON_KEY_AD_R = "r";
    public static final String JSON_KEY_AD_SOURCE_ID = "ad_source_id";
    public static final String JSON_KEY_AD_TRACKING_APK_END = "apk_download_end";
    public static final String JSON_KEY_AD_TRACKING_APK_INSTALL = "apk_install";
    public static final String JSON_KEY_AD_TRACKING_APK_START = "apk_download_start";
    public static final String JSON_KEY_AD_TRACKING_DROPOUT_TRACK = "dropout_track";
    public static final String JSON_KEY_AD_TRACKING_IMPRESSION_T2 = "impression_t2";
    public static final String JSON_KEY_AD_TRACKING_PLYCMPT_TRACK = "plycmpt_track";
    public static final String JSON_KEY_AD_URL_LIST = "ad_url_list";
    public static final String JSON_KEY_APP_SIZE = "app_size";
    public static final String JSON_KEY_BANNER_HTML = "banner_html";
    public static final String JSON_KEY_BANNER_URL = "banner_url";
    public static final String JSON_KEY_BTY = "ctype";
    public static final String JSON_KEY_CAMPAIGN_UNITID = "unitId";
    public static final String JSON_KEY_CLICK_INTERVAL = "c_ct";
    public static final String JSON_KEY_CLICK_MODE = "click_mode";
    public static final String JSON_KEY_CLICK_TIMEOUT_INTERVAL = "c_toi";
    public static final String JSON_KEY_CLICK_URL = "click_url";
    public static final String JSON_KEY_CREATIVE_ID = "creative_id";
    public static final String JSON_KEY_CTA_TEXT = "ctatext";
    public static final String JSON_KEY_C_UA = "c_ua";
    public static final String JSON_KEY_DEEP_LINK_URL = "deep_link";
    public static final String JSON_KEY_DESC = "desc";
    public static final String JSON_KEY_ENDCARD_CLICK = "endcard_click_result";
    public static final String JSON_KEY_FCA = "fca";
    public static final String JSON_KEY_FCB = "fcb";
    public static final String JSON_KEY_GIF_URL = "gif_url";
    public static final String JSON_KEY_GUIDELINES = "guidelines";
    public static final String JSON_KEY_HASMTGTPLMARK = "hasMtgTplMark";
    public static final String JSON_KEY_ICON_URL = "icon_url";
    public static final String JSON_KEY_ID = "id";
    public static final String JSON_KEY_IMAGE_SIZE = "image_size";
    public static final String JSON_KEY_IMAGE_URL = "image_url";
    public static final String JSON_KEY_IMPRESSION_URL = "impression_url";
    public static final String JSON_KEY_IMP_UA = "imp_ua";
    public static final String JSON_KEY_JM_PD = "jm_pd";
    public static final String JSON_KEY_LANDING_TYPE = "landing_type";
    public static final String JSON_KEY_LINK_TYPE = "link_type";
    public static final String JSON_KEY_MRAID = "mraid";
    public static final String JSON_KEY_NOTICE_URL = "notice_url";
    public static final String JSON_KEY_NV_T2 = "nv_t2";
    public static final String JSON_KEY_OFFER_TYPE = "offer_type";
    public static final String JSON_KEY_PACKAGE_NAME = "package_name";
    public static final String JSON_KEY_PLCT = "plct";
    public static final String JSON_KEY_PLCTB = "plctb";
    public static final String JSON_KEY_PRE_CLICK = "ttc";
    public static final String JSON_KEY_PRE_CLICK_ERROR_INTERVAL = "ttc_pe";
    public static final String JSON_KEY_PRE_CLICK_INTERVAL = "ttc_ct";
    public static final String JSON_KEY_PRE_CLICK_OTHER_INTERVAL = "ttc_po";
    public static final String JSON_KEY_PUB_IMP = "pub_imp";
    public static final String JSON_KEY_RETARGET_OFFER = "retarget_offer";
    public static final String JSON_KEY_REWARD_AMOUNT = "reward_amount";
    public static final String JSON_KEY_REWARD_NAME = "reward_name";
    public static final String JSON_KEY_REWARD_TEMPLATE = "rv";
    public static final String JSON_KEY_REWARD_VIDEO_MD5 = "md5_file";
    public static final String JSON_KEY_STAR = "rating";
    public static final String JSON_KEY_ST_IEX = "iex";
    public static final String JSON_KEY_ST_TS = "ts";
    public static final String JSON_KEY_TEMPLATE = "template";
    public static final String JSON_KEY_TITLE = "title";
    public static final String JSON_KEY_TTC_CT2 = "ttc_ct2";
    public static final String JSON_KEY_TTC_TYPE = "ttc_type";
    public static final String JSON_KEY_T_IMP = "t_imp";
    public static final String JSON_KEY_VIDEO_LENGTHL = "video_length";
    public static final String JSON_KEY_VIDEO_RESOLUTION = "video_resolution";
    public static final String JSON_KEY_VIDEO_SIZE = "video_size";
    public static final String JSON_KEY_VIDEO_URL = "video_url";
    public static final String JSON_KEY_WATCH_MILE = "watch_mile";
    public static final String JSON_KEY_WITHOUT_INSTALL_CHECK = "wtick";
    public static final String JSON_NATIVE_VIDEO_AD_TRACKING = "ad_tracking";
    public static final String JSON_NATIVE_VIDEO_CLICK = "click";
    public static final String JSON_NATIVE_VIDEO_CLOSE = "close";
    public static final String JSON_NATIVE_VIDEO_COMPLETE = "complete";
    public static final String JSON_NATIVE_VIDEO_ENDCARD = "endcard";
    public static final String JSON_NATIVE_VIDEO_ENDCARD_SHOW = "endcard_show";
    public static final String JSON_NATIVE_VIDEO_ERROR = "error";
    public static final String JSON_NATIVE_VIDEO_FIRST_QUARTILE = "first_quartile";
    public static final String JSON_NATIVE_VIDEO_MIDPOINT = "midpoint";
    public static final String JSON_NATIVE_VIDEO_MUTE = "mute";
    public static final String JSON_NATIVE_VIDEO_PAUSE = "pause";
    public static final String JSON_NATIVE_VIDEO_PLAY_PERCENTAGE = "play_percentage";
    public static final String JSON_NATIVE_VIDEO_RESUME = "resume";
    public static final String JSON_NATIVE_VIDEO_START = "start";
    public static final String JSON_NATIVE_VIDEO_THIRD_QUARTILE = "third_quartile";
    public static final String JSON_NATIVE_VIDEO_UNMUTE = "unmute";
    public static final String JSON_NATIVE_VIDEO_VIDEO_CLICK = "video_click";
    public static final String JSON_NATIVE_VIDOE_IMPRESSION = "impression";
    public static final String KEY_ADCHOICE = "adchoice";
    public static final String KEY_AD_TYPE = "ad_type";
    public static final String KEY_BIND_ID = "bind_id";
    public static final String KEY_GH_ID = "gh_id";
    public static final String KEY_GH_PATH = "gh_path";
    public static final String KEY_IA_CACHE = "ia_cache";
    public static final String KEY_IA_EXT1 = "ia_ext1";
    public static final String KEY_IA_EXT2 = "ia_ext2";
    public static final String KEY_IA_ICON = "ia_icon";
    public static final String KEY_IA_ORI = "ia_ori";
    public static final String KEY_IA_RST = "ia_rst";
    public static final String KEY_IA_URL = "ia_url";
    public static final String KEY_IS_DOWNLOAD = "is_download_zip";
    public static final String KEY_OC_TIME = "oc_time";
    public static final String KEY_OC_TYPE = "oc_type";
    public static final String KEY_OMID = "omid";
    public static final String KEY_T_LIST = "t_list";
    public static final int LANDING_TYPE_VALUE_OPEN_BROWSER = 1;
    public static final int LANDING_TYPE_VALUE_OPEN_GP_BY_PACKAGE = 3;
    public static final int LANDING_TYPE_VALUE_OPEN_WEBVIEW = 2;
    public static final int LINK_TYPE_1 = 1;
    public static final int LINK_TYPE_2 = 2;
    public static final int LINK_TYPE_3 = 3;
    public static final int LINK_TYPE_4 = 4;
    public static final int LINK_TYPE_8 = 8;
    public static final int LINK_TYPE_9 = 9;
    public static final String LOOPBACK = "loopback";
    public static final String LOOPBACK_DOMAIN = "domain";
    public static final String LOOPBACK_KEY = "key";
    public static final String LOOPBACK_VALUE = "value";
    public static final String PLAYABLE_ADS_WITHOUT_VIDEO = "playable_ads_without_video";
    public static final int PLAYABLE_ADS_WITHOUT_VIDEO_DEFAULT = 1;
    public static final int PLAYABLE_ADS_WITHOUT_VIDEO_ENDCARD = 2;
    public static final int RETAR_GETING_IS = 1;
    public static final int RETAR_GETING_NOT = 2;
    public static final String ROVER_KEY_IS_POST = "isPost";
    public static final String ROVER_KEY_MARK = "mark";
    public static final String TAG = "CampaignEx";
    public static final int TTC_CT2_DEFAULT_VALUE = 1800;
    public static final int TTC_CT_DEFAULT_VALUE = 604800;
    public static final String VIDEO_END_TYPE = "video_end_type";
    public static final int VIDEO_END_TYPE_BROWSER = 5;
    public static final int VIDEO_END_TYPE_DEFAULT = 2;
    public static final int VIDEO_END_TYPE_FINISH = 1;
    public static final int VIDEO_END_TYPE_REULSE = 2;
    public static final int VIDEO_END_TYPE_VAST = 3;
    public static final int VIDEO_END_TYPE_WEBVIEW = 4;
    private static final long serialVersionUID = 1;
    private int adType;
    private String ad_url_list;
    private a adchoice;
    private String advId;
    private String advImp;
    private HashMap<String, String> aks;
    private String al;
    private String bannerHtml = "";
    private String bannerUrl = "";
    private String bidToken = "";
    private String bindId;
    private int bty;
    private int cUA = 1;
    private int cacheLevel;
    private String campaignUnitId;
    private int clickInterval;
    private int clickTimeOutInterval = 2;
    private String clickURL = "";
    private String click_mode;
    private long creativeId = 0;
    private String deepLinkUrl = "";
    private String endScreenUrl;
    private int endcard_click_result;
    private String endcard_url;
    private int fca;
    private int fcb;
    private String ghId;
    private String ghPath;
    private String gifUrl;
    private String guidelines;
    private boolean hasMtgTplMark;
    private boolean hasReportAdTrackPause = false;
    private String htmlUrl;
    private String ia_ext1;
    private String ia_ext2;
    private int iex;
    private String imageSize = "";
    private int impUA = 1;
    private String impressionURL = "";
    private String interactiveCache;
    private int isAddSuccesful;
    private boolean isBidCampaign;
    private int isClick;
    private int isDeleted;
    private int isDownLoadZip;
    private boolean isMraid;
    private boolean isReport;
    private boolean isReportClick;
    private int jmPd;
    private CommonJumpLoader.JumpLoaderResult jumpResult;
    private String k;
    private String keyIaIcon;
    private int keyIaOri;
    private int keyIaRst;
    private String keyIaUrl;
    private String label;
    private String landingType;
    private int linkType;
    private Map<String, String> loopbackMap;
    private String loopbackString;
    private b mediaViewHolder;
    private String mp;
    private String mraid;
    private j nativeVideoTracking;
    private String nativeVideoTrackingString;
    private String noticeUrl = "";
    private int nvT2 = 6;
    private int oc_time;
    private int oc_type = 0;
    private int offerType;
    private String omid = null;
    private String onlyImpressionURL = "";
    private String pkgSource;
    private int playable_ads_without_video = 1;
    private long plct = 0;
    private long plctb = 0;
    private boolean preClick = false;
    private int preClickInterval;
    private String q;
    private String r;
    private String requestId;
    private String requestIdNotice;
    private int retarget_offer;
    private int rewardAmount;
    private int rewardPlayStatus;
    private c rewardTemplateMode;
    private String reward_name;
    private int roverIsPost;
    private String roverMark;
    private int spareOfferFlag = -1;
    private int t_imp;
    private String t_list;
    private int tab = -1;
    private int template;
    private long ts;
    private int ttc_ct2;
    private int ttc_type;
    private int videoLength;
    public String videoMD5Value = "";
    private String videoResolution;
    private int videoSize;
    private String videoUrlEncode = "";
    private int video_end_type = 2;
    private int watchMile;
    private int wtick = 0;

    public static final class b implements Serializable {
        public boolean a = false;
        public boolean b = false;
        public boolean c = false;
        public boolean d = false;
        public boolean e = false;
        public boolean f = false;
        public boolean g = false;
        public boolean h = false;
        public boolean i = false;
        public boolean j = false;
        public boolean k = false;
        public Map<Integer, String> l;
    }

    public int getSpareOfferFlag() {
        return this.spareOfferFlag;
    }

    public void setSpareOfferFlag(int i) {
        this.spareOfferFlag = i;
    }

    public long getPlct() {
        return this.plct;
    }

    public void setPlct(long j) {
        this.plct = j;
    }

    public long getPlctb() {
        return this.plctb;
    }

    public void setPlctb(long j) {
        this.plctb = j;
    }

    public String getBannerUrl() {
        return this.bannerUrl;
    }

    public void setBannerUrl(String str) {
        this.bannerUrl = str;
    }

    public String getBannerHtml() {
        return this.bannerHtml;
    }

    public void setBannerHtml(String str) {
        this.bannerHtml = str;
    }

    public long getCreativeId() {
        return this.creativeId;
    }

    public void setCreativeId(long j) {
        this.creativeId = j;
    }

    public boolean isHasMtgTplMark() {
        return this.hasMtgTplMark;
    }

    public void setHasMtgTplMark(boolean z) {
        this.hasMtgTplMark = z;
    }

    public a getAdchoice() {
        return this.adchoice;
    }

    public void setAdchoice(a aVar) {
        this.adchoice = aVar;
    }

    public int getOc_type() {
        return this.oc_type;
    }

    public void setOc_type(int i) {
        this.oc_type = i;
    }

    public int getOc_time() {
        return this.oc_time;
    }

    public void setOc_time(int i) {
        this.oc_time = i;
    }

    public String getT_list() {
        return this.t_list;
    }

    public void setT_list(String str) {
        this.t_list = str;
    }

    public String getGhId() {
        return this.ghId;
    }

    public void setGhId(String str) {
        this.ghId = str;
    }

    public String getGhPath() {
        return this.ghPath;
    }

    public void setGhPath(String str) {
        this.ghPath = str;
    }

    public String getBindId() {
        return this.bindId;
    }

    public void setBindId(String str) {
        this.bindId = str;
    }

    public String getInteractiveCache() {
        return this.interactiveCache;
    }

    public void setInteractiveCache(String str) {
        this.interactiveCache = str;
    }

    public int getIsDownLoadZip() {
        return this.isDownLoadZip;
    }

    public void setIsDownLoadZip(int i) {
        this.isDownLoadZip = i;
    }

    public int getAdType() {
        return this.adType;
    }

    public void setAdType(int i) {
        this.adType = i;
    }

    public String getIa_ext1() {
        return this.ia_ext1;
    }

    public void setIa_ext1(String str) {
        this.ia_ext1 = str;
    }

    public String getIa_ext2() {
        return this.ia_ext2;
    }

    public void setIa_ext2(String str) {
        this.ia_ext2 = str;
    }

    public String getKeyIaIcon() {
        return this.keyIaIcon;
    }

    public void setKeyIaIcon(String str) {
        this.keyIaIcon = str;
    }

    public int getKeyIaRst() {
        return this.keyIaRst;
    }

    public void setKeyIaRst(int i) {
        this.keyIaRst = i;
    }

    public String getKeyIaUrl() {
        return this.keyIaUrl;
    }

    public void setKeyIaUrl(String str) {
        this.keyIaUrl = str;
    }

    public int getKeyIaOri() {
        return this.keyIaOri;
    }

    public void setKeyIaOri(int i) {
        this.keyIaOri = i;
    }

    public int getIsAddSuccesful() {
        return this.isAddSuccesful;
    }

    public void setIsAddSuccesful(int i) {
        this.isAddSuccesful = i;
    }

    public int getJmPd() {
        return this.jmPd;
    }

    public int getIsDeleted() {
        return this.isDeleted;
    }

    public void setIsDeleted(int i) {
        this.isDeleted = i;
    }

    public int getIsClick() {
        return this.isClick;
    }

    public void setIsClick(int i) {
        this.isClick = i;
    }

    public void setJmPd(int i) {
        this.jmPd = i;
    }

    public int getNvT2() {
        return this.nvT2;
    }

    public void setNvT2(int i) {
        this.nvT2 = i;
    }

    public String getGifUrl() {
        return this.gifUrl;
    }

    public void setGifUrl(String str) {
        this.gifUrl = str;
    }

    public HashMap<String, String> getAks() {
        return this.aks;
    }

    public void setAks(HashMap<String, String> hashMap) {
        this.aks = hashMap;
    }

    public String getK() {
        return this.k;
    }

    public void setK(String str) {
        this.k = str;
    }

    public String getQ() {
        return this.q;
    }

    public void setQ(String str) {
        this.q = str;
    }

    public String getR() {
        return this.r;
    }

    public void setR(String str) {
        this.r = str;
    }

    public String getAl() {
        return this.al;
    }

    public void setAl(String str) {
        this.al = str;
    }

    public String getMp() {
        return this.mp;
    }

    public void setMp(String str) {
        this.mp = str;
    }

    public boolean isBidCampaign() {
        return this.isBidCampaign;
    }

    public void setIsBidCampaign(boolean z) {
        this.isBidCampaign = z;
    }

    public String getBidToken() {
        return this.bidToken;
    }

    public void setBidToken(String str) {
        this.bidToken = str;
    }

    public int getEndcard_click_result() {
        return this.endcard_click_result;
    }

    public void setEndcard_click_result(int i) {
        this.endcard_click_result = i;
    }

    public int getImpUA() {
        return this.impUA;
    }

    public void setImpUA(int i) {
        this.impUA = i;
    }

    public int getcUA() {
        return this.cUA;
    }

    public void setcUA(int i) {
        this.cUA = i;
    }

    public String getVideoMD5Value() {
        return this.videoMD5Value;
    }

    public void setVideoMD5Value(String str) {
        this.videoMD5Value = str;
    }

    public int getVideo_end_type() {
        return this.video_end_type;
    }

    public void setVideo_end_type(int i) {
        this.video_end_type = i;
    }

    public String getMraid() {
        return this.mraid;
    }

    public void setMraid(String str) {
        this.mraid = str;
    }

    public boolean isMraid() {
        return this.isMraid;
    }

    public void setIsMraid(boolean z) {
        this.isMraid = z;
    }

    public String getendcard_url() {
        return this.endcard_url;
    }

    public void setendcard_url(String str) {
        this.endcard_url = str;
    }

    public int getPlayable_ads_without_video() {
        return this.playable_ads_without_video;
    }

    public void setPlayable_ads_without_video(int i) {
        this.playable_ads_without_video = i;
    }

    public Map<String, String> getLoopbackMap() {
        return this.loopbackMap;
    }

    public void setLoopbackMap(Map<String, String> map) {
        this.loopbackMap = map;
    }

    public String getLoopbackString() {
        return this.loopbackString;
    }

    public void setLoopbackString(String str) {
        this.loopbackString = str;
    }

    public String getCampaignUnitId() {
        return this.campaignUnitId;
    }

    public void setCampaignUnitId(String str) {
        this.campaignUnitId = str;
    }

    public String getNativeVideoTrackingString() {
        return this.nativeVideoTrackingString;
    }

    public void setNativeVideoTrackingString(String str) {
        this.nativeVideoTrackingString = str;
    }

    public j getNativeVideoTracking() {
        return this.nativeVideoTracking;
    }

    public void setNativeVideoTracking(j jVar) {
        this.nativeVideoTracking = jVar;
    }

    public String getRoverMark() {
        return this.roverMark;
    }

    public void setRoverMark(String str) {
        this.roverMark = str;
    }

    public int getRoverIsPost() {
        return this.roverIsPost;
    }

    public void setRoverIsPost(int i) {
        this.roverIsPost = i;
    }

    public String getAd_url_list() {
        return this.ad_url_list;
    }

    public void setAd_url_list(String str) {
        this.ad_url_list = str;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String str) {
        this.label = str;
    }

    public String getPkgSource() {
        return this.pkgSource;
    }

    public void setPkgSource(String str) {
        this.pkgSource = str;
    }

    public int getIex() {
        return this.iex;
    }

    public void setIex(int i) {
        this.iex = i;
    }

    public long getTs() {
        return this.ts;
    }

    public void setTs(long j) {
        this.ts = j;
    }

    public b getMediaViewHolder() {
        return this.mediaViewHolder;
    }

    public void setMediaViewHolder(b bVar) {
        this.mediaViewHolder = bVar;
    }

    public c getRewardTemplateMode() {
        return this.rewardTemplateMode;
    }

    public void setRewardTemplateMode(c cVar) {
        this.rewardTemplateMode = cVar;
    }

    public int getRetarget_offer() {
        return this.retarget_offer;
    }

    public void setRetarget_offer(int i) {
        this.retarget_offer = i;
    }

    public int getTtc_ct2() {
        return this.ttc_ct2;
    }

    public void setTtc_ct2(int i) {
        this.ttc_ct2 = i;
    }

    public int getTtc_type() {
        return this.ttc_type;
    }

    public void setTtc_type(int i) {
        this.ttc_type = i;
    }

    public String getAdvId() {
        return this.advId;
    }

    public void setAdvId(String str) {
        this.advId = str;
    }

    public int getRewardPlayStatus() {
        return this.rewardPlayStatus;
    }

    public void setRewardPlayStatus(int i) {
        this.rewardPlayStatus = i;
    }

    public String getGuidelines() {
        return this.guidelines;
    }

    public void setGuidelines(String str) {
        this.guidelines = str;
    }

    public int getOfferType() {
        return this.offerType;
    }

    public void setOfferType(int i) {
        this.offerType = i;
    }

    public String getHtmlUrl() {
        return this.htmlUrl;
    }

    public void setHtmlUrl(String str) {
        this.htmlUrl = str;
    }

    public String getEndScreenUrl() {
        return this.endScreenUrl;
    }

    public void setEndScreenUrl(String str) {
        this.endScreenUrl = str;
    }

    public int getRewardAmount() {
        return this.rewardAmount;
    }

    public void setRewardAmount(int i) {
        this.rewardAmount = i;
    }

    public String getRewardName() {
        return this.reward_name;
    }

    public void setRewardName(String str) {
        this.reward_name = str;
    }

    public int getLinkType() {
        return this.linkType;
    }

    public void setLinkType(int i) {
        this.linkType = i;
    }

    public void setAdCall(String str) {
        if (TextUtils.isEmpty(str)) {
            str = this.linkType != 2 ? "learn more" : "install";
        }
        super.setAdCall(str);
    }

    public int getBty() {
        return this.bty;
    }

    public void setBty(int i) {
        this.bty = i;
    }

    public String getAdvImp() {
        return this.advImp;
    }

    public void setAdvImp(String str) {
        this.advImp = str;
    }

    public Map<Integer, String> getAdvImpList() {
        return generateAdImpression(this.advImp);
    }

    public int getTImp() {
        return this.t_imp;
    }

    public void setTImp(int i) {
        this.t_imp = i;
    }

    public String getVideoUrlEncode() {
        return this.videoUrlEncode;
    }

    public void setVideoUrlEncode(String str) {
        this.videoUrlEncode = str;
    }

    public int getVideoLength() {
        return this.videoLength;
    }

    public void setVideoLength(int i) {
        this.videoLength = i;
    }

    public int getVideoSize() {
        return this.videoSize;
    }

    public void setVideoSize(int i) {
        this.videoSize = i;
    }

    public String getVideoResolution() {
        return this.videoResolution;
    }

    public void setVideoResolution(String str) {
        this.videoResolution = str;
    }

    public int getWatchMile() {
        return this.watchMile;
    }

    public void setWatchMile(int i) {
        this.watchMile = i;
    }

    public int getCacheLevel() {
        return this.cacheLevel;
    }

    public void setCacheLevel(int i) {
        this.cacheLevel = i;
    }

    public void setReport(boolean z) {
        this.isReport = z;
    }

    public boolean isReport() {
        return this.isReport;
    }

    public boolean isReportClick() {
        return this.isReportClick;
    }

    public void setReportClick(boolean z) {
        this.isReportClick = z;
    }

    public int getClickInterval() {
        return this.clickInterval;
    }

    public void setClickInterval(int i) {
        this.clickInterval = i;
    }

    public int getPreClickInterval() {
        return this.preClickInterval;
    }

    public void setPreClickInterval(int i) {
        this.preClickInterval = i;
    }

    public int getClickTimeOutInterval() {
        return this.clickTimeOutInterval;
    }

    public void setClickTimeOutInterval(int i) {
        this.clickTimeOutInterval = i;
    }

    public String getRequestId() {
        try {
            if (!TextUtils.isEmpty(this.requestId)) {
                return this.requestId;
            }
            if (TextUtils.isEmpty(this.onlyImpressionURL)) {
                return null;
            }
            Uri parse = Uri.parse(this.onlyImpressionURL);
            if (parse != null) {
                this.requestId = parse.getQueryParameter(JSON_KEY_AD_K);
                setRequestId(this.requestId);
            }
            return this.requestId;
        } catch (Exception unused) {
            return null;
        }
    }

    public void setRequestId(String str) {
        this.requestId = str;
    }

    public String getRequestIdNotice() {
        try {
            if (!TextUtils.isEmpty(this.requestIdNotice)) {
                return this.requestIdNotice;
            }
            if (TextUtils.isEmpty(this.noticeUrl)) {
                return "";
            }
            Uri parse = Uri.parse(this.noticeUrl);
            if (parse != null) {
                this.requestIdNotice = parse.getQueryParameter(JSON_KEY_AD_K);
                setRequestIdNotice(this.requestIdNotice);
            }
            return this.requestIdNotice;
        } catch (Exception unused) {
            return "";
        }
    }

    public void setRequestIdNotice(String str) {
        this.requestIdNotice = str;
    }

    public String getClick_mode() {
        return this.click_mode;
    }

    public void setClick_mode(String str) {
        this.click_mode = str;
    }

    public String getLandingType() {
        return this.landingType;
    }

    public void setLandingType(String str) {
        this.landingType = str;
    }

    public int getFca() {
        return this.fca;
    }

    public void setFca(int i) {
        this.fca = i;
    }

    public int getFcb() {
        return this.fcb;
    }

    public void setFcb(int i) {
        this.fcb = i;
    }

    public int getTab() {
        return this.tab;
    }

    public void setTab(int i) {
        this.tab = i;
    }

    public String getClickURL() {
        return this.clickURL;
    }

    public void setClickURL(String str) {
        this.clickURL = str;
    }

    public int getWtick() {
        return this.wtick;
    }

    public void setWtick(int i) {
        this.wtick = i;
    }

    public String getDeepLinkURL() {
        return this.deepLinkUrl;
    }

    public void setDeepLinkUrl(String str) {
        this.deepLinkUrl = str;
    }

    public String getImpressionURL() {
        return this.impressionURL;
    }

    public void setImpressionURL(String str) {
        this.impressionURL = str;
    }

    public String getNoticeUrl() {
        return this.noticeUrl;
    }

    public void setNoticeUrl(String str) {
        this.noticeUrl = str;
    }

    public boolean isPreClick() {
        return this.preClick;
    }

    public void setPreClick(boolean z) {
        this.preClick = z;
    }

    public String getOnlyImpressionURL() {
        return this.onlyImpressionURL;
    }

    public void setOnlyImpressionURL(String str) {
        this.onlyImpressionURL = str;
    }

    public CommonJumpLoader.JumpLoaderResult getJumpResult() {
        return this.jumpResult;
    }

    public void setJumpResult(CommonJumpLoader.JumpLoaderResult jumpLoaderResult) {
        this.jumpResult = jumpLoaderResult;
    }

    public int getTemplate() {
        return this.template;
    }

    public void setTemplate(int i) {
        this.template = i;
    }

    public String getImageSize() {
        return this.imageSize;
    }

    public void setImageSize(String str) {
        this.imageSize = str;
    }

    public String getHost() {
        if (TextUtils.isEmpty(getNoticeUrl())) {
            return "";
        }
        try {
            URL url = new URL(getNoticeUrl());
            return url.getProtocol() + "://" + url.getHost();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static CampaignEx parseSettingCampaign(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        CampaignEx campaignEx = new CampaignEx();
        campaignEx.setId(jSONObject.optString("campaignid"));
        campaignEx.setPackageName(jSONObject.optString("packageName"));
        campaignEx.setAppName(jSONObject.optString("title"));
        campaignEx.setAdCall(jSONObject.optString("cta"));
        campaignEx.setAppDesc(jSONObject.optString(JSON_KEY_DESC));
        campaignEx.setImpressionURL(jSONObject.optString(JSON_KEY_IMPRESSION_URL));
        campaignEx.setImageUrl(jSONObject.optString("image_url"));
        campaignEx.setPlct(jSONObject.optLong(JSON_KEY_PLCT));
        campaignEx.setPlctb(jSONObject.optLong(JSON_KEY_PLCTB));
        campaignEx.setBannerUrl(jSONObject.optString("banner_url"));
        campaignEx.setBannerHtml(jSONObject.optString("banner_html"));
        campaignEx.setCreativeId((long) jSONObject.optInt(JSON_KEY_CREATIVE_ID));
        return campaignEx;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: com.mintegral.msdk.base.entity.CampaignEx} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: java.lang.String} */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v2 */
    /* JADX WARN: Type inference failed for: r0v10 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.mintegral.msdk.base.entity.CampaignEx parseShortCutsCampaign(org.json.JSONObject r6) {
        /*
            r0 = 0
            if (r6 == 0) goto L_0x0374
            com.mintegral.msdk.base.entity.CampaignEx r1 = new com.mintegral.msdk.base.entity.CampaignEx     // Catch:{ Exception -> 0x036c }
            r1.<init>()     // Catch:{ Exception -> 0x036c }
            java.lang.String r2 = "id"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setId(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "title"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setAppName(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "desc"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setAppDesc(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "package_name"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setPackageName(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "icon_url"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setIconUrl(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "image_url"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setImageUrl(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "app_size"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setSize(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "image_size"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setImageSize(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "impression_url"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setImpressionURL(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "click_url"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setClickURL(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "wtick"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setWtick(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "deep_link"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setDeepLinkUrl(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "notice_url"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setNoticeUrl(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ttc"
            boolean r2 = r6.optBoolean(r2)     // Catch:{ Exception -> 0x036b }
            r1.setPreClick(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "template"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setTemplate(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ad_source_id"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setType(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "fca"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setFca(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "fcb"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setFcb(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "endcard_click_result"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setEndcard_click_result(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "rating"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            boolean r2 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x036b }
            if (r2 != 0) goto L_0x00ce
            java.lang.String r2 = "rating"
            java.lang.String r3 = "0"
            java.lang.String r2 = r6.optString(r2, r3)     // Catch:{ Exception -> 0x036b }
            double r2 = java.lang.Double.parseDouble(r2)     // Catch:{ Exception -> 0x036b }
            r1.setRating(r2)     // Catch:{ Exception -> 0x036b }
        L_0x00ce:
            java.lang.String r2 = "click_mode"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setClick_mode(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "landing_type"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setLandingType(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "link_type"
            r3 = 4
            int r2 = r6.optInt(r2, r3)     // Catch:{ Exception -> 0x036b }
            r1.setLinkType(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "c_ct"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setClickInterval(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ttc_ct"
            r3 = 604800(0x93a80, float:8.47505E-40)
            int r2 = r6.optInt(r2, r3)     // Catch:{ Exception -> 0x036b }
            r1.setPreClickInterval(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ctatext"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setAdCall(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ad_url_list"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setAd_url_list(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "adv_id"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setAdvId(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ttc_type"
            r3 = 3
            int r2 = r6.optInt(r2, r3)     // Catch:{ Exception -> 0x036b }
            r1.setTtc_type(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ttc_ct2"
            r3 = 1800(0x708, float:2.522E-42)
            int r2 = r6.optInt(r2, r3)     // Catch:{ Exception -> 0x036b }
            r1.setTtc_ct2(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "retarget_offer"
            r3 = 2
            int r2 = r6.optInt(r2, r3)     // Catch:{ Exception -> 0x036b }
            r1.setRetarget_offer(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "video_url"
            r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "video_length"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setVideoLength(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "video_size"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setVideoSize(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "video_resolution"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setVideoResolution(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "watch_mile"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setWatchMile(r2)     // Catch:{ Exception -> 0x036b }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x036b }
            r1.setTimestamp(r4)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ctype"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setBty(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "adv_imp"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setAdvImp(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "t_imp"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setTImp(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "guidelines"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setGuidelines(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "offer_type"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setOfferType(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "reward_name"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setRewardName(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "reward_amount"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setRewardAmount(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "mark"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setRoverMark(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "isPost"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setRoverIsPost(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ad_tracking"
            boolean r2 = r6.has(r2)     // Catch:{ Exception -> 0x01d9 }
            if (r2 == 0) goto L_0x01e0
            java.lang.String r2 = "ad_tracking"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x01d9 }
            boolean r4 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x01d9 }
            if (r4 != 0) goto L_0x01e0
            r1.setNativeVideoTrackingString(r2)     // Catch:{ Exception -> 0x01d9 }
            com.mintegral.msdk.base.entity.j r2 = TrackingStr2Object(r2)     // Catch:{ Exception -> 0x01d9 }
            r1.setNativeVideoTracking(r2)     // Catch:{ Exception -> 0x01d9 }
            goto L_0x01e0
        L_0x01d9:
            java.lang.String r2 = ""
            java.lang.String r4 = "ad_tracking parser error"
            com.mintegral.msdk.base.utils.g.d(r2, r4)     // Catch:{ Exception -> 0x036b }
        L_0x01e0:
            java.lang.String r2 = "video_end_type"
            int r2 = r6.optInt(r2, r3)     // Catch:{ Exception -> 0x036b }
            r1.setVideo_end_type(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "endcard_url"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setendcard_url(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "playable_ads_without_video"
            r4 = 1
            int r2 = r6.optInt(r2, r4)     // Catch:{ Exception -> 0x036b }
            r1.setPlayable_ads_without_video(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "loopback"
            boolean r2 = r6.has(r2)     // Catch:{ Exception -> 0x021b }
            if (r2 == 0) goto L_0x0222
            java.lang.String r2 = "loopback"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x021b }
            boolean r5 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x021b }
            if (r5 != 0) goto L_0x0222
            r1.setLoopbackString(r2)     // Catch:{ Exception -> 0x021b }
            java.util.Map r2 = loopbackStrToMap(r2)     // Catch:{ Exception -> 0x021b }
            r1.setLoopbackMap(r2)     // Catch:{ Exception -> 0x021b }
            goto L_0x0222
        L_0x021b:
            java.lang.String r2 = ""
            java.lang.String r5 = "loopback parser error"
            com.mintegral.msdk.base.utils.g.d(r2, r5)     // Catch:{ Exception -> 0x036b }
        L_0x0222:
            java.lang.String r2 = "md5_file"
            boolean r2 = r6.has(r2)     // Catch:{ Exception -> 0x036b }
            if (r2 == 0) goto L_0x0233
            java.lang.String r2 = "md5_file"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setVideoMD5Value(r2)     // Catch:{ Exception -> 0x036b }
        L_0x0233:
            java.lang.String r2 = "nv_t2"
            boolean r2 = r6.has(r2)     // Catch:{ Exception -> 0x036b }
            if (r2 == 0) goto L_0x0244
            java.lang.String r2 = "nv_t2"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setNvT2(r2)     // Catch:{ Exception -> 0x036b }
        L_0x0244:
            java.lang.String r2 = "gif_url"
            boolean r2 = r6.has(r2)     // Catch:{ Exception -> 0x036b }
            if (r2 == 0) goto L_0x0255
            java.lang.String r2 = "gif_url"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setGifUrl(r2)     // Catch:{ Exception -> 0x036b }
        L_0x0255:
            java.lang.String r2 = "rv"
            org.json.JSONObject r2 = r6.optJSONObject(r2)     // Catch:{ Exception -> 0x036b }
            com.mintegral.msdk.base.entity.CampaignEx$c r2 = com.mintegral.msdk.base.entity.CampaignEx.c.a(r2)     // Catch:{ Exception -> 0x036b }
            r1.setRewardTemplateMode(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "c_toi"
            int r2 = r6.optInt(r2, r3)     // Catch:{ Exception -> 0x036b }
            r1.setClickTimeOutInterval(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "imp_ua"
            int r2 = r6.optInt(r2, r4)     // Catch:{ Exception -> 0x036b }
            r1.setImpUA(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "c_ua"
            int r2 = r6.optInt(r2, r4)     // Catch:{ Exception -> 0x036b }
            r1.setcUA(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "jm_pd"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setJmPd(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ia_icon"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setKeyIaIcon(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ia_rst"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setKeyIaRst(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ia_url"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setKeyIaUrl(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ia_ori"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setKeyIaOri(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ad_type"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setAdType(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ia_ext1"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setIa_ext1(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ia_ext2"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setIa_ext2(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "is_download_zip"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setIsDownLoadZip(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "ia_cache"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setInteractiveCache(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "gh_id"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            boolean r3 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x036b }
            if (r3 != 0) goto L_0x0302
            r1.setGhId(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "gh_path"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            boolean r3 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x036b }
            if (r3 != 0) goto L_0x02f9
            java.lang.String r2 = com.mintegral.msdk.base.utils.a.c(r2)     // Catch:{ Exception -> 0x036b }
            r1.setGhPath(r2)     // Catch:{ Exception -> 0x036b }
        L_0x02f9:
            java.lang.String r2 = "bind_id"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setBindId(r2)     // Catch:{ Exception -> 0x036b }
        L_0x0302:
            java.lang.String r2 = "oc_time"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setOc_time(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "oc_type"
            int r2 = r6.optInt(r2)     // Catch:{ Exception -> 0x036b }
            r1.setOc_type(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "t_list"
            java.lang.String r2 = r6.optString(r2)     // Catch:{ Exception -> 0x036b }
            r1.setT_list(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "adchoice"
            java.lang.String r3 = ""
            java.lang.String r2 = r6.optString(r2, r3)     // Catch:{ Exception -> 0x036b }
            com.mintegral.msdk.base.entity.CampaignEx$a r2 = com.mintegral.msdk.base.entity.CampaignEx.a.a(r2)     // Catch:{ Exception -> 0x036b }
            r1.setAdchoice(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "plct"
            long r2 = r6.optLong(r2)     // Catch:{ Exception -> 0x036b }
            r1.setPlct(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "plctb"
            long r2 = r6.optLong(r2)     // Catch:{ Exception -> 0x036b }
            r1.setPlctb(r2)     // Catch:{ Exception -> 0x036b }
            java.lang.String r2 = "omid"
            org.json.JSONArray r2 = r6.optJSONArray(r2)     // Catch:{ Exception -> 0x036b }
            if (r2 != 0) goto L_0x0347
            goto L_0x034b
        L_0x0347:
            java.lang.String r0 = r2.toString()     // Catch:{ Exception -> 0x036b }
        L_0x034b:
            r1.setOmid(r0)     // Catch:{ Exception -> 0x036b }
            java.lang.String r0 = "banner_url"
            java.lang.String r0 = r6.optString(r0)     // Catch:{ Exception -> 0x036b }
            r1.setBannerUrl(r0)     // Catch:{ Exception -> 0x036b }
            java.lang.String r0 = "banner_html"
            java.lang.String r0 = r6.optString(r0)     // Catch:{ Exception -> 0x036b }
            r1.setBannerHtml(r0)     // Catch:{ Exception -> 0x036b }
            java.lang.String r0 = "creative_id"
            int r6 = r6.optInt(r0)     // Catch:{ Exception -> 0x036b }
            long r2 = (long) r6     // Catch:{ Exception -> 0x036b }
            r1.setCreativeId(r2)     // Catch:{ Exception -> 0x036b }
            return r1
        L_0x036b:
            r0 = r1
        L_0x036c:
            java.lang.String r6 = com.mintegral.msdk.base.entity.CampaignEx.TAG
            java.lang.String r1 = "parse campaign json exception"
            com.mintegral.msdk.base.utils.g.d(r6, r1)
            return r0
        L_0x0374:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.entity.CampaignEx.parseShortCutsCampaign(org.json.JSONObject):com.mintegral.msdk.base.entity.CampaignEx");
    }

    private static String replaceValueByKey(CampaignUnit campaignUnit, CampaignEx campaignEx, String str) {
        if (campaignUnit == null || TextUtils.isEmpty(str) || campaignEx == null) {
            return str;
        }
        try {
            HashMap<String, String> rks = campaignUnit.getRks();
            if (rks != null) {
                rks.entrySet().iterator();
                for (Map.Entry next : rks.entrySet()) {
                    str = str.replaceAll("\\{" + ((String) next.getKey()) + "\\}", (String) next.getValue());
                }
            }
            HashMap<String, String> aks2 = campaignEx.getAks();
            if (aks2 != null) {
                aks2.entrySet().iterator();
                for (Map.Entry next2 : aks2.entrySet()) {
                    str = str.replaceAll("\\{" + ((String) next2.getKey()) + "\\}", (String) next2.getValue());
                }
            }
            String replaceAll = str.replaceAll("\\{c\\}", URLEncoder.encode(campaignUnit.assembCParams(), "utf-8"));
            try {
                Matcher matcher = Pattern.compile("=\\{.*?\\}").matcher(replaceAll);
                while (true) {
                    str = replaceAll;
                    if (!matcher.find()) {
                        break;
                    }
                    replaceAll = str.replace(matcher.group(0), Constants.RequestParameters.EQUAL);
                }
            } catch (Throwable th) {
                str = replaceAll;
                th = th;
                g.c(TAG, th.getMessage(), th);
                return str;
            }
        } catch (Throwable th2) {
            th = th2;
            g.c(TAG, th.getMessage(), th);
            return str;
        }
        return str;
    }

    public static CampaignEx parseCampaign(JSONObject jSONObject, String str, String str2, String str3, boolean z, CampaignUnit campaignUnit) {
        return parseCampaign(jSONObject, str, str2, str3, z, campaignUnit, "");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: com.mintegral.msdk.base.entity.CampaignEx} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: java.lang.String} */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v2 */
    /* JADX WARN: Type inference failed for: r0v5 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.mintegral.msdk.base.entity.CampaignEx parseCampaign(org.json.JSONObject r7, java.lang.String r8, java.lang.String r9, java.lang.String r10, boolean r11, com.mintegral.msdk.base.entity.CampaignUnit r12, java.lang.String r13) {
        /*
            r0 = 0
            if (r7 == 0) goto L_0x03fc
            com.mintegral.msdk.base.entity.CampaignEx r1 = new com.mintegral.msdk.base.entity.CampaignEx     // Catch:{ Exception -> 0x03f4 }
            r1.<init>()     // Catch:{ Exception -> 0x03f4 }
            java.lang.String r2 = "aks"
            java.lang.String r2 = r7.optString(r2)     // Catch:{ Exception -> 0x03f3 }
            boolean r3 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x03f3 }
            if (r3 != 0) goto L_0x003b
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Exception -> 0x03f3 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x03f3 }
            java.util.Iterator r2 = r3.keys()     // Catch:{ Exception -> 0x03f3 }
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ Exception -> 0x03f3 }
            r4.<init>()     // Catch:{ Exception -> 0x03f3 }
        L_0x0022:
            if (r2 == 0) goto L_0x0038
            boolean r5 = r2.hasNext()     // Catch:{ Exception -> 0x03f3 }
            if (r5 == 0) goto L_0x0038
            java.lang.Object r5 = r2.next()     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r6 = r3.optString(r5)     // Catch:{ Exception -> 0x03f3 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x03f3 }
            goto L_0x0022
        L_0x0038:
            r1.setAks(r4)     // Catch:{ Exception -> 0x03f3 }
        L_0x003b:
            boolean r2 = android.text.TextUtils.isEmpty(r13)     // Catch:{ Exception -> 0x03f3 }
            r3 = 1
            if (r2 != 0) goto L_0x0048
            r1.setBidToken(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setIsBidCampaign(r3)     // Catch:{ Exception -> 0x03f3 }
        L_0x0048:
            java.lang.String r13 = "id"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setId(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "title"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setAppName(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "desc"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setAppDesc(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "package_name"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setPackageName(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "icon_url"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setIconUrl(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "image_url"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setImageUrl(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "app_size"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setSize(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "image_size"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setImageSize(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "impression_url"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = replaceValueByKey(r12, r1, r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setImpressionURL(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "click_url"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = replaceValueByKey(r12, r1, r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setClickURL(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "wtick"
            int r13 = r7.optInt(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setWtick(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "deep_link"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = replaceValueByKey(r12, r1, r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setDeepLinkUrl(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "notice_url"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = replaceValueByKey(r12, r1, r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setNoticeUrl(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "ttc"
            boolean r13 = r7.optBoolean(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setPreClick(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "template"
            int r13 = r7.optInt(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setTemplate(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "ad_source_id"
            int r13 = r7.optInt(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setType(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "fca"
            int r13 = r7.optInt(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setFca(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "fcb"
            int r13 = r7.optInt(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setFcb(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "endcard_click_result"
            int r13 = r7.optInt(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setEndcard_click_result(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "rating"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            boolean r13 = android.text.TextUtils.isEmpty(r13)     // Catch:{ Exception -> 0x03f3 }
            if (r13 != 0) goto L_0x011e
            java.lang.String r13 = "rating"
            java.lang.String r2 = "0"
            java.lang.String r13 = r7.optString(r13, r2)     // Catch:{ Exception -> 0x03f3 }
            double r4 = java.lang.Double.parseDouble(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setRating(r4)     // Catch:{ Exception -> 0x03f3 }
        L_0x011e:
            java.lang.String r13 = "click_mode"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setClick_mode(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "landing_type"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setLandingType(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "link_type"
            r2 = 4
            int r13 = r7.optInt(r13, r2)     // Catch:{ Exception -> 0x03f3 }
            r1.setLinkType(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "c_ct"
            int r13 = r7.optInt(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setClickInterval(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "ttc_ct"
            r2 = 604800(0x93a80, float:8.47505E-40)
            int r13 = r7.optInt(r13, r2)     // Catch:{ Exception -> 0x03f3 }
            r1.setPreClickInterval(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "ctatext"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setAdCall(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "ad_url_list"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setAd_url_list(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "adv_id"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setAdvId(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "ttc_type"
            r2 = 3
            int r13 = r7.optInt(r13, r2)     // Catch:{ Exception -> 0x03f3 }
            r1.setTtc_type(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "ttc_ct2"
            r2 = 1800(0x708, float:2.522E-42)
            int r13 = r7.optInt(r13, r2)     // Catch:{ Exception -> 0x03f3 }
            r1.setTtc_ct2(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "retarget_offer"
            r2 = 2
            int r13 = r7.optInt(r13, r2)     // Catch:{ Exception -> 0x03f3 }
            r1.setRetarget_offer(r13)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r13 = "video_url"
            java.lang.String r13 = r7.optString(r13)     // Catch:{ Exception -> 0x03f3 }
            boolean r4 = android.text.TextUtils.isEmpty(r13)     // Catch:{ Exception -> 0x03f3 }
            if (r4 != 0) goto L_0x01a2
            if (r11 == 0) goto L_0x019b
            r1.setVideoUrlEncode(r13)     // Catch:{ Exception -> 0x03f3 }
            goto L_0x01a2
        L_0x019b:
            java.lang.String r11 = com.mintegral.msdk.base.utils.a.c(r13)     // Catch:{ Exception -> 0x03f3 }
            r1.setVideoUrlEncode(r11)     // Catch:{ Exception -> 0x03f3 }
        L_0x01a2:
            java.lang.String r11 = "video_length"
            int r11 = r7.optInt(r11)     // Catch:{ Exception -> 0x03f3 }
            r1.setVideoLength(r11)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r11 = "video_size"
            int r11 = r7.optInt(r11)     // Catch:{ Exception -> 0x03f3 }
            r1.setVideoSize(r11)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r11 = "video_resolution"
            java.lang.String r11 = r7.optString(r11)     // Catch:{ Exception -> 0x03f3 }
            r1.setVideoResolution(r11)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r11 = "watch_mile"
            int r11 = r7.optInt(r11)     // Catch:{ Exception -> 0x03f3 }
            r1.setWatchMile(r11)     // Catch:{ Exception -> 0x03f3 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x03f3 }
            r1.setTimestamp(r4)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = replaceValueByKey(r12, r1, r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setOnlyImpressionURL(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "ctype"
            int r8 = r7.optInt(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setBty(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "adv_imp"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setAdvImp(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "t_imp"
            int r8 = r7.optInt(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setTImp(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setHtmlUrl(r9)     // Catch:{ Exception -> 0x03f3 }
            r1.setEndScreenUrl(r10)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "guidelines"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setGuidelines(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "offer_type"
            int r8 = r7.optInt(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setOfferType(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "reward_name"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setRewardName(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "reward_amount"
            int r8 = r7.optInt(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setRewardAmount(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "mark"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setRoverMark(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "isPost"
            int r8 = r7.optInt(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setRoverIsPost(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "ad_tracking"
            boolean r8 = r7.has(r8)     // Catch:{ Exception -> 0x024e }
            if (r8 == 0) goto L_0x0255
            java.lang.String r8 = "ad_tracking"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x024e }
            java.lang.String r8 = replaceValueByKey(r12, r1, r8)     // Catch:{ Exception -> 0x024e }
            boolean r9 = android.text.TextUtils.isEmpty(r8)     // Catch:{ Exception -> 0x024e }
            if (r9 != 0) goto L_0x0255
            r1.setNativeVideoTrackingString(r8)     // Catch:{ Exception -> 0x024e }
            com.mintegral.msdk.base.entity.j r8 = TrackingStr2Object(r8)     // Catch:{ Exception -> 0x024e }
            r1.setNativeVideoTracking(r8)     // Catch:{ Exception -> 0x024e }
            goto L_0x0255
        L_0x024e:
            java.lang.String r8 = ""
            java.lang.String r9 = "ad_tracking parser error"
            com.mintegral.msdk.base.utils.g.d(r8, r9)     // Catch:{ Exception -> 0x03f3 }
        L_0x0255:
            java.lang.String r8 = "video_end_type"
            int r8 = r7.optInt(r8, r2)     // Catch:{ Exception -> 0x03f3 }
            r1.setVideo_end_type(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "endcard_url"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setendcard_url(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "playable_ads_without_video"
            int r8 = r7.optInt(r8, r3)     // Catch:{ Exception -> 0x03f3 }
            r1.setPlayable_ads_without_video(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "loopback"
            boolean r8 = r7.has(r8)     // Catch:{ Exception -> 0x028f }
            if (r8 == 0) goto L_0x0296
            java.lang.String r8 = "loopback"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x028f }
            boolean r9 = android.text.TextUtils.isEmpty(r8)     // Catch:{ Exception -> 0x028f }
            if (r9 != 0) goto L_0x0296
            r1.setLoopbackString(r8)     // Catch:{ Exception -> 0x028f }
            java.util.Map r8 = loopbackStrToMap(r8)     // Catch:{ Exception -> 0x028f }
            r1.setLoopbackMap(r8)     // Catch:{ Exception -> 0x028f }
            goto L_0x0296
        L_0x028f:
            java.lang.String r8 = ""
            java.lang.String r9 = "loopback parser error"
            com.mintegral.msdk.base.utils.g.d(r8, r9)     // Catch:{ Exception -> 0x03f3 }
        L_0x0296:
            java.lang.String r8 = "md5_file"
            boolean r8 = r7.has(r8)     // Catch:{ Exception -> 0x03f3 }
            if (r8 == 0) goto L_0x02a7
            java.lang.String r8 = "md5_file"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setVideoMD5Value(r8)     // Catch:{ Exception -> 0x03f3 }
        L_0x02a7:
            java.lang.String r8 = "nv_t2"
            boolean r8 = r7.has(r8)     // Catch:{ Exception -> 0x03f3 }
            if (r8 == 0) goto L_0x02b8
            java.lang.String r8 = "nv_t2"
            int r8 = r7.optInt(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setNvT2(r8)     // Catch:{ Exception -> 0x03f3 }
        L_0x02b8:
            java.lang.String r8 = "gif_url"
            boolean r8 = r7.has(r8)     // Catch:{ Exception -> 0x03f3 }
            if (r8 == 0) goto L_0x02c9
            java.lang.String r8 = "gif_url"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setGifUrl(r8)     // Catch:{ Exception -> 0x03f3 }
        L_0x02c9:
            java.lang.String r8 = "rv"
            org.json.JSONObject r8 = r7.optJSONObject(r8)     // Catch:{ Exception -> 0x03f3 }
            com.mintegral.msdk.base.entity.CampaignEx$c r8 = com.mintegral.msdk.base.entity.CampaignEx.c.a(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setRewardTemplateMode(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "c_toi"
            int r8 = r7.optInt(r8, r2)     // Catch:{ Exception -> 0x03f3 }
            r1.setClickTimeOutInterval(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "imp_ua"
            int r8 = r7.optInt(r8, r3)     // Catch:{ Exception -> 0x03f3 }
            r1.setImpUA(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "c_ua"
            int r8 = r7.optInt(r8, r3)     // Catch:{ Exception -> 0x03f3 }
            r1.setcUA(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "jm_pd"
            int r8 = r7.optInt(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setJmPd(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "ia_icon"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setKeyIaIcon(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "ia_rst"
            int r8 = r7.optInt(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setKeyIaRst(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "ia_url"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setKeyIaUrl(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "ia_ori"
            int r8 = r7.optInt(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setKeyIaOri(r8)     // Catch:{ Exception -> 0x03f3 }
            int r8 = r12.getAdType()     // Catch:{ Exception -> 0x03f3 }
            r1.setAdType(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "ia_ext1"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setIa_ext1(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "ia_ext2"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setIa_ext2(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "is_download_zip"
            int r8 = r7.optInt(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setIsDownLoadZip(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "ia_cache"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setInteractiveCache(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "gh_id"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            boolean r9 = android.text.TextUtils.isEmpty(r8)     // Catch:{ Exception -> 0x03f3 }
            if (r9 != 0) goto L_0x0374
            r1.setGhId(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "gh_path"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            boolean r9 = android.text.TextUtils.isEmpty(r8)     // Catch:{ Exception -> 0x03f3 }
            if (r9 != 0) goto L_0x036b
            java.lang.String r8 = com.mintegral.msdk.base.utils.a.c(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setGhPath(r8)     // Catch:{ Exception -> 0x03f3 }
        L_0x036b:
            java.lang.String r8 = "bind_id"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setBindId(r8)     // Catch:{ Exception -> 0x03f3 }
        L_0x0374:
            java.lang.String r8 = "oc_time"
            int r8 = r7.optInt(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setOc_time(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "oc_type"
            int r8 = r7.optInt(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setOc_type(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "t_list"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setT_list(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "adchoice"
            java.lang.String r9 = ""
            java.lang.String r8 = r7.optString(r8, r9)     // Catch:{ Exception -> 0x03f3 }
            com.mintegral.msdk.base.entity.CampaignEx$a r8 = com.mintegral.msdk.base.entity.CampaignEx.a.a(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setAdchoice(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "plct"
            long r8 = r7.optLong(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setPlct(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "plctb"
            long r8 = r7.optLong(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setPlctb(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "banner_url"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setBannerUrl(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "banner_html"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setBannerHtml(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "creative_id"
            long r8 = r7.optLong(r8)     // Catch:{ Exception -> 0x03f3 }
            r1.setCreativeId(r8)     // Catch:{ Exception -> 0x03f3 }
            java.lang.String r8 = "mraid"
            java.lang.String r8 = r7.optString(r8)     // Catch:{ Exception -> 0x03f3 }
            boolean r9 = android.text.TextUtils.isEmpty(r8)     // Catch:{ Exception -> 0x03f3 }
            if (r9 != 0) goto L_0x03de
            r1.setIsMraid(r3)     // Catch:{ Exception -> 0x03f3 }
            r1.setMraid(r8)     // Catch:{ Exception -> 0x03f3 }
            goto L_0x03e2
        L_0x03de:
            r8 = 0
            r1.setIsMraid(r8)     // Catch:{ Exception -> 0x03f3 }
        L_0x03e2:
            java.lang.String r8 = "omid"
            org.json.JSONArray r7 = r7.optJSONArray(r8)     // Catch:{ Exception -> 0x03f3 }
            if (r7 != 0) goto L_0x03eb
            goto L_0x03ef
        L_0x03eb:
            java.lang.String r0 = r7.toString()     // Catch:{ Exception -> 0x03f3 }
        L_0x03ef:
            r1.setOmid(r0)     // Catch:{ Exception -> 0x03f3 }
            return r1
        L_0x03f3:
            r0 = r1
        L_0x03f4:
            java.lang.String r7 = com.mintegral.msdk.base.entity.CampaignEx.TAG
            java.lang.String r8 = "parse campaign json exception"
            com.mintegral.msdk.base.utils.g.d(r7, r8)
            return r0
        L_0x03fc:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.entity.CampaignEx.parseCampaign(org.json.JSONObject, java.lang.String, java.lang.String, java.lang.String, boolean, com.mintegral.msdk.base.entity.CampaignUnit, java.lang.String):com.mintegral.msdk.base.entity.CampaignEx");
    }

    public static Map<String, String> loopbackStrToMap(String str) {
        HashMap hashMap = null;
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            HashMap hashMap2 = new HashMap();
            try {
                JSONObject jSONObject = new JSONObject(str);
                hashMap2.put("domain", jSONObject.getString("domain"));
                hashMap2.put("key", jSONObject.getString("key"));
                hashMap2.put("value", jSONObject.getString("value"));
                return hashMap2;
            } catch (Throwable unused) {
                hashMap = hashMap2;
                g.d("", "loopbackStrToMap error");
                return hashMap;
            }
        } catch (Throwable unused2) {
            g.d("", "loopbackStrToMap error");
            return hashMap;
        }
    }

    public static j TrackingStr2Object(String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            JSONObject jSONObject = new JSONObject(str);
            j jVar = new j();
            jVar.r(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_NATIVE_VIDOE_IMPRESSION)));
            jVar.g(processNativeVideoTrackingArray(jSONObject.optJSONArray("start")));
            jVar.h(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_NATIVE_VIDEO_FIRST_QUARTILE)));
            jVar.i(processNativeVideoTrackingArray(jSONObject.optJSONArray("midpoint")));
            jVar.j(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_NATIVE_VIDEO_THIRD_QUARTILE)));
            jVar.k(processNativeVideoTrackingArray(jSONObject.optJSONArray("complete")));
            jVar.a(parsePlayCentage(jSONObject.optJSONArray(JSON_NATIVE_VIDEO_PLAY_PERCENTAGE)));
            jVar.l(processNativeVideoTrackingArray(jSONObject.optJSONArray("mute")));
            jVar.m(processNativeVideoTrackingArray(jSONObject.optJSONArray("unmute")));
            jVar.n(processNativeVideoTrackingArray(jSONObject.optJSONArray("click")));
            jVar.o(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_NATIVE_VIDEO_PAUSE)));
            jVar.p(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_NATIVE_VIDEO_RESUME)));
            jVar.q(processNativeVideoTrackingArray(jSONObject.optJSONArray("error")));
            jVar.s(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_NATIVE_VIDEO_ENDCARD)));
            jVar.u(processNativeVideoTrackingArray(jSONObject.optJSONArray("close")));
            jVar.t(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_NATIVE_VIDEO_ENDCARD_SHOW)));
            jVar.v(processNativeVideoTrackingArray(jSONObject.optJSONArray("video_click")));
            jVar.f(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_KEY_AD_TRACKING_IMPRESSION_T2)));
            jVar.c(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_KEY_AD_TRACKING_APK_START)));
            jVar.d(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_KEY_AD_TRACKING_APK_END)));
            jVar.e(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_KEY_AD_TRACKING_APK_INSTALL)));
            jVar.a(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_KEY_AD_TRACKING_DROPOUT_TRACK)));
            jVar.b(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_KEY_AD_TRACKING_PLYCMPT_TRACK)));
            jVar.w(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_KEY_PUB_IMP)));
            return jVar;
        } catch (JSONException unused) {
            g.d(TAG, "parse error");
            return null;
        }
    }

    private static String[] processNativeVideoTrackingArray(JSONArray jSONArray) {
        if (jSONArray == null || jSONArray.length() <= 0) {
            return null;
        }
        String[] strArr = new String[jSONArray.length()];
        for (int i = 0; i < jSONArray.length(); i++) {
            strArr[i] = jSONArray.optString(i);
        }
        return strArr;
    }

    private Map<Integer, String> generateAdImpression(String str) {
        HashMap hashMap = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            JSONArray jSONArray = new JSONArray(str);
            if (jSONArray.length() <= 0) {
                return null;
            }
            HashMap hashMap2 = new HashMap();
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    JSONObject optJSONObject = jSONArray.optJSONObject(i);
                    int optInt = optJSONObject.optInt(JSON_AD_IMP_KEY);
                    hashMap2.put(Integer.valueOf(optInt), optJSONObject.optString("url"));
                    i++;
                } catch (Exception e) {
                    e = e;
                    hashMap = hashMap2;
                    e.printStackTrace();
                    return hashMap;
                }
            }
            return hashMap2;
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return hashMap;
        }
    }

    public List<String> getAdUrlList() {
        String ad_url_list2 = getAd_url_list();
        ArrayList arrayList = null;
        try {
            if (!TextUtils.isEmpty(ad_url_list2)) {
                JSONArray jSONArray = new JSONArray(ad_url_list2);
                ArrayList arrayList2 = new ArrayList();
                int i = 0;
                while (i < jSONArray.length()) {
                    try {
                        arrayList2.add(jSONArray.optString(i));
                        i++;
                    } catch (Exception e) {
                        Exception exc = e;
                        arrayList = arrayList2;
                        e = exc;
                        e.printStackTrace();
                        return arrayList;
                    }
                }
                return arrayList2;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return arrayList;
        }
        return arrayList;
    }

    public static JSONArray parseCamplistToJson(List<CampaignEx> list) {
        JSONArray jSONArray = null;
        if (list != null) {
            try {
                if (list.size() > 0) {
                    JSONArray jSONArray2 = new JSONArray();
                    try {
                        for (CampaignEx campaignToJsonObject : list) {
                            try {
                                jSONArray2.put(campaignToJsonObject(campaignToJsonObject));
                            } catch (Throwable th) {
                                th.printStackTrace();
                            }
                        }
                        return jSONArray2;
                    } catch (Exception e) {
                        e = e;
                        jSONArray = jSONArray2;
                    }
                }
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return jSONArray;
            }
        }
        return jSONArray;
    }

    public static JSONObject campaignToJsonObject(CampaignEx campaignEx) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("id", campaignEx.getId());
        if (!TextUtils.isEmpty(campaignEx.getCampaignUnitId())) {
            jSONObject.put("unitId", campaignEx.getCampaignUnitId());
        }
        jSONObject.put("title", campaignEx.getAppName());
        jSONObject.put(JSON_KEY_DESC, campaignEx.getAppDesc());
        jSONObject.put(JSON_KEY_PACKAGE_NAME, campaignEx.getPackageName());
        jSONObject.put(JSON_KEY_ICON_URL, campaignEx.getIconUrl());
        jSONObject.put("image_url", campaignEx.getImageUrl());
        jSONObject.put(JSON_KEY_APP_SIZE, campaignEx.getSize());
        jSONObject.put(JSON_KEY_IMAGE_SIZE, campaignEx.getImageSize());
        jSONObject.put(JSON_KEY_IMPRESSION_URL, campaignEx.getImpressionURL());
        jSONObject.put("click_url", campaignEx.getClickURL());
        jSONObject.put(JSON_KEY_WITHOUT_INSTALL_CHECK, campaignEx.getWtick());
        jSONObject.put(JSON_KEY_DEEP_LINK_URL, campaignEx.getDeepLinkURL());
        jSONObject.put(JSON_KEY_NOTICE_URL, campaignEx.getNoticeUrl());
        jSONObject.put(JSON_KEY_PRE_CLICK, campaignEx.isPreClick());
        jSONObject.put("template", campaignEx.getTemplate());
        jSONObject.put(JSON_KEY_AD_SOURCE_ID, campaignEx.getType());
        jSONObject.put(JSON_KEY_FCA, campaignEx.getFca());
        jSONObject.put(JSON_KEY_FCB, campaignEx.getFcb());
        StringBuilder sb = new StringBuilder();
        sb.append(campaignEx.getRating());
        jSONObject.put(JSON_KEY_STAR, sb.toString());
        jSONObject.put(JSON_KEY_CLICK_MODE, campaignEx.getClick_mode());
        jSONObject.put(JSON_KEY_LANDING_TYPE, campaignEx.getLandingType());
        jSONObject.put(JSON_KEY_LINK_TYPE, campaignEx.getLinkType());
        jSONObject.put(JSON_KEY_CLICK_INTERVAL, campaignEx.getClickInterval());
        jSONObject.put(JSON_KEY_PRE_CLICK_INTERVAL, campaignEx.getPreClickInterval());
        jSONObject.put(JSON_KEY_CTA_TEXT, campaignEx.getAdCall());
        jSONObject.put(JSON_KEY_ADV_ID, campaignEx.getAdvId());
        jSONObject.put(JSON_KEY_TTC_TYPE, campaignEx.getTtc_type());
        jSONObject.put(JSON_KEY_ENDCARD_CLICK, campaignEx.getEndcard_click_result());
        jSONObject.put(JSON_KEY_TTC_CT2, campaignEx.getTtc_ct2());
        jSONObject.put(JSON_KEY_RETARGET_OFFER, campaignEx.getRetarget_offer());
        jSONObject.put("video_url", campaignEx.getVideoUrlEncode());
        jSONObject.put(JSON_KEY_VIDEO_LENGTHL, campaignEx.getVideoLength());
        jSONObject.put(JSON_KEY_VIDEO_SIZE, campaignEx.getVideoSize());
        jSONObject.put(JSON_KEY_VIDEO_RESOLUTION, campaignEx.getVideoResolution());
        jSONObject.put(JSON_KEY_WATCH_MILE, campaignEx.getWatchMile());
        jSONObject.put(JSON_KEY_AD_URL_LIST, campaignEx.getAd_url_list());
        jSONObject.put("only_impression_url", campaignEx.getOnlyImpressionURL());
        jSONObject.put(JSON_KEY_BTY, campaignEx.getBty());
        jSONObject.put(JSON_KEY_T_IMP, campaignEx.getTImp());
        jSONObject.put(JSON_KEY_ADVIMP, campaignEx.getAdvImp());
        jSONObject.put("html_url", campaignEx.getHtmlUrl());
        jSONObject.put("end_screen_url", campaignEx.getEndScreenUrl());
        jSONObject.put(JSON_KEY_GUIDELINES, campaignEx.getGuidelines());
        jSONObject.put(JSON_KEY_OFFER_TYPE, campaignEx.getOfferType());
        jSONObject.put(JSON_KEY_REWARD_AMOUNT, campaignEx.getRewardAmount());
        jSONObject.put(JSON_KEY_REWARD_NAME, campaignEx.getRewardName());
        jSONObject.put(LOOPBACK, campaignEx.getLoopbackString());
        if (s.b(campaignEx.getNativeVideoTrackingString())) {
            jSONObject.put(JSON_NATIVE_VIDEO_AD_TRACKING, new JSONObject(campaignEx.getNativeVideoTrackingString()));
        }
        jSONObject.put(VIDEO_END_TYPE, campaignEx.getVideo_end_type());
        jSONObject.put(ENDCARD_URL, campaignEx.getendcard_url());
        jSONObject.put(PLAYABLE_ADS_WITHOUT_VIDEO, campaignEx.getPlayable_ads_without_video());
        if (!(campaignEx == null || campaignEx.getRewardTemplateMode() == null || !s.b(campaignEx.getRewardTemplateMode().a()))) {
            jSONObject.put(JSON_KEY_REWARD_TEMPLATE, new JSONObject(campaignEx.getRewardTemplateMode().a()));
        }
        jSONObject.put(JSON_KEY_REWARD_VIDEO_MD5, campaignEx.getVideoMD5Value());
        jSONObject.put(JSON_KEY_CLICK_TIMEOUT_INTERVAL, campaignEx.getClickTimeOutInterval());
        String str = TAG;
        g.b(str, "camapignJsonObject:" + jSONObject.toString());
        jSONObject.put(JSON_KEY_C_UA, campaignEx.getcUA());
        jSONObject.put(JSON_KEY_IMP_UA, campaignEx.getImpUA());
        jSONObject.put(JSON_KEY_JM_PD, campaignEx.getJmPd());
        jSONObject.put("ia_icon", campaignEx.getKeyIaIcon());
        jSONObject.put("ia_rst", campaignEx.getKeyIaRst());
        jSONObject.put("ia_url", campaignEx.getKeyIaUrl());
        jSONObject.put("ia_ori", campaignEx.getKeyIaOri());
        jSONObject.put("ad_type", campaignEx.getAdType());
        jSONObject.put(KEY_IA_EXT1, campaignEx.getIa_ext1());
        jSONObject.put(KEY_IA_EXT2, campaignEx.getIa_ext2());
        jSONObject.put(KEY_IS_DOWNLOAD, campaignEx.getIsDownLoadZip());
        jSONObject.put(KEY_IA_CACHE, campaignEx.getInteractiveCache());
        jSONObject.put(KEY_GH_ID, campaignEx.getGhId());
        jSONObject.put(KEY_GH_PATH, com.mintegral.msdk.base.utils.a.b(campaignEx.getGhPath()));
        jSONObject.put(KEY_BIND_ID, campaignEx.getBindId());
        jSONObject.put(KEY_OC_TYPE, campaignEx.getOc_type());
        jSONObject.put(KEY_OC_TIME, campaignEx.getOc_time());
        jSONObject.put(KEY_T_LIST, campaignEx.getT_list());
        a adchoice2 = campaignEx.getAdchoice();
        if (adchoice2 != null) {
            jSONObject.put(KEY_ADCHOICE, new JSONObject(adchoice2.c()));
        }
        jSONObject.put(JSON_KEY_PLCT, campaignEx.getPlct());
        jSONObject.put(JSON_KEY_PLCTB, campaignEx.getPlctb());
        jSONObject.put(KEY_OMID, campaignEx.getOmid());
        jSONObject.put("banner_url", campaignEx.getBannerUrl());
        jSONObject.put("banner_html", campaignEx.getBannerHtml());
        jSONObject.put(JSON_KEY_CREATIVE_ID, campaignEx.getCreativeId());
        return jSONObject;
    }

    public String matchLoopback(String str) {
        Map<String, String> loopbackMap2;
        String replace;
        try {
            if (TextUtils.isEmpty(str) || (loopbackMap2 = getLoopbackMap()) == null || loopbackMap2.size() <= 0) {
                return str;
            }
            Uri parse = Uri.parse(str);
            String host = parse.getHost();
            String str2 = loopbackMap2.get("domain");
            if (TextUtils.isEmpty(host) || !host.contains(str2)) {
                return str;
            }
            String str3 = loopbackMap2.get("key");
            String str4 = loopbackMap2.get("value");
            if (!str.contains(str3) && TextUtils.isEmpty(parse.getQueryParameter(str3)) && !TextUtils.isEmpty(str3) && !TextUtils.isEmpty(str4)) {
                replace = str + Constants.RequestParameters.AMPERSAND + str3 + Constants.RequestParameters.EQUAL + str4;
            } else if (TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4)) {
                return str;
            } else {
                String str5 = "";
                if (!TextUtils.isEmpty(parse.getQueryParameter(str3))) {
                    str5 = parse.getQueryParameter(str3);
                }
                replace = str.replace(str3 + Constants.RequestParameters.EQUAL + str5, str3 + Constants.RequestParameters.EQUAL + str4);
            }
            return replace;
        } catch (Throwable unused) {
            g.d("", "matchLoopback error");
            return str;
        }
    }

    private static List<Map<Integer, String>> parsePlayCentage(JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        if (jSONArray != null) {
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    String string = jSONArray.getString(i);
                    if (!TextUtils.isEmpty(string)) {
                        JSONObject jSONObject = new JSONObject(string);
                        HashMap hashMap = new HashMap();
                        int i2 = jSONObject.getInt("rate");
                        hashMap.put(Integer.valueOf(i2), jSONObject.getString("url"));
                        arrayList.add(hashMap);
                    }
                    i++;
                } catch (Throwable unused) {
                    g.d("com.mintegral.msdk", "parsePlayCentage error");
                }
            }
        }
        return arrayList;
    }

    public static final class c implements Serializable {
        private String a;
        private int b;
        private int c;
        private String d;
        private String e;
        private List<a> f;

        public static final class a implements Serializable {
            public String a;
            public List<String> b = new ArrayList();
        }

        private c(String str) {
            this.a = str;
        }

        public final String a() {
            return this.a;
        }

        public final int b() {
            return this.c;
        }

        public final String c() {
            return this.d;
        }

        public final String d() {
            return this.e;
        }

        public final List<a> e() {
            return this.f;
        }

        public static c a(String str) {
            try {
                if (s.b(str)) {
                    return a(new JSONObject(str));
                }
                return null;
            } catch (Throwable th) {
                g.c(CampaignEx.TAG, th.getMessage(), th);
                return null;
            }
        }

        public static c a(JSONObject jSONObject) {
            if (jSONObject == null) {
                return null;
            }
            try {
                if (!s.b(jSONObject.toString())) {
                    return null;
                }
                c cVar = new c(jSONObject.toString());
                cVar.b = jSONObject.optInt("video_template", 1);
                cVar.e = jSONObject.optString(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_TEMPLATE_URL);
                cVar.c = jSONObject.optInt("orientation");
                cVar.d = jSONObject.optString("paused_url");
                JSONObject optJSONObject = jSONObject.optJSONObject(MessengerShareContentUtility.MEDIA_IMAGE);
                if (optJSONObject != null) {
                    ArrayList arrayList = new ArrayList();
                    Iterator<String> keys = optJSONObject.keys();
                    while (keys != null && keys.hasNext()) {
                        String next = keys.next();
                        List<String> b2 = k.b(optJSONObject.optJSONArray(next));
                        if (b2 != null && b2.size() > 0) {
                            a aVar = new a();
                            aVar.a = next;
                            aVar.b.addAll(b2);
                            arrayList.add(aVar);
                        }
                    }
                    cVar.f = arrayList;
                }
                return cVar;
            } catch (Throwable th) {
                g.c(CampaignEx.TAG, th.getMessage(), th);
                return null;
            }
        }
    }

    public boolean isHasReportAdTrackPause() {
        return this.hasReportAdTrackPause;
    }

    public void setHasReportAdTrackPause(boolean z) {
        this.hasReportAdTrackPause = z;
    }

    public static final class a implements Serializable {
        private String a = "";
        private String b = "";
        private String c = "";
        private String d = "";
        private String e = "";
        private String f = "";
        private String g = "";
        private String h = "";
        private int i = 0;
        private int j = 0;
        private String k = "";

        public final int a() {
            return this.i;
        }

        public final int b() {
            return this.j;
        }

        public final String c() {
            return this.k;
        }

        public final String d() {
            return this.b;
        }

        public final String e() {
            return this.c;
        }

        public final String f() {
            return this.d;
        }

        public static a a(String str) {
            try {
                if (TextUtils.isEmpty(str)) {
                    return null;
                }
                return a(new JSONObject(str));
            } catch (Exception e2) {
                if (MIntegralConstans.DEBUG) {
                    e2.printStackTrace();
                }
                return null;
            } catch (Throwable th) {
                if (MIntegralConstans.DEBUG) {
                    th.printStackTrace();
                }
                return null;
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:12:0x0063  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static com.mintegral.msdk.base.entity.CampaignEx.a a(org.json.JSONObject r3) {
            /*
                r0 = 0
                com.mintegral.msdk.base.entity.CampaignEx$a r1 = new com.mintegral.msdk.base.entity.CampaignEx$a     // Catch:{ Exception -> 0x0067, Throwable -> 0x005d }
                r1.<init>()     // Catch:{ Exception -> 0x0067, Throwable -> 0x005d }
                java.lang.String r0 = "adchoice_icon"
                java.lang.String r0 = r3.optString(r0)     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                r1.c = r0     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                java.lang.String r0 = "adchoice_link"
                java.lang.String r0 = r3.optString(r0)     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                r1.b = r0     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                java.lang.String r0 = "adchoice_size"
                java.lang.String r0 = r3.optString(r0)     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                r1.d = r0     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                java.lang.String r2 = "ad_logo_link"
                java.lang.String r2 = r3.optString(r2)     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                r1.a = r2     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                java.lang.String r2 = "adv_logo"
                java.lang.String r2 = r3.optString(r2)     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                r1.h = r2     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                java.lang.String r2 = "adv_name"
                java.lang.String r2 = r3.optString(r2)     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                r1.g = r2     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                java.lang.String r2 = "platform_logo"
                java.lang.String r2 = r3.optString(r2)     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                r1.f = r2     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                java.lang.String r2 = "platform_name"
                java.lang.String r2 = r3.optString(r2)     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                r1.e = r2     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                int r2 = b(r0)     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                r1.j = r2     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                int r0 = c(r0)     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                r1.i = r0     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                r1.k = r3     // Catch:{ Exception -> 0x005b, Throwable -> 0x0059 }
                goto L_0x0070
            L_0x0059:
                r3 = move-exception
                goto L_0x005f
            L_0x005b:
                r3 = move-exception
                goto L_0x0069
            L_0x005d:
                r3 = move-exception
                r1 = r0
            L_0x005f:
                boolean r0 = com.mintegral.msdk.MIntegralConstans.DEBUG
                if (r0 == 0) goto L_0x0070
                r3.printStackTrace()
                goto L_0x0070
            L_0x0067:
                r3 = move-exception
                r1 = r0
            L_0x0069:
                boolean r0 = com.mintegral.msdk.MIntegralConstans.DEBUG
                if (r0 == 0) goto L_0x0070
                r3.printStackTrace()
            L_0x0070:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.entity.CampaignEx.a.a(org.json.JSONObject):com.mintegral.msdk.base.entity.CampaignEx$a");
        }

        private static int b(String str) {
            String[] split;
            if (TextUtils.isEmpty(str)) {
                return 0;
            }
            try {
                if (!str.contains("x") || (split = str.split("x")) == null || split.length <= 1) {
                    return 0;
                }
                return Integer.parseInt(split[1]);
            } catch (NumberFormatException unused) {
                return 0;
            } catch (Exception unused2) {
                return 0;
            }
        }

        private static int c(String str) {
            String[] split;
            if (TextUtils.isEmpty(str)) {
                return 0;
            }
            try {
                if (!str.contains("x") || (split = str.split("x")) == null || split.length <= 0) {
                    return 0;
                }
                return Integer.parseInt(split[0]);
            } catch (NumberFormatException unused) {
                return 0;
            } catch (Exception unused2) {
                return 0;
            }
        }
    }

    public boolean isSpareOffer(long j, long j2) {
        if (isEffectiveOffer(j)) {
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (getPlctb() > 0) {
            if (getTimestamp() + (getPlctb() * 1000) >= currentTimeMillis) {
                return true;
            }
            return false;
        } else if (getTimestamp() + j2 >= currentTimeMillis) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isEffectiveOffer(long j) {
        long currentTimeMillis = System.currentTimeMillis();
        return getPlct() > 0 ? getTimestamp() + (getPlct() * 1000) >= currentTimeMillis : getTimestamp() + j >= currentTimeMillis;
    }

    public String getOmid() {
        return this.omid;
    }

    public void setOmid(String str) {
        this.omid = str;
    }

    public boolean isActiveOm() {
        return !TextUtils.isEmpty(this.omid) && !TextUtils.isEmpty(MIntegralConstans.OMID_JS_SERVICE_URL);
    }
}
