package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.helpshift.util.ErrorReportProvider;
import com.mintegral.msdk.out.Campaign;

/* compiled from: CDIDtimeDao */
public class b extends a<Campaign> {
    private static final String b = "com.mintegral.msdk.base.b.b";
    private static b c;

    private b(h hVar) {
        super(hVar);
    }

    public static b a(h hVar) {
        if (c == null) {
            synchronized (b.class) {
                if (c == null) {
                    c = new b(hVar);
                }
            }
        }
        return c;
    }

    public final synchronized void c() {
        try {
            String str = "d_time<" + (System.currentTimeMillis() - ErrorReportProvider.BATCH_TIME);
            if (b() != null) {
                b().delete("c_did_time", str, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final synchronized long a(String str, long j) {
        try {
            if (b() == null) {
                return -1;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("did", str);
            contentValues.put("d_time", Long.valueOf(j));
            if (b(str)) {
                return (long) b().update("c_did_time", contentValues, "did = '" + str + "'", null);
            }
            return b().insert("c_did_time", null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    private synchronized boolean b(String str) {
        Cursor rawQuery = a().rawQuery("SELECT d_time FROM c_did_time WHERE did='" + str + "'", null);
        if (rawQuery == null || rawQuery.getCount() <= 0) {
            if (rawQuery != null) {
                rawQuery.close();
            }
            return false;
        }
        rawQuery.close();
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002f, code lost:
        if (r8.getCount() > 0) goto L_0x003b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0049 A[SYNTHETIC, Splitter:B:21:0x0049] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0050 A[SYNTHETIC, Splitter:B:26:0x0050] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean a(java.lang.String r8) {
        /*
            r7 = this;
            monitor-enter(r7)
            r0 = 1
            r1 = 0
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0043 }
            r4 = 86400000(0x5265c00, double:4.2687272E-316)
            long r2 = r2 - r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0043 }
            java.lang.String r5 = "SELECT * FROM c_did_time where did = '"
            r4.<init>(r5)     // Catch:{ Exception -> 0x0043 }
            r4.append(r8)     // Catch:{ Exception -> 0x0043 }
            java.lang.String r8 = "' AND d_time > "
            r4.append(r8)     // Catch:{ Exception -> 0x0043 }
            r4.append(r2)     // Catch:{ Exception -> 0x0043 }
            java.lang.String r8 = r4.toString()     // Catch:{ Exception -> 0x0043 }
            android.database.sqlite.SQLiteDatabase r2 = r7.a()     // Catch:{ Exception -> 0x0043 }
            android.database.Cursor r8 = r2.rawQuery(r8, r1)     // Catch:{ Exception -> 0x0043 }
            if (r8 == 0) goto L_0x003a
            int r1 = r8.getCount()     // Catch:{ Exception -> 0x0035, all -> 0x0032 }
            if (r1 <= 0) goto L_0x003a
            goto L_0x003b
        L_0x0032:
            r0 = move-exception
            r1 = r8
            goto L_0x004e
        L_0x0035:
            r1 = move-exception
            r6 = r1
            r1 = r8
            r8 = r6
            goto L_0x0044
        L_0x003a:
            r0 = 0
        L_0x003b:
            if (r8 == 0) goto L_0x004c
            r8.close()     // Catch:{ all -> 0x0054 }
            goto L_0x004c
        L_0x0041:
            r0 = move-exception
            goto L_0x004e
        L_0x0043:
            r8 = move-exception
        L_0x0044:
            r8.printStackTrace()     // Catch:{ all -> 0x0041 }
            if (r1 == 0) goto L_0x004c
            r1.close()     // Catch:{ all -> 0x0054 }
        L_0x004c:
            monitor-exit(r7)
            return r0
        L_0x004e:
            if (r1 == 0) goto L_0x0056
            r1.close()     // Catch:{ all -> 0x0054 }
            goto L_0x0056
        L_0x0054:
            r8 = move-exception
            goto L_0x0057
        L_0x0056:
            throw r0     // Catch:{ all -> 0x0054 }
        L_0x0057:
            monitor-exit(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.b.a(java.lang.String):boolean");
    }
}
