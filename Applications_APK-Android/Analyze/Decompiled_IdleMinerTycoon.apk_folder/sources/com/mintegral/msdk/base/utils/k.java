package com.mintegral.msdk.base.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Looper;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import com.facebook.appevents.AppEventsConstants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.s;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.d.b;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;

/* compiled from: CommonUtil */
public final class k {
    static List<String> a = null;
    private static String b = "[一-龥]";
    private static Pattern c = Pattern.compile("[一-龥]");
    private static int d = 1;
    private static boolean e = true;

    public static <T extends String> boolean a(String str) {
        return str == null || str.length() == 0;
    }

    public static <T extends String> boolean b(String str) {
        return str != null && str.length() > 0;
    }

    public static boolean a(Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null || !"wifi".equals(activeNetworkInfo.getTypeName().toLowerCase(Locale.US))) {
                return false;
            }
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static boolean b(Context context) {
        try {
            if (((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo() != null) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static boolean a(CampaignEx campaignEx) {
        return campaignEx != null && !TextUtils.isEmpty(campaignEx.getDeepLinkURL());
    }

    public static boolean a(Context context, String str) {
        if (a != null) {
            return a.contains(str);
        }
        a = new ArrayList();
        try {
            PackageManager packageManager = context.getPackageManager();
            if (a.b == null || a.b.size() == 0) {
                a.b = new ArrayList();
                List<PackageInfo> installedPackages = packageManager.getInstalledPackages(0);
                List<String> b2 = a.d().b();
                for (int i = 0; i < installedPackages.size(); i++) {
                    PackageInfo packageInfo = installedPackages.get(i);
                    if ((packageInfo.applicationInfo.flags & 1) <= 0) {
                        a.b.add(packageInfo.packageName);
                    } else if (b2 != null && b2.size() > 0 && b2.contains(packageInfo.packageName)) {
                        a.b.add(packageInfo.packageName);
                    }
                }
            }
            for (int i2 = 0; i2 < a.b.size(); i2++) {
                a.add(a.b.get(i2));
            }
            return a.contains(str);
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
            return false;
        }
    }

    public static boolean a() {
        return e;
    }

    public static float c(Context context) {
        try {
            float f = context.getResources().getDisplayMetrics().density;
            if (f == 0.0f) {
                return 2.5f;
            }
            return f;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 2.5f;
        }
    }

    public static int a(Context context, float f) {
        float f2 = 2.5f;
        try {
            float f3 = context.getResources().getDisplayMetrics().density;
            if (f3 != 0.0f) {
                f2 = f3;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return (int) ((f / f2) + 0.5f);
    }

    public static int b(Context context, float f) {
        Resources resources;
        if (context == null || (resources = context.getResources()) == null) {
            return 0;
        }
        return (int) ((f * resources.getDisplayMetrics().density) + 0.5f);
    }

    public static int d(Context context) {
        return (int) ((context.getResources().getDisplayMetrics().scaledDensity * 30.0f) + 0.5f);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0024, code lost:
        if (r2 != null) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0031, code lost:
        if (r2 != null) goto L_0x0026;
     */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x002d A[SYNTHETIC, Splitter:B:19:0x002d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long a(java.io.File r4) throws java.lang.Exception {
        /*
            r0 = 0
            r2 = 0
            boolean r3 = r4.exists()     // Catch:{ Exception -> 0x0031, all -> 0x002a }
            if (r3 == 0) goto L_0x001a
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0031, all -> 0x002a }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0031, all -> 0x002a }
            int r4 = r3.available()     // Catch:{ Exception -> 0x0018, all -> 0x0015 }
            long r0 = (long) r4
            r2 = r3
            goto L_0x0024
        L_0x0015:
            r4 = move-exception
            r2 = r3
            goto L_0x002b
        L_0x0018:
            r2 = r3
            goto L_0x0031
        L_0x001a:
            r4.createNewFile()     // Catch:{ Exception -> 0x0031, all -> 0x002a }
            java.lang.String r4 = "获取文件大小"
            java.lang.String r3 = "文件不存在!"
            com.mintegral.msdk.base.utils.g.d(r4, r3)     // Catch:{ Exception -> 0x0031, all -> 0x002a }
        L_0x0024:
            if (r2 == 0) goto L_0x0034
        L_0x0026:
            r2.close()     // Catch:{ Exception -> 0x0034 }
            goto L_0x0034
        L_0x002a:
            r4 = move-exception
        L_0x002b:
            if (r2 == 0) goto L_0x0030
            r2.close()     // Catch:{ Exception -> 0x0030 }
        L_0x0030:
            throw r4
        L_0x0031:
            if (r2 == 0) goto L_0x0034
            goto L_0x0026
        L_0x0034:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.utils.k.a(java.io.File):long");
    }

    public static int e(Context context) {
        try {
            Class<?> cls = Class.forName("com.android.internal.R$dimen");
            return context.getResources().getDimensionPixelSize(Integer.parseInt(cls.getField("status_bar_height").get(cls.newInstance()).toString()));
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static float f(Context context) {
        return (float) context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int b() {
        int i = d;
        d = i + 1;
        return i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0025 A[Catch:{ Exception -> 0x003b }, LOOP:0: B:12:0x0023->B:13:0x0025, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(int r5) {
        /*
            java.lang.String r0 = ""
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ Exception -> 0x003b }
            r1.<init>()     // Catch:{ Exception -> 0x003b }
            com.mintegral.msdk.base.controller.a.d()     // Catch:{ Exception -> 0x003b }
            java.util.List r2 = com.mintegral.msdk.base.controller.a.g()     // Catch:{ Exception -> 0x003b }
            if (r2 == 0) goto L_0x002f
            int r3 = r2.size()     // Catch:{ Exception -> 0x003b }
            if (r3 <= 0) goto L_0x002f
            int r3 = r2.size()     // Catch:{ Exception -> 0x003b }
            if (r3 <= r5) goto L_0x0022
            if (r5 != 0) goto L_0x001f
            goto L_0x0022
        L_0x001f:
            int r5 = r3 - r5
            goto L_0x0023
        L_0x0022:
            r5 = 0
        L_0x0023:
            if (r5 >= r3) goto L_0x002f
            java.lang.Object r4 = r2.get(r5)     // Catch:{ Exception -> 0x003b }
            r1.put(r4)     // Catch:{ Exception -> 0x003b }
            int r5 = r5 + 1
            goto L_0x0023
        L_0x002f:
            int r5 = r1.length()     // Catch:{ Exception -> 0x003b }
            if (r5 <= 0) goto L_0x003f
            java.lang.String r5 = a(r1)     // Catch:{ Exception -> 0x003b }
            r0 = r5
            goto L_0x003f
        L_0x003b:
            r5 = move-exception
            r5.printStackTrace()
        L_0x003f:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.utils.k.a(int):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0058 A[Catch:{ Exception -> 0x0092 }, LOOP:0: B:14:0x0056->B:15:0x0058, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String b(int r7) {
        /*
            java.lang.String r0 = ""
            com.mintegral.msdk.d.b.a()     // Catch:{ Exception -> 0x0092 }
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Exception -> 0x0092 }
            java.lang.String r1 = r1.j()     // Catch:{ Exception -> 0x0092 }
            com.mintegral.msdk.d.a r1 = com.mintegral.msdk.d.b.b(r1)     // Catch:{ Exception -> 0x0092 }
            org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ Exception -> 0x0092 }
            r2.<init>()     // Catch:{ Exception -> 0x0092 }
            if (r1 == 0) goto L_0x0073
            int r3 = r1.aK()     // Catch:{ Exception -> 0x0092 }
            r4 = 1
            if (r3 != r4) goto L_0x0073
            java.lang.String r3 = "CommonUtils"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0092 }
            java.lang.String r5 = "excludes cfc:"
            r4.<init>(r5)     // Catch:{ Exception -> 0x0092 }
            int r1 = r1.aK()     // Catch:{ Exception -> 0x0092 }
            r4.append(r1)     // Catch:{ Exception -> 0x0092 }
            java.lang.String r1 = r4.toString()     // Catch:{ Exception -> 0x0092 }
            com.mintegral.msdk.base.utils.g.b(r3, r1)     // Catch:{ Exception -> 0x0092 }
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Exception -> 0x0092 }
            android.content.Context r1 = r1.h()     // Catch:{ Exception -> 0x0092 }
            com.mintegral.msdk.base.b.i r1 = com.mintegral.msdk.base.b.i.a(r1)     // Catch:{ Exception -> 0x0092 }
            com.mintegral.msdk.base.b.m r1 = com.mintegral.msdk.base.b.m.a(r1)     // Catch:{ Exception -> 0x0092 }
            long[] r1 = r1.c()     // Catch:{ Exception -> 0x0092 }
            if (r1 == 0) goto L_0x0073
            int r3 = r1.length     // Catch:{ Exception -> 0x0092 }
            if (r3 <= r7) goto L_0x0055
            if (r7 != 0) goto L_0x0052
            goto L_0x0055
        L_0x0052:
            int r7 = r3 - r7
            goto L_0x0056
        L_0x0055:
            r7 = 0
        L_0x0056:
            if (r7 >= r3) goto L_0x0073
            java.lang.String r4 = "CommonUtils"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0092 }
            java.lang.String r6 = "excludes campaignIds:"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0092 }
            r5.append(r1)     // Catch:{ Exception -> 0x0092 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0092 }
            com.mintegral.msdk.base.utils.g.b(r4, r5)     // Catch:{ Exception -> 0x0092 }
            r4 = r1[r7]     // Catch:{ Exception -> 0x0092 }
            r2.put(r4)     // Catch:{ Exception -> 0x0092 }
            int r7 = r7 + 1
            goto L_0x0056
        L_0x0073:
            int r7 = r2.length()     // Catch:{ Exception -> 0x0092 }
            if (r7 <= 0) goto L_0x007e
            java.lang.String r7 = a(r2)     // Catch:{ Exception -> 0x0092 }
            r0 = r7
        L_0x007e:
            java.lang.String r7 = "CommonUtils"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0092 }
            java.lang.String r2 = "get excludes:"
            r1.<init>(r2)     // Catch:{ Exception -> 0x0092 }
            r1.append(r0)     // Catch:{ Exception -> 0x0092 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0092 }
            com.mintegral.msdk.base.utils.g.b(r7, r1)     // Catch:{ Exception -> 0x0092 }
            goto L_0x0096
        L_0x0092:
            r7 = move-exception
            r7.printStackTrace()
        L_0x0096:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.utils.k.b(int):java.lang.String");
    }

    public static String a(JSONArray jSONArray) {
        String j = a.d().j();
        b.a();
        com.mintegral.msdk.d.a b2 = b.b(j);
        if (b2 == null) {
            b.a();
            b2 = b.b();
        }
        int ae = b2.ae();
        if (jSONArray.length() <= ae) {
            return jSONArray.toString();
        }
        JSONArray jSONArray2 = new JSONArray();
        for (int i = 0; i < ae; i++) {
            try {
                jSONArray2.put(jSONArray.get(i));
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return jSONArray2.toString();
    }

    public static boolean a(String str, Context context) {
        try {
            if (context.getPackageManager().checkPermission(str, context.getPackageName()) == 0) {
                g.a("CommonUtils", "Permission " + str + " is granted");
                return true;
            }
            g.a("CommonUtils", "Permission " + str + " is NOT granted");
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    public static boolean b(CampaignEx campaignEx) {
        if (campaignEx != null) {
            try {
                return campaignEx.getRetarget_offer() == 1;
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return false;
    }

    public static void a(List<CampaignEx> list) {
        if (list != null && list.size() != 0) {
            ArrayList arrayList = new ArrayList();
            int size = list.size();
            for (int i = 0; i < size; i++) {
                CampaignEx campaignEx = list.get(i);
                if ((!a(a.d().h(), campaignEx.getPackageName()) && campaignEx.getOfferType() == 99) || a(campaignEx)) {
                    arrayList.add(campaignEx);
                }
            }
            s.a(i.a(a.d().h())).a(arrayList);
        }
    }

    public static int g(Context context) {
        if (context == null) {
            return -1;
        }
        try {
            List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
            if (installedPackages == null || installedPackages.size() <= 0) {
                return -1;
            }
            return installedPackages.size();
        } catch (Exception e2) {
            e2.printStackTrace();
            return -1;
        }
    }

    public static double c(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                return Double.parseDouble(str);
            }
            return 0.0d;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0.0d;
        }
    }

    public static int a(Object obj) {
        if (obj == null) {
            return 0;
        }
        try {
            if (obj instanceof String) {
                return Integer.parseInt((String) obj);
            }
            return 0;
        } catch (Throwable th) {
            g.c("CommonUtils", th.getMessage(), th);
            return 0;
        }
    }

    public static double a(Double d2) {
        try {
            g.b("CommonUtils", "format before num:" + d2);
            String format = new DecimalFormat("0.00", DecimalFormatSymbols.getInstance(Locale.US)).format(d2);
            g.b("CommonUtils", "format after format:" + format);
            if (s.b(format)) {
                return Double.parseDouble(format);
            }
            return 0.0d;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0.0d;
        }
    }

    private static DisplayMetrics o(Context context) {
        if (context == null) {
            return null;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        try {
            Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            Class.forName("android.view.Display").getMethod("getRealMetrics", DisplayMetrics.class).invoke(defaultDisplay, displayMetrics);
            return displayMetrics;
        } catch (Throwable th) {
            th.printStackTrace();
            return context.getResources().getDisplayMetrics();
        }
    }

    public static int h(Context context) {
        if (context == null) {
            return 0;
        }
        try {
            return o(context).heightPixels;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static int i(Context context) {
        if (context == null) {
            return 0;
        }
        try {
            return o(context).widthPixels;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static int j(Context context) {
        if (context == null) {
            return 0;
        }
        try {
            return context.getResources().getDisplayMetrics().widthPixels;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static int k(Context context) {
        if (context == null) {
            return 0;
        }
        try {
            return context.getResources().getDisplayMetrics().heightPixels;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static void a(View view) {
        if (view != null) {
            try {
                if (Build.VERSION.SDK_INT >= 11) {
                    view.setSystemUiVisibility(4102);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static int l(Context context) {
        try {
            if (context.getResources().getIdentifier("config_showNavigationBar", "bool", "android") == 0) {
                return 0;
            }
            return context.getResources().getDimensionPixelSize(context.getResources().getIdentifier("navigation_bar_height", "dimen", "android"));
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static boolean m(Context context) {
        boolean z;
        Throwable th;
        try {
            Resources resources = context.getResources();
            int identifier = resources.getIdentifier("config_showNavigationBar", "bool", "android");
            z = identifier > 0 ? resources.getBoolean(identifier) : false;
            try {
                Class<?> cls = Class.forName("android.os.SystemProperties");
                String str = (String) cls.getMethod("get", String.class).invoke(cls, "qemu.hw.mainkeys");
                if ("1".equals(str)) {
                    return false;
                }
                if (AppEventsConstants.EVENT_PARAM_VALUE_NO.equals(str)) {
                    return true;
                }
                return z;
            } catch (Throwable th2) {
                th = th2;
                g.c("CommonUtils", th.getMessage(), th);
                return z;
            }
        } catch (Throwable th3) {
            th = th3;
            z = false;
            g.c("CommonUtils", th.getMessage(), th);
            return z;
        }
    }

    public static void a(ImageView imageView) {
        if (imageView != null) {
            try {
                imageView.setImageResource(0);
                imageView.setImageDrawable(null);
                imageView.setImageURI(null);
                imageView.setImageBitmap(null);
            } catch (Throwable th) {
                if (MIntegralConstans.DEBUG) {
                    th.printStackTrace();
                }
            }
        }
    }

    public static List<String> b(JSONArray jSONArray) {
        if (jSONArray == null) {
            return null;
        }
        try {
            if (jSONArray.length() <= 0) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < jSONArray.length(); i++) {
                String optString = jSONArray.optString(i);
                if (s.b(optString)) {
                    arrayList.add(optString);
                }
            }
            return arrayList;
        } catch (Throwable th) {
            g.c("CommonUtils", th.getMessage(), th);
            return null;
        }
    }

    public static String d(String str) {
        try {
            if (s.b(str)) {
                return URLEncoder.encode(str, "utf-8");
            }
            return "";
        } catch (Throwable th) {
            g.c("CommonUtils", th.getMessage(), th);
            return "";
        }
    }

    public static boolean c() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    public static boolean n(Context context) {
        try {
            return ((PowerManager) context.getSystemService("power")).isScreenOn();
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0117, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x011c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x011d, code lost:
        com.mintegral.msdk.base.utils.g.c("CommonUtils", r1.getMessage(), r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0126, code lost:
        return null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:18:0x0069, B:56:0x0113] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x007a A[SYNTHETIC, Splitter:B:28:0x007a] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a5 A[Catch:{ Exception -> 0x0117, Throwable -> 0x011c }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00dc A[Catch:{ Exception -> 0x0117, Throwable -> 0x011c }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0113 A[SYNTHETIC, Splitter:B:56:0x0113] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<com.mintegral.msdk.base.entity.m> d() {
        /*
            r0 = 0
            java.lang.String r1 = "CommonUtils"
            java.lang.String r2 = "============getActivePbDatas strat"
            com.mintegral.msdk.base.utils.g.b(r1, r2)     // Catch:{ Throwable -> 0x011c }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Throwable -> 0x011c }
            r1.<init>()     // Catch:{ Throwable -> 0x011c }
            java.lang.Runtime r2 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x0073, all -> 0x006f }
            java.lang.String r3 = "ps"
            java.lang.Process r2 = r2.exec(r3)     // Catch:{ Exception -> 0x0073, all -> 0x006f }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0073, all -> 0x006f }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0073, all -> 0x006f }
            java.io.InputStream r2 = r2.getInputStream()     // Catch:{ Exception -> 0x0073, all -> 0x006f }
            r4.<init>(r2)     // Catch:{ Exception -> 0x0073, all -> 0x006f }
            r2 = 1024(0x400, float:1.435E-42)
            r3.<init>(r4, r2)     // Catch:{ Exception -> 0x0073, all -> 0x006f }
        L_0x0027:
            java.lang.String r2 = r3.readLine()     // Catch:{ Exception -> 0x006d }
            if (r2 == 0) goto L_0x0064
            java.lang.String r4 = "u0_"
            boolean r4 = r2.startsWith(r4)     // Catch:{ Exception -> 0x006d }
            if (r4 == 0) goto L_0x0027
            java.lang.String r4 = " "
            java.lang.String[] r4 = r2.split(r4)     // Catch:{ Exception -> 0x006d }
            if (r4 == 0) goto L_0x0027
            int r5 = r4.length     // Catch:{ Exception -> 0x006d }
            if (r5 <= 0) goto L_0x0027
            int r5 = r4.length     // Catch:{ Exception -> 0x006d }
            int r5 = r5 + -1
            r4 = r4[r5]     // Catch:{ Exception -> 0x006d }
            r1.add(r4)     // Catch:{ Exception -> 0x006d }
            java.lang.String r5 = "CommonUtils"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006d }
            java.lang.String r7 = "line:"
            r6.<init>(r7)     // Catch:{ Exception -> 0x006d }
            r6.append(r2)     // Catch:{ Exception -> 0x006d }
            java.lang.String r2 = "  =====pg:"
            r6.append(r2)     // Catch:{ Exception -> 0x006d }
            r6.append(r4)     // Catch:{ Exception -> 0x006d }
            java.lang.String r2 = r6.toString()     // Catch:{ Exception -> 0x006d }
            com.mintegral.msdk.base.utils.g.d(r5, r2)     // Catch:{ Exception -> 0x006d }
            goto L_0x0027
        L_0x0064:
            r3.close()     // Catch:{ Exception -> 0x0068 }
            goto L_0x0080
        L_0x0068:
            r2 = move-exception
        L_0x0069:
            r2.printStackTrace()     // Catch:{ Throwable -> 0x011c }
            goto L_0x0080
        L_0x006d:
            r2 = move-exception
            goto L_0x0075
        L_0x006f:
            r1 = move-exception
            r3 = r0
            goto L_0x0111
        L_0x0073:
            r2 = move-exception
            r3 = r0
        L_0x0075:
            r2.printStackTrace()     // Catch:{ all -> 0x0110 }
            if (r3 == 0) goto L_0x0080
            r3.close()     // Catch:{ Exception -> 0x007e }
            goto L_0x0080
        L_0x007e:
            r2 = move-exception
            goto L_0x0069
        L_0x0080:
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Throwable -> 0x011c }
            r2.<init>()     // Catch:{ Throwable -> 0x011c }
            com.mintegral.msdk.base.controller.a r3 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x011c }
            android.content.Context r3 = r3.h()     // Catch:{ Throwable -> 0x011c }
            android.content.pm.PackageManager r3 = r3.getPackageManager()     // Catch:{ Throwable -> 0x011c }
            r4 = 0
            java.util.List r3 = r3.getInstalledPackages(r4)     // Catch:{ Throwable -> 0x011c }
            com.mintegral.msdk.base.controller.a r5 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x011c }
            java.util.List r5 = r5.b()     // Catch:{ Throwable -> 0x011c }
            r6 = 0
        L_0x009f:
            int r7 = r3.size()     // Catch:{ Throwable -> 0x011c }
            if (r6 >= r7) goto L_0x00d1
            java.lang.Object r7 = r3.get(r6)     // Catch:{ Throwable -> 0x011c }
            android.content.pm.PackageInfo r7 = (android.content.pm.PackageInfo) r7     // Catch:{ Throwable -> 0x011c }
            android.content.pm.ApplicationInfo r8 = r7.applicationInfo     // Catch:{ Throwable -> 0x011c }
            int r8 = r8.flags     // Catch:{ Throwable -> 0x011c }
            r8 = r8 & 1
            if (r8 > 0) goto L_0x00b9
            java.lang.String r7 = r7.packageName     // Catch:{ Throwable -> 0x011c }
            r2.add(r7)     // Catch:{ Throwable -> 0x011c }
            goto L_0x00ce
        L_0x00b9:
            if (r5 == 0) goto L_0x00ce
            int r8 = r5.size()     // Catch:{ Throwable -> 0x011c }
            if (r8 <= 0) goto L_0x00ce
            java.lang.String r8 = r7.packageName     // Catch:{ Throwable -> 0x011c }
            boolean r8 = r5.contains(r8)     // Catch:{ Throwable -> 0x011c }
            if (r8 == 0) goto L_0x00ce
            java.lang.String r7 = r7.packageName     // Catch:{ Throwable -> 0x011c }
            r2.add(r7)     // Catch:{ Throwable -> 0x011c }
        L_0x00ce:
            int r6 = r6 + 1
            goto L_0x009f
        L_0x00d1:
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Throwable -> 0x011c }
            r3.<init>()     // Catch:{ Throwable -> 0x011c }
        L_0x00d6:
            int r5 = r1.size()     // Catch:{ Throwable -> 0x011c }
            if (r4 >= r5) goto L_0x0108
            java.lang.Object r5 = r1.get(r4)     // Catch:{ Throwable -> 0x011c }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ Throwable -> 0x011c }
            boolean r6 = r2.contains(r5)     // Catch:{ Throwable -> 0x011c }
            if (r6 == 0) goto L_0x0105
            java.lang.String r6 = "CommonUtils"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x011c }
            java.lang.String r8 = "后台活跃包名 pg:"
            r7.<init>(r8)     // Catch:{ Throwable -> 0x011c }
            r7.append(r5)     // Catch:{ Throwable -> 0x011c }
            java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x011c }
            com.mintegral.msdk.base.utils.g.b(r6, r7)     // Catch:{ Throwable -> 0x011c }
            com.mintegral.msdk.base.entity.m r6 = new com.mintegral.msdk.base.entity.m     // Catch:{ Throwable -> 0x011c }
            r6.<init>()     // Catch:{ Throwable -> 0x011c }
            r6.a = r5     // Catch:{ Throwable -> 0x011c }
            r3.add(r6)     // Catch:{ Throwable -> 0x011c }
        L_0x0105:
            int r4 = r4 + 1
            goto L_0x00d6
        L_0x0108:
            java.lang.String r1 = "CommonUtils"
            java.lang.String r2 = "=============getActivePbDatas end"
            com.mintegral.msdk.base.utils.g.b(r1, r2)     // Catch:{ Throwable -> 0x011c }
            return r3
        L_0x0110:
            r1 = move-exception
        L_0x0111:
            if (r3 == 0) goto L_0x011b
            r3.close()     // Catch:{ Exception -> 0x0117 }
            goto L_0x011b
        L_0x0117:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Throwable -> 0x011c }
        L_0x011b:
            throw r1     // Catch:{ Throwable -> 0x011c }
        L_0x011c:
            r1 = move-exception
            java.lang.String r2 = "CommonUtils"
            java.lang.String r3 = r1.getMessage()
            com.mintegral.msdk.base.utils.g.c(r2, r3, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.utils.k.d():java.util.List");
    }

    public static String e() {
        String str = "";
        try {
            str = UUID.randomUUID().toString() + System.currentTimeMillis();
        } catch (Throwable th) {
            th.printStackTrace();
        }
        if (!s.a(str)) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(System.currentTimeMillis());
        return sb.toString();
    }

    public static String b(List<String> list) {
        if (list == null) {
            return "[]";
        }
        try {
            if (list.size() == 0) {
                return "[]";
            }
            JSONArray jSONArray = new JSONArray();
            for (String next : list) {
                if (s.b(next)) {
                    jSONArray.put(next);
                }
            }
            return jSONArray.toString();
        } catch (Throwable th) {
            g.c("CommonUtils", th.getMessage(), th);
            return "[]";
        }
    }
}
