package com.mintegral.msdk.base.b;

import com.mintegral.msdk.out.Campaign;

/* compiled from: DailyPlayCapDao */
public class j extends a<Campaign> {
    private static final String b = "com.mintegral.msdk.base.b.j";
    private static j c;

    private j(h hVar) {
        super(hVar);
    }

    public static j a(h hVar) {
        if (c == null) {
            synchronized (j.class) {
                if (c == null) {
                    c = new j(hVar);
                }
            }
        }
        return c;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        if (r2.getCount() <= 0) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        if (r2.moveToFirst() == false) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002d, code lost:
        r3 = r2.getLong(r2.getColumnIndex("first_insert_timestamp"));
        r5 = (long) r2.getInt(r2.getColumnIndex("play_time"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0046, code lost:
        if (r3 == 0) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0052, code lost:
        if ((java.lang.System.currentTimeMillis() - com.helpshift.util.ErrorReportProvider.BATCH_TIME) <= r3) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0054, code lost:
        b(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0058, code lost:
        if (r13 <= 0) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005d, code lost:
        if (r5 < ((long) r13)) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005f, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0062, code lost:
        r12 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0063, code lost:
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0065, code lost:
        r12 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0066, code lost:
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0068, code lost:
        if (r2 == null) goto L_0x008f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0074, code lost:
        r12 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        com.mintegral.msdk.base.utils.g.b(com.mintegral.msdk.base.b.j.b, "isOverCap is error" + r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x008a, code lost:
        if (r0 != null) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x008f, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0090, code lost:
        if (r0 != null) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0095, code lost:
        throw r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001f, code lost:
        if (r2 == null) goto L_0x0068;
     */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0092 A[SYNTHETIC, Splitter:B:47:0x0092] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.lang.String r12, int r13) {
        /*
            r11 = this;
            r0 = 0
            r1 = 0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0076 }
            java.lang.String r3 = "SELECT * FROM dailyplaycap where unit_id ='"
            r2.<init>(r3)     // Catch:{ Throwable -> 0x0076 }
            r2.append(r12)     // Catch:{ Throwable -> 0x0076 }
            java.lang.String r3 = "'"
            r2.append(r3)     // Catch:{ Throwable -> 0x0076 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0076 }
            monitor-enter(r11)     // Catch:{ Throwable -> 0x0076 }
            android.database.sqlite.SQLiteDatabase r3 = r11.a()     // Catch:{ all -> 0x0071 }
            android.database.Cursor r2 = r3.rawQuery(r2, r0)     // Catch:{ all -> 0x0071 }
            monitor-exit(r11)     // Catch:{ all -> 0x006e }
            if (r2 == 0) goto L_0x0068
            int r0 = r2.getCount()     // Catch:{ Throwable -> 0x0065, all -> 0x0062 }
            if (r0 <= 0) goto L_0x0068
            boolean r0 = r2.moveToFirst()     // Catch:{ Throwable -> 0x0065, all -> 0x0062 }
            if (r0 == 0) goto L_0x0068
            java.lang.String r0 = "first_insert_timestamp"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ Throwable -> 0x0065, all -> 0x0062 }
            long r3 = r2.getLong(r0)     // Catch:{ Throwable -> 0x0065, all -> 0x0062 }
            java.lang.String r0 = "play_time"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ Throwable -> 0x0065, all -> 0x0062 }
            int r0 = r2.getInt(r0)     // Catch:{ Throwable -> 0x0065, all -> 0x0062 }
            long r5 = (long) r0     // Catch:{ Throwable -> 0x0065, all -> 0x0062 }
            r7 = 0
            int r0 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r0 == 0) goto L_0x0068
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0065, all -> 0x0062 }
            r9 = 86400000(0x5265c00, double:4.2687272E-316)
            long r7 = r7 - r9
            int r0 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x0058
            r11.b(r12)     // Catch:{ Throwable -> 0x0065, all -> 0x0062 }
            goto L_0x0068
        L_0x0058:
            if (r13 <= 0) goto L_0x0068
            long r12 = (long) r13
            int r0 = (r5 > r12 ? 1 : (r5 == r12 ? 0 : -1))
            if (r0 < 0) goto L_0x0068
            r12 = 1
            r1 = 1
            goto L_0x0068
        L_0x0062:
            r12 = move-exception
            r0 = r2
            goto L_0x0090
        L_0x0065:
            r12 = move-exception
            r0 = r2
            goto L_0x0077
        L_0x0068:
            if (r2 == 0) goto L_0x008f
            r2.close()     // Catch:{ Throwable -> 0x008f }
            goto L_0x008f
        L_0x006e:
            r12 = move-exception
            r0 = r2
            goto L_0x0072
        L_0x0071:
            r12 = move-exception
        L_0x0072:
            monitor-exit(r11)     // Catch:{ all -> 0x0071 }
            throw r12     // Catch:{ Throwable -> 0x0076 }
        L_0x0074:
            r12 = move-exception
            goto L_0x0090
        L_0x0076:
            r12 = move-exception
        L_0x0077:
            java.lang.String r13 = com.mintegral.msdk.base.b.j.b     // Catch:{ all -> 0x0074 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0074 }
            java.lang.String r3 = "isOverCap is error"
            r2.<init>(r3)     // Catch:{ all -> 0x0074 }
            r2.append(r12)     // Catch:{ all -> 0x0074 }
            java.lang.String r12 = r2.toString()     // Catch:{ all -> 0x0074 }
            com.mintegral.msdk.base.utils.g.b(r13, r12)     // Catch:{ all -> 0x0074 }
            if (r0 == 0) goto L_0x008f
            r0.close()     // Catch:{ Throwable -> 0x008f }
        L_0x008f:
            return r1
        L_0x0090:
            if (r0 == 0) goto L_0x0095
            r0.close()     // Catch:{ Throwable -> 0x0095 }
        L_0x0095:
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.j.a(java.lang.String, int):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        com.mintegral.msdk.base.utils.g.b(com.mintegral.msdk.base.b.j.b, "resetTimeAndTimestamp error");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004a, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0042 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void b(java.lang.String r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            android.database.sqlite.SQLiteDatabase r0 = r4.b()     // Catch:{ Throwable -> 0x0042 }
            if (r0 != 0) goto L_0x0009
            monitor-exit(r4)
            return
        L_0x0009:
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ Throwable -> 0x0042 }
            r0.<init>()     // Catch:{ Throwable -> 0x0042 }
            java.lang.String r1 = "first_insert_timestamp"
            r2 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)     // Catch:{ Throwable -> 0x0042 }
            r0.put(r1, r3)     // Catch:{ Throwable -> 0x0042 }
            java.lang.String r1 = "play_time"
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Throwable -> 0x0042 }
            r0.put(r1, r2)     // Catch:{ Throwable -> 0x0042 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0042 }
            java.lang.String r2 = "unit_id = '"
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0042 }
            r1.append(r5)     // Catch:{ Throwable -> 0x0042 }
            java.lang.String r5 = "'"
            r1.append(r5)     // Catch:{ Throwable -> 0x0042 }
            java.lang.String r5 = r1.toString()     // Catch:{ Throwable -> 0x0042 }
            android.database.sqlite.SQLiteDatabase r1 = r4.b()     // Catch:{ Throwable -> 0x0042 }
            java.lang.String r2 = "dailyplaycap"
            r3 = 0
            r1.update(r2, r0, r5, r3)     // Catch:{ Throwable -> 0x0042 }
            monitor-exit(r4)
            return
        L_0x0040:
            r5 = move-exception
            goto L_0x004b
        L_0x0042:
            java.lang.String r5 = com.mintegral.msdk.base.b.j.b     // Catch:{ all -> 0x0040 }
            java.lang.String r0 = "resetTimeAndTimestamp error"
            com.mintegral.msdk.base.utils.g.b(r5, r0)     // Catch:{ all -> 0x0040 }
            monitor-exit(r4)
            return
        L_0x004b:
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.j.b(java.lang.String):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
     arg types: [java.lang.String, long]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void} */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:59:0x00e9 */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0075 A[Catch:{ Exception -> 0x006c, all -> 0x0068 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00d8 A[SYNTHETIC, Splitter:B:46:0x00d8] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00e3 A[SYNTHETIC, Splitter:B:56:0x00e3] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.lang.String r18) {
        /*
            r17 = this;
            r0 = r18
            monitor-enter(r17)
            r1 = 0
            android.database.sqlite.SQLiteDatabase r2 = r17.b()     // Catch:{ Exception -> 0x00d2 }
            if (r2 != 0) goto L_0x000c
            monitor-exit(r17)
            return
        L_0x000c:
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ Exception -> 0x00d2 }
            r2.<init>()     // Catch:{ Exception -> 0x00d2 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00d2 }
            boolean r5 = r17.c(r18)     // Catch:{ Exception -> 0x00d2 }
            r6 = 1
            if (r5 == 0) goto L_0x00a7
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d2 }
            java.lang.String r8 = "SELECT * FROM dailyplaycap where unit_id ='"
            r5.<init>(r8)     // Catch:{ Exception -> 0x00d2 }
            r5.append(r0)     // Catch:{ Exception -> 0x00d2 }
            java.lang.String r8 = "'"
            r5.append(r8)     // Catch:{ Exception -> 0x00d2 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x00d2 }
            android.database.sqlite.SQLiteDatabase r8 = r17.a()     // Catch:{ Exception -> 0x00d2 }
            android.database.Cursor r5 = r8.rawQuery(r5, r1)     // Catch:{ Exception -> 0x00d2 }
            r8 = 0
            if (r5 == 0) goto L_0x006f
            int r10 = r5.getCount()     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            if (r10 <= 0) goto L_0x006f
            r5.moveToFirst()     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            java.lang.String r10 = "first_insert_timestamp"
            int r10 = r5.getColumnIndex(r10)     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            long r10 = r5.getLong(r10)     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            java.lang.String r12 = "play_time"
            int r12 = r5.getColumnIndex(r12)     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            int r12 = r5.getInt(r12)     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            long r12 = (long) r12     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            r14 = 86400000(0x5265c00, double:4.2687272E-316)
            long r14 = r3 - r14
            int r16 = (r14 > r10 ? 1 : (r14 == r10 ? 0 : -1))
            if (r16 <= 0) goto L_0x0071
            r17.b(r18)     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            r12 = r8
            goto L_0x0071
        L_0x0068:
            r0 = move-exception
            r1 = r5
            goto L_0x00e1
        L_0x006c:
            r0 = move-exception
            r1 = r5
            goto L_0x00d3
        L_0x006f:
            r10 = r8
            r12 = r10
        L_0x0071:
            int r14 = (r10 > r8 ? 1 : (r10 == r8 ? 0 : -1))
            if (r14 != 0) goto L_0x007e
            java.lang.String r8 = "first_insert_timestamp"
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            r2.put(r8, r3)     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
        L_0x007e:
            java.lang.String r3 = "play_time"
            r4 = 0
            long r12 = r12 + r6
            java.lang.Long r4 = java.lang.Long.valueOf(r12)     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            java.lang.String r4 = "unit_id = '"
            r3.<init>(r4)     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            r3.append(r0)     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            java.lang.String r0 = "'"
            r3.append(r0)     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            android.database.sqlite.SQLiteDatabase r3 = r17.b()     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            java.lang.String r4 = "dailyplaycap"
            r3.update(r4, r2, r0, r1)     // Catch:{ Exception -> 0x006c, all -> 0x0068 }
            r1 = r5
            goto L_0x00c7
        L_0x00a7:
            java.lang.String r5 = "first_insert_timestamp"
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x00d2 }
            r2.put(r5, r3)     // Catch:{ Exception -> 0x00d2 }
            java.lang.String r3 = "play_time"
            java.lang.Long r4 = java.lang.Long.valueOf(r6)     // Catch:{ Exception -> 0x00d2 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x00d2 }
            java.lang.String r3 = "unit_id"
            r2.put(r3, r0)     // Catch:{ Exception -> 0x00d2 }
            android.database.sqlite.SQLiteDatabase r0 = r17.b()     // Catch:{ Exception -> 0x00d2 }
            java.lang.String r3 = "dailyplaycap"
            r0.insert(r3, r1, r2)     // Catch:{ Exception -> 0x00d2 }
        L_0x00c7:
            if (r1 == 0) goto L_0x00df
            r1.close()     // Catch:{ Throwable -> 0x00ce }
            monitor-exit(r17)
            return
        L_0x00ce:
            monitor-exit(r17)
            return
        L_0x00d0:
            r0 = move-exception
            goto L_0x00e1
        L_0x00d2:
            r0 = move-exception
        L_0x00d3:
            r0.printStackTrace()     // Catch:{ all -> 0x00d0 }
            if (r1 == 0) goto L_0x00df
            r1.close()     // Catch:{ Throwable -> 0x00dd }
            monitor-exit(r17)
            return
        L_0x00dd:
            monitor-exit(r17)
            return
        L_0x00df:
            monitor-exit(r17)
            return
        L_0x00e1:
            if (r1 == 0) goto L_0x00e9
            r1.close()     // Catch:{ Throwable -> 0x00e9 }
            goto L_0x00e9
        L_0x00e7:
            r0 = move-exception
            goto L_0x00ea
        L_0x00e9:
            throw r0     // Catch:{ all -> 0x00e7 }
        L_0x00ea:
            monitor-exit(r17)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.j.a(java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0032, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0037, code lost:
        return false;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean c(java.lang.String r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0036, all -> 0x0033 }
            java.lang.String r2 = "SELECT play_time FROM dailyplaycap WHERE unit_id='"
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0036, all -> 0x0033 }
            r1.append(r4)     // Catch:{ Throwable -> 0x0036, all -> 0x0033 }
            java.lang.String r4 = "'"
            r1.append(r4)     // Catch:{ Throwable -> 0x0036, all -> 0x0033 }
            java.lang.String r4 = r1.toString()     // Catch:{ Throwable -> 0x0036, all -> 0x0033 }
            android.database.sqlite.SQLiteDatabase r1 = r3.a()     // Catch:{ Throwable -> 0x0036, all -> 0x0033 }
            r2 = 0
            android.database.Cursor r4 = r1.rawQuery(r4, r2)     // Catch:{ Throwable -> 0x0036, all -> 0x0033 }
            if (r4 == 0) goto L_0x002c
            int r1 = r4.getCount()     // Catch:{ Throwable -> 0x0036, all -> 0x0033 }
            if (r1 <= 0) goto L_0x002c
            r4.close()     // Catch:{ Throwable -> 0x0036, all -> 0x0033 }
            r4 = 1
            monitor-exit(r3)
            return r4
        L_0x002c:
            if (r4 == 0) goto L_0x0031
            r4.close()     // Catch:{ Throwable -> 0x0036, all -> 0x0033 }
        L_0x0031:
            monitor-exit(r3)
            return r0
        L_0x0033:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        L_0x0036:
            monitor-exit(r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.j.c(java.lang.String):boolean");
    }
}
