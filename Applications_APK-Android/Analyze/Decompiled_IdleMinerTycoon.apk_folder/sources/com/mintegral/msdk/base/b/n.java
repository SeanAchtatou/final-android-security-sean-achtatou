package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import com.ironsource.sdk.constants.LocationConst;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.i;

/* compiled from: LoadTimeDao */
public class n extends a<i> {
    private static n b;

    private n(h hVar) {
        super(hVar);
    }

    public static n a(h hVar) {
        if (b == null) {
            synchronized (n.class) {
                if (b == null) {
                    b = new n(hVar);
                }
            }
        }
        return b;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0032 A[SYNTHETIC, Splitter:B:22:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0039 A[SYNTHETIC, Splitter:B:27:0x0039] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int c() {
        /*
            r5 = this;
            monitor-enter(r5)
            r0 = 0
            r1 = 0
            java.lang.String r2 = "select count(*) from load_stat"
            android.database.sqlite.SQLiteDatabase r3 = r5.a()     // Catch:{ Exception -> 0x002c }
            android.database.Cursor r2 = r3.rawQuery(r2, r0)     // Catch:{ Exception -> 0x002c }
            if (r2 == 0) goto L_0x0024
            boolean r0 = r2.moveToFirst()     // Catch:{ Exception -> 0x001f, all -> 0x001b }
            if (r0 == 0) goto L_0x0024
            int r0 = r2.getInt(r1)     // Catch:{ Exception -> 0x001f, all -> 0x001b }
            r1 = r0
            goto L_0x0024
        L_0x001b:
            r0 = move-exception
            r1 = r0
            r0 = r2
            goto L_0x0037
        L_0x001f:
            r0 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x002d
        L_0x0024:
            if (r2 == 0) goto L_0x0035
            r2.close()     // Catch:{ all -> 0x003d }
            goto L_0x0035
        L_0x002a:
            r1 = move-exception
            goto L_0x0037
        L_0x002c:
            r2 = move-exception
        L_0x002d:
            r2.printStackTrace()     // Catch:{ all -> 0x002a }
            if (r0 == 0) goto L_0x0035
            r0.close()     // Catch:{ all -> 0x003d }
        L_0x0035:
            monitor-exit(r5)
            return r1
        L_0x0037:
            if (r0 == 0) goto L_0x003f
            r0.close()     // Catch:{ all -> 0x003d }
            goto L_0x003f
        L_0x003d:
            r0 = move-exception
            goto L_0x0040
        L_0x003f:
            throw r1     // Catch:{ all -> 0x003d }
        L_0x0040:
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.n.c():int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c6 A[SYNTHETIC, Splitter:B:32:0x00c6] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00cf A[SYNTHETIC, Splitter:B:39:0x00cf] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.mintegral.msdk.base.entity.i> d() {
        /*
            r15 = this;
            monitor-enter(r15)
            java.lang.String r0 = "select * from load_stat LIMIT 20"
            r1 = 0
            android.database.sqlite.SQLiteDatabase r2 = r15.a()     // Catch:{ Exception -> 0x00bd, all -> 0x00b8 }
            android.database.Cursor r0 = r2.rawQuery(r0, r1)     // Catch:{ Exception -> 0x00bd, all -> 0x00b8 }
            if (r0 == 0) goto L_0x00b2
            int r2 = r0.getCount()     // Catch:{ Exception -> 0x00ad }
            if (r2 <= 0) goto L_0x00b2
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Exception -> 0x00ad }
            r2.<init>()     // Catch:{ Exception -> 0x00ad }
            r1 = 0
            r3 = 0
        L_0x001b:
            boolean r4 = r0.moveToNext()     // Catch:{ Exception -> 0x00ab }
            if (r4 == 0) goto L_0x00a9
            r4 = 20
            if (r3 >= r4) goto L_0x00a9
            int r3 = r3 + 1
            java.lang.String r4 = "ad_source_id"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ab }
            int r6 = r0.getInt(r4)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r4 = "time"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r7 = r0.getString(r4)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r4 = "adNum"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ab }
            int r8 = r0.getInt(r4)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r4 = "unitId"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r9 = r0.getString(r4)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r4 = "fb"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ab }
            int r10 = r0.getInt(r4)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r4 = "timeout"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ab }
            int r11 = r0.getInt(r4)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r4 = "network_type"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ab }
            int r12 = r0.getInt(r4)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r4 = "hb"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ab }
            int r4 = r0.getInt(r4)     // Catch:{ Exception -> 0x00ab }
            com.mintegral.msdk.base.entity.i r13 = new com.mintegral.msdk.base.entity.i     // Catch:{ Exception -> 0x00ab }
            r5 = r13
            r5.<init>(r6, r7, r8, r9, r10, r11, r12)     // Catch:{ Exception -> 0x00ab }
            r13.a(r4)     // Catch:{ Exception -> 0x00ab }
            r2.add(r13)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r4 = "id"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ab }
            int r4 = r0.getInt(r4)     // Catch:{ Exception -> 0x00ab }
            android.database.sqlite.SQLiteDatabase r5 = r15.b()     // Catch:{ Exception -> 0x00ab }
            if (r5 == 0) goto L_0x001b
            android.database.sqlite.SQLiteDatabase r5 = r15.b()     // Catch:{ Exception -> 0x00ab }
            java.lang.String r6 = "load_stat"
            java.lang.String r7 = "id = ?"
            r8 = 1
            java.lang.String[] r8 = new java.lang.String[r8]     // Catch:{ Exception -> 0x00ab }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x00ab }
            r8[r1] = r4     // Catch:{ Exception -> 0x00ab }
            r5.delete(r6, r7, r8)     // Catch:{ Exception -> 0x00ab }
            goto L_0x001b
        L_0x00a9:
            r1 = r2
            goto L_0x00b2
        L_0x00ab:
            r1 = move-exception
            goto L_0x00c1
        L_0x00ad:
            r2 = move-exception
            r14 = r2
            r2 = r1
            r1 = r14
            goto L_0x00c1
        L_0x00b2:
            if (r0 == 0) goto L_0x00ca
            r0.close()     // Catch:{ all -> 0x00d3 }
            goto L_0x00ca
        L_0x00b8:
            r0 = move-exception
            r14 = r1
            r1 = r0
            r0 = r14
            goto L_0x00cd
        L_0x00bd:
            r0 = move-exception
            r2 = r1
            r1 = r0
            r0 = r2
        L_0x00c1:
            r1.printStackTrace()     // Catch:{ all -> 0x00cc }
            if (r0 == 0) goto L_0x00c9
            r0.close()     // Catch:{ all -> 0x00d3 }
        L_0x00c9:
            r1 = r2
        L_0x00ca:
            monitor-exit(r15)
            return r1
        L_0x00cc:
            r1 = move-exception
        L_0x00cd:
            if (r0 == 0) goto L_0x00d2
            r0.close()     // Catch:{ all -> 0x00d3 }
        L_0x00d2:
            throw r1     // Catch:{ all -> 0x00d3 }
        L_0x00d3:
            r0 = move-exception
            monitor-exit(r15)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.n.d():java.util.List");
    }

    public final synchronized void a(i iVar) {
        if (b() != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(LocationConst.TIME, iVar.d());
            contentValues.put(CampaignEx.JSON_KEY_AD_SOURCE_ID, Integer.valueOf(iVar.b()));
            contentValues.put("adNum", Integer.valueOf(iVar.e()));
            contentValues.put("unitId", iVar.f());
            contentValues.put("fb", Integer.valueOf(iVar.g()));
            contentValues.put("hb", Integer.valueOf(iVar.a()));
            contentValues.put("timeout", Integer.valueOf(iVar.h()));
            contentValues.put("network_type", Integer.valueOf(iVar.j()));
            b().insert("load_stat", null, contentValues);
        }
    }
}
