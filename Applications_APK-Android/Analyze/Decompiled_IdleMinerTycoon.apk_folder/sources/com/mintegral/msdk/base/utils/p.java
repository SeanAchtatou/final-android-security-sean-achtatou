package com.mintegral.msdk.base.utils;

import android.content.Context;
import com.mintegral.msdk.base.controller.a;

/* compiled from: ResourceUtil */
public final class p {
    public static int a(Context context, String str, String str2) {
        String str3 = "";
        try {
            str3 = a.d().a();
        } catch (Exception unused) {
            try {
                g.d("ResourceUtil", "MTGSDKContext.getInstance() is null resName:" + str);
            } catch (Exception unused2) {
                g.d("ResourceUtil", "Resource not found resName:" + str);
                return -1;
            }
        }
        if (s.a(str3) && context != null) {
            str3 = context.getPackageName();
        }
        if (!s.a(str3) && context != null) {
            return context.getResources().getIdentifier(str, str2, str3);
        }
        return -1;
    }
}
