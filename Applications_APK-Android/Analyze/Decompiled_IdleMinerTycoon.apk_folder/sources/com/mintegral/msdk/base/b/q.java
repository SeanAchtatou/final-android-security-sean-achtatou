package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.n;
import com.mintegral.msdk.out.Campaign;

/* compiled from: PInfoDao */
public class q extends a<Campaign> {
    private static final String b = "com.mintegral.msdk.base.b.q";
    private static q c;

    private q(h hVar) {
        super(hVar);
    }

    public static q a(h hVar) {
        if (c == null) {
            synchronized (q.class) {
                if (c == null) {
                    c = new q(hVar);
                }
            }
        }
        return c;
    }

    public final synchronized long a(n nVar) {
        if (nVar == null) {
            return 0;
        }
        try {
            if (b() == null) {
                return -1;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put(CampaignEx.JSON_KEY_PACKAGE_NAME, nVar.a());
            contentValues.put("label", Integer.valueOf(nVar.b()));
            contentValues.put("modify_time", Long.valueOf(nVar.c()));
            contentValues.put("pkg_source", nVar.d());
            if (a(nVar.a())) {
                return (long) b().update("pinfo", contentValues, "package_name = '" + nVar.a() + "'", null);
            }
            return b().insert("pinfo", null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(final java.util.List<com.mintegral.msdk.base.entity.n> r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            if (r3 == 0) goto L_0x001c
            int r0 = r3.size()     // Catch:{ all -> 0x0019 }
            if (r0 != 0) goto L_0x000a
            goto L_0x001c
        L_0x000a:
            java.lang.Thread r0 = new java.lang.Thread     // Catch:{ all -> 0x0019 }
            com.mintegral.msdk.base.b.q$1 r1 = new com.mintegral.msdk.base.b.q$1     // Catch:{ all -> 0x0019 }
            r1.<init>(r3)     // Catch:{ all -> 0x0019 }
            r0.<init>(r1)     // Catch:{ all -> 0x0019 }
            r0.start()     // Catch:{ all -> 0x0019 }
            monitor-exit(r2)
            return
        L_0x0019:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        L_0x001c:
            monitor-exit(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.q.a(java.util.List):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0032, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean a(java.lang.String r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0035 }
            java.lang.String r2 = "SELECT package_name FROM pinfo WHERE package_name='"
            r1.<init>(r2)     // Catch:{ Exception -> 0x0035 }
            r1.append(r4)     // Catch:{ Exception -> 0x0035 }
            java.lang.String r4 = "'"
            r1.append(r4)     // Catch:{ Exception -> 0x0035 }
            java.lang.String r4 = r1.toString()     // Catch:{ Exception -> 0x0035 }
            android.database.sqlite.SQLiteDatabase r1 = r3.a()     // Catch:{ Exception -> 0x0035 }
            r2 = 0
            android.database.Cursor r4 = r1.rawQuery(r4, r2)     // Catch:{ Exception -> 0x0035 }
            if (r4 == 0) goto L_0x002c
            int r1 = r4.getCount()     // Catch:{ Exception -> 0x0035 }
            if (r1 <= 0) goto L_0x002c
            r4.close()     // Catch:{ Exception -> 0x0035 }
            r4 = 1
            monitor-exit(r3)
            return r4
        L_0x002c:
            if (r4 == 0) goto L_0x0031
            r4.close()     // Catch:{ Exception -> 0x0035 }
        L_0x0031:
            monitor-exit(r3)
            return r0
        L_0x0033:
            r4 = move-exception
            goto L_0x003b
        L_0x0035:
            r4 = move-exception
            r4.printStackTrace()     // Catch:{ all -> 0x0033 }
            monitor-exit(r3)
            return r0
        L_0x003b:
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.q.a(java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0079 A[SYNTHETIC, Splitter:B:27:0x0079] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0082 A[SYNTHETIC, Splitter:B:34:0x0082] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.mintegral.msdk.base.entity.n> c() {
        /*
            r6 = this;
            monitor-enter(r6)
            r0 = 0
            java.lang.String r1 = "SELECT * FROM pinfo"
            android.database.sqlite.SQLiteDatabase r2 = r6.a()     // Catch:{ Exception -> 0x0070, all -> 0x006b }
            android.database.Cursor r1 = r2.rawQuery(r1, r0)     // Catch:{ Exception -> 0x0070, all -> 0x006b }
            if (r1 == 0) goto L_0x0065
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x0060 }
            if (r2 <= 0) goto L_0x0065
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Exception -> 0x0060 }
            r2.<init>()     // Catch:{ Exception -> 0x0060 }
        L_0x0019:
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x005e }
            if (r0 == 0) goto L_0x005c
            com.mintegral.msdk.base.entity.n r0 = new com.mintegral.msdk.base.entity.n     // Catch:{ Exception -> 0x005e }
            r0.<init>()     // Catch:{ Exception -> 0x005e }
            java.lang.String r3 = "package_name"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x005e }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x005e }
            r0.a(r3)     // Catch:{ Exception -> 0x005e }
            java.lang.String r3 = "label"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x005e }
            int r3 = r1.getInt(r3)     // Catch:{ Exception -> 0x005e }
            r0.a(r3)     // Catch:{ Exception -> 0x005e }
            java.lang.String r3 = "modify_time"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x005e }
            long r3 = r1.getLong(r3)     // Catch:{ Exception -> 0x005e }
            r0.a(r3)     // Catch:{ Exception -> 0x005e }
            java.lang.String r3 = "pkg_source"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x005e }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x005e }
            r0.b(r3)     // Catch:{ Exception -> 0x005e }
            r2.add(r0)     // Catch:{ Exception -> 0x005e }
            goto L_0x0019
        L_0x005c:
            r0 = r2
            goto L_0x0065
        L_0x005e:
            r0 = move-exception
            goto L_0x0074
        L_0x0060:
            r2 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
            goto L_0x0074
        L_0x0065:
            if (r1 == 0) goto L_0x007d
            r1.close()     // Catch:{ all -> 0x0086 }
            goto L_0x007d
        L_0x006b:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0080
        L_0x0070:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r2
        L_0x0074:
            r0.printStackTrace()     // Catch:{ all -> 0x007f }
            if (r1 == 0) goto L_0x007c
            r1.close()     // Catch:{ all -> 0x0086 }
        L_0x007c:
            r0 = r2
        L_0x007d:
            monitor-exit(r6)
            return r0
        L_0x007f:
            r0 = move-exception
        L_0x0080:
            if (r1 == 0) goto L_0x0088
            r1.close()     // Catch:{ all -> 0x0086 }
            goto L_0x0088
        L_0x0086:
            r0 = move-exception
            goto L_0x0089
        L_0x0088:
            throw r0     // Catch:{ all -> 0x0086 }
        L_0x0089:
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.q.c():java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0085 A[SYNTHETIC, Splitter:B:27:0x0085] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x008e A[SYNTHETIC, Splitter:B:34:0x008e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.mintegral.msdk.base.entity.n> a(int r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            java.lang.String r2 = "SELECT * FROM pinfo WHERE label <> 0  ORDER BY label DESC LIMIT "
            r1.<init>(r2)     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            r1.append(r6)     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            java.lang.String r6 = r1.toString()     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            android.database.sqlite.SQLiteDatabase r1 = r5.a()     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            android.database.Cursor r6 = r1.rawQuery(r6, r0)     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            if (r6 == 0) goto L_0x0071
            int r1 = r6.getCount()     // Catch:{ Exception -> 0x006c }
            if (r1 <= 0) goto L_0x0071
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x006c }
            r1.<init>()     // Catch:{ Exception -> 0x006c }
        L_0x0025:
            boolean r0 = r6.moveToNext()     // Catch:{ Exception -> 0x006a }
            if (r0 == 0) goto L_0x0068
            com.mintegral.msdk.base.entity.n r0 = new com.mintegral.msdk.base.entity.n     // Catch:{ Exception -> 0x006a }
            r0.<init>()     // Catch:{ Exception -> 0x006a }
            java.lang.String r2 = "package_name"
            int r2 = r6.getColumnIndex(r2)     // Catch:{ Exception -> 0x006a }
            java.lang.String r2 = r6.getString(r2)     // Catch:{ Exception -> 0x006a }
            r0.a(r2)     // Catch:{ Exception -> 0x006a }
            java.lang.String r2 = "label"
            int r2 = r6.getColumnIndex(r2)     // Catch:{ Exception -> 0x006a }
            int r2 = r6.getInt(r2)     // Catch:{ Exception -> 0x006a }
            r0.a(r2)     // Catch:{ Exception -> 0x006a }
            java.lang.String r2 = "modify_time"
            int r2 = r6.getColumnIndex(r2)     // Catch:{ Exception -> 0x006a }
            long r2 = r6.getLong(r2)     // Catch:{ Exception -> 0x006a }
            r0.a(r2)     // Catch:{ Exception -> 0x006a }
            java.lang.String r2 = "pkg_source"
            int r2 = r6.getColumnIndex(r2)     // Catch:{ Exception -> 0x006a }
            java.lang.String r2 = r6.getString(r2)     // Catch:{ Exception -> 0x006a }
            r0.b(r2)     // Catch:{ Exception -> 0x006a }
            r1.add(r0)     // Catch:{ Exception -> 0x006a }
            goto L_0x0025
        L_0x0068:
            r0 = r1
            goto L_0x0071
        L_0x006a:
            r0 = move-exception
            goto L_0x0080
        L_0x006c:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0080
        L_0x0071:
            if (r6 == 0) goto L_0x0089
            r6.close()     // Catch:{ all -> 0x0092 }
            goto L_0x0089
        L_0x0077:
            r6 = move-exception
            r4 = r0
            r0 = r6
            r6 = r4
            goto L_0x008c
        L_0x007c:
            r6 = move-exception
            r1 = r0
            r0 = r6
            r6 = r1
        L_0x0080:
            r0.printStackTrace()     // Catch:{ all -> 0x008b }
            if (r6 == 0) goto L_0x0088
            r6.close()     // Catch:{ all -> 0x0092 }
        L_0x0088:
            r0 = r1
        L_0x0089:
            monitor-exit(r5)
            return r0
        L_0x008b:
            r0 = move-exception
        L_0x008c:
            if (r6 == 0) goto L_0x0094
            r6.close()     // Catch:{ all -> 0x0092 }
            goto L_0x0094
        L_0x0092:
            r6 = move-exception
            goto L_0x0095
        L_0x0094:
            throw r0     // Catch:{ all -> 0x0092 }
        L_0x0095:
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.q.a(int):java.util.List");
    }
}
