package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.m;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.out.Campaign;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.List;

/* compiled from: PBInfoDao */
public class p extends a<Campaign> {
    /* access modifiers changed from: private */
    public static final String b = "com.mintegral.msdk.base.b.p";
    private static p c;

    private p(h hVar) {
        super(hVar);
    }

    public static p a(h hVar) {
        if (c == null) {
            synchronized (p.class) {
                if (c == null) {
                    c = new p(hVar);
                }
            }
        }
        return c;
    }

    public final synchronized void c() {
        try {
            if (b() != null) {
                b().delete("pbinfo", null, null);
                g.b(b, "deleteAll");
            }
        } catch (Throwable th) {
            g.c(b, th.getMessage(), th);
        }
    }

    public final synchronized void a(String str) {
        try {
            String str2 = "package_name = '" + str + "'";
            if (b() != null) {
                b().delete("pbinfo", str2, null);
                g.b(b, "deleteByPKG：" + str);
            }
        } catch (Throwable th) {
            g.c(b, th.getMessage(), th);
        }
    }

    public final synchronized long a(m mVar, String str) {
        if (mVar == null) {
            return 0;
        }
        try {
            if (b() == null) {
                return -1;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put(CampaignEx.JSON_KEY_PACKAGE_NAME, mVar.a);
            contentValues.put(ReportDBAdapter.ReportColumns.TABLE_NAME, mVar.c);
            contentValues.put("uuid", str);
            if (b(mVar.a)) {
                String str2 = b;
                g.b(str2, "insertOrUpdate update:" + mVar.a);
                return (long) b().update("pbinfo", contentValues, "package_name = '" + mVar.a + "'", null);
            }
            String str3 = b;
            g.b(str3, "insertOrUpdate insert:" + mVar.a);
            return b().insert("pbinfo", null, contentValues);
        } catch (Throwable th) {
            g.c(b, th.getMessage(), th);
            return -1;
        }
    }

    public final synchronized void a(final List<m> list, final String str) {
        if (list != null) {
            if (list.size() != 0) {
                new Thread(new Runnable() {
                    public final void run() {
                        for (m a2 : list) {
                            String e = p.b;
                            g.b(e, "insertOrUpdate strat list size:" + list.size() + " uuid:" + str);
                            p.this.a(a2, str);
                        }
                    }
                }).start();
                return;
            }
        }
        g.b(b, "insertOrUpdate size为空 return");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0032, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean b(java.lang.String r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0035 }
            java.lang.String r2 = "SELECT package_name FROM pbinfo WHERE package_name='"
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0035 }
            r1.append(r4)     // Catch:{ Throwable -> 0x0035 }
            java.lang.String r4 = "'"
            r1.append(r4)     // Catch:{ Throwable -> 0x0035 }
            java.lang.String r4 = r1.toString()     // Catch:{ Throwable -> 0x0035 }
            android.database.sqlite.SQLiteDatabase r1 = r3.a()     // Catch:{ Throwable -> 0x0035 }
            r2 = 0
            android.database.Cursor r4 = r1.rawQuery(r4, r2)     // Catch:{ Throwable -> 0x0035 }
            if (r4 == 0) goto L_0x002c
            int r1 = r4.getCount()     // Catch:{ Throwable -> 0x0035 }
            if (r1 <= 0) goto L_0x002c
            r4.close()     // Catch:{ Throwable -> 0x0035 }
            r4 = 1
            monitor-exit(r3)
            return r4
        L_0x002c:
            if (r4 == 0) goto L_0x0031
            r4.close()     // Catch:{ Throwable -> 0x0035 }
        L_0x0031:
            monitor-exit(r3)
            return r0
        L_0x0033:
            r4 = move-exception
            goto L_0x0041
        L_0x0035:
            r4 = move-exception
            java.lang.String r1 = com.mintegral.msdk.base.b.p.b     // Catch:{ all -> 0x0033 }
            java.lang.String r2 = r4.getMessage()     // Catch:{ all -> 0x0033 }
            com.mintegral.msdk.base.utils.g.c(r1, r2, r4)     // Catch:{ all -> 0x0033 }
            monitor-exit(r3)
            return r0
        L_0x0041:
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.p.b(java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b0 A[SYNTHETIC, Splitter:B:31:0x00b0] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c4 A[SYNTHETIC, Splitter:B:41:0x00c4] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.mintegral.msdk.base.entity.m> d() {
        /*
            r7 = this;
            monitor-enter(r7)
            r0 = 0
            java.lang.String r1 = "SELECT * FROM pbinfo"
            android.database.sqlite.SQLiteDatabase r2 = r7.a()     // Catch:{ Throwable -> 0x00a1, all -> 0x009c }
            android.database.Cursor r1 = r2.rawQuery(r1, r0)     // Catch:{ Throwable -> 0x00a1, all -> 0x009c }
            if (r1 == 0) goto L_0x008b
            int r2 = r1.getCount()     // Catch:{ Throwable -> 0x0086 }
            if (r2 <= 0) goto L_0x008b
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Throwable -> 0x0086 }
            r2.<init>()     // Catch:{ Throwable -> 0x0086 }
        L_0x0019:
            boolean r0 = r1.moveToNext()     // Catch:{ Throwable -> 0x0084 }
            if (r0 == 0) goto L_0x006b
            com.mintegral.msdk.base.entity.m r0 = new com.mintegral.msdk.base.entity.m     // Catch:{ Throwable -> 0x0084 }
            r0.<init>()     // Catch:{ Throwable -> 0x0084 }
            java.lang.String r3 = "package_name"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Throwable -> 0x0084 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Throwable -> 0x0084 }
            r0.a = r3     // Catch:{ Throwable -> 0x0084 }
            java.lang.String r3 = "report"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Throwable -> 0x0084 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Throwable -> 0x0084 }
            r0.c = r3     // Catch:{ Throwable -> 0x0084 }
            java.lang.String r3 = "uuid"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Throwable -> 0x0084 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Throwable -> 0x0084 }
            r0.b = r3     // Catch:{ Throwable -> 0x0084 }
            java.lang.String r3 = com.mintegral.msdk.base.b.p.b     // Catch:{ Throwable -> 0x0084 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0084 }
            java.lang.String r5 = "getAllPBInfo pb："
            r4.<init>(r5)     // Catch:{ Throwable -> 0x0084 }
            java.lang.String r5 = r0.a     // Catch:{ Throwable -> 0x0084 }
            r4.append(r5)     // Catch:{ Throwable -> 0x0084 }
            java.lang.String r5 = " uuid:"
            r4.append(r5)     // Catch:{ Throwable -> 0x0084 }
            java.lang.String r5 = r0.b     // Catch:{ Throwable -> 0x0084 }
            r4.append(r5)     // Catch:{ Throwable -> 0x0084 }
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x0084 }
            com.mintegral.msdk.base.utils.g.b(r3, r4)     // Catch:{ Throwable -> 0x0084 }
            r2.add(r0)     // Catch:{ Throwable -> 0x0084 }
            goto L_0x0019
        L_0x006b:
            java.lang.String r0 = com.mintegral.msdk.base.b.p.b     // Catch:{ Throwable -> 0x0084 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0084 }
            java.lang.String r4 = "getAllPBInfo lis.size:"
            r3.<init>(r4)     // Catch:{ Throwable -> 0x0084 }
            int r4 = r2.size()     // Catch:{ Throwable -> 0x0084 }
            r3.append(r4)     // Catch:{ Throwable -> 0x0084 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x0084 }
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ Throwable -> 0x0084 }
            r0 = r2
            goto L_0x008b
        L_0x0084:
            r0 = move-exception
            goto L_0x00a5
        L_0x0086:
            r2 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
            goto L_0x00a5
        L_0x008b:
            if (r1 == 0) goto L_0x00bf
            r1.close()     // Catch:{ Throwable -> 0x0091 }
            goto L_0x00bf
        L_0x0091:
            r1 = move-exception
            java.lang.String r2 = com.mintegral.msdk.base.b.p.b     // Catch:{ all -> 0x00c8 }
            java.lang.String r3 = r1.getMessage()     // Catch:{ all -> 0x00c8 }
            com.mintegral.msdk.base.utils.g.c(r2, r3, r1)     // Catch:{ all -> 0x00c8 }
            goto L_0x00bf
        L_0x009c:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00c2
        L_0x00a1:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r2
        L_0x00a5:
            java.lang.String r3 = com.mintegral.msdk.base.b.p.b     // Catch:{ all -> 0x00c1 }
            java.lang.String r4 = r0.getMessage()     // Catch:{ all -> 0x00c1 }
            com.mintegral.msdk.base.utils.g.c(r3, r4, r0)     // Catch:{ all -> 0x00c1 }
            if (r1 == 0) goto L_0x00be
            r1.close()     // Catch:{ Throwable -> 0x00b4 }
            goto L_0x00be
        L_0x00b4:
            r0 = move-exception
            java.lang.String r1 = com.mintegral.msdk.base.b.p.b     // Catch:{ all -> 0x00c8 }
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x00c8 }
            com.mintegral.msdk.base.utils.g.c(r1, r3, r0)     // Catch:{ all -> 0x00c8 }
        L_0x00be:
            r0 = r2
        L_0x00bf:
            monitor-exit(r7)
            return r0
        L_0x00c1:
            r0 = move-exception
        L_0x00c2:
            if (r1 == 0) goto L_0x00d4
            r1.close()     // Catch:{ Throwable -> 0x00ca }
            goto L_0x00d4
        L_0x00c8:
            r0 = move-exception
            goto L_0x00d5
        L_0x00ca:
            r1 = move-exception
            java.lang.String r2 = com.mintegral.msdk.base.b.p.b     // Catch:{ all -> 0x00c8 }
            java.lang.String r3 = r1.getMessage()     // Catch:{ all -> 0x00c8 }
            com.mintegral.msdk.base.utils.g.c(r2, r3, r1)     // Catch:{ all -> 0x00c8 }
        L_0x00d4:
            throw r0     // Catch:{ all -> 0x00c8 }
        L_0x00d5:
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.p.d():java.util.List");
    }
}
