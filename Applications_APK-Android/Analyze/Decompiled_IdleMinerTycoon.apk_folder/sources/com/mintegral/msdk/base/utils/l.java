package com.mintegral.msdk.base.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.h;
import com.tapjoy.TapjoyConstants;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: InstallAppManager */
public class l {
    private static l b;
    private Context a;
    private SharedPreferences c;

    public static l a(Context context) {
        if (b == null) {
            synchronized (l.class) {
                if (b == null) {
                    b = new l(context);
                }
            }
        }
        return b;
    }

    private l(Context context) {
        this.a = context;
    }

    public final CopyOnWriteArraySet<h> a(String str) {
        CopyOnWriteArraySet<h> copyOnWriteArraySet = new CopyOnWriteArraySet<>();
        if (this.a != null) {
            try {
                this.c = this.a.getSharedPreferences(TapjoyConstants.TJC_INSTALLED, 0);
                if (this.c != null) {
                    SharedPreferences sharedPreferences = this.c;
                    String string = sharedPreferences.getString(str + "_installed", "");
                    if (!TextUtils.isEmpty(string)) {
                        JSONArray jSONArray = new JSONArray(string);
                        for (int i = 0; i < jSONArray.length(); i++) {
                            h hVar = new h();
                            JSONObject jSONObject = jSONArray.getJSONObject(i);
                            hVar.a(jSONObject.optString(Constants.RequestParameters.CAMPAIGN_ID));
                            hVar.b(jSONObject.optString("packageName"));
                            copyOnWriteArraySet.add(hVar);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return copyOnWriteArraySet;
    }

    public final void a(Set<h> set) {
        SharedPreferences.Editor edit;
        if (set != null && set.size() >= 0) {
            try {
                String a2 = h.a(set);
                if (this.a != null) {
                    this.c = this.a.getSharedPreferences(TapjoyConstants.TJC_INSTALLED, 0);
                    if (this.c != null && (edit = this.c.edit()) != null) {
                        edit.putString(a.d().j() + "_installed", a2);
                        edit.apply();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
