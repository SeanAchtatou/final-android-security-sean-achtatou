package com.mintegral.msdk.base.entity;

/* compiled from: PInfo */
public final class n {
    private String a;
    private int b;
    private long c;
    private String d;

    public final String a() {
        return this.a;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final int b() {
        return this.b;
    }

    public final void a(int i) {
        this.b = i;
    }

    public final long c() {
        return this.c;
    }

    public final void a(long j) {
        this.c = j;
    }

    public final String d() {
        return this.d;
    }

    public final void b(String str) {
        this.d = str;
    }
}
