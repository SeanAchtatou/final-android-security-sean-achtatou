package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.out.Campaign;
import com.vungle.warren.model.AdvertisementDBAdapter;
import java.util.List;
import org.json.JSONArray;

/* compiled from: CampaignDao */
public class f extends a<Campaign> {
    private static final String b = "com.mintegral.msdk.base.b.f";
    private static f c;

    protected f(h hVar) {
        super(hVar);
    }

    public static f a(h hVar) {
        if (c == null) {
            synchronized (f.class) {
                if (c == null) {
                    c = new f(hVar);
                }
            }
        }
        return c;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.lang.String r3, int r4) {
        /*
            r2 = this;
            monitor-enter(r2)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r1 = "unitid = "
            r0.<init>(r1)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            r0.append(r3)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r3 = " AND level = 2 AND adSource = "
            r0.append(r3)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            r0.append(r4)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r3 = r0.toString()     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            android.database.sqlite.SQLiteDatabase r4 = r2.b()     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            if (r4 != 0) goto L_0x001f
            monitor-exit(r2)
            return
        L_0x001f:
            android.database.sqlite.SQLiteDatabase r4 = r2.b()     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r0 = "campaign"
            r1 = 0
            r4.delete(r0, r3, r1)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            monitor-exit(r2)
            return
        L_0x002b:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        L_0x002e:
            monitor-exit(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.a(java.lang.String, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004b, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.lang.String r3, int r4, int r5, boolean r6) {
        /*
            r2 = this;
            monitor-enter(r2)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            java.lang.String r1 = "unitid = "
            r0.<init>(r1)     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            r0.append(r3)     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            java.lang.String r3 = " AND level = "
            r0.append(r3)     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            r0.append(r4)     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            java.lang.String r3 = " AND adSource = "
            r0.append(r3)     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            r0.append(r5)     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            java.lang.String r3 = r0.toString()     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            r4.<init>()     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            r4.append(r3)     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            java.lang.String r3 = " AND is_bid_campaign = "
            r4.append(r3)     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            r4.append(r6)     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            java.lang.String r3 = r4.toString()     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            android.database.sqlite.SQLiteDatabase r4 = r2.b()     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            if (r4 != 0) goto L_0x003b
            monitor-exit(r2)
            return
        L_0x003b:
            android.database.sqlite.SQLiteDatabase r4 = r2.b()     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            java.lang.String r5 = "campaign"
            r6 = 0
            r4.delete(r5, r3, r6)     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            monitor-exit(r2)
            return
        L_0x0047:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        L_0x004a:
            monitor-exit(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.a(java.lang.String, int, int, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.lang.String r3, java.lang.String r4) {
        /*
            r2 = this;
            monitor-enter(r2)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r1 = "id = '"
            r0.<init>(r1)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            r0.append(r3)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r3 = "' AND unitid = "
            r0.append(r3)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            r0.append(r4)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r3 = r0.toString()     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            android.database.sqlite.SQLiteDatabase r4 = r2.b()     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            if (r4 != 0) goto L_0x001f
            monitor-exit(r2)
            return
        L_0x001f:
            android.database.sqlite.SQLiteDatabase r4 = r2.b()     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r0 = "campaign"
            r1 = 0
            r4.delete(r0, r3, r1)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            monitor-exit(r2)
            return
        L_0x002b:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        L_0x002e:
            monitor-exit(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.a(java.lang.String, java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003e, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.lang.String r3, java.lang.String r4, boolean r5) {
        /*
            r2 = this;
            monitor-enter(r2)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            java.lang.String r1 = "id = '"
            r0.<init>(r1)     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            r0.append(r3)     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            java.lang.String r3 = "' AND unitid = "
            r0.append(r3)     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            r0.append(r4)     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            java.lang.String r3 = " AND is_bid_campaign = "
            r0.append(r3)     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            if (r5 == 0) goto L_0x001d
            java.lang.String r3 = "1"
            goto L_0x001f
        L_0x001d:
            java.lang.String r3 = "0"
        L_0x001f:
            r0.append(r3)     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            java.lang.String r3 = r0.toString()     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            android.database.sqlite.SQLiteDatabase r4 = r2.b()     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            if (r4 != 0) goto L_0x002e
            monitor-exit(r2)
            return
        L_0x002e:
            android.database.sqlite.SQLiteDatabase r4 = r2.b()     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            java.lang.String r5 = "campaign"
            r0 = 0
            r4.delete(r5, r3, r0)     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            monitor-exit(r2)
            return
        L_0x003a:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        L_0x003d:
            monitor-exit(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.a(java.lang.String, java.lang.String, boolean):void");
    }

    public final synchronized void a(String str) {
        try {
            String str2 = "id = '" + str + "'";
            if (b() != null) {
                b().delete("campaign", str2, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            g.d(b, e.getMessage());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0064, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.lang.String r3, java.lang.String r4, int r5, int r6, boolean r7) {
        /*
            r2 = this;
            monitor-enter(r2)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            java.lang.String r1 = "id = '"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            r0.append(r3)     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            java.lang.String r3 = "' AND unitid = "
            r0.append(r3)     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            r0.append(r4)     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            java.lang.String r3 = " AND level = "
            r0.append(r3)     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            r0.append(r5)     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            java.lang.String r3 = " AND adSource = "
            r0.append(r3)     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            r0.append(r6)     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            java.lang.String r3 = r0.toString()     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            if (r7 == 0) goto L_0x003b
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            r4.<init>()     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            r4.append(r3)     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            java.lang.String r3 = " AND is_bid_campaign = 1"
            r4.append(r3)     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            java.lang.String r3 = r4.toString()     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            goto L_0x004c
        L_0x003b:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            r4.<init>()     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            r4.append(r3)     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            java.lang.String r3 = " AND is_bid_campaign = 0"
            r4.append(r3)     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            java.lang.String r3 = r4.toString()     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
        L_0x004c:
            android.database.sqlite.SQLiteDatabase r4 = r2.b()     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            if (r4 != 0) goto L_0x0054
            monitor-exit(r2)
            return
        L_0x0054:
            android.database.sqlite.SQLiteDatabase r4 = r2.b()     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            java.lang.String r5 = "campaign"
            r6 = 0
            r4.delete(r5, r3, r6)     // Catch:{ Exception -> 0x0063, all -> 0x0060 }
            monitor-exit(r2)
            return
        L_0x0060:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        L_0x0063:
            monitor-exit(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.a(java.lang.String, java.lang.String, int, int, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(int r3, java.lang.String r4) {
        /*
            r2 = this;
            monitor-enter(r2)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r1 = "tab = "
            r0.<init>(r1)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            r0.append(r3)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r3 = " AND unitid = "
            r0.append(r3)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            r0.append(r4)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r3 = r0.toString()     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            android.database.sqlite.SQLiteDatabase r4 = r2.b()     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            if (r4 != 0) goto L_0x001f
            monitor-exit(r2)
            return
        L_0x001f:
            android.database.sqlite.SQLiteDatabase r4 = r2.b()     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r0 = "campaign"
            r1 = 0
            r4.delete(r0, r3, r1)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            monitor-exit(r2)
            return
        L_0x002b:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        L_0x002e:
            monitor-exit(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.a(int, java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0027, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void b(java.lang.String r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0026, all -> 0x0023 }
            java.lang.String r1 = "unitid = "
            r0.<init>(r1)     // Catch:{ Exception -> 0x0026, all -> 0x0023 }
            r0.append(r4)     // Catch:{ Exception -> 0x0026, all -> 0x0023 }
            java.lang.String r4 = r0.toString()     // Catch:{ Exception -> 0x0026, all -> 0x0023 }
            android.database.sqlite.SQLiteDatabase r0 = r3.b()     // Catch:{ Exception -> 0x0026, all -> 0x0023 }
            if (r0 != 0) goto L_0x0017
            monitor-exit(r3)
            return
        L_0x0017:
            android.database.sqlite.SQLiteDatabase r0 = r3.b()     // Catch:{ Exception -> 0x0026, all -> 0x0023 }
            java.lang.String r1 = "campaign"
            r2 = 0
            r0.delete(r1, r4, r2)     // Catch:{ Exception -> 0x0026, all -> 0x0023 }
            monitor-exit(r3)
            return
        L_0x0023:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        L_0x0026:
            monitor-exit(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.b(java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void b(java.lang.String r3, int r4) {
        /*
            r2 = this;
            monitor-enter(r2)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r1 = "unitid = "
            r0.<init>(r1)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            r0.append(r3)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r3 = " AND ia_rst = "
            r0.append(r3)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            r0.append(r4)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r3 = r0.toString()     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            android.database.sqlite.SQLiteDatabase r4 = r2.b()     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            if (r4 != 0) goto L_0x001f
            monitor-exit(r2)
            return
        L_0x001f:
            android.database.sqlite.SQLiteDatabase r4 = r2.b()     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r0 = "campaign"
            r1 = 0
            r4.delete(r0, r3, r1)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            monitor-exit(r2)
            return
        L_0x002b:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        L_0x002e:
            monitor-exit(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.b(java.lang.String, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void b(java.lang.String r3, java.lang.String r4) {
        /*
            r2 = this;
            monitor-enter(r2)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r1 = "unitid = "
            r0.<init>(r1)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            r0.append(r3)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r3 = " AND ia_ext1 = "
            r0.append(r3)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            r0.append(r4)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r3 = r0.toString()     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            android.database.sqlite.SQLiteDatabase r4 = r2.b()     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            if (r4 != 0) goto L_0x001f
            monitor-exit(r2)
            return
        L_0x001f:
            android.database.sqlite.SQLiteDatabase r4 = r2.b()     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            java.lang.String r0 = "campaign"
            r1 = 0
            r4.delete(r0, r3, r1)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            monitor-exit(r2)
            return
        L_0x002b:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        L_0x002e:
            monitor-exit(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.b(java.lang.String, java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0037, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void c(java.lang.String r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            r2 = 604800000(0x240c8400, double:2.988109026E-315)
            long r0 = r0 - r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            java.lang.String r3 = "unitid = "
            r2.<init>(r3)     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            r2.append(r5)     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            java.lang.String r5 = " AND short_ctime<"
            r2.append(r5)     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            r2.append(r0)     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            java.lang.String r5 = r2.toString()     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            android.database.sqlite.SQLiteDatabase r0 = r4.b()     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            if (r0 != 0) goto L_0x0027
            monitor-exit(r4)
            return
        L_0x0027:
            android.database.sqlite.SQLiteDatabase r0 = r4.b()     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            java.lang.String r1 = "campaign"
            r2 = 0
            r0.delete(r1, r5, r2)     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            monitor-exit(r4)
            return
        L_0x0033:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        L_0x0036:
            monitor-exit(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.c(java.lang.String):void");
    }

    public final synchronized void a(String str, ContentValues contentValues) {
        try {
            b().update("campaign", contentValues, "id = ?", new String[]{str});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:?, code lost:
        r1.put("dp", r36);
        r1.put("c", r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0259, code lost:
        r36 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x025b, code lost:
        r8 = r30;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x025e, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0260, code lost:
        r36 = r2;
        r8 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0267, code lost:
        if (r1 == null) goto L_0x0280;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0276, code lost:
        if (r1 == null) goto L_0x0280;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0278, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0282, code lost:
        r1 = r29;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x025e A[ExcHandler: all (th java.lang.Throwable), Splitter:B:9:0x009b] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x026e A[SYNTHETIC, Splitter:B:87:0x026e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.lang.String d(java.lang.String r42) {
        /*
            r41 = this;
            monitor-enter(r41)
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ all -> 0x0299 }
            r1.<init>()     // Catch:{ all -> 0x0299 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x028f }
            java.lang.String r2 = "SELECT * FROM campaign WHERE unitid = '"
            r0.<init>(r2)     // Catch:{ Throwable -> 0x028f }
            r2 = r42
            r0.append(r2)     // Catch:{ Throwable -> 0x028f }
            java.lang.String r2 = "'"
            r0.append(r2)     // Catch:{ Throwable -> 0x028f }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x028f }
            org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ Throwable -> 0x028f }
            r2.<init>()     // Catch:{ Throwable -> 0x028f }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Throwable -> 0x028f }
            r3.<init>()     // Catch:{ Throwable -> 0x028f }
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Throwable -> 0x028f }
            r4.<init>()     // Catch:{ Throwable -> 0x028f }
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Throwable -> 0x028f }
            r5.<init>()     // Catch:{ Throwable -> 0x028f }
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Throwable -> 0x028f }
            r6.<init>()     // Catch:{ Throwable -> 0x028f }
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ Throwable -> 0x028f }
            r7.<init>()     // Catch:{ Throwable -> 0x028f }
            org.json.JSONObject r8 = new org.json.JSONObject     // Catch:{ Throwable -> 0x028f }
            r8.<init>()     // Catch:{ Throwable -> 0x028f }
            org.json.JSONObject r9 = new org.json.JSONObject     // Catch:{ Throwable -> 0x028f }
            r9.<init>()     // Catch:{ Throwable -> 0x028f }
            org.json.JSONObject r10 = new org.json.JSONObject     // Catch:{ Throwable -> 0x028f }
            r10.<init>()     // Catch:{ Throwable -> 0x028f }
            long r11 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x028f }
            java.lang.Long r11 = java.lang.Long.valueOf(r11)     // Catch:{ Throwable -> 0x028f }
            long r12 = r11.longValue()     // Catch:{ Throwable -> 0x028f }
            r14 = 86400000(0x5265c00, double:4.2687272E-316)
            long r12 = r12 - r14
            long r14 = r11.longValue()     // Catch:{ Throwable -> 0x028f }
            r16 = 172800000(0xa4cb800, double:8.53745436E-316)
            long r14 = r14 - r16
            long r16 = r11.longValue()     // Catch:{ Throwable -> 0x028f }
            r18 = 259200000(0xf731400, double:1.280618154E-315)
            long r16 = r16 - r18
            long r18 = r11.longValue()     // Catch:{ Throwable -> 0x028f }
            r20 = 345600000(0x14997000, double:1.70749087E-315)
            long r18 = r18 - r20
            long r20 = r11.longValue()     // Catch:{ Throwable -> 0x028f }
            r22 = 432000000(0x19bfcc00, double:2.13436359E-315)
            long r20 = r20 - r22
            long r22 = r11.longValue()     // Catch:{ Throwable -> 0x028f }
            r24 = 518400000(0x1ee62800, double:2.56123631E-315)
            long r22 = r22 - r24
            long r24 = r11.longValue()     // Catch:{ Throwable -> 0x028f }
            r26 = 604800000(0x240c8400, double:2.988109026E-315)
            long r24 = r24 - r26
            r11 = 0
            r29 = r1
            android.database.sqlite.SQLiteDatabase r1 = r41.a()     // Catch:{ Throwable -> 0x0272, all -> 0x026a }
            android.database.Cursor r1 = r1.rawQuery(r0, r11)     // Catch:{ Throwable -> 0x0272, all -> 0x026a }
            if (r1 == 0) goto L_0x0264
            int r0 = r1.getCount()     // Catch:{ Throwable -> 0x0260, all -> 0x025e }
            if (r0 <= 0) goto L_0x0264
            r30 = r3
            r32 = r9
            r31 = r10
            r0 = 0
            r3 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r33 = 0
            r34 = 0
        L_0x00b0:
            boolean r26 = r1.moveToNext()     // Catch:{ Throwable -> 0x0259, all -> 0x025e }
            if (r26 == 0) goto L_0x022c
            r35 = r8
            java.lang.String r8 = "is_deleted"
            int r8 = r1.getColumnIndex(r8)     // Catch:{ Throwable -> 0x0259, all -> 0x025e }
            int r8 = r1.getInt(r8)     // Catch:{ Throwable -> 0x0259, all -> 0x025e }
            if (r8 != 0) goto L_0x00d1
            java.lang.String r8 = "id"
            int r8 = r1.getColumnIndex(r8)     // Catch:{ Throwable -> 0x0259, all -> 0x025e }
            java.lang.String r8 = r1.getString(r8)     // Catch:{ Throwable -> 0x0259, all -> 0x025e }
            r2.put(r8)     // Catch:{ Throwable -> 0x0259, all -> 0x025e }
        L_0x00d1:
            java.lang.String r8 = "short_ctime"
            int r8 = r1.getColumnIndex(r8)     // Catch:{ Throwable -> 0x0259, all -> 0x025e }
            long r26 = r1.getLong(r8)     // Catch:{ Throwable -> 0x0259, all -> 0x025e }
            int r8 = (r26 > r12 ? 1 : (r26 == r12 ? 0 : -1))
            r36 = r2
            r2 = 10
            if (r8 <= 0) goto L_0x0103
            if (r11 >= r2) goto L_0x0103
            int r11 = r11 + 1
            java.lang.String r2 = "id"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            java.lang.String r8 = "is_click"
            int r8 = r1.getColumnIndex(r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            int r8 = r1.getInt(r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            r4.put(r2, r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
        L_0x00fe:
            r8 = r35
            r2 = r36
            goto L_0x00b0
        L_0x0103:
            int r8 = (r26 > r12 ? 1 : (r26 == r12 ? 0 : -1))
            if (r8 >= 0) goto L_0x0127
            int r8 = (r26 > r14 ? 1 : (r26 == r14 ? 0 : -1))
            if (r8 <= 0) goto L_0x0127
            if (r0 >= r2) goto L_0x0127
            int r0 = r0 + 1
            java.lang.String r2 = "id"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            java.lang.String r8 = "is_click"
            int r8 = r1.getColumnIndex(r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            int r8 = r1.getInt(r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            r5.put(r2, r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            goto L_0x00fe
        L_0x0127:
            int r8 = (r26 > r14 ? 1 : (r26 == r14 ? 0 : -1))
            if (r8 >= 0) goto L_0x014b
            int r8 = (r26 > r16 ? 1 : (r26 == r16 ? 0 : -1))
            if (r8 <= 0) goto L_0x014b
            if (r3 >= r2) goto L_0x014b
            int r3 = r3 + 1
            java.lang.String r2 = "id"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            java.lang.String r8 = "is_click"
            int r8 = r1.getColumnIndex(r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            int r8 = r1.getInt(r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            r6.put(r2, r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            goto L_0x00fe
        L_0x014b:
            int r8 = (r26 > r16 ? 1 : (r26 == r16 ? 0 : -1))
            if (r8 >= 0) goto L_0x016f
            int r8 = (r26 > r18 ? 1 : (r26 == r18 ? 0 : -1))
            if (r8 <= 0) goto L_0x016f
            if (r10 >= r2) goto L_0x016f
            int r10 = r10 + 1
            java.lang.String r2 = "id"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            java.lang.String r8 = "is_click"
            int r8 = r1.getColumnIndex(r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            int r8 = r1.getInt(r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            r7.put(r2, r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            goto L_0x00fe
        L_0x016f:
            int r8 = (r26 > r18 ? 1 : (r26 == r18 ? 0 : -1))
            if (r8 >= 0) goto L_0x019d
            int r8 = (r26 > r20 ? 1 : (r26 == r20 ? 0 : -1))
            if (r8 <= 0) goto L_0x019d
            if (r9 >= r2) goto L_0x019d
            int r9 = r9 + 1
            java.lang.String r2 = "id"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            java.lang.String r8 = "is_click"
            int r8 = r1.getColumnIndex(r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            int r8 = r1.getInt(r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            r37 = r0
            r0 = r35
            r0.put(r2, r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            r8 = r0
            r2 = r36
            r0 = r37
            goto L_0x00b0
        L_0x019d:
            r37 = r0
            r0 = r35
            int r8 = (r26 > r20 ? 1 : (r26 == r20 ? 0 : -1))
            if (r8 >= 0) goto L_0x01da
            int r8 = (r26 > r22 ? 1 : (r26 == r22 ? 0 : -1))
            if (r8 <= 0) goto L_0x01da
            r8 = r33
            if (r8 >= r2) goto L_0x01d5
            int r33 = r8 + 1
            java.lang.String r2 = "id"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            java.lang.String r8 = "is_click"
            int r8 = r1.getColumnIndex(r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            int r8 = r1.getInt(r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            r38 = r3
            r3 = r32
            r3.put(r2, r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            r8 = r0
            r32 = r3
            r2 = r36
            r0 = r37
            r3 = r38
            goto L_0x00b0
        L_0x01d5:
            r38 = r3
            r3 = r32
            goto L_0x01e0
        L_0x01da:
            r38 = r3
            r3 = r32
            r8 = r33
        L_0x01e0:
            int r28 = (r26 > r22 ? 1 : (r26 == r22 ? 0 : -1))
            if (r28 >= 0) goto L_0x0211
            int r28 = (r26 > r24 ? 1 : (r26 == r24 ? 0 : -1))
            if (r28 <= 0) goto L_0x0211
            r39 = r8
            r8 = r34
            if (r8 >= r2) goto L_0x020c
            int r34 = r8 + 1
            java.lang.String r2 = "id"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            java.lang.String r8 = "is_click"
            int r8 = r1.getColumnIndex(r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            int r8 = r1.getInt(r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            r40 = r9
            r9 = r31
            r9.put(r2, r8)     // Catch:{ Throwable -> 0x025b, all -> 0x025e }
            goto L_0x021b
        L_0x020c:
            r40 = r9
            r9 = r31
            goto L_0x0219
        L_0x0211:
            r39 = r8
            r40 = r9
            r9 = r31
            r8 = r34
        L_0x0219:
            r34 = r8
        L_0x021b:
            r8 = r0
            r32 = r3
            r31 = r9
            r2 = r36
            r0 = r37
            r3 = r38
            r33 = r39
            r9 = r40
            goto L_0x00b0
        L_0x022c:
            r36 = r2
            r0 = r8
            r9 = r31
            r3 = r32
            java.lang.String r2 = "1"
            r8 = r30
            r8.put(r2, r4)     // Catch:{ Throwable -> 0x0276, all -> 0x025e }
            java.lang.String r2 = "2"
            r8.put(r2, r5)     // Catch:{ Throwable -> 0x0276, all -> 0x025e }
            java.lang.String r2 = "3"
            r8.put(r2, r6)     // Catch:{ Throwable -> 0x0276, all -> 0x025e }
            java.lang.String r2 = "4"
            r8.put(r2, r7)     // Catch:{ Throwable -> 0x0276, all -> 0x025e }
            java.lang.String r2 = "5"
            r8.put(r2, r0)     // Catch:{ Throwable -> 0x0276, all -> 0x025e }
            java.lang.String r0 = "6"
            r8.put(r0, r3)     // Catch:{ Throwable -> 0x0276, all -> 0x025e }
            java.lang.String r0 = "7"
            r8.put(r0, r9)     // Catch:{ Throwable -> 0x0276, all -> 0x025e }
            goto L_0x0267
        L_0x0259:
            r36 = r2
        L_0x025b:
            r8 = r30
            goto L_0x0276
        L_0x025e:
            r0 = move-exception
            goto L_0x026c
        L_0x0260:
            r36 = r2
            r8 = r3
            goto L_0x0276
        L_0x0264:
            r36 = r2
            r8 = r3
        L_0x0267:
            if (r1 == 0) goto L_0x0280
            goto L_0x0278
        L_0x026a:
            r0 = move-exception
            r1 = r11
        L_0x026c:
            if (r1 == 0) goto L_0x0271
            r1.close()     // Catch:{ Throwable -> 0x027c }
        L_0x0271:
            throw r0     // Catch:{ Throwable -> 0x027c }
        L_0x0272:
            r36 = r2
            r8 = r3
            r1 = r11
        L_0x0276:
            if (r1 == 0) goto L_0x0280
        L_0x0278:
            r1.close()     // Catch:{ Throwable -> 0x027c }
            goto L_0x0280
        L_0x027c:
            r0 = move-exception
            r1 = r29
            goto L_0x0290
        L_0x0280:
            java.lang.String r0 = "dp"
            r1 = r29
            r2 = r36
            r1.put(r0, r2)     // Catch:{ Throwable -> 0x028f }
            java.lang.String r0 = "c"
            r1.put(r0, r8)     // Catch:{ Throwable -> 0x028f }
            goto L_0x0293
        L_0x028f:
            r0 = move-exception
        L_0x0290:
            r0.printStackTrace()     // Catch:{ all -> 0x0299 }
        L_0x0293:
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x0299 }
            monitor-exit(r41)
            return r0
        L_0x0299:
            r0 = move-exception
            monitor-exit(r41)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.d(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0053 A[SYNTHETIC, Splitter:B:24:0x0053] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x005a A[SYNTHETIC, Splitter:B:29:0x005a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int b(int r11, java.lang.String r12) {
        /*
            r10 = this;
            monitor-enter(r10)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x005e }
            java.lang.String r1 = "tab = "
            r0.<init>(r1)     // Catch:{ all -> 0x005e }
            r0.append(r11)     // Catch:{ all -> 0x005e }
            java.lang.String r11 = " AND unitid = "
            r0.append(r11)     // Catch:{ all -> 0x005e }
            r0.append(r12)     // Catch:{ all -> 0x005e }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x005e }
            r11 = 1
            java.lang.String[] r3 = new java.lang.String[r11]     // Catch:{ all -> 0x005e }
            java.lang.String r11 = " count(id) "
            r12 = 0
            r3[r12] = r11     // Catch:{ all -> 0x005e }
            r11 = 0
            android.database.sqlite.SQLiteDatabase r1 = r10.a()     // Catch:{ Exception -> 0x004d }
            java.lang.String r2 = "campaign"
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r0 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x004d }
            if (r0 == 0) goto L_0x0045
            boolean r11 = r0.moveToFirst()     // Catch:{ Exception -> 0x0040, all -> 0x003c }
            if (r11 == 0) goto L_0x0045
            int r11 = r0.getInt(r12)     // Catch:{ Exception -> 0x0040, all -> 0x003c }
            r12 = r11
            goto L_0x0045
        L_0x003c:
            r11 = move-exception
            r12 = r11
            r11 = r0
            goto L_0x0058
        L_0x0040:
            r11 = move-exception
            r9 = r0
            r0 = r11
            r11 = r9
            goto L_0x004e
        L_0x0045:
            if (r0 == 0) goto L_0x0056
            r0.close()     // Catch:{ all -> 0x005e }
            goto L_0x0056
        L_0x004b:
            r12 = move-exception
            goto L_0x0058
        L_0x004d:
            r0 = move-exception
        L_0x004e:
            r0.printStackTrace()     // Catch:{ all -> 0x004b }
            if (r11 == 0) goto L_0x0056
            r11.close()     // Catch:{ all -> 0x005e }
        L_0x0056:
            monitor-exit(r10)
            return r12
        L_0x0058:
            if (r11 == 0) goto L_0x005d
            r11.close()     // Catch:{ all -> 0x005e }
        L_0x005d:
            throw r12     // Catch:{ all -> 0x005e }
        L_0x005e:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.b(int, java.lang.String):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0032, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void c() {
        /*
            r4 = this;
            monitor-enter(r4)
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0031, all -> 0x002e }
            r2 = 3600000(0x36ee80, double:1.7786363E-317)
            long r0 = r0 - r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0031, all -> 0x002e }
            java.lang.String r3 = "ts<"
            r2.<init>(r3)     // Catch:{ Exception -> 0x0031, all -> 0x002e }
            r2.append(r0)     // Catch:{ Exception -> 0x0031, all -> 0x002e }
            java.lang.String r0 = " AND ts>0"
            r2.append(r0)     // Catch:{ Exception -> 0x0031, all -> 0x002e }
            java.lang.String r0 = r2.toString()     // Catch:{ Exception -> 0x0031, all -> 0x002e }
            android.database.sqlite.SQLiteDatabase r1 = r4.b()     // Catch:{ Exception -> 0x0031, all -> 0x002e }
            if (r1 == 0) goto L_0x002c
            android.database.sqlite.SQLiteDatabase r1 = r4.b()     // Catch:{ Exception -> 0x0031, all -> 0x002e }
            java.lang.String r2 = "campaign"
            r3 = 0
            r1.delete(r2, r0, r3)     // Catch:{ Exception -> 0x0031, all -> 0x002e }
        L_0x002c:
            monitor-exit(r4)
            return
        L_0x002e:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0031:
            monitor-exit(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.c():void");
    }

    public final synchronized void a(long j, String str) {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            String str2 = "(plctb>0 and (plctb* 1000+ts)<" + currentTimeMillis + ") or (plctb<=0 and ts<" + (currentTimeMillis - j) + ") and unitid=?";
            String[] strArr = {str};
            if (b() != null) {
                b().delete("campaign", str2, strArr);
            }
        } catch (Exception e) {
            g.d(b, e.getMessage());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0035, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void b(long r4, java.lang.String r6) {
        /*
            r3 = this;
            monitor-enter(r3)
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0034, all -> 0x0031 }
            r2 = 0
            long r0 = r0 - r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0034, all -> 0x0031 }
            java.lang.String r5 = "ts<"
            r4.<init>(r5)     // Catch:{ Exception -> 0x0034, all -> 0x0031 }
            r4.append(r0)     // Catch:{ Exception -> 0x0034, all -> 0x0031 }
            java.lang.String r5 = " and unitid=?"
            r4.append(r5)     // Catch:{ Exception -> 0x0034, all -> 0x0031 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0034, all -> 0x0031 }
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Exception -> 0x0034, all -> 0x0031 }
            r0 = 0
            r5[r0] = r6     // Catch:{ Exception -> 0x0034, all -> 0x0031 }
            android.database.sqlite.SQLiteDatabase r6 = r3.b()     // Catch:{ Exception -> 0x0034, all -> 0x0031 }
            if (r6 == 0) goto L_0x002f
            android.database.sqlite.SQLiteDatabase r6 = r3.b()     // Catch:{ Exception -> 0x0034, all -> 0x0031 }
            java.lang.String r0 = "campaign"
            r6.delete(r0, r4, r5)     // Catch:{ Exception -> 0x0034, all -> 0x0031 }
        L_0x002f:
            monitor-exit(r3)
            return
        L_0x0031:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        L_0x0034:
            monitor-exit(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.b(long, java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x04c5, code lost:
        return -1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized long a(com.mintegral.msdk.base.entity.CampaignEx r13, java.lang.String r14, int r15) {
        /*
            r12 = this;
            monitor-enter(r12)
            if (r13 != 0) goto L_0x0007
            r13 = 0
            monitor-exit(r12)
            return r13
        L_0x0007:
            r0 = -1
            android.database.sqlite.SQLiteDatabase r2 = r12.b()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            if (r2 != 0) goto L_0x0011
            monitor-exit(r12)
            return r0
        L_0x0011:
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.<init>()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "id"
            java.lang.String r4 = r13.getId()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "unitid"
            r2.put(r3, r14)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "tab"
            int r4 = r13.getTab()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "package_name"
            java.lang.String r4 = r13.getPackageName()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "app_name"
            java.lang.String r4 = r13.getAppName()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "app_desc"
            java.lang.String r4 = r13.getAppDesc()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "app_size"
            java.lang.String r4 = r13.getSize()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "image_size"
            java.lang.String r4 = r13.getImageSize()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "icon_url"
            java.lang.String r4 = r13.getIconUrl()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "image_url"
            java.lang.String r4 = r13.getImageUrl()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "impression_url"
            java.lang.String r4 = r13.getImpressionURL()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "notice_url"
            java.lang.String r4 = r13.getNoticeUrl()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "download_url"
            java.lang.String r4 = r13.getClickURL()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "wtick"
            int r4 = r13.getWtick()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "wtick"
            int r4 = r13.getWtick()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "deeplink_url"
            java.lang.String r4 = r13.getDeepLinkURL()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "only_impression"
            java.lang.String r4 = r13.getOnlyImpressionURL()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "ts"
            long r4 = r13.getTimestamp()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "template"
            int r4 = r13.getTemplate()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "click_mode"
            java.lang.String r4 = r13.getClick_mode()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "landing_type"
            java.lang.String r4 = r13.getLandingType()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "link_type"
            int r4 = r13.getLinkType()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "star"
            double r4 = r13.getRating()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Double r4 = java.lang.Double.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "cti"
            int r4 = r13.getClickInterval()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "cpti"
            int r4 = r13.getPreClickInterval()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "preclick"
            boolean r4 = r13.isPreClick()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "level"
            int r4 = r13.getCacheLevel()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "adSource"
            int r4 = r13.getType()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "ad_call"
            java.lang.String r4 = r13.getAdCall()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "fc_a"
            int r4 = r13.getFca()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "fc_b"
            int r4 = r13.getFcb()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "ad_url_list"
            java.lang.String r4 = r13.getAd_url_list()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "video_url"
            java.lang.String r4 = r13.getVideoUrlEncode()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "video_size"
            int r4 = r13.getVideoSize()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "video_length"
            int r4 = r13.getVideoLength()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "video_resolution"
            java.lang.String r4 = r13.getVideoResolution()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "endcard_click_result"
            int r4 = r13.getEndcard_click_result()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "watch_mile"
            int r4 = r13.getWatchMile()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "advImp"
            java.lang.String r4 = r13.getAdvImp()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "bty"
            int r4 = r13.getBty()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "t_imp"
            int r4 = r13.getTImp()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "guidelines"
            java.lang.String r4 = r13.getGuidelines()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "offer_type"
            int r4 = r13.getOfferType()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "html_url"
            java.lang.String r4 = r13.getHtmlUrl()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "end_screen_url"
            java.lang.String r4 = r13.getEndScreenUrl()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "reward_amount"
            int r4 = r13.getRewardAmount()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "reward_name"
            java.lang.String r4 = r13.getRewardName()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "reward_play_status"
            int r4 = r13.getRewardPlayStatus()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "adv_id"
            java.lang.String r4 = r13.getAdvId()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "ttc_ct2"
            int r4 = r13.getTtc_ct2()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            int r4 = r4 * 1000
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "ttc_type"
            int r4 = r13.getTtc_type()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "retarget"
            int r4 = r13.getRetarget_offer()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "native_ad_tracking"
            java.lang.String r4 = r13.getNativeVideoTrackingString()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "playable_ads_without_video"
            int r4 = r13.getPlayable_ads_without_video()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "endcard_url"
            java.lang.String r4 = r13.getendcard_url()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "video_end_type"
            int r4 = r13.getVideo_end_type()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "loopback"
            java.lang.String r4 = r13.getLoopbackString()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "md5_file"
            java.lang.String r4 = r13.getVideoMD5Value()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "nv_t2"
            int r4 = r13.getNvT2()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "gif_url"
            java.lang.String r4 = r13.getGifUrl()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            com.mintegral.msdk.base.entity.CampaignEx$c r3 = r13.getRewardTemplateMode()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            if (r3 == 0) goto L_0x02b7
            java.lang.String r3 = "reward_teamplate"
            com.mintegral.msdk.base.entity.CampaignEx$c r4 = r13.getRewardTemplateMode()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r4 = r4.a()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
        L_0x02b7:
            java.lang.String r3 = "c_coi"
            int r4 = r13.getClickTimeOutInterval()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "c_ua"
            int r4 = r13.getcUA()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "imp_ua"
            int r4 = r13.getImpUA()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "jm_pd"
            int r4 = r13.getJmPd()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "is_deleted"
            int r4 = r13.getIsDeleted()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "is_click"
            int r4 = r13.getIsClick()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "is_add_sucesful"
            int r4 = r13.getIsAddSuccesful()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "short_ctime"
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "ia_icon"
            java.lang.String r4 = r13.getKeyIaIcon()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "ia_url"
            java.lang.String r4 = r13.getKeyIaUrl()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "ia_rst"
            int r4 = r13.getKeyIaRst()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "ia_ori"
            int r4 = r13.getKeyIaOri()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "ad_type"
            int r4 = r13.getAdType()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "ia_ext1"
            java.lang.String r4 = r13.getIa_ext1()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "ia_ext2"
            java.lang.String r4 = r13.getIa_ext2()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "is_download_zip"
            int r4 = r13.getIsDownLoadZip()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "ia_cache"
            java.lang.String r4 = r13.getInteractiveCache()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "gh_id"
            java.lang.String r4 = r13.getGhId()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "gh_path"
            java.lang.String r4 = r13.getGhPath()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "bind_id"
            java.lang.String r4 = r13.getBindId()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "oc_time"
            int r4 = r13.getOc_time()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "oc_type"
            int r4 = r13.getOc_type()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "t_list"
            java.lang.String r4 = r13.getT_list()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            com.mintegral.msdk.base.entity.CampaignEx$a r3 = r13.getAdchoice()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            if (r3 == 0) goto L_0x03e7
            java.lang.String r4 = "adchoice"
            java.lang.String r5 = r3.c()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r4, r5)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r4 = "adchoice_size_height"
            int r5 = r3.b()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r4, r5)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r4 = "adchoice_size_width"
            int r3 = r3.a()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r4, r3)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
        L_0x03e7:
            java.lang.String r3 = "plct"
            long r4 = r13.getPlct()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "plctb"
            long r4 = r13.getPlctb()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "banner_url"
            java.lang.String r4 = r13.getBannerUrl()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "banner_html"
            java.lang.String r4 = r13.getBannerHtml()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "creative_id"
            long r4 = r13.getCreativeId()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "is_bid_campaign"
            boolean r4 = r13.isBidCampaign()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "bid_token"
            java.lang.String r4 = r13.getBidToken()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "mraid"
            java.lang.String r4 = r13.getMraid()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "is_mraid_campaign"
            boolean r4 = r13.isMraid()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r3 = "omid"
            java.lang.String r4 = r13.getOmid()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r6 = r13.getId()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            int r7 = r13.getTab()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            int r10 = r13.getType()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            boolean r11 = r13.isBidCampaign()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r5 = r12
            r8 = r14
            r9 = r15
            boolean r15 = r5.a(r6, r7, r8, r9, r10, r11)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r3 = 0
            if (r15 == 0) goto L_0x04b5
            boolean r15 = r13.isBidCampaign()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            if (r15 == 0) goto L_0x0489
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r15 = "unitid = "
            r13.<init>(r15)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r13.append(r14)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r14 = " AND is_bid_campaign = 1"
            r13.append(r14)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            goto L_0x04a8
        L_0x0489:
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r4 = "id = "
            r15.<init>(r4)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r13 = r13.getId()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r15.append(r13)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r13 = " AND unitid = "
            r15.append(r13)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            r15.append(r14)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r13 = " AND is_bid_campaign = 0"
            r15.append(r13)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r13 = r15.toString()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
        L_0x04a8:
            android.database.sqlite.SQLiteDatabase r14 = r12.b()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r15 = "campaign"
            int r13 = r14.update(r15, r2, r13, r3)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            long r13 = (long) r13
            monitor-exit(r12)
            return r13
        L_0x04b5:
            android.database.sqlite.SQLiteDatabase r13 = r12.b()     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            java.lang.String r14 = "campaign"
            long r13 = r13.insert(r14, r3, r2)     // Catch:{ Exception -> 0x04c4, all -> 0x04c1 }
            monitor-exit(r12)
            return r13
        L_0x04c1:
            r13 = move-exception
            monitor-exit(r12)
            throw r13
        L_0x04c4:
            monitor-exit(r12)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.a(com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, int):long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0459, code lost:
        return -1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized long a(com.mintegral.msdk.base.entity.CampaignEx r6, java.lang.String r7) {
        /*
            r5 = this;
            monitor-enter(r5)
            if (r6 != 0) goto L_0x0007
            r6 = 0
            monitor-exit(r5)
            return r6
        L_0x0007:
            r0 = -1
            android.database.sqlite.SQLiteDatabase r2 = r5.b()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            if (r2 != 0) goto L_0x0011
            monitor-exit(r5)
            return r0
        L_0x0011:
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.<init>()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r3 = "id"
            java.lang.String r4 = r6.getId()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r3 = "unitid"
            r2.put(r3, r7)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "tab"
            int r3 = r6.getTab()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "package_name"
            java.lang.String r3 = r6.getPackageName()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "app_name"
            java.lang.String r3 = r6.getAppName()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "app_desc"
            java.lang.String r3 = r6.getAppDesc()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "app_size"
            java.lang.String r3 = r6.getSize()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "image_size"
            java.lang.String r3 = r6.getImageSize()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "icon_url"
            java.lang.String r3 = r6.getIconUrl()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "image_url"
            java.lang.String r3 = r6.getImageUrl()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "impression_url"
            java.lang.String r3 = r6.getImpressionURL()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "notice_url"
            java.lang.String r3 = r6.getNoticeUrl()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "download_url"
            java.lang.String r3 = r6.getClickURL()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "wtick"
            int r3 = r6.getWtick()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "deeplink_url"
            java.lang.String r3 = r6.getDeepLinkURL()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "only_impression"
            java.lang.String r3 = r6.getOnlyImpressionURL()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "ts"
            long r3 = r6.getTimestamp()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "template"
            int r3 = r6.getTemplate()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "click_mode"
            java.lang.String r3 = r6.getClick_mode()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "landing_type"
            java.lang.String r3 = r6.getLandingType()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "link_type"
            int r3 = r6.getLinkType()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "star"
            double r3 = r6.getRating()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Double r3 = java.lang.Double.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "cti"
            int r3 = r6.getClickInterval()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "cpti"
            int r3 = r6.getPreClickInterval()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "preclick"
            boolean r3 = r6.isPreClick()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "level"
            int r3 = r6.getCacheLevel()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "adSource"
            int r3 = r6.getType()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "ad_call"
            java.lang.String r3 = r6.getAdCall()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "fc_a"
            int r3 = r6.getFca()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "fc_b"
            int r3 = r6.getFcb()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "ad_url_list"
            java.lang.String r3 = r6.getAd_url_list()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "video_url"
            java.lang.String r3 = r6.getVideoUrlEncode()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "video_size"
            int r3 = r6.getVideoSize()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "video_length"
            int r3 = r6.getVideoLength()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "video_resolution"
            java.lang.String r3 = r6.getVideoResolution()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "endcard_click_result"
            int r3 = r6.getEndcard_click_result()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "watch_mile"
            int r3 = r6.getWatchMile()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "advImp"
            java.lang.String r3 = r6.getAdvImp()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "bty"
            int r3 = r6.getBty()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "t_imp"
            int r3 = r6.getTImp()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "guidelines"
            java.lang.String r3 = r6.getGuidelines()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "offer_type"
            int r3 = r6.getOfferType()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "html_url"
            java.lang.String r3 = r6.getHtmlUrl()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "end_screen_url"
            java.lang.String r3 = r6.getEndScreenUrl()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "reward_amount"
            int r3 = r6.getRewardAmount()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "reward_name"
            java.lang.String r3 = r6.getRewardName()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "reward_play_status"
            int r3 = r6.getRewardPlayStatus()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "adv_id"
            java.lang.String r3 = r6.getAdvId()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "ttc_ct2"
            int r3 = r6.getTtc_ct2()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            int r3 = r3 * 1000
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "ttc_type"
            int r3 = r6.getTtc_type()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "retarget"
            int r3 = r6.getRetarget_offer()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "native_ad_tracking"
            java.lang.String r3 = r6.getNativeVideoTrackingString()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "playable_ads_without_video"
            int r3 = r6.getPlayable_ads_without_video()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "endcard_url"
            java.lang.String r3 = r6.getendcard_url()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "video_end_type"
            int r3 = r6.getVideo_end_type()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "loopback"
            java.lang.String r3 = r6.getLoopbackString()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "md5_file"
            java.lang.String r3 = r6.getVideoMD5Value()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "nv_t2"
            int r3 = r6.getNvT2()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "gif_url"
            java.lang.String r3 = r6.getGifUrl()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            com.mintegral.msdk.base.entity.CampaignEx$c r7 = r6.getRewardTemplateMode()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            if (r7 == 0) goto L_0x02aa
            java.lang.String r7 = "reward_teamplate"
            com.mintegral.msdk.base.entity.CampaignEx$c r3 = r6.getRewardTemplateMode()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r3 = r3.a()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
        L_0x02aa:
            java.lang.String r7 = "c_coi"
            int r3 = r6.getClickTimeOutInterval()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "c_ua"
            int r3 = r6.getcUA()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "imp_ua"
            int r3 = r6.getImpUA()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "jm_pd"
            int r3 = r6.getJmPd()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "is_deleted"
            int r3 = r6.getIsDeleted()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "is_click"
            int r3 = r6.getIsClick()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "is_add_sucesful"
            int r3 = r6.getIsAddSuccesful()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "short_ctime"
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "ia_icon"
            java.lang.String r3 = r6.getKeyIaIcon()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "ia_url"
            java.lang.String r3 = r6.getKeyIaUrl()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "ia_rst"
            int r3 = r6.getKeyIaRst()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "ia_ori"
            int r3 = r6.getKeyIaOri()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "ad_type"
            int r3 = r6.getAdType()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "ia_ext1"
            java.lang.String r3 = r6.getIa_ext1()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "ia_ext2"
            java.lang.String r3 = r6.getIa_ext2()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "is_download_zip"
            int r3 = r6.getIsDownLoadZip()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "ia_cache"
            java.lang.String r3 = r6.getInteractiveCache()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "gh_id"
            java.lang.String r3 = r6.getGhId()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "gh_path"
            java.lang.String r3 = r6.getGhPath()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "bind_id"
            java.lang.String r3 = r6.getBindId()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "oc_type"
            int r3 = r6.getOc_type()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "oc_time"
            int r3 = r6.getOc_time()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "t_list"
            java.lang.String r3 = r6.getT_list()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            com.mintegral.msdk.base.entity.CampaignEx$a r7 = r6.getAdchoice()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            if (r7 == 0) goto L_0x03da
            java.lang.String r3 = "adchoice"
            java.lang.String r4 = r7.c()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r3 = "adchoice_size_height"
            int r4 = r7.b()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r3 = "adchoice_size_width"
            int r7 = r7.a()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r3, r7)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
        L_0x03da:
            java.lang.String r7 = "plct"
            long r3 = r6.getPlct()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "plctb"
            long r3 = r6.getPlctb()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "banner_url"
            java.lang.String r3 = r6.getBannerUrl()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "banner_html"
            java.lang.String r3 = r6.getBannerHtml()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "creative_id"
            long r3 = r6.getCreativeId()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "is_bid_campaign"
            boolean r3 = r6.isBidCampaign()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "bid_token"
            java.lang.String r3 = r6.getBidToken()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "mraid"
            java.lang.String r3 = r6.getMraid()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "is_mraid_campaign"
            boolean r3 = r6.isMraid()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r3)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "omid"
            java.lang.String r6 = r6.getOmid()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            r2.put(r7, r6)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            android.database.sqlite.SQLiteDatabase r6 = r5.b()     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            java.lang.String r7 = "campaign"
            r3 = 0
            long r6 = r6.insert(r7, r3, r2)     // Catch:{ Exception -> 0x0458, all -> 0x0455 }
            monitor-exit(r5)
            return r6
        L_0x0455:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        L_0x0458:
            monitor-exit(r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.a(com.mintegral.msdk.base.entity.CampaignEx, java.lang.String):long");
    }

    public final synchronized void a(final List<CampaignEx> list, final String str) {
        if (list.size() != 0) {
            new Thread(new Runnable() {
                public final void run() {
                    for (CampaignEx a2 : list) {
                        f.this.a(a2, str, 0);
                    }
                }
            }).start();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void b(final java.util.List<com.mintegral.msdk.base.entity.CampaignEx> r3, final java.lang.String r4) {
        /*
            r2 = this;
            monitor-enter(r2)
            if (r3 == 0) goto L_0x001c
            int r0 = r3.size()     // Catch:{ all -> 0x0019 }
            if (r0 != 0) goto L_0x000a
            goto L_0x001c
        L_0x000a:
            java.lang.Thread r0 = new java.lang.Thread     // Catch:{ all -> 0x0019 }
            com.mintegral.msdk.base.b.f$2 r1 = new com.mintegral.msdk.base.b.f$2     // Catch:{ all -> 0x0019 }
            r1.<init>(r3, r4)     // Catch:{ all -> 0x0019 }
            r0.<init>(r1)     // Catch:{ all -> 0x0019 }
            r0.start()     // Catch:{ all -> 0x0019 }
            monitor-exit(r2)
            return
        L_0x0019:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        L_0x001c:
            monitor-exit(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.b(java.util.List, java.lang.String):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.base.b.f.a(java.lang.String, int, boolean):java.util.List<com.mintegral.msdk.base.entity.CampaignEx>
     arg types: [java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.base.b.f.a(com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, int):long
      com.mintegral.msdk.base.b.f.a(java.lang.String, java.lang.String, boolean):void
      com.mintegral.msdk.base.b.f.a(java.lang.String, int, boolean):java.util.List<com.mintegral.msdk.base.entity.CampaignEx> */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0054, code lost:
        return 0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int a(java.lang.String r6, long r7) {
        /*
            r5 = this;
            monitor-enter(r5)
            r0 = 0
            com.mintegral.msdk.d.b.a()     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
            java.lang.String r1 = r1.j()     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
            com.mintegral.msdk.d.a r1 = com.mintegral.msdk.d.b.b(r1)     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
            if (r1 != 0) goto L_0x001a
            com.mintegral.msdk.d.b.a()     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
            com.mintegral.msdk.d.a r1 = com.mintegral.msdk.d.b.b()     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
        L_0x001a:
            long r1 = r1.ak()     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
            r3 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 * r3
            r5.a(r1, r6)     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
            r3 = 1
            java.util.List r6 = r5.a(r6, r3, r0)     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
            if (r6 == 0) goto L_0x004d
            boolean r4 = r6.isEmpty()     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
            if (r4 == 0) goto L_0x0033
            goto L_0x004d
        L_0x0033:
            java.util.Iterator r6 = r6.iterator()     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
        L_0x0037:
            boolean r4 = r6.hasNext()     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
            if (r4 == 0) goto L_0x004b
            java.lang.Object r4 = r6.next()     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
            com.mintegral.msdk.base.entity.CampaignEx r4 = (com.mintegral.msdk.base.entity.CampaignEx) r4     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
            boolean r4 = r4.isSpareOffer(r7, r1)     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
            if (r4 != 0) goto L_0x0037
            monitor-exit(r5)
            return r0
        L_0x004b:
            monitor-exit(r5)
            return r3
        L_0x004d:
            r6 = -1
            monitor-exit(r5)
            return r6
        L_0x0050:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        L_0x0053:
            monitor-exit(r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.a(java.lang.String, long):int");
    }

    public final synchronized void e(String str) {
        a().execSQL("UPDATE campaign SET is_download_zip='1' WHERE ia_url='" + str + "'");
    }

    public final synchronized boolean a(String str, int i, String str2, int i2, int i3, boolean z) {
        String str3;
        if (z) {
            str3 = "SELECT id FROM campaign WHERE " + "unitid = " + str2 + " AND is_bid_campaign = 1";
        } else {
            str3 = "SELECT id FROM campaign WHERE " + "id='" + str + "' AND tab=" + i + " AND unitid = '" + str2 + "' AND level = " + i2 + " AND adSource = " + i3 + " AND is_bid_campaign = 0";
        }
        Cursor rawQuery = a().rawQuery(str3, null);
        if (rawQuery == null || rawQuery.getCount() <= 0) {
            if (rawQuery != null) {
                rawQuery.close();
            }
            return false;
        }
        rawQuery.close();
        return true;
    }

    public final List<CampaignEx> a(String str, int i, int i2, int i3) {
        String str2 = " WHERE unitid = '" + str + "' AND level = " + i2 + " AND adSource = " + i3;
        String str3 = "";
        if (i > 0) {
            str3 = " LIMIT " + i;
        }
        return l("SELECT * FROM campaign" + str2 + str3);
    }

    public final synchronized List<CampaignEx> a(String str, int i, boolean z) {
        String str2;
        str2 = " WHERE unitid = '" + str + "' AND level = 0 AND adSource = " + i;
        if (z) {
            str2 = str2 + " AND is_bid_campaign = 1";
        }
        return l("SELECT * FROM campaign" + str2 + "");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0017, code lost:
        if (r10.getCount() <= 0) goto L_0x04b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0019, code lost:
        r1 = new java.util.ArrayList();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0022, code lost:
        if (r10.moveToNext() == false) goto L_0x04a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0024, code lost:
        r2 = new com.mintegral.msdk.base.entity.CampaignEx();
        r2.setId(r10.getString(r10.getColumnIndex("id")));
        r2.setTab(r10.getInt(r10.getColumnIndex("tab")));
        r2.setPackageName(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PACKAGE_NAME)));
        r2.setAppName(r10.getString(r10.getColumnIndex("app_name")));
        r2.setAppDesc(r10.getString(r10.getColumnIndex("app_desc")));
        r2.setSize(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_APP_SIZE)));
        r2.setImageSize(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMAGE_SIZE)));
        r2.setIconUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ICON_URL)));
        r2.setImageUrl(r10.getString(r10.getColumnIndex("image_url")));
        r2.setImpressionURL(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMPRESSION_URL)));
        r2.setNoticeUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NOTICE_URL)));
        r2.setClickURL(r10.getString(r10.getColumnIndex("download_url")));
        r2.setWtick(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_WITHOUT_INSTALL_CHECK)));
        r2.setDeepLinkUrl(r10.getString(r10.getColumnIndex("deeplink_url")));
        r2.setOnlyImpressionURL(r10.getString(r10.getColumnIndex("only_impression")));
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00f8, code lost:
        if (r10.getInt(r10.getColumnIndex("preclick")) != 1) goto L_0x00fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00fa, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00fc, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00fd, code lost:
        r2.setPreClick(r3);
        r2.setTemplate(r10.getInt(r10.getColumnIndex("template")));
        r2.setLandingType(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LANDING_TYPE)));
        r2.setLinkType(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LINK_TYPE)));
        r2.setClick_mode(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_CLICK_MODE)));
        r2.setRating(java.lang.Double.parseDouble(r10.getString(r10.getColumnIndex("star"))));
        r2.setClickInterval(r10.getInt(r10.getColumnIndex("cti")));
        r2.setPreClickInterval(r10.getInt(r10.getColumnIndex("cpti")));
        r2.setTimestamp(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ST_TS)));
        r2.setCacheLevel(r10.getInt(r10.getColumnIndex("level")));
        r2.setType(r10.getInt(r10.getColumnIndex("adSource")));
        r2.setAdCall(r10.getString(r10.getColumnIndex("ad_call")));
        r2.setFca(r10.getInt(r10.getColumnIndex("fc_a")));
        r2.setFcb(r10.getInt(r10.getColumnIndex("fc_b")));
        r2.setAd_url_list(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_AD_URL_LIST)));
        r2.setVideoLength(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_LENGTHL)));
        r2.setVideoSize(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_SIZE)));
        r2.setVideoResolution(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_RESOLUTION)));
        r2.setEndcard_click_result(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ENDCARD_CLICK)));
        r2.setVideoUrlEncode(r10.getString(r10.getColumnIndex("video_url")));
        r2.setWatchMile(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_WATCH_MILE)));
        r2.setTImp(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_T_IMP)));
        r2.setBty(r10.getInt(r10.getColumnIndex("bty")));
        r2.setAdvImp(r10.getString(r10.getColumnIndex("advImp")));
        r2.setOfferType(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_OFFER_TYPE)));
        r2.setGuidelines(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GUIDELINES)));
        r2.setHtmlUrl(r10.getString(r10.getColumnIndex("html_url")));
        r2.setEndScreenUrl(r10.getString(r10.getColumnIndex("end_screen_url")));
        r2.setRewardName(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_NAME)));
        r2.setRewardAmount(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_AMOUNT)));
        r2.setRewardPlayStatus(r10.getInt(r10.getColumnIndex("reward_play_status")));
        r2.setAdvId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ADV_ID)));
        r2.setTtc_ct2(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_CT2)));
        r2.setTtc_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_TYPE)));
        r2.setRetarget_offer(r10.getInt(r10.getColumnIndex("retarget")));
        r2.setCampaignUnitId(r10.getString(r10.getColumnIndex("unitid")));
        r2.setNativeVideoTracking(com.mintegral.msdk.base.entity.CampaignEx.TrackingStr2Object(r10.getString(r10.getColumnIndex("native_ad_tracking"))));
        r2.setNativeVideoTrackingString(r10.getString(r10.getColumnIndex("native_ad_tracking")));
        r2.setVideo_end_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.VIDEO_END_TYPE)));
        r2.setendcard_url(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.ENDCARD_URL)));
        r2.setPlayable_ads_without_video(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.PLAYABLE_ADS_WITHOUT_VIDEO)));
        r2.setLoopbackString(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK)));
        r2.setLoopbackMap(com.mintegral.msdk.base.entity.CampaignEx.loopbackStrToMap(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK))));
        r2.setRewardTemplateMode(com.mintegral.msdk.base.entity.CampaignEx.c.a(r10.getString(r10.getColumnIndex("reward_teamplate"))));
        r2.setVideoMD5Value(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_VIDEO_MD5)));
        r2.setGifUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GIF_URL)));
        r2.setNvT2(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NV_T2)));
        r2.setClickTimeOutInterval(r10.getInt(r10.getColumnIndex("c_coi")));
        r2.setcUA(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_C_UA)));
        r2.setImpUA(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMP_UA)));
        r2.setGhId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_ID)));
        r2.setGhPath(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_PATH)));
        r2.setBindId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_BIND_ID)));
        r2.setOc_time(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TIME)));
        r2.setOc_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TYPE)));
        r2.setT_list(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_T_LIST)));
        r3 = r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_ADCHOICE));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x03e9, code lost:
        if (android.text.TextUtils.isEmpty(r3) != false) goto L_0x03f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x03eb, code lost:
        r2.setAdchoice(com.mintegral.msdk.base.entity.CampaignEx.a.a(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x03f2, code lost:
        r2.setAdchoiceSizeHeight(r10.getInt(r10.getColumnIndex("adchoice_size_height")));
        r2.setAdchoiceSizeWidth(r10.getInt(r10.getColumnIndex("adchoice_size_width")));
        r2.setPlct(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCT)));
        r2.setPlctb(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCTB)));
        r2.setBannerUrl(r10.getString(r10.getColumnIndex("banner_url")));
        r2.setBannerHtml(r10.getString(r10.getColumnIndex("banner_html")));
        r2.setCreativeId((long) r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_CREATIVE_ID)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0458, code lost:
        if (r10.getInt(r10.getColumnIndex("is_bid_campaign")) != 1) goto L_0x045c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x045a, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x045c, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x045d, code lost:
        r2.setIsBidCampaign(r3);
        r2.setBidToken(r10.getString(r10.getColumnIndex(com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns.COLUMN_BID_TOKEN)));
        r2.setAdType(r10.getInt(r10.getColumnIndex("ad_type")));
        r2.setMraid(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_MRAID)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0491, code lost:
        if (r10.getInt(r10.getColumnIndex("is_mraid_campaign")) != 1) goto L_0x0494;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0493, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0494, code lost:
        r2.setIsMraid(r4);
        r2.setOmid(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OMID)));
        r1.add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x04a9, code lost:
        if (r10 == null) goto L_0x04ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x04af, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x04b0, code lost:
        if (r10 == null) goto L_0x04cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x04ba, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x04cc, code lost:
        if (r10 != null) goto L_0x04b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x04d0, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x04d8, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x04dc, code lost:
        throw r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
        if (r10 == null) goto L_0x04b0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x04d4 A[SYNTHETIC, Splitter:B:60:0x04d4] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized java.util.List<com.mintegral.msdk.base.entity.CampaignEx> l(java.lang.String r10) {
        /*
            r9 = this;
            monitor-enter(r9)
            r0 = 0
            java.lang.Object r1 = new java.lang.Object     // Catch:{ Exception -> 0x04c3, all -> 0x04be }
            r1.<init>()     // Catch:{ Exception -> 0x04c3, all -> 0x04be }
            monitor-enter(r1)     // Catch:{ Exception -> 0x04c3, all -> 0x04be }
            android.database.sqlite.SQLiteDatabase r2 = r9.a()     // Catch:{ all -> 0x04b6 }
            android.database.Cursor r10 = r2.rawQuery(r10, r0)     // Catch:{ all -> 0x04b6 }
            monitor-exit(r1)     // Catch:{ all -> 0x04bc }
            if (r10 == 0) goto L_0x04b0
            int r1 = r10.getCount()     // Catch:{ Exception -> 0x04ba }
            if (r1 <= 0) goto L_0x04b0
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x04ba }
            r1.<init>()     // Catch:{ Exception -> 0x04ba }
        L_0x001e:
            boolean r2 = r10.moveToNext()     // Catch:{ Exception -> 0x04ba }
            if (r2 == 0) goto L_0x04a9
            com.mintegral.msdk.base.entity.CampaignEx r2 = new com.mintegral.msdk.base.entity.CampaignEx     // Catch:{ Exception -> 0x04ba }
            r2.<init>()     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setId(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "tab"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setTab(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "package_name"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setPackageName(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "app_name"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setAppName(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "app_desc"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setAppDesc(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "app_size"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setSize(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "image_size"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setImageSize(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "icon_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setIconUrl(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "image_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setImageUrl(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "impression_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setImpressionURL(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "notice_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setNoticeUrl(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "download_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setClickURL(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "wtick"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setWtick(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "deeplink_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setDeepLinkUrl(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "only_impression"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setOnlyImpressionURL(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "preclick"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r4 = 0
            r5 = 1
            if (r3 != r5) goto L_0x00fc
            r3 = 1
            goto L_0x00fd
        L_0x00fc:
            r3 = 0
        L_0x00fd:
            r2.setPreClick(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "template"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setTemplate(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "landing_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setLandingType(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "link_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setLinkType(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "click_mode"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setClick_mode(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "star"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            double r6 = java.lang.Double.parseDouble(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setRating(r6)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "cti"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setClickInterval(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "cpti"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setPreClickInterval(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "ts"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            long r6 = r10.getLong(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setTimestamp(r6)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "level"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setCacheLevel(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "adSource"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setType(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "ad_call"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setAdCall(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "fc_a"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setFca(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "fc_b"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setFcb(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "ad_url_list"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setAd_url_list(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "video_length"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setVideoLength(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "video_size"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setVideoSize(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "video_resolution"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setVideoResolution(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "endcard_click_result"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setEndcard_click_result(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "video_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setVideoUrlEncode(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "watch_mile"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setWatchMile(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "t_imp"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setTImp(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "bty"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setBty(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "advImp"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setAdvImp(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "offer_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setOfferType(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "guidelines"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setGuidelines(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "html_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setHtmlUrl(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "end_screen_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setEndScreenUrl(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "reward_name"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setRewardName(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "reward_amount"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setRewardAmount(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "reward_play_status"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setRewardPlayStatus(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "adv_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setAdvId(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "ttc_ct2"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setTtc_ct2(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "ttc_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setTtc_type(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "retarget"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setRetarget_offer(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "unitid"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setCampaignUnitId(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "native_ad_tracking"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            com.mintegral.msdk.base.entity.j r3 = com.mintegral.msdk.base.entity.CampaignEx.TrackingStr2Object(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setNativeVideoTracking(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "native_ad_tracking"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setNativeVideoTrackingString(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "video_end_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setVideo_end_type(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "endcard_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setendcard_url(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "playable_ads_without_video"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setPlayable_ads_without_video(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "loopback"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setLoopbackString(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "loopback"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            java.util.Map r3 = com.mintegral.msdk.base.entity.CampaignEx.loopbackStrToMap(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setLoopbackMap(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "reward_teamplate"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            com.mintegral.msdk.base.entity.CampaignEx$c r3 = com.mintegral.msdk.base.entity.CampaignEx.c.a(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setRewardTemplateMode(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "md5_file"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setVideoMD5Value(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "gif_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setGifUrl(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "nv_t2"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setNvT2(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "c_coi"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setClickTimeOutInterval(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "c_ua"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setcUA(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "imp_ua"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setImpUA(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "gh_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setGhId(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "gh_path"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setGhPath(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "bind_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setBindId(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "oc_time"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setOc_time(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "oc_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setOc_type(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "t_list"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setT_list(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "adchoice"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            boolean r6 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x04ba }
            if (r6 != 0) goto L_0x03f2
            com.mintegral.msdk.base.entity.CampaignEx$a r3 = com.mintegral.msdk.base.entity.CampaignEx.a.a(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setAdchoice(r3)     // Catch:{ Exception -> 0x04ba }
        L_0x03f2:
            java.lang.String r3 = "adchoice_size_height"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setAdchoiceSizeHeight(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "adchoice_size_width"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setAdchoiceSizeWidth(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "plct"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            long r6 = r10.getLong(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setPlct(r6)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "plctb"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            long r6 = r10.getLong(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setPlctb(r6)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "banner_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setBannerUrl(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "banner_html"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setBannerHtml(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "creative_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            long r6 = (long) r3     // Catch:{ Exception -> 0x04ba }
            r2.setCreativeId(r6)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "is_bid_campaign"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            if (r3 != r5) goto L_0x045c
            r3 = 1
            goto L_0x045d
        L_0x045c:
            r3 = 0
        L_0x045d:
            r2.setIsBidCampaign(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "bid_token"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setBidToken(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "ad_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setAdType(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "mraid"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setMraid(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "is_mraid_campaign"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ba }
            if (r3 != r5) goto L_0x0494
            r4 = 1
        L_0x0494:
            r2.setIsMraid(r4)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = "omid"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ba }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ba }
            r2.setOmid(r3)     // Catch:{ Exception -> 0x04ba }
            r1.add(r2)     // Catch:{ Exception -> 0x04ba }
            goto L_0x001e
        L_0x04a9:
            if (r10 == 0) goto L_0x04ae
            r10.close()     // Catch:{ all -> 0x04d8 }
        L_0x04ae:
            monitor-exit(r9)
            return r1
        L_0x04b0:
            if (r10 == 0) goto L_0x04cf
        L_0x04b2:
            r10.close()     // Catch:{ all -> 0x04d8 }
            goto L_0x04cf
        L_0x04b6:
            r2 = move-exception
            r10 = r0
        L_0x04b8:
            monitor-exit(r1)     // Catch:{ all -> 0x04bc }
            throw r2     // Catch:{ Exception -> 0x04ba }
        L_0x04ba:
            r1 = move-exception
            goto L_0x04c5
        L_0x04bc:
            r2 = move-exception
            goto L_0x04b8
        L_0x04be:
            r10 = move-exception
            r8 = r0
            r0 = r10
            r10 = r8
            goto L_0x04d2
        L_0x04c3:
            r1 = move-exception
            r10 = r0
        L_0x04c5:
            boolean r2 = com.mintegral.msdk.MIntegralConstans.DEBUG     // Catch:{ all -> 0x04d1 }
            if (r2 == 0) goto L_0x04cc
            r1.printStackTrace()     // Catch:{ all -> 0x04d1 }
        L_0x04cc:
            if (r10 == 0) goto L_0x04cf
            goto L_0x04b2
        L_0x04cf:
            monitor-exit(r9)
            return r0
        L_0x04d1:
            r0 = move-exception
        L_0x04d2:
            if (r10 == 0) goto L_0x04da
            r10.close()     // Catch:{ all -> 0x04d8 }
            goto L_0x04da
        L_0x04d8:
            r10 = move-exception
            goto L_0x04db
        L_0x04da:
            throw r0     // Catch:{ all -> 0x04d8 }
        L_0x04db:
            monitor-exit(r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.l(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003c, code lost:
        if (r10.getCount() <= 0) goto L_0x04d4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003e, code lost:
        r1 = new java.util.ArrayList();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0047, code lost:
        if (r10.moveToNext() == false) goto L_0x04ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0049, code lost:
        r2 = new com.mintegral.msdk.base.entity.CampaignEx();
        r2.setId(r10.getString(r10.getColumnIndex("id")));
        r2.setTab(r10.getInt(r10.getColumnIndex("tab")));
        r2.setPackageName(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PACKAGE_NAME)));
        r2.setAppName(r10.getString(r10.getColumnIndex("app_name")));
        r2.setAppDesc(r10.getString(r10.getColumnIndex("app_desc")));
        r2.setSize(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_APP_SIZE)));
        r2.setImageSize(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMAGE_SIZE)));
        r2.setIconUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ICON_URL)));
        r2.setImageUrl(r10.getString(r10.getColumnIndex("image_url")));
        r2.setImpressionURL(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMPRESSION_URL)));
        r2.setNoticeUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NOTICE_URL)));
        r2.setClickURL(r10.getString(r10.getColumnIndex("download_url")));
        r2.setWtick(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_WITHOUT_INSTALL_CHECK)));
        r2.setDeepLinkUrl(r10.getString(r10.getColumnIndex("deeplink_url")));
        r2.setOnlyImpressionURL(r10.getString(r10.getColumnIndex("only_impression")));
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x011d, code lost:
        if (r10.getInt(r10.getColumnIndex("preclick")) != 1) goto L_0x0121;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x011f, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0121, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0122, code lost:
        r2.setPreClick(r3);
        r2.setTemplate(r10.getInt(r10.getColumnIndex("template")));
        r2.setLandingType(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LANDING_TYPE)));
        r2.setLinkType(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LINK_TYPE)));
        r2.setClick_mode(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_CLICK_MODE)));
        r2.setRating(java.lang.Double.parseDouble(r10.getString(r10.getColumnIndex("star"))));
        r2.setClickInterval(r10.getInt(r10.getColumnIndex("cti")));
        r2.setPreClickInterval(r10.getInt(r10.getColumnIndex("cpti")));
        r2.setTimestamp(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ST_TS)));
        r2.setCacheLevel(r10.getInt(r10.getColumnIndex("level")));
        r2.setType(r10.getInt(r10.getColumnIndex("adSource")));
        r2.setAdCall(r10.getString(r10.getColumnIndex("ad_call")));
        r2.setFca(r10.getInt(r10.getColumnIndex("fc_a")));
        r2.setFcb(r10.getInt(r10.getColumnIndex("fc_b")));
        r2.setAd_url_list(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_AD_URL_LIST)));
        r2.setVideoLength(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_LENGTHL)));
        r2.setVideoSize(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_SIZE)));
        r2.setVideoResolution(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_RESOLUTION)));
        r2.setEndcard_click_result(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ENDCARD_CLICK)));
        r2.setVideoUrlEncode(r10.getString(r10.getColumnIndex("video_url")));
        r2.setWatchMile(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_WATCH_MILE)));
        r2.setTImp(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_T_IMP)));
        r2.setBty(r10.getInt(r10.getColumnIndex("bty")));
        r2.setAdvImp(r10.getString(r10.getColumnIndex("advImp")));
        r2.setOfferType(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_OFFER_TYPE)));
        r2.setGuidelines(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GUIDELINES)));
        r2.setHtmlUrl(r10.getString(r10.getColumnIndex("html_url")));
        r2.setEndScreenUrl(r10.getString(r10.getColumnIndex("end_screen_url")));
        r2.setRewardName(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_NAME)));
        r2.setRewardAmount(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_AMOUNT)));
        r2.setRewardPlayStatus(r10.getInt(r10.getColumnIndex("reward_play_status")));
        r2.setAdvId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ADV_ID)));
        r2.setTtc_ct2(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_CT2)));
        r2.setTtc_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_TYPE)));
        r2.setRetarget_offer(r10.getInt(r10.getColumnIndex("retarget")));
        r2.setCampaignUnitId(r10.getString(r10.getColumnIndex("unitid")));
        r2.setNativeVideoTracking(com.mintegral.msdk.base.entity.CampaignEx.TrackingStr2Object(r10.getString(r10.getColumnIndex("native_ad_tracking"))));
        r2.setNativeVideoTrackingString(r10.getString(r10.getColumnIndex("native_ad_tracking")));
        r2.setVideo_end_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.VIDEO_END_TYPE)));
        r2.setendcard_url(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.ENDCARD_URL)));
        r2.setPlayable_ads_without_video(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.PLAYABLE_ADS_WITHOUT_VIDEO)));
        r2.setLoopbackString(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK)));
        r2.setLoopbackMap(com.mintegral.msdk.base.entity.CampaignEx.loopbackStrToMap(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK))));
        r2.setRewardTemplateMode(com.mintegral.msdk.base.entity.CampaignEx.c.a(r10.getString(r10.getColumnIndex("reward_teamplate"))));
        r2.setVideoMD5Value(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_VIDEO_MD5)));
        r2.setGifUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GIF_URL)));
        r2.setNvT2(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NV_T2)));
        r2.setClickTimeOutInterval(r10.getInt(r10.getColumnIndex("c_coi")));
        r2.setcUA(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_C_UA)));
        r2.setImpUA(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMP_UA)));
        r2.setGhId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_ID)));
        r2.setGhPath(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_PATH)));
        r2.setBindId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_BIND_ID)));
        r2.setOc_time(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TIME)));
        r2.setOc_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TYPE)));
        r2.setT_list(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_T_LIST)));
        r3 = r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_ADCHOICE));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x040e, code lost:
        if (android.text.TextUtils.isEmpty(r3) != false) goto L_0x0417;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0410, code lost:
        r2.setAdchoice(com.mintegral.msdk.base.entity.CampaignEx.a.a(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0417, code lost:
        r2.setAdchoiceSizeHeight(r10.getInt(r10.getColumnIndex("adchoice_size_height")));
        r2.setAdchoiceSizeWidth(r10.getInt(r10.getColumnIndex("adchoice_size_width")));
        r2.setPlct(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCT)));
        r2.setPlctb(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCTB)));
        r2.setBannerUrl(r10.getString(r10.getColumnIndex("banner_url")));
        r2.setBannerHtml(r10.getString(r10.getColumnIndex("banner_html")));
        r2.setCreativeId((long) r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_CREATIVE_ID)));
        r2.setAdType(r10.getInt(r10.getColumnIndex("ad_type")));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x048a, code lost:
        if (r10.getInt(r10.getColumnIndex("is_bid_campaign")) != 1) goto L_0x048e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x048c, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x048e, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x048f, code lost:
        r2.setIsBidCampaign(r3);
        r2.setBidToken(r10.getString(r10.getColumnIndex(com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns.COLUMN_BID_TOKEN)));
        r2.setMraid(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_MRAID)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x04b6, code lost:
        if (r10.getInt(r10.getColumnIndex("is_mraid_campaign")) != 1) goto L_0x04b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x04b8, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x04b9, code lost:
        r2.setIsMraid(r4);
        r2.setOmid(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OMID)));
        r1.add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x04ce, code lost:
        if (r10 == null) goto L_0x04d3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x04d0, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x04d3, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x04d4, code lost:
        if (r10 == null) goto L_0x04ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x04db, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x04ea, code lost:
        if (r10 == null) goto L_0x04ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x04ec, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x04ef, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0036, code lost:
        if (r10 == null) goto L_0x04d4;
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x04e5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.mintegral.msdk.base.entity.CampaignEx> f(java.lang.String r10) {
        /*
            r9 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = " WHERE unitid = '"
            r0.<init>(r1)
            r0.append(r10)
            java.lang.String r10 = "' AND level = 0 AND adSource = 1 AND reward_play_status = 0"
            r0.append(r10)
            java.lang.String r10 = r0.toString()
            java.lang.String r0 = ""
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "SELECT * FROM campaign"
            r1.<init>(r2)
            r1.append(r10)
            r1.append(r0)
            java.lang.String r10 = r1.toString()
            r0 = 0
            java.lang.Object r1 = new java.lang.Object     // Catch:{ Exception -> 0x04e9, all -> 0x04df }
            r1.<init>()     // Catch:{ Exception -> 0x04e9, all -> 0x04df }
            monitor-enter(r1)     // Catch:{ Exception -> 0x04e9, all -> 0x04df }
            android.database.sqlite.SQLiteDatabase r2 = r9.a()     // Catch:{ all -> 0x04d7 }
            android.database.Cursor r10 = r2.rawQuery(r10, r0)     // Catch:{ all -> 0x04d7 }
            monitor-exit(r1)     // Catch:{ all -> 0x04dd }
            if (r10 == 0) goto L_0x04d4
            int r1 = r10.getCount()     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            if (r1 <= 0) goto L_0x04d4
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r1.<init>()     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
        L_0x0043:
            boolean r2 = r10.moveToNext()     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            if (r2 == 0) goto L_0x04ce
            com.mintegral.msdk.base.entity.CampaignEx r2 = new com.mintegral.msdk.base.entity.CampaignEx     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.<init>()     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setId(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "tab"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setTab(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "package_name"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setPackageName(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "app_name"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setAppName(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "app_desc"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setAppDesc(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "app_size"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setSize(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "image_size"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setImageSize(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "icon_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setIconUrl(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "image_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setImageUrl(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "impression_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setImpressionURL(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "notice_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setNoticeUrl(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "download_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setClickURL(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "wtick"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setWtick(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "deeplink_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setDeepLinkUrl(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "only_impression"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setOnlyImpressionURL(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "preclick"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r4 = 0
            r5 = 1
            if (r3 != r5) goto L_0x0121
            r3 = 1
            goto L_0x0122
        L_0x0121:
            r3 = 0
        L_0x0122:
            r2.setPreClick(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "template"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setTemplate(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "landing_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setLandingType(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "link_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setLinkType(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "click_mode"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setClick_mode(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "star"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            double r6 = java.lang.Double.parseDouble(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setRating(r6)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "cti"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setClickInterval(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "cpti"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setPreClickInterval(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "ts"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            long r6 = r10.getLong(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setTimestamp(r6)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "level"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setCacheLevel(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "adSource"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setType(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "ad_call"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setAdCall(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "fc_a"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setFca(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "fc_b"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setFcb(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "ad_url_list"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setAd_url_list(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "video_length"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setVideoLength(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "video_size"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setVideoSize(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "video_resolution"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setVideoResolution(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "endcard_click_result"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setEndcard_click_result(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "video_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setVideoUrlEncode(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "watch_mile"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setWatchMile(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "t_imp"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setTImp(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "bty"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setBty(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "advImp"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setAdvImp(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "offer_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setOfferType(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "guidelines"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setGuidelines(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "html_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setHtmlUrl(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "end_screen_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setEndScreenUrl(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "reward_name"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setRewardName(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "reward_amount"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setRewardAmount(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "reward_play_status"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setRewardPlayStatus(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "adv_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setAdvId(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "ttc_ct2"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setTtc_ct2(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "ttc_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setTtc_type(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "retarget"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setRetarget_offer(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "unitid"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setCampaignUnitId(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "native_ad_tracking"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            com.mintegral.msdk.base.entity.j r3 = com.mintegral.msdk.base.entity.CampaignEx.TrackingStr2Object(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setNativeVideoTracking(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "native_ad_tracking"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setNativeVideoTrackingString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "video_end_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setVideo_end_type(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "endcard_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setendcard_url(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "playable_ads_without_video"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setPlayable_ads_without_video(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "loopback"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setLoopbackString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "loopback"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.util.Map r3 = com.mintegral.msdk.base.entity.CampaignEx.loopbackStrToMap(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setLoopbackMap(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "reward_teamplate"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            com.mintegral.msdk.base.entity.CampaignEx$c r3 = com.mintegral.msdk.base.entity.CampaignEx.c.a(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setRewardTemplateMode(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "md5_file"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setVideoMD5Value(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "gif_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setGifUrl(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "nv_t2"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setNvT2(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "c_coi"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setClickTimeOutInterval(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "c_ua"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setcUA(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "imp_ua"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setImpUA(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "gh_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setGhId(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "gh_path"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setGhPath(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "bind_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setBindId(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "oc_time"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setOc_time(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "oc_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setOc_type(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "t_list"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setT_list(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "adchoice"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            boolean r6 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            if (r6 != 0) goto L_0x0417
            com.mintegral.msdk.base.entity.CampaignEx$a r3 = com.mintegral.msdk.base.entity.CampaignEx.a.a(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setAdchoice(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
        L_0x0417:
            java.lang.String r3 = "adchoice_size_height"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setAdchoiceSizeHeight(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "adchoice_size_width"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setAdchoiceSizeWidth(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "plct"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            long r6 = r10.getLong(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setPlct(r6)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "plctb"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            long r6 = r10.getLong(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setPlctb(r6)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "banner_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setBannerUrl(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "banner_html"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setBannerHtml(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "creative_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            long r6 = (long) r3     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setCreativeId(r6)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "ad_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setAdType(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "is_bid_campaign"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            if (r3 != r5) goto L_0x048e
            r3 = 1
            goto L_0x048f
        L_0x048e:
            r3 = 0
        L_0x048f:
            r2.setIsBidCampaign(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "bid_token"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setBidToken(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "mraid"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setMraid(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "is_mraid_campaign"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            if (r3 != r5) goto L_0x04b9
            r4 = 1
        L_0x04b9:
            r2.setIsMraid(r4)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = "omid"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r2.setOmid(r3)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            r1.add(r2)     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
            goto L_0x0043
        L_0x04ce:
            if (r10 == 0) goto L_0x04d3
            r10.close()
        L_0x04d3:
            return r1
        L_0x04d4:
            if (r10 == 0) goto L_0x04ef
            goto L_0x04ec
        L_0x04d7:
            r2 = move-exception
            r10 = r0
        L_0x04d9:
            monitor-exit(r1)     // Catch:{ all -> 0x04dd }
            throw r2     // Catch:{ Exception -> 0x04ea, all -> 0x04db }
        L_0x04db:
            r0 = move-exception
            goto L_0x04e3
        L_0x04dd:
            r2 = move-exception
            goto L_0x04d9
        L_0x04df:
            r10 = move-exception
            r8 = r0
            r0 = r10
            r10 = r8
        L_0x04e3:
            if (r10 == 0) goto L_0x04e8
            r10.close()
        L_0x04e8:
            throw r0
        L_0x04e9:
            r10 = r0
        L_0x04ea:
            if (r10 == 0) goto L_0x04ef
        L_0x04ec:
            r10.close()
        L_0x04ef:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.f(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0044, code lost:
        if (r9.getCount() <= 0) goto L_0x0537;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0046, code lost:
        r0 = new java.util.ArrayList();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004f, code lost:
        if (r9.moveToNext() == false) goto L_0x0531;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0051, code lost:
        r1 = new com.mintegral.msdk.base.entity.CampaignEx();
        r1.setId(r9.getString(r9.getColumnIndex("id")));
        r1.setTab(r9.getInt(r9.getColumnIndex("tab")));
        r1.setPackageName(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PACKAGE_NAME)));
        r1.setAppName(r9.getString(r9.getColumnIndex("app_name")));
        r1.setAppDesc(r9.getString(r9.getColumnIndex("app_desc")));
        r1.setSize(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_APP_SIZE)));
        r1.setImageSize(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMAGE_SIZE)));
        r1.setIconUrl(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ICON_URL)));
        r1.setImageUrl(r9.getString(r9.getColumnIndex("image_url")));
        r1.setImpressionURL(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMPRESSION_URL)));
        r1.setNoticeUrl(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NOTICE_URL)));
        r1.setClickURL(r9.getString(r9.getColumnIndex("download_url")));
        r1.setWtick(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_WITHOUT_INSTALL_CHECK)));
        r1.setDeepLinkUrl(r9.getString(r9.getColumnIndex("deeplink_url")));
        r1.setOnlyImpressionURL(r9.getString(r9.getColumnIndex("only_impression")));
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0125, code lost:
        if (r9.getInt(r9.getColumnIndex("preclick")) != 1) goto L_0x0129;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0127, code lost:
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0129, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x012a, code lost:
        r1.setPreClick(r2);
        r1.setTemplate(r9.getInt(r9.getColumnIndex("template")));
        r1.setLandingType(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LANDING_TYPE)));
        r1.setLinkType(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LINK_TYPE)));
        r1.setClick_mode(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_CLICK_MODE)));
        r1.setRating(java.lang.Double.parseDouble(r9.getString(r9.getColumnIndex("star"))));
        r1.setClickInterval(r9.getInt(r9.getColumnIndex("cti")));
        r1.setPreClickInterval(r9.getInt(r9.getColumnIndex("cpti")));
        r1.setTimestamp(r9.getLong(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ST_TS)));
        r1.setCacheLevel(r9.getInt(r9.getColumnIndex("level")));
        r1.setAdCall(r9.getString(r9.getColumnIndex("ad_call")));
        r1.setFca(r9.getInt(r9.getColumnIndex("fc_a")));
        r1.setFcb(r9.getInt(r9.getColumnIndex("fc_b")));
        r1.setAd_url_list(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_AD_URL_LIST)));
        r1.setVideoLength(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_LENGTHL)));
        r1.setVideoSize(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_SIZE)));
        r1.setVideoResolution(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_RESOLUTION)));
        r1.setEndcard_click_result(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ENDCARD_CLICK)));
        r1.setVideoUrlEncode(r9.getString(r9.getColumnIndex("video_url")));
        r1.setWatchMile(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_WATCH_MILE)));
        r1.setTImp(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_T_IMP)));
        r1.setBty(r9.getInt(r9.getColumnIndex("bty")));
        r1.setAdvImp(r9.getString(r9.getColumnIndex("advImp")));
        r1.setGuidelines(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GUIDELINES)));
        r1.setOfferType(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_OFFER_TYPE)));
        r1.setHtmlUrl(r9.getString(r9.getColumnIndex("html_url")));
        r1.setGuidelines(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GUIDELINES)));
        r1.setHtmlUrl(r9.getString(r9.getColumnIndex("html_url")));
        r1.setEndScreenUrl(r9.getString(r9.getColumnIndex("end_screen_url")));
        r1.setRewardName(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_NAME)));
        r1.setRewardAmount(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_AMOUNT)));
        r1.setRewardPlayStatus(r9.getInt(r9.getColumnIndex("reward_play_status")));
        r1.setAdvId(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ADV_ID)));
        r1.setTtc_ct2(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_CT2)));
        r1.setTtc_type(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_TYPE)));
        r1.setRetarget_offer(r9.getInt(r9.getColumnIndex("retarget")));
        r1.setCampaignUnitId(r9.getString(r9.getColumnIndex("unitid")));
        r1.setNativeVideoTracking(com.mintegral.msdk.base.entity.CampaignEx.TrackingStr2Object(r9.getString(r9.getColumnIndex("native_ad_tracking"))));
        r1.setNativeVideoTrackingString(r9.getString(r9.getColumnIndex("native_ad_tracking")));
        r1.setVideo_end_type(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.VIDEO_END_TYPE)));
        r1.setendcard_url(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.ENDCARD_URL)));
        r1.setPlayable_ads_without_video(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.PLAYABLE_ADS_WITHOUT_VIDEO)));
        r1.setLoopbackString(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK)));
        r1.setLoopbackMap(com.mintegral.msdk.base.entity.CampaignEx.loopbackStrToMap(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK))));
        r1.setRewardTemplateMode(com.mintegral.msdk.base.entity.CampaignEx.c.a(r9.getString(r9.getColumnIndex("reward_teamplate"))));
        r1.setVideoMD5Value(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_VIDEO_MD5)));
        r1.setGifUrl(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GIF_URL)));
        r1.setNvT2(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NV_T2)));
        r1.setClickTimeOutInterval(r9.getInt(r9.getColumnIndex("c_coi")));
        r1.setcUA(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_C_UA)));
        r1.setImpUA(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMP_UA)));
        r1.setKeyIaOri(r9.getInt(r9.getColumnIndex("ia_ori")));
        r1.setAdType(r9.getInt(r9.getColumnIndex("ad_type")));
        r1.setIa_ext1(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IA_EXT1)));
        r1.setIa_ext2(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IA_EXT2)));
        r1.setKeyIaRst(r9.getInt(r9.getColumnIndex("ia_rst")));
        r1.setKeyIaUrl(r9.getString(r9.getColumnIndex("ia_url")));
        r1.setKeyIaIcon(r9.getString(r9.getColumnIndex("ia_icon")));
        r1.setGhId(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_ID)));
        r1.setGhPath(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_PATH)));
        r1.setBindId(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_BIND_ID)));
        r1.setOc_time(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TIME)));
        r1.setOc_type(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TYPE)));
        r1.setT_list(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_T_LIST)));
        r2 = r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_ADCHOICE));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x047e, code lost:
        if (android.text.TextUtils.isEmpty(r2) != false) goto L_0x0487;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0480, code lost:
        r1.setAdchoice(com.mintegral.msdk.base.entity.CampaignEx.a.a(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0487, code lost:
        r1.setAdchoiceSizeHeight(r9.getInt(r9.getColumnIndex("adchoice_size_height")));
        r1.setAdchoiceSizeWidth(r9.getInt(r9.getColumnIndex("adchoice_size_width")));
        r1.setPlct(r9.getLong(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCT)));
        r1.setPlctb(r9.getLong(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCTB)));
        r1.setBannerUrl(r9.getString(r9.getColumnIndex("banner_url")));
        r1.setBannerHtml(r9.getString(r9.getColumnIndex("banner_html")));
        r1.setCreativeId((long) r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_CREATIVE_ID)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x04ed, code lost:
        if (r9.getInt(r9.getColumnIndex("is_bid_campaign")) != 1) goto L_0x04f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x04ef, code lost:
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x04f1, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x04f2, code lost:
        r1.setIsBidCampaign(r2);
        r1.setBidToken(r9.getString(r9.getColumnIndex(com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns.COLUMN_BID_TOKEN)));
        r1.setMraid(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_MRAID)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0519, code lost:
        if (r9.getInt(r9.getColumnIndex("is_mraid_campaign")) != 1) goto L_0x051c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x051b, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x051c, code lost:
        r1.setIsMraid(r3);
        r1.setOmid(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OMID)));
        r0.add(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0531, code lost:
        if (r9 == null) goto L_0x0536;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0533, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0536, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0537, code lost:
        if (r9 == null) goto L_0x0552;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x053e, code lost:
        r10 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x054d, code lost:
        if (r9 == null) goto L_0x0552;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x054f, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0552, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x003e, code lost:
        if (r9 == null) goto L_0x0537;
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0548  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.mintegral.msdk.base.entity.CampaignEx> c(int r9, java.lang.String r10) {
        /*
            r8 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = " WHERE tab = "
            r0.<init>(r1)
            r0.append(r9)
            java.lang.String r9 = " AND unitid = '"
            r0.append(r9)
            r0.append(r10)
            java.lang.String r9 = "'"
            r0.append(r9)
            java.lang.String r9 = r0.toString()
            java.lang.String r10 = " LIMIT 20"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "SELECT * FROM campaign"
            r0.<init>(r1)
            r0.append(r9)
            r0.append(r10)
            java.lang.String r9 = r0.toString()
            r10 = 0
            java.lang.Object r0 = new java.lang.Object     // Catch:{ Exception -> 0x054c, all -> 0x0542 }
            r0.<init>()     // Catch:{ Exception -> 0x054c, all -> 0x0542 }
            monitor-enter(r0)     // Catch:{ Exception -> 0x054c, all -> 0x0542 }
            android.database.sqlite.SQLiteDatabase r1 = r8.a()     // Catch:{ all -> 0x053a }
            android.database.Cursor r9 = r1.rawQuery(r9, r10)     // Catch:{ all -> 0x053a }
            monitor-exit(r0)     // Catch:{ all -> 0x0540 }
            if (r9 == 0) goto L_0x0537
            int r0 = r9.getCount()     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            if (r0 <= 0) goto L_0x0537
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r0.<init>()     // Catch:{ Exception -> 0x054d, all -> 0x053e }
        L_0x004b:
            boolean r1 = r9.moveToNext()     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            if (r1 == 0) goto L_0x0531
            com.mintegral.msdk.base.entity.CampaignEx r1 = new com.mintegral.msdk.base.entity.CampaignEx     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.<init>()     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "id"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setId(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "tab"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setTab(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "package_name"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setPackageName(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "app_name"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setAppName(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "app_desc"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setAppDesc(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "app_size"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setSize(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "image_size"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setImageSize(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "icon_url"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setIconUrl(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "image_url"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setImageUrl(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "impression_url"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setImpressionURL(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "notice_url"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setNoticeUrl(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "download_url"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setClickURL(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "wtick"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setWtick(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "deeplink_url"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setDeepLinkUrl(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "only_impression"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setOnlyImpressionURL(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "preclick"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r3 = 0
            r4 = 1
            if (r2 != r4) goto L_0x0129
            r2 = 1
            goto L_0x012a
        L_0x0129:
            r2 = 0
        L_0x012a:
            r1.setPreClick(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "template"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setTemplate(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "landing_type"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setLandingType(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "link_type"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setLinkType(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "click_mode"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setClick_mode(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "star"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            double r5 = java.lang.Double.parseDouble(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setRating(r5)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "cti"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setClickInterval(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "cpti"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setPreClickInterval(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "ts"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            long r5 = r9.getLong(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setTimestamp(r5)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "level"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setCacheLevel(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "ad_call"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setAdCall(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "fc_a"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setFca(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "fc_b"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setFcb(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "ad_url_list"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setAd_url_list(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "video_length"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setVideoLength(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "video_size"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setVideoSize(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "video_resolution"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setVideoResolution(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "endcard_click_result"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setEndcard_click_result(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "video_url"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setVideoUrlEncode(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "watch_mile"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setWatchMile(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "t_imp"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setTImp(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "bty"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setBty(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "advImp"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setAdvImp(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "guidelines"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setGuidelines(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "offer_type"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setOfferType(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "html_url"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setHtmlUrl(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "guidelines"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setGuidelines(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "html_url"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setHtmlUrl(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "end_screen_url"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setEndScreenUrl(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "reward_name"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setRewardName(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "reward_amount"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setRewardAmount(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "reward_play_status"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setRewardPlayStatus(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "adv_id"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setAdvId(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "ttc_ct2"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setTtc_ct2(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "ttc_type"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setTtc_type(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "retarget"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setRetarget_offer(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "unitid"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setCampaignUnitId(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "native_ad_tracking"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            com.mintegral.msdk.base.entity.j r2 = com.mintegral.msdk.base.entity.CampaignEx.TrackingStr2Object(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setNativeVideoTracking(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "native_ad_tracking"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setNativeVideoTrackingString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "video_end_type"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setVideo_end_type(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "endcard_url"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setendcard_url(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "playable_ads_without_video"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setPlayable_ads_without_video(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "loopback"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setLoopbackString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "loopback"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.util.Map r2 = com.mintegral.msdk.base.entity.CampaignEx.loopbackStrToMap(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setLoopbackMap(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "reward_teamplate"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            com.mintegral.msdk.base.entity.CampaignEx$c r2 = com.mintegral.msdk.base.entity.CampaignEx.c.a(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setRewardTemplateMode(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "md5_file"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setVideoMD5Value(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "gif_url"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setGifUrl(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "nv_t2"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setNvT2(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "c_coi"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setClickTimeOutInterval(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "c_ua"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setcUA(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "imp_ua"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setImpUA(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "ia_ori"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setKeyIaOri(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "ad_type"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setAdType(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "ia_ext1"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setIa_ext1(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "ia_ext2"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setIa_ext2(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "ia_rst"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setKeyIaRst(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "ia_url"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setKeyIaUrl(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "ia_icon"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setKeyIaIcon(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "gh_id"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setGhId(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "gh_path"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setGhPath(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "bind_id"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setBindId(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "oc_time"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setOc_time(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "oc_type"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setOc_type(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "t_list"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setT_list(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "adchoice"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            boolean r5 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            if (r5 != 0) goto L_0x0487
            com.mintegral.msdk.base.entity.CampaignEx$a r2 = com.mintegral.msdk.base.entity.CampaignEx.a.a(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setAdchoice(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
        L_0x0487:
            java.lang.String r2 = "adchoice_size_height"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setAdchoiceSizeHeight(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "adchoice_size_width"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setAdchoiceSizeWidth(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "plct"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            long r5 = r9.getLong(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setPlct(r5)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "plctb"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            long r5 = r9.getLong(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setPlctb(r5)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "banner_url"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setBannerUrl(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "banner_html"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setBannerHtml(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "creative_id"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            long r5 = (long) r2     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setCreativeId(r5)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "is_bid_campaign"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            if (r2 != r4) goto L_0x04f1
            r2 = 1
            goto L_0x04f2
        L_0x04f1:
            r2 = 0
        L_0x04f2:
            r1.setIsBidCampaign(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "bid_token"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setBidToken(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "mraid"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setMraid(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "is_mraid_campaign"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            if (r2 != r4) goto L_0x051c
            r3 = 1
        L_0x051c:
            r1.setIsMraid(r3)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = "omid"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r1.setOmid(r2)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            r0.add(r1)     // Catch:{ Exception -> 0x054d, all -> 0x053e }
            goto L_0x004b
        L_0x0531:
            if (r9 == 0) goto L_0x0536
            r9.close()
        L_0x0536:
            return r0
        L_0x0537:
            if (r9 == 0) goto L_0x0552
            goto L_0x054f
        L_0x053a:
            r1 = move-exception
            r9 = r10
        L_0x053c:
            monitor-exit(r0)     // Catch:{ all -> 0x0540 }
            throw r1     // Catch:{ Exception -> 0x054d, all -> 0x053e }
        L_0x053e:
            r10 = move-exception
            goto L_0x0546
        L_0x0540:
            r1 = move-exception
            goto L_0x053c
        L_0x0542:
            r9 = move-exception
            r7 = r10
            r10 = r9
            r9 = r7
        L_0x0546:
            if (r9 == 0) goto L_0x054b
            r9.close()
        L_0x054b:
            throw r10
        L_0x054c:
            r9 = r10
        L_0x054d:
            if (r9 == 0) goto L_0x0552
        L_0x054f:
            r9.close()
        L_0x0552:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.c(int, java.lang.String):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0037, code lost:
        if (r10.getCount() <= 0) goto L_0x056b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0039, code lost:
        r1 = new java.util.ArrayList();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0042, code lost:
        if (r10.moveToNext() == false) goto L_0x0565;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0044, code lost:
        r2 = new com.mintegral.msdk.base.entity.CampaignEx();
        r2.setId(r10.getString(r10.getColumnIndex("id")));
        r2.setTab(r10.getInt(r10.getColumnIndex("tab")));
        r2.setPackageName(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PACKAGE_NAME)));
        r2.setAppName(r10.getString(r10.getColumnIndex("app_name")));
        r2.setAppDesc(r10.getString(r10.getColumnIndex("app_desc")));
        r2.setSize(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_APP_SIZE)));
        r2.setImageSize(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMAGE_SIZE)));
        r2.setIconUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ICON_URL)));
        r2.setImageUrl(r10.getString(r10.getColumnIndex("image_url")));
        r2.setImpressionURL(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMPRESSION_URL)));
        r2.setNoticeUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NOTICE_URL)));
        r2.setClickURL(r10.getString(r10.getColumnIndex("download_url")));
        r2.setWtick(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_WITHOUT_INSTALL_CHECK)));
        r2.setDeepLinkUrl(r10.getString(r10.getColumnIndex("deeplink_url")));
        r2.setOnlyImpressionURL(r10.getString(r10.getColumnIndex("only_impression")));
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0118, code lost:
        if (r10.getInt(r10.getColumnIndex("preclick")) != 1) goto L_0x011c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x011a, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x011c, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x011d, code lost:
        r2.setPreClick(r3);
        r2.setTemplate(r10.getInt(r10.getColumnIndex("template")));
        r2.setLandingType(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LANDING_TYPE)));
        r2.setLinkType(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LINK_TYPE)));
        r2.setClick_mode(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_CLICK_MODE)));
        r2.setRating(java.lang.Double.parseDouble(r10.getString(r10.getColumnIndex("star"))));
        r2.setClickInterval(r10.getInt(r10.getColumnIndex("cti")));
        r2.setPreClickInterval(r10.getInt(r10.getColumnIndex("cpti")));
        r2.setTimestamp(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ST_TS)));
        r2.setCacheLevel(r10.getInt(r10.getColumnIndex("level")));
        r2.setAdCall(r10.getString(r10.getColumnIndex("ad_call")));
        r2.setFca(r10.getInt(r10.getColumnIndex("fc_a")));
        r2.setFcb(r10.getInt(r10.getColumnIndex("fc_b")));
        r2.setAd_url_list(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_AD_URL_LIST)));
        r2.setVideoLength(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_LENGTHL)));
        r2.setVideoSize(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_SIZE)));
        r2.setVideoResolution(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_RESOLUTION)));
        r2.setEndcard_click_result(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ENDCARD_CLICK)));
        r2.setVideoUrlEncode(r10.getString(r10.getColumnIndex("video_url")));
        r2.setWatchMile(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_WATCH_MILE)));
        r2.setTImp(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_T_IMP)));
        r2.setBty(r10.getInt(r10.getColumnIndex("bty")));
        r2.setAdvImp(r10.getString(r10.getColumnIndex("advImp")));
        r2.setGuidelines(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GUIDELINES)));
        r2.setOfferType(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_OFFER_TYPE)));
        r2.setHtmlUrl(r10.getString(r10.getColumnIndex("html_url")));
        r2.setGuidelines(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GUIDELINES)));
        r2.setHtmlUrl(r10.getString(r10.getColumnIndex("html_url")));
        r2.setEndScreenUrl(r10.getString(r10.getColumnIndex("end_screen_url")));
        r2.setRewardName(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_NAME)));
        r2.setRewardAmount(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_AMOUNT)));
        r2.setRewardPlayStatus(r10.getInt(r10.getColumnIndex("reward_play_status")));
        r2.setAdvId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ADV_ID)));
        r2.setTtc_ct2(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_CT2)));
        r2.setTtc_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_TYPE)));
        r2.setRetarget_offer(r10.getInt(r10.getColumnIndex("retarget")));
        r2.setCampaignUnitId(r10.getString(r10.getColumnIndex("unitid")));
        r2.setNativeVideoTracking(com.mintegral.msdk.base.entity.CampaignEx.TrackingStr2Object(r10.getString(r10.getColumnIndex("native_ad_tracking"))));
        r2.setNativeVideoTrackingString(r10.getString(r10.getColumnIndex("native_ad_tracking")));
        r2.setVideo_end_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.VIDEO_END_TYPE)));
        r2.setendcard_url(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.ENDCARD_URL)));
        r2.setPlayable_ads_without_video(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.PLAYABLE_ADS_WITHOUT_VIDEO)));
        r2.setLoopbackString(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK)));
        r2.setLoopbackMap(com.mintegral.msdk.base.entity.CampaignEx.loopbackStrToMap(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK))));
        r2.setRewardTemplateMode(com.mintegral.msdk.base.entity.CampaignEx.c.a(r10.getString(r10.getColumnIndex("reward_teamplate"))));
        r2.setVideoMD5Value(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_VIDEO_MD5)));
        r2.setGifUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GIF_URL)));
        r2.setNvT2(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NV_T2)));
        r2.setClickTimeOutInterval(r10.getInt(r10.getColumnIndex("c_coi")));
        r2.setcUA(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_C_UA)));
        r2.setImpUA(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMP_UA)));
        r2.setIsDeleted(r10.getInt(r10.getColumnIndex("is_deleted")));
        r2.setIsClick(r10.getInt(r10.getColumnIndex("is_click")));
        r2.setIsAddSuccesful(r10.getInt(r10.getColumnIndex("is_add_sucesful")));
        r2.setKeyIaOri(r10.getInt(r10.getColumnIndex("ia_ori")));
        r2.setAdType(r10.getInt(r10.getColumnIndex("ad_type")));
        r2.setIa_ext1(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IA_EXT1)));
        r2.setIa_ext2(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IA_EXT2)));
        r2.setKeyIaRst(r10.getInt(r10.getColumnIndex("ia_rst")));
        r2.setKeyIaUrl(r10.getString(r10.getColumnIndex("ia_url")));
        r2.setKeyIaIcon(r10.getString(r10.getColumnIndex("ia_icon")));
        r2.setIsDownLoadZip(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IS_DOWNLOAD)));
        r2.setInteractiveCache(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IA_CACHE)));
        r2.setGhId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_ID)));
        r2.setGhPath(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_PATH)));
        r2.setBindId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_BIND_ID)));
        r2.setOc_time(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TIME)));
        r2.setOc_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TYPE)));
        r2.setT_list(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_T_LIST)));
        r3 = r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_ADCHOICE));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x04b2, code lost:
        if (android.text.TextUtils.isEmpty(r3) != false) goto L_0x04bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x04b4, code lost:
        r2.setAdchoice(com.mintegral.msdk.base.entity.CampaignEx.a.a(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x04bb, code lost:
        r2.setAdchoiceSizeHeight(r10.getInt(r10.getColumnIndex("adchoice_size_height")));
        r2.setAdchoiceSizeWidth(r10.getInt(r10.getColumnIndex("adchoice_size_width")));
        r2.setPlct(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCT)));
        r2.setPlctb(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCTB)));
        r2.setBannerUrl(r10.getString(r10.getColumnIndex("banner_url")));
        r2.setBannerHtml(r10.getString(r10.getColumnIndex("banner_html")));
        r2.setCreativeId((long) r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_CREATIVE_ID)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0521, code lost:
        if (r10.getInt(r10.getColumnIndex("is_bid_campaign")) != 1) goto L_0x0525;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0523, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0525, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0526, code lost:
        r2.setIsBidCampaign(r3);
        r2.setBidToken(r10.getString(r10.getColumnIndex(com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns.COLUMN_BID_TOKEN)));
        r2.setMraid(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_MRAID)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x054d, code lost:
        if (r10.getInt(r10.getColumnIndex("is_mraid_campaign")) != 1) goto L_0x0550;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x054f, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0550, code lost:
        r2.setIsMraid(r4);
        r2.setOmid(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OMID)));
        r1.add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0565, code lost:
        if (r10 == null) goto L_0x056a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0567, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x056a, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x056b, code lost:
        if (r10 == null) goto L_0x0586;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0572, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0581, code lost:
        if (r10 == null) goto L_0x0586;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0583, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0586, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0031, code lost:
        if (r10 == null) goto L_0x056b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x057c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.mintegral.msdk.base.entity.CampaignEx> g(java.lang.String r10) {
        /*
            r9 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = " WHERE unitid = '"
            r0.<init>(r1)
            r0.append(r10)
            java.lang.String r10 = "' AND is_deleted=0"
            r0.append(r10)
            java.lang.String r10 = r0.toString()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "SELECT * FROM campaign"
            r0.<init>(r1)
            r0.append(r10)
            java.lang.String r10 = r0.toString()
            r0 = 0
            java.lang.Object r1 = new java.lang.Object     // Catch:{ Exception -> 0x0580, all -> 0x0576 }
            r1.<init>()     // Catch:{ Exception -> 0x0580, all -> 0x0576 }
            monitor-enter(r1)     // Catch:{ Exception -> 0x0580, all -> 0x0576 }
            android.database.sqlite.SQLiteDatabase r2 = r9.a()     // Catch:{ all -> 0x056e }
            android.database.Cursor r10 = r2.rawQuery(r10, r0)     // Catch:{ all -> 0x056e }
            monitor-exit(r1)     // Catch:{ all -> 0x0574 }
            if (r10 == 0) goto L_0x056b
            int r1 = r10.getCount()     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            if (r1 <= 0) goto L_0x056b
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r1.<init>()     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
        L_0x003e:
            boolean r2 = r10.moveToNext()     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            if (r2 == 0) goto L_0x0565
            com.mintegral.msdk.base.entity.CampaignEx r2 = new com.mintegral.msdk.base.entity.CampaignEx     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.<init>()     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setId(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "tab"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setTab(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "package_name"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setPackageName(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "app_name"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setAppName(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "app_desc"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setAppDesc(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "app_size"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setSize(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "image_size"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setImageSize(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "icon_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setIconUrl(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "image_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setImageUrl(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "impression_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setImpressionURL(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "notice_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setNoticeUrl(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "download_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setClickURL(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "wtick"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setWtick(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "deeplink_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setDeepLinkUrl(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "only_impression"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setOnlyImpressionURL(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "preclick"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r4 = 0
            r5 = 1
            if (r3 != r5) goto L_0x011c
            r3 = 1
            goto L_0x011d
        L_0x011c:
            r3 = 0
        L_0x011d:
            r2.setPreClick(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "template"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setTemplate(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "landing_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setLandingType(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "link_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setLinkType(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "click_mode"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setClick_mode(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "star"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            double r6 = java.lang.Double.parseDouble(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setRating(r6)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "cti"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setClickInterval(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "cpti"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setPreClickInterval(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "ts"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            long r6 = r10.getLong(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setTimestamp(r6)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "level"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setCacheLevel(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "ad_call"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setAdCall(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "fc_a"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setFca(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "fc_b"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setFcb(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "ad_url_list"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setAd_url_list(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "video_length"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setVideoLength(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "video_size"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setVideoSize(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "video_resolution"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setVideoResolution(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "endcard_click_result"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setEndcard_click_result(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "video_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setVideoUrlEncode(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "watch_mile"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setWatchMile(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "t_imp"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setTImp(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "bty"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setBty(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "advImp"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setAdvImp(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "guidelines"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setGuidelines(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "offer_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setOfferType(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "html_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setHtmlUrl(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "guidelines"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setGuidelines(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "html_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setHtmlUrl(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "end_screen_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setEndScreenUrl(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "reward_name"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setRewardName(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "reward_amount"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setRewardAmount(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "reward_play_status"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setRewardPlayStatus(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "adv_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setAdvId(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "ttc_ct2"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setTtc_ct2(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "ttc_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setTtc_type(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "retarget"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setRetarget_offer(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "unitid"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setCampaignUnitId(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "native_ad_tracking"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            com.mintegral.msdk.base.entity.j r3 = com.mintegral.msdk.base.entity.CampaignEx.TrackingStr2Object(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setNativeVideoTracking(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "native_ad_tracking"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setNativeVideoTrackingString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "video_end_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setVideo_end_type(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "endcard_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setendcard_url(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "playable_ads_without_video"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setPlayable_ads_without_video(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "loopback"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setLoopbackString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "loopback"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.util.Map r3 = com.mintegral.msdk.base.entity.CampaignEx.loopbackStrToMap(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setLoopbackMap(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "reward_teamplate"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            com.mintegral.msdk.base.entity.CampaignEx$c r3 = com.mintegral.msdk.base.entity.CampaignEx.c.a(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setRewardTemplateMode(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "md5_file"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setVideoMD5Value(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "gif_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setGifUrl(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "nv_t2"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setNvT2(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "c_coi"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setClickTimeOutInterval(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "c_ua"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setcUA(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "imp_ua"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setImpUA(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "is_deleted"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setIsDeleted(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "is_click"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setIsClick(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "is_add_sucesful"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setIsAddSuccesful(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "ia_ori"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setKeyIaOri(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "ad_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setAdType(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "ia_ext1"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setIa_ext1(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "ia_ext2"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setIa_ext2(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "ia_rst"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setKeyIaRst(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "ia_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setKeyIaUrl(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "ia_icon"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setKeyIaIcon(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "is_download_zip"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setIsDownLoadZip(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "ia_cache"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setInteractiveCache(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "gh_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setGhId(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "gh_path"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setGhPath(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "bind_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setBindId(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "oc_time"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setOc_time(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "oc_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setOc_type(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "t_list"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setT_list(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "adchoice"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            boolean r6 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            if (r6 != 0) goto L_0x04bb
            com.mintegral.msdk.base.entity.CampaignEx$a r3 = com.mintegral.msdk.base.entity.CampaignEx.a.a(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setAdchoice(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
        L_0x04bb:
            java.lang.String r3 = "adchoice_size_height"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setAdchoiceSizeHeight(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "adchoice_size_width"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setAdchoiceSizeWidth(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "plct"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            long r6 = r10.getLong(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setPlct(r6)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "plctb"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            long r6 = r10.getLong(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setPlctb(r6)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "banner_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setBannerUrl(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "banner_html"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setBannerHtml(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "creative_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            long r6 = (long) r3     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setCreativeId(r6)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "is_bid_campaign"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            if (r3 != r5) goto L_0x0525
            r3 = 1
            goto L_0x0526
        L_0x0525:
            r3 = 0
        L_0x0526:
            r2.setIsBidCampaign(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "bid_token"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setBidToken(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "mraid"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setMraid(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "is_mraid_campaign"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            if (r3 != r5) goto L_0x0550
            r4 = 1
        L_0x0550:
            r2.setIsMraid(r4)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = "omid"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r2.setOmid(r3)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            r1.add(r2)     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
            goto L_0x003e
        L_0x0565:
            if (r10 == 0) goto L_0x056a
            r10.close()
        L_0x056a:
            return r1
        L_0x056b:
            if (r10 == 0) goto L_0x0586
            goto L_0x0583
        L_0x056e:
            r2 = move-exception
            r10 = r0
        L_0x0570:
            monitor-exit(r1)     // Catch:{ all -> 0x0574 }
            throw r2     // Catch:{ Exception -> 0x0581, all -> 0x0572 }
        L_0x0572:
            r0 = move-exception
            goto L_0x057a
        L_0x0574:
            r2 = move-exception
            goto L_0x0570
        L_0x0576:
            r10 = move-exception
            r8 = r0
            r0 = r10
            r10 = r8
        L_0x057a:
            if (r10 == 0) goto L_0x057f
            r10.close()
        L_0x057f:
            throw r0
        L_0x0580:
            r10 = r0
        L_0x0581:
            if (r10 == 0) goto L_0x0586
        L_0x0583:
            r10.close()
        L_0x0586:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.g(java.lang.String):java.util.List");
    }

    public final List<CampaignEx> h(String str) {
        return m("SELECT * FROM campaign" + (" WHERE unitid = '" + str + "'"));
    }

    public final List<CampaignEx> a(JSONArray jSONArray, String str) {
        if (jSONArray == null) {
            return null;
        }
        try {
            StringBuffer stringBuffer = new StringBuffer(" WHERE unitid = '");
            stringBuffer.append(str);
            stringBuffer.append("' AND (");
            for (int i = 0; i < jSONArray.length(); i++) {
                Long valueOf = Long.valueOf(jSONArray.getLong(i));
                if (i != 0) {
                    stringBuffer.append(" or ");
                }
                stringBuffer.append("id = '");
                stringBuffer.append(valueOf);
                stringBuffer.append("'");
            }
            stringBuffer.append(" )");
            String str2 = b;
            g.a(str2, "tabWhereSb : " + ((Object) stringBuffer));
            return m("SELECT * FROM campaign" + ((Object) stringBuffer) + " AND ia_cache = 'onelevel'");
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0016, code lost:
        if (r10.getCount() <= 0) goto L_0x054a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0018, code lost:
        r1 = new java.util.ArrayList();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0021, code lost:
        if (r10.moveToNext() == false) goto L_0x0544;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0023, code lost:
        r2 = new com.mintegral.msdk.base.entity.CampaignEx();
        r2.setId(r10.getString(r10.getColumnIndex("id")));
        r2.setTab(r10.getInt(r10.getColumnIndex("tab")));
        r2.setPackageName(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PACKAGE_NAME)));
        r2.setAppName(r10.getString(r10.getColumnIndex("app_name")));
        r2.setAppDesc(r10.getString(r10.getColumnIndex("app_desc")));
        r2.setSize(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_APP_SIZE)));
        r2.setImageSize(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMAGE_SIZE)));
        r2.setIconUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ICON_URL)));
        r2.setImageUrl(r10.getString(r10.getColumnIndex("image_url")));
        r2.setImpressionURL(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMPRESSION_URL)));
        r2.setNoticeUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NOTICE_URL)));
        r2.setClickURL(r10.getString(r10.getColumnIndex("download_url")));
        r2.setWtick(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_WITHOUT_INSTALL_CHECK)));
        r2.setDeepLinkUrl(r10.getString(r10.getColumnIndex("deeplink_url")));
        r2.setOnlyImpressionURL(r10.getString(r10.getColumnIndex("only_impression")));
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00f7, code lost:
        if (r10.getInt(r10.getColumnIndex("preclick")) != 1) goto L_0x00fb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00f9, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00fb, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00fc, code lost:
        r2.setPreClick(r3);
        r2.setTemplate(r10.getInt(r10.getColumnIndex("template")));
        r2.setLandingType(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LANDING_TYPE)));
        r2.setLinkType(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LINK_TYPE)));
        r2.setClick_mode(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_CLICK_MODE)));
        r2.setRating(java.lang.Double.parseDouble(r10.getString(r10.getColumnIndex("star"))));
        r2.setClickInterval(r10.getInt(r10.getColumnIndex("cti")));
        r2.setPreClickInterval(r10.getInt(r10.getColumnIndex("cpti")));
        r2.setTimestamp(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ST_TS)));
        r2.setCacheLevel(r10.getInt(r10.getColumnIndex("level")));
        r2.setAdCall(r10.getString(r10.getColumnIndex("ad_call")));
        r2.setFca(r10.getInt(r10.getColumnIndex("fc_a")));
        r2.setFcb(r10.getInt(r10.getColumnIndex("fc_b")));
        r2.setAd_url_list(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_AD_URL_LIST)));
        r2.setVideoLength(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_LENGTHL)));
        r2.setVideoSize(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_SIZE)));
        r2.setVideoResolution(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_RESOLUTION)));
        r2.setEndcard_click_result(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ENDCARD_CLICK)));
        r2.setVideoUrlEncode(r10.getString(r10.getColumnIndex("video_url")));
        r2.setWatchMile(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_WATCH_MILE)));
        r2.setTImp(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_T_IMP)));
        r2.setBty(r10.getInt(r10.getColumnIndex("bty")));
        r2.setAdvImp(r10.getString(r10.getColumnIndex("advImp")));
        r2.setGuidelines(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GUIDELINES)));
        r2.setOfferType(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_OFFER_TYPE)));
        r2.setHtmlUrl(r10.getString(r10.getColumnIndex("html_url")));
        r2.setGuidelines(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GUIDELINES)));
        r2.setHtmlUrl(r10.getString(r10.getColumnIndex("html_url")));
        r2.setEndScreenUrl(r10.getString(r10.getColumnIndex("end_screen_url")));
        r2.setRewardName(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_NAME)));
        r2.setRewardAmount(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_AMOUNT)));
        r2.setRewardPlayStatus(r10.getInt(r10.getColumnIndex("reward_play_status")));
        r2.setAdvId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ADV_ID)));
        r2.setTtc_ct2(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_CT2)));
        r2.setTtc_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_TYPE)));
        r2.setRetarget_offer(r10.getInt(r10.getColumnIndex("retarget")));
        r2.setCampaignUnitId(r10.getString(r10.getColumnIndex("unitid")));
        r2.setNativeVideoTracking(com.mintegral.msdk.base.entity.CampaignEx.TrackingStr2Object(r10.getString(r10.getColumnIndex("native_ad_tracking"))));
        r2.setNativeVideoTrackingString(r10.getString(r10.getColumnIndex("native_ad_tracking")));
        r2.setVideo_end_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.VIDEO_END_TYPE)));
        r2.setendcard_url(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.ENDCARD_URL)));
        r2.setPlayable_ads_without_video(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.PLAYABLE_ADS_WITHOUT_VIDEO)));
        r2.setLoopbackString(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK)));
        r2.setLoopbackMap(com.mintegral.msdk.base.entity.CampaignEx.loopbackStrToMap(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK))));
        r2.setRewardTemplateMode(com.mintegral.msdk.base.entity.CampaignEx.c.a(r10.getString(r10.getColumnIndex("reward_teamplate"))));
        r2.setVideoMD5Value(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_VIDEO_MD5)));
        r2.setGifUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GIF_URL)));
        r2.setNvT2(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NV_T2)));
        r2.setClickTimeOutInterval(r10.getInt(r10.getColumnIndex("c_coi")));
        r2.setcUA(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_C_UA)));
        r2.setImpUA(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMP_UA)));
        r2.setIsDeleted(r10.getInt(r10.getColumnIndex("is_deleted")));
        r2.setIsClick(r10.getInt(r10.getColumnIndex("is_click")));
        r2.setIsAddSuccesful(r10.getInt(r10.getColumnIndex("is_add_sucesful")));
        r2.setKeyIaOri(r10.getInt(r10.getColumnIndex("ia_ori")));
        r2.setAdType(r10.getInt(r10.getColumnIndex("ad_type")));
        r2.setIa_ext1(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IA_EXT1)));
        r2.setIa_ext2(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IA_EXT2)));
        r2.setKeyIaRst(r10.getInt(r10.getColumnIndex("ia_rst")));
        r2.setKeyIaUrl(r10.getString(r10.getColumnIndex("ia_url")));
        r2.setKeyIaIcon(r10.getString(r10.getColumnIndex("ia_icon")));
        r2.setIsDownLoadZip(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IS_DOWNLOAD)));
        r2.setInteractiveCache(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IA_CACHE)));
        r2.setGhId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_ID)));
        r2.setGhPath(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_PATH)));
        r2.setBindId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_BIND_ID)));
        r2.setOc_time(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TIME)));
        r2.setOc_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TYPE)));
        r2.setT_list(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_T_LIST)));
        r3 = r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_ADCHOICE));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0491, code lost:
        if (android.text.TextUtils.isEmpty(r3) != false) goto L_0x049a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0493, code lost:
        r2.setAdchoice(com.mintegral.msdk.base.entity.CampaignEx.a.a(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x049a, code lost:
        r2.setAdchoiceSizeHeight(r10.getInt(r10.getColumnIndex("adchoice_size_height")));
        r2.setAdchoiceSizeWidth(r10.getInt(r10.getColumnIndex("adchoice_size_width")));
        r2.setPlct(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCT)));
        r2.setPlctb(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCTB)));
        r2.setBannerUrl(r10.getString(r10.getColumnIndex("banner_url")));
        r2.setBannerHtml(r10.getString(r10.getColumnIndex("banner_html")));
        r2.setCreativeId((long) r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_CREATIVE_ID)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0500, code lost:
        if (r10.getInt(r10.getColumnIndex("is_bid_campaign")) != 1) goto L_0x0504;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0502, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0504, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0505, code lost:
        r2.setIsBidCampaign(r3);
        r2.setBidToken(r10.getString(r10.getColumnIndex(com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns.COLUMN_BID_TOKEN)));
        r2.setMraid(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_MRAID)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x052c, code lost:
        if (r10.getInt(r10.getColumnIndex("is_mraid_campaign")) != 1) goto L_0x052f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x052e, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x052f, code lost:
        r2.setIsMraid(r4);
        r2.setOmid(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OMID)));
        r1.add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0544, code lost:
        if (r10 == null) goto L_0x0549;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0546, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0549, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x054a, code lost:
        if (r10 == null) goto L_0x0565;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0551, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0560, code lost:
        if (r10 == null) goto L_0x0565;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0562, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0565, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0010, code lost:
        if (r10 == null) goto L_0x054a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x055b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List<com.mintegral.msdk.base.entity.CampaignEx> m(java.lang.String r10) {
        /*
            r9 = this;
            r0 = 0
            java.lang.Object r1 = new java.lang.Object     // Catch:{ Exception -> 0x055f, all -> 0x0555 }
            r1.<init>()     // Catch:{ Exception -> 0x055f, all -> 0x0555 }
            monitor-enter(r1)     // Catch:{ Exception -> 0x055f, all -> 0x0555 }
            android.database.sqlite.SQLiteDatabase r2 = r9.a()     // Catch:{ all -> 0x054d }
            android.database.Cursor r10 = r2.rawQuery(r10, r0)     // Catch:{ all -> 0x054d }
            monitor-exit(r1)     // Catch:{ all -> 0x0553 }
            if (r10 == 0) goto L_0x054a
            int r1 = r10.getCount()     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            if (r1 <= 0) goto L_0x054a
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r1.<init>()     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
        L_0x001d:
            boolean r2 = r10.moveToNext()     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            if (r2 == 0) goto L_0x0544
            com.mintegral.msdk.base.entity.CampaignEx r2 = new com.mintegral.msdk.base.entity.CampaignEx     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.<init>()     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setId(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "tab"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setTab(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "package_name"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setPackageName(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "app_name"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setAppName(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "app_desc"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setAppDesc(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "app_size"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setSize(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "image_size"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setImageSize(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "icon_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setIconUrl(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "image_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setImageUrl(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "impression_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setImpressionURL(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "notice_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setNoticeUrl(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "download_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setClickURL(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "wtick"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setWtick(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "deeplink_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setDeepLinkUrl(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "only_impression"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setOnlyImpressionURL(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "preclick"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r4 = 0
            r5 = 1
            if (r3 != r5) goto L_0x00fb
            r3 = 1
            goto L_0x00fc
        L_0x00fb:
            r3 = 0
        L_0x00fc:
            r2.setPreClick(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "template"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setTemplate(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "landing_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setLandingType(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "link_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setLinkType(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "click_mode"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setClick_mode(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "star"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            double r6 = java.lang.Double.parseDouble(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setRating(r6)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "cti"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setClickInterval(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "cpti"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setPreClickInterval(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "ts"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            long r6 = r10.getLong(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setTimestamp(r6)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "level"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setCacheLevel(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "ad_call"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setAdCall(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "fc_a"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setFca(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "fc_b"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setFcb(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "ad_url_list"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setAd_url_list(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "video_length"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setVideoLength(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "video_size"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setVideoSize(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "video_resolution"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setVideoResolution(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "endcard_click_result"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setEndcard_click_result(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "video_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setVideoUrlEncode(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "watch_mile"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setWatchMile(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "t_imp"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setTImp(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "bty"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setBty(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "advImp"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setAdvImp(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "guidelines"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setGuidelines(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "offer_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setOfferType(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "html_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setHtmlUrl(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "guidelines"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setGuidelines(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "html_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setHtmlUrl(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "end_screen_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setEndScreenUrl(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "reward_name"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setRewardName(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "reward_amount"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setRewardAmount(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "reward_play_status"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setRewardPlayStatus(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "adv_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setAdvId(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "ttc_ct2"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setTtc_ct2(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "ttc_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setTtc_type(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "retarget"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setRetarget_offer(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "unitid"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setCampaignUnitId(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "native_ad_tracking"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            com.mintegral.msdk.base.entity.j r3 = com.mintegral.msdk.base.entity.CampaignEx.TrackingStr2Object(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setNativeVideoTracking(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "native_ad_tracking"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setNativeVideoTrackingString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "video_end_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setVideo_end_type(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "endcard_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setendcard_url(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "playable_ads_without_video"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setPlayable_ads_without_video(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "loopback"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setLoopbackString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "loopback"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.util.Map r3 = com.mintegral.msdk.base.entity.CampaignEx.loopbackStrToMap(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setLoopbackMap(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "reward_teamplate"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            com.mintegral.msdk.base.entity.CampaignEx$c r3 = com.mintegral.msdk.base.entity.CampaignEx.c.a(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setRewardTemplateMode(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "md5_file"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setVideoMD5Value(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "gif_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setGifUrl(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "nv_t2"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setNvT2(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "c_coi"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setClickTimeOutInterval(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "c_ua"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setcUA(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "imp_ua"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setImpUA(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "is_deleted"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setIsDeleted(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "is_click"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setIsClick(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "is_add_sucesful"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setIsAddSuccesful(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "ia_ori"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setKeyIaOri(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "ad_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setAdType(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "ia_ext1"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setIa_ext1(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "ia_ext2"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setIa_ext2(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "ia_rst"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setKeyIaRst(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "ia_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setKeyIaUrl(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "ia_icon"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setKeyIaIcon(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "is_download_zip"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setIsDownLoadZip(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "ia_cache"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setInteractiveCache(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "gh_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setGhId(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "gh_path"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setGhPath(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "bind_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setBindId(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "oc_time"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setOc_time(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "oc_type"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setOc_type(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "t_list"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setT_list(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "adchoice"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            boolean r6 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            if (r6 != 0) goto L_0x049a
            com.mintegral.msdk.base.entity.CampaignEx$a r3 = com.mintegral.msdk.base.entity.CampaignEx.a.a(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setAdchoice(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
        L_0x049a:
            java.lang.String r3 = "adchoice_size_height"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setAdchoiceSizeHeight(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "adchoice_size_width"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setAdchoiceSizeWidth(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "plct"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            long r6 = r10.getLong(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setPlct(r6)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "plctb"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            long r6 = r10.getLong(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setPlctb(r6)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "banner_url"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setBannerUrl(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "banner_html"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setBannerHtml(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "creative_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            long r6 = (long) r3     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setCreativeId(r6)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "is_bid_campaign"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            if (r3 != r5) goto L_0x0504
            r3 = 1
            goto L_0x0505
        L_0x0504:
            r3 = 0
        L_0x0505:
            r2.setIsBidCampaign(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "bid_token"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setBidToken(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "mraid"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setMraid(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "is_mraid_campaign"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            int r3 = r10.getInt(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            if (r3 != r5) goto L_0x052f
            r4 = 1
        L_0x052f:
            r2.setIsMraid(r4)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = "omid"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            java.lang.String r3 = r10.getString(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r2.setOmid(r3)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            r1.add(r2)     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
            goto L_0x001d
        L_0x0544:
            if (r10 == 0) goto L_0x0549
            r10.close()
        L_0x0549:
            return r1
        L_0x054a:
            if (r10 == 0) goto L_0x0565
            goto L_0x0562
        L_0x054d:
            r2 = move-exception
            r10 = r0
        L_0x054f:
            monitor-exit(r1)     // Catch:{ all -> 0x0553 }
            throw r2     // Catch:{ Exception -> 0x0560, all -> 0x0551 }
        L_0x0551:
            r0 = move-exception
            goto L_0x0559
        L_0x0553:
            r2 = move-exception
            goto L_0x054f
        L_0x0555:
            r10 = move-exception
            r8 = r0
            r0 = r10
            r10 = r8
        L_0x0559:
            if (r10 == 0) goto L_0x055e
            r10.close()
        L_0x055e:
            throw r0
        L_0x055f:
            r10 = r0
        L_0x0560:
            if (r10 == 0) goto L_0x0565
        L_0x0562:
            r10.close()
        L_0x0565:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.m(java.lang.String):java.util.List");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v2, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r0v4, types: [java.util.List<java.lang.String>] */
    /* JADX WARN: Type inference failed for: r0v5 */
    /* JADX WARN: Type inference failed for: r0v9 */
    /* JADX WARN: Type inference failed for: r0v11 */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0040, code lost:
        r3 = r0;
        r0 = r5;
        r5 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0044, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0045, code lost:
        r3 = r0;
        r0 = r5;
        r5 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005e, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0065, code lost:
        r0.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0044 A[ExcHandler: all (r0v6 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:4:0x001e] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0065  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> i(java.lang.String r5) {
        /*
            r4 = this;
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0057 }
            java.lang.String r2 = "select id from campaign where unitid='"
            r1.<init>(r2)     // Catch:{ Exception -> 0x0057 }
            r1.append(r5)     // Catch:{ Exception -> 0x0057 }
            java.lang.String r5 = "' and reward_play_status=1"
            r1.append(r5)     // Catch:{ Exception -> 0x0057 }
            java.lang.String r5 = r1.toString()     // Catch:{ Exception -> 0x0057 }
            android.database.sqlite.SQLiteDatabase r1 = r4.a()     // Catch:{ Exception -> 0x0057 }
            android.database.Cursor r5 = r1.rawQuery(r5, r0)     // Catch:{ Exception -> 0x0057 }
            if (r5 == 0) goto L_0x004f
            int r1 = r5.getCount()     // Catch:{ Exception -> 0x0049, all -> 0x0044 }
            if (r1 <= 0) goto L_0x004f
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x0049, all -> 0x0044 }
            r1.<init>()     // Catch:{ Exception -> 0x0049, all -> 0x0044 }
        L_0x0029:
            boolean r0 = r5.moveToNext()     // Catch:{ Exception -> 0x003f, all -> 0x0044 }
            if (r0 == 0) goto L_0x003d
            java.lang.String r0 = "id"
            int r0 = r5.getColumnIndex(r0)     // Catch:{ Exception -> 0x003f, all -> 0x0044 }
            java.lang.String r0 = r5.getString(r0)     // Catch:{ Exception -> 0x003f, all -> 0x0044 }
            r1.add(r0)     // Catch:{ Exception -> 0x003f, all -> 0x0044 }
            goto L_0x0029
        L_0x003d:
            r0 = r1
            goto L_0x004f
        L_0x003f:
            r0 = move-exception
            r3 = r0
            r0 = r5
            r5 = r3
            goto L_0x0059
        L_0x0044:
            r0 = move-exception
            r3 = r0
            r0 = r5
            r5 = r3
            goto L_0x0063
        L_0x0049:
            r1 = move-exception
            r3 = r0
            r0 = r5
            r5 = r1
            r1 = r3
            goto L_0x0059
        L_0x004f:
            if (r5 == 0) goto L_0x0062
            r5.close()
            goto L_0x0062
        L_0x0055:
            r5 = move-exception
            goto L_0x0063
        L_0x0057:
            r5 = move-exception
            r1 = r0
        L_0x0059:
            r5.printStackTrace()     // Catch:{ all -> 0x0055 }
            if (r0 == 0) goto L_0x0061
            r0.close()
        L_0x0061:
            r0 = r1
        L_0x0062:
            return r0
        L_0x0063:
            if (r0 == 0) goto L_0x0068
            r0.close()
        L_0x0068:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.i(java.lang.String):java.util.List");
    }

    private Cursor n(String str) {
        try {
            return a().rawQuery("SELECT * FROM campaign where unitid ='" + str + "'", null);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        if (r4 != null) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002a, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0038, code lost:
        if (r4 != null) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003b, code lost:
        return r0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x003f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> j(java.lang.String r4) {
        /*
            r3 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1 = 0
            android.database.Cursor r4 = r3.n(r4)     // Catch:{ Exception -> 0x0031, all -> 0x002e }
            if (r4 == 0) goto L_0x0028
            int r1 = r4.getCount()     // Catch:{ Exception -> 0x0026 }
            if (r1 <= 0) goto L_0x0028
        L_0x0012:
            boolean r1 = r4.moveToNext()     // Catch:{ Exception -> 0x0026 }
            if (r1 == 0) goto L_0x0028
            java.lang.String r1 = "video_url"
            int r1 = r4.getColumnIndex(r1)     // Catch:{ Exception -> 0x0026 }
            java.lang.String r1 = r4.getString(r1)     // Catch:{ Exception -> 0x0026 }
            r0.add(r1)     // Catch:{ Exception -> 0x0026 }
            goto L_0x0012
        L_0x0026:
            r1 = move-exception
            goto L_0x0035
        L_0x0028:
            if (r4 == 0) goto L_0x003b
        L_0x002a:
            r4.close()
            goto L_0x003b
        L_0x002e:
            r0 = move-exception
            r4 = r1
            goto L_0x003d
        L_0x0031:
            r4 = move-exception
            r2 = r1
            r1 = r4
            r4 = r2
        L_0x0035:
            r1.printStackTrace()     // Catch:{ all -> 0x003c }
            if (r4 == 0) goto L_0x003b
            goto L_0x002a
        L_0x003b:
            return r0
        L_0x003c:
            r0 = move-exception
        L_0x003d:
            if (r4 == 0) goto L_0x0042
            r4.close()
        L_0x0042:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.j(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0033, code lost:
        if (r4 != null) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0035, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0043, code lost:
        if (r4 != null) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0046, code lost:
        return r0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x004a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> k(java.lang.String r4) {
        /*
            r3 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1 = 0
            android.database.Cursor r4 = r3.n(r4)     // Catch:{ Exception -> 0x003c, all -> 0x0039 }
            if (r4 == 0) goto L_0x0033
            int r1 = r4.getCount()     // Catch:{ Exception -> 0x0031 }
            if (r1 <= 0) goto L_0x0033
        L_0x0012:
            boolean r1 = r4.moveToNext()     // Catch:{ Exception -> 0x0031 }
            if (r1 == 0) goto L_0x0033
            if (r4 == 0) goto L_0x002b
            int r1 = r4.getCount()     // Catch:{ Exception -> 0x0031 }
            if (r1 <= 0) goto L_0x002b
            java.lang.String r1 = "id"
            int r1 = r4.getColumnIndex(r1)     // Catch:{ Exception -> 0x0031 }
            java.lang.String r1 = r4.getString(r1)     // Catch:{ Exception -> 0x0031 }
            goto L_0x002d
        L_0x002b:
            java.lang.String r1 = ""
        L_0x002d:
            r0.add(r1)     // Catch:{ Exception -> 0x0031 }
            goto L_0x0012
        L_0x0031:
            r1 = move-exception
            goto L_0x0040
        L_0x0033:
            if (r4 == 0) goto L_0x0046
        L_0x0035:
            r4.close()
            goto L_0x0046
        L_0x0039:
            r0 = move-exception
            r4 = r1
            goto L_0x0048
        L_0x003c:
            r4 = move-exception
            r2 = r1
            r1 = r4
            r4 = r2
        L_0x0040:
            r1.printStackTrace()     // Catch:{ all -> 0x0047 }
            if (r4 == 0) goto L_0x0046
            goto L_0x0035
        L_0x0046:
            return r0
        L_0x0047:
            r0 = move-exception
        L_0x0048:
            if (r4 == 0) goto L_0x004d
            r4.close()
        L_0x004d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.k(java.lang.String):java.util.List");
    }

    private static CampaignEx a(Cursor cursor) {
        if (cursor == null || cursor.getCount() <= 0) {
            return null;
        }
        CampaignEx campaignEx = new CampaignEx();
        campaignEx.setId(cursor.getString(cursor.getColumnIndex("id")));
        campaignEx.setTab(cursor.getInt(cursor.getColumnIndex("tab")));
        campaignEx.setPackageName(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_PACKAGE_NAME)));
        campaignEx.setAppName(cursor.getString(cursor.getColumnIndex("app_name")));
        campaignEx.setAppDesc(cursor.getString(cursor.getColumnIndex("app_desc")));
        campaignEx.setSize(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_APP_SIZE)));
        campaignEx.setImageSize(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_IMAGE_SIZE)));
        campaignEx.setIconUrl(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_ICON_URL)));
        campaignEx.setImageUrl(cursor.getString(cursor.getColumnIndex("image_url")));
        campaignEx.setImpressionURL(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_IMPRESSION_URL)));
        campaignEx.setNoticeUrl(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_NOTICE_URL)));
        campaignEx.setClickURL(cursor.getString(cursor.getColumnIndex("download_url")));
        campaignEx.setWtick(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_WITHOUT_INSTALL_CHECK)));
        campaignEx.setDeepLinkUrl(cursor.getString(cursor.getColumnIndex("deeplink_url")));
        campaignEx.setOnlyImpressionURL(cursor.getString(cursor.getColumnIndex("only_impression")));
        boolean z = false;
        campaignEx.setPreClick(cursor.getInt(cursor.getColumnIndex("preclick")) == 1);
        campaignEx.setTemplate(cursor.getInt(cursor.getColumnIndex("template")));
        campaignEx.setLandingType(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_LANDING_TYPE)));
        campaignEx.setLinkType(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_LINK_TYPE)));
        campaignEx.setClick_mode(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_CLICK_MODE)));
        campaignEx.setRating(Double.parseDouble(cursor.getString(cursor.getColumnIndex("star"))));
        campaignEx.setClickInterval(cursor.getInt(cursor.getColumnIndex("cti")));
        campaignEx.setPreClickInterval(cursor.getInt(cursor.getColumnIndex("cpti")));
        campaignEx.setTimestamp(cursor.getLong(cursor.getColumnIndex(CampaignEx.JSON_KEY_ST_TS)));
        campaignEx.setCacheLevel(cursor.getInt(cursor.getColumnIndex("level")));
        campaignEx.setAdCall(cursor.getString(cursor.getColumnIndex("ad_call")));
        campaignEx.setFcb(cursor.getInt(cursor.getColumnIndex("fc_b")));
        campaignEx.setAd_url_list(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_AD_URL_LIST)));
        campaignEx.setVideoLength(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_VIDEO_LENGTHL)));
        campaignEx.setVideoSize(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_VIDEO_SIZE)));
        campaignEx.setVideoResolution(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_VIDEO_RESOLUTION)));
        campaignEx.setEndcard_click_result(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_ENDCARD_CLICK)));
        campaignEx.setVideoUrlEncode(cursor.getString(cursor.getColumnIndex("video_url")));
        campaignEx.setWatchMile(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_WATCH_MILE)));
        campaignEx.setTImp(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_T_IMP)));
        campaignEx.setBty(cursor.getInt(cursor.getColumnIndex("bty")));
        campaignEx.setAdvImp(cursor.getString(cursor.getColumnIndex("advImp")));
        campaignEx.setGuidelines(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_GUIDELINES)));
        campaignEx.setOfferType(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_OFFER_TYPE)));
        campaignEx.setHtmlUrl(cursor.getString(cursor.getColumnIndex("html_url")));
        campaignEx.setGuidelines(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_GUIDELINES)));
        campaignEx.setHtmlUrl(cursor.getString(cursor.getColumnIndex("html_url")));
        campaignEx.setEndScreenUrl(cursor.getString(cursor.getColumnIndex("end_screen_url")));
        campaignEx.setRewardName(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_REWARD_NAME)));
        campaignEx.setRewardAmount(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_REWARD_AMOUNT)));
        campaignEx.setRewardPlayStatus(cursor.getInt(cursor.getColumnIndex("reward_play_status")));
        campaignEx.setAdvId(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_ADV_ID)));
        campaignEx.setTtc_ct2(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_TTC_CT2)));
        campaignEx.setTtc_type(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_TTC_TYPE)));
        campaignEx.setRetarget_offer(cursor.getInt(cursor.getColumnIndex("retarget")));
        campaignEx.setCampaignUnitId(cursor.getString(cursor.getColumnIndex("unitid")));
        campaignEx.setNativeVideoTracking(CampaignEx.TrackingStr2Object(cursor.getString(cursor.getColumnIndex("native_ad_tracking"))));
        campaignEx.setNativeVideoTrackingString(cursor.getString(cursor.getColumnIndex("native_ad_tracking")));
        campaignEx.setVideo_end_type(cursor.getInt(cursor.getColumnIndex(CampaignEx.VIDEO_END_TYPE)));
        campaignEx.setendcard_url(cursor.getString(cursor.getColumnIndex(CampaignEx.ENDCARD_URL)));
        campaignEx.setPlayable_ads_without_video(cursor.getInt(cursor.getColumnIndex(CampaignEx.PLAYABLE_ADS_WITHOUT_VIDEO)));
        campaignEx.setLoopbackString(cursor.getString(cursor.getColumnIndex(CampaignEx.LOOPBACK)));
        campaignEx.setLoopbackMap(CampaignEx.loopbackStrToMap(cursor.getString(cursor.getColumnIndex(CampaignEx.LOOPBACK))));
        campaignEx.setRewardTemplateMode(CampaignEx.c.a(cursor.getString(cursor.getColumnIndex("reward_teamplate"))));
        campaignEx.setVideoMD5Value(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_REWARD_VIDEO_MD5)));
        campaignEx.setGifUrl(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_GIF_URL)));
        campaignEx.setNvT2(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_NV_T2)));
        campaignEx.setClickTimeOutInterval(cursor.getInt(cursor.getColumnIndex("c_coi")));
        campaignEx.setcUA(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_C_UA)));
        campaignEx.setImpUA(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_IMP_UA)));
        campaignEx.setJmPd(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_JM_PD)));
        campaignEx.setIsDeleted(cursor.getInt(cursor.getColumnIndex("is_deleted")));
        campaignEx.setIsClick(cursor.getInt(cursor.getColumnIndex("is_click")));
        campaignEx.setIsAddSuccesful(cursor.getInt(cursor.getColumnIndex("is_add_sucesful")));
        campaignEx.setIsDownLoadZip(cursor.getInt(cursor.getColumnIndex(CampaignEx.KEY_IS_DOWNLOAD)));
        campaignEx.setInteractiveCache(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_IA_CACHE)));
        campaignEx.setKeyIaOri(cursor.getInt(cursor.getColumnIndex("ia_ori")));
        campaignEx.setAdType(cursor.getInt(cursor.getColumnIndex("ad_type")));
        campaignEx.setIa_ext1(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_IA_EXT1)));
        campaignEx.setIa_ext2(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_IA_EXT2)));
        campaignEx.setKeyIaRst(cursor.getInt(cursor.getColumnIndex("ia_rst")));
        campaignEx.setKeyIaUrl(cursor.getString(cursor.getColumnIndex("ia_url")));
        campaignEx.setKeyIaIcon(cursor.getString(cursor.getColumnIndex("ia_icon")));
        campaignEx.setGhId(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_GH_ID)));
        campaignEx.setGhPath(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_GH_PATH)));
        campaignEx.setBindId(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_BIND_ID)));
        campaignEx.setOc_time(cursor.getInt(cursor.getColumnIndex(CampaignEx.KEY_OC_TIME)));
        campaignEx.setOc_type(cursor.getInt(cursor.getColumnIndex(CampaignEx.KEY_OC_TYPE)));
        campaignEx.setT_list(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_T_LIST)));
        String string = cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_ADCHOICE));
        if (!TextUtils.isEmpty(string)) {
            campaignEx.setAdchoice(CampaignEx.a.a(string));
        }
        campaignEx.setAdchoiceSizeHeight(cursor.getInt(cursor.getColumnIndex("adchoice_size_height")));
        campaignEx.setAdchoiceSizeWidth(cursor.getInt(cursor.getColumnIndex("adchoice_size_width")));
        campaignEx.setPlct(cursor.getLong(cursor.getColumnIndex(CampaignEx.JSON_KEY_PLCT)));
        campaignEx.setPlctb(cursor.getLong(cursor.getColumnIndex(CampaignEx.JSON_KEY_PLCTB)));
        campaignEx.setBannerUrl(cursor.getString(cursor.getColumnIndex("banner_url")));
        campaignEx.setBannerHtml(cursor.getString(cursor.getColumnIndex("banner_html")));
        campaignEx.setCreativeId((long) cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_CREATIVE_ID)));
        campaignEx.setIsBidCampaign(cursor.getInt(cursor.getColumnIndex("is_bid_campaign")) == 1);
        campaignEx.setBidToken(cursor.getString(cursor.getColumnIndex(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_BID_TOKEN)));
        campaignEx.setMraid(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_MRAID)));
        if (cursor.getInt(cursor.getColumnIndex("is_mraid_campaign")) == 1) {
            z = true;
        }
        campaignEx.setIsMraid(z);
        campaignEx.setOmid(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_OMID)));
        return campaignEx;
    }

    /* JADX WARN: Type inference failed for: r5v6, types: [com.mintegral.msdk.base.entity.CampaignEx] */
    /* JADX WARN: Type inference failed for: r6v11, types: [com.mintegral.msdk.base.entity.CampaignEx] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.mintegral.msdk.base.entity.CampaignEx c(java.lang.String r5, java.lang.String r6) {
        /*
            r4 = this;
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0049 }
            java.lang.String r2 = "SELECT * FROM campaign where unitid ='"
            r1.<init>(r2)     // Catch:{ Exception -> 0x0049 }
            r1.append(r6)     // Catch:{ Exception -> 0x0049 }
            java.lang.String r6 = "' and id = '"
            r1.append(r6)     // Catch:{ Exception -> 0x0049 }
            r1.append(r5)     // Catch:{ Exception -> 0x0049 }
            java.lang.String r5 = "'"
            r1.append(r5)     // Catch:{ Exception -> 0x0049 }
            java.lang.String r5 = r1.toString()     // Catch:{ Exception -> 0x0049 }
            android.database.sqlite.SQLiteDatabase r6 = r4.a()     // Catch:{ Exception -> 0x0049 }
            android.database.Cursor r5 = r6.rawQuery(r5, r0)     // Catch:{ Exception -> 0x0049 }
            if (r5 == 0) goto L_0x0040
            int r6 = r5.getCount()     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            if (r6 <= 0) goto L_0x0040
        L_0x002c:
            boolean r6 = r5.moveToNext()     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            if (r6 == 0) goto L_0x0040
            com.mintegral.msdk.base.entity.CampaignEx r6 = a(r5)     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            r0 = r6
            goto L_0x002c
        L_0x0038:
            r6 = move-exception
            r0 = r5
            goto L_0x0054
        L_0x003b:
            r6 = move-exception
            r3 = r0
            r0 = r5
            r5 = r3
            goto L_0x004b
        L_0x0040:
            if (r5 == 0) goto L_0x0045
            r5.close()
        L_0x0045:
            r5 = r0
            goto L_0x0053
        L_0x0047:
            r6 = move-exception
            goto L_0x0054
        L_0x0049:
            r6 = move-exception
            r5 = r0
        L_0x004b:
            r6.printStackTrace()     // Catch:{ all -> 0x0047 }
            if (r0 == 0) goto L_0x0053
            r0.close()
        L_0x0053:
            return r5
        L_0x0054:
            if (r0 == 0) goto L_0x0059
            r0.close()
        L_0x0059:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.c(java.lang.String, java.lang.String):com.mintegral.msdk.base.entity.CampaignEx");
    }

    /* JADX WARN: Type inference failed for: r5v6, types: [com.mintegral.msdk.base.entity.CampaignEx] */
    /* JADX WARN: Type inference failed for: r6v11, types: [com.mintegral.msdk.base.entity.CampaignEx] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.mintegral.msdk.base.entity.CampaignEx d(java.lang.String r5, java.lang.String r6) {
        /*
            r4 = this;
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0049 }
            java.lang.String r2 = "SELECT * FROM campaign where bid_token ='"
            r1.<init>(r2)     // Catch:{ Exception -> 0x0049 }
            r1.append(r6)     // Catch:{ Exception -> 0x0049 }
            java.lang.String r6 = "' and unitid ='"
            r1.append(r6)     // Catch:{ Exception -> 0x0049 }
            r1.append(r5)     // Catch:{ Exception -> 0x0049 }
            java.lang.String r5 = "'"
            r1.append(r5)     // Catch:{ Exception -> 0x0049 }
            java.lang.String r5 = r1.toString()     // Catch:{ Exception -> 0x0049 }
            android.database.sqlite.SQLiteDatabase r6 = r4.a()     // Catch:{ Exception -> 0x0049 }
            android.database.Cursor r5 = r6.rawQuery(r5, r0)     // Catch:{ Exception -> 0x0049 }
            if (r5 == 0) goto L_0x0040
            int r6 = r5.getCount()     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            if (r6 <= 0) goto L_0x0040
        L_0x002c:
            boolean r6 = r5.moveToNext()     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            if (r6 == 0) goto L_0x0040
            com.mintegral.msdk.base.entity.CampaignEx r6 = a(r5)     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            r0 = r6
            goto L_0x002c
        L_0x0038:
            r6 = move-exception
            r0 = r5
            goto L_0x0054
        L_0x003b:
            r6 = move-exception
            r3 = r0
            r0 = r5
            r5 = r3
            goto L_0x004b
        L_0x0040:
            if (r5 == 0) goto L_0x0045
            r5.close()
        L_0x0045:
            r5 = r0
            goto L_0x0053
        L_0x0047:
            r6 = move-exception
            goto L_0x0054
        L_0x0049:
            r6 = move-exception
            r5 = r0
        L_0x004b:
            r6.printStackTrace()     // Catch:{ all -> 0x0047 }
            if (r0 == 0) goto L_0x0053
            r0.close()
        L_0x0053:
            return r5
        L_0x0054:
            if (r0 == 0) goto L_0x0059
            r0.close()
        L_0x0059:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.f.d(java.lang.String, java.lang.String):com.mintegral.msdk.base.entity.CampaignEx");
    }
}
