package com.mintegral.msdk.base.common.e.a;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.b.n;
import com.mintegral.msdk.base.entity.i;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.k;

/* compiled from: CampaignRequestTimeUtil */
public final class a {
    private i a;
    private com.mintegral.msdk.base.b.i b = null;
    private Context c;

    public a(i iVar) {
        this.a = iVar;
        this.c = com.mintegral.msdk.base.controller.a.d().h();
        this.b = com.mintegral.msdk.base.b.i.a(this.c);
        if (this.a != null && this.c != null) {
            int s = c.s(this.c);
            this.a.d(s);
            this.a.c(c.a(this.c, s));
            if (k.a()) {
                this.a.c(1);
            } else {
                this.a.c(2);
            }
        }
    }

    public final void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.a.a(str);
        }
    }

    public final void a() {
        if (this.a != null) {
            this.a.c();
        }
    }

    public final void b() {
        if (this.a != null) {
            this.a.i();
        }
    }

    public final void a(int i) {
        if (this.a != null) {
            this.a.b(i);
        }
    }

    public final void c() {
        if (this.a != null) {
            n.a(this.b).a(this.a);
        }
    }

    public final void b(String str) {
        if (this.a != null) {
            this.a.b(str);
        }
    }

    public final void d() {
        if (this.a != null) {
            this.a.a(1);
        }
    }
}
