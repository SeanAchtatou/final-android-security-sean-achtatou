package com.mintegral.msdk.base.common.net;

import android.content.Context;
import android.text.TextUtils;
import com.appsflyer.AppsFlyerProperties;
import com.helpshift.common.domain.network.NetworkConstants;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.common.f.a;
import com.mintegral.msdk.base.common.net.a.e;
import com.mintegral.msdk.base.utils.g;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;

/* compiled from: CommonBaseHttpRequest */
public abstract class c {
    private String a;
    protected Context b;
    private int c;
    private int d;
    private int e;
    private boolean f;
    private int g;
    private int h;

    /* access modifiers changed from: protected */
    public abstract void a(d<?> dVar);

    /* access modifiers changed from: protected */
    public abstract void a(f fVar);

    public c(Context context) {
        this(context, 0);
    }

    public c(Context context, int i) {
        this.c = 15000;
        this.d = 0;
        this.e = 1000;
        this.f = true;
        this.g = 0;
        this.h = -1;
        this.b = context;
        this.d = i;
    }

    public final a a(String str, d<?> dVar) {
        return c(str, null, dVar);
    }

    public final a a(String str, l lVar, d<?> dVar) {
        return c(str, lVar, dVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.base.common.net.c.a(org.apache.http.client.methods.HttpRequestBase, java.util.Map<java.lang.String, java.lang.String>, com.mintegral.msdk.base.common.net.g):com.mintegral.msdk.base.common.net.f
     arg types: [org.apache.http.client.methods.HttpGet, java.util.HashMap, com.mintegral.msdk.base.common.net.d<?>]
     candidates:
      com.mintegral.msdk.base.common.net.c.a(java.lang.String, com.mintegral.msdk.base.common.net.l, com.mintegral.msdk.base.common.net.d<?>):com.mintegral.msdk.base.common.f.a
      com.mintegral.msdk.base.common.net.c.a(org.apache.http.client.methods.HttpRequestBase, java.util.Map<java.lang.String, java.lang.String>, com.mintegral.msdk.base.common.net.g):com.mintegral.msdk.base.common.net.f */
    private a c(String str, l lVar, d<?> dVar) {
        if (TextUtils.isEmpty(str)) {
            g.d("BaseHttpRequest", "http get request url cannot be empty");
            return new a() {
                public final void a() {
                }

                public final void b() {
                }
            };
        } else if (dVar != null) {
            this.a = str;
            HashMap hashMap = new HashMap();
            if (lVar == null) {
                lVar = new l();
            }
            a(lVar);
            try {
                e.d(lVar);
            } catch (Exception e2) {
                g.a("BaseHttpRequest", e2.getMessage());
            }
            String replace = str.replace(" ", "%20");
            if (lVar != null) {
                String trim = lVar.b().trim();
                if (!TextUtils.isEmpty(trim)) {
                    if (!replace.endsWith("?") && !replace.endsWith(Constants.RequestParameters.AMPERSAND)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(replace);
                        sb.append(replace.contains("?") ? Constants.RequestParameters.AMPERSAND : "?");
                        replace = sb.toString();
                    }
                    replace = replace + trim;
                }
            }
            HttpGet httpGet = new HttpGet(replace);
            if (lVar != null) {
                try {
                    httpGet.setHeader("Charset", lVar.a());
                } catch (Throwable unused) {
                    g.d("BaseHttpRequest", "setHeader failed");
                }
            }
            String str2 = "bytes=" + this.g + "-";
            if (this.h > 0) {
                str2 = str2 + this.h;
            }
            if (!(this.g == 0 && this.h == -1)) {
                httpGet.setHeader("Range", str2);
            }
            g.a("BaseHttpRequest", "request url: " + str);
            if (hashMap.size() > 0) {
                g.a("BaseHttpRequest", "request headers: " + hashMap.toString());
            }
            if (lVar != null) {
                g.a("BaseHttpRequest", "request params: " + lVar.toString());
            }
            g.a("BaseHttpRequest", "request method: GET");
            f a2 = a((HttpRequestBase) httpGet, (Map<String, String>) hashMap, (g) dVar);
            a(dVar);
            a(a2);
            return a2;
        } else {
            throw new UnsupportedOperationException("IResponseHandle cannot be null");
        }
    }

    public final a b(String str, l lVar, d<?> dVar) {
        return d(str, lVar, dVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.base.common.net.c.a(org.apache.http.client.methods.HttpRequestBase, java.util.Map<java.lang.String, java.lang.String>, com.mintegral.msdk.base.common.net.g):com.mintegral.msdk.base.common.net.f
     arg types: [org.apache.http.client.methods.HttpPost, java.util.HashMap, com.mintegral.msdk.base.common.net.d<?>]
     candidates:
      com.mintegral.msdk.base.common.net.c.a(java.lang.String, com.mintegral.msdk.base.common.net.l, com.mintegral.msdk.base.common.net.d<?>):com.mintegral.msdk.base.common.f.a
      com.mintegral.msdk.base.common.net.c.a(org.apache.http.client.methods.HttpRequestBase, java.util.Map<java.lang.String, java.lang.String>, com.mintegral.msdk.base.common.net.g):com.mintegral.msdk.base.common.net.f */
    private a d(String str, l lVar, d<?> dVar) {
        if (TextUtils.isEmpty(str)) {
            g.c("BaseHttpRequest", "http post request url cannot be empty");
            return new a() {
                public final void a() {
                }

                public final void b() {
                }
            };
        } else if (dVar != null) {
            this.a = str;
            HashMap hashMap = new HashMap();
            if (lVar == null) {
                lVar = new l();
            }
            a(lVar);
            try {
                e.d(lVar);
            } catch (Exception e2) {
                g.a("BaseHttpRequest", e2.getMessage());
            }
            try {
                HttpPost httpPost = new HttpPost(str);
                if (lVar != null) {
                    HttpEntity a2 = lVar.a(dVar);
                    httpPost.setHeader(a2.getContentType());
                    httpPost.setHeader("Charset", lVar.a());
                    httpPost.setEntity(a2);
                }
                String str2 = "bytes=" + this.g + "-";
                if (this.h > 0) {
                    str2 = str2 + this.h;
                }
                httpPost.setHeader("Range", str2);
                g.a("BaseHttpRequest", "request url: " + str);
                if (hashMap.size() > 0) {
                    g.a("BaseHttpRequest", "request headers: " + hashMap.toString());
                }
                if (lVar != null) {
                    g.a("BaseHttpRequest", "request params: " + lVar.toString());
                }
                g.a("BaseHttpRequest", "request method: POST");
                f a3 = a((HttpRequestBase) httpPost, (Map<String, String>) hashMap, (g) dVar);
                a(dVar);
                a(a3);
                return a3;
            } catch (IOException e3) {
                g.c("BaseHttpRequest", "write params an error occurred", e3);
                return new a() {
                    public final void a() {
                    }

                    public final void b() {
                    }
                };
            }
        } else {
            throw new UnsupportedOperationException("IResponseHandle cannot be null");
        }
    }

    public final void c() {
        this.c = NetworkConstants.UPLOAD_CONNECT_TIMEOUT;
    }

    /* access modifiers changed from: protected */
    public void a(l lVar) {
        if (lVar != null) {
            String a2 = Aa.a();
            if (a2 == null) {
                a2 = "";
            }
            lVar.a(AppsFlyerProperties.CHANNEL, a2);
            g.a("BaseHttpRequest", "excute addExtraParams , url:" + this.a);
            if (!TextUtils.isEmpty(this.a) && this.a.contains("setting")) {
                String b2 = Aa.b();
                if (!TextUtils.isEmpty(b2)) {
                    lVar.a("keyword", b2);
                    return;
                }
                return;
            }
            return;
        }
        g.d("BaseHttpRequest", "addExtraParams error, params is null,frame work error");
    }

    private f a(HttpRequestBase httpRequestBase, Map<String, String> map, g gVar) {
        if (!map.isEmpty()) {
            for (Map.Entry next : map.entrySet()) {
                if (!TextUtils.equals((CharSequence) next.getKey(), HttpRequest.HEADER_CONTENT_TYPE) && !TextUtils.equals((CharSequence) next.getKey(), "Charset")) {
                    httpRequestBase.setHeader((String) next.getKey(), (String) next.getValue());
                }
            }
        }
        return new f(this.b, httpRequestBase, gVar, this.c, this.d, this.e, this.f);
    }
}
