package com.mintegral.msdk.base.common.a;

import android.text.TextUtils;
import com.mintegral.msdk.base.common.d.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.mtgbid.common.BidResponsedEx;
import com.mintegral.msdk.reward.player.MTGRewardVideoActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: StaticDataPoll */
public class c {
    public static Map<String, Long> a = new HashMap();
    public static Map<String, List<a>> b = new HashMap();
    public static Map<String, List<a>> c = new HashMap();
    public static Map<String, List<a>> d = new HashMap();
    public static Map<String, List<a>> e = new HashMap();
    public static Map<String, List<a>> f = new HashMap();
    public static Map<String, List<a>> g = new HashMap();
    public static Map<String, List<a>> h = new HashMap();
    private static final String i = "c";

    public static void a(String str, CampaignEx campaignEx, String str2) {
        Map<String, List<a>> a2 = a(str2);
        if (campaignEx != null && a2 != null) {
            try {
                if (!TextUtils.isEmpty(campaignEx.getId())) {
                    a aVar = new a(campaignEx.getId(), campaignEx.getRequestId());
                    if (a2.containsKey(str)) {
                        a2.get(str).add(aVar);
                        return;
                    }
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(aVar);
                    a2.put(str, arrayList);
                }
            } catch (Throwable th) {
                g.c(i, th.getMessage(), th);
            }
        }
    }

    public static void b(String str, CampaignEx campaignEx, String str2) {
        Map<String, List<a>> a2 = a(str2);
        if (campaignEx != null && a2 != null) {
            try {
                if (!TextUtils.isEmpty(campaignEx.getId())) {
                    a aVar = new a(campaignEx.getId(), campaignEx.getRequestId());
                    if (a2.containsKey(str)) {
                        List list = a2.get(str);
                        if (list.size() == 20) {
                            list.remove(0);
                        }
                        list.add(aVar);
                        return;
                    }
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(aVar);
                    a2.put(str, arrayList);
                }
            } catch (Throwable th) {
                g.c(i, th.getMessage(), th);
            }
        }
    }

    public static String a(String str, String str2) {
        Map<String, List<a>> map;
        List list;
        JSONArray jSONArray = new JSONArray();
        if (TextUtils.isEmpty(str2)) {
            map = null;
            if (c.containsKey(str)) {
                map = c;
            } else if (d.containsKey(str)) {
                map = d;
            } else if (e.containsKey(str)) {
                map = e;
            } else if (f.containsKey(str)) {
                map = f;
            } else if (g.containsKey(str)) {
                map = g;
            } else if (h.containsKey(str)) {
                map = h;
            }
        } else {
            map = a(str2);
        }
        if (map != null) {
            try {
                if (s.b(str) && map.containsKey(str) && (list = (List) map.get(str)) != null && list.size() > 0) {
                    for (int i2 = 0; i2 < list.size(); i2++) {
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put(BidResponsedEx.KEY_CID, ((a) list.get(i2)).a());
                        jSONObject.put("rid", ((a) list.get(i2)).b());
                        jSONArray.put(jSONObject);
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return jSONArray.toString();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static Map<String, List<a>> a(String str) {
        char c2;
        switch (str.hashCode()) {
            case -1611837702:
                if (str.equals("videofeeds")) {
                    c2 = 3;
                    break;
                }
                c2 = 65535;
                break;
            case -1396342996:
                if (str.equals("banner")) {
                    c2 = 5;
                    break;
                }
                c2 = 65535;
                break;
            case -1052618729:
                if (str.equals("native")) {
                    c2 = 0;
                    break;
                }
                c2 = 65535;
                break;
            case -934326481:
                if (str.equals(MTGRewardVideoActivity.INTENT_REWARD)) {
                    c2 = 1;
                    break;
                }
                c2 = 65535;
                break;
            case 604727084:
                if (str.equals("interstitial")) {
                    c2 = 4;
                    break;
                }
                c2 = 65535;
                break;
            case 1844104930:
                if (str.equals("interactive")) {
                    c2 = 2;
                    break;
                }
                c2 = 65535;
                break;
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 0:
                return c;
            case 1:
                return d;
            case 2:
                return e;
            case 3:
                return f;
            case 4:
                return g;
            case 5:
                return h;
            default:
                return null;
        }
    }
}
