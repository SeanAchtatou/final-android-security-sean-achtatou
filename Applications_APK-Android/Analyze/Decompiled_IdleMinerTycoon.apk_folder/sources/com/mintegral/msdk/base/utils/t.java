package com.mintegral.msdk.base.utils;

import android.content.Context;
import android.content.pm.ProviderInfo;
import android.content.pm.ServiceInfo;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import com.mintegral.msdk.MIntegralConstans;
import com.vungle.warren.ui.JavascriptBridge;

/* compiled from: TargetAdaptionUtils */
public final class t {
    public static int a = -1;
    public static int b = -1;
    public static int c = -1;
    public static int d = -1;
    public static int e = -1;

    public static boolean a(Context context) {
        if (e == 1) {
            return true;
        }
        if (e == 0) {
            return false;
        }
        if (context == null) {
            try {
                e = -1;
                return false;
            } catch (Exception unused) {
                e = -1;
                return false;
            } catch (NoSuchMethodError unused2) {
                e = 0;
                return false;
            } catch (Throwable unused3) {
                e = -1;
                return false;
            }
        } else {
            if (c.m(context) >= 26 && Build.VERSION.SDK_INT >= 26) {
                new NotificationCompat.Builder(context, JavascriptBridge.MraidHandler.DOWNLOAD_ACTION);
                e = 1;
            }
            return true;
        }
    }

    public static boolean b(Context context) {
        if (c == 1) {
            return true;
        }
        if (c == 0) {
            return false;
        }
        if (context == null) {
            try {
                c = -1;
                return false;
            } catch (Exception unused) {
                c = -1;
                return false;
            } catch (Throwable unused2) {
                c = -1;
                return false;
            }
        } else if (c.m(context) < 26 || Build.VERSION.SDK_INT < 26) {
            return true;
        } else {
            String[] strArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
            if (strArr == null) {
                c = -1;
                return false;
            } else if (strArr.length == 0) {
                c = 0;
                return false;
            } else {
                int length = strArr.length;
                int i = 0;
                boolean z = true;
                while (i < length) {
                    if (strArr[i].equals("android.permission.REQUEST_INSTALL_PACKAGES")) {
                        c = 1;
                        return true;
                    }
                    c = 0;
                    i++;
                    z = false;
                }
                return z;
            }
        }
    }

    public static boolean c(Context context) {
        if (b == 1) {
            return true;
        }
        if (b == 0) {
            return false;
        }
        if (context == null) {
            try {
                b = -1;
                return false;
            } catch (Exception unused) {
                b = -1;
                return false;
            } catch (Throwable unused2) {
                b = -1;
                return false;
            }
        } else {
            ServiceInfo[] serviceInfoArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 4).services;
            if (serviceInfoArr == null) {
                b = -1;
                return false;
            } else if (serviceInfoArr.length == 0) {
                b = 0;
                return false;
            } else {
                int length = serviceInfoArr.length;
                int i = 0;
                boolean z = true;
                while (i < length) {
                    if (serviceInfoArr[i].name.equals("com.mintegral.msdk.shell.MTGService")) {
                        b = 1;
                        return true;
                    }
                    b = 0;
                    i++;
                    z = false;
                }
                return z;
            }
        }
    }

    public static boolean d(Context context) {
        if (d == 1) {
            return true;
        }
        if (d == 0) {
            return false;
        }
        if (context == null) {
            try {
                d = -1;
                return false;
            } catch (Exception unused) {
                d = -1;
                return false;
            } catch (Throwable unused2) {
                d = -1;
                return false;
            }
        } else {
            ProviderInfo[] providerInfoArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 8).providers;
            if (providerInfoArr == null) {
                d = -1;
                return false;
            } else if (providerInfoArr.length == 0) {
                d = 0;
                return false;
            } else {
                int length = providerInfoArr.length;
                int i = 0;
                boolean z = true;
                while (i < length) {
                    if (providerInfoArr[i].name.equals("com.mintegral.msdk.base.utils.MTGFileProvider")) {
                        d = 1;
                        return true;
                    }
                    d = 0;
                    i++;
                    z = false;
                }
                return z;
            }
        }
    }

    public static boolean e(Context context) {
        try {
            if (c.m(context) < 24 || Build.VERSION.SDK_INT < 24) {
                return false;
            }
            return true;
        } catch (Throwable th) {
            if (!MIntegralConstans.DEBUG) {
                return false;
            }
            th.printStackTrace();
            return false;
        }
    }
}
