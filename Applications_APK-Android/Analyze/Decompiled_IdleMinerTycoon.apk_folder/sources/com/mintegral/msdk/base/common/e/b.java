package com.mintegral.msdk.base.common.e;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.share.internal.ShareConstants;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.r;
import com.mintegral.msdk.base.b.t;
import com.mintegral.msdk.base.common.e.d.a;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.c;
import com.mintegral.msdk.base.entity.o;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.out.Frame;
import com.mintegral.msdk.out.MTGConfiguration;
import com.mintegral.msdk.rover.e;
import com.mintegral.msdk.rover.f;
import com.tapjoy.TapjoyConstants;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.net.URLEncoder;
import java.util.List;
import org.json.JSONObject;

/* compiled from: ReportController */
public class b {
    public static final String a = "b";
    /* access modifiers changed from: private */
    public Context b;
    private int c = 0;

    public b(Context context, int i) {
        this.b = context;
        this.c = i;
    }

    public b(Context context) {
        this.b = context;
    }

    public final void a(int i, String str) {
        new a(this.b, 0).b(com.mintegral.msdk.base.common.a.f, c.a("event", c.a(i, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID), this.b, str), new com.mintegral.msdk.base.common.e.d.b() {
            public final void a(String str) {
            }

            public final void b(String str) {
                g.b(b.a, "report success");
            }
        });
    }

    public final void a(final o oVar, final Boolean bool) {
        if (oVar == null) {
            return;
        }
        if (oVar.c().equals(HttpRequest.METHOD_GET)) {
            a aVar = new a(this.b, this.c);
            aVar.c();
            aVar.a(oVar.b(), new com.mintegral.msdk.base.common.e.d.b() {
                public final void a(String str) {
                }

                public final void b(String str) {
                    g.b(b.a, "report success");
                    r.a(i.a(b.this.b)).a(oVar.b());
                    if (bool.booleanValue() && r.a(i.a(b.this.b)).d() > 20) {
                        com.mintegral.msdk.base.controller.b.a().c();
                    }
                }
            });
        } else if (oVar.c().equals(HttpRequest.METHOD_POST)) {
            a aVar2 = new a(this.b, this.c);
            aVar2.c();
            if (!TextUtils.isEmpty(oVar.d())) {
                aVar2.b(oVar.b(), c.a(oVar.d(), this.b, oVar.a()), new com.mintegral.msdk.base.common.e.d.b() {
                    public final void a(String str) {
                    }

                    public final void b(String str) {
                        g.b(b.a, "report success");
                        r.a(i.a(b.this.b)).a(oVar.d(), oVar.b());
                        if (bool.booleanValue() && r.a(i.a(b.this.b)).d() > 20) {
                            com.mintegral.msdk.base.controller.b.a().c();
                        }
                    }
                });
            }
        }
    }

    public final void a(final o oVar) {
        a aVar = new a(this.b, this.c);
        aVar.c();
        aVar.b(com.mintegral.msdk.base.common.a.f, c.a(this.b, oVar), new com.mintegral.msdk.base.common.e.d.b() {
            public final void a(String str) {
            }

            public final void b(String str) {
                g.b(b.a, "report success");
                try {
                    if (oVar != null) {
                        t.a(i.a(b.this.b)).a(String.valueOf(oVar.e()));
                    }
                } catch (Exception unused) {
                }
            }
        });
    }

    public final void a(String str) {
        c(str);
    }

    private void c(String str) {
        try {
            a aVar = new a(this.b, this.c);
            aVar.c();
            aVar.b(com.mintegral.msdk.base.common.a.f, c.a(str, this.b, ""), new com.mintegral.msdk.base.common.e.d.b() {
                public final void b(String str) {
                    String str2 = b.a;
                    g.b(str2, "reportPB success data:" + str);
                }

                public final void a(String str) {
                    String str2 = b.a;
                    g.b(str2, "reportPB onFailed msg:" + str);
                }
            });
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public final void b(String str) {
        c(str);
    }

    public final void a(String str, String str2, String str3, String str4) {
        try {
            a aVar = new a(this.b, this.c);
            aVar.c();
            aVar.b(com.mintegral.msdk.base.common.a.f, c.a("click_type=" + URLEncoder.encode(str, "utf-8") + "&cid=" + URLEncoder.encode(str2, "utf-8") + "&unit_id=" + URLEncoder.encode(str3, "utf-8") + "&key=" + URLEncoder.encode("2000027", "utf-8") + "&http_url=" + URLEncoder.encode(str4, "utf-8"), this.b, str3), new com.mintegral.msdk.base.common.e.d.b() {
                public final void b(String str) {
                    g.a("", "SSL REPORT SUCCESS");
                }

                public final void a(String str) {
                    g.a("", "SSL REPORT FAILED");
                }
            });
        } catch (Exception unused) {
            g.d(a, "ssl  error report failed");
        }
    }

    public final void a(String str, final File file) {
        a aVar = new a(this.b, this.c);
        aVar.c();
        aVar.b(com.mintegral.msdk.base.common.a.f, c.a(this.b, str), new com.mintegral.msdk.base.common.e.d.b() {
            public final void b(String str) {
                g.b(b.a, "report success exception");
                if (file != null) {
                    file.delete();
                }
            }

            public final void a(String str) {
                g.b(b.a, "report failed exception");
            }
        });
    }

    public final void a(final String str, String str2, String str3, Frame frame) {
        a aVar = new a(this.b, this.c);
        aVar.c();
        l a2 = c.a(str2, this.b, str3);
        if (frame != null) {
            a2.a("session_id", frame.getSessionId());
            a2.a("parent_session_id", frame.getParentSessionId());
        }
        aVar.b(com.mintegral.msdk.base.common.a.f, a2, new com.mintegral.msdk.base.common.e.d.b() {
            public final void b(String str) {
                g.b(b.a, "report success");
                if ("net_time_stats".equals(str)) {
                    return;
                }
                if ("click_duration".equals(str)) {
                    com.mintegral.msdk.base.controller.b.a().c();
                } else if ("load_duration".equals(str)) {
                    com.mintegral.msdk.base.controller.b.a().c();
                    com.mintegral.msdk.base.controller.b.a().c();
                }
            }

            public final void a(String str) {
                g.b(b.a, "report success");
            }
        });
    }

    public final void a(c cVar, String str) {
        a aVar = new a(this.b, this.c);
        aVar.c();
        StringBuilder sb = new StringBuilder();
        String k = cVar.k();
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            sb.append("rid_n=" + cVar.n());
            sb.append("&network_type=" + cVar.a());
            sb.append("&network_str=" + cVar.b());
            sb.append("&click_type=" + cVar.g());
            sb.append("&type=" + cVar.j());
            sb.append("&cid=" + cVar.l());
            sb.append("&click_duration=" + cVar.m());
            sb.append("&key=2000012");
            sb.append("&unit_id=" + cVar.c());
            sb.append("&last_url=" + k);
            sb.append("&code=" + cVar.i());
            sb.append("&exception=" + cVar.h());
            sb.append("&landing_type=" + cVar.d());
            sb.append("&link_type=" + cVar.e());
            sb.append("&click_time=" + cVar.f() + "\n");
        } else {
            sb.append("rid_n=" + cVar.n());
            sb.append("&click_type=" + cVar.g());
            sb.append("&type=" + cVar.j());
            sb.append("&cid=" + cVar.l());
            sb.append("&click_duration=" + cVar.m());
            sb.append("&key=2000012");
            sb.append("&unit_id=" + cVar.c());
            sb.append("&last_url=" + k);
            sb.append("&code=" + cVar.i());
            sb.append("&exception=" + cVar.h());
            sb.append("&landing_type=" + cVar.d());
            sb.append("&link_type=" + cVar.e());
            sb.append("&click_time=" + cVar.f() + "\n");
        }
        String sb2 = sb.toString();
        if (!TextUtils.isEmpty(sb2)) {
            aVar.b(com.mintegral.msdk.base.common.a.f, c.a(sb2, this.b, str), new com.mintegral.msdk.base.common.e.d.b() {
                public final void a(String str) {
                }

                public final void b(String str) {
                    g.b(b.a, "report success");
                }
            });
        }
    }

    public final void a(CampaignEx campaignEx, List<e> list, f fVar) {
        a aVar = new a(this.b, this.c);
        aVar.c();
        String b2 = b();
        l a2 = c.a(campaignEx, list);
        aVar.b(com.mintegral.msdk.rover.a.b + b2, a2, fVar);
    }

    private String b() {
        StringBuffer stringBuffer = new StringBuffer("?");
        stringBuffer.append("platform=");
        stringBuffer.append(URLEncoder.encode("1"));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("os_version=");
        stringBuffer.append(URLEncoder.encode(Build.VERSION.RELEASE));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("package_name=");
        stringBuffer.append(URLEncoder.encode(com.mintegral.msdk.base.utils.c.q(this.b)));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("app_version_name=");
        stringBuffer.append(URLEncoder.encode(com.mintegral.msdk.base.utils.c.l(this.b)));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("app_version_code=");
        StringBuilder sb = new StringBuilder();
        sb.append(com.mintegral.msdk.base.utils.c.k(this.b));
        stringBuffer.append(URLEncoder.encode(sb.toString()));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("screen_size=");
        stringBuffer.append(URLEncoder.encode(com.mintegral.msdk.base.utils.c.n(this.b) + "x" + com.mintegral.msdk.base.utils.c.o(this.b)));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("orientation=");
        StringBuilder sb2 = new StringBuilder();
        sb2.append(com.mintegral.msdk.base.utils.c.i(this.b));
        stringBuffer.append(URLEncoder.encode(sb2.toString()));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("gaid=");
        stringBuffer.append(URLEncoder.encode(com.mintegral.msdk.base.utils.c.k()));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        String encode = URLEncoder.encode(com.mintegral.msdk.base.utils.c.e());
        stringBuffer.append("brand=");
        stringBuffer.append(encode);
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("mnc=");
        stringBuffer.append(URLEncoder.encode(com.mintegral.msdk.base.utils.c.b()));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("mcc=");
        stringBuffer.append(URLEncoder.encode(com.mintegral.msdk.base.utils.c.a()));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        int s = com.mintegral.msdk.base.utils.c.s(this.b);
        stringBuffer.append("network_type=");
        stringBuffer.append(URLEncoder.encode(String.valueOf(s)));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("network_str");
        stringBuffer.append(URLEncoder.encode(com.mintegral.msdk.base.utils.c.a(this.b, s)));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("language=");
        stringBuffer.append(URLEncoder.encode(com.mintegral.msdk.base.utils.c.h(this.b)));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("timezone=");
        stringBuffer.append(URLEncoder.encode(com.mintegral.msdk.base.utils.c.h()));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        String encode2 = URLEncoder.encode(com.mintegral.msdk.base.utils.c.f());
        stringBuffer.append("useragent=");
        stringBuffer.append(encode2);
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("sdk_version=");
        stringBuffer.append(URLEncoder.encode(MTGConfiguration.SDK_VERSION));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        String encode3 = URLEncoder.encode(com.mintegral.msdk.base.utils.c.t(this.b));
        stringBuffer.append("gp_version=");
        stringBuffer.append(encode3);
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("sign=");
        stringBuffer.append(URLEncoder.encode(CommonMD5.getMD5(com.mintegral.msdk.base.controller.a.d().j() + com.mintegral.msdk.base.controller.a.d().k())));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("app_id=");
        stringBuffer.append(URLEncoder.encode(com.mintegral.msdk.base.controller.a.d().j()));
        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
        com.mintegral.msdk.d.b.a();
        com.mintegral.msdk.d.a b2 = com.mintegral.msdk.d.b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b2 != null) {
            JSONObject jSONObject = new JSONObject();
            try {
                if (b2.as() == 1) {
                    if (com.mintegral.msdk.base.utils.c.c(this.b) != null) {
                        jSONObject.put("imei", com.mintegral.msdk.base.utils.c.c(this.b));
                    }
                    if (com.mintegral.msdk.base.utils.c.j(this.b) != null) {
                        jSONObject.put("mac", com.mintegral.msdk.base.utils.c.j(this.b));
                    }
                }
                if (b2.au() == 1 && com.mintegral.msdk.base.utils.c.f(this.b) != null) {
                    jSONObject.put(TapjoyConstants.TJC_ANDROID_ID, com.mintegral.msdk.base.utils.c.f(this.b));
                }
                if (!TextUtils.isEmpty(jSONObject.toString())) {
                    String b3 = com.mintegral.msdk.base.utils.a.b(jSONObject.toString());
                    if (!TextUtils.isEmpty(b3)) {
                        stringBuffer.append("dvi=");
                        stringBuffer.append(b3);
                        stringBuffer.append(Constants.RequestParameters.AMPERSAND);
                    } else {
                        stringBuffer.append("dvi=&");
                    }
                } else {
                    stringBuffer.append("dvi=&");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            stringBuffer.append("dvi=&");
        }
        stringBuffer.append("unit_id=0");
        return stringBuffer.toString();
    }

    public final void a() {
        try {
            if (c.a()) {
                a aVar = new a(this.b);
                aVar.c();
                String j = com.mintegral.msdk.base.controller.a.d().j();
                com.mintegral.msdk.d.b.a();
                com.mintegral.msdk.d.a b2 = com.mintegral.msdk.d.b.b(j);
                if (b2 == null) {
                    com.mintegral.msdk.d.b.a();
                    b2 = com.mintegral.msdk.d.b.b();
                }
                int v = b2.v();
                String str = "key=2000053&Appid=" + j + "&uptips2=" + b2.s() + "&info_status=" + com.mintegral.msdk.base.controller.authoritycontroller.a.a().d() + "&iseu=" + v;
                String k = com.mintegral.msdk.base.utils.c.k();
                if (!TextUtils.isEmpty(k)) {
                    str = str + "&gaid=" + k;
                }
                g.a(a, "reportPrivateAuthorityStatus  data:" + str);
                aVar.b(com.mintegral.msdk.base.common.a.f, c.a(str, this.b, ""), new com.mintegral.msdk.base.common.e.d.b() {
                    public final void b(String str) {
                        g.a("", "PrivateAuthorityStatus onSuccess ");
                    }

                    public final void a(String str) {
                        g.a("", "PrivateAuthorityStatus onFailed:" + str);
                    }
                });
                c.b();
            }
        } catch (Throwable unused) {
            g.d(a, "PrivateAuthorityStatus onFailed");
        }
    }

    public final void a(int i, int i2, String str, String str2) {
        try {
            a aVar = new a(this.b, this.c);
            aVar.c();
            StringBuilder sb = new StringBuilder();
            sb.append("key=");
            sb.append(URLEncoder.encode("2000058", "utf-8"));
            sb.append("&appid=");
            sb.append(URLEncoder.encode(com.mintegral.msdk.base.controller.a.d().j(), "utf-8"));
            sb.append("&dl_service=");
            StringBuilder sb2 = new StringBuilder();
            sb2.append(com.mintegral.msdk.base.utils.t.b);
            sb.append(URLEncoder.encode(sb2.toString(), "utf-8"));
            sb.append("&dl_type=");
            sb.append(URLEncoder.encode(String.valueOf(i), "utf-8"));
            sb.append("&dl_link_type=");
            sb.append(URLEncoder.encode(String.valueOf(i2), "utf-8"));
            sb.append("&rid_n=");
            sb.append(URLEncoder.encode(str, "utf-8"));
            sb.append("&cid=");
            sb.append(URLEncoder.encode(str2, "utf-8"));
            sb.append("&dl_v4=");
            StringBuilder sb3 = new StringBuilder();
            sb3.append(com.mintegral.msdk.base.utils.t.e);
            sb.append(URLEncoder.encode(sb3.toString(), "utf-8"));
            sb.append("&dl_pkg=");
            StringBuilder sb4 = new StringBuilder();
            sb4.append(com.mintegral.msdk.base.utils.t.a);
            sb.append(URLEncoder.encode(sb4.toString(), "utf-8"));
            sb.append("&dl_i_p=");
            StringBuilder sb5 = new StringBuilder();
            sb5.append(com.mintegral.msdk.base.utils.t.c);
            sb.append(URLEncoder.encode(sb5.toString(), "utf-8"));
            sb.append("&dl_fp=");
            StringBuilder sb6 = new StringBuilder();
            sb6.append(com.mintegral.msdk.base.utils.t.d);
            sb.append(URLEncoder.encode(sb6.toString(), "utf-8"));
            sb.append("&tgt_v=");
            StringBuilder sb7 = new StringBuilder();
            sb7.append(com.mintegral.msdk.base.utils.c.m(this.b));
            sb.append(URLEncoder.encode(sb7.toString(), "utf-8"));
            sb.append("&app_v_n=");
            sb.append(URLEncoder.encode(com.mintegral.msdk.base.utils.c.l(this.b), "utf-8"));
            sb.append("&app_v_c=");
            StringBuilder sb8 = new StringBuilder();
            sb8.append(com.mintegral.msdk.base.utils.c.k(this.b));
            sb.append(URLEncoder.encode(sb8.toString(), "utf-8"));
            aVar.b(com.mintegral.msdk.base.common.a.f, c.a(sb.toString(), this.b, ""), new com.mintegral.msdk.base.common.e.d.b() {
                public final void b(String str) {
                    g.a("", "reportDownloadMethod REPORT SUCCESS");
                }

                public final void a(String str) {
                    g.a("", "reportDownloadMethod REPORT FAILED");
                }
            });
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    public final void a(String str, String str2, String str3, String str4, boolean z) {
        try {
            if (!TextUtils.isEmpty(str4)) {
                a aVar = new a(this.b, this.c);
                aVar.c();
                StringBuilder sb = new StringBuilder();
                if (z) {
                    sb.append("hb=1&");
                }
                sb.append("key=");
                sb.append(URLEncoder.encode("2000066", "utf-8"));
                sb.append("&rid_n=");
                sb.append(URLEncoder.encode(str, "utf-8"));
                sb.append("&cid=");
                sb.append(URLEncoder.encode(str2, "utf-8"));
                sb.append("&unit_id=");
                sb.append(URLEncoder.encode(str3, "utf-8"));
                sb.append("&err_method=");
                sb.append(str4);
                aVar.b(com.mintegral.msdk.base.common.a.f, c.a(sb.toString(), this.b, str3), new com.mintegral.msdk.base.common.e.d.b() {
                    public final void b(String str) {
                        g.a("", "MraidUnSupportMethod REPORT SUCCESS");
                    }

                    public final void a(String str) {
                        g.a("", "MraidUnSupportMethod REPORT FAILED");
                    }
                });
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    public final void b(String str, String str2, String str3, String str4, boolean z) {
        try {
            a aVar = new a(this.b, this.c);
            aVar.c();
            StringBuilder sb = new StringBuilder();
            if (z) {
                sb.append("hb=1&");
            }
            sb.append("key=");
            sb.append(URLEncoder.encode("2000065", "utf-8"));
            sb.append("&rid_n=");
            sb.append(URLEncoder.encode(str, "utf-8"));
            sb.append("&cid=");
            sb.append(URLEncoder.encode(str2, "utf-8"));
            sb.append("&unit_id=");
            sb.append(URLEncoder.encode(str3, "utf-8"));
            sb.append("&click_url=");
            sb.append(URLEncoder.encode(str4, "utf-8"));
            aVar.b(com.mintegral.msdk.base.common.a.f, c.a(sb.toString(), this.b, str3), new com.mintegral.msdk.base.common.e.d.b() {
                public final void b(String str) {
                    g.a("", "MraidClic REPORT SUCCESS");
                }

                public final void a(String str) {
                    g.a("", "MraidClic REPORT FAILED");
                }
            });
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    public final void b(String str, String str2, String str3, String str4) {
        try {
            a aVar = new a(this.b, this.c);
            aVar.c();
            StringBuilder sb = new StringBuilder();
            int s = com.mintegral.msdk.base.utils.c.s(this.b);
            sb.append("key=");
            sb.append(URLEncoder.encode("2000071", "utf-8"));
            sb.append("&rid_n=");
            sb.append(URLEncoder.encode(str, "utf-8"));
            sb.append("&cid=");
            sb.append(URLEncoder.encode(str2, "utf-8"));
            sb.append("&unit_id=");
            sb.append(URLEncoder.encode(str3, "utf-8"));
            sb.append("&reason=");
            sb.append(URLEncoder.encode(str4, "utf-8"));
            sb.append("&network_type=");
            sb.append(URLEncoder.encode(String.valueOf(s), "utf-8"));
            sb.append("&result=");
            sb.append(URLEncoder.encode(AppEventsConstants.EVENT_PARAM_VALUE_NO, "utf-8"));
            aVar.b(com.mintegral.msdk.base.common.a.f, c.a(sb.toString(), this.b, str3), new com.mintegral.msdk.base.common.e.d.b() {
                public final void b(String str) {
                    g.a("", "OMSDK REPORT SUCCESS");
                }

                public final void a(String str) {
                    g.a("", "OMSDK REPORT FAILED");
                }
            });
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }
}
