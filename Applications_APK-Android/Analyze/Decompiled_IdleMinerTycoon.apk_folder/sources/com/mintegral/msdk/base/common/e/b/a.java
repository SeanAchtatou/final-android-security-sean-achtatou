package com.mintegral.msdk.base.common.e.b;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.text.TextUtils;
import android.text.format.Formatter;
import com.appsflyer.AppsFlyerProperties;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.constants.LocationConst;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.b.c;
import com.mintegral.msdk.base.common.b.e;
import com.mintegral.msdk.base.common.e.b;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONObject;

/* compiled from: CrashHandlerUtil */
public class a implements Thread.UncaughtExceptionHandler {
    private static volatile a c;
    Handler a = new Handler() {
        public final void handleMessage(Message message) {
            Context context;
            HashMap hashMap;
            super.handleMessage(message);
            if (message.what == 101 && (context = (Context) a.this.b.get()) != null && (message.obj instanceof HashMap) && (hashMap = (HashMap) message.obj) != null) {
                new b(context).a((String) hashMap.get("crashinfo"), (File) hashMap.get(Constants.ParametersKeys.FILE));
            }
        }
    };
    /* access modifiers changed from: private */
    public WeakReference<Context> b;
    private Thread.UncaughtExceptionHandler d;
    private Throwable e;

    private a(Context context) {
        this.b = new WeakReference<>(context);
    }

    public static a a(Context context) {
        if (c == null) {
            synchronized (a.class) {
                if (c == null) {
                    c = new a(context);
                }
            }
        }
        return c;
    }

    public final void a() {
        this.d = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    private String a(Throwable th, String str) {
        try {
            JSONObject jSONObject = new JSONObject();
            Context context = this.b.get();
            if (context == null) {
                return "";
            }
            double maxMemory = (double) Runtime.getRuntime().maxMemory();
            Double.isNaN(maxMemory);
            float f = (float) ((maxMemory * 1.0d) / 1048576.0d);
            double d2 = (double) Runtime.getRuntime().totalMemory();
            Double.isNaN(d2);
            float f2 = (float) ((d2 * 1.0d) / 1048576.0d);
            double freeMemory = (double) Runtime.getRuntime().freeMemory();
            Double.isNaN(freeMemory);
            float f3 = (float) ((freeMemory * 1.0d) / 1048576.0d);
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            long usableSpace = externalStorageDirectory.getUsableSpace();
            long totalSpace = externalStorageDirectory.getTotalSpace();
            String formatFileSize = Formatter.formatFileSize(context, usableSpace);
            String formatFileSize2 = Formatter.formatFileSize(context, totalSpace);
            jSONObject.put("max_memory", String.valueOf(f));
            jSONObject.put("memoryby_app", String.valueOf(f2));
            jSONObject.put("remaining_memory", (double) f3);
            jSONObject.put("sdcard_remainder", formatFileSize);
            jSONObject.put("totalspacestr", formatFileSize2);
            jSONObject.put("crashtime", str);
            String a2 = com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.SDK_APP_ID);
            if (!TextUtils.isEmpty(a2)) {
                jSONObject.put(AppsFlyerProperties.APP_ID, a2);
            }
            StringWriter stringWriter = new StringWriter();
            th.printStackTrace(new PrintWriter(stringWriter));
            jSONObject.put("crashinfo", stringWriter.toString());
            String jSONObject2 = jSONObject.toString();
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("key=2000052");
            stringBuffer.append("&exception=" + jSONObject2);
            return stringBuffer.toString();
        } catch (Throwable th2) {
            th2.printStackTrace();
            return "";
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        try {
            this.e = th;
            com.mintegral.msdk.d.b.a();
            com.mintegral.msdk.d.a b2 = com.mintegral.msdk.d.b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (b2 == null) {
                com.mintegral.msdk.d.b.a();
                b2 = com.mintegral.msdk.d.b.b();
            }
            String z = b2.z() == null ? "mintegral" : b2.z();
            List<String> a2 = a(z, "<mvpackage>(.*?)</mvpackage>");
            if (a2.size() > 0) {
                for (int i = 0; i < a2.size(); i++) {
                    a(a(th), a2.get(i), th, thread);
                }
                return;
            }
            a(a(th), z, th, thread);
        } catch (Exception e2) {
            b();
            e2.printStackTrace();
        }
    }

    private static String a(Throwable th) {
        StackTraceElement[] stackTrace = th.getStackTrace();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < stackTrace.length; i++) {
            sb.append(stackTrace[i].toString() + "\n");
        }
        return sb.toString();
    }

    private void a(String str, String str2, Throwable th, Thread thread) {
        if (str.contains(str2)) {
            HashMap<String, Object> c2 = c();
            if (c2 != null && c2.get(Constants.ParametersKeys.FILE) != null) {
                Message obtain = Message.obtain();
                HashMap hashMap = new HashMap();
                hashMap.put("crashinfo", a(th, (String) c2.get(LocationConst.TIME)));
                hashMap.put(Constants.ParametersKeys.FILE, c2.get(Constants.ParametersKeys.FILE));
                obtain.obj = hashMap;
                obtain.what = 101;
                this.a.sendMessage(obtain);
                b();
            }
        } else if (this.d != null) {
            this.d.uncaughtException(thread, th);
        }
    }

    private static List<String> a(String str, String str2) {
        ArrayList arrayList = new ArrayList();
        try {
            Matcher matcher = Pattern.compile(str2).matcher(str);
            while (matcher.find()) {
                arrayList.add(matcher.group(1));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return arrayList;
    }

    private static void b() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        Process.killProcess(Process.myPid());
    }

    private HashMap<String, Object> c() {
        HashMap<String, Object> hashMap = new HashMap<>();
        if (this.e == null) {
            return null;
        }
        String b2 = e.b(c.MINTEGRAL_CRASH_INFO);
        File file = new File(b2 + com.appsflyer.share.Constants.URL_PATH_DELIMITER);
        if (!file.exists()) {
            file.mkdirs();
        }
        String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
        File file2 = new File(b2 + "/sdkcrash" + format + ".txt");
        try {
            PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(file2)));
            printWriter.println(a(this.e, format));
            printWriter.println("====");
            this.e.printStackTrace(printWriter);
            printWriter.close();
            hashMap.put(Constants.ParametersKeys.FILE, file2);
            hashMap.put(LocationConst.TIME, format);
            return hashMap;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
