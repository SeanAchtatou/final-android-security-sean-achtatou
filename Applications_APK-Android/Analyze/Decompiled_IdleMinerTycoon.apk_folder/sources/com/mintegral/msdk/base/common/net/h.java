package com.mintegral.msdk.base.common.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import com.mintegral.msdk.base.utils.g;
import java.util.Locale;

/* compiled from: CommonNetConnectManager */
public final class h {
    private static h b;
    private final String a = "NetConnectManager";
    private ConnectivityManager c;
    private j d;

    public static synchronized h a(Context context) {
        h hVar;
        synchronized (h.class) {
            if (b == null) {
                b = new h();
                if (context != null) {
                    b.c = (ConnectivityManager) context.getSystemService("connectivity");
                }
                b.d = new j();
            }
            hVar = b;
        }
        return hVar;
    }

    public final void a() {
        NetworkInfo activeNetworkInfo;
        String lowerCase;
        try {
            if (this.c != null && (activeNetworkInfo = this.c.getActiveNetworkInfo()) != null) {
                if ("wifi".equals(activeNetworkInfo.getTypeName().toLowerCase(Locale.US))) {
                    this.d.e = "wifi";
                    this.d.d = false;
                } else {
                    if (!(activeNetworkInfo.getExtraInfo() == null || (lowerCase = activeNetworkInfo.getExtraInfo().toLowerCase(Locale.US)) == null)) {
                        if (!lowerCase.startsWith("cmwap") && !lowerCase.startsWith("uniwap")) {
                            if (!lowerCase.startsWith("3gwap")) {
                                if (lowerCase.startsWith("ctwap")) {
                                    this.d.d = true;
                                    this.d.a = lowerCase;
                                    this.d.b = "10.0.0.200";
                                    this.d.c = "80";
                                } else if (lowerCase.startsWith("cmnet") || lowerCase.startsWith("uninet") || lowerCase.startsWith("ctnet") || lowerCase.startsWith("3gnet")) {
                                    this.d.d = false;
                                    this.d.a = lowerCase;
                                }
                                this.d.e = this.d.a;
                            }
                        }
                        this.d.d = true;
                        this.d.a = lowerCase;
                        this.d.b = "10.0.0.172";
                        this.d.c = "80";
                        this.d.e = this.d.a;
                    }
                    String defaultHost = Proxy.getDefaultHost();
                    int defaultPort = Proxy.getDefaultPort();
                    if (defaultHost == null || defaultHost.length() <= 0) {
                        this.d.d = false;
                        this.d.e = this.d.a;
                    } else {
                        this.d.b = defaultHost;
                        if ("10.0.0.172".equals(this.d.b.trim())) {
                            this.d.d = true;
                            this.d.c = "80";
                        } else if ("10.0.0.200".equals(this.d.b.trim())) {
                            this.d.d = true;
                            this.d.c = "80";
                        } else {
                            this.d.d = false;
                            this.d.c = Integer.toString(defaultPort);
                        }
                        this.d.e = this.d.a;
                    }
                }
                g.a("NetConnectManager", "current net connect type is " + this.d.e);
            }
        } catch (Exception unused) {
            g.d("NetConnectManager", "NETWORK FAILED");
        }
    }

    public final boolean b() {
        try {
            NetworkInfo activeNetworkInfo = this.c.getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                return activeNetworkInfo.isConnectedOrConnecting();
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public final j c() {
        return this.d;
    }
}
