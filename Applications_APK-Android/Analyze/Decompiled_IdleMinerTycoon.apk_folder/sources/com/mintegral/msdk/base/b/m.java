package com.mintegral.msdk.base.b;

import com.mintegral.msdk.base.entity.g;

/* compiled from: FrequenceDao */
public class m extends a<g> {
    private static m b;

    private m(h hVar) {
        super(hVar);
    }

    public static m a(h hVar) {
        if (b == null) {
            synchronized (m.class) {
                if (b == null) {
                    b = new m(hVar);
                }
            }
        }
        return b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0036, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0039, code lost:
        if (r0 != null) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x004a, code lost:
        if (r0 != null) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x004e, code lost:
        return null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0045 A[Catch:{ Exception -> 0x0049, all -> 0x003f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized long[] c() {
        /*
            r7 = this;
            monitor-enter(r7)
            java.lang.String r0 = "SELECT id FROM frequence WHERE fc_a<impression_count"
            r1 = 0
            android.database.sqlite.SQLiteDatabase r2 = r7.a()     // Catch:{ Exception -> 0x0049, all -> 0x003f }
            android.database.Cursor r0 = r2.rawQuery(r0, r1)     // Catch:{ Exception -> 0x0049, all -> 0x003f }
            if (r0 == 0) goto L_0x0039
            int r2 = r0.getCount()     // Catch:{ Exception -> 0x004a, all -> 0x0037 }
            if (r2 <= 0) goto L_0x0039
            int r2 = r0.getCount()     // Catch:{ Exception -> 0x004a, all -> 0x0037 }
            long[] r2 = new long[r2]     // Catch:{ Exception -> 0x004a, all -> 0x0037 }
            r3 = 0
        L_0x001b:
            boolean r4 = r0.moveToNext()     // Catch:{ Exception -> 0x004a, all -> 0x0037 }
            if (r4 == 0) goto L_0x0030
            java.lang.String r4 = "id"
            int r4 = r0.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x004a, all -> 0x0037 }
            long r4 = r0.getLong(r4)     // Catch:{ Exception -> 0x004a, all -> 0x0037 }
            r2[r3] = r4     // Catch:{ Exception -> 0x004a, all -> 0x0037 }
            int r3 = r3 + 1
            goto L_0x001b
        L_0x0030:
            if (r0 == 0) goto L_0x0035
            r0.close()     // Catch:{ all -> 0x004f }
        L_0x0035:
            monitor-exit(r7)
            return r2
        L_0x0037:
            r1 = move-exception
            goto L_0x0043
        L_0x0039:
            if (r0 == 0) goto L_0x004d
        L_0x003b:
            r0.close()     // Catch:{ all -> 0x004f }
            goto L_0x004d
        L_0x003f:
            r0 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0043:
            if (r0 == 0) goto L_0x0048
            r0.close()     // Catch:{ all -> 0x004f }
        L_0x0048:
            throw r1     // Catch:{ all -> 0x004f }
        L_0x0049:
            r0 = r1
        L_0x004a:
            if (r0 == 0) goto L_0x004d
            goto L_0x003b
        L_0x004d:
            monitor-exit(r7)
            return r1
        L_0x004f:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.m.c():long[]");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002d, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void d() {
        /*
            r4 = this;
            monitor-enter(r4)
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x002c, all -> 0x0029 }
            r2 = 86400000(0x5265c00, double:4.2687272E-316)
            long r0 = r0 - r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x002c, all -> 0x0029 }
            java.lang.String r3 = "ts<"
            r2.<init>(r3)     // Catch:{ Exception -> 0x002c, all -> 0x0029 }
            r2.append(r0)     // Catch:{ Exception -> 0x002c, all -> 0x0029 }
            java.lang.String r0 = r2.toString()     // Catch:{ Exception -> 0x002c, all -> 0x0029 }
            android.database.sqlite.SQLiteDatabase r1 = r4.b()     // Catch:{ Exception -> 0x002c, all -> 0x0029 }
            if (r1 == 0) goto L_0x0027
            android.database.sqlite.SQLiteDatabase r1 = r4.b()     // Catch:{ Exception -> 0x002c, all -> 0x0029 }
            java.lang.String r2 = "frequence"
            r3 = 0
            r1.delete(r2, r0, r3)     // Catch:{ Exception -> 0x002c, all -> 0x0029 }
        L_0x0027:
            monitor-exit(r4)
            return
        L_0x0029:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x002c:
            monitor-exit(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.m.d():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        if (r2 == null) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0036, code lost:
        if (r2 == null) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003c, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean a(java.lang.String r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            r0 = 0
            r1 = 0
            monitor-enter(r6)     // Catch:{ Exception -> 0x0047, all -> 0x0040 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x003d }
            java.lang.String r3 = "SELECT id FROM frequence WHERE id='"
            r2.<init>(r3)     // Catch:{ all -> 0x003d }
            r2.append(r6)     // Catch:{ all -> 0x003d }
            java.lang.String r3 = "'"
            r2.append(r3)     // Catch:{ all -> 0x003d }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x003d }
            android.database.sqlite.SQLiteDatabase r3 = r5.a()     // Catch:{ all -> 0x003d }
            android.database.Cursor r2 = r3.rawQuery(r2, r1)     // Catch:{ all -> 0x003d }
            if (r2 == 0) goto L_0x0035
            int r1 = r2.getCount()     // Catch:{ all -> 0x0030 }
            if (r1 <= 0) goto L_0x0035
            monitor-exit(r6)     // Catch:{ all -> 0x0030 }
            if (r2 == 0) goto L_0x002d
            r2.close()     // Catch:{ all -> 0x004d }
        L_0x002d:
            r6 = 1
            monitor-exit(r5)
            return r6
        L_0x0030:
            r1 = move-exception
            r4 = r2
            r2 = r1
            r1 = r4
            goto L_0x003e
        L_0x0035:
            monitor-exit(r6)     // Catch:{ all -> 0x0030 }
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ all -> 0x004d }
        L_0x003b:
            monitor-exit(r5)
            return r0
        L_0x003d:
            r2 = move-exception
        L_0x003e:
            monitor-exit(r6)     // Catch:{ all -> 0x003d }
            throw r2     // Catch:{ Exception -> 0x0047, all -> 0x0040 }
        L_0x0040:
            r6 = move-exception
            if (r1 == 0) goto L_0x0046
            r1.close()     // Catch:{ all -> 0x004d }
        L_0x0046:
            throw r6     // Catch:{ all -> 0x004d }
        L_0x0047:
            if (r1 == 0) goto L_0x0050
            r1.close()     // Catch:{ all -> 0x004d }
            goto L_0x0050
        L_0x004d:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        L_0x0050:
            monitor-exit(r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.m.a(java.lang.String):boolean");
    }

    public final synchronized void b(String str) {
        if (a(str)) {
            String str2 = "UPDATE frequence Set impression_count=impression_count+1 WHERE id=" + str;
            if (a() != null) {
                a().execSQL(str2);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0078, code lost:
        return -1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized long b(com.mintegral.msdk.base.entity.g r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            r0 = -1
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            r2.<init>()     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            java.lang.String r3 = "id"
            java.lang.String r4 = r7.a()     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            java.lang.String r3 = "fc_a"
            int r4 = r7.b()     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            java.lang.String r3 = "fc_b"
            int r4 = r7.c()     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            java.lang.String r3 = "ts"
            long r4 = r7.h()     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            java.lang.String r3 = "impression_count"
            int r4 = r7.d()     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            java.lang.String r3 = "click_count"
            int r4 = r7.f()     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            java.lang.String r3 = "ts"
            long r4 = r7.h()     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            java.lang.Long r7 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            r2.put(r3, r7)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            android.database.sqlite.SQLiteDatabase r7 = r6.b()     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            if (r7 != 0) goto L_0x0067
            monitor-exit(r6)
            return r0
        L_0x0067:
            android.database.sqlite.SQLiteDatabase r7 = r6.b()     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            java.lang.String r3 = "frequence"
            r4 = 0
            long r2 = r7.insert(r3, r4, r2)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            monitor-exit(r6)
            return r2
        L_0x0074:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        L_0x0077:
            monitor-exit(r6)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.m.b(com.mintegral.msdk.base.entity.g):long");
    }

    public final synchronized void a(g gVar) {
        if (!a(gVar.a())) {
            b(gVar);
        }
    }
}
