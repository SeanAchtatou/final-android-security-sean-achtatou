package com.mintegral.msdk.base.common.c;

import android.text.TextUtils;
import com.mintegral.msdk.base.utils.g;
import java.io.File;

/* compiled from: CommonImageTask */
public final class d extends com.mintegral.msdk.base.common.f.a {
    private String a;
    private String b;
    private String c;
    private boolean d = false;
    private a e;

    /* compiled from: CommonImageTask */
    public interface a {
        void a(String str, String str2);

        void b(String str, String str2);
    }

    public final void b() {
    }

    public final void a(boolean z) {
        this.d = z;
    }

    public final void a(a aVar) {
        this.e = aVar;
    }

    public d(String str, String str2, String str3) {
        this.a = str;
        this.b = str2;
        this.c = str3;
    }

    private void e() {
        if (f()) {
            g();
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:81:0x0152 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:64:0x012c */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v1, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r0v4 */
    /* JADX WARN: Type inference failed for: r0v5 */
    /* JADX WARN: Type inference failed for: r0v6, types: [com.mintegral.msdk.base.common.net.i] */
    /* JADX WARN: Type inference failed for: r0v9, types: [com.mintegral.msdk.base.common.net.i] */
    /* JADX WARN: Type inference failed for: r0v13 */
    /* JADX WARN: Type inference failed for: r0v14 */
    /* JADX WARN: Type inference failed for: r0v15, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r0v20 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0181 A[SYNTHETIC, Splitter:B:103:0x0181] */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x018b A[SYNTHETIC, Splitter:B:108:0x018b] */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x0195 A[SYNTHETIC, Splitter:B:113:0x0195] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0137 A[SYNTHETIC, Splitter:B:67:0x0137] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0141 A[SYNTHETIC, Splitter:B:72:0x0141] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x014b A[SYNTHETIC, Splitter:B:77:0x014b] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x015f A[Catch:{ all -> 0x017c }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0164 A[SYNTHETIC, Splitter:B:86:0x0164] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x016e A[SYNTHETIC, Splitter:B:91:0x016e] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0178 A[SYNTHETIC, Splitter:B:96:0x0178] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:81:0x0152=Splitter:B:81:0x0152, B:64:0x012c=Splitter:B:64:0x012c} */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean f() {
        /*
            r10 = this;
            r0 = 0
            r1 = 0
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            java.lang.String r3 = r10.c     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            boolean r3 = r2.exists()     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            if (r3 != 0) goto L_0x0012
            r2.mkdirs()     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
        L_0x0012:
            boolean r3 = r2.exists()     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            if (r3 == 0) goto L_0x001b
            r2.delete()     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
        L_0x001b:
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            r4.<init>()     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            java.lang.String r5 = r10.c     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            r4.append(r5)     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            java.lang.String r5 = ".temp"
            r4.append(r5)     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            boolean r4 = r3.exists()     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            if (r4 == 0) goto L_0x003c
            r3.delete()     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
        L_0x003c:
            com.mintegral.msdk.base.common.net.h r4 = com.mintegral.msdk.base.common.net.h.a(r0)     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            com.mintegral.msdk.base.common.net.i r5 = new com.mintegral.msdk.base.common.net.i     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            java.lang.String r6 = r10.b     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            com.mintegral.msdk.base.common.net.j r4 = r4.c()     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            r7 = 30000(0x7530, float:4.2039E-41)
            r5.<init>(r6, r7, r4)     // Catch:{ Exception -> 0x014f, OutOfMemoryError -> 0x0129, all -> 0x0125 }
            org.apache.http.params.HttpParams r4 = r5.getParams()     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            java.lang.String r6 = "http.protocol.expect-continue"
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            r4.setParameter(r6, r8)     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            org.apache.http.impl.client.DefaultHttpRequestRetryHandler r6 = new org.apache.http.impl.client.DefaultHttpRequestRetryHandler     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            r6.<init>(r1, r1)     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            r5.setHttpRequestRetryHandler(r6)     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r4, r7)     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r4, r7)     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            org.apache.http.client.methods.HttpGet r4 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            java.lang.String r6 = r10.b     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            r4.<init>(r6)     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            org.apache.http.HttpResponse r4 = r5.execute(r4)     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            org.apache.http.StatusLine r6 = r4.getStatusLine()     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            int r6 = r6.getStatusCode()     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            r7 = 200(0xc8, float:2.8E-43)
            r8 = 1
            if (r6 != r7) goto L_0x00da
            org.apache.http.HttpEntity r4 = r4.getEntity()     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            java.io.InputStream r4 = r4.getContent()     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00d7, OutOfMemoryError -> 0x00d4, all -> 0x00d0 }
            r6.<init>(r3, r8)     // Catch:{ Exception -> 0x00d7, OutOfMemoryError -> 0x00d4, all -> 0x00d0 }
            r0 = 2048(0x800, float:2.87E-42)
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x00ce, OutOfMemoryError -> 0x00cc, all -> 0x00c9 }
        L_0x0091:
            int r7 = r4.read(r0)     // Catch:{ Exception -> 0x00ce, OutOfMemoryError -> 0x00cc, all -> 0x00c9 }
            r9 = -1
            if (r7 == r9) goto L_0x009c
            r6.write(r0, r1, r7)     // Catch:{ Exception -> 0x00ce, OutOfMemoryError -> 0x00cc, all -> 0x00c9 }
            goto L_0x0091
        L_0x009c:
            r6.flush()     // Catch:{ Exception -> 0x00ce, OutOfMemoryError -> 0x00cc, all -> 0x00c9 }
            r3.renameTo(r2)     // Catch:{ Exception -> 0x00ce, OutOfMemoryError -> 0x00cc, all -> 0x00c9 }
            java.lang.String r0 = "ImageWorker"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ce, OutOfMemoryError -> 0x00cc, all -> 0x00c9 }
            java.lang.String r3 = "download file from ["
            r2.<init>(r3)     // Catch:{ Exception -> 0x00ce, OutOfMemoryError -> 0x00cc, all -> 0x00c9 }
            java.lang.String r3 = r10.b     // Catch:{ Exception -> 0x00ce, OutOfMemoryError -> 0x00cc, all -> 0x00c9 }
            r2.append(r3)     // Catch:{ Exception -> 0x00ce, OutOfMemoryError -> 0x00cc, all -> 0x00c9 }
            java.lang.String r3 = "] save to ["
            r2.append(r3)     // Catch:{ Exception -> 0x00ce, OutOfMemoryError -> 0x00cc, all -> 0x00c9 }
            java.lang.String r3 = r10.c     // Catch:{ Exception -> 0x00ce, OutOfMemoryError -> 0x00cc, all -> 0x00c9 }
            r2.append(r3)     // Catch:{ Exception -> 0x00ce, OutOfMemoryError -> 0x00cc, all -> 0x00c9 }
            java.lang.String r3 = "]"
            r2.append(r3)     // Catch:{ Exception -> 0x00ce, OutOfMemoryError -> 0x00cc, all -> 0x00c9 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00ce, OutOfMemoryError -> 0x00cc, all -> 0x00c9 }
            com.mintegral.msdk.base.utils.g.a(r0, r2)     // Catch:{ Exception -> 0x00ce, OutOfMemoryError -> 0x00cc, all -> 0x00c9 }
            r0 = r4
            r1 = 1
            goto L_0x00f8
        L_0x00c9:
            r1 = move-exception
            goto L_0x017e
        L_0x00cc:
            r2 = move-exception
            goto L_0x011e
        L_0x00ce:
            r2 = move-exception
            goto L_0x0123
        L_0x00d0:
            r1 = move-exception
            r6 = r0
            goto L_0x017e
        L_0x00d4:
            r2 = move-exception
            r6 = r0
            goto L_0x011e
        L_0x00d7:
            r2 = move-exception
            r6 = r0
            goto L_0x0123
        L_0x00da:
            java.lang.String r2 = r10.b     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            java.lang.String r4 = "load image from http faild because http return code: "
            r3.<init>(r4)     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            r3.append(r6)     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            java.lang.String r4 = ".image url is "
            r3.append(r4)     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            java.lang.String r4 = r10.b     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            r3.append(r4)     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            r10.a(r2, r3)     // Catch:{ Exception -> 0x0120, OutOfMemoryError -> 0x011b, all -> 0x0117 }
            r6 = r0
        L_0x00f8:
            r5.a()     // Catch:{ Exception -> 0x00fc }
            goto L_0x0100
        L_0x00fc:
            r2 = move-exception
            r2.printStackTrace()
        L_0x0100:
            if (r0 == 0) goto L_0x010a
            r0.close()     // Catch:{ IOException -> 0x0106 }
            goto L_0x010a
        L_0x0106:
            r0 = move-exception
            r0.printStackTrace()
        L_0x010a:
            if (r6 == 0) goto L_0x017b
            r6.close()     // Catch:{ IOException -> 0x0111 }
            goto L_0x017b
        L_0x0111:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x017b
        L_0x0117:
            r1 = move-exception
            r6 = r0
            goto L_0x017f
        L_0x011b:
            r2 = move-exception
            r4 = r0
            r6 = r4
        L_0x011e:
            r0 = r5
            goto L_0x012c
        L_0x0120:
            r2 = move-exception
            r4 = r0
            r6 = r4
        L_0x0123:
            r0 = r5
            goto L_0x0152
        L_0x0125:
            r1 = move-exception
            r5 = r0
            r6 = r5
            goto L_0x017f
        L_0x0129:
            r2 = move-exception
            r4 = r0
            r6 = r4
        L_0x012c:
            java.lang.String r3 = r10.b     // Catch:{ all -> 0x017c }
            java.lang.String r2 = r2.getMessage()     // Catch:{ all -> 0x017c }
            r10.a(r3, r2)     // Catch:{ all -> 0x017c }
            if (r0 == 0) goto L_0x013f
            r0.a()     // Catch:{ Exception -> 0x013b }
            goto L_0x013f
        L_0x013b:
            r0 = move-exception
            r0.printStackTrace()
        L_0x013f:
            if (r4 == 0) goto L_0x0149
            r4.close()     // Catch:{ IOException -> 0x0145 }
            goto L_0x0149
        L_0x0145:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0149:
            if (r6 == 0) goto L_0x017b
            r6.close()     // Catch:{ IOException -> 0x0111 }
            goto L_0x017b
        L_0x014f:
            r2 = move-exception
            r4 = r0
            r6 = r4
        L_0x0152:
            java.lang.String r3 = r10.b     // Catch:{ all -> 0x017c }
            java.lang.String r5 = r2.getMessage()     // Catch:{ all -> 0x017c }
            r10.a(r3, r5)     // Catch:{ all -> 0x017c }
            boolean r3 = com.mintegral.msdk.MIntegralConstans.DEBUG     // Catch:{ all -> 0x017c }
            if (r3 == 0) goto L_0x0162
            r2.printStackTrace()     // Catch:{ all -> 0x017c }
        L_0x0162:
            if (r0 == 0) goto L_0x016c
            r0.a()     // Catch:{ Exception -> 0x0168 }
            goto L_0x016c
        L_0x0168:
            r0 = move-exception
            r0.printStackTrace()
        L_0x016c:
            if (r4 == 0) goto L_0x0176
            r4.close()     // Catch:{ IOException -> 0x0172 }
            goto L_0x0176
        L_0x0172:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0176:
            if (r6 == 0) goto L_0x017b
            r6.close()     // Catch:{ IOException -> 0x0111 }
        L_0x017b:
            return r1
        L_0x017c:
            r1 = move-exception
            r5 = r0
        L_0x017e:
            r0 = r4
        L_0x017f:
            if (r5 == 0) goto L_0x0189
            r5.a()     // Catch:{ Exception -> 0x0185 }
            goto L_0x0189
        L_0x0185:
            r2 = move-exception
            r2.printStackTrace()
        L_0x0189:
            if (r0 == 0) goto L_0x0193
            r0.close()     // Catch:{ IOException -> 0x018f }
            goto L_0x0193
        L_0x018f:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0193:
            if (r6 == 0) goto L_0x019d
            r6.close()     // Catch:{ IOException -> 0x0199 }
            goto L_0x019d
        L_0x0199:
            r0 = move-exception
            r0.printStackTrace()
        L_0x019d:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.common.c.d.f():boolean");
    }

    private void g() {
        if (new File(this.c).exists()) {
            String str = this.b;
            String str2 = this.c;
            if (this.e != null) {
                this.e.a(str, str2);
                return;
            }
            return;
        }
        String str3 = "load image faild.because file[" + this.c + "] is not exist!";
        g.a("ImageWorker", str3);
        a(this.b, str3);
    }

    private void a(String str, String str2) {
        if (this.e != null) {
            this.e.b(str, str2);
        }
    }

    public final void a() {
        if (this.d) {
            e();
        } else if (TextUtils.isEmpty(this.c)) {
            a(this.b, "save path is null.");
        } else {
            File file = new File(this.c);
            if (!file.exists() || file.length() <= 0) {
                e();
            } else {
                g();
            }
        }
    }
}
