package com.mintegral.msdk.base.common.f;

import android.content.Context;
import com.mintegral.msdk.base.common.f.a;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: CommonTaskLoader */
public final class b {
    ThreadPoolExecutor a;
    HashMap<Long, a> b;
    WeakReference<Context> c;

    public b(Context context, int i) {
        if (i == 0) {
            this.a = new ThreadPoolExecutor(1, 1, 15, TimeUnit.SECONDS, new LinkedBlockingDeque());
        } else {
            this.a = new ThreadPoolExecutor(i, i, 15, TimeUnit.SECONDS, new LinkedBlockingDeque());
        }
        this.a.allowCoreThreadTimeOut(true);
        this.b = new HashMap<>();
        this.c = new WeakReference<>(context);
    }

    public b(Context context) {
        this.a = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 15, TimeUnit.SECONDS, new SynchronousQueue());
        this.a.allowCoreThreadTimeOut(true);
        this.b = new HashMap<>();
        this.c = new WeakReference<>(context);
    }

    public b(Context context, byte b2) {
        int availableProcessors = (Runtime.getRuntime().availableProcessors() * 2) + 1;
        this.a = new ThreadPoolExecutor(availableProcessors, availableProcessors, 1, TimeUnit.SECONDS, new LinkedBlockingDeque());
        this.a.allowCoreThreadTimeOut(true);
        this.b = new HashMap<>();
        this.c = new WeakReference<>(context);
    }

    public final synchronized void a(a aVar) {
        if (aVar != null) {
            if (this.b.containsKey(Long.valueOf(a.c()))) {
                a aVar2 = this.b.get(Long.valueOf(a.c()));
                if (aVar2 != null) {
                    aVar2.d();
                }
                this.b.remove(Long.valueOf(a.c()));
            }
        }
    }

    private synchronized void b(final a aVar, final a.b bVar) {
        this.b.put(Long.valueOf(a.c()), aVar);
        aVar.j = new a.b() {
            public final void a(int i) {
                if (i == a.C0050a.d) {
                    b.this.b.remove(Long.valueOf(a.c()));
                } else if (i == a.C0050a.e) {
                    b.this.b.remove(Long.valueOf(a.c()));
                } else if (i == a.C0050a.b && b.this.c.get() == null) {
                    b.this.a();
                }
                if (bVar != null) {
                    bVar.a(i);
                }
            }
        };
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a() {
        /*
            r2 = this;
            monitor-enter(r2)
            java.util.HashMap<java.lang.Long, com.mintegral.msdk.base.common.f.a> r0 = r2.b     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            java.util.Set r0 = r0.entrySet()     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
        L_0x000b:
            boolean r1 = r0.hasNext()     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            if (r1 == 0) goto L_0x0021
            java.lang.Object r1 = r0.next()     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            java.lang.Object r1 = r1.getValue()     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            com.mintegral.msdk.base.common.f.a r1 = (com.mintegral.msdk.base.common.f.a) r1     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            r1.d()     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            goto L_0x000b
        L_0x0021:
            java.util.HashMap<java.lang.Long, com.mintegral.msdk.base.common.f.a> r0 = r2.b     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            r0.clear()     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            monitor-exit(r2)
            return
        L_0x0028:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x002b:
            monitor-exit(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.common.f.b.a():void");
    }

    public final void b(a aVar) {
        b(aVar, null);
        this.a.execute(aVar);
    }

    public final void a(a aVar, a.b bVar) {
        b(aVar, bVar);
        this.a.execute(aVar);
    }
}
