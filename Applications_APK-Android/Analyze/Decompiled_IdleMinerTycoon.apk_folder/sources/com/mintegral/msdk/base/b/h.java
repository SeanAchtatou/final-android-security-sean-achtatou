package com.mintegral.msdk.base.b;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* compiled from: CommonAbsDBHelper */
public abstract class h {
    private a a;

    /* access modifiers changed from: protected */
    public abstract void a(SQLiteDatabase sQLiteDatabase);

    /* access modifiers changed from: protected */
    public abstract void b(SQLiteDatabase sQLiteDatabase);

    /* access modifiers changed from: protected */
    public abstract String c();

    /* access modifiers changed from: protected */
    public abstract void c(SQLiteDatabase sQLiteDatabase);

    public h(Context context) {
        this.a = new a(context, c());
    }

    public final SQLiteDatabase a() {
        return this.a.getReadableDatabase();
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized android.database.sqlite.SQLiteDatabase b() {
        /*
            r2 = this;
            monitor-enter(r2)
            r0 = 0
            com.mintegral.msdk.base.b.h$a r1 = r2.a     // Catch:{ Exception -> 0x000d, all -> 0x000a }
            android.database.sqlite.SQLiteDatabase r1 = r1.getWritableDatabase()     // Catch:{ Exception -> 0x000d, all -> 0x000a }
            r0 = r1
            goto L_0x000d
        L_0x000a:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x000d:
            monitor-exit(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.h.b():android.database.sqlite.SQLiteDatabase");
    }

    /* compiled from: CommonAbsDBHelper */
    private class a extends SQLiteOpenHelper {
        public a(Context context, String str) {
            super(context, str, (SQLiteDatabase.CursorFactory) null, 44);
        }

        public final void onCreate(SQLiteDatabase sQLiteDatabase) {
            h.this.a(sQLiteDatabase);
        }

        public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            h.this.b(sQLiteDatabase);
        }

        public final void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            h.this.c(sQLiteDatabase);
        }
    }
}
