package com.mintegral.msdk.base.entity;

import com.facebook.appevents.AppEventsConstants;
import com.mintegral.msdk.base.utils.a;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.s;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/* compiled from: PBInfo */
public final class m {
    public String a;
    public String b;
    public String c = AppEventsConstants.EVENT_PARAM_VALUE_NO;

    public static String a(String str, List<String> list, List<String> list2, List<String> list3, List<String> list4, boolean z) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("key=2000042");
            StringBuilder sb2 = new StringBuilder();
            String b2 = k.b(list);
            String b3 = k.b(list2);
            String b4 = k.b(list3);
            String b5 = k.b(list4);
            sb2.append("pid=" + str);
            sb2.append("&full=" + b2);
            sb2.append("&add=" + b3);
            sb2.append("&dele=" + b4);
            sb2.append("&detail=" + b5);
            String str2 = "1";
            if (!z) {
                str2 = AppEventsConstants.EVENT_PARAM_VALUE_NO;
            }
            sb2.append("&first_time=" + str2);
            String b6 = a.b(sb2.toString());
            g.b("PBInfo", "content:" + sb2.toString());
            sb.append("&content=" + URLEncoder.encode(b6, "utf-8"));
            String sb3 = sb.toString();
            g.d("PBInfo", "pbSb report:" + sb3);
            return sb3;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static List<String> a(List<m> list) {
        ArrayList arrayList = new ArrayList();
        if (list == null) {
            try {
                if (list.size() > 0) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return arrayList;
        }
        for (m next : list) {
            if (s.b(next.a)) {
                arrayList.add(next.a);
            }
        }
        return arrayList;
    }
}
