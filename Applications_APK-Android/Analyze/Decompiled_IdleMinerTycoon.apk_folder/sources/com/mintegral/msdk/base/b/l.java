package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import com.ironsource.sdk.constants.LocationConst;
import com.mintegral.msdk.base.entity.f;

/* compiled from: ExcludeInfoDao */
public class l extends a<f> {
    private static l b;

    private l(h hVar) {
        super(hVar);
    }

    public static l a(h hVar) {
        if (b == null) {
            synchronized (l.class) {
                if (b == null) {
                    b = new l(hVar);
                }
            }
        }
        return b;
    }

    public final synchronized void a(f fVar) {
        try {
            if (b() != null) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("id", fVar.a());
                contentValues.put(LocationConst.TIME, Long.valueOf(fVar.d()));
                contentValues.put("unitId", fVar.b());
                contentValues.put("type", Integer.valueOf(fVar.c()));
                if (a(fVar.b(), fVar.a())) {
                    b().update("exclude_info", contentValues, "id = " + fVar.a() + " AND unitId = " + fVar.b(), null);
                    return;
                }
                b().insert("exclude_info", null, contentValues);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003a, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean a(java.lang.String r4, java.lang.String r5) {
        /*
            r3 = this;
            monitor-enter(r3)
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x003d }
            java.lang.String r2 = "select id from exclude_info where unitId='"
            r1.<init>(r2)     // Catch:{ Exception -> 0x003d }
            r1.append(r4)     // Catch:{ Exception -> 0x003d }
            java.lang.String r4 = "' and id='"
            r1.append(r4)     // Catch:{ Exception -> 0x003d }
            r1.append(r5)     // Catch:{ Exception -> 0x003d }
            java.lang.String r4 = "'"
            r1.append(r4)     // Catch:{ Exception -> 0x003d }
            java.lang.String r4 = r1.toString()     // Catch:{ Exception -> 0x003d }
            android.database.sqlite.SQLiteDatabase r5 = r3.a()     // Catch:{ Exception -> 0x003d }
            r1 = 0
            android.database.Cursor r4 = r5.rawQuery(r4, r1)     // Catch:{ Exception -> 0x003d }
            if (r4 == 0) goto L_0x0034
            int r5 = r4.getCount()     // Catch:{ Exception -> 0x003d }
            if (r5 <= 0) goto L_0x0034
            r4.close()     // Catch:{ Exception -> 0x003d }
            r4 = 1
            monitor-exit(r3)
            return r4
        L_0x0034:
            if (r4 == 0) goto L_0x0039
            r4.close()     // Catch:{ Exception -> 0x003d }
        L_0x0039:
            monitor-exit(r3)
            return r0
        L_0x003b:
            r4 = move-exception
            goto L_0x0043
        L_0x003d:
            r4 = move-exception
            r4.printStackTrace()     // Catch:{ all -> 0x003b }
            monitor-exit(r3)
            return r0
        L_0x0043:
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.l.a(java.lang.String, java.lang.String):boolean");
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:25:0x004a */
    /* JADX WARN: Type inference failed for: r0v2, types: [java.util.List<java.lang.String>] */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARN: Type inference failed for: r0v12 */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003e, code lost:
        r4 = r0;
        r0 = r6;
        r6 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0042, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0042 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:11:0x001c] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x005c A[SYNTHETIC, Splitter:B:35:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0064 A[SYNTHETIC, Splitter:B:41:0x0064] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<java.lang.String> a(java.lang.String r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r5.a()     // Catch:{ Exception -> 0x0055 }
            if (r1 != 0) goto L_0x000a
            monitor-exit(r5)
            return r0
        L_0x000a:
            java.lang.String r1 = "select id from exclude_info where unitId=?"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0055 }
            r3 = 0
            r2[r3] = r6     // Catch:{ Exception -> 0x0055 }
            android.database.sqlite.SQLiteDatabase r6 = r5.a()     // Catch:{ Exception -> 0x0055 }
            android.database.Cursor r6 = r6.rawQuery(r1, r2)     // Catch:{ Exception -> 0x0055 }
            if (r6 == 0) goto L_0x004a
            int r1 = r6.getCount()     // Catch:{ Exception -> 0x0044, all -> 0x0042 }
            if (r1 <= 0) goto L_0x004a
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x0044, all -> 0x0042 }
            r1.<init>()     // Catch:{ Exception -> 0x0044, all -> 0x0042 }
        L_0x0027:
            boolean r0 = r6.moveToNext()     // Catch:{ Exception -> 0x003d, all -> 0x0042 }
            if (r0 == 0) goto L_0x003b
            java.lang.String r0 = "id"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x003d, all -> 0x0042 }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x003d, all -> 0x0042 }
            r1.add(r0)     // Catch:{ Exception -> 0x003d, all -> 0x0042 }
            goto L_0x0027
        L_0x003b:
            r0 = r1
            goto L_0x004a
        L_0x003d:
            r0 = move-exception
            r4 = r0
            r0 = r6
            r6 = r4
            goto L_0x0057
        L_0x0042:
            r0 = move-exception
            goto L_0x0062
        L_0x0044:
            r1 = move-exception
            r4 = r0
            r0 = r6
            r6 = r1
            r1 = r4
            goto L_0x0057
        L_0x004a:
            if (r6 == 0) goto L_0x0060
            r6.close()     // Catch:{ all -> 0x0068 }
            goto L_0x0060
        L_0x0050:
            r6 = move-exception
            r4 = r0
            r0 = r6
            r6 = r4
            goto L_0x0062
        L_0x0055:
            r6 = move-exception
            r1 = r0
        L_0x0057:
            r6.printStackTrace()     // Catch:{ all -> 0x0050 }
            if (r0 == 0) goto L_0x005f
            r0.close()     // Catch:{ all -> 0x0068 }
        L_0x005f:
            r0 = r1
        L_0x0060:
            monitor-exit(r5)
            return r0
        L_0x0062:
            if (r6 == 0) goto L_0x006a
            r6.close()     // Catch:{ all -> 0x0068 }
            goto L_0x006a
        L_0x0068:
            r6 = move-exception
            goto L_0x006b
        L_0x006a:
            throw r0     // Catch:{ all -> 0x0068 }
        L_0x006b:
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.l.a(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0037, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void b(java.lang.String r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            r2 = 86400000(0x5265c00, double:4.2687272E-316)
            long r0 = r0 - r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            java.lang.String r3 = "time<"
            r2.<init>(r3)     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            r2.append(r0)     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            java.lang.String r0 = " and unitId=?"
            r2.append(r0)     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            java.lang.String r0 = r2.toString()     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            r1 = 1
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            r2 = 0
            r1[r2] = r5     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            android.database.sqlite.SQLiteDatabase r5 = r4.b()     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            if (r5 == 0) goto L_0x0031
            android.database.sqlite.SQLiteDatabase r5 = r4.b()     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            java.lang.String r2 = "exclude_info"
            r5.delete(r2, r0, r1)     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
        L_0x0031:
            monitor-exit(r4)
            return
        L_0x0033:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        L_0x0036:
            monitor-exit(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.l.b(java.lang.String):void");
    }
}
