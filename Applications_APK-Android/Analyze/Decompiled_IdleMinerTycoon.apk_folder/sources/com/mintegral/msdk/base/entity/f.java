package com.mintegral.msdk.base.entity;

/* compiled from: ExcludeInfo */
public final class f {
    private String a;
    private String b;
    private int c;
    private long d;

    public final String a() {
        return this.a;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final int c() {
        return this.c;
    }

    public final long d() {
        return this.d;
    }

    public final void a(long j) {
        this.d = j;
    }
}
