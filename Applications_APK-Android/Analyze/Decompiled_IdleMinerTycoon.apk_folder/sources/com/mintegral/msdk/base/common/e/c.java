package com.mintegral.msdk.base.common.e;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.appsflyer.AppsFlyerProperties;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.helpshift.support.constants.FaqsColumns;
import com.helpshift.util.ErrorReportProvider;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.net.Aa;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.o;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.r;
import com.mintegral.msdk.d.b;
import com.mintegral.msdk.mtgbid.common.BidResponsedEx;
import com.mintegral.msdk.out.MTGConfiguration;
import com.mintegral.msdk.rover.e;
import com.tapjoy.TapjoyConstants;
import im.getsocial.sdk.activities.MentionTypes;
import im.getsocial.sdk.consts.LanguageCodes;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: ReportUtil */
public class c {
    public static final String a = "c";

    private static String b(Context context, String str) {
        if (context == null) {
            return "";
        }
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("pf", "1");
            jSONObject.put("ov", com.mintegral.msdk.base.utils.c.i());
            jSONObject.put("pn", com.mintegral.msdk.base.utils.c.q(context));
            jSONObject.put("vn", com.mintegral.msdk.base.utils.c.l(context));
            jSONObject.put("vc", com.mintegral.msdk.base.utils.c.k(context));
            jSONObject.put("ot", com.mintegral.msdk.base.utils.c.i(context));
            jSONObject.put("dm", com.mintegral.msdk.base.utils.c.c());
            jSONObject.put("bd", com.mintegral.msdk.base.utils.c.e());
            jSONObject.put("gaid", com.mintegral.msdk.base.utils.c.k());
            jSONObject.put(Constants.RequestParameters.NETWORK_MNC, com.mintegral.msdk.base.utils.c.b());
            jSONObject.put(Constants.RequestParameters.NETWORK_MCC, com.mintegral.msdk.base.utils.c.a());
            int s = com.mintegral.msdk.base.utils.c.s(context);
            jSONObject.put("nt", s);
            jSONObject.put("nts", com.mintegral.msdk.base.utils.c.a(context, s));
            jSONObject.put("l", com.mintegral.msdk.base.utils.c.h(context));
            jSONObject.put("tz", com.mintegral.msdk.base.utils.c.h());
            jSONObject.put("ua", com.mintegral.msdk.base.utils.c.f());
            jSONObject.put("app_id", a.d().j());
            jSONObject.put(MIntegralConstans.PROPERTIES_UNIT_ID, str);
            jSONObject.put(LanguageCodes.SWEDISH, MTGConfiguration.SDK_VERSION);
            jSONObject.put("gpv", com.mintegral.msdk.base.utils.c.t(context));
            jSONObject.put("ss", com.mintegral.msdk.base.utils.c.n(context) + "x" + com.mintegral.msdk.base.utils.c.o(context));
            b.a();
            com.mintegral.msdk.d.a b = b.b(a.d().j());
            if (b != null) {
                JSONObject jSONObject2 = new JSONObject();
                try {
                    if (b.as() == 1) {
                        if (com.mintegral.msdk.base.utils.c.c(context) != null) {
                            jSONObject2.put("imei", com.mintegral.msdk.base.utils.c.c(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.j(context) != null) {
                            jSONObject2.put("mac", com.mintegral.msdk.base.utils.c.j(context));
                        }
                    }
                    if (b.au() == 1 && com.mintegral.msdk.base.utils.c.f(context) != null) {
                        jSONObject2.put(TapjoyConstants.TJC_ANDROID_ID, com.mintegral.msdk.base.utils.c.f(context));
                    }
                    try {
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.o())) {
                            jSONObject2.put("manufacturer", com.mintegral.msdk.base.utils.c.o());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.p())) {
                            jSONObject2.put("cpu2", com.mintegral.msdk.base.utils.c.p());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.r())) {
                            jSONObject2.put(FaqsColumns.TAGS, com.mintegral.msdk.base.utils.c.r());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.s())) {
                            jSONObject2.put(MentionTypes.USER, com.mintegral.msdk.base.utils.c.s());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.t())) {
                            jSONObject2.put("radio", com.mintegral.msdk.base.utils.c.t());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.u())) {
                            jSONObject2.put("bootloader", com.mintegral.msdk.base.utils.c.u());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.v())) {
                            jSONObject2.put("hardware", com.mintegral.msdk.base.utils.c.v());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.w())) {
                            jSONObject2.put("host", com.mintegral.msdk.base.utils.c.w());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.x())) {
                            jSONObject2.put("codename", com.mintegral.msdk.base.utils.c.x());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.y())) {
                            jSONObject2.put("incremental", com.mintegral.msdk.base.utils.c.y());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.z())) {
                            jSONObject2.put("serial", com.mintegral.msdk.base.utils.c.z());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.A())) {
                            jSONObject2.put("display", com.mintegral.msdk.base.utils.c.A());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.B())) {
                            jSONObject2.put("board", com.mintegral.msdk.base.utils.c.B());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.C())) {
                            jSONObject2.put("type", com.mintegral.msdk.base.utils.c.C());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.q())) {
                            jSONObject2.put("support", com.mintegral.msdk.base.utils.c.q());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.D())) {
                            jSONObject2.put("release", com.mintegral.msdk.base.utils.c.D());
                        }
                        if (com.mintegral.msdk.base.utils.c.E() != -1) {
                            jSONObject2.put("sdkint", com.mintegral.msdk.base.utils.c.E());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.w(context))) {
                            jSONObject2.put("battery", com.mintegral.msdk.base.utils.c.w(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.v(context) != -1) {
                            jSONObject2.put("batterystatus", com.mintegral.msdk.base.utils.c.v(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.F() != -1) {
                            jSONObject2.put("baseos", com.mintegral.msdk.base.utils.c.F());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.y(context))) {
                            jSONObject2.put("is24H", com.mintegral.msdk.base.utils.c.y(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.z(context) != -1) {
                            jSONObject2.put("sensor", com.mintegral.msdk.base.utils.c.z(context));
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.A(context))) {
                            jSONObject2.put("ime", com.mintegral.msdk.base.utils.c.A(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.x(context) != -1) {
                            jSONObject2.put("phonetype", com.mintegral.msdk.base.utils.c.x(context));
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.G())) {
                            jSONObject2.put("totalram", com.mintegral.msdk.base.utils.c.G());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.B(context))) {
                            jSONObject2.put("totalmemory", com.mintegral.msdk.base.utils.c.B(context));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                    if (!TextUtils.isEmpty(jSONObject2.toString())) {
                        String b2 = com.mintegral.msdk.base.utils.a.b(jSONObject2.toString());
                        if (!TextUtils.isEmpty(b2)) {
                            jSONObject.put("dvi", URLEncoder.encode(b2, "utf-8"));
                        }
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            return jSONObject.toString();
        } catch (Exception e3) {
            e3.printStackTrace();
            return "";
        }
    }

    public static String a(int i, String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("category", "adtrack");
            jSONObject.put("action", str);
            jSONObject.put("label", i);
            jSONObject.put("value", "");
        } catch (Exception unused) {
            g.d(a, "ad track data failed !");
        }
        return jSONObject.toString();
    }

    public static l a(String str, String str2, Context context, String str3) {
        String str4;
        if (context != null) {
            context = context.getApplicationContext();
        }
        l lVar = new l();
        lVar.a("m_device_info", b(context, str3));
        lVar.a("m_action", str);
        try {
            if (!TextUtils.isEmpty(str2)) {
                String str5 = a;
                g.a(str5, "8.5.0 add channel ,before value : " + str2);
                String a2 = Aa.a();
                if (a2 == null) {
                    a2 = "";
                }
                JSONObject jSONObject = new JSONObject(str2);
                jSONObject.put(AppsFlyerProperties.CHANNEL, a2);
                str4 = jSONObject.toString();
                try {
                    String str6 = a;
                    g.a(str6, "8.5.0 add channel ,update value : " + str4);
                } catch (Throwable th) {
                    Throwable th2 = th;
                    str2 = str4;
                    th = th2;
                }
                lVar.a("m_data", str4);
                lVar.a("m_sdk", "msdk");
                return lVar;
            }
        } catch (Throwable th3) {
            th = th3;
            th.printStackTrace();
            str4 = str2;
            lVar.a("m_data", str4);
            lVar.a("m_sdk", "msdk");
            return lVar;
        }
        str4 = str2;
        lVar.a("m_data", str4);
        lVar.a("m_sdk", "msdk");
        return lVar;
    }

    private static l a(Context context) {
        l lVar = new l();
        try {
            lVar.a(TapjoyConstants.TJC_PLATFORM, "1");
            lVar.a(CampaignEx.JSON_KEY_PACKAGE_NAME, URLEncoder.encode(com.mintegral.msdk.base.utils.c.q(context)));
            com.mintegral.msdk.base.controller.authoritycontroller.a.a();
            if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                lVar.a(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, Build.VERSION.RELEASE);
                lVar.a("brand", URLEncoder.encode(com.mintegral.msdk.base.utils.c.e()));
                lVar.a("model", URLEncoder.encode(com.mintegral.msdk.base.utils.c.c()));
                lVar.a("gaid", com.mintegral.msdk.base.utils.c.k());
                lVar.a(Constants.RequestParameters.NETWORK_MNC, com.mintegral.msdk.base.utils.c.b());
                lVar.a(Constants.RequestParameters.NETWORK_MCC, com.mintegral.msdk.base.utils.c.a());
                int s = com.mintegral.msdk.base.utils.c.s(context);
                lVar.a("network_type", String.valueOf(s));
                lVar.a("network_str", com.mintegral.msdk.base.utils.c.a(context, s));
                lVar.a(FaqsColumns.LANGUAGE, URLEncoder.encode(com.mintegral.msdk.base.utils.c.h(context)));
                lVar.a(TapjoyConstants.TJC_DEVICE_TIMEZONE, URLEncoder.encode(com.mintegral.msdk.base.utils.c.h()));
                lVar.a("ua", URLEncoder.encode(com.mintegral.msdk.base.utils.c.f()));
                lVar.a("gp_version", URLEncoder.encode(com.mintegral.msdk.base.utils.c.t(context)));
            }
            lVar.a("sdk_version", MTGConfiguration.SDK_VERSION);
            lVar.a("app_version_name", URLEncoder.encode(com.mintegral.msdk.base.utils.c.l(context)));
            StringBuilder sb = new StringBuilder();
            sb.append(com.mintegral.msdk.base.utils.c.i(context));
            lVar.a("orientation", URLEncoder.encode(sb.toString()));
            try {
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                    Class.forName("com.google.android.gms.common.GooglePlayServicesUtil");
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE);
                    lVar.a("gpsv", sb2.toString());
                }
            } catch (Exception unused) {
                g.d(a, "can't find com.google.android.gms.common.GooglePlayServicesUtil class");
            } catch (Throwable th) {
                th.printStackTrace();
            }
            lVar.a("screen_size", com.mintegral.msdk.base.utils.c.n(context) + "x" + com.mintegral.msdk.base.utils.c.o(context));
            b.a();
            com.mintegral.msdk.d.a b = b.b(a.d().j());
            if (b != null) {
                JSONObject jSONObject = new JSONObject();
                try {
                    if (b.as() == 1) {
                        if (com.mintegral.msdk.base.utils.c.c(context) != null) {
                            jSONObject.put("imei", com.mintegral.msdk.base.utils.c.c(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.j(context) != null) {
                            jSONObject.put("mac", com.mintegral.msdk.base.utils.c.j(context));
                        }
                    }
                    if (b.au() == 1 && com.mintegral.msdk.base.utils.c.f(context) != null) {
                        jSONObject.put(TapjoyConstants.TJC_ANDROID_ID, com.mintegral.msdk.base.utils.c.f(context));
                    }
                    try {
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.o())) {
                            jSONObject.put("manufacturer", com.mintegral.msdk.base.utils.c.o());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.p())) {
                            jSONObject.put("cpu2", com.mintegral.msdk.base.utils.c.p());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.r())) {
                            jSONObject.put(FaqsColumns.TAGS, com.mintegral.msdk.base.utils.c.r());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.s())) {
                            jSONObject.put(MentionTypes.USER, com.mintegral.msdk.base.utils.c.s());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.t())) {
                            jSONObject.put("radio", com.mintegral.msdk.base.utils.c.t());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.u())) {
                            jSONObject.put("bootloader", com.mintegral.msdk.base.utils.c.u());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.v())) {
                            jSONObject.put("hardware", com.mintegral.msdk.base.utils.c.v());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.w())) {
                            jSONObject.put("host", com.mintegral.msdk.base.utils.c.w());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.x())) {
                            jSONObject.put("codename", com.mintegral.msdk.base.utils.c.x());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.y())) {
                            jSONObject.put("incremental", com.mintegral.msdk.base.utils.c.y());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.z())) {
                            jSONObject.put("serial", com.mintegral.msdk.base.utils.c.z());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.A())) {
                            jSONObject.put("display", com.mintegral.msdk.base.utils.c.A());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.B())) {
                            jSONObject.put("board", com.mintegral.msdk.base.utils.c.B());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.C())) {
                            jSONObject.put("type", com.mintegral.msdk.base.utils.c.C());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.q())) {
                            jSONObject.put("support", com.mintegral.msdk.base.utils.c.q());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.D())) {
                            jSONObject.put("release", com.mintegral.msdk.base.utils.c.D());
                        }
                        if (com.mintegral.msdk.base.utils.c.E() != -1) {
                            jSONObject.put("sdkint", com.mintegral.msdk.base.utils.c.E());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.w(context))) {
                            jSONObject.put("battery", com.mintegral.msdk.base.utils.c.w(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.v(context) != -1) {
                            jSONObject.put("batterystatus", com.mintegral.msdk.base.utils.c.v(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.F() != -1) {
                            jSONObject.put("baseos", com.mintegral.msdk.base.utils.c.F());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.y(context))) {
                            jSONObject.put("is24H", com.mintegral.msdk.base.utils.c.y(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.z(context) != -1) {
                            jSONObject.put("sensor", com.mintegral.msdk.base.utils.c.z(context));
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.A(context))) {
                            jSONObject.put("ime", com.mintegral.msdk.base.utils.c.A(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.x(context) != -1) {
                            jSONObject.put("phonetype", com.mintegral.msdk.base.utils.c.x(context));
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.G())) {
                            jSONObject.put("totalram", com.mintegral.msdk.base.utils.c.G());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.B(context))) {
                            jSONObject.put("totalmemory", com.mintegral.msdk.base.utils.c.B(context));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } catch (Throwable th2) {
                        th2.printStackTrace();
                    }
                    if (!TextUtils.isEmpty(jSONObject.toString())) {
                        String b2 = com.mintegral.msdk.base.utils.a.b(jSONObject.toString());
                        if (!TextUtils.isEmpty(b2)) {
                            lVar.a("dvi", b2);
                        }
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        } catch (Exception e3) {
            if (MIntegralConstans.DEBUG) {
                e3.printStackTrace();
            }
        }
        return lVar;
    }

    public static l a(Context context, o oVar) {
        try {
            l a2 = a(context);
            try {
                a2.a("app_id", a.d().j());
                a2.a("data", URLEncoder.encode(oVar.d()));
                return a2;
            } catch (Exception unused) {
                return a2;
            }
        } catch (Exception unused2) {
            return null;
        }
    }

    public static l a(String str, Context context) {
        l a2 = a(context);
        a2.a("app_id", a.d().j());
        if (!TextUtils.isEmpty(str)) {
            try {
                a2.a("data", URLEncoder.encode(str, "utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        a2.a("m_sdk", "msdk");
        return a2;
    }

    public static l a(Context context, String str) {
        l a2 = a(context);
        a2.a("app_id", a.d().j());
        if (!TextUtils.isEmpty(str)) {
            try {
                a2.a("data", URLEncoder.encode(str, "utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return a2;
    }

    public static l a(String str, Context context, String str2) {
        l a2 = a(context);
        a2.a("app_id", a.d().j());
        a2.a(MIntegralConstans.PROPERTIES_UNIT_ID, str2);
        if (!TextUtils.isEmpty(str)) {
            try {
                a2.a("data", URLEncoder.encode(str, "utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        a2.a("m_sdk", "msdk");
        return a2;
    }

    public static l b(String str, Context context) {
        l a2 = a(context);
        a2.a("app_id", a.d().j());
        if (!TextUtils.isEmpty(str)) {
            try {
                a2.a("data", URLEncoder.encode(str, "utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        a2.a("m_sdk", "msdk");
        return a2;
    }

    public static String a(String str, Map<String, Object> map) {
        StringBuilder sb = new StringBuilder();
        sb.append("key=" + str);
        if (!map.isEmpty()) {
            for (Map.Entry next : map.entrySet()) {
                sb.append(Constants.RequestParameters.AMPERSAND + ((String) next.getKey()) + Constants.RequestParameters.EQUAL + next.getValue());
            }
        }
        sb.append("\n");
        return sb.toString();
    }

    public static l a(CampaignEx campaignEx, List<e> list) {
        l lVar = new l();
        JSONArray jSONArray = new JSONArray();
        JSONObject jSONObject = new JSONObject();
        if (campaignEx != null) {
            try {
                jSONObject.put(BidResponsedEx.KEY_CID, campaignEx.getId());
                jSONObject.put(CampaignEx.ROVER_KEY_MARK, campaignEx.getRoverMark());
            } catch (Exception unused) {
            }
        }
        if (list != null && list.size() >= 0) {
            JSONArray jSONArray2 = new JSONArray();
            for (e next : list) {
                if (next != null) {
                    jSONArray2.put(next.a());
                }
            }
            jSONObject.put("urls", jSONArray2);
        }
        jSONArray.put(jSONObject);
        lVar.a("data", jSONArray.toString());
        return lVar;
    }

    public static boolean a() {
        try {
            if (System.currentTimeMillis() - ErrorReportProvider.BATCH_TIME > ((Long) r.b(a.d().h(), "privateAuthorityTimesTamp", 0L)).longValue()) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    public static void b() {
        try {
            r.a(a.d().h(), "privateAuthorityTimesTamp", Long.valueOf(System.currentTimeMillis()));
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void a(Context context, CampaignEx campaignEx, int i, int i2) {
        try {
            StringBuffer stringBuffer = new StringBuffer("key=2000056&");
            if (campaignEx != null) {
                stringBuffer.append("cid=" + campaignEx.getId() + Constants.RequestParameters.AMPERSAND);
            }
            b.a();
            com.mintegral.msdk.d.a b = b.b(a.d().j());
            if (b == null) {
                b.a();
                b = b.b();
            }
            stringBuffer.append("unit_id=" + b.j() + Constants.RequestParameters.AMPERSAND);
            String k = com.mintegral.msdk.base.utils.c.k();
            if (!TextUtils.isEmpty(k)) {
                stringBuffer.append("gaid=" + k + Constants.RequestParameters.AMPERSAND);
            }
            stringBuffer.append("action_type=" + i + Constants.RequestParameters.AMPERSAND);
            stringBuffer.append("jm_a=" + com.mintegral.msdk.e.b.a(context).c() + Constants.RequestParameters.AMPERSAND);
            stringBuffer.append("jm_n=" + com.mintegral.msdk.e.b.a(context).a() + Constants.RequestParameters.AMPERSAND);
            if (campaignEx != null) {
                stringBuffer.append("rid_n=" + campaignEx.getRequestIdNotice() + Constants.RequestParameters.AMPERSAND);
            }
            stringBuffer.append("result_type=" + i2);
            new b(context).b(stringBuffer.toString());
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void a(Context context, String str, String str2, boolean z) {
        if (context != null) {
            try {
                if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000047&");
                    stringBuffer.append("network_type=" + com.mintegral.msdk.base.utils.c.s(context) + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("unit_id=" + str2 + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("hb=");
                    stringBuffer.append(z ? 1 : 0);
                    stringBuffer.append(Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("reason=" + str);
                    String stringBuffer2 = stringBuffer.toString();
                    if (context != null && !TextUtils.isEmpty(stringBuffer2)) {
                        com.mintegral.msdk.base.common.e.d.a aVar = new com.mintegral.msdk.base.common.e.d.a(context);
                        aVar.c();
                        aVar.b(com.mintegral.msdk.base.common.a.f, b(stringBuffer2, context), new com.mintegral.msdk.base.common.e.d.b() {
                            public final void b(String str) {
                                g.d(c.a, str);
                            }

                            public final void a(String str) {
                                g.d(c.a, str);
                            }
                        });
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                g.d(a, e.getMessage());
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static void a(Context context, List<CampaignEx> list, String str, boolean z) {
        if (context != null && list != null) {
            try {
                if (list.size() > 0 && !TextUtils.isEmpty(str)) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000048&");
                    if (list != null && list.size() > 0) {
                        stringBuffer.append("cid=" + list.get(0).getId() + Constants.RequestParameters.AMPERSAND);
                    }
                    stringBuffer.append("network_type=" + com.mintegral.msdk.base.utils.c.s(context) + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("unit_id=" + str + Constants.RequestParameters.AMPERSAND);
                    if (list != null && list.size() > 1) {
                        String requestId = list.get(0).getRequestId();
                        if (z) {
                            stringBuffer.append("hb=1&");
                        }
                        stringBuffer.append("rid_n=" + requestId);
                    } else if (list != null && list.size() == 1) {
                        String requestIdNotice = list.get(0).getRequestIdNotice();
                        if (z) {
                            stringBuffer.append("hb=1&");
                        }
                        stringBuffer.append("rid_n=" + requestIdNotice);
                    }
                    a(context, stringBuffer.toString(), str);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static void a(Context context, CampaignEx campaignEx, String str) {
        if (context != null && campaignEx != null) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000054&");
                    stringBuffer.append("network_type=" + com.mintegral.msdk.base.utils.c.s(context) + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("unit_id=" + str + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("cid=" + campaignEx.getId() + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("reason=&");
                    stringBuffer.append("result=2&");
                    if (campaignEx != null && campaignEx.getAdType() == 287) {
                        stringBuffer.append("creative=" + campaignEx.getendcard_url() + Constants.RequestParameters.AMPERSAND);
                        stringBuffer.append("ad_type=3&");
                    } else if (campaignEx != null && campaignEx.getAdType() == 94) {
                        stringBuffer.append("creative=" + campaignEx.getendcard_url() + Constants.RequestParameters.AMPERSAND);
                        stringBuffer.append("ad_type=1&");
                    } else if (campaignEx == null || campaignEx.getAdType() != 296) {
                        stringBuffer.append("creative=" + campaignEx.getendcard_url() + Constants.RequestParameters.AMPERSAND);
                        stringBuffer.append("ad_type=1&");
                    } else {
                        stringBuffer.append("ad_type=5&");
                        stringBuffer.append("creative=" + campaignEx.getCreativeId() + Constants.RequestParameters.AMPERSAND);
                    }
                    stringBuffer.append("devid=" + com.mintegral.msdk.base.utils.c.k() + Constants.RequestParameters.AMPERSAND);
                    if (campaignEx != null) {
                        if (campaignEx.isBidCampaign()) {
                            stringBuffer.append("hb=1&");
                        }
                        stringBuffer.append("rid_n=" + campaignEx.getRequestIdNotice());
                    }
                    a(context, stringBuffer.toString(), str);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static void a(Context context, CampaignEx campaignEx, String str, String str2) {
        if (context != null && campaignEx != null) {
            try {
                if ((!TextUtils.isEmpty(str)) && (!TextUtils.isEmpty(str2))) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000054&");
                    stringBuffer.append("network_type=" + com.mintegral.msdk.base.utils.c.s(context) + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("unit_id=" + str + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("cid=" + campaignEx.getId() + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("reason=" + str2 + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("result=1&");
                    stringBuffer.append("devid=" + com.mintegral.msdk.base.utils.c.k() + Constants.RequestParameters.AMPERSAND);
                    if (campaignEx != null && campaignEx.getAdType() == 287) {
                        stringBuffer.append("creative=" + campaignEx.getendcard_url() + Constants.RequestParameters.AMPERSAND);
                        stringBuffer.append("ad_type=3&");
                    } else if (campaignEx != null && campaignEx.getAdType() == 94) {
                        stringBuffer.append("creative=" + campaignEx.getendcard_url() + Constants.RequestParameters.AMPERSAND);
                        stringBuffer.append("ad_type=1&");
                    } else if (campaignEx == null || campaignEx.getAdType() != 296) {
                        stringBuffer.append("creative=" + campaignEx.getendcard_url() + Constants.RequestParameters.AMPERSAND);
                        stringBuffer.append("ad_type=1&");
                    } else {
                        stringBuffer.append("ad_type=5&");
                        stringBuffer.append("creative=" + campaignEx.getCreativeId() + Constants.RequestParameters.AMPERSAND);
                    }
                    if (campaignEx != null) {
                        stringBuffer.append("rid_n=" + campaignEx.getRequestIdNotice());
                    }
                    a(context, stringBuffer.toString(), str);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    private static void a(Context context, String str, String str2) {
        if (context != null && !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            try {
                com.mintegral.msdk.base.common.e.d.a aVar = new com.mintegral.msdk.base.common.e.d.a(context);
                aVar.c();
                aVar.b(com.mintegral.msdk.base.common.a.f, a(str, context, str2), new com.mintegral.msdk.base.common.e.d.b() {
                    public final void b(String str) {
                        g.d(c.a, str);
                    }

                    public final void a(String str) {
                        g.d(c.a, str);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                g.d(a, e.getMessage());
            }
        }
    }
}
