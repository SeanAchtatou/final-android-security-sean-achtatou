package com.mintegral.msdk.base.controller;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.q;
import com.mintegral.msdk.base.entity.h;
import com.mintegral.msdk.base.entity.n;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.l;
import com.mintegral.msdk.base.utils.r;
import com.mintegral.msdk.base.utils.s;
import io.fabric.sdk.android.services.common.AdvertisingInfoServiceStrategy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.LinkedBlockingQueue;

/* compiled from: MTGSDKContext */
public class a {
    public static final String a = "a";
    public static List<String> b = new ArrayList();
    private static a c;
    private static CopyOnWriteArraySet<h> j = new CopyOnWriteArraySet<>();
    /* access modifiers changed from: private */
    public Context d;
    private String e;
    /* access modifiers changed from: private */
    public String f;
    private String g;
    private boolean h = false;
    private List<String> i = null;
    private String k;

    /* compiled from: MTGSDKContext */
    public interface b {
        void a();
    }

    public final String a() {
        try {
            if (this.d != null) {
                return this.d.getPackageName();
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public final List<String> b() {
        return this.i;
    }

    public final void a(String str) {
        try {
            this.k = str;
            if (!TextUtils.isEmpty(str) && this.d != null) {
                r.a(this.d, "applicationIds", str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static Set<h> c() {
        return j;
    }

    private a() {
    }

    public static a d() {
        if (c == null) {
            synchronized (a.class) {
                if (c == null) {
                    c = new a();
                }
            }
        }
        return c;
    }

    public final void a(final b bVar, final Handler handler) {
        if (!this.h) {
            try {
                Object b2 = r.b(this.d, "ga_id", "-1");
                if (b2 != null && (b2 instanceof String)) {
                    String str = (String) b2;
                    if (s.b(str) && !"-1".equals(str)) {
                        String str2 = a;
                        g.b(str2, "sp init gaid:" + str);
                        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                        if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
                            c.a(str);
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            new Thread(new Runnable() {
                public final void run() {
                    com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                    if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
                        try {
                            Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient");
                            AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(a.this.d);
                            c.a(advertisingIdInfo.getId());
                            a.a(a.this, advertisingIdInfo.getId());
                        } catch (Exception unused) {
                            g.c(a.a, "GET ADID ERROR TRY TO GET FROM GOOGLE PLAY APP");
                            try {
                                C0051a.C0052a a2 = new C0051a().a(a.this.d);
                                c.a(a2.a());
                                a.a(a.this, a2.a());
                            } catch (Exception unused2) {
                                g.c(a.a, "GET ADID FROM GOOGLE PLAY APP ERROR");
                            }
                        } catch (Throwable th) {
                            th.printStackTrace();
                        }
                    }
                    try {
                        com.mintegral.msdk.d.b.a();
                        com.mintegral.msdk.d.a b2 = com.mintegral.msdk.d.b.b(a.d().j());
                        if (b2 == null) {
                            com.mintegral.msdk.d.b.a();
                            b2 = com.mintegral.msdk.d.b.b();
                        }
                        Message obtain = Message.obtain();
                        obtain.obj = b2;
                        obtain.what = 9;
                        handler.sendMessage(obtain);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        c.a(a.this.d);
                        a.e();
                        com.mintegral.msdk.d.b.a(a.this.d, a.this.f);
                        a.this.l();
                        a.c(a.this);
                        a.this.a(bVar);
                    } catch (Exception unused3) {
                    }
                }
            }).start();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0063, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void e() {
        /*
            java.lang.Class<com.mintegral.msdk.base.controller.a> r0 = com.mintegral.msdk.base.controller.a.class
            monitor-enter(r0)
            com.mintegral.msdk.base.controller.a r1 = d()     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            java.lang.String r1 = r1.j()     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            if (r1 != 0) goto L_0x005d
            com.mintegral.msdk.d.b.a()     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            com.mintegral.msdk.base.controller.a r1 = d()     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            java.lang.String r1 = r1.j()     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            com.mintegral.msdk.d.a r1 = com.mintegral.msdk.d.b.b(r1)     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            if (r1 == 0) goto L_0x005d
            java.lang.String r1 = r1.aP()     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            if (r2 != 0) goto L_0x005d
            java.lang.String r1 = com.mintegral.msdk.base.utils.a.c(r1)     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            if (r2 != 0) goto L_0x005d
            org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            int r1 = r2.length()     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            if (r1 <= 0) goto L_0x005d
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            r1.<init>()     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            r3 = 0
        L_0x0047:
            int r4 = r2.length()     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            if (r3 >= r4) goto L_0x0057
            java.lang.String r4 = r2.optString(r3)     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            r1.add(r4)     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            int r3 = r3 + 1
            goto L_0x0047
        L_0x0057:
            com.mintegral.msdk.base.controller.a r2 = d()     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            r2.i = r1     // Catch:{ Exception -> 0x0062, all -> 0x005f }
        L_0x005d:
            monitor-exit(r0)
            return
        L_0x005f:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x0062:
            monitor-exit(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.controller.a.e():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a1, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00a6, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001b, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(com.mintegral.msdk.base.controller.a.b r8) {
        /*
            r7 = this;
            monitor-enter(r7)
            android.content.Context r0 = r7.d     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            com.mintegral.msdk.base.utils.l r0 = com.mintegral.msdk.base.utils.l.a(r0)     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            java.lang.String r1 = r7.f     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            java.util.concurrent.CopyOnWriteArraySet r0 = r0.a(r1)     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            com.mintegral.msdk.base.controller.a.j = r0     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            int r0 = r0.size()     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            if (r0 != 0) goto L_0x001c
            if (r8 == 0) goto L_0x001a
            r8.a()     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
        L_0x001a:
            monitor-exit(r7)
            return
        L_0x001c:
            java.util.concurrent.CopyOnWriteArraySet r0 = new java.util.concurrent.CopyOnWriteArraySet     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            r0.<init>()     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            java.util.concurrent.CopyOnWriteArraySet<com.mintegral.msdk.base.entity.h> r1 = com.mintegral.msdk.base.controller.a.j     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            if (r1 == 0) goto L_0x007c
        L_0x0029:
            boolean r2 = r1.hasNext()     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            if (r2 == 0) goto L_0x007c
            java.lang.Object r2 = r1.next()     // Catch:{ Exception -> 0x0070 }
            com.mintegral.msdk.base.entity.h r2 = (com.mintegral.msdk.base.entity.h) r2     // Catch:{ Exception -> 0x0070 }
            java.util.List<java.lang.String> r3 = com.mintegral.msdk.base.controller.a.b     // Catch:{ Exception -> 0x0070 }
            if (r3 == 0) goto L_0x0029
            java.util.List<java.lang.String> r3 = com.mintegral.msdk.base.controller.a.b     // Catch:{ Exception -> 0x0070 }
            int r3 = r3.size()     // Catch:{ Exception -> 0x0070 }
            if (r3 <= 0) goto L_0x0029
            if (r2 == 0) goto L_0x0029
            r3 = 0
        L_0x0044:
            java.util.List<java.lang.String> r4 = com.mintegral.msdk.base.controller.a.b     // Catch:{ Exception -> 0x0070 }
            int r4 = r4.size()     // Catch:{ Exception -> 0x0070 }
            if (r3 >= r4) goto L_0x0029
            java.util.List<java.lang.String> r4 = com.mintegral.msdk.base.controller.a.b     // Catch:{ Exception -> 0x0070 }
            java.lang.Object r4 = r4.get(r3)     // Catch:{ Exception -> 0x0070 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ Exception -> 0x0070 }
            java.lang.String r5 = r2.b()     // Catch:{ Exception -> 0x0070 }
            boolean r6 = android.text.TextUtils.isEmpty(r4)     // Catch:{ Exception -> 0x0070 }
            if (r6 != 0) goto L_0x006d
            boolean r6 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Exception -> 0x0070 }
            if (r6 != 0) goto L_0x006d
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x0070 }
            if (r4 == 0) goto L_0x006d
            r0.add(r2)     // Catch:{ Exception -> 0x0070 }
        L_0x006d:
            int r3 = r3 + 1
            goto L_0x0044
        L_0x0070:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            java.lang.String r2 = com.mintegral.msdk.base.controller.a.a     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            java.lang.String r3 = "remove list error"
            com.mintegral.msdk.base.utils.g.d(r2, r3)     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            goto L_0x0029
        L_0x007c:
            java.util.concurrent.CopyOnWriteArraySet<com.mintegral.msdk.base.entity.h> r1 = com.mintegral.msdk.base.controller.a.j     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            if (r1 == 0) goto L_0x0085
            java.util.concurrent.CopyOnWriteArraySet<com.mintegral.msdk.base.entity.h> r1 = com.mintegral.msdk.base.controller.a.j     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            r1.clear()     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
        L_0x0085:
            int r1 = r0.size()     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            if (r1 <= 0) goto L_0x0090
            java.util.concurrent.CopyOnWriteArraySet<com.mintegral.msdk.base.entity.h> r1 = com.mintegral.msdk.base.controller.a.j     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            r1.addAll(r0)     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
        L_0x0090:
            android.content.Context r0 = r7.d     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            com.mintegral.msdk.base.utils.l r0 = com.mintegral.msdk.base.utils.l.a(r0)     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            java.util.concurrent.CopyOnWriteArraySet<com.mintegral.msdk.base.entity.h> r1 = com.mintegral.msdk.base.controller.a.j     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            r0.a(r1)     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
            if (r8 == 0) goto L_0x00a0
            r8.a()     // Catch:{ Throwable -> 0x00a5, all -> 0x00a2 }
        L_0x00a0:
            monitor-exit(r7)
            return
        L_0x00a2:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        L_0x00a5:
            monitor-exit(r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.controller.a.a(com.mintegral.msdk.base.controller.a$b):void");
    }

    public final void f() {
        try {
            if (j != null && j.size() > 0) {
                l.a(this.d).a(j);
            }
        } catch (Throwable unused) {
        }
    }

    public static List<Long> g() {
        try {
            if (j == null || j.size() <= 0) {
                return null;
            }
            Iterator<h> it = j.iterator();
            ArrayList arrayList = new ArrayList();
            while (it.hasNext()) {
                h next = it.next();
                if (!arrayList.contains(next.a())) {
                    arrayList.add(Long.valueOf(Long.parseLong(next.a())));
                }
            }
            return arrayList;
        } catch (NumberFormatException e2) {
            e2.printStackTrace();
        } catch (Throwable unused) {
            return null;
        }
    }

    public final Context h() {
        return this.d;
    }

    public final void a(Context context) {
        this.d = context;
    }

    public final String i() {
        return this.e;
    }

    public final void b(String str) {
        this.e = str;
    }

    public final String j() {
        try {
            if (!TextUtils.isEmpty(this.f)) {
                return this.f;
            }
            if (this.d != null) {
                return (String) r.b(this.d, "sp_appId", "");
            }
            return "";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public final void c(String str) {
        try {
            this.f = str;
            if (!TextUtils.isEmpty(str) && this.d != null) {
                r.a(this.d, "sp_appId", str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final String k() {
        if (!TextUtils.isEmpty(this.g)) {
            return this.g;
        }
        if (this.d != null) {
            return (String) r.b(this.d, "sp_appKey", "");
        }
        return null;
    }

    public final void d(String str) {
        try {
            this.g = str;
            if (!TextUtils.isEmpty(str) && this.d != null) {
                r.a(this.d, "sp_appKey", str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final List<String> l() {
        try {
            List<String> list = d().i;
            if (b != null && b.size() > 0) {
                return b;
            }
            List<PackageInfo> installedPackages = this.d.getPackageManager().getInstalledPackages(0);
            for (int i2 = 0; i2 < installedPackages.size(); i2++) {
                if ((installedPackages.get(i2).applicationInfo.flags & 1) <= 0) {
                    b.add(installedPackages.get(i2).packageName);
                } else if (list != null && list.size() > 0 && list.contains(installedPackages.get(i2).packageName)) {
                    b.add(installedPackages.get(i2).packageName);
                }
            }
            return b;
        } catch (Exception unused) {
            g.d(a, "get package info list error");
            return null;
        }
    }

    /* renamed from: com.mintegral.msdk.base.controller.a$a  reason: collision with other inner class name */
    /* compiled from: MTGSDKContext */
    public class C0051a {
        public C0051a() {
        }

        /* renamed from: com.mintegral.msdk.base.controller.a$a$a  reason: collision with other inner class name */
        /* compiled from: MTGSDKContext */
        public final class C0052a {
            private final String b;
            private final boolean c;

            C0052a(String str, boolean z) {
                this.b = str;
                this.c = z;
            }

            public final String a() {
                return this.b;
            }
        }

        public final C0052a a(Context context) throws Exception {
            if (Looper.myLooper() != Looper.getMainLooper()) {
                try {
                    context.getPackageManager().getPackageInfo("com.android.vending", 0);
                    b bVar = new b(this, (byte) 0);
                    Intent intent = new Intent(AdvertisingInfoServiceStrategy.GOOGLE_PLAY_SERVICES_INTENT);
                    intent.setPackage("com.google.android.gms");
                    if (context.bindService(intent, bVar, 1)) {
                        try {
                            c cVar = new c(bVar.a());
                            C0052a aVar = new C0052a(cVar.a(), cVar.b());
                            context.unbindService(bVar);
                            return aVar;
                        } catch (Exception e) {
                            throw e;
                        } catch (Throwable th) {
                            context.unbindService(bVar);
                            throw th;
                        }
                    } else {
                        throw new IOException("Google Play connection failed");
                    }
                } catch (Exception e2) {
                    throw e2;
                }
            } else {
                throw new IllegalStateException("Cannot be called from the main thread");
            }
        }

        /* renamed from: com.mintegral.msdk.base.controller.a$a$b */
        /* compiled from: MTGSDKContext */
        private final class b implements ServiceConnection {
            boolean a;
            private final LinkedBlockingQueue<IBinder> c;

            public final void onServiceDisconnected(ComponentName componentName) {
            }

            private b() {
                this.a = false;
                this.c = new LinkedBlockingQueue<>(1);
            }

            /* synthetic */ b(C0051a aVar, byte b2) {
                this();
            }

            public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                try {
                    this.c.put(iBinder);
                } catch (InterruptedException unused) {
                }
            }

            public final IBinder a() throws InterruptedException {
                if (!this.a) {
                    this.a = true;
                    return this.c.take();
                }
                throw new IllegalStateException();
            }
        }

        /* renamed from: com.mintegral.msdk.base.controller.a$a$c */
        /* compiled from: MTGSDKContext */
        private final class c implements IInterface {
            private IBinder b;

            public c(IBinder iBinder) {
                this.b = iBinder;
            }

            public final IBinder asBinder() {
                return this.b;
            }

            /* JADX INFO: finally extract failed */
            public final String a() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(AdvertisingInfoServiceStrategy.AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
                    this.b.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    String readString = obtain2.readString();
                    obtain2.recycle();
                    obtain.recycle();
                    return readString;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                    throw th;
                }
            }

            public final boolean b() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                boolean z = false;
                try {
                    obtain.writeInterfaceToken(AdvertisingInfoServiceStrategy.AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
                    obtain.writeInt(1);
                    this.b.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                    throw th;
                }
                obtain2.recycle();
                obtain.recycle();
                return z;
            }
        }
    }

    static /* synthetic */ void a(a aVar, String str) {
        try {
            if (s.b(str)) {
                String str2 = a;
                g.b(str2, "saveGAID gaid:" + str);
                r.a(aVar.d, "ga_id", str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void c(a aVar) {
        ArrayList arrayList;
        try {
            long currentTimeMillis = System.currentTimeMillis();
            if (b != null && b.size() > 0) {
                ArrayList<n> arrayList2 = new ArrayList<>();
                PackageManager packageManager = aVar.d.getPackageManager();
                for (String next : b) {
                    if (!TextUtils.isEmpty(next)) {
                        n nVar = new n();
                        nVar.a(currentTimeMillis);
                        nVar.a(next);
                        nVar.a(1);
                        if (packageManager != null) {
                            nVar.b(packageManager.getInstallerPackageName(next));
                        }
                        arrayList2.add(nVar);
                    }
                }
                if (arrayList2.size() > 0) {
                    List<n> c2 = q.a(i.a(aVar.d)).c();
                    if (c2 != null) {
                        arrayList = new ArrayList(c2);
                    } else {
                        arrayList = new ArrayList();
                    }
                    if (c2 == null || c2.size() <= 0) {
                        q.a(i.a(aVar.d)).a(arrayList2);
                        return;
                    }
                    boolean z = false;
                    for (n next2 : c2) {
                        boolean z2 = false;
                        for (n nVar2 : arrayList2) {
                            if (nVar2.a().equals(next2.a())) {
                                nVar2.a(4);
                                z2 = true;
                            }
                        }
                        if (!(z2 || next2.b() == 0 || next2.b() == 1)) {
                            n nVar3 = new n();
                            nVar3.a(2);
                            nVar3.a(next2.a());
                            nVar3.a(currentTimeMillis);
                            nVar3.b(next2.d());
                            arrayList.add(nVar3);
                            z = true;
                        }
                    }
                    for (n nVar4 : arrayList2) {
                        if (nVar4.b() != 4) {
                            n nVar5 = new n();
                            nVar5.a(3);
                            nVar5.a(nVar4.a());
                            nVar5.a(currentTimeMillis);
                            nVar5.b(nVar4.d());
                            arrayList.add(nVar5);
                            z = true;
                        }
                    }
                    if (z) {
                        q.a(i.a(aVar.d)).a(arrayList);
                    }
                }
            }
        } catch (Exception e2) {
            if (MIntegralConstans.DEBUG) {
                e2.printStackTrace();
            }
        }
    }
}
