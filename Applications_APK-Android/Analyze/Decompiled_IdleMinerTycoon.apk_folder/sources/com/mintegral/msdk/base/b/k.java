package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import com.helpshift.common.domain.network.NetworkConstants;
import com.helpshift.util.ErrorReportProvider;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.e;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: DisplayResourceTypeDao */
public class k extends a<e> {
    private static final String b = "com.mintegral.msdk.base.b.k";
    private static k c;

    private k(h hVar) {
        super(hVar);
    }

    public static k a(h hVar) {
        if (c == null) {
            synchronized (k.class) {
                if (c == null) {
                    c = new k(hVar);
                }
            }
        }
        return c;
    }

    public final synchronized void a(e eVar) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("campaign_id", Long.valueOf(eVar.a));
            contentValues.put("click_time", Long.valueOf(eVar.b));
            contentValues.put("is_click", Integer.valueOf(eVar.d));
            contentValues.put("resource_type", Integer.valueOf(eVar.c));
            if (b() != null) {
                b().insert("display_resource_type", null, contentValues);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final String c() {
        boolean z;
        long currentTimeMillis = System.currentTimeMillis();
        List<e> a = a(currentTimeMillis);
        if (a == null) {
            return "";
        }
        String[] strArr = {"1", "2", NetworkConstants.apiVersion, "4", CampaignEx.CLICKMODE_ON, "6", "7"};
        JSONObject jSONObject = new JSONObject();
        ArrayList arrayList = new ArrayList(7);
        for (int i = 0; i < 7; i++) {
            arrayList.add(new JSONArray());
        }
        try {
            for (e next : a) {
                JSONArray jSONArray = (JSONArray) arrayList.get((int) ((currentTimeMillis - next.b) / ErrorReportProvider.BATCH_TIME));
                if (jSONArray.length() == 0) {
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put(String.valueOf(next.c), next.d);
                    jSONArray.put(jSONObject2);
                } else {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= jSONArray.length()) {
                            z = true;
                            break;
                        }
                        JSONObject jSONObject3 = jSONArray.getJSONObject(i2);
                        String valueOf = String.valueOf(next.c);
                        if (jSONObject3.has(valueOf)) {
                            if (jSONObject3.getInt(valueOf) == 0 && next.d == 1) {
                                jSONObject3.put(valueOf, next.d);
                            }
                            z = false;
                        } else {
                            i2++;
                        }
                    }
                    if (z) {
                        JSONObject jSONObject4 = new JSONObject();
                        jSONObject4.put(String.valueOf(next.c), next.d);
                        jSONArray.put(jSONObject4);
                    }
                }
            }
            for (int i3 = 0; i3 < 7; i3++) {
                jSONObject.put(strArr[i3], arrayList.get(i3));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0075, code lost:
        if (r10 == null) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0085, code lost:
        if (r10 == null) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0089, code lost:
        return r11;
     */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x008d A[SYNTHETIC, Splitter:B:37:0x008d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized java.util.List<com.mintegral.msdk.base.entity.e> a(long r10) {
        /*
            r9 = this;
            monitor-enter(r9)
            r0 = 604800000(0x240c8400, double:2.988109026E-315)
            long r10 = r10 - r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0091 }
            java.lang.String r1 = "select * from display_resource_type where click_time >= "
            r0.<init>(r1)     // Catch:{ all -> 0x0091 }
            r0.append(r10)     // Catch:{ all -> 0x0091 }
            java.lang.String r10 = r0.toString()     // Catch:{ all -> 0x0091 }
            android.database.sqlite.SQLiteDatabase r11 = r9.a()     // Catch:{ all -> 0x0091 }
            r0 = 0
            if (r11 != 0) goto L_0x001c
            monitor-exit(r9)
            return r0
        L_0x001c:
            android.database.sqlite.SQLiteDatabase r11 = r9.a()     // Catch:{ Exception -> 0x007e, all -> 0x007b }
            android.database.Cursor r10 = r11.rawQuery(r10, r0)     // Catch:{ Exception -> 0x007e, all -> 0x007b }
            if (r10 == 0) goto L_0x0074
            boolean r11 = r10.moveToFirst()     // Catch:{ Exception -> 0x006f }
            if (r11 == 0) goto L_0x0074
            java.util.ArrayList r11 = new java.util.ArrayList     // Catch:{ Exception -> 0x006f }
            int r1 = r10.getCount()     // Catch:{ Exception -> 0x006f }
            r11.<init>(r1)     // Catch:{ Exception -> 0x006f }
        L_0x0035:
            java.lang.String r0 = "click_time"
            int r0 = r10.getColumnIndex(r0)     // Catch:{ Exception -> 0x006d }
            long r4 = r10.getLong(r0)     // Catch:{ Exception -> 0x006d }
            java.lang.String r0 = "campaign_id"
            int r0 = r10.getColumnIndex(r0)     // Catch:{ Exception -> 0x006d }
            long r2 = r10.getLong(r0)     // Catch:{ Exception -> 0x006d }
            java.lang.String r0 = "resource_type"
            int r0 = r10.getColumnIndex(r0)     // Catch:{ Exception -> 0x006d }
            int r6 = r10.getInt(r0)     // Catch:{ Exception -> 0x006d }
            java.lang.String r0 = "is_click"
            int r0 = r10.getColumnIndex(r0)     // Catch:{ Exception -> 0x006d }
            int r7 = r10.getInt(r0)     // Catch:{ Exception -> 0x006d }
            com.mintegral.msdk.base.entity.e r0 = new com.mintegral.msdk.base.entity.e     // Catch:{ Exception -> 0x006d }
            r1 = r0
            r1.<init>(r2, r4, r6, r7)     // Catch:{ Exception -> 0x006d }
            r11.add(r0)     // Catch:{ Exception -> 0x006d }
            boolean r0 = r10.moveToNext()     // Catch:{ Exception -> 0x006d }
            if (r0 != 0) goto L_0x0035
            goto L_0x0075
        L_0x006d:
            r0 = move-exception
            goto L_0x0082
        L_0x006f:
            r11 = move-exception
            r8 = r0
            r0 = r11
            r11 = r8
            goto L_0x0082
        L_0x0074:
            r11 = r0
        L_0x0075:
            if (r10 == 0) goto L_0x0088
        L_0x0077:
            r10.close()     // Catch:{ all -> 0x0091 }
            goto L_0x0088
        L_0x007b:
            r11 = move-exception
            r10 = r0
            goto L_0x008b
        L_0x007e:
            r10 = move-exception
            r11 = r0
            r0 = r10
            r10 = r11
        L_0x0082:
            r0.printStackTrace()     // Catch:{ all -> 0x008a }
            if (r10 == 0) goto L_0x0088
            goto L_0x0077
        L_0x0088:
            monitor-exit(r9)
            return r11
        L_0x008a:
            r11 = move-exception
        L_0x008b:
            if (r10 == 0) goto L_0x0090
            r10.close()     // Catch:{ all -> 0x0091 }
        L_0x0090:
            throw r11     // Catch:{ all -> 0x0091 }
        L_0x0091:
            r10 = move-exception
            monitor-exit(r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.k.a(long):java.util.List");
    }
}
