package com.mintegral.msdk.base.common.g;

import com.mintegral.msdk.base.common.e.c;
import com.mintegral.msdk.base.common.e.d.b;
import com.mintegral.msdk.base.utils.g;

/* compiled from: ReportTask */
public final class a implements Runnable {
    private String a;
    private String b;

    public a(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public final void run() {
        try {
            g.d("ReportTask", "start report");
            com.mintegral.msdk.base.common.e.d.a aVar = new com.mintegral.msdk.base.common.e.d.a(com.mintegral.msdk.base.controller.a.d().h());
            aVar.c();
            aVar.b(com.mintegral.msdk.base.common.a.f, c.a(this.a, com.mintegral.msdk.base.controller.a.d().h(), this.b), new b() {
                public final void b(String str) {
                    g.d("ReportTask", str);
                }

                public final void a(String str) {
                    g.d("ReportTask", str);
                }
            });
        } catch (Throwable th) {
            g.d("ReportTask", th.getMessage());
        }
    }
}
