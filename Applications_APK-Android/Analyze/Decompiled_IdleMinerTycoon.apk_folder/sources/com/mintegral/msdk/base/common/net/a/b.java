package com.mintegral.msdk.base.common.net.a;

import com.mintegral.msdk.base.common.net.d;
import com.mintegral.msdk.base.utils.g;
import org.apache.http.HttpEntity;
import org.json.JSONArray;
import org.json.JSONException;

/* compiled from: CommonJSONArrayResponseHandler */
public abstract class b extends d<JSONArray> {
    /* access modifiers changed from: private */
    /* renamed from: e */
    public JSONArray b(HttpEntity httpEntity) throws Exception {
        try {
            return d(httpEntity);
        } catch (JSONException e) {
            g.a("JSONArrayResponseHandler", e);
            g.c("JSONArrayResponseHandler", "wrong json format : " + c(httpEntity));
            return null;
        }
    }
}
