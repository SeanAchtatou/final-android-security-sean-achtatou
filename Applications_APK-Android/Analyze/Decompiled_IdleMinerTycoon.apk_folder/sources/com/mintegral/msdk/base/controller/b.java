package com.mintegral.msdk.base.controller;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.appsflyer.share.Constants;
import com.facebook.appevents.AppEventsConstants;
import com.helpshift.util.ErrorReportProvider;
import com.iab.omid.library.mintegral.Omid;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.MIntegralSDK;
import com.mintegral.msdk.MIntegralUser;
import com.mintegral.msdk.base.b.n;
import com.mintegral.msdk.base.b.p;
import com.mintegral.msdk.base.b.t;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.d;
import com.mintegral.msdk.base.entity.m;
import com.mintegral.msdk.base.entity.o;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.e;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.i;
import com.mintegral.msdk.base.utils.j;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.r;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.out.AdMobClickListener;
import com.mintegral.msdk.out.Frame;
import com.mintegral.msdk.out.MIntegralSDKFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;

/* compiled from: SDKController */
public class b {
    private static b j;
    public final int a = 1;
    public final int b = 2;
    public final int c = 3;
    public final int d = 4;
    public final int e = 5;
    public final int f = 6;
    public final int g = 7;
    public final int h = 8;
    Handler i = new Handler() {
        public final void handleMessage(Message message) {
            List list;
            List<o> list2;
            try {
                switch (message.what) {
                    case 2:
                        if ((message.obj instanceof List) && (list = (List) message.obj) != null && list.size() > 0) {
                            com.mintegral.msdk.base.common.e.b bVar = new com.mintegral.msdk.base.common.e.b(b.this.k, 0);
                            for (int i = 0; i < list.size(); i++) {
                                o oVar = (o) list.get(i);
                                boolean z = false;
                                if (i == list.size()) {
                                    z = true;
                                }
                                bVar.a(oVar, z);
                            }
                            return;
                        }
                        return;
                    case 3:
                        File file = (File) message.obj;
                        if (file != null) {
                            String a2 = e.a(file);
                            if (!TextUtils.isEmpty(a2)) {
                                String[] split = a2.split("====");
                                if (split.length > 0) {
                                    new com.mintegral.msdk.base.common.e.b(b.this.k).a(split[0], file);
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    case 4:
                        String str = (String) message.obj;
                        if (!TextUtils.isEmpty(str)) {
                            new com.mintegral.msdk.base.common.e.b(b.this.k, 0).a("click_duration", str, (String) null, (Frame) null);
                            return;
                        }
                        return;
                    case 5:
                        String str2 = (String) message.obj;
                        if (!TextUtils.isEmpty(str2)) {
                            new com.mintegral.msdk.base.common.e.b(b.this.k, 0).a("load_duration", str2, (String) null, (Frame) null);
                            return;
                        }
                        return;
                    case 6:
                        if (message.obj != null && (message.obj instanceof String)) {
                            String str3 = (String) message.obj;
                            if (!TextUtils.isEmpty(str3)) {
                                new com.mintegral.msdk.base.common.e.b(b.this.k, 0).a("device_data", str3, (String) null, (Frame) null);
                                return;
                            }
                            return;
                        }
                        return;
                    case 7:
                        if (message.obj != null && (message.obj instanceof List) && (list2 = (List) message.obj) != null && list2.size() > 0) {
                            com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                            if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_APPLIST)) {
                                r.a(b.this.k, "mintegral_setting_campaign_time", new Long(System.currentTimeMillis()));
                                for (o a3 : list2) {
                                    new com.mintegral.msdk.base.common.e.b(b.this.k, 0).a(a3);
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    case 8:
                        if (message.obj != null && (message.obj instanceof String)) {
                            String str4 = (String) message.obj;
                            if (s.b(str4)) {
                                new com.mintegral.msdk.base.common.e.b(b.this.k, 0).a(str4);
                                return;
                            }
                            return;
                        }
                        return;
                    case 9:
                        if (((com.mintegral.msdk.d.a) message.obj).x() == 1) {
                            com.mintegral.msdk.base.common.e.b.a.a(b.this.k).a();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            } catch (Exception unused) {
                g.d("SDKController", "REPORT HANDLE ERROR!");
            }
        }
    };
    /* access modifiers changed from: private */
    public Context k;
    private String l;
    private String m;
    /* access modifiers changed from: private */
    public String n;
    private String o;
    private boolean p = false;
    private com.mintegral.msdk.c.a q;
    private String r;
    private long s;
    private MIntegralUser t;
    private boolean u = false;
    private Timer v;
    private Map<String, Object> w;
    private int x;
    private AdMobClickListener y;

    private b() {
    }

    public static b a() {
        if (j == null) {
            synchronized (b.class) {
                if (j == null) {
                    j = new b();
                }
            }
        }
        return j;
    }

    public final void a(Map map, Context context) {
        Context context2;
        Class<?> cls;
        Object newInstance;
        Object invoke;
        MIntegralUser mIntegralUser;
        if (context != null) {
            if (map.containsKey(MIntegralConstans.ID_MINTEGRAL_APPID)) {
                this.n = (String) map.get(MIntegralConstans.ID_MINTEGRAL_APPID);
            }
            this.k = context.getApplicationContext();
            a.d().a(this.k);
            try {
                String str = (String) map.get(MIntegralConstans.ID_MINTEGRAL_APPID);
                if (!TextUtils.isEmpty(str)) {
                    com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.SDK_APP_ID, str);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            } catch (Throwable th) {
                th.printStackTrace();
            }
            if (Build.VERSION.SDK_INT < 26) {
                com.mintegral.msdk.e.b.a(context).a(this.n);
            }
            if (this.p) {
                com.mintegral.msdk.rover.b a2 = com.mintegral.msdk.rover.b.a();
                a2.a(context);
                a2.b();
                new com.mintegral.msdk.base.common.e.b(context).a();
                return;
            }
            if (this.t == null) {
                this.s = ((Long) r.b(context, "mintegral_user_expiretime", 0L)).longValue();
                if (System.currentTimeMillis() - this.s < 259200000) {
                    String str2 = (String) r.b(context, "mintegral_user", "");
                    mIntegralUser = !TextUtils.isEmpty(str2) ? MIntegralUser.getMintegralUser(str2) : null;
                    g.a("com.mintegral.msdk", "load user,json:" + str2);
                } else {
                    g.a("com.mintegral.msdk", "user expire,clear user");
                    i();
                    mIntegralUser = null;
                }
                this.t = mIntegralUser;
            }
            c.r(context);
            if (map != null) {
                if (map.containsKey(MIntegralConstans.ID_FACE_BOOK_PLACEMENT)) {
                    this.l = (String) map.get(MIntegralConstans.ID_FACE_BOOK_PLACEMENT);
                }
                if (map.containsKey(MIntegralConstans.ID_MINTEGRAL_APPID)) {
                    this.n = (String) map.get(MIntegralConstans.ID_MINTEGRAL_APPID);
                }
                if (map.containsKey(MIntegralConstans.ID_MINTEGRAL_APPKEY)) {
                    this.o = (String) map.get(MIntegralConstans.ID_MINTEGRAL_APPKEY);
                }
                if (map.containsKey(MIntegralConstans.PACKAGE_NAME_MANIFEST)) {
                    this.r = (String) map.get(MIntegralConstans.PACKAGE_NAME_MANIFEST);
                }
                if (map.containsKey(MIntegralConstans.ID_MINTEGRAL_STARTUPCRASH)) {
                    this.m = (String) map.get(MIntegralConstans.ID_MINTEGRAL_STARTUPCRASH);
                }
                g();
                a.d().c(this.n);
                a.d().d(this.o);
                a.d().b(this.l);
                a.d().a(this.r);
                a.d().a(new a.b() {
                    public final void a() {
                        b.c(b.this);
                        b.d(b.this);
                    }
                }, this.i);
                Context applicationContext = this.k.getApplicationContext();
                try {
                    if (!Omid.activateWithOmidApiVersion(Omid.getVersion(), applicationContext)) {
                        new com.mintegral.msdk.base.common.e.b(applicationContext, 0).b("", "", "", "activate om failed");
                    }
                } catch (IllegalArgumentException e3) {
                    g.c("SDKController", e3.getMessage(), e3);
                }
                g.b("SDKController", "facebook = " + this.l + "appId = " + this.n + "appKey = " + this.o);
                try {
                    if (MIntegralConstans.INIT_UA_IN) {
                        com.mintegral.msdk.base.common.g.b.a().execute(new Runnable() {
                            public final void run() {
                                Looper.prepare();
                                b.f();
                                b.a(b.this, b.this.n);
                                Looper.loop();
                            }
                        });
                    } else if (Looper.myLooper() == Looper.getMainLooper()) {
                        com.mintegral.msdk.base.common.g.b.a().execute(new Runnable() {
                            public final void run() {
                                Looper.prepare();
                                b.f();
                                Looper.loop();
                            }
                        });
                    } else {
                        f();
                    }
                } catch (Exception unused) {
                    g.d("SDKController", "get app setting failed");
                }
                c();
                if (MIntegralConstans.INIT_UA_IN) {
                    try {
                        Class<?> cls2 = Class.forName("com.mintegral.msdk.appwall.service.HandlerProvider");
                        new HashMap().put(MIntegralConstans.PLUGIN_NAME, new String[]{MIntegralConstans.PLUGIN_WALL});
                        cls2.getMethod("getLayout", Context.class, String.class, String.class).invoke(cls2.newInstance(), this.k, this.n, null);
                    } catch (Exception unused2) {
                        g.b("SDKController", "if you integrate the appwall ad style,please import appwall.aar or ignore it");
                    }
                }
                i.a(this.k);
                try {
                    Class<?> cls3 = Class.forName("com.alphab.AlphabFactory");
                    if (this.k != null) {
                        if (this.k instanceof Activity) {
                            context2 = this.k.getApplicationContext();
                        } else {
                            context2 = this.k;
                        }
                        if (!(cls3 == null || (newInstance = (cls = Class.forName("com.alphab.i.AlphabRelFactory")).newInstance()) == null || (invoke = cls.getMethod("createAlphab", new Class[0]).invoke(newInstance, new Object[0])) == null)) {
                            Class.forName("com.alphab.Alphab").getMethod("init", Context.class).invoke(Class.forName("com.alphab.Operation").getMethod("process", new Class[0]).invoke(invoke, new Object[0]), context2);
                        }
                    }
                } catch (Throwable th2) {
                    if (MIntegralConstans.DEBUG) {
                        th2.printStackTrace();
                    }
                }
                try {
                    com.mintegral.msdk.d.b.a();
                    com.mintegral.msdk.d.a b2 = com.mintegral.msdk.d.b.b(a.d().j());
                    if (b2 == null) {
                        g.b("SDKController", "initPB 获取默认的appsetting");
                        com.mintegral.msdk.d.b.a();
                        b2 = com.mintegral.msdk.d.b.b();
                    }
                    if (b2.G() > 0) {
                        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                        if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_APP_PROGRESS)) {
                            int D = b2.D();
                            try {
                                g.b("SDKController", "pf:" + D);
                                if (this.v != null) {
                                    this.v.cancel();
                                }
                            } catch (Throwable th3) {
                                g.c("SDKController", th3.getMessage(), th3);
                            }
                            this.v = new Timer();
                            this.v.schedule(new a(this, (byte) 0), 0, (long) (D * 1000));
                            this.p = true;
                            j.a(this.n, context);
                            com.mintegral.msdk.rover.b a3 = com.mintegral.msdk.rover.b.a();
                            a3.a(context);
                            a3.b();
                        }
                    }
                    g.b("SDKController", "initPB pmax 为0 return");
                } catch (Throwable th4) {
                    g.c("SDKController", th4.getMessage(), th4);
                }
                this.p = true;
                j.a(this.n, context);
                com.mintegral.msdk.rover.b a32 = com.mintegral.msdk.rover.b.a();
                a32.a(context);
                a32.b();
            }
        }
    }

    /* compiled from: SDKController */
    private class a extends TimerTask {
        private a() {
        }

        /* synthetic */ a(b bVar, byte b) {
            this();
        }

        public final void run() {
            ArrayList arrayList;
            boolean z;
            Iterator<m> it;
            try {
                com.mintegral.msdk.d.b.a();
                com.mintegral.msdk.d.a b = com.mintegral.msdk.d.b.b(a.d().j());
                if (b == null) {
                    com.mintegral.msdk.d.b.a();
                    b = com.mintegral.msdk.d.b.b();
                    g.b("SDKController", "PBTask 获取默认的appsetting");
                }
                int G = b.G();
                if (G > 0) {
                    com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                    if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_APP_PROGRESS)) {
                        List<String> F = b.F();
                        List<m> d = k.d();
                        p a2 = p.a(com.mintegral.msdk.base.b.i.a(b.this.k));
                        ArrayList arrayList2 = new ArrayList();
                        ArrayList arrayList3 = new ArrayList();
                        ArrayList arrayList4 = new ArrayList();
                        ArrayList arrayList5 = new ArrayList();
                        g.b("SDKController", "task permission:" + com.mintegral.msdk.base.common.a.u);
                        if (com.mintegral.msdk.base.common.a.u) {
                            List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) b.this.k.getSystemService("activity")).getRunningTasks(1);
                            if (!runningTasks.isEmpty()) {
                                ComponentName componentName = runningTasks.get(0).topActivity;
                                arrayList5.add(componentName.getClassName() + "|" + componentName.getPackageName());
                            }
                        }
                        Object b2 = r.b(b.this.k, "pb_first_report", true);
                        boolean booleanValue = (b2 == null || !(b2 instanceof Boolean)) ? false : ((Boolean) b2).booleanValue();
                        long j = 0;
                        Object b3 = r.b(b.this.k, "pb_pre_report_time", 0L);
                        if (b3 != null && (b3 instanceof Long)) {
                            j = ((Long) b3).longValue();
                        }
                        long j2 = j;
                        long currentTimeMillis = System.currentTimeMillis();
                        long j3 = currentTimeMillis - j2;
                        if (j3 > ErrorReportProvider.BATCH_TIME) {
                            arrayList = arrayList5;
                            try {
                                g.b("SDKController", "超过24小时 curTime:" + currentTimeMillis + "  preReportTime:" + j2 + " 超过的时间分：" + (j3 / 60000) + " full重新上报");
                                z = true;
                            } catch (Throwable th) {
                                th = th;
                                g.c("SDKController", th.getMessage(), th);
                            }
                        } else {
                            arrayList = arrayList5;
                            z = false;
                        }
                        List<m> d2 = a2.d();
                        StringBuilder sb = new StringBuilder("full数组是否超时isExpire:");
                        sb.append(z);
                        sb.append("  firstReport:");
                        sb.append(booleanValue);
                        sb.append(" dbPBList:");
                        sb.append(d2 == null ? "null" : Integer.valueOf(d2.size()));
                        g.b("SDKController", sb.toString());
                        if (!booleanValue && !z && d2 != null) {
                            if (d2.size() != 0) {
                                String str = d2.get(0).b;
                                int I = b.I();
                                int K = b.K();
                                int M = b.M();
                                g.b("SDKController", "PBTask 非首次 pctrlFull:" + I + "  pctrlAdd:" + K + "  pctrlDele:" + M + " dbPBList.size:" + d2.size() + " uuid:" + str);
                                if (I == 1) {
                                    Iterator<m> it2 = d2.iterator();
                                    while (true) {
                                        if (!it2.hasNext()) {
                                            break;
                                        }
                                        m next = it2.next();
                                        if (arrayList2.size() >= G) {
                                            g.b("SDKController", "PBTask 非首次 fulllist 不能超过pmax break size:" + arrayList2.size() + " pmax:" + G);
                                            break;
                                        }
                                        if (next == null || !AppEventsConstants.EVENT_PARAM_VALUE_NO.equals(next.c) || !s.b(next.a) || b.a(F, next.a)) {
                                            it = it2;
                                        } else {
                                            next.c = "1";
                                            arrayList2.add(next.a);
                                            a2.a(next, str);
                                            it = it2;
                                            g.b("SDKController", "PBTask 非首次 full insertOrUpdate pb：" + next.a);
                                        }
                                        it2 = it;
                                    }
                                }
                                if (d != null && d.size() > 0) {
                                    g.b("SDKController", "PBTask 非首次 realActivePbList.size:" + d.size());
                                    int i = 1;
                                    if (K == 1) {
                                        List<String> a3 = m.a(d2);
                                        for (m next2 : d) {
                                            if (next2 != null && s.b(next2.a) && !b.a(F, next2.a) && !a3.contains(next2.a)) {
                                                next2.c = "1";
                                                arrayList3.add(next2.a);
                                                a2.a(next2, str);
                                                g.b("SDKController", "PBTask 非首次 add insertOrUpdate pb：" + next2.a + " uuid:" + str);
                                            }
                                        }
                                        i = 1;
                                    }
                                    if (M == i) {
                                        List<String> a4 = m.a(d);
                                        for (m next3 : d2) {
                                            if (next3 != null && s.b(next3.a) && !b.a(F, next3.a) && !a4.contains(next3.a)) {
                                                arrayList4.add(next3.a);
                                                a2.a(next3.a);
                                                g.b("SDKController", "PBTask 非首次 dele deleteByPKG pb：" + next3.a);
                                            }
                                        }
                                    }
                                }
                                if (arrayList2.size() > 0 || arrayList3.size() > 0 || arrayList4.size() > 0) {
                                    try {
                                        b.a(b.this, arrayList2, arrayList3, arrayList4, arrayList, str, booleanValue);
                                        return;
                                    } catch (Throwable th2) {
                                        th = th2;
                                        g.c("SDKController", th.getMessage(), th);
                                    }
                                } else {
                                    g.d("SDKController", "PBTask 非首次 不上报 集合大小都为0");
                                    return;
                                }
                            }
                        }
                        int i2 = 0;
                        if ((d == null || d.size() <= 0) && b.I() != 0) {
                            g.b("SDKController", " PBTask 首次full 但是active pb为0 或者pctrlfull为0 pctrlfull:" + b.I());
                            return;
                        }
                        g.b("SDKController", "PBTask 首次 realActivePbList.size:" + d.size());
                        while (true) {
                            if (i2 >= d.size()) {
                                break;
                            } else if (arrayList2.size() >= G) {
                                g.b("SDKController", "PBTask 首次 fulllist 不能超过pmax break first＝true size:" + arrayList2.size() + " pmax:" + G);
                                break;
                            } else {
                                m mVar = d.get(i2);
                                if (mVar != null && s.b(mVar.a) && !b.a(F, mVar.a)) {
                                    mVar.c = "1";
                                    arrayList2.add(mVar.a);
                                }
                                i2++;
                            }
                        }
                        if (arrayList2.size() > 0) {
                            a2.c();
                            String e = k.e();
                            a2.a(d, e);
                            b.a(b.this, arrayList2, null, null, arrayList, e, booleanValue);
                            g.a("SDKController", "PBTask 首次插入full insertOrUpdate size：" + d.size());
                            return;
                        }
                        g.a("SDKController", "PBTask 首次插入full active pb 为 0");
                        return;
                    }
                }
                g.b("SDKController", "PBTask pmax ＝0 return");
            } catch (Throwable th3) {
                th = th3;
                g.c("SDKController", th.getMessage(), th);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void f() {
        try {
            Class<?> cls = Class.forName("com.mintegral.msdk.f.b");
            cls.getDeclaredMethod("start", new Class[0]).invoke(cls.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]), new Object[0]);
        } catch (Throwable th) {
            g.c("SDKController", th.getMessage(), th);
        }
    }

    private void g() {
        List<com.mintegral.msdk.base.entity.b> a2;
        Class<?> cls;
        Object newInstance;
        Object newInstance2;
        try {
            com.mintegral.msdk.d.b.a();
            com.mintegral.msdk.d.a b2 = com.mintegral.msdk.d.b.b(a.d().j());
            if (b2 != null && (a2 = b2.a()) != null && a2.size() > 0) {
                for (com.mintegral.msdk.base.entity.b next : a2) {
                    if (next.a() == 287) {
                        Class<?> cls2 = Class.forName("com.mintegral.msdk.interstitialvideo.out.MTGInterstitialVideoHandler");
                        if (!(this.k == null || cls2 == null || (newInstance2 = cls2.getConstructor(String.class).newInstance(next.b())) == null)) {
                            cls2.getMethod("loadFormSelfFilling", new Class[0]).invoke(newInstance2, new Object[0]);
                        }
                    } else if (!(next.a() != 94 || (cls = Class.forName("com.mintegral.msdk.out.MTGRewardVideoHandler")) == null || (newInstance = cls.getConstructor(String.class).newInstance(next.b())) == null)) {
                        cls.getMethod("loadFormSelfFilling", new Class[0]).invoke(newInstance, new Object[0]);
                    }
                }
            }
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }

    public final void b() {
        if (this.i != null) {
            this.i.removeCallbacksAndMessages(null);
        }
    }

    public final void c() {
        new Thread(new Runnable() {
            public final void run() {
                String str;
                try {
                    com.mintegral.msdk.base.common.e.c.a.a();
                    List<o> a2 = com.mintegral.msdk.base.common.e.c.a.a(b.this.k);
                    if (a2 != null && a2.size() > 0) {
                        Message obtainMessage = b.this.i.obtainMessage();
                        obtainMessage.what = 2;
                        obtainMessage.obj = a2;
                        b.this.i.sendMessage(obtainMessage);
                    }
                    com.mintegral.msdk.base.b.i a3 = com.mintegral.msdk.base.b.i.a(b.this.k);
                    com.mintegral.msdk.base.b.e a4 = com.mintegral.msdk.base.b.e.a(a3);
                    if (a4.d() >= 20) {
                        String a5 = com.mintegral.msdk.base.entity.c.a(a4.c());
                        Message obtain = Message.obtain();
                        obtain.obj = a5;
                        obtain.what = 4;
                        b.this.i.sendMessage(obtain);
                    }
                    n a6 = n.a(a3);
                    if (a6.c() > 20) {
                        List<com.mintegral.msdk.base.entity.i> d = a6.d();
                        if (d == null || d.size() <= 0) {
                            str = null;
                        } else {
                            StringBuffer stringBuffer = new StringBuffer();
                            for (com.mintegral.msdk.base.entity.i next : d) {
                                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                                    stringBuffer.append("ad_source_id=" + next.b());
                                    stringBuffer.append("&time=" + next.d());
                                    stringBuffer.append("&num=" + next.e());
                                    stringBuffer.append("&unit_id=" + next.f());
                                    stringBuffer.append("&key=2000006");
                                    stringBuffer.append("&fb=" + next.g());
                                    stringBuffer.append("&network_str=" + next.k());
                                    stringBuffer.append("&network_type=" + next.j());
                                } else {
                                    stringBuffer.append("ad_source_id=" + next.b());
                                    stringBuffer.append("&time=" + next.d());
                                    stringBuffer.append("&num=" + next.e());
                                    stringBuffer.append("&unit_id=" + next.f());
                                    stringBuffer.append("&key=2000006");
                                    stringBuffer.append("&fb=" + next.g());
                                }
                                if (next.a() == 1) {
                                    stringBuffer.append("&hb=1");
                                }
                                stringBuffer.append("&timeout=" + next.h() + "\n");
                            }
                            str = stringBuffer.toString();
                        }
                        Message obtain2 = Message.obtain();
                        obtain2.obj = str;
                        obtain2.what = 5;
                        b.this.i.sendMessage(obtain2);
                    }
                    String b = com.mintegral.msdk.base.common.b.e.b(com.mintegral.msdk.base.common.b.c.MINTEGRAL_CRASH_INFO);
                    File file = new File(b);
                    if (file.exists() && file.isDirectory() && file.list().length > 0) {
                        for (String str2 : file.list()) {
                            File file2 = new File(b + Constants.URL_PATH_DELIMITER + str2);
                            Message obtain3 = Message.obtain();
                            obtain3.obj = file2;
                            obtain3.what = 3;
                            b.this.i.sendMessage(obtain3);
                        }
                    }
                } catch (Exception unused) {
                    g.d("SDKController", "report netstate error !");
                }
            }
        }).start();
    }

    public final void a(AdMobClickListener adMobClickListener) {
        this.y = adMobClickListener;
    }

    public final void a(Map<String, Object> map, int i2) {
        if (MIntegralSDKFactory.getMIntegralSDK().getStatus() != MIntegralSDK.PLUGIN_LOAD_STATUS.COMPLETED) {
            g.d("SDKController", "preloaad failed,sdk do not inited");
            return;
        }
        this.w = map;
        this.x = i2;
        a.d().j();
        if (map != null) {
            if (this.q == null) {
                this.q = new com.mintegral.msdk.c.a();
            }
            try {
                if (this.w != null && this.w.size() > 0 && this.w.containsKey(MIntegralConstans.PROPERTIES_LAYOUT_TYPE)) {
                    int intValue = ((Integer) this.w.get(MIntegralConstans.PROPERTIES_LAYOUT_TYPE)).intValue();
                    if (intValue == 0) {
                        com.mintegral.msdk.c.a.a(this.w, this.x, this.y);
                    } else if (3 == intValue) {
                        com.mintegral.msdk.c.a.a(this.w);
                    } else if (1 == intValue) {
                        com.mintegral.msdk.c.a.a();
                    } else if (2 == intValue) {
                        com.mintegral.msdk.c.a.b();
                    } else {
                        g.d("SDKController", "unknow layout type in preload");
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private static String h() {
        try {
            JSONArray jSONArray = new JSONArray();
            a.d();
            List<Long> g2 = a.g();
            if (g2 != null && g2.size() > 0) {
                for (Long longValue : g2) {
                    jSONArray.put(longValue.longValue());
                }
            }
            if (jSONArray.length() > 0) {
                return k.a(jSONArray);
            }
            return "";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public final synchronized void a(MIntegralUser mIntegralUser) {
        if (mIntegralUser != null) {
            if (this.t == mIntegralUser) {
                g.a("com.mintegral.msdk", "can't update mintegralUser");
                return;
            }
        }
        this.u = true;
        g.a("com.mintegral.msdk", "update mintegralUser:" + mIntegralUser);
        b(mIntegralUser);
    }

    public final MIntegralUser d() {
        if (System.currentTimeMillis() - this.s < 259200000 || this.s == 0) {
            g.a("com.mintegral.msdk", "getMIntegralUser,user:" + this.t);
            if (this.u) {
                b(this.t);
            }
        } else {
            g.a("com.mintegral.msdk", "getMIntegralUser,expire");
            i();
        }
        return this.t;
    }

    private synchronized void b(MIntegralUser mIntegralUser) {
        this.t = mIntegralUser;
        if (!this.u || this.k == null) {
            g.a("com.mintegral.msdk", "can't write mintegralUser");
            return;
        }
        String json = MIntegralUser.toJSON(mIntegralUser);
        if (!TextUtils.isEmpty(json)) {
            r.a(this.k, "mintegral_user", json);
            this.s = System.currentTimeMillis();
            r.a(this.k, "mintegral_user_expiretime", Long.valueOf(this.s));
        } else {
            i();
        }
        g.a("com.mintegral.msdk", "writeMintegralUser,json:" + json);
        this.u = false;
    }

    private void i() {
        this.t = null;
        this.s = 0;
        if (!this.u || this.k == null) {
            g.a("com.mintegral.msdk", "can't clear mintegralUser");
            return;
        }
        g.a("com.mintegral.msdk", "clearMintegralUser");
        r.a(this.k, "mintegral_user", "");
        r.a(this.k, "mintegral_user_expiretime", 0L);
        this.u = false;
    }

    static /* synthetic */ void a(b bVar, String str) {
        if (com.mintegral.msdk.d.b.a() != null) {
            com.mintegral.msdk.d.b.a();
            if (com.mintegral.msdk.d.b.a(str)) {
                com.mintegral.msdk.d.b.a();
                if (com.mintegral.msdk.d.b.a(str, 1, (String) null)) {
                    new com.mintegral.msdk.d.c().a(bVar.k, str, bVar.o);
                    return;
                }
            }
            new com.mintegral.msdk.d.c().b(bVar.k, str, bVar.o);
            new com.mintegral.msdk.base.common.e.b(bVar.k).a();
            if (com.mintegral.msdk.d.b.a() != null) {
                com.mintegral.msdk.d.a b2 = com.mintegral.msdk.d.b.b(str);
                if (b2 != null) {
                    MIntegralConstans.OMID_JS_SERVICE_URL = b2.bh();
                } else {
                    MIntegralConstans.OMID_JS_SERVICE_URL = "https://cdn-adn-https.rayjump.com/cdn-adn/v2/portal/19/08/20/11/06/5d5b63cb457e2.js";
                }
            }
        }
    }

    static /* synthetic */ boolean a(List list, String str) {
        if (list == null || !list.contains(str)) {
            return false;
        }
        g.b("SDKController", "PBTask fulllist black pb 过滤掉");
        return true;
    }

    static /* synthetic */ void a(b bVar, List list, List list2, List list3, List list4, String str, boolean z) {
        Message obtain = Message.obtain();
        obtain.obj = m.a(str, list, list2, list3, list4, z);
        obtain.what = 8;
        bVar.i.sendMessage(obtain);
        g.d("SDKController", "PBTask 上报数据");
        r.a(bVar.k, "pb_first_report", false);
        r.a(bVar.k, "pb_pre_report_time", Long.valueOf(System.currentTimeMillis()));
    }

    static /* synthetic */ void c(b bVar) {
        try {
            int g2 = k.g(bVar.k);
            String h2 = h();
            g.d("SDKController", "appInstallNums:" + g2 + " installIds:" + h2);
            String a2 = d.a(g2, h2);
            if (s.b(a2)) {
                Message obtain = Message.obtain();
                obtain.obj = a2;
                obtain.what = 6;
                bVar.i.sendMessage(obtain);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void d(b bVar) {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            com.mintegral.msdk.d.b.a();
            com.mintegral.msdk.d.a b2 = com.mintegral.msdk.d.b.b(bVar.n);
            if (b2 == null) {
                com.mintegral.msdk.d.b.a();
                b2 = com.mintegral.msdk.d.b.b();
            }
            long longValue = ((Long) r.b(bVar.k, "mintegral_setting_campaign_time", new Long(0))).longValue();
            if (longValue > 0 && longValue + ((long) (b2.Q() * 1000)) > currentTimeMillis) {
                return;
            }
            if (b2.O() <= 0) {
                t.a(com.mintegral.msdk.base.b.i.a(a.d().h())).d();
                return;
            }
            List<o> e2 = t.a(com.mintegral.msdk.base.b.i.a(bVar.k)).e();
            if (e2 != null && e2.size() > 0) {
                Message obtain = Message.obtain();
                obtain.obj = e2;
                obtain.what = 7;
                bVar.i.sendMessage(obtain);
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }
}
