package com.mintegral.msdk.base.common.net;

import android.text.TextUtils;
import com.mintegral.msdk.base.utils.g;
import java.security.KeyStore;
import java.util.Locale;
import org.apache.http.HttpHost;
import org.apache.http.HttpVersion;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

/* compiled from: CommonPPHttpClient */
public final class i extends DefaultHttpClient {
    private j a;
    private RuntimeException b;

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    /* compiled from: CommonPPHttpClient */
    public static final class a extends Enum<a> {
        public static final int a = 1;
        public static final int b = 2;
        private static final /* synthetic */ int[] c = {a, b};
    }

    public i(String str, int i, j jVar) {
        this(a(str), i, jVar);
    }

    private i(int i, int i2, j jVar) {
        this.b = new IllegalStateException("QihooHttpClient created and never closed");
        HttpParams params = getParams();
        this.a = jVar;
        if (!TextUtils.isEmpty(this.a.b)) {
            params.setParameter("http.route.default-proxy", new HttpHost(this.a.b, Integer.parseInt(this.a.c)));
        }
        params.setParameter("http.connection.timeout", Integer.valueOf(i2));
        params.setParameter("http.socket.timeout", Integer.valueOf(i2));
        params.setParameter("http.socket.buffer-size", 8192);
        params.setParameter("http.protocol.allow-circular-redirects", false);
        try {
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, "UTF-8");
            setHttpRequestRetryHandler(new DefaultHttpRequestRetryHandler(0, false));
        } catch (Throwable unused) {
            g.d("PPHttpClient", "setversion deprecated");
        }
        if (!TextUtils.isEmpty(null)) {
            HttpProtocolParams.setUserAgent(params, (String) null);
        }
        if (i != a.a && i == a.b) {
            try {
                KeyStore.getInstance(KeyStore.getDefaultType()).load(null, null);
                SchemeRegistry schemeRegistry = getConnectionManager().getSchemeRegistry();
                g.a("PPHttpClient", "system api level lower than API 8 FROYO");
                schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
            } catch (Exception e) {
                g.c("PPHttpClient", "Exception", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        i.super.finalize();
        if (this.b != null) {
            g.c("PPHttpClient", "Leak found", this.b);
        }
    }

    public final void a() {
        if (this.b != null) {
            try {
                getConnectionManager().shutdown();
            } catch (Throwable th) {
                th.getStackTrace();
            }
            this.b = null;
        }
    }

    /* access modifiers changed from: protected */
    public final HttpParams createHttpParams() {
        HttpParams httpParams;
        try {
            httpParams = i.super.createHttpParams();
            try {
                HttpProtocolParams.setUseExpectContinue(httpParams, false);
            } catch (Throwable unused) {
            }
        } catch (Throwable unused2) {
            httpParams = null;
            g.d("PPHttpClient", "createHttpParams  failed");
            return httpParams;
        }
        return httpParams;
    }

    public static int a(String str) {
        if (str.toLowerCase(Locale.US).startsWith("https")) {
            return a.b;
        }
        return a.a;
    }
}
