package com.mintegral.msdk.base.common.net;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLKeyException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import org.apache.http.NoHttpResponseException;

/* compiled from: CommonRequestExceptionManager */
public final class k {
    private static HashSet<Class<? extends Exception>> a = new HashSet<>();
    private static HashSet<Class<? extends Exception>> b = new HashSet<>();
    private static HashMap<Class<? extends Exception>, Integer> c = new HashMap<>();
    private int d;
    private int e;
    private int f;

    static {
        a.add(NoHttpResponseException.class);
        a.add(UnknownHostException.class);
        a.add(SocketTimeoutException.class);
        a.add(SocketException.class);
        a.add(ConnectException.class);
        b.add(InterruptedException.class);
        b.add(SSLHandshakeException.class);
        b.add(SSLKeyException.class);
        b.add(SSLPeerUnverifiedException.class);
        b.add(SSLProtocolException.class);
        b.add(SSLException.class);
        b.add(NullPointerException.class);
        c.put(SSLException.class, 6);
        c.put(SocketTimeoutException.class, 3);
        c.put(SocketException.class, 2);
        c.put(NoHttpResponseException.class, 8);
        c.put(SSLHandshakeException.class, 6);
        c.put(SSLKeyException.class, 6);
        c.put(SSLPeerUnverifiedException.class, 6);
        c.put(SSLProtocolException.class, 6);
        c.put(SSLException.class, 6);
        c.put(InterruptedException.class, -2);
        c.put(IOException.class, 2);
        c.put(NullPointerException.class, 4);
        c.put(Exception.class, 5);
    }

    k(int i, int i2) {
        this.d = i;
        this.e = i2;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.mintegral.msdk.base.common.net.k.a a(java.lang.Exception r5) {
        /*
            r4 = this;
            com.mintegral.msdk.base.common.net.k$a r0 = new com.mintegral.msdk.base.common.net.k$a
            r0.<init>()
            int r1 = r4.f
            int r2 = r4.d
            r3 = 1
            if (r1 >= r2) goto L_0x001c
            java.util.HashSet<java.lang.Class<? extends java.lang.Exception>> r1 = com.mintegral.msdk.base.common.net.k.a
            boolean r1 = a(r1, r5)
            if (r1 == 0) goto L_0x0017
            r0.b = r3
            goto L_0x001f
        L_0x0017:
            java.util.HashSet<java.lang.Class<? extends java.lang.Exception>> r1 = com.mintegral.msdk.base.common.net.k.b
            a(r1, r5)
        L_0x001c:
            r1 = 0
            r0.b = r1
        L_0x001f:
            boolean r1 = r0.b
            if (r1 == 0) goto L_0x0036
            java.lang.String r1 = "CommonRequestExceptionManager"
            com.mintegral.msdk.base.utils.g.a(r1, r5)
            int r1 = r4.e
            long r1 = (long) r1
            android.os.SystemClock.sleep(r1)
            int r1 = r4.f
            int r1 = r1 + r3
            r4.f = r1
            r0.c = r1
            goto L_0x0051
        L_0x0036:
            java.lang.String r1 = "CommonRequestExceptionManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "httprequest exception stop retry "
            r2.<init>(r3)
            java.lang.Class r3 = r5.getClass()
            java.lang.String r3 = r3.getSimpleName()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.mintegral.msdk.base.utils.g.b(r1, r2, r5)
        L_0x0051:
            java.util.HashMap<java.lang.Class<? extends java.lang.Exception>, java.lang.Integer> r1 = com.mintegral.msdk.base.common.net.k.c
            java.lang.Class r5 = r5.getClass()
            java.lang.Object r5 = r1.get(r5)
            java.lang.Integer r5 = (java.lang.Integer) r5
            if (r5 != 0) goto L_0x0064
            r5 = 5
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
        L_0x0064:
            int r5 = r5.intValue()
            r0.a = r5
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.common.net.k.a(java.lang.Exception):com.mintegral.msdk.base.common.net.k$a");
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.f = 0;
    }

    private static boolean a(HashSet<Class<? extends Exception>> hashSet, Throwable th) {
        Iterator<Class<? extends Exception>> it = hashSet.iterator();
        while (it.hasNext()) {
            if (it.next().isInstance(th)) {
                return true;
            }
        }
        return false;
    }

    /* compiled from: CommonRequestExceptionManager */
    static class a {
        int a;
        boolean b;
        int c;

        a() {
        }
    }
}
