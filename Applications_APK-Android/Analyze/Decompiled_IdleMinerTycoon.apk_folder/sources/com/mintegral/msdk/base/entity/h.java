package com.mintegral.msdk.base.entity;

import java.util.Set;

/* compiled from: InstallApp */
public final class h {
    private String a;
    private String b;

    public h() {
    }

    public h(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public final String a() {
        return this.a;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.b = str;
    }

    public static String a(Set<h> set) {
        if (set == null) {
            return null;
        }
        try {
            if (set.size() <= 0) {
                return null;
            }
            StringBuffer stringBuffer = new StringBuffer();
            for (h next : set) {
                stringBuffer.append("{\"campaignId\":");
                stringBuffer.append(next.a + ",");
                stringBuffer.append("\"packageName\":");
                stringBuffer.append(next.b + "},");
            }
            return "[{$native_info}]".replace("{$native_info}", stringBuffer.subSequence(0, stringBuffer.lastIndexOf(",")));
        } catch (Exception unused) {
            return null;
        }
    }

    public final int hashCode() {
        int i;
        int i2 = 0;
        if (this.a == null) {
            i = 0;
        } else {
            i = this.a.hashCode();
        }
        int i3 = (i + 31) * 31;
        if (this.b != null) {
            i2 = this.b.hashCode();
        }
        return i3 + i2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        h hVar = (h) obj;
        if (this.a == null) {
            if (hVar.a != null) {
                return false;
            }
        } else if (!this.a.equals(hVar.a)) {
            return false;
        }
        if (this.b == null) {
            if (hVar.b != null) {
                return false;
            }
        } else if (!this.b.equals(hVar.b)) {
            return false;
        }
        return true;
    }
}
