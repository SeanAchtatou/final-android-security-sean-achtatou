package com.mintegral.msdk.base.b;

import android.database.Cursor;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.click.CommonJumpLoader;
import com.mintegral.msdk.d.a;

/* compiled from: CampaignClickDao */
public class d extends a<CommonJumpLoader.JumpLoaderResult> {
    public static final String b = "d";
    private static d c;
    private a d;
    private int e = 100;

    private d(h hVar) {
        super(hVar);
    }

    public static d a(h hVar) {
        if (c == null) {
            synchronized (d.class) {
                if (c == null) {
                    c = new d(hVar);
                }
            }
        }
        return c;
    }

    public final synchronized void c() {
        try {
            b().delete("campaignclick", "(pts not " + ((Object) null) + " AND (  ( ttc_type = 2 AND (  ( cps = 1 AND (" + System.currentTimeMillis() + " - pts) > cpti )  OR  (cps = 0 AND (" + System.currentTimeMillis() + " - pts) > cpei )  )  ) OR ( ttc_type = 1 AND  ( " + System.currentTimeMillis() + " - pts )  > ttc_ct2 ) ) ) OR ( pts is " + ((Object) null) + " AND  ( " + System.currentTimeMillis() + " - ts) > cti)", null);
        } catch (Exception e2) {
            g.d(b, e2.getMessage());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00e4, code lost:
        return -1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized long a(com.mintegral.msdk.base.entity.CampaignEx r8, java.lang.String r9) {
        /*
            r7 = this;
            monitor-enter(r7)
            if (r8 != 0) goto L_0x0007
            r8 = 0
            monitor-exit(r7)
            return r8
        L_0x0007:
            r0 = -1
            com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult r2 = r8.getJumpResult()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            java.lang.String r2 = com.mintegral.msdk.base.utils.q.a(r2)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            r3.<init>()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            java.lang.String r4 = "id"
            java.lang.String r5 = r8.getId()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            java.lang.String r4 = "unitid"
            r3.put(r4, r9)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            java.lang.String r4 = "result"
            r3.put(r4, r2)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            java.lang.String r2 = "cpti"
            int r4 = r8.getPreClickInterval()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            int r4 = r4 * 1000
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            r3.put(r2, r4)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            java.lang.String r2 = "cti"
            int r4 = r8.getClickInterval()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            int r4 = r4 * 1000
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            r3.put(r2, r4)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            java.lang.String r2 = "package_name"
            java.lang.String r4 = r8.getPackageName()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            r3.put(r2, r4)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            com.mintegral.msdk.d.b.a()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            java.lang.String r2 = r2.j()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            com.mintegral.msdk.d.a r2 = com.mintegral.msdk.d.b.b(r2)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            java.lang.String r4 = "ts"
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            if (r2 == 0) goto L_0x0083
            int r4 = r2.aT()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            if (r4 <= 0) goto L_0x0083
            java.lang.String r4 = "cpei"
            int r5 = r2.aT()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            int r5 = r5 * 1000
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
        L_0x0083:
            if (r2 == 0) goto L_0x009a
            int r4 = r2.aU()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            if (r4 <= 0) goto L_0x009a
            java.lang.String r4 = "cpoci"
            int r2 = r2.aU()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            int r2 = r2 * 1000
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            r3.put(r4, r2)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
        L_0x009a:
            java.lang.String r2 = r8.getId()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            boolean r2 = r7.c(r2, r9)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            r4 = 0
            if (r2 == 0) goto L_0x00d4
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            java.lang.String r5 = "id = "
            r2.<init>(r5)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            java.lang.String r8 = r8.getId()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            r2.append(r8)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            java.lang.String r8 = " AND unitid = "
            r2.append(r8)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            r2.append(r9)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            java.lang.String r8 = r2.toString()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            android.database.sqlite.SQLiteDatabase r9 = r7.b()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            if (r9 != 0) goto L_0x00c7
            monitor-exit(r7)
            return r0
        L_0x00c7:
            android.database.sqlite.SQLiteDatabase r9 = r7.b()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            java.lang.String r2 = "campaignclick"
            int r8 = r9.update(r2, r3, r8, r4)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            long r8 = (long) r8
            monitor-exit(r7)
            return r8
        L_0x00d4:
            android.database.sqlite.SQLiteDatabase r8 = r7.b()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            java.lang.String r9 = "campaignclick"
            long r8 = r8.insert(r9, r4, r3)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            monitor-exit(r7)
            return r8
        L_0x00e0:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        L_0x00e3:
            monitor-exit(r7)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.d.a(com.mintegral.msdk.base.entity.CampaignEx, java.lang.String):long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:40:0x0102 */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x010b A[SYNTHETIC, Splitter:B:43:0x010b] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0112 A[SYNTHETIC, Splitter:B:48:0x0112] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.lang.String a(java.lang.String r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            java.lang.String r0 = ""
            com.mintegral.msdk.d.a r1 = r5.d     // Catch:{ all -> 0x0116 }
            if (r1 != 0) goto L_0x0018
            com.mintegral.msdk.d.b.a()     // Catch:{ all -> 0x0116 }
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ all -> 0x0116 }
            java.lang.String r1 = r1.j()     // Catch:{ all -> 0x0116 }
            com.mintegral.msdk.d.a r1 = com.mintegral.msdk.d.b.b(r1)     // Catch:{ all -> 0x0116 }
            r5.d = r1     // Catch:{ all -> 0x0116 }
        L_0x0018:
            com.mintegral.msdk.d.a r1 = r5.d     // Catch:{ all -> 0x0116 }
            if (r1 == 0) goto L_0x002c
            com.mintegral.msdk.d.a r1 = r5.d     // Catch:{ all -> 0x0116 }
            int r1 = r1.ae()     // Catch:{ all -> 0x0116 }
            if (r1 <= 0) goto L_0x002c
            com.mintegral.msdk.d.a r1 = r5.d     // Catch:{ all -> 0x0116 }
            int r1 = r1.ae()     // Catch:{ all -> 0x0116 }
            r5.e = r1     // Catch:{ all -> 0x0116 }
        L_0x002c:
            java.util.HashSet r1 = new java.util.HashSet     // Catch:{ all -> 0x0116 }
            r1.<init>()     // Catch:{ all -> 0x0116 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0116 }
            java.lang.String r3 = "SELECT id FROM campaignclick WHERE (ttc_type = 2 AND ( (cps = 1 AND unitid = '"
            r2.<init>(r3)     // Catch:{ all -> 0x0116 }
            r2.append(r6)     // Catch:{ all -> 0x0116 }
            java.lang.String r3 = "' AND (cpti + pts) > "
            r2.append(r3)     // Catch:{ all -> 0x0116 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0116 }
            r2.append(r3)     // Catch:{ all -> 0x0116 }
            java.lang.String r3 = ") OR  (cps = 0 AND (pts + cpei) > "
            r2.append(r3)     // Catch:{ all -> 0x0116 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0116 }
            r2.append(r3)     // Catch:{ all -> 0x0116 }
            java.lang.String r3 = ") OR (unitid <> '"
            r2.append(r3)     // Catch:{ all -> 0x0116 }
            r2.append(r6)     // Catch:{ all -> 0x0116 }
            java.lang.String r3 = "' AND (pts + cpoci) > "
            r2.append(r3)     // Catch:{ all -> 0x0116 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0116 }
            r2.append(r3)     // Catch:{ all -> 0x0116 }
            java.lang.String r3 = " AND cps = 1 ))  ) OR (ttc_type = 1 AND ( (unitid = '"
            r2.append(r3)     // Catch:{ all -> 0x0116 }
            r2.append(r6)     // Catch:{ all -> 0x0116 }
            java.lang.String r3 = "' AND ("
            r2.append(r3)     // Catch:{ all -> 0x0116 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0116 }
            r2.append(r3)     // Catch:{ all -> 0x0116 }
            java.lang.String r3 = " - pts ) <= ttc_ct2 ) OR (unitid <> '"
            r2.append(r3)     // Catch:{ all -> 0x0116 }
            r2.append(r6)     // Catch:{ all -> 0x0116 }
            java.lang.String r6 = "' AND ("
            r2.append(r6)     // Catch:{ all -> 0x0116 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0116 }
            r2.append(r3)     // Catch:{ all -> 0x0116 }
            java.lang.String r6 = " - pts) <= n4 ) ) ) ORDER BY pts DESC  LIMIT "
            r2.append(r6)     // Catch:{ all -> 0x0116 }
            int r6 = r5.e     // Catch:{ all -> 0x0116 }
            r2.append(r6)     // Catch:{ all -> 0x0116 }
            java.lang.String r6 = r2.toString()     // Catch:{ all -> 0x0116 }
            r2 = 0
            android.database.sqlite.SQLiteDatabase r3 = r5.a()     // Catch:{ Exception -> 0x0102 }
            android.database.Cursor r6 = r3.rawQuery(r6, r2)     // Catch:{ Exception -> 0x0102 }
            if (r6 == 0) goto L_0x00d5
            int r2 = r6.getCount()     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
            if (r2 <= 0) goto L_0x00d5
            r2 = 0
            r6.moveToFirst()     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
        L_0x00b2:
            boolean r3 = r6.isAfterLast()     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
            if (r3 != 0) goto L_0x00d5
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 >= r3) goto L_0x00d5
            java.lang.String r3 = "id"
            int r3 = r6.getColumnIndex(r3)     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
            int r3 = r6.getInt(r3)     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
            r1.add(r3)     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
            r6.moveToNext()     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
            int r2 = r2 + 1
            goto L_0x00b2
        L_0x00d3:
            r0 = move-exception
            goto L_0x0110
        L_0x00d5:
            java.util.Iterator r1 = r1.iterator()     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
            org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
            r2.<init>()     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
        L_0x00de:
            boolean r3 = r1.hasNext()     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
            if (r3 == 0) goto L_0x00f2
            java.lang.Object r3 = r1.next()     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
            r2.put(r3)     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
            goto L_0x00de
        L_0x00f2:
            java.lang.String r1 = r2.toString()     // Catch:{ Exception -> 0x00fd, all -> 0x00d3 }
            if (r6 == 0) goto L_0x00fb
            r6.close()     // Catch:{ all -> 0x0116 }
        L_0x00fb:
            r0 = r1
            goto L_0x010e
        L_0x00fd:
            r2 = r6
            goto L_0x0102
        L_0x00ff:
            r0 = move-exception
            r6 = r2
            goto L_0x0110
        L_0x0102:
            java.lang.String r6 = com.mintegral.msdk.base.b.d.b     // Catch:{ all -> 0x00ff }
            java.lang.String r1 = "AvoidRepetition report fail"
            com.mintegral.msdk.base.utils.g.d(r6, r1)     // Catch:{ all -> 0x00ff }
            if (r2 == 0) goto L_0x010e
            r2.close()     // Catch:{ all -> 0x0116 }
        L_0x010e:
            monitor-exit(r5)
            return r0
        L_0x0110:
            if (r6 == 0) goto L_0x0115
            r6.close()     // Catch:{ all -> 0x0116 }
        L_0x0115:
            throw r0     // Catch:{ all -> 0x0116 }
        L_0x0116:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.d.a(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0041, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean a(java.lang.String r4, java.lang.String r5) {
        /*
            r3 = this;
            monitor-enter(r3)
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0044 }
            java.lang.String r2 = "SELECT id FROM campaignclick WHERE id='"
            r1.<init>(r2)     // Catch:{ Exception -> 0x0044 }
            r1.append(r4)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r4 = "' AND unitid='"
            r1.append(r4)     // Catch:{ Exception -> 0x0044 }
            r1.append(r5)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r4 = "' AND cti + ts > "
            r1.append(r4)     // Catch:{ Exception -> 0x0044 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0044 }
            r1.append(r4)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r4 = r1.toString()     // Catch:{ Exception -> 0x0044 }
            android.database.sqlite.SQLiteDatabase r5 = r3.a()     // Catch:{ Exception -> 0x0044 }
            r1 = 0
            android.database.Cursor r4 = r5.rawQuery(r4, r1)     // Catch:{ Exception -> 0x0044 }
            if (r4 == 0) goto L_0x003b
            int r5 = r4.getCount()     // Catch:{ Exception -> 0x0044 }
            if (r5 <= 0) goto L_0x003b
            r4.close()     // Catch:{ Exception -> 0x0044 }
            r4 = 1
            monitor-exit(r3)
            return r4
        L_0x003b:
            if (r4 == 0) goto L_0x0040
            r4.close()     // Catch:{ Exception -> 0x0044 }
        L_0x0040:
            monitor-exit(r3)
            return r0
        L_0x0042:
            r4 = move-exception
            goto L_0x0050
        L_0x0044:
            r4 = move-exception
            java.lang.String r5 = com.mintegral.msdk.base.b.d.b     // Catch:{ all -> 0x0042 }
            java.lang.String r4 = r4.getMessage()     // Catch:{ all -> 0x0042 }
            com.mintegral.msdk.base.utils.g.d(r5, r4)     // Catch:{ all -> 0x0042 }
            monitor-exit(r3)
            return r0
        L_0x0050:
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.d.a(java.lang.String, java.lang.String):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0046, code lost:
        return r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0049, code lost:
        if (r11 != null) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0057, code lost:
        if (r11 != null) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005b, code lost:
        return null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x005f A[SYNTHETIC, Splitter:B:33:0x005f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.mintegral.msdk.click.CommonJumpLoader.JumpLoaderResult b(java.lang.String r11, java.lang.String r12) {
        /*
            r10 = this;
            monitor-enter(r10)
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r10.a()     // Catch:{ Exception -> 0x0052, all -> 0x004f }
            java.lang.String r2 = "campaignclick"
            r3 = 0
            java.lang.String r4 = "id=? AND unitid=?"
            r5 = 2
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Exception -> 0x0052, all -> 0x004f }
            r6 = 0
            r5[r6] = r11     // Catch:{ Exception -> 0x0052, all -> 0x004f }
            r11 = 1
            r5[r11] = r12     // Catch:{ Exception -> 0x0052, all -> 0x004f }
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r11 = r1.query(r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x0052, all -> 0x004f }
            if (r11 == 0) goto L_0x0049
            int r12 = r11.getCount()     // Catch:{ Exception -> 0x0047 }
            if (r12 <= 0) goto L_0x0049
            boolean r12 = r11.moveToFirst()     // Catch:{ Exception -> 0x0047 }
            if (r12 == 0) goto L_0x0049
            java.lang.String r12 = "result"
            int r12 = r11.getColumnIndex(r12)     // Catch:{ Exception -> 0x0047 }
            java.lang.String r12 = r11.getString(r12)     // Catch:{ Exception -> 0x0047 }
            boolean r1 = android.text.TextUtils.isEmpty(r12)     // Catch:{ Exception -> 0x0047 }
            if (r1 != 0) goto L_0x0049
            java.lang.Object r12 = com.mintegral.msdk.base.utils.q.a(r12)     // Catch:{ Exception -> 0x0047 }
            com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult r12 = (com.mintegral.msdk.click.CommonJumpLoader.JumpLoaderResult) r12     // Catch:{ Exception -> 0x0047 }
            if (r11 == 0) goto L_0x0045
            r11.close()     // Catch:{ all -> 0x0063 }
        L_0x0045:
            monitor-exit(r10)
            return r12
        L_0x0047:
            r12 = move-exception
            goto L_0x0054
        L_0x0049:
            if (r11 == 0) goto L_0x005a
        L_0x004b:
            r11.close()     // Catch:{ all -> 0x0063 }
            goto L_0x005a
        L_0x004f:
            r12 = move-exception
            r11 = r0
            goto L_0x005d
        L_0x0052:
            r12 = move-exception
            r11 = r0
        L_0x0054:
            r12.printStackTrace()     // Catch:{ all -> 0x005c }
            if (r11 == 0) goto L_0x005a
            goto L_0x004b
        L_0x005a:
            monitor-exit(r10)
            return r0
        L_0x005c:
            r12 = move-exception
        L_0x005d:
            if (r11 == 0) goto L_0x0065
            r11.close()     // Catch:{ all -> 0x0063 }
            goto L_0x0065
        L_0x0063:
            r11 = move-exception
            goto L_0x0066
        L_0x0065:
            throw r12     // Catch:{ all -> 0x0063 }
        L_0x0066:
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.d.b(java.lang.String, java.lang.String):com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult");
    }

    private synchronized boolean c(String str, String str2) {
        Cursor rawQuery = a().rawQuery("SELECT id FROM campaignclick WHERE id='" + str + "' AND unitid= '" + str2 + "'", null);
        if (rawQuery == null || rawQuery.getCount() <= 0) {
            if (rawQuery != null) {
                rawQuery.close();
            }
            return false;
        }
        rawQuery.close();
        return true;
    }
}
