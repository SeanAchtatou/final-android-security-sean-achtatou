package com.mintegral.msdk.base.entity;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.controller.authoritycontroller.a;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import java.net.URLEncoder;
import java.util.List;

/* compiled from: VideoReportData */
public final class q {
    public static int a = 1;
    public static int b;
    private String A;
    private String B;
    private String C;
    private String D;
    private String E;
    private String F;
    private String G;
    private int H = 0;
    private String c;
    private String d;
    private String e;
    private String f;
    private int g;
    private String h;
    private int i;
    private int j;
    private int k;
    private String l;
    private String m;
    private int n;
    private int o;
    private String p;
    private int q;
    private int r;
    private int s = 0;
    private String t;
    private String u;
    private String v;
    private String w;
    private String x;
    private String y;
    private String z;

    public final String a() {
        return this.c;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final String b() {
        return this.d;
    }

    public final void b(String str) {
        this.d = str;
    }

    public final String c() {
        return this.e;
    }

    public final void c(String str) {
        this.e = str;
    }

    public final String d() {
        return this.F;
    }

    public final void d(String str) {
        this.F = str;
    }

    public final String e() {
        return this.E;
    }

    public final void e(String str) {
        this.E = str;
    }

    public final String f() {
        return this.D;
    }

    public final void f(String str) {
        this.D = str;
    }

    public final String g() {
        return this.C;
    }

    public final void g(String str) {
        this.C = str;
    }

    public final String h() {
        return this.G;
    }

    public final void h(String str) {
        this.G = str;
    }

    public final String i() {
        return this.w;
    }

    public final void i(String str) {
        this.w = str;
    }

    public final void j(String str) {
        this.y = str;
    }

    public final void a(int i2) {
        this.H = i2;
    }

    public q() {
    }

    public q(String str, int i2, int i3, String str2, int i4, int i5, String str3) {
        this.f = str;
        this.g = i2;
        this.h = str3;
        this.k = i3;
        this.l = str2;
        this.r = i4;
        this.s = i5;
    }

    public q(String str, int i2, String str2, String str3, String str4) {
        this.f = str;
        this.h = str4;
        this.g = i2;
        this.l = str2;
        this.m = str3;
    }

    public q(String str, int i2, int i3, String str2, int i4, String str3, int i5, String str4) {
        this.f = str;
        this.g = i2;
        this.h = str4;
        this.k = i3;
        this.l = str2;
        this.o = i4;
        this.p = str3;
        this.q = i5;
    }

    public q(Context context, CampaignEx campaignEx, int i2, String str, int i3, int i4) {
        switch (i4) {
            case 1:
                this.f = "2000022";
                break;
            case 2:
                this.f = "2000025";
                break;
            case 3:
                this.f = "2000022";
                break;
        }
        this.g = c.s(context);
        this.h = c.a(context, this.g);
        this.k = campaignEx.getVideoLength();
        this.l = campaignEx.getNoticeUrl() == null ? campaignEx.getClickURL() : campaignEx.getNoticeUrl();
        this.o = i2;
        this.p = str;
        this.q = i3 == 0 ? campaignEx.getVideoSize() : i3;
    }

    public q(String str, String str2, String str3, String str4, String str5, String str6, int i2, String str7) {
        this.f = str;
        this.z = str2;
        this.x = str3;
        this.A = str4;
        this.u = str5;
        this.v = str6;
        this.g = i2;
        this.h = str7;
    }

    public q(String str) {
        this.B = str;
    }

    public q(String str, int i2, String str2, String str3, String str4, String str5, String str6, String str7) {
        this.f = str;
        this.o = i2;
        this.p = str2;
        this.D = str3;
        this.v = str4;
        this.u = str5;
        this.m = str6;
        this.C = str7;
        if (Integer.valueOf(str2).intValue() > 60000) {
            this.o = 2;
        }
    }

    public q(String str, String str2, String str3, String str4, int i2) {
        this.f = str;
        this.v = str2;
        this.t = str3;
        this.u = str4;
        this.g = i2;
    }

    public q(String str, String str2, String str3, String str4, int i2, int i3, String str5) {
        this.f = str;
        this.v = str2;
        this.t = str3;
        this.u = str4;
        this.g = i2;
        this.m = str5;
        this.n = i3;
    }

    public final String j() {
        return this.t;
    }

    public final void k(String str) {
        this.t = str;
    }

    public final String k() {
        return this.u;
    }

    public final void l(String str) {
        this.u = str;
    }

    public final String l() {
        return this.v;
    }

    public final void m(String str) {
        this.v = str;
    }

    public final String m() {
        return this.f;
    }

    public final void n(String str) {
        this.f = str;
    }

    public final int n() {
        return this.j;
    }

    public final int o() {
        return this.k;
    }

    public final String p() {
        return this.l;
    }

    public final String q() {
        return this.m;
    }

    public final void o(String str) {
        this.m = str;
    }

    public final String r() {
        return this.p;
    }

    public final void p(String str) {
        this.p = str;
    }

    public final int s() {
        return this.q;
    }

    public final int t() {
        return this.g;
    }

    public final void b(int i2) {
        this.g = i2;
    }

    public final String u() {
        return this.h;
    }

    public final void q(String str) {
        this.h = str;
    }

    public final int v() {
        return this.i;
    }

    public final int w() {
        return this.o;
    }

    public final void c(int i2) {
        this.o = i2;
    }

    public static String a(List<q> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (q next : list) {
            stringBuffer.append("key=" + next.f + Constants.RequestParameters.AMPERSAND);
            stringBuffer.append("resource_type=" + next.c + Constants.RequestParameters.AMPERSAND);
            stringBuffer.append("unit_id=" + next.u + Constants.RequestParameters.AMPERSAND);
            StringBuilder sb = new StringBuilder("ad_type=");
            sb.append(next.G);
            stringBuffer.append(sb.toString());
            stringBuffer.append("cid=" + next.v + Constants.RequestParameters.AMPERSAND);
            stringBuffer.append("rid_n=" + next.t + Constants.RequestParameters.AMPERSAND);
            stringBuffer.append("reason=" + next.m + Constants.RequestParameters.AMPERSAND);
            StringBuilder sb2 = new StringBuilder("creative=");
            sb2.append(next.e);
            stringBuffer.append(sb2.toString());
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
                stringBuffer.append("devid=" + next.d + Constants.RequestParameters.AMPERSAND);
            }
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                stringBuffer.append("network_type=" + next.g + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("\n");
            } else {
                stringBuffer.append("\n");
            }
        }
        return stringBuffer.toString();
    }

    public static String b(List<q> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (q next : list) {
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                stringBuffer.append("key=" + next.f + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("network_type=" + next.g + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("network_str=" + next.h + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("reason=" + next.m + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("cid=" + next.v + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("video_url=" + next.E + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("rid_n=" + next.t + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("unit_id=" + next.u + Constants.RequestParameters.AMPERSAND);
                StringBuilder sb = new StringBuilder("offer_url=");
                sb.append(next.l);
                stringBuffer.append(sb.toString());
                stringBuffer.append("\n");
            } else {
                stringBuffer.append("key=" + next.f + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("reason=" + next.m + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("cid=" + next.v + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("video_url=" + next.E + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("rid_n=" + next.t + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("unit_id=" + next.u + Constants.RequestParameters.AMPERSAND);
                StringBuilder sb2 = new StringBuilder("offer_url=");
                sb2.append(next.l);
                stringBuffer.append(sb2.toString());
                stringBuffer.append("\n");
            }
        }
        return stringBuffer.toString();
    }

    public static String c(List<q> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (q next : list) {
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                stringBuffer.append("key=" + next.f + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("network_type=" + next.g + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("network_str=" + next.h + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("result=" + next.o + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("duration=" + next.p + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("video_size=" + next.q + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("video_length=" + next.k + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("reason=" + next.m + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("cid=" + next.v + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("video_url=" + next.E + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("rid_n=" + next.t + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("unit_id=" + next.u + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("offer_url=" + next.l + Constants.RequestParameters.AMPERSAND);
            } else {
                stringBuffer.append("key=" + next.f + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("result=" + next.o + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("duration=" + next.p + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("video_size=" + next.q + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("video_length=" + next.k + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("reason=" + next.m + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("cid=" + next.v + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("video_url=" + next.E + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("rid_n=" + next.t + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("unit_id=" + next.u + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("offer_url=" + next.l + Constants.RequestParameters.AMPERSAND);
            }
            if (next.c != null) {
                stringBuffer.append("resource_type=" + next.c + Constants.RequestParameters.AMPERSAND);
            }
            if (next.e != null) {
                stringBuffer.append("creative=" + next.e + Constants.RequestParameters.AMPERSAND);
            }
            stringBuffer.append("\n");
        }
        return stringBuffer.toString();
    }

    public static String a(q qVar) {
        if (qVar == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("key=" + qVar.f + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("cid=" + qVar.v + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("unit_id=" + qVar.u + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("network_type=" + qVar.g + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("rid_n=" + qVar.t + Constants.RequestParameters.AMPERSAND);
        StringBuilder sb = new StringBuilder("reason=");
        sb.append(qVar.m);
        stringBuffer.append(sb.toString());
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }

    public static String b(q qVar) {
        if (qVar == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("key=" + qVar.f + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("cid=" + qVar.v + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("unit_id=" + qVar.u + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("network_type=" + qVar.g + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("mraid_type=" + qVar.H + Constants.RequestParameters.AMPERSAND);
        StringBuilder sb = new StringBuilder("rid_n=");
        sb.append(qVar.t);
        stringBuffer.append(sb.toString());
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }

    public static String c(q qVar) {
        if (qVar == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("key=" + qVar.f + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("result=" + qVar.o + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("duration=" + qVar.p + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("endcard_url=" + qVar.D + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("cid=" + qVar.v + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("reason=" + qVar.m + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("type=" + qVar.C + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("ad_type=" + qVar.G + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("unit_id=" + qVar.u + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("devid=" + qVar.d + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("mraid_type=" + qVar.H + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("network_type=" + qVar.g + Constants.RequestParameters.AMPERSAND);
        if (qVar.c != null) {
            stringBuffer.append("resource_type=" + qVar.c + Constants.RequestParameters.AMPERSAND);
        }
        stringBuffer.append("rid_n=" + qVar.t);
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }

    public static String d(q qVar) {
        if (qVar == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("key=" + qVar.f + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("result=" + qVar.o + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("duration=" + qVar.p + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("cid=" + qVar.v + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("unit_id=" + qVar.u + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("reason=" + qVar.m + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("ad_type=" + qVar.G + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("rid_n=" + qVar.t + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("network_type=" + qVar.g + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("mraid_type=" + qVar.H + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("devid=" + qVar.d + Constants.RequestParameters.AMPERSAND);
        if (qVar.c != null) {
            stringBuffer.append("resource_type=" + qVar.c + Constants.RequestParameters.AMPERSAND);
        }
        if (!TextUtils.isEmpty(qVar.D)) {
            stringBuffer.append("endcard_url=" + qVar.D + Constants.RequestParameters.AMPERSAND);
        }
        stringBuffer.append("type=" + qVar.C);
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }

    public static String d(List<q> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (q next : list) {
            stringBuffer.append("key=" + next.f + Constants.RequestParameters.AMPERSAND);
            stringBuffer.append("result=" + next.o + Constants.RequestParameters.AMPERSAND);
            stringBuffer.append("duration=" + next.p + Constants.RequestParameters.AMPERSAND);
            stringBuffer.append("endcard_url=" + next.D + Constants.RequestParameters.AMPERSAND);
            stringBuffer.append("cid=" + next.v + Constants.RequestParameters.AMPERSAND);
            stringBuffer.append("unit_id=" + next.u + Constants.RequestParameters.AMPERSAND);
            stringBuffer.append("reason=" + next.m + Constants.RequestParameters.AMPERSAND);
            stringBuffer.append("ad_type=" + next.G + Constants.RequestParameters.AMPERSAND);
            stringBuffer.append("rid_n=" + next.t + Constants.RequestParameters.AMPERSAND);
            StringBuilder sb = new StringBuilder("type=");
            sb.append(next.C);
            stringBuffer.append(sb.toString());
            stringBuffer.append("\n");
        }
        return stringBuffer.toString();
    }

    public static String e(List<q> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (q next : list) {
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                stringBuffer.append("key=" + next.f + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("network_type=" + next.g + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("result=" + next.o + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("cid=" + next.v + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("template_url=" + next.w + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("reason=" + next.m + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("rid_n=" + next.t + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("unit_id=" + next.u + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("\n");
            } else {
                stringBuffer.append("key=" + next.f + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("result=" + next.o + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("cid=" + next.v + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("template_url=" + next.w + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("reason=" + next.m + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("rid_n=" + next.t + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("unit_id=" + next.u + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("\n");
            }
        }
        return stringBuffer.toString();
    }

    public static String f(List<q> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (q next : list) {
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                stringBuffer.append("key=" + next.f + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("network_type=" + next.g + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("cid=" + next.v + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("image_url=" + next.F + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("reason=" + next.m + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("rid_n=" + next.t + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("unit_id=" + next.u + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("\n");
            } else {
                stringBuffer.append("key=" + next.f + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("cid=" + next.v + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("image_url=" + next.F + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("reason=" + next.m + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("rid_n=" + next.t + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("unit_id=" + next.u + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("\n");
            }
        }
        return stringBuffer.toString();
    }

    public static String e(q qVar) {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                stringBuffer.append("key=" + qVar.f + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("network_type=" + qVar.g + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("network_str=" + qVar.h + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("video_length=" + qVar.k + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("ctype=" + qVar.s + Constants.RequestParameters.AMPERSAND);
            } else {
                stringBuffer.append("key=" + qVar.f + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("video_length=" + qVar.k + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("ctype=" + qVar.s + Constants.RequestParameters.AMPERSAND);
            }
            if (!TextUtils.isEmpty(qVar.l)) {
                stringBuffer.append("offer_url=" + URLEncoder.encode(qVar.l, "utf-8") + Constants.RequestParameters.AMPERSAND);
            }
            stringBuffer.append("time=" + qVar.r);
            return stringBuffer.toString();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String f(q qVar) {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                stringBuffer.append("key=" + qVar.f + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("error=" + k.d(qVar.y) + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("template_url=" + k.d(qVar.w) + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("unit_id=" + k.d(qVar.u) + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("cid=" + k.d(qVar.v) + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("network_str=" + qVar.h + Constants.RequestParameters.AMPERSAND);
                StringBuilder sb = new StringBuilder("network_type=");
                sb.append(qVar.g);
                stringBuffer.append(sb.toString());
            } else {
                stringBuffer.append("key=" + qVar.f + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("error=" + k.d(qVar.y) + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("template_url=" + k.d(qVar.w) + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("unit_id=" + k.d(qVar.u) + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("cid=" + k.d(qVar.v) + Constants.RequestParameters.AMPERSAND);
            }
            return stringBuffer.toString();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String g(q qVar) {
        if (qVar == null) {
            return null;
        }
        try {
            StringBuffer stringBuffer = new StringBuffer();
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                stringBuffer.append("key=" + qVar.f + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("event=" + k.d(qVar.z) + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("template=" + k.d(qVar.x) + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("layout=" + k.d(qVar.A) + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("unit_id=" + k.d(qVar.u) + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("cid=" + k.d(qVar.v) + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("network_str=" + qVar.h + Constants.RequestParameters.AMPERSAND);
                StringBuilder sb = new StringBuilder("network_type=");
                sb.append(qVar.g);
                stringBuffer.append(sb.toString());
            } else {
                stringBuffer.append("key=" + qVar.f + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("event=" + k.d(qVar.z) + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("template=" + k.d(qVar.x) + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("layout=" + k.d(qVar.A) + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("unit_id=" + k.d(qVar.u) + Constants.RequestParameters.AMPERSAND);
                stringBuffer.append("cid=" + k.d(qVar.v) + Constants.RequestParameters.AMPERSAND);
            }
            return stringBuffer.toString();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String g(List<q> list) {
        if (list == null) {
            return null;
        }
        try {
            if (list.size() <= 0) {
                return null;
            }
            StringBuffer stringBuffer = new StringBuffer();
            for (q qVar : list) {
                stringBuffer.append(qVar.B);
                stringBuffer.append("\n");
            }
            return stringBuffer.toString();
        } catch (Throwable th) {
            g.c("VideoReportData", th.getMessage(), th);
            return null;
        }
    }

    public static String h(q qVar) {
        if (qVar == null) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("key=" + qVar.f + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("cid=" + qVar.v + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("rid_n=" + qVar.t + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("unit_id=" + qVar.u + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("network_type=" + qVar.g + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("mraid_type=" + qVar.H + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("platform=1");
        return stringBuffer.toString();
    }

    public static String i(q qVar) {
        if (qVar == null) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("key=" + qVar.f + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("cid=" + qVar.v + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("rid_n=" + qVar.t + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("unit_id=" + qVar.u + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("reason=" + qVar.m + Constants.RequestParameters.AMPERSAND);
        stringBuffer.append("case=" + qVar.n + Constants.RequestParameters.AMPERSAND);
        StringBuilder sb = new StringBuilder("network_type=");
        sb.append(qVar.g);
        stringBuffer.append(sb.toString());
        return stringBuffer.toString();
    }

    public final String toString() {
        return "RewardReportData [key=" + this.f + ", networkType=" + this.g + ", isCompleteView=" + this.i + ", watchedMillis=" + this.j + ", videoLength=" + this.k + ", offerUrl=" + this.l + ", reason=" + this.m + ", result=" + this.o + ", duration=" + this.p + ", videoSize=" + this.q + Constants.RequestParameters.RIGHT_BRACKETS;
    }
}
