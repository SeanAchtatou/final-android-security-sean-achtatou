package com.mintegral.msdk.base.utils;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.a.b;
import com.mintegral.msdk.base.controller.authoritycontroller.a;
import com.mintegral.msdk.optimize.SensitiveDataUtil;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

/* compiled from: CommonDeviceUtil */
public final class c {
    private static String a = null;
    /* access modifiers changed from: private */
    public static String b = "";
    private static String c = null;
    private static int d = 0;
    /* access modifiers changed from: private */
    public static int e = 0;
    private static String f = null;
    private static String g = null;
    /* access modifiers changed from: private */
    public static String h = "";
    private static String i = "";
    private static String j = "";
    private static String k = "";
    private static int l = 0;
    private static String m = "";
    private static String n = "";
    private static String o = "";
    /* access modifiers changed from: private */
    public static String p = "";
    private static String q = "";
    private static String r = "";
    private static String s = "";
    private static String t = "";
    private static int u = -1;
    private static String v = "";
    private static int w;

    /* JADX WARNING: Removed duplicated region for block: B:19:0x00b3 A[Catch:{ Exception -> 0x00ee }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r4) {
        /*
            i()     // Catch:{ Exception -> 0x00ee }
            q(r4)     // Catch:{ Exception -> 0x00ee }
            l(r4)     // Catch:{ Exception -> 0x00ee }
            k(r4)     // Catch:{ Exception -> 0x00ee }
            i(r4)     // Catch:{ Exception -> 0x00ee }
            c()     // Catch:{ Exception -> 0x00ee }
            e()     // Catch:{ Exception -> 0x00ee }
            f(r4)     // Catch:{ Exception -> 0x00ee }
            c(r4)     // Catch:{ Exception -> 0x00ee }
            d(r4)     // Catch:{ Exception -> 0x00ee }
            j(r4)     // Catch:{ Exception -> 0x00ee }
            k()     // Catch:{ Exception -> 0x00ee }
            h(r4)     // Catch:{ Exception -> 0x00ee }
            h()     // Catch:{ Exception -> 0x00ee }
            t(r4)     // Catch:{ Exception -> 0x00ee }
            r0 = 0
            int r1 = j()     // Catch:{ Throwable -> 0x006c }
            r2 = 21
            r3 = 17
            if (r1 < r3) goto L_0x004f
            if (r1 >= r2) goto L_0x004f
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x006c }
            android.content.Context r1 = r1.h()     // Catch:{ Throwable -> 0x006c }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ Throwable -> 0x006c }
            java.lang.String r2 = "install_non_market_apps"
            int r1 = android.provider.Settings.Global.getInt(r1, r2, r0)     // Catch:{ Throwable -> 0x006c }
            com.mintegral.msdk.base.utils.c.u = r1     // Catch:{ Throwable -> 0x006c }
            goto L_0x0076
        L_0x004f:
            if (r1 >= r2) goto L_0x0057
            if (r1 >= r3) goto L_0x0076
            r2 = 10
            if (r1 <= r2) goto L_0x0076
        L_0x0057:
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x006c }
            android.content.Context r1 = r1.h()     // Catch:{ Throwable -> 0x006c }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ Throwable -> 0x006c }
            java.lang.String r2 = "install_non_market_apps"
            int r1 = android.provider.Settings.Secure.getInt(r1, r2, r0)     // Catch:{ Throwable -> 0x006c }
            com.mintegral.msdk.base.utils.c.u = r1     // Catch:{ Throwable -> 0x006c }
            goto L_0x0076
        L_0x006c:
            r1 = move-exception
            java.lang.String r2 = "CommonDeviceUtil"
            java.lang.String r3 = r1.getMessage()     // Catch:{ Exception -> 0x00ee }
            com.mintegral.msdk.base.utils.g.c(r2, r3, r1)     // Catch:{ Exception -> 0x00ee }
        L_0x0076:
            java.lang.String r1 = "CommonDeviceUtil"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ee }
            java.lang.String r3 = "getUnknowSourceStateForPrivate:"
            r2.<init>(r3)     // Catch:{ Exception -> 0x00ee }
            int r3 = com.mintegral.msdk.base.utils.c.u     // Catch:{ Exception -> 0x00ee }
            r2.append(r3)     // Catch:{ Exception -> 0x00ee }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00ee }
            com.mintegral.msdk.base.utils.g.d(r1, r2)     // Catch:{ Exception -> 0x00ee }
            java.lang.String r1 = "android.permission.WRITE_EXTERNAL_STORAGE"
            boolean r1 = com.mintegral.msdk.base.utils.k.a(r1, r4)     // Catch:{ Exception -> 0x00ee }
            com.mintegral.msdk.base.common.a.s = r1     // Catch:{ Exception -> 0x00ee }
            java.lang.String r1 = "android.permission.ACCESS_NETWORK_STATE"
            boolean r1 = com.mintegral.msdk.base.utils.k.a(r1, r4)     // Catch:{ Exception -> 0x00ee }
            com.mintegral.msdk.base.common.a.r = r1     // Catch:{ Exception -> 0x00ee }
            java.lang.String r1 = "android.permission.GET_TASKS"
            boolean r1 = com.mintegral.msdk.base.utils.k.a(r1, r4)     // Catch:{ Exception -> 0x00ee }
            com.mintegral.msdk.base.common.a.u = r1     // Catch:{ Exception -> 0x00ee }
            java.lang.String r1 = "android.permission.ACCESS_COARSE_LOCATION"
            boolean r1 = com.mintegral.msdk.base.utils.k.a(r1, r4)     // Catch:{ Exception -> 0x00ee }
            com.mintegral.msdk.base.common.a.t = r1     // Catch:{ Exception -> 0x00ee }
            java.lang.String r1 = "android.permission.READ_PHONE_STATE"
            boolean r1 = com.mintegral.msdk.base.utils.k.a(r1, r4)     // Catch:{ Exception -> 0x00ee }
            if (r1 == 0) goto L_0x00e7
            com.mintegral.msdk.base.controller.authoritycontroller.a.a()     // Catch:{ Exception -> 0x00ee }
            java.lang.String r1 = "authority_general_data"
            boolean r1 = com.mintegral.msdk.base.controller.authoritycontroller.a.a(r1)     // Catch:{ Exception -> 0x00ee }
            if (r1 == 0) goto L_0x00e7
            java.lang.String r1 = "phone"
            java.lang.Object r1 = r4.getSystemService(r1)     // Catch:{ Exception -> 0x00ee }
            android.telephony.TelephonyManager r1 = (android.telephony.TelephonyManager) r1     // Catch:{ Exception -> 0x00ee }
            java.lang.String r1 = r1.getSimOperator()     // Catch:{ Exception -> 0x00ee }
            boolean r2 = com.mintegral.msdk.base.utils.k.b(r1)     // Catch:{ Exception -> 0x00ee }
            if (r2 == 0) goto L_0x00e7
            int r2 = r1.length()     // Catch:{ Exception -> 0x00ee }
            r3 = 3
            if (r2 <= r3) goto L_0x00e7
            java.lang.String r0 = r1.substring(r0, r3)     // Catch:{ Exception -> 0x00ee }
            com.mintegral.msdk.base.utils.c.s = r0     // Catch:{ Exception -> 0x00ee }
            int r0 = r1.length()     // Catch:{ Exception -> 0x00ee }
            java.lang.String r0 = r1.substring(r3, r0)     // Catch:{ Exception -> 0x00ee }
            com.mintegral.msdk.base.utils.c.t = r0     // Catch:{ Exception -> 0x00ee }
        L_0x00e7:
            m(r4)     // Catch:{ Exception -> 0x00ee }
            b(r4)     // Catch:{ Exception -> 0x00ee }
            return
        L_0x00ee:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.utils.c.a(android.content.Context):void");
    }

    public static String b(final Context context) {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_DEVICE_ID) || context == null) {
            return "";
        }
        try {
            if (TextUtils.isEmpty(b)) {
                new Thread() {
                    public final void run() {
                        new com.mintegral.msdk.a.a(context).a(new b() {
                            public final void a(String str, boolean z) {
                                String unused = c.b = str;
                                g.a("CommonDeviceUtil", "getOaid oaid = " + str + " isOaidTrackLimited = " + z);
                            }

                            public final void a(String str) {
                                g.a("CommonDeviceUtil", "getOaid failed " + str);
                            }
                        });
                    }
                }.start();
            }
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
        }
        return b;
    }

    public static String c(Context context) {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITYIMEIMAC) || context == null) {
            return "";
        }
        try {
            return SensitiveDataUtil.getIMEI(context);
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
            return "";
        }
    }

    public static String d(Context context) {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITYIMEIMAC) || context == null) {
            return "";
        }
        try {
            return SensitiveDataUtil.getImsi(context);
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
            return "";
        }
    }

    public static String a() {
        return s;
    }

    public static String b() {
        return t;
    }

    public static String e(Context context) {
        if (context == null) {
            return m;
        }
        try {
            if (TextUtils.isEmpty(m)) {
                m = a.b(SensitiveDataUtil.getAndroidID(context));
            }
        } catch (Throwable unused) {
        }
        return m;
    }

    public static String f(Context context) {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_ANDROID_ID)) {
            return "";
        }
        if (context == null) {
            return f;
        }
        try {
            if (TextUtils.isEmpty(f)) {
                f = SensitiveDataUtil.getAndroidID(context);
            }
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
        }
        return f;
    }

    public static String g(Context context) {
        if (context == null) {
            return "";
        }
        g = null;
        try {
            if (TextUtils.isEmpty(g)) {
                String f2 = f(context);
                g = f2;
                g = CommonMD5.getUPMD5(f2);
            }
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
        }
        return g;
    }

    public static String c() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.MODEL;
    }

    public static String d() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.MANUFACTURER + " " + Build.MODEL;
    }

    public static String e() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.BRAND;
    }

    public static String h(Context context) {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        if (!TextUtils.isEmpty(o)) {
            return o;
        }
        if (context == null) {
            return "en";
        }
        String language = context.getResources().getConfiguration().locale.getLanguage();
        o = language;
        return language;
    }

    public static int i(Context context) {
        Configuration configuration;
        if (context == null || context.getResources() == null || (configuration = context.getResources().getConfiguration()) == null) {
            return 1;
        }
        int i2 = configuration.orientation;
        if (i2 == 2) {
            return 2;
        }
        return i2 == 1 ? 1 : 1;
    }

    public static String j(Context context) {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITYIMEIMAC)) {
            return "";
        }
        if (context == null) {
            return n;
        }
        try {
            if (TextUtils.isEmpty(n)) {
                n = SensitiveDataUtil.getMacAddress(context);
            }
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
        }
        return n;
    }

    public static int k(Context context) {
        if (context == null) {
            return l;
        }
        if (l != 0) {
            return l;
        }
        try {
            int i2 = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            l = i2;
            return i2;
        } catch (Exception e2) {
            e2.printStackTrace();
            return -1;
        }
    }

    public static String l(Context context) {
        if (context == null) {
            return k;
        }
        try {
            if (!TextUtils.isEmpty(k)) {
                return k;
            }
            String str = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            k = str;
            return str;
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public static int m(Context context) {
        if (context == null) {
            return w;
        }
        if (w == 0) {
            try {
                w = context.getApplicationInfo().targetSdkVersion;
            } catch (Exception e2) {
                g.d("CommonDeviceUtil", e2.getMessage());
            }
        }
        return w;
    }

    public static int n(Context context) {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA) || context == null) {
            return 0;
        }
        try {
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            HashMap p2 = p(context);
            return p2.get("width") == null ? displayMetrics.widthPixels : ((Integer) p2.get("width")).intValue();
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static int o(Context context) {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA) || context == null) {
            return 0;
        }
        try {
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            HashMap p2 = p(context);
            return p2.get("height") == null ? displayMetrics.heightPixels : ((Integer) p2.get("height")).intValue();
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static HashMap p(Context context) {
        HashMap hashMap = new HashMap();
        if (context == null) {
            return hashMap;
        }
        try {
            Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            Class.forName("android.view.Display").getMethod("getRealMetrics", DisplayMetrics.class).invoke(defaultDisplay, displayMetrics);
            hashMap.put("height", Integer.valueOf(displayMetrics.heightPixels));
            hashMap.put("width", Integer.valueOf(displayMetrics.widthPixels));
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
        }
        return hashMap;
    }

    public static String q(Context context) {
        if (context == null) {
            return j;
        }
        try {
            if (!TextUtils.isEmpty(j)) {
                return j;
            }
            String str = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).packageName;
            j = str;
            return str;
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public static String r(final Context context) {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return null;
        }
        if (TextUtils.isEmpty(h)) {
            try {
                h = r.b(context, "mintegral_ua", "").toString();
            } catch (Throwable th) {
                g.c("CommonDeviceUtil", th.getMessage(), th);
            }
        }
        try {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                g.a("CommonDeviceUtil", "get ua in mainThread");
                if (TextUtils.isEmpty(h)) {
                    if (K() > 17) {
                        h = WebSettings.getDefaultUserAgent(context);
                        g.a("CommonDeviceUtil", "getDefaultUserAgent:" + h);
                    }
                    if (TextUtils.isEmpty(h)) {
                        try {
                            Constructor<WebSettings> declaredConstructor = WebSettings.class.getDeclaredConstructor(Context.class, WebView.class);
                            declaredConstructor.setAccessible(true);
                            h = declaredConstructor.newInstance(context, null).getUserAgentString();
                            declaredConstructor.setAccessible(false);
                            g.a("CommonDeviceUtil", "invoke getUserAgentString:" + h);
                        } catch (Throwable th2) {
                            th2.printStackTrace();
                        }
                        if (TextUtils.isEmpty(h)) {
                            try {
                                h = new WebView(context).getSettings().getUserAgentString();
                                g.a("CommonDeviceUtil", "getUserAgentString:" + h);
                            } catch (Throwable th3) {
                                th3.printStackTrace();
                            }
                        }
                        if (TextUtils.isEmpty(h)) {
                            J();
                        }
                    }
                } else {
                    try {
                        new Thread(new Runnable() {
                            public final void run() {
                                String str = null;
                                try {
                                    if (c.K() > 17) {
                                        str = WebSettings.getDefaultUserAgent(context);
                                    }
                                    if (!TextUtils.isEmpty(str) && !str.equals(c.h)) {
                                        String unused = c.h = str;
                                        c.D(context);
                                    }
                                } catch (Throwable th) {
                                    th.printStackTrace();
                                }
                            }
                        }).start();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            } else {
                g.a("CommonDeviceUtil", "get ua in subThread");
                J();
            }
        } catch (Throwable th4) {
            g.c("CommonDeviceUtil", th4.getMessage(), th4);
        }
        D(context);
        return h;
    }

    /* access modifiers changed from: private */
    public static void D(Context context) {
        try {
            r.a(context, "mintegral_ua", h);
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
        }
    }

    public static String f() {
        if (TextUtils.isEmpty(h)) {
            r(com.mintegral.msdk.base.controller.a.d().h());
        }
        return h;
    }

    private static void J() {
        String str = Build.VERSION.RELEASE;
        String c2 = c();
        String str2 = Build.ID;
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(c2) || TextUtils.isEmpty(str2)) {
            h = "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19";
        } else {
            h = "Mozilla/5.0 (Linux; Android " + str + "; " + c2 + " Build/" + str2 + ") AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19";
        }
        g.a("CommonDeviceUtil", "append ua:" + h);
    }

    public static int s(final Context context) {
        try {
            a.a();
            if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                return e;
            }
            if (context == null) {
                return e;
            }
            if (e != 0) {
                new Thread(new Runnable() {
                    public final void run() {
                        ConnectivityManager connectivityManager;
                        try {
                            a.a();
                            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA) && context != null && (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) != null && com.mintegral.msdk.base.common.a.r) {
                                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                                if (activeNetworkInfo == null) {
                                    int unused = c.e = 0;
                                } else if (activeNetworkInfo.getType() == 1) {
                                    int unused2 = c.e = 9;
                                } else {
                                    TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                                    if (telephonyManager == null) {
                                        int unused3 = c.e = 0;
                                    } else {
                                        int unused4 = c.e = telephonyManager.getNetworkType();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            g.c("CommonDeviceUtil", e.getMessage(), e);
                            int unused5 = c.e = 0;
                        }
                    }
                }).start();
                return e;
            }
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                return e;
            }
            if (com.mintegral.msdk.base.common.a.r) {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo == null) {
                    e = 0;
                    return 0;
                } else if (activeNetworkInfo.getType() == 1) {
                    e = 9;
                    return 9;
                } else {
                    TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                    if (telephonyManager == null) {
                        e = 0;
                        return 0;
                    }
                    int networkType = telephonyManager.getNetworkType();
                    e = networkType;
                    switch (networkType) {
                        case 1:
                        case 2:
                        case 4:
                        case 7:
                        case 11:
                            return 2;
                        case 3:
                        case 5:
                        case 6:
                        case 8:
                        case 9:
                        case 10:
                        case 12:
                        case 14:
                        case 15:
                            return 3;
                        case 13:
                            return 4;
                        default:
                            return 0;
                    }
                }
            } else {
                e = 0;
                return e;
            }
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
            e = 0;
            return 0;
        }
    }

    public static int g() {
        return u;
    }

    public static String a(Context context, int i2) {
        TelephonyManager telephonyManager;
        if (i2 == 0 || i2 == 9) {
            return "";
        }
        try {
            if (!com.mintegral.msdk.base.common.a.r || (telephonyManager = (TelephonyManager) context.getSystemService("phone")) == null) {
                return "";
            }
            return String.valueOf(telephonyManager.getNetworkType());
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
            return "";
        }
    }

    public static String h() {
        try {
            a.a();
            if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                return "";
            }
            if (j() > 25) {
                return "";
            }
            if (TextUtils.isEmpty(p)) {
                new Thread(new Runnable() {
                    public final void run() {
                        try {
                            String unused = c.p = TimeZone.getDefault().getDisplayName(false, 0, Locale.ENGLISH);
                        } catch (Throwable th) {
                            th.printStackTrace();
                        }
                    }
                }).start();
                return p;
            }
            return p;
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
        }
    }

    public static String i() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        if (TextUtils.isEmpty(i)) {
            i = String.valueOf(j());
        }
        return i;
    }

    /* access modifiers changed from: private */
    public static int K() {
        try {
            if (TextUtils.isEmpty(i)) {
                return j();
            }
            return 0;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static int j() {
        try {
            return Build.VERSION.SDK_INT;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static void a(String str) {
        c = a.b(str);
        a = str;
    }

    public static String k() {
        a.a();
        if (a.a(MIntegralConstans.AUTHORITY_DEVICE_ID) && a != null) {
            return a;
        }
        return "";
    }

    public static String l() {
        a.a();
        if (a.a(MIntegralConstans.AUTHORITY_DEVICE_ID) && c != null) {
            return c;
        }
        return "";
    }

    public static String t(Context context) {
        if (context == null) {
            return "";
        }
        if (!TextUtils.isEmpty(q)) {
            return q;
        }
        try {
            String str = context.getPackageManager().getPackageInfo("com.android.vending", 0).versionName;
            q = str;
            return str;
        } catch (Exception unused) {
            return "";
        }
    }

    public static UUID m() {
        try {
            return UUID.randomUUID();
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
            return null;
        }
    }

    public static int n() {
        TelephonyManager telephonyManager;
        CellLocation cellLocation;
        try {
            if (!com.mintegral.msdk.base.common.a.t || (telephonyManager = (TelephonyManager) com.mintegral.msdk.base.controller.a.d().h().getSystemService("phone")) == null || (cellLocation = telephonyManager.getCellLocation()) == null || !(cellLocation instanceof GsmCellLocation)) {
                return 0;
            }
            return ((GsmCellLocation) cellLocation).getCid();
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
            return 0;
        }
    }

    public static String a(String str, Context context) {
        try {
            if (!TextUtils.isEmpty(v)) {
                return v;
            }
            if (!TextUtils.isEmpty(str) && context != null) {
                v = context.getPackageManager().getInstallerPackageName(str);
                g.a("CommonDeviceUtil", "PKGSource:" + v);
            }
            return v;
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
        }
    }

    public static boolean u(Context context) {
        if (context == null) {
            return false;
        }
        try {
            Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            defaultDisplay.getMetrics(displayMetrics);
            if (Math.sqrt(Math.pow((double) (((float) displayMetrics.widthPixels) / displayMetrics.xdpi), 2.0d) + Math.pow((double) (((float) displayMetrics.heightPixels) / displayMetrics.ydpi), 2.0d)) >= 6.0d) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
        }
    }

    public static String o() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.MANUFACTURER;
    }

    public static String p() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.CPU_ABI2;
    }

    public static String q() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.CPU_ABI;
    }

    public static String r() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.TAGS;
    }

    public static String s() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.USER;
    }

    public static String t() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.RADIO;
    }

    public static String u() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.BOOTLOADER;
    }

    public static String v() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.HARDWARE;
    }

    public static String w() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.HOST;
    }

    public static String x() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.VERSION.CODENAME;
    }

    public static String y() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.VERSION.INCREMENTAL;
    }

    public static String z() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_SERIAL_ID)) {
            return "";
        }
        return Build.SERIAL;
    }

    public static String A() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.DISPLAY;
    }

    public static String B() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.BOARD;
    }

    public static String C() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.TYPE;
    }

    public static String D() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.VERSION.RELEASE;
    }

    public static int E() {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return -1;
        }
        return Build.VERSION.SDK_INT;
    }

    public static int F() {
        a.a();
        return !a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA) ? -1 : 1;
    }

    public static int v(Context context) {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA) || context == null) {
            return -1;
        }
        try {
            Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            if (registerReceiver == null || registerReceiver.getExtras() == null) {
                return 0;
            }
            int intExtra = registerReceiver.getIntExtra("status", -1);
            if (intExtra == 2 || intExtra == 5) {
                return 1;
            }
            return 0;
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
            return 0;
        }
    }

    public static String w(Context context) {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        double d2 = 0.0d;
        if (context != null) {
            return "";
        }
        try {
            d2 = ((Double) Class.forName("com.android.internal.os.PowerProfile").getMethod("getBatteryCapacity", new Class[0]).invoke(Class.forName("com.android.internal.os.PowerProfile").getConstructor(Context.class).newInstance(context), new Object[0])).doubleValue();
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
        }
        return String.valueOf(d2 + " mAh");
    }

    public static int x(Context context) {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA) || context == null) {
            return -1;
        }
        try {
            String simOperator = ((TelephonyManager) context.getSystemService("phone")).getSimOperator();
            if (!"46000".equals(simOperator) && !"46002".equals(simOperator) && !"46007".equals(simOperator) && !"46008".equals(simOperator)) {
                if (!"45412".equals(simOperator)) {
                    if (!"46001".equals(simOperator) && !"46006".equals(simOperator)) {
                        if (!"46009".equals(simOperator)) {
                            return ("46003".equals(simOperator) || "46005".equals(simOperator) || "46011".equals(simOperator) || "45502".equals(simOperator) || "45507".equals(simOperator)) ? 2 : -2;
                        }
                    }
                    return 1;
                }
            }
            return 0;
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
            return -1;
        }
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x004c */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0079 A[SYNTHETIC, Splitter:B:35:0x0079] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x007e A[SYNTHETIC, Splitter:B:39:0x007e] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0091 A[SYNTHETIC, Splitter:B:47:0x0091] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0096 A[SYNTHETIC, Splitter:B:51:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00ae A[SYNTHETIC, Splitter:B:57:0x00ae] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00b3 A[SYNTHETIC, Splitter:B:61:0x00b3] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String G() {
        /*
            com.mintegral.msdk.base.controller.authoritycontroller.a.a()
            java.lang.String r0 = "authority_general_data"
            boolean r0 = com.mintegral.msdk.base.controller.authoritycontroller.a.a(r0)
            if (r0 != 0) goto L_0x000e
            java.lang.String r0 = ""
            return r0
        L_0x000e:
            java.lang.String r0 = "/proc/meminfo"
            r1 = 0
            r2 = 0
            java.io.FileReader r3 = new java.io.FileReader     // Catch:{ Exception -> 0x0082, Throwable -> 0x006a, all -> 0x0066 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0082, Throwable -> 0x006a, all -> 0x0066 }
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0061, Throwable -> 0x005c, all -> 0x0059 }
            r4 = 8192(0x2000, float:1.14794E-41)
            r0.<init>(r3, r4)     // Catch:{ Exception -> 0x0061, Throwable -> 0x005c, all -> 0x0059 }
            java.lang.String r2 = r0.readLine()     // Catch:{ Exception -> 0x0057, Throwable -> 0x0055 }
            java.lang.String r4 = "\\s+"
            java.lang.String[] r2 = r2.split(r4)     // Catch:{ Exception -> 0x0057, Throwable -> 0x0055 }
            r4 = 1
            r2 = r2[r4]     // Catch:{ Exception -> 0x0057, Throwable -> 0x0055 }
            r0.close()     // Catch:{ Exception -> 0x0057, Throwable -> 0x0055 }
            if (r2 == 0) goto L_0x0049
            java.lang.Float r4 = new java.lang.Float     // Catch:{ Exception -> 0x0057, Throwable -> 0x0055 }
            java.lang.Float r2 = java.lang.Float.valueOf(r2)     // Catch:{ Exception -> 0x0057, Throwable -> 0x0055 }
            float r2 = r2.floatValue()     // Catch:{ Exception -> 0x0057, Throwable -> 0x0055 }
            r5 = 1233125376(0x49800000, float:1048576.0)
            float r2 = r2 / r5
            r4.<init>(r2)     // Catch:{ Exception -> 0x0057, Throwable -> 0x0055 }
            double r4 = r4.doubleValue()     // Catch:{ Exception -> 0x0057, Throwable -> 0x0055 }
            double r4 = java.lang.Math.ceil(r4)     // Catch:{ Exception -> 0x0057, Throwable -> 0x0055 }
            int r1 = (int) r4
        L_0x0049:
            r3.close()     // Catch:{ IOException -> 0x004c }
        L_0x004c:
            r0.close()     // Catch:{ IOException -> 0x0050 }
            goto L_0x0099
        L_0x0050:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0099
        L_0x0055:
            r2 = move-exception
            goto L_0x006e
        L_0x0057:
            r2 = move-exception
            goto L_0x0086
        L_0x0059:
            r1 = move-exception
            r0 = r2
            goto L_0x00ac
        L_0x005c:
            r0 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
            goto L_0x006e
        L_0x0061:
            r0 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
            goto L_0x0086
        L_0x0066:
            r1 = move-exception
            r0 = r2
            r3 = r0
            goto L_0x00ac
        L_0x006a:
            r0 = move-exception
            r3 = r2
            r2 = r0
            r0 = r3
        L_0x006e:
            java.lang.String r4 = "CommonDeviceUtil"
            java.lang.String r5 = r2.getMessage()     // Catch:{ all -> 0x00ab }
            com.mintegral.msdk.base.utils.g.c(r4, r5, r2)     // Catch:{ all -> 0x00ab }
            if (r3 == 0) goto L_0x007c
            r3.close()     // Catch:{ IOException -> 0x007c }
        L_0x007c:
            if (r0 == 0) goto L_0x0099
            r0.close()     // Catch:{ IOException -> 0x0050 }
            goto L_0x0099
        L_0x0082:
            r0 = move-exception
            r3 = r2
            r2 = r0
            r0 = r3
        L_0x0086:
            java.lang.String r4 = "CommonDeviceUtil"
            java.lang.String r5 = r2.getMessage()     // Catch:{ all -> 0x00ab }
            com.mintegral.msdk.base.utils.g.c(r4, r5, r2)     // Catch:{ all -> 0x00ab }
            if (r3 == 0) goto L_0x0094
            r3.close()     // Catch:{ IOException -> 0x0094 }
        L_0x0094:
            if (r0 == 0) goto L_0x0099
            r0.close()     // Catch:{ IOException -> 0x0050 }
        L_0x0099:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r1)
            java.lang.String r1 = "GB"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            return r0
        L_0x00ab:
            r1 = move-exception
        L_0x00ac:
            if (r3 == 0) goto L_0x00b1
            r3.close()     // Catch:{ IOException -> 0x00b1 }
        L_0x00b1:
            if (r0 == 0) goto L_0x00bb
            r0.close()     // Catch:{ IOException -> 0x00b7 }
            goto L_0x00bb
        L_0x00b7:
            r0 = move-exception
            r0.printStackTrace()
        L_0x00bb:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.utils.c.G():java.lang.String");
    }

    public static String y(Context context) {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA) || context == null) {
            return "";
        }
        try {
            return Settings.System.getString(context.getContentResolver(), "time_12_24");
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
            return "";
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
            return "";
        }
    }

    public static int z(Context context) {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA) || context == null) {
            return -1;
        }
        try {
            return ((SensorManager) context.getSystemService("sensor")).getSensorList(-1).size();
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
            return -1;
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
            return -1;
        }
    }

    public static String A(Context context) {
        a.a();
        if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA) || context == null) {
            return "";
        }
        try {
            List<InputMethodInfo> inputMethodList = ((InputMethodManager) context.getSystemService("input_method")).getInputMethodList();
            StringBuilder sb = new StringBuilder();
            for (int i2 = 0; i2 < inputMethodList.size(); i2++) {
                CharSequence loadLabel = inputMethodList.get(i2).loadLabel(context.getPackageManager());
                sb.append("keybroad" + i2 + ((Object) loadLabel) + " ");
            }
            return sb.toString();
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
            return "";
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
            return "";
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:51:0x00a2=Splitter:B:51:0x00a2, B:40:0x0085=Splitter:B:40:0x0085} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String B(android.content.Context r7) {
        /*
            com.mintegral.msdk.base.controller.authoritycontroller.a.a()
            java.lang.String r0 = "authority_general_data"
            boolean r0 = com.mintegral.msdk.base.controller.authoritycontroller.a.a(r0)
            if (r0 != 0) goto L_0x000e
            java.lang.String r7 = ""
            return r7
        L_0x000e:
            if (r7 != 0) goto L_0x0013
            java.lang.String r7 = ""
            return r7
        L_0x0013:
            java.lang.String r0 = "/proc/meminfo"
            r1 = 0
            java.io.FileReader r2 = new java.io.FileReader     // Catch:{ IOException -> 0x0089, Throwable -> 0x006c, all -> 0x0069 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x0089, Throwable -> 0x006c, all -> 0x0069 }
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0067, Throwable -> 0x0065 }
            r3 = 8192(0x2000, float:1.14794E-41)
            r0.<init>(r2, r3)     // Catch:{ IOException -> 0x0067, Throwable -> 0x0065 }
            java.lang.String r1 = r0.readLine()     // Catch:{ IOException -> 0x0062, Throwable -> 0x005f, all -> 0x005c }
            java.lang.String r3 = "\\s+"
            java.lang.String[] r1 = r1.split(r3)     // Catch:{ IOException -> 0x0062, Throwable -> 0x005f, all -> 0x005c }
            r3 = 1
            r1 = r1[r3]     // Catch:{ IOException -> 0x0062, Throwable -> 0x005f, all -> 0x005c }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ IOException -> 0x0062, Throwable -> 0x005f, all -> 0x005c }
            long r3 = r1.longValue()     // Catch:{ IOException -> 0x0062, Throwable -> 0x005f, all -> 0x005c }
            r5 = 1024(0x400, double:5.06E-321)
            long r3 = r3 * r5
            java.lang.String r7 = android.text.format.Formatter.formatFileSize(r7, r3)     // Catch:{ IOException -> 0x0062, Throwable -> 0x005f, all -> 0x005c }
            r0.close()     // Catch:{ IOException -> 0x0043 }
            goto L_0x004d
        L_0x0043:
            r0 = move-exception
            java.lang.String r1 = "CommonDeviceUtil"
            java.lang.String r3 = r0.getMessage()
            com.mintegral.msdk.base.utils.g.c(r1, r3, r0)
        L_0x004d:
            r2.close()     // Catch:{ IOException -> 0x0051 }
            goto L_0x005b
        L_0x0051:
            r0 = move-exception
            java.lang.String r1 = "CommonDeviceUtil"
            java.lang.String r2 = r0.getMessage()
            com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
        L_0x005b:
            return r7
        L_0x005c:
            r7 = move-exception
            r1 = r0
            goto L_0x00b4
        L_0x005f:
            r7 = move-exception
            r1 = r0
            goto L_0x006e
        L_0x0062:
            r7 = move-exception
            r1 = r0
            goto L_0x008b
        L_0x0065:
            r7 = move-exception
            goto L_0x006e
        L_0x0067:
            r7 = move-exception
            goto L_0x008b
        L_0x0069:
            r7 = move-exception
            r2 = r1
            goto L_0x00b4
        L_0x006c:
            r7 = move-exception
            r2 = r1
        L_0x006e:
            java.lang.String r0 = "CommonDeviceUtil"
            java.lang.String r3 = r7.getMessage()     // Catch:{ all -> 0x00b3 }
            com.mintegral.msdk.base.utils.g.c(r0, r3, r7)     // Catch:{ all -> 0x00b3 }
            r1.close()     // Catch:{ IOException -> 0x007b }
            goto L_0x0085
        L_0x007b:
            r7 = move-exception
            java.lang.String r0 = "CommonDeviceUtil"
            java.lang.String r1 = r7.getMessage()
            com.mintegral.msdk.base.utils.g.c(r0, r1, r7)
        L_0x0085:
            r2.close()     // Catch:{ IOException -> 0x00a6 }
            goto L_0x00b0
        L_0x0089:
            r7 = move-exception
            r2 = r1
        L_0x008b:
            java.lang.String r0 = "CommonDeviceUtil"
            java.lang.String r3 = r7.getMessage()     // Catch:{ all -> 0x00b3 }
            com.mintegral.msdk.base.utils.g.c(r0, r3, r7)     // Catch:{ all -> 0x00b3 }
            r1.close()     // Catch:{ IOException -> 0x0098 }
            goto L_0x00a2
        L_0x0098:
            r7 = move-exception
            java.lang.String r0 = "CommonDeviceUtil"
            java.lang.String r1 = r7.getMessage()
            com.mintegral.msdk.base.utils.g.c(r0, r1, r7)
        L_0x00a2:
            r2.close()     // Catch:{ IOException -> 0x00a6 }
            goto L_0x00b0
        L_0x00a6:
            r7 = move-exception
            java.lang.String r0 = "CommonDeviceUtil"
            java.lang.String r1 = r7.getMessage()
            com.mintegral.msdk.base.utils.g.c(r0, r1, r7)
        L_0x00b0:
            java.lang.String r7 = ""
            return r7
        L_0x00b3:
            r7 = move-exception
        L_0x00b4:
            r1.close()     // Catch:{ IOException -> 0x00b8 }
            goto L_0x00c2
        L_0x00b8:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            java.lang.String r3 = "CommonDeviceUtil"
            com.mintegral.msdk.base.utils.g.c(r3, r1, r0)
        L_0x00c2:
            r2.close()     // Catch:{ IOException -> 0x00c6 }
            goto L_0x00d0
        L_0x00c6:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            java.lang.String r2 = "CommonDeviceUtil"
            com.mintegral.msdk.base.utils.g.c(r2, r1, r0)
        L_0x00d0:
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.utils.c.B(android.content.Context):java.lang.String");
    }
}
