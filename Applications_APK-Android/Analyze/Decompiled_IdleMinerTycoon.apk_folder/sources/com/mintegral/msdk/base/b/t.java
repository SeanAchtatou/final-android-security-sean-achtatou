package com.mintegral.msdk.base.b;

/* compiled from: SettingCampaignDao */
public class t extends a {
    private static t b;

    private t(h hVar) {
        super(hVar);
    }

    public static t a(h hVar) {
        if (b == null) {
            synchronized (t.class) {
                if (b == null) {
                    b = new t(hVar);
                }
            }
        }
        return b;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x003f A[SYNTHETIC, Splitter:B:25:0x003f] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0046 A[SYNTHETIC, Splitter:B:30:0x0046] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int c() {
        /*
            r11 = this;
            monitor-enter(r11)
            r0 = 1
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch:{ all -> 0x004a }
            java.lang.String r0 = " count(*) "
            r9 = 0
            r3[r9] = r0     // Catch:{ all -> 0x004a }
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r11.a()     // Catch:{ Exception -> 0x0039 }
            java.lang.String r2 = "settingCampaign"
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r1 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0039 }
            if (r1 == 0) goto L_0x0031
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x002c, all -> 0x0027 }
            if (r0 == 0) goto L_0x0031
            int r0 = r1.getInt(r9)     // Catch:{ Exception -> 0x002c, all -> 0x0027 }
            r9 = r0
            goto L_0x0031
        L_0x0027:
            r0 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0044
        L_0x002c:
            r0 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x003a
        L_0x0031:
            if (r1 == 0) goto L_0x0042
            r1.close()     // Catch:{ all -> 0x004a }
            goto L_0x0042
        L_0x0037:
            r1 = move-exception
            goto L_0x0044
        L_0x0039:
            r1 = move-exception
        L_0x003a:
            r1.printStackTrace()     // Catch:{ all -> 0x0037 }
            if (r0 == 0) goto L_0x0042
            r0.close()     // Catch:{ all -> 0x004a }
        L_0x0042:
            monitor-exit(r11)
            return r9
        L_0x0044:
            if (r0 == 0) goto L_0x0049
            r0.close()     // Catch:{ all -> 0x004a }
        L_0x0049:
            throw r1     // Catch:{ all -> 0x004a }
        L_0x004a:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.t.c():int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x001b, code lost:
        return -1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int d() {
        /*
            r4 = this;
            monitor-enter(r4)
            r0 = -1
            android.database.sqlite.SQLiteDatabase r1 = r4.b()     // Catch:{ Exception -> 0x001a, all -> 0x0017 }
            if (r1 != 0) goto L_0x000a
            monitor-exit(r4)
            return r0
        L_0x000a:
            android.database.sqlite.SQLiteDatabase r1 = r4.b()     // Catch:{ Exception -> 0x001a, all -> 0x0017 }
            java.lang.String r2 = "settingCampaign"
            r3 = 0
            int r1 = r1.delete(r2, r3, r3)     // Catch:{ Exception -> 0x001a, all -> 0x0017 }
            monitor-exit(r4)
            return r1
        L_0x0017:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x001a:
            monitor-exit(r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.t.d():int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0022, code lost:
        return -1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int a(java.lang.String r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            r0 = -1
            java.lang.String r1 = "id=?"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0021, all -> 0x001e }
            r3 = 0
            r2[r3] = r5     // Catch:{ Exception -> 0x0021, all -> 0x001e }
            android.database.sqlite.SQLiteDatabase r5 = r4.b()     // Catch:{ Exception -> 0x0021, all -> 0x001e }
            if (r5 != 0) goto L_0x0012
            monitor-exit(r4)
            return r0
        L_0x0012:
            android.database.sqlite.SQLiteDatabase r5 = r4.b()     // Catch:{ Exception -> 0x0021, all -> 0x001e }
            java.lang.String r3 = "settingCampaign"
            int r5 = r5.delete(r3, r1, r2)     // Catch:{ Exception -> 0x0021, all -> 0x001e }
            monitor-exit(r4)
            return r5
        L_0x001e:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        L_0x0021:
            monitor-exit(r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.t.a(java.lang.String):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0022, code lost:
        return -1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int b(java.lang.String r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            r0 = -1
            java.lang.String r1 = "iex=?"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0021, all -> 0x001e }
            r3 = 0
            r2[r3] = r5     // Catch:{ Exception -> 0x0021, all -> 0x001e }
            android.database.sqlite.SQLiteDatabase r5 = r4.b()     // Catch:{ Exception -> 0x0021, all -> 0x001e }
            if (r5 != 0) goto L_0x0012
            monitor-exit(r4)
            return r0
        L_0x0012:
            android.database.sqlite.SQLiteDatabase r5 = r4.b()     // Catch:{ Exception -> 0x0021, all -> 0x001e }
            java.lang.String r3 = "settingCampaign"
            int r5 = r5.delete(r3, r1, r2)     // Catch:{ Exception -> 0x0021, all -> 0x001e }
            monitor-exit(r4)
            return r5
        L_0x001e:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        L_0x0021:
            monitor-exit(r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.t.b(java.lang.String):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0078 A[SYNTHETIC, Splitter:B:27:0x0078] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0081 A[SYNTHETIC, Splitter:B:34:0x0081] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.mintegral.msdk.base.entity.o> e() {
        /*
            r10 = this;
            monitor-enter(r10)
            r0 = 0
            java.lang.String r1 = "select * from settingCampaign ORDER BY iex LIMIT 3"
            android.database.sqlite.SQLiteDatabase r2 = r10.a()     // Catch:{ Exception -> 0x006f, all -> 0x006a }
            android.database.Cursor r1 = r2.rawQuery(r1, r0)     // Catch:{ Exception -> 0x006f, all -> 0x006a }
            if (r1 == 0) goto L_0x0064
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x005f }
            if (r2 <= 0) goto L_0x0064
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Exception -> 0x005f }
            r2.<init>()     // Catch:{ Exception -> 0x005f }
        L_0x0019:
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x005d }
            if (r0 == 0) goto L_0x005b
            java.lang.String r0 = "url"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x005d }
            java.lang.String r4 = r1.getString(r0)     // Catch:{ Exception -> 0x005d }
            java.lang.String r0 = "data"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x005d }
            java.lang.String r6 = r1.getString(r0)     // Catch:{ Exception -> 0x005d }
            java.lang.String r0 = "method"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x005d }
            java.lang.String r5 = r1.getString(r0)     // Catch:{ Exception -> 0x005d }
            java.lang.String r0 = "iex"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x005d }
            java.lang.String r7 = r1.getString(r0)     // Catch:{ Exception -> 0x005d }
            java.lang.String r0 = "id"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x005d }
            int r8 = r1.getInt(r0)     // Catch:{ Exception -> 0x005d }
            com.mintegral.msdk.base.entity.o r0 = new com.mintegral.msdk.base.entity.o     // Catch:{ Exception -> 0x005d }
            r3 = r0
            r3.<init>(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x005d }
            r2.add(r0)     // Catch:{ Exception -> 0x005d }
            goto L_0x0019
        L_0x005b:
            r0 = r2
            goto L_0x0064
        L_0x005d:
            r0 = move-exception
            goto L_0x0073
        L_0x005f:
            r2 = move-exception
            r9 = r2
            r2 = r0
            r0 = r9
            goto L_0x0073
        L_0x0064:
            if (r1 == 0) goto L_0x007c
            r1.close()     // Catch:{ all -> 0x0085 }
            goto L_0x007c
        L_0x006a:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x007f
        L_0x006f:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r2
        L_0x0073:
            r0.printStackTrace()     // Catch:{ all -> 0x007e }
            if (r1 == 0) goto L_0x007b
            r1.close()     // Catch:{ all -> 0x0085 }
        L_0x007b:
            r0 = r2
        L_0x007c:
            monitor-exit(r10)
            return r0
        L_0x007e:
            r0 = move-exception
        L_0x007f:
            if (r1 == 0) goto L_0x0087
            r1.close()     // Catch:{ all -> 0x0085 }
            goto L_0x0087
        L_0x0085:
            r0 = move-exception
            goto L_0x0088
        L_0x0087:
            throw r0     // Catch:{ all -> 0x0085 }
        L_0x0088:
            monitor-exit(r10)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.t.e():java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0045, code lost:
        return -1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized long a(com.mintegral.msdk.base.entity.o r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            r0 = -1
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            r2.<init>()     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            java.lang.String r3 = "url"
            java.lang.String r4 = r6.b()     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            java.lang.String r3 = "method"
            java.lang.String r4 = r6.c()     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            java.lang.String r3 = "data"
            java.lang.String r4 = r6.d()     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            java.lang.String r3 = "iex"
            java.lang.String r6 = r6.a()     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            r2.put(r3, r6)     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            android.database.sqlite.SQLiteDatabase r6 = r5.b()     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            if (r6 != 0) goto L_0x0034
            monitor-exit(r5)
            return r0
        L_0x0034:
            android.database.sqlite.SQLiteDatabase r6 = r5.b()     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            java.lang.String r3 = "settingCampaign"
            r4 = 0
            long r2 = r6.insert(r3, r4, r2)     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            monitor-exit(r5)
            return r2
        L_0x0041:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        L_0x0044:
            monitor-exit(r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.t.a(com.mintegral.msdk.base.entity.o):long");
    }
}
