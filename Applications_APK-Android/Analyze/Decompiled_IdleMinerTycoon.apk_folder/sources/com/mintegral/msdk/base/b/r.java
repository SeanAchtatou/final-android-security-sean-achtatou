package com.mintegral.msdk.base.b;

/* compiled from: ReportErrorDao */
public class r extends a {
    private static r b;

    private r(h hVar) {
        super(hVar);
    }

    public static r a(h hVar) {
        if (b == null) {
            synchronized (r.class) {
                if (b == null) {
                    b = new r(hVar);
                }
            }
        }
        return b;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x003f A[SYNTHETIC, Splitter:B:25:0x003f] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0046 A[SYNTHETIC, Splitter:B:30:0x0046] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int c() {
        /*
            r11 = this;
            monitor-enter(r11)
            r0 = 1
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch:{ all -> 0x004a }
            java.lang.String r0 = " count(*) "
            r9 = 0
            r3[r9] = r0     // Catch:{ all -> 0x004a }
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r11.a()     // Catch:{ Exception -> 0x0039 }
            java.lang.String r2 = "reporterror"
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r1 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0039 }
            if (r1 == 0) goto L_0x0031
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x002c, all -> 0x0027 }
            if (r0 == 0) goto L_0x0031
            int r0 = r1.getInt(r9)     // Catch:{ Exception -> 0x002c, all -> 0x0027 }
            r9 = r0
            goto L_0x0031
        L_0x0027:
            r0 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0044
        L_0x002c:
            r0 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x003a
        L_0x0031:
            if (r1 == 0) goto L_0x0042
            r1.close()     // Catch:{ all -> 0x004a }
            goto L_0x0042
        L_0x0037:
            r1 = move-exception
            goto L_0x0044
        L_0x0039:
            r1 = move-exception
        L_0x003a:
            r1.printStackTrace()     // Catch:{ all -> 0x0037 }
            if (r0 == 0) goto L_0x0042
            r0.close()     // Catch:{ all -> 0x004a }
        L_0x0042:
            monitor-exit(r11)
            return r9
        L_0x0044:
            if (r0 == 0) goto L_0x0049
            r0.close()     // Catch:{ all -> 0x004a }
        L_0x0049:
            throw r1     // Catch:{ all -> 0x004a }
        L_0x004a:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.r.c():int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0022, code lost:
        return -1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int a(java.lang.String r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            r0 = -1
            java.lang.String r1 = "url=?"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0021, all -> 0x001e }
            r3 = 0
            r2[r3] = r5     // Catch:{ Exception -> 0x0021, all -> 0x001e }
            android.database.sqlite.SQLiteDatabase r5 = r4.b()     // Catch:{ Exception -> 0x0021, all -> 0x001e }
            if (r5 != 0) goto L_0x0012
            monitor-exit(r4)
            return r0
        L_0x0012:
            android.database.sqlite.SQLiteDatabase r5 = r4.b()     // Catch:{ Exception -> 0x0021, all -> 0x001e }
            java.lang.String r3 = "reporterror"
            int r5 = r5.delete(r3, r1, r2)     // Catch:{ Exception -> 0x0021, all -> 0x001e }
            monitor-exit(r4)
            return r5
        L_0x001e:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        L_0x0021:
            monitor-exit(r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.r.a(java.lang.String):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0029, code lost:
        return -1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int a(java.lang.String r5, java.lang.String r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            r0 = -1
            java.lang.String r1 = "url=? and data=?"
            if (r5 != 0) goto L_0x0008
            java.lang.String r5 = ""
        L_0x0008:
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0028, all -> 0x0025 }
            r3 = 0
            r2[r3] = r6     // Catch:{ Exception -> 0x0028, all -> 0x0025 }
            r6 = 1
            r2[r6] = r5     // Catch:{ Exception -> 0x0028, all -> 0x0025 }
            android.database.sqlite.SQLiteDatabase r5 = r4.b()     // Catch:{ Exception -> 0x0028, all -> 0x0025 }
            if (r5 != 0) goto L_0x0019
            monitor-exit(r4)
            return r0
        L_0x0019:
            android.database.sqlite.SQLiteDatabase r5 = r4.b()     // Catch:{ Exception -> 0x0028, all -> 0x0025 }
            java.lang.String r6 = "reporterror"
            int r5 = r5.delete(r6, r1, r2)     // Catch:{ Exception -> 0x0028, all -> 0x0025 }
            monitor-exit(r4)
            return r5
        L_0x0025:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        L_0x0028:
            monitor-exit(r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.r.a(java.lang.String, java.lang.String):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0036 A[SYNTHETIC, Splitter:B:20:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003e A[SYNTHETIC, Splitter:B:26:0x003e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int d() {
        /*
            r13 = this;
            monitor-enter(r13)
            r0 = 0
            r1 = 1
            java.lang.String[] r4 = new java.lang.String[r1]     // Catch:{ all -> 0x0042 }
            java.lang.String r1 = " count(*) "
            r11 = 0
            r4[r11] = r1     // Catch:{ all -> 0x0042 }
            android.database.sqlite.SQLiteDatabase r2 = r13.a()     // Catch:{ Exception -> 0x0030 }
            java.lang.String r3 = "reporterror"
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r1 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x0030 }
            int r0 = r1.getCount()     // Catch:{ Exception -> 0x0029, all -> 0x0024 }
            if (r1 == 0) goto L_0x003a
            r1.close()     // Catch:{ all -> 0x0042 }
            goto L_0x003a
        L_0x0024:
            r0 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x003c
        L_0x0029:
            r0 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x0031
        L_0x002e:
            r1 = move-exception
            goto L_0x003c
        L_0x0030:
            r1 = move-exception
        L_0x0031:
            r1.printStackTrace()     // Catch:{ all -> 0x002e }
            if (r0 == 0) goto L_0x0039
            r0.close()     // Catch:{ all -> 0x0042 }
        L_0x0039:
            r0 = 0
        L_0x003a:
            monitor-exit(r13)
            return r0
        L_0x003c:
            if (r0 == 0) goto L_0x0041
            r0.close()     // Catch:{ all -> 0x0042 }
        L_0x0041:
            throw r1     // Catch:{ all -> 0x0042 }
        L_0x0042:
            r0 = move-exception
            monitor-exit(r13)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.r.d():int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0075 A[SYNTHETIC, Splitter:B:27:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x007e A[SYNTHETIC, Splitter:B:34:0x007e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.mintegral.msdk.base.entity.o> e() {
        /*
            r11 = this;
            monitor-enter(r11)
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r11.a()     // Catch:{ Exception -> 0x006c, all -> 0x0067 }
            java.lang.String r2 = "reporterror"
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.String r9 = "20"
            android.database.Cursor r1 = r1.query(r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x006c, all -> 0x0067 }
            if (r1 == 0) goto L_0x0061
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x005c }
            if (r2 <= 0) goto L_0x0061
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Exception -> 0x005c }
            r2.<init>()     // Catch:{ Exception -> 0x005c }
        L_0x0021:
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x005a }
            if (r0 == 0) goto L_0x0058
            java.lang.String r0 = "url"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x005a }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x005a }
            java.lang.String r3 = "data"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x005a }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x005a }
            java.lang.String r4 = "method"
            int r4 = r1.getColumnIndex(r4)     // Catch:{ Exception -> 0x005a }
            java.lang.String r4 = r1.getString(r4)     // Catch:{ Exception -> 0x005a }
            java.lang.String r5 = "unitId"
            int r5 = r1.getColumnIndex(r5)     // Catch:{ Exception -> 0x005a }
            java.lang.String r5 = r1.getString(r5)     // Catch:{ Exception -> 0x005a }
            com.mintegral.msdk.base.entity.o r6 = new com.mintegral.msdk.base.entity.o     // Catch:{ Exception -> 0x005a }
            r6.<init>(r0, r4, r3, r5)     // Catch:{ Exception -> 0x005a }
            r2.add(r6)     // Catch:{ Exception -> 0x005a }
            goto L_0x0021
        L_0x0058:
            r0 = r2
            goto L_0x0061
        L_0x005a:
            r0 = move-exception
            goto L_0x0070
        L_0x005c:
            r2 = move-exception
            r10 = r2
            r2 = r0
            r0 = r10
            goto L_0x0070
        L_0x0061:
            if (r1 == 0) goto L_0x0079
            r1.close()     // Catch:{ all -> 0x0082 }
            goto L_0x0079
        L_0x0067:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x007c
        L_0x006c:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r2
        L_0x0070:
            r0.printStackTrace()     // Catch:{ all -> 0x007b }
            if (r1 == 0) goto L_0x0078
            r1.close()     // Catch:{ all -> 0x0082 }
        L_0x0078:
            r0 = r2
        L_0x0079:
            monitor-exit(r11)
            return r0
        L_0x007b:
            r0 = move-exception
        L_0x007c:
            if (r1 == 0) goto L_0x0084
            r1.close()     // Catch:{ all -> 0x0082 }
            goto L_0x0084
        L_0x0082:
            r0 = move-exception
            goto L_0x0085
        L_0x0084:
            throw r0     // Catch:{ all -> 0x0082 }
        L_0x0085:
            monitor-exit(r11)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.r.e():java.util.List");
    }
}
