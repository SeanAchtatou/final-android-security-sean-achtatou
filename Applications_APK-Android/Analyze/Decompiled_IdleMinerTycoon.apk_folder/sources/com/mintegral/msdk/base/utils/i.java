package com.mintegral.msdk.base.utils;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import com.mintegral.msdk.base.common.b.d;
import com.mintegral.msdk.base.common.b.e;
import com.mintegral.msdk.base.controller.a;
import com.tapjoy.TapjoyConstants;
import java.io.File;
import java.util.UUID;

/* compiled from: CommonSDCardUtil */
public final class i {
    static boolean a = false;
    static String b = "";
    private static boolean c = false;
    private static int d = -1;
    private static int e = -1;

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        com.mintegral.msdk.base.utils.i.b = r4.getFilesDir().getAbsolutePath() + java.io.File.separator;
        b(r4);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x003a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r4) {
        /*
            boolean r0 = com.mintegral.msdk.base.utils.i.c
            if (r0 != 0) goto L_0x005a
            r0 = 1
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x003a }
            r1.<init>()     // Catch:{ Exception -> 0x003a }
            java.io.File r2 = r4.getFilesDir()     // Catch:{ Exception -> 0x003a }
            java.lang.String r2 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x003a }
            r1.append(r2)     // Catch:{ Exception -> 0x003a }
            java.lang.String r2 = java.io.File.separator     // Catch:{ Exception -> 0x003a }
            r1.append(r2)     // Catch:{ Exception -> 0x003a }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x003a }
            com.mintegral.msdk.base.utils.i.b = r1     // Catch:{ Exception -> 0x003a }
            android.content.pm.PackageManager r1 = r4.getPackageManager()     // Catch:{ Exception -> 0x003a }
            java.lang.String r2 = "android.permission.WRITE_EXTERNAL_STORAGE"
            java.lang.String r3 = r4.getPackageName()     // Catch:{ Exception -> 0x003a }
            int r1 = r1.checkPermission(r2, r3)     // Catch:{ Exception -> 0x003a }
            if (r1 != 0) goto L_0x0033
            com.mintegral.msdk.base.utils.i.a = r0     // Catch:{ Exception -> 0x003a }
            goto L_0x0036
        L_0x0033:
            r1 = 0
            com.mintegral.msdk.base.utils.i.a = r1     // Catch:{ Exception -> 0x003a }
        L_0x0036:
            b(r4)     // Catch:{ Exception -> 0x003a }
            goto L_0x0058
        L_0x003a:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0058 }
            r1.<init>()     // Catch:{ Exception -> 0x0058 }
            java.io.File r2 = r4.getFilesDir()     // Catch:{ Exception -> 0x0058 }
            java.lang.String r2 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x0058 }
            r1.append(r2)     // Catch:{ Exception -> 0x0058 }
            java.lang.String r2 = java.io.File.separator     // Catch:{ Exception -> 0x0058 }
            r1.append(r2)     // Catch:{ Exception -> 0x0058 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0058 }
            com.mintegral.msdk.base.utils.i.b = r1     // Catch:{ Exception -> 0x0058 }
            b(r4)     // Catch:{ Exception -> 0x0058 }
        L_0x0058:
            com.mintegral.msdk.base.utils.i.c = r0
        L_0x005a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.utils.i.a(android.content.Context):void");
    }

    private static void b(Context context) {
        e.a(new d(c(context)));
        e.a().b();
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String c(android.content.Context r7) {
        /*
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 0
            r2 = 18
            if (r0 < r2) goto L_0x001a
            java.io.File r0 = r7.getExternalFilesDir(r1)     // Catch:{ Throwable -> 0x0012 }
            if (r0 == 0) goto L_0x001a
            java.io.File r0 = a(r0)     // Catch:{ Throwable -> 0x0012 }
            goto L_0x001b
        L_0x0012:
            r0 = move-exception
            java.lang.String r2 = "common-exception"
            java.lang.String r3 = "hasSDCard is failed"
            com.mintegral.msdk.base.utils.g.c(r2, r3, r0)
        L_0x001a:
            r0 = r1
        L_0x001b:
            boolean r2 = com.mintegral.msdk.base.utils.i.a
            if (r2 == 0) goto L_0x006f
            if (r0 != 0) goto L_0x005e
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.io.File r2 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r2 = r2.getPath()
            r0.append(r2)
            java.lang.String r2 = java.io.File.separator
            r0.append(r2)
            java.lang.String r2 = "Android"
            r0.append(r2)
            java.lang.String r2 = java.io.File.separator
            r0.append(r2)
            java.lang.String r2 = "data"
            r0.append(r2)
            java.lang.String r2 = java.io.File.separator
            r0.append(r2)
            java.lang.String r2 = r7.getPackageName()
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            java.io.File r2 = new java.io.File
            r2.<init>(r0)
            java.io.File r0 = a(r2)
        L_0x005e:
            long r2 = b()
            r4 = 31457280(0x1e00000, double:1.55419614E-316)
            int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r6 <= 0) goto L_0x006b
            r2 = 1
            goto L_0x006c
        L_0x006b:
            r2 = 0
        L_0x006c:
            if (r2 != 0) goto L_0x006f
            r0 = r1
        L_0x006f:
            if (r0 == 0) goto L_0x0077
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x007f
        L_0x0077:
            java.io.File r7 = r7.getFilesDir()
            java.io.File r0 = r7.getAbsoluteFile()
        L_0x007f:
            java.lang.String r7 = r0.getAbsolutePath()
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.utils.i.c(android.content.Context):java.lang.String");
    }

    private static File a(File file) {
        StringBuilder sb = new StringBuilder();
        sb.append(UUID.randomUUID());
        File file2 = new File(file, sb.toString());
        if (file2.exists()) {
            file2.delete();
        }
        if (!file2.mkdirs()) {
            return null;
        }
        file2.delete();
        return file.getAbsoluteFile();
    }

    private static boolean d() {
        try {
            if ("mounted".equals(Environment.getExternalStorageState())) {
                return true;
            }
            return false;
        } catch (Exception unused) {
            g.d("", "hasSDCard is failed");
            return false;
        }
    }

    public static int a() {
        try {
            Context h = a.d().h();
            long longValue = ((Long) r.b(h, "freeExternalSize", new Long(0))).longValue();
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - longValue > TapjoyConstants.SESSION_ID_INACTIVITY_TIME || e == -1) {
                e = new Long((b() / 1000) / 1000).intValue();
                r.a(h, "freeExternalSize", Long.valueOf(currentTimeMillis));
            }
        } catch (Throwable th) {
            g.c("CommonSDCardUtil", th.getMessage(), th);
        }
        return e;
    }

    public static long b() {
        if (!d()) {
            return 0;
        }
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static int c() {
        if (d == -1) {
            try {
                d = new Long((e() / 1000) / 1000).intValue();
            } catch (Throwable th) {
                g.c("CommonSDCardUtil", th.getMessage(), th);
            }
        }
        return d;
    }

    private static long e() {
        if (!d()) {
            return 0;
        }
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }
}
