package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.out.Campaign;
import java.util.List;

/* compiled from: SCDao */
public class s extends a<Campaign> {
    private static final String b = "com.mintegral.msdk.base.b.s";
    private static s c;

    private s(h hVar) {
        super(hVar);
    }

    public static s a(h hVar) {
        if (c == null) {
            synchronized (s.class) {
                if (c == null) {
                    c = new s(hVar);
                }
            }
        }
        return c;
    }

    public final synchronized void a(Long l) {
        try {
            String str = "get_time<" + (System.currentTimeMillis() - (l.longValue() * 1000));
            if (b() != null) {
                b().delete("sc", str, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final synchronized long a(CampaignEx campaignEx) {
        if (campaignEx == null) {
            return 0;
        }
        try {
            if (b() == null) {
                return -1;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put(CampaignEx.JSON_KEY_PACKAGE_NAME, campaignEx.getPackageName());
            contentValues.put("download_url", campaignEx.getClickURL());
            contentValues.put(CampaignEx.JSON_KEY_CLICK_MODE, campaignEx.getClick_mode());
            contentValues.put(CampaignEx.JSON_KEY_PRE_CLICK, Boolean.valueOf(campaignEx.isPreClick()));
            contentValues.put("get_time", Long.valueOf(System.currentTimeMillis()));
            if (a(campaignEx.getPackageName())) {
                return (long) b().update("sc", contentValues, "package_name = '" + campaignEx.getPackageName() + "'", null);
            }
            return b().insert("sc", null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public final synchronized void a(final List<CampaignEx> list) {
        if (list.size() != 0) {
            new Thread(new Runnable() {
                public final void run() {
                    for (CampaignEx a2 : list) {
                        s.this.a(a2);
                    }
                }
            }).start();
        }
    }

    private synchronized boolean a(String str) {
        Cursor rawQuery = a().rawQuery("SELECT get_time FROM sc WHERE package_name='" + str + "'", null);
        if (rawQuery == null || rawQuery.getCount() <= 0) {
            if (rawQuery != null) {
                rawQuery.close();
            }
            return false;
        }
        rawQuery.close();
        return true;
    }
}
