package com.mintegral.msdk.base.b;

import android.content.Context;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.k;
import com.mintegral.msdk.base.utils.g;

/* compiled from: OfferWallClickDao */
public class o extends a<k> {
    private static final String b = "com.mintegral.msdk.base.b.o";
    private static o c;

    private o(h hVar) {
        super(hVar);
    }

    public static o c() {
        try {
            if (c == null) {
                synchronized (w.class) {
                    if (c == null) {
                        Context h = a.d().h();
                        if (h != null) {
                            c = new o(i.a(h));
                        } else {
                            g.d(b, "OfferWallClickDao get Context is null");
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return c;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0044 A[SYNTHETIC, Splitter:B:22:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x004b A[SYNTHETIC, Splitter:B:27:0x004b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized int c(java.lang.String r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            r0 = 0
            r1 = 0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x003e }
            java.lang.String r3 = "select count(*) from offer_wall_click where host='"
            r2.<init>(r3)     // Catch:{ Exception -> 0x003e }
            r2.append(r6)     // Catch:{ Exception -> 0x003e }
            java.lang.String r6 = "'"
            r2.append(r6)     // Catch:{ Exception -> 0x003e }
            java.lang.String r6 = r2.toString()     // Catch:{ Exception -> 0x003e }
            android.database.sqlite.SQLiteDatabase r2 = r5.a()     // Catch:{ Exception -> 0x003e }
            android.database.Cursor r6 = r2.rawQuery(r6, r0)     // Catch:{ Exception -> 0x003e }
            if (r6 == 0) goto L_0x0036
            boolean r0 = r6.moveToFirst()     // Catch:{ Exception -> 0x0031, all -> 0x002c }
            if (r0 == 0) goto L_0x0036
            int r0 = r6.getInt(r1)     // Catch:{ Exception -> 0x0031, all -> 0x002c }
            r1 = r0
            goto L_0x0036
        L_0x002c:
            r0 = move-exception
            r4 = r0
            r0 = r6
            r6 = r4
            goto L_0x0049
        L_0x0031:
            r0 = move-exception
            r4 = r0
            r0 = r6
            r6 = r4
            goto L_0x003f
        L_0x0036:
            if (r6 == 0) goto L_0x0047
            r6.close()     // Catch:{ all -> 0x004f }
            goto L_0x0047
        L_0x003c:
            r6 = move-exception
            goto L_0x0049
        L_0x003e:
            r6 = move-exception
        L_0x003f:
            r6.printStackTrace()     // Catch:{ all -> 0x003c }
            if (r0 == 0) goto L_0x0047
            r0.close()     // Catch:{ all -> 0x004f }
        L_0x0047:
            monitor-exit(r5)
            return r1
        L_0x0049:
            if (r0 == 0) goto L_0x0051
            r0.close()     // Catch:{ all -> 0x004f }
            goto L_0x0051
        L_0x004f:
            r6 = move-exception
            goto L_0x0052
        L_0x0051:
            throw r6     // Catch:{ all -> 0x004f }
        L_0x0052:
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.o.c(java.lang.String):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0036, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(long r4, java.lang.String r6) {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = android.text.TextUtils.isEmpty(r6)     // Catch:{ Exception -> 0x0042 }
            if (r0 != 0) goto L_0x0037
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0042 }
            r2 = 0
            long r0 = r0 - r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0042 }
            java.lang.String r5 = "time<"
            r4.<init>(r5)     // Catch:{ Exception -> 0x0042 }
            r4.append(r0)     // Catch:{ Exception -> 0x0042 }
            java.lang.String r5 = " and unitId=? and install_status=0"
            r4.append(r5)     // Catch:{ Exception -> 0x0042 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0042 }
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Exception -> 0x0042 }
            r0 = 0
            r5[r0] = r6     // Catch:{ Exception -> 0x0042 }
            android.database.sqlite.SQLiteDatabase r6 = r3.b()     // Catch:{ Exception -> 0x0042 }
            if (r6 == 0) goto L_0x0035
            android.database.sqlite.SQLiteDatabase r6 = r3.b()     // Catch:{ Exception -> 0x0042 }
            java.lang.String r0 = "offer_wall_click"
            r6.delete(r0, r4, r5)     // Catch:{ Exception -> 0x0042 }
        L_0x0035:
            monitor-exit(r3)
            return
        L_0x0037:
            java.lang.String r4 = com.mintegral.msdk.base.b.o.b     // Catch:{ Exception -> 0x0042 }
            java.lang.String r5 = "unitId not for null"
            com.mintegral.msdk.base.utils.g.d(r4, r5)     // Catch:{ Exception -> 0x0042 }
            monitor-exit(r3)
            return
        L_0x0040:
            r4 = move-exception
            goto L_0x0048
        L_0x0042:
            r4 = move-exception
            r4.printStackTrace()     // Catch:{ all -> 0x0040 }
            monitor-exit(r3)
            return
        L_0x0048:
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.o.a(long, java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0057 A[SYNTHETIC, Splitter:B:30:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x005f A[SYNTHETIC, Splitter:B:36:0x005f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean d(java.lang.String r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0051 }
            java.lang.String r2 = "select * from offer_wall_click where clickId = '"
            r1.<init>(r2)     // Catch:{ Exception -> 0x0051 }
            r1.append(r4)     // Catch:{ Exception -> 0x0051 }
            java.lang.String r2 = "'"
            r1.append(r2)     // Catch:{ Exception -> 0x0051 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0051 }
            android.database.sqlite.SQLiteDatabase r2 = r3.a()     // Catch:{ Exception -> 0x0051 }
            android.database.Cursor r1 = r2.rawQuery(r1, r0)     // Catch:{ Exception -> 0x0051 }
            if (r1 == 0) goto L_0x0049
            int r0 = r1.getCount()     // Catch:{ Exception -> 0x0046, all -> 0x0043 }
            if (r0 <= 0) goto L_0x0049
        L_0x0025:
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0046, all -> 0x0043 }
            if (r0 == 0) goto L_0x0049
            java.lang.String r0 = "clickId"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x0046, all -> 0x0043 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x0046, all -> 0x0043 }
            boolean r0 = r0.equals(r4)     // Catch:{ Exception -> 0x0046, all -> 0x0043 }
            if (r0 == 0) goto L_0x0025
            if (r1 == 0) goto L_0x0040
            r1.close()     // Catch:{ all -> 0x0063 }
        L_0x0040:
            r4 = 1
            monitor-exit(r3)
            return r4
        L_0x0043:
            r4 = move-exception
            r0 = r1
            goto L_0x005d
        L_0x0046:
            r4 = move-exception
            r0 = r1
            goto L_0x0052
        L_0x0049:
            if (r1 == 0) goto L_0x005a
            r1.close()     // Catch:{ all -> 0x0063 }
            goto L_0x005a
        L_0x004f:
            r4 = move-exception
            goto L_0x005d
        L_0x0051:
            r4 = move-exception
        L_0x0052:
            r4.printStackTrace()     // Catch:{ all -> 0x004f }
            if (r0 == 0) goto L_0x005a
            r0.close()     // Catch:{ all -> 0x0063 }
        L_0x005a:
            r4 = 0
            monitor-exit(r3)
            return r4
        L_0x005d:
            if (r0 == 0) goto L_0x0065
            r0.close()     // Catch:{ all -> 0x0063 }
            goto L_0x0065
        L_0x0063:
            r4 = move-exception
            goto L_0x0066
        L_0x0065:
            throw r4     // Catch:{ all -> 0x0063 }
        L_0x0066:
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.o.d(java.lang.String):boolean");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:23:0x0054 */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [java.util.List<java.lang.String>] */
    /* JADX WARN: Type inference failed for: r1v3, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r1v4 */
    /* JADX WARN: Type inference failed for: r1v5 */
    /* JADX WARN: Type inference failed for: r1v6 */
    /* JADX WARN: Type inference failed for: r1v9 */
    /* JADX WARN: Type inference failed for: r1v11 */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0046, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0047, code lost:
        r3 = r1;
        r1 = r5;
        r5 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004b, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004c, code lost:
        r1 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x006a, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004b A[ExcHandler: all (th java.lang.Throwable), Splitter:B:8:0x0025] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0063 A[SYNTHETIC, Splitter:B:32:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x006a  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<java.lang.String> a(java.lang.String r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            boolean r0 = android.text.TextUtils.isEmpty(r5)     // Catch:{ all -> 0x0070 }
            r1 = 0
            if (r0 != 0) goto L_0x006e
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005c }
            java.lang.String r2 = "SELECT * FROM offer_wall_click where unitId="
            r0.<init>(r2)     // Catch:{ Exception -> 0x005c }
            r0.append(r5)     // Catch:{ Exception -> 0x005c }
            java.lang.String r5 = " and install_status=1"
            r0.append(r5)     // Catch:{ Exception -> 0x005c }
            java.lang.String r5 = r0.toString()     // Catch:{ Exception -> 0x005c }
            android.database.sqlite.SQLiteDatabase r0 = r4.a()     // Catch:{ Exception -> 0x005c }
            android.database.Cursor r5 = r0.rawQuery(r5, r1)     // Catch:{ Exception -> 0x005c }
            if (r5 == 0) goto L_0x0054
            int r0 = r5.getCount()     // Catch:{ Exception -> 0x004e, all -> 0x004b }
            if (r0 <= 0) goto L_0x0054
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Exception -> 0x004e, all -> 0x004b }
            r0.<init>()     // Catch:{ Exception -> 0x004e, all -> 0x004b }
        L_0x0030:
            boolean r1 = r5.moveToNext()     // Catch:{ Exception -> 0x0046, all -> 0x004b }
            if (r1 == 0) goto L_0x0044
            java.lang.String r1 = "campaignId"
            int r1 = r5.getColumnIndex(r1)     // Catch:{ Exception -> 0x0046, all -> 0x004b }
            java.lang.String r1 = r5.getString(r1)     // Catch:{ Exception -> 0x0046, all -> 0x004b }
            r0.add(r1)     // Catch:{ Exception -> 0x0046, all -> 0x004b }
            goto L_0x0030
        L_0x0044:
            r1 = r0
            goto L_0x0054
        L_0x0046:
            r1 = move-exception
            r3 = r1
            r1 = r5
            r5 = r3
            goto L_0x005e
        L_0x004b:
            r0 = move-exception
            r1 = r5
            goto L_0x0068
        L_0x004e:
            r0 = move-exception
            r3 = r1
            r1 = r5
            r5 = r0
            r0 = r3
            goto L_0x005e
        L_0x0054:
            if (r5 == 0) goto L_0x006e
            r5.close()     // Catch:{ all -> 0x0070 }
            goto L_0x006e
        L_0x005a:
            r0 = move-exception
            goto L_0x0068
        L_0x005c:
            r5 = move-exception
            r0 = r1
        L_0x005e:
            r5.printStackTrace()     // Catch:{ all -> 0x005a }
            if (r1 == 0) goto L_0x0066
            r1.close()     // Catch:{ all -> 0x0070 }
        L_0x0066:
            r1 = r0
            goto L_0x006e
        L_0x0068:
            if (r1 == 0) goto L_0x006d
            r1.close()     // Catch:{ all -> 0x0070 }
        L_0x006d:
            throw r0     // Catch:{ all -> 0x0070 }
        L_0x006e:
            monitor-exit(r4)
            return r1
        L_0x0070:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.o.a(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a1 A[SYNTHETIC, Splitter:B:30:0x00a1] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a9 A[Catch:{ all -> 0x00a6 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.mintegral.msdk.base.entity.k> b(java.lang.String r13) {
        /*
            r12 = this;
            monitor-enter(r12)
            boolean r0 = android.text.TextUtils.isEmpty(r13)     // Catch:{ all -> 0x00af }
            r1 = 0
            if (r0 != 0) goto L_0x00ad
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            java.lang.String r2 = "SELECT * FROM offer_wall_click where unitId="
            r0.<init>(r2)     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            r0.append(r13)     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            java.lang.String r13 = " and install_status=0"
            r0.append(r13)     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            java.lang.String r13 = r0.toString()     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            android.database.sqlite.SQLiteDatabase r0 = r12.a()     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            android.database.Cursor r13 = r0.rawQuery(r13, r1)     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            if (r13 == 0) goto L_0x008f
            int r0 = r13.getCount()     // Catch:{ Exception -> 0x008a }
            if (r0 <= 0) goto L_0x008f
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Exception -> 0x008a }
            r0.<init>()     // Catch:{ Exception -> 0x008a }
        L_0x0030:
            boolean r1 = r13.moveToNext()     // Catch:{ Exception -> 0x0088 }
            if (r1 == 0) goto L_0x0086
            java.lang.String r1 = "unitId"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r8 = r13.getString(r1)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r1 = "clickId"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r5 = r13.getString(r1)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r1 = "noticeUrl"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r4 = r13.getString(r1)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r1 = "time"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x0088 }
            long r6 = r13.getLong(r1)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r1 = "host"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r9 = r13.getString(r1)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r1 = "campaignId"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r3 = r13.getString(r1)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r1 = "install_status"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Exception -> 0x0088 }
            int r10 = r13.getInt(r1)     // Catch:{ Exception -> 0x0088 }
            com.mintegral.msdk.base.entity.k r1 = new com.mintegral.msdk.base.entity.k     // Catch:{ Exception -> 0x0088 }
            r2 = r1
            r2.<init>(r3, r4, r5, r6, r8, r9, r10)     // Catch:{ Exception -> 0x0088 }
            r0.add(r1)     // Catch:{ Exception -> 0x0088 }
            goto L_0x0030
        L_0x0086:
            r1 = r0
            goto L_0x008f
        L_0x0088:
            r1 = move-exception
            goto L_0x009c
        L_0x008a:
            r0 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x009c
        L_0x008f:
            if (r13 == 0) goto L_0x00ad
            r13.close()     // Catch:{ all -> 0x00af }
            goto L_0x00ad
        L_0x0095:
            r0 = move-exception
            r13 = r1
            goto L_0x00a7
        L_0x0098:
            r13 = move-exception
            r0 = r1
            r1 = r13
            r13 = r0
        L_0x009c:
            r1.printStackTrace()     // Catch:{ all -> 0x00a6 }
            if (r13 == 0) goto L_0x00a4
            r13.close()     // Catch:{ all -> 0x00af }
        L_0x00a4:
            r1 = r0
            goto L_0x00ad
        L_0x00a6:
            r0 = move-exception
        L_0x00a7:
            if (r13 == 0) goto L_0x00ac
            r13.close()     // Catch:{ all -> 0x00af }
        L_0x00ac:
            throw r0     // Catch:{ all -> 0x00af }
        L_0x00ad:
            monitor-exit(r12)
            return r1
        L_0x00af:
            r13 = move-exception
            monitor-exit(r12)
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.o.b(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0031, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.lang.String r3, java.lang.String r4) {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = android.text.TextUtils.isEmpty(r3)     // Catch:{ all -> 0x0038 }
            if (r0 != 0) goto L_0x0036
            boolean r0 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x0038 }
            if (r0 != 0) goto L_0x0036
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0032 }
            java.lang.String r1 = "UPDATE offer_wall_click Set install_status==1 WHERE campaignId="
            r0.<init>(r1)     // Catch:{ SQLException -> 0x0032 }
            r0.append(r3)     // Catch:{ SQLException -> 0x0032 }
            java.lang.String r3 = " AND unitId="
            r0.append(r3)     // Catch:{ SQLException -> 0x0032 }
            r0.append(r4)     // Catch:{ SQLException -> 0x0032 }
            java.lang.String r3 = r0.toString()     // Catch:{ SQLException -> 0x0032 }
            android.database.sqlite.SQLiteDatabase r4 = r2.a()     // Catch:{ SQLException -> 0x0032 }
            if (r4 == 0) goto L_0x0030
            android.database.sqlite.SQLiteDatabase r4 = r2.a()     // Catch:{ SQLException -> 0x0032 }
            r4.execSQL(r3)     // Catch:{ SQLException -> 0x0032 }
        L_0x0030:
            monitor-exit(r2)
            return
        L_0x0032:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x0038 }
        L_0x0036:
            monitor-exit(r2)
            return
        L_0x0038:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.o.a(java.lang.String, java.lang.String):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00a6, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(com.mintegral.msdk.base.entity.CampaignEx r5, java.lang.String r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            android.database.sqlite.SQLiteDatabase r0 = r4.b()     // Catch:{ Exception -> 0x00a9 }
            if (r0 == 0) goto L_0x00a5
            if (r5 != 0) goto L_0x0011
            boolean r0 = android.text.TextUtils.isEmpty(r6)     // Catch:{ Exception -> 0x00a9 }
            if (r0 == 0) goto L_0x0011
            goto L_0x00a5
        L_0x0011:
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ Exception -> 0x00a9 }
            r0.<init>()     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r1 = "clickId"
            java.lang.String r2 = r5.getRequestId()     // Catch:{ Exception -> 0x00a9 }
            r0.put(r1, r2)     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r1 = "time"
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00a9 }
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ Exception -> 0x00a9 }
            r0.put(r1, r2)     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r1 = "noticeUrl"
            java.lang.String r2 = r5.getNoticeUrl()     // Catch:{ Exception -> 0x00a9 }
            r0.put(r1, r2)     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r1 = "unitId"
            r0.put(r1, r6)     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r6 = "host"
            java.lang.String r1 = r5.getHost()     // Catch:{ Exception -> 0x00a9 }
            r0.put(r6, r1)     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r6 = "campaignId"
            java.lang.String r1 = r5.getId()     // Catch:{ Exception -> 0x00a9 }
            r0.put(r6, r1)     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r6 = "install_status"
            r1 = 0
            java.lang.Integer r2 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x00a9 }
            r0.put(r6, r2)     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r6 = r5.getRequestId()     // Catch:{ Exception -> 0x00a9 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ Exception -> 0x00a9 }
            if (r6 != 0) goto L_0x0084
            java.lang.String r6 = r5.getRequestId()     // Catch:{ Exception -> 0x00a9 }
            boolean r6 = r4.d(r6)     // Catch:{ Exception -> 0x00a9 }
            if (r6 == 0) goto L_0x0084
            r6 = 1
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r5 = r5.getRequestId()     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x00a9 }
            r6[r1] = r5     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r5 = "clickId=?"
            android.database.sqlite.SQLiteDatabase r1 = r4.b()     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r2 = "offer_wall_click"
            r1.update(r2, r0, r5, r6)     // Catch:{ Exception -> 0x00a9 }
            monitor-exit(r4)
            return
        L_0x0084:
            java.lang.String r5 = r5.getHost()     // Catch:{ Exception -> 0x00a9 }
            int r5 = r4.c(r5)     // Catch:{ Exception -> 0x00a9 }
            r6 = 99
            if (r5 <= r6) goto L_0x0099
            java.lang.String r5 = "delete from offer_wall_click WHERE id in(select id from offer_wall_click order by time desc limit 1)"
            android.database.sqlite.SQLiteDatabase r6 = r4.b()     // Catch:{ Exception -> 0x00a9 }
            r6.execSQL(r5)     // Catch:{ Exception -> 0x00a9 }
        L_0x0099:
            android.database.sqlite.SQLiteDatabase r5 = r4.b()     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r6 = "offer_wall_click"
            r1 = 0
            r5.insert(r6, r1, r0)     // Catch:{ Exception -> 0x00a9 }
            monitor-exit(r4)
            return
        L_0x00a5:
            monitor-exit(r4)
            return
        L_0x00a7:
            r5 = move-exception
            goto L_0x00af
        L_0x00a9:
            r5 = move-exception
            r5.printStackTrace()     // Catch:{ all -> 0x00a7 }
            monitor-exit(r4)
            return
        L_0x00af:
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.o.a(com.mintegral.msdk.base.entity.CampaignEx, java.lang.String):void");
    }
}
