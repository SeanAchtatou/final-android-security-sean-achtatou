package com.mintegral.msdk.base.b;

import android.database.sqlite.SQLiteDatabase;

/* compiled from: BaseDao */
public class a<T> {
    protected h a = null;

    public a(h hVar) {
        this.a = hVar;
    }

    /* access modifiers changed from: protected */
    public final synchronized SQLiteDatabase a() {
        return this.a.a();
    }

    /* access modifiers changed from: protected */
    public final synchronized SQLiteDatabase b() {
        return this.a.b();
    }
}
