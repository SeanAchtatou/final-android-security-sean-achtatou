package com.mintegral.msdk.base.b;

import com.mintegral.msdk.base.entity.c;

/* compiled from: CampaignClickTimeDao */
public class e extends a<c> {
    private static e b;

    private e(h hVar) {
        super(hVar);
    }

    public static e a(h hVar) {
        if (b == null) {
            synchronized (e.class) {
                if (b == null) {
                    b = new e(hVar);
                }
            }
        }
        return b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0108, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x010b, code lost:
        if (r1 != null) goto L_0x010d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0119, code lost:
        if (r1 != null) goto L_0x010d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x011d, code lost:
        return null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0121 A[SYNTHETIC, Splitter:B:40:0x0121] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.mintegral.msdk.base.entity.c> c() {
        /*
            r25 = this;
            monitor-enter(r25)
            java.lang.String r0 = "select * from click_time LIMIT 20"
            android.database.sqlite.SQLiteDatabase r1 = r25.b()     // Catch:{ all -> 0x0125 }
            r2 = 0
            if (r1 != 0) goto L_0x000c
            monitor-exit(r25)
            return r2
        L_0x000c:
            android.database.sqlite.SQLiteDatabase r1 = r25.b()     // Catch:{ Exception -> 0x0114, all -> 0x0111 }
            android.database.Cursor r1 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x0114, all -> 0x0111 }
            if (r1 == 0) goto L_0x010b
            int r0 = r1.getCount()     // Catch:{ Exception -> 0x0109 }
            if (r0 <= 0) goto L_0x010b
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Exception -> 0x0109 }
            r0.<init>()     // Catch:{ Exception -> 0x0109 }
            r3 = 0
            r4 = 0
        L_0x0023:
            boolean r5 = r1.moveToNext()     // Catch:{ Exception -> 0x0109 }
            if (r5 == 0) goto L_0x0102
            r5 = 20
            if (r4 >= r5) goto L_0x0102
            int r4 = r4 + 1
            java.lang.String r5 = "id"
            int r5 = r1.getColumnIndex(r5)     // Catch:{ Exception -> 0x0109 }
            int r5 = r1.getInt(r5)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "campaignId"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r9 = r1.getString(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "click_type"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            int r17 = r1.getInt(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "click_duration"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r10 = r1.getString(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "last_url"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r11 = r1.getString(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "type"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            int r16 = r1.getInt(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "code"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            int r12 = r1.getInt(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "header"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r14 = r1.getString(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "exception"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r13 = r1.getString(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "content"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r15 = r1.getString(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "unit_id"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r18 = r1.getString(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "rid"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r8 = r1.getString(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "landing_type"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            int r19 = r1.getInt(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "link_type"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            int r20 = r1.getInt(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "network_type"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            int r22 = r1.getInt(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "network_str"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r24 = r1.getString(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "market_result"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            int r23 = r1.getInt(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r6 = "click_time"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Exception -> 0x0109 }
            java.lang.String r21 = r1.getString(r6)     // Catch:{ Exception -> 0x0109 }
            com.mintegral.msdk.base.entity.c r6 = new com.mintegral.msdk.base.entity.c     // Catch:{ Exception -> 0x0109 }
            r7 = r6
            r7.<init>(r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24)     // Catch:{ Exception -> 0x0109 }
            r0.add(r6)     // Catch:{ Exception -> 0x0109 }
            android.database.sqlite.SQLiteDatabase r6 = r25.b()     // Catch:{ Exception -> 0x0109 }
            java.lang.String r7 = "click_time"
            java.lang.String r8 = "id = ?"
            r9 = 1
            java.lang.String[] r9 = new java.lang.String[r9]     // Catch:{ Exception -> 0x0109 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x0109 }
            r9[r3] = r5     // Catch:{ Exception -> 0x0109 }
            r6.delete(r7, r8, r9)     // Catch:{ Exception -> 0x0109 }
            goto L_0x0023
        L_0x0102:
            if (r1 == 0) goto L_0x0107
            r1.close()     // Catch:{ all -> 0x0125 }
        L_0x0107:
            monitor-exit(r25)
            return r0
        L_0x0109:
            r0 = move-exception
            goto L_0x0116
        L_0x010b:
            if (r1 == 0) goto L_0x011c
        L_0x010d:
            r1.close()     // Catch:{ all -> 0x0125 }
            goto L_0x011c
        L_0x0111:
            r0 = move-exception
            r1 = r2
            goto L_0x011f
        L_0x0114:
            r0 = move-exception
            r1 = r2
        L_0x0116:
            r0.printStackTrace()     // Catch:{ all -> 0x011e }
            if (r1 == 0) goto L_0x011c
            goto L_0x010d
        L_0x011c:
            monitor-exit(r25)
            return r2
        L_0x011e:
            r0 = move-exception
        L_0x011f:
            if (r1 == 0) goto L_0x0124
            r1.close()     // Catch:{ all -> 0x0125 }
        L_0x0124:
            throw r0     // Catch:{ all -> 0x0125 }
        L_0x0125:
            r0 = move-exception
            monitor-exit(r25)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.e.c():java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0032 A[SYNTHETIC, Splitter:B:22:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0039 A[SYNTHETIC, Splitter:B:27:0x0039] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int d() {
        /*
            r5 = this;
            monitor-enter(r5)
            r0 = 0
            r1 = 0
            java.lang.String r2 = "select count(*) from click_time"
            android.database.sqlite.SQLiteDatabase r3 = r5.a()     // Catch:{ Exception -> 0x002c }
            android.database.Cursor r2 = r3.rawQuery(r2, r0)     // Catch:{ Exception -> 0x002c }
            if (r2 == 0) goto L_0x0024
            boolean r0 = r2.moveToFirst()     // Catch:{ Exception -> 0x001f, all -> 0x001b }
            if (r0 == 0) goto L_0x0024
            int r0 = r2.getInt(r1)     // Catch:{ Exception -> 0x001f, all -> 0x001b }
            r1 = r0
            goto L_0x0024
        L_0x001b:
            r0 = move-exception
            r1 = r0
            r0 = r2
            goto L_0x0037
        L_0x001f:
            r0 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x002d
        L_0x0024:
            if (r2 == 0) goto L_0x0035
            r2.close()     // Catch:{ all -> 0x003d }
            goto L_0x0035
        L_0x002a:
            r1 = move-exception
            goto L_0x0037
        L_0x002c:
            r2 = move-exception
        L_0x002d:
            r2.printStackTrace()     // Catch:{ all -> 0x002a }
            if (r0 == 0) goto L_0x0035
            r0.close()     // Catch:{ all -> 0x003d }
        L_0x0035:
            monitor-exit(r5)
            return r1
        L_0x0037:
            if (r0 == 0) goto L_0x003f
            r0.close()     // Catch:{ all -> 0x003d }
            goto L_0x003f
        L_0x003d:
            r0 = move-exception
            goto L_0x0040
        L_0x003f:
            throw r1     // Catch:{ all -> 0x003d }
        L_0x0040:
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.base.b.e.d():int");
    }
}
