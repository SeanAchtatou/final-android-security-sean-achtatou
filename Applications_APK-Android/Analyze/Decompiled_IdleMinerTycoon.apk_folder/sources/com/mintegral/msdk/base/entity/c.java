package com.mintegral.msdk.base.entity;

import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.controller.authoritycontroller.a;
import java.util.List;

/* compiled from: ClickTime */
public final class c {
    private String a;
    private String b;
    private String c;
    private String d;
    private int e;
    private String f;
    private String g;
    private String h;
    private int i;
    private int j;
    private String k;
    private int l;
    private int m;
    private String n;
    private int o;
    private String p;
    private int q;

    public c() {
    }

    public c(String str, String str2, String str3, String str4, int i2, String str5, String str6, String str7, int i3, int i4, String str8, int i5, int i6, String str9, int i7, int i8, String str10) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = i2;
        this.f = str5;
        this.g = str6;
        this.h = str7;
        this.i = i3;
        this.j = i4;
        this.k = str8;
        this.l = i5;
        this.m = i6;
        this.n = str9;
        this.o = i7;
        this.p = str10;
        this.q = i8;
    }

    public final int a() {
        return this.o;
    }

    public final void a(int i2) {
        this.o = i2;
    }

    public final String b() {
        return this.p;
    }

    public final void a(String str) {
        this.p = str;
    }

    public final String c() {
        return this.k;
    }

    public final void b(String str) {
        this.k = str;
    }

    public final int d() {
        return this.l;
    }

    public final void b(int i2) {
        this.l = i2;
    }

    public final int e() {
        return this.m;
    }

    public final void c(int i2) {
        this.m = i2;
    }

    public final String f() {
        return this.n;
    }

    public final void c(String str) {
        this.n = str;
    }

    public final int g() {
        return this.j;
    }

    public final void d(int i2) {
        this.j = 1;
    }

    public final String h() {
        return this.f;
    }

    public final void d(String str) {
        this.f = str;
    }

    public final int i() {
        return this.e;
    }

    public final void e(int i2) {
        this.e = i2;
    }

    public final void e(String str) {
        this.g = str;
    }

    public final void f(String str) {
        this.h = str;
    }

    public final int j() {
        return this.i;
    }

    public final void f(int i2) {
        this.i = i2;
    }

    public final String k() {
        return this.d;
    }

    public final void g(String str) {
        this.d = str;
    }

    public final String l() {
        return this.b;
    }

    public final void h(String str) {
        this.b = str;
    }

    public final String m() {
        return this.c;
    }

    public final void i(String str) {
        this.c = str;
    }

    public final String n() {
        return this.a;
    }

    public final void j(String str) {
        this.a = str;
    }

    public static String a(List<c> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (c next : list) {
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                sb.append("rid_n=" + next.a);
                sb.append("&network_type=" + next.o);
                sb.append("&network_str=" + next.p);
                sb.append("&cid=" + next.b);
                sb.append("&click_type=" + next.j);
                sb.append("&type=" + next.i);
                sb.append("&click_duration=" + next.c);
                sb.append("&key=2000013");
                sb.append("&unit_id=" + next.k);
                sb.append("&last_url=" + next.d);
                sb.append("&content=" + next.h);
                sb.append("&code=" + next.e);
                sb.append("&exception=" + next.f);
                sb.append("&header=" + next.g);
                sb.append("&landing_type=" + next.l);
                sb.append("&link_type=" + next.m);
                sb.append("&click_time=" + next.n + "\n");
            } else {
                sb.append("rid_n=" + next.a);
                sb.append("&cid=" + next.b);
                sb.append("&click_type=" + next.j);
                sb.append("&type=" + next.i);
                sb.append("&click_duration=" + next.c);
                sb.append("&key=2000013");
                sb.append("&unit_id=" + next.k);
                sb.append("&last_url=" + next.d);
                sb.append("&content=" + next.h);
                sb.append("&code=" + next.e);
                sb.append("&exception=" + next.f);
                sb.append("&header=" + next.g);
                sb.append("&landing_type=" + next.l);
                sb.append("&link_type=" + next.m);
                sb.append("&click_time=" + next.n + "\n");
            }
        }
        return sb.toString();
    }

    public final String toString() {
        return "ClickTime [campaignId=" + this.b + ", click_duration=" + this.c + ", lastUrl=" + this.d + ", code=" + this.e + ", excepiton=" + this.f + ", header=" + this.g + ", content=" + this.h + ", type=" + this.i + ", click_type=" + this.j + Constants.RequestParameters.RIGHT_BRACKETS;
    }
}
