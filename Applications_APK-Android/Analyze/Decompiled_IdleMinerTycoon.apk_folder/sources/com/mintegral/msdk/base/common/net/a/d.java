package com.mintegral.msdk.base.common.net.a;

import android.support.v4.app.NotificationCompat;
import com.mintegral.msdk.base.utils.g;
import org.json.JSONObject;

/* compiled from: CommonMTGJSONObjectResponseHandler */
public abstract class d extends c {
    private static final String a = "d";

    public abstract void a(String str);

    public abstract void a(JSONObject jSONObject);

    public final /* synthetic */ void a(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        String str = a;
        g.b(str, "content = " + jSONObject);
        int optInt = jSONObject.optInt("status");
        if (optInt == 1 || optInt == 200) {
            a(jSONObject.optJSONObject("data"));
        } else {
            a(jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
        }
    }
}
