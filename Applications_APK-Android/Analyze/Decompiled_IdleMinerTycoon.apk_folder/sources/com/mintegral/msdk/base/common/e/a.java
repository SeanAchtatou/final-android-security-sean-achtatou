package com.mintegral.msdk.base.common.e;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.q;
import com.mintegral.msdk.base.utils.c;

/* compiled from: PlayableReportUtils */
public final class a {
    private static Handler a = new Handler(Looper.getMainLooper());

    public static void a(CampaignEx campaignEx, String str, String str2, String str3) {
        if (campaignEx != null && campaignEx.isMraid()) {
            q qVar = new q("2000043", TextUtils.isEmpty(campaignEx.getMraid()) ^ true ? 1 : 3, AppEventsConstants.EVENT_PARAM_VALUE_NO, "", campaignEx.getId(), str2, str, String.valueOf(campaignEx.getKeyIaRst()));
            qVar.m(campaignEx.getId());
            qVar.k(campaignEx.getRequestIdNotice());
            qVar.h(str3);
            qVar.b(c.s(com.mintegral.msdk.base.controller.a.d().h()));
            qVar.a(campaignEx.isMraid() ? q.a : q.b);
            a(qVar, com.mintegral.msdk.base.controller.a.d().h(), str2);
        }
    }

    public static void a(q qVar, Context context, String str) {
        if (qVar != null) {
            qVar.b(c.k());
            a(q.d(qVar), context, str);
        }
    }

    public static void b(q qVar, Context context, String str) {
        qVar.n("2000060");
        qVar.l(str);
        qVar.b(c.s(context));
        a(q.b(qVar), context, str);
    }

    public static void c(q qVar, Context context, String str) {
        qVar.n("2000059");
        qVar.l(str);
        qVar.b(c.s(context));
        qVar.b(c.k());
        a(q.c(qVar), context, str);
    }

    public static void a(String str, Context context, String str2) {
        if (context != null && !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            com.mintegral.msdk.base.common.g.a aVar = new com.mintegral.msdk.base.common.g.a(str, str2);
            if (a != null) {
                a.post(aVar);
            }
        }
    }

    public static void d(q qVar, Context context, String str) {
        a(q.h(qVar), context, str);
    }

    public static void e(q qVar, Context context, String str) {
        a(q.i(qVar), context, str);
    }

    public static void f(q qVar, Context context, String str) {
        qVar.n("2000063");
        qVar.l(str);
        qVar.b(c.s(context));
        a(q.a(qVar), context, str);
    }
}
