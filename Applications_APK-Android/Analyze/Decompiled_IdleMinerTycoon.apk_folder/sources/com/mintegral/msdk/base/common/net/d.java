package com.mintegral.msdk.base.common.net;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.mintegral.msdk.base.utils.g;
import java.io.IOException;
import java.lang.ref.WeakReference;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: CommonBaseResponseHandler */
public abstract class d<T> implements g {
    private boolean a;
    private a b;
    private long c;
    private String d;

    public void a() {
    }

    public abstract void a(Object obj);

    public abstract void a(String str);

    public d() {
        this("UTF-8");
    }

    private d(String str) {
        if (TextUtils.isEmpty(str)) {
            this.d = "UTF-8";
        } else {
            this.d = str;
        }
    }

    /* access modifiers changed from: private */
    public void a(Message message) {
        Object[] objArr = (Object[]) message.obj;
        switch (message.what) {
            case 100:
                a();
                return;
            case 101:
                a(objArr[1]);
                return;
            case 102:
                if (objArr[0] != null) {
                    a(objArr[0].toString());
                    return;
                } else {
                    a("network error  msg is null");
                    return;
                }
            case 103:
                return;
            case 104:
                ((Integer) objArr[0]).intValue();
                return;
            case 105:
                return;
            case 106:
                ((Integer) objArr[0]).intValue();
                ((Integer) objArr[1]).intValue();
                return;
            case 107:
                ((Integer) objArr[0]).intValue();
                ((Integer) objArr[1]).intValue();
                return;
            default:
                return;
        }
    }

    private void b(Message message) {
        if (!this.a) {
            a(message);
        } else if (!Thread.currentThread().isInterrupted() && Thread.currentThread().isAlive()) {
            this.b.sendMessage(message);
        }
    }

    private Message a(int i, Object... objArr) {
        Message message;
        if (this.a) {
            message = Message.obtain(this.b);
        } else {
            message = new Message();
        }
        message.what = i;
        message.obj = objArr;
        return message;
    }

    public final void b() {
        b(a(100, new Object[0]));
    }

    public final void a(Header[] headerArr, Object obj) {
        b(a(101, headerArr, obj));
    }

    public final void c(String str) {
        b(a(102, str));
    }

    public final void c() {
        b(a(103, new Object[0]));
    }

    public final void a(int i, Exception exc) {
        b(a(104, Integer.valueOf(i), exc));
    }

    public final void d() {
        b(a(105, new Object[0]));
    }

    public final void a(int i, int i2) {
        if (i2 > 0 && i >= 0 && System.currentTimeMillis() - this.c >= 1000) {
            this.c = System.currentTimeMillis();
            b(a(106, Integer.valueOf(i), Integer.valueOf(i2)));
        }
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        this.b = new a(this);
        this.a = true;
    }

    /* access modifiers changed from: protected */
    public final String c(HttpEntity httpEntity) throws ParseException, IOException {
        return httpEntity != null ? EntityUtils.toString(httpEntity, this.d) : "";
    }

    /* access modifiers changed from: protected */
    public JSONObject a(HttpEntity httpEntity) throws ParseException, JSONException, IOException {
        String str;
        try {
            str = c(httpEntity);
            try {
                return new JSONObject(str);
            } catch (Exception unused) {
                g.d("BaseResponseHandler", "wrong json  : " + str);
                return null;
            } catch (Throwable unused2) {
                g.d("BaseResponseHandler", "wrong json : " + str);
                return null;
            }
        } catch (Exception unused3) {
            str = "";
            g.d("BaseResponseHandler", "wrong json  : " + str);
            return null;
        } catch (Throwable unused4) {
            str = "";
            g.d("BaseResponseHandler", "wrong json : " + str);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final JSONArray d(HttpEntity httpEntity) throws ParseException, JSONException, IOException {
        return new JSONArray(c(httpEntity));
    }

    /* compiled from: CommonBaseResponseHandler */
    private static class a extends Handler {
        private WeakReference<d<?>> a;

        a(d<?> dVar) {
            this.a = new WeakReference<>(dVar);
        }

        public final void handleMessage(Message message) {
            d dVar = this.a.get();
            if (dVar != null) {
                dVar.a(message);
            }
        }
    }
}
