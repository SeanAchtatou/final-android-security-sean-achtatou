package com.mintegral.msdk.playercommon;

import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.view.SurfaceHolder;
import android.view.View;
import com.mintegral.msdk.base.utils.g;
import im.getsocial.sdk.ErrorCode;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: VideoFeedsPlayer */
public final class b implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener, MediaPlayer.OnPreparedListener {
    /* access modifiers changed from: private */
    public boolean a = false;
    /* access modifiers changed from: private */
    public boolean b = false;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public boolean d = false;
    private boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = true;
    private SurfaceHolder g;
    private int h = 5;
    /* access modifiers changed from: private */
    public int i;
    private Timer j;
    private Timer k;
    /* access modifiers changed from: private */
    public c l;
    /* access modifiers changed from: private */
    public c m;
    private Object n = new Object();
    private String o;
    private String p;
    /* access modifiers changed from: private */
    public MediaPlayer q;
    /* access modifiers changed from: private */
    public View r;
    /* access modifiers changed from: private */
    public View s;
    private boolean t;
    /* access modifiers changed from: private */
    public boolean u = false;
    private final Handler v = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            super.handleMessage(message);
        }
    };

    public final void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
    }

    public final boolean a(String str, View view, c cVar) {
        try {
            synchronized (this.n) {
                if (this.q == null) {
                    this.q = new MediaPlayer();
                    this.q.reset();
                } else {
                    this.q.release();
                    this.q = new MediaPlayer();
                    this.q.reset();
                }
                if (view == null) {
                    g.b("VideoFeedsPlayer", "loadingView为空");
                    b("MediaPlayer init error");
                    return false;
                }
                if (!TextUtils.isEmpty(str)) {
                    this.p = str;
                }
                this.l = cVar;
                this.r = view;
                this.q.setOnCompletionListener(this);
                this.q.setOnErrorListener(this);
                this.q.setOnPreparedListener(this);
                this.q.setOnInfoListener(this);
                this.q.setOnBufferingUpdateListener(this);
                return true;
            }
        } catch (Throwable th) {
            th.printStackTrace();
            b(th.toString());
            return false;
        }
    }

    public final void a(SurfaceHolder surfaceHolder) {
        try {
            if (this.q != null) {
                this.q.setDisplay(surfaceHolder);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public final void a(String str, int i2) {
        try {
            synchronized (this.n) {
                g.d("VideoFeedsPlayer", "进来播放 currentionPosition:" + this.i);
                if (i2 > 0) {
                    this.i = i2;
                }
                if (TextUtils.isEmpty(str)) {
                    b("play url is null");
                    return;
                }
                this.o = str;
                this.c = false;
                this.f = true;
                a();
                e();
                g.b("VideoFeedsPlayer", "mPlayUrl:" + this.o);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            f();
            q();
            b("mediaplayer cannot play");
        }
    }

    /* compiled from: VideoFeedsPlayer */
    private class a extends TimerTask {
        private a() {
        }

        /* synthetic */ a(b bVar, byte b) {
            this();
        }

        public final void run() {
            try {
                if (b.this.q != null && b.this.q.isPlaying()) {
                    int unused = b.this.i = b.this.q.getCurrentPosition();
                    int round = Math.round(((float) b.this.i) / 1000.0f);
                    g.b("VideoFeedsPlayer", "currentPosition:" + round + " mCurrentPosition:" + b.this.i);
                    int i = 0;
                    if (b.this.q != null && b.this.q.getDuration() > 0) {
                        i = b.this.q.getDuration() / 1000;
                    }
                    if (round >= 0 && i > 0 && b.this.q.isPlaying()) {
                        b.a(b.this, round, i);
                    }
                    boolean unused2 = b.this.a = false;
                    if (!b.this.d) {
                        b.this.q();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void o() {
        try {
            if (this.j != null) {
                this.j.cancel();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void p() {
        try {
            if (this.k != null) {
                this.k.cancel();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void a(final String str) {
        if (!this.e) {
            g.d("VideoFeedsPlayer", "不需要缓冲超时功能");
            return;
        }
        p();
        this.k = new Timer();
        this.k.schedule(new TimerTask() {
            public final void run() {
                try {
                    if (!b.this.c || b.this.d) {
                        g.d("VideoFeedsPlayer", "缓冲超时");
                        b.a(b.this, str);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, (long) (this.h * 1000));
    }

    public final void a(int i2) {
        if (i2 > 0) {
            this.h = i2;
        }
        this.e = true;
        g.b("VideoFeedsPlayer", "mIsNeedBufferingTimeout:" + this.e + "  mMaxBufferTime:" + this.h);
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        try {
            this.a = true;
            this.b = false;
            this.i = 0;
            q();
            try {
                if (this.v != null) {
                    this.v.post(new Runnable() {
                        public final void run() {
                            if (b.this.l != null) {
                                b.this.l.onPlayCompleted();
                            }
                            if (b.this.m != null) {
                                b.this.m.onPlayCompleted();
                            }
                        }
                    });
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            g.b("VideoFeedsPlayer", "======onCompletion");
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        try {
            g.b("VideoFeedsPlayer", "进来了 onprepar listener");
            if (mediaPlayer == null || !mediaPlayer.isPlaying()) {
                g.b("VideoFeedsPlayer", "onPrepared:" + this.c);
                if (this.f) {
                    this.q.seekTo(this.i);
                    this.q.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                        public final void onSeekComplete(MediaPlayer mediaPlayer) {
                            try {
                                if (b.this.f) {
                                    b.this.q();
                                    boolean unused = b.this.c = true;
                                    if (b.this.q != null) {
                                        boolean unused2 = b.this.b = true;
                                        if (!b.this.u) {
                                            b.b(b.this, b.this.q.getDuration() / 1000);
                                            g.b("VideoFeedsPlayer", "onPlayStarted()，getCurrentPosition:" + b.this.q.getCurrentPosition());
                                            boolean unused3 = b.this.u = true;
                                        }
                                        b.this.q.start();
                                    }
                                    b.this.r();
                                    b.m(b.this);
                                    g.b("VideoFeedsPlayer", "onprepare mCurrentPosition:" + b.this.i + " onprepare 开始播放 mHasPrepare：" + b.this.c);
                                }
                            } catch (Throwable th) {
                                th.printStackTrace();
                            }
                        }
                    });
                    return;
                }
                g.b("VideoFeedsPlayer", "此时在后台 不做处理");
                return;
            }
            g.b("VideoFeedsPlayer", "onprepare 正在播放");
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public final void a() {
        try {
            if (this.v != null) {
                this.v.post(new Runnable() {
                    public final void run() {
                        if (b.this.s != null) {
                            b.this.s.setVisibility(0);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void q() {
        try {
            if (this.v != null) {
                this.v.post(new Runnable() {
                    public final void run() {
                        if (b.this.r != null) {
                            b.this.r.setVisibility(8);
                        }
                        if (b.this.s != null) {
                            b.this.s.setVisibility(8);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void r() {
        try {
            if (this.v != null) {
                this.v.post(new Runnable() {
                    public final void run() {
                        if (b.this.l != null) {
                            b.this.l.OnBufferingEnd();
                        }
                        if (b.this.m != null) {
                            b.this.m.OnBufferingEnd();
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void b(final String str) {
        try {
            if (this.v != null) {
                this.v.post(new Runnable() {
                    public final void run() {
                        if (b.this.l != null) {
                            b.this.l.onPlayError(str);
                        }
                        if (b.this.m != null) {
                            b.this.m.onPlayError(str);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void b() {
        try {
            g.b("VideoFeedsPlayer", "player pause");
            if (this.c && this.q != null && this.q.isPlaying()) {
                g.b("VideoFeedsPlayer", "pause isPalying:" + this.q.isPlaying() + " mIsPlaying:" + this.b);
                q();
                this.q.pause();
                this.b = false;
            }
        } catch (Exception unused) {
        }
    }

    public final void c() {
        try {
            if (this.c && this.q != null && this.q.isPlaying()) {
                q();
                this.q.stop();
                this.c = false;
                this.b = false;
                this.a = true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void d() {
        try {
            if (!this.c) {
                g.b("VideoFeedsPlayer", "!mHasPrepare");
            } else if (this.q != null && !this.q.isPlaying()) {
                a();
                this.q.start();
                this.b = true;
                g.b("VideoFeedsPlayer", "start");
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void b(final int i2) {
        try {
            if (!this.c || this.q == null || this.q.isPlaying()) {
                return;
            }
            if (i2 > 0) {
                this.q.seekTo(i2);
                this.q.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                    public final void onSeekComplete(MediaPlayer mediaPlayer) {
                        b.this.q.start();
                        boolean unused = b.this.b = true;
                        g.b("VideoFeedsPlayer", "==================start 指定进度 curposition:" + i2);
                    }
                });
                return;
            }
            this.q.start();
            this.b = true;
            g.b("VideoFeedsPlayer", "=========start 指定进度");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void c(final int i2) {
        try {
            this.i = i2;
            if (!this.c) {
                g.a("VideoFeedsPlayer", "seekTo return mHasPrepare false");
            } else if (this.q != null) {
                this.q.seekTo(i2);
                this.q.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                    public final void onSeekComplete(MediaPlayer mediaPlayer) {
                        try {
                            if (!b.this.q.isPlaying()) {
                                b.this.q.start();
                                g.b("VideoFeedsPlayer", "seekTo start");
                            }
                            boolean unused = b.this.b = true;
                            g.b("VideoFeedsPlayer", "==================seekTo 指定进度 curposition:" + i2);
                        } catch (Throwable th) {
                            th.printStackTrace();
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void e() {
        try {
            g.b("VideoFeedsPlayer", "setDataSource");
            if (this.q != null) {
                this.q.reset();
                this.q.setDataSource(this.o);
                if (this.g != null) {
                    this.q.setDisplay(this.g);
                }
                this.c = false;
                this.q.prepareAsync();
                a("mediaplayer prepare timeout");
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            q();
            b("illegal video address");
            try {
                if (this.v != null) {
                    this.v.post(new Runnable("illegal video address") {
                        public final void run() {
                            if (b.this.m != null) {
                                b.this.m.onPlaySetDataSourceError("illegal video address");
                            }
                            if (b.this.l != null) {
                                b.this.l.onPlaySetDataSourceError("illegal video address");
                            }
                        }
                    });
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    public final void a(c cVar) {
        this.m = cVar;
    }

    public final void f() {
        try {
            g.b("VideoFeedsPlayer", "release");
            o();
            p();
            if (this.q != null) {
                c();
                new Thread(new Runnable() {
                    public final void run() {
                        try {
                            g.b("VideoFeedsPlayer", "MediaPlayer release in sub thread");
                            b.this.q.stop();
                            b.this.q.reset();
                            b.this.q.release();
                            MediaPlayer unused = b.this.q = null;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                this.m = null;
                this.l = null;
            }
            q();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public final void g() {
        try {
            if (this.q != null) {
                this.q.setVolume(0.0f, 0.0f);
                this.t = true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void h() {
        try {
            if (this.q != null) {
                this.q.setVolume(1.0f, 1.0f);
                this.t = false;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final int i() {
        return this.i;
    }

    public final boolean j() {
        try {
            if (this.q == null || !this.q.isPlaying()) {
                return false;
            }
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        try {
            g.d("VideoFeedsPlayer", "onError what:" + i2 + " extra:" + i3);
            q();
            this.c = false;
            b("unknow error");
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return true;
        }
    }

    public final boolean onInfo(MediaPlayer mediaPlayer, int i2, int i3) {
        try {
            g.d("VideoFeedsPlayer", "onInfo what:" + i2);
            switch (i2) {
                case ErrorCode.CONNECTION_TIMEOUT:
                    g.d("VideoFeedsPlayer", "BUFFERING_START:" + i2);
                    this.d = true;
                    a();
                    a("play buffering tiemout");
                    break;
                case ErrorCode.NO_INTERNET:
                    g.d("VideoFeedsPlayer", "BUFFERING_END:" + i2);
                    this.d = false;
                    q();
                    r();
                    break;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return false;
    }

    public final boolean k() {
        return this.c;
    }

    public final void a(boolean z) {
        try {
            this.f = z;
            StringBuilder sb = new StringBuilder("isFrontDesk:");
            sb.append(z ? "设置在前台" : "设置在后台");
            g.d("VideoFeedsPlayer", sb.toString());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final boolean l() {
        return this.a;
    }

    public final boolean m() {
        return this.t;
    }

    public final int n() {
        if (this.q == null) {
            return 0;
        }
        this.q.getDuration();
        return 0;
    }

    static /* synthetic */ void a(b bVar, final int i2, final int i3) {
        try {
            if (bVar.v != null) {
                bVar.v.post(new Runnable() {
                    public final void run() {
                        if (b.this.l != null) {
                            b.this.l.onPlayProgress(i2, i3);
                        }
                        if (b.this.m != null) {
                            b.this.m.onPlayProgress(i2, i3);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void a(b bVar, final String str) {
        try {
            if (bVar.v != null) {
                bVar.v.post(new Runnable() {
                    public final void run() {
                        if (b.this.l != null) {
                            b.this.l.OnBufferingStart(str);
                        }
                        if (b.this.m != null) {
                            b.this.m.OnBufferingStart(str);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void b(b bVar, final int i2) {
        try {
            if (bVar.v != null) {
                bVar.v.post(new Runnable() {
                    public final void run() {
                        if (b.this.l != null) {
                            b.this.l.onPlayStarted(i2);
                        }
                        if (b.this.m != null) {
                            b.this.m.onPlayStarted(i2);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void m(b bVar) {
        try {
            bVar.o();
            bVar.j = new Timer();
            bVar.j.schedule(new a(bVar, (byte) 0), 0, 1000);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
