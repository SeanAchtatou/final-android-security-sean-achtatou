package com.mintegral.msdk.playercommon;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.p;

public class PlayerView extends LinearLayout {
    public static final String TAG = "PlayerView";
    private LinearLayout a;
    private LinearLayout b;
    /* access modifiers changed from: private */
    public b c;
    private String d;
    private boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = true;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public SurfaceHolder j;

    public static void isCompletePlay() {
    }

    public PlayerView(Context context) {
        super(context);
        a();
    }

    public PlayerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public void addSurfaceView() {
        try {
            g.b(TAG, "addSurfaceView");
            SurfaceView surfaceView = new SurfaceView(getContext());
            this.j = surfaceView.getHolder();
            this.j.setType(3);
            this.j.setKeepScreenOn(true);
            this.j.addCallback(new a(this, (byte) 0));
            this.a.addView(surfaceView, -1, -1);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void removeSurface() {
        try {
            g.b(TAG, "removeSurface");
            this.a.removeAllViews();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean initVFPData(String str, String str2, c cVar) {
        if (TextUtils.isEmpty(str)) {
            g.b(TAG, "playUrl==null");
            return false;
        }
        this.d = str;
        this.c.a(str2, this.b, cVar);
        this.e = true;
        return true;
    }

    public void playVideo(int i2) {
        try {
            if (this.c == null) {
                g.b(TAG, "player init error 播放失败");
            } else if (!this.e) {
                g.b(TAG, "vfp init failed 播放失败");
            } else {
                this.c.a(this.d, i2);
                this.i = false;
            }
        } catch (Throwable th) {
            g.c(TAG, th.getMessage(), th);
        }
    }

    public void playVideo() {
        playVideo(0);
    }

    public void onPause() {
        try {
            pause();
            if (this.c != null) {
                this.c.a(false);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void pause() {
        try {
            if (this.c != null) {
                this.c.b();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void resumeStar() {
        try {
            if (Build.VERSION.SDK_INT >= 24) {
                setDataSource();
            } else {
                start();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void setDataSource() {
        try {
            if (this.c != null) {
                this.c.a();
                this.c.e();
                this.i = false;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void start() {
        try {
            if (this.c != null) {
                this.c.d();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void start(int i2) {
        try {
            if (this.c != null) {
                this.c.b(i2);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void seekTo(int i2) {
        try {
            if (this.c != null) {
                this.c.c(i2);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void stop() {
        try {
            if (this.c != null) {
                this.c.c();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void openSound() {
        if (this.c != null) {
            this.c.h();
        }
    }

    public void closeSound() {
        if (this.c != null) {
            this.c.g();
        }
    }

    public void onResume() {
        try {
            this.c.a(true);
            if (this.c != null && !this.f && !this.g && !isComplete()) {
                g.b(TAG, "onresume========");
                if (this.c.k()) {
                    resumeStar();
                } else {
                    playVideo(0);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void coverUnlockResume() {
        try {
            this.c.a(true);
            if (this.c != null) {
                g.b(TAG, "coverUnlockResume========");
                if (this.c.k()) {
                    if (!this.i) {
                        start();
                        return;
                    }
                }
                playVideo(0);
            }
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }

    public void release() {
        try {
            if (this.c != null) {
                this.c.f();
            }
            if (Build.VERSION.SDK_INT >= 14 && this.j != null) {
                g.d(TAG, "mSurfaceHolder release");
                this.j.getSurface().release();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public int getCurPosition() {
        try {
            if (this.c != null) {
                return this.c.i();
            }
            return 0;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public int getDuration() {
        if (this.c != null) {
            return this.c.n();
        }
        return 0;
    }

    private class a implements SurfaceHolder.Callback {
        private a() {
        }

        /* synthetic */ a(PlayerView playerView, byte b) {
            this();
        }

        public final void surfaceCreated(SurfaceHolder surfaceHolder) {
            try {
                g.b(PlayerView.TAG, "surfaceCreated");
                if (!(PlayerView.this.c == null || surfaceHolder == null)) {
                    SurfaceHolder unused = PlayerView.this.j = surfaceHolder;
                    PlayerView.this.c.a(surfaceHolder);
                }
                boolean unused2 = PlayerView.this.f = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.mintegral.msdk.playercommon.PlayerView.a(com.mintegral.msdk.playercommon.PlayerView, boolean):boolean
         arg types: [com.mintegral.msdk.playercommon.PlayerView, int]
         candidates:
          com.mintegral.msdk.playercommon.PlayerView.a(com.mintegral.msdk.playercommon.PlayerView, android.view.SurfaceHolder):android.view.SurfaceHolder
          com.mintegral.msdk.playercommon.PlayerView.a(com.mintegral.msdk.playercommon.PlayerView, boolean):boolean */
        public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            try {
                g.b(PlayerView.TAG, "surfaceDestroyed ");
                boolean unused = PlayerView.this.g = true;
                boolean unused2 = PlayerView.this.i = true;
                PlayerView.this.c.b();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.mintegral.msdk.playercommon.PlayerView.a(com.mintegral.msdk.playercommon.PlayerView, boolean):boolean
         arg types: [com.mintegral.msdk.playercommon.PlayerView, int]
         candidates:
          com.mintegral.msdk.playercommon.PlayerView.a(com.mintegral.msdk.playercommon.PlayerView, android.view.SurfaceHolder):android.view.SurfaceHolder
          com.mintegral.msdk.playercommon.PlayerView.a(com.mintegral.msdk.playercommon.PlayerView, boolean):boolean */
        public final void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
            try {
                g.b(PlayerView.TAG, "surfaceChanged");
                if (PlayerView.this.g && !PlayerView.this.h && !PlayerView.this.isComplete()) {
                    if (PlayerView.this.c.k()) {
                        g.b(PlayerView.TAG, "surfaceChanged  start====");
                        PlayerView.this.resumeStar();
                    } else {
                        g.b(PlayerView.TAG, "surfaceChanged  PLAY====");
                        PlayerView.this.playVideo(0);
                    }
                }
                boolean unused = PlayerView.this.g = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isComplete() {
        try {
            return this.c != null && this.c.l();
        } catch (Throwable th) {
            g.c(TAG, th.getMessage(), th);
            return false;
        }
    }

    public void initBufferIngParam(int i2) {
        if (this.c != null) {
            this.c.a(i2);
        }
    }

    public boolean isPlayIng() {
        try {
            if (this.c != null) {
                return this.c.j();
            }
            return false;
        } catch (Throwable th) {
            if (!MIntegralConstans.DEBUG) {
                return false;
            }
            th.printStackTrace();
            return false;
        }
    }

    public void setIsCovered(boolean z) {
        try {
            this.h = z;
            g.d(TAG, "mIsCovered:" + z);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean isSilent() {
        return this.c.m();
    }

    private void a() {
        try {
            View inflate = LayoutInflater.from(getContext()).inflate(p.a(getContext(), "mintegral_playercommon_player_view", "layout"), (ViewGroup) null);
            if (inflate != null) {
                this.a = (LinearLayout) inflate.findViewById(p.a(getContext(), "mintegral_playercommon_ll_sur_container", "id"));
                this.b = (LinearLayout) inflate.findViewById(p.a(getContext(), "mintegral_playercommon_ll_loading", "id"));
                addSurfaceView();
                addView(inflate, -1, -1);
            }
            this.c = new b();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
