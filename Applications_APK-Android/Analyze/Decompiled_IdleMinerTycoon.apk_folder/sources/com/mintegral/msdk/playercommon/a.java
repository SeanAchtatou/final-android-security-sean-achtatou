package com.mintegral.msdk.playercommon;

import com.mintegral.msdk.base.utils.g;

/* compiled from: DefaultVideoFeedsPlayerListener */
public class a implements c {
    public void onPlayStarted(int i) {
        g.a("DefaultVideoFeedsPlayerListener", "onPlayStarted:" + i);
    }

    public void onPlayCompleted() {
        g.a("DefaultVideoFeedsPlayerListener", "onPlayCompleted");
    }

    public void onPlayError(String str) {
        g.a("DefaultVideoFeedsPlayerListener", "onPlayError:" + str);
    }

    public void onPlayProgress(int i, int i2) {
        g.a("DefaultVideoFeedsPlayerListener", "onPlayProgress:" + i + ",allDuration:" + i2);
    }

    public void OnBufferingStart(String str) {
        g.a("DefaultVideoFeedsPlayerListener", "OnBufferingStart:" + str);
    }

    public void OnBufferingEnd() {
        g.a("DefaultVideoFeedsPlayerListener", "OnBufferingEnd");
    }

    public void onPlaySetDataSourceError(String str) {
        g.a("DefaultVideoFeedsPlayerListener", "onPlaySetDataSourceError:" + str);
    }

    public void onPlayProgressMS(int i, int i2) {
        g.a("DefaultVideoFeedsPlayerListener", "onPlayProgressMS:");
    }
}
