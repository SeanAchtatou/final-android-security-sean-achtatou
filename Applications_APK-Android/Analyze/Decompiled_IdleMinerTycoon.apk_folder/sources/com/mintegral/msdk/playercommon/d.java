package com.mintegral.msdk.playercommon;

import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.view.Surface;
import android.view.View;
import android.webkit.URLUtil;
import com.mintegral.msdk.base.utils.g;
import im.getsocial.sdk.ErrorCode;
import java.io.File;
import java.io.FileInputStream;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: VideoNativePlayer */
public final class d implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener, MediaPlayer.OnPreparedListener {
    /* access modifiers changed from: private */
    public boolean a = false;
    /* access modifiers changed from: private */
    public boolean b = false;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public boolean d = true;
    private boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = false;
    private boolean g = false;
    private boolean h = true;
    private Surface i;
    private int j = 5;
    /* access modifiers changed from: private */
    public int k;
    private Timer l;
    private Timer m;
    /* access modifiers changed from: private */
    public c n;
    /* access modifiers changed from: private */
    public c o;
    private Object p = new Object();
    private String q;
    private String r;
    /* access modifiers changed from: private */
    public MediaPlayer s;
    /* access modifiers changed from: private */
    public View t;
    private Surface u;
    private boolean v = true;
    private final Handler w = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            super.handleMessage(message);
        }
    };

    public final void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
    }

    public final void a(String str, Surface surface) {
        try {
            if (TextUtils.isEmpty(str)) {
                b("play url is null");
                return;
            }
            m();
            this.q = str;
            this.c = false;
            this.h = true;
            this.i = surface;
            p();
            g.b("VideoFeedsPlayer", "mPlayUrl:" + this.q);
        } catch (Exception e2) {
            e2.printStackTrace();
            c();
            n();
            b("mediaplayer cannot play");
        }
    }

    /* compiled from: VideoNativePlayer */
    private class b extends TimerTask {
        private b() {
        }

        /* synthetic */ b(d dVar, byte b) {
            this();
        }

        public final void run() {
            try {
                if (d.this.s != null && d.this.s.isPlaying()) {
                    int unused = d.this.k = d.this.s.getCurrentPosition();
                    int b = d.this.k / 1000;
                    g.b("VideoFeedsPlayer", "currentPosition:" + b);
                    int i = 0;
                    if (d.this.s != null && d.this.s.getDuration() > 0) {
                        i = d.this.s.getDuration() / 1000;
                    }
                    if (d.this.d) {
                        d.b(d.this, i);
                        g.b("VideoFeedsPlayer", "onPlayStarted()");
                        boolean unused2 = d.this.d = false;
                    }
                    if (b >= 0 && i > 0 && d.this.s.isPlaying()) {
                        d.a(d.this, b, i);
                    }
                    boolean unused3 = d.this.a = false;
                    if (!d.this.f) {
                        d.this.n();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* compiled from: VideoNativePlayer */
    private class a extends TimerTask {
        private a() {
        }

        /* synthetic */ a(d dVar, byte b) {
            this();
        }

        public final void run() {
            try {
                if (d.this.s != null && d.this.s.isPlaying()) {
                    int unused = d.this.k = d.this.s.getCurrentPosition();
                    int b = d.this.k / 100;
                    int i = 0;
                    if (d.this.s != null && d.this.s.getDuration() > 0) {
                        i = d.this.s.getDuration() / 100;
                    }
                    if (b >= 0 && i > 0 && d.this.s.isPlaying()) {
                        d.b(d.this, b, i);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        try {
            if (this.l != null) {
                this.l.cancel();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        try {
            if (this.m != null) {
                this.m.cancel();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void a(final String str) {
        if (!this.g) {
            g.d("VideoFeedsPlayer", "不需要缓冲超时功能");
            return;
        }
        l();
        this.m = new Timer();
        this.m.schedule(new TimerTask() {
            public final void run() {
                try {
                    if (!d.this.c || d.this.f) {
                        g.d("VideoFeedsPlayer", "缓冲超时");
                        d.a(d.this, str);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, (long) (this.j * 1000));
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        try {
            this.a = true;
            this.b = false;
            this.k = 0;
            n();
            try {
                if (this.w != null) {
                    this.w.post(new Runnable() {
                        public final void run() {
                            if (d.this.n != null) {
                                d.this.n.onPlayCompleted();
                            }
                            if (d.this.o != null) {
                                d.this.o.onPlayCompleted();
                            }
                        }
                    });
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            g.b("VideoFeedsPlayer", "======onCompletion");
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public final void onPrepared(final MediaPlayer mediaPlayer) {
        try {
            g.b("VideoFeedsPlayer", "进来了 onprepar listener");
            if (mediaPlayer == null || !mediaPlayer.isPlaying()) {
                g.b("VideoFeedsPlayer", "onPrepared:" + this.c + " mIsFrontDesk:" + this.h);
                if (this.h) {
                    this.s.seekTo(this.k);
                    g.b("VideoFeedsPlayer", "onPrepared:" + this.k);
                    this.s.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.mintegral.msdk.playercommon.d.a(com.mintegral.msdk.playercommon.d, boolean):boolean
                         arg types: [com.mintegral.msdk.playercommon.d, int]
                         candidates:
                          com.mintegral.msdk.playercommon.d.a(com.mintegral.msdk.playercommon.d, int):int
                          com.mintegral.msdk.playercommon.d.a(com.mintegral.msdk.playercommon.d, java.lang.String):void
                          com.mintegral.msdk.playercommon.d.a(java.lang.String, android.view.Surface):void
                          com.mintegral.msdk.playercommon.d.a(com.mintegral.msdk.playercommon.d, boolean):boolean */
                        public final void onSeekComplete(MediaPlayer mediaPlayer) {
                            try {
                                boolean unused = d.this.c = true;
                                d.this.o();
                                d.k(d.this);
                                if (mediaPlayer != null) {
                                    mediaPlayer.start();
                                    boolean unused2 = d.this.b = true;
                                }
                                g.b("VideoFeedsPlayer", "onprepare mCurrentPosition:" + d.this.k + " onprepare 开始播放 mHasPrepare：" + d.this.c);
                            } catch (Throwable th) {
                                th.printStackTrace();
                            }
                        }
                    });
                    return;
                }
                g.b("VideoFeedsPlayer", "此时在后台 不做处理");
                return;
            }
            g.b("VideoFeedsPlayer", "onprepare 正在播放");
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private void m() {
        try {
            if (this.w != null) {
                this.w.post(new Runnable() {
                    public final void run() {
                        if (d.this.t != null) {
                            d.this.t.setVisibility(0);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void n() {
        try {
            if (this.w != null) {
                this.w.post(new Runnable() {
                    public final void run() {
                        if (d.this.t != null) {
                            d.this.t.setVisibility(8);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void o() {
        try {
            if (this.w != null) {
                this.w.post(new Runnable() {
                    public final void run() {
                        if (d.this.n != null) {
                            d.this.n.OnBufferingEnd();
                        }
                        if (d.this.o != null) {
                            d.this.o.OnBufferingEnd();
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void b(final String str) {
        try {
            if (this.w != null) {
                this.w.post(new Runnable() {
                    public final void run() {
                        if (d.this.n != null) {
                            d.this.n.onPlayError(str);
                        }
                        if (d.this.o != null) {
                            d.this.o.onPlayError(str);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void c(final String str) {
        try {
            if (this.w != null) {
                this.w.post(new Runnable() {
                    public final void run() {
                        if (d.this.o != null) {
                            d.this.o.onPlaySetDataSourceError(str);
                        }
                        if (d.this.n != null) {
                            d.this.n.onPlaySetDataSourceError(str);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void a() {
        try {
            g.b("VideoFeedsPlayer", "player pause");
            if (!this.c) {
                g.b("VideoFeedsPlayer", "pause !mHasPrepare retrun");
                return;
            }
            boolean z = false;
            if (this.s == null || !this.s.isPlaying()) {
                StringBuilder sb = new StringBuilder("pause mMediaPlayer==null?");
                if (this.s == null) {
                    z = true;
                }
                sb.append(z);
                sb.append(" mediaplayer is null or haspause return");
                g.b("VideoFeedsPlayer", sb.toString());
                return;
            }
            g.b("VideoFeedsPlayer", "pause " + this.b);
            n();
            this.s.pause();
            this.b = false;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void b() {
        try {
            if (!this.c) {
                g.b("VideoFeedsPlayer", "stop !mHasPrepare retrun");
            } else if (this.s != null && this.s.isPlaying()) {
                n();
                this.s.stop();
                this.b = false;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void a(Surface surface) {
        try {
            if (!this.c) {
                g.b("VideoFeedsPlayer", "start !mHasPrepare retrun");
                return;
            }
            boolean z = true;
            if (this.s == null || this.s.isPlaying()) {
                StringBuilder sb = new StringBuilder("start mMediaPlayer==null?");
                if (this.s != null) {
                    z = false;
                }
                sb.append(z);
                sb.append(" mediaplayer is null or isplaying return");
                g.b("VideoFeedsPlayer", sb.toString());
                return;
            }
            m();
            if (surface != null) {
                try {
                    if (this.s != null) {
                        this.u = surface;
                        this.s.setSurface(surface);
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
            this.s.start();
            this.b = true;
            g.b("VideoFeedsPlayer", "调用 start");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final boolean a(String str, View view, c cVar) {
        try {
            return b(str, view, cVar);
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    private boolean b(String str, View view, c cVar) {
        try {
            if (TextUtils.isEmpty(str)) {
                g.b("VideoFeedsPlayer", "netUrl为空 return");
                b("MediaPlayer init error");
                return false;
            } else if (view == null) {
                g.b("VideoFeedsPlayer", "loadingView为空 return");
                b("MediaPlayer init error");
                return false;
            } else {
                this.v = true;
                this.t = view;
                this.r = str;
                this.n = cVar;
                return true;
            }
        } catch (Throwable th) {
            th.printStackTrace();
            b(th.toString());
            return false;
        }
    }

    private void p() {
        try {
            g.b("VideoFeedsPlayer", "setDataSource begin");
            if (this.s != null) {
                try {
                    if (this.s.isPlaying()) {
                        this.s.stop();
                    }
                    this.s.release();
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
            this.s = new MediaPlayer();
            this.s.setOnCompletionListener(this);
            this.s.setOnErrorListener(this);
            this.s.setOnPreparedListener(this);
            this.s.setOnInfoListener(this);
            this.s.setOnBufferingUpdateListener(this);
            if (!this.v) {
                d();
            }
            if ((TextUtils.isEmpty(this.q) || !this.q.startsWith("http")) && !this.q.startsWith("https")) {
                File file = new File(this.q);
                if (file.exists()) {
                    this.s.setDataSource(new FileInputStream(file).getFD());
                } else {
                    this.s.setDataSource(this.q);
                }
            } else {
                this.s.setDataSource(this.q);
            }
            if (this.i != null) {
                this.s.setSurface(this.i);
            }
            this.c = false;
            g.b("VideoFeedsPlayer", "setDataSource prepareAsync");
            this.s.prepareAsync();
            a("mediaplayer prepare timeout");
            g.b("VideoFeedsPlayer", "setDataSource done");
        } catch (Exception e2) {
            e2.printStackTrace();
            n();
            if (URLUtil.isNetworkUrl(this.q)) {
                g.b("VideoFeedsPlayer", "setDataSource error 当前已经是网络url 不能抄底播放了 :" + this.r);
                b("mediaplayer cannot play");
                c("set data source error");
            } else if (TextUtils.isEmpty(this.r) || this.e) {
                b("mediaplayer cannot play");
            } else {
                this.e = true;
                g.b("VideoFeedsPlayer", "setDataSource error 抄底播放  mNetUrl:" + this.r);
                this.q = this.r;
                m();
                p();
            }
            c("set data source error");
        }
    }

    public final void a(c cVar) {
        this.o = cVar;
    }

    public final void c() {
        new Thread(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.mintegral.msdk.playercommon.d.a(com.mintegral.msdk.playercommon.d, boolean):boolean
             arg types: [com.mintegral.msdk.playercommon.d, int]
             candidates:
              com.mintegral.msdk.playercommon.d.a(com.mintegral.msdk.playercommon.d, int):int
              com.mintegral.msdk.playercommon.d.a(com.mintegral.msdk.playercommon.d, java.lang.String):void
              com.mintegral.msdk.playercommon.d.a(java.lang.String, android.view.Surface):void
              com.mintegral.msdk.playercommon.d.a(com.mintegral.msdk.playercommon.d, boolean):boolean */
            public final void run() {
                try {
                    g.b("VideoFeedsPlayer", "releasePlayer");
                    d.this.k();
                    d.this.l();
                    if (d.this.s != null) {
                        d.this.b();
                        d.this.s.release();
                        MediaPlayer unused = d.this.s = null;
                        boolean unused2 = d.this.b = false;
                    }
                } catch (Throwable th) {
                    g.c("VideoFeedsPlayer", th.getMessage(), th);
                }
            }
        }).start();
        n();
    }

    public final void d() {
        try {
            if (this.s != null) {
                this.s.setVolume(0.0f, 0.0f);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void e() {
        try {
            if (this.s != null) {
                this.s.setVolume(1.0f, 1.0f);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final boolean f() {
        try {
            if (this.s == null || !this.s.isPlaying()) {
                return false;
            }
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        try {
            g.d("VideoFeedsPlayer", "onError what:" + i2 + " extra:" + i3);
            n();
            this.c = false;
            this.b = false;
            b("unknow error");
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return true;
        }
    }

    public final boolean onInfo(MediaPlayer mediaPlayer, int i2, int i3) {
        try {
            g.d("VideoFeedsPlayer", "onInfo what:" + i2);
            switch (i2) {
                case ErrorCode.CONNECTION_TIMEOUT:
                    g.d("VideoFeedsPlayer", "BUFFERING_START:" + i2);
                    this.f = true;
                    m();
                    a("play buffering tiemout");
                    break;
                case ErrorCode.NO_INTERNET:
                    g.d("VideoFeedsPlayer", "BUFFERING_END:" + i2);
                    this.f = false;
                    n();
                    o();
                    break;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return false;
    }

    public final boolean g() {
        return this.c;
    }

    public final void a(boolean z) {
        try {
            this.h = z;
            StringBuilder sb = new StringBuilder("isFrontDesk:");
            sb.append(z ? "设置在前台" : "设置在后台");
            g.d("VideoFeedsPlayer", sb.toString());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final boolean h() {
        return this.a;
    }

    public final void b(boolean z) {
        this.a = z;
    }

    public final boolean i() {
        return this.b;
    }

    public final boolean j() {
        try {
            if (this.t == null || this.t.getVisibility() != 0) {
                return false;
            }
            return true;
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    static /* synthetic */ void b(d dVar, final int i2) {
        try {
            if (dVar.w != null) {
                dVar.w.post(new Runnable() {
                    public final void run() {
                        if (d.this.n != null) {
                            d.this.n.onPlayStarted(i2);
                        }
                        if (d.this.o != null) {
                            d.this.o.onPlayStarted(i2);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void a(d dVar, final int i2, final int i3) {
        try {
            if (dVar.w != null) {
                dVar.w.post(new Runnable() {
                    public final void run() {
                        if (d.this.n != null) {
                            d.this.n.onPlayProgress(i2, i3);
                        }
                        if (d.this.o != null) {
                            d.this.o.onPlayProgress(i2, i3);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void b(d dVar, final int i2, final int i3) {
        try {
            if (dVar.w != null) {
                dVar.w.post(new Runnable() {
                    public final void run() {
                        if (d.this.n != null) {
                            d.this.n.onPlayProgressMS(i2, i3);
                        }
                        if (d.this.o != null) {
                            d.this.o.onPlayProgressMS(i2, i3);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void a(d dVar, final String str) {
        try {
            if (dVar.w != null) {
                dVar.w.post(new Runnable() {
                    public final void run() {
                        if (d.this.n != null) {
                            d.this.n.OnBufferingStart(str);
                        }
                        if (d.this.o != null) {
                            d.this.o.OnBufferingStart(str);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void k(d dVar) {
        try {
            dVar.k();
            dVar.l = new Timer();
            dVar.l.schedule(new b(dVar, (byte) 0), 1000, 1000);
            dVar.l.schedule(new a(dVar, (byte) 0), 100, 100);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
