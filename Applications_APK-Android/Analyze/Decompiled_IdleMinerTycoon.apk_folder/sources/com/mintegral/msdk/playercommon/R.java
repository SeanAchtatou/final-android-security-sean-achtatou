package com.mintegral.msdk.playercommon;

public final class R {
    private R() {
    }

    public static final class id {
        public static final int mintegral_playercommon_ll_loading = 2131230972;
        public static final int mintegral_playercommon_ll_sur_container = 2131230973;
        public static final int mintegral_playercommon_rl_root = 2131230974;
        public static final int progressBar = 2131231044;

        private id() {
        }
    }

    public static final class layout {
        public static final int mintegral_playercommon_player_view = 2131427448;

        private layout() {
        }
    }
}
