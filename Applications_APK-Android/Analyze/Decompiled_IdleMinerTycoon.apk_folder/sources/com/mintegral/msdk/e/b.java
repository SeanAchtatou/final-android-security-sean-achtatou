package com.mintegral.msdk.e;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.helpshift.common.domain.network.NetworkConstants;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.activity.MTGCommonActivity;
import com.mintegral.msdk.base.b.f;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.common.e.c;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.d.a;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.lang.ref.WeakReference;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ShortCutsManager */
public class b {
    private static volatile b b;
    private WeakReference<Context> a;
    /* access modifiers changed from: private */
    public Handler c = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            if (message.what == 10000) {
                try {
                    int i = message.arg1;
                    Object obj = message.obj;
                    String str = null;
                    if (obj instanceof String) {
                        str = (String) obj;
                    }
                    b.a(b.this, i, str);
                } catch (Exception e) {
                    if (MIntegralConstans.DEBUG) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };

    private b(Context context) {
        this.a = new WeakReference<>(context);
    }

    public static b a(Context context) {
        if (b == null) {
            synchronized (b.class) {
                if (b == null) {
                    b = new b(context);
                }
            }
        }
        return b;
    }

    /* access modifiers changed from: private */
    public boolean b(String str) {
        Context context = this.a.get();
        if (context == null || context.getPackageManager().checkPermission(str, context.getPackageName()) != 0) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0072  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(final com.mintegral.msdk.base.entity.CampaignEx r6, final android.app.Activity r7) {
        /*
            r5 = this;
            java.lang.ref.WeakReference<android.content.Context> r0 = r5.a
            java.lang.Object r0 = r0.get()
            android.content.Context r0 = (android.content.Context) r0
            if (r0 != 0) goto L_0x000b
            return
        L_0x000b:
            if (r0 == 0) goto L_0x006b
            com.mintegral.msdk.base.b.i r1 = com.mintegral.msdk.base.b.i.a(r0)
            com.mintegral.msdk.base.b.f r1 = com.mintegral.msdk.base.b.f.a(r1)
            com.mintegral.msdk.d.b.a()
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()
            java.lang.String r2 = r2.j()
            com.mintegral.msdk.d.a r2 = com.mintegral.msdk.d.b.b(r2)
            if (r2 != 0) goto L_0x002d
            com.mintegral.msdk.d.b.a()
            com.mintegral.msdk.d.a r2 = com.mintegral.msdk.d.b.b()
        L_0x002d:
            java.lang.String r2 = r2.j()
            java.util.List r1 = r1.g(r2)
            if (r1 == 0) goto L_0x006b
            int r2 = r1.size()
            if (r2 <= 0) goto L_0x006b
            java.util.Iterator r1 = r1.iterator()
        L_0x0041:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x006b
            java.lang.Object r2 = r1.next()
            com.mintegral.msdk.base.entity.CampaignEx r2 = (com.mintegral.msdk.base.entity.CampaignEx) r2
            java.lang.String r3 = r2.getId()
            java.lang.String r4 = r6.getId()
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0041
            java.lang.String r2 = r2.getAppName()
            java.lang.String r3 = r6.getAppName()
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0041
            r1 = 1
            goto L_0x006c
        L_0x006b:
            r1 = 0
        L_0x006c:
            if (r1 == 0) goto L_0x0072
            b(r7)
            return
        L_0x0072:
            java.lang.String r1 = r6.getIconUrl()
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x008d
            com.mintegral.msdk.base.common.c.b r1 = com.mintegral.msdk.base.common.c.b.a(r0)
            java.lang.String r2 = r6.getIconUrl()
            com.mintegral.msdk.e.b$1 r3 = new com.mintegral.msdk.e.b$1
            r3.<init>(r7, r0, r6)
            r1.a(r2, r3)
            return
        L_0x008d:
            b(r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.e.b.a(com.mintegral.msdk.base.entity.CampaignEx, android.app.Activity):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void a(Context context, CampaignEx campaignEx, int i) {
        Intent intent = new Intent("com.android.launcher.action.UNINSTALL_SHORTCUT");
        intent.putExtra("android.intent.extra.shortcut.NAME", campaignEx.getAppName());
        Intent intent2 = new Intent(context, MTGCommonActivity.class);
        intent2.setAction("android.intent.action.VIEW");
        intent.putExtra("android.intent.extra.shortcut.INTENT", intent2);
        context.sendBroadcast(intent);
        f a2 = f.a(i.a(context));
        com.mintegral.msdk.d.b.a();
        a b2 = com.mintegral.msdk.d.b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b2 == null) {
            com.mintegral.msdk.d.b.a();
            b2 = com.mintegral.msdk.d.b.b();
        }
        String j = b2.j();
        if (b("com.android.launcher.permission.READ_SETTINGS") || b("com.google.android.launcher.permission.READ_SETTINGS")) {
            if (c(campaignEx.getAppName()) < i) {
                a(campaignEx, 2, 1);
                campaignEx.setIsDeleted(1);
                ContentValues contentValues = new ContentValues();
                contentValues.put("is_deleted", (Integer) 1);
                a2.a(campaignEx.getId(), contentValues);
            } else {
                a(campaignEx, 2, 0);
            }
        } else {
            a(campaignEx, 2, -1);
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put("is_deleted", (Integer) 1);
            a2.a(campaignEx.getId(), contentValues2);
        }
        a2.c(j);
    }

    public final int a() {
        Context context = this.a.get();
        if (context == null) {
            return 0;
        }
        f a2 = f.a(i.a(context));
        com.mintegral.msdk.d.b.a();
        a b2 = com.mintegral.msdk.d.b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b2 == null) {
            com.mintegral.msdk.d.b.a();
            b2 = com.mintegral.msdk.d.b.b();
        }
        List<CampaignEx> g = a2.g(b2.j());
        if (g == null || g.size() <= 0) {
            return 0;
        }
        return g.size();
    }

    private boolean a(Context context, String str) {
        boolean z = false;
        if (context == null || TextUtils.isEmpty(str)) {
            return false;
        }
        String str2 = null;
        if (TextUtils.isEmpty(null)) {
            str2 = b(context);
        }
        if (!TextUtils.isEmpty(str2)) {
            try {
                Cursor a2 = a(str2, str, context);
                if (a2 != null && a2.getCount() > 0) {
                    z = true;
                }
                if (a2 != null && !a2.isClosed()) {
                    a2.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return z;
    }

    private static Cursor a(String str, String str2, Context context) {
        return context.getContentResolver().query(Uri.parse(str), new String[]{"title"}, "title=?", new String[]{str2}, null);
    }

    /* access modifiers changed from: private */
    public int c(String str) {
        Context context = this.a.get();
        if (context == null || (!b("com.android.launcher.permission.READ_SETTINGS") && !b("com.google.android.launcher.permission.READ_SETTINGS"))) {
            return 0;
        }
        String b2 = b(context);
        if (!TextUtils.isEmpty(b2)) {
            try {
                Cursor a2 = a(b2, str, context);
                if (a2 != null && a2.getCount() > 0) {
                    return a2.getCount();
                }
                if (a2 != null && !a2.isClosed()) {
                    a2.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public final String b() {
        ResolveInfo resolveActivity;
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        Context context = this.a.get();
        if (context == null || (resolveActivity = context.getPackageManager().resolveActivity(intent, 0)) == null || resolveActivity.activityInfo == null || resolveActivity.activityInfo.packageName.equals("android")) {
            return "";
        }
        return resolveActivity.activityInfo.packageName;
    }

    private String b(Context context, String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        try {
            List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(8);
            if (installedPackages == null) {
                return "";
            }
            for (PackageInfo packageInfo : installedPackages) {
                ProviderInfo[] providerInfoArr = packageInfo.providers;
                if (providerInfoArr != null) {
                    for (ProviderInfo providerInfo : providerInfoArr) {
                        if (!b("com.android.launcher.permission.READ_SETTINGS")) {
                            str = "com.google.android.launcher.permission.READ_SETTINGS";
                        }
                        if ((str.equals(providerInfo.readPermission) || str.equals(providerInfo.writePermission)) && !TextUtils.isEmpty(providerInfo.authority) && providerInfo.authority.contains(".launcher.settings")) {
                            return providerInfo.authority;
                        }
                    }
                    continue;
                }
            }
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public final String c() {
        String str = b("com.android.launcher.permission.INSTALL_SHORTCUT") ? "1" : AppEventsConstants.EVENT_PARAM_VALUE_NO;
        String str2 = b("com.android.launcher.permission.UNINSTALL_SHORTCUT") ? "1" : AppEventsConstants.EVENT_PARAM_VALUE_NO;
        String str3 = (b("com.android.launcher.permission.READ_SETTINGS") || b("com.google.android.launcher.permission.READ_SETTINGS")) ? "1" : AppEventsConstants.EVENT_PARAM_VALUE_NO;
        return Constants.RequestParameters.LEFT_BRACKETS + str + "," + str2 + "," + str3 + Constants.RequestParameters.RIGHT_BRACKETS;
    }

    private void a(CampaignEx campaignEx, int i, int i2) {
        if (this.a.get() != null && campaignEx.getIsDeleted() != 1) {
            c.a(this.a.get(), campaignEx, i, i2);
        }
    }

    public final void a(a aVar) {
        f a2;
        List<CampaignEx> g;
        try {
            Context context = this.a.get();
            if (context != null && (g = (a2 = f.a(i.a(context))).g(aVar.j())) != null && g.size() > 0) {
                for (CampaignEx next : g) {
                    if (next.getIsAddSuccesful() == 0 && next.getIsDeleted() == 0) {
                        if (c(next.getAppName()) != 0) {
                            a(next, 1, 1);
                            com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.SHORTCUTS_CTIME, String.valueOf(System.currentTimeMillis()));
                        } else {
                            a(next, 1, 0);
                        }
                        next.setIsAddSuccesful(-1);
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("is_add_sucesful", Integer.valueOf(next.getIsAddSuccesful()));
                        a2.a(next.getId(), contentValues);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void a(final String str) {
        try {
            if (this.a.get() != null) {
                new Thread(new Runnable() {
                    final /* synthetic */ int b = NetworkConstants.UPLOAD_CONNECT_TIMEOUT;

                    public final void run() {
                        a aVar;
                        if (str != null) {
                            com.mintegral.msdk.d.b.a();
                            aVar = com.mintegral.msdk.d.b.b(str);
                        } else {
                            aVar = null;
                        }
                        if (aVar == null) {
                            com.mintegral.msdk.d.b.a();
                            aVar = com.mintegral.msdk.d.b.b();
                        }
                        b.this.a(aVar);
                        Message obtain = Message.obtain();
                        obtain.arg1 = this.b;
                        obtain.obj = str;
                        b.this.c.sendEmptyMessage(AbstractSpiCall.DEFAULT_TIMEOUT);
                    }
                }).start();
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    private static boolean b(a aVar) {
        if (aVar == null) {
            return false;
        }
        int f = aVar.f();
        long currentTimeMillis = System.currentTimeMillis();
        long j = 0;
        if (!com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.SHORTCUTS_CTIME).equals("")) {
            j = Long.parseLong(com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.SHORTCUTS_CTIME));
        }
        if ((currentTimeMillis - j) / 1000 > ((long) f)) {
            return true;
        }
        return false;
    }

    public final void b(CampaignEx campaignEx, final Activity activity) {
        if (Build.VERSION.SDK_INT < 26) {
            final Context context = this.a.get();
            if (context != null) {
                com.mintegral.msdk.d.b.a();
                final a b2 = com.mintegral.msdk.d.b.b(com.mintegral.msdk.base.controller.a.d().j());
                if (b2 == null) {
                    com.mintegral.msdk.d.b.a();
                    b2 = com.mintegral.msdk.d.b.b();
                }
                if (!b(b2)) {
                    a.a().a(context, campaignEx, 1);
                    b(activity);
                    return;
                } else if (b2.h() == 1) {
                    a.a().a(context, campaignEx, new com.mintegral.msdk.e.a.a() {
                        public final void a(List<CampaignEx> list) {
                            if (list == null || list.size() <= 0) {
                                b.b(activity);
                                return;
                            }
                            WeakReference weakReference = new WeakReference(activity);
                            if (weakReference.get() != null) {
                                b.this.a(list.get(0), (Activity) weakReference.get());
                            }
                        }

                        public final void a() {
                            b.b(activity);
                        }

                        public final void b() {
                            b.this.a(context, b2);
                            b.b(activity);
                        }
                    });
                    return;
                } else {
                    a.a().a(context, campaignEx, 1);
                }
            } else {
                return;
            }
        }
        b(activity);
    }

    /* access modifiers changed from: private */
    public static void b(Activity activity) {
        if (activity != null) {
            activity.finish();
        }
    }

    /* access modifiers changed from: private */
    public void a(Context context, a aVar) {
        List<CampaignEx> g;
        if (!TextUtils.isEmpty(aVar.j()) && (g = f.a(i.a(context)).g(aVar.j())) != null && g.size() > 0 && b("com.android.launcher.permission.UNINSTALL_SHORTCUT")) {
            for (CampaignEx next : g) {
                int c2 = c(next.getAppName());
                Context context2 = this.a.get();
                if (context2 != null) {
                    if (b("com.android.launcher.permission.READ_SETTINGS") || b("com.google.android.launcher.permission.READ_SETTINGS")) {
                        if (a(context2, next.getAppName())) {
                            a(context2, next, c2);
                        }
                    } else if (b("com.android.launcher.permission.UNINSTALL_SHORTCUT")) {
                        a(context2, next, c2);
                    }
                }
            }
        }
    }

    private String b(Context context) {
        String b2 = b(context, "com.android.launcher.permission.READ_SETTINGS");
        if (b2 == null || b2.trim().equals("")) {
            String b3 = b();
            b2 = b(context, b3 + ".permission.READ_SETTINGS");
        }
        if (TextUtils.isEmpty(b2)) {
            int i = Build.VERSION.SDK_INT;
            b2 = i < 8 ? "com.android.launcher.settings" : i < 19 ? "com.android.launcher2.settings" : "com.android.launcher3.settings";
        }
        return "content://" + b2 + "/favorites?notify=true";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    static /* synthetic */ void a(Context context, Bitmap bitmap, CampaignEx campaignEx, Activity activity) {
        JSONObject jSONObject;
        if (!TextUtils.isEmpty(campaignEx.getAppName()) && bitmap != null) {
            try {
                Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
                intent.putExtra("android.intent.extra.shortcut.NAME", campaignEx.getAppName());
                intent.putExtra("duplicate", false);
                intent.putExtra("android.intent.extra.shortcut.ICON", Bitmap.createScaledBitmap(bitmap, IronSourceConstants.USING_CACHE_FOR_INIT_EVENT, IronSourceConstants.USING_CACHE_FOR_INIT_EVENT, true));
                Intent intent2 = new Intent(context, MTGCommonActivity.class);
                intent2.putExtra("intent_flag", "shortcuts");
                try {
                    jSONObject = CampaignEx.campaignToJsonObject(campaignEx);
                } catch (JSONException e) {
                    e.printStackTrace();
                    jSONObject = null;
                }
                if (jSONObject != null) {
                    intent2.putExtra("intent_jsonobject", jSONObject.toString());
                }
                intent2.setFlags(32768);
                intent.putExtra("android.intent.extra.shortcut.INTENT", intent2);
                context.sendBroadcast(intent);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        b(activity);
    }

    static /* synthetic */ void a(b bVar, CampaignEx campaignEx, a aVar, f fVar, int i) {
        campaignEx.setTimestamp(0);
        if (bVar.b("com.android.launcher.permission.READ_SETTINGS") || bVar.b("com.google.android.launcher.permission.READ_SETTINGS")) {
            if (bVar.c(campaignEx.getAppName()) > i) {
                campaignEx.setIsAddSuccesful(1);
                fVar.a(campaignEx, aVar.j(), 0);
                bVar.a(campaignEx, 1, 1);
                campaignEx.setIsAddSuccesful(-1);
                com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.SHORTCUTS_CTIME, String.valueOf(System.currentTimeMillis()));
                return;
            }
            campaignEx.setIsAddSuccesful(0);
            fVar.a(campaignEx, aVar.j(), 0);
            com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.SHORTCUTS_CTIME, String.valueOf(System.currentTimeMillis()));
            return;
        }
        campaignEx.setIsAddSuccesful(1);
        bVar.a(campaignEx, 1, -1);
        campaignEx.setIsAddSuccesful(-1);
        fVar.a(campaignEx, aVar.j(), 0);
        com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.SHORTCUTS_CTIME, String.valueOf(System.currentTimeMillis()));
    }

    static /* synthetic */ void a(b bVar, int i, String str) {
        try {
            final Context context = bVar.a.get();
            if (context != null) {
                a aVar = null;
                if (str != null) {
                    com.mintegral.msdk.d.b.a();
                    aVar = com.mintegral.msdk.d.b.b(str);
                }
                if (aVar == null) {
                    com.mintegral.msdk.d.b.a();
                    aVar = com.mintegral.msdk.d.b.b();
                }
                if (b(aVar)) {
                    new Handler().postDelayed(new Runnable() {
                        public final void run() {
                            com.mintegral.msdk.d.b.a();
                            final a b2 = com.mintegral.msdk.d.b.b(com.mintegral.msdk.base.controller.a.d().j());
                            if (b2 == null) {
                                com.mintegral.msdk.d.b.a();
                                b2 = com.mintegral.msdk.d.b.b();
                            }
                            if (b2.h() == 1) {
                                a.a().a(context, a.c, new com.mintegral.msdk.e.a.a() {
                                    public final void a() {
                                    }

                                    public final void a(List<CampaignEx> list) {
                                        if (list != null && list.size() > 0) {
                                            b.this.a(list.get(0), (Activity) null);
                                        }
                                    }

                                    public final void b() {
                                        b.this.a(context, b2);
                                    }
                                });
                            }
                        }
                    }, (long) i);
                }
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }
}
