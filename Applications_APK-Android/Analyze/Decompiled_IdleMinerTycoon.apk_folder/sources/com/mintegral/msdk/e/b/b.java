package com.mintegral.msdk.e.b;

import android.support.v4.app.NotificationCompat;
import com.mintegral.msdk.base.common.net.a.a;
import com.mintegral.msdk.base.entity.CampaignUnit;
import org.json.JSONObject;

/* compiled from: ShortCutsResponseHandler */
public abstract class b extends a {
    public abstract void a(CampaignUnit campaignUnit);

    public abstract void b(CampaignUnit campaignUnit);

    public final /* synthetic */ void a(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        if (1 == jSONObject.optInt("status")) {
            a(System.currentTimeMillis());
            CampaignUnit parseCampaignUnit = CampaignUnit.parseCampaignUnit(jSONObject.optJSONObject("data"));
            if (parseCampaignUnit == null || parseCampaignUnit.getAds() == null || parseCampaignUnit.getAds().size() <= 0) {
                jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE);
                b(parseCampaignUnit);
                return;
            }
            a(parseCampaignUnit);
            a(parseCampaignUnit.getAds().size());
            return;
        }
        jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE);
        b((CampaignUnit) null);
    }

    public final void a(String str) {
        b((CampaignUnit) null);
    }
}
