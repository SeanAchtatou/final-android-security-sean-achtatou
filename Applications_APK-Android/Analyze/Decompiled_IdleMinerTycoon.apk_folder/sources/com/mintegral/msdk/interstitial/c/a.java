package com.mintegral.msdk.interstitial.c;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.google.android.gms.drive.DriveFile;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.d.d;
import com.mintegral.msdk.interstitial.view.MTGInterstitialActivity;
import com.mintegral.msdk.out.InterstitialListener;
import java.util.HashMap;
import java.util.Map;

/* compiled from: InterstitialController */
public final class a {
    public static String a;
    public static Map<String, Integer> b = new HashMap();
    public static Map<String, Integer> c = new HashMap();
    public static Map<String, c> d = new HashMap();
    /* access modifiers changed from: private */
    public String e = "InterstitialController";
    private Context f;
    private String g;
    private String h;
    /* access modifiers changed from: private */
    public Handler i;
    private d j;
    /* access modifiers changed from: private */
    public InterstitialListener k;
    private boolean l = false;

    public static void a(String str, int i2) {
        try {
            if (b != null && !TextUtils.isEmpty(str)) {
                b.put(str, Integer.valueOf(i2));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static int a(String str) {
        Integer num;
        try {
            if (TextUtils.isEmpty(str) || b == null || !b.containsKey(str) || (num = b.get(str)) == null) {
                return 0;
            }
            return num.intValue();
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public final void a(InterstitialListener interstitialListener) {
        this.k = interstitialListener;
    }

    public a() {
        try {
            this.i = new Handler(Looper.getMainLooper()) {
                /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v12, resolved type: java.lang.Object} */
                /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: java.lang.String} */
                /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v24, resolved type: java.lang.Object} */
                /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v19, resolved type: java.lang.String} */
                /* JADX WARNING: Multi-variable type inference failed */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final void handleMessage(android.os.Message r4) {
                    /*
                        r3 = this;
                        if (r4 != 0) goto L_0x0003
                        return
                    L_0x0003:
                        int r0 = r4.what
                        switch(r0) {
                            case 1: goto L_0x00d0;
                            case 2: goto L_0x008e;
                            case 3: goto L_0x0071;
                            case 4: goto L_0x002f;
                            case 5: goto L_0x0008;
                            case 6: goto L_0x001d;
                            case 7: goto L_0x000a;
                            default: goto L_0x0008;
                        }
                    L_0x0008:
                        goto L_0x00ed
                    L_0x000a:
                        com.mintegral.msdk.interstitial.c.a r4 = com.mintegral.msdk.interstitial.c.a.this
                        com.mintegral.msdk.out.InterstitialListener r4 = r4.k
                        if (r4 == 0) goto L_0x00ed
                        com.mintegral.msdk.interstitial.c.a r4 = com.mintegral.msdk.interstitial.c.a.this
                        com.mintegral.msdk.out.InterstitialListener r4 = r4.k
                        r4.onInterstitialClosed()
                        goto L_0x00ed
                    L_0x001d:
                        com.mintegral.msdk.interstitial.c.a r4 = com.mintegral.msdk.interstitial.c.a.this
                        com.mintegral.msdk.out.InterstitialListener r4 = r4.k
                        if (r4 == 0) goto L_0x00ed
                        com.mintegral.msdk.interstitial.c.a r4 = com.mintegral.msdk.interstitial.c.a.this
                        com.mintegral.msdk.out.InterstitialListener r4 = r4.k
                        r4.onInterstitialAdClick()
                        return
                    L_0x002f:
                        com.mintegral.msdk.interstitial.c.a r0 = com.mintegral.msdk.interstitial.c.a.this
                        com.mintegral.msdk.out.InterstitialListener r0 = r0.k
                        if (r0 == 0) goto L_0x00ed
                        java.lang.String r0 = ""
                        java.lang.Object r1 = r4.obj
                        if (r1 == 0) goto L_0x0048
                        java.lang.Object r1 = r4.obj
                        boolean r1 = r1 instanceof java.lang.String
                        if (r1 == 0) goto L_0x0048
                        java.lang.Object r4 = r4.obj
                        r0 = r4
                        java.lang.String r0 = (java.lang.String) r0
                    L_0x0048:
                        boolean r4 = android.text.TextUtils.isEmpty(r0)
                        if (r4 == 0) goto L_0x0050
                        java.lang.String r0 = "can't show because unknow error"
                    L_0x0050:
                        com.mintegral.msdk.interstitial.c.a r4 = com.mintegral.msdk.interstitial.c.a.this
                        com.mintegral.msdk.out.InterstitialListener r4 = r4.k
                        r4.onInterstitialShowFail(r0)
                        com.mintegral.msdk.interstitial.c.a r4 = com.mintegral.msdk.interstitial.c.a.this
                        java.lang.String r4 = r4.e
                        java.lang.StringBuilder r1 = new java.lang.StringBuilder
                        java.lang.String r2 = "handler 数据show失败:"
                        r1.<init>(r2)
                        r1.append(r0)
                        java.lang.String r0 = r1.toString()
                        com.mintegral.msdk.base.utils.g.b(r4, r0)
                        return
                    L_0x0071:
                        com.mintegral.msdk.interstitial.c.a r4 = com.mintegral.msdk.interstitial.c.a.this
                        com.mintegral.msdk.out.InterstitialListener r4 = r4.k
                        if (r4 == 0) goto L_0x00ed
                        com.mintegral.msdk.interstitial.c.a r4 = com.mintegral.msdk.interstitial.c.a.this
                        com.mintegral.msdk.out.InterstitialListener r4 = r4.k
                        r4.onInterstitialShowSuccess()
                        com.mintegral.msdk.interstitial.c.a r4 = com.mintegral.msdk.interstitial.c.a.this
                        java.lang.String r4 = r4.e
                        java.lang.String r0 = "handler 数据show成功"
                        com.mintegral.msdk.base.utils.g.b(r4, r0)
                        return
                    L_0x008e:
                        com.mintegral.msdk.interstitial.c.a r0 = com.mintegral.msdk.interstitial.c.a.this
                        com.mintegral.msdk.out.InterstitialListener r0 = r0.k
                        if (r0 == 0) goto L_0x00ed
                        java.lang.String r0 = ""
                        java.lang.Object r1 = r4.obj
                        if (r1 == 0) goto L_0x00a7
                        java.lang.Object r1 = r4.obj
                        boolean r1 = r1 instanceof java.lang.String
                        if (r1 == 0) goto L_0x00a7
                        java.lang.Object r4 = r4.obj
                        r0 = r4
                        java.lang.String r0 = (java.lang.String) r0
                    L_0x00a7:
                        boolean r4 = android.text.TextUtils.isEmpty(r0)
                        if (r4 == 0) goto L_0x00af
                        java.lang.String r0 = "can't show because unknow error"
                    L_0x00af:
                        com.mintegral.msdk.interstitial.c.a r4 = com.mintegral.msdk.interstitial.c.a.this
                        com.mintegral.msdk.out.InterstitialListener r4 = r4.k
                        r4.onInterstitialLoadFail(r0)
                        com.mintegral.msdk.interstitial.c.a r4 = com.mintegral.msdk.interstitial.c.a.this
                        java.lang.String r4 = r4.e
                        java.lang.StringBuilder r1 = new java.lang.StringBuilder
                        java.lang.String r2 = "handler 数据load失败:"
                        r1.<init>(r2)
                        r1.append(r0)
                        java.lang.String r0 = r1.toString()
                        com.mintegral.msdk.base.utils.g.b(r4, r0)
                        return
                    L_0x00d0:
                        com.mintegral.msdk.interstitial.c.a r4 = com.mintegral.msdk.interstitial.c.a.this
                        com.mintegral.msdk.out.InterstitialListener r4 = r4.k
                        if (r4 == 0) goto L_0x00ed
                        com.mintegral.msdk.interstitial.c.a r4 = com.mintegral.msdk.interstitial.c.a.this
                        com.mintegral.msdk.out.InterstitialListener r4 = r4.k
                        r4.onInterstitialLoadSuccess()
                        com.mintegral.msdk.interstitial.c.a r4 = com.mintegral.msdk.interstitial.c.a.this
                        java.lang.String r4 = r4.e
                        java.lang.String r0 = "handler 数据load成功"
                        com.mintegral.msdk.base.utils.g.b(r4, r0)
                        return
                    L_0x00ed:
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.interstitial.c.a.AnonymousClass1.handleMessage(android.os.Message):void");
                }
            };
        } catch (Exception e2) {
            try {
                e2.printStackTrace();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    public final boolean a(Context context, Map<String, Object> map) {
        try {
            this.l = false;
            if (map == null) {
                g.c(this.e, "init error params==null");
                return false;
            } else if (context == null) {
                g.c(this.e, "init context ==null");
                return false;
            } else {
                if (map.containsKey(MIntegralConstans.PROPERTIES_UNIT_ID)) {
                    if (map.get(MIntegralConstans.PROPERTIES_UNIT_ID) instanceof String) {
                        if (map.containsKey(MIntegralConstans.PROPERTIES_API_REUQEST_CATEGORY) && (map.get(MIntegralConstans.PROPERTIES_API_REUQEST_CATEGORY) instanceof String)) {
                            this.h = (String) map.get(MIntegralConstans.PROPERTIES_API_REUQEST_CATEGORY);
                        }
                        this.g = (String) map.get(MIntegralConstans.PROPERTIES_UNIT_ID);
                        this.f = context;
                        this.l = true;
                        return this.l;
                    }
                }
                g.c(this.e, "init error,make sure you have unitid");
                return false;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            this.l = false;
        }
    }

    public final void a() {
        try {
            if (this.f == null) {
                b("context is null");
                g.b(this.e, "load context is null");
            } else if (TextUtils.isEmpty(this.g)) {
                b("unitid is null");
                g.b(this.e, "load unitid is null");
            } else if (!this.l) {
                b("init error");
                g.b(this.e, "load init error");
            } else {
                c();
                try {
                    if (this.j != null) {
                        int t = this.j.t();
                        int z = this.j.z();
                        if (t <= 0) {
                            t = 1;
                        }
                        if (z <= 0) {
                            z = 1;
                        }
                        int i2 = z * t;
                        if (c != null && !TextUtils.isEmpty(this.g)) {
                            c.put(this.g, Integer.valueOf(i2));
                        }
                        String str = this.e;
                        g.b(str, "maxOffset:" + i2 + " apiCacheNum:" + t + " mUnitId:" + this.g);
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                a(false);
            }
        } catch (Exception e3) {
            e3.printStackTrace();
            b("can't show because unknow error");
        }
    }

    public final void b() {
        try {
            if (this.f == null) {
                c("context is null");
                g.b(this.e, "show context is null");
            } else if (TextUtils.isEmpty(this.g)) {
                c("unitid is null");
                g.b(this.e, "show unitid is null");
            } else if (!this.l) {
                c("init error");
                g.b(this.e, "show init error");
            } else {
                c();
                b(true);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            c("can't show because unknow error");
        }
    }

    private void c() {
        try {
            g.b(this.e, "initUnitSetting");
            try {
                new com.mintegral.msdk.d.c().a(this.f, null, null, this.g);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            com.mintegral.msdk.d.b.a();
            this.j = com.mintegral.msdk.d.b.c(com.mintegral.msdk.base.controller.a.d().j(), this.g);
            if (this.j == null) {
                this.j = d.e(this.g);
                g.b(this.e, "获取默认的unitsetting");
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private void a(boolean z) {
        try {
            com.mintegral.msdk.interstitial.a.a aVar = new com.mintegral.msdk.interstitial.a.a(this.f, this.g, this.h, z);
            b bVar = new b(aVar);
            aVar.a(new C0061a(aVar, bVar));
            if (this.i != null) {
                this.i.postDelayed(bVar, 30000);
            }
            aVar.b();
        } catch (Exception e2) {
            e2.printStackTrace();
            if (!z) {
                b("can't show because unknow error");
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        try {
            String str = this.e;
            g.b(str, "showInterstitial isShowCall:" + z);
            CampaignEx a2 = new com.mintegral.msdk.interstitial.a.a(this.f, this.g, this.h, true).a();
            if (a2 != null) {
                c cVar = new c();
                if (d != null && !TextUtils.isEmpty(this.g)) {
                    d.put(this.g, cVar);
                }
                Intent intent = new Intent(this.f, MTGInterstitialActivity.class);
                intent.addFlags(67108864);
                intent.addFlags(DriveFile.MODE_READ_ONLY);
                if (!TextUtils.isEmpty(this.g)) {
                    intent.putExtra("unitId", this.g);
                }
                if (a2 != null) {
                    intent.putExtra("campaign", a2);
                }
                if (this.f != null) {
                    this.f.startActivity(intent);
                }
            } else if (z) {
                g.d(this.e, "showInterstitial 发现cmapaign为空 去load一遍=========");
                a(true);
            } else {
                c("no ads available can show");
                g.b(this.e, "showInterstitial 发现cmapaign为空");
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (this.k != null) {
                c("can't show because unknow error");
            }
        }
    }

    /* compiled from: InterstitialController */
    public class b implements Runnable {
        private com.mintegral.msdk.interstitial.a.a b;

        public b(com.mintegral.msdk.interstitial.a.a aVar) {
            this.b = aVar;
        }

        public final void run() {
            try {
                g.d(a.this.e, "CommonCancelTimeTask");
                if (this.b != null) {
                    if (this.b.d()) {
                        a.this.c("load timeout");
                    } else if (a.this.k != null) {
                        a.this.b("load timeout");
                    }
                    this.b.a((C0061a) null);
                    this.b = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: com.mintegral.msdk.interstitial.c.a$a  reason: collision with other inner class name */
    /* compiled from: InterstitialController */
    public class C0061a {
        private com.mintegral.msdk.interstitial.a.a b;
        private b c;

        public C0061a(com.mintegral.msdk.interstitial.a.a aVar, b bVar) {
            this.b = aVar;
            this.c = bVar;
        }

        public final void a(boolean z) {
            try {
                if (this.c != null) {
                    if (a.this.i != null) {
                        a.this.i.removeCallbacks(this.c);
                    }
                    if (z) {
                        a.this.b(false);
                    } else if (a.this.k != null) {
                        a.e(a.this);
                    }
                    g.d(a.this.e, "onInterstitialLoadSuccess remove task ");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public final void a(boolean z, String str) {
            try {
                if (this.b != null) {
                    this.b.a((C0061a) null);
                    this.b = null;
                }
                if (this.c != null) {
                    g.d(a.this.e, "LoadIntersInnerListener onIntersLoadFail remove task");
                    if (a.this.i != null) {
                        a.this.i.removeCallbacks(this.c);
                    }
                    if (z) {
                        if (a.this.k != null) {
                            a.this.c(str);
                        }
                    } else if (a.this.k != null) {
                        a.this.b(str);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* compiled from: InterstitialController */
    public class c {
        public c() {
        }

        public final void a(String str) {
            try {
                a.this.c(str);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public final void a() {
            try {
                a.f(a.this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public final void b() {
            try {
                if (a.this.i != null) {
                    a.this.i.sendEmptyMessage(7);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public final void c() {
            try {
                if (a.this.i != null) {
                    a.this.i.sendEmptyMessage(6);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        try {
            if (this.i != null) {
                Message obtain = Message.obtain();
                obtain.obj = str;
                obtain.what = 2;
                this.i.sendMessage(obtain);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        try {
            if (this.i != null) {
                Message obtain = Message.obtain();
                obtain.obj = str;
                obtain.what = 4;
                this.i.sendMessage(obtain);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void e(a aVar) {
        if (aVar.i != null) {
            aVar.i.sendEmptyMessage(1);
        }
    }

    static /* synthetic */ void f(a aVar) {
        if (aVar.i != null) {
            aVar.i.sendEmptyMessage(3);
        }
    }
}
