package com.mintegral.msdk.interstitial.a;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.common.a.c;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.mintegral.msdk.base.entity.h;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.n;
import com.mintegral.msdk.d.d;
import com.mintegral.msdk.interstitial.c.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;

/* compiled from: IntersAdapter */
public final class a {
    /* access modifiers changed from: private */
    public Context a;
    /* access modifiers changed from: private */
    public String b;
    private int c;
    /* access modifiers changed from: private */
    public boolean d;
    private int e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public String g;
    private String h;
    /* access modifiers changed from: private */
    public a.C0061a i;
    private d j;
    /* access modifiers changed from: private */
    public Handler k;
    /* access modifiers changed from: private */
    public b l;
    /* access modifiers changed from: private */
    public boolean m = false;
    /* access modifiers changed from: private */
    public boolean n = false;

    public a(Context context, String str, String str2, boolean z) {
        this.a = context;
        this.b = str;
        this.h = str2;
        this.d = z;
        String j2 = com.mintegral.msdk.base.controller.a.d().j();
        com.mintegral.msdk.d.b.a();
        this.j = com.mintegral.msdk.d.b.c(j2, str);
        if (this.j == null) {
            g.b("IntersAdapter", "获取默认的unitsetting");
            this.j = d.e(this.b);
        }
        this.k = new Handler(Looper.getMainLooper()) {
            public final void handleMessage(Message message) {
                try {
                    switch (message.what) {
                        case 1:
                            g.b("IntersAdapter", "handler id获取成功 开始load mTtcIds:" + a.this.f + "  mExcludes:" + a.this.g);
                            a.this.c();
                            return;
                        case 2:
                            g.b("IntersAdapter", "handler id获取超时  开始load mTtcIds:" + a.this.f + "  mExcludes:" + a.this.g);
                            a.this.c();
                            return;
                        case 3:
                            if (a.this.i != null) {
                                g.b("IntersAdapter", "handler 数据load成功");
                                a.this.i.a(a.this.d);
                                return;
                            }
                            return;
                        case 4:
                            if (a.this.i != null) {
                                g.b("IntersAdapter", "handler 数据load失败");
                                if (message.obj != null && (message.obj instanceof String)) {
                                    a.this.i.a(a.this.d, (String) message.obj);
                                    return;
                                }
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public final CampaignEx a() {
        try {
            if (TextUtils.isEmpty(this.b)) {
                return null;
            }
            h();
            List<CampaignEx> i2 = i();
            if (i2 != null) {
                if (i2.size() > 0) {
                    int i3 = 0;
                    while (i3 < i2.size()) {
                        CampaignEx campaignEx = i2.get(i3);
                        g.a("IntersAdapter", "html url:" + campaignEx.getHtmlUrl());
                        if (campaignEx == null || (TextUtils.isEmpty(campaignEx.getHtmlUrl()) && TextUtils.isEmpty(campaignEx.getMraid()))) {
                            i3++;
                        } else {
                            g.b("IntersAdapter", "adapter htmlurl:" + campaignEx.getHtmlUrl() + " id:" + campaignEx.getId());
                            return campaignEx;
                        }
                    }
                    return null;
                }
            }
            g.b("IntersAdapter", "adapter allCamp is null");
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            g.d("IntersAdapter", "==getIntersAvaCampaign 获取campaign 出错");
        }
    }

    public final void b() {
        if (this.a == null) {
            a("context is null");
        } else if (TextUtils.isEmpty(this.b)) {
            a("unitid is null");
        } else if (this.j == null) {
            a("unitSetting is null please call load");
        } else {
            int s = this.j.s();
            if (s <= 0) {
                g.b("IntersAdapter", "aqn为-1和0 不请求 直接返回失败 apiRepNum：" + s);
                a("controller don't request ad");
                return;
            }
            g.b("IntersAdapter", "load 开始清除过期数据");
            h();
            List<CampaignEx> i2 = i();
            if (i2 == null || i2.size() <= 0) {
                new Thread(new C0060a()).start();
                if (this.k != null) {
                    this.l = new b();
                    this.k.postDelayed(this.l, 90000);
                    return;
                }
                g.b("IntersAdapter", "handler 为空 直接load");
                c();
                return;
            }
            g.b("IntersAdapter", "load 本地已有缓存数量：" + i2.size());
            f();
        }
    }

    public final void c() {
        try {
            if (this.a == null) {
                a("context is null");
            } else if (TextUtils.isEmpty(this.b)) {
                a("unitid is null");
            } else if (this.j == null) {
                a("unitSetting is null please call load");
            } else {
                g.b("IntersAdapter", "load 开始准备请求参数");
                String j2 = com.mintegral.msdk.base.controller.a.d().j();
                String md5 = CommonMD5.getMD5(com.mintegral.msdk.base.controller.a.d().j() + com.mintegral.msdk.base.controller.a.d().k());
                int i2 = this.d ? 3 : 2;
                int i3 = 1;
                this.e = 1;
                if (this.j.t() > 0) {
                    this.e = this.j.t();
                }
                if (this.j.s() > 0) {
                    i3 = this.j.s();
                }
                String str = this.g;
                String str2 = this.f;
                String a2 = c.a(this.b, "interstitial");
                String n2 = n();
                this.c = k();
                String m2 = m();
                if (TextUtils.isEmpty(this.h)) {
                    this.h = AppEventsConstants.EVENT_PARAM_VALUE_NO;
                }
                l lVar = new l();
                n.a(lVar, "app_id", j2);
                n.a(lVar, MIntegralConstans.PROPERTIES_UNIT_ID, this.b);
                n.a(lVar, "sign", md5);
                n.a(lVar, "category", this.h);
                n.a(lVar, "req_type", String.valueOf(i2));
                n.a(lVar, "ad_num", String.valueOf(i3));
                StringBuilder sb = new StringBuilder();
                sb.append(this.e);
                n.a(lVar, "tnum", sb.toString());
                n.a(lVar, "only_impression", "1");
                n.a(lVar, "ping_mode", "1");
                n.a(lVar, "ttc_ids", str2);
                n.a(lVar, "display_info", a2);
                n.a(lVar, "exclude_ids", str);
                n.a(lVar, "install_ids", n2);
                n.a(lVar, CampaignEx.JSON_KEY_AD_SOURCE_ID, "1");
                n.a(lVar, "session_id", m2);
                n.a(lVar, "ad_type", "279");
                StringBuilder sb2 = new StringBuilder();
                sb2.append(this.c);
                n.a(lVar, "offset", sb2.toString());
                com.mintegral.msdk.interstitial.d.a aVar = new com.mintegral.msdk.interstitial.d.a(this.a);
                AnonymousClass2 r1 = new com.mintegral.msdk.interstitial.d.b() {
                    public final void a(CampaignUnit campaignUnit) {
                        try {
                            g.b("IntersAdapter", "onLoadCompaginSuccess 数据刚请求回来");
                            a.a(a.this, campaignUnit);
                        } catch (Exception e) {
                            e.printStackTrace();
                            g.b("IntersAdapter", "onLoadCompaginSuccess 数据刚请求失败");
                            a.this.a("can't show because unknow error");
                            a.this.l();
                        }
                    }

                    public final void a(int i, String str) {
                        g.d("IntersAdapter", str);
                        g.b("IntersAdapter", "onLoadCompaginFailed load失败 errorCode:" + i + " msg:" + str);
                        a.this.a(str);
                        a.this.l();
                    }
                };
                r1.d = this.b;
                aVar.a(com.mintegral.msdk.base.common.a.k, lVar, r1);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            a("can't show because unknow error");
            l();
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.k != null) {
            this.k.sendEmptyMessage(3);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        try {
            if (this.k != null) {
                Message obtain = Message.obtain();
                obtain.obj = str;
                obtain.what = 4;
                this.k.sendMessage(obtain);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public static String g() {
        String str = "";
        try {
            com.mintegral.msdk.d.b.a();
            com.mintegral.msdk.d.a b2 = com.mintegral.msdk.d.b.b(com.mintegral.msdk.base.controller.a.d().j());
            JSONArray jSONArray = new JSONArray();
            if (b2 != null && b2.aK() == 1) {
                g.b("IntersAdapter", "excludes cfc:" + b2.aK());
                long[] c2 = m.a(i.a(com.mintegral.msdk.base.controller.a.d().h())).c();
                if (c2 != null) {
                    for (long put : c2) {
                        g.b("IntersAdapter", "excludes campaignIds:" + c2);
                        jSONArray.put(put);
                    }
                }
            }
            if (jSONArray.length() > 0) {
                str = k.a(jSONArray);
            }
            g.b("IntersAdapter", "get excludes:" + str);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return str;
    }

    private void h() {
        try {
            if (com.mintegral.msdk.interstitial.b.a.a() != null) {
                com.mintegral.msdk.d.b.a();
                com.mintegral.msdk.d.a b2 = com.mintegral.msdk.d.b.b(com.mintegral.msdk.base.controller.a.d().j());
                if (b2 == null) {
                    com.mintegral.msdk.d.b.a();
                    b2 = com.mintegral.msdk.d.b.b();
                }
                com.mintegral.msdk.interstitial.b.a.a().a(b2.aF() * 1000, this.b);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private List<CampaignEx> i() {
        try {
            if (com.mintegral.msdk.interstitial.b.a.a() != null) {
                return com.mintegral.msdk.interstitial.b.a.a().a(this.b);
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public final void a(a.C0061a aVar) {
        this.i = aVar;
    }

    private List<CampaignEx> a(List<CampaignEx> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            try {
                if (list.size() > 0) {
                    g.b("IntersAdapter", "onload 总共返回 的compaign有：" + list.size());
                    int t = this.j.t();
                    int i2 = 0;
                    while (i2 < list.size() && i2 < this.e && arrayList.size() < t) {
                        CampaignEx campaignEx = list.get(i2);
                        if (campaignEx != null && campaignEx.getOfferType() == 1 && TextUtils.isEmpty(campaignEx.getVideoUrlEncode())) {
                            g.b("IntersAdapter", "offertype=1 但是videourl为空");
                        } else if (campaignEx != null && ((!TextUtils.isEmpty(campaignEx.getHtmlUrl()) || campaignEx.isMraid()) && campaignEx.getOfferType() != 99)) {
                            if (campaignEx.getWtick() != 1) {
                                if (k.a(this.a, campaignEx.getPackageName())) {
                                    if (k.b(campaignEx) || k.a(campaignEx)) {
                                        arrayList.add(campaignEx);
                                    }
                                }
                            }
                            arrayList.add(campaignEx);
                        }
                        i2++;
                    }
                    g.b("IntersAdapter", "onload 返回有以下有效的compaign：" + arrayList.size());
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00b1 A[SYNTHETIC, Splitter:B:25:0x00b1] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00de A[SYNTHETIC, Splitter:B:38:0x00de] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.mintegral.msdk.base.entity.CampaignEx> b(java.util.List<com.mintegral.msdk.base.entity.CampaignEx> r8) {
        /*
            r7 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            int r1 = r8.size()
            r0.<init>(r1)
            java.util.Iterator r8 = r8.iterator()
        L_0x000d:
            boolean r1 = r8.hasNext()
            if (r1 == 0) goto L_0x00ec
            java.lang.Object r1 = r8.next()
            com.mintegral.msdk.base.entity.CampaignEx r1 = (com.mintegral.msdk.base.entity.CampaignEx) r1
            boolean r2 = r1.isMraid()
            if (r2 == 0) goto L_0x00e7
            java.lang.String r2 = r1.getMraid()
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x00e7
            r2 = 0
            com.mintegral.msdk.base.common.b.c r3 = com.mintegral.msdk.base.common.b.c.MINTEGRAL_700_HTML     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            java.lang.String r3 = com.mintegral.msdk.base.common.b.e.b(r3)     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            java.lang.String r4 = r1.getMraid()     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            java.lang.String r4 = com.mintegral.msdk.base.utils.CommonMD5.getMD5(r4)     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            boolean r5 = android.text.TextUtils.isEmpty(r4)     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            if (r5 == 0) goto L_0x0046
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
        L_0x0046:
            java.lang.String r5 = ".html"
            java.lang.String r4 = r4.concat(r5)     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            r5.<init>(r3, r4)     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            r3.<init>(r5)     // Catch:{ Exception -> 0x0098, all -> 0x0095 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0093 }
            r2.<init>()     // Catch:{ Exception -> 0x0093 }
            java.lang.String r4 = "<script>"
            r2.append(r4)     // Catch:{ Exception -> 0x0093 }
            java.lang.String r4 = com.mintegral.msdk.base.utils.o.a     // Catch:{ Exception -> 0x0093 }
            r2.append(r4)     // Catch:{ Exception -> 0x0093 }
            java.lang.String r4 = "</script>"
            r2.append(r4)     // Catch:{ Exception -> 0x0093 }
            java.lang.String r4 = r1.getMraid()     // Catch:{ Exception -> 0x0093 }
            r2.append(r4)     // Catch:{ Exception -> 0x0093 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0093 }
            byte[] r2 = r2.getBytes()     // Catch:{ Exception -> 0x0093 }
            r3.write(r2)     // Catch:{ Exception -> 0x0093 }
            r3.flush()     // Catch:{ Exception -> 0x0093 }
            java.lang.String r2 = r5.getAbsolutePath()     // Catch:{ Exception -> 0x0093 }
            r1.setMraid(r2)     // Catch:{ Exception -> 0x0093 }
            java.lang.String r2 = ""
            java.lang.String r4 = r7.b     // Catch:{ Exception -> 0x0093 }
            java.lang.String r5 = "5"
            com.mintegral.msdk.base.common.e.a.a(r1, r2, r4, r5)     // Catch:{ Exception -> 0x0093 }
            r3.close()     // Catch:{ Exception -> 0x00b5 }
            goto L_0x00b9
        L_0x0093:
            r2 = move-exception
            goto L_0x009c
        L_0x0095:
            r8 = move-exception
            r3 = r2
            goto L_0x00dc
        L_0x0098:
            r3 = move-exception
            r6 = r3
            r3 = r2
            r2 = r6
        L_0x009c:
            r2.printStackTrace()     // Catch:{ all -> 0x00db }
            java.lang.String r4 = ""
            r1.setMraid(r4)     // Catch:{ all -> 0x00db }
            java.lang.String r2 = r2.getMessage()     // Catch:{ all -> 0x00db }
            java.lang.String r4 = r7.b     // Catch:{ all -> 0x00db }
            java.lang.String r5 = "5"
            com.mintegral.msdk.base.common.e.a.a(r1, r2, r4, r5)     // Catch:{ all -> 0x00db }
            if (r3 == 0) goto L_0x00b9
            r3.close()     // Catch:{ Exception -> 0x00b5 }
            goto L_0x00b9
        L_0x00b5:
            r2 = move-exception
            r2.printStackTrace()
        L_0x00b9:
            java.io.File r2 = new java.io.File
            java.lang.String r3 = r1.getMraid()
            r2.<init>(r3)
            boolean r3 = r2.exists()
            if (r3 == 0) goto L_0x00d4
            boolean r3 = r2.isFile()
            if (r3 == 0) goto L_0x00d4
            boolean r2 = r2.canRead()
            if (r2 != 0) goto L_0x00e7
        L_0x00d4:
            java.lang.String r1 = "mraid resource write fail"
            r7.a(r1)
            goto L_0x000d
        L_0x00db:
            r8 = move-exception
        L_0x00dc:
            if (r3 == 0) goto L_0x00e6
            r3.close()     // Catch:{ Exception -> 0x00e2 }
            goto L_0x00e6
        L_0x00e2:
            r0 = move-exception
            r0.printStackTrace()
        L_0x00e6:
            throw r8
        L_0x00e7:
            r0.add(r1)
            goto L_0x000d
        L_0x00ec:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.interstitial.a.a.b(java.util.List):java.util.List");
    }

    private int j() {
        try {
            Map<String, Integer> map = com.mintegral.msdk.interstitial.c.a.c;
            int intValue = (TextUtils.isEmpty(this.b) || map == null || !map.containsKey(this.b)) ? 1 : map.get(this.b).intValue();
            if (intValue <= 0) {
                return 1;
            }
            return intValue;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 1;
        }
    }

    private int k() {
        int i2 = 0;
        try {
            int a2 = !TextUtils.isEmpty(this.b) ? com.mintegral.msdk.interstitial.c.a.a(this.b) : 0;
            if (a2 <= j()) {
                i2 = a2;
            }
            g.b("IntersAdapter", "getCurrentOffset:" + i2);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return i2;
    }

    /* access modifiers changed from: private */
    public void l() {
        try {
            if (!TextUtils.isEmpty(this.b)) {
                com.mintegral.msdk.interstitial.c.a.a(this.b, 0);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private static String m() {
        try {
            if (!TextUtils.isEmpty(com.mintegral.msdk.interstitial.c.a.a)) {
                return com.mintegral.msdk.interstitial.c.a.a;
            }
            return "";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    private static String n() {
        try {
            JSONArray jSONArray = new JSONArray();
            com.mintegral.msdk.base.controller.a.d();
            List<Long> g2 = com.mintegral.msdk.base.controller.a.g();
            if (g2 != null && g2.size() > 0) {
                for (Long longValue : g2) {
                    jSONArray.put(longValue.longValue());
                }
            }
            if (jSONArray.length() > 0) {
                return k.a(jSONArray);
            }
            return "";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    /* renamed from: com.mintegral.msdk.interstitial.a.a$a  reason: collision with other inner class name */
    /* compiled from: IntersAdapter */
    public class C0060a implements Runnable {
        public C0060a() {
        }

        public final void run() {
            i a2;
            try {
                g.b("IntersAdapter", "=====getTtcRunnable 开始获取 mTtcIds:" + a.this.f + "  mExcludes:" + a.this.g);
                if (!(a.this.a == null || (a2 = i.a(a.this.a)) == null)) {
                    com.mintegral.msdk.base.b.d a3 = com.mintegral.msdk.base.b.d.a(a2);
                    a3.c();
                    String unused = a.this.f = a3.a(a.this.b);
                }
                String unused2 = a.this.g = a.g();
                g.b("IntersAdapter", "=====getTtcRunnable 获取完毕 mTtcIds:" + a.this.f + "  mExcludes:" + a.this.g);
                if (a.this.n) {
                    g.b("IntersAdapter", "=====getTtcRunnable 获取ttcid和excludeids超时 mIsGetTtcExcIdsTimeout：" + a.this.n + " mIsGetTtcExcIdsSuccess:" + a.this.m);
                    return;
                }
                g.b("IntersAdapter", "=====getTtcRunnable 获取ttcid和excludeids没有超时 mIsGetTtcExcIdsTimeout:" + a.this.n + " mIsGetTtcExcIdsSuccess:" + a.this.m);
                if (a.this.l != null) {
                    g.b("IntersAdapter", "=====getTtcRunnable 删除 获取ttcid的超时任务");
                    a.this.k.removeCallbacks(a.this.l);
                }
                boolean unused3 = a.this.m = true;
                g.b("IntersAdapter", "=====getTtcRunnable 给handler发送消息 mTtcIds:" + a.this.f + "  mExcludes:" + a.this.g);
                if (a.this.k != null) {
                    a.this.k.sendEmptyMessage(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* compiled from: IntersAdapter */
    public class b implements Runnable {
        public b() {
        }

        public final void run() {
            try {
                g.b("IntersAdapter", "=====超时task 开始执行 mTtcIds:" + a.this.f + "  mExcludes:" + a.this.g);
                if (a.this.m) {
                    g.b("IntersAdapter", "超时task 已经成功获取ttcid excludeids mIsGetTtcExcIdsTimeout:" + a.this.n + " mIsGetTtcExcIdsSuccess:" + a.this.m + "超时task不做处理");
                    return;
                }
                g.b("IntersAdapter", "获取ttcid excludeids超时 mIsGetTtcExcIdsTimeout:" + a.this.n + " mIsGetTtcExcIdsSuccess:" + a.this.m);
                boolean unused = a.this.n = true;
                if (a.this.k != null) {
                    a.this.k.sendEmptyMessage(2);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public final boolean d() {
        return this.d;
    }

    static /* synthetic */ void a(a aVar, CampaignUnit campaignUnit) {
        if (campaignUnit == null || campaignUnit.getAds() == null || campaignUnit.getAds().size() <= 0) {
            aVar.a("no server ads available");
            return;
        }
        final ArrayList<CampaignEx> ads = campaignUnit.getAds();
        final List<CampaignEx> a2 = aVar.a(ads);
        if (campaignUnit != null) {
            String sessionId = campaignUnit.getSessionId();
            if (!TextUtils.isEmpty(sessionId)) {
                g.b("IntersAdapter", "onload sessionId:" + sessionId);
                com.mintegral.msdk.interstitial.c.a.a = sessionId;
            }
        }
        try {
            g.b("IntersAdapter", "onload offset相加前 " + aVar.c + " mTnum:" + aVar.e);
            aVar.c = aVar.c + aVar.e;
            int j2 = aVar.j();
            if (aVar.c > j2) {
                g.b("IntersAdapter", "onload 重置offset为0 :" + j2);
                aVar.c = 0;
            }
            g.b("IntersAdapter", "onload 算出 下次的offset是:" + aVar.c);
            if (!TextUtils.isEmpty(aVar.b)) {
                com.mintegral.msdk.interstitial.c.a.a(aVar.b, aVar.c);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (ads != null && ads.size() > 0) {
            g.b("IntersAdapter", "在主线程 开始处理vba");
            if (ads == null || ads.size() == 0 || aVar.j == null || aVar.a == null) {
                g.b("IntersAdapter", "处理vba null retun");
            }
        }
        k.a((List<CampaignEx>) ads);
        new Thread(new Runnable() {
            public final void run() {
                g.b("IntersAdapter", "在子线程处理业务逻辑 开始");
                if (a2 == null || a2.size() <= 0) {
                    g.d("IntersAdapter", "onload load失败 size:0");
                    a.this.a("no ads available");
                } else {
                    g.d("IntersAdapter", "onload load成功 size:" + a2.size());
                    g.b("IntersAdapter", "onload 把广告存在本地 size:" + a2.size());
                    a.a(a.this.b, a.this.b(a2));
                    a.this.f();
                }
                m.a(i.a(a.this.a)).d();
                if (ads != null && ads.size() > 0) {
                    a.b(a.this, ads);
                }
                g.b("IntersAdapter", "在子线程处理业务逻辑 完成");
            }
        }).start();
    }

    static /* synthetic */ void a(String str, List list) {
        if (com.mintegral.msdk.interstitial.b.a.a() != null) {
            com.mintegral.msdk.interstitial.b.a a2 = com.mintegral.msdk.interstitial.b.a.a();
            try {
                if (!TextUtils.isEmpty(str) && list != null && list.size() > 0) {
                    Iterator it = list.iterator();
                    while (it.hasNext()) {
                        a2.b((CampaignEx) it.next(), str);
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    static /* synthetic */ void b(a aVar, List list) {
        g.b("IntersAdapter", "onload 开始 更新本机已安装广告列表");
        if (aVar.a == null || list == null || list.size() == 0) {
            g.b("IntersAdapter", "onload 列表为空 不做更新本机已安装广告列表");
            return;
        }
        m a2 = m.a(i.a(aVar.a));
        boolean z = false;
        for (int i2 = 0; i2 < list.size(); i2++) {
            CampaignEx campaignEx = (CampaignEx) list.get(i2);
            if (campaignEx != null) {
                if (k.a(aVar.a, campaignEx.getPackageName())) {
                    if (com.mintegral.msdk.base.controller.a.c() != null) {
                        com.mintegral.msdk.base.controller.a.c().add(new h(campaignEx.getId(), campaignEx.getPackageName()));
                        z = true;
                    }
                } else if (a2 != null && !a2.a(campaignEx.getId())) {
                    com.mintegral.msdk.base.entity.g gVar = new com.mintegral.msdk.base.entity.g();
                    gVar.a(campaignEx.getId());
                    gVar.a(campaignEx.getFca());
                    gVar.b(campaignEx.getFcb());
                    gVar.g();
                    gVar.e();
                    gVar.a(System.currentTimeMillis());
                    a2.a(gVar);
                }
            }
        }
        if (z) {
            g.b("IntersAdapter", "更新安装列表");
            com.mintegral.msdk.base.controller.a.d().f();
        }
    }
}
