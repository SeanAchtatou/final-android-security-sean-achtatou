package com.mintegral.msdk.interstitial.jscommon;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.common.a.c;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.interstitial.b.a;
import com.mintegral.msdk.interstitial.view.MTGInterstitialActivity;
import com.mintegral.msdk.mtgjscommon.windvane.i;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class interstitial extends i {
    /* access modifiers changed from: private */
    public static final String a = "com.mintegral.msdk.interstitial.jscommon.interstitial";
    private Object b;

    public void getInfo(Object obj, String str) {
        MTGInterstitialActivity mTGInterstitialActivity;
        try {
            String str2 = a;
            g.b(str2, "======前端调用 getInfo() 获取campaign数据 params:" + str);
            this.b = obj;
            if (this.mContext == null) {
                g.d(a, "getInfo() context 为空 return");
                b();
                return;
            }
            int a2 = a(this.mContext);
            String c = c();
            if (TextUtils.isEmpty(c)) {
                g.d(a, "getInfo() unitid is null");
                b();
                return;
            }
            String str3 = a;
            g.b(str3, "getInfo() mCurrentCallState:" + a2 + " unitid:" + c);
            if (a2 == 1) {
                try {
                    g.b(a, "instersGetInfo hideLoading");
                    try {
                        if (this.mContext != null) {
                            if (a(this.mContext) == 1) {
                                try {
                                    MTGInterstitialActivity mTGInterstitialActivity2 = (MTGInterstitialActivity) this.mContext;
                                    if (mTGInterstitialActivity2 != null) {
                                        mTGInterstitialActivity2.hideLoading();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    try {
                        if (this.mContext != null) {
                            if (a(this.mContext) == 1 && (mTGInterstitialActivity = (MTGInterstitialActivity) this.mContext) != null) {
                                mTGInterstitialActivity.showWebView();
                            }
                        }
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                    if (this.mContext == null) {
                        g.b(a, "instersGetInfo context 为空 通知前端没有数据");
                        b();
                    } else if (a(this.mContext) != 1) {
                        g.d(a, "instersGetInfo mCurrentCallState!=INTERSTITIAL_CALL");
                        b();
                    } else {
                        if (this.mContext instanceof MTGInterstitialActivity) {
                            ((MTGInterstitialActivity) this.mContext).mIsMtgPage = true;
                            g.b(a, "set mIsMtgPage true");
                        }
                        final String c2 = c();
                        if (TextUtils.isEmpty(c2)) {
                            g.d(a, "instersGetInfo unitid is null");
                            b();
                            return;
                        }
                        final List<CampaignEx> a3 = a(c2);
                        g.b(a, "instersGetInfo 开始从缓存里取数据");
                        if (a3 != null) {
                            String str4 = a;
                            g.d(str4, "instersGetInfo 从缓存里取到的数据 不为空 size：" + a3.size());
                            String a4 = a(a3);
                            if (TextUtils.isEmpty(a4)) {
                                g.d(a, "instersGetInfo campListJson is null return");
                                b();
                                return;
                            }
                            com.mintegral.msdk.mtgjscommon.windvane.g.a();
                            com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a4);
                            a(c2, a3);
                            try {
                                if (!TextUtils.isEmpty(c2)) {
                                    if (a3 != null) {
                                        if (a3.size() != 0) {
                                            new Thread(new Runnable() {
                                                public final void run() {
                                                    for (int i = 0; i < a3.size(); i++) {
                                                        CampaignEx campaignEx = (CampaignEx) a3.get(i);
                                                        if (campaignEx != null) {
                                                            m.a(com.mintegral.msdk.base.b.i.a(interstitial.this.mContext)).b(campaignEx.getId());
                                                            String a2 = interstitial.a;
                                                            g.b(a2, "======更新frequence：" + campaignEx.getId() + " sUnitId:" + c2);
                                                        }
                                                    }
                                                }
                                            }).start();
                                        }
                                    }
                                }
                            } catch (Exception e4) {
                                e4.printStackTrace();
                            }
                            try {
                                new Thread(new Runnable() {
                                    public final void run() {
                                        g.b(interstitial.a, "清除本地的InterstitialCampaign集合");
                                        a a2 = a.a();
                                        if (a2 != null) {
                                            a2.a(a3, c2);
                                        }
                                    }
                                }).start();
                            } catch (Exception e5) {
                                e5.printStackTrace();
                            }
                        } else {
                            b();
                        }
                    }
                } catch (Exception e6) {
                    e6.printStackTrace();
                    b();
                }
            }
        } catch (Exception e7) {
            e7.printStackTrace();
            b();
        }
    }

    public void install(Object obj, String str) {
        try {
            String str2 = a;
            g.b(str2, "======前端调用 install()  params:" + str);
            if (this.mContext == null) {
                g.d(a, "install() context 为空 return");
                return;
            }
            g.b(a, "install() 开始tracking跳转");
            if (this.mContext instanceof MTGInterstitialActivity) {
                ((MTGInterstitialActivity) this.mContext).clickTracking();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void b() {
        try {
            g.b(a, "通知h5 没有数据");
            com.mintegral.msdk.mtgjscommon.windvane.g.a();
            com.mintegral.msdk.mtgjscommon.windvane.g.a(this.b, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static List<CampaignEx> a(String str) {
        try {
            if (TextUtils.isEmpty(str) || a.a() == null) {
                return null;
            }
            return a.a().a(str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String a(List<CampaignEx> list) {
        if (list == null) {
            return "";
        }
        try {
            if (list.size() <= 0) {
                return "";
            }
            JSONArray parseCamplistToJson = CampaignEx.parseCamplistToJson(list);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("campaignList", parseCamplistToJson);
            String jSONObject2 = jSONObject.toString();
            String str = a;
            g.b(str, "===========campListJson:" + jSONObject2);
            if (!TextUtils.isEmpty(jSONObject2)) {
                return Base64.encodeToString(jSONObject2.getBytes(), 2);
            }
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public List<String> getExcludeIdList(String str) {
        ArrayList arrayList = null;
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            String optString = new JSONObject(str).optString("exclude_ids");
            if (!TextUtils.isEmpty(optString)) {
                JSONArray jSONArray = new JSONArray(optString);
                if (jSONArray.length() > 0) {
                    ArrayList arrayList2 = new ArrayList();
                    int i = 0;
                    while (i < jSONArray.length()) {
                        try {
                            if (!TextUtils.isEmpty(jSONArray.optString(i))) {
                                arrayList2.add(jSONArray.optString(i));
                            }
                            i++;
                        } catch (Exception e) {
                            Exception exc = e;
                            arrayList = arrayList2;
                            e = exc;
                            e.printStackTrace();
                            return arrayList;
                        }
                    }
                    return arrayList2;
                }
            }
            return arrayList;
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return arrayList;
        }
    }

    private static int a(Context context) {
        return (context == null || !(context instanceof MTGInterstitialActivity)) ? -1 : 1;
    }

    private String c() {
        MTGInterstitialActivity mTGInterstitialActivity;
        try {
            if (this.mContext != null && a(this.mContext) == 1) {
                try {
                    if (!(this.mContext == null || !(this.mContext instanceof MTGInterstitialActivity) || (mTGInterstitialActivity = (MTGInterstitialActivity) this.mContext) == null)) {
                        return mTGInterstitialActivity.mUnitid;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    private static void a(String str, List<CampaignEx> list) {
        try {
            if (TextUtils.isEmpty(str) || list == null) {
                return;
            }
            if (list.size() != 0) {
                for (int i = 0; i < list.size(); i++) {
                    CampaignEx campaignEx = list.get(i);
                    if (campaignEx != null) {
                        String str2 = a;
                        g.b(str2, "======更新displayid：" + campaignEx.getId());
                        c.b(str, campaignEx, "interstitial");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
