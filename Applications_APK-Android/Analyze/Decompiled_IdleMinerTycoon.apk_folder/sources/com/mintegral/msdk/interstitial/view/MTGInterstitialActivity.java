package com.mintegral.msdk.interstitial.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.helpshift.support.FaqTagFilter;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.q;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.p;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.base.webview.BrowserView;
import com.mintegral.msdk.interstitial.b.a;
import com.mintegral.msdk.interstitial.c.a;
import com.mintegral.msdk.mtgjscommon.mraid.b;
import com.mintegral.msdk.mtgjscommon.mraid.d;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.mtgjscommon.windvane.c;
import com.mintegral.msdk.out.Campaign;
import com.mintegral.msdk.out.NativeListener;
import java.io.File;
import java.util.HashMap;
import org.json.JSONObject;

public class MTGInterstitialActivity extends Activity implements b {
    public static final String INTENT_CAMAPIGN = "campaign";
    public static final String INTENT_UNIT_ID = "unitId";
    public static final long WATI_JS_INVOKE = 2000;
    public static final long WEB_LOAD_TIME = 15000;
    Runnable a = new Runnable() {
        public final void run() {
            g.d("MTGInterstitialActivity", "load page timeOut");
            if (MTGInterstitialActivity.this.d) {
                g.d("MTGInterstitialActivity", "mLoadTimeTask 已经打开非mtg的页面了 return ");
                return;
            }
            boolean unused = MTGInterstitialActivity.this.c = true;
            if (MTGInterstitialActivity.this.h != null) {
                MTGInterstitialActivity.this.h.a("load page timeout");
                if (MTGInterstitialActivity.this.f != null) {
                    MTGInterstitialActivity.this.f.setVisibility(8);
                    MTGInterstitialActivity.this.f.setWebViewListener(null);
                    MTGInterstitialActivity.this.f.release();
                }
                MTGInterstitialActivity.this.hideLoading();
            }
        }
    };
    Runnable b = new Runnable() {
        public final void run() {
            g.d("MTGInterstitialActivity", "mWaitJsInvokeTask 开始执行run方法");
            if (MTGInterstitialActivity.this.mIsMtgPage) {
                g.b("MTGInterstitialActivity", "mWaitJsInvokeTask getinfo 已调用 return");
                return;
            }
            if (MTGInterstitialActivity.this.e != null && MTGInterstitialActivity.this.e.isMraid()) {
                MTGInterstitialActivity.j(MTGInterstitialActivity.this);
            }
            a.a().a(MTGInterstitialActivity.this.e, MTGInterstitialActivity.this.mUnitid);
            if (MTGInterstitialActivity.this.c) {
                g.b("MTGInterstitialActivity", "mWaitJsInvokeTask webview 已加载超时 return");
                return;
            }
            boolean unused = MTGInterstitialActivity.this.d = true;
            MTGInterstitialActivity.this.hideLoading();
            MTGInterstitialActivity.this.showWebView();
            g.d("MTGInterstitialActivity", "mWaitJsInvokeTask 最终显示非mtg的页面 ");
        }
    };
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public CampaignEx e;
    /* access modifiers changed from: private */
    public WindVaneWebView f;
    private ImageView g;
    /* access modifiers changed from: private */
    public a.c h;
    /* access modifiers changed from: private */
    public boolean i;
    private d j;
    private long k;
    private boolean l;
    private boolean m;
    public boolean mIsMtgPage = false;
    public ProgressBar mProgressBar;
    public String mUnitid;
    /* access modifiers changed from: private */
    public Handler n = new Handler() {
        public final void handleMessage(Message message) {
        }
    };
    private com.mintegral.msdk.click.a o;

    public void expand(String str, boolean z) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            requestWindowFeature(1);
            int a2 = p.a(getApplicationContext(), "mintegral_interstitial_activity", "layout");
            if (a2 != -1) {
                setContentView(a2);
                initView();
                a();
                this.g.setOnClickListener(new View.OnClickListener() {
                    public final void onClick(View view) {
                        MTGInterstitialActivity.this.finish();
                    }
                });
                if (!(this.f == null || this.e == null)) {
                    BrowserView.MTGDownloadListener mTGDownloadListener = new BrowserView.MTGDownloadListener(this.e);
                    mTGDownloadListener.setTitle(this.e.getAppName());
                    this.f.setCampaignId(this.e.getId());
                    this.f.setDownloadListener(mTGDownloadListener);
                }
                b();
                try {
                    if (this.e != null && (!TextUtils.isEmpty(this.e.getHtmlUrl()) || this.e.isMraid())) {
                        g.b("MTGInterstitialActivity", "url:" + this.e.getHtmlUrl());
                        goneWebView();
                        this.f.setWebViewListener(new c() {
                            public final void a(int i) {
                            }

                            public final void c() {
                            }

                            public final boolean b() {
                                g.d("MTGInterstitialActivity", "shouldOverrideUrlLoading");
                                return true;
                            }

                            public final void a(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
                                g.d("MTGInterstitialActivity", "onReceivedSslError");
                                boolean unused = MTGInterstitialActivity.this.i = true;
                            }

                            public final void a(WebView webView, int i, String str, String str2) {
                                try {
                                    boolean unused = MTGInterstitialActivity.this.i = true;
                                    g.d("MTGInterstitialActivity", "onReceivedError");
                                    if (MTGInterstitialActivity.this.h != null) {
                                        MTGInterstitialActivity.this.h.a(str);
                                    }
                                    MTGInterstitialActivity.a(MTGInterstitialActivity.this, 3, str);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            public final void a() {
                                g.b("MTGInterstitialActivity", "onPageStarted");
                            }

                            public final void a(WebView webView, String str) {
                                try {
                                    if (!MTGInterstitialActivity.this.i) {
                                        MTGInterstitialActivity.a(MTGInterstitialActivity.this, 1, "");
                                        g.d("MTGInterstitialActivity", "onPageFinished");
                                        if (!(MTGInterstitialActivity.this.a == null || MTGInterstitialActivity.this.n == null)) {
                                            MTGInterstitialActivity.this.n.removeCallbacks(MTGInterstitialActivity.this.a);
                                        }
                                        if (MTGInterstitialActivity.this.h != null) {
                                            MTGInterstitialActivity.this.h.a();
                                        }
                                        if (!MTGInterstitialActivity.this.mIsMtgPage) {
                                            MTGInterstitialActivity.this.n.postDelayed(MTGInterstitialActivity.this.b, 2000);
                                            g.d("MTGInterstitialActivity", "不是mtg页面 getinfo还没调用 2秒后执行task");
                                        } else {
                                            g.d("MTGInterstitialActivity", "是mtg页面 getinfo已调用 不做处理");
                                        }
                                        MTGInterstitialActivity.e(MTGInterstitialActivity.this);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    if (MTGInterstitialActivity.this.h != null) {
                                        MTGInterstitialActivity.this.h.a("load page failed");
                                    }
                                }
                            }
                        });
                        String htmlUrl = this.e.getHtmlUrl();
                        if (this.e.isMraid()) {
                            File file = new File(this.e.getMraid());
                            if (file.exists() && file.isFile() && file.canRead()) {
                                htmlUrl = "file:////" + this.e.getMraid();
                            }
                        }
                        this.k = System.currentTimeMillis();
                        this.f.loadUrl(htmlUrl);
                        this.n.postDelayed(this.a, WEB_LOAD_TIME);
                    } else if (this.h != null) {
                        this.h.a("offerwall htmlurl is null");
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                a();
                b();
                if (this.h != null) {
                    this.h.a("not found resource");
                }
                finish();
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.e != null && this.e.isMraid()) {
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.b(this.f, "true");
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.e != null && this.e.isMraid()) {
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.b(this.f, "false");
        }
    }

    private void a() {
        Intent intent = getIntent();
        if (intent != null) {
            this.mUnitid = intent.getStringExtra("unitId");
            this.e = (CampaignEx) intent.getSerializableExtra("campaign");
        }
        if (this.e != null && this.e.isMraid()) {
            this.j = new d(this);
            this.j.c();
            this.j.a(new d.b() {
                public final void a(double d) {
                    g.d("MTGInterstitialActivity", "volume is : " + d);
                    com.mintegral.msdk.mtgjscommon.mraid.a.a();
                    com.mintegral.msdk.mtgjscommon.mraid.a.a(MTGInterstitialActivity.this.f, d);
                }
            });
        }
    }

    public void initView() {
        this.f = (WindVaneWebView) findViewById(p.a(getApplicationContext(), "mintegral_interstitial_wv", "id"));
        this.mProgressBar = (ProgressBar) findViewById(p.a(getApplicationContext(), "mintegral_interstitial_pb", "id"));
        this.g = (ImageView) findViewById(p.a(getApplicationContext(), "mintegral_interstitial_iv_close", "id"));
    }

    private void b() {
        try {
            if (com.mintegral.msdk.interstitial.c.a.d != null && !TextUtils.isEmpty(this.mUnitid) && com.mintegral.msdk.interstitial.c.a.d.containsKey(this.mUnitid)) {
                this.h = com.mintegral.msdk.interstitial.c.a.d.get(this.mUnitid);
                g.b("MTGInterstitialActivity", "mShowIntersInnerListener 初始化成功");
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void showWebView() {
        try {
            runOnUiThread(new Runnable() {
                public final void run() {
                    if (MTGInterstitialActivity.this.f != null) {
                        MTGInterstitialActivity.this.f.setVisibility(0);
                        if (MTGInterstitialActivity.this.e.isMraid()) {
                            MTGInterstitialActivity.m(MTGInterstitialActivity.this);
                        }
                        MTGInterstitialActivity.n(MTGInterstitialActivity.this);
                    }
                }
            });
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void goneWebView() {
        try {
            runOnUiThread(new Runnable() {
                public final void run() {
                    if (MTGInterstitialActivity.this.f != null) {
                        MTGInterstitialActivity.this.f.setVisibility(8);
                    }
                }
            });
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void showLoading() {
        try {
            runOnUiThread(new Runnable() {
                public final void run() {
                    if (MTGInterstitialActivity.this.mProgressBar != null) {
                        MTGInterstitialActivity.this.mProgressBar.setVisibility(0);
                    }
                }
            });
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void hideLoading() {
        try {
            runOnUiThread(new Runnable() {
                public final void run() {
                    if (MTGInterstitialActivity.this.mProgressBar != null) {
                        MTGInterstitialActivity.this.mProgressBar.setVisibility(8);
                    }
                }
            });
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            if (this.h != null) {
                this.h.b();
            }
            if (this.o != null) {
                this.o.a();
            }
            if (this.j != null) {
                this.j.d();
            }
            if (!this.l) {
                c();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void finish() {
        super.finish();
        if (!this.l) {
            c();
        }
        if (!this.m) {
            reportPlayableClosed();
        }
    }

    public void reportPlayableClosed() {
        if (this.e != null) {
            q qVar = new q("2000061", this.e.getId(), this.e.getRequestId(), this.mUnitid, com.mintegral.msdk.base.utils.c.s(com.mintegral.msdk.base.controller.a.d().h()));
            qVar.a(this.e.isMraid() ? q.a : q.b);
            com.mintegral.msdk.base.common.e.a.d(qVar, com.mintegral.msdk.base.controller.a.d().h(), this.mUnitid);
            this.m = true;
        }
    }

    private void c() {
        com.mintegral.msdk.base.common.e.b bVar = new com.mintegral.msdk.base.common.e.b(getApplicationContext());
        if (this.e != null) {
            bVar.a(this.e.getRequestIdNotice(), this.e.getId(), this.mUnitid, com.mintegral.msdk.mtgjscommon.mraid.c.a(this.e.getId()), this.e.isBidCampaign());
            com.mintegral.msdk.mtgjscommon.mraid.c.b(this.e.getId());
            this.l = true;
        }
    }

    public void onIntersClick() {
        try {
            if (this.h != null) {
                this.h.c();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public com.mintegral.msdk.d.d getIntersUnitSetting() {
        try {
            if (TextUtils.isEmpty(this.mUnitid)) {
                return null;
            }
            String j2 = com.mintegral.msdk.base.controller.a.d().j();
            com.mintegral.msdk.d.b.a();
            com.mintegral.msdk.d.d c2 = com.mintegral.msdk.d.b.c(j2, this.mUnitid);
            if (c2 != null) {
                return c2;
            }
            g.b("MTGInterstitialActivity", "获取默认的unitsetting");
            return com.mintegral.msdk.d.d.e(this.mUnitid);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void clickTracking() {
        try {
            if (this.e == null) {
                return;
            }
            if (!s.a(this.mUnitid)) {
                onIntersClick();
                this.o = new com.mintegral.msdk.click.a(getApplicationContext(), this.mUnitid);
                this.o.a(new NativeListener.NativeTrackingListener() {
                    public final void onDismissLoading(Campaign campaign) {
                    }

                    public final void onDownloadFinish(Campaign campaign) {
                    }

                    public final void onDownloadProgress(int i) {
                    }

                    public final void onDownloadStart(Campaign campaign) {
                    }

                    public final boolean onInterceptDefaultLoadingDialog() {
                        return false;
                    }

                    public final void onShowLoading(Campaign campaign) {
                    }

                    public final void onStartRedirection(Campaign campaign, String str) {
                        try {
                            g.d("MTGInterstitialActivity", "=====showloading");
                            MTGInterstitialActivity.this.showLoading();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    public final void onRedirectionFailed(Campaign campaign, String str) {
                        MTGInterstitialActivity.this.hideLoading();
                    }

                    public final void onFinishRedirection(Campaign campaign, String str) {
                        try {
                            MTGInterstitialActivity.this.hideLoading();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                this.o.b(this.e);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void open(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.e.setClickURL(str);
            CampaignEx mraidCampaign = getMraidCampaign();
            if (mraidCampaign != null) {
                new com.mintegral.msdk.base.common.e.b(getApplicationContext()).b(mraidCampaign.getRequestIdNotice(), mraidCampaign.getId(), this.mUnitid, str, mraidCampaign.isBidCampaign());
            }
        }
        clickTracking();
    }

    public void close() {
        finish();
    }

    public void unload() {
        close();
    }

    public CampaignEx getMraidCampaign() {
        return this.e;
    }

    public void useCustomClose(boolean z) {
        if (z) {
            this.g.setImageDrawable(new ColorDrawable(0));
        } else {
            this.g.setImageResource(p.a(getApplicationContext(), "mintegral_interstitial_close", "drawable"));
        }
    }

    static /* synthetic */ void a(MTGInterstitialActivity mTGInterstitialActivity, int i2, String str) {
        if (mTGInterstitialActivity.e != null && mTGInterstitialActivity.e.isMraid()) {
            q qVar = new q();
            qVar.k(mTGInterstitialActivity.e.getRequestIdNotice());
            qVar.m(mTGInterstitialActivity.e.getId());
            qVar.c(i2);
            qVar.p(String.valueOf(System.currentTimeMillis() - mTGInterstitialActivity.k));
            qVar.f("");
            qVar.o(str);
            qVar.h(CampaignEx.CLICKMODE_ON);
            qVar.a(mTGInterstitialActivity.e.isMraid() ? q.a : q.b);
            com.mintegral.msdk.base.common.e.a.c(qVar, mTGInterstitialActivity.getApplicationContext(), mTGInterstitialActivity.mUnitid);
        }
    }

    static /* synthetic */ void e(MTGInterstitialActivity mTGInterstitialActivity) {
        if (mTGInterstitialActivity.e != null && mTGInterstitialActivity.e.isMraid()) {
            String str = "UNDEFINED";
            switch (mTGInterstitialActivity.getResources().getConfiguration().orientation) {
                case 0:
                    str = FaqTagFilter.Operator.UNDEFINED;
                    break;
                case 1:
                    str = "portrait";
                    break;
                case 2:
                    str = "landscape";
                    break;
            }
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("orientation", str);
                jSONObject.put("locked", "true");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            DisplayMetrics displayMetrics = new DisplayMetrics();
            mTGInterstitialActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            HashMap hashMap = new HashMap();
            hashMap.put("placementType", "Interstitial");
            hashMap.put("state", "default");
            hashMap.put("viewable", "true");
            hashMap.put("currentAppOrientation", jSONObject);
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.b(mTGInterstitialActivity.f, (float) com.mintegral.msdk.base.utils.c.n(mTGInterstitialActivity), (float) com.mintegral.msdk.base.utils.c.o(mTGInterstitialActivity));
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.c(mTGInterstitialActivity.f, (float) displayMetrics.widthPixels, (float) displayMetrics.heightPixels);
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.a(mTGInterstitialActivity.f, hashMap);
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.a(mTGInterstitialActivity.f, mTGInterstitialActivity.j.a());
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.a(mTGInterstitialActivity.f);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    static /* synthetic */ void j(MTGInterstitialActivity mTGInterstitialActivity) {
        if (!TextUtils.isEmpty(mTGInterstitialActivity.e.getImpressionURL())) {
            com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), mTGInterstitialActivity.e, mTGInterstitialActivity.mUnitid, mTGInterstitialActivity.e.getImpressionURL(), false, true);
        }
        if (!TextUtils.isEmpty(mTGInterstitialActivity.e.getOnlyImpressionURL())) {
            com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), mTGInterstitialActivity.e, mTGInterstitialActivity.mUnitid, mTGInterstitialActivity.e.getOnlyImpressionURL(), false, true);
        }
        com.mintegral.msdk.base.common.a.c.b(mTGInterstitialActivity.mUnitid, mTGInterstitialActivity.e, "interstitial");
        m.a(i.a(mTGInterstitialActivity)).b(mTGInterstitialActivity.e.getId());
    }

    static /* synthetic */ void m(MTGInterstitialActivity mTGInterstitialActivity) {
        try {
            q qVar = new q();
            qVar.k(mTGInterstitialActivity.e.getRequestIdNotice());
            qVar.m(mTGInterstitialActivity.e.getId());
            qVar.a(mTGInterstitialActivity.e.isMraid() ? q.a : q.b);
            com.mintegral.msdk.base.common.e.a.b(qVar, mTGInterstitialActivity.getApplicationContext(), mTGInterstitialActivity.mUnitid);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void n(MTGInterstitialActivity mTGInterstitialActivity) {
        if (mTGInterstitialActivity.e != null && mTGInterstitialActivity.e.isMraid()) {
            mTGInterstitialActivity.f.post(new Runnable() {
                public final void run() {
                    com.mintegral.msdk.mtgjscommon.mraid.a.a();
                    com.mintegral.msdk.mtgjscommon.mraid.a.a(MTGInterstitialActivity.this.f, (float) MTGInterstitialActivity.this.f.getLeft(), (float) MTGInterstitialActivity.this.f.getTop(), (float) MTGInterstitialActivity.this.f.getWidth(), (float) MTGInterstitialActivity.this.f.getHeight());
                    com.mintegral.msdk.mtgjscommon.mraid.a.a();
                    com.mintegral.msdk.mtgjscommon.mraid.a.b(MTGInterstitialActivity.this.f, (float) MTGInterstitialActivity.this.f.getLeft(), (float) MTGInterstitialActivity.this.f.getTop(), (float) MTGInterstitialActivity.this.f.getWidth(), (float) MTGInterstitialActivity.this.f.getHeight());
                }
            });
        }
    }
}
