package com.mintegral.msdk.interstitial.d;

import android.content.Context;
import android.os.Build;
import com.helpshift.support.constants.FaqsColumns;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.common.net.a.e;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.out.MTGConfiguration;
import com.tapjoy.TapjoyConstants;

/* compiled from: InterstitialLoadVideoRequest */
public final class a extends com.mintegral.msdk.base.common.net.a {
    public a(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public final void a(l lVar) {
        super.a(lVar);
        lVar.a(TapjoyConstants.TJC_PLATFORM, "1");
        lVar.a(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, Build.VERSION.RELEASE);
        lVar.a(CampaignEx.JSON_KEY_PACKAGE_NAME, c.q(this.b));
        lVar.a("app_version_name", c.l(this.b));
        StringBuilder sb = new StringBuilder();
        sb.append(c.k(this.b));
        lVar.a("app_version_code", sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append(c.i(this.b));
        lVar.a("orientation", sb2.toString());
        lVar.a("model", c.c());
        lVar.a("brand", c.e());
        lVar.a("gaid", "");
        lVar.a("gaid2", c.l());
        lVar.a(Constants.RequestParameters.NETWORK_MNC, c.b());
        lVar.a(Constants.RequestParameters.NETWORK_MCC, c.a());
        int s = c.s(this.b);
        lVar.a("network_type", String.valueOf(s));
        lVar.a("network_str", c.a(this.b, s));
        lVar.a(FaqsColumns.LANGUAGE, c.h(this.b));
        lVar.a(TapjoyConstants.TJC_DEVICE_TIMEZONE, c.h());
        lVar.a("useragent", c.f());
        lVar.a("sdk_version", MTGConfiguration.SDK_VERSION);
        lVar.a("gp_version", c.t(this.b));
        lVar.a("screen_size", c.n(this.b) + "x" + c.o(this.b));
        lVar.a("is_clever", com.mintegral.msdk.base.common.a.v);
        e.a(lVar, this.b);
        e.a(lVar);
    }
}
