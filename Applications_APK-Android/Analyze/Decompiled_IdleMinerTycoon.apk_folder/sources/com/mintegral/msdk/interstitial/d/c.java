package com.mintegral.msdk.interstitial.d;

import android.support.v4.app.NotificationCompat;
import com.mintegral.msdk.base.common.net.a.a;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.mintegral.msdk.base.utils.g;
import org.json.JSONObject;

/* compiled from: InterstitialResponseHandler */
public abstract class c extends a {
    private int a;

    public abstract void b(int i, String str);

    public abstract void b(CampaignUnit campaignUnit);

    public final /* synthetic */ void a(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        if (this.a == 0) {
            g.d("", "content = " + jSONObject);
            int optInt = jSONObject.optInt("status");
            if (1 == optInt) {
                a(System.currentTimeMillis());
                CampaignUnit parseCampaignUnit = CampaignUnit.parseCampaignUnit(jSONObject.optJSONObject("data"));
                if (parseCampaignUnit == null || parseCampaignUnit.getAds() == null || parseCampaignUnit.getAds().size() <= 0) {
                    b(optInt, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
                    return;
                }
                b(parseCampaignUnit);
                a(parseCampaignUnit.getAds().size());
                return;
            }
            b(optInt, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
        } else if (this.a == 1) {
            g.d("", "content frames = " + jSONObject);
            int optInt2 = jSONObject.optInt("status");
            if (1 == optInt2) {
                a(System.currentTimeMillis());
                CampaignUnit parseCampaignUnit2 = CampaignUnit.parseCampaignUnit(jSONObject.optJSONObject("data"));
                if (parseCampaignUnit2 == null || parseCampaignUnit2.getListFrames() == null || parseCampaignUnit2.getListFrames().size() <= 0) {
                    b(optInt2, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
                } else {
                    a(parseCampaignUnit2.getListFrames().size());
                }
            } else {
                b(optInt2, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
            }
        }
    }

    public final void a(String str) {
        g.d("", "errorCode = " + str);
        b(0, str);
    }
}
