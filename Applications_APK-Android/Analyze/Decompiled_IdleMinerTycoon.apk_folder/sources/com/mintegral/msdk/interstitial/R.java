package com.mintegral.msdk.interstitial;

public final class R {
    private R() {
    }

    public static final class color {
        public static final int mintegral_interstitial_black = 2131034275;
        public static final int mintegral_interstitial_white = 2131034276;

        private color() {
        }
    }

    public static final class drawable {
        public static final int mintegral_interstitial_close = 2131165416;
        public static final int mintegral_interstitial_over = 2131165417;

        private drawable() {
        }
    }

    public static final class id {
        public static final int mintegral_interstitial_iv_close = 2131230955;
        public static final int mintegral_interstitial_pb = 2131230956;
        public static final int mintegral_interstitial_wv = 2131230957;

        private id() {
        }
    }

    public static final class layout {
        public static final int mintegral_interstitial_activity = 2131427444;

        private layout() {
        }
    }
}
