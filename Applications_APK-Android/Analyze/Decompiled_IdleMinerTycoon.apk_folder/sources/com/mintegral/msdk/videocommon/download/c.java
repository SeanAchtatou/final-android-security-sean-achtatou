package com.mintegral.msdk.videocommon.download;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.videocommon.listener.a;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: DownLoadManager */
public class c {
    private static c a;
    private ThreadPoolExecutor b = new ThreadPoolExecutor(5, 5, 15, TimeUnit.SECONDS, new LinkedBlockingDeque());
    private boolean c = false;
    private ConcurrentHashMap<String, k> d = new ConcurrentHashMap<>();

    private c() {
        this.b.allowCoreThreadTimeOut(true);
    }

    public static c getInstance() {
        if (a == null) {
            synchronized (c.class) {
                if (a == null) {
                    a = new c();
                }
            }
        }
        return a;
    }

    public final a a(String str, String str2) {
        k a2 = a(str);
        if (a2 != null) {
            return a2.a(str2);
        }
        return null;
    }

    public final a a(String str, boolean z) {
        k a2 = a(str);
        if (a2 != null) {
            return a2.b(z);
        }
        return null;
    }

    private k a(String str) {
        if (this.d == null || !this.d.containsKey(str)) {
            return null;
        }
        return this.d.get(str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:50:0x00a7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(int r6, java.lang.String r7, boolean r8) {
        /*
            r5 = this;
            r0 = 0
            com.mintegral.msdk.videocommon.download.k r7 = r5.a(r7)     // Catch:{ Exception -> 0x00a1 }
            if (r7 == 0) goto L_0x0010
            com.mintegral.msdk.videocommon.download.a r1 = r7.a(r8)     // Catch:{ Exception -> 0x00a1 }
            if (r1 != 0) goto L_0x000e
            goto L_0x0010
        L_0x000e:
            r1 = 1
            goto L_0x0011
        L_0x0010:
            r1 = 0
        L_0x0011:
            java.util.concurrent.CopyOnWriteArrayList r7 = r7.c()     // Catch:{ Exception -> 0x0096 }
            if (r7 == 0) goto L_0x00aa
            int r2 = r7.size()     // Catch:{ Exception -> 0x0096 }
            if (r2 <= 0) goto L_0x00aa
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Exception -> 0x0096 }
            r2.<init>()     // Catch:{ Exception -> 0x0096 }
        L_0x0022:
            int r3 = r7.size()     // Catch:{ Exception -> 0x0096 }
            if (r0 >= r3) goto L_0x0062
            java.lang.Object r3 = r7.get(r0)     // Catch:{ Exception -> 0x0096 }
            java.util.Map r3 = (java.util.Map) r3     // Catch:{ Exception -> 0x0096 }
            if (r3 == 0) goto L_0x005f
            java.util.Set r3 = r3.entrySet()     // Catch:{ Exception -> 0x0096 }
            if (r3 == 0) goto L_0x0062
            int r4 = r3.size()     // Catch:{ Exception -> 0x0096 }
            if (r4 <= 0) goto L_0x0062
            java.util.Iterator r3 = r3.iterator()     // Catch:{ Exception -> 0x0096 }
            if (r3 == 0) goto L_0x0062
            boolean r4 = r3.hasNext()     // Catch:{ Exception -> 0x0096 }
            if (r4 == 0) goto L_0x005f
            java.lang.Object r3 = r3.next()     // Catch:{ Exception -> 0x0096 }
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3     // Catch:{ Exception -> 0x0096 }
            java.lang.Object r3 = r3.getValue()     // Catch:{ Exception -> 0x0096 }
            com.mintegral.msdk.videocommon.download.a r3 = (com.mintegral.msdk.videocommon.download.a) r3     // Catch:{ Exception -> 0x0096 }
            com.mintegral.msdk.base.entity.CampaignEx r3 = r3.k()     // Catch:{ Exception -> 0x0096 }
            java.lang.String r3 = r3.getRequestIdNotice()     // Catch:{ Exception -> 0x0096 }
            r2.add(r3)     // Catch:{ Exception -> 0x0096 }
        L_0x005f:
            int r0 = r0 + 1
            goto L_0x0022
        L_0x0062:
            java.util.concurrent.ConcurrentHashMap r6 = com.mintegral.msdk.videocommon.a.a(r6, r8)     // Catch:{ Exception -> 0x0096 }
            if (r6 == 0) goto L_0x00aa
            int r7 = r6.size()     // Catch:{ Exception -> 0x0096 }
            if (r7 <= 0) goto L_0x00aa
            int r7 = r2.size()     // Catch:{ Exception -> 0x0096 }
            if (r7 <= 0) goto L_0x00aa
            java.util.Set r6 = r6.entrySet()     // Catch:{ Exception -> 0x0096 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ Exception -> 0x0096 }
        L_0x007c:
            boolean r7 = r6.hasNext()     // Catch:{ Exception -> 0x0096 }
            if (r7 == 0) goto L_0x00aa
            java.lang.Object r7 = r6.next()     // Catch:{ Exception -> 0x0096 }
            java.util.Map$Entry r7 = (java.util.Map.Entry) r7     // Catch:{ Exception -> 0x0096 }
            java.lang.Object r7 = r7.getKey()     // Catch:{ Exception -> 0x0096 }
            boolean r7 = r2.contains(r7)     // Catch:{ Exception -> 0x0096 }
            if (r7 != 0) goto L_0x007c
            r6.remove()     // Catch:{ Exception -> 0x0096 }
            goto L_0x007c
        L_0x0096:
            r6 = move-exception
            boolean r7 = com.mintegral.msdk.MIntegralConstans.DEBUG     // Catch:{ Exception -> 0x009f }
            if (r7 == 0) goto L_0x00aa
            r6.printStackTrace()     // Catch:{ Exception -> 0x009f }
            goto L_0x00aa
        L_0x009f:
            r6 = move-exception
            goto L_0x00a3
        L_0x00a1:
            r6 = move-exception
            r1 = 0
        L_0x00a3:
            boolean r7 = com.mintegral.msdk.MIntegralConstans.DEBUG
            if (r7 == 0) goto L_0x00aa
            r6.printStackTrace()
        L_0x00aa:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.videocommon.download.c.a(int, java.lang.String, boolean):boolean");
    }

    public void load(String str) {
        k a2 = a(str);
        if (a2 != null) {
            a2.a();
        }
    }

    public k createUnitCache(Context context, String str, List list, int i, a aVar) {
        if (TextUtils.isEmpty(str) || list == null) {
            return null;
        }
        if (this.d.containsKey(str)) {
            k kVar = this.d.get(str);
            kVar.a(aVar);
            kVar.a(list);
            return kVar;
        }
        k kVar2 = new k(list, this.b, str, i);
        kVar2.a(aVar);
        this.d.put(str, kVar2);
        return kVar2;
    }

    public final void a(boolean z) {
        this.c = z;
        if (this.d != null) {
            for (Map.Entry<String, k> value : this.d.entrySet()) {
                k kVar = (k) value.getValue();
                if (kVar != null) {
                    kVar.b();
                }
            }
        }
    }

    public final int a(String str, d dVar) {
        CopyOnWriteArrayList<Map<String, a>> c2;
        a aVar;
        CampaignEx k;
        if (this.d != null) {
            for (Map.Entry<String, k> value : this.d.entrySet()) {
                k kVar = (k) value.getValue();
                if (!(kVar == null || (c2 = kVar.c()) == null)) {
                    int size = c2.size();
                    for (int i = 0; i < size; i++) {
                        Map map = c2.get(i);
                        if (map != null) {
                            Iterator it = map.entrySet().iterator();
                            if (it.hasNext() && (aVar = (a) ((Map.Entry) it.next()).getValue()) != null && aVar.g() && (k = aVar.k()) != null) {
                                String videoUrlEncode = k.getVideoUrlEncode();
                                if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(videoUrlEncode) && str.equals(videoUrlEncode) && aVar.g()) {
                                    aVar.a(dVar);
                                    return aVar.h();
                                }
                            }
                        }
                    }
                    continue;
                }
            }
        }
        return 0;
    }

    public final void b(boolean z) {
        if (!z) {
            this.c = false;
        } else if (this.c) {
            return;
        }
        if (this.d != null) {
            for (Map.Entry<String, k> value : this.d.entrySet()) {
                ((k) value.getValue()).a();
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:15|16|(4:18|(1:20)|21|(2:23|31)(2:24|32))(1:30)) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0050, code lost:
        if (android.text.TextUtils.isEmpty(r1) == false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0052, code lost:
        com.mintegral.msdk.d.b.a();
        r4 = com.mintegral.msdk.d.b.c(com.mintegral.msdk.base.controller.a.d().j(), r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0061, code lost:
        if (r4 == null) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0063, code lost:
        r4 = com.mintegral.msdk.d.d.c(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006b, code lost:
        if (r4.n() == 2) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x006d, code lost:
        r2.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0071, code lost:
        r2.a();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x004c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r5 = this;
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.mintegral.msdk.videocommon.download.k> r0 = r5.d
            if (r0 == 0) goto L_0x0075
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.mintegral.msdk.videocommon.download.k> r0 = r5.d
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r0 = r0.iterator()
        L_0x000e:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0075
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.Object r2 = r1.getValue()
            com.mintegral.msdk.videocommon.download.k r2 = (com.mintegral.msdk.videocommon.download.k) r2
            java.lang.Object r1 = r1.getKey()
            java.lang.String r1 = (java.lang.String) r1
            r3 = 2
            java.lang.String r4 = "com.mintegral.msdk.videocommon.e.a"
            java.lang.Class.forName(r4)     // Catch:{ Exception -> 0x004c }
            com.mintegral.msdk.videocommon.e.b.a()     // Catch:{ Exception -> 0x004c }
            com.mintegral.msdk.base.controller.a r4 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Exception -> 0x004c }
            java.lang.String r4 = r4.j()     // Catch:{ Exception -> 0x004c }
            com.mintegral.msdk.videocommon.e.c r4 = com.mintegral.msdk.videocommon.e.b.a(r4, r1)     // Catch:{ Exception -> 0x004c }
            if (r4 != 0) goto L_0x003e
            goto L_0x000e
        L_0x003e:
            int r4 = r4.F()     // Catch:{ Exception -> 0x004c }
            if (r4 != r3) goto L_0x0048
            r2.b()     // Catch:{ Exception -> 0x004c }
            goto L_0x000e
        L_0x0048:
            r2.a()     // Catch:{ Exception -> 0x004c }
            goto L_0x000e
        L_0x004c:
            boolean r4 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x000e }
            if (r4 != 0) goto L_0x000e
            com.mintegral.msdk.d.b.a()     // Catch:{ Exception -> 0x000e }
            com.mintegral.msdk.base.controller.a r4 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Exception -> 0x000e }
            java.lang.String r4 = r4.j()     // Catch:{ Exception -> 0x000e }
            com.mintegral.msdk.d.d r4 = com.mintegral.msdk.d.b.c(r4, r1)     // Catch:{ Exception -> 0x000e }
            if (r4 != 0) goto L_0x0067
            com.mintegral.msdk.d.d r4 = com.mintegral.msdk.d.d.c(r1)     // Catch:{ Exception -> 0x000e }
        L_0x0067:
            int r1 = r4.n()     // Catch:{ Exception -> 0x000e }
            if (r1 != r3) goto L_0x0071
            r2.b()     // Catch:{ Exception -> 0x000e }
            goto L_0x000e
        L_0x0071:
            r2.a()     // Catch:{ Exception -> 0x000e }
            goto L_0x000e
        L_0x0075:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.videocommon.download.c.a():void");
    }
}
