package com.mintegral.msdk.videocommon.e;

import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.videocommon.b.d;
import com.mintegral.msdk.videocommon.c.a;
import com.mintegral.msdk.videocommon.c.c;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: RewardSettingManager */
public class b {
    public static a a = null;
    private static Map<String, c> b = new HashMap();
    private static b c;

    private b() {
    }

    public static b a() {
        if (c == null) {
            synchronized (b.class) {
                if (c == null) {
                    c = new b();
                }
            }
        }
        return c;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x006b A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x006c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.mintegral.msdk.videocommon.e.a b() {
        /*
            com.mintegral.msdk.videocommon.e.a r0 = com.mintegral.msdk.videocommon.e.b.a
            if (r0 != 0) goto L_0x009c
            com.mintegral.msdk.base.a.a.a r0 = com.mintegral.msdk.base.a.a.a.a()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "reward_"
            r1.<init>(r2)
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()
            java.lang.String r2 = r2.j()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r0 = r0.a(r1)
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 != 0) goto L_0x0084
            com.mintegral.msdk.videocommon.e.a r0 = com.mintegral.msdk.videocommon.e.a.a(r0)
            if (r0 == 0) goto L_0x0061
            long r1 = r0.a()
            long r3 = java.lang.System.currentTimeMillis()
            long r5 = r0.k()
            long r5 = r5 + r1
            int r1 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x0061
            java.lang.String r1 = "RewardSettingManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r7 = "app setting nexttime is not ready  [settingNextRequestTime= "
            r2.<init>(r7)
            r2.append(r5)
            java.lang.String r5 = " currentTime = "
            r2.append(r5)
            r2.append(r3)
            java.lang.String r3 = "]"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.mintegral.msdk.base.utils.g.b(r1, r2)
            r1 = 0
            goto L_0x0069
        L_0x0061:
            java.lang.String r1 = "RewardSettingManager"
            java.lang.String r2 = "app setting timeout or not exists"
            com.mintegral.msdk.base.utils.g.b(r1, r2)
            r1 = 1
        L_0x0069:
            if (r1 != 0) goto L_0x006c
            return r0
        L_0x006c:
            com.mintegral.msdk.base.controller.a r0 = com.mintegral.msdk.base.controller.a.d()
            java.lang.String r0 = r0.j()
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()
            java.lang.String r1 = r1.k()
            c(r0, r1)
            com.mintegral.msdk.videocommon.e.a r0 = c()
            return r0
        L_0x0084:
            com.mintegral.msdk.base.controller.a r0 = com.mintegral.msdk.base.controller.a.d()
            java.lang.String r0 = r0.j()
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()
            java.lang.String r1 = r1.k()
            c(r0, r1)
            com.mintegral.msdk.videocommon.e.a r0 = c()
            return r0
        L_0x009c:
            com.mintegral.msdk.videocommon.e.a r0 = com.mintegral.msdk.videocommon.e.b.a
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.videocommon.e.b.b():com.mintegral.msdk.videocommon.e.a");
    }

    private static void c(String str, String str2) {
        new a().a(com.mintegral.msdk.base.controller.a.d().h(), str, str2);
    }

    public static void a(String str, String str2, String str3, c cVar) {
        new a().a(com.mintegral.msdk.base.controller.a.d().h(), str, str2, str3, cVar);
    }

    public static c a(String str, String str2) {
        String str3 = "reward_" + str + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + str2;
        if (b.containsKey(str3)) {
            return b.get(str3);
        }
        c a2 = c.a(com.mintegral.msdk.base.a.a.a.a().a(str3));
        if (a(a2)) {
            return null;
        }
        b.put(str3, a2);
        return a2;
    }

    public static c a(String str, String str2, boolean z) {
        String str3 = "reward_" + str + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + str2;
        if (b.containsKey(str3)) {
            return b.get(str3);
        }
        c a2 = c.a(com.mintegral.msdk.base.a.a.a.a().a(str3));
        if (a(a2)) {
            return a2 == null ? a(z) : a2;
        }
        b.put(str3, a2);
        return a2;
    }

    public static a c() {
        a aVar = new a();
        HashMap hashMap = new HashMap();
        hashMap.put("1", 1000);
        hashMap.put("9", 1000);
        hashMap.put("8", 1000);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("1", new d("Virtual Item", 1));
        aVar.a(hashMap);
        aVar.b(hashMap2);
        aVar.b();
        aVar.d();
        aVar.f();
        aVar.h();
        aVar.j();
        return aVar;
    }

    private static boolean a(c cVar) {
        a b2 = b();
        if (b2 == null || cVar == null) {
            return true;
        }
        long c2 = b2.c();
        long currentTimeMillis = System.currentTimeMillis();
        long I = cVar.I() + c2;
        if (I <= currentTimeMillis) {
            return true;
        }
        g.b("RewardSettingManager", "unit setting  nexttime is not ready  [settingNextRequestTime= " + I + " currentTime = " + currentTimeMillis + Constants.RequestParameters.RIGHT_BRACKETS);
        return false;
    }

    public static void a(String str, String str2, String str3) {
        String str4 = "reward_" + str + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + str2;
        com.mintegral.msdk.base.a.a.a.a().a(str4, str3);
        b.put(str4, c.a(str3));
    }

    public static void b(String str, String str2) {
        com.mintegral.msdk.base.a.a.a.a().a("reward_" + str, str2);
        a = a.a(str2);
    }

    public static boolean a(String str) {
        JSONArray optJSONArray;
        try {
            if (!TextUtils.isEmpty(str) && (optJSONArray = new JSONObject(str).optJSONArray("unitSetting")) != null) {
                String optString = optJSONArray.optJSONObject(0).optString("unitId");
                if (optJSONArray == null || optJSONArray.length() <= 0 || TextUtils.isEmpty(optString)) {
                    return false;
                }
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static c a(boolean z) {
        c cVar = new c();
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new com.mintegral.msdk.videocommon.b.b(1, 15000, null));
            cVar.a(arrayList);
            cVar.x();
            cVar.v();
            cVar.z();
            cVar.H();
            cVar.A();
            cVar.P();
            cVar.N();
            cVar.C();
            cVar.E();
            cVar.G();
            cVar.q();
            cVar.r();
            cVar.t();
            cVar.p();
            cVar.i();
            cVar.g();
            if (z) {
                cVar.a(5);
            } else {
                cVar.a(-1);
            }
            cVar.d();
            cVar.b();
            cVar.n();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cVar;
    }
}
