package com.mintegral.msdk.videocommon.d;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.common.e.c;
import com.mintegral.msdk.base.common.e.d.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.q;
import com.mintegral.msdk.base.utils.g;
import java.util.List;

/* compiled from: VideoReport */
public class a {
    /* access modifiers changed from: private */
    public static final String a = "com.mintegral.msdk.videocommon.d.a";

    private static void a(Context context, String str, String str2) {
        if (context != null && !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            try {
                com.mintegral.msdk.base.common.e.d.a aVar = new com.mintegral.msdk.base.common.e.d.a(context);
                aVar.c();
                aVar.b(com.mintegral.msdk.base.common.a.f, c.a(str, context, str2), new b() {
                    public final void b(String str) {
                    }

                    public final void a(String str) {
                        g.d(a.a, str);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                g.d(a, e.getMessage());
            }
        }
    }

    public static void a(Context context, CampaignEx campaignEx, int i, String str) {
        if (context != null && campaignEx != null && !TextUtils.isEmpty(str)) {
            int s = com.mintegral.msdk.base.utils.c.s(context);
            a(context, q.e(new q("2000023", s, campaignEx.getVideoLength(), campaignEx.getNoticeUrl() != null ? campaignEx.getNoticeUrl() : campaignEx.getClickURL(), i, campaignEx.getBty(), com.mintegral.msdk.base.utils.c.a(context, s))), str);
        }
    }

    public static void a(Context context, String str) {
        if (context != null) {
            try {
                w a2 = w.a(i.a(context));
                if (a2 != null && a2.c() > 30 && !TextUtils.isEmpty(str)) {
                    List<q> a3 = a2.a("2000025");
                    List<q> a4 = a2.a("2000024");
                    List<q> a5 = a2.a("2000044");
                    String b = q.b(a4);
                    String c = q.c(a3);
                    String c2 = q.c(a5);
                    StringBuilder sb = new StringBuilder();
                    if (!TextUtils.isEmpty(b)) {
                        sb.append(b);
                    }
                    if (!TextUtils.isEmpty(c)) {
                        sb.append(c);
                    }
                    if (!TextUtils.isEmpty(c2)) {
                        sb.append(c2);
                    }
                    if (!TextUtils.isEmpty(sb.toString())) {
                        a(context, sb.toString(), str);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
