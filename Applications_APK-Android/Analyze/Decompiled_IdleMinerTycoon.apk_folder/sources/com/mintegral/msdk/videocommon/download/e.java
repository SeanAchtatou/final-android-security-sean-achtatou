package com.mintegral.msdk.videocommon.download;

import android.webkit.URLUtil;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.f.a;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.videocommon.download.f;
import com.mintegral.msdk.videocommon.download.g;

/* compiled from: DownLoadUtils */
public final class e {
    public static void a(final String str, final g.b bVar) {
        try {
            if (!s.a(str)) {
                if (URLUtil.isNetworkUrl(str)) {
                    f.a.a.a(new a() {
                        final /* synthetic */ boolean c = true;

                        public final void b() {
                        }

                        /* JADX INFO: additional move instructions added (1) to help type inference */
                        /* JADX WARN: Failed to insert an additional move for type inference into block B:51:0x00c9 */
                        /* JADX WARN: Failed to insert an additional move for type inference into block B:38:0x0098 */
                        /* JADX WARN: Type inference failed for: r4v13 */
                        /* JADX WARN: Type inference failed for: r7v7, types: [java.lang.String] */
                        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0063, code lost:
                            r0 = e;
                         */
                        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0064, code lost:
                            r10 = r4;
                            r4 = null;
                            r1 = r10;
                         */
                        /* JADX WARNING: Code restructure failed: missing block: B:24:0x006f, code lost:
                            r0 = th;
                         */
                        /* JADX WARNING: Failed to process nested try/catch */
                        /* JADX WARNING: Multi-variable type inference failed */
                        /* JADX WARNING: Removed duplicated region for block: B:101:0x0152 A[Catch:{ Exception -> 0x014e }] */
                        /* JADX WARNING: Removed duplicated region for block: B:107:? A[RETURN, SYNTHETIC] */
                        /* JADX WARNING: Removed duplicated region for block: B:24:0x006f A[ExcHandler: all (th java.lang.Throwable), Splitter:B:8:0x0043] */
                        /* JADX WARNING: Removed duplicated region for block: B:51:0x00c9 A[SYNTHETIC, Splitter:B:51:0x00c9] */
                        /* JADX WARNING: Removed duplicated region for block: B:56:0x00d1 A[Catch:{ Exception -> 0x00cd }] */
                        /* JADX WARNING: Removed duplicated region for block: B:60:0x00e0 A[SYNTHETIC, Splitter:B:60:0x00e0] */
                        /* JADX WARNING: Removed duplicated region for block: B:71:0x00f5 A[Catch:{ Throwable -> 0x00f1 }] */
                        /* JADX WARNING: Removed duplicated region for block: B:83:0x0119 A[Catch:{ Throwable -> 0x00f1 }] */
                        /* JADX WARNING: Removed duplicated region for block: B:96:0x014a A[SYNTHETIC, Splitter:B:96:0x014a] */
                        /* Code decompiled incorrectly, please refer to instructions dump. */
                        public final void a() {
                            /*
                                r11 = this;
                                java.lang.String r0 = ""
                                r1 = 0
                                r2 = 0
                                java.net.URL r3 = new java.net.URL     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                java.lang.String r4 = r2     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                r3.<init>(r4)     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                java.net.URLConnection r3 = r3.openConnection()     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                java.net.HttpURLConnection r3 = (java.net.HttpURLConnection) r3     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                r4 = 30000(0x7530, float:4.2039E-41)
                                r3.setReadTimeout(r4)     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                r4 = 20000(0x4e20, float:2.8026E-41)
                                r3.setConnectTimeout(r4)     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                int r4 = r3.getResponseCode()     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                java.lang.String r5 = "DownLoadUtils"
                                java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                java.lang.String r7 = "response code "
                                r6.<init>(r7)     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                r6.append(r4)     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                com.mintegral.msdk.base.utils.g.a(r5, r6)     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                r5 = 200(0xc8, float:2.8E-43)
                                if (r4 != r5) goto L_0x0081
                                java.io.InputStream r4 = r3.getInputStream()     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                r5 = 6144(0x1800, float:8.61E-42)
                                byte[] r5 = new byte[r5]     // Catch:{ Exception -> 0x007b, all -> 0x0076 }
                                java.io.ByteArrayOutputStream r6 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x007b, all -> 0x0076 }
                                r6.<init>()     // Catch:{ Exception -> 0x007b, all -> 0x0076 }
                            L_0x0043:
                                int r7 = r4.read(r5)     // Catch:{ Exception -> 0x0071, all -> 0x006f }
                                r8 = -1
                                if (r7 == r8) goto L_0x004e
                                r6.write(r5, r2, r7)     // Catch:{ Exception -> 0x0071, all -> 0x006f }
                                goto L_0x0043
                            L_0x004e:
                                byte[] r5 = r6.toByteArray()     // Catch:{ Exception -> 0x0071, all -> 0x006f }
                                if (r5 == 0) goto L_0x0069
                                byte[] r5 = r6.toByteArray()     // Catch:{ Exception -> 0x0071, all -> 0x006f }
                                boolean r7 = r11.c     // Catch:{ Exception -> 0x0063, all -> 0x006f }
                                if (r7 != 0) goto L_0x006a
                                java.lang.String r7 = new java.lang.String     // Catch:{ Exception -> 0x0063, all -> 0x006f }
                                r7.<init>(r5)     // Catch:{ Exception -> 0x0063, all -> 0x006f }
                                r1 = r7
                                goto L_0x006a
                            L_0x0063:
                                r0 = move-exception
                                r10 = r4
                                r4 = r1
                                r1 = r10
                                goto L_0x00ac
                            L_0x0069:
                                r5 = r1
                            L_0x006a:
                                r7 = 1
                                r10 = r4
                                r4 = r1
                                r1 = r10
                                goto L_0x0093
                            L_0x006f:
                                r0 = move-exception
                                goto L_0x0078
                            L_0x0071:
                                r0 = move-exception
                                r5 = r1
                                r1 = r4
                                r4 = r5
                                goto L_0x00ac
                            L_0x0076:
                                r0 = move-exception
                                r6 = r1
                            L_0x0078:
                                r1 = r4
                                goto L_0x0148
                            L_0x007b:
                                r0 = move-exception
                                r5 = r1
                                r6 = r5
                                r1 = r4
                                r4 = r6
                                goto L_0x00ac
                            L_0x0081:
                                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                java.lang.String r5 = "responseCode is "
                                r0.<init>(r5)     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                r0.append(r4)     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
                                r4 = r1
                                r5 = r4
                                r6 = r5
                                r7 = 0
                            L_0x0093:
                                r3.disconnect()     // Catch:{ Exception -> 0x00a2 }
                                if (r1 == 0) goto L_0x009b
                                r1.close()     // Catch:{ Exception -> 0x00cd }
                            L_0x009b:
                                if (r6 == 0) goto L_0x00a0
                                r6.close()     // Catch:{ Exception -> 0x00cd }
                            L_0x00a0:
                                r2 = r7
                                goto L_0x00de
                            L_0x00a2:
                                r0 = move-exception
                                goto L_0x00ac
                            L_0x00a4:
                                r0 = move-exception
                                r6 = r1
                                goto L_0x0148
                            L_0x00a8:
                                r0 = move-exception
                                r4 = r1
                                r5 = r4
                                r6 = r5
                            L_0x00ac:
                                java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x0147 }
                                java.lang.String r7 = "DownLoadUtils"
                                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0147 }
                                java.lang.String r9 = "getStringFromUrl failed "
                                r8.<init>(r9)     // Catch:{ all -> 0x0147 }
                                java.lang.String r0 = r0.getLocalizedMessage()     // Catch:{ all -> 0x0147 }
                                r8.append(r0)     // Catch:{ all -> 0x0147 }
                                java.lang.String r0 = r8.toString()     // Catch:{ all -> 0x0147 }
                                com.mintegral.msdk.base.utils.g.d(r7, r0)     // Catch:{ all -> 0x0147 }
                                if (r1 == 0) goto L_0x00cf
                                r1.close()     // Catch:{ Exception -> 0x00cd }
                                goto L_0x00cf
                            L_0x00cd:
                                r0 = move-exception
                                goto L_0x00d5
                            L_0x00cf:
                                if (r6 == 0) goto L_0x00dd
                                r6.close()     // Catch:{ Exception -> 0x00cd }
                                goto L_0x00dd
                            L_0x00d5:
                                r0.printStackTrace()
                                java.lang.String r0 = r0.getMessage()
                                goto L_0x00de
                            L_0x00dd:
                                r0 = r3
                            L_0x00de:
                                if (r2 == 0) goto L_0x00f3
                                boolean r1 = r11.c     // Catch:{ Throwable -> 0x00f1 }
                                if (r1 == 0) goto L_0x00f3
                                if (r5 == 0) goto L_0x00f3
                                int r1 = r5.length     // Catch:{ Throwable -> 0x00f1 }
                                if (r1 <= 0) goto L_0x00f3
                                com.mintegral.msdk.videocommon.download.g$b r0 = r3     // Catch:{ Throwable -> 0x00f1 }
                                java.lang.String r1 = r2     // Catch:{ Throwable -> 0x00f1 }
                                r0.a(r5, r1)     // Catch:{ Throwable -> 0x00f1 }
                                return
                            L_0x00f1:
                                r0 = move-exception
                                goto L_0x012d
                            L_0x00f3:
                                if (r2 == 0) goto L_0x0115
                                boolean r1 = com.mintegral.msdk.base.utils.s.b(r4)     // Catch:{ Throwable -> 0x00f1 }
                                if (r1 == 0) goto L_0x0115
                                int r1 = r4.length()     // Catch:{ Throwable -> 0x00f1 }
                                if (r1 <= 0) goto L_0x0115
                                java.lang.String r1 = "<mintegralloadend></mintegralloadend>"
                                boolean r1 = r4.contains(r1)     // Catch:{ Throwable -> 0x00f1 }
                                if (r1 == 0) goto L_0x0115
                                com.mintegral.msdk.videocommon.download.g$b r0 = r3     // Catch:{ Throwable -> 0x00f1 }
                                if (r0 == 0) goto L_0x012c
                                com.mintegral.msdk.videocommon.download.g$b r0 = r3     // Catch:{ Throwable -> 0x00f1 }
                                java.lang.String r1 = r2     // Catch:{ Throwable -> 0x00f1 }
                                r0.a(r5, r1)     // Catch:{ Throwable -> 0x00f1 }
                                return
                            L_0x0115:
                                com.mintegral.msdk.videocommon.download.g$b r1 = r3     // Catch:{ Throwable -> 0x00f1 }
                                if (r1 == 0) goto L_0x012c
                                com.mintegral.msdk.videocommon.download.g$b r1 = r3     // Catch:{ Throwable -> 0x00f1 }
                                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00f1 }
                                java.lang.String r3 = "content write failed:"
                                r2.<init>(r3)     // Catch:{ Throwable -> 0x00f1 }
                                r2.append(r0)     // Catch:{ Throwable -> 0x00f1 }
                                java.lang.String r0 = r2.toString()     // Catch:{ Throwable -> 0x00f1 }
                                r1.a(r0)     // Catch:{ Throwable -> 0x00f1 }
                            L_0x012c:
                                return
                            L_0x012d:
                                boolean r1 = com.mintegral.msdk.MIntegralConstans.DEBUG
                                if (r1 == 0) goto L_0x0134
                                r0.printStackTrace()
                            L_0x0134:
                                com.mintegral.msdk.videocommon.download.g$b r1 = r3
                                if (r1 == 0) goto L_0x0146
                                com.mintegral.msdk.videocommon.download.g$b r1 = r3     // Catch:{ Exception -> 0x0142 }
                                java.lang.String r0 = r0.getMessage()     // Catch:{ Exception -> 0x0142 }
                                r1.a(r0)     // Catch:{ Exception -> 0x0142 }
                                return
                            L_0x0142:
                                r0 = move-exception
                                r0.printStackTrace()
                            L_0x0146:
                                return
                            L_0x0147:
                                r0 = move-exception
                            L_0x0148:
                                if (r1 == 0) goto L_0x0150
                                r1.close()     // Catch:{ Exception -> 0x014e }
                                goto L_0x0150
                            L_0x014e:
                                r1 = move-exception
                                goto L_0x0156
                            L_0x0150:
                                if (r6 == 0) goto L_0x015c
                                r6.close()     // Catch:{ Exception -> 0x014e }
                                goto L_0x015c
                            L_0x0156:
                                r1.printStackTrace()
                                r1.getMessage()
                            L_0x015c:
                                throw r0
                            */
                            throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.videocommon.download.e.AnonymousClass1.a():void");
                        }
                    });
                    return;
                }
            }
            bVar.a("url is error");
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }
}
