package com.mintegral.msdk.videocommon.e;

import android.text.TextUtils;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.reward.player.MTGRewardVideoActivity;
import com.mintegral.msdk.videocommon.b.d;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

/* compiled from: RewardSetting */
public class a {
    private Map<String, Integer> a;
    private Map<String, d> b;
    private long c;
    private long d;
    private long e;
    private long f;
    private long g;
    private long h;

    public final long a() {
        return this.c * 1000;
    }

    public final void b() {
        this.c = 43200;
    }

    public final long c() {
        return this.d * 1000;
    }

    public final void d() {
        this.d = 5400;
    }

    public final long e() {
        return this.e * 1000;
    }

    public final void f() {
        this.e = 3600;
    }

    public final long g() {
        return this.f;
    }

    public final void h() {
        this.f = 3600;
    }

    public final long i() {
        return this.g;
    }

    public final void j() {
        this.g = 5;
    }

    public final long k() {
        return this.h;
    }

    public final Map<String, Integer> l() {
        if (this.a == null) {
            this.a = new HashMap();
            this.a.put("1", 1000);
            this.a.put("9", 1000);
            this.a.put("8", 1000);
        }
        return this.a;
    }

    public final void a(Map<String, Integer> map) {
        this.a = map;
    }

    public final Map<String, d> m() {
        return this.b;
    }

    public final void b(Map<String, d> map) {
        this.b = map;
    }

    public static a a(String str) {
        a aVar;
        a aVar2 = null;
        if (!TextUtils.isEmpty(str)) {
            try {
                aVar = new a();
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return aVar2;
            }
            try {
                JSONObject jSONObject = new JSONObject(str);
                JSONObject optJSONObject = jSONObject.optJSONObject("caplist");
                if (optJSONObject != null && optJSONObject.length() > 0) {
                    HashMap hashMap = new HashMap();
                    Iterator<String> keys = optJSONObject.keys();
                    while (keys != null && keys.hasNext()) {
                        String next = keys.next();
                        int intValue = Integer.valueOf(optJSONObject.optInt(next, 1000)).intValue();
                        if (!TextUtils.isEmpty(next)) {
                            if (TextUtils.isEmpty(next) || intValue != 0) {
                                hashMap.put(next, Integer.valueOf(intValue));
                            } else {
                                hashMap.put(next, 1000);
                            }
                        }
                    }
                    aVar.a = hashMap;
                }
                aVar.b = d.a(jSONObject.optJSONArray(MTGRewardVideoActivity.INTENT_REWARD));
                aVar.c = jSONObject.optLong("getpf", 43200);
                aVar.d = jSONObject.optLong("ruct", 5400);
                aVar.e = jSONObject.optLong(CampaignEx.JSON_KEY_PLCT, 3600);
                aVar.f = jSONObject.optLong("dlct", 3600);
                aVar.g = jSONObject.optLong("vcct", 5);
                aVar.h = jSONObject.optLong("current_time");
                return aVar;
            } catch (Exception e3) {
                e = e3;
                aVar2 = aVar;
                e.printStackTrace();
                return aVar2;
            }
        }
        return aVar2;
    }
}
