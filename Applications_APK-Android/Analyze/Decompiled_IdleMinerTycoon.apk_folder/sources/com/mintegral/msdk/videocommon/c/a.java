package com.mintegral.msdk.videocommon.c;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.common.net.a.d;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.videocommon.e.b;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: RewardSettingController */
public class a {
    /* access modifiers changed from: private */
    public static final String a = "com.mintegral.msdk.videocommon.c.a";

    public final void a(Context context, final String str, String str2) {
        b bVar = new b(context);
        l lVar = new l();
        lVar.a("app_id", str);
        lVar.a("sign", CommonMD5.getMD5(str + str2));
        bVar.a(com.mintegral.msdk.base.common.a.p, lVar, new d() {
            public final void a(JSONObject jSONObject) {
                if (jSONObject != null) {
                    try {
                        jSONObject.put("current_time", System.currentTimeMillis());
                        b.a();
                        b.b(str, jSONObject.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            public final void a(String str) {
                g.d(a.a, str);
            }
        });
    }

    public final void a(Context context, final String str, String str2, final String str3, final c cVar) {
        b bVar = new b(context);
        l lVar = new l();
        lVar.a("app_id", str);
        lVar.a("sign", CommonMD5.getMD5(str + str2));
        lVar.a("unit_ids", Constants.RequestParameters.LEFT_BRACKETS + str3 + Constants.RequestParameters.RIGHT_BRACKETS);
        bVar.a(com.mintegral.msdk.base.common.a.p, lVar, new d() {
            public final void a(JSONObject jSONObject) {
                if (jSONObject != null) {
                    try {
                        if (b.a(jSONObject.toString())) {
                            jSONObject.put("current_time", System.currentTimeMillis());
                            b.a();
                            b.a(str, str3, jSONObject.toString());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            public final void a(String str) {
                TextUtils.isEmpty(str);
            }
        });
    }
}
