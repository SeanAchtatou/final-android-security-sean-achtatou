package com.mintegral.msdk.videocommon.b;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: Reward */
public final class d {
    private String a;
    private int b;

    public d(String str, int i) {
        this.a = str;
        this.b = i;
    }

    public final String a() {
        return this.a;
    }

    public final int b() {
        return this.b;
    }

    private static d c() {
        return new d("Virtual Item", 1);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0029 A[Catch:{ Exception -> 0x002e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.mintegral.msdk.videocommon.b.d a(java.lang.String r3) {
        /*
            r0 = 0
            com.mintegral.msdk.videocommon.e.b.a()     // Catch:{ Exception -> 0x002e }
            com.mintegral.msdk.videocommon.e.a r1 = com.mintegral.msdk.videocommon.e.b.b()     // Catch:{ Exception -> 0x002e }
            boolean r2 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x002e }
            if (r2 == 0) goto L_0x0014
            com.mintegral.msdk.videocommon.b.d r3 = c()     // Catch:{ Exception -> 0x002e }
        L_0x0012:
            r0 = r3
            goto L_0x0027
        L_0x0014:
            if (r1 == 0) goto L_0x0027
            java.util.Map r2 = r1.m()     // Catch:{ Exception -> 0x002e }
            if (r2 == 0) goto L_0x0027
            java.util.Map r1 = r1.m()     // Catch:{ Exception -> 0x002e }
            java.lang.Object r3 = r1.get(r3)     // Catch:{ Exception -> 0x002e }
            com.mintegral.msdk.videocommon.b.d r3 = (com.mintegral.msdk.videocommon.b.d) r3     // Catch:{ Exception -> 0x002e }
            goto L_0x0012
        L_0x0027:
            if (r0 != 0) goto L_0x0032
            com.mintegral.msdk.videocommon.b.d r3 = c()     // Catch:{ Exception -> 0x002e }
            goto L_0x0033
        L_0x002e:
            r3 = move-exception
            r3.printStackTrace()
        L_0x0032:
            r3 = r0
        L_0x0033:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.videocommon.b.d.a(java.lang.String):com.mintegral.msdk.videocommon.b.d");
    }

    public static Map<String, d> a(JSONArray jSONArray) {
        if (jSONArray == null || jSONArray.length() <= 0) {
            return null;
        }
        try {
            HashMap hashMap = new HashMap();
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject optJSONObject = jSONArray.optJSONObject(i);
                hashMap.put(optJSONObject.optString("id"), new d(optJSONObject.optString("name"), optJSONObject.optInt("amount")));
            }
            return hashMap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final String toString() {
        return "Reward{name='" + this.a + '\'' + ", amount=" + this.b + '}';
    }
}
