package com.mintegral.msdk.videocommon.download;

import android.text.TextUtils;
import com.appsflyer.share.Constants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.b.c;
import com.mintegral.msdk.base.common.b.e;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.videocommon.download.f;
import java.io.File;

/* compiled from: HTMLResourceManager */
public final class h {
    /* access modifiers changed from: private */
    public String a;

    /* compiled from: HTMLResourceManager */
    private static class a {
        public static h a = new h((byte) 0);
    }

    /* synthetic */ h(byte b) {
        this();
    }

    private h() {
        this.a = e.b(c.MINTEGRAL_700_HTML);
    }

    public static h a() {
        return a.a;
    }

    public final void b() {
        try {
            if (!TextUtils.isEmpty(this.a)) {
                f.a.a.a(new com.mintegral.msdk.base.common.f.a() {
                    public final void b() {
                    }

                    public final void a() {
                        com.mintegral.msdk.base.utils.e.c(h.this.a);
                    }
                });
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    public final boolean a(String str, byte[] bArr) {
        try {
            g.b("HTMLResourceManager", "saveResHtmlFile url:" + str);
            if (bArr == null || bArr.length <= 0) {
                return false;
            }
            String str2 = this.a + Constants.URL_PATH_DELIMITER + CommonMD5.getMD5(str) + ".html";
            g.b("HTMLResourceManager", "saveResHtmlFile folderName:" + str2);
            if (com.mintegral.msdk.base.utils.e.a(bArr, new File(str2))) {
                return true;
            }
            return false;
        } catch (Exception e) {
            if (!MIntegralConstans.DEBUG) {
                return false;
            }
            e.printStackTrace();
            return false;
        }
    }

    public final String a(String str) {
        try {
            String md5 = CommonMD5.getMD5(str);
            File file = new File(this.a + Constants.URL_PATH_DELIMITER + md5 + ".html");
            if (file.exists()) {
                return com.mintegral.msdk.base.utils.e.a(file);
            }
            return null;
        } catch (Throwable th) {
            if (!MIntegralConstans.DEBUG) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }
}
