package com.mintegral.msdk.videocommon;

import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: TemplateWebviewCache */
public final class a {
    private static ConcurrentHashMap<String, C0069a> a = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, C0069a> b = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, C0069a> c = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, C0069a> d = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, C0069a> e = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, C0069a> f = new ConcurrentHashMap<>();

    /* renamed from: com.mintegral.msdk.videocommon.a$a  reason: collision with other inner class name */
    /* compiled from: TemplateWebviewCache */
    public static class C0069a {
        private WindVaneWebView a;
        private boolean b;

        public final WindVaneWebView a() {
            return this.a;
        }

        public final void a(WindVaneWebView windVaneWebView) {
            this.a = windVaneWebView;
        }

        public final boolean b() {
            return this.b;
        }

        public final void c() {
            this.b = true;
        }
    }

    public static C0069a a(int i, CampaignEx campaignEx) {
        if (campaignEx == null) {
            return null;
        }
        g.a("TemplateWebviewCache", "adType = " + i + " isBidCampaign = " + campaignEx.isBidCampaign());
        try {
            String requestIdNotice = campaignEx.getRequestIdNotice();
            if (i == 288) {
                requestIdNotice = campaignEx.getKeyIaUrl();
            }
            if (i != 94) {
                switch (i) {
                    case 287:
                        if (campaignEx.isBidCampaign()) {
                            if (e != null && e.size() > 0) {
                                return e.get(requestIdNotice);
                            }
                        } else if (d != null && d.size() > 0) {
                            return d.get(requestIdNotice);
                        }
                    case 288:
                        if (f != null && f.size() > 0) {
                            return f.get(requestIdNotice);
                        }
                    default:
                        if (a != null && a.size() > 0) {
                            return a.get(requestIdNotice);
                        }
                }
            } else if (campaignEx.isBidCampaign()) {
                if (c != null && c.size() > 0) {
                    return c.get(requestIdNotice);
                }
            } else if (b != null && b.size() > 0) {
                return b.get(requestIdNotice);
            }
        } catch (Exception e2) {
            if (MIntegralConstans.DEBUG) {
                e2.printStackTrace();
            }
        }
        return null;
    }

    public static ConcurrentHashMap<String, C0069a> a(int i, boolean z) {
        if (i == 94) {
            return z ? c : b;
        }
        switch (i) {
            case 287:
                return z ? e : d;
            case 288:
                return f;
            default:
                return a;
        }
    }

    public static void b(int i, CampaignEx campaignEx) {
        if (campaignEx != null) {
            try {
                String requestIdNotice = campaignEx.getRequestIdNotice();
                if (i == 288) {
                    requestIdNotice = campaignEx.getKeyIaUrl();
                }
                if (i != 94) {
                    switch (i) {
                        case 287:
                            if (campaignEx.isBidCampaign()) {
                                if (e != null) {
                                    e.remove(requestIdNotice);
                                    return;
                                }
                                return;
                            } else if (d != null) {
                                d.remove(requestIdNotice);
                                return;
                            } else {
                                return;
                            }
                        case 288:
                            if (f != null) {
                                f.remove(requestIdNotice);
                                return;
                            }
                            return;
                        default:
                            if (a != null) {
                                a.remove(requestIdNotice);
                                return;
                            }
                            return;
                    }
                } else if (campaignEx.isBidCampaign()) {
                    if (c != null) {
                        c.remove(requestIdNotice);
                    }
                } else if (b != null) {
                    b.remove(requestIdNotice);
                }
            } catch (Exception e2) {
                if (MIntegralConstans.DEBUG) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public static void a(int i) {
        if (i != 94) {
            if (i == 287) {
                try {
                    if (e != null) {
                        e.clear();
                    }
                } catch (Exception e2) {
                    if (MIntegralConstans.DEBUG) {
                        e2.printStackTrace();
                    }
                }
            }
        } else if (c != null) {
            c.clear();
        }
    }

    public static void b(int i) {
        if (i != 94) {
            switch (i) {
                case 287:
                    if (d != null) {
                        d.clear();
                        return;
                    }
                    return;
                case 288:
                    if (f != null) {
                        f.clear();
                        return;
                    }
                    return;
                default:
                    try {
                        if (a != null) {
                            a.clear();
                            return;
                        }
                        return;
                    } catch (Exception e2) {
                        if (MIntegralConstans.DEBUG) {
                            e2.printStackTrace();
                            return;
                        }
                        return;
                    }
            }
        } else if (b != null) {
            b.clear();
        }
    }

    public static void a(int i, String str, C0069a aVar) {
        if (i == 94) {
            if (c == null) {
                c = new ConcurrentHashMap<>();
            }
            c.put(str, aVar);
        } else if (i == 287) {
            try {
                if (e == null) {
                    e = new ConcurrentHashMap<>();
                }
                e.put(str, aVar);
            } catch (Exception e2) {
                if (MIntegralConstans.DEBUG) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public static void b(int i, String str, C0069a aVar) {
        if (i != 94) {
            switch (i) {
                case 287:
                    if (d == null) {
                        d = new ConcurrentHashMap<>();
                    }
                    d.put(str, aVar);
                    return;
                case 288:
                    if (f == null) {
                        f = new ConcurrentHashMap<>();
                    }
                    f.put(str, aVar);
                    return;
                default:
                    try {
                        if (a == null) {
                            a = new ConcurrentHashMap<>();
                        }
                        a.put(str, aVar);
                        return;
                    } catch (Exception e2) {
                        if (MIntegralConstans.DEBUG) {
                            e2.printStackTrace();
                            return;
                        }
                        return;
                    }
            }
        } else {
            if (b == null) {
                b = new ConcurrentHashMap<>();
            }
            b.put(str, aVar);
        }
    }
}
