package com.mintegral.msdk.videocommon.download;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.v;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.common.b.c;
import com.mintegral.msdk.base.common.b.e;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.p;
import com.mintegral.msdk.base.entity.q;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.g;
import java.io.File;
import java.io.Serializable;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;

/* compiled from: CampaignDownLoadTask */
public final class a implements Serializable {
    private int A = 100;
    private boolean B = false;
    /* access modifiers changed from: private */
    public v C;
    private boolean D = false;
    /* access modifiers changed from: private */
    public String E;
    private d F = new d() {
        public final void a(long j, int i) {
            if (!a.this.m) {
                a.a(a.this, j, i);
            }
        }
    };
    /* access modifiers changed from: private */
    public Handler G = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            v unused = a.this.C = v.a(i.a(com.mintegral.msdk.base.controller.a.d().h()));
            switch (message.what) {
                case 1:
                    a.a(a.this, a.this.s, a.this.c);
                    return;
                case 2:
                    if (a.this.c != 2) {
                        int unused2 = a.this.c = 2;
                        a.a(a.this, a.this.s, a.this.c);
                        a.this.o();
                        return;
                    }
                    return;
                case 3:
                    if (a.this.c != 4 && a.this.c != 2 && a.this.c != 5) {
                        int unused3 = a.this.c = 4;
                        a.a(a.this, a.this.s, a.this.c);
                        a.this.o();
                        return;
                    }
                    return;
                case 4:
                    int unused4 = a.this.c = 5;
                    a.this.a(1, "");
                    a.a(a.this, a.this.s, a.this.c);
                    return;
                case 5:
                    a.this.f();
                    return;
                default:
                    return;
            }
        }
    };
    private int H;
    private File I;
    private boolean a = false;
    private Runnable b;
    /* access modifiers changed from: private */
    public int c = 0;
    private CopyOnWriteArrayList<d> d = new CopyOnWriteArrayList<>();
    private d e;
    private ExecutorService f;
    private Class g;
    private Object h;
    private Class i;
    private Object j;
    /* access modifiers changed from: private */
    public CampaignEx k;
    private String l;
    /* access modifiers changed from: private */
    public boolean m = false;
    private Context n;
    /* access modifiers changed from: private */
    public int o;
    private String p;
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public boolean r = true;
    /* access modifiers changed from: private */
    public long s = 0;
    private int t = 0;
    private String u;
    private boolean v = false;
    private boolean w = false;
    private long x;
    private com.mintegral.msdk.videocommon.listener.a y;
    private com.mintegral.msdk.videocommon.listener.a z;

    public a(Context context, CampaignEx campaignEx, ExecutorService executorService, String str) {
        if (context != null || campaignEx != null) {
            this.x = System.currentTimeMillis();
            this.n = com.mintegral.msdk.base.controller.a.d().h();
            this.k = campaignEx;
            this.l = str;
            this.f = executorService;
            if (this.k != null) {
                this.p = this.k.getVideoUrlEncode();
                g.d("CampaignDownLoadTask", "=========CampaignDownLoadTask: title:" + campaignEx.getAppName());
            }
            String str2 = this.p;
            this.E = !TextUtils.isEmpty(str2) ? CommonMD5.getMD5(str2.trim()) : "";
            this.q = e.b(c.MINTEGRAL_VC);
            this.u = this.q + File.separator + this.E;
            StringBuilder sb = new StringBuilder("videoLocalPath:");
            sb.append(this.u);
            g.b("CampaignDownLoadTask", sb.toString());
            try {
                if (!TextUtils.isEmpty(this.p)) {
                    File file = null;
                    if (!TextUtils.isEmpty(this.q)) {
                        file = new File(this.q);
                        if (!file.exists()) {
                            file.mkdirs();
                        }
                    }
                    if (file != null && file.exists() && (this.I == null || !this.I.exists())) {
                        this.I = new File(file + "/.nomedia");
                        if (!this.I.exists()) {
                            this.I.createNewFile();
                        }
                    }
                    c(true);
                    b(this.p);
                }
            } catch (Exception e2) {
                g.b("CampaignDownLoadTask", e2.getMessage());
            }
        }
    }

    public final boolean a() {
        return this.D;
    }

    public final void a(boolean z2) {
        this.D = z2;
    }

    public final long b() {
        return this.x;
    }

    public final void b(boolean z2) {
        this.w = z2;
    }

    public final String c() {
        return this.u;
    }

    public final long d() {
        return (long) this.o;
    }

    public final Runnable e() {
        return this.b;
    }

    private void b(final String str) {
        this.b = new Runnable() {
            /* JADX INFO: additional move instructions added (1) to help type inference */
            /* JADX WARN: Failed to insert an additional move for type inference into block B:267:0x0422 */
            /* JADX WARN: Failed to insert an additional move for type inference into block B:241:0x03d0 */
            /* JADX INFO: additional move instructions added (3) to help type inference */
            /* JADX WARN: Type inference failed for: r6v5, types: [java.io.OutputStream] */
            /* JADX WARN: Type inference failed for: r6v24 */
            /* JADX WARN: Type inference failed for: r6v25 */
            /* JADX WARN: Type inference failed for: r6v26 */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.mintegral.msdk.videocommon.download.a.a(com.mintegral.msdk.videocommon.download.a, long, boolean):void
             arg types: [com.mintegral.msdk.videocommon.download.a, long, int]
             candidates:
              com.mintegral.msdk.videocommon.download.a.a(com.mintegral.msdk.videocommon.download.a, long, int):void
              com.mintegral.msdk.videocommon.download.a.a(com.mintegral.msdk.videocommon.download.a, long, boolean):void */
            /* JADX WARNING: Code restructure failed: missing block: B:160:0x02df, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:163:?, code lost:
                com.mintegral.msdk.base.utils.g.c("CampaignDownLoadTask", r0.getMessage(), r0);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:215:0x0392, code lost:
                r0 = th;
                r6 = r6;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:216:0x0394, code lost:
                r0 = e;
                r6 = r6;
             */
            /* JADX WARNING: Failed to process nested try/catch */
            /* JADX WARNING: Multi-variable type inference failed */
            /* JADX WARNING: Removed duplicated region for block: B:168:0x02f9  */
            /* JADX WARNING: Removed duplicated region for block: B:170:0x0300 A[SYNTHETIC, Splitter:B:170:0x0300] */
            /* JADX WARNING: Removed duplicated region for block: B:175:0x0310 A[SYNTHETIC, Splitter:B:175:0x0310] */
            /* JADX WARNING: Removed duplicated region for block: B:180:0x0320 A[SYNTHETIC, Splitter:B:180:0x0320] */
            /* JADX WARNING: Removed duplicated region for block: B:216:0x0394 A[ExcHandler: SocketTimeoutException (e java.net.SocketTimeoutException), Splitter:B:57:0x0149] */
            /* JADX WARNING: Removed duplicated region for block: B:245:0x03e4  */
            /* JADX WARNING: Removed duplicated region for block: B:247:0x03eb A[SYNTHETIC, Splitter:B:247:0x03eb] */
            /* JADX WARNING: Removed duplicated region for block: B:252:0x03fb A[SYNTHETIC, Splitter:B:252:0x03fb] */
            /* JADX WARNING: Removed duplicated region for block: B:257:0x040b A[SYNTHETIC, Splitter:B:257:0x040b] */
            /* JADX WARNING: Removed duplicated region for block: B:271:0x0433  */
            /* JADX WARNING: Removed duplicated region for block: B:273:0x043a A[SYNTHETIC, Splitter:B:273:0x043a] */
            /* JADX WARNING: Removed duplicated region for block: B:278:0x044a A[SYNTHETIC, Splitter:B:278:0x044a] */
            /* JADX WARNING: Removed duplicated region for block: B:283:0x045a A[SYNTHETIC, Splitter:B:283:0x045a] */
            /* JADX WARNING: Removed duplicated region for block: B:293:0x0476  */
            /* JADX WARNING: Removed duplicated region for block: B:295:0x047d A[SYNTHETIC, Splitter:B:295:0x047d] */
            /* JADX WARNING: Removed duplicated region for block: B:300:0x048d A[SYNTHETIC, Splitter:B:300:0x048d] */
            /* JADX WARNING: Removed duplicated region for block: B:305:0x049d A[SYNTHETIC, Splitter:B:305:0x049d] */
            /* JADX WARNING: Removed duplicated region for block: B:323:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:324:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:325:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Unknown top exception splitter block from list: {B:241:0x03d0=Splitter:B:241:0x03d0, B:267:0x0422=Splitter:B:267:0x0422} */
            /* JADX WARNING: Unknown top exception splitter block from list: {B:287:0x0468=Splitter:B:287:0x0468, B:261:0x0419=Splitter:B:261:0x0419, B:184:0x032e=Splitter:B:184:0x032e} */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void run() {
                /*
                    r13 = this;
                    java.lang.String r0 = "CampaignDownLoadTask"
                    java.lang.String r1 = "=======开始下载"
                    com.mintegral.msdk.base.utils.g.a(r0, r1)
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    r0.c(false)
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int r0 = r0.c
                    r1 = 2
                    r2 = 1
                    if (r0 != r2) goto L_0x001b
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int unused = r0.c = r1
                L_0x001b:
                    r0 = 0
                    com.mintegral.msdk.videocommon.download.a r3 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    int r3 = r3.c     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    r4 = 5
                    if (r3 == 0) goto L_0x0041
                    com.mintegral.msdk.videocommon.download.a r3 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    int r3 = r3.c     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    if (r3 == r4) goto L_0x0041
                    com.mintegral.msdk.videocommon.download.a r3 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    int r3 = r3.c     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    if (r3 == r2) goto L_0x0041
                    com.mintegral.msdk.videocommon.download.a r3 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    r3.o()     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    java.lang.String r3 = "CampaignDownLoadTask"
                    java.lang.String r5 = "=======删除资源"
                    com.mintegral.msdk.base.utils.g.a(r3, r5)     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                L_0x0041:
                    com.mintegral.msdk.videocommon.download.a r3 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    int r3 = r3.c     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    if (r3 == r2) goto L_0x03af
                    com.mintegral.msdk.videocommon.download.a r3 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    int r3 = r3.c     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    if (r3 != r4) goto L_0x0053
                    goto L_0x03af
                L_0x0053:
                    java.lang.String r3 = r2     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    boolean r3 = android.text.TextUtils.isEmpty(r3)     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    if (r3 == 0) goto L_0x0069
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int r0 = r0.c
                    if (r0 != r2) goto L_0x0068
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int unused = r0.c = r1
                L_0x0068:
                    return
                L_0x0069:
                    java.net.URL r3 = new java.net.URL     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    java.lang.String r4 = r2     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    r3.<init>(r4)     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    java.net.URLConnection r4 = r3.openConnection()     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    java.net.HttpURLConnection r4 = (java.net.HttpURLConnection) r4     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    r5 = 15000(0x3a98, float:2.102E-41)
                    r4.setConnectTimeout(r5)     // Catch:{ SocketTimeoutException -> 0x03ab, Throwable -> 0x03a8, all -> 0x03a5 }
                    r5 = 600000(0x927c0, float:8.40779E-40)
                    r4.setReadTimeout(r5)     // Catch:{ SocketTimeoutException -> 0x03ab, Throwable -> 0x03a8, all -> 0x03a5 }
                    java.lang.String r5 = "GET"
                    r4.setRequestMethod(r5)     // Catch:{ SocketTimeoutException -> 0x03ab, Throwable -> 0x03a8, all -> 0x03a5 }
                    r4.setInstanceFollowRedirects(r2)     // Catch:{ SocketTimeoutException -> 0x03ab, Throwable -> 0x03a8, all -> 0x03a5 }
                    java.lang.String r5 = "CampaignDownLoadTask"
                    java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x03ab, Throwable -> 0x03a8, all -> 0x03a5 }
                    java.lang.String r7 = "=========CampaignDownLoadTask,run url:"
                    r6.<init>(r7)     // Catch:{ SocketTimeoutException -> 0x03ab, Throwable -> 0x03a8, all -> 0x03a5 }
                    r6.append(r3)     // Catch:{ SocketTimeoutException -> 0x03ab, Throwable -> 0x03a8, all -> 0x03a5 }
                    java.lang.String r3 = r6.toString()     // Catch:{ SocketTimeoutException -> 0x03ab, Throwable -> 0x03a8, all -> 0x03a5 }
                    com.mintegral.msdk.base.utils.g.d(r5, r3)     // Catch:{ SocketTimeoutException -> 0x03ab, Throwable -> 0x03a8, all -> 0x03a5 }
                    long r5 = com.mintegral.msdk.base.utils.i.b()     // Catch:{ SocketTimeoutException -> 0x03ab, Throwable -> 0x03a8, all -> 0x03a5 }
                    int r3 = r4.getResponseCode()     // Catch:{ SocketTimeoutException -> 0x03ab, Throwable -> 0x03a8, all -> 0x03a5 }
                    r7 = 200(0xc8, float:2.8E-43)
                    if (r3 == r7) goto L_0x00b7
                    r7 = 206(0xce, float:2.89E-43)
                    if (r3 != r7) goto L_0x00ad
                    goto L_0x00b7
                L_0x00ad:
                    com.mintegral.msdk.videocommon.download.a r3 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x03ab, Throwable -> 0x03a8, all -> 0x03a5 }
                    java.lang.String r5 = "http response failed"
                    com.mintegral.msdk.videocommon.download.a.c(r3, r5)     // Catch:{ SocketTimeoutException -> 0x03ab, Throwable -> 0x03a8, all -> 0x03a5 }
                    r6 = r0
                    goto L_0x02f1
                L_0x00b7:
                    java.io.InputStream r3 = r4.getInputStream()     // Catch:{ SocketTimeoutException -> 0x03ab, Throwable -> 0x03a8, all -> 0x03a5 }
                    com.mintegral.msdk.videocommon.download.a r7 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    int r8 = r4.getContentLength()     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    int unused = r7.o = r8     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    java.lang.String r7 = "CampaignDownLoadTask"
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    java.lang.String r9 = "=========正在下载中,空间:"
                    r8.<init>(r9)     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    r8.append(r5)     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    java.lang.String r9 = ",contentLength:"
                    r8.append(r9)     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    com.mintegral.msdk.videocommon.download.a r9 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    int r9 = r9.o     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    r8.append(r9)     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    java.lang.String r8 = r8.toString()     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    com.mintegral.msdk.base.utils.g.d(r7, r8)     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    r7 = 0
                    int r9 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
                    if (r9 == 0) goto L_0x0129
                    int r9 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
                    if (r9 == 0) goto L_0x00fb
                    com.mintegral.msdk.videocommon.download.a r7 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    int r7 = r7.o     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    long r7 = (long) r7
                    int r9 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
                    if (r9 <= 0) goto L_0x00fb
                    goto L_0x0129
                L_0x00fb:
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int r0 = r0.c
                    if (r0 != r2) goto L_0x0108
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int unused = r0.c = r1
                L_0x0108:
                    if (r4 == 0) goto L_0x0118
                    r4.disconnect()     // Catch:{ Throwable -> 0x010e }
                    goto L_0x0118
                L_0x010e:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x0118:
                    if (r3 == 0) goto L_0x0128
                    r3.close()     // Catch:{ Throwable -> 0x011e }
                    return
                L_0x011e:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x0128:
                    return
                L_0x0129:
                    com.mintegral.msdk.videocommon.download.a r5 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    boolean unused = r5.r = true     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    java.io.File r5 = new java.io.File     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    com.mintegral.msdk.videocommon.download.a r6 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    java.lang.String r6 = r6.q     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    com.mintegral.msdk.videocommon.download.a r7 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    java.lang.String r7 = r7.E     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    r5.<init>(r6, r7)     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    com.mintegral.msdk.videocommon.download.a r6 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    int unused = r6.c = r2     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    r6.<init>(r5)     // Catch:{ SocketTimeoutException -> 0x03a0, Throwable -> 0x039c, all -> 0x0397 }
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.base.b.v r0 = r0.C     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    if (r0 != 0) goto L_0x0166
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.base.controller.a r7 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    android.content.Context r7 = r7.h()     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.base.b.i r7 = com.mintegral.msdk.base.b.i.a(r7)     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.base.b.v r7 = com.mintegral.msdk.base.b.v.a(r7)     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.base.b.v unused = r0.C = r7     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                L_0x0166:
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.base.b.v r0 = r0.C     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.videocommon.download.a r7 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.base.entity.CampaignEx r7 = r7.k     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.videocommon.download.a r8 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    int r8 = r8.o     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    long r8 = (long) r8     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    r0.a(r7, r8)     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    r0 = 4096(0x1000, float:5.74E-42)
                    byte[] r0 = new byte[r0]     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    java.lang.String r7 = "CampaignDownLoadTask"
                    java.lang.String r8 = "=========开始下载，while循环读流"
                    com.mintegral.msdk.base.utils.g.d(r7, r8)     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    r7 = 0
                    r8 = 0
                L_0x0189:
                    int r9 = r3.read(r0)     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    r10 = -1
                    if (r9 == r10) goto L_0x0270
                    boolean r10 = r5.exists()     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    if (r10 != 0) goto L_0x01e0
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int r0 = r0.c
                    if (r0 != r2) goto L_0x01a3
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int unused = r0.c = r1
                L_0x01a3:
                    if (r4 == 0) goto L_0x01b3
                    r4.disconnect()     // Catch:{ Throwable -> 0x01a9 }
                    goto L_0x01b3
                L_0x01a9:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x01b3:
                    if (r3 == 0) goto L_0x01c3
                    r3.close()     // Catch:{ Throwable -> 0x01b9 }
                    goto L_0x01c3
                L_0x01b9:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x01c3:
                    r6.flush()     // Catch:{ Throwable -> 0x01c7 }
                    goto L_0x01d1
                L_0x01c7:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x01d1:
                    r6.close()     // Catch:{ Throwable -> 0x01d5 }
                    return
                L_0x01d5:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                    return
                L_0x01e0:
                    r6.write(r0, r7, r9)     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    int r8 = r8 + r9
                    com.mintegral.msdk.videocommon.download.a r9 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ Throwable -> 0x01f1, SocketTimeoutException -> 0x0394 }
                    long r10 = (long) r8     // Catch:{ Throwable -> 0x01f1, SocketTimeoutException -> 0x0394 }
                    com.mintegral.msdk.videocommon.download.a r12 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ Throwable -> 0x01f1, SocketTimeoutException -> 0x0394 }
                    int r12 = r12.c     // Catch:{ Throwable -> 0x01f1, SocketTimeoutException -> 0x0394 }
                    com.mintegral.msdk.videocommon.download.a.a(r9, r10, r12)     // Catch:{ Throwable -> 0x01f1, SocketTimeoutException -> 0x0394 }
                    goto L_0x01fb
                L_0x01f1:
                    r9 = move-exception
                    java.lang.String r10 = "CampaignDownLoadTask"
                    java.lang.String r11 = r9.getMessage()     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.base.utils.g.c(r10, r11, r9)     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                L_0x01fb:
                    com.mintegral.msdk.videocommon.download.a r9 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    int r9 = r9.c     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    r10 = 4
                    if (r9 == r1) goto L_0x020c
                    com.mintegral.msdk.videocommon.download.a r9 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    int r9 = r9.c     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    if (r9 != r10) goto L_0x0189
                L_0x020c:
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    int r0 = r0.c     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    if (r0 != r10) goto L_0x0216
                    r0 = 3
                    goto L_0x0217
                L_0x0216:
                    r0 = 2
                L_0x0217:
                    android.os.Message r5 = android.os.Message.obtain()     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    r5.what = r0     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    android.os.Handler r0 = r0.G     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    r0.sendMessage(r5)     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int r0 = r0.c
                    if (r0 != r2) goto L_0x0233
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int unused = r0.c = r1
                L_0x0233:
                    if (r4 == 0) goto L_0x0243
                    r4.disconnect()     // Catch:{ Throwable -> 0x0239 }
                    goto L_0x0243
                L_0x0239:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x0243:
                    if (r3 == 0) goto L_0x0253
                    r3.close()     // Catch:{ Throwable -> 0x0249 }
                    goto L_0x0253
                L_0x0249:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x0253:
                    r6.flush()     // Catch:{ Throwable -> 0x0257 }
                    goto L_0x0261
                L_0x0257:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x0261:
                    r6.close()     // Catch:{ Throwable -> 0x0265 }
                    return
                L_0x0265:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                    return
                L_0x0270:
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.base.entity.CampaignEx r0 = r0.k     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    java.lang.String r0 = r0.getVideoMD5Value()     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    boolean r8 = android.text.TextUtils.isEmpty(r0)     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    if (r8 != 0) goto L_0x033d
                    java.lang.String r5 = com.mintegral.msdk.base.utils.d.a(r5)     // Catch:{ Throwable -> 0x02df, SocketTimeoutException -> 0x0394 }
                    boolean r0 = r0.equals(r5)     // Catch:{ Throwable -> 0x02df, SocketTimeoutException -> 0x0394 }
                    if (r0 == 0) goto L_0x02e9
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ Throwable -> 0x02df, SocketTimeoutException -> 0x0394 }
                    com.mintegral.msdk.videocommon.download.a r5 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ Throwable -> 0x02df, SocketTimeoutException -> 0x0394 }
                    long r7 = r5.s     // Catch:{ Throwable -> 0x02df, SocketTimeoutException -> 0x0394 }
                    com.mintegral.msdk.videocommon.download.a.a(r0, r7, r2)     // Catch:{ Throwable -> 0x02df, SocketTimeoutException -> 0x0394 }
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int r0 = r0.c
                    if (r0 != r2) goto L_0x02a2
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int unused = r0.c = r1
                L_0x02a2:
                    if (r4 == 0) goto L_0x02b2
                    r4.disconnect()     // Catch:{ Throwable -> 0x02a8 }
                    goto L_0x02b2
                L_0x02a8:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x02b2:
                    if (r3 == 0) goto L_0x02c2
                    r3.close()     // Catch:{ Throwable -> 0x02b8 }
                    goto L_0x02c2
                L_0x02b8:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x02c2:
                    r6.flush()     // Catch:{ Throwable -> 0x02c6 }
                    goto L_0x02d0
                L_0x02c6:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x02d0:
                    r6.close()     // Catch:{ Throwable -> 0x02d4 }
                    return
                L_0x02d4:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                    return
                L_0x02df:
                    r0 = move-exception
                    java.lang.String r5 = "CampaignDownLoadTask"
                    java.lang.String r7 = r0.getMessage()     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.base.utils.g.c(r5, r7, r0)     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                L_0x02e9:
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    java.lang.String r5 = "MD5 check failed"
                    r0.c(r5)     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    r0 = r3
                L_0x02f1:
                    com.mintegral.msdk.videocommon.download.a r3 = com.mintegral.msdk.videocommon.download.a.this
                    int r3 = r3.c
                    if (r3 != r2) goto L_0x02fe
                    com.mintegral.msdk.videocommon.download.a r2 = com.mintegral.msdk.videocommon.download.a.this
                    int unused = r2.c = r1
                L_0x02fe:
                    if (r4 == 0) goto L_0x030e
                    r4.disconnect()     // Catch:{ Throwable -> 0x0304 }
                    goto L_0x030e
                L_0x0304:
                    r1 = move-exception
                    java.lang.String r2 = "CampaignDownLoadTask"
                    java.lang.String r3 = r1.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r2, r3, r1)
                L_0x030e:
                    if (r0 == 0) goto L_0x031e
                    r0.close()     // Catch:{ Throwable -> 0x0314 }
                    goto L_0x031e
                L_0x0314:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x031e:
                    if (r6 == 0) goto L_0x046c
                    r6.flush()     // Catch:{ Throwable -> 0x0324 }
                    goto L_0x032e
                L_0x0324:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x032e:
                    r6.close()     // Catch:{ Throwable -> 0x0332 }
                    return
                L_0x0332:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                    return
                L_0x033d:
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.videocommon.download.a r5 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    long r8 = r5.s     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.videocommon.download.a.a(r0, r8, r7)     // Catch:{ SocketTimeoutException -> 0x0394, Throwable -> 0x0392 }
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int r0 = r0.c
                    if (r0 != r2) goto L_0x0355
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int unused = r0.c = r1
                L_0x0355:
                    if (r4 == 0) goto L_0x0365
                    r4.disconnect()     // Catch:{ Throwable -> 0x035b }
                    goto L_0x0365
                L_0x035b:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x0365:
                    if (r3 == 0) goto L_0x0375
                    r3.close()     // Catch:{ Throwable -> 0x036b }
                    goto L_0x0375
                L_0x036b:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x0375:
                    r6.flush()     // Catch:{ Throwable -> 0x0379 }
                    goto L_0x0383
                L_0x0379:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x0383:
                    r6.close()     // Catch:{ Throwable -> 0x0387 }
                    return
                L_0x0387:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                    return
                L_0x0392:
                    r0 = move-exception
                    goto L_0x03d0
                L_0x0394:
                    r0 = move-exception
                    goto L_0x0422
                L_0x0397:
                    r5 = move-exception
                    r6 = r0
                    r0 = r5
                    goto L_0x046e
                L_0x039c:
                    r5 = move-exception
                    r6 = r0
                    r0 = r5
                    goto L_0x03d0
                L_0x03a0:
                    r5 = move-exception
                    r6 = r0
                    r0 = r5
                    goto L_0x0422
                L_0x03a5:
                    r3 = move-exception
                    r6 = r0
                    goto L_0x03c7
                L_0x03a8:
                    r3 = move-exception
                    r6 = r0
                    goto L_0x03ce
                L_0x03ab:
                    r3 = move-exception
                    r6 = r0
                    goto L_0x0420
                L_0x03af:
                    java.lang.String r3 = "CampaignDownLoadTask"
                    java.lang.String r4 = "=======正在下载中或下载完成"
                    com.mintegral.msdk.base.utils.g.a(r3, r4)     // Catch:{ SocketTimeoutException -> 0x041d, Throwable -> 0x03cb, all -> 0x03c4 }
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int r0 = r0.c
                    if (r0 != r2) goto L_0x03c3
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int unused = r0.c = r1
                L_0x03c3:
                    return
                L_0x03c4:
                    r3 = move-exception
                    r4 = r0
                    r6 = r4
                L_0x03c7:
                    r0 = r3
                    r3 = r6
                    goto L_0x046e
                L_0x03cb:
                    r3 = move-exception
                    r4 = r0
                    r6 = r4
                L_0x03ce:
                    r0 = r3
                    r3 = r6
                L_0x03d0:
                    com.mintegral.msdk.videocommon.download.a r5 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ all -> 0x046d }
                    java.lang.String r7 = r0.getMessage()     // Catch:{ all -> 0x046d }
                    com.mintegral.msdk.videocommon.download.a.c(r5, r7)     // Catch:{ all -> 0x046d }
                    r0.printStackTrace()     // Catch:{ all -> 0x046d }
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int r0 = r0.c
                    if (r0 != r2) goto L_0x03e9
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int unused = r0.c = r1
                L_0x03e9:
                    if (r4 == 0) goto L_0x03f9
                    r4.disconnect()     // Catch:{ Throwable -> 0x03ef }
                    goto L_0x03f9
                L_0x03ef:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x03f9:
                    if (r3 == 0) goto L_0x0409
                    r3.close()     // Catch:{ Throwable -> 0x03ff }
                    goto L_0x0409
                L_0x03ff:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x0409:
                    if (r6 == 0) goto L_0x046c
                    r6.flush()     // Catch:{ Throwable -> 0x040f }
                    goto L_0x0419
                L_0x040f:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x0419:
                    r6.close()     // Catch:{ Throwable -> 0x0332 }
                    return
                L_0x041d:
                    r3 = move-exception
                    r4 = r0
                    r6 = r4
                L_0x0420:
                    r0 = r3
                    r3 = r6
                L_0x0422:
                    com.mintegral.msdk.videocommon.download.a r5 = com.mintegral.msdk.videocommon.download.a.this     // Catch:{ all -> 0x046d }
                    java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x046d }
                    com.mintegral.msdk.videocommon.download.a.c(r5, r0)     // Catch:{ all -> 0x046d }
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int r0 = r0.c
                    if (r0 != r2) goto L_0x0438
                    com.mintegral.msdk.videocommon.download.a r0 = com.mintegral.msdk.videocommon.download.a.this
                    int unused = r0.c = r1
                L_0x0438:
                    if (r4 == 0) goto L_0x0448
                    r4.disconnect()     // Catch:{ Throwable -> 0x043e }
                    goto L_0x0448
                L_0x043e:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x0448:
                    if (r3 == 0) goto L_0x0458
                    r3.close()     // Catch:{ Throwable -> 0x044e }
                    goto L_0x0458
                L_0x044e:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x0458:
                    if (r6 == 0) goto L_0x046c
                    r6.flush()     // Catch:{ Throwable -> 0x045e }
                    goto L_0x0468
                L_0x045e:
                    r0 = move-exception
                    java.lang.String r1 = "CampaignDownLoadTask"
                    java.lang.String r2 = r0.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r1, r2, r0)
                L_0x0468:
                    r6.close()     // Catch:{ Throwable -> 0x0332 }
                    return
                L_0x046c:
                    return
                L_0x046d:
                    r0 = move-exception
                L_0x046e:
                    com.mintegral.msdk.videocommon.download.a r5 = com.mintegral.msdk.videocommon.download.a.this
                    int r5 = r5.c
                    if (r5 != r2) goto L_0x047b
                    com.mintegral.msdk.videocommon.download.a r2 = com.mintegral.msdk.videocommon.download.a.this
                    int unused = r2.c = r1
                L_0x047b:
                    if (r4 == 0) goto L_0x048b
                    r4.disconnect()     // Catch:{ Throwable -> 0x0481 }
                    goto L_0x048b
                L_0x0481:
                    r1 = move-exception
                    java.lang.String r2 = "CampaignDownLoadTask"
                    java.lang.String r4 = r1.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r2, r4, r1)
                L_0x048b:
                    if (r3 == 0) goto L_0x049b
                    r3.close()     // Catch:{ Throwable -> 0x0491 }
                    goto L_0x049b
                L_0x0491:
                    r1 = move-exception
                    java.lang.String r2 = "CampaignDownLoadTask"
                    java.lang.String r3 = r1.getMessage()
                    com.mintegral.msdk.base.utils.g.c(r2, r3, r1)
                L_0x049b:
                    if (r6 == 0) goto L_0x04b9
                    r6.flush()     // Catch:{ Throwable -> 0x04a1 }
                    goto L_0x04ab
                L_0x04a1:
                    r1 = move-exception
                    java.lang.String r2 = r1.getMessage()
                    java.lang.String r3 = "CampaignDownLoadTask"
                    com.mintegral.msdk.base.utils.g.c(r3, r2, r1)
                L_0x04ab:
                    r6.close()     // Catch:{ Throwable -> 0x04af }
                    goto L_0x04b9
                L_0x04af:
                    r1 = move-exception
                    java.lang.String r2 = r1.getMessage()
                    java.lang.String r3 = "CampaignDownLoadTask"
                    com.mintegral.msdk.base.utils.g.c(r3, r2, r1)
                L_0x04b9:
                    throw r0
                */
                throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.videocommon.download.a.AnonymousClass3.run():void");
            }
        };
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        if (this.y != null) {
            g.d("CampaignDownLoadTask", "video load retry fail");
            this.y.a(str, this.p);
        }
        if (this.z != null) {
            this.z.a(str, this.p);
        }
        Message obtain = Message.obtain();
        obtain.what = 3;
        this.G.sendMessage(obtain);
    }

    public final void f() {
        g.d("CampaignDownLoadTask", "start()");
        if (this.b != null) {
            this.f.execute(this.b);
            this.m = true;
            return;
        }
        b(this.p);
        this.f.execute(this.b);
        this.m = true;
    }

    public final boolean g() {
        return this.m;
    }

    public final void a(String str) {
        q();
        a(2, str);
        this.c = 4;
    }

    public final int h() {
        return this.c;
    }

    public final void i() {
        this.c = 0;
        if (this.C == null) {
            this.C = v.a(i.a(com.mintegral.msdk.base.controller.a.d().h()));
        }
        this.C.a(this.p, 0, 0);
    }

    public final String j() {
        String str;
        String str2 = this.q + File.separator + this.E;
        File file = new File(str2);
        try {
            if (!file.exists()) {
                str = "file is not exist ";
            } else if (!file.isFile()) {
                str = "file is not file ";
            } else if (file.canRead()) {
                this.u = str2;
                str = "";
            } else {
                str = "file can not readed ";
            }
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
            str = th.getMessage();
        }
        if (this.c == 5 && !TextUtils.isEmpty(str)) {
            p();
        }
        return str;
    }

    public final CampaignEx k() {
        return this.k;
    }

    public final void a(CampaignEx campaignEx) {
        this.k = campaignEx;
    }

    /* access modifiers changed from: private */
    public void c(boolean z2) {
        p a2 = v.a(i.a(com.mintegral.msdk.base.controller.a.d().h())).a(this.p, this.k.getBidToken());
        this.s = a2.b();
        if (this.c != 2) {
            this.c = a2.d();
        }
        if (z2 && this.c == 1) {
            this.c = 2;
        }
        this.o = a2.c();
        if (a2.a() > 0) {
            this.x = a2.a();
        }
        if (this.c == 5 && !this.v) {
            if (new File(this.q + File.separator + this.E).exists()) {
                this.u = this.q + File.separator + this.E;
                return;
            }
            p();
            g.b("CampaignDownLoadTask", "restore state==5 文件不存在");
        } else if (this.c != 0) {
            this.u = this.q + File.separator + this.E;
        }
    }

    public final void a(com.mintegral.msdk.videocommon.listener.a aVar) {
        this.y = aVar;
    }

    public final void b(com.mintegral.msdk.videocommon.listener.a aVar) {
        this.z = aVar;
    }

    public final void a(int i2) {
        this.A = i2;
        g.b("CampaignDownLoadTask", "mReadyRate:" + this.A);
    }

    public final void a(d dVar) {
        if (this.d != null) {
            this.d.add(dVar);
        }
    }

    public final void b(d dVar) {
        this.e = dVar;
    }

    public final void l() {
        if (this.d != null) {
            this.d = null;
        }
    }

    public final long m() {
        return this.s;
    }

    public final d n() {
        return this.F;
    }

    private void p() {
        if (this.C == null) {
            this.C = v.a(i.a(com.mintegral.msdk.base.controller.a.d().h()));
        }
        try {
            this.C.a(this.p);
            File file = new File(this.u);
            if (file.exists() && file.isFile()) {
                file.delete();
            }
        } catch (Throwable unused) {
            g.d("CampaignDownLoadTask", "del DB or file failed");
        } finally {
            this.c = 0;
        }
    }

    public final void o() {
        try {
            p();
            if (this.k == null || this.k.getPlayable_ads_without_video() != 2) {
                com.mintegral.msdk.videocommon.a.a a2 = com.mintegral.msdk.videocommon.a.a.a();
                if (a2 != null) {
                    a2.b(this.k);
                }
                this.c = 0;
            }
        } catch (Exception unused) {
            g.d("CampaignDownLoadTask", "del file is failed");
        } finally {
            this.c = 0;
        }
    }

    private void q() {
        try {
            if (this.g == null || this.h == null) {
                this.g = Class.forName("com.mintegral.msdk.reward.b.a");
                this.h = this.g.newInstance();
                this.g.getMethod("insertExcludeId", String.class, CampaignEx.class).invoke(this.h, this.l, this.k);
            }
            if (this.i == null || this.j == null) {
                this.i = Class.forName("com.mintegral.msdk.mtgnative.c.b");
                this.j = this.i.newInstance();
                this.i.getMethod("insertExcludeId", String.class, CampaignEx.class).invoke(this.j, this.l, this.k);
            }
        } catch (Exception unused) {
            g.d("CampaignDownLoadTask", "");
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, String str) {
        w a2 = w.a(i.a(com.mintegral.msdk.base.controller.a.d().h()));
        long j2 = 0;
        if (this.x != 0) {
            j2 = System.currentTimeMillis() - this.x;
        }
        q qVar = new q(this.n, this.k, i2, Long.toString(j2), this.o, this.H);
        qVar.m(this.k.getId());
        qVar.e(this.k.getVideoUrlEncode());
        qVar.o(str);
        qVar.k(this.k.getRequestIdNotice());
        qVar.l(this.l);
        a2.a(qVar);
    }

    public final void b(int i2) {
        this.H = i2;
    }

    static /* synthetic */ void a(a aVar, long j2, int i2) {
        aVar.s = j2;
        if (100 * j2 >= ((long) (aVar.A * aVar.o)) && !aVar.B && i2 != 4) {
            if (aVar.A != 100 || i2 == 5) {
                aVar.B = true;
                g.d("CampaignDownLoadTask", "video load sucessed state:" + i2 + "  mReadyRate:" + aVar.A);
                String j3 = aVar.j();
                if (TextUtils.isEmpty(j3)) {
                    if (aVar.y != null) {
                        aVar.y.a(aVar.p);
                    }
                    if (aVar.z != null) {
                        aVar.z.a(aVar.p);
                    }
                } else {
                    if (aVar.y != null) {
                        com.mintegral.msdk.videocommon.listener.a aVar2 = aVar.y;
                        aVar2.a("file is not effective" + j3, aVar.p);
                    }
                    if (aVar.z != null) {
                        com.mintegral.msdk.videocommon.listener.a aVar3 = aVar.z;
                        aVar3.a("file is not effective" + j3, aVar.p);
                    }
                }
            } else {
                return;
            }
        }
        if (aVar.m) {
            if (aVar.d != null) {
                Iterator<d> it = aVar.d.iterator();
                while (it.hasNext()) {
                    d next = it.next();
                    if (next != null) {
                        next.a(j2, i2);
                    }
                }
            }
            if (aVar.e != null && (aVar.c == 5 || aVar.c == 4 || aVar.c == 2)) {
                aVar.e.a(j2, i2);
                aVar.e = null;
            }
        }
        if (!aVar.a && j2 > 0) {
            aVar.a = true;
            if (aVar.C == null) {
                aVar.C = v.a(i.a(com.mintegral.msdk.base.controller.a.d().h()));
            }
            aVar.C.a(aVar.p, j2, aVar.c);
        }
    }

    static /* synthetic */ void a(a aVar, long j2, boolean z2) {
        if (j2 == ((long) aVar.o) || z2) {
            g.d("CampaignDownLoadTask", "=========下载完成");
            Message obtain = Message.obtain();
            obtain.what = 4;
            aVar.G.sendMessage(obtain);
            aVar.C.a(aVar.p, j2, 5);
            return;
        }
        aVar.c("File downloaded is smaller than the file");
    }

    static /* synthetic */ void c(a aVar, String str) {
        aVar.t++;
        g.a("CampaignDownLoadTask", "retryReq" + aVar.t);
        try {
            if (aVar.n != null) {
                Object systemService = aVar.n.getSystemService("connectivity");
                ConnectivityManager connectivityManager = null;
                if (systemService instanceof ConnectivityManager) {
                    connectivityManager = (ConnectivityManager) systemService;
                }
                if (!(connectivityManager == null || connectivityManager.getActiveNetworkInfo() == null || connectivityManager.getActiveNetworkInfo().isAvailable())) {
                    return;
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        if (aVar.t <= 1) {
            aVar.p();
            aVar.G.sendEmptyMessageDelayed(5, 2000);
            return;
        }
        aVar.q();
        aVar.a(3, str);
        aVar.G.sendEmptyMessage(3);
        if (aVar.y != null) {
            g.d("CampaignDownLoadTask", "video load retry fail");
            aVar.y.a(str, aVar.p);
        }
        if (aVar.z != null) {
            aVar.z.a(str, aVar.p);
        }
    }
}
