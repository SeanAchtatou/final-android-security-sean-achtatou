package com.mintegral.msdk.videocommon.b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: Cbprule */
public final class c {
    private int a;
    private List<Integer> b;

    private c(int i, List<Integer> list) {
        this.a = i;
        this.b = list;
    }

    public final int a() {
        return this.a;
    }

    public final List<Integer> b() {
        return this.b;
    }

    public static c a(JSONObject jSONObject) {
        c cVar = new c(1, Arrays.asList(1, 2, 3, 4));
        if (jSONObject == null) {
            return cVar;
        }
        try {
            int optInt = jSONObject.optInt("type");
            ArrayList arrayList = new ArrayList();
            JSONArray optJSONArray = jSONObject.optJSONArray("value");
            if (optJSONArray != null && optJSONArray.length() > 0) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    arrayList.add(Integer.valueOf(optJSONArray.optInt(i)));
                }
            }
            return new c(optInt, arrayList);
        } catch (Exception e) {
            e.printStackTrace();
            return cVar;
        }
    }
}
