package com.mintegral.msdk.videocommon.download;

import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.videocommon.download.g;
import java.util.concurrent.ConcurrentMap;

/* compiled from: DownLoadH5SourceListener */
public final class b implements g.b {
    private static String a = "DownLoadH5SourceListener";
    private ConcurrentMap<String, b> b;
    private j c;
    private g.c d;
    private String e;

    public b(ConcurrentMap<String, b> concurrentMap, j jVar, g.c cVar, String str) {
        this.b = concurrentMap;
        this.c = jVar;
        this.d = cVar;
        this.e = str;
    }

    public final void a(g.c cVar) {
        this.d = cVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(byte[] r4, java.lang.String r5) {
        /*
            r3 = this;
            java.lang.String r0 = ""
            java.util.concurrent.ConcurrentMap<java.lang.String, com.mintegral.msdk.videocommon.download.b> r1 = r3.b     // Catch:{ Exception -> 0x004d }
            if (r1 != 0) goto L_0x0012
            com.mintegral.msdk.videocommon.download.g$c r4 = r3.d     // Catch:{ Exception -> 0x004d }
            if (r4 == 0) goto L_0x0011
            com.mintegral.msdk.videocommon.download.g$c r4 = r3.d     // Catch:{ Exception -> 0x004d }
            java.lang.String r1 = "mResDownloadingMap  is null"
            r4.a(r1, r5)     // Catch:{ Exception -> 0x004d }
        L_0x0011:
            return
        L_0x0012:
            java.util.concurrent.ConcurrentMap<java.lang.String, com.mintegral.msdk.videocommon.download.b> r1 = r3.b     // Catch:{ Exception -> 0x004d }
            boolean r1 = r1.containsKey(r5)     // Catch:{ Exception -> 0x004d }
            if (r1 == 0) goto L_0x001f
            java.util.concurrent.ConcurrentMap<java.lang.String, com.mintegral.msdk.videocommon.download.b> r1 = r3.b     // Catch:{ Exception -> 0x004d }
            r1.remove(r5)     // Catch:{ Exception -> 0x004d }
        L_0x001f:
            if (r4 == 0) goto L_0x004a
            int r1 = r4.length     // Catch:{ Exception -> 0x004d }
            if (r1 <= 0) goto L_0x004a
            com.mintegral.msdk.videocommon.download.j r1 = r3.c     // Catch:{ Exception -> 0x004d }
            java.lang.String r4 = r1.a(r5, r4)     // Catch:{ Exception -> 0x004d }
            boolean r1 = android.text.TextUtils.isEmpty(r4)     // Catch:{ Exception -> 0x004d }
            if (r1 == 0) goto L_0x003a
            com.mintegral.msdk.videocommon.download.g$c r4 = r3.d     // Catch:{ Exception -> 0x004d }
            if (r4 == 0) goto L_0x0064
            com.mintegral.msdk.videocommon.download.g$c r4 = r3.d     // Catch:{ Exception -> 0x004d }
            r4.a(r5)     // Catch:{ Exception -> 0x004d }
            return
        L_0x003a:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004d }
            java.lang.String r2 = "data save failed:"
            r1.<init>(r2)     // Catch:{ Exception -> 0x004d }
            r1.append(r4)     // Catch:{ Exception -> 0x004d }
            java.lang.String r4 = r1.toString()     // Catch:{ Exception -> 0x004d }
            r0 = r4
            goto L_0x0064
        L_0x004a:
            java.lang.String r4 = "response data is error"
            goto L_0x0065
        L_0x004d:
            r4 = move-exception
            boolean r1 = com.mintegral.msdk.MIntegralConstans.DEBUG
            if (r1 == 0) goto L_0x0055
            r4.printStackTrace()
        L_0x0055:
            java.lang.String r4 = r4.getMessage()     // Catch:{ Throwable -> 0x005a }
            goto L_0x0065
        L_0x005a:
            r4 = move-exception
            java.lang.String r1 = com.mintegral.msdk.videocommon.download.b.a
            java.lang.String r2 = r4.getMessage()
            com.mintegral.msdk.base.utils.g.c(r1, r2, r4)
        L_0x0064:
            r4 = r0
        L_0x0065:
            com.mintegral.msdk.videocommon.download.g$c r0 = r3.d
            if (r0 == 0) goto L_0x006e
            com.mintegral.msdk.videocommon.download.g$c r0 = r3.d
            r0.a(r4, r5)
        L_0x006e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.videocommon.download.b.a(byte[], java.lang.String):void");
    }

    public final void a(String str) {
        try {
            if (this.b != null) {
                if (this.b.containsKey(this.e)) {
                    this.b.remove(this.e);
                }
                if (this.d != null) {
                    this.d.a(str, this.e);
                }
            } else if (this.d != null) {
                this.d.a("mResDownloadingMap  is null", this.e);
            }
        } catch (Exception e2) {
            if (MIntegralConstans.DEBUG) {
                e2.printStackTrace();
            }
            str = e2.getMessage();
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c(a, th.getMessage(), th);
        }
    }
}
