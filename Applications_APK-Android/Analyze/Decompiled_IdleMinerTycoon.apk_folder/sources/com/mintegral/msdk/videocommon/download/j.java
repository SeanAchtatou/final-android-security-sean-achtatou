package com.mintegral.msdk.videocommon.download;

import android.net.Uri;
import android.text.TextUtils;
import com.appsflyer.share.Constants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.b.c;
import com.mintegral.msdk.base.common.b.e;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.d;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.videocommon.download.f;
import com.vungle.warren.model.Advertisement;
import java.io.File;
import java.util.List;

/* compiled from: ResourceManager */
public final class j {
    private static String a = "ResourceManager";
    /* access modifiers changed from: private */
    public String b;

    /* compiled from: ResourceManager */
    private static class a {
        public static j a = new j((byte) 0);
    }

    /* synthetic */ j(byte b2) {
        this();
    }

    private j() {
        this.b = e.b(c.MINTEGRAL_700_RES);
    }

    public static j a() {
        return a.a;
    }

    public final void b() {
        try {
            if (!TextUtils.isEmpty(this.b)) {
                f.a.a.a(new com.mintegral.msdk.base.common.f.a() {
                    public final void b() {
                    }

                    public final void a() {
                        com.mintegral.msdk.base.utils.e.c(j.this.b);
                    }
                });
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    public final String a(String str, byte[] bArr) {
        String str2 = "unknow exception ";
        if (bArr != null) {
            try {
                if (bArr.length > 0) {
                    String str3 = this.b + Constants.URL_PATH_DELIMITER + CommonMD5.getMD5(str) + ".zip";
                    File file = new File(str3);
                    if (com.mintegral.msdk.base.utils.e.a(bArr, file)) {
                        Uri parse = Uri.parse(str);
                        List<String> queryParameters = parse.getQueryParameters("nc");
                        if (queryParameters != null) {
                            if (queryParameters.size() != 0) {
                                str2 = a(str, str3, file);
                            }
                        }
                        List<String> queryParameters2 = parse.getQueryParameters("md5filename");
                        if (queryParameters2 != null && queryParameters2.size() > 0) {
                            String str4 = queryParameters2.get(0);
                            if (!TextUtils.isEmpty(str4) && str4.equals(d.a(file))) {
                                str2 = a(str, str3, file);
                            }
                        }
                    }
                    if (!TextUtils.isEmpty(str2)) {
                        com.mintegral.msdk.base.utils.e.b(file);
                    }
                }
            } catch (Exception e) {
                if (MIntegralConstans.DEBUG) {
                    e.printStackTrace();
                }
                return e.getMessage();
            }
        }
        return str2;
    }

    private String a(String str, String str2, File file) {
        String a2 = com.mintegral.msdk.base.utils.e.a(str2, this.b + Constants.URL_PATH_DELIMITER + CommonMD5.getMD5(str));
        return TextUtils.isEmpty(a2) ? com.mintegral.msdk.base.utils.e.b(file) : a2;
    }

    public final String a(String str) {
        String str2;
        String str3;
        try {
            String str4 = this.b + Constants.URL_PATH_DELIMITER + CommonMD5.getMD5(str);
            List<String> queryParameters = Uri.parse(str).getQueryParameters("foldername");
            g.a(a, "check zip 下载情况：url:" + str);
            g.a(a, "check zip 下载情况：indexHtml:" + queryParameters);
            if (queryParameters == null || queryParameters.size() <= 0) {
                return null;
            }
            String str5 = queryParameters.get(0);
            if (TextUtils.isEmpty(str5)) {
                return null;
            }
            String str6 = str4 + Constants.URL_PATH_DELIMITER + str5 + Constants.URL_PATH_DELIMITER + str5 + ".html";
            if (!com.mintegral.msdk.base.utils.e.a(str6)) {
                return null;
            }
            try {
                str2 = str.substring(str.indexOf("?") + 1);
            } catch (Exception unused) {
                str2 = "";
            }
            if (!TextUtils.isEmpty(str2)) {
                str3 = "?" + str2;
            } else {
                str3 = "";
            }
            return Advertisement.FILE_SCHEME + str6 + str3;
        } catch (Exception e) {
            if (!MIntegralConstans.DEBUG) {
                return null;
            }
            e.printStackTrace();
            return null;
        }
    }
}
