package com.mintegral.msdk.videocommon;

public final class R {
    private R() {
    }

    public static final class color {
        public static final int mintegral_video_common_alertview_bg = 2131034289;
        public static final int mintegral_video_common_alertview_cancel_button_bg_default = 2131034290;
        public static final int mintegral_video_common_alertview_cancel_button_bg_pressed = 2131034291;
        public static final int mintegral_video_common_alertview_cancel_button_textcolor = 2131034292;
        public static final int mintegral_video_common_alertview_confirm_button_bg_default = 2131034293;
        public static final int mintegral_video_common_alertview_confirm_button_bg_pressed = 2131034294;
        public static final int mintegral_video_common_alertview_confirm_button_textcolor = 2131034295;
        public static final int mintegral_video_common_alertview_content_textcolor = 2131034296;
        public static final int mintegral_video_common_alertview_title_textcolor = 2131034297;

        private color() {
        }
    }

    public static final class dimen {
        public static final int mintegral_video_common_alertview_bg_padding = 2131099851;
        public static final int mintegral_video_common_alertview_button_height = 2131099852;
        public static final int mintegral_video_common_alertview_button_margintop = 2131099853;
        public static final int mintegral_video_common_alertview_button_radius = 2131099854;
        public static final int mintegral_video_common_alertview_button_textsize = 2131099855;
        public static final int mintegral_video_common_alertview_button_width = 2131099856;
        public static final int mintegral_video_common_alertview_content_margintop = 2131099857;
        public static final int mintegral_video_common_alertview_content_size = 2131099858;
        public static final int mintegral_video_common_alertview_contentview_maxwidth = 2131099859;
        public static final int mintegral_video_common_alertview_contentview_minwidth = 2131099860;
        public static final int mintegral_video_common_alertview_title_size = 2131099861;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int mintegral_video_common_alertview_bg = 2131165431;
        public static final int mintegral_video_common_alertview_cancel_bg = 2131165432;
        public static final int mintegral_video_common_alertview_cancel_bg_nor = 2131165433;
        public static final int mintegral_video_common_alertview_cancel_bg_pressed = 2131165434;
        public static final int mintegral_video_common_alertview_confirm_bg = 2131165435;
        public static final int mintegral_video_common_alertview_confirm_bg_nor = 2131165436;
        public static final int mintegral_video_common_alertview_confirm_bg_pressed = 2131165437;
        public static final int mintegral_video_common_full_star = 2131165438;
        public static final int mintegral_video_common_full_while_star = 2131165439;
        public static final int mintegral_video_common_half_star = 2131165440;

        private drawable() {
        }
    }

    public static final class id {
        public static final int mintegral_video_common_alertview_cancel_button = 2131230993;
        public static final int mintegral_video_common_alertview_confirm_button = 2131230994;
        public static final int mintegral_video_common_alertview_contentview = 2131230995;
        public static final int mintegral_video_common_alertview_titleview = 2131230996;

        private id() {
        }
    }

    public static final class layout {
        public static final int mintegral_video_common_alertview = 2131427457;

        private layout() {
        }
    }
}
