package com.mintegral.msdk.videocommon.download;

import android.text.TextUtils;
import com.mintegral.msdk.base.common.c.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ResDownloadCheckManager */
public final class i {
    private static Map<String, Boolean> b = new HashMap();
    private Map<String, Boolean> a;
    private Map<String, Boolean> c;
    private Map<String, Boolean> d;

    /* compiled from: ResDownloadCheckManager */
    private static final class a {
        public static i a = new i((byte) 0);
    }

    /* synthetic */ i(byte b2) {
        this();
    }

    private i() {
        this.a = new HashMap();
        this.c = new HashMap();
        this.d = new HashMap();
    }

    public static i a() {
        return a.a;
    }

    public final void a(List<CampaignEx> list) {
        List<CampaignEx.c.a> e;
        List<String> list2;
        if (list != null && list.size() != 0) {
            for (CampaignEx next : list) {
                if (next != null) {
                    if (next != null) {
                        String videoUrlEncode = next.getVideoUrlEncode();
                        if (this.a != null && !this.a.containsKey(videoUrlEncode)) {
                            this.a.put(videoUrlEncode, false);
                        }
                        String str = next.getendcard_url();
                        if (this.c != null && !this.c.containsKey(str)) {
                            this.c.put(str, false);
                        }
                        CampaignEx.c rewardTemplateMode = next.getRewardTemplateMode();
                        if (!(rewardTemplateMode == null || (e = rewardTemplateMode.e()) == null)) {
                            for (CampaignEx.c.a next2 : e) {
                                if (!(next2 == null || (list2 = next2.b) == null || list2.size() == 0)) {
                                    for (String next3 : list2) {
                                        if (!TextUtils.isEmpty(next3) && b != null && !b.containsKey(next3)) {
                                            b.put(next3, Boolean.valueOf(b.a(com.mintegral.msdk.base.controller.a.d().h()).b(next3)));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    String id = next.getId();
                    if (this.d == null) {
                        this.d = new HashMap();
                    }
                    this.d.put(id, false);
                }
            }
        }
    }

    public final void a(String str) {
        if (this.a != null) {
            this.a = new HashMap();
        }
        this.a.put(str, true);
    }

    public final void b(String str) {
        if (this.c != null) {
            this.c = new HashMap();
        }
        this.c.put(str, true);
    }

    public static void c(String str) {
        if (b != null) {
            b = new HashMap();
        }
        b.put(str, true);
    }

    public static boolean a(CampaignEx.c cVar) {
        List<CampaignEx.c.a> e;
        boolean z;
        if (!(cVar == null || (e = cVar.e()) == null)) {
            for (CampaignEx.c.a next : e) {
                if (!(next == null || next.b == null)) {
                    for (String next2 : next.b) {
                        Map<String, Boolean> map = b;
                        if (TextUtils.isEmpty(next2)) {
                            z = true;
                        } else {
                            if (map == null) {
                                new HashMap().put(next2, false);
                            } else if (map.containsKey(next2)) {
                                z = map.get(next2).booleanValue();
                            } else {
                                map.put(next2, false);
                            }
                            z = false;
                        }
                        if (z || b.a(com.mintegral.msdk.base.controller.a.d().h()).b(next2)) {
                            z = true;
                            continue;
                        }
                        if (!z) {
                            return false;
                        }
                    }
                    continue;
                }
            }
        }
        return true;
    }
}
