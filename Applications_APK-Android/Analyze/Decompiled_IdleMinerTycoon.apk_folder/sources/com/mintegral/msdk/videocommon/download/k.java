package com.mintegral.msdk.videocommon.download;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.d.d;
import com.mintegral.msdk.videocommon.download.h;
import com.mintegral.msdk.videocommon.e.b;
import com.mintegral.msdk.videocommon.e.c;
import com.mintegral.msdk.videocommon.listener.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;

/* compiled from: UnitCacheCtroller */
public final class k {
    d a = null;
    private List<CampaignEx> b = new ArrayList();
    /* access modifiers changed from: private */
    public boolean c = true;
    private a d;
    private d e = new d() {
        public final void a(long j, int i) {
            if (i == 5 || i == 4) {
                boolean unused = k.this.c = true;
                k.this.a();
            }
            if (i == 2) {
                boolean unused2 = k.this.c = true;
            }
        }
    };
    private CopyOnWriteArrayList<Map<String, a>> f = new CopyOnWriteArrayList<>();
    private Context g = com.mintegral.msdk.base.controller.a.d().h();
    private ExecutorService h;
    private long i = 3600;
    private String j;
    private c k;
    private int l = 2;
    private int m = 1;

    public k(List<CampaignEx> list, ExecutorService executorService, String str, int i2) {
        if (!(this.b == null || list == null)) {
            this.b.addAll(list);
        }
        this.h = executorService;
        this.j = str;
        this.m = i2;
        b(this.b);
    }

    public final void a(a aVar) {
        this.d = aVar;
    }

    public final void a(List<CampaignEx> list) {
        if (!(this.b == null || list == null)) {
            this.b.addAll(list);
        }
        b(this.b);
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:98:0x01c4 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(java.util.List<com.mintegral.msdk.base.entity.CampaignEx> r10) {
        /*
            r9 = this;
            if (r10 == 0) goto L_0x01de
            int r0 = r10.size()
            if (r0 != 0) goto L_0x000a
            goto L_0x01de
        L_0x000a:
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r0 = r9.f
            r1 = 0
            if (r0 == 0) goto L_0x0065
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r0 = r9.f     // Catch:{ Throwable -> 0x005e }
            monitor-enter(r0)     // Catch:{ Throwable -> 0x005e }
            r2 = 0
        L_0x0013:
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r3 = r9.f     // Catch:{ all -> 0x005b }
            int r3 = r3.size()     // Catch:{ all -> 0x005b }
            if (r2 >= r3) goto L_0x0059
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r3 = r9.f     // Catch:{ all -> 0x005b }
            java.lang.Object r3 = r3.get(r2)     // Catch:{ all -> 0x005b }
            java.util.Map r3 = (java.util.Map) r3     // Catch:{ all -> 0x005b }
            java.util.Set r4 = r3.entrySet()     // Catch:{ all -> 0x005b }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x005b }
        L_0x002b:
            boolean r5 = r4.hasNext()     // Catch:{ all -> 0x005b }
            if (r5 == 0) goto L_0x0056
            java.lang.Object r5 = r4.next()     // Catch:{ all -> 0x005b }
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ all -> 0x005b }
            java.lang.Object r5 = r5.getValue()     // Catch:{ all -> 0x005b }
            com.mintegral.msdk.videocommon.download.a r5 = (com.mintegral.msdk.videocommon.download.a) r5     // Catch:{ all -> 0x005b }
            if (r5 == 0) goto L_0x002b
            com.mintegral.msdk.base.entity.CampaignEx r6 = r5.k()     // Catch:{ all -> 0x005b }
            if (r6 == 0) goto L_0x002b
            boolean r6 = r5.a()     // Catch:{ all -> 0x005b }
            if (r6 == 0) goto L_0x002b
            r5.l()     // Catch:{ all -> 0x005b }
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r5 = r9.f     // Catch:{ all -> 0x005b }
            r5.remove(r3)     // Catch:{ all -> 0x005b }
            int r2 = r2 + -1
            goto L_0x002b
        L_0x0056:
            int r2 = r2 + 1
            goto L_0x0013
        L_0x0059:
            monitor-exit(r0)     // Catch:{ all -> 0x005b }
            goto L_0x0065
        L_0x005b:
            r2 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x005b }
            throw r2     // Catch:{ Throwable -> 0x005e }
        L_0x005e:
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.String r2 = "cleanDisplayTask ERROR"
            com.mintegral.msdk.base.utils.g.d(r0, r2)
        L_0x0065:
            int r0 = r9.m
            switch(r0) {
                case 1: goto L_0x00f1;
                case 2: goto L_0x00b9;
                case 3: goto L_0x006c;
                default: goto L_0x006a;
            }
        L_0x006a:
            goto L_0x0135
        L_0x006c:
            java.lang.String r0 = "com.mintegral.msdk.videocommon.e.a"
            java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x00b1 }
            com.mintegral.msdk.videocommon.e.b.a()     // Catch:{ Exception -> 0x00b1 }
            com.mintegral.msdk.videocommon.e.a r0 = com.mintegral.msdk.videocommon.e.b.b()     // Catch:{ Exception -> 0x00b1 }
            if (r0 != 0) goto L_0x0080
            com.mintegral.msdk.videocommon.e.b.a()     // Catch:{ Exception -> 0x00b1 }
            com.mintegral.msdk.videocommon.e.b.c()     // Catch:{ Exception -> 0x00b1 }
        L_0x0080:
            if (r0 == 0) goto L_0x0088
            long r2 = r0.g()     // Catch:{ Exception -> 0x00b1 }
            r9.i = r2     // Catch:{ Exception -> 0x00b1 }
        L_0x0088:
            java.lang.String r0 = r9.j     // Catch:{ Exception -> 0x00b1 }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x00b1 }
            if (r0 != 0) goto L_0x00a3
            com.mintegral.msdk.videocommon.e.b.a()     // Catch:{ Exception -> 0x00b1 }
            com.mintegral.msdk.base.controller.a r0 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r0 = r0.j()     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r2 = r9.j     // Catch:{ Exception -> 0x00b1 }
            com.mintegral.msdk.videocommon.e.c r0 = com.mintegral.msdk.videocommon.e.b.a(r0, r2)     // Catch:{ Exception -> 0x00b1 }
            r9.k = r0     // Catch:{ Exception -> 0x00b1 }
        L_0x00a3:
            com.mintegral.msdk.videocommon.e.c r0 = r9.k     // Catch:{ Exception -> 0x00b1 }
            if (r0 == 0) goto L_0x0135
            com.mintegral.msdk.videocommon.e.c r0 = r9.k     // Catch:{ Exception -> 0x00b1 }
            int r0 = r0.F()     // Catch:{ Exception -> 0x00b1 }
            r9.l = r0     // Catch:{ Exception -> 0x00b1 }
            goto L_0x0135
        L_0x00b1:
            java.lang.String r10 = "UnitCacheCtroller"
            java.lang.String r0 = "make sure your had put reward jar into your project"
            com.mintegral.msdk.base.utils.g.d(r10, r0)
            return
        L_0x00b9:
            java.lang.String r0 = r9.j     // Catch:{ Exception -> 0x00e9 }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x00e9 }
            if (r0 != 0) goto L_0x0135
            com.mintegral.msdk.d.b.a()     // Catch:{ Exception -> 0x00e9 }
            com.mintegral.msdk.base.controller.a r0 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Exception -> 0x00e9 }
            java.lang.String r0 = r0.j()     // Catch:{ Exception -> 0x00e9 }
            java.lang.String r2 = r9.j     // Catch:{ Exception -> 0x00e9 }
            com.mintegral.msdk.d.d r0 = com.mintegral.msdk.d.b.c(r0, r2)     // Catch:{ Exception -> 0x00e9 }
            if (r0 != 0) goto L_0x00da
            java.lang.String r0 = r9.j     // Catch:{ Exception -> 0x00e9 }
            com.mintegral.msdk.d.d r0 = com.mintegral.msdk.d.d.c(r0)     // Catch:{ Exception -> 0x00e9 }
        L_0x00da:
            if (r0 == 0) goto L_0x0135
            long r2 = r0.l()     // Catch:{ Exception -> 0x00e9 }
            r9.i = r2     // Catch:{ Exception -> 0x00e9 }
            int r0 = r0.n()     // Catch:{ Exception -> 0x00e9 }
            r9.l = r0     // Catch:{ Exception -> 0x00e9 }
            goto L_0x0135
        L_0x00e9:
            java.lang.String r10 = "UnitCacheCtroller"
            java.lang.String r0 = "make sure your had put feeds jar into your project"
            com.mintegral.msdk.base.utils.g.d(r10, r0)
            return
        L_0x00f1:
            java.lang.String r0 = r9.j     // Catch:{ Exception -> 0x012d }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x012d }
            if (r0 != 0) goto L_0x0135
            com.mintegral.msdk.d.b.a()     // Catch:{ Exception -> 0x012d }
            com.mintegral.msdk.base.controller.a r0 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Exception -> 0x012d }
            java.lang.String r0 = r0.j()     // Catch:{ Exception -> 0x012d }
            java.lang.String r2 = r9.j     // Catch:{ Exception -> 0x012d }
            com.mintegral.msdk.d.d r0 = com.mintegral.msdk.d.b.c(r0, r2)     // Catch:{ Exception -> 0x012d }
            r9.a = r0     // Catch:{ Exception -> 0x012d }
            com.mintegral.msdk.d.d r0 = r9.a     // Catch:{ Exception -> 0x012d }
            if (r0 != 0) goto L_0x0118
            java.lang.String r0 = r9.j     // Catch:{ Exception -> 0x012d }
            com.mintegral.msdk.d.d r0 = com.mintegral.msdk.d.d.b(r0)     // Catch:{ Exception -> 0x012d }
            r9.a = r0     // Catch:{ Exception -> 0x012d }
        L_0x0118:
            com.mintegral.msdk.d.d r0 = r9.a     // Catch:{ Exception -> 0x012d }
            if (r0 == 0) goto L_0x0135
            com.mintegral.msdk.d.d r0 = r9.a     // Catch:{ Exception -> 0x012d }
            long r2 = r0.l()     // Catch:{ Exception -> 0x012d }
            r9.i = r2     // Catch:{ Exception -> 0x012d }
            com.mintegral.msdk.d.d r0 = r9.a     // Catch:{ Exception -> 0x012d }
            int r0 = r0.n()     // Catch:{ Exception -> 0x012d }
            r9.l = r0     // Catch:{ Exception -> 0x012d }
            goto L_0x0135
        L_0x012d:
            java.lang.String r10 = "UnitCacheCtroller"
            java.lang.String r0 = "make sure your had put feeds jar into your project"
            com.mintegral.msdk.base.utils.g.d(r10, r0)
            return
        L_0x0135:
            r0 = 0
        L_0x0136:
            int r2 = r10.size()
            if (r0 >= r2) goto L_0x01cc
            java.lang.Object r2 = r10.get(r0)
            com.mintegral.msdk.base.entity.CampaignEx r2 = (com.mintegral.msdk.base.entity.CampaignEx) r2
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = r2.getId()
            r3.append(r4)
            java.lang.String r4 = r2.getVideoUrlEncode()
            r3.append(r4)
            java.lang.String r4 = r2.getBidToken()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            if (r2 == 0) goto L_0x01c8
            boolean r4 = a(r2)
            if (r4 != 0) goto L_0x0172
            java.lang.String r4 = r2.getVideoUrlEncode()
            boolean r4 = android.text.TextUtils.isEmpty(r4)
            if (r4 != 0) goto L_0x01c8
        L_0x0172:
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r4 = r9.f
            if (r4 == 0) goto L_0x01c8
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r4 = r9.f
            monitor-enter(r4)
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r5 = r9.f     // Catch:{ Throwable -> 0x01c4 }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ Throwable -> 0x01c4 }
        L_0x017f:
            boolean r6 = r5.hasNext()     // Catch:{ Throwable -> 0x01c4 }
            if (r6 == 0) goto L_0x01a1
            java.lang.Object r6 = r5.next()     // Catch:{ Throwable -> 0x01c4 }
            java.util.Map r6 = (java.util.Map) r6     // Catch:{ Throwable -> 0x01c4 }
            if (r6 == 0) goto L_0x017f
            boolean r7 = r6.containsKey(r3)     // Catch:{ Throwable -> 0x01c4 }
            if (r7 == 0) goto L_0x017f
            java.lang.Object r5 = r6.get(r3)     // Catch:{ Throwable -> 0x01c4 }
            com.mintegral.msdk.videocommon.download.a r5 = (com.mintegral.msdk.videocommon.download.a) r5     // Catch:{ Throwable -> 0x01c4 }
            r5.a(r2)     // Catch:{ Throwable -> 0x01c4 }
            r5.a(r1)     // Catch:{ Throwable -> 0x01c4 }
            r5 = 1
            goto L_0x01a2
        L_0x01a1:
            r5 = 0
        L_0x01a2:
            if (r5 != 0) goto L_0x01c4
            com.mintegral.msdk.videocommon.download.a r5 = new com.mintegral.msdk.videocommon.download.a     // Catch:{ Throwable -> 0x01c4 }
            android.content.Context r6 = r9.g     // Catch:{ Throwable -> 0x01c4 }
            java.util.concurrent.ExecutorService r7 = r9.h     // Catch:{ Throwable -> 0x01c4 }
            java.lang.String r8 = r9.j     // Catch:{ Throwable -> 0x01c4 }
            r5.<init>(r6, r2, r7, r8)     // Catch:{ Throwable -> 0x01c4 }
            int r2 = r9.m     // Catch:{ Throwable -> 0x01c4 }
            r5.b(r2)     // Catch:{ Throwable -> 0x01c4 }
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ Throwable -> 0x01c4 }
            r2.<init>()     // Catch:{ Throwable -> 0x01c4 }
            r2.put(r3, r5)     // Catch:{ Throwable -> 0x01c4 }
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r3 = r9.f     // Catch:{ Throwable -> 0x01c4 }
            r3.add(r2)     // Catch:{ Throwable -> 0x01c4 }
            goto L_0x01c4
        L_0x01c2:
            r10 = move-exception
            goto L_0x01c6
        L_0x01c4:
            monitor-exit(r4)     // Catch:{ all -> 0x01c2 }
            goto L_0x01c8
        L_0x01c6:
            monitor-exit(r4)     // Catch:{ all -> 0x01c2 }
            throw r10
        L_0x01c8:
            int r0 = r0 + 1
            goto L_0x0136
        L_0x01cc:
            java.util.List<com.mintegral.msdk.base.entity.CampaignEx> r10 = r9.b
            if (r10 == 0) goto L_0x01dd
            java.util.List<com.mintegral.msdk.base.entity.CampaignEx> r10 = r9.b
            int r10 = r10.size()
            if (r10 <= 0) goto L_0x01dd
            java.util.List<com.mintegral.msdk.base.entity.CampaignEx> r10 = r9.b
            r10.clear()
        L_0x01dd:
            return
        L_0x01de:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.videocommon.download.k.b(java.util.List):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:105:0x0264 A[Catch:{ Throwable -> 0x0330 }] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x028d A[Catch:{ Throwable -> 0x0330 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x013c A[Catch:{ Throwable -> 0x0330 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0145 A[Catch:{ Throwable -> 0x0330 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.mintegral.msdk.videocommon.download.a a(boolean r25) {
        /*
            r24 = this;
            r1 = r24
            r0 = r25
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r2 = r1.f
            if (r2 == 0) goto L_0x033a
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r2 = r1.f     // Catch:{ Throwable -> 0x0330 }
            monitor-enter(r2)     // Catch:{ Throwable -> 0x0330 }
            java.lang.String r4 = "UnitCacheCtroller"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x032d }
            java.lang.String r6 = "UnitCache isReady ===== campaignDownLoadTaskList.size() = "
            r5.<init>(r6)     // Catch:{ all -> 0x032d }
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r6 = r1.f     // Catch:{ all -> 0x032d }
            int r6 = r6.size()     // Catch:{ all -> 0x032d }
            r5.append(r6)     // Catch:{ all -> 0x032d }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x032d }
            com.mintegral.msdk.base.utils.g.a(r4, r5)     // Catch:{ all -> 0x032d }
            com.mintegral.msdk.base.controller.a r4 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ all -> 0x032d }
            android.content.Context r4 = r4.h()     // Catch:{ all -> 0x032d }
            com.mintegral.msdk.base.b.i r4 = com.mintegral.msdk.base.b.i.a(r4)     // Catch:{ all -> 0x032d }
            com.mintegral.msdk.base.b.f r4 = com.mintegral.msdk.base.b.f.a(r4)     // Catch:{ all -> 0x032d }
            java.lang.String r5 = r1.j     // Catch:{ all -> 0x032d }
            java.util.List r5 = r4.k(r5)     // Catch:{ all -> 0x032d }
            java.lang.String r6 = r1.j     // Catch:{ all -> 0x032d }
            java.util.List r4 = r4.j(r6)     // Catch:{ all -> 0x032d }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x032d }
            r9 = 0
        L_0x0045:
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r10 = r1.f     // Catch:{ all -> 0x032d }
            int r10 = r10.size()     // Catch:{ all -> 0x032d }
            if (r9 >= r10) goto L_0x032b
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r10 = r1.f     // Catch:{ all -> 0x032d }
            java.lang.Object r10 = r10.get(r9)     // Catch:{ all -> 0x032d }
            java.util.Map r10 = (java.util.Map) r10     // Catch:{ all -> 0x032d }
            java.util.Set r11 = r10.entrySet()     // Catch:{ all -> 0x032d }
            java.util.Iterator r11 = r11.iterator()     // Catch:{ all -> 0x032d }
        L_0x005d:
            boolean r12 = r11.hasNext()     // Catch:{ all -> 0x032d }
            if (r12 == 0) goto L_0x0321
            java.lang.Object r12 = r11.next()     // Catch:{ all -> 0x032d }
            java.util.Map$Entry r12 = (java.util.Map.Entry) r12     // Catch:{ all -> 0x032d }
            java.lang.Object r12 = r12.getValue()     // Catch:{ all -> 0x032d }
            com.mintegral.msdk.videocommon.download.a r12 = (com.mintegral.msdk.videocommon.download.a) r12     // Catch:{ all -> 0x032d }
            if (r12 == 0) goto L_0x030e
            com.mintegral.msdk.base.entity.CampaignEx r13 = r12.k()     // Catch:{ all -> 0x032d }
            if (r13 != 0) goto L_0x0079
            goto L_0x030e
        L_0x0079:
            com.mintegral.msdk.base.entity.CampaignEx r13 = r12.k()     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x0085
            boolean r14 = r13.isBidCampaign()     // Catch:{ all -> 0x032d }
            if (r14 == 0) goto L_0x008d
        L_0x0085:
            if (r0 != 0) goto L_0x00ad
            boolean r14 = r13.isBidCampaign()     // Catch:{ all -> 0x032d }
            if (r14 == 0) goto L_0x00ad
        L_0x008d:
            java.lang.String r12 = "UnitCacheCtroller"
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x032d }
            java.lang.String r15 = "UnitCache isReady ==== isBidCampaign = "
            r14.<init>(r15)     // Catch:{ all -> 0x032d }
            r14.append(r0)     // Catch:{ all -> 0x032d }
            java.lang.String r15 = " campaign.isBidCampaign() = "
            r14.append(r15)     // Catch:{ all -> 0x032d }
            boolean r13 = r13.isBidCampaign()     // Catch:{ all -> 0x032d }
            r14.append(r13)     // Catch:{ all -> 0x032d }
            java.lang.String r13 = r14.toString()     // Catch:{ all -> 0x032d }
            com.mintegral.msdk.base.utils.g.b(r12, r13)     // Catch:{ all -> 0x032d }
            goto L_0x00be
        L_0x00ad:
            java.lang.String r14 = r13.getId()     // Catch:{ all -> 0x032d }
            boolean r14 = r5.contains(r14)     // Catch:{ all -> 0x032d }
            if (r14 != 0) goto L_0x00c4
            java.lang.String r12 = "UnitCacheCtroller"
            java.lang.String r13 = "UnitCache isReady ====  Campaign Expired continue"
            com.mintegral.msdk.base.utils.g.b(r12, r13)     // Catch:{ all -> 0x032d }
        L_0x00be:
            r18 = r5
        L_0x00c0:
            r21 = r9
            goto L_0x0319
        L_0x00c4:
            java.lang.String r14 = r13.getendcard_url()     // Catch:{ all -> 0x032d }
            java.lang.String r15 = r13.getVideoUrlEncode()     // Catch:{ all -> 0x032d }
            java.lang.String r16 = ""
            if (r13 == 0) goto L_0x00de
            com.mintegral.msdk.base.entity.CampaignEx$c r17 = r13.getRewardTemplateMode()     // Catch:{ all -> 0x032d }
            if (r17 == 0) goto L_0x00de
            com.mintegral.msdk.base.entity.CampaignEx$c r16 = r13.getRewardTemplateMode()     // Catch:{ all -> 0x032d }
            java.lang.String r16 = r16.d()     // Catch:{ all -> 0x032d }
        L_0x00de:
            r8 = r16
            com.mintegral.msdk.base.entity.CampaignEx$c r16 = r13.getRewardTemplateMode()     // Catch:{ all -> 0x032d }
            int r3 = r1.m     // Catch:{ all -> 0x032d }
            r0 = 3
            r18 = r5
            if (r3 != r0) goto L_0x018b
            boolean r3 = r4.contains(r15)     // Catch:{ all -> 0x032d }
            if (r3 != 0) goto L_0x00f9
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.String r3 = "UnitCache isReady ====  videoUrlList not contain videoURl continue"
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ all -> 0x032d }
            goto L_0x00c0
        L_0x00f9:
            java.lang.String r3 = "UnitCacheCtroller"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x032d }
            java.lang.String r5 = "check template"
            r0.<init>(r5)     // Catch:{ all -> 0x032d }
            r0.append(r8)     // Catch:{ all -> 0x032d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x032d }
            com.mintegral.msdk.base.utils.g.a(r3, r0)     // Catch:{ all -> 0x032d }
            boolean r0 = android.text.TextUtils.isEmpty(r8)     // Catch:{ all -> 0x032d }
            if (r0 != 0) goto L_0x0139
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x032d }
            java.lang.String r5 = "check template 下载情况："
            r3.<init>(r5)     // Catch:{ all -> 0x032d }
            com.mintegral.msdk.videocommon.download.g r5 = com.mintegral.msdk.videocommon.download.g.a()     // Catch:{ all -> 0x032d }
            java.lang.String r5 = r5.a(r8)     // Catch:{ all -> 0x032d }
            r3.append(r5)     // Catch:{ all -> 0x032d }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x032d }
            com.mintegral.msdk.base.utils.g.a(r0, r3)     // Catch:{ all -> 0x032d }
            com.mintegral.msdk.videocommon.download.g r0 = com.mintegral.msdk.videocommon.download.g.a()     // Catch:{ all -> 0x032d }
            java.lang.String r0 = r0.a(r8)     // Catch:{ all -> 0x032d }
            if (r0 != 0) goto L_0x0139
            r0 = 0
            goto L_0x013a
        L_0x0139:
            r0 = 1
        L_0x013a:
            if (r0 != 0) goto L_0x0145
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.String r3 = "UnitCache isReady ====  templateZipDownload check false continue"
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ all -> 0x032d }
            goto L_0x00c0
        L_0x0145:
            boolean r0 = b(r14, r13)     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x018b
            boolean r0 = r12.a()     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x015d
            r12.l()     // Catch:{ all -> 0x032d }
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.String r3 = "isready endcard下载完 但是offer展示过 continue"
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ all -> 0x032d }
            goto L_0x00c0
        L_0x015d:
            boolean r0 = com.mintegral.msdk.base.utils.s.a(r15)     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x0172
            boolean r0 = a(r16)     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x018b
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.String r3 = "endcard为基准 endcard和图片下载完成 videourl为空不用下载 return task"
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ all -> 0x032d }
            monitor-exit(r2)     // Catch:{ all -> 0x032d }
            return r12
        L_0x0172:
            int r0 = r24.d()     // Catch:{ all -> 0x032d }
            boolean r0 = a(r12, r0)     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x018b
            boolean r0 = a(r16)     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x018b
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.String r3 = "endcard为基准 endcard 图片 和 videourl 下载完成 return task"
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ all -> 0x032d }
            monitor-exit(r2)     // Catch:{ all -> 0x032d }
            return r12
        L_0x018b:
            java.lang.String r0 = r12.j()     // Catch:{ all -> 0x032d }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x032d }
            int r3 = r12.h()     // Catch:{ all -> 0x032d }
            java.lang.String r5 = "UnitCacheCtroller"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x032d }
            java.lang.String r15 = "isready unit state:"
            r8.<init>(r15)     // Catch:{ all -> 0x032d }
            r8.append(r3)     // Catch:{ all -> 0x032d }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x032d }
            com.mintegral.msdk.base.utils.g.a(r5, r8)     // Catch:{ all -> 0x032d }
            r5 = 5
            r8 = 2
            if (r3 != r5) goto L_0x0219
            boolean r3 = r12.a()     // Catch:{ all -> 0x032d }
            if (r3 == 0) goto L_0x01c9
            r12.l()     // Catch:{ all -> 0x032d }
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r0 = r1.f     // Catch:{ all -> 0x032d }
            r0.remove(r10)     // Catch:{ all -> 0x032d }
            int r9 = r9 + -1
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.String r3 = "isready state == DownLoadConstant.DOWNLOAD_DONE 但是offer展示过 continue"
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ all -> 0x032d }
        L_0x01c5:
            r5 = r18
            goto L_0x031d
        L_0x01c9:
            if (r0 != 0) goto L_0x01fa
            r12.i()     // Catch:{ all -> 0x032d }
            int r3 = r1.m     // Catch:{ all -> 0x032d }
            if (r3 != r8) goto L_0x01f1
            java.lang.String r3 = "UnitCacheCtroller"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x032d }
            java.lang.String r5 = "isready ==========done but isEffectivePath:"
            r4.<init>(r5)     // Catch:{ all -> 0x032d }
            r4.append(r0)     // Catch:{ all -> 0x032d }
            java.lang.String r0 = " is feed"
            r4.append(r0)     // Catch:{ all -> 0x032d }
            int r0 = r1.m     // Catch:{ all -> 0x032d }
            r4.append(r0)     // Catch:{ all -> 0x032d }
            java.lang.String r0 = r4.toString()     // Catch:{ all -> 0x032d }
            com.mintegral.msdk.base.utils.g.b(r3, r0)     // Catch:{ all -> 0x032d }
            monitor-exit(r2)     // Catch:{ all -> 0x032d }
            return r12
        L_0x01f1:
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.String r3 = "isready !isEffectivePath continue"
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ all -> 0x032d }
            goto L_0x00c0
        L_0x01fa:
            boolean r0 = a(r14, r13)     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x020f
            boolean r0 = a(r16)     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x020f
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.String r3 = "isready videourl为基准 state＝done endcard 图片 和 videourl 下载完成 return task"
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ all -> 0x032d }
            monitor-exit(r2)     // Catch:{ all -> 0x032d }
            return r12
        L_0x020f:
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.String r3 = "isready done but continue"
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ all -> 0x032d }
            monitor-exit(r2)     // Catch:{ all -> 0x032d }
            r3 = 0
            return r3
        L_0x0219:
            long r19 = r12.b()     // Catch:{ all -> 0x032d }
            int r0 = r12.h()     // Catch:{ all -> 0x032d }
            r5 = 1
            if (r0 != r5) goto L_0x025b
            java.lang.Runnable r0 = r12.e()     // Catch:{ all -> 0x032d }
            r5 = 0
            long r19 = r6 - r19
            r21 = r9
            long r8 = r1.i     // Catch:{ all -> 0x032d }
            r22 = 1000(0x3e8, double:4.94E-321)
            long r8 = r8 * r22
            int r5 = (r19 > r8 ? 1 : (r19 == r8 ? 0 : -1))
            if (r5 > 0) goto L_0x0239
            if (r0 != 0) goto L_0x025d
        L_0x0239:
            java.lang.String r0 = "download timeout"
            r12.a(r0)     // Catch:{ all -> 0x032d }
            r12.o()     // Catch:{ all -> 0x032d }
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r0 = r1.f     // Catch:{ all -> 0x032d }
            r0.remove(r10)     // Catch:{ all -> 0x032d }
            int r9 = r21 + -1
            int r0 = r1.m     // Catch:{ all -> 0x032d }
            r5 = 1
            if (r0 == r5) goto L_0x0252
            int r0 = r1.m     // Catch:{ all -> 0x032d }
            r5 = 3
            if (r0 != r5) goto L_0x025f
        L_0x0252:
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.String r3 = "isready download !timeout continue"
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ all -> 0x032d }
            goto L_0x01c5
        L_0x025b:
            r21 = r9
        L_0x025d:
            r9 = r21
        L_0x025f:
            int r0 = r1.m     // Catch:{ all -> 0x032d }
            r5 = 2
            if (r0 != r5) goto L_0x028d
            boolean r0 = r12.a()     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x0276
            r12.l()     // Catch:{ all -> 0x032d }
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r0 = r1.f     // Catch:{ all -> 0x032d }
            r0.remove(r10)     // Catch:{ all -> 0x032d }
            int r9 = r9 + -1
            goto L_0x01c5
        L_0x0276:
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x032d }
            java.lang.String r4 = "==========isready ad_type is :"
            r3.<init>(r4)     // Catch:{ all -> 0x032d }
            int r4 = r1.m     // Catch:{ all -> 0x032d }
            r3.append(r4)     // Catch:{ all -> 0x032d }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x032d }
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ all -> 0x032d }
            monitor-exit(r2)     // Catch:{ all -> 0x032d }
            return r12
        L_0x028d:
            r0 = 4
            if (r3 == r0) goto L_0x02fb
            r0 = 2
            if (r3 != r0) goto L_0x0294
            goto L_0x02fb
        L_0x0294:
            r0 = 1
            if (r3 != r0) goto L_0x02d7
            boolean r0 = r12.a()     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x02a6
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.String r3 = "isready run 已经被展示过 continue"
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ all -> 0x032d }
            goto L_0x01c5
        L_0x02a6:
            boolean r0 = com.mintegral.msdk.MIntegralConstans.IS_DOWANLOAD_FINSH_PLAY     // Catch:{ all -> 0x032d }
            if (r0 != 0) goto L_0x02d7
            int r0 = r24.d()     // Catch:{ all -> 0x032d }
            boolean r0 = a(r12, r0)     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x02d7
            boolean r0 = a(r14, r13)     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x02d7
            boolean r0 = a(r16)     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x02d7
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x032d }
            java.lang.String r4 = "isready  IS_DOWANLOAD_FINSH_PLAY is :"
            r3.<init>(r4)     // Catch:{ all -> 0x032d }
            boolean r4 = com.mintegral.msdk.MIntegralConstans.IS_DOWANLOAD_FINSH_PLAY     // Catch:{ all -> 0x032d }
            r3.append(r4)     // Catch:{ all -> 0x032d }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x032d }
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ all -> 0x032d }
            monitor-exit(r2)     // Catch:{ all -> 0x032d }
            return r12
        L_0x02d7:
            int r0 = r1.m     // Catch:{ all -> 0x032d }
            r3 = 3
            if (r0 != r3) goto L_0x01c5
            int r0 = r24.d()     // Catch:{ all -> 0x032d }
            boolean r0 = a(r12, r0)     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x01c5
            boolean r0 = a(r14, r13)     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x01c5
            boolean r0 = a(r16)     // Catch:{ all -> 0x032d }
            if (r0 == 0) goto L_0x01c5
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.String r3 = "isready ad_type == CommonConst.REWARD_VIDEO_AD_TYPE ready_rate 三个条件is true"
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ all -> 0x032d }
            monitor-exit(r2)     // Catch:{ all -> 0x032d }
            return r12
        L_0x02fb:
            java.util.concurrent.CopyOnWriteArrayList<java.util.Map<java.lang.String, com.mintegral.msdk.videocommon.download.a>> r0 = r1.f     // Catch:{ all -> 0x032d }
            r0.remove(r10)     // Catch:{ all -> 0x032d }
            r12.o()     // Catch:{ all -> 0x032d }
            int r9 = r9 + -1
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.String r3 = "isready stop continue"
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ all -> 0x032d }
            goto L_0x01c5
        L_0x030e:
            r18 = r5
            r21 = r9
            java.lang.String r0 = "UnitCacheCtroller"
            java.lang.String r3 = "UnitCache isReady ==== task 或者 campaign为空 continue"
            com.mintegral.msdk.base.utils.g.b(r0, r3)     // Catch:{ all -> 0x032d }
        L_0x0319:
            r5 = r18
            r9 = r21
        L_0x031d:
            r0 = r25
            goto L_0x005d
        L_0x0321:
            r18 = r5
            r21 = r9
            int r9 = r21 + 1
            r0 = r25
            goto L_0x0045
        L_0x032b:
            monitor-exit(r2)     // Catch:{ all -> 0x032d }
            goto L_0x033a
        L_0x032d:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x032d }
            throw r0     // Catch:{ Throwable -> 0x0330 }
        L_0x0330:
            r0 = move-exception
            java.lang.String r2 = "UnitCacheCtroller"
            java.lang.String r3 = r0.getMessage()
            com.mintegral.msdk.base.utils.g.c(r2, r3, r0)
        L_0x033a:
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.videocommon.download.k.a(boolean):com.mintegral.msdk.videocommon.download.a");
    }

    public static boolean a(a aVar, int i2) {
        long m2 = aVar.m();
        long d2 = aVar.d();
        g.b("UnitCacheCtroller", "=========downloaded_file_size:" + m2 + "--file_size:" + d2 + "---ready_rate:" + i2 + "--getVideoUrlEncode:" + aVar.k().getVideoUrlEncode());
        if (i2 == 0) {
            return aVar.k() != null && !TextUtils.isEmpty(aVar.k().getVideoUrlEncode());
        }
        if (d2 > 0 && m2 * 100 >= d2 * ((long) i2)) {
            if (i2 != 100 || aVar.h() == 5) {
                return true;
            }
            aVar.o();
            return false;
        }
    }

    private static boolean a(String str, CampaignEx campaignEx) {
        try {
            if (s.a(str)) {
                g.b("UnitCacheCtroller", "checkEndcardDownload endcardUrl is null return true");
                return true;
            }
            if (b(str, campaignEx)) {
                g.b("UnitCacheCtroller", "checkEndcardDownload endcardUrl done return true");
                return true;
            }
            g.b("UnitCacheCtroller", "checkEndcardDownload endcardUrl return false");
            return false;
        } catch (Throwable th) {
            g.c("UnitCacheCtroller", th.getMessage(), th);
        }
    }

    private static boolean b(String str, CampaignEx campaignEx) {
        if (campaignEx.isMraid()) {
            g.b("UnitCacheCtroller", "Campaign is Mraid, do not need download endcardurl");
            return true;
        } else if (s.b(g.a().a(str))) {
            g.b("UnitCacheCtroller", "endcard zip 下载完成 return true endcardUrl:" + str);
            return true;
        } else if (s.b(h.a.a.a(str))) {
            g.b("UnitCacheCtroller", "endcard url 源码 下载完成 return true endcardUrl:" + str);
            return true;
        } else {
            g.b("UnitCacheCtroller", "checkEndcardZipOrSourceDownLoad endcardUrl return false endcardUrl:" + str);
            return false;
        }
    }

    private static boolean a(CampaignEx.c cVar) {
        try {
            i.a();
            boolean a2 = i.a(cVar);
            g.b("UnitCacheCtroller", "checkImageListDownload checkImageState:" + a2);
            return a2;
        } catch (Throwable th) {
            g.c("UnitCacheCtroller", th.getMessage(), th);
            g.b("UnitCacheCtroller", "checkImageListDownload checkImageState:false");
            return false;
        }
    }

    public final a b(boolean z) {
        try {
            return a(z);
        } catch (Throwable th) {
            g.c("UnitCacheCtroller", th.getMessage(), th);
            return null;
        }
    }

    public final a a(String str) {
        if (this.f == null) {
            return null;
        }
        synchronized (this.f) {
            try {
                Iterator<Map<String, a>> it = this.f.iterator();
                while (it.hasNext()) {
                    Map next = it.next();
                    if (next != null && next.containsKey(str)) {
                        a aVar = (a) next.get(str);
                        return aVar;
                    }
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return null;
    }

    private static boolean a(CopyOnWriteArrayList<Map<String, a>> copyOnWriteArrayList) {
        try {
            Iterator<Map<String, a>> it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                Map next = it.next();
                if (next != null) {
                    for (Map.Entry value : next.entrySet()) {
                        if (((a) value.getValue()).h() == 1) {
                            return true;
                        }
                    }
                    continue;
                }
            }
            return false;
        } catch (Throwable th) {
            if (!MIntegralConstans.DEBUG) {
                return false;
            }
            th.printStackTrace();
            return false;
        }
    }

    public final void b() {
        if (this.f != null) {
            try {
                synchronized (this.f) {
                    Iterator<Map<String, a>> it = this.f.iterator();
                    while (it.hasNext()) {
                        Map next = it.next();
                        if (next != null) {
                            for (Map.Entry value : next.entrySet()) {
                                a aVar = (a) value.getValue();
                                if (aVar != null && aVar.h() == 1 && aVar.g()) {
                                    g.b("UnitCacheCtroller", "暂停所有下载");
                                    aVar.a("playing and stop download");
                                    aVar.o();
                                    this.f.remove(next);
                                    return;
                                }
                            }
                            continue;
                        }
                    }
                }
            } catch (Throwable unused) {
            }
        }
    }

    public final CopyOnWriteArrayList<Map<String, a>> c() {
        return this.f;
    }

    private static boolean a(CampaignEx campaignEx) {
        if (campaignEx == null) {
            return false;
        }
        try {
            if (campaignEx.getPlayable_ads_without_video() == 2) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            if (!MIntegralConstans.DEBUG) {
                return false;
            }
            th.printStackTrace();
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.videocommon.e.b.a(java.lang.String, java.lang.String, boolean):com.mintegral.msdk.videocommon.e.c
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mintegral.msdk.videocommon.e.b.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mintegral.msdk.videocommon.e.b.a(java.lang.String, java.lang.String, boolean):com.mintegral.msdk.videocommon.e.c */
    private int d() {
        try {
            if (this.k == null) {
                b.a();
                this.k = b.a(com.mintegral.msdk.base.controller.a.d().j(), this.j, false);
            }
            return this.k.s();
        } catch (Throwable th) {
            g.c("UnitCacheCtroller", th.getMessage(), th);
            return 100;
        }
    }

    public final void a() {
        if (this.f != null) {
            try {
                synchronized (this.f) {
                    long currentTimeMillis = System.currentTimeMillis();
                    int i2 = 0;
                    while (i2 < this.f.size()) {
                        Map map = this.f.get(i2);
                        for (Map.Entry value : map.entrySet()) {
                            a aVar = (a) value.getValue();
                            if (aVar != null) {
                                if (currentTimeMillis - aVar.b() > this.i * 1000 && aVar.h() == 1) {
                                    aVar.a("download timeout");
                                    aVar.o();
                                    this.f.remove(map);
                                    i2--;
                                }
                                if (!(aVar.h() == 1 || aVar.h() == 5 || aVar.h() == 0)) {
                                    aVar.o();
                                    this.f.remove(map);
                                    i2--;
                                }
                            }
                        }
                        i2++;
                    }
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        if (this.f != null) {
            try {
                synchronized (this.f) {
                    if (!a(this.f)) {
                        this.c = true;
                    }
                    Iterator<Map<String, a>> it = this.f.iterator();
                    while (it.hasNext()) {
                        Map next = it.next();
                        if (next != null) {
                            for (Map.Entry value2 : next.entrySet()) {
                                a aVar2 = (a) value2.getValue();
                                if (aVar2 != null && !aVar2.a()) {
                                    if (this.m == 2) {
                                        this.c = true;
                                    }
                                    int h2 = aVar2.h();
                                    CampaignEx k2 = aVar2.k();
                                    if (k2 != null && h2 == 0) {
                                        c instance = c.getInstance();
                                        k2.getId();
                                        h2 = instance.a(k2.getVideoUrlEncode(), aVar2.n());
                                    }
                                    aVar2.a(this.d);
                                    if (!(h2 == 1 || h2 == 5 || h2 == 4)) {
                                        if (com.mintegral.msdk.base.utils.c.s(this.g) != 9 && this.l == 2) {
                                            return;
                                        }
                                        if (this.l != 3) {
                                            if (h2 == 2 || this.c) {
                                                this.c = false;
                                                aVar2.b(this.e);
                                                int d2 = d();
                                                if (this.m == 1) {
                                                    if (this.a == null) {
                                                        this.a = d.b(this.j);
                                                    }
                                                    d2 = this.a.f();
                                                }
                                                aVar2.a(d2);
                                                aVar2.f();
                                                if (this.m == 3) {
                                                    return;
                                                }
                                            }
                                        } else {
                                            return;
                                        }
                                    }
                                }
                            }
                            continue;
                        }
                    }
                }
            } catch (Throwable th2) {
                th2.printStackTrace();
            }
        }
    }
}
