package com.mintegral.msdk.videocommon.a;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.b.f;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.videocommon.e.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: VideoCampaignCache */
public class a {
    private static final String a = "com.mintegral.msdk.videocommon.a.a";
    private static a b;
    private f c;

    private a() {
        try {
            Context h = com.mintegral.msdk.base.controller.a.d().h();
            if (h != null) {
                this.c = f.a(i.a(h));
            } else {
                g.d(a, "RewardCampaignCache get Context is null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static a a() {
        if (b == null) {
            synchronized (a.class) {
                if (b == null) {
                    b = new a();
                }
            }
        }
        return b;
    }

    public final boolean a(String str, long j) {
        List<CampaignEx> a2;
        long currentTimeMillis = System.currentTimeMillis();
        long j2 = currentTimeMillis - j;
        try {
            if (!TextUtils.isEmpty(str) && (a2 = this.c.a(str, 0, 0, 1)) != null) {
                for (CampaignEx next : a2) {
                    if ((next.getPlct() > 0 && (next.getPlct() * 1000) + next.getTimestamp() >= currentTimeMillis) || (next.getPlct() <= 0 && next.getTimestamp() >= j2)) {
                        return false;
                    }
                }
            }
        } catch (Throwable th) {
            g.a(a, th.getMessage(), th);
        }
        return true;
    }

    public final List<CampaignEx> a(String str, int i) {
        ArrayList arrayList;
        List<CampaignEx> a2;
        try {
            if (TextUtils.isEmpty(str) || (a2 = this.c.a(str, 0, 0, i)) == null) {
                return null;
            }
            arrayList = new ArrayList();
            try {
                for (CampaignEx next : a2) {
                    if (next != null) {
                        arrayList.add(next);
                    }
                }
                return arrayList;
            } catch (Exception e) {
                e = e;
                e.printStackTrace();
                return arrayList;
            }
        } catch (Exception e2) {
            e = e2;
            arrayList = null;
            e.printStackTrace();
            return arrayList;
        }
    }

    public final CampaignEx a(String str, String str2) {
        if (this.c == null || TextUtils.isEmpty(str2) || TextUtils.isEmpty(str)) {
            return null;
        }
        return this.c.d(str, str2);
    }

    public static boolean a(CampaignEx campaignEx) {
        try {
            b.a();
            com.mintegral.msdk.videocommon.e.a b2 = b.b();
            long e = b2 != null ? b2.e() : 0;
            long currentTimeMillis = System.currentTimeMillis();
            if (campaignEx != null) {
                long plct = campaignEx.getPlct() * 1000;
                long timestamp = currentTimeMillis - campaignEx.getTimestamp();
                if (plct > 0 && plct >= timestamp) {
                    return false;
                }
                if (plct > 0 || e < timestamp) {
                    return true;
                }
                return false;
            }
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return true;
        }
    }

    public final List<CampaignEx> a(String str, int i, boolean z) {
        ArrayList arrayList = null;
        try {
            b.a();
            com.mintegral.msdk.videocommon.e.a b2 = b.b();
            long e = b2 != null ? b2.e() : 0;
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            List<CampaignEx> a2 = this.c.a(str, i, z);
            long currentTimeMillis = System.currentTimeMillis();
            if (a2 == null) {
                return null;
            }
            ArrayList arrayList2 = new ArrayList();
            try {
                for (CampaignEx next : a2) {
                    if (next != null) {
                        long plct = next.getPlct() * 1000;
                        long timestamp = currentTimeMillis - next.getTimestamp();
                        if ((plct > 0 && plct >= timestamp) || (plct <= 0 && e >= timestamp)) {
                            arrayList2.add(next);
                        }
                    }
                }
                return arrayList2;
            } catch (Exception e2) {
                e = e2;
                arrayList = arrayList2;
                e.printStackTrace();
                return arrayList;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return arrayList;
        }
    }

    public final List<CampaignEx> a(String str, boolean z) {
        ArrayList arrayList = null;
        try {
            String j = com.mintegral.msdk.base.controller.a.d().j();
            com.mintegral.msdk.d.b.a();
            com.mintegral.msdk.d.a b2 = com.mintegral.msdk.d.b.b(j);
            long ak = b2 != null ? b2.ak() * 1000 : 0;
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            try {
                List<CampaignEx> a2 = this.c.a(str, 1, z);
                long currentTimeMillis = System.currentTimeMillis();
                if (a2 == null) {
                    return null;
                }
                ArrayList arrayList2 = new ArrayList();
                try {
                    for (CampaignEx next : a2) {
                        if (next != null) {
                            long plctb = next.getPlctb() * 1000;
                            long timestamp = currentTimeMillis - next.getTimestamp();
                            if ((plctb <= 0 && ak >= timestamp) || (plctb > 0 && plctb >= timestamp)) {
                                arrayList2.add(next);
                            }
                        }
                    }
                    return arrayList2;
                } catch (Exception e) {
                    e = e;
                    arrayList = arrayList2;
                    e.printStackTrace();
                    return arrayList;
                }
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return arrayList;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return arrayList;
        }
    }

    public final void a(CampaignEx campaignEx, String str) {
        if (campaignEx != null) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    this.c.a(campaignEx.getId(), str, campaignEx.isBidCampaign());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public final void b(CampaignEx campaignEx) {
        if (campaignEx != null) {
            try {
                if (!TextUtils.isEmpty(campaignEx.getId())) {
                    this.c.a(campaignEx.getId());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public final void a(String str, List<CampaignEx> list) {
        try {
            if (!TextUtils.isEmpty(str) && list != null && list.size() > 0) {
                for (CampaignEx next : list) {
                    if (next != null) {
                        try {
                            if (!TextUtils.isEmpty(str)) {
                                this.c.a(next, str, 0);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final synchronized void a(long j, String str) {
        try {
            this.c.b(j, str);
        } catch (Exception e) {
            e.printStackTrace();
            g.d(a, e.getMessage());
        }
    }

    public final synchronized void b(long j, String str) {
        try {
            this.c.a(j, str);
        } catch (Exception e) {
            e.printStackTrace();
            g.d(a, e.getMessage());
        }
    }
}
