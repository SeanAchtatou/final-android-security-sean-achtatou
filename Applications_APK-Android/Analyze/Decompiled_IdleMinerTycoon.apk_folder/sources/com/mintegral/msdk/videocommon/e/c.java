package com.mintegral.msdk.videocommon.e;

import android.text.TextUtils;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.videocommon.b.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: RewardUnitSetting */
public final class c {
    private static i E;
    private int A = -1;
    private int B = -1;
    private int C;
    private int D = 20;
    private int F;
    private String a;
    private List<b> b;
    private long c;
    private int d = -1;
    private int e = 0;
    private int f = 0;
    private int g = 1;
    private int h = 1;
    private int i = 1;
    private int j = 1;
    private double k = 1.0d;
    private int l = 2;
    private int m = 5;
    private int n = 2;
    private int o = 3;
    private int p = 80;
    private int q = 100;
    private com.mintegral.msdk.videocommon.b.c r;
    private double s = 1.0d;
    private int t = -1;
    private int u = 2;
    private double v = 1.0d;
    private ArrayList<Integer> w;
    private int x = 0;
    private int y = 0;
    private int z = -1;

    public final int a() {
        return this.x;
    }

    public final void b() {
        this.x = 0;
    }

    public final int c() {
        return this.y;
    }

    public final void d() {
        this.y = 0;
    }

    public final int e() {
        return this.z;
    }

    public final void a(int i2) {
        this.z = i2;
    }

    public final int f() {
        return this.A;
    }

    public final void g() {
        this.A = 1;
    }

    public final int h() {
        return this.B;
    }

    public final void i() {
        this.B = -1;
    }

    public final int j() {
        return this.d;
    }

    public final int k() {
        return this.f;
    }

    public final int l() {
        return this.D;
    }

    public final int m() {
        return this.C;
    }

    public final void n() {
        this.C = 1;
    }

    public final int o() {
        return this.F;
    }

    public final void p() {
        this.F = 1;
    }

    public final void q() {
        this.o = 3;
    }

    public final void r() {
        this.p = 80;
    }

    public final int s() {
        return this.q;
    }

    public final void t() {
        this.q = 100;
    }

    public final int u() {
        return this.h;
    }

    public final void v() {
        this.h = 1;
    }

    public final int w() {
        return this.i;
    }

    public final void x() {
        this.i = 1;
    }

    public final int y() {
        return this.j;
    }

    public final void z() {
        this.j = 1;
    }

    public final void A() {
        this.k = 1.0d;
    }

    public final double B() {
        return this.k;
    }

    public final void C() {
        this.l = 2;
    }

    public final int D() {
        return this.m;
    }

    public final void E() {
        this.m = 1;
    }

    public final int F() {
        return this.n;
    }

    public final void G() {
        this.n = 1;
    }

    public final void H() {
        this.g = 1;
    }

    public final long I() {
        return this.c;
    }

    public final List<b> J() {
        return this.b;
    }

    public final void a(List<b> list) {
        this.b = list;
    }

    public final com.mintegral.msdk.videocommon.b.c K() {
        return this.r;
    }

    public final double L() {
        return this.s;
    }

    public final int b(int i2) {
        if (this.t == -1) {
            if (i2 == 94) {
                return 2;
            }
            if (i2 == 287) {
                return 3;
            }
        }
        return this.t;
    }

    public final int M() {
        return this.u;
    }

    public final void N() {
        this.u = 2;
    }

    public final double O() {
        return this.v;
    }

    public final void P() {
        this.v = 1.0d;
    }

    public final boolean c(int i2) {
        if (this.w == null || this.w.size() <= 0) {
            return false;
        }
        return this.w.contains(Integer.valueOf(i2));
    }

    public final Queue<Integer> Q() {
        LinkedList linkedList;
        Exception e2;
        try {
            if (this.b == null || this.b.size() <= 0) {
                return null;
            }
            linkedList = new LinkedList();
            int i2 = 0;
            while (i2 < this.b.size()) {
                try {
                    linkedList.add(Integer.valueOf(this.b.get(i2).b()));
                    i2++;
                } catch (Exception e3) {
                    e2 = e3;
                    e2.printStackTrace();
                    return linkedList;
                }
            }
            return linkedList;
        } catch (Exception e4) {
            Exception exc = e4;
            linkedList = null;
            e2 = exc;
            e2.printStackTrace();
            return linkedList;
        }
    }

    public static c a(String str) {
        if (E == null) {
            E = i.a(a.d().h());
        }
        c cVar = null;
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                JSONArray optJSONArray = jSONObject.optJSONArray("unitSetting");
                if (optJSONArray == null) {
                    return null;
                }
                int i2 = 0;
                JSONObject optJSONObject = optJSONArray.optJSONObject(0);
                if (optJSONObject == null) {
                    return null;
                }
                String optString = optJSONObject.optString("unitId");
                if (TextUtils.isEmpty(optString)) {
                    return null;
                }
                c cVar2 = new c();
                try {
                    List<b> a2 = b.a(optJSONObject.optJSONArray("adSourceList"));
                    cVar2.a = optString;
                    cVar2.b = a2;
                    cVar2.g = optJSONObject.optInt("callbackType");
                    int optInt = optJSONObject.optInt("aqn", 1);
                    if (optInt <= 0) {
                        optInt = 1;
                    }
                    cVar2.h = optInt;
                    int optInt2 = optJSONObject.optInt("acn", 1);
                    if (optInt2 < 0) {
                        optInt2 = 1;
                    }
                    cVar2.i = optInt2;
                    cVar2.j = optJSONObject.optInt("vcn", 5);
                    cVar2.k = optJSONObject.optDouble("cbp", 1.0d);
                    cVar2.l = optJSONObject.optInt(CampaignEx.JSON_KEY_TTC_TYPE, 2);
                    cVar2.m = optJSONObject.optInt("offset", 5);
                    cVar2.n = optJSONObject.optInt("dlnet", 2);
                    cVar2.F = optJSONObject.optInt("endscreen_type", 1);
                    cVar2.o = optJSONObject.optInt("tv_start", 3);
                    cVar2.p = optJSONObject.optInt("tv_end", 80);
                    cVar2.q = optJSONObject.optInt("ready_rate", 100);
                    cVar2.c = jSONObject.optLong("current_time");
                    cVar2.x = optJSONObject.optInt("orientation", 0);
                    cVar2.y = optJSONObject.optInt("daily_play_cap", 0);
                    cVar2.z = optJSONObject.optInt("video_skip_time", -1);
                    cVar2.A = optJSONObject.optInt("video_skip_result", 1);
                    cVar2.B = optJSONObject.optInt("video_interactive_type", -1);
                    cVar2.C = optJSONObject.optInt("close_button_delay", 1);
                    cVar2.d = optJSONObject.optInt("playclosebtn_tm", -1);
                    cVar2.e = optJSONObject.optInt("play_ctdown", 0);
                    cVar2.f = optJSONObject.optInt("close_alert", 0);
                    cVar2.D = optJSONObject.optInt("rdrct", 20);
                    cVar2.t = optJSONObject.optInt("rfpv", -1);
                    cVar2.s = optJSONObject.optDouble("vdcmp", 1.0d);
                    cVar2.r = com.mintegral.msdk.videocommon.b.c.a(optJSONObject.optJSONObject("cbprule"));
                    cVar2.u = optJSONObject.optInt("ccbprule", 2);
                    cVar2.v = optJSONObject.optDouble("ccbp", 1.0d);
                    JSONArray optJSONArray2 = optJSONObject.optJSONArray("atl_type");
                    ArrayList<Integer> arrayList = new ArrayList<>();
                    if (optJSONArray2 != null) {
                        while (i2 < optJSONArray2.length()) {
                            try {
                                arrayList.add(Integer.valueOf(optJSONArray2.getInt(i2)));
                                i2++;
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                        }
                    } else {
                        arrayList.add(1);
                        arrayList.add(2);
                        arrayList.add(3);
                        arrayList.add(4);
                    }
                    cVar2.w = arrayList;
                    return cVar2;
                } catch (Exception e3) {
                    e = e3;
                    cVar = cVar2;
                    e.printStackTrace();
                    return cVar;
                }
            } catch (Exception e4) {
                e = e4;
                e.printStackTrace();
                return cVar;
            }
        }
        return cVar;
    }

    public final JSONObject R() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("unitId", this.a);
            jSONObject.put("callbackType", this.g);
            if (this.b != null && this.b.size() > 0) {
                JSONArray jSONArray = new JSONArray();
                for (b next : this.b) {
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("id", next.a());
                    jSONObject2.put("timeout", next.b());
                    jSONArray.put(jSONObject2);
                }
                jSONObject.put("adSourceList", jSONArray);
            }
            jSONObject.put("aqn", this.h);
            jSONObject.put("acn", this.i);
            jSONObject.put("vcn", this.j);
            jSONObject.put("cbp", this.k);
            jSONObject.put(CampaignEx.JSON_KEY_TTC_TYPE, this.l);
            jSONObject.put("offset", this.m);
            jSONObject.put("dlnet", this.n);
            jSONObject.put("tv_start", this.o);
            jSONObject.put("tv_end", this.p);
            jSONObject.put("ready_rate", this.q);
            jSONObject.put("endscreen_type", this.F);
            jSONObject.put("daily_play_cap", this.y);
            jSONObject.put("video_skip_time", this.z);
            jSONObject.put("video_skip_result", this.A);
            jSONObject.put("video_interactive_type", this.B);
            jSONObject.put("orientation", this.x);
            jSONObject.put("close_button_delay", this.C);
            jSONObject.put("playclosebtn_tm", this.d);
            jSONObject.put("play_ctdown", this.e);
            jSONObject.put("close_alert", this.f);
            jSONObject.put("rfpv", this.t);
            jSONObject.put("vdcmp", this.s);
            if (this.r != null) {
                JSONObject jSONObject3 = new JSONObject();
                jSONObject3.put("type", this.r.a());
                JSONArray jSONArray2 = new JSONArray();
                for (Integer put : this.r.b()) {
                    jSONArray2.put(put);
                }
                jSONObject3.put("value", jSONArray2);
                jSONObject.put("cbprule", jSONObject3);
            }
            jSONObject.put("ccbprule", this.u);
            jSONObject.put("ccbp", this.v);
            JSONArray jSONArray3 = new JSONArray();
            if (this.w != null) {
                if (this.w.size() > 0) {
                    Iterator<Integer> it = this.w.iterator();
                    while (it.hasNext()) {
                        jSONArray3.put(it.next());
                    }
                }
                jSONObject.put("atl_type", jSONArray3);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }
}
