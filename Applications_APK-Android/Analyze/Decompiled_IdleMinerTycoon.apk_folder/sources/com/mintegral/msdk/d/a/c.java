package com.mintegral.msdk.d.a;

import android.content.Context;
import android.os.Build;
import com.helpshift.support.constants.FaqsColumns;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.common.net.a;
import com.mintegral.msdk.base.common.net.a.e;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.out.MTGConfiguration;
import com.tapjoy.TapjoyConstants;

/* compiled from: SettingRequest */
public final class c extends a {
    public c(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public final void a(l lVar) {
        super.a(lVar);
        lVar.a(TapjoyConstants.TJC_PLATFORM, "1");
        lVar.a(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, Build.VERSION.RELEASE);
        lVar.a(CampaignEx.JSON_KEY_PACKAGE_NAME, com.mintegral.msdk.base.utils.c.q(this.b));
        lVar.a("app_version_name", com.mintegral.msdk.base.utils.c.l(this.b));
        StringBuilder sb = new StringBuilder();
        sb.append(com.mintegral.msdk.base.utils.c.k(this.b));
        lVar.a("app_version_code", sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append(com.mintegral.msdk.base.utils.c.i(this.b));
        lVar.a("orientation", sb2.toString());
        lVar.a("model", com.mintegral.msdk.base.utils.c.c());
        lVar.a("brand", com.mintegral.msdk.base.utils.c.e());
        lVar.a("gaid", com.mintegral.msdk.base.utils.c.k());
        lVar.a(Constants.RequestParameters.NETWORK_MNC, com.mintegral.msdk.base.utils.c.b());
        lVar.a(Constants.RequestParameters.NETWORK_MCC, com.mintegral.msdk.base.utils.c.a());
        int s = com.mintegral.msdk.base.utils.c.s(this.b);
        lVar.a("network_type", String.valueOf(s));
        lVar.a("network_str", com.mintegral.msdk.base.utils.c.a(this.b, s));
        lVar.a(FaqsColumns.LANGUAGE, com.mintegral.msdk.base.utils.c.h(this.b));
        lVar.a(TapjoyConstants.TJC_DEVICE_TIMEZONE, com.mintegral.msdk.base.utils.c.h());
        lVar.a("useragent", com.mintegral.msdk.base.utils.c.f());
        lVar.a("sdk_version", MTGConfiguration.SDK_VERSION);
        lVar.a("gp_version", com.mintegral.msdk.base.utils.c.t(this.b));
        lVar.a("screen_size", com.mintegral.msdk.base.utils.c.n(this.b) + "x" + com.mintegral.msdk.base.utils.c.o(this.b));
        lVar.a("is_clever", com.mintegral.msdk.base.common.a.v);
        e.b(lVar);
    }
}
