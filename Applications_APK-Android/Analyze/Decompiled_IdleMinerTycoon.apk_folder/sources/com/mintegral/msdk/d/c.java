package com.mintegral.msdk.d;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Looper;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.a;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.d.a.d;
import com.mintegral.msdk.e.b;
import org.json.JSONObject;

/* compiled from: SettingRequestController */
public class c {
    /* access modifiers changed from: private */
    public static final String a = "c";

    public final void a(final Context context, final String str, final String str2) {
        if (context != null) {
            com.mintegral.msdk.d.a.c cVar = new com.mintegral.msdk.d.a.c(context);
            l lVar = new l();
            lVar.a("app_id", str);
            lVar.a("sign", CommonMD5.getMD5(str + str2));
            lVar.a("jm_a", b.a(context).c());
            StringBuilder sb = new StringBuilder();
            sb.append(b.a(context).a());
            lVar.a("jm_n", sb.toString());
            lVar.a("launcher", b.a(context).b());
            cVar.a(a.n, lVar, new d() {
                public final void a(JSONObject jSONObject) {
                    if (jSONObject != null) {
                        try {
                            MIntegralConstans.ALLOW_APK_DOWNLOAD = jSONObject.optBoolean("aa");
                            SharedPreferences.Editor edit = com.mintegral.msdk.base.controller.a.d().h().getApplicationContext().getSharedPreferences("cv", 0).edit();
                            edit.clear();
                            edit.commit();
                            jSONObject.put("current_time", System.currentTimeMillis());
                            b.a();
                            b.b(str, jSONObject.toString());
                            if (!com.mintegral.msdk.d.b.a.a().c()) {
                                new Thread(new Runnable() {
                                    public final void run() {
                                        Looper.prepare();
                                        com.mintegral.msdk.d.b.a.a().e();
                                        Looper.loop();
                                    }
                                }).start();
                            } else {
                                com.mintegral.msdk.d.b.a.a().d();
                            }
                            new Thread(new Runnable() {
                                public final void run() {
                                    com.mintegral.msdk.base.controller.a.e();
                                }
                            }).start();
                        } catch (Exception e) {
                            e.printStackTrace();
                            return;
                        }
                    } else {
                        g.d(c.a, "app setting is null");
                    }
                    new com.mintegral.msdk.base.common.e.b(context).a();
                    c.this.b(context, str, str2);
                    c.a(context, str);
                }

                public final void a(String str) {
                    new com.mintegral.msdk.base.common.e.b(context).a();
                    c.this.b(context, str, str2);
                    String a2 = c.a;
                    g.d(a2, "get app setting error" + str);
                }
            });
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.content.Context r8, final java.lang.String r9, java.lang.String r10, final java.lang.String r11) {
        /*
            r7 = this;
            boolean r0 = android.text.TextUtils.isEmpty(r9)
            if (r0 != 0) goto L_0x000c
            boolean r0 = android.text.TextUtils.isEmpty(r10)
            if (r0 == 0) goto L_0x001c
        L_0x000c:
            com.mintegral.msdk.base.controller.a r9 = com.mintegral.msdk.base.controller.a.d()
            java.lang.String r9 = r9.j()
            com.mintegral.msdk.base.controller.a r10 = com.mintegral.msdk.base.controller.a.d()
            java.lang.String r10 = r10.k()
        L_0x001c:
            com.mintegral.msdk.d.b.a()
            com.mintegral.msdk.d.a r0 = com.mintegral.msdk.d.b.b(r9)
            boolean r1 = com.mintegral.msdk.d.b.a(r9)
            r2 = 1
            if (r1 == 0) goto L_0x0048
            boolean r1 = com.mintegral.msdk.d.b.a(r9, r2, r11)
            if (r1 == 0) goto L_0x0048
            com.mintegral.msdk.d.c r1 = new com.mintegral.msdk.d.c
            r1.<init>()
            com.mintegral.msdk.base.controller.a r3 = com.mintegral.msdk.base.controller.a.d()
            android.content.Context r3 = r3.h()
            com.mintegral.msdk.base.controller.a r4 = com.mintegral.msdk.base.controller.a.d()
            java.lang.String r4 = r4.k()
            r1.a(r3, r9, r4)
        L_0x0048:
            com.mintegral.msdk.d.d r1 = com.mintegral.msdk.d.b.c(r9, r11)
            if (r0 == 0) goto L_0x0087
            if (r1 == 0) goto L_0x0087
            long r3 = r0.aC()
            r5 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 * r5
            long r5 = java.lang.System.currentTimeMillis()
            long r0 = r1.y()
            long r0 = r0 + r3
            int r3 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x0087
            java.lang.String r2 = com.mintegral.msdk.d.b.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "unit setting  nexttime is not ready  [settingNextRequestTime= "
            r3.<init>(r4)
            r3.append(r0)
            java.lang.String r0 = " currentTime = "
            r3.append(r0)
            r3.append(r5)
            java.lang.String r0 = "]"
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            com.mintegral.msdk.base.utils.g.b(r2, r0)
            r2 = 0
            goto L_0x008e
        L_0x0087:
            java.lang.String r0 = com.mintegral.msdk.d.b.a
            java.lang.String r1 = "unit setting timeout or not exists"
            com.mintegral.msdk.base.utils.g.b(r0, r1)
        L_0x008e:
            if (r2 == 0) goto L_0x00e3
            com.mintegral.msdk.d.b.a()
            r0 = 2
            boolean r0 = com.mintegral.msdk.d.b.a(r9, r0, r11)
            if (r0 == 0) goto L_0x00e3
            com.mintegral.msdk.d.a.c r0 = new com.mintegral.msdk.d.a.c
            r0.<init>(r8)
            com.mintegral.msdk.base.common.net.l r8 = new com.mintegral.msdk.base.common.net.l
            r8.<init>()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "["
            r1.<init>(r2)
            r1.append(r11)
            java.lang.String r2 = "]"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "unit_ids"
            r8.a(r2, r1)
            java.lang.String r1 = "app_id"
            r8.a(r1, r9)
            java.lang.String r1 = "sign"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r9)
            r2.append(r10)
            java.lang.String r10 = r2.toString()
            java.lang.String r10 = com.mintegral.msdk.base.utils.CommonMD5.getMD5(r10)
            r8.a(r1, r10)
            java.lang.String r10 = com.mintegral.msdk.base.common.a.n
            com.mintegral.msdk.d.c$2 r1 = new com.mintegral.msdk.d.c$2
            r1.<init>(r9, r11)
            r0.a(r10, r8, r1)
        L_0x00e3:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.d.c.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void");
    }

    private void c(Context context, String str, String str2) {
        if (context != null) {
            com.mintegral.msdk.d.a.a aVar = new com.mintegral.msdk.d.a.a(context);
            l lVar = new l();
            lVar.a("app_id", str);
            lVar.a("sign", CommonMD5.getMD5(str + str2));
            aVar.a(a.o, lVar, new d() {
                public final void a(JSONObject jSONObject) {
                    if (jSONObject != null) {
                        try {
                            String optString = jSONObject.optString("system_id");
                            if (!TextUtils.isEmpty(optString) && !TextUtils.equals(a.D, optString)) {
                                a.D = optString;
                                com.mintegral.msdk.base.a.a.a.a().a("sys_id", a.D);
                            }
                            String optString2 = jSONObject.optString("sysbkup_id");
                            if (!TextUtils.isEmpty(optString2) && !TextUtils.equals(a.E, optString2)) {
                                a.E = optString2;
                                com.mintegral.msdk.base.a.a.a.a().a("bkup_id", a.E);
                            }
                        } catch (Exception e) {
                            if (MIntegralConstans.DEBUG) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        g.d(c.a, "custom id return data null");
                    }
                }

                public final void a(String str) {
                    String a2 = c.a;
                    g.d(a2, "get custom id error" + str);
                }
            });
        }
    }

    public final void b(Context context, String str, String str2) {
        a b;
        if (b.a() != null && (b = b.b(str)) != null && b.bg() == 1) {
            c(context, str, str2);
        } else if (TextUtils.isEmpty(a.D)) {
            String a2 = com.mintegral.msdk.base.a.a.a.a().a("sys_id");
            a.D = a2;
            if (TextUtils.isEmpty(a2)) {
                c(context, str, str2);
            }
        }
    }

    static /* synthetic */ void a(Context context, String str) {
        a b;
        if (!(b.a() == null || (b = b.b(str)) == null)) {
            MIntegralConstans.OMID_JS_SERVICE_URL = b.bh();
        }
        com.mintegral.msdk.b.b.a(context);
    }
}
