package com.mintegral.msdk.d;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.appsflyer.share.Constants;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.google.android.gms.nearby.messages.Strategy;
import com.helpshift.util.DatabaseUtils;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.b;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.m;
import com.mintegral.msdk.base.utils.s;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Setting */
public final class a {
    private long A;
    private long B;
    private int C;
    private int D;
    private int E;
    private String F;
    private String G;
    private int H;
    private List<CampaignEx> I;
    private List<b> J;
    private LinkedList<String> K;
    private int L = 0;
    private int M = 3600;
    private int N;
    private List<String> O;
    private int P;
    private int Q;
    private int R;
    private int S;
    private String T;
    private String U;
    private int V = 1;
    private int W = 1;
    private int X = 1;
    private int Y = 0;
    private int Z = 1;
    private int a = 0;
    private int aA = 0;
    private int aB = CampaignEx.TTC_CT_DEFAULT_VALUE;
    private int aC = 0;
    private String aD = "";
    private String aE = "";
    private String aF = "";
    private String aG = "";
    private String aH = "";
    private Map<String, C0054a> aI;
    private int aJ = 0;
    private String aK = "";
    private int aL = 2;
    private int aM = 7200;
    private List<com.mintegral.msdk.base.entity.a> aN;
    private String aa = "";
    private int ab = 0;
    private int ac = 2;
    private String ad;
    private int ae = Strategy.TTL_SECONDS_MAX;
    private String af = "LdxThdi1WBK\\/WgfPhbxQYkeXHBPwHZKAJ7eXHM==";
    private String ag = "LdxThdi1WBK\\/WgfPhbxQYkeXHBPwHZKsYFh=";
    private int ah = 1;
    private int ai;
    private int aj = 1;
    private long ak;
    private long al;
    private int am;
    private int an;
    private long ao;
    private int ap = 3;
    private int aq;
    private String ar = "";
    private String as = "";
    private String at = "";
    private String au = "";
    private String av = "";
    private int aw = 0;
    private int ax = 21600;
    private int ay = 2;
    private int az = 0;
    private String b;
    private long c;
    private int d;
    private long e = 86400;
    private int f;
    private boolean g;
    private Map<String, String> h;
    private boolean i;
    private long j;
    private boolean k;
    private long l;
    private long m;
    private long n;
    private boolean o;
    private int p;
    private int q;
    private int r;
    private long s;
    private int t;
    private int u;
    private int v;
    private int w;
    private String x;
    private long y;
    private int z;

    public final List<b> a() {
        return this.J;
    }

    public final int b() {
        return this.a;
    }

    public final void c() {
        this.a = 0;
    }

    public final String d() {
        return this.af;
    }

    public final String e() {
        return this.ag;
    }

    public final int f() {
        return this.ae;
    }

    public final void g() {
        this.ae = Strategy.TTL_SECONDS_MAX;
    }

    public final int h() {
        return this.ac;
    }

    public final void i() {
        this.ac = 2;
    }

    public final String j() {
        return this.ad;
    }

    public final int k() {
        return this.V;
    }

    public final void l() {
        this.V = 1;
    }

    public final int m() {
        return this.W;
    }

    public final void n() {
        this.W = 1;
    }

    public final int o() {
        return this.X;
    }

    public final void p() {
        this.X = 1;
    }

    public final int q() {
        return this.Y;
    }

    public final void r() {
        this.Y = 0;
    }

    public final int s() {
        return this.Z;
    }

    public final void t() {
        this.Z = 1;
    }

    public final String u() {
        return this.aa;
    }

    public final void a(String str) {
        this.aa = str;
    }

    public final int v() {
        return this.ab;
    }

    public final void w() {
        this.ab = -1;
    }

    public final int x() {
        return this.H;
    }

    public final void y() {
        this.H = 1;
    }

    public final String z() {
        return this.G;
    }

    public final void b(String str) {
        this.G = str;
    }

    public final void A() {
        this.aj = 1;
    }

    public final int B() {
        return this.ai;
    }

    public final void C() {
        this.ai = 1;
    }

    public final int D() {
        return this.N;
    }

    public final void E() {
        this.N = DatabaseUtils.MAX_WILDCARD_COUNT;
    }

    public final List<String> F() {
        return this.O;
    }

    public final int G() {
        return this.P;
    }

    public final void H() {
        this.P = 20;
    }

    public final int I() {
        return this.Q;
    }

    public final void J() {
        this.Q = 1;
    }

    public final int K() {
        return this.R;
    }

    public final void L() {
        this.R = 1;
    }

    public final int M() {
        return this.S;
    }

    public final void N() {
        this.S = 1;
    }

    public final void c(String str) {
        this.T = str;
    }

    public final int O() {
        return this.C;
    }

    public final void P() {
        this.C = 1;
    }

    public final int Q() {
        return this.D;
    }

    public final void R() {
        this.D = Strategy.TTL_SECONDS_MAX;
    }

    public final List<CampaignEx> S() {
        return this.I;
    }

    public final int T() {
        return this.E;
    }

    public final void U() {
        this.E = 1;
    }

    public final String V() {
        return this.F;
    }

    public final void d(String str) {
        this.F = str;
    }

    public final int W() {
        return this.z;
    }

    public final void X() {
        this.z = 1;
    }

    public final long Y() {
        return this.A;
    }

    public final void Z() {
        this.A = 604800;
    }

    public final long aa() {
        return this.B;
    }

    public final void ab() {
        this.B = 43200;
    }

    public final long ac() {
        return this.y * 1000;
    }

    public final int ad() {
        return this.w;
    }

    public final int ae() {
        return this.t;
    }

    public final void af() {
        this.t = 100;
    }

    public final int ag() {
        return this.u;
    }

    public final void ah() {
        this.u = 1;
    }

    public final int ai() {
        return this.v;
    }

    public final void aj() {
        this.v = 1;
    }

    public final long ak() {
        return this.s;
    }

    public final void al() {
        this.s = 7200;
    }

    public final int am() {
        return this.ap;
    }

    public final void an() {
        this.ap = 3;
    }

    public final int ao() {
        return this.aq;
    }

    public final void ap() {
        this.aq = 259200;
    }

    public final long aq() {
        return this.ao;
    }

    public final void ar() {
        this.ao = 1800;
    }

    public final int as() {
        return this.am;
    }

    public final void at() {
        this.am = 1;
    }

    public final int au() {
        return this.an;
    }

    public final void av() {
        this.an = 1;
    }

    public final long aw() {
        return this.ak * 1000;
    }

    public final void ax() {
        this.ak = 20;
    }

    public final long ay() {
        return this.al * 1000;
    }

    public final void az() {
        this.al = 10;
    }

    public final boolean aA() {
        return this.o;
    }

    public final void aB() {
        this.o = false;
    }

    public final long aC() {
        return this.n;
    }

    public final boolean aD() {
        return this.k;
    }

    public final void aE() {
        this.k = false;
    }

    public final long aF() {
        return this.l;
    }

    public final void aG() {
        this.l = 3600;
    }

    public final long aH() {
        return this.m;
    }

    public final void aI() {
        this.m = 0;
    }

    public final String toString() {
        return "cc=" + this.b + " upal=" + this.c + " cfc=" + this.d + " getpf=" + this.e + " uplc=" + this.f + " rurl=" + this.k;
    }

    public final void e(String str) {
        this.b = str;
    }

    public final void aJ() {
        this.c = 86400;
    }

    public final int aK() {
        return this.d;
    }

    public final void aL() {
        this.d = 1;
    }

    public final long aM() {
        return this.e;
    }

    public final int aN() {
        return this.f;
    }

    public final void aO() {
        this.f = 1;
    }

    public final String aP() {
        return this.U;
    }

    public static a f(String str) {
        a aVar;
        Exception e2;
        long j2;
        long j3;
        ArrayList arrayList;
        CampaignEx parseSettingCampaign;
        ArrayList arrayList2 = null;
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            JSONObject jSONObject = new JSONObject(str);
            aVar = new a();
            try {
                aVar.b = jSONObject.optString("cc");
                aVar.F = jSONObject.optString("apk_toast", "正在下载中，请去通知栏查看下载进度");
                aVar.G = jSONObject.optString("mv_wildcard", "mintegral");
                aVar.c = jSONObject.optLong("upal");
                aVar.d = jSONObject.optInt("cfc");
                aVar.e = jSONObject.optLong("getpf");
                aVar.f = jSONObject.optInt("uplc");
                aVar.g = jSONObject.optBoolean("aa");
                aVar.j = jSONObject.optLong("current_time");
                aVar.i = jSONObject.optBoolean("cfb");
                aVar.m = jSONObject.optLong("awct");
                if (jSONObject.optLong(CampaignEx.JSON_KEY_PLCT) == 0) {
                    j2 = 3600;
                } else {
                    j2 = jSONObject.optLong(CampaignEx.JSON_KEY_PLCT);
                }
                aVar.l = j2;
                aVar.k = jSONObject.optBoolean("rurl");
                aVar.n = jSONObject.optLong("uct");
                aVar.o = jSONObject.optBoolean("ujds");
                aVar.p = jSONObject.optInt("n2");
                aVar.q = jSONObject.optInt("n3");
                aVar.H = jSONObject.optInt("is_startup_crashsystem");
                aVar.r = jSONObject.optInt("n4", CampaignEx.TTC_CT2_DEFAULT_VALUE);
                aVar.t = jSONObject.optInt("pcrn");
                if (jSONObject.optLong(CampaignEx.JSON_KEY_PLCTB) == 0) {
                    j3 = 7200;
                } else {
                    j3 = jSONObject.optLong(CampaignEx.JSON_KEY_PLCTB);
                }
                aVar.s = j3;
                aVar.am = jSONObject.optInt("upmi");
                aVar.an = jSONObject.optInt("upaid");
                aVar.t = jSONObject.optInt("pcrn", 100);
                aVar.u = jSONObject.optInt("wicon", 2);
                aVar.v = jSONObject.optInt("wreq", 2);
                aVar.w = jSONObject.optInt("opent", 1);
                aVar.ao = jSONObject.optLong("sfct", 1800);
                aVar.x = jSONObject.optString("t_vba");
                aVar.y = jSONObject.optLong("tcct", 21600000);
                aVar.z = jSONObject.optInt("dlrf", 1);
                aVar.A = (long) jSONObject.optInt("dlrfct", CampaignEx.TTC_CT_DEFAULT_VALUE);
                aVar.B = (long) jSONObject.optInt("pcct", 43200);
                aVar.ap = jSONObject.optInt("pctn", 3);
                aVar.aj = jSONObject.optInt("ilrf", 1);
                aVar.U = jSONObject.optString("pw", "");
                aVar.ai = jSONObject.optInt("dlapk", 1);
                aVar.W = jSONObject.optInt("upgd", 1);
                aVar.V = jSONObject.optInt("upsrl", 1);
                aVar.X = jSONObject.optInt("updevid", 1);
                aVar.Y = jSONObject.optInt("sc", 0);
                aVar.Z = jSONObject.optInt("up_tips", 1);
                aVar.ab = jSONObject.optInt("iseu", -1);
                aVar.aa = jSONObject.optString("up_tips_url", com.mintegral.msdk.d.a.b.a);
                aVar.ac = jSONObject.optInt("jmc", 2);
                aVar.ae = jSONObject.optInt("jmct", Strategy.TTL_SECONDS_MAX);
                aVar.ad = jSONObject.optString("jm_unit");
                aVar.ag = jSONObject.optString("cdai");
                aVar.af = jSONObject.optString("csdai");
                aVar.ah = jSONObject.optInt("ils");
                aVar.M = jSONObject.optInt("clptm", 3600);
                aVar.L = jSONObject.optInt("clptype", 0);
                try {
                    JSONArray optJSONArray = jSONObject.optJSONArray("clpcode");
                    if (optJSONArray != null && optJSONArray.length() > 0) {
                        LinkedList<String> linkedList = new LinkedList<>();
                        for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                            linkedList.add(optJSONArray.optString(i2));
                        }
                        aVar.K = linkedList;
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                }
                long optLong = jSONObject.optLong("pcto");
                if (optLong == 0) {
                    aVar.ak = 20;
                } else {
                    aVar.ak = optLong;
                }
                long optLong2 = jSONObject.optLong("tcto");
                if (optLong2 == 0) {
                    aVar.al = 10;
                } else {
                    aVar.al = optLong2;
                    JSONArray optJSONArray2 = jSONObject.optJSONArray("jt");
                    if (optJSONArray2 != null && optJSONArray2.length() > 0) {
                        HashMap hashMap = new HashMap();
                        for (int i3 = 0; i3 < optJSONArray2.length(); i3++) {
                            JSONObject optJSONObject = optJSONArray2.optJSONObject(i3);
                            hashMap.put(optJSONObject.optString("domain"), optJSONObject.optString("format"));
                        }
                        aVar.h = hashMap;
                    }
                }
                aVar.C = jSONObject.optInt("plc", 3);
                aVar.D = jSONObject.optInt("dut", Strategy.TTL_SECONDS_MAX);
                aVar.E = jSONObject.optInt(CampaignEx.JSON_KEY_ST_IEX, 1);
                JSONArray optJSONArray3 = jSONObject.optJSONArray("cal");
                if (optJSONArray3 == null || optJSONArray3.length() <= 0) {
                    arrayList = null;
                } else {
                    arrayList = new ArrayList();
                    for (int i4 = 0; i4 < optJSONArray3.length(); i4++) {
                        String optString = optJSONArray3.optString(i4);
                        if (s.b(optString) && (parseSettingCampaign = CampaignEx.parseSettingCampaign(new JSONObject(optString))) != null) {
                            arrayList.add(parseSettingCampaign);
                        }
                    }
                }
                if (arrayList != null) {
                    aVar.I = arrayList;
                }
                try {
                    JSONArray optJSONArray4 = jSONObject.optJSONArray("atf");
                    if (optJSONArray4 != null && optJSONArray4.length() > 0) {
                        arrayList2 = new ArrayList();
                        for (int i5 = 0; i5 < optJSONArray4.length(); i5++) {
                            String optString2 = optJSONArray4.optString(i5);
                            if (s.b(optString2)) {
                                JSONObject jSONObject2 = new JSONObject(optString2);
                                arrayList2.add(new b(jSONObject2.optInt("adtype"), jSONObject2.optString("unitid")));
                            }
                        }
                    }
                    if (arrayList2 != null) {
                        aVar.J = arrayList2;
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                aVar.aq = jSONObject.optInt("adct", 259200);
                aVar.N = jSONObject.optInt("pf", DatabaseUtils.MAX_WILDCARD_COUNT);
                aVar.P = jSONObject.optInt("pmax", 20);
                aVar.T = jSONObject.optString(Constants.URL_MEDIA_SOURCE);
                JSONArray optJSONArray5 = jSONObject.optJSONArray("pb");
                if (optJSONArray5 != null && optJSONArray5.length() > 0) {
                    ArrayList arrayList3 = new ArrayList();
                    for (int i6 = 0; i6 < optJSONArray5.length(); i6++) {
                        arrayList3.add(optJSONArray5.optString(i6));
                    }
                    aVar.O = arrayList3;
                }
                JSONObject optJSONObject2 = jSONObject.optJSONObject("pctrl");
                if (optJSONObject2 != null) {
                    aVar.Q = optJSONObject2.optInt(MessengerShareContentUtility.WEBVIEW_RATIO_FULL, 1);
                    aVar.R = optJSONObject2.optInt("add", 1);
                    aVar.S = optJSONObject2.optInt("delete", 1);
                }
                aVar.ar = jSONObject.optString("confirm_title", "");
                aVar.as = jSONObject.optString("confirm_description", "");
                aVar.at = jSONObject.optString("confirm_t", "");
                aVar.au = jSONObject.optString("confirm_c_rv", "");
                aVar.av = jSONObject.optString("confirm_c_play", "");
                aVar.aw = jSONObject.optInt("offercacheRate", 0);
                aVar.ax = jSONObject.optInt("offercachepacing", 21600);
                aVar.ay = jSONObject.optInt("useexpriedcacheoffer", 2);
                aVar.az = jSONObject.optInt("retryoffer", 0);
                aVar.aA = jSONObject.optInt("mapping_cache_rate", 0);
                aVar.aB = jSONObject.optInt("tokencachetime", CampaignEx.TTC_CT_DEFAULT_VALUE);
                aVar.aC = jSONObject.optInt("protect", 0);
                aVar.aD = jSONObject.optString("adchoice_icon", "");
                aVar.aF = jSONObject.optString("adchoice_link", "");
                aVar.aE = jSONObject.optString("adchoice_size", "");
                aVar.aH = jSONObject.optString("platform_logo", "");
                aVar.aG = jSONObject.optString("platform_name", "");
                aVar.aI = h(jSONObject.optString("cdnate_cfg", ""));
                aVar.a = jSONObject.optInt("atrqt", 0);
                aVar.aJ = jSONObject.optInt("iupdid", 0);
                aVar.aK = jSONObject.optString("omsdkjs_url", "");
                aVar.aL = jSONObject.optInt("activeAppStatus", 2);
                aVar.aM = jSONObject.optInt("activeAppTime", 7200);
                String optString3 = jSONObject.optString("activeAppConfig");
                if (TextUtils.isEmpty(optString3)) {
                    return aVar;
                }
                String c2 = com.mintegral.msdk.base.utils.a.c(optString3);
                if (!c2.startsWith(Constants.RequestParameters.LEFT_BRACKETS)) {
                    return aVar;
                }
                JSONArray jSONArray = new JSONArray(c2);
                if (jSONArray.length() <= 0) {
                    return aVar;
                }
                ArrayList arrayList4 = new ArrayList();
                for (int i7 = 0; i7 < jSONArray.length(); i7++) {
                    JSONObject jSONObject3 = jSONArray.getJSONObject(i7);
                    if (jSONObject3 != null) {
                        com.mintegral.msdk.base.entity.a aVar2 = new com.mintegral.msdk.base.entity.a();
                        aVar2.a(jSONObject3.optString("pn"));
                        aVar2.b(jSONObject3.optString("at"));
                        aVar2.c(jSONObject3.optString("ai"));
                        aVar2.d(jSONObject3.optString("ac"));
                        arrayList4.add(aVar2);
                    }
                }
                aVar.aN = arrayList4;
                return aVar;
            } catch (Exception e4) {
                e2 = e4;
                e2.printStackTrace();
                return aVar;
            }
        } catch (Exception e5) {
            e2 = e5;
            aVar = null;
            e2.printStackTrace();
            return aVar;
        }
    }

    public final long aQ() {
        return this.j;
    }

    public final void aR() {
        this.g = true;
    }

    public static String a(Context context, String str) {
        try {
            b.a();
            a b2 = b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (b2 == null) {
                return "";
            }
            if (b2.h == null) {
                return "";
            }
            String host = Uri.parse(str).getHost();
            for (Map.Entry<String, String> key : b2.h.entrySet()) {
                String str2 = (String) key.getKey();
                if (!TextUtils.isEmpty(host) && host.contains(str2)) {
                    String str3 = b2.h.get(str2);
                    if (TextUtils.isEmpty(str3)) {
                        return "";
                    }
                    String replace = str3.replace("{gaid}", c.k());
                    if (!replace.contains("{android_id}")) {
                        return (!replace.contains("{android_id_md5_upper}") || c.g(context) == null) ? replace : replace.replace("{android_id_md5_upper}", c.g(context));
                    }
                    if (c.f(context) != null) {
                        return replace.replace("{android_id}", c.f(context));
                    }
                    return replace;
                }
            }
            return "";
        } catch (Throwable unused) {
            return "";
        }
    }

    public final void aS() {
        this.i = true;
    }

    public final int aT() {
        return this.p;
    }

    public final int aU() {
        return this.q;
    }

    public static boolean aV() {
        try {
            b.a();
            a b2 = b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (b2 != null) {
                return b2.g;
            }
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return true;
        }
    }

    public final String aW() {
        return this.ar;
    }

    public final String aX() {
        return this.as;
    }

    public final String aY() {
        return this.at;
    }

    public final String aZ() {
        return this.au;
    }

    public final String ba() {
        return this.av;
    }

    public final void bb() {
        String language = Locale.getDefault().getLanguage();
        boolean z2 = true;
        if (!(!TextUtils.isEmpty(this.ar) && !TextUtils.isEmpty(this.as) && !TextUtils.isEmpty(this.at) && !TextUtils.isEmpty(this.au))) {
            if (TextUtils.isEmpty(language) || !language.equals("zh")) {
                this.ar = "Confirm to close? ";
                this.as = "You will not be rewarded after closing the window";
                this.at = "Close it";
                this.au = "Continue";
            } else {
                this.ar = "确认关闭？";
                this.as = "关闭后您将不会获得任何奖励噢~ ";
                this.at = "确认关闭";
                this.au = "继续观看";
            }
        }
        if (TextUtils.isEmpty(this.ar) || TextUtils.isEmpty(this.as) || TextUtils.isEmpty(this.at) || TextUtils.isEmpty(this.av)) {
            z2 = false;
        }
        if (z2) {
            return;
        }
        if (TextUtils.isEmpty(language) || !language.equals("zh")) {
            this.ar = "Confirm to close? ";
            this.as = "You will not be rewarded after closing the window";
            this.at = "Close it";
            this.av = "Continue";
            return;
        }
        this.ar = "确认关闭？";
        this.as = "关闭后您将不会获得任何奖励噢~ ";
        this.at = "确认关闭";
        this.av = "继续试玩";
    }

    public final String bc() {
        return this.aD;
    }

    public final String bd() {
        return this.aE;
    }

    public final String be() {
        return this.aF;
    }

    public final Map<String, C0054a> bf() {
        return this.aI;
    }

    private static Map<String, C0054a> h(String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            HashMap hashMap = new HashMap();
            JSONObject jSONObject = new JSONObject(str);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                C0054a aVar = new C0054a();
                JSONObject optJSONObject = jSONObject.optJSONObject(next);
                if (optJSONObject != null) {
                    aVar.a(optJSONObject);
                }
                hashMap.put(next, aVar);
            }
            return hashMap;
        } catch (JSONException e2) {
            if (MIntegralConstans.DEBUG) {
                e2.printStackTrace();
            }
            return null;
        } catch (Exception e3) {
            if (MIntegralConstans.DEBUG) {
                e3.printStackTrace();
            }
            return null;
        }
    }

    /* renamed from: com.mintegral.msdk.d.a$a  reason: collision with other inner class name */
    /* compiled from: Setting */
    public static class C0054a {
        private List<String> a;
        private List<String> b;
        private List<String> c;
        private List<String> d;

        public final List<String> a() {
            return this.a;
        }

        public final List<String> b() {
            return this.b;
        }

        public final List<String> c() {
            return this.c;
        }

        public final List<String> d() {
            return this.d;
        }

        public final void a(JSONObject jSONObject) {
            try {
                JSONArray optJSONArray = jSONObject.optJSONArray("x");
                if (optJSONArray != null) {
                    this.a = m.a(optJSONArray);
                }
                JSONArray optJSONArray2 = jSONObject.optJSONArray("y");
                if (optJSONArray2 != null) {
                    this.b = m.a(optJSONArray2);
                }
                JSONArray optJSONArray3 = jSONObject.optJSONArray("width");
                if (optJSONArray3 != null) {
                    this.c = m.a(optJSONArray3);
                }
                JSONArray optJSONArray4 = jSONObject.optJSONArray("height");
                if (optJSONArray4 != null) {
                    this.d = m.a(optJSONArray4);
                }
            } catch (Exception e) {
                if (MIntegralConstans.DEBUG) {
                    e.printStackTrace();
                }
            }
        }
    }

    public final int bg() {
        return this.aJ;
    }

    public final String bh() {
        return this.aK;
    }

    public final void g(String str) {
        this.aK = str;
    }

    public final int bi() {
        return this.aL;
    }

    public final int bj() {
        return this.aM;
    }

    public final List<com.mintegral.msdk.base.entity.a> bk() {
        return this.aN;
    }
}
