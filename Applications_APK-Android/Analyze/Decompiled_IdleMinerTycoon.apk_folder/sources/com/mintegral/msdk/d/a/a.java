package com.mintegral.msdk.d.a;

import android.content.Context;
import android.os.Build;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.common.net.a.e;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.out.MTGConfiguration;
import com.tapjoy.TapjoyConstants;

/* compiled from: CustomIdRequest */
public final class a extends com.mintegral.msdk.base.common.net.a {
    public a(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public final void a(l lVar) {
        super.a(lVar);
        lVar.a(TapjoyConstants.TJC_PLATFORM, "1");
        lVar.a(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, Build.VERSION.RELEASE);
        lVar.a(CampaignEx.JSON_KEY_PACKAGE_NAME, c.q(this.b));
        lVar.a("app_version_name", c.l(this.b));
        StringBuilder sb = new StringBuilder();
        sb.append(c.k(this.b));
        lVar.a("app_version_code", sb.toString());
        lVar.a("model", c.c());
        lVar.a("brand", c.e());
        lVar.a("gaid", c.k());
        lVar.a(Constants.RequestParameters.NETWORK_MNC, c.b());
        lVar.a(Constants.RequestParameters.NETWORK_MCC, c.a());
        int s = c.s(this.b);
        lVar.a("network_type", String.valueOf(s));
        lVar.a("network_str", c.a(this.b, s));
        lVar.a(TapjoyConstants.TJC_DEVICE_TIMEZONE, c.h());
        lVar.a("useragent", c.f());
        lVar.a("sdk_version", MTGConfiguration.SDK_VERSION);
        lVar.a("gp_version", c.t(this.b));
        e.b(lVar);
    }
}
