package com.mintegral.msdk.d;

import android.content.Context;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.q;
import com.mintegral.msdk.base.b.t;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.n;
import com.mintegral.msdk.base.entity.o;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.r;
import im.getsocial.sdk.consts.LanguageCodes;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: SettingManager */
public class b {
    public static final String a = "b";
    private static b b;
    private static HashMap<String, d> c = new HashMap<>();
    private static a d = null;

    private b() {
    }

    public static b a() {
        if (b == null) {
            synchronized (b.class) {
                if (b == null) {
                    b = new b();
                }
            }
        }
        return b;
    }

    public static boolean a(String str) {
        a b2 = b(str);
        if (b2 != null) {
            long currentTimeMillis = System.currentTimeMillis();
            long aQ = b2.aQ() + (b2.aM() * 1000);
            if (aQ > currentTimeMillis) {
                String str2 = a;
                g.b(str2, "app setting nexttime is not ready  [settingNextRequestTime= " + aQ + " currentTime = " + currentTimeMillis + Constants.RequestParameters.RIGHT_BRACKETS);
                return false;
            }
        }
        g.b(a, "app setting timeout or not exists");
        return true;
    }

    public static boolean a(String str, int i, String str2) {
        try {
            a b2 = b(str);
            if (b2 == null) {
                a();
                b2 = b();
            }
            Context h = a.d().h();
            String str3 = str + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + i + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + str2;
            long currentTimeMillis = System.currentTimeMillis();
            if (((Long) r.b(h, str3, 0L)).longValue() + (b2.aq() * 1000) > currentTimeMillis) {
                return false;
            }
            r.a(h, str3, Long.valueOf(currentTimeMillis));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static long a(String str, String str2) {
        List<String> b2;
        String str3 = "wall_style_" + str + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + str2;
        if (TextUtils.isEmpty(str2) && (b2 = com.mintegral.msdk.base.a.a.a.a().b()) != null && b2.size() > 0) {
            int i = 0;
            while (true) {
                if (i >= b2.size()) {
                    break;
                }
                String str4 = b2.get(i);
                if (str4.contains(str3.replace("null", ""))) {
                    str3 = str4;
                    break;
                }
                i++;
            }
        }
        String a2 = com.mintegral.msdk.base.a.a.a.a().a(str3);
        try {
            if (TextUtils.isEmpty(a2)) {
                return 0;
            }
            long optLong = new JSONObject(a2).optLong("current_time");
            g.b(a, "lastGetTime" + optLong);
            return optLong;
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static void b(String str, String str2) {
        com.mintegral.msdk.base.a.a.a.a().a(str, str2);
        a f = a.f(str2);
        d = f;
        if (f != null) {
            d.bb();
        }
        a aVar = d;
        if (aVar != null) {
            try {
                if (aVar.O() <= 0) {
                    t.a(i.a(a.d().h())).d();
                } else if (aVar.S() != null && aVar.S().size() > 0) {
                    if (aVar.T() == 1) {
                        List<n> a2 = q.a(i.a(a.d().h())).a(aVar.S().size());
                        ArrayList arrayList = new ArrayList();
                        if (a2 == null || a2.size() <= 0) {
                            a(aVar.S(), 0);
                            return;
                        }
                        int i = 0;
                        boolean z = false;
                        while (i < aVar.S().size()) {
                            int i2 = i + 1;
                            if (a2.size() >= i2) {
                                n nVar = a2.get(i);
                                CampaignEx campaignEx = aVar.S().get(i);
                                campaignEx.setPackageName(nVar.a());
                                campaignEx.setIex(1);
                                campaignEx.setTs(nVar.c());
                                String str3 = null;
                                switch (nVar.b()) {
                                    case 1:
                                        str3 = "im";
                                        break;
                                    case 2:
                                        str3 = "if";
                                        break;
                                    case 3:
                                        str3 = LanguageCodes.ICELANDIC;
                                        break;
                                }
                                campaignEx.setLabel(str3);
                                campaignEx.setPkgSource(nVar.d());
                                arrayList.add(campaignEx);
                                nVar.a(0);
                                z = true;
                            } else {
                                CampaignEx campaignEx2 = aVar.S().get(i);
                                campaignEx2.setIex(0);
                                campaignEx2.setTs(0);
                                campaignEx2.setLabel("im");
                                arrayList.add(campaignEx2);
                            }
                            i = i2;
                        }
                        if (z) {
                            a(arrayList, 1);
                            q.a(i.a(a.d().h())).a(a2);
                            return;
                        }
                        a(arrayList, 0);
                        return;
                    }
                    a(aVar.S(), 0);
                }
            } catch (Exception e) {
                if (MIntegralConstans.DEBUG) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static o a(List<CampaignEx> list, int i) {
        o oVar = new o();
        try {
            oVar.b(b(list, i));
            oVar.a(String.valueOf(i));
            if (t.a(i.a(a.d().h())).c() > 500) {
                t.a(i.a(a.d().h())).b(AppEventsConstants.EVENT_PARAM_VALUE_NO);
            }
            t.a(i.a(a.d().h())).a(oVar);
        } catch (Exception unused) {
        }
        return oVar;
    }

    private static String b(List<CampaignEx> list, int i) {
        String str = "key=2000041&iex=" + i + "&cal=";
        try {
            JSONArray jSONArray = new JSONArray();
            for (CampaignEx a2 : list) {
                jSONArray.put(a(a2));
            }
            return str + com.mintegral.msdk.base.utils.a.b(jSONArray.toString());
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
            return str;
        }
    }

    private static JSONObject a(CampaignEx campaignEx) {
        JSONObject jSONObject;
        if (campaignEx == null) {
            return null;
        }
        try {
            jSONObject = new JSONObject();
            try {
                jSONObject.put("campaignid", campaignEx.getId());
                jSONObject.put("packageName", campaignEx.getPackageName());
                jSONObject.put("title", campaignEx.getAppName());
                jSONObject.put("cta", campaignEx.getAdCall());
                jSONObject.put(CampaignEx.JSON_KEY_DESC, campaignEx.getAppDesc());
                jSONObject.put("image_url", campaignEx.getImageUrl());
                jSONObject.put(CampaignEx.JSON_KEY_IMPRESSION_URL, campaignEx.getImpressionURL());
                jSONObject.put(CampaignEx.JSON_KEY_ST_IEX, campaignEx.getIex());
                jSONObject.put(CampaignEx.JSON_KEY_ST_TS, campaignEx.getTs());
                jSONObject.put("label", campaignEx.getLabel());
                jSONObject.put("pkg_source", campaignEx.getPkgSource());
                return jSONObject;
            } catch (Exception e) {
                e = e;
            }
        } catch (Exception e2) {
            e = e2;
            jSONObject = null;
            if (!MIntegralConstans.DEBUG) {
                return jSONObject;
            }
            e.printStackTrace();
            return jSONObject;
        }
    }

    public static a b(String str) {
        if (d == null) {
            try {
                a f = a.f(com.mintegral.msdk.base.a.a.a.a().a(str));
                d = f;
                if (f != null) {
                    d.bb();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return d;
    }

    public static String c(String str) {
        if (str == null) {
            return "";
        }
        try {
            String a2 = com.mintegral.msdk.base.a.a.a.a().a(str);
            return a2 == null ? "" : a2;
        } catch (Exception e) {
            if (!MIntegralConstans.DEBUG) {
                return "";
            }
            e.printStackTrace();
            return "";
        }
    }

    public static void a(Context context, String str) {
        try {
            Map<String, ?> all = context.getSharedPreferences("mintegral", 0).getAll();
            for (String next : all.keySet()) {
                if (next.startsWith(str + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR)) {
                    c.put(next, d.a((String) all.get(next)));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static d c(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            str = a.d().j();
        }
        String str3 = str + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + str2;
        d dVar = null;
        if (c.containsKey(str3)) {
            return c.get(str3);
        }
        try {
            d a2 = d.a(com.mintegral.msdk.base.a.a.a.a().a(str3));
            try {
                c.put(str3, a2);
                return a2;
            } catch (Exception e) {
                e = e;
                dVar = a2;
                e.printStackTrace();
                return dVar;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return dVar;
        }
    }

    public static void a(String str, String str2, String str3) {
        String str4 = str + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + str2;
        com.mintegral.msdk.base.a.a.a.a().a(str4, str3);
        c.put(str4, d.a(str3));
    }

    public static a b() {
        a aVar = new a();
        aVar.e("US");
        aVar.aJ();
        aVar.aL();
        aVar.aO();
        aVar.aR();
        aVar.aS();
        aVar.aG();
        aVar.aI();
        aVar.aE();
        aVar.aB();
        aVar.al();
        aVar.ax();
        aVar.az();
        aVar.ah();
        aVar.aj();
        aVar.d("正在下载中，请去通知栏查看下载进度");
        aVar.b("mintegral");
        aVar.y();
        aVar.ar();
        aVar.af();
        aVar.ab();
        aVar.X();
        aVar.Z();
        aVar.av();
        aVar.an();
        aVar.A();
        aVar.U();
        aVar.P();
        aVar.R();
        aVar.ap();
        aVar.C();
        aVar.E();
        aVar.H();
        aVar.c(com.appsflyer.share.Constants.URL_MEDIA_SOURCE);
        aVar.J();
        aVar.L();
        aVar.N();
        aVar.at();
        aVar.n();
        aVar.l();
        aVar.p();
        aVar.r();
        aVar.t();
        aVar.w();
        aVar.a(com.mintegral.msdk.d.a.b.a);
        aVar.i();
        aVar.g();
        aVar.c();
        aVar.g("https://cdn-adn-https.rayjump.com/cdn-adn/v2/portal/19/08/20/11/06/5d5b63cb457e2.js");
        return aVar;
    }
}
