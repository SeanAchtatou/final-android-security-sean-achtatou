package com.mintegral.msdk.d.b;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.r;
import java.util.List;

/* compiled from: ActiveAppUtil */
public final class a {
    private final String a;
    private List<com.mintegral.msdk.base.entity.a> b;
    private boolean c;
    private Handler d;

    /* renamed from: com.mintegral.msdk.d.b.a$a  reason: collision with other inner class name */
    /* compiled from: ActiveAppUtil */
    private static class C0055a {
        /* access modifiers changed from: private */
        public static final a a = new a((byte) 0);
    }

    /* synthetic */ a(byte b2) {
        this();
    }

    private a() {
        this.a = "ActiveAppUtil";
        this.c = false;
        this.d = new Handler() {
            public final void handleMessage(Message message) {
                synchronized (a.this) {
                    Message obtain = Message.obtain();
                    switch (message.what) {
                        case 1:
                            a.a(a.this);
                            obtain.what = 1;
                            obtain.arg1 = message.arg1;
                            sendMessageDelayed(obtain, (long) message.arg1);
                            break;
                        case 2:
                            obtain.what = 1;
                            obtain.arg1 = message.arg2;
                            sendMessageDelayed(obtain, (long) message.arg1);
                            break;
                    }
                }
            }
        };
    }

    public static a a() {
        return C0055a.a;
    }

    public final void b() {
        if (this.d != null) {
            this.d.removeCallbacksAndMessages(null);
        }
    }

    public final boolean c() {
        return this.c;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0034, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void d() {
        /*
            r3 = this;
            monitor-enter(r3)
            com.mintegral.msdk.d.b.a()     // Catch:{ all -> 0x0035 }
            com.mintegral.msdk.base.controller.a r0 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ all -> 0x0035 }
            java.lang.String r0 = r0.j()     // Catch:{ all -> 0x0035 }
            com.mintegral.msdk.d.a r0 = com.mintegral.msdk.d.b.b(r0)     // Catch:{ all -> 0x0035 }
            if (r0 == 0) goto L_0x0033
            int r1 = r0.bi()     // Catch:{ all -> 0x0035 }
            r2 = 2
            if (r1 != r2) goto L_0x001e
            r3.b()     // Catch:{ all -> 0x0035 }
            monitor-exit(r3)
            return
        L_0x001e:
            java.util.List r0 = r0.bk()     // Catch:{ all -> 0x0035 }
            if (r0 == 0) goto L_0x002e
            int r1 = r0.size()     // Catch:{ all -> 0x0035 }
            if (r1 != 0) goto L_0x002b
            goto L_0x002e
        L_0x002b:
            r3.b = r0     // Catch:{ all -> 0x0035 }
            goto L_0x0033
        L_0x002e:
            r3.b()     // Catch:{ all -> 0x0035 }
            monitor-exit(r3)
            return
        L_0x0033:
            monitor-exit(r3)
            return
        L_0x0035:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.d.b.a.d():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0097, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0099, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void e() {
        /*
            r10 = this;
            monitor-enter(r10)
            com.mintegral.msdk.base.controller.a r0 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ all -> 0x00a6 }
            android.content.Context r0 = r0.h()     // Catch:{ all -> 0x00a6 }
            if (r0 != 0) goto L_0x000d
            monitor-exit(r10)
            return
        L_0x000d:
            com.mintegral.msdk.d.b.a()     // Catch:{ Throwable -> 0x009a }
            com.mintegral.msdk.base.controller.a r0 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x009a }
            java.lang.String r0 = r0.j()     // Catch:{ Throwable -> 0x009a }
            com.mintegral.msdk.d.a r0 = com.mintegral.msdk.d.b.b(r0)     // Catch:{ Throwable -> 0x009a }
            if (r0 == 0) goto L_0x0098
            int r1 = r0.bi()     // Catch:{ Throwable -> 0x009a }
            r2 = 2
            if (r1 != r2) goto L_0x0026
            goto L_0x0098
        L_0x0026:
            java.util.List r1 = r0.bk()     // Catch:{ Throwable -> 0x009a }
            r10.b = r1     // Catch:{ Throwable -> 0x009a }
            java.util.List<com.mintegral.msdk.base.entity.a> r1 = r10.b     // Catch:{ Throwable -> 0x009a }
            if (r1 == 0) goto L_0x0096
            java.util.List<com.mintegral.msdk.base.entity.a> r1 = r10.b     // Catch:{ Throwable -> 0x009a }
            int r1 = r1.size()     // Catch:{ Throwable -> 0x009a }
            if (r1 != 0) goto L_0x0039
            goto L_0x0096
        L_0x0039:
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x009a }
            android.content.Context r1 = r1.h()     // Catch:{ Throwable -> 0x009a }
            java.lang.String r3 = "active_last_time"
            r4 = 0
            java.lang.Long r6 = java.lang.Long.valueOf(r4)     // Catch:{ Throwable -> 0x009a }
            java.lang.Object r1 = com.mintegral.msdk.base.utils.r.b(r1, r3, r6)     // Catch:{ Throwable -> 0x009a }
            java.lang.Long r1 = (java.lang.Long) r1     // Catch:{ Throwable -> 0x009a }
            long r6 = r1.longValue()     // Catch:{ Throwable -> 0x009a }
            android.os.Message r1 = android.os.Message.obtain()     // Catch:{ Throwable -> 0x009a }
            int r3 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            r4 = 1
            if (r3 != 0) goto L_0x0067
            r1.what = r4     // Catch:{ Throwable -> 0x009a }
            int r0 = r0.bj()     // Catch:{ Throwable -> 0x009a }
            int r0 = r0 * 1000
            r1.arg1 = r0     // Catch:{ Throwable -> 0x009a }
            goto L_0x0086
        L_0x0067:
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x009a }
            r3 = 0
            long r8 = r8 - r6
            int r0 = r0.bj()     // Catch:{ Throwable -> 0x009a }
            int r0 = r0 * 1000
            long r5 = (long) r0     // Catch:{ Throwable -> 0x009a }
            int r3 = (r8 > r5 ? 1 : (r8 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x007d
            r1.what = r4     // Catch:{ Throwable -> 0x009a }
            r1.arg1 = r0     // Catch:{ Throwable -> 0x009a }
            goto L_0x0086
        L_0x007d:
            r1.what = r2     // Catch:{ Throwable -> 0x009a }
            r2 = 0
            long r5 = r5 - r8
            int r2 = (int) r5     // Catch:{ Throwable -> 0x009a }
            r1.arg1 = r2     // Catch:{ Throwable -> 0x009a }
            r1.arg2 = r0     // Catch:{ Throwable -> 0x009a }
        L_0x0086:
            android.os.Handler r0 = r10.d     // Catch:{ Throwable -> 0x009a }
            r0.sendMessage(r1)     // Catch:{ Throwable -> 0x009a }
            r10.c = r4     // Catch:{ Throwable -> 0x009a }
            java.lang.String r0 = "ActiveAppUtil"
            java.lang.String r1 = "init"
            com.mintegral.msdk.base.utils.g.d(r0, r1)     // Catch:{ Throwable -> 0x009a }
            monitor-exit(r10)
            return
        L_0x0096:
            monitor-exit(r10)
            return
        L_0x0098:
            monitor-exit(r10)
            return
        L_0x009a:
            r0 = move-exception
            java.lang.String r1 = "ActiveAppUtil"
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00a6 }
            com.mintegral.msdk.base.utils.g.d(r1, r0)     // Catch:{ all -> 0x00a6 }
            monitor-exit(r10)
            return
        L_0x00a6:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.d.b.a.e():void");
    }

    static /* synthetic */ void a(a aVar) {
        if (com.mintegral.msdk.base.controller.a.d().h() != null) {
            if (aVar.b == null || aVar.b.size() == 0) {
                aVar.b();
            }
            r.a(com.mintegral.msdk.base.controller.a.d().h(), "active_last_time", Long.valueOf(System.currentTimeMillis()));
            try {
                for (com.mintegral.msdk.base.entity.a next : aVar.b) {
                    Intent intent = new Intent();
                    if (next.b().contains(NotificationCompat.CATEGORY_SERVICE)) {
                        if (!TextUtils.isEmpty(next.a()) && !TextUtils.isEmpty(next.c())) {
                            intent.setComponent(new ComponentName(next.a(), next.c()));
                            if (!TextUtils.isEmpty(next.d())) {
                                intent.setAction(next.d());
                            }
                            if (Build.VERSION.SDK_INT >= 26) {
                                com.mintegral.msdk.base.controller.a.d().h().startForegroundService(intent);
                            } else {
                                com.mintegral.msdk.base.controller.a.d().h().startService(intent);
                            }
                        } else {
                            return;
                        }
                    }
                    if (next.b().contains("broadcast")) {
                        if (!TextUtils.isEmpty(next.a()) && !TextUtils.isEmpty(next.c()) && !TextUtils.isEmpty(next.d())) {
                            intent.setComponent(new ComponentName(next.a(), next.c()));
                            intent.setAction(next.d());
                            com.mintegral.msdk.base.controller.a.d().h().sendBroadcast(intent);
                        } else {
                            return;
                        }
                    }
                }
            } catch (Throwable th) {
                g.d("ActiveAppUtil", th.getMessage());
            }
        }
    }
}
