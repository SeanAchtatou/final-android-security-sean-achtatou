package com.mintegral.msdk.interstitialvideo.out;

import android.content.Context;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.reward.b.a;

public class MTGBidInterstitialVideoHandler {
    private a a;

    public MTGBidInterstitialVideoHandler(Context context, String str) {
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context);
        }
        a(str);
    }

    public MTGBidInterstitialVideoHandler(String str) {
        a(str);
    }

    private void a(String str) {
        try {
            if (this.a == null) {
                this.a = new a();
                this.a.a(true);
                this.a.a();
            }
            this.a.b(str);
        } catch (Throwable th) {
            g.c("MTGBidRewardVideoHandler", th.getMessage(), th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.reward.b.a.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.mintegral.msdk.reward.b.a.a(com.mintegral.msdk.reward.b.a, java.lang.String):void
      com.mintegral.msdk.reward.b.a.a(java.lang.String, int):void
      com.mintegral.msdk.reward.b.a.a(boolean, boolean):void
      com.mintegral.msdk.reward.b.a.a(java.lang.String, java.lang.String):void
      com.mintegral.msdk.reward.b.a.a(boolean, java.lang.String):void */
    public void loadFromBid(String str) {
        if (this.a != null) {
            this.a.a(true, str);
        }
    }

    public void loadFormSelfFilling() {
        if (this.a != null) {
            this.a.b(false);
        }
    }

    public boolean isBidReady() {
        if (this.a != null) {
            return this.a.c(true);
        }
        return false;
    }

    public void showFromBid() {
        if (this.a != null) {
            this.a.a((String) null, (String) null);
        }
    }

    public void setRewardVideoListener(InterstitialVideoListener interstitialVideoListener) {
        if (this.a != null) {
            this.a.a(new com.mintegral.msdk.interstitialvideo.a.a(interstitialVideoListener));
        }
    }

    public void setInterstitialVideoListener(InterstitialVideoListener interstitialVideoListener) {
        if (this.a != null) {
            this.a.a(new com.mintegral.msdk.interstitialvideo.a.a(interstitialVideoListener));
        }
    }

    public void clearVideoCache() {
        try {
            if (this.a != null) {
                a.b();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playVideoMute(int i) {
        if (this.a != null) {
            this.a.a(i);
        }
    }
}
