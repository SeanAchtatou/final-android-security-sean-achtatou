package com.mintegral.msdk.interstitialvideo.out;

import android.content.Context;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.reward.b.a;

public class MTGInterstitialVideoHandler {
    private a a;

    public MTGInterstitialVideoHandler(Context context, String str) {
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context);
        }
        a(str);
    }

    public MTGInterstitialVideoHandler(String str) {
        a(str);
    }

    private void a(String str) {
        try {
            if (this.a == null) {
                this.a = new a();
                this.a.a(true);
            }
            this.a.b(str);
        } catch (Throwable th) {
            g.c("MTGRewardVideoHandler", th.getMessage(), th);
        }
    }

    public void load() {
        if (this.a != null) {
            this.a.b(true);
        }
    }

    public void loadFormSelfFilling() {
        if (this.a != null) {
            this.a.b(false);
        }
    }

    public boolean isReady() {
        if (this.a != null) {
            return this.a.c(true);
        }
        return false;
    }

    public void show() {
        if (this.a != null) {
            this.a.a((String) null, (String) null);
        }
    }

    public void setRewardVideoListener(InterstitialVideoListener interstitialVideoListener) {
        if (this.a != null) {
            this.a.a(new com.mintegral.msdk.interstitialvideo.a.a(interstitialVideoListener));
        }
    }

    public void setInterstitialVideoListener(InterstitialVideoListener interstitialVideoListener) {
        if (this.a != null) {
            this.a.a(new com.mintegral.msdk.interstitialvideo.a.a(interstitialVideoListener));
        }
    }

    public void clearVideoCache() {
        try {
            if (this.a != null) {
                a.b();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playVideoMute(int i) {
        if (this.a != null) {
            this.a.a(i);
        }
    }
}
