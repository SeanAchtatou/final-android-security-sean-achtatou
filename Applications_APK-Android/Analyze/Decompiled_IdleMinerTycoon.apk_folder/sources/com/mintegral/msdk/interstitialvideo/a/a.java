package com.mintegral.msdk.interstitialvideo.a;

import com.mintegral.msdk.interstitialvideo.out.InterstitialVideoListener;
import com.mintegral.msdk.videocommon.listener.InterVideoOutListener;

/* compiled from: DecoratorInterstitialListener */
public final class a implements InterVideoOutListener {
    private InterstitialVideoListener a;

    public a(InterstitialVideoListener interstitialVideoListener) {
        this.a = interstitialVideoListener;
    }

    public final void onAdShow() {
        if (this.a != null) {
            this.a.onAdShow();
        }
    }

    public final void onAdClose(boolean z, String str, float f) {
        if (this.a != null) {
            this.a.onAdClose(z);
        }
    }

    public final void onShowFail(String str) {
        if (this.a != null) {
            this.a.onShowFail(str);
        }
    }

    public final void onVideoAdClicked(boolean z, String str) {
        if (this.a != null) {
            this.a.onVideoAdClicked(str);
        }
    }

    public final void onVideoComplete(String str) {
        if (this.a != null) {
            this.a.onVideoComplete(str);
        }
    }

    public final void onEndcardShow(String str) {
        if (this.a != null) {
            this.a.onEndcardShow(str);
        }
    }

    public final void onVideoLoadFail(String str) {
        if (this.a != null) {
            this.a.onVideoLoadFail(str);
        }
    }

    public final void onVideoLoadSuccess(String str) {
        if (this.a != null) {
            this.a.onVideoLoadSuccess(str);
        }
    }

    public final void onLoadSuccess(String str) {
        if (this.a != null) {
            this.a.onLoadSuccess(str);
        }
    }
}
