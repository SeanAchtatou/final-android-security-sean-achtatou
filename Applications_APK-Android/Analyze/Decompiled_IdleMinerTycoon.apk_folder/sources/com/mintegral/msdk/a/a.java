package com.mintegral.msdk.a;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import com.mintegral.msdk.base.utils.g;
import com.uodis.opendevice.aidl.OpenDeviceIdentifierService;

/* compiled from: OaidAidlUtil */
public final class a {
    private Context a;
    private ServiceConnection b;
    /* access modifiers changed from: private */
    public OpenDeviceIdentifierService c;
    /* access modifiers changed from: private */
    public b d;

    public a(Context context) {
        this.a = context;
    }

    public final void a(b bVar) {
        this.d = bVar;
        g.a("OaidAidlUtil", "bindService");
        if (this.a == null) {
            g.d("OaidAidlUtil", "context is null");
            return;
        }
        this.b = new C0049a(this, (byte) 0);
        Intent intent = new Intent("com.uodis.opendevice.OPENIDS_SERVICE");
        intent.setPackage("com.huawei.hwid");
        boolean bindService = this.a.bindService(intent, this.b, 1);
        g.b("OaidAidlUtil", "bindService result: " + bindService);
    }

    /* renamed from: com.mintegral.msdk.a.a$a  reason: collision with other inner class name */
    /* compiled from: OaidAidlUtil */
    private final class C0049a implements ServiceConnection {
        /* synthetic */ C0049a(a aVar, byte b) {
            this();
        }

        private C0049a() {
        }

        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            g.b("OaidAidlUtil", "onServiceConnected");
            OpenDeviceIdentifierService unused = a.this.c = OpenDeviceIdentifierService.Stub.asInterface(iBinder);
            if (a.this.c != null) {
                try {
                    if (a.this.d != null) {
                        a.this.d.a(a.this.c.getOaid(), a.this.c.isOaidTrackLimited());
                    }
                } catch (RemoteException e) {
                    g.d("OaidAidlUtil", "getChannelInfo RemoteException");
                    if (a.this.d != null) {
                        a.this.d.a(e.getMessage());
                    }
                } catch (Exception e2) {
                    g.d("OaidAidlUtil", "getChannelInfo Excepition");
                    if (a.this.d != null) {
                        a.this.d.a(e2.getMessage());
                    }
                } catch (Throwable th) {
                    a.c(a.this);
                    throw th;
                }
                a.c(a.this);
            }
        }

        public final void onServiceDisconnected(ComponentName componentName) {
            g.b("OaidAidlUtil", "onServiceDisconnected");
            OpenDeviceIdentifierService unused = a.this.c = null;
        }
    }

    static /* synthetic */ void c(a aVar) {
        g.b("OaidAidlUtil", "unbindService");
        if (aVar.a == null) {
            g.d("OaidAidlUtil", "context is null");
        } else if (aVar.b != null) {
            aVar.a.unbindService(aVar.b);
            aVar.c = null;
            aVar.a = null;
            aVar.d = null;
        }
    }
}
