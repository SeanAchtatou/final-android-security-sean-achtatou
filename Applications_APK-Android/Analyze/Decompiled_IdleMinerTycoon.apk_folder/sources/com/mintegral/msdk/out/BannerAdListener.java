package com.mintegral.msdk.out;

public interface BannerAdListener {
    void closeFullScreen();

    void onClick();

    void onLeaveApp();

    void onLoadFailed(String str);

    void onLoadSuccessed();

    void onLogImpression();

    void showFullScreen();
}
