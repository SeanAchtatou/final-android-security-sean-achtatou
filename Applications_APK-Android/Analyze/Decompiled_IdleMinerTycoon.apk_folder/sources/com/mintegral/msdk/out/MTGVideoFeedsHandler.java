package com.mintegral.msdk.out;

import android.content.Context;
import com.mintegral.msdk.videofeeds.b.a;
import com.mintegral.msdk.videofeeds.vfplayer.VideoFeedsAdView;
import java.util.Map;

public class MTGVideoFeedsHandler {
    private a a;

    public MTGVideoFeedsHandler(Context context, Map<String, Object> map) {
        if (this.a == null) {
            this.a = new a();
        }
        this.a.a(context, map);
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context.getApplicationContext());
        }
    }

    public void load() {
        if (this.a != null) {
            this.a.a();
        }
    }

    public VideoFeedsAdView show() {
        try {
            if (this.a != null) {
                return this.a.b();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setVideoFeedsListener(VideoFeedsListener videoFeedsListener) {
        if (this.a != null) {
            this.a.a(videoFeedsListener);
        }
    }

    public void clearVideoCache() {
        try {
            if (this.a != null) {
                a.c();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
