package com.mintegral.msdk.out;

import android.content.Context;
import com.mintegral.msdk.interstitial.c.a;
import java.util.Map;

public class MTGInterstitialHandler {
    private a a;

    public MTGInterstitialHandler(Context context, Map<String, Object> map) {
        if (this.a == null) {
            this.a = new a();
        }
        this.a.a(context, map);
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context);
        }
    }

    public void preload() {
        try {
            if (this.a != null) {
                this.a.a();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void show() {
        try {
            this.a.b();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setInterstitialListener(InterstitialListener interstitialListener) {
        try {
            if (this.a != null) {
                this.a.a(interstitialListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
