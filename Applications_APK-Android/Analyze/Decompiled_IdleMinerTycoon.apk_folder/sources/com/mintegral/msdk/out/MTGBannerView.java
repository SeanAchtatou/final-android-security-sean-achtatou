package com.mintegral.msdk.out;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import com.mintegral.msdk.mtgbanner.a.a;

public class MTGBannerView extends RelativeLayout {
    /* access modifiers changed from: private */
    public a a;
    private BannerAdListener b;
    private boolean c;

    public MTGBannerView(Context context) {
        this(context, null);
    }

    public MTGBannerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public MTGBannerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.c = false;
    }

    public void init(BannerSize bannerSize, String str) {
        this.a = new a(this, bannerSize, str);
    }

    public void updateBannerSize(BannerSize bannerSize) {
        if (this.a != null) {
            this.a.a(bannerSize);
        }
    }

    public void setRefreshTime(int i) {
        if (this.a != null) {
            this.a.a(i);
        }
    }

    public void setAllowShowCloseBtn(boolean z) {
        if (this.a != null) {
            this.a.a(z);
        }
    }

    public void load() {
        if (this.a != null) {
            this.a.b(this.c);
            this.a.a("");
        } else if (this.b != null) {
            this.b.onLoadFailed("banner controler init error，please check it");
        }
    }

    public void loadFromBid(String str) {
        if (this.a != null) {
            if (!TextUtils.isEmpty(str)) {
                this.a.b(this.c);
                this.a.a(str);
            } else if (this.b != null) {
                this.b.onLoadFailed("banner token is null or empty，please check it");
            }
        } else if (this.b != null) {
            this.b.onLoadFailed("banner controler init error，please check it");
        }
    }

    public void setBannerAdListener(BannerAdListener bannerAdListener) {
        this.b = bannerAdListener;
        if (this.a != null) {
            this.a.a(bannerAdListener);
        }
    }

    public void release() {
        if (this.b != null) {
            this.b = null;
        }
        if (this.a != null) {
            this.a.a((BannerAdListener) null);
            this.a.a();
        }
        removeAllViews();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        a(true);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        a(false);
    }

    private void a(boolean z) {
        this.c = z;
        if (this.a != null) {
            this.a.b(this.c);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        if (this.a == null) {
            return;
        }
        if (i == 0) {
            a();
        } else {
            this.a.c(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        if (this.a == null) {
            return;
        }
        if (i == 0) {
            a();
        } else {
            this.a.c(false);
        }
    }

    private void a() {
        postDelayed(new Runnable() {
            public void run() {
                if (MTGBannerView.this.a != null) {
                    MTGBannerView.this.a.c(true);
                }
            }
        }, 200);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (this.a != null) {
            this.a.a(i, i2, i3, i4);
        }
    }
}
