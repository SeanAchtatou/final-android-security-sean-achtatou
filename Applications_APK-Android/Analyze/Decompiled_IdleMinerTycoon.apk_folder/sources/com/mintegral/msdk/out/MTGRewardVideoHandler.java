package com.mintegral.msdk.out;

import android.content.Context;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.reward.b.a;

public class MTGRewardVideoHandler {
    private a a;

    public MTGRewardVideoHandler(Context context, String str) {
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context);
        }
        a(str);
    }

    public MTGRewardVideoHandler(String str) {
        a(str);
    }

    private void a(String str) {
        try {
            if (this.a == null) {
                this.a = new a();
                this.a.a(false);
            }
            this.a.b(str);
        } catch (Throwable th) {
            g.c("MTGRewardVideoHandler", th.getMessage(), th);
        }
    }

    public void load() {
        if (this.a != null) {
            this.a.b(true);
        }
    }

    public void loadFormSelfFilling() {
        if (this.a != null) {
            this.a.b(false);
        }
    }

    public boolean isReady() {
        if (this.a != null) {
            return this.a.c(false);
        }
        return false;
    }

    public void show(String str) {
        if (this.a != null) {
            this.a.a(str, (String) null);
        }
    }

    public void show(String str, String str2) {
        if (this.a != null) {
            this.a.a(str, str2);
        }
    }

    public void setRewardVideoListener(RewardVideoListener rewardVideoListener) {
        if (this.a != null) {
            this.a.a(new com.mintegral.msdk.reward.c.a(rewardVideoListener));
        }
    }

    public void clearVideoCache() {
        try {
            if (this.a != null) {
                a.b();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playVideoMute(int i) {
        if (this.a != null) {
            this.a.a(i);
        }
    }

    public void setAlertDialogText(String str, String str2, String str3, String str4) {
        if (this.a != null) {
            this.a.a(str, str2, str3, str4);
        }
    }
}
