package com.mintegral.msdk.out;

import android.content.Context;
import java.util.Map;

public abstract class MtgBidCommonHandler {
    protected Map<String, Object> a;
    protected Context b;

    public abstract void bidLoad(String str);

    public abstract void bidRelease();

    public MtgBidCommonHandler() {
    }

    public MtgBidCommonHandler(Map<String, Object> map, Context context) {
        this.a = map;
        this.b = context;
    }
}
