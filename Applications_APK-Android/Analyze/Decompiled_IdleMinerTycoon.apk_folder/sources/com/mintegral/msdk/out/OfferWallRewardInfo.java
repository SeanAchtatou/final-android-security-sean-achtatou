package com.mintegral.msdk.out;

public class OfferWallRewardInfo {
    private String a;
    private int b;

    public OfferWallRewardInfo(String str, int i) {
        this.a = str;
        this.b = i;
    }

    public String getRewardName() {
        return this.a;
    }

    public void setRewardName(String str) {
        this.a = str;
    }

    public int getRewardAmount() {
        return this.b;
    }

    public void setRewardAmount(int i) {
        this.b = i;
    }
}
