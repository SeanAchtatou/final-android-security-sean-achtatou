package com.mintegral.msdk.out;

import android.content.Context;
import com.mintegral.msdk.offerwall.c.a;
import java.util.Map;

public class MTGOfferWallHandler {
    private a a;

    public MTGOfferWallHandler(Context context, Map<String, Object> map) {
        if (this.a == null) {
            this.a = new a();
        }
        this.a.a(context, map);
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context.getApplicationContext());
        }
    }

    public void load() {
        try {
            if (this.a != null) {
                this.a.a();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void show() {
        try {
            if (this.a != null) {
                this.a.b();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setOfferWallListener(OfferWallListener offerWallListener) {
        try {
            if (this.a != null) {
                this.a.a(offerWallListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void queryOfferWallRewards(MTGOfferWallRewardListener mTGOfferWallRewardListener) {
        try {
            if (this.a != null) {
                this.a.a(mTGOfferWallRewardListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
