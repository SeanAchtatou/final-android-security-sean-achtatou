package com.mintegral.msdk.out;

import com.mintegral.msdk.system.a;

public class MIntegralSDKFactory {
    private static a a;

    private MIntegralSDKFactory() {
    }

    public static a getMIntegralSDK() {
        if (a == null) {
            synchronized (MIntegralSDKFactory.class) {
                if (a == null) {
                    a = new a();
                }
            }
        }
        return a;
    }
}
