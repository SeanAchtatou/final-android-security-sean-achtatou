package com.mintegral.msdk.out;

import android.content.Context;
import java.util.Map;

public abstract class MtgCommonHandler {
    protected Map<String, Object> a;
    protected Context b;

    public abstract boolean load();

    public abstract void release();

    public MtgCommonHandler() {
    }

    public MtgCommonHandler(Map<String, Object> map, Context context) {
        this.a = map;
        this.b = context;
    }
}
