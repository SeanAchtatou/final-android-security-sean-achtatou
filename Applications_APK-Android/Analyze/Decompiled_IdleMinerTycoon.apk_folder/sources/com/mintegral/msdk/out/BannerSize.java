package com.mintegral.msdk.out;

import com.google.android.gms.nearby.messages.Strategy;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.utils.c;

public class BannerSize {
    public static final int DEV_SET_TYPE = 5;
    public static final int LARGE_TYPE = 1;
    public static final int MEDIUM_TYPE = 2;
    public static final int SMART_TYPE = 3;
    public static final int STANDARD_TYPE = 4;
    private int a;
    private int b;

    public BannerSize(int i, int i2, int i3) {
        switch (i) {
            case 1:
                this.a = 90;
                this.b = 320;
                return;
            case 2:
                this.a = 250;
                this.b = Strategy.TTL_SECONDS_DEFAULT;
                return;
            case 3:
                if (c.o(a.d().h()) < 720) {
                    this.a = 50;
                    this.b = 320;
                    return;
                }
                this.a = 90;
                this.b = 728;
                return;
            case 4:
                this.a = 50;
                this.b = 320;
                return;
            case 5:
                this.a = i3;
                this.b = i2;
                return;
            default:
                return;
        }
    }

    public int getHeight() {
        return this.a;
    }

    public int getWidth() {
        return this.b;
    }
}
