package com.mintegral.msdk.shell;

import android.content.Intent;
import android.os.IBinder;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgdownload.c;
import com.mintegral.msdk.pluginFramework.PluginService;
import com.mintegral.msdk.pluginFramework.a;

public class MTGService extends PluginService {
    public void onCreate() {
        MTGService.super.onCreate();
        this.a.a.b();
    }

    public final a a() {
        try {
            return new a(new a.a(new c()));
        } catch (Exception e) {
            g.c("Download", "Find Provider Error", e);
            return null;
        }
    }

    public IBinder onBind(Intent intent) {
        return this.a.a.a();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        return this.a.a.a(intent);
    }

    public void onDestroy() {
        this.a.a.c();
    }
}
