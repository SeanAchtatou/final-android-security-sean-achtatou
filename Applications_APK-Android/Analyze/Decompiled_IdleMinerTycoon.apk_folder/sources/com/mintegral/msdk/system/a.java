package com.mintegral.msdk.system;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Looper;
import com.google.android.gms.drive.DriveFile;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.MIntegralSDK;
import com.mintegral.msdk.MIntegralUser;
import com.mintegral.msdk.base.controller.authoritycontroller.AuthorityInfoBean;
import com.mintegral.msdk.base.controller.authoritycontroller.CallBackForDeveloper;
import com.mintegral.msdk.base.controller.b;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.click.AppReceiver;
import com.mintegral.msdk.out.AdMobClickListener;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* compiled from: MIntegralSDKImpl */
public final class a implements MIntegralSDK {
    public static Map<String, String> a;
    private static final Lock b = new ReentrantReadWriteLock().writeLock();
    private volatile MIntegralSDK.PLUGIN_LOAD_STATUS c = MIntegralSDK.PLUGIN_LOAD_STATUS.INITIAL;
    /* access modifiers changed from: private */
    public Context d;
    private boolean e = false;
    private AppReceiver f = null;
    private boolean g = false;
    private BroadcastReceiver h = null;

    public final void setThirdPartyFeatures(Map<String, Object> map) {
    }

    /* access modifiers changed from: private */
    public static boolean b(Context context, String str) {
        if (context != null) {
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 2);
                if (!(packageInfo == null || packageInfo.receivers == null)) {
                    for (ActivityInfo activityInfo : packageInfo.receivers) {
                        if (activityInfo != null && str.equals(activityInfo.name)) {
                            return true;
                        }
                    }
                }
            } catch (PackageManager.NameNotFoundException e2) {
                if (MIntegralConstans.DEBUG) {
                    e2.printStackTrace();
                }
            } catch (Exception e3) {
                if (MIntegralConstans.DEBUG) {
                    e3.printStackTrace();
                }
            }
        }
        return false;
    }

    private void a() {
        b.lock();
        try {
            b.a().a(a, this.d);
            this.c = MIntegralSDK.PLUGIN_LOAD_STATUS.COMPLETED;
            new Thread(new Runnable() {
                public final void run() {
                    if (a.a(a.this.d)) {
                        if (a.b(a.this.d, "com.mintegral.msdk.click.AppReceiver")) {
                            a.a(a.this, a.this.d);
                        }
                        if (a.b(a.this.d, "com.alphab.receiver.AlphabReceiver")) {
                            a.b(a.this, a.this.d);
                        }
                    }
                }
            }).start();
            new Thread(new Runnable() {
                public final void run() {
                    Looper.prepare();
                    com.mintegral.msdk.d.b.a.a().e();
                    Looper.loop();
                }
            }).start();
        } catch (Exception e2) {
            if (MIntegralConstans.DEBUG) {
                g.c("com.mintegral.msdk", "无法初始化MMSDK", e2);
                e2.printStackTrace();
            }
        }
        b.unlock();
    }

    public final MIntegralSDK.PLUGIN_LOAD_STATUS getStatus() {
        return this.c;
    }

    public final void preload(Map<String, Object> map) {
        if (this.c == MIntegralSDK.PLUGIN_LOAD_STATUS.COMPLETED) {
            b.a().a(map, 0);
        }
    }

    public final void setAdMobClickListener(AdMobClickListener adMobClickListener) {
        b.a().a(adMobClickListener);
    }

    public final void reportUser(MIntegralUser mIntegralUser) {
        b.a().a(mIntegralUser);
    }

    public final void setUserPrivateInfoType(Context context, String str, int i) {
        b(context);
        com.mintegral.msdk.base.controller.authoritycontroller.a.a().a(str, i);
    }

    public final AuthorityInfoBean userPrivateInfo(Context context) {
        b(context);
        return com.mintegral.msdk.base.controller.authoritycontroller.a.a().b();
    }

    public final void showUserPrivateInfoTips(Context context, CallBackForDeveloper callBackForDeveloper) {
        b(context);
        com.mintegral.msdk.base.controller.authoritycontroller.a.a().b = callBackForDeveloper;
        WeakReference weakReference = new WeakReference(context);
        com.mintegral.msdk.d.b.a();
        com.mintegral.msdk.d.a b2 = com.mintegral.msdk.d.b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b2 == null) {
            com.mintegral.msdk.d.b.a();
            b2 = com.mintegral.msdk.d.b.b();
        }
        int s = b2.s();
        if (s == 1) {
            Context h2 = com.mintegral.msdk.base.controller.a.d().h();
            if (h2 == null && (weakReference.get() instanceof Context) && weakReference.get() != null) {
                h2 = (Context) weakReference.get();
            }
            try {
                Class.forName("com.mintegral.msdk.mtgjscommon.authority.activity.MTGAuthorityActivity");
                Intent intent = new Intent(h2, Class.forName("com.mintegral.msdk.mtgjscommon.authority.activity.MTGAuthorityActivity"));
                intent.setFlags(DriveFile.MODE_READ_ONLY);
                h2.startActivity(intent);
            } catch (ClassNotFoundException e2) {
                e2.printStackTrace();
            } catch (Throwable th) {
                th.printStackTrace();
            }
        } else if (s == 0) {
            callBackForDeveloper.onShowPopWindowStatusFaile(MIntegralConstans.AUTHORITY_APP_LOAD_FAILED);
        }
    }

    public final void setConsentStatus(Context context, int i) {
        b(context);
        com.mintegral.msdk.base.controller.authoritycontroller.a.a().a(i);
    }

    public final boolean getConsentStatus(Context context) {
        b(context);
        return com.mintegral.msdk.base.controller.authoritycontroller.a.a().e();
    }

    public final void preloadFrame(Map<String, Object> map) {
        b.a().a(map, 1);
    }

    public final Map<String, String> getMTGConfigurationMap(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put(MIntegralConstans.ID_MINTEGRAL_APPID, str);
        hashMap.put(MIntegralConstans.ID_MINTEGRAL_APPKEY, str2);
        hashMap.put(MIntegralConstans.ID_MINTEGRAL_STARTUPCRASH, "1");
        return hashMap;
    }

    public final void release() {
        if (this.c == MIntegralSDK.PLUGIN_LOAD_STATUS.COMPLETED) {
            b.a().b();
        }
        Context context = this.d;
        if (!(context == null || this.f == null || !this.e)) {
            this.e = false;
            context.unregisterReceiver(this.f);
        }
        Context context2 = this.d;
        if (!(context2 == null || this.h == null || !this.g)) {
            this.g = false;
            context2.unregisterReceiver(this.h);
        }
        com.mintegral.msdk.d.b.a.a().b();
    }

    public final void a(Application application) {
        this.d = application.getApplicationContext();
        a();
    }

    public final void init(Map<String, String> map, Application application) {
        this.d = application.getApplicationContext();
        a = map;
        a();
    }

    public final void initAsync(Map<String, String> map, Application application) {
        this.d = application.getApplicationContext();
        a = map;
        a();
    }

    public final void init(Map<String, String> map, Context context) {
        this.d = context;
        a = map;
        a();
    }

    public final void initAsync(Map<String, String> map, Context context) {
        this.d = context;
        a = map;
        a();
    }

    private static void b(Context context) {
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context);
        }
    }

    static /* synthetic */ boolean a(Context context) {
        if ((context != null ? c.m(context) : 0) < 26 || Build.VERSION.SDK_INT < 26) {
            return false;
        }
        return true;
    }

    static /* synthetic */ void a(a aVar, Context context) {
        if (context != null && !aVar.e) {
            aVar.e = true;
            aVar.f = new AppReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
            intentFilter.addDataScheme("package");
            context.registerReceiver(aVar.f, intentFilter);
        }
    }

    static /* synthetic */ void b(a aVar, Context context) {
        Class<?> cls;
        if (context != null) {
            try {
                if (!aVar.g && (cls = Class.forName("com.alphab.receiver.AlphabReceiver")) != null && (cls.newInstance() instanceof BroadcastReceiver)) {
                    aVar.g = true;
                    aVar.h = (BroadcastReceiver) cls.newInstance();
                    IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
                    intentFilter.addDataScheme("package");
                    context.registerReceiver(aVar.h, intentFilter);
                }
            } catch (ClassNotFoundException e2) {
                if (MIntegralConstans.DEBUG) {
                    e2.printStackTrace();
                }
            } catch (Exception e3) {
                if (MIntegralConstans.DEBUG) {
                    e3.printStackTrace();
                }
            }
        }
    }
}
