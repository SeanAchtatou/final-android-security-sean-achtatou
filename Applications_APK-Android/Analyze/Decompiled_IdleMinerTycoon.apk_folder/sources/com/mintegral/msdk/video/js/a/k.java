package com.mintegral.msdk.video.js.a;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.video.module.MintegralContainerView;
import com.mintegral.msdk.videocommon.e.b;
import com.mintegral.msdk.videocommon.e.c;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: JSRewardVideoV1 */
public final class k extends e {
    private Activity a;
    private MintegralContainerView b;

    public k(Activity activity, MintegralContainerView mintegralContainerView) {
        this.a = activity;
        this.b = mintegralContainerView;
    }

    public final String a() {
        c cVar;
        if (this.b == null) {
            super.a();
        } else {
            try {
                ArrayList arrayList = new ArrayList();
                arrayList.add(this.b.getCampaign());
                String unitID = this.b.getUnitID();
                if (b.a() == null) {
                    cVar = null;
                } else {
                    b.a();
                    cVar = b.a(com.mintegral.msdk.base.controller.a.d().j(), unitID);
                }
                JSONObject jSONObject = new JSONObject();
                if (cVar != null) {
                    jSONObject = cVar.R();
                }
                return a(arrayList, unitID, "MAL_10.1.51,3.0.1", jSONObject);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return super.a();
    }

    public final void toggleCloseBtn(int i) {
        super.toggleCloseBtn(i);
        if (this.b != null) {
            this.b.toggleCloseBtn(i);
        }
    }

    public final void a(String str) {
        super.a(str);
        try {
            if (this.a != null && !TextUtils.isEmpty(str) && str.equals("click")) {
                if (this.b != null) {
                    this.b.triggerCloseBtn(str);
                }
                this.a.finish();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public final void notifyCloseBtn(int i) {
        super.notifyCloseBtn(i);
        if (this.b != null) {
            this.b.notifyCloseBtn(i);
        }
    }

    public final void c(String str) {
        super.c(str);
        try {
            if (this.a != null && !TextUtils.isEmpty(str) && this.b != null) {
                this.b.handlerPlayableException(str);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public final void b(String str) {
        super.b(str);
        try {
            if (this.a != null && !TextUtils.isEmpty(str)) {
                if (str.equals("landscape")) {
                    this.a.setRequestedOrientation(0);
                } else if (str.equals("portrait")) {
                    this.a.setRequestedOrientation(1);
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private static String a(List<CampaignEx> list, String str, String str2, JSONObject jSONObject) {
        try {
            if (list.size() <= 0) {
                return null;
            }
            a aVar = new a(com.mintegral.msdk.base.controller.a.d().h());
            JSONArray parseCamplistToJson = CampaignEx.parseCamplistToJson(list);
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("campaignList", parseCamplistToJson);
            jSONObject2.put("device", aVar.a());
            jSONObject2.put(MIntegralConstans.PROPERTIES_UNIT_ID, str);
            jSONObject2.put("sdk_info", str2);
            jSONObject2.put("unitSetting", jSONObject);
            if (com.mintegral.msdk.d.b.a() != null) {
                com.mintegral.msdk.d.b.a();
                String c = com.mintegral.msdk.d.b.c(com.mintegral.msdk.base.controller.a.d().j());
                if (!TextUtils.isEmpty(c)) {
                    jSONObject2.put("appSetting", new JSONObject(c));
                }
            }
            return jSONObject2.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* compiled from: JSRewardVideoV1 */
    private static final class a {
        public String a = com.mintegral.msdk.base.utils.c.d();
        public String b = com.mintegral.msdk.base.utils.c.i();
        public String c = "android";
        public String d;
        public String e;
        public String f;
        public String g;
        public String h;
        public String i;
        public String j;
        public String k;
        public String l;
        public String m;
        public String n;
        public String o;

        public a(Context context) {
            this.d = com.mintegral.msdk.base.utils.c.c(context);
            this.e = com.mintegral.msdk.base.utils.c.f(context);
            this.f = com.mintegral.msdk.base.utils.c.k();
            int s = com.mintegral.msdk.base.utils.c.s(context);
            this.g = String.valueOf(s);
            this.h = com.mintegral.msdk.base.utils.c.a(context, s);
            this.i = com.mintegral.msdk.base.utils.c.r(context);
            this.j = com.mintegral.msdk.base.controller.a.d().k();
            this.k = com.mintegral.msdk.base.controller.a.d().j();
            this.l = String.valueOf(com.mintegral.msdk.base.utils.k.i(context));
            this.m = String.valueOf(com.mintegral.msdk.base.utils.k.h(context));
            this.o = String.valueOf(com.mintegral.msdk.base.utils.k.c(context));
            if (context.getResources().getConfiguration().orientation == 2) {
                this.n = "landscape";
            } else {
                this.n = "portrait";
            }
        }

        public final JSONObject a() {
            JSONObject jSONObject = new JSONObject();
            try {
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                    jSONObject.put("device", this.a);
                    jSONObject.put("system_version", this.b);
                    jSONObject.put("network_type", this.g);
                    jSONObject.put("network_type_str", this.h);
                    jSONObject.put("device_ua", this.i);
                }
                jSONObject.put("plantform", this.c);
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITYIMEIMAC)) {
                    jSONObject.put("device_imei", this.d);
                }
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_ANDROID_ID)) {
                    jSONObject.put(TapjoyConstants.TJC_ANDROID_ID, this.e);
                }
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
                    jSONObject.put("google_ad_id", this.f);
                }
                jSONObject.put("appkey", this.j);
                jSONObject.put("appId", this.k);
                jSONObject.put("screen_width", this.l);
                jSONObject.put("screen_height", this.m);
                jSONObject.put("orientation", this.n);
                jSONObject.put("scale", this.o);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            return jSONObject;
        }
    }
}
