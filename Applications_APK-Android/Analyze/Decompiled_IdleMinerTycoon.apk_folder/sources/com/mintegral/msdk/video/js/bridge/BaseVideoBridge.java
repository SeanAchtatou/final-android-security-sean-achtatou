package com.mintegral.msdk.video.js.bridge;

import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Base64;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.mtgjscommon.windvane.a;
import com.mintegral.msdk.mtgjscommon.windvane.i;
import com.mintegral.msdk.video.js.a.h;
import com.mintegral.msdk.video.js.b;
import com.mintegral.msdk.video.js.factory.IJSFactory;
import im.getsocial.sdk.consts.LanguageCodes;
import org.json.JSONException;
import org.json.JSONObject;

public class BaseVideoBridge extends i implements IVideoBridge {
    protected IJSFactory a;

    public void initialize(Context context, WindVaneWebView windVaneWebView) {
        super.initialize(context, windVaneWebView);
        if (this.mContext instanceof IJSFactory) {
            this.a = (IJSFactory) this.mContext;
        }
    }

    public void init(Object obj, String str) {
        g.b("JS-Video-Brigde", "init");
        try {
            boolean z = false;
            if (this.a != null) {
                String c = this.a.getJSCommon().c();
                if (!TextUtils.isEmpty(c)) {
                    c = Base64.encodeToString(c.getBytes(), 2);
                }
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, c);
                this.a.getJSCommon().b();
                if (!TextUtils.isEmpty(str)) {
                    JSONObject jSONObject = new JSONObject(str);
                    int optInt = jSONObject.optInt("showTransparent");
                    int optInt2 = jSONObject.optInt("mute");
                    int optInt3 = jSONObject.optInt("closeType");
                    int optInt4 = jSONObject.optInt("orientationType");
                    b jSCommon = this.a.getJSCommon();
                    if (optInt == 1) {
                        z = true;
                    }
                    jSCommon.a(z);
                    this.a.getJSCommon().b(optInt2);
                    this.a.getJSCommon().c(optInt3);
                    this.a.getJSCommon().d(optInt4);
                }
            } else if (obj != null) {
                a aVar = (a) obj;
                if (aVar.a.getObject() instanceof h) {
                    h hVar = (h) aVar.a.getObject();
                    String c2 = hVar.c();
                    if (!TextUtils.isEmpty(str)) {
                        JSONObject jSONObject2 = new JSONObject(str);
                        int optInt5 = jSONObject2.optInt("showTransparent");
                        int optInt6 = jSONObject2.optInt("mute");
                        int optInt7 = jSONObject2.optInt("closeType");
                        int optInt8 = jSONObject2.optInt("orientationType");
                        if (optInt5 == 1) {
                            z = true;
                        }
                        hVar.a(z);
                        hVar.b(optInt6);
                        hVar.c(optInt7);
                        hVar.d(optInt8);
                        g.b("JS-Video-Brigde", "init jsCommon.setIsShowingTransparent = " + optInt5);
                    }
                    String encodeToString = Base64.encodeToString(c2.getBytes(), 2);
                    com.mintegral.msdk.mtgjscommon.windvane.g.a();
                    com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, encodeToString);
                }
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "init error", th);
        }
    }

    public void click(Object obj, String str) {
        int i;
        String str2;
        g.b("JS-Video-Brigde", "install");
        try {
            if (this.a != null && !TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                i = jSONObject.optInt("type");
                str2 = jSONObject.optString(LanguageCodes.PORTUGUESE);
                this.a.getJSCommon().b(i, str2);
            }
        } catch (JSONException e) {
            i = 1;
            e.printStackTrace();
            str2 = "";
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "install error", th);
        }
    }

    public void statistics(Object obj, String str) {
        g.b("JS-Video-Brigde", "statistics,params:" + str);
        try {
            if (this.a != null && !TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                this.a.getJSCommon().a(jSONObject.optInt("type"), jSONObject.optString("data"));
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "statistics error", th);
        }
    }

    public void triggerCloseBtn(Object obj, String str) {
        g.b("JS-Video-Brigde", "triggerCloseBtn");
        try {
            if (this.a != null && !TextUtils.isEmpty(str) && new JSONObject(str).optString("state").equals("click")) {
                this.a.getJSVideoModule().closeVideoOperate(1, -1);
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a(0));
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "triggerCloseBtn error", th);
        }
    }

    public void showVideoLocation(Object obj, String str) {
        boolean z = this.a != null;
        g.b("Test", "showVideoLocation in basevideobridge ff = " + z);
        try {
            if (this.a != null && !TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                int optInt = jSONObject.optInt("margin_top", 0);
                int optInt2 = jSONObject.optInt("margin_left", 0);
                int optInt3 = jSONObject.optInt("view_width", 0);
                int optInt4 = jSONObject.optInt("view_height", 0);
                int optInt5 = jSONObject.optInt("radius", 0);
                int optInt6 = jSONObject.optInt("border_top", 0);
                int optInt7 = jSONObject.optInt("border_left", 0);
                int optInt8 = jSONObject.optInt("border_width", 0);
                int optInt9 = jSONObject.optInt("border_height", 0);
                g.b("JS-Video-Brigde", "showVideoLocation,margin_top:" + optInt + ",marginLeft:" + optInt2 + ",viewWidth:" + optInt3 + ",viewHeight:" + optInt4 + ",radius:" + optInt5 + ",borderTop: " + optInt6 + ",borderLeft: " + optInt7 + ",borderWidth: " + optInt8 + ",borderHeight: " + optInt9);
                this.a.getJSVideoModule().showVideoLocation(optInt, optInt2, optInt3, optInt4, optInt5, optInt6, optInt7, optInt8, optInt9);
                this.a.getJSCommon().f();
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "showVideoLocation error", th);
        }
    }

    public void soundOperate(Object obj, String str) {
        try {
            if (this.a != null && !TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                int optInt = jSONObject.optInt("mute");
                int optInt2 = jSONObject.optInt("view_visible");
                String optString = jSONObject.optString(LanguageCodes.PORTUGUESE, "");
                g.b("JS-Video-Brigde", "soundOperate,mute:" + optInt + ",viewVisible:" + optInt2 + ",pt:" + optString);
                if (TextUtils.isEmpty(optString)) {
                    this.a.getJSVideoModule().soundOperate(optInt, optInt2);
                } else {
                    this.a.getJSVideoModule().soundOperate(optInt, optInt2, optString);
                }
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a(0));
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "soundOperate error", th);
        }
    }

    public void videoOperate(Object obj, String str) {
        try {
            if (this.a != null && !TextUtils.isEmpty(str)) {
                int optInt = new JSONObject(str).optInt("pause_or_resume");
                g.b("JS-Video-Brigde", "videoOperate,pauseOrResume:" + optInt);
                this.a.getJSVideoModule().videoOperate(optInt);
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a(0));
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "videoOperate error", th);
        }
    }

    public void closeVideoOperte(Object obj, String str) {
        try {
            if (this.a != null && !TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                int optInt = jSONObject.optInt("close");
                int optInt2 = jSONObject.optInt("view_visible");
                g.b("JS-Video-Brigde", "closeVideoOperte,close:" + optInt + ",viewVisible:" + optInt2);
                this.a.getJSVideoModule().closeVideoOperate(optInt, optInt2);
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a(0));
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "closeOperte error", th);
        }
    }

    public void progressOperate(Object obj, String str) {
        try {
            if (this.a != null && !TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                int optInt = jSONObject.optInt(NotificationCompat.CATEGORY_PROGRESS);
                int optInt2 = jSONObject.optInt("view_visible");
                g.b("JS-Video-Brigde", "progressOperate,progress:" + optInt + ",viewVisible:" + optInt2);
                this.a.getJSVideoModule().progressOperate(optInt, optInt2);
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a(0));
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "progressOperate error", th);
        }
    }

    public void getCurrentProgress(Object obj, String str) {
        try {
            if (this.a != null) {
                String currentProgress = this.a.getJSVideoModule().getCurrentProgress();
                g.b("JS-Video-Brigde", "getCurrentProgress:" + currentProgress);
                if (!TextUtils.isEmpty(currentProgress)) {
                    currentProgress = Base64.encodeToString(currentProgress.getBytes(), 2);
                }
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, currentProgress);
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "getCurrentProgress error", th);
        }
    }

    public void showVideoClickView(Object obj, String str) {
        try {
            if (this.a != null && !TextUtils.isEmpty(str)) {
                int optInt = new JSONObject(str).optInt("type");
                g.b("JS-Video-Brigde", "showVideoClickView,type:" + optInt);
                this.a.getJSContainerModule().showVideoClickView(optInt);
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "showVideoClickView error", th);
        }
    }

    public void setScaleFitXY(Object obj, String str) {
        try {
            if (this.a != null && !TextUtils.isEmpty(str)) {
                int optInt = new JSONObject(str).optInt("fitxy");
                g.b("JS-Video-Brigde", "setScaleFitXY,type:" + optInt);
                this.a.getJSVideoModule().setScaleFitXY(optInt);
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "showVideoClickView error", th);
        }
    }

    public void notifyCloseBtn(Object obj, String str) {
        try {
            if (this.a != null && !TextUtils.isEmpty(str)) {
                int optInt = new JSONObject(str).optInt("state");
                g.b("JS-Video-Brigde", "notifyCloseBtn,result:" + optInt);
                this.a.getJSVideoModule().notifyCloseBtn(optInt);
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "notifyCloseBtn", th);
        }
    }

    public void toggleCloseBtn(Object obj, String str) {
        try {
            if (this.a != null && !TextUtils.isEmpty(str)) {
                int optInt = new JSONObject(str).optInt("state");
                g.b("JS-Video-Brigde", "toggleCloseBtn,result:" + optInt);
                int i = 2;
                if (optInt != 1) {
                    i = optInt == 2 ? 1 : 0;
                }
                this.a.getJSVideoModule().closeVideoOperate(0, i);
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "toggleCloseBtn", th);
        }
    }

    public void handlerH5Exception(Object obj, String str) {
        try {
            if (this.a != null && !TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                g.b("JS-Video-Brigde", "handlerH5Exception,params:" + str);
                this.a.getJSCommon().c(jSONObject.optInt("code", -999), jSONObject.optString("message", "h5 error"));
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "toggleCloseBtn", th);
        }
    }

    public void isSystemResume(Object obj, String str) {
        try {
            if (this.a != null) {
                g.b("JS-Video-Brigde", "isSystemResume,params:" + str);
                int e = this.a.getActivityProxy().e();
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a(e));
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "toggleCloseBtn", th);
        }
    }

    public void readyStatus(Object obj, String str) {
        try {
            if (this.a != null) {
                g.b("JS-Video-Brigde", "readyStatus,isReady:" + str);
                this.a.getJSContainerModule().readyStatus(new JSONObject(str).optInt("isReady", 1));
            } else if (obj != null) {
                a aVar = (a) obj;
                if (aVar.a.getObject() instanceof h) {
                    int optInt = new JSONObject(str).optInt("isReady", 1);
                    ((h) aVar.a.getObject()).f(optInt);
                    if (aVar.a instanceof WindVaneWebView) {
                        WindVaneWebView windVaneWebView = aVar.a;
                        if (windVaneWebView.getWebViewListener() != null) {
                            windVaneWebView.getWebViewListener().a(optInt);
                        }
                    }
                }
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "readyStatus", th);
        }
    }

    public void playVideoFinishOperate(Object obj, String str) {
        try {
            if (!TextUtils.isEmpty(str) && this.a != null && !TextUtils.isEmpty(str)) {
                int optInt = new JSONObject(str).optInt("type");
                g.b("JS-Video-Brigde", "playVideoFinishOperate,type: " + optInt);
                this.a.getJSCommon().e(optInt);
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "playVideoFinishOperate error", th);
        }
    }

    private static String a(int i) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("code", i);
            String jSONObject2 = jSONObject.toString();
            if (!TextUtils.isEmpty(jSONObject2)) {
                return Base64.encodeToString(jSONObject2.getBytes(), 2);
            }
            return "";
        } catch (Throwable unused) {
            g.d("JS-Video-Brigde", "code to string is error");
            return "";
        }
    }
}
