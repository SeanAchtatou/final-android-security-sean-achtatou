package com.mintegral.msdk.video.js.a;

import com.mintegral.msdk.video.module.MintegralVideoView;

/* compiled from: JSVideoModule */
public final class l extends f {
    private MintegralVideoView a;

    public l(MintegralVideoView mintegralVideoView) {
        this.a = mintegralVideoView;
    }

    public final void showVideoLocation(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        super.showVideoLocation(i, i2, i3, i4, i5, i6, i7, i8, i9);
        if (this.a != null) {
            this.a.showVideoLocation(i, i2, i3, i4, i5, i6, i7, i8, i9);
        }
    }

    public final void soundOperate(int i, int i2) {
        super.soundOperate(i, i2);
        if (this.a != null) {
            this.a.soundOperate(i, i2);
        }
    }

    public final void soundOperate(int i, int i2, String str) {
        super.soundOperate(i, i2, str);
        if (this.a != null) {
            this.a.soundOperate(i, i2, str);
        }
    }

    public final void videoOperate(int i) {
        super.videoOperate(i);
        if (this.a != null) {
            this.a.videoOperate(i);
        }
    }

    public final void closeVideoOperate(int i, int i2) {
        super.closeVideoOperate(i, i2);
        if (this.a != null) {
            this.a.closeVideoOperate(i, i2);
        }
    }

    public final void progressOperate(int i, int i2) {
        super.progressOperate(i, i2);
        if (this.a != null) {
            this.a.progressOperate(i, i2);
        }
    }

    public final String getCurrentProgress() {
        if (this.a != null) {
            return this.a.getCurrentProgress();
        }
        return super.getCurrentProgress();
    }

    public final void setVisible(int i) {
        if (this.a != null) {
            this.a.setVisible(i);
        } else {
            super.setVisible(i);
        }
    }

    public final void setCover(boolean z) {
        if (this.a != null) {
            this.a.setCover(z);
        } else {
            super.setCover(z);
        }
    }

    public final void setScaleFitXY(int i) {
        if (this.a != null) {
            this.a.setScaleFitXY(i);
        } else {
            super.setScaleFitXY(i);
        }
    }

    public final boolean isH5Canvas() {
        if (this.a != null) {
            return this.a.isH5Canvas();
        }
        return super.isH5Canvas();
    }

    public final int getBorderViewWidth() {
        if (this.a != null) {
            return this.a.getBorderViewWidth();
        }
        return super.getBorderViewWidth();
    }

    public final int getBorderViewHeight() {
        if (this.a != null) {
            return this.a.getBorderViewHeight();
        }
        return super.getBorderViewHeight();
    }

    public final int getBorderViewRadius() {
        if (this.a != null) {
            return this.a.getBorderViewRadius();
        }
        return super.getBorderViewRadius();
    }

    public final int getBorderViewTop() {
        if (this.a != null) {
            return this.a.getBorderViewTop();
        }
        return super.getBorderViewTop();
    }

    public final int getBorderViewLeft() {
        if (this.a != null) {
            return this.a.getBorderViewLeft();
        }
        return super.getBorderViewLeft();
    }
}
