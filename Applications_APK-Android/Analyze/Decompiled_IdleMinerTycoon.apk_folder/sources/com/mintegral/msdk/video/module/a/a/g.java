package com.mintegral.msdk.video.module.a.a;

import com.mintegral.msdk.video.module.MintegralClickMiniCardView;
import com.mintegral.msdk.video.module.a.a;

/* compiled from: MiniCardProxyNotifyListener */
public final class g extends i {
    private MintegralClickMiniCardView a;

    public g(MintegralClickMiniCardView mintegralClickMiniCardView, a aVar) {
        super(aVar);
        this.a = mintegralClickMiniCardView;
    }

    public final void a(int i, Object obj) {
        boolean z = false;
        switch (i) {
            case 100:
                if (this.a != null) {
                    this.a.webviewshow();
                    this.a.onSelfConfigurationChanged(this.a.getResources().getConfiguration());
                    break;
                }
                break;
            case 101:
            case 102:
                z = true;
                break;
            case 103:
                i = 107;
                break;
        }
        if (!z) {
            super.a(i, obj);
        }
    }
}
