package com.mintegral.msdk.video.js.a;

import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.video.js.h;

/* compiled from: DefaultJSVideoModule */
public class f implements h {
    public int getBorderViewHeight() {
        return 0;
    }

    public int getBorderViewLeft() {
        return 0;
    }

    public int getBorderViewRadius() {
        return 0;
    }

    public int getBorderViewTop() {
        return 0;
    }

    public int getBorderViewWidth() {
        return 0;
    }

    public boolean isH5Canvas() {
        return false;
    }

    public void showVideoLocation(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        g.a("js", "showVideoLocation:marginTop=" + i + ",marginLeft=" + i2 + ",width=" + i3 + ",height=" + i4 + ",radius=" + i5 + ",borderTop=" + i6 + ",borderTop=" + i6 + ",borderLeft=" + i7 + ",borderWidth=" + i8 + ",borderHeight=" + i9);
    }

    public void soundOperate(int i, int i2) {
        g.a("js", "soundOperate:mute=" + i + ",soundViewVisible=" + i2);
    }

    public void soundOperate(int i, int i2, String str) {
        g.a("js", "soundOperate:mute=" + i + ",soundViewVisible=" + i2 + ",pt=" + str);
    }

    public void videoOperate(int i) {
        g.a("js", "videoOperate:" + i);
    }

    public void closeVideoOperate(int i, int i2) {
        g.a("js", "closeOperte:close=" + i + "closeViewVisible=" + i2);
    }

    public void progressOperate(int i, int i2) {
        g.a("js", "progressOperate:progress=" + i + "progressViewVisible=" + i2);
    }

    public String getCurrentProgress() {
        g.a("js", "getCurrentProgress");
        return "{}";
    }

    public void setScaleFitXY(int i) {
        g.a("js", "setScaleFitXY:" + i);
    }

    public void setVisible(int i) {
        g.a("js", "setVisible:" + i);
    }

    public void setCover(boolean z) {
        g.a("js", "setCover:" + z);
    }

    public void notifyCloseBtn(int i) {
        g.a("js", "notifyCloseBtn:" + i);
    }
}
