package com.mintegral.msdk.video.js.a;

import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.out.Campaign;
import com.mintegral.msdk.out.NativeListener;
import com.mintegral.msdk.video.js.b;
import com.mintegral.msdk.videocommon.e.c;

/* compiled from: DefaultJSCommon */
public class b implements com.mintegral.msdk.video.js.b {
    protected boolean a = false;
    protected boolean b = false;
    protected int c = 0;
    protected int d = 0;
    protected int e = 0;
    protected int f = -1;
    protected String g;
    protected c h;
    protected com.mintegral.msdk.click.a i;
    public b.a j = new a();
    protected int k = 2;

    public void f() {
    }

    public final void a(int i2) {
        this.k = i2;
    }

    public final void c(int i2) {
        this.c = i2;
    }

    public final void b(int i2) {
        this.d = i2;
    }

    public final void d(int i2) {
        this.e = i2;
    }

    public final int h() {
        if (this.c == 0 && this.b) {
            this.c = 1;
        }
        return this.c;
    }

    public final int i() {
        if (this.d == 0 && this.b) {
            this.d = 1;
        }
        return this.d;
    }

    public final int j() {
        if (this.e == 0 && this.b) {
            this.e = 1;
        }
        return this.e;
    }

    public final boolean k() {
        return this.b;
    }

    public final void a(boolean z) {
        g.a("js", "setIsShowingTransparent:" + z);
        this.b = z;
    }

    public final boolean a() {
        return this.a;
    }

    public final void b() {
        this.a = true;
    }

    public final void a(String str) {
        g.a("js", "setUnitId:" + str);
        this.g = str;
    }

    public final void a(b.a aVar) {
        g.a("js", "setTrackingListener:" + aVar);
        this.j = aVar;
    }

    public final void a(c cVar) {
        g.a("js", "setSetting:" + cVar);
        this.h = cVar;
    }

    public final void e() {
        g.a("js", "release");
        if (this.i != null) {
            this.i.a();
            this.i.a((NativeListener.NativeTrackingListener) null);
            this.i.b();
        }
    }

    public void a(int i2, String str) {
        g.a("js", "statistics,type:" + i2 + ",json:" + str);
    }

    public final void e(int i2) {
        this.f = i2;
    }

    public final int g() {
        return this.f;
    }

    public String c() {
        g.a("js", "init");
        return "{}";
    }

    public void b(int i2, String str) {
        g.a("js", "click:type" + i2 + ",pt:" + str);
    }

    public void c(int i2, String str) {
        g.a("js", "handlerH5Exception,code=" + i2 + ",msg:" + str);
    }

    public void d() {
        g.a("js", "finish");
    }

    /* renamed from: com.mintegral.msdk.video.js.a.b$b  reason: collision with other inner class name */
    /* compiled from: DefaultJSCommon */
    public static class C0067b implements b.a {
        private com.mintegral.msdk.video.js.b a;
        private b.a b;

        public C0067b(com.mintegral.msdk.video.js.b bVar, b.a aVar) {
            this.a = bVar;
            this.b = aVar;
        }

        public final boolean onInterceptDefaultLoadingDialog() {
            return this.b != null && this.b.onInterceptDefaultLoadingDialog();
        }

        public final void onShowLoading(Campaign campaign) {
            if (this.b != null) {
                this.b.onShowLoading(campaign);
            }
        }

        public final void onDismissLoading(Campaign campaign) {
            if (this.b != null) {
                this.b.onDismissLoading(campaign);
            }
        }

        public final void onStartRedirection(Campaign campaign, String str) {
            if (this.b != null) {
                this.b.onStartRedirection(campaign, str);
            }
        }

        public final void onFinishRedirection(Campaign campaign, String str) {
            if (this.b != null) {
                this.b.onFinishRedirection(campaign, str);
            }
            if (this.a != null) {
                this.a.d();
            }
        }

        public final void onRedirectionFailed(Campaign campaign, String str) {
            if (this.b != null) {
                this.b.onRedirectionFailed(campaign, str);
            }
            if (this.a != null) {
                this.a.d();
            }
        }

        public final void onDownloadStart(Campaign campaign) {
            if (this.b != null) {
                this.b.onDownloadStart(campaign);
            }
        }

        public final void onDownloadFinish(Campaign campaign) {
            if (this.b != null) {
                this.b.onDownloadFinish(campaign);
            }
        }

        public final void onDownloadProgress(int i) {
            if (this.b != null) {
                this.b.onDownloadProgress(i);
            }
        }

        public final void a() {
            if (this.b != null) {
                this.b.a();
            }
        }

        public final void a(boolean z) {
            if (this.b != null) {
                this.b.a(z);
            }
        }

        public final void a(int i, String str) {
            if (this.b != null) {
                this.b.a(i, str);
            }
        }

        public final void b() {
            if (this.b != null) {
                this.b.b();
            }
        }
    }

    /* compiled from: DefaultJSCommon */
    public static class a implements b.a {
        public boolean onInterceptDefaultLoadingDialog() {
            g.a("js", "onInterceptDefaultLoadingDialog");
            return false;
        }

        public void onShowLoading(Campaign campaign) {
            g.a("js", "onShowLoading,campaign:" + campaign);
        }

        public void onDismissLoading(Campaign campaign) {
            g.a("js", "onDismissLoading,campaign:" + campaign);
        }

        public void onStartRedirection(Campaign campaign, String str) {
            g.a("js", "onStartRedirection,campaign:" + campaign + ",url:" + str);
        }

        public void onFinishRedirection(Campaign campaign, String str) {
            g.a("js", "onFinishRedirection,campaign:" + campaign + ",url:" + str);
        }

        public void onRedirectionFailed(Campaign campaign, String str) {
            g.a("js", "onFinishRedirection,campaign:" + campaign + ",url:" + str);
        }

        public void onDownloadStart(Campaign campaign) {
            g.a("js", "onDownloadStart,campaign:" + campaign);
        }

        public void onDownloadFinish(Campaign campaign) {
            g.a("js", "onDownloadFinish,campaign:" + campaign);
        }

        public void onDownloadProgress(int i) {
            g.a("js", "onDownloadProgress,progress:" + i);
        }

        public void a() {
            g.a("js", "onInitSuccess");
        }

        public void a(boolean z) {
            g.a("js", "onStartInstall");
        }

        public void a(int i, String str) {
            g.a("js", "onH5Error,code:" + i + "，msg:" + str);
        }

        public void b() {
            g.a("js", "videoLocationReady");
        }
    }
}
