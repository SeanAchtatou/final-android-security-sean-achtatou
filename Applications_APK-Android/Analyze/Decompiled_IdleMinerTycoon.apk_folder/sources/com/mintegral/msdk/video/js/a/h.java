package com.mintegral.msdk.video.js.a;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.q;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.d.b;
import com.tapjoy.TapjoyConstants;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: JSCommon */
public final class h extends b {
    private Activity l;
    private CampaignEx m;
    private int n;

    public h(Activity activity, CampaignEx campaignEx) {
        this.l = activity;
        this.m = campaignEx;
    }

    public final int l() {
        return this.n;
    }

    public final void f(int i) {
        this.n = i;
    }

    public final String c() {
        JSONObject jSONObject = new JSONObject();
        a aVar = new a(com.mintegral.msdk.base.controller.a.d().h());
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("playVideoMute", this.k);
            jSONObject.put("sdkSetting", jSONObject2);
            jSONObject.put("device", aVar.a());
            JSONArray jSONArray = new JSONArray();
            jSONArray.put(CampaignEx.campaignToJsonObject(this.m));
            jSONObject.put("campaignList", jSONArray);
            JSONObject jSONObject3 = new JSONObject();
            if (this.h != null) {
                jSONObject3 = this.h.R();
            }
            jSONObject.put("unitSetting", jSONObject3);
            String j = com.mintegral.msdk.base.controller.a.d().j();
            b.a();
            String c = b.c(j);
            if (!TextUtils.isEmpty(c)) {
                jSONObject.put("appSetting", new JSONObject(c));
            }
            this.j.a();
            this.a = true;
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return jSONObject.toString();
    }

    private CampaignEx a(String str, CampaignEx campaignEx) {
        if (TextUtils.isEmpty(str)) {
            return campaignEx;
        }
        if (TextUtils.isEmpty(str) && campaignEx == null) {
            return null;
        }
        if (!str.contains("notice")) {
            try {
                JSONObject campaignToJsonObject = CampaignEx.campaignToJsonObject(campaignEx);
                CampaignEx parseShortCutsCampaign = CampaignEx.parseShortCutsCampaign(campaignToJsonObject);
                if (parseShortCutsCampaign == null) {
                    parseShortCutsCampaign = campaignEx;
                }
                if (!TextUtils.isEmpty(str)) {
                    a(campaignToJsonObject, parseShortCutsCampaign);
                    JSONObject optJSONObject = new JSONObject(str).optJSONObject(com.mintegral.msdk.base.common.a.z);
                    String str2 = "-999";
                    String str3 = "-999";
                    if (optJSONObject != null) {
                        str2 = String.valueOf(k.b(this.l, (float) Integer.valueOf(optJSONObject.getString(com.mintegral.msdk.base.common.a.x)).intValue()));
                        str3 = String.valueOf(k.b(this.l, (float) Integer.valueOf(optJSONObject.getString(com.mintegral.msdk.base.common.a.y)).intValue()));
                    }
                    parseShortCutsCampaign.setClickURL(com.mintegral.msdk.click.b.a(parseShortCutsCampaign.getClickURL(), str2, str3));
                    String noticeUrl = parseShortCutsCampaign.getNoticeUrl();
                    if (optJSONObject != null) {
                        Iterator<String> keys = optJSONObject.keys();
                        StringBuilder sb = new StringBuilder();
                        while (keys.hasNext()) {
                            sb.append(Constants.RequestParameters.AMPERSAND);
                            String next = keys.next();
                            String optString = optJSONObject.optString(next);
                            if (com.mintegral.msdk.base.common.a.x.equals(next) || com.mintegral.msdk.base.common.a.y.equals(next)) {
                                optString = String.valueOf(k.b(this.l, (float) Integer.valueOf(optString).intValue()));
                            }
                            sb.append(next);
                            sb.append(Constants.RequestParameters.EQUAL);
                            sb.append(optString);
                        }
                        parseShortCutsCampaign.setNoticeUrl(noticeUrl + ((Object) sb));
                    }
                }
                return parseShortCutsCampaign;
            } catch (Throwable unused) {
                return campaignEx;
            }
        } else {
            try {
                JSONObject campaignToJsonObject2 = CampaignEx.campaignToJsonObject(campaignEx);
                JSONObject jSONObject = new JSONObject(str);
                Iterator<String> keys2 = jSONObject.keys();
                while (keys2.hasNext()) {
                    String next2 = keys2.next();
                    campaignToJsonObject2.put(next2, jSONObject.getString(next2));
                }
                CampaignEx parseShortCutsCampaign2 = CampaignEx.parseShortCutsCampaign(campaignToJsonObject2);
                a(campaignToJsonObject2, parseShortCutsCampaign2);
                return parseShortCutsCampaign2;
            } catch (JSONException e) {
                e.printStackTrace();
                return campaignEx;
            }
        }
    }

    private static void a(JSONObject jSONObject, CampaignEx campaignEx) {
        try {
            String optString = jSONObject.optString("unitId");
            if (!TextUtils.isEmpty(optString)) {
                campaignEx.setCampaignUnitId(optString);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0046, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        com.mintegral.msdk.base.utils.g.c("js", "INSTALL", r6);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:2:0x0009, B:13:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0054 A[Catch:{ Throwable -> 0x0046, Throwable -> 0x0075 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0055 A[Catch:{ Throwable -> 0x0046, Throwable -> 0x0075 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(int r5, java.lang.String r6) {
        /*
            r4 = this;
            super.b(r5, r6)
            r0 = 1
            switch(r5) {
                case 1: goto L_0x0021;
                case 2: goto L_0x0020;
                case 3: goto L_0x0009;
                default: goto L_0x0007;
            }
        L_0x0007:
            goto L_0x0080
        L_0x0009:
            com.mintegral.msdk.videocommon.e.c r5 = r4.h     // Catch:{ Throwable -> 0x0075 }
            int r5 = r5.m()     // Catch:{ Throwable -> 0x0075 }
            r1 = -1
            if (r5 != r1) goto L_0x001c
            com.mintegral.msdk.video.js.a.b$b r5 = new com.mintegral.msdk.video.js.a.b$b     // Catch:{ Throwable -> 0x0075 }
            com.mintegral.msdk.video.js.b$a r1 = r4.j     // Catch:{ Throwable -> 0x0075 }
            r5.<init>(r4, r1)     // Catch:{ Throwable -> 0x0075 }
            r4.a(r5)     // Catch:{ Throwable -> 0x0075 }
        L_0x001c:
            r4.b(r0, r6)     // Catch:{ Throwable -> 0x0075 }
            goto L_0x0080
        L_0x0020:
            return
        L_0x0021:
            com.mintegral.msdk.base.entity.CampaignEx r5 = r4.m     // Catch:{ Throwable -> 0x0075 }
            if (r5 != 0) goto L_0x0026
            return
        L_0x0026:
            com.mintegral.msdk.base.entity.CampaignEx r5 = r4.m     // Catch:{ Throwable -> 0x0075 }
            com.mintegral.msdk.base.entity.CampaignEx r5 = r4.a(r6, r5)     // Catch:{ Throwable -> 0x0075 }
            java.lang.String r6 = r5.getNoticeUrl()     // Catch:{ Throwable -> 0x0075 }
            r1 = 0
            android.net.Uri r6 = android.net.Uri.parse(r6)     // Catch:{ Throwable -> 0x0046 }
            java.lang.String r2 = com.mintegral.msdk.base.common.a.A     // Catch:{ Throwable -> 0x0046 }
            java.lang.String r6 = r6.getQueryParameter(r2)     // Catch:{ Throwable -> 0x0046 }
            boolean r2 = android.text.TextUtils.isEmpty(r6)     // Catch:{ Throwable -> 0x0046 }
            if (r2 != 0) goto L_0x004e
            int r6 = java.lang.Integer.parseInt(r6)     // Catch:{ Throwable -> 0x0046 }
            goto L_0x004f
        L_0x0046:
            r6 = move-exception
            java.lang.String r2 = "js"
            java.lang.String r3 = "INSTALL"
            com.mintegral.msdk.base.utils.g.c(r2, r3, r6)     // Catch:{ Throwable -> 0x0075 }
        L_0x004e:
            r6 = 0
        L_0x004f:
            com.mintegral.msdk.video.js.b$a r2 = r4.j     // Catch:{ Throwable -> 0x0075 }
            r3 = 2
            if (r6 != r3) goto L_0x0055
            goto L_0x0056
        L_0x0055:
            r0 = 0
        L_0x0056:
            r2.a(r0)     // Catch:{ Throwable -> 0x0075 }
            com.mintegral.msdk.click.a r6 = r4.m()     // Catch:{ Throwable -> 0x0075 }
            com.mintegral.msdk.video.js.b$a r0 = r4.j     // Catch:{ Throwable -> 0x0075 }
            r6.a(r0)     // Catch:{ Throwable -> 0x0075 }
            com.mintegral.msdk.click.a r6 = r4.m()     // Catch:{ Throwable -> 0x0075 }
            r6.b(r5)     // Catch:{ Throwable -> 0x0075 }
            com.mintegral.msdk.base.controller.a r6 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0075 }
            android.content.Context r6 = r6.h()     // Catch:{ Throwable -> 0x0075 }
            com.mintegral.msdk.video.module.b.a.d(r6, r5)     // Catch:{ Throwable -> 0x0075 }
            return
        L_0x0075:
            r5 = move-exception
            java.lang.String r6 = "js"
            java.lang.String r0 = r5.getMessage()
            com.mintegral.msdk.base.utils.g.c(r6, r0, r5)
            return
        L_0x0080:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.video.js.a.h.b(int, java.lang.String):void");
    }

    public final void c(int i, String str) {
        super.c(i, str);
        try {
            this.j.a(i, str);
        } catch (Throwable th) {
            g.c("js", th.getMessage(), th);
        }
    }

    public final void a(int i, String str) {
        super.a(i, str);
        if (i == 2) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                String optString = jSONObject.optString("event", "event");
                String optString2 = jSONObject.optString("template", "-1");
                String optString3 = jSONObject.optString("layout", "-1");
                String optString4 = jSONObject.optString(MIntegralConstans.PROPERTIES_UNIT_ID, this.g);
                int s = c.s(this.l.getApplication());
                w.a(i.a(this.l.getApplication())).a(new q("2000039", optString, optString2, optString3, optString4, this.m.getId(), s, c.a(this.l.getApplication(), s)));
            } catch (Throwable th) {
                g.c("js", th.getMessage(), th);
            }
        }
    }

    public final void f() {
        super.f();
        if (this.j != null) {
            this.j.b();
        }
    }

    public final void d() {
        super.d();
        try {
            this.l.finish();
        } catch (Throwable th) {
            g.c("js", th.getMessage(), th);
        }
    }

    private com.mintegral.msdk.click.a m() {
        if (this.i == null) {
            this.i = new com.mintegral.msdk.click.a(this.l.getApplicationContext(), this.g);
        }
        return this.i;
    }

    /* compiled from: JSCommon */
    private static final class a {
        public String a = c.d();
        public String b = c.i();
        public String c = "android";
        public String d;
        public String e;
        public String f;
        public String g;
        public String h;
        public String i;
        public String j;
        public String k;
        public String l;
        public String m;
        public String n;
        public String o;
        public String p;

        public a(Context context) {
            this.d = c.c(context);
            this.e = c.f(context);
            this.f = c.k();
            int s = c.s(context);
            this.g = String.valueOf(s);
            this.h = c.a(context, s);
            this.i = c.r(context);
            this.j = com.mintegral.msdk.base.controller.a.d().k();
            this.k = com.mintegral.msdk.base.controller.a.d().j();
            this.l = String.valueOf(k.i(context));
            this.m = String.valueOf(k.h(context));
            this.o = String.valueOf(k.c(context));
            if (context.getResources().getConfiguration().orientation == 2) {
                this.n = "landscape";
            } else {
                this.n = "portrait";
            }
            this.p = c.b(context);
        }

        public final JSONObject a() {
            JSONObject jSONObject = new JSONObject();
            try {
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                    jSONObject.put("device", this.a);
                    jSONObject.put("system_version", this.b);
                    jSONObject.put("network_type", this.g);
                    jSONObject.put("network_type_str", this.h);
                    jSONObject.put("device_ua", this.i);
                }
                jSONObject.put("plantform", this.c);
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITYIMEIMAC)) {
                    jSONObject.put("device_imei", this.d);
                }
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_ANDROID_ID)) {
                    jSONObject.put(TapjoyConstants.TJC_ANDROID_ID, this.e);
                }
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
                    jSONObject.put("google_ad_id", this.f);
                    jSONObject.put("oaid", this.p);
                }
                jSONObject.put("appkey", this.j);
                jSONObject.put("appId", this.k);
                jSONObject.put("screen_width", this.l);
                jSONObject.put("screen_height", this.m);
                jSONObject.put("orientation", this.n);
                jSONObject.put("scale", this.o);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            return jSONObject;
        }
    }
}
