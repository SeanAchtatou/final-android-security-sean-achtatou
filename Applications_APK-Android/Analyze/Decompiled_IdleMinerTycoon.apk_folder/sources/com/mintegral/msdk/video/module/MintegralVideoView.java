package com.mintegral.msdk.video.module;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.facebook.appevents.AppEventsConstants;
import com.iab.omid.library.mintegral.adsession.AdSession;
import com.iab.omid.library.mintegral.adsession.video.InteractionType;
import com.iab.omid.library.mintegral.adsession.video.VideoEvents;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.LocationConst;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.playercommon.PlayerView;
import com.mintegral.msdk.video.js.h;
import com.mintegral.msdk.video.widget.SoundImageView;
import com.mintegral.msdk.videocommon.dialog.MTGAlertDialog;
import org.json.JSONObject;

public class MintegralVideoView extends MintegralBaseView implements h {
    /* access modifiers changed from: private */
    public static boolean M = false;
    private static int i;
    private static int j;
    private static int k;
    private static int l;
    private static int m;
    private double A;
    private boolean B = false;
    private boolean C = false;
    private boolean D = false;
    private boolean E = false;
    private boolean F = false;
    private boolean G = false;
    private boolean H = false;
    /* access modifiers changed from: private */
    public boolean I = false;
    private boolean J = false;
    private int K;
    private boolean L = false;
    private int N = 2;
    private AdSession O;
    /* access modifiers changed from: private */
    public VideoEvents P;
    private b Q = new b(this);
    private boolean R = false;
    /* access modifiers changed from: private */
    public PlayerView n;
    private SoundImageView o;
    /* access modifiers changed from: private */
    public TextView p;
    private View q;
    /* access modifiers changed from: private */
    public boolean r = false;
    private String s;
    private int t;
    private int u;
    private int v;
    private MTGAlertDialog w;
    private com.mintegral.msdk.videocommon.dialog.a x;
    private String y = "";
    private double z;

    public boolean isShowingAlertView() {
        return this.r;
    }

    public void setUnitId(String str) {
        this.y = str;
    }

    public boolean isMiniCardShowing() {
        return this.E;
    }

    public boolean isShowingTransparent() {
        return this.J;
    }

    public void setShowingTransparent(boolean z2) {
        this.J = z2;
    }

    public MintegralVideoView(Context context) {
        super(context);
    }

    public MintegralVideoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void init(Context context) {
        int findLayout = findLayout("mintegral_reward_videoview_item");
        if (findLayout > 0) {
            this.c.inflate(findLayout, this);
            this.f = e();
            if (!this.f) {
                g.d(MintegralBaseView.TAG, "MintegralVideoView init fail");
            }
            a();
        }
        M = false;
    }

    public void setIsIV(boolean z2) {
        this.L = z2;
    }

    public void setSoundState(int i2) {
        this.N = i2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.f) {
            this.n.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    MintegralVideoView.this.e.a(1, "");
                    if (MintegralVideoView.this.P != null) {
                        MintegralVideoView.this.P.adUserInteraction(InteractionType.CLICK);
                    }
                }
            });
            this.o.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    int i = 2;
                    if (MintegralVideoView.this.n.isSilent()) {
                        i = 1;
                    }
                    MintegralVideoView.this.e.a(5, i);
                }
            });
            this.q.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    MintegralVideoView.this.d();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.t < 0 || this.v != 1 || this.J) {
            this.e.a(2, "");
            return;
        }
        f();
        if (this.x == null) {
            this.x = new com.mintegral.msdk.videocommon.dialog.a() {
                public final void a() {
                    boolean unused = MintegralVideoView.this.r = false;
                    MintegralVideoView.this.setShowingAlertViewCover(MintegralVideoView.this.r);
                    MintegralVideoView.this.g();
                }

                public final void b() {
                    boolean unused = MintegralVideoView.this.r = false;
                    MintegralVideoView.this.setShowingAlertViewCover(MintegralVideoView.this.r);
                    MintegralVideoView.this.e.a(2, "");
                }
            };
        }
        if (this.w == null) {
            this.w = new MTGAlertDialog(getContext(), this.x);
            if (this.O != null) {
                this.O.addFriendlyObstruction(this.w.getWindow().getDecorView());
            }
        }
        this.w.makeRVAlertView(this.y);
        if (this.n != null && !this.n.isComplete()) {
            this.w.show();
            this.r = true;
            setShowingAlertViewCover(this.r);
        }
    }

    public void preLoadData() {
        if (this.f && !TextUtils.isEmpty(this.s) && this.b != null) {
            if (this.O != null) {
                this.O.registerAdView(this.n);
                this.O.addFriendlyObstruction(this.o);
                this.O.addFriendlyObstruction(this.p);
                this.O.addFriendlyObstruction(this.q);
                g.a("OMSDK", "RV registerView");
            }
            if (this.b != null && s.b(this.b.getVideoResolution())) {
                String videoResolution = this.b.getVideoResolution();
                g.b(MintegralBaseView.TAG, "MintegralBaseView videoResolution:" + videoResolution);
                String[] split = videoResolution.split("x");
                if (split.length == 2) {
                    if (k.c(split[0]) > 0.0d) {
                        this.z = k.c(split[0]);
                    }
                    if (k.c(split[1]) > 0.0d) {
                        this.A = k.c(split[1]);
                    }
                    g.b(MintegralBaseView.TAG, "MintegralBaseView mVideoW:" + this.z + "  mVideoH:" + this.A);
                }
                if (this.z <= 0.0d) {
                    this.z = 1280.0d;
                }
                if (this.A <= 0.0d) {
                    this.A = 720.0d;
                }
            }
            this.n.initBufferIngParam(this.u);
            this.n.initVFPData(this.s, this.b.getVideoUrlEncode(), this.Q);
            soundOperate(this.N, -1, null);
        }
        M = false;
    }

    public void defaultShow() {
        super.defaultShow();
        this.B = true;
        showVideoLocation(0, 0, k.i(this.a), k.h(this.a), 0, 0, 0, 0, 0);
        videoOperate(1);
        if (this.t == 0) {
            closeVideoOperate(-1, 2);
        }
    }

    public void showVideoLocation(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10) {
        g.b(MintegralBaseView.TAG, "showVideoLocation marginTop:" + i2 + " marginLeft:" + i3 + " width:" + i4 + "  height:" + i5 + " radius:" + i6 + " borderTop:" + i7 + " borderLeft:" + i8 + " borderWidth:" + i9 + " borderHeight:" + i10);
        if (this.f) {
            setVisibility(0);
            if (!(i4 > 0 && i5 > 0 && k.i(this.a) >= i4 && k.h(this.a) >= i5) || this.B) {
                h();
                return;
            }
            j = i7;
            k = i8;
            l = i9 + 4;
            m = i10 + 4;
            float f = ((float) i4) / ((float) i5);
            float f2 = 0.0f;
            try {
                f2 = (float) (this.z / this.A);
            } catch (Throwable th) {
                g.c(MintegralBaseView.TAG, th.getMessage(), th);
            }
            if (i6 > 0) {
                i = i6;
                if (i6 > 0) {
                    GradientDrawable gradientDrawable = new GradientDrawable();
                    gradientDrawable.setCornerRadius((float) k.b(getContext(), (float) i6));
                    gradientDrawable.setColor(-1);
                    gradientDrawable.setStroke(1, 0);
                    if (Build.VERSION.SDK_INT >= 16) {
                        setBackground(gradientDrawable);
                        this.n.setBackground(gradientDrawable);
                    } else {
                        setBackgroundDrawable(gradientDrawable);
                        this.n.setBackgroundDrawable(gradientDrawable);
                    }
                    if (Build.VERSION.SDK_INT >= 21) {
                        setClipToOutline(true);
                        this.n.setClipToOutline(true);
                    }
                }
            }
            if (Math.abs(f - f2) <= 0.1f || this.K == 1) {
                g.b(MintegralBaseView.TAG, "showVideoLocation USE H5 SIZE.");
                h();
                if (this.J) {
                    setLayoutCenter(i4, i5);
                    if (M) {
                        this.e.a(114, "");
                    } else {
                        this.e.a(116, "");
                    }
                } else {
                    setLayoutParam(i3, i2, i4, i5);
                }
            } else {
                h();
                videoOperate(1);
            }
        }
    }

    public void soundOperate(int i2, int i3) {
        soundOperate(i2, i3, "2");
    }

    public void soundOperate(int i2, int i3, String str) {
        if (this.f) {
            this.N = i2;
            if (i2 == 1) {
                this.o.setSoundStatus(false);
                this.n.closeSound();
                try {
                    if (this.P != null) {
                        this.P.volumeChange(0.0f);
                    }
                } catch (IllegalArgumentException e) {
                    g.a("OMSDK", e.getMessage());
                }
            } else if (i2 == 2) {
                this.o.setSoundStatus(true);
                this.n.openSound();
                try {
                    if (this.P != null) {
                        this.P.volumeChange(com.mintegral.msdk.b.b.b(this.a));
                    }
                } catch (IllegalArgumentException e2) {
                    g.a("OMSDK", e2.getMessage());
                }
            }
            if (i3 == 1) {
                this.o.setVisibility(8);
            } else if (i3 == 2) {
                this.o.setVisibility(0);
            }
        }
        if (str != null && str.equals("2")) {
            this.e.a(7, Integer.valueOf(i2));
        }
    }

    public void videoOperate(int i2) {
        g.a(MintegralBaseView.TAG, "VideoView videoOperate:" + i2);
        if (!this.f) {
            return;
        }
        if (i2 == 1) {
            if (getVisibility() == 0 && isfront()) {
                g.a(MintegralBaseView.TAG, "VideoView videoOperate:play");
                if (!this.r) {
                    g();
                }
            }
        } else if (i2 == 2) {
            if (getVisibility() == 0 && isfront()) {
                g.a(MintegralBaseView.TAG, "VideoView videoOperate:pause");
                f();
            }
        } else if (i2 == 3 && !this.D) {
            this.n.release();
            this.D = true;
        }
    }

    public void closeVideoOperate(int i2, int i3) {
        if (i2 == 1) {
            d();
        }
        if (i3 == 1) {
            if (this.f && this.q.getVisibility() != 8) {
                this.q.setVisibility(8);
                this.F = false;
            }
            if (!this.R && !this.I && !this.G) {
                this.R = true;
                if (this.t < 0) {
                    return;
                }
                if (this.t == 0) {
                    this.I = true;
                } else {
                    new Handler().postDelayed(new Runnable() {
                        public final void run() {
                            boolean unused = MintegralVideoView.this.I = true;
                        }
                    }, (long) (this.t * 1000));
                }
            }
        } else if (i3 == 2 && this.f && this.q.getVisibility() != 0) {
            this.q.setVisibility(0);
            this.F = true;
        }
    }

    public void progressOperate(int i2, int i3) {
        if (this.f) {
            g.b(MintegralBaseView.TAG, "progressOperate progress:" + i2);
            int videoLength = this.b != null ? this.b.getVideoLength() : 0;
            if (i2 > 0 && i2 <= videoLength && this.n != null) {
                g.b(MintegralBaseView.TAG, "progressOperate progress:" + i2);
                this.n.seekTo(i2 * 1000);
            }
            if (i3 == 1) {
                this.p.setVisibility(8);
            } else if (i3 == 2) {
                this.p.setVisibility(0);
            }
        }
    }

    public String getCurrentProgress() {
        try {
            int a2 = this.Q.a();
            int i2 = 0;
            if (this.b != null) {
                i2 = this.b.getVideoLength();
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(NotificationCompat.CATEGORY_PROGRESS, a(a2, i2));
            jSONObject.put(LocationConst.TIME, a2);
            jSONObject.put(IronSourceConstants.EVENTS_DURATION, String.valueOf(i2));
            return jSONObject.toString();
        } catch (Throwable th) {
            g.c(MintegralBaseView.TAG, th.getMessage(), th);
            return "{}";
        }
    }

    public void setScaleFitXY(int i2) {
        this.K = i2;
    }

    public void setVisible(int i2) {
        setVisibility(i2);
    }

    public void setCover(boolean z2) {
        if (this.f) {
            this.E = z2;
            this.n.setIsCovered(z2);
        }
    }

    public void setShowingAlertViewCover(boolean z2) {
        this.n.setIsCovered(z2);
    }

    public boolean isH5Canvas() {
        return getLayoutParams().height < k.h(this.a.getApplicationContext());
    }

    public int getBorderViewHeight() {
        return m;
    }

    public int getBorderViewWidth() {
        return l;
    }

    public int getBorderViewLeft() {
        return k;
    }

    public int getBorderViewTop() {
        return j;
    }

    public int getBorderViewRadius() {
        return i;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.f && this.B) {
            h();
        }
    }

    private static String a(int i2, int i3) {
        if (i3 != 0) {
            double d = (double) (((float) i2) / ((float) i3));
            try {
                StringBuilder sb = new StringBuilder();
                sb.append(k.a(Double.valueOf(d)));
                return sb.toString();
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return String.valueOf(i3);
    }

    public int getCloseAlert() {
        return this.v;
    }

    public void setCloseAlert(int i2) {
        this.v = i2;
    }

    public int getVideoSkipTime() {
        return this.t;
    }

    public void setVideoSkipTime(int i2) {
        this.t = i2;
    }

    public void setPlayURL(String str) {
        this.s = str;
    }

    public void setBufferTimeout(int i2) {
        this.u = i2;
    }

    private boolean e() {
        try {
            this.n = (PlayerView) findViewById(findID("mintegral_vfpv"));
            this.o = (SoundImageView) findViewById(findID("mintegral_sound_switch"));
            this.p = (TextView) findViewById(findID("mintegral_tv_sound"));
            this.q = findViewById(findID("mintegral_rl_playing_close"));
            return isNotNULL(this.n, this.o, this.p, this.q);
        } catch (Throwable th) {
            g.c(MintegralBaseView.TAG, th.getMessage(), th);
            return false;
        }
    }

    public boolean isfront() {
        ViewGroup viewGroup = (ViewGroup) getParent();
        if (viewGroup == null) {
            return false;
        }
        int indexOfChild = viewGroup.indexOfChild(this);
        int childCount = viewGroup.getChildCount();
        int i2 = indexOfChild + 1;
        boolean z2 = false;
        while (i2 <= childCount - 1) {
            if (viewGroup.getChildAt(i2).getVisibility() == 0 && this.E) {
                return false;
            }
            i2++;
            z2 = true;
        }
        return z2;
    }

    private void f() {
        try {
            if (this.n != null) {
                this.n.onPause();
                if (this.P != null) {
                    this.P.pause();
                    g.a("omsdk", "play:  videoEvents.pause()");
                }
            }
        } catch (Throwable th) {
            g.c(MintegralBaseView.TAG, th.getMessage(), th);
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        try {
            if (!this.C) {
                try {
                    if (this.P != null) {
                        float b2 = this.N != 1 ? com.mintegral.msdk.b.b.b(this.a) : 0.0f;
                        if (b2 > 1.0f) {
                            b2 = 1.0f;
                        }
                        float duration = (float) this.n.getDuration();
                        if (duration == 0.0f && this.b != null) {
                            duration = (float) this.b.getVideoLength();
                        }
                        g.a("omsdk", "play: duration = " + duration + " volume = " + b2);
                        this.P.start(duration, b2);
                        g.a("omsdk", "play: videoEvents.start()");
                    }
                } catch (IllegalArgumentException e) {
                    g.a(MintegralBaseView.TAG, e.getMessage());
                }
                this.n.playVideo();
                this.C = true;
                return;
            }
            this.n.onResume();
            try {
                if (this.P != null) {
                    this.P.resume();
                    g.a("omsdk", "play:  videoEvents.resume()");
                }
            } catch (IllegalArgumentException e2) {
                g.a(MintegralBaseView.TAG, e2.getMessage());
            }
        } catch (Exception e3) {
            g.c(MintegralBaseView.TAG, e3.getMessage(), e3);
        }
    }

    private void h() {
        float i2 = (float) k.i(this.a);
        float h = (float) k.h(this.a);
        if (this.z <= 0.0d || this.A <= 0.0d || i2 <= 0.0f || h <= 0.0f) {
            try {
                setLayoutParam(0, 0, -1, -1);
                if (!isLandscape() && this.f) {
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.n.getLayoutParams();
                    int i3 = k.i(this.a);
                    layoutParams.width = -1;
                    layoutParams.height = (i3 * 9) / 16;
                    layoutParams.addRule(13);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        } else {
            double d = this.z / this.A;
            double d2 = (double) (i2 / h);
            g.b(MintegralBaseView.TAG, "videoWHDivide:" + d + "  screenWHDivide:" + d2);
            double a2 = k.a(Double.valueOf(d));
            double a3 = k.a(Double.valueOf(d2));
            g.b(MintegralBaseView.TAG, "videoWHDivideFinal:" + a2 + "  screenWHDivideFinal:" + a3);
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.n.getLayoutParams();
            if (a2 > a3) {
                double d3 = (double) i2;
                double d4 = this.A;
                Double.isNaN(d3);
                layoutParams2.width = -1;
                layoutParams2.height = (int) ((d3 * d4) / this.z);
                layoutParams2.addRule(13, -1);
            } else if (a2 < a3) {
                double d5 = (double) h;
                Double.isNaN(d5);
                layoutParams2.width = (int) (d5 * d);
                layoutParams2.height = -1;
                layoutParams2.addRule(13, -1);
            } else {
                layoutParams2.width = -1;
                layoutParams2.height = -1;
            }
            this.n.setLayoutParams(layoutParams2);
            setMatchParent();
        }
    }

    private static final class b extends com.mintegral.msdk.playercommon.a {
        private MintegralVideoView a;
        private int b;
        private int c;
        private boolean d;
        /* access modifiers changed from: private */
        public VideoEvents e;
        private a f = new a();
        private boolean g = false;
        private boolean h = false;
        private boolean i = false;

        public final int a() {
            return this.b;
        }

        public b(MintegralVideoView mintegralVideoView) {
            this.a = mintegralVideoView;
        }

        public final void onPlayStarted(int i2) {
            super.onPlayStarted(i2);
            if (!this.d) {
                this.a.e.a(10, this.f);
                this.d = true;
            }
            boolean unused = MintegralVideoView.M = false;
        }

        public final void onPlayCompleted() {
            super.onPlayCompleted();
            if (this.e != null) {
                this.e.complete();
                g.a("omsdk", "play:  videoEvents.complete()");
            }
            this.a.p.setText(String.valueOf(AppEventsConstants.EVENT_PARAM_VALUE_NO));
            this.a.n.setClickable(false);
            this.a.e.a(121, "");
            this.a.e.a(11, "");
            this.b = this.c;
            boolean unused = MintegralVideoView.M = true;
        }

        public final void onPlayError(String str) {
            super.onPlayError(str);
            this.a.e.a(12, "");
        }

        public final void onPlayProgress(int i2, int i3) {
            super.onPlayProgress(i2, i3);
            if (this.a.f) {
                int i4 = i3 - i2;
                if (i4 <= 0) {
                    i4 = 0;
                }
                this.a.p.setText(String.valueOf(i4));
            }
            this.c = i3;
            this.f.a = i2;
            this.f.b = i3;
            this.b = i2;
            this.a.e.a(15, this.f);
            if (this.e != null) {
                int i5 = (i2 * 100) / i3;
                int i6 = ((i2 + 1) * 100) / i3;
                if (i5 <= 25 && 25 < i6 && !this.g) {
                    this.g = true;
                    this.e.firstQuartile();
                    g.a("omsdk", "play:  videoEvents.firstQuartile()");
                } else if (i5 <= 50 && 50 < i6 && !this.h) {
                    this.h = true;
                    this.e.midpoint();
                    g.a("omsdk", "play:  videoEvents.midpoint()");
                } else if (i5 <= 75 && 75 < i6 && !this.i) {
                    this.i = true;
                    this.e.thirdQuartile();
                    g.a("omsdk", "play:  videoEvents.thirdQuartile()");
                }
            }
        }

        public final void OnBufferingStart(String str) {
            try {
                super.OnBufferingStart(str);
                if (this.e != null) {
                    this.e.bufferStart();
                }
                this.a.e.a(13, "");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        public final void OnBufferingEnd() {
            try {
                super.OnBufferingEnd();
                if (this.e != null) {
                    this.e.bufferFinish();
                    g.a("omsdk", "play:  videoEvents.bufferFinish()");
                }
                this.a.e.a(14, "");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        public final void onPlaySetDataSourceError(String str) {
            super.onPlaySetDataSourceError(str);
        }
    }

    public static class a {
        public int a;
        public int b;

        public final String toString() {
            return "ProgressData{curPlayPosition=" + this.a + ", allDuration=" + this.b + '}';
        }
    }

    public void onBackPress() {
        if (!this.E && !this.r) {
            if (this.F) {
                d();
            } else if (this.G && this.H) {
                d();
            } else if (!this.G && this.I) {
                d();
            }
        }
    }

    public void notifyCloseBtn(int i2) {
        if (i2 == 0) {
            this.G = true;
            this.I = false;
        } else if (i2 == 1) {
            this.H = true;
        }
    }

    public void notifyVideoClose() {
        this.e.a(2, "");
    }

    public void setAdSession(AdSession adSession) {
        this.O = adSession;
    }

    public void setVideoEvents(VideoEvents videoEvents) {
        this.P = videoEvents;
        if (this.Q != null) {
            VideoEvents unused = this.Q.e = videoEvents;
        }
    }

    public int getMute() {
        return this.N;
    }
}
