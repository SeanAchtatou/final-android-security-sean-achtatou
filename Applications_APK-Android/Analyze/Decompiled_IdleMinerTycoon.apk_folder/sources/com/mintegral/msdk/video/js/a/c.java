package com.mintegral.msdk.video.js.a;

import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.video.js.d;
import com.mintegral.msdk.video.js.f;

/* compiled from: DefaultJSContainerModule */
public class c implements d, f {
    public void configurationChanged(int i, int i2, int i3) {
    }

    public void showVideoClickView(int i) {
        g.a("js", "showVideoClickView:" + i);
    }

    public void showEndcard(int i) {
        g.a("js", "showEndcard,type=" + i);
    }

    public boolean endCardShowing() {
        g.a("js", "endCardShowing");
        return true;
    }

    public boolean miniCardShowing() {
        g.a("js", "miniCardShowing");
        return false;
    }

    public void notifyCloseBtn(int i) {
        g.a("js", "notifyCloseBtn:state = " + i);
    }

    public void toggleCloseBtn(int i) {
        g.a("js", "toggleCloseBtn:state=" + i);
    }

    public void readyStatus(int i) {
        g.a("js", "readyStatus:isReady=" + i);
    }

    public void showMiniCard(int i, int i2, int i3, int i4, int i5) {
        g.a("js", "showMiniCard top = " + i + " left = " + i2 + " width = " + i3 + " height = " + i4 + " radius = " + i5);
    }

    public void resizeMiniCard(int i, int i2, int i3) {
        g.a("js", "showMiniCard width = " + i + " height = " + i2 + " radius = " + i3);
    }
}
