package com.mintegral.msdk.video.module;

import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mintegral.msdk.base.common.c.b;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.video.module.a.a.e;
import com.mintegral.msdk.video.module.a.a.j;
import com.mintegral.msdk.videocommon.view.StarLevelView;
import org.json.JSONException;
import org.json.JSONObject;

public class MintegralNativeEndCardView extends MintegralBaseView {
    private ViewGroup i;
    private ViewGroup j;
    private ImageView k;
    private ImageView l;
    private TextView m;
    private TextView n;
    private StarLevelView o;
    private View p;
    private View q;
    private String r;

    public void setUnitId(String str) {
        this.r = str;
    }

    public MintegralNativeEndCardView(Context context) {
        super(context);
    }

    public MintegralNativeEndCardView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void preLoadData() {
        if (this.b != null && this.f) {
            b.a(this.a.getApplicationContext()).a(this.b.getImageUrl(), new e(this.k, this.b, this.r));
            b.a(this.a.getApplicationContext()).a(this.b.getIconUrl(), new j(this.l, k.b(a.d().h(), 8.0f)));
            this.m.setText(this.b.getAppName());
            this.n.setText(this.b.getAppDesc());
            this.o.removeAllViews();
            double rating = this.b.getRating();
            if (rating <= 0.0d) {
                rating = 5.0d;
            }
            this.o.initScore(rating);
        }
    }

    public void onSelfConfigurationChanged(Configuration configuration) {
        super.onSelfConfigurationChanged(configuration);
        this.d = configuration.orientation;
        g.d(MintegralBaseView.TAG, " native onSelfConfigurationChanged:" + this.d);
        if (this.d == 2) {
            removeView(this.i);
            a(this.j);
            return;
        }
        removeView(this.j);
        a(this.i);
    }

    public boolean canBackPress() {
        return this.p != null && this.p.getVisibility() == 0;
    }

    public void notifyShowListener() {
        this.e.a(110, "");
    }

    private void a(View view) {
        if (view == null) {
            init(this.a);
            preLoadData();
            return;
        }
        if (view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
        addView(view);
        b(view);
        a();
        d();
    }

    private void d() {
        float f;
        if (this.f) {
            float f2 = k.f(this.a);
            if (isLandscape()) {
                f2 *= 0.6f;
                f = (627.0f * f2) / 1200.0f;
                int b = k.b(this.a.getApplicationContext(), 20.0f);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.j.findViewById(findID("mintegral_view_shadow")).getLayoutParams();
                if (layoutParams == null) {
                    layoutParams = new RelativeLayout.LayoutParams(b, -1);
                }
                layoutParams.leftMargin = (int) (f2 - ((float) b));
            } else {
                f = (627.0f * f2) / 1200.0f;
            }
            ViewGroup.LayoutParams layoutParams2 = this.k.getLayoutParams();
            if (layoutParams2 == null) {
                layoutParams2 = new ViewGroup.LayoutParams(-2, -2);
            }
            layoutParams2.width = (int) f2;
            layoutParams2.height = (int) f;
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.f) {
            this.p.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    MintegralNativeEndCardView.this.e.a(104, "");
                }
            });
            this.q.setOnClickListener(new com.mintegral.msdk.widget.a() {
                /* access modifiers changed from: protected */
                public final void a() {
                    JSONObject jSONObject;
                    JSONException e;
                    try {
                        jSONObject = new JSONObject();
                        try {
                            jSONObject.put(com.mintegral.msdk.base.common.a.z, MintegralNativeEndCardView.this.c());
                        } catch (JSONException e2) {
                            e = e2;
                        }
                    } catch (JSONException e3) {
                        JSONException jSONException = e3;
                        jSONObject = null;
                        e = jSONException;
                        e.printStackTrace();
                        MintegralNativeEndCardView.this.e.a(105, jSONObject);
                    }
                    MintegralNativeEndCardView.this.e.a(105, jSONObject);
                }
            });
            this.l.setOnClickListener(new com.mintegral.msdk.widget.a() {
                /* access modifiers changed from: protected */
                public final void a() {
                    JSONObject jSONObject;
                    JSONException e;
                    try {
                        jSONObject = new JSONObject();
                        try {
                            jSONObject.put(com.mintegral.msdk.base.common.a.z, MintegralNativeEndCardView.this.c());
                        } catch (JSONException e2) {
                            e = e2;
                        }
                    } catch (JSONException e3) {
                        JSONException jSONException = e3;
                        jSONObject = null;
                        e = jSONException;
                        e.printStackTrace();
                        MintegralNativeEndCardView.this.e.a(105, jSONObject);
                    }
                    MintegralNativeEndCardView.this.e.a(105, jSONObject);
                }
            });
            this.k.setOnClickListener(new com.mintegral.msdk.widget.a() {
                /* access modifiers changed from: protected */
                public final void a() {
                    JSONObject jSONObject;
                    JSONException e;
                    try {
                        jSONObject = new JSONObject();
                        try {
                            jSONObject.put(com.mintegral.msdk.base.common.a.z, MintegralNativeEndCardView.this.c());
                        } catch (JSONException e2) {
                            e = e2;
                        }
                    } catch (JSONException e3) {
                        JSONException jSONException = e3;
                        jSONObject = null;
                        e = jSONException;
                        e.printStackTrace();
                        MintegralNativeEndCardView.this.e.a(105, jSONObject);
                    }
                    MintegralNativeEndCardView.this.e.a(105, jSONObject);
                }
            });
        }
    }

    private boolean b(View view) {
        try {
            this.k = (ImageView) view.findViewById(findID("mintegral_iv_adbanner"));
            this.l = (ImageView) view.findViewById(findID("mintegral_iv_icon"));
            this.m = (TextView) view.findViewById(findID("mintegral_tv_apptitle"));
            this.n = (TextView) view.findViewById(findID("mintegral_tv_appdesc"));
            this.o = (StarLevelView) view.findViewById(findID("mintegral_sv_starlevel"));
            this.p = view.findViewById(findID("mintegral_iv_close"));
            this.q = view.findViewById(findID("mintegral_tv_cta"));
            return isNotNULL(this.k, this.l, this.m, this.n, this.o, this.p, this.q);
        } catch (Throwable th) {
            g.c(MintegralBaseView.TAG, th.getMessage(), th);
            return false;
        }
    }

    public void init(Context context) {
        boolean z;
        String str = "mintegral_reward_endcard_native_hor";
        if (isLandscape()) {
            str = "mintegral_reward_endcard_native_land";
        }
        int findLayout = findLayout(str);
        if (findLayout > 0) {
            if (isLandscape()) {
                this.j = (ViewGroup) this.c.inflate(findLayout, (ViewGroup) null);
                addView(this.j);
                z = b(this.j);
            } else {
                this.i = (ViewGroup) this.c.inflate(findLayout, (ViewGroup) null);
                addView(this.i);
                z = b(this.i);
            }
            this.f = z;
            a();
            d();
        }
    }
}
