package com.mintegral.msdk.video.module.a.a;

import android.text.TextUtils;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.l;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.b.v;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.f;
import com.mintegral.msdk.base.entity.q;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.reward.player.MTGRewardVideoActivity;
import com.mintegral.msdk.videocommon.b.d;
import com.mintegral.msdk.videocommon.download.a;
import java.io.File;

/* compiled from: StatisticsOnNotifyListener */
public class k extends f {
    protected boolean a;
    protected CampaignEx b;
    protected a c;
    protected d d;
    protected String e;
    protected com.mintegral.msdk.video.module.a.a f = new f();
    private boolean g = false;
    private boolean h = false;

    public k(CampaignEx campaignEx, a aVar, d dVar, String str, com.mintegral.msdk.video.module.a.a aVar2) {
        if (campaignEx != null && s.b(str) && aVar != null && aVar2 != null) {
            this.b = campaignEx;
            this.e = str;
            this.c = aVar;
            this.d = dVar;
            this.f = aVar2;
            this.a = true;
        }
    }

    public void a(int i, Object obj) {
        super.a(i, obj);
        this.f.a(i, obj);
    }

    public final void a(int i) {
        if (this.b != null) {
            switch (i) {
                case 1:
                case 2:
                    com.mintegral.msdk.video.module.b.a.a(com.mintegral.msdk.base.controller.a.d().h(), this.b, i);
                    return;
                default:
                    return;
            }
        }
    }

    public final void a() {
        if (this.a && this.b != null) {
            q qVar = new q("2000061", this.b.getId(), this.b.getRequestId(), this.e, c.s(com.mintegral.msdk.base.controller.a.d().h()));
            qVar.a(this.b.isMraid() ? q.a : q.b);
            com.mintegral.msdk.base.common.e.a.d(qVar, com.mintegral.msdk.base.controller.a.d().h(), this.e);
        }
    }

    public final void a(int i, String str) {
        if (this.b != null) {
            com.mintegral.msdk.base.common.e.a.e(new q("2000062", this.b.getId(), this.b.getRequestId(), this.e, c.s(com.mintegral.msdk.base.controller.a.d().h()), i, str), com.mintegral.msdk.base.controller.a.d().h(), this.e);
        }
    }

    public final void b(int i) {
        if (this.b != null) {
            String noticeUrl = this.b.getNoticeUrl();
            if (TextUtils.isEmpty(noticeUrl)) {
                return;
            }
            if (i == 1 || i == 2) {
                if (!noticeUrl.contains("endscreen_type")) {
                    StringBuilder sb = new StringBuilder(noticeUrl);
                    if (noticeUrl.contains("?")) {
                        sb.append("&endscreen_type=");
                        sb.append(i);
                    } else {
                        sb.append("?endscreen_type=");
                        sb.append(i);
                    }
                    noticeUrl = sb.toString();
                } else if (i == 2) {
                    if (noticeUrl.contains("endscreen_type=1")) {
                        noticeUrl = noticeUrl.replace("endscreen_type=1", "endscreen_type=2");
                    }
                } else if (noticeUrl.contains("endscreen_type=2")) {
                    noticeUrl = noticeUrl.replace("endscreen_type=2", "endscreen_type=1");
                }
                this.b.setNoticeUrl(noticeUrl);
            }
        }
    }

    public final void b() {
        try {
            if (this.a && this.b != null && s.b(this.e) && com.mintegral.msdk.base.controller.a.d().h() != null) {
                l a2 = l.a(i.a(com.mintegral.msdk.base.controller.a.d().h()));
                f fVar = new f();
                fVar.a(System.currentTimeMillis());
                fVar.b(this.e);
                fVar.a(this.b.getId());
                a2.a(fVar);
            }
        } catch (Throwable th) {
            g.c("NotifyListener", th.getMessage(), th);
        }
    }

    public final void c() {
        try {
            if (this.a && this.b != null && s.b(this.e)) {
                com.mintegral.msdk.videocommon.a.a.a().a(this.b, this.e);
            }
        } catch (Throwable th) {
            g.c("NotifyListener", th.getMessage(), th);
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        if (this.c != null) {
            this.c.b(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    /* access modifiers changed from: protected */
    public final void e() {
        try {
            if (this.a && !TextUtils.isEmpty(this.b.getOnlyImpressionURL()) && com.mintegral.msdk.base.common.a.c.a != null && !com.mintegral.msdk.base.common.a.c.a.containsKey(this.b.getOnlyImpressionURL()) && !this.h) {
                com.mintegral.msdk.base.common.a.c.a.put(this.b.getOnlyImpressionURL(), Long.valueOf(System.currentTimeMillis()));
                com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), this.b, this.e, this.b.getOnlyImpressionURL(), false, true);
                c();
                this.h = true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    /* access modifiers changed from: protected */
    public final void f() {
        try {
            if (this.a && !this.g && !TextUtils.isEmpty(this.b.getImpressionURL())) {
                this.g = true;
                String impressionURL = this.b.getImpressionURL();
                if (this.b.getSpareOfferFlag() == 1) {
                    impressionURL = impressionURL + "&to=1";
                }
                com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), this.b, this.e, impressionURL, false, true);
                com.mintegral.msdk.video.module.b.a.a(com.mintegral.msdk.base.controller.a.d().h(), this.b);
                new Thread(new Runnable() {
                    public final void run() {
                        try {
                            m.a(i.a(com.mintegral.msdk.base.controller.a.d().h())).b(k.this.b.getId());
                        } catch (Throwable th) {
                            g.c("NotifyListener", th.getMessage(), th);
                        }
                    }
                }).start();
                if (this.a && com.mintegral.msdk.base.common.a.c.d != null && !TextUtils.isEmpty(this.b.getId())) {
                    com.mintegral.msdk.base.common.a.c.b(this.e, this.b, MTGRewardVideoActivity.INTENT_REWARD);
                }
            }
        } catch (Throwable th) {
            g.c("NotifyListener", th.getMessage(), th);
        }
    }

    /* access modifiers changed from: protected */
    public final void g() {
        try {
            if (this.a && this.c != null) {
                if (this.c.k() != null && !TextUtils.isEmpty(this.c.k().getVideoUrlEncode())) {
                    v.a(i.a(com.mintegral.msdk.base.controller.a.d().h())).a(this.c.k().getVideoUrlEncode());
                }
                if (!TextUtils.isEmpty(this.c.c())) {
                    File file = new File(this.c.c());
                    if (file.exists() && file.isFile() && file.delete()) {
                        g.a("NotifyListener", "DEL File :" + file.getAbsolutePath());
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        try {
            if (this.a) {
                w a2 = w.a(i.a(com.mintegral.msdk.base.controller.a.d().h()));
                q qVar = null;
                if (!TextUtils.isEmpty(this.b.getNoticeUrl())) {
                    int s = c.s(com.mintegral.msdk.base.controller.a.d().h());
                    qVar = new q("2000021", s, this.b.getNoticeUrl(), str, c.a(com.mintegral.msdk.base.controller.a.d().h(), s));
                } else if (!TextUtils.isEmpty(this.b.getClickURL())) {
                    int s2 = c.s(com.mintegral.msdk.base.controller.a.d().h());
                    qVar = new q("2000021", s2, this.b.getClickURL(), str, c.a(com.mintegral.msdk.base.controller.a.d().h(), s2));
                }
                if (qVar != null) {
                    qVar.m(this.b.getId());
                    qVar.e(this.b.getVideoUrlEncode());
                    qVar.o(str);
                    qVar.k(this.b.getRequestIdNotice());
                    qVar.l(this.e);
                    a2.a(qVar);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
