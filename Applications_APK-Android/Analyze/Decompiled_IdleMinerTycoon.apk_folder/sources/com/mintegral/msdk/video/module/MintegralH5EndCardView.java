package com.mintegral.msdk.video.module;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.helpshift.common.domain.network.NetworkConstants;
import com.helpshift.support.FaqTagFilter;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.q;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.mtgjscommon.mraid.d;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.mtgjscommon.windvane.g;
import com.mintegral.msdk.video.js.f;
import com.mintegral.msdk.video.module.a.a;
import java.io.File;
import java.util.HashMap;
import org.json.JSONObject;

public class MintegralH5EndCardView extends MintegralBaseView implements f {
    /* access modifiers changed from: private */
    public boolean A = false;
    /* access modifiers changed from: private */
    public boolean B = false;
    private boolean C = false;
    private boolean D = false;
    /* access modifiers changed from: private */
    public boolean E = false;
    /* access modifiers changed from: private */
    public boolean F = false;
    /* access modifiers changed from: private */
    public boolean G = false;
    private boolean H = false;
    private boolean I = false;
    protected View i;
    protected RelativeLayout j;
    protected ImageView k;
    protected WindVaneWebView l;
    protected Handler m = new Handler();
    protected String n;
    Handler o = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.what == 100) {
                if (MintegralH5EndCardView.this.A) {
                    MintegralH5EndCardView.this.e.a(122, "");
                }
                MintegralH5EndCardView.this.e.a(103, "");
            }
        }
    };
    boolean p = false;
    private boolean q = false;
    /* access modifiers changed from: private */
    public boolean r = false;
    /* access modifiers changed from: private */
    public boolean s = false;
    private boolean t = false;
    private int u = 1;
    private int v = 1;
    private boolean w = false;
    private int x = 1;
    /* access modifiers changed from: private */
    public String y;
    /* access modifiers changed from: private */
    public long z = 0;

    public void install(CampaignEx campaignEx) {
    }

    public MintegralH5EndCardView(Context context) {
        super(context);
    }

    public MintegralH5EndCardView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void onSelfConfigurationChanged(Configuration configuration) {
        super.onSelfConfigurationChanged(configuration);
        orientation(configuration);
    }

    public void init(Context context) {
        int findLayout = findLayout("mintegral_reward_endcard_h5");
        if (findLayout >= 0) {
            this.i = this.c.inflate(findLayout, (ViewGroup) null);
            View view = this.i;
            this.k = (ImageView) view.findViewById(findID("mintegral_windwv_close"));
            this.j = (RelativeLayout) view.findViewById(findID("mintegral_windwv_content_rl"));
            this.l = new WindVaneWebView(getContext());
            this.l.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            this.j.addView(this.l);
            this.f = isNotNULL(this.k, this.l);
            addView(this.i, d());
            a();
            e();
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.f) {
            this.k.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    MintegralH5EndCardView.this.onCloseViewClick();
                }
            });
        }
    }

    public void onCloseViewClick() {
        try {
            if (this.l != null) {
                g.a();
                g.a(this.l, "onSystemDestory", "");
                new Thread(new Runnable() {
                    public final void run() {
                        try {
                            Thread.sleep(300);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        MintegralH5EndCardView.this.o.sendEmptyMessage(100);
                    }
                }).start();
                return;
            }
            this.e.a(103, "");
            this.e.a(119, "webview is null when closing webview");
        } catch (Exception e) {
            this.e.a(103, "");
            a aVar = this.e;
            aVar.a(119, "close webview exception" + e.getMessage());
            com.mintegral.msdk.base.utils.g.a(MintegralBaseView.TAG, e.getMessage());
        }
    }

    public void setError(boolean z2) {
        this.s = z2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x00fb A[Catch:{ Throwable -> 0x011d }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0112 A[Catch:{ Throwable -> 0x011d }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void preLoadData() {
        /*
            r8 = this;
            java.lang.String r1 = r8.f()
            boolean r0 = r8.f
            r6 = 0
            if (r0 == 0) goto L_0x0167
            com.mintegral.msdk.base.entity.CampaignEx r0 = r8.b
            if (r0 == 0) goto L_0x0167
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0167
            com.mintegral.msdk.base.webview.BrowserView$MTGDownloadListener r0 = new com.mintegral.msdk.base.webview.BrowserView$MTGDownloadListener
            com.mintegral.msdk.base.entity.CampaignEx r2 = r8.b
            r0.<init>(r2)
            com.mintegral.msdk.base.entity.CampaignEx r2 = r8.b
            java.lang.String r2 = r2.getAppName()
            r0.setTitle(r2)
            com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView r2 = r8.l
            r2.setDownloadListener(r0)
            com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView r0 = r8.l
            com.mintegral.msdk.base.entity.CampaignEx r2 = r8.b
            java.lang.String r2 = r2.getId()
            r0.setCampaignId(r2)
            r0 = 8
            r8.setCloseVisible(r0)
            com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView r0 = r8.l
            com.mintegral.msdk.video.module.MintegralH5EndCardView$4 r2 = new com.mintegral.msdk.video.module.MintegralH5EndCardView$4
            r2.<init>()
            r0.setWebViewListener(r2)
            com.mintegral.msdk.base.entity.CampaignEx r0 = r8.b
            java.lang.String r0 = r0.getMraid()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x0127
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x011d }
            r8.z = r2     // Catch:{ Throwable -> 0x011d }
            com.mintegral.msdk.base.entity.CampaignEx r0 = r8.b     // Catch:{ Throwable -> 0x011d }
            java.lang.String r0 = r0.getendcard_url()     // Catch:{ Throwable -> 0x011d }
            com.mintegral.msdk.videocommon.e.b.a()     // Catch:{ Throwable -> 0x011d }
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x011d }
            java.lang.String r2 = r2.j()     // Catch:{ Throwable -> 0x011d }
            java.lang.String r3 = r8.y     // Catch:{ Throwable -> 0x011d }
            com.mintegral.msdk.videocommon.e.c r2 = com.mintegral.msdk.videocommon.e.b.a(r2, r3)     // Catch:{ Throwable -> 0x011d }
            boolean r3 = r8.t     // Catch:{ Throwable -> 0x011d }
            if (r3 == 0) goto L_0x0127
            boolean r3 = com.mintegral.msdk.base.utils.s.b(r0)     // Catch:{ Throwable -> 0x011d }
            if (r3 == 0) goto L_0x0127
            java.lang.String r3 = "wfr=1"
            boolean r3 = r0.contains(r3)     // Catch:{ Throwable -> 0x011d }
            if (r3 != 0) goto L_0x0085
            if (r2 == 0) goto L_0x0127
            int r3 = r2.l()     // Catch:{ Throwable -> 0x011d }
            if (r3 <= 0) goto L_0x0127
        L_0x0085:
            java.lang.String r3 = "MintegralBaseView"
            java.lang.String r4 = "需要上报endcard加载时间"
            com.mintegral.msdk.base.utils.g.d(r3, r4)     // Catch:{ Throwable -> 0x011d }
            java.lang.String r3 = "wfr=1"
            boolean r3 = r0.contains(r3)     // Catch:{ Throwable -> 0x011d }
            r4 = 20
            if (r3 == 0) goto L_0x00ea
            java.lang.String r2 = "&"
            java.lang.String[] r0 = r0.split(r2)     // Catch:{ Throwable -> 0x011d }
            if (r0 == 0) goto L_0x00f7
            int r2 = r0.length     // Catch:{ Throwable -> 0x011d }
            if (r2 <= 0) goto L_0x00f7
            int r2 = r0.length     // Catch:{ Throwable -> 0x011d }
            r3 = 0
        L_0x00a3:
            if (r3 >= r2) goto L_0x00f7
            r5 = r0[r3]     // Catch:{ Throwable -> 0x011d }
            boolean r7 = com.mintegral.msdk.base.utils.s.b(r5)     // Catch:{ Throwable -> 0x011d }
            if (r7 == 0) goto L_0x00e7
            java.lang.String r7 = "to"
            boolean r7 = r5.contains(r7)     // Catch:{ Throwable -> 0x011d }
            if (r7 == 0) goto L_0x00e7
            java.lang.String r7 = "="
            java.lang.String[] r7 = r5.split(r7)     // Catch:{ Throwable -> 0x011d }
            if (r7 == 0) goto L_0x00e7
            java.lang.String r7 = "="
            java.lang.String[] r7 = r5.split(r7)     // Catch:{ Throwable -> 0x011d }
            int r7 = r7.length     // Catch:{ Throwable -> 0x011d }
            if (r7 <= 0) goto L_0x00e7
            java.lang.String r0 = "="
            java.lang.String[] r0 = r5.split(r0)     // Catch:{ Throwable -> 0x011d }
            r2 = 1
            r0 = r0[r2]     // Catch:{ Throwable -> 0x011d }
            int r0 = com.mintegral.msdk.base.utils.k.a(r0)     // Catch:{ Throwable -> 0x011d }
            java.lang.String r2 = "MintegralBaseView"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x011d }
            java.lang.String r5 = "从url获取的waitingtime:"
            r3.<init>(r5)     // Catch:{ Throwable -> 0x011d }
            r3.append(r0)     // Catch:{ Throwable -> 0x011d }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x011d }
            com.mintegral.msdk.base.utils.g.b(r2, r3)     // Catch:{ Throwable -> 0x011d }
            goto L_0x00f9
        L_0x00e7:
            int r3 = r3 + 1
            goto L_0x00a3
        L_0x00ea:
            if (r2 == 0) goto L_0x00f7
            int r0 = r2.l()     // Catch:{ Throwable -> 0x011d }
            if (r0 <= 0) goto L_0x00f7
            int r0 = r2.l()     // Catch:{ Throwable -> 0x011d }
            goto L_0x00f9
        L_0x00f7:
            r0 = 20
        L_0x00f9:
            if (r0 < 0) goto L_0x0112
            r8.excuteEndCardShowTask(r0)     // Catch:{ Throwable -> 0x011d }
            java.lang.String r2 = "MintegralBaseView"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x011d }
            java.lang.String r4 = "开启excuteEndCardShowTask:"
            r3.<init>(r4)     // Catch:{ Throwable -> 0x011d }
            r3.append(r0)     // Catch:{ Throwable -> 0x011d }
            java.lang.String r0 = r3.toString()     // Catch:{ Throwable -> 0x011d }
            com.mintegral.msdk.base.utils.g.b(r2, r0)     // Catch:{ Throwable -> 0x011d }
            goto L_0x0127
        L_0x0112:
            r8.excuteEndCardShowTask(r4)     // Catch:{ Throwable -> 0x011d }
            java.lang.String r0 = "MintegralBaseView"
            java.lang.String r2 = "开启excuteEndCardShowTask: 20s def"
            com.mintegral.msdk.base.utils.g.b(r0, r2)     // Catch:{ Throwable -> 0x011d }
            goto L_0x0127
        L_0x011d:
            r0 = move-exception
            java.lang.String r2 = "MintegralBaseView"
            java.lang.String r3 = r0.getMessage()
            com.mintegral.msdk.base.utils.g.c(r2, r3, r0)
        L_0x0127:
            com.mintegral.msdk.videocommon.download.h r0 = com.mintegral.msdk.videocommon.download.h.a()
            java.lang.String r0 = r0.a(r1)
            r8.setHtmlSource(r0)
            java.lang.String r0 = r8.n
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x0153
            java.lang.String r0 = "MintegralBaseView"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "load url:"
            r2.<init>(r3)
            r2.append(r1)
            java.lang.String r2 = r2.toString()
            com.mintegral.msdk.base.utils.g.a(r0, r2)
            com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView r0 = r8.l
            r0.loadUrl(r1)
            goto L_0x0170
        L_0x0153:
            java.lang.String r0 = "MintegralBaseView"
            java.lang.String r2 = "load html..."
            com.mintegral.msdk.base.utils.g.a(r0, r2)
            com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView r0 = r8.l
            java.lang.String r2 = r8.n
            java.lang.String r3 = "text/html"
            java.lang.String r4 = "UTF-8"
            r5 = 0
            r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)
            goto L_0x0170
        L_0x0167:
            com.mintegral.msdk.video.module.a.a r0 = r8.e
            r1 = 101(0x65, float:1.42E-43)
            java.lang.String r2 = ""
            r0.a(r1, r2)
        L_0x0170:
            r8.p = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.video.module.MintegralH5EndCardView.preLoadData():void");
    }

    public void reportRenderFailed(String str) {
        if (this.b != null && !this.s) {
            q qVar = new q();
            qVar.k(this.b.getRequestIdNotice());
            qVar.m(this.b.getId());
            qVar.c(3);
            qVar.p(String.valueOf(System.currentTimeMillis() - this.z));
            qVar.f(this.b.getendcard_url());
            String str2 = "2";
            if (s.b(this.b.getendcard_url()) && this.b.getendcard_url().contains(".zip")) {
                str2 = "1";
            }
            qVar.g(str2);
            qVar.o(str);
            if (this.b.getAdType() == 287) {
                qVar.h(NetworkConstants.apiVersion);
            } else if (this.b.getAdType() == 94) {
                qVar.h("1");
            } else if (this.b.getAdType() == 42) {
                qVar.h("2");
            }
            qVar.a(this.b.isMraid() ? q.a : q.b);
            com.mintegral.msdk.base.common.e.a.c(qVar, this.a, this.y);
        }
    }

    public void defaultShow() {
        super.defaultShow();
    }

    public void notifyCloseBtn(int i2) {
        switch (i2) {
            case 0:
                this.C = true;
                return;
            case 1:
                this.D = true;
                return;
            default:
                return;
        }
    }

    public void toggleCloseBtn(int i2) {
        int visibility = this.k.getVisibility();
        switch (i2) {
            case 1:
                this.B = true;
                visibility = 0;
                break;
            case 2:
                this.B = false;
                visibility = 8;
                if (!this.p) {
                    if (!this.H && !this.C) {
                        this.H = true;
                        if (this.u != 0) {
                            this.E = false;
                            if (this.u >= 0) {
                                this.m.postDelayed(new Runnable() {
                                    public final void run() {
                                        boolean unused = MintegralH5EndCardView.this.E = true;
                                    }
                                }, (long) (this.u * 1000));
                                break;
                            }
                        } else {
                            this.E = true;
                            break;
                        }
                    }
                } else if (!this.I && !this.C) {
                    this.I = true;
                    if (this.v != 0) {
                        this.F = false;
                        if (this.v >= 0) {
                            this.m.postDelayed(new Runnable() {
                                public final void run() {
                                    boolean unused = MintegralH5EndCardView.this.F = true;
                                }
                            }, (long) (this.v * 1000));
                            break;
                        }
                    } else {
                        this.F = true;
                        break;
                    }
                }
                break;
        }
        setCloseVisible(visibility);
    }

    public void handlerPlayableException(String str) {
        com.mintegral.msdk.base.utils.g.d("========", "===========handlerPlayableException");
        if (!this.s) {
            this.s = true;
            this.r = false;
            if (this.b != null) {
                q qVar = new q();
                qVar.k(this.b.getRequestIdNotice());
                qVar.m(this.b.getId());
                qVar.o(str);
                com.mintegral.msdk.base.common.e.a.f(qVar, this.a, this.y);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.video.module.MintegralH5EndCardView.a(long, boolean):void
     arg types: [long, int]
     candidates:
      com.mintegral.msdk.video.module.MintegralH5EndCardView.a(com.mintegral.msdk.video.module.MintegralH5EndCardView, long):void
      com.mintegral.msdk.video.module.MintegralH5EndCardView.a(long, boolean):void */
    public void readyStatus(int i2) {
        com.mintegral.msdk.base.utils.g.b(MintegralBaseView.TAG, "h5EncardView readyStatus:" + i2 + "- isError" + this.s);
        this.x = i2;
        if (!this.s) {
            a(System.currentTimeMillis() - this.z, false);
        }
    }

    public void webviewshow() {
        if (this.l != null) {
            this.l.post(new Runnable() {
                public final void run() {
                    try {
                        com.mintegral.msdk.base.utils.g.a(MintegralBaseView.TAG, "webviewshow");
                        String str = "";
                        try {
                            int[] iArr = new int[2];
                            MintegralH5EndCardView.this.l.getLocationOnScreen(iArr);
                            com.mintegral.msdk.base.utils.g.d(MintegralBaseView.TAG, "coordinate:" + iArr[0] + "--" + iArr[1]);
                            JSONObject jSONObject = new JSONObject();
                            Context h = com.mintegral.msdk.base.controller.a.d().h();
                            if (h != null) {
                                jSONObject.put("startX", k.a(h, (float) iArr[0]));
                                jSONObject.put("startY", k.a(h, (float) iArr[1]));
                                jSONObject.put(com.mintegral.msdk.base.common.a.C, (double) k.c(h));
                            }
                            str = jSONObject.toString();
                        } catch (Throwable th) {
                            com.mintegral.msdk.base.utils.g.c(MintegralBaseView.TAG, th.getMessage(), th);
                        }
                        String encodeToString = Base64.encodeToString(str.toString().getBytes(), 2);
                        g.a();
                        g.a(MintegralH5EndCardView.this.l, "webviewshow", encodeToString);
                        MintegralH5EndCardView.this.e.a(109, "");
                        MintegralH5EndCardView.i(MintegralH5EndCardView.this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public void orientation(Configuration configuration) {
        try {
            JSONObject jSONObject = new JSONObject();
            if (configuration.orientation == 2) {
                jSONObject.put("orientation", "landscape");
            } else {
                jSONObject.put("orientation", "portrait");
            }
            String encodeToString = Base64.encodeToString(jSONObject.toString().getBytes(), 2);
            g.a();
            g.a(this.l, "orientation", encodeToString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean canBackPress() {
        return this.k != null && this.k.getVisibility() == 0;
    }

    public boolean isLoadSuccess() {
        return this.r;
    }

    public void setUnitId(String str) {
        this.y = str;
    }

    public void setCloseDelayShowTime(int i2) {
        this.u = i2;
    }

    public void setPlayCloseBtnTm(int i2) {
        this.v = i2;
    }

    public void setHtmlSource(String str) {
        this.n = str;
    }

    public void setCloseVisible(int i2) {
        if (this.f) {
            this.k.setVisibility(i2);
        }
    }

    public void setCloseVisibleForMraid(int i2) {
        if (this.f) {
            this.G = true;
            if (i2 == 4) {
                this.k.setImageDrawable(new ColorDrawable(16711680));
            } else {
                this.k.setImageResource(findDrawable("mintegral_reward_close"));
            }
            this.k.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public RelativeLayout.LayoutParams d() {
        return new RelativeLayout.LayoutParams(-1, -1);
    }

    /* access modifiers changed from: protected */
    public void e() {
        if (this.f) {
            setMatchParent();
        }
    }

    /* access modifiers changed from: protected */
    public String f() {
        String str;
        if (this.b != null) {
            this.A = true;
            if (this.b.isMraid()) {
                this.t = false;
                String mraid = this.b.getMraid();
                if (!TextUtils.isEmpty(mraid)) {
                    File file = new File(mraid);
                    try {
                        if (!file.exists() || !file.isFile() || !file.canRead()) {
                            com.mintegral.msdk.base.utils.g.b(MintegralBaseView.TAG, "Mraid file not found. Will use endcard url.");
                            str = this.b.getEndScreenUrl();
                        } else {
                            com.mintegral.msdk.base.utils.g.b(MintegralBaseView.TAG, "Mraid file " + mraid);
                            str = "file:////" + mraid;
                        }
                        return str;
                    } catch (Throwable th) {
                        if (!MIntegralConstans.DEBUG) {
                            return mraid;
                        }
                        th.printStackTrace();
                        return mraid;
                    }
                } else {
                    String endScreenUrl = this.b.getEndScreenUrl();
                    com.mintegral.msdk.base.utils.g.d(MintegralBaseView.TAG, "getURL playable=false endscreenurl兜底:" + endScreenUrl);
                    return endScreenUrl;
                }
            } else {
                String str2 = this.b.getendcard_url();
                if (!s.a(str2)) {
                    this.t = true;
                    String a = com.mintegral.msdk.videocommon.download.g.a().a(str2);
                    if (TextUtils.isEmpty(a)) {
                        com.mintegral.msdk.base.utils.g.b(MintegralBaseView.TAG, "getURL playable=true endcard本地资源地址为空拿服务端地址:" + str2);
                        return str2;
                    }
                    com.mintegral.msdk.base.utils.g.b(MintegralBaseView.TAG, "getURL playable=true 资源不为空endcard地址:" + a);
                    return a;
                }
                this.t = false;
                String endScreenUrl2 = this.b.getEndScreenUrl();
                com.mintegral.msdk.base.utils.g.d(MintegralBaseView.TAG, "getURL playable=false endscreenurl兜底:" + endScreenUrl2);
                return endScreenUrl2;
            }
        } else {
            this.A = false;
            com.mintegral.msdk.base.utils.g.d(MintegralBaseView.TAG, "getURL playable=false url为空");
            return null;
        }
    }

    public void excuteTask() {
        if (!this.t && this.u >= 0) {
            this.m.postDelayed(new Runnable() {
                public final void run() {
                    if (!MintegralH5EndCardView.this.G) {
                        MintegralH5EndCardView.this.setCloseVisible(0);
                    }
                    boolean unused = MintegralH5EndCardView.this.B = true;
                }
            }, (long) (this.u * 1000));
        }
    }

    public void excuteEndCardShowTask(final int i2) {
        this.m.postDelayed(new Runnable() {
            public final void run() {
                com.mintegral.msdk.base.utils.g.b(MintegralBaseView.TAG, "endcard 加载等待结束 开始插入数据库");
                MintegralH5EndCardView.this.a((long) (i2 * 1000), true);
            }
        }, (long) (i2 * 1000));
    }

    /* access modifiers changed from: private */
    public void a(long j2, boolean z2) {
        int i2;
        String str;
        int i3;
        try {
            if (this.w) {
                com.mintegral.msdk.base.utils.g.b(MintegralBaseView.TAG, "insertEndCardReadyState hasInsertLoadEndCardReport true return");
                return;
            }
            this.w = true;
            String str2 = "2";
            if (s.b(this.b.getendcard_url()) && this.b.getendcard_url().contains(".zip")) {
                str2 = "1";
            }
            String str3 = str2;
            if (z2) {
                str = "ready timeout";
                i3 = 12;
                i2 = 2;
            } else if (this.x == 2) {
                str = "ready no";
                i3 = 11;
                i2 = 3;
            } else {
                str = "ready yes";
                i3 = 10;
                i2 = 1;
            }
            q qVar = r3;
            q qVar2 = new q("2000043", i3, String.valueOf(j2), this.b.getendcard_url(), this.b.getId(), this.y, str, str3);
            if (this.b.getAdType() == 287) {
                qVar.h(NetworkConstants.apiVersion);
            } else if (this.b.getAdType() == 94) {
                qVar.h("1");
            } else if (this.b.getAdType() == 42) {
                qVar.h("2");
            }
            qVar.k(this.b.getRequestIdNotice());
            com.mintegral.msdk.base.common.e.a.a(qVar, com.mintegral.msdk.base.controller.a.d().h(), this.y);
            if (!isLoadSuccess() && i2 == 1) {
                qVar.c(i2);
                qVar.p(String.valueOf(j2));
                qVar.g(str3);
                qVar.f(this.b.getendcard_url());
                String str4 = "2";
                if (s.b(this.b.getendcard_url()) && this.b.getendcard_url().contains(".zip")) {
                    str4 = "1";
                }
                qVar.g(str4);
                qVar.m(this.b.getId());
                qVar.o(str);
                qVar.a(this.b.isMraid() ? q.a : q.b);
                com.mintegral.msdk.base.common.e.a.c(qVar, this.a, this.y);
            }
            com.mintegral.msdk.base.utils.g.b(MintegralBaseView.TAG, "insertEndCardReadyState result:" + i3 + " endCardLoadTime:" + j2 + " endcardurl:" + this.b.getendcard_url() + "  id:" + this.b.getId() + "  unitid:" + this.y + "  reason:" + str + "  type:" + str3);
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c(MintegralBaseView.TAG, th.getMessage(), th);
        }
    }

    public boolean isPlayable() {
        return this.t;
    }

    public void onBackPress() {
        if (this.B || ((this.C && this.D) || ((!this.C && this.E && !this.p) || (!this.C && this.F && this.p)))) {
            onCloseViewClick();
        }
    }

    public void setLoadPlayable(boolean z2) {
        this.p = z2;
    }

    public void release() {
        if (this.m != null) {
            this.m.removeCallbacksAndMessages(null);
        }
        if (this.o != null) {
            this.o.removeCallbacksAndMessages(null);
        }
        this.j.removeAllViews();
        this.l.release();
        this.l = null;
    }

    public void volumeChange(double d) {
        com.mintegral.msdk.mtgjscommon.mraid.a.a();
        com.mintegral.msdk.mtgjscommon.mraid.a.a(this.l, d);
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (this.b != null && this.b.isMraid()) {
            if (z2) {
                com.mintegral.msdk.mtgjscommon.mraid.a.a();
                com.mintegral.msdk.mtgjscommon.mraid.a.b(this.l, "true");
                return;
            }
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.b(this.l, "false");
        }
    }

    static /* synthetic */ void i(MintegralH5EndCardView mintegralH5EndCardView) {
        if (mintegralH5EndCardView.b != null && mintegralH5EndCardView.b.isMraid()) {
            int i2 = mintegralH5EndCardView.getResources().getConfiguration().orientation;
            String str = FaqTagFilter.Operator.UNDEFINED;
            switch (i2) {
                case 0:
                    str = FaqTagFilter.Operator.UNDEFINED;
                    break;
                case 1:
                    str = "portrait";
                    break;
                case 2:
                    str = "landscape";
                    break;
            }
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("orientation", str);
                jSONObject.put("locked", "true");
            } catch (Exception e) {
                e.printStackTrace();
            }
            HashMap hashMap = new HashMap();
            hashMap.put("placementType", "Interstitial");
            hashMap.put("state", "default");
            hashMap.put("viewable", "true");
            hashMap.put("currentAppOrientation", jSONObject);
            if (mintegralH5EndCardView.getContext() instanceof Activity) {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((Activity) mintegralH5EndCardView.getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                com.mintegral.msdk.mtgjscommon.mraid.a.a();
                com.mintegral.msdk.mtgjscommon.mraid.a.b(mintegralH5EndCardView.l, (float) c.n(mintegralH5EndCardView.getContext()), (float) c.o(mintegralH5EndCardView.getContext()));
                com.mintegral.msdk.mtgjscommon.mraid.a.a();
                com.mintegral.msdk.mtgjscommon.mraid.a.c(mintegralH5EndCardView.l, (float) displayMetrics.widthPixels, (float) displayMetrics.heightPixels);
            }
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.a(mintegralH5EndCardView.l, (float) mintegralH5EndCardView.l.getLeft(), (float) mintegralH5EndCardView.l.getTop(), (float) mintegralH5EndCardView.l.getWidth(), (float) mintegralH5EndCardView.l.getHeight());
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.b(mintegralH5EndCardView.l, (float) mintegralH5EndCardView.l.getLeft(), (float) mintegralH5EndCardView.l.getTop(), (float) mintegralH5EndCardView.l.getWidth(), (float) mintegralH5EndCardView.l.getHeight());
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.a(mintegralH5EndCardView.l, hashMap);
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.a(mintegralH5EndCardView.l, d.a);
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.a(mintegralH5EndCardView.l);
        }
    }
}
