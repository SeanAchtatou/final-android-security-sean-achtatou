package com.mintegral.msdk.video.module;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.videocommon.download.j;
import org.json.JSONObject;

public class MintegralClickMiniCardView extends MintegralH5EndCardView {
    private boolean q = false;

    public MintegralClickMiniCardView(Context context) {
        super(context);
    }

    public MintegralClickMiniCardView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void onSelfConfigurationChanged(Configuration configuration) {
        if (this.f) {
            a(this.i);
        }
        super.onSelfConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public final RelativeLayout.LayoutParams d() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13, -1);
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
        if (this.f) {
            setBackgroundResource(findColor("mintegral_reward_minicard_bg"));
            a(this.i);
            setClickable(true);
        }
    }

    /* access modifiers changed from: protected */
    public final String f() {
        String str = null;
        if (this.b == null) {
            return null;
        }
        CampaignEx.c rewardTemplateMode = this.b.getRewardTemplateMode();
        if (rewardTemplateMode != null) {
            str = rewardTemplateMode.c();
        }
        if (TextUtils.isEmpty(str) || !str.contains(".zip")) {
            return str;
        }
        String a = j.a().a(str);
        return !TextUtils.isEmpty(a) ? a : str;
    }

    public void preLoadData() {
        super.preLoadData();
        setCloseVisible(0);
    }

    public void webviewshow() {
        if (this.l != null) {
            this.l.post(new Runnable() {
                public final void run() {
                    try {
                        g.a(MintegralBaseView.TAG, "webviewshow");
                        String str = "";
                        try {
                            int[] iArr = new int[2];
                            MintegralClickMiniCardView.this.l.getLocationOnScreen(iArr);
                            g.d(MintegralBaseView.TAG, "coordinate:" + iArr[0] + "--" + iArr[1]);
                            JSONObject jSONObject = new JSONObject();
                            jSONObject.put("startX", k.a(a.d().h(), (float) iArr[0]));
                            jSONObject.put("startY", k.a(a.d().h(), (float) iArr[1]));
                            str = jSONObject.toString();
                        } catch (Throwable th) {
                            g.c(MintegralBaseView.TAG, th.getMessage(), th);
                        }
                        String encodeToString = Base64.encodeToString(str.toString().getBytes(), 2);
                        com.mintegral.msdk.mtgjscommon.windvane.g.a();
                        com.mintegral.msdk.mtgjscommon.windvane.g.a(MintegralClickMiniCardView.this.l, "webviewshow", encodeToString);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void a(View view) {
        int i = k.i(this.a);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = (int) ((((float) i) * 0.7f) + 0.5f);
        layoutParams.height = (int) ((((float) k.h(this.a)) * 0.7f) + 0.5f);
        view.setLayoutParams(layoutParams);
    }

    public void setMintegralClickMiniCardViewTransparent() {
        setBackgroundColor(0);
    }

    public void setMintegralClickMiniCardViewClickable(boolean z) {
        setClickable(z);
    }

    public void setMiniCardLocation(int i, int i2, int i3, int i4) {
        this.q = true;
        resizeMiniCard(i3, i4);
    }

    public void resizeMiniCard(int i, int i2) {
        if (Build.VERSION.SDK_INT >= 11) {
            View findViewById = ((Activity) this.a).getWindow().findViewById(16908290);
            int width = findViewById.getWidth();
            int height = findViewById.getHeight();
            if (i > 0 && i2 > 0 && i <= width && i2 <= height) {
                ViewGroup.LayoutParams layoutParams = this.i.getLayoutParams();
                layoutParams.width = i;
                layoutParams.height = i2;
                this.i.setLayoutParams(layoutParams);
            }
        }
    }

    public void setRadius(int i) {
        if (i > 0) {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setCornerRadius((float) k.b(getContext(), (float) i));
            gradientDrawable.setColor(-1);
            if (Build.VERSION.SDK_INT >= 16) {
                this.l.setBackground(gradientDrawable);
            } else {
                this.l.setBackgroundDrawable(gradientDrawable);
            }
            if (Build.VERSION.SDK_INT >= 21) {
                this.l.setClipToOutline(true);
            }
        }
    }
}
