package com.mintegral.msdk.video;

public final class R {
    private R() {
    }

    public static final class color {
        public static final int mintegral_reward_black = 2131034277;
        public static final int mintegral_reward_cta_bg = 2131034278;
        public static final int mintegral_reward_desc_textcolor = 2131034279;
        public static final int mintegral_reward_endcard_hor_bg = 2131034280;
        public static final int mintegral_reward_endcard_land_bg = 2131034281;
        public static final int mintegral_reward_endcard_line_bg = 2131034282;
        public static final int mintegral_reward_endcard_vast_bg = 2131034283;
        public static final int mintegral_reward_minicard_bg = 2131034285;
        public static final int mintegral_reward_six_black_transparent = 2131034286;
        public static final int mintegral_reward_title_textcolor = 2131034287;
        public static final int mintegral_reward_white = 2131034288;

        private color() {
        }
    }

    public static final class drawable {
        public static final int mintegral_reward_activity_ad_end_land_des_rl_hot = 2131165419;
        public static final int mintegral_reward_close = 2131165420;
        public static final int mintegral_reward_end_close_shape_oval = 2131165421;
        public static final int mintegral_reward_end_land_shape = 2131165422;
        public static final int mintegral_reward_end_pager_logo = 2131165423;
        public static final int mintegral_reward_end_shape_oval = 2131165424;
        public static final int mintegral_reward_shape_end_pager = 2131165425;
        public static final int mintegral_reward_shape_progress = 2131165426;
        public static final int mintegral_reward_sound_close = 2131165427;
        public static final int mintegral_reward_sound_open = 2131165428;
        public static final int mintegral_reward_vast_end_close = 2131165429;
        public static final int mintegral_reward_vast_end_ok = 2131165430;

        private drawable() {
        }
    }

    public static final class id {
        public static final int mintegral_iv_adbanner = 2131230958;
        public static final int mintegral_iv_appicon = 2131230959;
        public static final int mintegral_iv_close = 2131230960;
        public static final int mintegral_iv_hottag = 2131230961;
        public static final int mintegral_iv_icon = 2131230962;
        public static final int mintegral_iv_iconbg = 2131230963;
        public static final int mintegral_iv_vastclose = 2131230964;
        public static final int mintegral_iv_vastok = 2131230965;
        public static final int mintegral_ll_bottomlayout = 2131230970;
        public static final int mintegral_rl_bodycontainer = 2131230976;
        public static final int mintegral_rl_bottomcontainer = 2131230977;
        public static final int mintegral_rl_content = 2131230978;
        public static final int mintegral_rl_playing_close = 2131230979;
        public static final int mintegral_rl_topcontainer = 2131230980;
        public static final int mintegral_sound_switch = 2131230981;
        public static final int mintegral_sv_starlevel = 2131230982;
        public static final int mintegral_tv_adtag = 2131230983;
        public static final int mintegral_tv_appdesc = 2131230984;
        public static final int mintegral_tv_apptitle = 2131230985;
        public static final int mintegral_tv_cta = 2131230986;
        public static final int mintegral_tv_desc = 2131230987;
        public static final int mintegral_tv_install = 2131230988;
        public static final int mintegral_tv_sound = 2131230989;
        public static final int mintegral_tv_vasttag = 2131230990;
        public static final int mintegral_tv_vasttitle = 2131230991;
        public static final int mintegral_vfpv = 2131230992;
        public static final int mintegral_view_bottomline = 2131231001;
        public static final int mintegral_view_shadow = 2131231002;
        public static final int mintegral_viewgroup_ctaroot = 2131231003;
        public static final int mintegral_windwv_close = 2131231004;
        public static final int mintegral_windwv_content_rl = 2131231005;

        private id() {
        }
    }

    public static final class layout {
        public static final int mintegral_reward_clickable_cta = 2131427451;
        public static final int mintegral_reward_endcard_h5 = 2131427452;
        public static final int mintegral_reward_endcard_native_hor = 2131427453;
        public static final int mintegral_reward_endcard_native_land = 2131427454;
        public static final int mintegral_reward_endcard_vast = 2131427455;
        public static final int mintegral_reward_videoview_item = 2131427456;

        private layout() {
        }
    }

    public static final class string {
        public static final int mintegral_reward_appdesc = 2131689715;
        public static final int mintegral_reward_apptitle = 2131689716;
        public static final int mintegral_reward_clickable_cta_btntext = 2131689717;
        public static final int mintegral_reward_endcard_ad = 2131689718;
        public static final int mintegral_reward_endcard_vast_notice = 2131689719;
        public static final int mintegral_reward_install = 2131689720;

        private string() {
        }
    }
}
