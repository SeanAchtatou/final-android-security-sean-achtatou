package com.mintegral.msdk.video.js.a;

import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.video.js.e;
import com.mintegral.msdk.video.module.MintegralVideoView;

/* compiled from: DefaultJSNotifyProxy */
public class d implements e {
    public void a(int i) {
        g.a("js", "onVideoStatusNotify:" + i);
    }

    public void a(int i, String str) {
        g.a("js", "onClick:" + i + ",pt:" + str);
    }

    public void a(MintegralVideoView.a aVar) {
        g.a("js", "onProgressNotify:" + aVar.toString());
    }

    public void a(Object obj) {
        g.a("js", "onWebviewShow:" + obj);
    }

    public void a(int i, int i2, int i3, int i4) {
        g.a("js", "showDataInfo");
    }
}
