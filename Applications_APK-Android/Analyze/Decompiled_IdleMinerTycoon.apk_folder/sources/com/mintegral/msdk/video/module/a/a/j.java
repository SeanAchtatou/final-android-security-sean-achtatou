package com.mintegral.msdk.video.module.a.a;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.utils.f;
import com.mintegral.msdk.base.utils.g;

/* compiled from: RoundedImageLoaderListener */
public final class j extends e {
    private int a;

    public j(ImageView imageView, int i) {
        super(imageView);
        this.a = i;
    }

    public final void onSuccessLoad(Bitmap bitmap, String str) {
        Bitmap a2;
        if (bitmap == null) {
            try {
                g.d("ImageLoaderListener", "bitmap=null");
            } catch (Throwable th) {
                if (MIntegralConstans.DEBUG) {
                    th.printStackTrace();
                }
            }
        } else if (this.b != null && this.b.get() != null && (a2 = f.a(bitmap, this.a)) != null) {
            ((ImageView) this.b.get()).setImageBitmap(a2);
        }
    }
}
