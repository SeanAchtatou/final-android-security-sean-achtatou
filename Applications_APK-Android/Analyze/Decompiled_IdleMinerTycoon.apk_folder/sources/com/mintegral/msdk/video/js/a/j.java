package com.mintegral.msdk.video.js.a;

import android.support.v4.app.NotificationCompat;
import android.util.Base64;
import android.webkit.WebView;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.LocationConst;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.mtgjscommon.windvane.g;
import com.mintegral.msdk.video.module.MintegralVideoView;
import im.getsocial.sdk.consts.LanguageCodes;
import org.json.JSONObject;

/* compiled from: JSNotifyProxy */
public final class j extends d {
    private WebView a;

    public j(WebView webView) {
        this.a = webView;
    }

    public final void a(int i) {
        super.a(i);
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("status", i);
            String encodeToString = Base64.encodeToString(jSONObject.toString().getBytes(), 2);
            g.a();
            g.a(this.a, "onVideoStatusNotify", encodeToString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void a(int i, String str) {
        super.a(i, str);
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("type", i);
            jSONObject.put(LanguageCodes.PORTUGUESE, str);
            String encodeToString = Base64.encodeToString(jSONObject.toString().getBytes(), 2);
            g.a();
            g.a(this.a, "onJSClick", encodeToString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void a(MintegralVideoView.a aVar) {
        super.a(aVar);
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(NotificationCompat.CATEGORY_PROGRESS, a(aVar.a, aVar.b));
            jSONObject.put(LocationConst.TIME, String.valueOf(aVar.a));
            jSONObject.put(IronSourceConstants.EVENTS_DURATION, String.valueOf(aVar.b));
            String encodeToString = Base64.encodeToString(jSONObject.toString().getBytes(), 2);
            g.a();
            g.a(this.a, "onVideoProgressNotify", encodeToString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String a(int i, int i2) {
        if (i2 != 0) {
            double d = (double) (((float) i) / ((float) i2));
            try {
                StringBuilder sb = new StringBuilder();
                sb.append(k.a(Double.valueOf(d)));
                return sb.toString();
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return String.valueOf(i2);
    }

    public final void a(Object obj) {
        super.a(obj);
        String str = "";
        if (obj != null && (obj instanceof String)) {
            str = Base64.encodeToString(obj.toString().getBytes(), 2);
        }
        g.a();
        g.a(this.a, "webviewshow", str);
    }

    public final void a(int i, int i2, int i3, int i4) {
        String str;
        super.a(i, i2, i3, i4);
        try {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            if (i == 2) {
                if (i2 != 1) {
                    str = "landscape";
                    jSONObject2.put("orientation", str);
                    jSONObject2.put("screen_width", i3);
                    jSONObject2.put("screen_height", i4);
                    jSONObject.put("data", jSONObject2);
                    String encodeToString = Base64.encodeToString(jSONObject.toString().getBytes(), 2);
                    g.a();
                    g.a(this.a, "showDataInfo", encodeToString);
                }
            } else if (i2 == 2) {
                str = "landscape";
                jSONObject2.put("orientation", str);
                jSONObject2.put("screen_width", i3);
                jSONObject2.put("screen_height", i4);
                jSONObject.put("data", jSONObject2);
                String encodeToString2 = Base64.encodeToString(jSONObject.toString().getBytes(), 2);
                g.a();
                g.a(this.a, "showDataInfo", encodeToString2);
            }
            str = "portrait";
            jSONObject2.put("orientation", str);
            jSONObject2.put("screen_width", i3);
            jSONObject2.put("screen_height", i4);
            jSONObject.put("data", jSONObject2);
            String encodeToString22 = Base64.encodeToString(jSONObject.toString().getBytes(), 2);
            g.a();
            g.a(this.a, "showDataInfo", encodeToString22);
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }
}
