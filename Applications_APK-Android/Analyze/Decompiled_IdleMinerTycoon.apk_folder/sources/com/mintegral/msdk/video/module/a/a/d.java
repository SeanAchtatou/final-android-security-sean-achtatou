package com.mintegral.msdk.video.module.a.a;

import android.net.Uri;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.videocommon.download.a;

/* compiled from: ContainerViewStatisticsListener */
public class d extends k {
    public d(CampaignEx campaignEx, a aVar, com.mintegral.msdk.videocommon.b.d dVar, String str, com.mintegral.msdk.video.module.a.a aVar2) {
        super(campaignEx, aVar, dVar, str, aVar2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    public void a(int i, Object obj) {
        super.a(i, obj);
        if (this.a) {
            switch (i) {
                case 100:
                    return;
                case 101:
                    return;
                case 103:
                    return;
                case 105:
                case 106:
                case 113:
                    com.mintegral.msdk.video.module.b.a.d(com.mintegral.msdk.base.controller.a.d().h(), this.b);
                    if (i != 105) {
                        String noticeUrl = this.b.getNoticeUrl();
                        if (!TextUtils.isEmpty(noticeUrl)) {
                            if (!noticeUrl.contains(com.mintegral.msdk.base.common.a.A)) {
                                noticeUrl = noticeUrl + Constants.RequestParameters.AMPERSAND + com.mintegral.msdk.base.common.a.A + "=2";
                            } else {
                                noticeUrl = noticeUrl.replace(com.mintegral.msdk.base.common.a.A + Constants.RequestParameters.EQUAL + Uri.parse(noticeUrl).getQueryParameter(com.mintegral.msdk.base.common.a.A), com.mintegral.msdk.base.common.a.A + "=2");
                            }
                        }
                        com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), this.b, this.e, noticeUrl, true, false);
                        return;
                    }
                    return;
                case 107:
                    return;
                case 109:
                    b(2);
                    a(2);
                    return;
                case 110:
                    b(1);
                    a(1);
                    return;
                case 111:
                    a(1);
                    return;
                case 122:
                    a();
                    return;
                default:
                    return;
            }
        }
    }
}
