package com.mintegral.msdk.video.module.b;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.e.c;
import com.mintegral.msdk.base.common.e.d.b;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.videocommon.b.d;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

/* compiled from: VideoViewReport */
public final class a {
    public static boolean a = false;

    public static void a() {
        a = false;
    }

    public static void a(String str, String str2) {
        try {
            if (com.mintegral.msdk.base.controller.a.d().h() != null && !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
                com.mintegral.msdk.base.common.e.d.a aVar = new com.mintegral.msdk.base.common.e.d.a(com.mintegral.msdk.base.controller.a.d().h());
                aVar.c();
                aVar.b(com.mintegral.msdk.base.common.a.f, c.a(str, com.mintegral.msdk.base.controller.a.d().h(), str2), new b() {
                    public final void b(String str) {
                        g.d("VideoViewReport", str);
                    }

                    public final void a(String str) {
                        g.d("VideoViewReport", str);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
            g.d("VideoViewReport", e.getMessage());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void */
    public static void a(Context context, CampaignEx campaignEx) {
        if (campaignEx != null && campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().n() != null) {
            com.mintegral.msdk.click.a.a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().n(), false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void */
    public static void b(Context context, CampaignEx campaignEx) {
        if (campaignEx != null && campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().h() != null) {
            com.mintegral.msdk.click.a.a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().h(), false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void */
    public static void c(Context context, CampaignEx campaignEx) {
        if (campaignEx != null && campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().i() != null) {
            com.mintegral.msdk.click.a.a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().i(), false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void */
    public static void d(Context context, CampaignEx campaignEx) {
        if (!a && campaignEx != null && campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().j() != null) {
            com.mintegral.msdk.click.a.a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().j(), false);
            a = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void */
    public static void a(Context context, CampaignEx campaignEx, int i) {
        try {
            String[] o = campaignEx.getNativeVideoTracking().o();
            if (campaignEx != null && campaignEx.getNativeVideoTracking() != null && o != null) {
                String[] strArr = new String[o.length];
                for (int i2 = 0; i2 < o.length; i2++) {
                    String str = o[i2];
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("endscreen_type", i);
                    String jSONObject2 = jSONObject.toString();
                    strArr[i2] = str + "&value=" + URLEncoder.encode(com.mintegral.msdk.base.utils.a.b(jSONObject2));
                }
                com.mintegral.msdk.click.a.a(context, campaignEx, campaignEx.getCampaignUnitId(), strArr, true);
            }
        } catch (Throwable unused) {
            g.d("", "reportEndcardshowData error");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void */
    public static void e(Context context, CampaignEx campaignEx) {
        if (campaignEx != null && campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().p() != null) {
            com.mintegral.msdk.click.a.a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().p(), false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void */
    public static void f(Context context, CampaignEx campaignEx) {
        if (campaignEx != null && campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().k() != null) {
            com.mintegral.msdk.click.a.a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().k(), false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void */
    public static void a(Context context, CampaignEx campaignEx, int i, int i2) {
        if (i2 != 0 && context != null && campaignEx != null) {
            try {
                List<Map<Integer, String>> g = campaignEx.getNativeVideoTracking().g();
                int i3 = ((i + 1) * 100) / i2;
                if (g != null) {
                    int i4 = 0;
                    while (i4 < g.size()) {
                        Map map = g.get(i4);
                        if (map != null && map.size() > 0) {
                            Iterator it = map.entrySet().iterator();
                            while (it.hasNext()) {
                                Map.Entry entry = (Map.Entry) it.next();
                                int intValue = ((Integer) entry.getKey()).intValue();
                                String str = (String) entry.getValue();
                                if (intValue <= i3 && !TextUtils.isEmpty(str)) {
                                    com.mintegral.msdk.click.a.a(context, campaignEx, campaignEx.getCampaignUnitId(), new String[]{str}, true);
                                    it.remove();
                                    g.remove(i4);
                                    i4--;
                                }
                            }
                        }
                        i4++;
                    }
                }
            } catch (Throwable unused) {
                g.d("", "reportPlayPercentageData error");
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    public static void a(CampaignEx campaignEx, Map<Integer, String> map, String str, int i) {
        if (campaignEx != null && map != null) {
            try {
                if (map.size() > 0) {
                    Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry next = it.next();
                        String str2 = (String) next.getValue();
                        if (i == ((Integer) next.getKey()).intValue() && !TextUtils.isEmpty(str2)) {
                            com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), campaignEx, str, str2, false, false);
                            it.remove();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    public static void a(CampaignEx campaignEx, String str) {
        if (campaignEx != null) {
            try {
                if (campaignEx.getAdUrlList() != null && campaignEx.getAdUrlList().size() > 0) {
                    for (String next : campaignEx.getAdUrlList()) {
                        if (!TextUtils.isEmpty(next)) {
                            com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), campaignEx, str, next, false, false);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    public static void a(CampaignEx campaignEx, d dVar, String str, String str2) {
        if (campaignEx != null) {
            try {
                com.mintegral.msdk.video.module.c.a aVar = new com.mintegral.msdk.video.module.c.a(com.mintegral.msdk.base.controller.a.d().h());
                l lVar = new l();
                lVar.a("user_id", com.mintegral.msdk.base.utils.a.b(str2));
                lVar.a("cb_type", "1");
                lVar.a(CampaignEx.JSON_KEY_REWARD_NAME, dVar.a());
                StringBuilder sb = new StringBuilder();
                sb.append(dVar.b());
                lVar.a(CampaignEx.JSON_KEY_REWARD_AMOUNT, sb.toString());
                lVar.a(MIntegralConstans.PROPERTIES_UNIT_ID, str);
                lVar.a("click_id", campaignEx.getRequestIdNotice());
                aVar.b(lVar);
                String str3 = campaignEx.getHost() + "/addReward?";
                String str4 = "";
                String trim = lVar.b().trim();
                if (!TextUtils.isEmpty(trim)) {
                    if (!str3.endsWith("?") && !str3.endsWith(Constants.RequestParameters.AMPERSAND)) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(str3);
                        sb2.append(str3.contains("?") ? Constants.RequestParameters.AMPERSAND : "?");
                        str3 = sb2.toString();
                    }
                    str4 = str3 + trim;
                }
                String str5 = str4;
                g.d("VideoViewReport", "rewardUrl:" + str5);
                com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), campaignEx, campaignEx.getCampaignUnitId(), str5, false, false);
            } catch (Throwable th) {
                g.c("VideoViewReport", th.getMessage(), th);
            }
        }
    }
}
