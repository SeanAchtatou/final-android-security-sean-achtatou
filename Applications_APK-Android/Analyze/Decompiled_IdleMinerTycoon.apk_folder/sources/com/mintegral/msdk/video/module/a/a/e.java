package com.mintegral.msdk.video.module.a.a;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.common.c.c;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.q;
import com.mintegral.msdk.base.utils.g;
import java.lang.ref.WeakReference;

/* compiled from: DefaultImageLoaderListener */
public class e implements c {
    private CampaignEx a;
    protected WeakReference<ImageView> b;
    private String c;

    public e(ImageView imageView) {
        this.b = new WeakReference<>(imageView);
    }

    public e(ImageView imageView, CampaignEx campaignEx, String str) {
        this.b = new WeakReference<>(imageView);
        this.a = campaignEx;
        this.c = str;
    }

    public void onSuccessLoad(Bitmap bitmap, String str) {
        if (bitmap == null) {
            try {
                g.d("ImageLoaderListener", "bitmap=null");
            } catch (Throwable th) {
                if (MIntegralConstans.DEBUG) {
                    th.printStackTrace();
                }
            }
        } else if (this.b != null && this.b.get() != null) {
            this.b.get().setImageBitmap(bitmap);
            this.b.get().setVisibility(0);
        }
    }

    public void onFailedLoad(String str, String str2) {
        try {
            w a2 = w.a(i.a(a.d().h()));
            if (this.a == null) {
                g.a("ImageLoaderListener", "campaign is null");
                return;
            }
            q qVar = new q();
            qVar.n("2000044");
            qVar.b(com.mintegral.msdk.base.utils.c.s(a.d().h()));
            qVar.m(this.a.getId());
            qVar.d(this.a.getImageUrl());
            qVar.k(this.a.getRequestIdNotice());
            qVar.l(this.c);
            qVar.o(str);
            a2.a(qVar);
            g.d("ImageLoaderListener", "desc:" + str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
