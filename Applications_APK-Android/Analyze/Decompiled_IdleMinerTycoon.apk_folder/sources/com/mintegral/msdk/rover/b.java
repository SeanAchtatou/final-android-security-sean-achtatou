package com.mintegral.msdk.rover;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.r;
import com.mintegral.msdk.d.a;
import java.util.ArrayList;

/* compiled from: RoverController */
public class b {
    private static b d;
    Context a;
    long b = 259200000;
    private Handler c = new Handler() {
        public final void handleMessage(Message message) {
            b.a(b.this);
        }
    };

    private b() {
    }

    public final void a(Context context) {
        this.a = context;
    }

    public static b a() {
        if (d == null) {
            synchronized (b.class) {
                if (d == null) {
                    d = new b();
                }
            }
        }
        return d;
    }

    public final void b() {
        if (this.a == null) {
            g.d("RoverController", "Context is null");
        } else if (c()) {
            Object b2 = r.b(this.a, a.f, 0L);
            long longValue = b2 instanceof Long ? ((Long) b2).longValue() : 1;
            long currentTimeMillis = System.currentTimeMillis();
            int i = a.e;
            com.mintegral.msdk.d.b.a();
            a b3 = com.mintegral.msdk.d.b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (b3 != null && b3.ac() > 0) {
                i = (int) b3.ac();
            }
            if (currentTimeMillis - longValue > ((long) i) && longValue != 1) {
                this.c.sendEmptyMessageDelayed(0, 30000);
            }
        }
    }

    private boolean c() {
        boolean z = false;
        if (this.a != null) {
            try {
                long j = this.a.getPackageManager().getPackageInfo(this.a.getPackageName(), 0).lastUpdateTime;
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - j > this.b) {
                    z = true;
                }
                g.a("", "currentTime=" + currentTimeMillis + ",lastUpdateTime:" + j);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return z;
    }

    static /* synthetic */ void a(b bVar) {
        r.a(bVar.a, a.f, Long.valueOf(System.currentTimeMillis()));
        new g(bVar.a, (byte) 0).a(a.a, new l(), new h() {
            public final void a(String str) {
            }

            public final void a(RoverCampaignUnit roverCampaignUnit) {
                ArrayList<CampaignEx> arrayList = roverCampaignUnit.ads;
                if (arrayList != null) {
                    for (CampaignEx next : arrayList) {
                        if (next != null) {
                            new c(next, b.this.a);
                        }
                    }
                }
            }
        });
    }
}
