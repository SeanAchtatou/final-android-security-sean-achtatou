package com.mintegral.msdk.rover;

import com.helpshift.support.search.storage.TableSearchToken;
import com.ironsource.sdk.constants.LocationConst;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: RoverReportData */
public final class e {
    private String a;
    private int b;
    private int c;
    private int d;
    private String e;
    private String f;

    public final void a(String str) {
        this.a = str;
    }

    public final void a(int i) {
        this.b = i;
    }

    public final void b(int i) {
        this.c = i;
    }

    public final void c(int i) {
        this.d = i;
    }

    public final void b(String str) {
        this.e = str;
    }

    public final void c(String str) {
        this.f = str;
    }

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("url", this.a);
            jSONObject.put("type", this.b);
            jSONObject.put(LocationConst.TIME, this.c);
            jSONObject.put("code", this.d);
            jSONObject.put("header", this.e);
            jSONObject.put("exception", this.f);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }

    public final String toString() {
        return "" + "url=" + this.a + TableSearchToken.COMMA_SEP + "type=" + this.b + TableSearchToken.COMMA_SEP + "time=" + this.c + TableSearchToken.COMMA_SEP + "code=" + this.d + TableSearchToken.COMMA_SEP + "header=" + this.e + TableSearchToken.COMMA_SEP + "exception=" + this.f;
    }
}
