package com.mintegral.msdk.rover;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.helpshift.support.constants.FaqsColumns;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.common.net.a;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.d.b;
import com.mintegral.msdk.out.MTGConfiguration;
import com.tapjoy.TapjoyConstants;
import org.json.JSONObject;

/* compiled from: RoverRequest */
public final class g extends a {
    private g(Context context) {
        super(context, 0);
    }

    public g(Context context, byte b) {
        this(context);
    }

    /* access modifiers changed from: protected */
    public final void a(l lVar) {
        lVar.a(TapjoyConstants.TJC_PLATFORM, "1");
        lVar.a(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, Build.VERSION.RELEASE);
        lVar.a(CampaignEx.JSON_KEY_PACKAGE_NAME, c.q(this.b));
        lVar.a("app_version_name", c.l(this.b));
        StringBuilder sb = new StringBuilder();
        sb.append(c.k(this.b));
        lVar.a("app_version_code", sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append(c.i(this.b));
        lVar.a("orientation", sb2.toString());
        lVar.a("model", c.c());
        lVar.a("brand", c.e());
        lVar.a("gaid", "");
        lVar.a("gaid2", c.l());
        lVar.a(Constants.RequestParameters.NETWORK_MNC, c.b());
        lVar.a(Constants.RequestParameters.NETWORK_MCC, c.a());
        int s = c.s(this.b);
        lVar.a("network_type", String.valueOf(s));
        lVar.a("network_str", c.a(this.b, s));
        lVar.a(FaqsColumns.LANGUAGE, c.h(this.b));
        lVar.a(TapjoyConstants.TJC_DEVICE_TIMEZONE, c.h());
        lVar.a("useragent", c.f());
        lVar.a("sdk_version", MTGConfiguration.SDK_VERSION);
        lVar.a("gp_version", c.t(this.b));
        lVar.a("screen_size", c.n(this.b) + "x" + c.o(this.b));
        StringBuilder sb3 = new StringBuilder();
        sb3.append(com.mintegral.msdk.base.controller.a.d().j());
        sb3.append(com.mintegral.msdk.base.controller.a.d().k());
        lVar.a("sign", CommonMD5.getMD5(sb3.toString()));
        lVar.a("app_id", com.mintegral.msdk.base.controller.a.d().j());
        b.a();
        com.mintegral.msdk.d.a b = b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b != null) {
            JSONObject jSONObject = new JSONObject();
            try {
                if (b.as() == 1) {
                    if (c.c(this.b) != null) {
                        jSONObject.put("imei", c.c(this.b));
                    }
                    if (c.j(this.b) != null) {
                        jSONObject.put("mac", c.j(this.b));
                    }
                }
                if (b.au() == 1 && c.f(this.b) != null) {
                    jSONObject.put(TapjoyConstants.TJC_ANDROID_ID, c.f(this.b));
                }
                if (!TextUtils.isEmpty(jSONObject.toString())) {
                    String b2 = com.mintegral.msdk.base.utils.a.b(jSONObject.toString());
                    if (!TextUtils.isEmpty(b2)) {
                        lVar.a("dvi", b2);
                    } else {
                        lVar.a("dvi", "");
                    }
                } else {
                    lVar.a("dvi", "");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            lVar.a("dvi", "");
        }
    }
}
