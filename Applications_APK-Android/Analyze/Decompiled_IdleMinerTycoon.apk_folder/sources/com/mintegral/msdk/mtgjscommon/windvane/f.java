package com.mintegral.msdk.mtgjscommon.windvane;

import android.content.Context;
import java.util.HashMap;

/* compiled from: WindVaneApiManager */
public final class f {
    private static HashMap<String, Class> a = new HashMap<>();
    private Context b;
    private WindVaneWebView c;

    public f(Context context, WindVaneWebView windVaneWebView) {
        this.b = context;
        this.c = windVaneWebView;
        a(MvBridge.class);
        try {
            a(Class.forName("com.mintegral.msdk.offerwall.jscommon.OfferWall"));
        } catch (ClassNotFoundException unused) {
        }
        try {
            a(Class.forName("com.mintegral.msdk.interstitial.jscommon.interstitial"));
        } catch (ClassNotFoundException unused2) {
        }
        try {
            a(Class.forName("com.mintegral.msdk.video.js.bridge.RewardJs"));
        } catch (ClassNotFoundException unused3) {
        }
        try {
            a(Class.forName("com.mintegral.msdk.video.js.bridge.VideoBridge"));
        } catch (ClassNotFoundException unused4) {
        }
        try {
            a(Class.forName("com.mintegral.msdk.mtgjscommon.authority.jscommon.PrivateAuthorityJSBridge"));
        } catch (ClassNotFoundException unused5) {
        }
        try {
            a(Class.forName("com.mintegral.msdk.interactiveads.jscommon.Interactive"));
        } catch (ClassNotFoundException unused6) {
        }
        try {
            a(Class.forName("com.mintegral.msdk.mtgjscommon.mraid.MraidJSBridge"));
        } catch (ClassNotFoundException unused7) {
        }
        try {
            a(Class.forName("com.mintegral.msdk.mtgbanner.common.bridge.BannerJSPlugin"));
        } catch (ClassNotFoundException unused8) {
        }
    }

    public final void a(Context context) {
        this.b = context;
    }

    private static Object a(String str, WindVaneWebView windVaneWebView, Context context) {
        Class cls = a.get(str);
        if (cls == null) {
            return null;
        }
        try {
            if (!i.class.isAssignableFrom(cls)) {
                return null;
            }
            i iVar = (i) cls.newInstance();
            iVar.initialize(context, windVaneWebView);
            return iVar;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void a(Class cls) {
        if (a == null) {
            a = new HashMap<>();
        }
        a.put(cls.getSimpleName(), cls);
    }

    public final Object a(String str) {
        if (a == null) {
            a = new HashMap<>();
        }
        return a(str, this.c, this.b);
    }
}
