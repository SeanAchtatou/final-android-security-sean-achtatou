package com.mintegral.msdk.mtgjscommon.mraid;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgjscommon.mraid.a;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.mtgjscommon.windvane.a;
import com.mintegral.msdk.mtgjscommon.windvane.i;
import org.json.JSONObject;

public class MraidJSBridge extends i {
    private b a;

    public void initialize(Context context, WindVaneWebView windVaneWebView) {
        super.initialize(context, windVaneWebView);
        try {
            if (context instanceof b) {
                this.a = (b) context;
            } else if (windVaneWebView.getObject() != null && (windVaneWebView.getObject() instanceof b)) {
                this.a = (b) windVaneWebView.getObject();
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    public void open(Object obj, String str) {
        if (obj instanceof a) {
            a unused = a.C0064a.a;
            a.a(((com.mintegral.msdk.mtgjscommon.windvane.a) obj).a, "open");
        }
        try {
            String optString = new JSONObject(str).optString("url");
            g.d("MraidJSBridge", "MRAID Open " + optString);
            if (this.a != null && !TextUtils.isEmpty(optString)) {
                this.a.open(optString);
            }
        } catch (Throwable th) {
            g.c("MraidJSBridge", "MRAID Open", th);
        }
    }

    public void close(Object obj, String str) {
        if (obj instanceof com.mintegral.msdk.mtgjscommon.windvane.a) {
            a unused = a.C0064a.a;
            a.a(((com.mintegral.msdk.mtgjscommon.windvane.a) obj).a, "close");
        }
        try {
            g.d("MraidJSBridge", "MRAID close");
            if (this.a != null) {
                this.a.close();
            }
        } catch (Throwable th) {
            g.c("MraidJSBridge", "MRAID close", th);
        }
    }

    public void unload(Object obj, String str) {
        if (obj instanceof com.mintegral.msdk.mtgjscommon.windvane.a) {
            a unused = a.C0064a.a;
            a.a(((com.mintegral.msdk.mtgjscommon.windvane.a) obj).a, "unload");
        }
        try {
            g.d("MraidJSBridge", "MRAID unload");
            if (this.a != null) {
                this.a.unload();
            }
        } catch (Throwable th) {
            g.c("MraidJSBridge", "MRAID unload", th);
        }
    }

    public void useCustomClose(Object obj, String str) {
        if (obj instanceof com.mintegral.msdk.mtgjscommon.windvane.a) {
            a unused = a.C0064a.a;
            a.a(((com.mintegral.msdk.mtgjscommon.windvane.a) obj).a, "useCustomClose");
        }
        try {
            String optString = new JSONObject(str).optString("shouldUseCustomClose");
            g.d("MraidJSBridge", "MRAID useCustomClose " + optString);
            if (!TextUtils.isEmpty(optString) && this.a != null) {
                this.a.useCustomClose(optString.toLowerCase().equals("true"));
            }
        } catch (Throwable th) {
            g.c("MraidJSBridge", "MRAID useCustomClose", th);
        }
    }

    public void expand(Object obj, String str) {
        if (obj instanceof com.mintegral.msdk.mtgjscommon.windvane.a) {
            a unused = a.C0064a.a;
            a.a(((com.mintegral.msdk.mtgjscommon.windvane.a) obj).a, "expand");
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("url");
            String optString2 = jSONObject.optString("shouldUseCustomClose");
            g.d("MraidJSBridge", "MRAID expand " + optString + " " + optString2);
            if (!TextUtils.isEmpty(optString) && !TextUtils.isEmpty(optString2) && this.a != null) {
                this.a.expand(optString, optString2.toLowerCase().equals("true"));
            }
        } catch (Throwable th) {
            g.c("MraidJSBridge", "MRAID expand", th);
        }
    }

    public void setOrientationProperties(Object obj, String str) {
        if (obj instanceof com.mintegral.msdk.mtgjscommon.windvane.a) {
            a unused = a.C0064a.a;
            a.a(((com.mintegral.msdk.mtgjscommon.windvane.a) obj).a, "setOrientationProperties");
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("allowOrientationChange");
            String optString2 = jSONObject.optString("forceOrientation");
            g.d("MraidJSBridge", "MRAID setOrientationProperties");
            if (!TextUtils.isEmpty(optString) && !TextUtils.isEmpty(optString2) && this.a != null) {
                optString.toLowerCase().equals("true");
                String lowerCase = optString2.toLowerCase();
                int hashCode = lowerCase.hashCode();
                if (hashCode != 729267099) {
                    if (hashCode == 1430647483) {
                        lowerCase.equals("landscape");
                    }
                } else if (!lowerCase.equals("portrait")) {
                }
            }
        } catch (Throwable th) {
            g.c("MraidJSBridge", "MRAID setOrientationProperties", th);
        }
    }
}
