package com.mintegral.msdk.mtgjscommon.windvane;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgjscommon.base.b;

/* compiled from: WindVaneWebViewClient */
public final class l extends b {
    public static boolean b = true;
    protected String a = null;
    private int c = 0;
    private c d;

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        this.a = str;
        if (this.d != null) {
            this.d.a();
        }
    }

    public final WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        WebResourceResponse a2 = a(str);
        if (a2 == null) {
            return super.shouldInterceptRequest(webView, str);
        }
        g.b("WindVaneWebViewClient", "find WebResourceResponse url is " + str);
        return a2;
    }

    private static WebResourceResponse a(String str) {
        try {
            if (TextUtils.isEmpty(str) || !j.d(str)) {
                return null;
            }
            g.b("WindVaneWebViewClient", "is image " + str);
            Bitmap a2 = com.mintegral.msdk.base.common.c.b.a(a.d().h()).a(str);
            g.b("WindVaneWebViewClient", "find image from cache " + str);
            if (a2 == null || a2.isRecycled()) {
                return null;
            }
            return new WebResourceResponse(j.e(str), "utf-8", com.mintegral.msdk.base.common.c.a.a(a2));
        } catch (Throwable unused) {
            return null;
        }
    }
}
