package com.mintegral.msdk.mtgjscommon.mraid;

/* compiled from: IMraidJSBridge */
public interface b {
    void close();

    void expand(String str, boolean z);

    void open(String str);

    void unload();

    void useCustomClose(boolean z);
}
