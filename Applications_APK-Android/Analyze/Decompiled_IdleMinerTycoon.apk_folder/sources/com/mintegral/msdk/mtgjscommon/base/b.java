package com.mintegral.msdk.mtgjscommon.base;

import android.net.http.SslError;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.mintegral.msdk.mtgjscommon.windvane.c;

/* compiled from: BaseWebViewClient */
public class b extends WebViewClient {
    private a a;
    private c b;

    public final void a(a aVar) {
        this.a = aVar;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (this.a != null && this.a.a(str)) {
            return true;
        }
        if (this.b != null) {
            this.b.b();
        }
        return super.shouldOverrideUrlLoading(webView, str);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
        if (this.b != null) {
            this.b.a(webView, i, str, str2);
        }
    }

    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        super.onReceivedSslError(webView, sslErrorHandler, sslError);
        if (this.b != null) {
            this.b.a(webView, sslErrorHandler, sslError);
        }
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (this.b != null) {
            this.b.a(webView, str);
        }
    }

    public final void a(c cVar) {
        this.b = cVar;
    }
}
