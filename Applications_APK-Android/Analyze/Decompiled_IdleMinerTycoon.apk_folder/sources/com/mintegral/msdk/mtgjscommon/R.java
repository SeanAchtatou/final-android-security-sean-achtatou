package com.mintegral.msdk.mtgjscommon;

public final class R {
    private R() {
    }

    public static final class id {
        public static final int linescroll = 2131230939;
        public static final int mintegral_jscommon_checkBox = 2131230966;
        public static final int mintegral_jscommon_okbutton = 2131230967;
        public static final int mintegral_jscommon_webcontent = 2131230968;
        public static final int progressBar1 = 2131231045;
        public static final int textView = 2131231146;

        private id() {
        }
    }

    public static final class layout {
        public static final int loading_alert = 2131427440;
        public static final int mintegral_jscommon_authoritylayout = 2131427445;

        private layout() {
        }
    }
}
