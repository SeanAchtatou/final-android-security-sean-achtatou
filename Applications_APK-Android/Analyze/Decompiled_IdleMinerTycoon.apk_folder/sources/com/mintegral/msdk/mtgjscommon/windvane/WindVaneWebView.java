package com.mintegral.msdk.mtgjscommon.windvane;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.mintegral.msdk.mtgjscommon.base.BaseWebView;

public class WindVaneWebView extends BaseWebView {
    protected k b;
    protected b c;
    protected f d;
    private Object e;
    private String f;
    private c g;

    public WindVaneWebView(Context context) {
        super(context);
    }

    public WindVaneWebView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public WindVaneWebView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setJsBridge(b bVar) {
        this.c = bVar;
        bVar.a(this);
    }

    public b getJsBridge() {
        return this.c;
    }

    public Object getJsObject(String str) {
        if (this.d == null) {
            return null;
        }
        return this.d.a(str);
    }

    public void registerWindVanePlugin(Class cls) {
        if (this.d != null) {
            f.a(cls);
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        getSettings().setSavePassword(false);
        WebSettings settings = getSettings();
        settings.setUserAgentString(getSettings().getUserAgentString() + " WindVane/3.0.2");
        if (this.b == null) {
            this.b = new k(this);
        }
        setWebViewChromeClient(this.b);
        this.mWebViewClient = new l();
        setWebViewClient(this.mWebViewClient);
        if (this.c == null) {
            this.c = new h(this.a);
            setJsBridge(this.c);
        }
        this.d = new f(this.a, this);
    }

    public void setApiManagerContext(Context context) {
        if (this.d != null) {
            this.d.a(context);
        }
    }

    public void setWebViewChromeClient(k kVar) {
        this.b = kVar;
        setWebChromeClient(kVar);
    }

    public void setObject(Object obj) {
        this.e = obj;
    }

    public Object getObject() {
        return this.e;
    }

    public void setWebViewListener(c cVar) {
        this.g = cVar;
        if (this.b != null) {
            this.b.a(cVar);
        }
        if (this.mWebViewClient != null) {
            this.mWebViewClient.a(cVar);
        }
    }

    public c getWebViewListener() {
        return this.g;
    }

    public void release() {
        try {
            setVisibility(8);
            removeAllViews();
            setDownloadListener(null);
            if (b()) {
                destroy();
            } else {
                new Handler().postDelayed(new Runnable() {
                    public final void run() {
                        WindVaneWebView.this.destroy();
                    }
                }, 5000);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void clearWebView() {
        loadUrl("about:blank");
    }

    public void setWebViewTransparent() {
        super.setTransparent();
    }

    public String getCampaignId() {
        return this.f;
    }

    public void setCampaignId(String str) {
        this.f = str;
    }

    private boolean b() {
        PackageInfo packageInfo;
        try {
            if (Build.VERSION.SDK_INT >= 26) {
                packageInfo = WebView.getCurrentWebViewPackage();
            } else {
                packageInfo = getContext().getPackageManager().getPackageInfo("com.google.android.webview", 1);
            }
            if (packageInfo == null || TextUtils.isEmpty(packageInfo.versionName) || !packageInfo.versionName.equals("77.0.3865.92")) {
                return true;
            }
            return false;
        } catch (Exception unused) {
        }
        return true;
    }
}
