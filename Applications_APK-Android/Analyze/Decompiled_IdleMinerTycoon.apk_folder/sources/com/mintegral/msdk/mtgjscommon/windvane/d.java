package com.mintegral.msdk.mtgjscommon.windvane;

import android.text.TextUtils;

/* compiled from: MVCallJs */
public final class d {
    private static d a = new d();

    private d() {
    }

    public static d a() {
        return a;
    }

    public static void a(Object obj, String str, String str2) {
        String str3;
        if (obj instanceof a) {
            a aVar = (a) obj;
            if (TextUtils.isEmpty(str2)) {
                str3 = String.format("javascript:window.MvBridge.fireEvent('%s', '');", str);
            } else {
                str3 = String.format("javascript:window.MvBridge.fireEvent('%s','%s');", str, j.c(str2));
            }
            if (aVar.a != null) {
                try {
                    aVar.a.loadUrl(str3);
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        }
    }

    public static void a(Object obj, String str) {
        String str2;
        if (obj instanceof a) {
            a aVar = (a) obj;
            if (TextUtils.isEmpty(str)) {
                str2 = String.format("javascript:window.OfferWall.onSuccess(%s,'');", aVar.g);
            } else {
                str2 = String.format("javascript:window.OfferWall.onSuccess(%s,'%s');", aVar.g, j.c(str));
            }
            if (aVar.a != null) {
                try {
                    aVar.a.loadUrl(str2);
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        }
    }

    public static void b(Object obj, String str) {
        if (obj instanceof a) {
            a aVar = (a) obj;
            if (TextUtils.isEmpty(str)) {
                String.format("javascript:window.MvBridge.onFailure(%s,'');", aVar.g);
            } else {
                str = j.c(str);
            }
            String format = String.format("javascript:window.MvBridge.onFailure(%s,'%s');", aVar.g, str);
            if (aVar.a != null) {
                try {
                    aVar.a.loadUrl(format);
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        }
    }
}
