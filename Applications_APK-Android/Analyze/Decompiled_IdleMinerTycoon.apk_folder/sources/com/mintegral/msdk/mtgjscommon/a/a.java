package com.mintegral.msdk.mtgjscommon.a;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: Hack */
public final class a {
    private static C0062a a;

    /* renamed from: com.mintegral.msdk.mtgjscommon.a.a$a  reason: collision with other inner class name */
    /* compiled from: Hack */
    public interface C0062a {
        boolean a();
    }

    /* compiled from: Hack */
    public static abstract class b {

        /* renamed from: com.mintegral.msdk.mtgjscommon.a.a$b$a  reason: collision with other inner class name */
        /* compiled from: Hack */
        public static class C0063a extends Throwable {
            private Class<?> a;
            private String b;

            public C0063a(Exception exc) {
                super(exc);
            }

            public final String toString() {
                if (getCause() == null) {
                    return super.toString();
                }
                return getClass().getName() + ": " + getCause();
            }

            public final void a(Class<?> cls) {
                this.a = cls;
            }

            public final void a(String str) {
                this.b = str;
            }
        }
    }

    /* compiled from: Hack */
    public static class d {
        protected final Method a;

        public final Object a(Object obj, Object... objArr) throws IllegalArgumentException, InvocationTargetException {
            try {
                return this.a.invoke(obj, objArr);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        }

        d(Class<?> cls, String str, Class<?>[] clsArr) throws b.C0063a {
            Method method = null;
            if (cls == null) {
                this.a = null;
                return;
            }
            try {
                Method declaredMethod = cls.getDeclaredMethod(str, clsArr);
                try {
                    declaredMethod.setAccessible(true);
                    this.a = declaredMethod;
                } catch (NoSuchMethodException e) {
                    NoSuchMethodException noSuchMethodException = e;
                    method = declaredMethod;
                    e = noSuchMethodException;
                    try {
                        b.C0063a aVar = new b.C0063a(e);
                        aVar.a(cls);
                        aVar.a(str);
                        a.b(aVar);
                        this.a = method;
                    } catch (Throwable th) {
                        th = th;
                        this.a = method;
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    method = declaredMethod;
                    this.a = method;
                    throw th;
                }
            } catch (NoSuchMethodException e2) {
                e = e2;
                b.C0063a aVar2 = new b.C0063a(e);
                aVar2.a(cls);
                aVar2.a(str);
                a.b(aVar2);
                this.a = method;
            }
        }
    }

    /* compiled from: Hack */
    public static class c<C> {
        protected Class<C> a;

        public final d a(String str, Class<?>... clsArr) throws b.C0063a {
            return new d(this.a, str, clsArr);
        }

        public c(Class<C> cls) {
            this.a = cls;
        }
    }

    public static <T> c<T> a(ClassLoader classLoader, String str) throws b.C0063a {
        try {
            return new c<>(classLoader.loadClass(str));
        } catch (Exception e) {
            b(new b.C0063a(e));
            return new c<>(null);
        }
    }

    /* access modifiers changed from: private */
    public static void b(b.C0063a aVar) throws b.C0063a {
        if (a == null || !a.a()) {
            throw aVar;
        }
    }
}
