package com.mintegral.msdk.mtgbid.common;

/* compiled from: BidRequestParams */
public abstract class b {
    private String a;
    private String b;

    public b(String str) {
        this.a = str;
    }

    public b(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public String getmUnitId() {
        return this.a;
    }

    public void setmUnitId(String str) {
        this.a = str;
    }

    public String getmFloorPrice() {
        return this.b;
    }

    public void setmFloorPrice(String str) {
        this.b = str;
    }
}
