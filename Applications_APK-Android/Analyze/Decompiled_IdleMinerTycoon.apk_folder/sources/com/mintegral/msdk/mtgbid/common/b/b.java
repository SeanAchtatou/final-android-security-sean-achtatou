package com.mintegral.msdk.mtgbid.common.b;

import android.support.v4.app.NotificationCompat;
import com.mintegral.msdk.base.common.net.a.c;
import com.mintegral.msdk.mtgbid.common.BidResponsedEx;
import org.json.JSONObject;

/* compiled from: BidResponseHandler */
public abstract class b extends c {
    private String a;

    public abstract void a(BidResponsedEx bidResponsedEx);

    public abstract void b(String str);

    public final /* synthetic */ void a(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        if (200 == jSONObject.optInt("status")) {
            BidResponsedEx parseBidResponsedEx = BidResponsedEx.parseBidResponsedEx(jSONObject.optJSONObject("data"), this.a);
            if (parseBidResponsedEx != null) {
                a(parseBidResponsedEx);
            } else {
                b(jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
            }
        } else {
            b(jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
        }
    }

    public b(String str) {
        this.a = str;
    }

    public final void a(String str) {
        b(str);
    }
}
