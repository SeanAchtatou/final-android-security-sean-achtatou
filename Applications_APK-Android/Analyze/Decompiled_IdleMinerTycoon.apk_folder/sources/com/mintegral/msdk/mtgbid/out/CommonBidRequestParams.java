package com.mintegral.msdk.mtgbid.out;

import com.mintegral.msdk.mtgbid.common.b;

public class CommonBidRequestParams extends b {
    public CommonBidRequestParams(String str) {
        super(str);
    }

    public CommonBidRequestParams(String str, String str2) {
        super(str, str2);
    }
}
