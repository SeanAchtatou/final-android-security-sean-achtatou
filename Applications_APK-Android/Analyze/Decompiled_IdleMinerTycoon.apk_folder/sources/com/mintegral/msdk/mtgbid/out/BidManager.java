package com.mintegral.msdk.mtgbid.out;

import android.content.Context;
import com.facebook.appevents.AppEventsConstants;
import com.mintegral.msdk.mtgbid.common.a;
import com.mintegral.msdk.mtgbid.common.a.b;

public class BidManager {
    private b a;
    private BidListennning b;

    public BidManager(String str) {
        this(str, AppEventsConstants.EVENT_PARAM_VALUE_NO);
    }

    public BidManager(String str, String str2) {
        this.a = new b(str, str2);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public <T extends CommonBidRequestParams> BidManager(T t) {
        this(t == null ? "" : t.getmUnitId(), t == null ? "" : t.getmFloorPrice());
        if (t instanceof BannerBidRequestParams) {
            BannerBidRequestParams bannerBidRequestParams = (BannerBidRequestParams) t;
            this.a.a((long) bannerBidRequestParams.getHeight());
            this.a.b((long) bannerBidRequestParams.getWeigh());
            this.a.a();
        }
    }

    public void bid() {
        if (this.a != null) {
            this.a.b();
        } else if (this.b != null) {
            this.b.onFailed("you need init the class :BidManager");
        }
    }

    public static String getBuyerUid(Context context) {
        return a.a(context);
    }

    public void setBidListener(BidListennning bidListennning) {
        this.b = bidListennning;
        if (this.a != null) {
            this.a.a(bidListennning);
        }
    }
}
