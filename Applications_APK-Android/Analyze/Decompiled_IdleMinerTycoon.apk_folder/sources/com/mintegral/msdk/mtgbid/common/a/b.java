package com.mintegral.msdk.mtgbid.common.a;

import android.content.Context;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.a.c;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.mtgbid.common.BidResponsedEx;
import com.mintegral.msdk.mtgbid.out.BidListennning;
import com.mintegral.msdk.mtgbid.out.BidResponsed;
import java.lang.reflect.Method;

/* compiled from: RequesManager */
public final class b {
    /* access modifiers changed from: private */
    public String a;
    private String b;
    /* access modifiers changed from: private */
    public Context c;
    private BidListennning d;
    /* access modifiers changed from: private */
    public BidResponsedEx e;
    private int f;
    /* access modifiers changed from: private */
    public boolean g = false;
    private long h;
    private long i;

    public final void a() {
        this.f = 296;
    }

    public final void a(long j) {
        this.h = j;
    }

    public final void b(long j) {
        this.i = j;
    }

    public b(String str, String str2) {
        this.a = str;
        this.b = str2;
        this.c = a.d().h();
    }

    public final void b() {
        try {
            if (!this.g) {
                this.g = true;
                if (this.c == null) {
                    a("context is null");
                }
                com.mintegral.msdk.mtgbid.common.b.a aVar = new com.mintegral.msdk.mtgbid.common.b.a(this.c);
                l lVar = new l();
                lVar.a("app_id", a.d().j());
                lVar.a(MIntegralConstans.PROPERTIES_UNIT_ID, this.a);
                lVar.a("bid_floor", this.b);
                lVar.a("exclude_ids", k.b(0));
                lVar.a("install_ids", k.a(0));
                lVar.a("display_info", c.a(this.a, ""));
                if (this.f == 296) {
                    if (this.h <= 0 || this.i <= 0) {
                        a("banner bid required param is missing or error");
                        return;
                    }
                    lVar.a("unit_size", this.i + "x" + this.h);
                    Method method = Class.forName("com.mintegral.msdk.mtgbanner.common.util.BannerUtils").getMethod("getCloseIds", String.class);
                    if (method.invoke(null, this.a) instanceof String) {
                        lVar.a("close_id", method.invoke(null, this.a).toString());
                    }
                }
                aVar.a(com.mintegral.msdk.base.common.a.j, lVar, new a(this.a) {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.mintegral.msdk.mtgbid.common.a.b.a(com.mintegral.msdk.mtgbid.common.a.b, com.mintegral.msdk.mtgbid.out.BidResponsed):void
                     arg types: [com.mintegral.msdk.mtgbid.common.a.b, com.mintegral.msdk.mtgbid.common.BidResponsedEx]
                     candidates:
                      com.mintegral.msdk.mtgbid.common.a.b.a(com.mintegral.msdk.mtgbid.common.a.b, com.mintegral.msdk.mtgbid.common.BidResponsedEx):com.mintegral.msdk.mtgbid.common.BidResponsedEx
                      com.mintegral.msdk.mtgbid.common.a.b.a(com.mintegral.msdk.mtgbid.common.a.b, java.lang.String):void
                      com.mintegral.msdk.mtgbid.common.a.b.a(com.mintegral.msdk.mtgbid.common.a.b, com.mintegral.msdk.mtgbid.out.BidResponsed):void */
                    public final void a(BidResponsedEx bidResponsedEx) {
                        boolean unused = b.this.g = false;
                        BidResponsedEx unused2 = b.this.e = bidResponsedEx;
                        com.mintegral.msdk.mtgbid.common.c.a.a(b.this.c, b.this.a, bidResponsedEx.getBidId());
                        b.a(b.this, (BidResponsed) bidResponsedEx);
                    }

                    public final void b(String str) {
                        boolean unused = b.this.g = false;
                        com.mintegral.msdk.mtgbid.common.c.a.b(b.this.c, b.this.a, str);
                        b.this.a(str);
                    }
                });
                return;
            }
            a("current unit is biding");
        } catch (Exception unused) {
            a("banner module is miss");
        } catch (Throwable th) {
            a(th.getMessage());
        }
    }

    public final void a(BidListennning bidListennning) {
        this.d = bidListennning;
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        if (this.d != null) {
            this.d.onFailed(str);
        }
    }

    static /* synthetic */ void a(b bVar, BidResponsed bidResponsed) {
        if (bVar.d != null) {
            bVar.d.onSuccessed(bidResponsed);
        }
    }
}
