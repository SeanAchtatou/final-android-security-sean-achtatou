package com.mintegral.msdk.mtgbid.out;

public class BannerBidRequestParams extends CommonBidRequestParams {
    private int a;
    private int b;

    public BannerBidRequestParams(String str, int i, int i2) {
        super(str);
        this.a = i2;
        this.b = i;
    }

    public BannerBidRequestParams(String str, String str2, int i, int i2) {
        super(str, str2);
        this.a = i2;
        this.b = i;
    }

    public int getHeight() {
        return this.a;
    }

    public void setHeight(int i) {
        this.a = i;
    }

    public int getWeigh() {
        return this.b;
    }

    public void setWeigh(int i) {
        this.b = i;
    }
}
