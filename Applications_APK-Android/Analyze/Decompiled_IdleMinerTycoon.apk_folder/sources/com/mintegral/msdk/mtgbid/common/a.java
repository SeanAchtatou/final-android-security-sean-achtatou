package com.mintegral.msdk.mtgbid.common;

import android.content.Context;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.d.b;

/* compiled from: BidCommon */
public final class a {
    public static String a(Context context) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            b.a();
            com.mintegral.msdk.d.a b = b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (b == null) {
                b.a();
                b = b.b();
            }
            stringBuffer.append(c.k());
            stringBuffer.append("|");
            String str = "";
            if (b.au() == 1) {
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_ANDROID_ID)) {
                    str = c.f(context);
                }
            }
            stringBuffer.append(str);
            stringBuffer.append("|");
            String str2 = "";
            if (b.as() == 1) {
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITYIMEIMAC)) {
                    str2 = c.c(context);
                }
            }
            stringBuffer.append(str2);
            stringBuffer.append("|");
            stringBuffer.append(com.mintegral.msdk.base.a.a.a.a().a("sys_id"));
            stringBuffer.append("|");
            stringBuffer.append(com.mintegral.msdk.base.a.a.a.a().a("bkup_id"));
            stringBuffer.append("|");
            stringBuffer.append(c.e());
            stringBuffer.append("|");
            stringBuffer.append(c.d());
            stringBuffer.append("|");
            stringBuffer.append(c.s(context));
            stringBuffer.append("|");
            stringBuffer.append("MAL_10.1.51|");
            stringBuffer.append(c.n(context));
            stringBuffer.append("x");
            stringBuffer.append(c.o(context));
            stringBuffer.append("|");
            stringBuffer.append(c.f());
            stringBuffer.append("|");
            stringBuffer.append(k.a(20));
            stringBuffer.append("|");
            stringBuffer.append(k.b(20));
            stringBuffer.append("|");
            stringBuffer.append(System.currentTimeMillis());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return com.mintegral.msdk.base.utils.a.b(stringBuffer.toString());
    }
}
