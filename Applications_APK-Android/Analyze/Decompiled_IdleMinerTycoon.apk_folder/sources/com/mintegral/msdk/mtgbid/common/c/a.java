package com.mintegral.msdk.mtgbid.common.c;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.common.e.c;
import com.mintegral.msdk.base.common.e.d.b;
import com.mintegral.msdk.base.utils.g;

/* compiled from: BidReport */
public class a {
    /* access modifiers changed from: private */
    public static final String a = "com.mintegral.msdk.mtgbid.common.c.a";

    public static void a(Context context, String str, String str2) {
        if (context != null && !TextUtils.isEmpty(str)) {
            try {
                com.mintegral.msdk.base.common.e.d.a aVar = new com.mintegral.msdk.base.common.e.d.a(context);
                aVar.c();
                aVar.b(com.mintegral.msdk.base.common.a.f, c.a("key=2000064&result=1&network_type=" + com.mintegral.msdk.base.utils.c.s(context) + "&bidid=" + str2, context, str), new b() {
                    public final void b(String str) {
                        g.d(a.a, str);
                    }

                    public final void a(String str) {
                        g.d(a.a, str);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                g.d(a, e.getMessage());
            }
        }
    }

    public static void b(Context context, String str, String str2) {
        if (context != null && !TextUtils.isEmpty(str)) {
            try {
                com.mintegral.msdk.base.common.e.d.a aVar = new com.mintegral.msdk.base.common.e.d.a(context);
                aVar.c();
                aVar.b(com.mintegral.msdk.base.common.a.f, c.a("key=2000064&result=2&network_type=" + com.mintegral.msdk.base.utils.c.s(context) + "&reason=" + str2, context, str), new b() {
                    public final void b(String str) {
                        g.d(a.a, str);
                    }

                    public final void a(String str) {
                        g.d(a.a, str);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                g.d(a, e.getMessage());
            }
        }
    }
}
