package com.mintegral.msdk.c;

import com.mintegral.msdk.out.AdMobClickListener;
import java.util.Map;

/* compiled from: PreloadController */
public class a {
    public static final String a = "a";

    public static void a() {
    }

    public static void b() {
    }

    public static void a(Map<String, Object> map, int i, AdMobClickListener adMobClickListener) {
        try {
            Class<?> cls = Class.forName("com.mintegral.msdk.mtgnative.f.a");
            Object newInstance = cls.newInstance();
            cls.getMethod("preload", Map.class, Integer.TYPE, AdMobClickListener.class).invoke(newInstance, map, Integer.valueOf(i), adMobClickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(Map<String, Object> map) {
        try {
            Class<?> cls = Class.forName("com.mintegral.msdk.appwall.service.HandlerProvider");
            Object newInstance = cls.newInstance();
            cls.getMethod("preload", Map.class).invoke(newInstance, map);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
