package com.mintegral.msdk.c.a;

import com.mintegral.msdk.out.PreloadListener;
import java.lang.ref.WeakReference;

/* compiled from: PreloadListenerEx */
public final class a implements PreloadListener {
    WeakReference<PreloadListener> a;
    private boolean b = false;
    private int c = 0;

    public final boolean a() {
        return this.b;
    }

    public final void b() {
        this.b = true;
    }

    public a(PreloadListener preloadListener) {
        if (preloadListener != null) {
            this.a = new WeakReference<>(preloadListener);
        }
    }

    public final void onPreloadSucceed() {
        if (this.a != null && this.a.get() != null) {
            this.a.get().onPreloadSucceed();
        }
    }

    public final void onPreloadFaild(String str) {
        if (this.a != null && this.a.get() != null) {
            this.a.get().onPreloadFaild(str);
        }
    }
}
