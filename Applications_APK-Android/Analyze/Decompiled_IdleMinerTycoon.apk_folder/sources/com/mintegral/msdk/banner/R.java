package com.mintegral.msdk.banner;

public final class R {
    private R() {
    }

    public static final class drawable {
        public static final int mintegral_banner_close = 2131165394;

        private drawable() {
        }
    }

    public static final class string {
        public static final int app_name = 2131689511;

        private string() {
        }
    }
}
