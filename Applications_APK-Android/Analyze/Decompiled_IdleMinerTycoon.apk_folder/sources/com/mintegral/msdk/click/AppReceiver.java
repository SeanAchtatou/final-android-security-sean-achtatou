package com.mintegral.msdk.click;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.mintegral.msdk.base.b.g;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.r;
import com.mintegral.msdk.mtgbid.common.BidResponsedEx;

public class AppReceiver extends BroadcastReceiver {
    private static final String a = "AppReceiver";

    public void onReceive(Context context, Intent intent) {
        if (TextUtils.equals(intent.getAction(), "android.intent.action.PACKAGE_ADDED")) {
            String schemeSpecificPart = intent.getData().getSchemeSpecificPart();
            CampaignEx l = g.b(i.a(context)).l(schemeSpecificPart);
            if (l != null) {
                a.b(l, context, "install");
            }
            try {
                r.a(context, schemeSpecificPart, schemeSpecificPart + "downloadType", schemeSpecificPart + "linkType", schemeSpecificPart + "rid", schemeSpecificPart + BidResponsedEx.KEY_CID, schemeSpecificPart + "isDowning", schemeSpecificPart + "process");
            } catch (Exception e) {
                com.mintegral.msdk.base.utils.g.a(a, e.getMessage());
            }
        }
    }
}
