package com.mintegral.msdk.click;

import com.helpshift.support.search.storage.TableSearchToken;
import com.mintegral.msdk.d.b;

/* compiled from: JavaHttpSpider */
public class e {
    private static final String a = "e";
    private com.mintegral.msdk.d.a b;
    private String c;
    private boolean d = true;
    private final int e = 3145728;
    private a f;

    public final void a() {
        this.d = false;
    }

    public e() {
        b.a();
        this.b = b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (this.b == null) {
            b.a();
            this.b = b.b();
        }
    }

    /* compiled from: JavaHttpSpider */
    public static class a {
        public String a;
        public String b;
        public String c;
        public String d;
        public int e;
        public int f;
        public String g;
        public String h;

        public final String toString() {
            return "http响应头：...\n" + "statusCode=" + this.f + TableSearchToken.COMMA_SEP + "location=" + this.a + TableSearchToken.COMMA_SEP + "contentType=" + this.b + TableSearchToken.COMMA_SEP + "contentLength=" + this.e + TableSearchToken.COMMA_SEP + "contentEncoding=" + this.c + TableSearchToken.COMMA_SEP + "referer=" + this.d;
        }

        public final String a() {
            return "statusCode=" + this.f + TableSearchToken.COMMA_SEP + "location=" + this.a + TableSearchToken.COMMA_SEP + "contentType=" + this.b + TableSearchToken.COMMA_SEP + "contentLength=" + this.e + TableSearchToken.COMMA_SEP + "contentEncoding=" + this.c + TableSearchToken.COMMA_SEP + "referer=" + this.d;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0130, code lost:
        r4 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0132, code lost:
        r4 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0133, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x014c, code lost:
        r1.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0152, code lost:
        r0.disconnect();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x0126 */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0130 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:7:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x014c  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0152  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.mintegral.msdk.click.e.a a(java.lang.String r4, boolean r5, boolean r6, com.mintegral.msdk.base.entity.CampaignEx r7) {
        /*
            r3 = this;
            boolean r0 = android.webkit.URLUtil.isNetworkUrl(r4)
            r1 = 0
            if (r0 != 0) goto L_0x0008
            return r1
        L_0x0008:
            java.lang.String r0 = " "
            java.lang.String r2 = "%20"
            java.lang.String r4 = r4.replace(r0, r2)
            android.webkit.URLUtil.isHttpsUrl(r4)
            java.lang.String r0 = com.mintegral.msdk.click.e.a
            com.mintegral.msdk.base.utils.g.b(r0, r4)
            com.mintegral.msdk.click.e$a r0 = new com.mintegral.msdk.click.e$a
            r0.<init>()
            r3.f = r0
            java.net.URL r0 = new java.net.URL     // Catch:{ Throwable -> 0x0138 }
            r0.<init>(r4)     // Catch:{ Throwable -> 0x0138 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Throwable -> 0x0138 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Throwable -> 0x0138 }
            java.lang.String r1 = "GET"
            r0.setRequestMethod(r1)     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            if (r5 != 0) goto L_0x0033
            if (r6 == 0) goto L_0x0035
        L_0x0033:
            if (r7 != 0) goto L_0x003e
        L_0x0035:
            java.lang.String r1 = "User-Agent"
            java.lang.String r2 = com.mintegral.msdk.base.utils.c.f()     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            r0.setRequestProperty(r1, r2)     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
        L_0x003e:
            r1 = 1
            if (r5 == 0) goto L_0x0052
            if (r7 == 0) goto L_0x0052
            int r5 = r7.getcUA()     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            if (r5 != r1) goto L_0x0052
            java.lang.String r5 = "User-Agent"
            java.lang.String r2 = com.mintegral.msdk.base.utils.c.f()     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            r0.setRequestProperty(r5, r2)     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
        L_0x0052:
            if (r6 == 0) goto L_0x0065
            if (r7 == 0) goto L_0x0065
            int r5 = r7.getImpUA()     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            if (r5 != r1) goto L_0x0065
            java.lang.String r5 = "User-Agent"
            java.lang.String r6 = com.mintegral.msdk.base.utils.c.f()     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            r0.setRequestProperty(r5, r6)     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
        L_0x0065:
            java.lang.String r5 = "Accept-Encoding"
            java.lang.String r6 = "gzip"
            r0.setRequestProperty(r5, r6)     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            com.mintegral.msdk.d.a r5 = r3.b     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            boolean r5 = r5.aD()     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            if (r5 == 0) goto L_0x0083
            java.lang.String r5 = r3.c     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            boolean r5 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            if (r5 != 0) goto L_0x0083
            java.lang.String r5 = "referer"
            java.lang.String r6 = r3.c     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            r0.setRequestProperty(r5, r6)     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
        L_0x0083:
            r5 = 60000(0xea60, float:8.4078E-41)
            r0.setConnectTimeout(r5)     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            r0.setReadTimeout(r5)     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            r5 = 0
            r0.setInstanceFollowRedirects(r5)     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            r0.connect()     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            com.mintegral.msdk.click.e$a r5 = r3.f     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            java.lang.String r6 = "Location"
            java.lang.String r6 = r0.getHeaderField(r6)     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            r5.a = r6     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            com.mintegral.msdk.click.e$a r5 = r3.f     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            java.lang.String r6 = "Referer"
            java.lang.String r6 = r0.getHeaderField(r6)     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            r5.d = r6     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            com.mintegral.msdk.click.e$a r5 = r3.f     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            int r6 = r0.getResponseCode()     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            r5.f = r6     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            com.mintegral.msdk.click.e$a r5 = r3.f     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            java.lang.String r6 = r0.getContentType()     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            r5.b = r6     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            com.mintegral.msdk.click.e$a r5 = r3.f     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            int r6 = r0.getContentLength()     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            r5.e = r6     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            com.mintegral.msdk.click.e$a r5 = r3.f     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            java.lang.String r6 = r0.getContentEncoding()     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            r5.c = r6     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            java.lang.String r5 = com.mintegral.msdk.click.e.a     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            com.mintegral.msdk.click.e$a r6 = r3.f     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            com.mintegral.msdk.base.utils.g.b(r5, r6)     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            java.lang.String r5 = "gzip"
            com.mintegral.msdk.click.e$a r6 = r3.f     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            java.lang.String r6 = r6.c     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            boolean r5 = r5.equalsIgnoreCase(r6)     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            com.mintegral.msdk.click.e$a r6 = r3.f     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            int r6 = r6.f     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            r7 = 200(0xc8, float:2.8E-43)
            if (r6 != r7) goto L_0x0126
            boolean r6 = r3.d     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            if (r6 == 0) goto L_0x0126
            com.mintegral.msdk.click.e$a r6 = r3.f     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            int r6 = r6.e     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            if (r6 <= 0) goto L_0x0126
            com.mintegral.msdk.click.e$a r6 = r3.f     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            int r6 = r6.e     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            r7 = 3145728(0x300000, float:4.408104E-39)
            if (r6 >= r7) goto L_0x0126
            boolean r6 = android.text.TextUtils.isEmpty(r4)     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            if (r6 != 0) goto L_0x0126
            java.lang.String r6 = ".apk"
            boolean r6 = r4.endsWith(r6)     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            if (r6 != 0) goto L_0x0126
            java.io.InputStream r6 = r0.getInputStream()     // Catch:{ Throwable -> 0x0126, all -> 0x0130 }
            java.lang.String r5 = r3.a(r6, r5)     // Catch:{ Throwable -> 0x0126, all -> 0x0130 }
            boolean r6 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Throwable -> 0x0126, all -> 0x0130 }
            if (r6 != 0) goto L_0x0126
            byte[] r6 = r5.getBytes()     // Catch:{ Throwable -> 0x0126, all -> 0x0130 }
            if (r6 == 0) goto L_0x0126
            int r1 = r6.length     // Catch:{ Throwable -> 0x0126, all -> 0x0130 }
            if (r1 <= 0) goto L_0x0126
            int r6 = r6.length     // Catch:{ Throwable -> 0x0126, all -> 0x0130 }
            if (r6 >= r7) goto L_0x0126
            com.mintegral.msdk.click.e$a r6 = r3.f     // Catch:{ Throwable -> 0x0126, all -> 0x0130 }
            java.lang.String r5 = r5.trim()     // Catch:{ Throwable -> 0x0126, all -> 0x0130 }
            r6.g = r5     // Catch:{ Throwable -> 0x0126, all -> 0x0130 }
        L_0x0126:
            r3.c = r4     // Catch:{ Throwable -> 0x0132, all -> 0x0130 }
            if (r0 == 0) goto L_0x012d
            r0.disconnect()
        L_0x012d:
            com.mintegral.msdk.click.e$a r4 = r3.f
            return r4
        L_0x0130:
            r4 = move-exception
            goto L_0x0150
        L_0x0132:
            r4 = move-exception
            r1 = r0
            goto L_0x0139
        L_0x0135:
            r4 = move-exception
            r0 = r1
            goto L_0x0150
        L_0x0138:
            r4 = move-exception
        L_0x0139:
            com.mintegral.msdk.click.e$a r5 = r3.f     // Catch:{ all -> 0x0135 }
            java.lang.String r4 = r4.getMessage()     // Catch:{ all -> 0x0135 }
            r5.h = r4     // Catch:{ all -> 0x0135 }
            java.lang.String r4 = "http jump"
            java.lang.String r5 = "connecting"
            com.mintegral.msdk.base.utils.g.c(r4, r5)     // Catch:{ all -> 0x0135 }
            com.mintegral.msdk.click.e$a r4 = r3.f     // Catch:{ all -> 0x0135 }
            if (r1 == 0) goto L_0x014f
            r1.disconnect()
        L_0x014f:
            return r4
        L_0x0150:
            if (r0 == 0) goto L_0x0155
            r0.disconnect()
        L_0x0155:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.click.e.a(java.lang.String, boolean, boolean, com.mintegral.msdk.base.entity.CampaignEx):com.mintegral.msdk.click.e$a");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0034 A[Catch:{ all -> 0x000f }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0048 A[SYNTHETIC, Splitter:B:25:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0057 A[SYNTHETIC, Splitter:B:32:0x0057] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(java.io.InputStream r4, boolean r5) {
        /*
            r3 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r1 = 0
            if (r5 == 0) goto L_0x0013
            java.util.zip.GZIPInputStream r5 = new java.util.zip.GZIPInputStream     // Catch:{ Exception -> 0x0011 }
            r5.<init>(r4)     // Catch:{ Exception -> 0x0011 }
            r4 = r5
            goto L_0x0013
        L_0x000f:
            r4 = move-exception
            goto L_0x0055
        L_0x0011:
            r4 = move-exception
            goto L_0x0030
        L_0x0013:
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0011 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0011 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0011 }
            r5.<init>(r2)     // Catch:{ Exception -> 0x0011 }
        L_0x001d:
            java.lang.String r4 = r5.readLine()     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            if (r4 == 0) goto L_0x0027
            r0.append(r4)     // Catch:{ Exception -> 0x002e, all -> 0x002b }
            goto L_0x001d
        L_0x0027:
            r5.close()     // Catch:{ Exception -> 0x004c }
            goto L_0x0050
        L_0x002b:
            r4 = move-exception
            r1 = r5
            goto L_0x0055
        L_0x002e:
            r4 = move-exception
            r1 = r5
        L_0x0030:
            com.mintegral.msdk.click.e$a r5 = r3.f     // Catch:{ all -> 0x000f }
            if (r5 != 0) goto L_0x0043
            com.mintegral.msdk.click.e$a r5 = new com.mintegral.msdk.click.e$a     // Catch:{ all -> 0x000f }
            r5.<init>()     // Catch:{ all -> 0x000f }
            r3.f = r5     // Catch:{ all -> 0x000f }
            com.mintegral.msdk.click.e$a r5 = r3.f     // Catch:{ all -> 0x000f }
            java.lang.String r2 = r4.getMessage()     // Catch:{ all -> 0x000f }
            r5.h = r2     // Catch:{ all -> 0x000f }
        L_0x0043:
            r4.printStackTrace()     // Catch:{ all -> 0x000f }
            if (r1 == 0) goto L_0x0050
            r1.close()     // Catch:{ Exception -> 0x004c }
            goto L_0x0050
        L_0x004c:
            r4 = move-exception
            r4.printStackTrace()
        L_0x0050:
            java.lang.String r4 = r0.toString()
            return r4
        L_0x0055:
            if (r1 == 0) goto L_0x005f
            r1.close()     // Catch:{ Exception -> 0x005b }
            goto L_0x005f
        L_0x005b:
            r5 = move-exception
            r5.printStackTrace()
        L_0x005f:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.click.e.a(java.io.InputStream, boolean):java.lang.String");
    }
}
