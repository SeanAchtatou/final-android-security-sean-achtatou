package com.mintegral.msdk.click;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.widget.Toast;
import com.google.android.gms.drive.DriveFile;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.d;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.common.e.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.j;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.click.CommonJumpLoader;
import com.mintegral.msdk.out.AppWallTrackingListener;
import com.mintegral.msdk.out.Campaign;
import com.mintegral.msdk.out.Frame;
import com.mintegral.msdk.out.LoadingActivity;
import com.mintegral.msdk.out.NativeListener;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/* compiled from: CommonClickControl */
public class a {
    public static boolean a = false;
    public static Set<String> b = new HashSet();
    public static final String c = a.class.getName();
    public static Map<String, Long> d = new HashMap();
    public static Map<String, Long> e = new HashMap();
    public static Set<String> f = new HashSet();
    static Handler g = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 0:
                    Object obj = message.obj;
                    if (obj != null && (obj instanceof Context)) {
                        a.a((Context) obj);
                        return;
                    }
                    return;
                case 1:
                    a.f();
                    return;
                default:
                    return;
            }
        }
    };
    private String h = "CommonClickControl";
    /* access modifiers changed from: private */
    public String i;
    private long j;
    private long k;
    /* access modifiers changed from: private */
    public i l = null;
    /* access modifiers changed from: private */
    public Context m = null;
    private CommonJumpLoader n;
    private HashMap<String, CommonJumpLoader> o;
    private AppWallTrackingListener p;
    /* access modifiers changed from: private */
    public NativeListener.NativeTrackingListener q = null;
    private b r;
    /* access modifiers changed from: private */
    public boolean s;
    private com.mintegral.msdk.d.a t;
    private boolean u;
    /* access modifiers changed from: private */
    public boolean v = false;
    private boolean w = true;
    private Handler x = new Handler() {
        public final void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 0:
                    if (a.this.q != null) {
                        a.this.q.onDownloadStart(null);
                        return;
                    }
                    return;
                case 1:
                    if (a.this.q != null) {
                        a.this.q.onDownloadProgress(message.arg1);
                        return;
                    }
                    return;
                case 2:
                    if (a.this.q != null) {
                        a.this.q.onDownloadFinish((Campaign) message.obj);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean y;

    public a(Context context, String str) {
        com.mintegral.msdk.d.b.a();
        this.t = com.mintegral.msdk.d.b.b(str);
        if (this.t == null) {
            com.mintegral.msdk.d.b.a();
            this.t = com.mintegral.msdk.d.b.b();
        }
        this.u = this.t.aA();
        this.m = context.getApplicationContext();
        this.i = str;
        if (this.l == null) {
            this.l = i.a(this.m);
        }
        this.r = new b(this.m);
        this.o = new HashMap<>();
    }

    public final void a(String str) {
        this.i = str;
    }

    public final void a(NativeListener.NativeTrackingListener nativeTrackingListener) {
        this.q = nativeTrackingListener;
    }

    public final void a() {
        this.w = false;
    }

    public final void a(AppWallTrackingListener appWallTrackingListener) {
        this.p = appWallTrackingListener;
    }

    public final void b() {
        Set<Map.Entry<String, CommonJumpLoader>> entrySet;
        CommonJumpLoader commonJumpLoader;
        try {
            if (!(this.o == null || (entrySet = this.o.entrySet()) == null || entrySet.size() <= 0)) {
                for (Map.Entry next : entrySet) {
                    if (!(next == null || (commonJumpLoader = (CommonJumpLoader) next.getValue()) == null)) {
                        commonJumpLoader.b();
                    }
                }
            }
            this.q = null;
        } catch (Exception unused) {
        }
    }

    public final void a(CampaignEx campaignEx, NativeListener.NativeAdListener nativeAdListener) {
        if (!(nativeAdListener == null || campaignEx == null)) {
            nativeAdListener.onAdClick(campaignEx);
        }
        g.b("Mintegral SDK M", "clickStart");
        b(campaignEx);
    }

    public final void c() {
        if (this.n != null && this.n.a()) {
            this.n.b();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x001c A[Catch:{ Exception -> 0x000a }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean c(com.mintegral.msdk.base.entity.CampaignEx r6) {
        /*
            if (r6 == 0) goto L_0x000d
            r0 = 2
            int r1 = r6.getLinkType()     // Catch:{ Exception -> 0x000a }
            if (r0 == r1) goto L_0x0014
            goto L_0x000d
        L_0x000a:
            r6 = move-exception
            goto L_0x0094
        L_0x000d:
            r0 = 3
            int r1 = r6.getLinkType()     // Catch:{ Exception -> 0x000a }
            if (r0 != r1) goto L_0x009b
        L_0x0014:
            java.lang.String r0 = r6.getId()     // Catch:{ Exception -> 0x000a }
            java.util.Map<java.lang.String, java.lang.Long> r1 = com.mintegral.msdk.click.a.e     // Catch:{ Exception -> 0x000a }
            if (r1 == 0) goto L_0x009b
            java.util.Map<java.lang.String, java.lang.Long> r1 = com.mintegral.msdk.click.a.e     // Catch:{ Exception -> 0x000a }
            boolean r1 = r1.containsKey(r0)     // Catch:{ Exception -> 0x000a }
            if (r1 == 0) goto L_0x0063
            java.util.Map<java.lang.String, java.lang.Long> r1 = com.mintegral.msdk.click.a.e     // Catch:{ Exception -> 0x000a }
            java.lang.Object r0 = r1.get(r0)     // Catch:{ Exception -> 0x000a }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ Exception -> 0x000a }
            if (r0 == 0) goto L_0x0063
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x000a }
            long r3 = r0.longValue()     // Catch:{ Exception -> 0x000a }
            int r5 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r5 > 0) goto L_0x0046
            java.util.Set<java.lang.String> r3 = com.mintegral.msdk.click.a.f     // Catch:{ Exception -> 0x000a }
            java.lang.String r4 = r6.getId()     // Catch:{ Exception -> 0x000a }
            boolean r3 = r3.contains(r4)     // Catch:{ Exception -> 0x000a }
            if (r3 == 0) goto L_0x0063
        L_0x0046:
            java.lang.String r6 = "Mintegral SDK M"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x000a }
            java.lang.String r4 = "点击时间未超过coit "
            r3.<init>(r4)     // Catch:{ Exception -> 0x000a }
            r3.append(r1)     // Catch:{ Exception -> 0x000a }
            java.lang.String r1 = "|"
            r3.append(r1)     // Catch:{ Exception -> 0x000a }
            r3.append(r0)     // Catch:{ Exception -> 0x000a }
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x000a }
            com.mintegral.msdk.base.utils.g.b(r6, r0)     // Catch:{ Exception -> 0x000a }
            r6 = 0
            return r6
        L_0x0063:
            java.lang.String r0 = "Mintegral SDK M"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x000a }
            java.lang.String r2 = "未发现有点击或点击超时保存点击时间 interval = "
            r1.<init>(r2)     // Catch:{ Exception -> 0x000a }
            int r2 = r6.getClickTimeOutInterval()     // Catch:{ Exception -> 0x000a }
            r1.append(r2)     // Catch:{ Exception -> 0x000a }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x000a }
            com.mintegral.msdk.base.utils.g.b(r0, r1)     // Catch:{ Exception -> 0x000a }
            int r0 = r6.getClickTimeOutInterval()     // Catch:{ Exception -> 0x000a }
            int r0 = r0 * 1000
            java.util.Map<java.lang.String, java.lang.Long> r1 = com.mintegral.msdk.click.a.e     // Catch:{ Exception -> 0x000a }
            java.lang.String r6 = r6.getId()     // Catch:{ Exception -> 0x000a }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x000a }
            long r4 = (long) r0     // Catch:{ Exception -> 0x000a }
            long r2 = r2 + r4
            java.lang.Long r0 = java.lang.Long.valueOf(r2)     // Catch:{ Exception -> 0x000a }
            r1.put(r6, r0)     // Catch:{ Exception -> 0x000a }
            goto L_0x009b
        L_0x0094:
            boolean r0 = com.mintegral.msdk.MIntegralConstans.DEBUG
            if (r0 == 0) goto L_0x009b
            r6.printStackTrace()
        L_0x009b:
            r6 = 1
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.click.a.c(com.mintegral.msdk.base.entity.CampaignEx):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, int, boolean, java.lang.Boolean):void
     arg types: [com.mintegral.msdk.base.entity.CampaignEx, int, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(int, java.lang.String, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.out.NativeListener$NativeTrackingListener):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, boolean):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, int):boolean
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, int, boolean, java.lang.Boolean):void */
    public final void a(CampaignEx campaignEx) {
        try {
            if (c(campaignEx)) {
                d a2 = d.a(this.l);
                a2.c();
                CommonJumpLoader.JumpLoaderResult b2 = a2.b(campaignEx.getId(), this.i);
                if (b2 != null) {
                    if (b2.getNoticeurl() != null) {
                        b2.setNoticeurl(null);
                    }
                    campaignEx.setJumpResult(b2);
                    String str = this.i;
                    campaignEx.getTtc_type();
                    a2.a(campaignEx, str);
                }
                if (b.b(this.m, campaignEx.getPackageName())) {
                    g.a("Mintegral SDK M", campaignEx.getPackageName() + " is intalled.");
                    return;
                }
                a(campaignEx, campaignEx.getTtc_type(), false, (Boolean) false);
            }
        } catch (Throwable th) {
            g.c("Mintegral SDK M", th.getMessage(), th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    private void a(CampaignEx campaignEx, String str) {
        if (!TextUtils.isEmpty(str)) {
            a(this.m, campaignEx, this.i, str, true, false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, int, boolean, java.lang.Boolean):void
     arg types: [com.mintegral.msdk.base.entity.CampaignEx, int, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(int, java.lang.String, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.out.NativeListener$NativeTrackingListener):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, boolean):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, int):boolean
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, int, boolean, java.lang.Boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx, boolean):void
     arg types: [com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, android.content.Context, java.lang.String):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, boolean):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, boolean, com.mintegral.msdk.out.Campaign):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean):boolean
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0167 A[Catch:{ Throwable -> 0x0300 }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0207 A[Catch:{ Throwable -> 0x0300 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(com.mintegral.msdk.base.entity.CampaignEx r15) {
        /*
            r14 = this;
            r0 = 0
            r14.s = r0     // Catch:{ Throwable -> 0x0300 }
            boolean r1 = c(r15)     // Catch:{ Throwable -> 0x0300 }
            if (r1 != 0) goto L_0x000a
            return
        L_0x000a:
            java.lang.String r1 = r15.getNoticeUrl()     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.base.b.i r2 = r14.l     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.base.b.d r2 = com.mintegral.msdk.base.b.d.a(r2)     // Catch:{ Throwable -> 0x0300 }
            r2.c()     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r3 = r15.getId()     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r4 = r14.i     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult r3 = r2.b(r3, r4)     // Catch:{ Throwable -> 0x0300 }
            if (r3 == 0) goto L_0x0038
            java.lang.String r4 = r3.getNoticeurl()     // Catch:{ Throwable -> 0x0300 }
            if (r4 == 0) goto L_0x002d
            r4 = 0
            r3.setNoticeurl(r4)     // Catch:{ Throwable -> 0x0300 }
        L_0x002d:
            r15.setJumpResult(r3)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r4 = r14.i     // Catch:{ Throwable -> 0x0300 }
            r15.getTtc_type()     // Catch:{ Throwable -> 0x0300 }
            r2.a(r15, r4)     // Catch:{ Throwable -> 0x0300 }
        L_0x0038:
            boolean r2 = com.mintegral.msdk.base.utils.k.a(r15)     // Catch:{ Throwable -> 0x0300 }
            if (r2 == 0) goto L_0x0070
            android.content.Context r2 = r14.m     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r4 = r15.getDeepLinkURL()     // Catch:{ Throwable -> 0x0300 }
            boolean r2 = com.mintegral.msdk.click.b.c(r2, r4)     // Catch:{ Throwable -> 0x0300 }
            if (r2 == 0) goto L_0x005f
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0300 }
            r0.<init>()     // Catch:{ Throwable -> 0x0300 }
            r0.append(r1)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r1 = "&opdptype=1"
            r0.append(r1)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0300 }
            r14.a(r15, r0)     // Catch:{ Throwable -> 0x0300 }
            return
        L_0x005f:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0300 }
            r2.<init>()     // Catch:{ Throwable -> 0x0300 }
            r2.append(r1)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r1 = "&opdptype=0"
            r2.append(r1)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r1 = r2.toString()     // Catch:{ Throwable -> 0x0300 }
        L_0x0070:
            r14.a(r15, r1)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r1 = r15.getGhId()     // Catch:{ Throwable -> 0x0300 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Throwable -> 0x0300 }
            r2 = 3
            r4 = 2
            r5 = 4
            r6 = 1
            if (r1 != 0) goto L_0x00ce
            android.content.Context r1 = r14.m     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r3 = r15.getGhId()     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r7 = r15.getGhPath()     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r15 = r15.getBindId()     // Catch:{ Throwable -> 0x0300 }
            if (r1 == 0) goto L_0x00cd
            boolean r8 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Throwable -> 0x0300 }
            if (r8 != 0) goto L_0x00cd
            boolean r8 = android.text.TextUtils.isEmpty(r15)     // Catch:{ Throwable -> 0x0300 }
            if (r8 != 0) goto L_0x00cd
            java.lang.String r8 = ""
            boolean r9 = android.text.TextUtils.isEmpty(r7)     // Catch:{ Throwable -> 0x0300 }
            if (r9 != 0) goto L_0x00a6
            goto L_0x00a7
        L_0x00a6:
            r7 = r8
        L_0x00a7:
            android.content.ContentResolver r8 = r1.getContentResolver()     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r1 = "DFK/J75/JaEXWFfXYZPTHkPUHkPTWr2wWgf3LBPUYF2wWgSBYbHuH75BWFetJkPULcJDnkQ/L+SBYFJBDkT="
            java.lang.String r1 = com.mintegral.msdk.base.utils.a.c(r1)     // Catch:{ Throwable -> 0x0300 }
            android.net.Uri r9 = android.net.Uri.parse(r1)     // Catch:{ Throwable -> 0x0300 }
            r10 = 0
            r11 = 0
            java.lang.String[] r12 = new java.lang.String[r5]     // Catch:{ Throwable -> 0x0300 }
            r12[r0] = r15     // Catch:{ Throwable -> 0x0300 }
            r12[r6] = r3     // Catch:{ Throwable -> 0x0300 }
            r12[r4] = r7     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r15 = "0"
            r12[r2] = r15     // Catch:{ Throwable -> 0x0300 }
            r13 = 0
            android.database.Cursor r15 = r8.query(r9, r10, r11, r12, r13)     // Catch:{ Throwable -> 0x0300 }
            if (r15 == 0) goto L_0x00cd
            r15.close()     // Catch:{ Throwable -> 0x0300 }
        L_0x00cd:
            return
        L_0x00ce:
            android.content.Context r1 = r14.m     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r7 = r15.getPackageName()     // Catch:{ Throwable -> 0x0300 }
            boolean r1 = com.mintegral.msdk.click.b.b(r1, r7)     // Catch:{ Throwable -> 0x0300 }
            if (r1 == 0) goto L_0x0128
            android.content.Context r0 = r14.m     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r1 = r15.getPackageName()     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.click.b.d(r0, r1)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r0 = "Mintegral SDK M"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0300 }
            r1.<init>()     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r2 = r15.getPackageName()     // Catch:{ Throwable -> 0x0300 }
            r1.append(r2)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r2 = " is intalled."
            r1.append(r2)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.base.utils.g.a(r0, r1)     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r0 = r14.q     // Catch:{ Throwable -> 0x0300 }
            if (r0 == 0) goto L_0x010a
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r0 = r14.q     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r1 = r15.getClickURL()     // Catch:{ Throwable -> 0x0300 }
            r0.onStartRedirection(r15, r1)     // Catch:{ Throwable -> 0x0300 }
        L_0x010a:
            int r0 = r15.getTtc_type()     // Catch:{ Throwable -> 0x0300 }
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)     // Catch:{ Throwable -> 0x0300 }
            r14.a(r15, r0, r6, r1)     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r0 = r14.q     // Catch:{ Throwable -> 0x0300 }
            if (r0 == 0) goto L_0x0127
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r0 = r14.q     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r1 = r15.getClickURL()     // Catch:{ Throwable -> 0x0300 }
            r0.onFinishRedirection(r15, r1)     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r15 = r14.q     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.base.utils.j.a(r15)     // Catch:{ Throwable -> 0x0300 }
        L_0x0127:
            return
        L_0x0128:
            int r1 = r15.getLinkType()     // Catch:{ Throwable -> 0x0300 }
            int r7 = r14.e()     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r8 = "Mintegral SDK M"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r10 = "======302跳转前linkType:"
            r9.<init>(r10)     // Catch:{ Throwable -> 0x0300 }
            r9.append(r1)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r10 = " openType:"
            r9.append(r10)     // Catch:{ Throwable -> 0x0300 }
            r9.append(r7)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r10 = "======landingType："
            r9.append(r10)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r10 = r15.getLandingType()     // Catch:{ Throwable -> 0x0300 }
            r9.append(r10)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.base.utils.g.b(r8, r9)     // Catch:{ Throwable -> 0x0300 }
            r8 = 9
            r9 = 8
            if (r1 == r9) goto L_0x0164
            if (r1 == r8) goto L_0x0164
            if (r1 != r5) goto L_0x0162
            goto L_0x0164
        L_0x0162:
            r10 = 0
            goto L_0x0165
        L_0x0164:
            r10 = 1
        L_0x0165:
            if (r10 == 0) goto L_0x0207
            java.lang.String r2 = r15.getClickURL()     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r10 = r14.q     // Catch:{ Throwable -> 0x0300 }
            if (r10 == 0) goto L_0x0174
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r10 = r14.q     // Catch:{ Throwable -> 0x0300 }
            r10.onStartRedirection(r15, r2)     // Catch:{ Throwable -> 0x0300 }
        L_0x0174:
            boolean r10 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Throwable -> 0x0300 }
            if (r10 == 0) goto L_0x019f
            java.lang.String r0 = "Mintegral SDK M"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r5 = "linketype="
            r4.<init>(r5)     // Catch:{ Throwable -> 0x0300 }
            r4.append(r1)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r1 = " clickurl 为空"
            r4.append(r1)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r1 = r4.toString()     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.base.utils.g.b(r0, r1)     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r0 = r14.q     // Catch:{ Throwable -> 0x0300 }
            if (r0 == 0) goto L_0x019b
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r0 = r14.q     // Catch:{ Throwable -> 0x0300 }
            r0.onRedirectionFailed(r15, r2)     // Catch:{ Throwable -> 0x0300 }
        L_0x019b:
            r14.a(r3, r15, r6)     // Catch:{ Throwable -> 0x0300 }
            return
        L_0x019f:
            if (r1 != r9) goto L_0x01bc
            java.lang.String r1 = "Mintegral SDK M"
            java.lang.String r4 = "linketype=8 用webview 打开"
            com.mintegral.msdk.base.utils.g.b(r1, r4)     // Catch:{ Throwable -> 0x0300 }
            android.content.Context r1 = r14.m     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r4 = r14.q     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.base.utils.j.a(r1, r2, r15, r4)     // Catch:{ Throwable -> 0x0300 }
            r14.a(r3, r15, r0)     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r0 = r14.q     // Catch:{ Throwable -> 0x0300 }
            if (r0 == 0) goto L_0x01bb
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r0 = r14.q     // Catch:{ Throwable -> 0x0300 }
            r0.onFinishRedirection(r15, r2)     // Catch:{ Throwable -> 0x0300 }
        L_0x01bb:
            return
        L_0x01bc:
            if (r1 != r8) goto L_0x01d9
            java.lang.String r1 = "Mintegral SDK M"
            java.lang.String r4 = "linketype=9 用浏览器 打开"
            com.mintegral.msdk.base.utils.g.b(r1, r4)     // Catch:{ Throwable -> 0x0300 }
            android.content.Context r1 = r14.m     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r4 = r14.q     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.base.utils.j.b(r1, r2, r4)     // Catch:{ Throwable -> 0x0300 }
            r14.a(r3, r15, r0)     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r0 = r14.q     // Catch:{ Throwable -> 0x0300 }
            if (r0 == 0) goto L_0x01d8
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r0 = r14.q     // Catch:{ Throwable -> 0x0300 }
            r0.onFinishRedirection(r15, r2)     // Catch:{ Throwable -> 0x0300 }
        L_0x01d8:
            return
        L_0x01d9:
            if (r1 != r5) goto L_0x01fa
            if (r7 != r4) goto L_0x01ec
            java.lang.String r1 = "Mintegral SDK M"
            java.lang.String r4 = "linketype=4 opent=2 用webview 打开"
            com.mintegral.msdk.base.utils.g.b(r1, r4)     // Catch:{ Throwable -> 0x0300 }
            android.content.Context r1 = r14.m     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r4 = r14.q     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.base.utils.j.a(r1, r2, r15, r4)     // Catch:{ Throwable -> 0x0300 }
            goto L_0x01fa
        L_0x01ec:
            java.lang.String r1 = "Mintegral SDK M"
            java.lang.String r4 = "linketype=4 opent=不为2 用Browser 打开"
            com.mintegral.msdk.base.utils.g.b(r1, r4)     // Catch:{ Throwable -> 0x0300 }
            android.content.Context r1 = r14.m     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r4 = r14.q     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.base.utils.j.b(r1, r2, r4)     // Catch:{ Throwable -> 0x0300 }
        L_0x01fa:
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r1 = r14.q     // Catch:{ Throwable -> 0x0300 }
            if (r1 == 0) goto L_0x0203
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r1 = r14.q     // Catch:{ Throwable -> 0x0300 }
            r1.onFinishRedirection(r15, r2)     // Catch:{ Throwable -> 0x0300 }
        L_0x0203:
            r14.a(r3, r15, r0)     // Catch:{ Throwable -> 0x0300 }
            return
        L_0x0207:
            if (r1 != r4) goto L_0x0291
            java.lang.String r1 = "Mintegral SDK M"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r4 = "linktype为2 开始做302跳转"
            r2.<init>(r4)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r4 = r15.getClickURL()     // Catch:{ Throwable -> 0x0300 }
            r2.append(r4)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.base.utils.g.b(r1, r2)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r1 = r15.getClickURL()     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r2 = "market://"
            boolean r1 = r1.startsWith(r2)     // Catch:{ Throwable -> 0x0300 }
            if (r1 != 0) goto L_0x0245
            java.lang.String r1 = r15.getClickURL()     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r2 = "https://play.google.com/"
            boolean r1 = r1.startsWith(r2)     // Catch:{ Throwable -> 0x0300 }
            if (r1 == 0) goto L_0x0239
            goto L_0x0245
        L_0x0239:
            int r1 = r15.getTtc_type()     // Catch:{ Throwable -> 0x0300 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r6)     // Catch:{ Throwable -> 0x0300 }
            r14.a(r15, r1, r0, r2)     // Catch:{ Throwable -> 0x0300 }
            return
        L_0x0245:
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r1 = r14.q     // Catch:{ Throwable -> 0x0300 }
            if (r1 == 0) goto L_0x0252
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r1 = r14.q     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r2 = r15.getClickURL()     // Catch:{ Throwable -> 0x0300 }
            r1.onStartRedirection(r15, r2)     // Catch:{ Throwable -> 0x0300 }
        L_0x0252:
            android.content.Context r1 = r14.m     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r2 = r15.getClickURL()     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r4 = r14.q     // Catch:{ Throwable -> 0x0300 }
            boolean r1 = com.mintegral.msdk.base.utils.j.a.a(r1, r2, r4)     // Catch:{ Throwable -> 0x0300 }
            if (r1 != 0) goto L_0x0269
            java.lang.String r1 = r15.getClickURL()     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r2 = r14.q     // Catch:{ Throwable -> 0x0300 }
            r14.a(r7, r1, r15, r2)     // Catch:{ Throwable -> 0x0300 }
        L_0x0269:
            r14.a(r3, r15, r0)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r0 = "Mintegral SDK M"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r2 = "不用做302跳转 最终地址已经是gp了："
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r2 = r15.getClickURL()     // Catch:{ Throwable -> 0x0300 }
            r1.append(r2)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.base.utils.g.b(r0, r1)     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r0 = r14.q     // Catch:{ Throwable -> 0x0300 }
            if (r0 == 0) goto L_0x0290
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r0 = r14.q     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r1 = r15.getClickURL()     // Catch:{ Throwable -> 0x0300 }
            r0.onFinishRedirection(r15, r1)     // Catch:{ Throwable -> 0x0300 }
        L_0x0290:
            return
        L_0x0291:
            if (r1 != r2) goto L_0x02b6
            java.lang.String r1 = "Mintegral SDK M"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r3 = "linktype为3 开始做302跳转"
            r2.<init>(r3)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r3 = r15.getClickURL()     // Catch:{ Throwable -> 0x0300 }
            r2.append(r3)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.base.utils.g.b(r1, r2)     // Catch:{ Throwable -> 0x0300 }
            int r1 = r15.getTtc_type()     // Catch:{ Throwable -> 0x0300 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r6)     // Catch:{ Throwable -> 0x0300 }
            r14.a(r15, r1, r0, r2)     // Catch:{ Throwable -> 0x0300 }
            return
        L_0x02b6:
            java.lang.String r2 = r15.getClickURL()     // Catch:{ Throwable -> 0x0300 }
            boolean r4 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Throwable -> 0x0300 }
            if (r4 == 0) goto L_0x02e5
            java.lang.String r0 = "Mintegral SDK M"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r5 = "linketype="
            r4.<init>(r5)     // Catch:{ Throwable -> 0x0300 }
            r4.append(r1)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r1 = " clickurl 为空"
            r4.append(r1)     // Catch:{ Throwable -> 0x0300 }
            java.lang.String r1 = r4.toString()     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.base.utils.g.b(r0, r1)     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r0 = r14.q     // Catch:{ Throwable -> 0x0300 }
            if (r0 == 0) goto L_0x02e1
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r0 = r14.q     // Catch:{ Throwable -> 0x0300 }
            r0.onRedirectionFailed(r15, r2)     // Catch:{ Throwable -> 0x0300 }
        L_0x02e1:
            r14.a(r3, r15, r6)     // Catch:{ Throwable -> 0x0300 }
            return
        L_0x02e5:
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r1 = r14.q     // Catch:{ Throwable -> 0x0300 }
            if (r1 == 0) goto L_0x02ee
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r1 = r14.q     // Catch:{ Throwable -> 0x0300 }
            r1.onFinishRedirection(r15, r2)     // Catch:{ Throwable -> 0x0300 }
        L_0x02ee:
            java.lang.String r1 = "Mintegral SDK M"
            java.lang.String r4 = "linketyp不是23489的值 用浏览器 打开"
            com.mintegral.msdk.base.utils.g.b(r1, r4)     // Catch:{ Throwable -> 0x0300 }
            android.content.Context r1 = r14.m     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r4 = r14.q     // Catch:{ Throwable -> 0x0300 }
            com.mintegral.msdk.base.utils.j.b(r1, r2, r4)     // Catch:{ Throwable -> 0x0300 }
            r14.a(r3, r15, r0)     // Catch:{ Throwable -> 0x0300 }
            return
        L_0x0300:
            r15 = move-exception
            java.lang.String r0 = "Mintegral SDK M"
            java.lang.String r1 = r15.getMessage()
            com.mintegral.msdk.base.utils.g.c(r0, r1, r15)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.click.a.b(com.mintegral.msdk.base.entity.CampaignEx):void");
    }

    public static void a(Context context, CampaignEx campaignEx, String str, String str2, boolean z, boolean z2) {
        if (context != null) {
            new CommonJumpLoader(context.getApplicationContext(), true).a("2", str, campaignEx, new d() {
                public final void a(Object obj) {
                }

                public final void a(Object obj, String str) {
                }
            }, str2, z, z2);
        }
    }

    public static void a(Context context, CampaignEx campaignEx, String str, String[] strArr, boolean z) {
        if (context != null && campaignEx != null && !TextUtils.isEmpty(str) && strArr != null) {
            CommonJumpLoader commonJumpLoader = new CommonJumpLoader(context.getApplicationContext(), true);
            for (String a2 : strArr) {
                commonJumpLoader.a("2", str, campaignEx, new d() {
                    public final void a(Object obj) {
                    }

                    public final void a(Object obj, String str) {
                    }
                }, a2, false, z);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
     arg types: [com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, int, boolean, java.lang.Boolean]
     candidates:
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x011c A[Catch:{ Exception -> 0x017a }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0125 A[Catch:{ Exception -> 0x017a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(final com.mintegral.msdk.base.entity.CampaignEx r15, int r16, boolean r17, java.lang.Boolean r18) {
        /*
            r14 = this;
            r8 = r14
            r0 = r15
            r7 = r17
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x017a }
            r8.j = r1     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r1 = r8.q     // Catch:{ Exception -> 0x017a }
            r9 = 1
            if (r1 == 0) goto L_0x0023
            if (r7 != 0) goto L_0x0023
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r1 = r8.q     // Catch:{ Exception -> 0x017a }
            java.lang.String r2 = r15.getClickURL()     // Catch:{ Exception -> 0x017a }
            r1.onStartRedirection(r15, r2)     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r1 = r8.q     // Catch:{ Exception -> 0x017a }
            boolean r1 = r1.onInterceptDefaultLoadingDialog()     // Catch:{ Exception -> 0x017a }
            r1 = r1 ^ r9
            r10 = r1
            goto L_0x0024
        L_0x0023:
            r10 = 1
        L_0x0024:
            java.util.HashMap<java.lang.String, com.mintegral.msdk.click.CommonJumpLoader> r1 = r8.o     // Catch:{ Exception -> 0x017a }
            java.lang.String r2 = r15.getClickURL()     // Catch:{ Exception -> 0x017a }
            boolean r1 = r1.containsKey(r2)     // Catch:{ Exception -> 0x017a }
            if (r1 == 0) goto L_0x0048
            java.util.HashMap<java.lang.String, com.mintegral.msdk.click.CommonJumpLoader> r1 = r8.o     // Catch:{ Exception -> 0x017a }
            java.lang.String r2 = r15.getClickURL()     // Catch:{ Exception -> 0x017a }
            java.lang.Object r1 = r1.get(r2)     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.click.CommonJumpLoader r1 = (com.mintegral.msdk.click.CommonJumpLoader) r1     // Catch:{ Exception -> 0x017a }
            r1.b()     // Catch:{ Exception -> 0x017a }
            java.util.HashMap<java.lang.String, com.mintegral.msdk.click.CommonJumpLoader> r1 = r8.o     // Catch:{ Exception -> 0x017a }
            java.lang.String r2 = r15.getClickURL()     // Catch:{ Exception -> 0x017a }
            r1.remove(r2)     // Catch:{ Exception -> 0x017a }
        L_0x0048:
            r11 = 0
            r8.y = r11     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult r1 = r15.getJumpResult()     // Catch:{ Exception -> 0x017a }
            if (r1 == 0) goto L_0x0067
            if (r7 != 0) goto L_0x0061
            com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult r3 = r15.getJumpResult()     // Catch:{ Exception -> 0x017a }
            r4 = 1
            boolean r5 = r8.v     // Catch:{ Exception -> 0x017a }
            r1 = r14
            r2 = r15
            r6 = r18
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x017a }
        L_0x0061:
            r8.y = r9     // Catch:{ Exception -> 0x017a }
            r8.v = r11     // Catch:{ Exception -> 0x017a }
            r4 = 0
            goto L_0x0068
        L_0x0067:
            r4 = 1
        L_0x0068:
            com.mintegral.msdk.base.b.i r1 = r8.l     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.base.b.d r1 = com.mintegral.msdk.base.b.d.a(r1)     // Catch:{ Exception -> 0x017a }
            java.lang.String r2 = r15.getId()     // Catch:{ Exception -> 0x017a }
            java.lang.String r3 = r8.i     // Catch:{ Exception -> 0x017a }
            boolean r1 = r1.a(r2, r3)     // Catch:{ Exception -> 0x017a }
            if (r1 == 0) goto L_0x0080
            com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult r1 = r15.getJumpResult()     // Catch:{ Exception -> 0x017a }
            if (r1 != 0) goto L_0x0179
        L_0x0080:
            com.mintegral.msdk.base.b.i r1 = r8.l     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.base.b.d r1 = com.mintegral.msdk.base.b.d.a(r1)     // Catch:{ Exception -> 0x017a }
            r1.c()     // Catch:{ Exception -> 0x017a }
            java.lang.String r2 = r15.getId()     // Catch:{ Exception -> 0x017a }
            java.lang.String r3 = r8.i     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult r3 = r1.b(r2, r3)     // Catch:{ Exception -> 0x017a }
            if (r3 == 0) goto L_0x00ad
            if (r7 != 0) goto L_0x00ad
            r15.setJumpResult(r3)     // Catch:{ Exception -> 0x017a }
            if (r4 == 0) goto L_0x0100
            if (r7 != 0) goto L_0x00a7
            boolean r5 = r8.v     // Catch:{ Exception -> 0x017a }
            r1 = r14
            r2 = r15
            r6 = r18
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x017a }
        L_0x00a7:
            r8.y = r9     // Catch:{ Exception -> 0x017a }
            r8.v = r11     // Catch:{ Exception -> 0x017a }
        L_0x00ab:
            r4 = 0
            goto L_0x0100
        L_0x00ad:
            java.lang.String r1 = r15.getClick_mode()     // Catch:{ Exception -> 0x017a }
            java.lang.String r2 = "6"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x017a }
            if (r1 == 0) goto L_0x00f9
            java.lang.String r1 = r15.getPackageName()     // Catch:{ Exception -> 0x017a }
            boolean r1 = r1.isEmpty()     // Catch:{ Exception -> 0x017a }
            if (r1 != 0) goto L_0x00f9
            int r1 = r15.getLinkType()     // Catch:{ Exception -> 0x017a }
            r2 = 2
            if (r1 != r2) goto L_0x00f9
            if (r7 != 0) goto L_0x00f9
            android.content.Context r1 = r8.m     // Catch:{ Exception -> 0x017a }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x017a }
            java.lang.String r3 = "market://details?id="
            r2.<init>(r3)     // Catch:{ Exception -> 0x017a }
            java.lang.String r3 = r15.getPackageName()     // Catch:{ Exception -> 0x017a }
            r2.append(r3)     // Catch:{ Exception -> 0x017a }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r3 = r8.q     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.base.utils.j.a.a(r1, r2, r3)     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r1 = r8.q     // Catch:{ Exception -> 0x017a }
            if (r1 == 0) goto L_0x00f6
            if (r4 == 0) goto L_0x00f6
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r1 = r8.q     // Catch:{ Exception -> 0x017a }
            r1.onDismissLoading(r15)     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r1 = r8.q     // Catch:{ Exception -> 0x017a }
            r2 = 0
            r1.onFinishRedirection(r15, r2)     // Catch:{ Exception -> 0x017a }
        L_0x00f6:
            r8.y = r9     // Catch:{ Exception -> 0x017a }
            r4 = 0
        L_0x00f9:
            if (r7 == 0) goto L_0x0100
            r8.y = r9     // Catch:{ Exception -> 0x017a }
            r8.v = r11     // Catch:{ Exception -> 0x017a }
            goto L_0x00ab
        L_0x0100:
            java.lang.String r1 = "Mintegral SDK M"
            java.lang.String r2 = "Start 302 Redirection... "
            com.mintegral.msdk.base.utils.g.b(r1, r2)     // Catch:{ Exception -> 0x017a }
            android.os.Handler r1 = new android.os.Handler     // Catch:{ Exception -> 0x017a }
            android.os.Looper r2 = android.os.Looper.getMainLooper()     // Catch:{ Exception -> 0x017a }
            r1.<init>(r2)     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.click.a$7 r2 = new com.mintegral.msdk.click.a$7     // Catch:{ Exception -> 0x017a }
            r2.<init>(r10, r7, r15)     // Catch:{ Exception -> 0x017a }
            r1.post(r2)     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.click.CommonJumpLoader r1 = r8.n     // Catch:{ Exception -> 0x017a }
            if (r1 == 0) goto L_0x0121
            com.mintegral.msdk.click.CommonJumpLoader r1 = r8.n     // Catch:{ Exception -> 0x017a }
            r1.b()     // Catch:{ Exception -> 0x017a }
        L_0x0121:
            java.util.Set<java.lang.String> r1 = com.mintegral.msdk.click.a.f     // Catch:{ Exception -> 0x017a }
            if (r1 == 0) goto L_0x014b
            java.util.Set<java.lang.String> r1 = com.mintegral.msdk.click.a.f     // Catch:{ Exception -> 0x017a }
            java.lang.String r2 = r15.getId()     // Catch:{ Exception -> 0x017a }
            boolean r1 = r1.contains(r2)     // Catch:{ Exception -> 0x017a }
            if (r1 == 0) goto L_0x014b
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r1 = r8.q     // Catch:{ Exception -> 0x017a }
            if (r1 == 0) goto L_0x0143
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r1 = r8.q     // Catch:{ Exception -> 0x017a }
            r1.onDismissLoading(r15)     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r1 = r8.q     // Catch:{ Exception -> 0x017a }
            java.lang.String r2 = r15.getClickURL()     // Catch:{ Exception -> 0x017a }
            r1.onFinishRedirection(r15, r2)     // Catch:{ Exception -> 0x017a }
        L_0x0143:
            java.lang.String r0 = "Mintegral SDK M"
            java.lang.String r1 = "点击正在tracking"
            com.mintegral.msdk.base.utils.g.b(r0, r1)     // Catch:{ Exception -> 0x017a }
            return
        L_0x014b:
            java.util.Set<java.lang.String> r1 = com.mintegral.msdk.click.a.f     // Catch:{ Exception -> 0x017a }
            java.lang.String r2 = r15.getId()     // Catch:{ Exception -> 0x017a }
            r1.add(r2)     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.click.CommonJumpLoader r1 = new com.mintegral.msdk.click.CommonJumpLoader     // Catch:{ Exception -> 0x017a }
            android.content.Context r2 = r8.m     // Catch:{ Exception -> 0x017a }
            r1.<init>(r2, r11)     // Catch:{ Exception -> 0x017a }
            r8.n = r1     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.click.CommonJumpLoader r9 = r8.n     // Catch:{ Exception -> 0x017a }
            java.lang.String r11 = "1"
            java.lang.String r12 = r8.i     // Catch:{ Exception -> 0x017a }
            com.mintegral.msdk.click.a$8 r13 = new com.mintegral.msdk.click.a$8     // Catch:{ Exception -> 0x017a }
            r1 = r13
            r2 = r14
            r3 = r15
            r5 = r18
            r6 = r16
            r7 = r10
            r1.<init>(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x017a }
            r6 = 0
            r1 = r9
            r2 = r11
            r3 = r12
            r4 = r15
            r5 = r13
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x017a }
        L_0x0179:
            return
        L_0x017a:
            r0 = move-exception
            r0.printStackTrace()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, int, boolean, java.lang.Boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, boolean):void
     arg types: [com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, android.content.Context, java.lang.String):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx, boolean):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, boolean, com.mintegral.msdk.out.Campaign):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean):boolean
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, boolean):void */
    public final void a(Campaign campaign, String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return;
            }
            if (campaign != null) {
                CampaignEx campaignEx = null;
                if (campaign != null && (campaign instanceof CampaignEx)) {
                    campaignEx = (CampaignEx) campaign;
                }
                if (!str.startsWith("market://")) {
                    if (!str.startsWith("https://play.google.com/")) {
                        MIntegralConstans.ALLOW_APK_DOWNLOAD = com.mintegral.msdk.d.a.aV();
                        if (str.toLowerCase().endsWith(".apk") && !MIntegralConstans.ALLOW_APK_DOWNLOAD) {
                            if (campaignEx != null && !TextUtils.isEmpty(campaignEx.getPackageName())) {
                                Context context = this.m;
                                if (!j.a.a(context, "market://details?id=" + campaignEx.getPackageName(), this.q)) {
                                    try {
                                        this.x.post(new Runnable() {
                                            public final void run() {
                                                Toast.makeText(a.this.m, "Opps!Access Unavailable", 0).show();
                                            }
                                        });
                                        return;
                                    } catch (Exception unused) {
                                        g.d("Mintegral SDK M", "Opps!Access Unavailable.");
                                        return;
                                    }
                                } else if (MIntegralConstans.ALLOW_APK_DOWNLOAD) {
                                    a(campaignEx, str, true);
                                    return;
                                } else {
                                    return;
                                }
                            } else if (MIntegralConstans.ALLOW_APK_DOWNLOAD) {
                                a(campaignEx, str, true);
                                return;
                            } else {
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                }
                if (!j.a.a(this.m, str, this.q) && campaignEx != null) {
                    if (!TextUtils.isEmpty(campaignEx.getPackageName())) {
                        Context context2 = this.m;
                        j.a.a(context2, "market://details?id=" + campaignEx.getPackageName(), this.q);
                    } else if (e() == 2) {
                        j.a(this.m, campaignEx.getClickURL(), campaignEx, this.q);
                    } else {
                        j.b(this.m, campaignEx.getClickURL(), this.q);
                    }
                }
                g.b("Mintegral SDK M", "Jump to Google Play: " + str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void a(CommonJumpLoader.JumpLoaderResult jumpLoaderResult, CampaignEx campaignEx, boolean z) {
        if (this.s && campaignEx != null && jumpLoaderResult != null) {
            try {
                this.k = System.currentTimeMillis() - this.j;
                c cVar = new c();
                int s2 = com.mintegral.msdk.base.utils.c.s(this.m);
                cVar.a(s2);
                cVar.a(com.mintegral.msdk.base.utils.c.a(this.m, s2));
                cVar.j(campaignEx.getRequestIdNotice());
                cVar.d(1);
                StringBuilder sb = new StringBuilder();
                sb.append(this.k);
                cVar.i(sb.toString());
                cVar.h(campaignEx.getId());
                cVar.f(jumpLoaderResult.getType());
                if (!TextUtils.isEmpty(jumpLoaderResult.getUrl())) {
                    cVar.g(URLEncoder.encode(jumpLoaderResult.getUrl(), "utf-8"));
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append(this.j / 1000);
                cVar.c(sb2.toString());
                cVar.b(Integer.parseInt(campaignEx.getLandingType()));
                cVar.c(campaignEx.getLinkType());
                cVar.b(this.i);
                if (jumpLoaderResult != null) {
                    cVar.f(jumpLoaderResult.getType());
                    if (!TextUtils.isEmpty(jumpLoaderResult.getUrl())) {
                        cVar.g(URLEncoder.encode(jumpLoaderResult.getUrl(), "utf-8"));
                    }
                    if (this.u) {
                        cVar.e(jumpLoaderResult.getStatusCode());
                        if (!TextUtils.isEmpty(jumpLoaderResult.getHeader())) {
                            cVar.e(URLEncoder.encode(jumpLoaderResult.getHeader(), "utf-8"));
                        }
                        if (!TextUtils.isEmpty(jumpLoaderResult.getContent())) {
                            cVar.f(URLEncoder.encode(jumpLoaderResult.getContent(), "UTF-8"));
                        }
                        if (!TextUtils.isEmpty(jumpLoaderResult.getExceptionMsg())) {
                            cVar.d(URLEncoder.encode(jumpLoaderResult.getExceptionMsg(), "utf-8"));
                        }
                    }
                    if (z) {
                        this.r.a(cVar, this.i);
                        return;
                    }
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(cVar);
                    String a2 = c.a(arrayList);
                    if (s.b(a2)) {
                        new b(this.m, 0).a("click_jump_success", a2, (String) null, (Frame) null);
                    }
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    private void a(int i2, String str, CampaignEx campaignEx, NativeListener.NativeTrackingListener nativeTrackingListener) {
        try {
            if (TextUtils.isEmpty(str)) {
                return;
            }
            if (i2 == 2) {
                j.a(this.m, str, campaignEx, nativeTrackingListener);
            } else {
                j.b(this.m, str, nativeTrackingListener);
            }
        } catch (Throwable th) {
            g.c("Mintegral SDK M", th.getMessage(), th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx, boolean):void
     arg types: [com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, android.content.Context, java.lang.String):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, boolean):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, boolean, com.mintegral.msdk.out.Campaign):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean):boolean
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx, boolean):void */
    private boolean a(CampaignEx campaignEx, CommonJumpLoader.JumpLoaderResult jumpLoaderResult, boolean z, int i2) {
        boolean z2 = false;
        if (z) {
            try {
                int intValue = Integer.valueOf(campaignEx.getLandingType()).intValue();
                if (intValue == 1) {
                    j.b(this.m, jumpLoaderResult.getUrl(), this.q);
                } else if (intValue == 2) {
                    j.a(this.m, jumpLoaderResult.getUrl(), campaignEx, this.q);
                } else if (campaignEx.getPackageName() != null) {
                    Context context = this.m;
                    if (!j.a.a(context, "market://details?id=" + campaignEx.getPackageName(), this.q)) {
                        a(i2, jumpLoaderResult.getUrl(), campaignEx, this.q);
                    }
                } else {
                    a(i2, jumpLoaderResult.getUrl(), campaignEx, this.q);
                }
                z2 = true;
            } catch (Throwable th) {
                g.c("Mintegral SDK M", th.getMessage(), th);
            }
        }
        if (z2) {
            a(jumpLoaderResult, campaignEx, true);
            if (this.q != null && z) {
                this.q.onFinishRedirection(campaignEx, jumpLoaderResult.getUrl());
            }
        } else {
            a(jumpLoaderResult, campaignEx, true);
            if (this.q != null && z) {
                this.q.onRedirectionFailed(campaignEx, jumpLoaderResult.getUrl());
            }
        }
        return z2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx, boolean):void
     arg types: [com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, android.content.Context, java.lang.String):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, boolean):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, boolean, com.mintegral.msdk.out.Campaign):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean):boolean
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx, boolean):void */
    private boolean a(CampaignEx campaignEx, CommonJumpLoader.JumpLoaderResult jumpLoaderResult, boolean z) {
        boolean z2 = false;
        if (z) {
            try {
                j.b(this.m, campaignEx.getClickURL(), this.q);
                z2 = true;
            } catch (Throwable th) {
                g.c("Mintegral SDK M", th.getMessage(), th);
            }
        }
        if (z2) {
            a(jumpLoaderResult, campaignEx, true);
            if (this.q != null && z) {
                this.q.onFinishRedirection(campaignEx, jumpLoaderResult.getUrl());
            }
        } else {
            a(jumpLoaderResult, campaignEx, true);
            if (this.q != null && z) {
                this.q.onRedirectionFailed(campaignEx, jumpLoaderResult.getUrl());
            }
        }
        return z2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx, boolean):void
     arg types: [com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, android.content.Context, java.lang.String):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, boolean):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, boolean, com.mintegral.msdk.out.Campaign):void
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean):boolean
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, com.mintegral.msdk.base.entity.CampaignEx, boolean):void */
    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f1, code lost:
        if (com.mintegral.msdk.base.utils.j.a.a(r11, "market://details?id=" + r7.getPackageName(), r6.q) == false) goto L_0x00f3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.mintegral.msdk.base.entity.CampaignEx r7, com.mintegral.msdk.click.CommonJumpLoader.JumpLoaderResult r8, boolean r9, boolean r10, java.lang.Boolean r11) {
        /*
            r6 = this;
            boolean r0 = r6.w     // Catch:{ Exception -> 0x01ef }
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            boolean r0 = com.mintegral.msdk.d.a.aV()     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.MIntegralConstans.ALLOW_APK_DOWNLOAD = r0     // Catch:{ Exception -> 0x01ef }
            r0 = 1
            if (r7 == 0) goto L_0x01dd
            if (r8 != 0) goto L_0x0012
            goto L_0x01dd
        L_0x0012:
            int r1 = r6.e()     // Catch:{ Exception -> 0x01ef }
            int r2 = r8.getCode()     // Catch:{ Exception -> 0x01ef }
            boolean r3 = r11.booleanValue()     // Catch:{ Exception -> 0x01ef }
            if (r3 != 0) goto L_0x0036
            java.lang.String r9 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            boolean r9 = android.text.TextUtils.isEmpty(r9)     // Catch:{ Exception -> 0x01ef }
            if (r9 != 0) goto L_0x0035
            java.lang.String r8 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            boolean r9 = r11.booleanValue()     // Catch:{ Exception -> 0x01ef }
            r6.a(r7, r8, r9)     // Catch:{ Exception -> 0x01ef }
        L_0x0035:
            return
        L_0x0036:
            java.lang.String r3 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            boolean r3 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x01ef }
            r4 = 2
            r5 = 3
            if (r3 == 0) goto L_0x0076
            if (r9 == 0) goto L_0x0076
            int r10 = r7.getLinkType()     // Catch:{ Exception -> 0x01ef }
            if (r10 != r4) goto L_0x0052
            int r10 = r6.e()     // Catch:{ Exception -> 0x01ef }
            r6.a(r7, r8, r9, r10)     // Catch:{ Exception -> 0x01ef }
            return
        L_0x0052:
            if (r10 != r5) goto L_0x0058
            r6.a(r7, r8, r9)     // Catch:{ Exception -> 0x01ef }
            return
        L_0x0058:
            android.content.Context r10 = r6.m     // Catch:{ Exception -> 0x01ef }
            java.lang.String r11 = r7.getClickURL()     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r1 = r6.q     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.base.utils.j.b(r10, r11, r1)     // Catch:{ Exception -> 0x01ef }
            r6.a(r8, r7, r0)     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r10 = r6.q     // Catch:{ Exception -> 0x01ef }
            if (r10 == 0) goto L_0x0075
            if (r9 == 0) goto L_0x0075
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r9 = r6.q     // Catch:{ Exception -> 0x01ef }
            java.lang.String r8 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            r9.onFinishRedirection(r7, r8)     // Catch:{ Exception -> 0x01ef }
        L_0x0075:
            return
        L_0x0076:
            if (r2 != r0) goto L_0x0124
            java.lang.String r11 = "Mintegral SDK M"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ef }
            java.lang.String r3 = "Jump to Google Play: "
            r2.<init>(r3)     // Catch:{ Exception -> 0x01ef }
            java.lang.String r3 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            r2.append(r3)     // Catch:{ Exception -> 0x01ef }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.base.utils.g.b(r11, r2)     // Catch:{ Exception -> 0x01ef }
            java.lang.String r11 = r7.getPackageName()     // Catch:{ Exception -> 0x01ef }
            boolean r11 = android.text.TextUtils.isEmpty(r11)     // Catch:{ Exception -> 0x01ef }
            if (r11 != 0) goto L_0x00cb
            java.lang.String r11 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            boolean r11 = android.text.TextUtils.isEmpty(r11)     // Catch:{ Exception -> 0x01ef }
            if (r11 != 0) goto L_0x00cb
            java.lang.String r11 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            java.lang.String r2 = r7.getPackageName()     // Catch:{ Exception -> 0x01ef }
            boolean r11 = r11.contains(r2)     // Catch:{ Exception -> 0x01ef }
            if (r11 == 0) goto L_0x00cb
            if (r9 == 0) goto L_0x00cb
            android.content.Context r11 = r6.m     // Catch:{ Exception -> 0x01ef }
            java.lang.String r2 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r3 = r6.q     // Catch:{ Exception -> 0x01ef }
            boolean r11 = com.mintegral.msdk.base.utils.j.a.a(r11, r2, r3)     // Catch:{ Exception -> 0x01ef }
            if (r11 != 0) goto L_0x0113
            java.lang.String r11 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r2 = r6.q     // Catch:{ Exception -> 0x01ef }
            r6.a(r1, r11, r7, r2)     // Catch:{ Exception -> 0x01ef }
            goto L_0x0113
        L_0x00cb:
            if (r9 == 0) goto L_0x0113
            java.lang.String r11 = r7.getPackageName()     // Catch:{ Exception -> 0x01ef }
            boolean r11 = android.text.TextUtils.isEmpty(r11)     // Catch:{ Exception -> 0x01ef }
            if (r11 != 0) goto L_0x00f3
            android.content.Context r11 = r6.m     // Catch:{ Exception -> 0x01ef }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ef }
            java.lang.String r3 = "market://details?id="
            r2.<init>(r3)     // Catch:{ Exception -> 0x01ef }
            java.lang.String r3 = r7.getPackageName()     // Catch:{ Exception -> 0x01ef }
            r2.append(r3)     // Catch:{ Exception -> 0x01ef }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r3 = r6.q     // Catch:{ Exception -> 0x01ef }
            boolean r11 = com.mintegral.msdk.base.utils.j.a.a(r11, r2, r3)     // Catch:{ Exception -> 0x01ef }
            if (r11 != 0) goto L_0x00fc
        L_0x00f3:
            java.lang.String r11 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r2 = r6.q     // Catch:{ Exception -> 0x01ef }
            r6.a(r1, r11, r7, r2)     // Catch:{ Exception -> 0x01ef }
        L_0x00fc:
            java.lang.String r11 = "Mintegral SDK M"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ef }
            java.lang.String r2 = "code market This pkg is "
            r1.<init>(r2)     // Catch:{ Exception -> 0x01ef }
            java.lang.String r2 = r7.getPackageName()     // Catch:{ Exception -> 0x01ef }
            r1.append(r2)     // Catch:{ Exception -> 0x01ef }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.base.utils.g.d(r11, r1)     // Catch:{ Exception -> 0x01ef }
        L_0x0113:
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r11 = r6.q     // Catch:{ Exception -> 0x01ef }
            if (r11 == 0) goto L_0x01b5
            if (r9 == 0) goto L_0x01b5
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r11 = r6.q     // Catch:{ Exception -> 0x01ef }
            java.lang.String r1 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            r11.onFinishRedirection(r7, r1)     // Catch:{ Exception -> 0x01ef }
            goto L_0x01b5
        L_0x0124:
            if (r2 != r5) goto L_0x016a
            if (r9 == 0) goto L_0x015a
            boolean r1 = com.mintegral.msdk.MIntegralConstans.ALLOW_APK_DOWNLOAD     // Catch:{ Exception -> 0x01ef }
            if (r1 == 0) goto L_0x014f
            java.lang.String r1 = "Mintegral SDK M"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ef }
            java.lang.String r3 = "Jump to download: "
            r2.<init>(r3)     // Catch:{ Exception -> 0x01ef }
            java.lang.String r3 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            r2.append(r3)     // Catch:{ Exception -> 0x01ef }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.base.utils.g.b(r1, r2)     // Catch:{ Exception -> 0x01ef }
            java.lang.String r1 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            boolean r11 = r11.booleanValue()     // Catch:{ Exception -> 0x01ef }
            r6.a(r7, r1, r11)     // Catch:{ Exception -> 0x01ef }
            goto L_0x015a
        L_0x014f:
            android.content.Context r11 = r6.m     // Catch:{ Exception -> 0x01ef }
            java.lang.String r1 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r2 = r6.q     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.base.utils.j.b(r11, r1, r2)     // Catch:{ Exception -> 0x01ef }
        L_0x015a:
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r11 = r6.q     // Catch:{ Exception -> 0x01ef }
            if (r11 == 0) goto L_0x01b5
            if (r9 == 0) goto L_0x01b5
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r11 = r6.q     // Catch:{ Exception -> 0x01ef }
            java.lang.String r1 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            r11.onFinishRedirection(r7, r1)     // Catch:{ Exception -> 0x01ef }
            goto L_0x01b5
        L_0x016a:
            if (r9 == 0) goto L_0x01b5
            java.lang.String r11 = "Mintegral SDK M"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ef }
            java.lang.String r2 = "Jump to Web: "
            r1.<init>(r2)     // Catch:{ Exception -> 0x01ef }
            java.lang.String r2 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            r1.append(r2)     // Catch:{ Exception -> 0x01ef }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.base.utils.g.b(r11, r1)     // Catch:{ Exception -> 0x01ef }
            int r11 = r7.getLinkType()     // Catch:{ Exception -> 0x01ef }
            if (r5 != r11) goto L_0x018d
            r6.a(r7, r8, r9)     // Catch:{ Exception -> 0x01ef }
            return
        L_0x018d:
            int r11 = r7.getLinkType()     // Catch:{ Exception -> 0x01ef }
            if (r4 != r11) goto L_0x019b
            int r10 = r6.e()     // Catch:{ Exception -> 0x01ef }
            r6.a(r7, r8, r9, r10)     // Catch:{ Exception -> 0x01ef }
            return
        L_0x019b:
            android.content.Context r11 = r6.m     // Catch:{ Exception -> 0x01ef }
            java.lang.String r1 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r2 = r6.q     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.base.utils.j.b(r11, r1, r2)     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r11 = r6.q     // Catch:{ Exception -> 0x01ef }
            if (r11 == 0) goto L_0x01b5
            if (r9 == 0) goto L_0x01b5
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r11 = r6.q     // Catch:{ Exception -> 0x01ef }
            java.lang.String r1 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            r11.onFinishRedirection(r7, r1)     // Catch:{ Exception -> 0x01ef }
        L_0x01b5:
            int r11 = r7.getLinkType()     // Catch:{ Exception -> 0x01ef }
            java.lang.String r1 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            boolean r11 = a(r11, r1)     // Catch:{ Exception -> 0x01ef }
            if (r11 == 0) goto L_0x01c8
            r11 = 0
            r6.a(r8, r7, r11)     // Catch:{ Exception -> 0x01ef }
            goto L_0x01cb
        L_0x01c8:
            r6.a(r8, r7, r0)     // Catch:{ Exception -> 0x01ef }
        L_0x01cb:
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r11 = r6.q     // Catch:{ Exception -> 0x01ef }
            if (r11 == 0) goto L_0x01dc
            if (r9 != 0) goto L_0x01dc
            if (r10 == 0) goto L_0x01dc
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r9 = r6.q     // Catch:{ Exception -> 0x01ef }
            java.lang.String r8 = r8.getUrl()     // Catch:{ Exception -> 0x01ef }
            r9.onFinishRedirection(r7, r8)     // Catch:{ Exception -> 0x01ef }
        L_0x01dc:
            return
        L_0x01dd:
            if (r9 == 0) goto L_0x01ee
            r6.a(r8, r7, r0)     // Catch:{ Exception -> 0x01ef }
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r7 = r6.q     // Catch:{ Exception -> 0x01ef }
            if (r7 == 0) goto L_0x01ee
            if (r9 == 0) goto L_0x01ee
            com.mintegral.msdk.out.NativeListener$NativeTrackingListener r7 = r6.q     // Catch:{ Exception -> 0x01ef }
            r8 = 0
            r7.onRedirectionFailed(r8, r8)     // Catch:{ Exception -> 0x01ef }
        L_0x01ee:
            return
        L_0x01ef:
            r7 = move-exception
            r7.printStackTrace()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void");
    }

    private int e() {
        try {
            if (this.t != null) {
                return this.t.ad();
            }
            return 1;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 1;
        }
    }

    private static boolean a(int i2, String str) {
        if (i2 == 2) {
            try {
                if (j.a.a(str)) {
                    return true;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else if (!TextUtils.isEmpty(str)) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:23|24|25|26|27) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x00f1 */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.mintegral.msdk.base.entity.CampaignEx r17, java.lang.String r18, boolean r19) {
        /*
            r16 = this;
            r7 = r16
            r8 = r17
            r9 = r18
            java.lang.String r0 = r17.getPackageName()
            r10 = -1
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0240 }
            android.content.Context r1 = r1.h()     // Catch:{ Throwable -> 0x0240 }
            java.lang.String r2 = ""
            java.lang.Object r1 = com.mintegral.msdk.base.utils.r.b(r1, r0, r2)     // Catch:{ Throwable -> 0x0240 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x0240 }
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Throwable -> 0x0240 }
            r11 = 0
            r3 = 1
            r13 = 0
            if (r2 != 0) goto L_0x003e
            java.io.File r2 = new java.io.File     // Catch:{ Throwable -> 0x0240 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x0240 }
            boolean r1 = r2.exists()     // Catch:{ Throwable -> 0x0240 }
            if (r1 == 0) goto L_0x00e0
            if (r19 == 0) goto L_0x003d
            android.content.Context r1 = r7.m     // Catch:{ Throwable -> 0x0240 }
            android.net.Uri r2 = android.net.Uri.fromFile(r2)     // Catch:{ Throwable -> 0x0240 }
            com.mintegral.msdk.click.b.a(r1, r2, r9, r0)     // Catch:{ Throwable -> 0x0240 }
        L_0x003d:
            return
        L_0x003e:
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0240 }
            android.content.Context r1 = r1.h()     // Catch:{ Throwable -> 0x0240 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0240 }
            r2.<init>()     // Catch:{ Throwable -> 0x0240 }
            r2.append(r0)     // Catch:{ Throwable -> 0x0240 }
            java.lang.String r4 = "process"
            r2.append(r4)     // Catch:{ Throwable -> 0x0240 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0240 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r13)     // Catch:{ Throwable -> 0x0240 }
            java.lang.Object r1 = com.mintegral.msdk.base.utils.r.b(r1, r2, r4)     // Catch:{ Throwable -> 0x0240 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Throwable -> 0x0240 }
            int r1 = r1.intValue()     // Catch:{ Throwable -> 0x0240 }
            int r2 = android.os.Process.myPid()     // Catch:{ Throwable -> 0x0240 }
            if (r1 == 0) goto L_0x00e0
            if (r1 != r2) goto L_0x00e0
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0240 }
            android.content.Context r1 = r1.h()     // Catch:{ Throwable -> 0x0240 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0240 }
            r2.<init>()     // Catch:{ Throwable -> 0x0240 }
            r2.append(r0)     // Catch:{ Throwable -> 0x0240 }
            java.lang.String r4 = "isDowning"
            r2.append(r4)     // Catch:{ Throwable -> 0x0240 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0240 }
            java.lang.Long r4 = java.lang.Long.valueOf(r11)     // Catch:{ Throwable -> 0x0240 }
            java.lang.Object r1 = com.mintegral.msdk.base.utils.r.b(r1, r2, r4)     // Catch:{ Throwable -> 0x0240 }
            java.lang.Long r1 = (java.lang.Long) r1     // Catch:{ Throwable -> 0x0240 }
            long r1 = r1.longValue()     // Catch:{ Throwable -> 0x0240 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0240 }
            r6 = 0
            long r4 = r4 - r1
            int r6 = (r1 > r11 ? 1 : (r1 == r11 ? 0 : -1))
            if (r6 == 0) goto L_0x00e0
            r1 = 600000(0x927c0, double:2.964394E-318)
            int r6 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r6 >= 0) goto L_0x00e0
            if (r19 == 0) goto L_0x00df
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0240 }
            android.content.Context r1 = r1.h()     // Catch:{ Throwable -> 0x0240 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0240 }
            r2.<init>()     // Catch:{ Throwable -> 0x0240 }
            r2.append(r0)     // Catch:{ Throwable -> 0x0240 }
            java.lang.String r0 = "downloadType"
            r2.append(r0)     // Catch:{ Throwable -> 0x0240 }
            java.lang.String r0 = r2.toString()     // Catch:{ Throwable -> 0x0240 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r10)     // Catch:{ Throwable -> 0x0240 }
            java.lang.Object r0 = com.mintegral.msdk.base.utils.r.b(r1, r0, r2)     // Catch:{ Throwable -> 0x0240 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Throwable -> 0x0240 }
            int r0 = r0.intValue()     // Catch:{ Throwable -> 0x0240 }
            if (r0 != r3) goto L_0x00d8
            android.content.Context r0 = r7.m     // Catch:{ Throwable -> 0x0240 }
            java.lang.String r1 = "downloading"
            b(r8, r0, r1)     // Catch:{ Throwable -> 0x0240 }
            return
        L_0x00d8:
            android.content.Context r0 = r7.m     // Catch:{ Throwable -> 0x0240 }
            java.lang.String r1 = "downloading"
            a(r8, r0, r1)     // Catch:{ Throwable -> 0x0240 }
        L_0x00df:
            return
        L_0x00e0:
            int r0 = com.mintegral.msdk.base.utils.t.a     // Catch:{ Throwable -> 0x0240 }
            if (r0 != r10) goto L_0x00f3
            java.lang.String r0 = "com.mintegral.msdk.mtgdownload.b"
            java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x00f1 }
            java.lang.String r0 = "com.mintegral.msdk.mtgdownload.g"
            java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x00f1 }
            com.mintegral.msdk.base.utils.t.a = r3     // Catch:{ ClassNotFoundException -> 0x00f1 }
            goto L_0x00f3
        L_0x00f1:
            com.mintegral.msdk.base.utils.t.a = r13     // Catch:{ Throwable -> 0x0240 }
        L_0x00f3:
            int r0 = com.mintegral.msdk.base.utils.t.a     // Catch:{ Throwable -> 0x0240 }
            if (r0 != r3) goto L_0x023c
            if (r19 == 0) goto L_0x023c
            if (r8 == 0) goto L_0x010b
            java.lang.String r0 = r17.getPackageName()     // Catch:{ Throwable -> 0x0240 }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0240 }
            if (r0 != 0) goto L_0x010b
            java.lang.String r0 = r17.getPackageName()     // Catch:{ Throwable -> 0x0240 }
            r14 = r0
            goto L_0x010c
        L_0x010b:
            r14 = r9
        L_0x010c:
            com.mintegral.msdk.base.utils.j.a(r9, r3, r8)     // Catch:{ Throwable -> 0x01ee }
            com.mintegral.msdk.base.controller.a r0 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x01ee }
            android.content.Context r0 = r0.h()     // Catch:{ Throwable -> 0x01ee }
            boolean r1 = com.mintegral.msdk.base.utils.t.a(r0)     // Catch:{ Throwable -> 0x01ee }
            boolean r2 = com.mintegral.msdk.base.utils.t.c(r0)     // Catch:{ Throwable -> 0x01ee }
            boolean r4 = com.mintegral.msdk.base.utils.t.b(r0)     // Catch:{ Throwable -> 0x01ee }
            if (r4 != 0) goto L_0x0129
            com.mintegral.msdk.click.b.a(r0, r9, r14)     // Catch:{ Throwable -> 0x01ee }
            return
        L_0x0129:
            if (r2 != 0) goto L_0x012f
            r16.b(r17, r18, r19)     // Catch:{ Throwable -> 0x01ee }
            return
        L_0x012f:
            if (r1 != 0) goto L_0x0135
            r16.b(r17, r18, r19)     // Catch:{ Throwable -> 0x01ee }
            return
        L_0x0135:
            com.mintegral.msdk.base.controller.a r0 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x01ee }
            android.content.Context r0 = r0.h()     // Catch:{ Throwable -> 0x01ee }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01ee }
            r1.<init>()     // Catch:{ Throwable -> 0x01ee }
            r1.append(r14)     // Catch:{ Throwable -> 0x01ee }
            java.lang.String r2 = "isDowning"
            r1.append(r2)     // Catch:{ Throwable -> 0x01ee }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x01ee }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x01ee }
            java.lang.Long r2 = java.lang.Long.valueOf(r4)     // Catch:{ Throwable -> 0x01ee }
            com.mintegral.msdk.base.utils.r.a(r0, r1, r2)     // Catch:{ Throwable -> 0x01ee }
            com.mintegral.msdk.base.controller.a r0 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x01ee }
            android.content.Context r0 = r0.h()     // Catch:{ Throwable -> 0x01ee }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01ee }
            r1.<init>()     // Catch:{ Throwable -> 0x01ee }
            r1.append(r14)     // Catch:{ Throwable -> 0x01ee }
            java.lang.String r2 = "process"
            r1.append(r2)     // Catch:{ Throwable -> 0x01ee }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x01ee }
            int r2 = android.os.Process.myPid()     // Catch:{ Throwable -> 0x01ee }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Throwable -> 0x01ee }
            com.mintegral.msdk.base.utils.r.a(r0, r1, r2)     // Catch:{ Throwable -> 0x01ee }
            java.lang.String r0 = "com.mintegral.msdk.mtgdownload.g"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Throwable -> 0x01ee }
            r1 = 2
            java.lang.Class[] r2 = new java.lang.Class[r1]     // Catch:{ Throwable -> 0x01ee }
            java.lang.Class<android.content.Context> r4 = android.content.Context.class
            r2[r13] = r4     // Catch:{ Throwable -> 0x01ee }
            java.lang.Class<java.lang.String> r4 = java.lang.String.class
            r2[r3] = r4     // Catch:{ Throwable -> 0x01ee }
            java.lang.reflect.Constructor r2 = r0.getConstructor(r2)     // Catch:{ Throwable -> 0x01ee }
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x01ee }
            com.mintegral.msdk.base.controller.a r4 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x01ee }
            android.content.Context r4 = r4.h()     // Catch:{ Throwable -> 0x01ee }
            r1[r13] = r4     // Catch:{ Throwable -> 0x01ee }
            r1[r3] = r9     // Catch:{ Throwable -> 0x01ee }
            java.lang.Object r6 = r2.newInstance(r1)     // Catch:{ Throwable -> 0x01ee }
            java.lang.String r1 = "setTitle"
            java.lang.Class[] r2 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x01ee }
            java.lang.Class<java.lang.String> r4 = java.lang.String.class
            r2[r13] = r4     // Catch:{ Throwable -> 0x01ee }
            java.lang.reflect.Method r1 = r0.getMethod(r1, r2)     // Catch:{ Throwable -> 0x01ee }
            java.lang.Object[] r2 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x01ee }
            java.lang.String r4 = r17.getAppName()     // Catch:{ Throwable -> 0x01ee }
            r2[r13] = r4     // Catch:{ Throwable -> 0x01ee }
            r1.invoke(r6, r2)     // Catch:{ Throwable -> 0x01ee }
            java.lang.String r1 = "setDownloadListener"
            java.lang.Class[] r2 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x01ee }
            java.lang.Class<com.mintegral.msdk.out.IDownloadListener> r4 = com.mintegral.msdk.out.IDownloadListener.class
            r2[r13] = r4     // Catch:{ Throwable -> 0x01ee }
            java.lang.reflect.Method r5 = r0.getMethod(r1, r2)     // Catch:{ Throwable -> 0x01ee }
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x01ee }
            com.mintegral.msdk.click.a$3 r15 = new com.mintegral.msdk.click.a$3     // Catch:{ Throwable -> 0x01ee }
            r1 = r15
            r2 = r16
            r3 = r17
            r10 = r4
            r4 = r19
            r11 = r5
            r5 = r14
            r12 = r6
            r6 = r18
            r1.<init>(r3, r4, r5, r6)     // Catch:{ Throwable -> 0x01ee }
            r10[r13] = r15     // Catch:{ Throwable -> 0x01ee }
            r11.invoke(r12, r10)     // Catch:{ Throwable -> 0x01ee }
            java.lang.String r1 = "start"
            java.lang.Class[] r2 = new java.lang.Class[r13]     // Catch:{ Throwable -> 0x01ee }
            java.lang.reflect.Method r0 = r0.getMethod(r1, r2)     // Catch:{ Throwable -> 0x01ee }
            java.lang.Object[] r1 = new java.lang.Object[r13]     // Catch:{ Throwable -> 0x01ee }
            r0.invoke(r12, r1)     // Catch:{ Throwable -> 0x01ee }
            return
        L_0x01ee:
            r0 = move-exception
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0240 }
            android.content.Context r1 = r1.h()     // Catch:{ Throwable -> 0x0240 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0240 }
            r2.<init>()     // Catch:{ Throwable -> 0x0240 }
            r2.append(r14)     // Catch:{ Throwable -> 0x0240 }
            java.lang.String r3 = "isDowning"
            r2.append(r3)     // Catch:{ Throwable -> 0x0240 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0240 }
            r3 = 0
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Throwable -> 0x0240 }
            com.mintegral.msdk.base.utils.r.a(r1, r2, r3)     // Catch:{ Throwable -> 0x0240 }
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0240 }
            android.content.Context r1 = r1.h()     // Catch:{ Throwable -> 0x0240 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0240 }
            r2.<init>()     // Catch:{ Throwable -> 0x0240 }
            r2.append(r14)     // Catch:{ Throwable -> 0x0240 }
            java.lang.String r3 = "process"
            r2.append(r3)     // Catch:{ Throwable -> 0x0240 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0240 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r13)     // Catch:{ Throwable -> 0x0240 }
            com.mintegral.msdk.base.utils.r.a(r1, r2, r3)     // Catch:{ Throwable -> 0x0240 }
            boolean r1 = com.mintegral.msdk.MIntegralConstans.DEBUG     // Catch:{ Throwable -> 0x0240 }
            if (r1 == 0) goto L_0x0238
            r0.printStackTrace()     // Catch:{ Throwable -> 0x0240 }
        L_0x0238:
            r16.b(r17, r18, r19)     // Catch:{ Throwable -> 0x0240 }
            return
        L_0x023c:
            r16.b(r17, r18, r19)     // Catch:{ Throwable -> 0x0240 }
            return
        L_0x0240:
            r1 = -1
            com.mintegral.msdk.base.utils.t.a = r1
            java.lang.String r0 = "downloadapk"
            java.lang.String r1 = "can't find download jar, use simple method"
            com.mintegral.msdk.base.utils.g.b(r0, r1)
            r16.b(r17, r18, r19)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, boolean):void");
    }

    public static void a(CampaignEx campaignEx, Context context, String str) {
        try {
            if (str.equals("start") || str.equals("downloading")) {
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    f();
                } else {
                    Message obtainMessage = g.obtainMessage(1);
                    obtainMessage.obj = context;
                    g.sendMessage(obtainMessage);
                }
            }
            c(campaignEx, context, str);
        } catch (Throwable th) {
            g.c("Mintegral SDK M", th.getMessage(), th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    private static void c(CampaignEx campaignEx, Context context, String str) {
        if (campaignEx != null) {
            try {
                if (campaignEx.getNativeVideoTracking() != null) {
                    int i2 = 0;
                    if (!str.equals("start")) {
                        if (!str.equals("shortcuts_start")) {
                            if (str.equals("end")) {
                                if (campaignEx.getNativeVideoTracking().d() != null) {
                                    while (i2 < campaignEx.getNativeVideoTracking().d().length) {
                                        a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().d()[i2], false, false);
                                        i2++;
                                    }
                                    return;
                                }
                                return;
                            } else if (str.equals("install")) {
                                if (campaignEx.getNativeVideoTracking().e() != null) {
                                    while (i2 < campaignEx.getNativeVideoTracking().e().length) {
                                        a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().e()[i2], false, false);
                                        i2++;
                                    }
                                }
                                com.mintegral.msdk.base.b.g.b(i.a(context)).m(campaignEx.getPackageName());
                                return;
                            } else {
                                return;
                            }
                        }
                    }
                    if (campaignEx.getNativeVideoTracking().c() != null) {
                        while (i2 < campaignEx.getNativeVideoTracking().c().length) {
                            a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().c()[i2], false, false);
                            i2++;
                        }
                    }
                }
            } catch (Throwable th) {
                g.c("Mintegral SDK M", th.getMessage(), th);
            }
        }
    }

    public static void b(CampaignEx campaignEx, Context context, String str) {
        try {
            if (str.equals("start") || str.equals("downloading")) {
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    a(context);
                } else {
                    Message obtainMessage = g.obtainMessage(0);
                    obtainMessage.obj = context;
                    g.sendMessage(obtainMessage);
                }
            }
            c(campaignEx, context, str);
        } catch (Throwable th) {
            g.c("Mintegral SDK M", th.getMessage(), th);
        }
    }

    public static void a(Context context) {
        com.mintegral.msdk.d.b.a();
        com.mintegral.msdk.d.a b2 = com.mintegral.msdk.d.b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b2 == null) {
            return;
        }
        if (com.mintegral.msdk.base.controller.a.d().h() != null || context == null) {
            Toast.makeText(com.mintegral.msdk.base.controller.a.d().h(), b2.V(), 0).show();
        } else {
            Toast.makeText(context, b2.V(), 0).show();
        }
    }

    /* access modifiers changed from: private */
    public static void f() {
        try {
            String language = Locale.getDefault().getLanguage();
            if (TextUtils.isEmpty(language) || !language.equals("zh")) {
                Toast.makeText(com.mintegral.msdk.base.controller.a.d().h(), "Downloading....", 0).show();
            } else {
                Toast.makeText(com.mintegral.msdk.base.controller.a.d().h(), "正在下载中,请稍候...", 0).show();
            }
        } catch (Exception unused) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0026 A[Catch:{ Throwable -> 0x0011 }] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002a A[Catch:{ Throwable -> 0x0011 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(final com.mintegral.msdk.base.entity.CampaignEx r6, final java.lang.String r7, final boolean r8) {
        /*
            r5 = this;
            if (r6 == 0) goto L_0x0013
            java.lang.String r0 = r6.getPackageName()     // Catch:{ Throwable -> 0x0011 }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0011 }
            if (r0 != 0) goto L_0x0013
            java.lang.String r0 = r6.getPackageName()     // Catch:{ Throwable -> 0x0011 }
            goto L_0x0014
        L_0x0011:
            r6 = move-exception
            goto L_0x0080
        L_0x0013:
            r0 = r7
        L_0x0014:
            r1 = 2
            com.mintegral.msdk.base.utils.j.a(r7, r1, r6)     // Catch:{ Throwable -> 0x0011 }
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0011 }
            android.content.Context r1 = r1.h()     // Catch:{ Throwable -> 0x0011 }
            boolean r2 = com.mintegral.msdk.base.utils.t.b(r1)     // Catch:{ Throwable -> 0x0011 }
            if (r2 != 0) goto L_0x002a
            com.mintegral.msdk.click.b.a(r1, r7, r0)     // Catch:{ Throwable -> 0x0011 }
            return
        L_0x002a:
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0011 }
            android.content.Context r1 = r1.h()     // Catch:{ Throwable -> 0x0011 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0011 }
            r2.<init>()     // Catch:{ Throwable -> 0x0011 }
            r2.append(r0)     // Catch:{ Throwable -> 0x0011 }
            java.lang.String r3 = "isDowning"
            r2.append(r3)     // Catch:{ Throwable -> 0x0011 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0011 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0011 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Throwable -> 0x0011 }
            com.mintegral.msdk.base.utils.r.a(r1, r2, r3)     // Catch:{ Throwable -> 0x0011 }
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0011 }
            android.content.Context r1 = r1.h()     // Catch:{ Throwable -> 0x0011 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0011 }
            r2.<init>()     // Catch:{ Throwable -> 0x0011 }
            r2.append(r0)     // Catch:{ Throwable -> 0x0011 }
            java.lang.String r0 = "process"
            r2.append(r0)     // Catch:{ Throwable -> 0x0011 }
            java.lang.String r0 = r2.toString()     // Catch:{ Throwable -> 0x0011 }
            int r2 = android.os.Process.myPid()     // Catch:{ Throwable -> 0x0011 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Throwable -> 0x0011 }
            com.mintegral.msdk.base.utils.r.a(r1, r0, r2)     // Catch:{ Throwable -> 0x0011 }
            java.lang.Thread r0 = new java.lang.Thread     // Catch:{ Throwable -> 0x0011 }
            com.mintegral.msdk.click.a$4 r1 = new com.mintegral.msdk.click.a$4     // Catch:{ Throwable -> 0x0011 }
            r1.<init>(r6, r7, r8)     // Catch:{ Throwable -> 0x0011 }
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0011 }
            r0.start()     // Catch:{ Throwable -> 0x0011 }
            return
        L_0x0080:
            boolean r7 = com.mintegral.msdk.MIntegralConstans.DEBUG
            if (r7 == 0) goto L_0x0087
            r6.printStackTrace()
        L_0x0087:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.click.a.b(com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, boolean):void");
    }

    static /* synthetic */ void a(a aVar, CampaignEx campaignEx) {
        try {
            Intent intent = new Intent(aVar.m, LoadingActivity.class);
            intent.setFlags(DriveFile.MODE_READ_ONLY);
            intent.putExtra(CampaignEx.JSON_KEY_ICON_URL, campaignEx.getIconUrl());
            aVar.m.startActivity(intent);
        } catch (Exception e2) {
            g.c("Mintegral SDK M", "Exception", e2);
        }
    }

    static /* synthetic */ void g(a aVar) {
        try {
            Intent intent = new Intent();
            intent.setAction("ExitApp");
            aVar.m.sendBroadcast(intent);
        } catch (Exception e2) {
            g.c("Mintegral SDK M", "Exception", e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:106:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0191 A[Catch:{ all -> 0x01b7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x01a1 A[SYNTHETIC, Splitter:B:78:0x01a1] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01ac A[SYNTHETIC, Splitter:B:83:0x01ac] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01bc A[SYNTHETIC, Splitter:B:91:0x01bc] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x01c7 A[SYNTHETIC, Splitter:B:96:0x01c7] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void a(com.mintegral.msdk.click.a r16, com.mintegral.msdk.base.entity.CampaignEx r17, java.lang.String r18, boolean r19) {
        /*
            r0 = r16
            r1 = r17
            r2 = r18
            java.lang.String r3 = "Mintegral SDK M"
            java.lang.String r4 = "CommonclickControl    simple"
            com.mintegral.msdk.base.utils.g.d(r3, r4)
            if (r1 == 0) goto L_0x001e
            java.lang.String r3 = r17.getPackageName()
            boolean r3 = android.text.TextUtils.isEmpty(r3)
            if (r3 != 0) goto L_0x001e
            java.lang.String r3 = r17.getPackageName()
            goto L_0x001f
        L_0x001e:
            r3 = r2
        L_0x001f:
            r4 = 0
            r5 = 1
            boolean[] r6 = new boolean[r5]     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            java.lang.String r7 = "/apk"
            android.content.Context r8 = r0.m     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            java.io.File r6 = com.mintegral.msdk.base.utils.e.a(r7, r8, r6)     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            java.lang.String r7 = com.mintegral.msdk.click.b.a(r18)     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            r8.<init>()     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            r8.append(r7)     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            java.lang.String r7 = ".apk"
            r8.append(r7)     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            java.lang.String r7 = r8.toString()     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            java.io.File r8 = new java.io.File     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            r8.<init>(r6, r7)     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            boolean r6 = r8.exists()     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            if (r6 == 0) goto L_0x004e
            r8.delete()     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
        L_0x004e:
            android.os.Handler r6 = r0.x     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            r7 = 0
            android.os.Message r6 = r6.obtainMessage(r7)     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            android.os.Handler r9 = r0.x     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            r9.sendMessage(r6)     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            if (r19 == 0) goto L_0x0064
            android.content.Context r6 = r0.m     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            java.lang.String r9 = "start"
            a(r1, r6, r9)     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            goto L_0x006b
        L_0x0064:
            android.content.Context r6 = r0.m     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            java.lang.String r9 = "shortcuts_start"
            a(r1, r6, r9)     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
        L_0x006b:
            java.net.URL r6 = new java.net.URL     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            r6.<init>(r2)     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            java.net.URLConnection r6 = r6.openConnection()     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            r9 = 8000(0x1f40, float:1.121E-41)
            r6.setConnectTimeout(r9)     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            int r9 = r6.getContentLength()     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            r10 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r12 = (double) r9
            java.lang.Double.isNaN(r12)
            double r10 = r10 / r12
            java.io.InputStream r6 = r6.getInputStream()     // Catch:{ Throwable -> 0x018b, all -> 0x0186 }
            r12 = 1024(0x400, float:1.435E-42)
            byte[] r12 = new byte[r12]     // Catch:{ Throwable -> 0x0183, all -> 0x0180 }
            java.io.FileOutputStream r13 = new java.io.FileOutputStream     // Catch:{ Throwable -> 0x0183, all -> 0x0180 }
            r13.<init>(r8, r5)     // Catch:{ Throwable -> 0x0183, all -> 0x0180 }
            r4 = 0
            r14 = 0
        L_0x0093:
            int r5 = r6.read(r12)     // Catch:{ Throwable -> 0x017c, all -> 0x0178 }
            r15 = r6
            r6 = -1
            if (r5 == r6) goto L_0x00cb
            r13.write(r12, r7, r5)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            int r4 = r4 + r5
            double r5 = (double) r4
            java.lang.Double.isNaN(r5)
            double r5 = r5 * r10
            int r5 = (int) r5
            r6 = 512(0x200, float:7.175E-43)
            if (r14 >= r6) goto L_0x00b2
            r6 = 100
            if (r5 != r6) goto L_0x00af
            goto L_0x00b2
        L_0x00af:
            r5 = r14
            r14 = 1
            goto L_0x00c1
        L_0x00b2:
            android.os.Handler r6 = r0.x     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            r14 = 1
            android.os.Message r6 = r6.obtainMessage(r14)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            r6.arg1 = r5     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            android.os.Handler r5 = r0.x     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            r5.sendMessage(r6)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            r5 = 0
        L_0x00c1:
            int r5 = r5 + r14
            r14 = r5
            r6 = r15
            goto L_0x0093
        L_0x00c5:
            r0 = move-exception
            goto L_0x017a
        L_0x00c8:
            r0 = move-exception
            goto L_0x017e
        L_0x00cb:
            if (r4 != r9) goto L_0x0162
            com.mintegral.msdk.base.controller.a r4 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            android.content.Context r4 = r4.h()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            r5.<init>()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            r5.append(r3)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            java.lang.String r6 = "isDowning"
            r5.append(r6)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            r9 = 0
            java.lang.Long r6 = java.lang.Long.valueOf(r9)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            com.mintegral.msdk.base.utils.r.a(r4, r5, r6)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            com.mintegral.msdk.base.controller.a r4 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            android.content.Context r4 = r4.h()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            r5.<init>()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            r5.append(r3)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            java.lang.String r6 = "process"
            r5.append(r6)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r7)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            com.mintegral.msdk.base.utils.r.a(r4, r5, r6)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            android.os.Handler r4 = r0.x     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            r5 = 2
            android.os.Message r4 = r4.obtainMessage(r5)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            r4.obj = r1     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            com.mintegral.msdk.base.controller.a r5 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            android.content.Context r5 = r5.h()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            com.mintegral.msdk.base.b.i r5 = com.mintegral.msdk.base.b.i.a(r5)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            com.mintegral.msdk.base.b.g r5 = com.mintegral.msdk.base.b.g.b(r5)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            r5.a(r1)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            android.os.Handler r1 = r0.x     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            r1.sendMessage(r4)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            boolean r1 = r8.exists()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            if (r1 == 0) goto L_0x0151
            if (r19 == 0) goto L_0x0151
            android.content.Context r0 = r0.m     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            android.net.Uri r1 = android.net.Uri.fromFile(r8)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            com.mintegral.msdk.click.b.a(r0, r1, r2, r3)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            com.mintegral.msdk.base.controller.a r0 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            android.content.Context r0 = r0.h()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            java.lang.String r1 = r8.getAbsolutePath()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            com.mintegral.msdk.base.utils.r.a(r0, r3, r1)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            goto L_0x0162
        L_0x0151:
            if (r19 != 0) goto L_0x0162
            com.mintegral.msdk.base.controller.a r0 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            android.content.Context r0 = r0.h()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            java.lang.String r1 = r8.getAbsolutePath()     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
            com.mintegral.msdk.base.utils.r.a(r0, r3, r1)     // Catch:{ Throwable -> 0x00c8, all -> 0x00c5 }
        L_0x0162:
            r13.close()     // Catch:{ IOException -> 0x0166 }
            goto L_0x016b
        L_0x0166:
            r0 = move-exception
            r1 = r0
            r1.printStackTrace()
        L_0x016b:
            if (r15 == 0) goto L_0x0177
            r15.close()     // Catch:{ IOException -> 0x0171 }
            goto L_0x0177
        L_0x0171:
            r0 = move-exception
            r1 = r0
            r1.printStackTrace()
            return
        L_0x0177:
            return
        L_0x0178:
            r0 = move-exception
            r15 = r6
        L_0x017a:
            r1 = r0
            goto L_0x01ba
        L_0x017c:
            r0 = move-exception
            r15 = r6
        L_0x017e:
            r4 = r13
            goto L_0x018d
        L_0x0180:
            r0 = move-exception
            r15 = r6
            goto L_0x01b8
        L_0x0183:
            r0 = move-exception
            r15 = r6
            goto L_0x018d
        L_0x0186:
            r0 = move-exception
            r1 = r0
            r13 = r4
            r15 = r13
            goto L_0x01ba
        L_0x018b:
            r0 = move-exception
            r15 = r4
        L_0x018d:
            boolean r1 = com.mintegral.msdk.MIntegralConstans.DEBUG     // Catch:{ all -> 0x01b7 }
            if (r1 == 0) goto L_0x0194
            r0.printStackTrace()     // Catch:{ all -> 0x01b7 }
        L_0x0194:
            com.mintegral.msdk.base.controller.a r0 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ all -> 0x01b7 }
            android.content.Context r0 = r0.h()     // Catch:{ all -> 0x01b7 }
            com.mintegral.msdk.click.b.a(r0, r2, r3)     // Catch:{ all -> 0x01b7 }
            if (r4 == 0) goto L_0x01aa
            r4.close()     // Catch:{ IOException -> 0x01a5 }
            goto L_0x01aa
        L_0x01a5:
            r0 = move-exception
            r1 = r0
            r1.printStackTrace()
        L_0x01aa:
            if (r15 == 0) goto L_0x01b6
            r15.close()     // Catch:{ IOException -> 0x01b0 }
            goto L_0x01b6
        L_0x01b0:
            r0 = move-exception
            r1 = r0
            r1.printStackTrace()
            return
        L_0x01b6:
            return
        L_0x01b7:
            r0 = move-exception
        L_0x01b8:
            r1 = r0
            r13 = r4
        L_0x01ba:
            if (r13 == 0) goto L_0x01c5
            r13.close()     // Catch:{ IOException -> 0x01c0 }
            goto L_0x01c5
        L_0x01c0:
            r0 = move-exception
            r2 = r0
            r2.printStackTrace()
        L_0x01c5:
            if (r15 == 0) goto L_0x01d0
            r15.close()     // Catch:{ IOException -> 0x01cb }
            goto L_0x01d0
        L_0x01cb:
            r0 = move-exception
            r2 = r0
            r2.printStackTrace()
        L_0x01d0:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, boolean):void");
    }
}
