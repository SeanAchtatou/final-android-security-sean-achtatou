package com.mintegral.msdk.click;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.d.b;
import im.getsocial.sdk.consts.LanguageCodes;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;

/* compiled from: WebViewSpider */
public class f {
    public static long a = 0;
    /* access modifiers changed from: private */
    public static final String d = "f";
    boolean b;
    boolean c;
    /* access modifiers changed from: private */
    public int e = 15000;
    /* access modifiers changed from: private */
    public int f = 3000;
    private Handler g = new Handler(Looper.getMainLooper());
    private com.mintegral.msdk.d.a h;
    /* access modifiers changed from: private */
    public a i;
    /* access modifiers changed from: private */
    public String j;
    private String k;
    /* access modifiers changed from: private */
    public WebView l;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public String n;
    /* access modifiers changed from: private */
    public int o;
    /* access modifiers changed from: private */
    public boolean p = false;
    /* access modifiers changed from: private */
    public boolean q;
    private final Runnable r = new Runnable() {
        public final void run() {
            boolean unused = f.this.p = true;
            int unused2 = f.this.o = 1;
            String a2 = f.d;
            g.d(a2, "js超时！超时上限：" + f.this.f + LanguageCodes.MALAY);
            f.p(f.this);
        }
    };
    private final Runnable s = new Runnable() {
        public final void run() {
            boolean unused = f.this.p = true;
            int unused2 = f.this.o = 2;
            String a2 = f.d;
            g.d(a2, "http超时！超时上限：" + f.this.e + LanguageCodes.MALAY);
            f.p(f.this);
        }
    };

    /* compiled from: WebViewSpider */
    interface a {
        void a(String str, String str2);

        void a(String str, String str2, String str3);

        boolean a(String str);

        boolean b(String str);

        boolean c(String str);
    }

    public f(boolean z) {
        b.a();
        this.h = b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (this.h == null) {
            b.a();
            this.h = b.b();
        }
        this.m = this.h.aD();
        if (z) {
            this.e = (int) this.h.aw();
            this.f = (int) this.h.aw();
            return;
        }
        this.e = (int) this.h.ay();
        this.f = (int) this.h.ay();
    }

    public final void a(String str, String str2, String str3, Context context, String str4, String str5, a aVar) {
        if (aVar != null) {
            this.k = str5;
            this.j = str4;
            this.i = aVar;
            a(str, str2, str3, context);
            return;
        }
        throw new NullPointerException("OverrideUrlLoadingListener can not be null");
    }

    public final void a(String str, String str2, String str3, Context context, String str4, a aVar) {
        if (aVar != null) {
            this.j = str4;
            this.i = aVar;
            a(str, str2, str3, context);
            return;
        }
        throw new NullPointerException("OverrideUrlLoadingListener can not be null");
    }

    private void a(String str, String str2, String str3, Context context) {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            a(str, str2, str3, context, this.j);
            return;
        }
        final String str4 = str;
        final String str5 = str2;
        final String str6 = str3;
        final Context context2 = context;
        this.g.post(new Runnable() {
            public final void run() {
                f.this.a(str4, str5, str6, context2, f.this.j);
            }
        });
    }

    /* access modifiers changed from: private */
    public void b() {
        d();
        c();
    }

    /* access modifiers changed from: private */
    public void c() {
        this.g.removeCallbacks(this.s);
    }

    /* access modifiers changed from: private */
    public void d() {
        this.g.removeCallbacks(this.r);
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, String str3, Context context, String str4) {
        try {
            this.l = new WebView(context);
            this.l.getSettings().setJavaScriptEnabled(true);
            this.l.getSettings().setCacheMode(2);
            this.l.getSettings().setLoadsImagesAutomatically(false);
            final String str5 = str3;
            final String str6 = str2;
            final Context context2 = context;
            final String str7 = str;
            this.l.setWebViewClient(new WebViewClient() {
                public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                    try {
                        webView.loadUrl("javascript:window.navigator.vibrate([]);");
                        boolean z = false;
                        if (f.this.q) {
                            int unused = f.this.o = 0;
                            f.c(f.this);
                            return;
                        }
                        f.this.c = false;
                        if (webView.getTag() == null) {
                            webView.setTag("has_first_started");
                        } else {
                            f.this.b = true;
                        }
                        synchronized (f.d) {
                            String str2 = "加载页面-开始：";
                            if (f.this.b || f.this.c) {
                                z = true;
                            }
                            if (z) {
                                str2 = str2 + "（重定向）";
                            }
                            if (URLUtil.isHttpsUrl(str)) {
                                g.d(f.d, str2 + str);
                            } else {
                                g.b(f.d, str2 + str);
                            }
                            String unused2 = f.this.j = str;
                            if (f.this.i == null || !f.this.i.a(str)) {
                                f.f(f.this);
                            } else {
                                boolean unused3 = f.this.q = true;
                                f.c(f.this);
                            }
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }

                /* JADX WARNING: Code restructure failed: missing block: B:17:0x0069, code lost:
                    if (com.mintegral.msdk.click.f.i(r3.e) == false) goto L_0x0095;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:18:0x006b, code lost:
                    r4 = new java.util.HashMap();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:19:0x007a, code lost:
                    if (com.mintegral.msdk.click.f.j(r3.e).getUrl() == null) goto L_0x008b;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:20:0x007c, code lost:
                    r4.put(io.fabric.sdk.android.services.network.HttpRequest.HEADER_REFERER, com.mintegral.msdk.click.f.j(r3.e).getUrl());
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:21:0x008b, code lost:
                    com.mintegral.msdk.click.f.j(r3.e).loadUrl(r5, r4);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:22:0x0095, code lost:
                    com.mintegral.msdk.click.f.j(r3.e).loadUrl(r5);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:23:0x009e, code lost:
                    return true;
                 */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final boolean shouldOverrideUrlLoading(android.webkit.WebView r4, java.lang.String r5) {
                    /*
                        r3 = this;
                        java.lang.String r4 = com.mintegral.msdk.click.f.d
                        monitor-enter(r4)
                        java.lang.String r0 = com.mintegral.msdk.click.f.d     // Catch:{ all -> 0x009f }
                        java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x009f }
                        java.lang.String r2 = "override js跳转："
                        r1.<init>(r2)     // Catch:{ all -> 0x009f }
                        r1.append(r5)     // Catch:{ all -> 0x009f }
                        java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x009f }
                        com.mintegral.msdk.base.utils.g.a(r0, r1)     // Catch:{ all -> 0x009f }
                        com.mintegral.msdk.click.f r0 = com.mintegral.msdk.click.f.this     // Catch:{ all -> 0x009f }
                        r1 = 1
                        r0.c = r1     // Catch:{ all -> 0x009f }
                        com.mintegral.msdk.click.f r0 = com.mintegral.msdk.click.f.this     // Catch:{ all -> 0x009f }
                        r0.d()     // Catch:{ all -> 0x009f }
                        com.mintegral.msdk.click.f r0 = com.mintegral.msdk.click.f.this     // Catch:{ all -> 0x009f }
                        boolean r0 = r0.q     // Catch:{ all -> 0x009f }
                        if (r0 == 0) goto L_0x0038
                        com.mintegral.msdk.click.f r5 = com.mintegral.msdk.click.f.this     // Catch:{ all -> 0x009f }
                        r5.c()     // Catch:{ all -> 0x009f }
                        com.mintegral.msdk.click.f r5 = com.mintegral.msdk.click.f.this     // Catch:{ all -> 0x009f }
                        com.mintegral.msdk.click.f.c(r5)     // Catch:{ all -> 0x009f }
                        monitor-exit(r4)     // Catch:{ all -> 0x009f }
                        return r1
                    L_0x0038:
                        com.mintegral.msdk.click.f r0 = com.mintegral.msdk.click.f.this     // Catch:{ all -> 0x009f }
                        java.lang.String unused = r0.j = r5     // Catch:{ all -> 0x009f }
                        com.mintegral.msdk.click.f r0 = com.mintegral.msdk.click.f.this     // Catch:{ all -> 0x009f }
                        com.mintegral.msdk.click.f$a r0 = r0.i     // Catch:{ all -> 0x009f }
                        if (r0 == 0) goto L_0x0062
                        com.mintegral.msdk.click.f r0 = com.mintegral.msdk.click.f.this     // Catch:{ all -> 0x009f }
                        com.mintegral.msdk.click.f$a r0 = r0.i     // Catch:{ all -> 0x009f }
                        boolean r0 = r0.b(r5)     // Catch:{ all -> 0x009f }
                        if (r0 == 0) goto L_0x0062
                        com.mintegral.msdk.click.f r5 = com.mintegral.msdk.click.f.this     // Catch:{ all -> 0x009f }
                        boolean unused = r5.q = true     // Catch:{ all -> 0x009f }
                        com.mintegral.msdk.click.f r5 = com.mintegral.msdk.click.f.this     // Catch:{ all -> 0x009f }
                        r5.c()     // Catch:{ all -> 0x009f }
                        com.mintegral.msdk.click.f r5 = com.mintegral.msdk.click.f.this     // Catch:{ all -> 0x009f }
                        com.mintegral.msdk.click.f.c(r5)     // Catch:{ all -> 0x009f }
                        monitor-exit(r4)     // Catch:{ all -> 0x009f }
                        return r1
                    L_0x0062:
                        monitor-exit(r4)     // Catch:{ all -> 0x009f }
                        com.mintegral.msdk.click.f r4 = com.mintegral.msdk.click.f.this
                        boolean r4 = r4.m
                        if (r4 == 0) goto L_0x0095
                        java.util.HashMap r4 = new java.util.HashMap
                        r4.<init>()
                        com.mintegral.msdk.click.f r0 = com.mintegral.msdk.click.f.this
                        android.webkit.WebView r0 = r0.l
                        java.lang.String r0 = r0.getUrl()
                        if (r0 == 0) goto L_0x008b
                        java.lang.String r0 = "Referer"
                        com.mintegral.msdk.click.f r2 = com.mintegral.msdk.click.f.this
                        android.webkit.WebView r2 = r2.l
                        java.lang.String r2 = r2.getUrl()
                        r4.put(r0, r2)
                    L_0x008b:
                        com.mintegral.msdk.click.f r0 = com.mintegral.msdk.click.f.this
                        android.webkit.WebView r0 = r0.l
                        r0.loadUrl(r5, r4)
                        goto L_0x009e
                    L_0x0095:
                        com.mintegral.msdk.click.f r4 = com.mintegral.msdk.click.f.this
                        android.webkit.WebView r4 = r4.l
                        r4.loadUrl(r5)
                    L_0x009e:
                        return r1
                    L_0x009f:
                        r5 = move-exception
                        monitor-exit(r4)     // Catch:{ all -> 0x009f }
                        throw r5
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.click.f.AnonymousClass2.shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String):boolean");
                }

                public final void onReceivedError(WebView webView, int i, String str, String str2) {
                    String a2 = f.d;
                    g.b(a2, "onReceivedError: errno = " + i + ", url: " + webView.getUrl() + ",\n onReceivedError：, description: " + str + ", failingUrl: " + str2);
                    synchronized (f.d) {
                        boolean unused = f.this.q = true;
                        f.this.b();
                        f.c(f.this);
                    }
                    if (f.this.i != null) {
                        f.this.i.a(webView.getUrl(), str, f.this.n);
                    }
                }

                public final void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
                    try {
                        String a2 = f.d;
                        g.a(a2, "onReceivedSslError IS_SP_CBT_CF:" + MIntegralConstans.IS_SP_CBT_CF);
                        if (MIntegralConstans.IS_SP_CBT_CF && sslErrorHandler != null) {
                            sslErrorHandler.cancel();
                        }
                        if (!TextUtils.isEmpty(str5) && !TextUtils.isEmpty(str6)) {
                            new com.mintegral.msdk.base.common.e.b(context2).a(str7, str6, str5, webView.getUrl());
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }

                public final void onPageFinished(WebView webView, String str) {
                    super.onPageFinished(webView, str);
                    try {
                        webView.loadUrl("javascript:window.navigator.vibrate([]);");
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            });
            this.l.setWebChromeClient(new WebChromeClient() {
                public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
                    return true;
                }

                public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
                    return true;
                }

                public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
                    return true;
                }

                public final void onProgressChanged(WebView webView, int i) {
                    if (i == 100) {
                        try {
                            String a2 = f.d;
                            g.b(a2, "加载页面-进度完成：" + webView.getUrl());
                            webView.loadUrl("javascript:window.navigator.vibrate([]);");
                            if (!f.this.q && !f.this.c) {
                                f.m(f.this);
                            }
                            if (f.this.i != null) {
                                f.this.i.c(webView.getUrl());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            if (!TextUtils.isEmpty(this.k)) {
                this.l.getSettings().setDefaultTextEncodingName("utf-8");
                this.f = 2000;
                this.e = 2000;
                g.b(d, this.k);
                this.l.loadDataWithBaseURL(str4, this.k, "*/*", "utf-8", str4);
            } else if (this.m) {
                HashMap hashMap = new HashMap();
                if (this.l.getUrl() != null) {
                    hashMap.put(HttpRequest.HEADER_REFERER, this.l.getUrl());
                }
                this.l.loadUrl(str4, hashMap);
            } else {
                this.l.loadUrl(str4);
            }
        } catch (Throwable th) {
            try {
                if (this.i != null) {
                    this.i.a(this.j, th.getMessage(), this.n);
                }
            } catch (Exception unused) {
            }
        }
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0016 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void c(com.mintegral.msdk.click.f r3) {
        /*
            java.lang.String r0 = com.mintegral.msdk.click.f.d
            monitor-enter(r0)
            r3.b()     // Catch:{ Exception -> 0x001e, Throwable -> 0x0016 }
            com.mintegral.msdk.click.f$a r1 = r3.i     // Catch:{ Exception -> 0x001e, Throwable -> 0x0016 }
            if (r1 == 0) goto L_0x0025
            com.mintegral.msdk.click.f$a r1 = r3.i     // Catch:{ Exception -> 0x001e, Throwable -> 0x0016 }
            java.lang.String r2 = r3.j     // Catch:{ Exception -> 0x001e, Throwable -> 0x0016 }
            java.lang.String r3 = r3.n     // Catch:{ Exception -> 0x001e, Throwable -> 0x0016 }
            r1.a(r2, r3)     // Catch:{ Exception -> 0x001e, Throwable -> 0x0016 }
            goto L_0x0025
        L_0x0014:
            r3 = move-exception
            goto L_0x0027
        L_0x0016:
            java.lang.String r3 = com.mintegral.msdk.click.f.d     // Catch:{ all -> 0x0014 }
            java.lang.String r1 = "webview colse to failed"
            com.mintegral.msdk.base.utils.g.d(r3, r1)     // Catch:{ all -> 0x0014 }
            goto L_0x0025
        L_0x001e:
            java.lang.String r3 = com.mintegral.msdk.click.f.d     // Catch:{ all -> 0x0014 }
            java.lang.String r1 = "webview colse to failed"
            com.mintegral.msdk.base.utils.g.d(r3, r1)     // Catch:{ all -> 0x0014 }
        L_0x0025:
            monitor-exit(r0)     // Catch:{ all -> 0x0014 }
            return
        L_0x0027:
            monitor-exit(r0)     // Catch:{ all -> 0x0014 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.click.f.c(com.mintegral.msdk.click.f):void");
    }

    static /* synthetic */ void f(f fVar) {
        fVar.c();
        fVar.g.postDelayed(fVar.s, (long) fVar.e);
    }

    static /* synthetic */ void m(f fVar) {
        fVar.d();
        fVar.g.postDelayed(fVar.r, (long) fVar.f);
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x001b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void p(com.mintegral.msdk.click.f r3) {
        /*
            java.lang.String r0 = com.mintegral.msdk.click.f.d
            monitor-enter(r0)
            r3.b()     // Catch:{ Exception -> 0x0023, Throwable -> 0x001b }
            android.webkit.WebView r1 = r3.l     // Catch:{ Exception -> 0x0023, Throwable -> 0x001b }
            r1.destroy()     // Catch:{ Exception -> 0x0023, Throwable -> 0x001b }
            com.mintegral.msdk.click.f$a r1 = r3.i     // Catch:{ Exception -> 0x0023, Throwable -> 0x001b }
            if (r1 == 0) goto L_0x002a
            com.mintegral.msdk.click.f$a r1 = r3.i     // Catch:{ Exception -> 0x0023, Throwable -> 0x001b }
            java.lang.String r2 = r3.j     // Catch:{ Exception -> 0x0023, Throwable -> 0x001b }
            java.lang.String r3 = r3.n     // Catch:{ Exception -> 0x0023, Throwable -> 0x001b }
            r1.a(r2, r3)     // Catch:{ Exception -> 0x0023, Throwable -> 0x001b }
            goto L_0x002a
        L_0x0019:
            r3 = move-exception
            goto L_0x002c
        L_0x001b:
            java.lang.String r3 = com.mintegral.msdk.click.f.d     // Catch:{ all -> 0x0019 }
            java.lang.String r1 = "webview colse to failed"
            com.mintegral.msdk.base.utils.g.d(r3, r1)     // Catch:{ all -> 0x0019 }
            goto L_0x002a
        L_0x0023:
            java.lang.String r3 = com.mintegral.msdk.click.f.d     // Catch:{ all -> 0x0019 }
            java.lang.String r1 = "webview colse to failed"
            com.mintegral.msdk.base.utils.g.d(r3, r1)     // Catch:{ all -> 0x0019 }
        L_0x002a:
            monitor-exit(r0)     // Catch:{ all -> 0x0019 }
            return
        L_0x002c:
            monitor-exit(r0)     // Catch:{ all -> 0x0019 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.click.f.p(com.mintegral.msdk.click.f):void");
    }
}
