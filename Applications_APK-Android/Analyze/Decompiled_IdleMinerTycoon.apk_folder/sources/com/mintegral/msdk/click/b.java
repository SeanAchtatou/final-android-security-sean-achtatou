package com.mintegral.msdk.click;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import android.widget.Toast;
import com.appsflyer.share.Constants;
import com.google.android.gms.drive.DriveFile;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.r;
import com.mintegral.msdk.d.a;
import com.mintegral.msdk.mtgbid.common.BidResponsedEx;
import java.io.File;
import java.util.List;
import java.util.Map;

/* compiled from: CommonClickUtil */
public final class b {
    static Handler a = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            super.handleMessage(message);
            try {
                if (message.what == 1000) {
                    int i = message.arg1;
                    int i2 = message.arg2;
                    Bundle data = message.getData();
                    String str = "";
                    String str2 = "";
                    if (data != null) {
                        str = data.getString("rid");
                        str2 = data.getString(BidResponsedEx.KEY_CID);
                    }
                    new com.mintegral.msdk.base.common.e.b(a.d().h()).a(i, i2, str, str2);
                }
            } catch (Throwable th) {
                if (MIntegralConstans.DEBUG) {
                    th.printStackTrace();
                }
            }
        }
    };

    public static String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        if (str.lastIndexOf(Constants.URL_PATH_DELIMITER) == -1) {
            StringBuilder sb = new StringBuilder();
            sb.append(str.hashCode());
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str.hashCode() + str.substring(str.lastIndexOf(Constants.URL_PATH_DELIMITER) + 1).hashCode());
        return sb2.toString();
    }

    public static void a(Context context, String str) {
        if (str != null && context != null) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent.addFlags(DriveFile.MODE_READ_ONLY);
                ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent, 65536);
                if (resolveActivity != null) {
                    intent.setClassName(resolveActivity.activityInfo.packageName, resolveActivity.activityInfo.name);
                }
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(str));
                    intent2.addFlags(DriveFile.MODE_READ_ONLY);
                    context.startActivity(intent2);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x0111 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r5, android.net.Uri r6, java.lang.String r7, java.lang.String r8) {
        /*
            java.io.File r0 = new java.io.File     // Catch:{ URISyntaxException -> 0x000f }
            java.net.URI r1 = new java.net.URI     // Catch:{ URISyntaxException -> 0x000f }
            java.lang.String r6 = r6.toString()     // Catch:{ URISyntaxException -> 0x000f }
            r1.<init>(r6)     // Catch:{ URISyntaxException -> 0x000f }
            r0.<init>(r1)     // Catch:{ URISyntaxException -> 0x000f }
            goto L_0x0014
        L_0x000f:
            r6 = move-exception
            r6.printStackTrace()
            r0 = 0
        L_0x0014:
            android.content.Intent r6 = new android.content.Intent
            r6.<init>()
            r1 = 268435456(0x10000000, float:2.5243549E-29)
            r6.addFlags(r1)
            java.lang.String r1 = "android.intent.action.VIEW"
            r6.setAction(r1)
            r1 = 3
            r2 = -1
            boolean r3 = com.mintegral.msdk.base.utils.t.b(r5)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            if (r3 == 0) goto L_0x0095
            boolean r3 = com.mintegral.msdk.base.utils.t.d(r5)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            if (r3 == 0) goto L_0x007c
            int r3 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            r4 = 24
            if (r3 < r4) goto L_0x006d
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            r3.<init>()     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            android.content.Context r4 = r5.getApplicationContext()     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            java.lang.String r4 = r4.getPackageName()     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            r3.append(r4)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            java.lang.String r4 = ".mtgFileProvider"
            r3.append(r4)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            android.net.Uri r0 = com.mintegral.msdk.base.utils.MTGFileProvider.getUriForFile(r5, r3, r0)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            if (r0 == 0) goto L_0x0069
            r3 = 1
            r6.addFlags(r3)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            android.content.ContentResolver r3 = r5.getContentResolver()     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            java.lang.String r3 = r3.getType(r0)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            r6.setDataAndType(r0, r3)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            r5.startActivity(r6)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            goto L_0x0098
        L_0x0069:
            a(r5, r7, r8)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            goto L_0x0098
        L_0x006d:
            android.net.Uri r3 = android.net.Uri.fromFile(r0)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            java.lang.String r0 = a(r0)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            r6.setDataAndType(r3, r0)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            r5.startActivity(r6)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            goto L_0x0098
        L_0x007c:
            boolean r3 = com.mintegral.msdk.base.utils.t.e(r5)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            if (r3 == 0) goto L_0x0086
            a(r5, r7, r8)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            goto L_0x0098
        L_0x0086:
            android.net.Uri r3 = android.net.Uri.fromFile(r0)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            java.lang.String r0 = a(r0)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            r6.setDataAndType(r3, r0)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            r5.startActivity(r6)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
            goto L_0x0098
        L_0x0095:
            a(r5, r7, r8)     // Catch:{ Exception -> 0x018a, Throwable -> 0x0111 }
        L_0x0098:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r8)
            java.lang.String r7 = "downloadType"
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            java.lang.Integer r7 = java.lang.Integer.valueOf(r2)
            java.lang.Object r6 = com.mintegral.msdk.base.utils.r.b(r5, r6, r7)
            java.lang.Integer r6 = (java.lang.Integer) r6
            int r6 = r6.intValue()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r8)
            java.lang.String r0 = "linkType"
            r7.append(r0)
            java.lang.String r7 = r7.toString()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r2)
            java.lang.Object r7 = com.mintegral.msdk.base.utils.r.b(r5, r7, r0)
            java.lang.Integer r7 = (java.lang.Integer) r7
            int r7 = r7.intValue()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r8)
            java.lang.String r2 = "rid"
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.String r2 = ""
            java.lang.Object r0 = com.mintegral.msdk.base.utils.r.b(r5, r0, r2)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r8)
            java.lang.String r8 = "cid"
            r2.append(r8)
            java.lang.String r8 = r2.toString()
            java.lang.String r2 = ""
            java.lang.Object r5 = com.mintegral.msdk.base.utils.r.b(r5, r8, r2)
            java.lang.String r5 = (java.lang.String) r5
            if (r6 == r1) goto L_0x010d
            a(r6, r7, r0, r5)
        L_0x010d:
            return
        L_0x010e:
            r6 = move-exception
            goto L_0x0207
        L_0x0111:
            a(r5, r7, r8)     // Catch:{ all -> 0x010e }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r8)
            java.lang.String r7 = "downloadType"
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            java.lang.Integer r7 = java.lang.Integer.valueOf(r2)
            java.lang.Object r6 = com.mintegral.msdk.base.utils.r.b(r5, r6, r7)
            java.lang.Integer r6 = (java.lang.Integer) r6
            int r6 = r6.intValue()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r8)
            java.lang.String r0 = "linkType"
            r7.append(r0)
            java.lang.String r7 = r7.toString()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r2)
            java.lang.Object r7 = com.mintegral.msdk.base.utils.r.b(r5, r7, r0)
            java.lang.Integer r7 = (java.lang.Integer) r7
            int r7 = r7.intValue()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r8)
            java.lang.String r2 = "rid"
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.String r2 = ""
            java.lang.Object r0 = com.mintegral.msdk.base.utils.r.b(r5, r0, r2)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r8)
            java.lang.String r8 = "cid"
            r2.append(r8)
            java.lang.String r8 = r2.toString()
            java.lang.String r2 = ""
            java.lang.Object r5 = com.mintegral.msdk.base.utils.r.b(r5, r8, r2)
            java.lang.String r5 = (java.lang.String) r5
            if (r6 == r1) goto L_0x0189
            a(r6, r7, r0, r5)
        L_0x0189:
            return
        L_0x018a:
            r6 = move-exception
            r6.printStackTrace()     // Catch:{ all -> 0x010e }
            a(r5, r7, r8)     // Catch:{ all -> 0x010e }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r8)
            java.lang.String r7 = "downloadType"
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            java.lang.Integer r7 = java.lang.Integer.valueOf(r2)
            java.lang.Object r6 = com.mintegral.msdk.base.utils.r.b(r5, r6, r7)
            java.lang.Integer r6 = (java.lang.Integer) r6
            int r6 = r6.intValue()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r8)
            java.lang.String r0 = "linkType"
            r7.append(r0)
            java.lang.String r7 = r7.toString()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r2)
            java.lang.Object r7 = com.mintegral.msdk.base.utils.r.b(r5, r7, r0)
            java.lang.Integer r7 = (java.lang.Integer) r7
            int r7 = r7.intValue()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r8)
            java.lang.String r2 = "rid"
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.String r2 = ""
            java.lang.Object r0 = com.mintegral.msdk.base.utils.r.b(r5, r0, r2)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r8)
            java.lang.String r8 = "cid"
            r2.append(r8)
            java.lang.String r8 = r2.toString()
            java.lang.String r2 = ""
            java.lang.Object r5 = com.mintegral.msdk.base.utils.r.b(r5, r8, r2)
            java.lang.String r5 = (java.lang.String) r5
            if (r6 == r1) goto L_0x0206
            a(r6, r7, r0, r5)
        L_0x0206:
            return
        L_0x0207:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r8)
            java.lang.String r0 = "downloadType"
            r7.append(r0)
            java.lang.String r7 = r7.toString()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r2)
            java.lang.Object r7 = com.mintegral.msdk.base.utils.r.b(r5, r7, r0)
            java.lang.Integer r7 = (java.lang.Integer) r7
            int r7 = r7.intValue()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r8)
            java.lang.String r3 = "linkType"
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            java.lang.Object r0 = com.mintegral.msdk.base.utils.r.b(r5, r0, r2)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r8)
            java.lang.String r3 = "rid"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.String r3 = ""
            java.lang.Object r2 = com.mintegral.msdk.base.utils.r.b(r5, r2, r3)
            java.lang.String r2 = (java.lang.String) r2
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r8)
            java.lang.String r8 = "cid"
            r3.append(r8)
            java.lang.String r8 = r3.toString()
            java.lang.String r3 = ""
            java.lang.Object r5 = com.mintegral.msdk.base.utils.r.b(r5, r8, r3)
            java.lang.String r5 = (java.lang.String) r5
            if (r7 == r1) goto L_0x027c
            a(r7, r0, r2, r5)
        L_0x027c:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.click.b.a(android.content.Context, android.net.Uri, java.lang.String, java.lang.String):void");
    }

    private static void a(int i, int i2, String str, String str2) {
        try {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                new com.mintegral.msdk.base.common.e.b(a.d().h()).a(i, i2, str, str2);
                return;
            }
            Message obtain = Message.obtain();
            obtain.what = 1000;
            obtain.arg1 = i;
            obtain.arg2 = i2;
            Bundle bundle = new Bundle();
            bundle.putString("rid", str);
            bundle.putString(BidResponsedEx.KEY_CID, str2);
            obtain.setData(bundle);
            a.sendMessage(obtain);
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }

    public static void a(Context context, String str, String str2) {
        try {
            Context h = a.d().h();
            r.a(h, str2 + "downloadType", 3);
            a(context, str);
            int intValue = ((Integer) r.b(context, str2 + "downloadType", -1)).intValue();
            int intValue2 = ((Integer) r.b(context, str2 + "linkType", -1)).intValue();
            a(intValue, intValue2, (String) r.b(context, str2 + "rid", ""), (String) r.b(context, str2 + BidResponsedEx.KEY_CID, ""));
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }

    private static String a(File file) {
        String name = file.getName();
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(name.substring(name.lastIndexOf(".") + 1, name.length()).toLowerCase());
    }

    public static boolean b(Context context, String str) {
        if (str == null || "".equals(str)) {
            return false;
        }
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 8192);
            if (packageInfo != null) {
                return str.equals(packageInfo.packageName);
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    public static boolean c(Context context, String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return false;
            }
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        } catch (Throwable th) {
            g.c("SDKCLICK", th.getMessage(), th);
            return false;
        }
    }

    public static void d(Context context, String str) {
        Intent launchIntentForPackage;
        List<ResolveInfo> queryIntentActivities;
        ResolveInfo next;
        try {
            if (!TextUtils.isEmpty(str) && b(context, str) && (launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(str)) != null && (queryIntentActivities = context.getPackageManager().queryIntentActivities(launchIntentForPackage, 0)) != null && queryIntentActivities.size() > 0 && (next = queryIntentActivities.iterator().next()) != null) {
                ComponentName componentName = new ComponentName(next.activityInfo.packageName, next.activityInfo.name);
                Intent intent = new Intent();
                intent.setComponent(componentName);
                intent.addFlags(DriveFile.MODE_READ_ONLY);
                context.startActivity(intent);
            }
        } catch (Exception e) {
            Toast.makeText(context, "The app connot start up", 0).show();
            e.printStackTrace();
        }
    }

    private static String a(String str, List<String> list, String str2) {
        if (list != null) {
            for (String next : list) {
                if (!TextUtils.isEmpty(next)) {
                    str = str.replaceAll(next, str2);
                }
            }
        }
        return str;
    }

    public static String a(String str, String str2, String str3) {
        Map<String, a.C0054a> bf;
        try {
            if (TextUtils.isEmpty(str)) {
                return str;
            }
            String host = Uri.parse(str).getHost();
            com.mintegral.msdk.d.b.a();
            com.mintegral.msdk.d.a b = com.mintegral.msdk.d.b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (b == null || (bf = b.bf()) == null || TextUtils.isEmpty(host)) {
                return str;
            }
            for (Map.Entry<String, a.C0054a> key : bf.entrySet()) {
                String str4 = (String) key.getKey();
                if (!TextUtils.isEmpty(str4) && host.contains(str4)) {
                    a.C0054a aVar = bf.get(str4);
                    String a2 = a(str, aVar.d(), String.valueOf((float) c.o(com.mintegral.msdk.base.controller.a.d().h())));
                    try {
                        str = a(a2, aVar.c(), String.valueOf((float) c.n(com.mintegral.msdk.base.controller.a.d().h())));
                        String a3 = a(str, aVar.a(), str2);
                        try {
                            return a(a3, aVar.b(), str3);
                        } catch (Exception e) {
                            String str5 = a3;
                            e = e;
                            str = str5;
                            e.printStackTrace();
                            return str;
                        }
                    } catch (Exception e2) {
                        e = e2;
                        str = a2;
                        e.printStackTrace();
                        return str;
                    }
                }
            }
            return str;
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return str;
        }
    }
}
