package com.mintegral.msdk.b;

import android.content.Context;
import android.media.AudioManager;
import android.text.TextUtils;
import android.webkit.WebView;
import com.iab.omid.library.mintegral.Omid;
import com.iab.omid.library.mintegral.adsession.AdSession;
import com.iab.omid.library.mintegral.adsession.AdSessionConfiguration;
import com.iab.omid.library.mintegral.adsession.AdSessionContext;
import com.iab.omid.library.mintegral.adsession.Owner;
import com.iab.omid.library.mintegral.adsession.Partner;
import com.iab.omid.library.mintegral.adsession.VerificationScriptResource;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.b.c;
import com.mintegral.msdk.base.common.b.e;
import com.mintegral.msdk.base.common.net.a.f;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.out.MTGConfiguration;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OmsdkUtils */
public final class b {
    public static void a(final Context context) {
        if (TextUtils.isEmpty(MIntegralConstans.OMID_JS_SERVICE_URL)) {
            MIntegralConstans.OMID_JS_SERVICE_CONTENT = "";
            new com.mintegral.msdk.base.common.e.b(context).b("", "", "", "fetch OM failed, OMID_JS_SERVICE_URL null");
        } else if (context != null) {
            try {
                new a(context.getApplicationContext()).a(MIntegralConstans.OMID_JS_SERVICE_URL, new f() {
                    /* renamed from: d */
                    public final void a(String str) {
                        MIntegralConstans.OMID_JS_SERVICE_CONTENT = str;
                        g.a("OMSDK", "fetch OMJSContent success, content = " + str);
                        new Thread(new Runnable(str) {
                            final /* synthetic */ String a;

                            {
                                this.a = r1;
                            }

                            public final void run() {
                                try {
                                    File file = new File(e.b(c.MINTEGRAL_OTHER), "/omsdk/om_js_content.txt");
                                    if (file.exists()) {
                                        file.delete();
                                    }
                                    com.mintegral.msdk.base.utils.e.a(this.a.getBytes(), file);
                                } catch (Exception e) {
                                    g.a("OMSDK", e.getMessage());
                                }
                            }
                        }).start();
                    }

                    public final void a(String str) {
                        g.a("OMSDK", "fetch OMJSContent failed, errorCode = " + str);
                        new com.mintegral.msdk.base.common.e.b(context).b("", "", "", "fetch OM failed, request failed");
                    }
                });
            } catch (Exception e) {
                g.d("OMSDK", e.getMessage());
            }
        }
    }

    private static void c(Context context) {
        if (!Omid.isActive()) {
            Omid.activateWithOmidApiVersion(Omid.getVersion(), context.getApplicationContext());
        }
    }

    private static List<VerificationScriptResource> a(String str, Context context, String str2, String str3, String str4) {
        VerificationScriptResource verificationScriptResource;
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = new JSONArray(str);
            if (str != null) {
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject optJSONObject = jSONArray.optJSONObject(i);
                    String optString = optJSONObject.optString("vkey", "");
                    URL url = new URL(optJSONObject.optString("et_url", ""));
                    String optString2 = optJSONObject.optString("verification_p", "");
                    if (!TextUtils.isEmpty(optString2)) {
                        verificationScriptResource = VerificationScriptResource.createVerificationScriptResourceWithParameters(optString, url, optString2);
                    } else if (TextUtils.isEmpty(optString)) {
                        verificationScriptResource = VerificationScriptResource.createVerificationScriptResourceWithoutParameters(url);
                    } else {
                        verificationScriptResource = VerificationScriptResource.createVerificationScriptResourceWithoutParameters(optString, url);
                    }
                    arrayList.add(verificationScriptResource);
                }
            }
        } catch (IllegalArgumentException e) {
            g.d("OMSDK", e.getMessage());
            com.mintegral.msdk.base.common.e.b bVar = new com.mintegral.msdk.base.common.e.b(context);
            bVar.b(str2, str3, str4, "failed, exception " + e.getMessage());
        } catch (MalformedURLException e2) {
            g.d("OMSDK", e2.getMessage());
            com.mintegral.msdk.base.common.e.b bVar2 = new com.mintegral.msdk.base.common.e.b(context);
            bVar2.b(str2, str3, str4, "failed, exception " + e2.getMessage());
        } catch (JSONException e3) {
            g.d("OMSDK", e3.getMessage());
            com.mintegral.msdk.base.common.e.b bVar3 = new com.mintegral.msdk.base.common.e.b(context);
            bVar3.b(str2, str3, str4, "failed, exception " + e3.getMessage());
        }
        return arrayList;
    }

    public static AdSession a(Context context, WebView webView, String str) {
        AdSession adSession;
        try {
            c(context);
            adSession = AdSession.createAdSession(AdSessionConfiguration.createAdSessionConfiguration(Owner.NATIVE, null, false), AdSessionContext.createHtmlAdSessionContext(Partner.createPartner("Mintegral", MTGConfiguration.SDK_VERSION), webView, str));
            try {
                g.a("OMSDK", "adSession create success");
            } catch (IllegalArgumentException e) {
                e = e;
            }
        } catch (IllegalArgumentException e2) {
            e = e2;
            adSession = null;
            g.a("OMSDK", e.getMessage());
            return adSession;
        }
        return adSession;
    }

    public static AdSession a(Context context, boolean z, String str, String str2, String str3, String str4) {
        if (TextUtils.isEmpty(MIntegralConstans.OMID_JS_SERVICE_CONTENT)) {
            MIntegralConstans.OMID_JS_SERVICE_CONTENT = a();
        }
        AdSession adSession = null;
        if (TextUtils.isEmpty(MIntegralConstans.OMID_JS_SERVICE_CONTENT) || TextUtils.isEmpty(str)) {
            g.a("OMSDK", "createNativeAdSession: TextUtils.isEmpty(omid) = " + TextUtils.isEmpty(str) + " TextUtils.isEmpty(MIntegralConstans.OMID_JS_SERVICE_CONTENT) = " + TextUtils.isEmpty(MIntegralConstans.OMID_JS_SERVICE_CONTENT));
            new com.mintegral.msdk.base.common.e.b(context).b(str2, str3, str4, "failed, OMID_JS_SERVICE_CONTENT null or omid null");
            return null;
        }
        try {
            c(context);
            AdSession createAdSession = AdSession.createAdSession(AdSessionConfiguration.createAdSessionConfiguration(Owner.NATIVE, z ? Owner.NONE : Owner.NATIVE, false), AdSessionContext.createNativeAdSessionContext(Partner.createPartner("Mintegral", MTGConfiguration.SDK_VERSION), MIntegralConstans.OMID_JS_SERVICE_CONTENT, a(str, context, str2, str3, str4), str2));
            try {
                g.a("OMSDK", "adSession create success");
                return createAdSession;
            } catch (IllegalArgumentException e) {
                adSession = createAdSession;
                e = e;
                g.d("OMSDK", e.getMessage());
                com.mintegral.msdk.base.common.e.b bVar = new com.mintegral.msdk.base.common.e.b(context);
                bVar.b(str2, str3, str4, "failed, exception " + e.getMessage());
                return adSession;
            } catch (Exception e2) {
                adSession = createAdSession;
                e = e2;
                g.d("OMSDK", e.getMessage());
                com.mintegral.msdk.base.common.e.b bVar2 = new com.mintegral.msdk.base.common.e.b(context);
                bVar2.b(str2, str3, str4, "failed, exception " + e.getMessage());
                return adSession;
            }
        } catch (IllegalArgumentException e3) {
            e = e3;
            g.d("OMSDK", e.getMessage());
            com.mintegral.msdk.base.common.e.b bVar3 = new com.mintegral.msdk.base.common.e.b(context);
            bVar3.b(str2, str3, str4, "failed, exception " + e.getMessage());
            return adSession;
        } catch (Exception e4) {
            e = e4;
            g.d("OMSDK", e.getMessage());
            com.mintegral.msdk.base.common.e.b bVar22 = new com.mintegral.msdk.base.common.e.b(context);
            bVar22.b(str2, str3, str4, "failed, exception " + e.getMessage());
            return adSession;
        }
    }

    public static float b(Context context) {
        AudioManager audioManager = (AudioManager) context.getApplicationContext().getSystemService("audio");
        int i = -1;
        int streamMaxVolume = audioManager != null ? audioManager.getStreamMaxVolume(3) : -1;
        if (audioManager != null) {
            i = audioManager.getStreamVolume(3);
        }
        double d = (double) i;
        Double.isNaN(d);
        double d2 = (double) streamMaxVolume;
        Double.isNaN(d2);
        return (float) ((d * 100.0d) / d2);
    }

    private static String a() {
        try {
            return com.mintegral.msdk.base.utils.e.a(new File(e.b(c.MINTEGRAL_OTHER), "/omsdk/om_js_content.txt"));
        } catch (Exception e) {
            g.a("OMSDK", e.getMessage());
            return "";
        }
    }
}
