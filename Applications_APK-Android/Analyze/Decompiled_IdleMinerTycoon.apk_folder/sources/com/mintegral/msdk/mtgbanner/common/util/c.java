package com.mintegral.msdk.mtgbanner.common.util;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: ViewUtils */
public final class c {
    public static boolean a(View view) {
        Rect rect = new Rect();
        if (!(view.getGlobalVisibleRect(rect) && (rect.bottom - rect.top >= view.getMeasuredHeight()) && (rect.right - rect.left >= view.getMeasuredWidth()))) {
            return true;
        }
        ViewGroup viewGroup = view;
        while (viewGroup.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup2 = (ViewGroup) viewGroup.getParent();
            if (viewGroup2.getVisibility() != 0) {
                return true;
            }
            for (int a = a(viewGroup, viewGroup2) + 1; a < viewGroup2.getChildCount(); a++) {
                Rect rect2 = new Rect();
                view.getGlobalVisibleRect(rect2);
                View childAt = viewGroup2.getChildAt(a);
                Rect rect3 = new Rect();
                childAt.getGlobalVisibleRect(rect3);
                if (Rect.intersects(rect2, rect3) && (Math.min(rect2.right, rect3.right) - Math.max(rect2.left, rect3.left)) * (Math.min(rect2.bottom, rect3.bottom) - Math.max(rect2.top, rect3.top)) * 2 >= view.getMeasuredHeight() * view.getMeasuredWidth()) {
                    return true;
                }
            }
            viewGroup = viewGroup2;
        }
        return false;
    }

    private static int a(View view, ViewGroup viewGroup) {
        int i = 0;
        while (i < viewGroup.getChildCount() && viewGroup.getChildAt(i) != view) {
            i++;
        }
        return i;
    }

    public static boolean b(View view) {
        return view != null && view.getAlpha() >= 0.5f;
    }
}
