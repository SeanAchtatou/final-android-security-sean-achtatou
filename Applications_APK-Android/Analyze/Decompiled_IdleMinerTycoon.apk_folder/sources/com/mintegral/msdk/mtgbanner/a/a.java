package com.mintegral.msdk.mtgbanner.a;

import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.d.d;
import com.mintegral.msdk.mtgbanner.common.a.b;
import com.mintegral.msdk.mtgbanner.common.c.c;
import com.mintegral.msdk.out.BannerAdListener;
import com.mintegral.msdk.out.BannerSize;
import com.mintegral.msdk.out.MTGBannerView;
import java.util.List;

/* compiled from: BannerController */
public final class a {
    /* access modifiers changed from: private */
    public static String a = "BannerController";
    /* access modifiers changed from: private */
    public String b;
    private boolean c;
    private int d;
    /* access modifiers changed from: private */
    public MTGBannerView e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public int h;
    private int i;
    /* access modifiers changed from: private */
    public BannerAdListener j;
    /* access modifiers changed from: private */
    public CampaignUnit k;
    private c l;
    private d m;
    private com.mintegral.msdk.d.c n;
    private boolean o;
    private boolean p;
    private boolean q;
    /* access modifiers changed from: private */
    public boolean r;
    /* access modifiers changed from: private */
    public boolean s;
    private com.mintegral.msdk.mtgbanner.common.b.c t = new com.mintegral.msdk.mtgbanner.common.b.c() {
        public final void a(List<CampaignEx> list) {
            if (a.this.j != null) {
                a.this.j.onLoadSuccessed();
            }
            g.d(a.a, "onShowSuccessed:");
        }

        public final void a(CampaignEx campaignEx, boolean z) {
            a.this.b();
            if (a.this.j != null && !z) {
                a.this.j.onLogImpression();
            }
        }

        public final void a(String str) {
            a.this.b(str);
        }

        public final void a() {
            if (a.this.j != null) {
                a.this.j.onClick();
            }
        }

        public final void b() {
            if (a.this.j != null) {
                a.this.j.onLeaveApp();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.mintegral.msdk.mtgbanner.a.a.a(com.mintegral.msdk.mtgbanner.a.a, boolean):boolean
         arg types: [com.mintegral.msdk.mtgbanner.a.a, int]
         candidates:
          com.mintegral.msdk.mtgbanner.a.a.a(com.mintegral.msdk.mtgbanner.a.a, com.mintegral.msdk.base.entity.CampaignUnit):com.mintegral.msdk.base.entity.CampaignUnit
          com.mintegral.msdk.mtgbanner.a.a.a(com.mintegral.msdk.mtgbanner.a.a, java.lang.String):void
          com.mintegral.msdk.mtgbanner.a.a.a(com.mintegral.msdk.mtgbanner.a.a, boolean):boolean */
        public final void c() {
            if (a.this.j != null) {
                a.this.j.showFullScreen();
                boolean unused = a.this.s = true;
                com.mintegral.msdk.mtgbanner.common.c.a.a().a(2, a.this.b, null, null);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.mintegral.msdk.mtgbanner.a.a.a(com.mintegral.msdk.mtgbanner.a.a, boolean):boolean
         arg types: [com.mintegral.msdk.mtgbanner.a.a, int]
         candidates:
          com.mintegral.msdk.mtgbanner.a.a.a(com.mintegral.msdk.mtgbanner.a.a, com.mintegral.msdk.base.entity.CampaignUnit):com.mintegral.msdk.base.entity.CampaignUnit
          com.mintegral.msdk.mtgbanner.a.a.a(com.mintegral.msdk.mtgbanner.a.a, java.lang.String):void
          com.mintegral.msdk.mtgbanner.a.a.a(com.mintegral.msdk.mtgbanner.a.a, boolean):boolean */
        public final void d() {
            if (a.this.j != null) {
                a.this.j.closeFullScreen();
                boolean unused = a.this.s = false;
                com.mintegral.msdk.mtgbanner.common.c.a a2 = com.mintegral.msdk.mtgbanner.common.c.a.a();
                String b = a.this.b;
                a2.a(3, b, new b(a.this.g + "x" + a.this.f, a.this.h * 1000), a.this.u);
            }
        }
    };
    /* access modifiers changed from: private */
    public com.mintegral.msdk.mtgbanner.common.b.b u = new com.mintegral.msdk.mtgbanner.common.b.b() {
        public final void a(CampaignUnit campaignUnit) {
            CampaignUnit unused = a.this.k = campaignUnit;
        }

        public final void a(String str, boolean z) {
            if (a.this.j != null) {
                a.this.j.onLoadFailed(str);
            }
            String c = a.a;
            g.d(c, "onCampaignFail:" + str);
            a.this.b();
            com.mintegral.msdk.base.common.e.c.a(com.mintegral.msdk.base.controller.a.d().h(), str, a.this.b, z);
        }

        public final void a(boolean z) {
            if (a.this.k != null) {
                com.mintegral.msdk.base.common.e.c.a(com.mintegral.msdk.base.controller.a.d().h(), a.this.k.getAds(), a.this.b, z);
            }
            if (a.this.e != null) {
                boolean unused = a.this.r = true;
                a.this.e();
            }
        }

        public final void b(boolean z) {
            if (a.this.j != null) {
                a.this.j.onLoadFailed("banner res load failed");
            }
            g.d(a.a, "onResourceFail:");
            a.this.b();
            com.mintegral.msdk.base.common.e.c.a(com.mintegral.msdk.base.controller.a.d().h(), "banner res load failed", a.this.b, z);
        }
    };

    private static int b(int i2) {
        if (i2 <= 0) {
            return i2;
        }
        if (i2 < 10) {
            return 10;
        }
        if (i2 > 180) {
            return 180;
        }
        return i2;
    }

    public a(MTGBannerView mTGBannerView, BannerSize bannerSize, String str) {
        this.e = mTGBannerView;
        if (bannerSize != null) {
            this.f = bannerSize.getHeight();
            this.g = bannerSize.getWidth();
        }
        this.b = str;
        String j2 = com.mintegral.msdk.base.controller.a.d().j();
        String k2 = com.mintegral.msdk.base.controller.a.d().k();
        if (this.n == null) {
            this.n = new com.mintegral.msdk.d.c();
        }
        this.n.a(com.mintegral.msdk.base.controller.a.d().h(), j2, k2, this.b);
        d();
    }

    public final void a(BannerSize bannerSize) {
        if (bannerSize != null) {
            this.f = bannerSize.getHeight();
            this.g = bannerSize.getWidth();
        }
    }

    public final void a(int i2) {
        this.i = b(i2);
        this.h = this.i;
    }

    public final void a(boolean z) {
        this.c = z;
        this.d = z ? 1 : 2;
    }

    private void d() {
        com.mintegral.msdk.d.b.a();
        this.m = com.mintegral.msdk.d.b.c(com.mintegral.msdk.base.controller.a.d().j(), this.b);
        if (this.m == null) {
            this.m = d.b(this.b);
        }
        if (this.i == 0) {
            this.h = b(this.m.a());
        }
        if (this.d == 0) {
            boolean z = true;
            if (this.m.b() != 1) {
                z = false;
            }
            this.c = z;
            if (this.l != null) {
                this.l.a(this.c);
            }
        }
    }

    public final void a(String str) {
        if (this.f > 0 && this.g > 0) {
            b bVar = new b(this.g + "x" + this.f, this.h * 1000);
            bVar.a(str);
            com.mintegral.msdk.mtgbanner.common.c.a.a().a(this.b, bVar, this.u);
            com.mintegral.msdk.mtgbanner.common.c.a.a().a(1, this.b, bVar, this.u);
        } else if (this.j != null) {
            this.j.onLoadFailed("banner load failed because params are exception");
        }
    }

    public final void a(BannerAdListener bannerAdListener) {
        this.j = bannerAdListener;
    }

    public final void a() {
        this.q = true;
        if (this.j != null) {
            this.j = null;
        }
        if (this.u != null) {
            this.u = null;
        }
        if (this.t != null) {
            this.t = null;
        }
        if (this.e != null) {
            this.e = null;
        }
        com.mintegral.msdk.mtgbanner.common.c.a.a().a(4, this.b, null, null);
        com.mintegral.msdk.mtgbanner.common.c.a.a().a(this.b);
        com.mintegral.msdk.mtgbanner.common.c.a.a().b();
        if (this.l != null) {
            this.l.a();
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (!this.q && this.r) {
            if (this.k != null) {
                if (this.l == null) {
                    this.l = new c(this.e, this.t, this.b, this.c, this.m);
                }
                this.l.b(this.o);
                this.l.c(this.p);
                this.l.a(this.c, this.d);
                this.l.a(this.k);
            } else {
                b("banner show failed because campain is exception");
            }
            this.r = false;
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        if (this.j != null) {
            this.j.onLoadFailed(str);
        }
        String str2 = a;
        g.d(str2, "showFailed:" + str);
        b();
    }

    public final void b() {
        if (!this.q) {
            f();
            d();
            com.mintegral.msdk.mtgbanner.common.c.a.a().b(this.b, new b(this.g + "x" + this.f, this.h * 1000), this.u);
        }
    }

    public final void b(boolean z) {
        this.o = z;
        g();
        e();
    }

    public final void c(boolean z) {
        this.p = z;
        g();
    }

    private void f() {
        if (this.e != null) {
            boolean z = false;
            this.o = this.e.getParent() != null;
            if (this.e.getVisibility() == 0) {
                z = true;
            }
            this.p = z;
            if (!this.o || !this.p || this.s || com.mintegral.msdk.mtgbanner.common.util.c.a(this.e) || !com.mintegral.msdk.mtgbanner.common.util.c.b(this.e)) {
                com.mintegral.msdk.mtgbanner.common.c.a.a().a(2, this.b, null, null);
            } else {
                com.mintegral.msdk.mtgbanner.common.c.a a2 = com.mintegral.msdk.mtgbanner.common.c.a.a();
                String str = this.b;
                a2.a(3, str, new b(this.g + "x" + this.f, this.h * 1000), this.u);
            }
            if (!this.o) {
                com.mintegral.msdk.mtgbanner.common.c.a.a().a(4, this.b, null, null);
                com.mintegral.msdk.mtgbanner.common.c.a.a().a(this.b);
            }
        }
    }

    private void g() {
        f();
        if (this.l != null) {
            this.l.b(this.o);
            this.l.c(this.p);
        }
    }

    public final void a(int i2, int i3, int i4, int i5) {
        if (this.l != null) {
            this.l.a(i2, i3, i4, i5);
        }
    }
}
