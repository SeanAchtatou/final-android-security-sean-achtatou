package com.mintegral.msdk.mtgbanner.common.b;

import android.graphics.Bitmap;
import com.mintegral.msdk.base.common.c.c;
import com.mintegral.msdk.mtgbanner.common.c.b;

/* compiled from: DownloadImageListener */
public class g implements c {
    private static final String a = "g";
    private b b;
    private String c;

    public g(b bVar, String str) {
        if (bVar != null) {
            this.b = bVar;
        }
        this.c = str;
    }

    public void onSuccessLoad(Bitmap bitmap, String str) {
        com.mintegral.msdk.base.utils.g.b(a, "DownloadImageListener campaign image success");
        this.b.a(this.c, 1, str, true);
    }

    public void onFailedLoad(String str, String str2) {
        com.mintegral.msdk.base.utils.g.b(a, "DownloadImageListener campaign image fail");
        this.b.a(this.c, 1, str2, false);
    }
}
