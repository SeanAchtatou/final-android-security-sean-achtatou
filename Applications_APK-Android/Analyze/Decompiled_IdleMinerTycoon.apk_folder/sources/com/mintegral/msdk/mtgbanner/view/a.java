package com.mintegral.msdk.mtgbanner.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.o;
import com.mintegral.msdk.mtgjscommon.base.b;
import java.util.List;

/* compiled from: BannerWebViewClient */
public final class a extends b {
    String a;
    List<CampaignEx> b;
    com.mintegral.msdk.mtgbanner.common.b.a c;
    private final String d = "BannerWebViewClient";

    public a(String str, List<CampaignEx> list, com.mintegral.msdk.mtgbanner.common.b.a aVar) {
        this.a = str;
        this.b = list;
        this.c = aVar;
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        try {
            StringBuilder sb = new StringBuilder("javascript:");
            sb.append(o.a);
            if (Build.VERSION.SDK_INT <= 19) {
                webView.loadUrl(sb.toString());
            } else {
                webView.evaluateJavascript(sb.toString(), new ValueCallback<String>() {
                    public final /* bridge */ /* synthetic */ void onReceiveValue(Object obj) {
                    }
                });
            }
        } catch (Throwable th) {
            g.c("BannerWebViewClient", "onPageStarted", th);
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        String str2 = null;
        try {
            if (this.b.size() > 1) {
                com.mintegral.msdk.base.controller.a.d().h().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            } else {
                str2 = str;
            }
            if (this.c != null) {
                this.c.a(false, str2);
            }
            return true;
        } catch (Throwable th) {
            g.c("BannerWebViewClient", "shouldOverrideUrlLoading", th);
            return false;
        }
    }
}
