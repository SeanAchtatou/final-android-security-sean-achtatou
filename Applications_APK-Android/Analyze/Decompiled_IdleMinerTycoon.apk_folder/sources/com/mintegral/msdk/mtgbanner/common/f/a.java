package com.mintegral.msdk.mtgbanner.common.f;

import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.mintegral.msdk.base.utils.g;
import org.json.JSONObject;

/* compiled from: BannerResponseHandler */
public abstract class a extends com.mintegral.msdk.base.common.net.a.a {
    private static final String a = "a";
    private String b = "";

    public abstract void a(int i, String str);

    public abstract void a(CampaignUnit campaignUnit);

    public final /* synthetic */ void a(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        String str = a;
        g.b(str, "parseLoad content = " + jSONObject);
        int optInt = jSONObject.optInt("status");
        if (1 == optInt) {
            a(System.currentTimeMillis());
            CampaignUnit parseCampaignUnit = CampaignUnit.parseCampaignUnit(jSONObject.optJSONObject("data"), this.b);
            if (parseCampaignUnit == null || parseCampaignUnit.getAds() == null || parseCampaignUnit.getAds().size() <= 0) {
                a(optInt, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
                return;
            }
            a(parseCampaignUnit);
            if (!TextUtils.isEmpty(this.b)) {
                f();
            }
            a(parseCampaignUnit.getAds().size());
            return;
        }
        a(optInt, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
    }

    public final void b(String str) {
        this.b = str;
    }

    public final void a(String str) {
        String str2 = a;
        g.b(str2, "onFailed errorCode = " + str);
        a(0, str);
    }
}
