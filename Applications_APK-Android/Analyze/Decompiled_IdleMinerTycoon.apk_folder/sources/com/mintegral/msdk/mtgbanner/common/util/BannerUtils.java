package com.mintegral.msdk.mtgbanner.common.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.b.d;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.l;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.common.a.c;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.h;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.d.a;
import com.mintegral.msdk.d.b;
import com.mintegral.msdk.mtgbid.common.BidResponsedEx;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BannerUtils {
    private static final String TAG = "BannerUtils";

    public static String getTtcIds(Context context, String str) {
        if (context == null) {
            return "";
        }
        try {
            i a = i.a(context);
            if (a == null) {
                return "";
            }
            d a2 = d.a(a);
            a2.c();
            return a2.a(str);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getExcludeIds(Context context, String str) {
        String str2 = "";
        try {
            b.a();
            a b = b.b(com.mintegral.msdk.base.controller.a.d().j());
            JSONArray jSONArray = new JSONArray();
            if (b != null && b.aK() == 1) {
                g.b(TAG, "excludes cfc:" + b.aK());
                long[] c = m.a(i.a(context)).c();
                if (c != null) {
                    for (long put : c) {
                        g.b(TAG, "cfc campaignIds:" + c);
                        jSONArray.put(put);
                    }
                }
            }
            List<String> a = l.a(i.a(context)).a(str);
            if (a != null && a.size() > 0) {
                for (int i = 0; i < a.size(); i++) {
                    String str3 = a.get(i);
                    if (s.b(str3)) {
                        try {
                            jSONArray.put(Long.parseLong(str3));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (jSONArray.length() > 0) {
                str2 = k.a(jSONArray);
            }
            g.b(TAG, "get excludes:" + str2);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return str2;
    }

    public static String getInstallIds() {
        try {
            JSONArray jSONArray = new JSONArray();
            com.mintegral.msdk.base.controller.a.d();
            List<Long> g = com.mintegral.msdk.base.controller.a.g();
            if (g != null && g.size() > 0) {
                for (Long longValue : g) {
                    jSONArray.put(longValue.longValue());
                }
            }
            if (jSONArray.length() > 0) {
                return k.a(jSONArray);
            }
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getCloseIds(String str) {
        List list;
        try {
            if (c.b == null) {
                return "";
            }
            Map<String, List<com.mintegral.msdk.base.common.d.a>> map = c.b;
            if (!s.b(str) || !map.containsKey(str) || (list = map.get(str)) == null || list.size() <= 0) {
                return "";
            }
            JSONArray jSONArray = new JSONArray();
            for (int i = 0; i < list.size(); i++) {
                JSONObject jSONObject = new JSONObject();
                com.mintegral.msdk.base.common.d.a aVar = (com.mintegral.msdk.base.common.d.a) list.get(i);
                jSONObject.put(BidResponsedEx.KEY_CID, aVar.a());
                jSONObject.put("crid", aVar.c());
                jSONArray.put(jSONObject);
            }
            return jSONArray.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void inserCloseId(String str, List<CampaignEx> list) {
        Map<String, List<com.mintegral.msdk.base.common.d.a>> map = c.b;
        if (map != null && list != null && list.size() > 0) {
            if (s.b(str)) {
                if (map.containsKey(str)) {
                    map.put(str, fillIdInList(map.get(str), list));
                } else {
                    map.put(str, fillIdInList(new ArrayList(), list));
                }
            }
            c.b = map;
        }
    }

    private static synchronized List<com.mintegral.msdk.base.common.d.a> fillIdInList(List<com.mintegral.msdk.base.common.d.a> list, List<CampaignEx> list2) {
        synchronized (BannerUtils.class) {
            if (list2 != null) {
                if (list2.size() > 0) {
                    if (list == null) {
                        list = new ArrayList<>();
                    }
                    for (CampaignEx next : list2) {
                        if (next != null) {
                            com.mintegral.msdk.base.common.d.a aVar = new com.mintegral.msdk.base.common.d.a(next.getId(), next.getCreativeId());
                            if (list.size() >= 20) {
                                list.remove(0);
                            }
                            list.add(aVar);
                        }
                    }
                }
            }
        }
        return list;
    }

    public static void updateInstallList(Context context, List<CampaignEx> list) {
        g.b(TAG, "updateInstallList 开始 更新本机已安装广告列表");
        if (context == null || list == null || list.size() == 0) {
            g.b(TAG, "updateInstallList 列表为空 不做更新本机已安装广告列表");
            return;
        }
        m a = m.a(i.a(context));
        boolean z = false;
        for (int i = 0; i < list.size(); i++) {
            CampaignEx campaignEx = list.get(i);
            if (campaignEx != null) {
                if (k.a(context, campaignEx.getPackageName())) {
                    if (com.mintegral.msdk.base.controller.a.c() != null) {
                        com.mintegral.msdk.base.controller.a.c().add(new h(campaignEx.getId(), campaignEx.getPackageName()));
                        z = true;
                    }
                } else if (a != null && !a.a(campaignEx.getId())) {
                    com.mintegral.msdk.base.entity.g gVar = new com.mintegral.msdk.base.entity.g();
                    gVar.a(campaignEx.getId());
                    gVar.a(campaignEx.getFca());
                    gVar.b(campaignEx.getFcb());
                    gVar.g();
                    gVar.e();
                    gVar.a(System.currentTimeMillis());
                    a.a(gVar);
                }
            }
        }
        if (z) {
            g.b(TAG, "更新安装列表");
            com.mintegral.msdk.base.controller.a.d().f();
        }
    }

    public static CampaignEx managerCampaignEX(String str, CampaignEx campaignEx) {
        if (TextUtils.isEmpty(str)) {
            return campaignEx;
        }
        if (TextUtils.isEmpty(str) && campaignEx == null) {
            return null;
        }
        if (!str.contains("notice")) {
            try {
                JSONObject campaignToJsonObject = CampaignEx.campaignToJsonObject(campaignEx);
                CampaignEx parseShortCutsCampaign = CampaignEx.parseShortCutsCampaign(campaignToJsonObject);
                if (parseShortCutsCampaign == null) {
                    parseShortCutsCampaign = campaignEx;
                }
                if (!TextUtils.isEmpty(str)) {
                    String optString = campaignToJsonObject.optString("unitId");
                    if (!TextUtils.isEmpty(optString)) {
                        parseShortCutsCampaign.setCampaignUnitId(optString);
                    }
                    JSONObject optJSONObject = new JSONObject(str).optJSONObject(com.mintegral.msdk.base.common.a.z);
                    String str2 = "-999";
                    String str3 = "-999";
                    if (optJSONObject != null) {
                        str2 = String.valueOf(k.b(com.mintegral.msdk.base.controller.a.d().h(), (float) Integer.valueOf(optJSONObject.getString(com.mintegral.msdk.base.common.a.x)).intValue()));
                        str3 = String.valueOf(k.b(com.mintegral.msdk.base.controller.a.d().h(), (float) Integer.valueOf(optJSONObject.getString(com.mintegral.msdk.base.common.a.y)).intValue()));
                    }
                    parseShortCutsCampaign.setClickURL(com.mintegral.msdk.click.b.a(parseShortCutsCampaign.getClickURL(), str2, str3));
                    String noticeUrl = parseShortCutsCampaign.getNoticeUrl();
                    if (optJSONObject != null) {
                        Iterator<String> keys = optJSONObject.keys();
                        StringBuilder sb = new StringBuilder();
                        while (keys.hasNext()) {
                            sb.append(Constants.RequestParameters.AMPERSAND);
                            String next = keys.next();
                            String optString2 = optJSONObject.optString(next);
                            if (com.mintegral.msdk.base.common.a.x.equals(next) || com.mintegral.msdk.base.common.a.y.equals(next)) {
                                optString2 = String.valueOf(k.b(com.mintegral.msdk.base.controller.a.d().h(), (float) Integer.valueOf(optString2).intValue()));
                            }
                            sb.append(next);
                            sb.append(Constants.RequestParameters.EQUAL);
                            sb.append(optString2);
                        }
                        parseShortCutsCampaign.setNoticeUrl(noticeUrl + ((Object) sb));
                    }
                }
                return parseShortCutsCampaign;
            } catch (Throwable unused) {
                return campaignEx;
            }
        } else {
            try {
                JSONObject campaignToJsonObject2 = CampaignEx.campaignToJsonObject(campaignEx);
                JSONObject jSONObject = new JSONObject(str);
                Iterator<String> keys2 = jSONObject.keys();
                while (keys2.hasNext()) {
                    String next2 = keys2.next();
                    campaignToJsonObject2.put(next2, jSONObject.getString(next2));
                }
                CampaignEx parseShortCutsCampaign2 = CampaignEx.parseShortCutsCampaign(campaignToJsonObject2);
                String optString3 = campaignToJsonObject2.optString("unitId");
                if (!TextUtils.isEmpty(optString3)) {
                    parseShortCutsCampaign2.setCampaignUnitId(optString3);
                }
                return parseShortCutsCampaign2;
            } catch (JSONException e) {
                e.printStackTrace();
                return campaignEx;
            }
        }
    }

    public static String buildClickJsonObject(float f, float f2) {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(com.mintegral.msdk.base.common.a.x, k.a(com.mintegral.msdk.base.controller.a.d().h(), f));
            jSONObject2.put(com.mintegral.msdk.base.common.a.y, k.a(com.mintegral.msdk.base.controller.a.d().h(), f2));
            jSONObject2.put(com.mintegral.msdk.base.common.a.A, 0);
            jSONObject2.put(com.mintegral.msdk.base.common.a.B, com.mintegral.msdk.base.controller.a.d().h().getResources().getConfiguration().orientation);
            jSONObject2.put(com.mintegral.msdk.base.common.a.C, (double) k.c(com.mintegral.msdk.base.controller.a.d().h()));
            jSONObject.put(com.mintegral.msdk.base.common.a.z, jSONObject2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    public static String codeToJsonString(int i) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("code", i);
            String jSONObject2 = jSONObject.toString();
            if (!TextUtils.isEmpty(jSONObject2)) {
                return Base64.encodeToString(jSONObject2.getBytes(), 2);
            }
            return "";
        } catch (Throwable unused) {
            g.d(TAG, "code to string is error");
            return "";
        }
    }
}
