package com.mintegral.msdk.mtgbanner.common.bridge;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.common.a.c;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.d.d;
import com.mintegral.msdk.mtgbanner.common.util.BannerUtils;
import com.mintegral.msdk.mtgbanner.common.util.b;
import com.mintegral.msdk.mtgjscommon.windvane.g;
import im.getsocial.sdk.consts.LanguageCodes;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: BannerJSBridgeImpl */
public final class a implements b {
    private final String a = "BannerJSBridgeImpl";
    private WeakReference<Context> b;
    private List<CampaignEx> c;
    private String d;
    private int e;
    private com.mintegral.msdk.mtgbanner.common.b.a f;
    private BannerExpandDialog g;
    private boolean h = false;

    public a(Context context, String str) {
        this.d = str;
        this.b = new WeakReference<>(context);
    }

    public final void a(com.mintegral.msdk.mtgbanner.common.b.a aVar) {
        if (aVar != null) {
            this.f = aVar;
        }
    }

    public final void a(List<CampaignEx> list) {
        this.c = list;
    }

    public final void a(int i) {
        this.e = i;
    }

    public final void a(Object obj) {
        try {
            if (obj instanceof com.mintegral.msdk.mtgjscommon.windvane.a) {
                g.a();
                g.a(((com.mintegral.msdk.mtgjscommon.windvane.a) obj).a, "onJSBridgeConnected", "");
            }
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("BannerJSBridgeImpl", "onJSBridgeConnect", th);
        }
    }

    public final void a(Object obj, String str) {
        if (obj != null) {
            try {
                int optInt = new JSONObject(str).optInt("isReady", 1);
                g.a();
                g.a(obj, BannerUtils.codeToJsonString(0));
                if (this.f != null) {
                    this.f.b(optInt);
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("BannerJSBridgeImpl", "readyStatus", th);
            }
        }
    }

    public final void b(Object obj) {
        com.mintegral.msdk.base.utils.g.d("BannerJSBridgeImpl", "BANNER INIT INVOKE");
        try {
            JSONObject jSONObject = new JSONObject();
            b bVar = new b(com.mintegral.msdk.base.controller.a.d().h());
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("dev_close_state", this.e);
            jSONObject.put("sdkSetting", jSONObject2);
            jSONObject.put("device", bVar.a());
            jSONObject.put("campaignList", CampaignEx.parseCamplistToJson(this.c));
            com.mintegral.msdk.d.b.a();
            d c2 = com.mintegral.msdk.d.b.c(com.mintegral.msdk.base.controller.a.d().j(), this.d);
            if (c2 == null) {
                c2 = d.b(this.d);
            }
            jSONObject.put("unitSetting", c2.x());
            com.mintegral.msdk.d.b.a();
            String c3 = com.mintegral.msdk.d.b.c(com.mintegral.msdk.base.controller.a.d().j());
            if (!TextUtils.isEmpty(c3)) {
                jSONObject.put("appSetting", new JSONObject(c3));
            }
            String encodeToString = Base64.encodeToString(jSONObject.toString().getBytes(), 2);
            g.a();
            g.a(obj, encodeToString);
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("BannerJSBridgeImpl", "init", th);
        }
    }

    public final void a(String str) {
        com.mintegral.msdk.base.utils.g.d("BannerJSBridgeImpl", "click");
        try {
            if (this.c != null && !TextUtils.isEmpty(str)) {
                CampaignEx parseShortCutsCampaign = CampaignEx.parseShortCutsCampaign(new JSONObject(str).getJSONObject(LanguageCodes.PORTUGUESE));
                if (this.f != null) {
                    this.f.a(parseShortCutsCampaign);
                }
            }
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("BannerJSBridgeImpl", "click", th);
        }
    }

    public final void b(String str) {
        com.mintegral.msdk.base.utils.g.d("BannerJSBridgeImpl", "toggleCloseBtn");
        try {
            if (!TextUtils.isEmpty(str)) {
                int optInt = new JSONObject(str).optInt("state");
                if (this.f != null) {
                    this.f.a(optInt);
                }
            }
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("BannerJSBridgeImpl", "toggleCloseBtn", th);
        }
    }

    public final void b(Object obj, String str) {
        com.mintegral.msdk.base.utils.g.d("BannerJSBridgeImpl", "triggerCloseBtn");
        try {
            if (!TextUtils.isEmpty(str)) {
                new JSONObject(str).optString("state");
                if (this.f != null) {
                    this.f.a();
                }
                g.a();
                g.a(obj, BannerUtils.codeToJsonString(0));
            }
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("BannerJSBridgeImpl", "triggerCloseBtn", th);
            g.a();
            g.a(obj, BannerUtils.codeToJsonString(-1));
        }
    }

    public final void c(String str) {
        com.mintegral.msdk.base.utils.g.a("BannerJSBridgeImpl", "sendImpressions:" + str);
        try {
            if (!TextUtils.isEmpty(str)) {
                JSONArray jSONArray = new JSONArray(str);
                final ArrayList arrayList = new ArrayList();
                for (int i = 0; i < jSONArray.length(); i++) {
                    String string = jSONArray.getString(i);
                    for (CampaignEx next : this.c) {
                        if (next.getId().equals(string)) {
                            c.b(this.d, next, "banner");
                            arrayList.add(string);
                        }
                    }
                }
                new Thread(new Runnable() {
                    public final void run() {
                        try {
                            m a2 = m.a(i.a(com.mintegral.msdk.base.controller.a.d().h()));
                            Iterator it = arrayList.iterator();
                            while (it.hasNext()) {
                                a2.b((String) it.next());
                            }
                        } catch (Exception unused) {
                            com.mintegral.msdk.base.utils.g.d("BannerJSBridgeImpl", "campain can't insert db");
                        }
                    }
                }).start();
            }
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("BannerJSBridgeImpl", "sendImpressions", th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, ?[OBJECT, ARRAY], java.lang.String, java.lang.String, int, boolean]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    public final void c(Object obj, String str) {
        com.mintegral.msdk.base.utils.g.a("BannerJSBridgeImpl", "reportUrls:" + str);
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONArray jSONArray = new JSONArray(str);
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    int optInt = jSONObject.optInt("type");
                    com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), (CampaignEx) null, "", jSONObject.optString("url"), false, optInt != 0);
                }
                g.a();
                g.a(obj, BannerUtils.codeToJsonString(0));
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("BannerJSBridgeImpl", "reportUrls", th);
            }
        }
    }

    public final void open(String str) {
        com.mintegral.msdk.base.utils.g.d("BannerJSBridgeImpl", "open");
        try {
            com.mintegral.msdk.base.utils.g.d("BannerJSBridgeImpl", str);
            if (this.c.size() > 1) {
                com.mintegral.msdk.base.controller.a.d().h().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                str = null;
            }
            if (this.f != null) {
                this.f.a(true, str);
            }
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("BannerJSBridgeImpl", "open", th);
        }
    }

    public final void close() {
        com.mintegral.msdk.base.utils.g.d("BannerJSBridgeImpl", "close");
        try {
            if (this.f != null) {
                this.f.b();
            }
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("BannerJSBridgeImpl", "close", th);
        }
    }

    public final void unload() {
        close();
    }

    private CampaignEx a() {
        if (this.c == null || this.c.size() <= 0) {
            return null;
        }
        return this.c.get(0);
    }

    public final void useCustomClose(boolean z) {
        boolean z2 = !z;
        try {
            if (this.f != null) {
                this.f.a(z2 ? 1 : 0);
            }
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("BannerJSBridgeImpl", "useCustomClose", th);
        }
    }

    public final void expand(String str, boolean z) {
        String str2 = "";
        try {
            if (a() != null) {
                if (TextUtils.isEmpty(a().getBannerHtml())) {
                    str2 = a().getBannerUrl();
                } else {
                    str2 = "file:////" + a().getBannerHtml();
                }
            }
            Bundle bundle = new Bundle();
            if (!TextUtils.isEmpty(str)) {
                str2 = str;
            }
            bundle.putString("url", str2);
            bundle.putBoolean("shouldUseCustomClose", z);
            if (!(this.b == null || this.b.get() == null)) {
                if (this.g == null || !this.g.isShowing()) {
                    this.g = new BannerExpandDialog(this.b.get(), bundle, this.f);
                    this.g.setCampaignList(this.d, this.c);
                    this.g.show();
                } else {
                    return;
                }
            }
            if (this.f != null) {
                this.f.a(true);
            }
            com.mintegral.msdk.mtgbanner.common.d.a.a(this.d, a(), str);
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("BannerJSBridgeImpl", "expand", th);
        }
    }
}
