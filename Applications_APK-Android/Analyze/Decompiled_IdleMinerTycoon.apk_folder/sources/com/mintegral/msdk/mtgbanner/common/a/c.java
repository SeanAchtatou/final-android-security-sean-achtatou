package com.mintegral.msdk.mtgbanner.common.a;

/* compiled from: BannerUnitData */
public final class c {
    private String a = "";
    private String b = "";
    private int c = 0;
    private int d = 1;

    public c(String str, String str2, int i) {
        this.a = str;
        this.b = str2;
        this.c = 0;
        this.d = i;
    }

    public final String a() {
        return this.b;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final int b() {
        return this.c;
    }

    public final void a(int i) {
        this.c = i;
    }

    public final int c() {
        return this.d;
    }
}
