package com.mintegral.msdk.mtgbanner.common.bridge;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.WindowManager;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.helpshift.support.FaqTagFilter;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.o;
import com.mintegral.msdk.mtgbanner.common.b.a;
import com.mintegral.msdk.mtgjscommon.mraid.b;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import java.util.HashMap;
import java.util.List;
import org.json.JSONObject;

public class BannerExpandDialog extends Dialog {
    private final String a = "BannerExpandDialog";
    private String b;
    private boolean c;
    /* access modifiers changed from: private */
    public FrameLayout d;
    /* access modifiers changed from: private */
    public WindVaneWebView e;
    /* access modifiers changed from: private */
    public TextView f;
    private String g;
    /* access modifiers changed from: private */
    public List<CampaignEx> h;
    /* access modifiers changed from: private */
    public a i;
    private b j = new b() {
        public final void expand(String str, boolean z) {
        }

        public final void open(String str) {
            try {
                g.d("BannerExpandDialog", str);
                if (BannerExpandDialog.this.h.size() > 1) {
                    com.mintegral.msdk.base.controller.a.d().h().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                    str = null;
                }
                if (BannerExpandDialog.this.i != null) {
                    BannerExpandDialog.this.i.a(true, str);
                }
            } catch (Throwable th) {
                g.c("BannerExpandDialog", "open", th);
            }
        }

        public final void close() {
            BannerExpandDialog.this.dismiss();
        }

        public final void unload() {
            close();
        }

        public final void useCustomClose(boolean z) {
            try {
                BannerExpandDialog.this.f.setVisibility(z ? 4 : 0);
            } catch (Throwable th) {
                g.c("BannerExpandDialog", "useCustomClose", th);
            }
        }
    };

    public BannerExpandDialog(Context context, Bundle bundle, a aVar) {
        super(context);
        if (bundle != null) {
            this.b = bundle.getString("url");
            this.c = bundle.getBoolean("shouldUseCustomClose");
        }
        this.i = aVar;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(false);
        setCancelable(true);
        this.d = new FrameLayout(getContext());
        this.d.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.e = new WindVaneWebView(getContext().getApplicationContext());
        this.e.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.d.addView(this.e);
        this.f = new TextView(getContext());
        this.f.setBackgroundColor(0);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(96, 96);
        layoutParams.gravity = 8388661;
        layoutParams.setMargins(30, 30, 30, 30);
        this.f.setLayoutParams(layoutParams);
        this.f.setVisibility(this.c ? 4 : 0);
        this.f.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                BannerExpandDialog.this.dismiss();
            }
        });
        this.d.addView(this.f);
        TextView textView = new TextView(getContext());
        textView.setTextColor(-1);
        textView.setText("MRAID EXPAND TEST");
        textView.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        textView.setLayoutParams(new FrameLayout.LayoutParams(-2, 120));
        textView.setGravity(17);
        this.d.addView(textView);
        setContentView(this.d);
        if (getWindow() != null) {
            getWindow().setBackgroundDrawable(new ColorDrawable(0));
            getWindow().getDecorView().setPadding(0, 0, 0, 0);
            WindowManager.LayoutParams attributes = getWindow().getAttributes();
            attributes.width = -1;
            attributes.height = -1;
            getWindow().setAttributes(attributes);
            if (Build.VERSION.SDK_INT >= 16) {
                int i2 = 519;
                if (Build.VERSION.SDK_INT >= 19) {
                    i2 = 4615;
                }
                getWindow().getDecorView().setSystemUiVisibility(i2);
            }
        }
        this.e.setWebViewListener(new com.mintegral.msdk.mtgjscommon.b.a() {
            public final void a(WebView webView, String str) {
                super.a(webView, str);
                StringBuilder sb = new StringBuilder("javascript:");
                sb.append(o.a);
                if (Build.VERSION.SDK_INT <= 19) {
                    webView.loadUrl(sb.toString());
                } else {
                    webView.evaluateJavascript(sb.toString(), new ValueCallback<String>() {
                        public final /* bridge */ /* synthetic */ void onReceiveValue(Object obj) {
                        }
                    });
                }
                BannerExpandDialog.a(BannerExpandDialog.this);
            }
        });
        this.e.setObject(this.j);
        this.e.loadUrl(this.b);
        setOnDismissListener(new DialogInterface.OnDismissListener() {
            public final void onDismiss(DialogInterface dialogInterface) {
                if (BannerExpandDialog.this.i != null) {
                    BannerExpandDialog.this.i.a(false);
                }
                BannerExpandDialog.this.e.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
                BannerExpandDialog.this.d.removeView(BannerExpandDialog.this.e);
                BannerExpandDialog.this.e.release();
                WindVaneWebView unused = BannerExpandDialog.this.e = null;
                a unused2 = BannerExpandDialog.this.i = null;
            }
        });
    }

    public void setCampaignList(String str, List<CampaignEx> list) {
        this.g = str;
        this.h = list;
    }

    static /* synthetic */ void a(BannerExpandDialog bannerExpandDialog) {
        try {
            int i2 = com.mintegral.msdk.base.controller.a.d().h().getResources().getConfiguration().orientation;
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("orientation", i2 == 2 ? "landscape" : i2 == 1 ? "portrait" : FaqTagFilter.Operator.UNDEFINED);
            jSONObject.put("locked", "true");
            HashMap p = c.p(com.mintegral.msdk.base.controller.a.d().h());
            int intValue = ((Integer) p.get("width")).intValue();
            int intValue2 = ((Integer) p.get("height")).intValue();
            HashMap hashMap = new HashMap();
            hashMap.put("placementType", "Interstitial");
            hashMap.put("state", "expanded");
            hashMap.put("viewable", "true");
            hashMap.put("currentAppOrientation", jSONObject);
            int[] iArr = new int[2];
            bannerExpandDialog.e.getLocationInWindow(iArr);
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.a(bannerExpandDialog.e, (float) iArr[0], (float) iArr[1], (float) bannerExpandDialog.e.getWidth(), (float) bannerExpandDialog.e.getHeight());
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.b(bannerExpandDialog.e, (float) iArr[0], (float) iArr[1], (float) bannerExpandDialog.e.getWidth(), (float) bannerExpandDialog.e.getHeight());
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.b(bannerExpandDialog.e, (float) c.n(com.mintegral.msdk.base.controller.a.d().h()), (float) c.o(com.mintegral.msdk.base.controller.a.d().h()));
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.c(bannerExpandDialog.e, (float) intValue, (float) intValue2);
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.a(bannerExpandDialog.e, hashMap);
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.a(bannerExpandDialog.e);
        } catch (Throwable th) {
            g.c("BannerExpandDialog", "notifyMraid", th);
        }
    }
}
