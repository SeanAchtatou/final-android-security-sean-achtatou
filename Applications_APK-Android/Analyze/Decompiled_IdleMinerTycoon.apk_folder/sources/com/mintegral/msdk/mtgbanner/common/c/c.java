package com.mintegral.msdk.mtgbanner.common.c;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.helpshift.support.FaqTagFilter;
import com.iab.omid.library.mintegral.ScriptInjector;
import com.iab.omid.library.mintegral.adsession.AdEvents;
import com.iab.omid.library.mintegral.adsession.AdSession;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.common.e.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.d.d;
import com.mintegral.msdk.interstitial.view.MTGInterstitialActivity;
import com.mintegral.msdk.mtgbanner.common.b.e;
import com.mintegral.msdk.mtgbanner.common.util.BannerUtils;
import com.mintegral.msdk.mtgbanner.view.MTGBannerWebView;
import com.mintegral.msdk.out.Campaign;
import com.mintegral.msdk.out.MTGBannerView;
import com.mintegral.msdk.out.NativeListener;
import com.mintegral.msdk.videocommon.download.h;
import com.tapjoy.TJAdUnitConstants;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import org.json.JSONObject;

/* compiled from: BannerShowManager */
public final class c {
    /* access modifiers changed from: private */
    public static String b = "BannerShowManager";
    private a A = new a(this);
    com.mintegral.msdk.click.a a;
    /* access modifiers changed from: private */
    public com.mintegral.msdk.mtgbanner.common.b.c c;
    private boolean d;
    /* access modifiers changed from: private */
    public CampaignEx e;
    private MTGBannerView f;
    /* access modifiers changed from: private */
    public ImageView g;
    private MTGBannerWebView h;
    /* access modifiers changed from: private */
    public ImageView i;
    private boolean j;
    private boolean k;
    /* access modifiers changed from: private */
    public boolean l;
    private boolean m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public boolean o;
    /* access modifiers changed from: private */
    public String p;
    private List<CampaignEx> q;
    private int r;
    private long s = MTGInterstitialActivity.WEB_LOAD_TIME;
    /* access modifiers changed from: private */
    public AdSession t;
    private Handler u = new Handler(Looper.getMainLooper());
    private com.mintegral.msdk.base.common.f.a v = new com.mintegral.msdk.base.common.f.a() {
        public final void b() {
        }

        public final void a() {
            c.this.a("banner render failed because render is timeout");
        }
    };
    /* access modifiers changed from: private */
    public float w;
    /* access modifiers changed from: private */
    public float x;
    private View.OnClickListener y = new View.OnClickListener() {
        public final void onClick(View view) {
            if (c.this.d()) {
                c.b(c.this);
            }
        }
    };
    private com.mintegral.msdk.mtgbanner.common.b.a z = new com.mintegral.msdk.mtgbanner.common.b.a() {
        public final void a(int i) {
            if (i == 2) {
                c.c(c.this);
            } else {
                c.this.g();
            }
        }

        public final void a() {
            c.b(c.this);
        }

        public final void b() {
            c.b(c.this);
        }

        public final void b(int i) {
            if (i == 1) {
                c.this.e();
                c.this.a("", 1);
                return;
            }
            c.this.c();
        }

        public final void a(CampaignEx campaignEx) {
            c.this.a(campaignEx);
        }

        public final void a(boolean z) {
            if (c.this.c != null) {
                boolean unused = c.this.o = z;
                if (z) {
                    c.this.c.c();
                } else {
                    c.this.c.d();
                }
            }
        }

        public final void a(boolean z, String str) {
            try {
                if (c.this.c != null) {
                    c.this.c.a();
                    if (TextUtils.isEmpty(str)) {
                        c.this.c.b();
                        return;
                    }
                    CampaignEx parseShortCutsCampaign = CampaignEx.parseShortCutsCampaign(CampaignEx.campaignToJsonObject(c.this.e));
                    parseShortCutsCampaign.setClickURL(str);
                    if (z) {
                        String i = c.this.p;
                        if (parseShortCutsCampaign != null) {
                            try {
                                if (!TextUtils.isEmpty(str)) {
                                    new b(com.mintegral.msdk.base.controller.a.d().h()).b(parseShortCutsCampaign.getRequestIdNotice(), parseShortCutsCampaign.getId(), i, str, parseShortCutsCampaign.isBidCampaign());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    c.this.a(parseShortCutsCampaign);
                }
            } catch (Exception e2) {
                g.d(c.b, e2.getMessage());
            }
        }
    };

    /* compiled from: BannerShowManager */
    private static class a extends com.mintegral.msdk.mtgjscommon.b.a {
        c a;

        public a(c cVar) {
            this.a = cVar;
        }

        public final void a(WebView webView, String str) {
            try {
                if (this.a.t == null && this.a.e.isActiveOm()) {
                    AdSession unused = this.a.t = com.mintegral.msdk.b.b.a(webView.getContext(), webView, this.a.e.getId());
                    if (this.a.t != null) {
                        try {
                            this.a.t.registerAdView(webView);
                            if (this.a.i != null) {
                                this.a.t.addFriendlyObstruction(this.a.i);
                            }
                            if (this.a.g != null) {
                                this.a.t.addFriendlyObstruction(this.a.g);
                            }
                            AdEvents createAdEvents = AdEvents.createAdEvents(this.a.t);
                            this.a.t.start();
                            createAdEvents.impressionOccurred();
                        } catch (Exception e) {
                            g.a("omsdk", e.getMessage());
                        }
                        g.a("OMSDK", "adSession.start()");
                    } else if (this.a.e != null) {
                        new b(webView.getContext()).b(this.a.e.getRequestId(), this.a.e.getId(), this.a.p, "fetch OM failed, context null");
                    }
                }
            } catch (Exception e2) {
                g.a("OMSDK", e2.getMessage());
                if (this.a.e != null) {
                    String requestId = this.a.e.getRequestId();
                    String id = this.a.e.getId();
                    String i = this.a.p;
                    b bVar = new b(webView.getContext());
                    bVar.b(requestId, id, i, "fetch OM failed, exception" + e2.getMessage());
                }
            }
            if (this.a != null) {
                boolean unused2 = this.a.l = true;
                g.d("WindVaneWebView", "BANNER onPageFinished");
                g.d("BannerCallJS", "fireOnJSBridgeConnected");
                try {
                    com.mintegral.msdk.mtgjscommon.windvane.g.a();
                    com.mintegral.msdk.mtgjscommon.windvane.g.a(webView, "onJSBridgeConnected", "");
                } catch (Throwable th) {
                    g.c("BannerCallJS", "fireOnJSBridgeConnected", th);
                }
                boolean unused3 = this.a.n = true;
                if (!this.a.e.isHasMtgTplMark()) {
                    this.a.e();
                    this.a.a("", 1);
                }
            }
        }

        public final void a(WebView webView, int i, String str, String str2) {
            this.a.a(str);
            this.a.a(str, 2);
        }
    }

    public c(MTGBannerView mTGBannerView, com.mintegral.msdk.mtgbanner.common.b.c cVar, String str, boolean z2, d dVar) {
        this.d = z2;
        this.f = mTGBannerView;
        this.p = str;
        this.c = new e(cVar, dVar);
    }

    public final void a(boolean z2, int i2) {
        this.r = i2;
        if (i2 == 0) {
            com.mintegral.msdk.d.b.a();
            d c2 = com.mintegral.msdk.d.b.c(com.mintegral.msdk.base.controller.a.d().j(), this.p);
            if (c2 != null) {
                z2 = c2.b() == 1;
            } else {
                return;
            }
        }
        this.d = z2;
    }

    public final void a(boolean z2) {
        this.d = z2;
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.f != null) {
            if (!(this.h == null || this.h.getParent() == null)) {
                this.f.removeView(this.h);
            }
            if (this.g == null) {
                this.g = new ImageView(com.mintegral.msdk.base.controller.a.d().h());
                this.g.setOnTouchListener(new View.OnTouchListener() {
                    public final boolean onTouch(View view, MotionEvent motionEvent) {
                        float unused = c.this.w = motionEvent.getRawX();
                        float unused2 = c.this.x = motionEvent.getRawY();
                        String b = c.b;
                        g.d(b, c.this.w + "  " + c.this.x);
                        return false;
                    }
                });
                this.g.setOnClickListener(new View.OnClickListener() {
                    public final void onClick(View view) {
                        c.this.a(BannerUtils.managerCampaignEX(BannerUtils.buildClickJsonObject(c.this.w, c.this.x), c.this.e));
                    }
                });
            }
            String imageUrl = this.e.getImageUrl();
            if (!TextUtils.isEmpty(imageUrl)) {
                com.mintegral.msdk.base.common.c.b.a(com.mintegral.msdk.base.controller.a.d().h()).a(imageUrl, new com.mintegral.msdk.base.common.c.c() {
                    public final void onSuccessLoad(Bitmap bitmap, String str) {
                        if (c.this.g != null) {
                            c.this.g.setImageBitmap(bitmap);
                        }
                        boolean unused = c.this.l = true;
                        c.q(c.this);
                        c.this.g();
                        c.this.e();
                    }

                    public final void onFailedLoad(String str, String str2) {
                        c.this.a("banner show failed because banner default view is exception");
                    }
                });
            } else {
                a("banner show failed because campain is exception");
            }
        } else {
            a("banner show failed because banner view is exception");
        }
    }

    private static String b(CampaignEx campaignEx) {
        String str;
        if (campaignEx == null) {
            return "";
        }
        String a2 = h.a().a(campaignEx.getBannerUrl());
        if (!TextUtils.isEmpty(a2)) {
            return a2;
        }
        String bannerHtml = campaignEx.getBannerHtml();
        if (campaignEx.isActiveOm()) {
            String str2 = "";
            try {
                File file = new File(bannerHtml);
                if (file.exists()) {
                    str2 = com.mintegral.msdk.base.utils.e.a(file);
                }
                str = ScriptInjector.injectScriptContentIntoHtml(MIntegralConstans.OMID_JS_SERVICE_CONTENT, str2);
            } catch (Exception unused) {
                File file2 = new File(bannerHtml);
                if (!file2.exists() || !file2.isFile() || !file2.canRead()) {
                    return bannerHtml;
                }
                str = "file:////" + bannerHtml;
            }
            return str;
        }
        File file3 = new File(bannerHtml);
        if (!file3.exists() || !file3.isFile() || !file3.canRead()) {
            return bannerHtml;
        }
        return "file:////" + bannerHtml;
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        if (this.e != null && !f()) {
            this.u.removeCallbacks(this.v);
            if (this.c != null) {
                this.c.a(str);
            }
            com.mintegral.msdk.base.common.e.c.a(com.mintegral.msdk.base.controller.a.d().h(), this.e, this.p, str);
        }
    }

    /* access modifiers changed from: private */
    public boolean d() {
        if (this.f != null && this.f.getVisibility() == 0 && !com.mintegral.msdk.mtgbanner.common.util.c.a(this.f) && com.mintegral.msdk.mtgbanner.common.util.c.b(this.f) && !this.o) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.l && !this.m && this.c != null) {
            this.m = true;
            this.u.removeCallbacks(this.v);
            this.c.a(this.q);
        }
        if (this.l && this.j && this.k && this.m) {
            boolean d2 = d();
            if (this.n && d2) {
                g.d(b, "onBannerWebViewShow && transInfoToMraid");
                int[] iArr = new int[2];
                this.f.getLocationInWindow(iArr);
                MTGBannerWebView mTGBannerWebView = this.h;
                float f2 = (float) iArr[0];
                float f3 = (float) iArr[1];
                g.d("BannerCallJS", "fireOnBannerWebViewShow");
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("startX", (double) f2);
                    jSONObject.put("startY", (double) f3);
                    jSONObject.put("scale", (double) k.c(com.mintegral.msdk.base.controller.a.d().h()));
                    String encodeToString = Base64.encodeToString(jSONObject.toString().getBytes(), 2);
                    com.mintegral.msdk.mtgjscommon.windvane.g.a();
                    com.mintegral.msdk.mtgjscommon.windvane.g.a(mTGBannerWebView, "webviewshow", encodeToString);
                } catch (Throwable th) {
                    g.c("BannerCallJS", "fireOnBannerWebViewShow", th);
                }
                MTGBannerWebView mTGBannerWebView2 = this.h;
                int i2 = iArr[0];
                int i3 = iArr[1];
                int width = this.f.getWidth();
                int height = this.f.getHeight();
                g.d("BannerCallJS", "transInfoForMraid");
                try {
                    int i4 = com.mintegral.msdk.base.controller.a.d().h().getResources().getConfiguration().orientation;
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("orientation", i4 == 2 ? "landscape" : i4 == 1 ? "portrait" : FaqTagFilter.Operator.UNDEFINED);
                    jSONObject2.put("locked", "true");
                    HashMap p2 = com.mintegral.msdk.base.utils.c.p(com.mintegral.msdk.base.controller.a.d().h());
                    int intValue = ((Integer) p2.get("width")).intValue();
                    int intValue2 = ((Integer) p2.get("height")).intValue();
                    HashMap hashMap = new HashMap();
                    hashMap.put("placementType", TJAdUnitConstants.String.INLINE);
                    hashMap.put("state", "default");
                    hashMap.put("viewable", "true");
                    hashMap.put("currentAppOrientation", jSONObject2);
                    com.mintegral.msdk.mtgjscommon.mraid.a.a();
                    float f4 = (float) i2;
                    float f5 = (float) i3;
                    float f6 = (float) width;
                    float f7 = (float) height;
                    com.mintegral.msdk.mtgjscommon.mraid.a.a(mTGBannerWebView2, f4, f5, f6, f7);
                    com.mintegral.msdk.mtgjscommon.mraid.a.a();
                    com.mintegral.msdk.mtgjscommon.mraid.a.b(mTGBannerWebView2, f4, f5, f6, f7);
                    com.mintegral.msdk.mtgjscommon.mraid.a.a();
                    com.mintegral.msdk.mtgjscommon.mraid.a.b(mTGBannerWebView2, (float) com.mintegral.msdk.base.utils.c.n(com.mintegral.msdk.base.controller.a.d().h()), (float) com.mintegral.msdk.base.utils.c.o(com.mintegral.msdk.base.controller.a.d().h()));
                    com.mintegral.msdk.mtgjscommon.mraid.a.a();
                    com.mintegral.msdk.mtgjscommon.mraid.a.c(mTGBannerWebView2, (float) intValue, (float) intValue2);
                    com.mintegral.msdk.mtgjscommon.mraid.a.a();
                    com.mintegral.msdk.mtgjscommon.mraid.a.a(mTGBannerWebView2, hashMap);
                    com.mintegral.msdk.mtgjscommon.mraid.a.a();
                    com.mintegral.msdk.mtgjscommon.mraid.a.a(mTGBannerWebView2);
                } catch (Throwable th2) {
                    g.c("BannerCallJS", "transInfoForMraid", th2);
                }
                this.n = false;
            }
            if (this.e != null && !f()) {
                String str = b;
                g.d(str, "showSuccessed:" + this.e.getId());
                if (d2) {
                    if (this.g == null || this.g.getVisibility() != 0) {
                        if (this.q != null && this.q.size() > 0) {
                            boolean z2 = false;
                            int i5 = 0;
                            for (int i6 = 0; i6 < this.q.size(); i6++) {
                                if (!this.q.get(i6).isHasMtgTplMark() && (i6 == 0 || !this.q.get(i6).isReport())) {
                                    b(this.q.get(i6), com.mintegral.msdk.base.controller.a.d().h(), this.p);
                                    this.q.get(i6).setReport(true);
                                    com.mintegral.msdk.base.common.a.c.b(this.p, this.q.get(i6), "banner");
                                    i5 = i6;
                                    z2 = true;
                                }
                            }
                            if (z2) {
                                a(this.q.get(i5), com.mintegral.msdk.base.controller.a.d().h(), this.p);
                            }
                        }
                    } else if (this.e != null) {
                        CampaignEx campaignEx = this.e;
                        if (campaignEx != null) {
                            b(campaignEx, com.mintegral.msdk.base.controller.a.d().h(), this.p);
                            a(campaignEx, com.mintegral.msdk.base.controller.a.d().h(), this.p);
                        }
                        this.e.setReport(true);
                        com.mintegral.msdk.base.common.a.c.b(this.p, this.e, "banner");
                    }
                    if (this.c != null) {
                        this.c.a(this.e, false);
                    }
                    com.mintegral.msdk.base.common.e.c.a(com.mintegral.msdk.base.controller.a.d().h(), this.e, this.p);
                    if (this.t != null) {
                        this.t.finish();
                        this.t = null;
                        g.a("omsdk", " adSession.finish() ");
                        return;
                    }
                    return;
                }
                this.e.setReport(false);
            }
        }
    }

    private synchronized boolean f() {
        boolean isReport;
        isReport = this.e.isReport();
        if (!isReport) {
            this.e.setReport(true);
        }
        return isReport;
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.d && this.i != null) {
            if (this.i.getVisibility() != 0) {
                this.i.setVisibility(0);
                this.i.setOnClickListener(this.y);
            }
            if (this.i.getParent() == null && this.f != null) {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(k.b(com.mintegral.msdk.base.controller.a.d().h(), 12.0f), k.b(com.mintegral.msdk.base.controller.a.d().h(), 12.0f));
                layoutParams.addRule(11);
                layoutParams.addRule(10);
                this.f.addView(this.i, layoutParams);
            }
        }
    }

    public final void a() {
        if (this.t != null) {
            this.t.finish();
            this.t = null;
            g.a("omsdk", " adSession.finish() ");
        }
        if (this.c != null) {
            this.c = null;
        }
        if (this.h != null) {
            this.h.setWebViewListener(null);
        }
        if (this.A != null) {
            this.A.a = null;
            this.A = null;
        }
        if (this.i != null) {
            this.i.setOnClickListener(null);
        }
        if (this.g != null) {
            this.g.setOnClickListener(null);
        }
        if (this.f != null) {
            this.f.removeAllViews();
        }
        if (this.h != null) {
            this.h.release();
        }
        if (this.z != null) {
            this.z = null;
        }
    }

    public final void b(boolean z2) {
        this.j = z2;
        e();
        if (!z2) {
            CampaignEx campaignEx = this.e;
            String str = this.p;
            if (campaignEx != null) {
                String a2 = com.mintegral.msdk.mtgjscommon.mraid.c.a(campaignEx.getId());
                if (!TextUtils.isEmpty(a2)) {
                    new b(com.mintegral.msdk.base.controller.a.d().h()).a(campaignEx.getRequestIdNotice(), campaignEx.getId(), str, a2, campaignEx.isBidCampaign());
                    com.mintegral.msdk.mtgjscommon.mraid.c.b(campaignEx.getId());
                }
            }
        }
    }

    public final void c(boolean z2) {
        this.k = z2;
        e();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    private static void a(CampaignEx campaignEx, Context context, String str) {
        if (campaignEx != null && !TextUtils.isEmpty(campaignEx.getOnlyImpressionURL())) {
            com.mintegral.msdk.click.a.a(context, campaignEx, str, campaignEx.getOnlyImpressionURL(), false, true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void */
    private void b(final CampaignEx campaignEx, final Context context, String str) {
        if (!TextUtils.isEmpty(campaignEx.getImpressionURL())) {
            new Thread(new Runnable() {
                public final void run() {
                    try {
                        m.a(i.a(context)).b(campaignEx.getId());
                    } catch (Exception unused) {
                        g.d(c.b, "campain can't insert db");
                    }
                }
            }).start();
            com.mintegral.msdk.click.a.a(context, campaignEx, str, campaignEx.getImpressionURL(), false, true);
        }
        if (!TextUtils.isEmpty(str) && campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().n() != null) {
            com.mintegral.msdk.click.a.a(context, campaignEx, str, campaignEx.getNativeVideoTracking().n(), false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String[], boolean):void */
    public final void a(CampaignEx campaignEx) {
        if (d()) {
            if (this.a == null) {
                this.a = new com.mintegral.msdk.click.a(com.mintegral.msdk.base.controller.a.d().h(), this.p);
            }
            this.a.a(new NativeListener.TrackingExListener() {
                public final void onDismissLoading(Campaign campaign) {
                }

                public final void onDownloadFinish(Campaign campaign) {
                }

                public final void onDownloadProgress(int i) {
                }

                public final void onDownloadStart(Campaign campaign) {
                }

                public final void onFinishRedirection(Campaign campaign, String str) {
                }

                public final boolean onInterceptDefaultLoadingDialog() {
                    return false;
                }

                public final void onRedirectionFailed(Campaign campaign, String str) {
                }

                public final void onShowLoading(Campaign campaign) {
                }

                public final void onStartRedirection(Campaign campaign, String str) {
                }

                public final void onLeaveApp() {
                    if (c.this.c != null) {
                        c.this.c.b();
                    }
                }
            });
            campaignEx.setCampaignUnitId(this.p);
            this.a.b(campaignEx);
            if (!this.e.isReportClick()) {
                this.e.setReportClick(true);
                Context h2 = com.mintegral.msdk.base.controller.a.d().h();
                if (!(campaignEx == null || campaignEx.getNativeVideoTracking() == null || campaignEx.getNativeVideoTracking().j() == null)) {
                    com.mintegral.msdk.click.a.a(h2, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().j(), false);
                }
            }
            if (this.c != null) {
                this.c.a();
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, int i2) {
        com.mintegral.msdk.mtgbanner.common.a.a a2 = com.mintegral.msdk.mtgbanner.common.a.a.a().c(this.e.getId()).b(this.p).d(this.e.getRequestIdNotice()).f(str).a(i2).a(this.e.isBidCampaign());
        String str2 = this.p;
        if (a2 != null) {
            a2.a("2000068");
            com.mintegral.msdk.base.common.e.a.a(a2.b(), com.mintegral.msdk.base.controller.a.d().h(), str2);
        }
    }

    public final void a(int i2, int i3, int i4, int i5) {
        if (i2 != i4 || i3 != i5) {
            MTGBannerWebView mTGBannerWebView = this.h;
            g.d("BannerCallJS", "fireOnBannerViewSizeChange");
            try {
                com.mintegral.msdk.mtgjscommon.mraid.a.a();
                com.mintegral.msdk.mtgjscommon.mraid.a.a(mTGBannerWebView, (float) i2, (float) i3);
            } catch (Throwable th) {
                g.c("BannerCallJS", "fireOnBannerViewSizeChange", th);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.mintegral.msdk.base.entity.CampaignUnit r10) {
        /*
            r9 = this;
            r0 = 0
            if (r10 == 0) goto L_0x001e
            java.util.ArrayList r10 = r10.getAds()
            r9.q = r10
            java.util.List<com.mintegral.msdk.base.entity.CampaignEx> r10 = r9.q
            if (r10 == 0) goto L_0x001e
            java.util.List<com.mintegral.msdk.base.entity.CampaignEx> r10 = r9.q
            int r10 = r10.size()
            if (r10 <= 0) goto L_0x001e
            java.util.List<com.mintegral.msdk.base.entity.CampaignEx> r10 = r9.q
            java.lang.Object r10 = r10.get(r0)
            com.mintegral.msdk.base.entity.CampaignEx r10 = (com.mintegral.msdk.base.entity.CampaignEx) r10
            goto L_0x001f
        L_0x001e:
            r10 = 0
        L_0x001f:
            r9.e = r10
            com.mintegral.msdk.base.entity.CampaignEx r10 = r9.e
            if (r10 != 0) goto L_0x002b
            java.lang.String r10 = "banner show failed because campain is exception"
            r9.a(r10)
            return
        L_0x002b:
            android.os.Handler r10 = r9.u
            com.mintegral.msdk.base.common.f.a r1 = r9.v
            r10.removeCallbacks(r1)
            boolean r10 = r9.d
            r1 = 8
            if (r10 == 0) goto L_0x0065
            android.widget.ImageView r10 = r9.i
            if (r10 != 0) goto L_0x0065
            android.widget.ImageView r10 = new android.widget.ImageView
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()
            android.content.Context r2 = r2.h()
            r10.<init>(r2)
            r9.i = r10
            android.widget.ImageView r10 = r9.i
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()
            android.content.Context r2 = r2.h()
            java.lang.String r3 = "mintegral_banner_close"
            java.lang.String r4 = "drawable"
            int r2 = com.mintegral.msdk.base.utils.p.a(r2, r3, r4)
            r10.setBackgroundResource(r2)
            android.widget.ImageView r10 = r9.i
            r10.setVisibility(r1)
        L_0x0065:
            r9.l = r0
            r9.m = r0
            com.mintegral.msdk.base.entity.CampaignEx r10 = r9.e
            java.lang.String r10 = r10.getBannerHtml()
            boolean r10 = android.text.TextUtils.isEmpty(r10)
            if (r10 == 0) goto L_0x0081
            com.mintegral.msdk.base.entity.CampaignEx r10 = r9.e
            java.lang.String r10 = r10.getBannerUrl()
            boolean r10 = android.text.TextUtils.isEmpty(r10)
            if (r10 != 0) goto L_0x00c1
        L_0x0081:
            com.mintegral.msdk.mtgbanner.common.a.a r10 = com.mintegral.msdk.mtgbanner.common.a.a.a()
            com.mintegral.msdk.base.entity.CampaignEx r2 = r9.e
            java.lang.String r2 = r2.getId()
            com.mintegral.msdk.mtgbanner.common.a.a r10 = r10.c(r2)
            com.mintegral.msdk.base.entity.CampaignEx r2 = r9.e
            java.lang.String r2 = r2.getRequestIdNotice()
            com.mintegral.msdk.mtgbanner.common.a.a r10 = r10.d(r2)
            java.lang.String r2 = r9.p
            com.mintegral.msdk.mtgbanner.common.a.a r10 = r10.b(r2)
            com.mintegral.msdk.base.entity.CampaignEx r2 = r9.e
            boolean r2 = r2.isBidCampaign()
            com.mintegral.msdk.mtgbanner.common.a.a r10 = r10.a(r2)
            java.lang.String r2 = r9.p
            if (r10 == 0) goto L_0x00c1
            java.lang.String r3 = "2000067"
            r10.a(r3)
            java.lang.String r10 = r10.b()
            com.mintegral.msdk.base.controller.a r3 = com.mintegral.msdk.base.controller.a.d()
            android.content.Context r3 = r3.h()
            com.mintegral.msdk.base.common.e.a.a(r10, r3, r2)
        L_0x00c1:
            android.os.Handler r10 = r9.u
            com.mintegral.msdk.base.common.f.a r2 = r9.v
            long r3 = r9.s
            r10.postDelayed(r2, r3)
            com.mintegral.msdk.base.entity.CampaignEx r10 = r9.e
            java.lang.String r4 = b(r10)
            boolean r10 = android.text.TextUtils.isEmpty(r4)
            r8 = 2
            if (r10 != 0) goto L_0x0183
            com.mintegral.msdk.out.MTGBannerView r10 = r9.f
            if (r10 == 0) goto L_0x0178
            com.mintegral.msdk.mtgbanner.view.MTGBannerWebView r10 = r9.h
            if (r10 != 0) goto L_0x0109
            com.mintegral.msdk.mtgbanner.view.MTGBannerWebView r10 = new com.mintegral.msdk.mtgbanner.view.MTGBannerWebView
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()
            android.content.Context r2 = r2.h()
            r10.<init>(r2)
            r9.h = r10
            com.mintegral.msdk.mtgbanner.view.MTGBannerWebView r10 = r9.h
            android.widget.RelativeLayout$LayoutParams r2 = new android.widget.RelativeLayout$LayoutParams
            r3 = -1
            r2.<init>(r3, r3)
            r10.setLayoutParams(r2)
            com.mintegral.msdk.mtgbanner.view.MTGBannerWebView r10 = r9.h
            com.mintegral.msdk.mtgbanner.view.a r2 = new com.mintegral.msdk.mtgbanner.view.a
            java.lang.String r3 = r9.p
            java.util.List<com.mintegral.msdk.base.entity.CampaignEx> r5 = r9.q
            com.mintegral.msdk.mtgbanner.common.b.a r6 = r9.z
            r2.<init>(r3, r5, r6)
            r10.setWebViewClient(r2)
        L_0x0109:
            android.widget.ImageView r10 = r9.g
            if (r10 == 0) goto L_0x0112
            android.widget.ImageView r10 = r9.g
            r10.setVisibility(r1)
        L_0x0112:
            com.mintegral.msdk.mtgbanner.view.MTGBannerWebView r10 = r9.h
            int r10 = r10.getVisibility()
            if (r10 == 0) goto L_0x011f
            com.mintegral.msdk.mtgbanner.view.MTGBannerWebView r10 = r9.h
            r10.setVisibility(r0)
        L_0x011f:
            com.mintegral.msdk.mtgbanner.view.MTGBannerWebView r10 = r9.h
            android.view.ViewParent r10 = r10.getParent()
            if (r10 != 0) goto L_0x012e
            com.mintegral.msdk.out.MTGBannerView r10 = r9.f
            com.mintegral.msdk.mtgbanner.view.MTGBannerWebView r0 = r9.h
            r10.addView(r0)
        L_0x012e:
            r9.g()
            com.mintegral.msdk.mtgbanner.common.bridge.a r10 = new com.mintegral.msdk.mtgbanner.common.bridge.a
            com.mintegral.msdk.out.MTGBannerView r0 = r9.f
            android.content.Context r0 = r0.getContext()
            java.lang.String r1 = r9.p
            r10.<init>(r0, r1)
            java.util.List<com.mintegral.msdk.base.entity.CampaignEx> r0 = r9.q
            r10.a(r0)
            com.mintegral.msdk.mtgbanner.common.b.a r0 = r9.z
            r10.a(r0)
            int r0 = r9.r
            r10.a(r0)
            com.mintegral.msdk.mtgbanner.view.MTGBannerWebView r0 = r9.h
            com.mintegral.msdk.mtgbanner.common.c.c$a r1 = r9.A
            r0.setWebViewListener(r1)
            com.mintegral.msdk.mtgbanner.view.MTGBannerWebView r0 = r9.h
            r0.setObject(r10)
            java.lang.String r10 = "file"
            boolean r10 = r4.startsWith(r10)
            if (r10 != 0) goto L_0x0172
            com.mintegral.msdk.mtgbanner.view.MTGBannerWebView r2 = r9.h
            com.mintegral.msdk.base.entity.CampaignEx r10 = r9.e
            java.lang.String r3 = r10.getBannerUrl()
            java.lang.String r5 = "text/html"
            java.lang.String r6 = "utf-8"
            r7 = 0
            r2.loadDataWithBaseURL(r3, r4, r5, r6, r7)
            goto L_0x0182
        L_0x0172:
            com.mintegral.msdk.mtgbanner.view.MTGBannerWebView r10 = r9.h
            r10.loadUrl(r4)
            goto L_0x0182
        L_0x0178:
            java.lang.String r10 = "banner render failed because banner view is null"
            r9.a(r10, r8)
            java.lang.String r10 = "banner show failed because banner view is exception"
            r9.a(r10)
        L_0x0182:
            r0 = 1
        L_0x0183:
            if (r0 != 0) goto L_0x01a5
            com.mintegral.msdk.base.entity.CampaignEx r10 = r9.e
            java.lang.String r10 = r10.getBannerHtml()
            boolean r10 = android.text.TextUtils.isEmpty(r10)
            if (r10 == 0) goto L_0x019d
            com.mintegral.msdk.base.entity.CampaignEx r10 = r9.e
            java.lang.String r10 = r10.getBannerUrl()
            boolean r10 = android.text.TextUtils.isEmpty(r10)
            if (r10 != 0) goto L_0x01a2
        L_0x019d:
            java.lang.String r10 = "banner render failed because res load failed"
            r9.a(r10, r8)
        L_0x01a2:
            r9.c()
        L_0x01a5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.mtgbanner.common.c.c.a(com.mintegral.msdk.base.entity.CampaignUnit):void");
    }

    static /* synthetic */ void b(c cVar) {
        if (!(cVar.h == null || cVar.h.getParent() == null)) {
            cVar.f.removeView(cVar.h);
        }
        if (!(cVar.g == null || cVar.g.getParent() == null)) {
            cVar.g.setVisibility(8);
            cVar.f.removeView(cVar.g);
        }
        if (!(cVar.i == null || cVar.i.getParent() == null)) {
            cVar.f.removeView(cVar.i);
            cVar.i.setVisibility(8);
        }
        com.mintegral.msdk.mtgbanner.common.a.a c2 = com.mintegral.msdk.mtgbanner.common.a.a.a().b(cVar.p).d(cVar.e.getRequestIdNotice()).c(cVar.e.getId());
        StringBuilder sb = new StringBuilder();
        sb.append(cVar.e.getCreativeId());
        com.mintegral.msdk.mtgbanner.common.a.a a2 = c2.e(sb.toString()).a(cVar.e.isBidCampaign());
        String str = cVar.p;
        if (a2 != null) {
            a2.a("2000069");
            com.mintegral.msdk.base.common.e.a.a(a2.b(), com.mintegral.msdk.base.controller.a.d().h(), str);
        }
        BannerUtils.inserCloseId(cVar.p, cVar.q);
    }

    static /* synthetic */ void c(c cVar) {
        if (cVar.d && cVar.i != null && cVar.i.getVisibility() == 0) {
            cVar.i.setVisibility(8);
            cVar.i.setOnClickListener(null);
            if (cVar.f != null && cVar.i.getParent() != null) {
                cVar.f.removeView(cVar.i);
            }
        }
    }

    static /* synthetic */ void q(c cVar) {
        if (cVar.g != null) {
            if (cVar.h != null) {
                cVar.h.setVisibility(8);
            }
            if (cVar.g.getVisibility() != 0) {
                cVar.g.setVisibility(0);
            }
            if (cVar.f != null) {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                layoutParams.addRule(10);
                cVar.g.setScaleType(ImageView.ScaleType.FIT_XY);
                if (cVar.g.getParent() == null) {
                    cVar.f.addView(cVar.g, layoutParams);
                }
            }
        }
    }
}
