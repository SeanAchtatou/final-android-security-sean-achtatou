package com.mintegral.msdk.mtgbanner.common.c;

import android.content.Context;
import android.os.Handler;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.d.d;
import com.mintegral.msdk.mtgbanner.common.a.b;
import com.mintegral.msdk.mtgbanner.common.a.c;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: BannerLoadManager */
public class a {
    /* access modifiers changed from: private */
    public static final String a = "a";
    private static volatile a h;
    private Context b = com.mintegral.msdk.base.controller.a.d().h();
    /* access modifiers changed from: private */
    public com.mintegral.msdk.mtgbanner.common.util.a c = new com.mintegral.msdk.mtgbanner.common.util.a();
    private Map<String, c> d = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public Map<String, Boolean> e = new ConcurrentHashMap();
    private Map<String, Handler> f = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public Map<String, Integer> g = new ConcurrentHashMap();

    public static a a() {
        if (h == null) {
            synchronized (a.class) {
                if (h == null) {
                    h = new a();
                }
            }
        }
        return h;
    }

    private a() {
    }

    public final void a(String str, final b bVar, com.mintegral.msdk.mtgbanner.common.b.b bVar2) {
        c cVar;
        if (this.b == null) {
            this.c.a(bVar2, "Banner Context == null!", str);
        } else if (bVar == null || bVar2 == null) {
            this.c.a(bVar2, "Banner request parameters or callback empty!", str);
        } else {
            synchronized (a()) {
                if (this.e == null || !this.e.containsKey(str) || !this.e.get(str).booleanValue()) {
                    this.e.put(str, true);
                    if (this.d.containsKey(str)) {
                        cVar = this.d.get(str);
                    } else {
                        String j = com.mintegral.msdk.base.controller.a.d().j();
                        com.mintegral.msdk.d.b.a();
                        d c2 = com.mintegral.msdk.d.b.c(j, str);
                        if (c2 == null) {
                            c2 = d.b(str);
                        }
                        c cVar2 = new c(str, "", c2.z() * 1);
                        this.d.put(str, cVar2);
                        cVar = cVar2;
                    }
                    new b(this.b, cVar, bVar2, this.c).a(str, bVar, new com.mintegral.msdk.mtgbanner.common.b.d() {
                        public final void a(String str) {
                            synchronized (a.a()) {
                                bVar.a("");
                                a.this.e.put(str, false);
                            }
                        }
                    });
                    return;
                }
                this.c.a(bVar2, "Current unit is loading!", str);
            }
        }
    }

    public final void b(final String str, final b bVar, final com.mintegral.msdk.mtgbanner.common.b.b bVar2) {
        Handler handler;
        if (bVar == null || bVar.b() <= 0) {
            String str2 = a;
            g.d(str2, "doUnitRotation: Illegal banner request parameters! && unitId=" + str);
            return;
        }
        if (this.f.containsKey(str)) {
            handler = this.f.get(str);
        } else {
            handler = new Handler();
            this.f.put(str, handler);
        }
        AnonymousClass2 r1 = new Runnable() {
            public final void run() {
                int intValue;
                if (a.this.e != null && a.this.e.containsKey(str) && ((Boolean) a.this.e.get(str)).booleanValue()) {
                    return;
                }
                if (!a.this.g.containsKey(str) || !((intValue = ((Integer) a.this.g.get(str)).intValue()) == 2 || intValue == 4)) {
                    a.this.a(str, bVar, bVar2);
                    return;
                }
                String c2 = a.a;
                g.d(c2, "doUnitRotation: autoRotationStatus=" + intValue + " && unitId=" + str);
                if (a.this.c != null) {
                    a.this.c.a(bVar2, "banner load failed because env is exception", str);
                }
            }
        };
        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(r1, (long) bVar.b());
    }

    public final void a(String str) {
        if (this.f.containsKey(str)) {
            this.f.get(str).removeCallbacksAndMessages(null);
            this.f.remove(str);
        }
    }

    public final void a(int i, String str, b bVar, com.mintegral.msdk.mtgbanner.common.b.b bVar2) {
        int intValue = this.g.containsKey(str) ? this.g.get(str).intValue() : 0;
        switch (i) {
            case 1:
                if (this.f.containsKey(str)) {
                    this.f.get(str).removeCallbacksAndMessages(null);
                }
                this.g.put(str, Integer.valueOf(i));
                return;
            case 2:
                if (intValue == 1) {
                    if (this.f.containsKey(str)) {
                        this.f.get(str).removeCallbacksAndMessages(null);
                    }
                    this.g.put(str, Integer.valueOf(i));
                    return;
                }
                return;
            case 3:
                if (intValue == 2 || intValue == 4) {
                    this.g.put(str, 1);
                    b(str, bVar, bVar2);
                    return;
                }
                return;
            case 4:
                if (intValue == 0) {
                    this.g.put(str, 0);
                    return;
                }
                if (this.f.containsKey(str)) {
                    this.f.get(str).removeCallbacksAndMessages(null);
                }
                this.g.put(str, Integer.valueOf(i));
                return;
            default:
                return;
        }
    }

    public final void b() {
        if (this.d != null) {
            this.d.clear();
        }
        if (this.e != null) {
            this.e.clear();
        }
        if (this.f != null) {
            for (Map.Entry next : this.f.entrySet()) {
                if (next.getValue() != null) {
                    ((Handler) next.getValue()).removeCallbacksAndMessages(null);
                }
            }
            this.f.clear();
        }
        if (this.g != null) {
            this.g.clear();
        }
    }
}
