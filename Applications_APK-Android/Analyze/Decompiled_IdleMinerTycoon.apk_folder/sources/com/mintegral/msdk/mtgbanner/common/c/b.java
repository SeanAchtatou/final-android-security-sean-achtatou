package com.mintegral.msdk.mtgbanner.common.c;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.mtgbanner.common.a.c;
import com.mintegral.msdk.mtgbanner.common.b.d;
import com.mintegral.msdk.mtgbanner.common.b.f;
import com.mintegral.msdk.mtgbanner.common.util.BannerUtils;
import com.mintegral.msdk.mtgbanner.common.util.a;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: BannerLoader */
public class b {
    /* access modifiers changed from: private */
    public static final String a = "b";
    /* access modifiers changed from: private */
    public Context b;
    private int c = 0;
    private c d;
    /* access modifiers changed from: private */
    public a e;
    /* access modifiers changed from: private */
    public com.mintegral.msdk.mtgbanner.common.b.b f;
    /* access modifiers changed from: private */
    public d g;
    /* access modifiers changed from: private */
    public volatile boolean h = false;
    private Timer i = new Timer();
    private volatile List<String> j = new ArrayList();
    private volatile boolean k = false;
    private volatile boolean l = false;
    private volatile boolean m = false;

    public b(Context context, c cVar, com.mintegral.msdk.mtgbanner.common.b.b bVar, a aVar) {
        this.b = context.getApplicationContext();
        this.d = cVar;
        this.f = bVar;
        this.e = aVar;
    }

    private void a(String str) {
        if (this.m) {
            return;
        }
        if ((this.k || this.l) && this.j.size() == 0) {
            g.b(a, "在子线程处理业务逻辑 完成");
            this.h = true;
            this.m = true;
            this.i.cancel();
            this.e.a(this.f, str);
            this.g.a(str);
        }
    }

    public final void a(String str, int i2, String str2, boolean z) {
        if (!z) {
            if (i2 == -1) {
                String str3 = a;
                g.d(str3, " unitId =" + str + " --> time out!");
            }
            this.i.cancel();
            g.b(a, "在子线程处理业务逻辑 完成");
            g.b(a, "downloadResource--> Fail");
            this.h = true;
            this.e.b(this.f, str);
            this.g.a(str);
        } else if (i2 == 1) {
            g.b(a, "downloadResource--> Success Image");
            synchronized (this) {
                this.j.remove(str2);
                if (this.j.size() == 0) {
                    a(str);
                }
            }
        } else if (i2 == 2) {
            g.b(a, "downloadResource--> Success banner_html");
            this.l = true;
            a(str);
        } else if (i2 == 3) {
            g.b(a, "downloadResource--> Success banner_url");
            this.k = true;
            a(str);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r2 = com.mintegral.msdk.base.common.a.i;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0065 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String r6, com.mintegral.msdk.mtgbanner.common.a.b r7, com.mintegral.msdk.mtgbanner.common.b.d r8) {
        /*
            r5 = this;
            java.lang.String r0 = com.mintegral.msdk.mtgbanner.common.c.b.a     // Catch:{ Exception -> 0x0072 }
            java.lang.String r1 = "requestCampaign--> started"
            com.mintegral.msdk.base.utils.g.b(r0, r1)     // Catch:{ Exception -> 0x0072 }
            r5.g = r8     // Catch:{ Exception -> 0x0072 }
            com.mintegral.msdk.mtgbanner.common.c.b$2 r8 = new com.mintegral.msdk.mtgbanner.common.c.b$2     // Catch:{ Exception -> 0x0072 }
            r8.<init>()     // Catch:{ Exception -> 0x0072 }
            r8.d = r6     // Catch:{ Exception -> 0x0072 }
            com.mintegral.msdk.mtgbanner.common.e.a r0 = new com.mintegral.msdk.mtgbanner.common.e.a     // Catch:{ Exception -> 0x0072 }
            android.content.Context r1 = r5.b     // Catch:{ Exception -> 0x0072 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0072 }
            int r1 = r5.b()     // Catch:{ Exception -> 0x0072 }
            r5.c = r1     // Catch:{ Exception -> 0x0072 }
            android.content.Context r1 = r5.b     // Catch:{ Exception -> 0x0072 }
            com.mintegral.msdk.mtgbanner.common.a.c r2 = r5.d     // Catch:{ Exception -> 0x0072 }
            java.lang.String r2 = r2.a()     // Catch:{ Exception -> 0x0072 }
            int r3 = r5.c     // Catch:{ Exception -> 0x0072 }
            com.mintegral.msdk.base.common.net.l r1 = com.mintegral.msdk.mtgbanner.common.a.d.a(r1, r6, r2, r3, r7)     // Catch:{ Exception -> 0x0072 }
            java.lang.String r2 = com.mintegral.msdk.base.common.a.k     // Catch:{ Exception -> 0x0072 }
            java.lang.String r7 = r7.c()     // Catch:{ Exception -> 0x0072 }
            boolean r3 = android.text.TextUtils.isEmpty(r7)     // Catch:{ Exception -> 0x0072 }
            if (r3 != 0) goto L_0x0068
            r8.b(r7)     // Catch:{ Exception -> 0x0065 }
            com.mintegral.msdk.mtgbanner.common.util.a r2 = r5.e     // Catch:{ Exception -> 0x0065 }
            r3 = 1
            r2.a(r3)     // Catch:{ Exception -> 0x0065 }
            java.lang.String r2 = "_"
            java.lang.String[] r7 = r7.split(r2)     // Catch:{ Exception -> 0x0065 }
            if (r7 == 0) goto L_0x0061
            int r2 = r7.length     // Catch:{ Exception -> 0x0065 }
            if (r2 <= r3) goto L_0x0061
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0065 }
            java.lang.String r4 = "https://"
            r2.<init>(r4)     // Catch:{ Exception -> 0x0065 }
            r7 = r7[r3]     // Catch:{ Exception -> 0x0065 }
            r2.append(r7)     // Catch:{ Exception -> 0x0065 }
            java.lang.String r7 = "-hb.rayjump.com/load"
            r2.append(r7)     // Catch:{ Exception -> 0x0065 }
            java.lang.String r7 = r2.toString()     // Catch:{ Exception -> 0x0065 }
            goto L_0x0063
        L_0x0061:
            java.lang.String r7 = com.mintegral.msdk.base.common.a.i     // Catch:{ Exception -> 0x0065 }
        L_0x0063:
            r2 = r7
            goto L_0x006e
        L_0x0065:
            java.lang.String r2 = com.mintegral.msdk.base.common.a.i     // Catch:{ Exception -> 0x0072 }
            goto L_0x006e
        L_0x0068:
            com.mintegral.msdk.mtgbanner.common.util.a r7 = r5.e     // Catch:{ Exception -> 0x0072 }
            r3 = 0
            r7.a(r3)     // Catch:{ Exception -> 0x0072 }
        L_0x006e:
            r0.a(r2, r1, r8)     // Catch:{ Exception -> 0x0072 }
            return
        L_0x0072:
            r7 = move-exception
            r7.printStackTrace()
            com.mintegral.msdk.mtgbanner.common.util.a r8 = r5.e
            com.mintegral.msdk.mtgbanner.common.b.b r0 = r5.f
            java.lang.String r7 = r7.getMessage()
            r8.a(r0, r7, r6)
            com.mintegral.msdk.mtgbanner.common.b.d r7 = r5.g
            r7.a(r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.mtgbanner.common.c.b.a(java.lang.String, com.mintegral.msdk.mtgbanner.common.a.b, com.mintegral.msdk.mtgbanner.common.b.d):void");
    }

    private List<CampaignEx> a(CampaignUnit campaignUnit) {
        ArrayList arrayList = new ArrayList();
        if (campaignUnit != null) {
            try {
                if (campaignUnit.getAds() != null && campaignUnit.getAds().size() > 0) {
                    ArrayList<CampaignEx> ads = campaignUnit.getAds();
                    String str = a;
                    g.b(str, "getNeedShowList 总共返回的campaign有：" + ads.size());
                    k.a((List<CampaignEx>) ads);
                    for (int i2 = 0; i2 < ads.size(); i2++) {
                        CampaignEx campaignEx = ads.get(i2);
                        if (!(campaignEx == null || campaignEx.getOfferType() == 99)) {
                            if (campaignEx.getWtick() != 1) {
                                if (k.a(this.b, campaignEx.getPackageName())) {
                                    if (k.b(campaignEx) || k.a(campaignEx)) {
                                        arrayList.add(campaignEx);
                                    }
                                }
                            }
                            arrayList.add(campaignEx);
                        }
                    }
                    String str2 = a;
                    g.b(str2, "getNeedShowList 返回有以下带有视频素材的campaign：" + arrayList.size());
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return arrayList;
    }

    private void a(String str, List<CampaignEx> list) {
        if (list != null && list.size() > 0) {
            for (CampaignEx next : list) {
                if (!TextUtils.isEmpty(next.getImageUrl())) {
                    this.j.add(next.getImageUrl());
                    com.mintegral.msdk.base.common.c.b.a(this.b).a(next.getImageUrl(), new com.mintegral.msdk.mtgbanner.common.b.g(this, str));
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0059 A[SYNTHETIC, Splitter:B:24:0x0059] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0086 A[SYNTHETIC, Splitter:B:37:0x0086] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(java.lang.String r7, java.lang.String r8) {
        /*
            r6 = this;
            java.lang.String r0 = ""
            boolean r1 = android.text.TextUtils.isEmpty(r8)
            if (r1 != 0) goto L_0x008f
            r1 = 0
            com.mintegral.msdk.base.common.b.c r2 = com.mintegral.msdk.base.common.b.c.MINTEGRAL_700_HTML     // Catch:{ Exception -> 0x0053 }
            java.lang.String r2 = com.mintegral.msdk.base.common.b.e.b(r2)     // Catch:{ Exception -> 0x0053 }
            java.lang.String r3 = com.mintegral.msdk.base.utils.CommonMD5.getMD5(r8)     // Catch:{ Exception -> 0x0053 }
            boolean r4 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x0053 }
            if (r4 == 0) goto L_0x0021
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0053 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x0053 }
        L_0x0021:
            java.lang.String r4 = ".html"
            java.lang.String r3 = r3.concat(r4)     // Catch:{ Exception -> 0x0053 }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0053 }
            r4.<init>(r2, r3)     // Catch:{ Exception -> 0x0053 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0053 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0053 }
            byte[] r1 = r8.getBytes()     // Catch:{ Exception -> 0x004c, all -> 0x0049 }
            r2.write(r1)     // Catch:{ Exception -> 0x004c, all -> 0x0049 }
            r2.flush()     // Catch:{ Exception -> 0x004c, all -> 0x0049 }
            java.lang.String r1 = r4.getAbsolutePath()     // Catch:{ Exception -> 0x004c, all -> 0x0049 }
            r2.close()     // Catch:{ Exception -> 0x0043 }
            goto L_0x0047
        L_0x0043:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0047:
            r0 = r1
            goto L_0x0061
        L_0x0049:
            r7 = move-exception
            r1 = r2
            goto L_0x0084
        L_0x004c:
            r1 = move-exception
            r5 = r2
            r2 = r1
            r1 = r5
            goto L_0x0054
        L_0x0051:
            r7 = move-exception
            goto L_0x0084
        L_0x0053:
            r2 = move-exception
        L_0x0054:
            r2.printStackTrace()     // Catch:{ all -> 0x0051 }
            if (r1 == 0) goto L_0x0061
            r1.close()     // Catch:{ Exception -> 0x005d }
            goto L_0x0061
        L_0x005d:
            r1 = move-exception
            r1.printStackTrace()
        L_0x0061:
            java.io.File r1 = new java.io.File
            r1.<init>(r0)
            boolean r2 = r1.exists()
            r3 = 2
            if (r2 == 0) goto L_0x007f
            boolean r2 = r1.isFile()
            if (r2 == 0) goto L_0x007f
            boolean r1 = r1.canRead()
            if (r1 != 0) goto L_0x007a
            goto L_0x007f
        L_0x007a:
            r1 = 1
            r6.a(r7, r3, r8, r1)
            goto L_0x008f
        L_0x007f:
            r1 = 0
            r6.a(r7, r3, r8, r1)
            goto L_0x008f
        L_0x0084:
            if (r1 == 0) goto L_0x008e
            r1.close()     // Catch:{ Exception -> 0x008a }
            goto L_0x008e
        L_0x008a:
            r8 = move-exception
            r8.printStackTrace()
        L_0x008e:
            throw r7
        L_0x008f:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.mtgbanner.common.c.b.a(java.lang.String, java.lang.String):java.lang.String");
    }

    private int b() {
        try {
            int b2 = this.d.b();
            if (b2 > this.d.c()) {
                return 0;
            }
            return b2;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    static /* synthetic */ void a(b bVar, final String str, final CampaignUnit campaignUnit) {
        if (campaignUnit == null) {
            bVar.e.a(bVar.f, "campaignUnit is NULL!", str);
            bVar.g.a(str);
            return;
        }
        List<CampaignEx> a2 = bVar.a(campaignUnit);
        if (a2.size() == 0) {
            g.b(a, "tryDownloadOnLoadSuccess 返回的campaign 没有符合下载规则的");
            bVar.e.a(bVar.f, "Need show campaign list is NULL!", str);
            bVar.g.a(str);
        }
        g.b(a, "在子线程处理业务逻辑 开始");
        bVar.i.schedule(new TimerTask() {
            public final void run() {
                if (!b.this.h) {
                    boolean unused = b.this.h = true;
                    b.this.a(str, -1, "", false);
                }
            }
        }, 60000);
        bVar.d.a(campaignUnit.getSessionId());
        int i2 = bVar.c;
        int i3 = 0;
        try {
            if (a2.size() > 0) {
                i2 += a2.size();
            }
            if (i2 > bVar.d.c()) {
                g.b(a, "saveNextOffset 重置offset为0");
                i2 = 0;
            }
            String str2 = a;
            g.b(str2, "saveNextOffset 算出 下次的offset是:" + i2);
            if (s.b(str)) {
                bVar.d.a(i2);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        String trim = campaignUnit.getBannerUrl().trim();
        if (!TextUtils.isEmpty(trim)) {
            if (!TextUtils.isEmpty(trim)) {
                com.mintegral.msdk.videocommon.download.g.a().a(trim, new f(bVar, str));
            }
            if (a2.size() > 0) {
                while (i3 < a2.size()) {
                    a2.get(i3).setBannerUrl(campaignUnit.getBannerUrl());
                    a2.get(i3).setHasMtgTplMark(true);
                    i3++;
                }
            }
        } else {
            String trim2 = campaignUnit.getBannerHtml().trim();
            if (!TextUtils.isEmpty(trim2)) {
                String a3 = bVar.a(str, trim2);
                if (a2.size() > 0) {
                    while (i3 < a2.size()) {
                        a2.get(i3).setBannerHtml(a3);
                        a2.get(i3).setHasMtgTplMark(trim2.contains("<MTGTPLMARK>"));
                        i3++;
                    }
                }
            } else {
                bVar.l = true;
                bVar.k = true;
            }
        }
        bVar.a(str, a2);
        new Thread(new Runnable() {
            public final void run() {
                g.b(b.a, "在单独子线程保存数据库 开始");
                m.a(i.a(b.this.b)).d();
                if (!(campaignUnit == null || campaignUnit.getAds() == null || campaignUnit.getAds().size() <= 0)) {
                    BannerUtils.updateInstallList(b.this.b, campaignUnit.getAds());
                }
                g.b(b.a, "在单独子线程保存数据库 完成");
            }
        }).start();
    }
}
