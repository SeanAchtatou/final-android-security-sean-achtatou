package com.mintegral.msdk.mtgbanner.common.a;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.a.c;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.n;
import com.mintegral.msdk.mtgbanner.common.util.BannerUtils;

/* compiled from: BannerV3Params */
public class d {
    private static final String a = "d";

    public static l a(Context context, String str, String str2, int i, b bVar) {
        String j = a.d().j();
        String md5 = CommonMD5.getMD5(a.d().j() + a.d().k());
        String ttcIds = BannerUtils.getTtcIds(context, str);
        String excludeIds = BannerUtils.getExcludeIds(context, str);
        String installIds = BannerUtils.getInstallIds();
        String a2 = c.a(str, "banner");
        String closeIds = BannerUtils.getCloseIds(str);
        l lVar = new l();
        n.a(lVar, "app_id", j);
        n.a(lVar, MIntegralConstans.PROPERTIES_UNIT_ID, str);
        n.a(lVar, "sign", md5);
        n.a(lVar, "only_impression", "1");
        n.a(lVar, "ping_mode", "1");
        n.a(lVar, "ttc_ids", ttcIds);
        n.a(lVar, "exclude_ids", excludeIds);
        n.a(lVar, "install_ids", installIds);
        n.a(lVar, CampaignEx.JSON_KEY_AD_SOURCE_ID, "1");
        n.a(lVar, "session_id", str2);
        n.a(lVar, "ad_type", "296");
        n.a(lVar, "offset", String.valueOf(i));
        n.a(lVar, "display_info", a2);
        n.a(lVar, "close_id", closeIds);
        n.a(lVar, "unit_size", bVar.a());
        StringBuilder sb = new StringBuilder();
        sb.append(bVar.b());
        n.a(lVar, "refresh_time", sb.toString());
        if (!TextUtils.isEmpty(bVar.c())) {
            n.a(lVar, "token", bVar.c());
        }
        return lVar;
    }
}
