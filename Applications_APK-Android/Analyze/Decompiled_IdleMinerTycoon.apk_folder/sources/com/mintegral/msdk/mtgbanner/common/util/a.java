package com.mintegral.msdk.mtgbanner.common.util;

import android.os.Handler;
import android.os.Looper;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgbanner.common.b.b;

/* compiled from: BannerDelivery */
public class a {
    private static final String a = "a";
    private final Handler b = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public boolean c;

    public final void a(boolean z) {
        this.c = z;
    }

    public final void a(final b bVar, final CampaignUnit campaignUnit, final String str) {
        String str2 = a;
        g.d(str2, "postCampaignSuccess unitId=" + str);
        this.b.post(new Runnable() {
            public final void run() {
                if (bVar != null) {
                    b bVar = bVar;
                    CampaignUnit campaignUnit = campaignUnit;
                    boolean unused = a.this.c;
                    bVar.a(campaignUnit);
                }
            }
        });
    }

    public final void a(final b bVar, final String str, final String str2) {
        String str3 = a;
        g.b(str3, "postCampaignFail errorMsg=" + str + " unitId=" + str2);
        this.b.post(new Runnable() {
            public final void run() {
                if (bVar != null) {
                    bVar.a(str, a.this.c);
                }
            }
        });
    }

    public final void a(final b bVar, final String str) {
        String str2 = a;
        g.d(str2, "postResourceSuccess unitId=" + str);
        this.b.post(new Runnable() {
            public final void run() {
                if (bVar != null) {
                    bVar.a(a.this.c);
                }
            }
        });
    }

    public final void b(final b bVar, final String str) {
        String str2 = a;
        g.d(str2, "postResourceFail unitId=" + str);
        this.b.post(new Runnable() {
            public final void run() {
                if (bVar != null) {
                    bVar.b(a.this.c);
                }
            }
        });
    }
}
