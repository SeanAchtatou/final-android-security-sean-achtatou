package com.mintegral.msdk.mtgbanner.view;

import android.content.Context;
import android.util.AttributeSet;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;

public class MTGBannerWebView extends WindVaneWebView {
    public MTGBannerWebView(Context context) {
        super(context);
    }

    public MTGBannerWebView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MTGBannerWebView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
