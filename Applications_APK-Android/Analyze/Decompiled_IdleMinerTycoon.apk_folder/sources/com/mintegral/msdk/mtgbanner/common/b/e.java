package com.mintegral.msdk.mtgbanner.common.b;

import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.d.d;
import java.util.List;
import java.util.Random;

/* compiled from: DeductionBannerShowListener */
public class e implements c {
    private static final String a = "e";
    private c b;
    private d c;
    private boolean d = false;

    public e(c cVar, d dVar) {
        boolean z = false;
        this.c = dVar;
        this.b = cVar;
        if (!(dVar == null || dVar.d() == 1.0d)) {
            if (new Random().nextDouble() > dVar.d()) {
                z = true;
            }
        }
        this.d = z;
    }

    public final void a(List<CampaignEx> list) {
        if (this.b != null) {
            this.b.a(list);
        }
    }

    public final void a(String str) {
        if (this.b != null) {
            this.b.a(str);
        }
    }

    public final void a() {
        if (this.b != null && !this.d) {
            this.b.a();
        }
    }

    public final void b() {
        if (this.b != null) {
            this.b.b();
        }
    }

    public final void c() {
        if (this.b != null) {
            this.b.c();
        }
    }

    public final void d() {
        if (this.b != null) {
            this.b.d();
        }
    }

    public final void a(CampaignEx campaignEx, boolean z) {
        if (this.b != null) {
            this.b.a(campaignEx, this.d);
        }
    }
}
