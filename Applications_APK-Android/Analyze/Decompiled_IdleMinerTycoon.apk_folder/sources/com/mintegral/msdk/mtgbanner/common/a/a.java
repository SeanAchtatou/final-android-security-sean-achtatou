package com.mintegral.msdk.mtgbanner.common.a;

import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.utils.c;

/* compiled from: BannerReportData */
public final class a {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private int g;
    private boolean h;

    public final a a(boolean z) {
        this.h = z;
        return this;
    }

    private a() {
    }

    public static a a() {
        return new a();
    }

    public final a a(String str) {
        this.a = str;
        return this;
    }

    public final a b(String str) {
        this.b = str;
        return this;
    }

    public final a c(String str) {
        this.c = str;
        return this;
    }

    public final a d(String str) {
        this.d = str;
        return this;
    }

    public final a e(String str) {
        this.e = str;
        return this;
    }

    public final a f(String str) {
        this.f = str;
        return this;
    }

    public final a a(int i) {
        this.g = i;
        return this;
    }

    public final String b() {
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(this.b)) {
            sb.append("unit_id=");
            sb.append(this.b);
            sb.append(Constants.RequestParameters.AMPERSAND);
        }
        if (!TextUtils.isEmpty(this.c)) {
            sb.append("cid=");
            sb.append(this.c);
            sb.append(Constants.RequestParameters.AMPERSAND);
        }
        if (!TextUtils.isEmpty(this.d)) {
            sb.append("rid_n=");
            sb.append(this.d);
            sb.append(Constants.RequestParameters.AMPERSAND);
        }
        if (!TextUtils.isEmpty(this.e)) {
            sb.append("creative_id=");
            sb.append(this.e);
            sb.append(Constants.RequestParameters.AMPERSAND);
        }
        if (!TextUtils.isEmpty(this.f)) {
            sb.append("reason=");
            sb.append(this.f);
            sb.append(Constants.RequestParameters.AMPERSAND);
        }
        if (this.g != 0) {
            sb.append("result=");
            sb.append(this.g);
            sb.append(Constants.RequestParameters.AMPERSAND);
        }
        if (this.h) {
            sb.append("hb=1&");
        }
        sb.append("network_type=");
        sb.append(c.s(com.mintegral.msdk.base.controller.a.d().h()));
        sb.append(Constants.RequestParameters.AMPERSAND);
        if (!TextUtils.isEmpty(this.a)) {
            sb.append("key=");
            sb.append(this.a);
        }
        return sb.toString();
    }
}
