package com.mintegral.msdk.mtgbanner.common.util;

import android.content.Context;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.k;
import com.tapjoy.TapjoyConstants;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: DeviceInfo */
public final class b {
    public String a = c.d();
    public String b = c.i();
    public String c = "android";
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public String i;
    public String j;
    public String k;
    public String l;
    public String m;
    public String n;
    public String o;
    public String p;

    public b(Context context) {
        this.d = c.c(context);
        this.e = c.f(context);
        this.f = c.k();
        int s = c.s(context);
        this.h = String.valueOf(s);
        this.i = c.a(context, s);
        this.j = c.r(context);
        this.k = a.d().k();
        this.l = a.d().j();
        this.m = String.valueOf(k.i(context));
        this.n = String.valueOf(k.h(context));
        this.p = String.valueOf(k.c(context));
        if (context.getResources().getConfiguration().orientation == 2) {
            this.o = "landscape";
        } else {
            this.o = "portrait";
        }
        this.g = c.b(context);
    }

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            com.mintegral.msdk.base.controller.authoritycontroller.a.a();
            if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                jSONObject.put("device", this.a);
                jSONObject.put("system_version", this.b);
                jSONObject.put("network_type", this.h);
                jSONObject.put("network_type_str", this.i);
                jSONObject.put("device_ua", this.j);
            }
            jSONObject.put("plantform", this.c);
            com.mintegral.msdk.base.controller.authoritycontroller.a.a();
            if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITYIMEIMAC)) {
                jSONObject.put("device_imei", this.d);
            }
            com.mintegral.msdk.base.controller.authoritycontroller.a.a();
            if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_ANDROID_ID)) {
                jSONObject.put(TapjoyConstants.TJC_ANDROID_ID, this.e);
            }
            com.mintegral.msdk.base.controller.authoritycontroller.a.a();
            if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
                jSONObject.put("google_ad_id", this.f);
                jSONObject.put("oaid", this.g);
            }
            jSONObject.put("appkey", this.k);
            jSONObject.put("appId", this.l);
            jSONObject.put("screen_width", this.m);
            jSONObject.put("screen_height", this.n);
            jSONObject.put("orientation", this.o);
            jSONObject.put("scale", this.p);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }
}
