package com.mintegral.msdk.mtgbanner.common.b;

import com.mintegral.msdk.mtgbanner.common.c.b;
import com.mintegral.msdk.videocommon.download.g;

/* compiled from: DownloadBannerUrlListener */
public class f implements g.a {
    private static final String a = "f";
    private String b;
    private b c;

    public f(b bVar, String str) {
        this.c = bVar;
        this.b = str;
    }

    public final void a(String str) {
        String str2 = a;
        com.mintegral.msdk.base.utils.g.b(str2, "DownloadBannerUrlListener HTML SUCCESS:" + str);
        this.c.a(this.b, 3, str, true);
    }

    public final void a(String str, String str2) {
        String str3 = a;
        com.mintegral.msdk.base.utils.g.b(str3, "DownloadBannerUrlListener HTML FAIL:" + str);
        this.c.a(this.b, 3, str, false);
    }
}
