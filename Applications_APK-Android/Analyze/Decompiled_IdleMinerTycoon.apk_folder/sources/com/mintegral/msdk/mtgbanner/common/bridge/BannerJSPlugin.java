package com.mintegral.msdk.mtgbanner.common.bridge;

import android.content.Context;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.mtgjscommon.windvane.i;

public class BannerJSPlugin extends i {
    private final String a = "BannerJSBridge";
    private b b;

    public void initialize(Context context, WindVaneWebView windVaneWebView) {
        super.initialize(context, windVaneWebView);
        try {
            if (context instanceof b) {
                this.b = (b) context;
            } else if (windVaneWebView.getObject() != null && (windVaneWebView.getObject() instanceof b)) {
                this.b = (b) windVaneWebView.getObject();
            }
        } catch (Throwable th) {
            g.c("BannerJSBridge", "initialize", th);
        }
    }

    public void onJSBridgeConnect(Object obj, String str) {
        try {
            g.d("BannerJSBridge", "onJSBridgeConnect");
            if (this.b != null) {
                this.b.a(obj);
            }
        } catch (Throwable th) {
            g.c("BannerJSBridge", "onJSBridgeConnect", th);
        }
    }

    public void readyStatus(Object obj, String str) {
        try {
            g.d("BannerJSBridge", "readyStatus");
            if (this.b != null) {
                this.b.a(obj, str);
            }
        } catch (Throwable th) {
            g.c("BannerJSBridge", "readyStatus", th);
        }
    }

    public void init(Object obj, String str) {
        try {
            g.d("BannerJSBridge", "init");
            if (this.b != null) {
                this.b.b(obj);
            }
        } catch (Throwable th) {
            g.c("BannerJSBridge", "init", th);
        }
    }

    public void click(Object obj, String str) {
        try {
            g.d("BannerJSBridge", "click");
            if (this.b != null) {
                this.b.a(str);
            }
        } catch (Throwable th) {
            g.c("BannerJSBridge", "click", th);
        }
    }

    public void toggleCloseBtn(Object obj, String str) {
        try {
            g.d("BannerJSBridge", "toggleCloseBtn");
            if (this.b != null) {
                this.b.b(str);
            }
        } catch (Throwable th) {
            g.c("BannerJSBridge", "toggleCloseBtn", th);
        }
    }

    public void triggerCloseBtn(Object obj, String str) {
        try {
            g.d("BannerJSBridge", "triggerCloseBtn");
            if (this.b != null) {
                this.b.b(obj, str);
            }
        } catch (Throwable th) {
            g.c("BannerJSBridge", "triggerCloseBtn", th);
        }
    }

    public void sendImpressions(Object obj, String str) {
        try {
            g.d("BannerJSBridge", "sendImpressions");
            if (this.b != null) {
                this.b.c(str);
            }
        } catch (Throwable th) {
            g.c("BannerJSBridge", "sendImpressions", th);
        }
    }

    public void reportUrls(Object obj, String str) {
        try {
            g.d("BannerJSBridge", "reportUrls");
            if (this.b != null) {
                this.b.c(obj, str);
            }
        } catch (Throwable th) {
            g.c("BannerJSBridge", "reportUrls", th);
        }
    }
}
