package com.mintegral.msdk.mtgbanner.common.d;

import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.e.d.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import java.net.URLEncoder;

/* compiled from: BannerReport */
public final class a {
    public static void a(String str, CampaignEx campaignEx, String str2) {
        try {
            com.mintegral.msdk.base.common.e.d.a aVar = new com.mintegral.msdk.base.common.e.d.a(com.mintegral.msdk.base.controller.a.d().h(), 0);
            aVar.c();
            StringBuilder sb = new StringBuilder();
            if (campaignEx.isBidCampaign()) {
                sb.append("hb=1&");
            }
            sb.append("key=");
            sb.append(URLEncoder.encode("2000070", "utf-8"));
            sb.append("&rid_n=");
            sb.append(URLEncoder.encode(campaignEx.getRequestIdNotice(), "utf-8"));
            sb.append("&cid=");
            sb.append(URLEncoder.encode(campaignEx.getId(), "utf-8"));
            sb.append("&unit_id=");
            sb.append(URLEncoder.encode(str, "utf-8"));
            sb.append("&click_url=");
            sb.append(URLEncoder.encode(str2, "utf-8"));
            sb.append("&network_type=");
            sb.append(URLEncoder.encode(String.valueOf(c.s(com.mintegral.msdk.base.controller.a.d().h())), "utf-8"));
            aVar.b(com.mintegral.msdk.base.common.a.f, com.mintegral.msdk.base.common.e.c.a(sb.toString(), com.mintegral.msdk.base.controller.a.d().h(), str), new b() {
                public final void b(String str) {
                    g.a("", "Mraid Expand REPORT SUCCESS");
                }

                public final void a(String str) {
                    g.a("", "Mraid Expand REPORT FAILED");
                }
            });
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }
}
