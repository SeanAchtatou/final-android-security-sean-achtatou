package com.mintegral.msdk.f;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import com.mintegral.msdk.base.b.f;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.u;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.reward.a.b;
import com.mintegral.msdk.reward.a.c;
import java.util.LinkedList;
import java.util.List;

/* compiled from: LoopTimer */
public final class a {
    /* access modifiers changed from: private */
    public long a;
    /* access modifiers changed from: private */
    public boolean b;
    private LinkedList<String> c;
    private LinkedList<String> d;
    private int e;
    private int f;
    private f g;
    private com.mintegral.msdk.videocommon.e.a h;
    private u i;
    private i j;
    /* access modifiers changed from: private */
    public Handler k;

    /* renamed from: com.mintegral.msdk.f.a$a  reason: collision with other inner class name */
    /* compiled from: LoopTimer */
    static class C0057a {
        static a a = new a((byte) 0);
    }

    /* synthetic */ a(byte b2) {
        this();
    }

    private a() {
        this.b = false;
        this.c = new LinkedList<>();
        this.d = new LinkedList<>();
        this.e = 0;
        this.f = 0;
        this.k = new Handler() {
            /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
                return;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void handleMessage(android.os.Message r4) {
                /*
                    r3 = this;
                    com.mintegral.msdk.f.a r0 = com.mintegral.msdk.f.a.this
                    monitor-enter(r0)
                    int r4 = r4.what     // Catch:{ all -> 0x002e }
                    switch(r4) {
                        case 1: goto L_0x000f;
                        case 2: goto L_0x0009;
                        default: goto L_0x0008;
                    }     // Catch:{ all -> 0x002e }
                L_0x0008:
                    goto L_0x002c
                L_0x0009:
                    com.mintegral.msdk.f.a r4 = com.mintegral.msdk.f.a.this     // Catch:{ all -> 0x002e }
                    com.mintegral.msdk.f.a.d(r4)     // Catch:{ all -> 0x002e }
                    goto L_0x002c
                L_0x000f:
                    com.mintegral.msdk.f.a r4 = com.mintegral.msdk.f.a.this     // Catch:{ all -> 0x002e }
                    boolean r4 = r4.b     // Catch:{ all -> 0x002e }
                    if (r4 == 0) goto L_0x0019
                    monitor-exit(r0)     // Catch:{ all -> 0x002e }
                    return
                L_0x0019:
                    com.mintegral.msdk.f.a r4 = com.mintegral.msdk.f.a.this     // Catch:{ all -> 0x002e }
                    com.mintegral.msdk.f.a.c(r4)     // Catch:{ all -> 0x002e }
                    r4 = 1
                    android.os.Message r4 = r3.obtainMessage(r4)     // Catch:{ all -> 0x002e }
                    com.mintegral.msdk.f.a r1 = com.mintegral.msdk.f.a.this     // Catch:{ all -> 0x002e }
                    long r1 = r1.a     // Catch:{ all -> 0x002e }
                    r3.sendMessageDelayed(r4, r1)     // Catch:{ all -> 0x002e }
                L_0x002c:
                    monitor-exit(r0)     // Catch:{ all -> 0x002e }
                    return
                L_0x002e:
                    r4 = move-exception
                    monitor-exit(r0)     // Catch:{ all -> 0x002e }
                    throw r4
                */
                throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.f.a.AnonymousClass1.handleMessage(android.os.Message):void");
            }
        };
    }

    private boolean c(String str) {
        try {
            if (this.g == null) {
                return true;
            }
            long j2 = 0;
            if (this.h != null) {
                j2 = this.h.e();
            }
            int a2 = this.g.a(str, j2);
            if (a2 != -1) {
                if (a2 == 1) {
                    return true;
                }
            } else if (!TextUtils.isEmpty(str)) {
                if (this.c != null && this.c.contains(str)) {
                    this.c.remove(str);
                } else if (this.d != null && this.d.contains(str)) {
                    this.d.remove(str);
                }
                if (this.i != null) {
                    this.i.a(str);
                }
            }
            this.k.sendMessage(this.k.obtainMessage(2));
            return false;
        } catch (Throwable th) {
            g.c("LoopTimer", th.getMessage(), th);
            return true;
        }
    }

    private void a(String str, boolean z) {
        try {
            Context h2 = com.mintegral.msdk.base.controller.a.d().h();
            if (h2 != null) {
                final c cVar = new c(h2, str);
                cVar.a(z);
                cVar.a(new b() {
                    public final void a() {
                    }

                    public final void a(String str) {
                        a.this.k.sendMessage(a.this.k.obtainMessage(2));
                        cVar.a((b) null);
                    }

                    public final void b() {
                        a.this.k.sendMessage(a.this.k.obtainMessage(2));
                        cVar.a((b) null);
                    }
                });
                cVar.f();
            }
        } catch (Exception e2) {
            g.c("LoopTimer", e2.getMessage(), e2);
        }
    }

    public final void a(String str) {
        if (!this.c.contains(str)) {
            this.c.add(str);
            if (this.i != null) {
                this.i.a(str, 94);
            }
        }
    }

    public final void b(String str) {
        if (!this.d.contains(str)) {
            this.d.add(str);
            if (this.i != null) {
                this.i.a(str, 287);
            }
        }
    }

    public final void a(long j2) {
        if (this.j == null) {
            this.j = i.a(com.mintegral.msdk.base.controller.a.d().h());
        }
        if (this.i == null) {
            this.i = u.a(this.j);
        }
        List<String> a2 = this.i.a(287);
        if (a2 != null) {
            this.d.addAll(a2);
            for (String b2 : a2) {
                b(b2);
            }
        }
        List<String> a3 = this.i.a(94);
        if (a3 != null) {
            this.c.addAll(a3);
            for (String a4 : a3) {
                a(a4);
            }
        }
        if (this.g == null) {
            this.g = f.a(this.j);
        }
        if (this.h == null) {
            com.mintegral.msdk.videocommon.e.b.a();
            this.h = com.mintegral.msdk.videocommon.e.b.b();
        }
        this.a = j2;
        this.b = false;
        this.k.sendMessageDelayed(this.k.obtainMessage(1), this.a);
    }

    static /* synthetic */ void c(a aVar) {
        if (aVar.c != null && aVar.c.size() > 0 && aVar.e != 0 && aVar.c.size() > aVar.e) {
            return;
        }
        if (aVar.d == null || aVar.d.size() <= 0 || aVar.f == 0 || aVar.d.size() == aVar.f) {
            aVar.f = 0;
            aVar.e = 0;
            aVar.k.sendMessage(aVar.k.obtainMessage(2));
        }
    }

    static /* synthetic */ void d(a aVar) {
        try {
            if (aVar.c != null && aVar.c.size() > 0 && aVar.e < aVar.c.size()) {
                String str = aVar.c.get(aVar.e);
                aVar.e++;
                if (aVar.c(str)) {
                    aVar.a(str, false);
                }
            } else if (aVar.d != null && aVar.d.size() > 0 && aVar.f < aVar.d.size()) {
                String str2 = aVar.d.get(aVar.f);
                aVar.f++;
                if (aVar.c(str2)) {
                    aVar.a(str2, true);
                }
            }
        } catch (Throwable th) {
            g.c("LoopTimer", th.getMessage(), th);
        }
    }
}
