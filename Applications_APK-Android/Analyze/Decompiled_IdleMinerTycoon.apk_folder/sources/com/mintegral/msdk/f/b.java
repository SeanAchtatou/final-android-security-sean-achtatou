package com.mintegral.msdk.f;

import com.mintegral.msdk.f.a;

/* compiled from: TimerController */
public class b {

    /* compiled from: TimerController */
    static class a {
        static b a = new b((byte) 0);
    }

    /* synthetic */ b(byte b) {
        this();
    }

    private b() {
    }

    public static b getInstance() {
        return a.a;
    }

    public void start() {
        com.mintegral.msdk.d.b.a();
        com.mintegral.msdk.d.a b = com.mintegral.msdk.d.b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b == null) {
            com.mintegral.msdk.d.b.a();
            b = com.mintegral.msdk.d.b.b();
        }
        int b2 = b.b();
        if (b2 > 0) {
            a.C0057a.a.a((long) (b2 * 1000));
        }
    }

    public void addRewardList(String str) {
        a.C0057a.a.a(str);
    }

    public void addInterstitialList(String str) {
        a.C0057a.a.b(str);
    }
}
