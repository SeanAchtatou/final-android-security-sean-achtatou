package com.mintegral.msdk;

public final class R {
    private R() {
    }

    public static final class drawable {
        public static final int mintegral_cm_backward = 2131165397;
        public static final int mintegral_cm_backward_disabled = 2131165398;
        public static final int mintegral_cm_backward_nor = 2131165399;
        public static final int mintegral_cm_backward_selected = 2131165400;
        public static final int mintegral_cm_end_animation = 2131165401;
        public static final int mintegral_cm_exits = 2131165402;
        public static final int mintegral_cm_exits_nor = 2131165403;
        public static final int mintegral_cm_exits_selected = 2131165404;
        public static final int mintegral_cm_forward = 2131165405;
        public static final int mintegral_cm_forward_disabled = 2131165406;
        public static final int mintegral_cm_forward_nor = 2131165407;
        public static final int mintegral_cm_forward_selected = 2131165408;
        public static final int mintegral_cm_head = 2131165409;
        public static final int mintegral_cm_highlight = 2131165410;
        public static final int mintegral_cm_progress = 2131165411;
        public static final int mintegral_cm_refresh = 2131165412;
        public static final int mintegral_cm_refresh_nor = 2131165413;
        public static final int mintegral_cm_refresh_selected = 2131165414;
        public static final int mintegral_cm_tail = 2131165415;

        private drawable() {
        }
    }
}
