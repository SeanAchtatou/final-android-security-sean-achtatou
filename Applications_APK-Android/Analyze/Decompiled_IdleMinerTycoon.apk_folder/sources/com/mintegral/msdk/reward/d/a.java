package com.mintegral.msdk.reward.d;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.common.e.c;
import com.mintegral.msdk.base.common.e.d.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.q;
import com.mintegral.msdk.base.utils.g;
import java.util.List;

/* compiled from: RewardReport */
public class a {
    /* access modifiers changed from: private */
    public static final String a = "com.mintegral.msdk.reward.d.a";

    public static void a(Context context, String str) {
        if (context != null) {
            try {
                w a2 = w.a(i.a(context));
                if (!TextUtils.isEmpty(str) && a2 != null && a2.c() > 0) {
                    List<q> a3 = a2.a("2000022");
                    List<q> a4 = a2.a("2000021");
                    List<q> a5 = a2.a("2000039");
                    List<q> a6 = a2.a("2000043");
                    List<q> a7 = a2.a("2000045");
                    List<q> a8 = a2.a("2000044");
                    String b = q.b(a4);
                    String c = q.c(a3);
                    String g = q.g(a5);
                    String d = q.d(a6);
                    String e = q.e(a7);
                    String f = q.f(a8);
                    StringBuilder sb = new StringBuilder();
                    if (!TextUtils.isEmpty(b)) {
                        sb.append(b);
                    }
                    if (!TextUtils.isEmpty(c)) {
                        sb.append(c);
                    }
                    if (!TextUtils.isEmpty(g)) {
                        sb.append(g);
                    }
                    if (!TextUtils.isEmpty(d)) {
                        sb.append(d);
                    }
                    if (!TextUtils.isEmpty(e)) {
                        sb.append(e);
                    }
                    if (!TextUtils.isEmpty(f)) {
                        sb.append(f);
                    }
                    String str2 = a;
                    g.b(str2, "reward 批量上报：" + ((Object) sb));
                    if (!TextUtils.isEmpty(sb.toString())) {
                        String sb2 = sb.toString();
                        if (context != null && !TextUtils.isEmpty(sb2) && !TextUtils.isEmpty(str)) {
                            try {
                                com.mintegral.msdk.base.common.e.d.a aVar = new com.mintegral.msdk.base.common.e.d.a(context);
                                aVar.c();
                                aVar.b(com.mintegral.msdk.base.common.a.f, c.a(sb2, context, str), new b() {
                                    public final void b(String str) {
                                        g.d(a.a, str);
                                    }

                                    public final void a(String str) {
                                        g.d(a.a, str);
                                    }
                                });
                            } catch (Exception e2) {
                                e2.printStackTrace();
                                g.d(a, e2.getMessage());
                            }
                        }
                    }
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    private static void a(Context context, String str, String str2) {
        if (context != null && !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            try {
                com.mintegral.msdk.base.common.e.d.a aVar = new com.mintegral.msdk.base.common.e.d.a(context);
                aVar.c();
                aVar.b(com.mintegral.msdk.base.common.a.f, c.a(str, context, str2), new b() {
                    public final void b(String str) {
                        g.d(a.a, str);
                    }

                    public final void a(String str) {
                        g.d(a.a, str);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                g.d(a, e.getMessage());
            }
        }
    }

    public static void a(Context context, CampaignEx campaignEx, String str) {
        if (context != null && campaignEx != null) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000048&");
                    if (campaignEx != null) {
                        stringBuffer.append("cid=" + campaignEx.getId() + Constants.RequestParameters.AMPERSAND);
                    }
                    stringBuffer.append("network_type=" + com.mintegral.msdk.base.utils.c.s(context) + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("unit_id=" + str + Constants.RequestParameters.AMPERSAND);
                    if (campaignEx != null) {
                        stringBuffer.append("rid_n=" + campaignEx.getRequestIdNotice());
                    }
                    a(context, stringBuffer.toString(), str);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static void a(Context context, String str, String str2, boolean z) {
        if (context != null) {
            try {
                if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000047&");
                    stringBuffer.append("network_type=" + com.mintegral.msdk.base.utils.c.s(context) + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("unit_id=" + str2 + Constants.RequestParameters.AMPERSAND);
                    if (!TextUtils.isEmpty(com.mintegral.msdk.base.common.a.D)) {
                        stringBuffer.append("sys_id=" + com.mintegral.msdk.base.common.a.D + Constants.RequestParameters.AMPERSAND);
                    }
                    if (!TextUtils.isEmpty(com.mintegral.msdk.base.common.a.E)) {
                        stringBuffer.append("bkup_id=" + com.mintegral.msdk.base.common.a.E + Constants.RequestParameters.AMPERSAND);
                    }
                    if (z) {
                        stringBuffer.append("hb=1&");
                    }
                    stringBuffer.append("reason=" + str);
                    String stringBuffer2 = stringBuffer.toString();
                    if (context != null && !TextUtils.isEmpty(stringBuffer2)) {
                        com.mintegral.msdk.base.common.e.d.a aVar = new com.mintegral.msdk.base.common.e.d.a(context);
                        aVar.c();
                        aVar.b(com.mintegral.msdk.base.common.a.f, c.b(stringBuffer2, context), new b() {
                            public final void b(String str) {
                                g.d(a.a, str);
                            }

                            public final void a(String str) {
                                g.d(a.a, str);
                            }
                        });
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                g.d(a, e.getMessage());
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static void b(Context context, CampaignEx campaignEx, String str) {
        if (context != null && campaignEx != null) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000054&");
                    stringBuffer.append("network_type=" + com.mintegral.msdk.base.utils.c.s(context) + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("unit_id=" + str + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("cid=" + campaignEx.getId() + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("reason=&");
                    stringBuffer.append("result=2&");
                    if (campaignEx != null && campaignEx.getAdType() == 287) {
                        stringBuffer.append("ad_type=3&");
                    } else if (campaignEx == null || campaignEx.getAdType() != 94) {
                        stringBuffer.append("ad_type=1&");
                    } else {
                        stringBuffer.append("ad_type=1&");
                    }
                    stringBuffer.append("creative=" + campaignEx.getendcard_url() + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("devid=" + com.mintegral.msdk.base.utils.c.k() + Constants.RequestParameters.AMPERSAND);
                    if (campaignEx != null) {
                        stringBuffer.append("rid_n=" + campaignEx.getRequestIdNotice());
                    }
                    a(context, stringBuffer.toString(), str);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static void a(Context context, CampaignEx campaignEx, String str, String str2) {
        if (context != null && campaignEx != null) {
            try {
                if ((!TextUtils.isEmpty(str)) && (!TextUtils.isEmpty(str2))) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000054&");
                    stringBuffer.append("network_type=" + com.mintegral.msdk.base.utils.c.s(context) + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("unit_id=" + str + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("cid=" + campaignEx.getId() + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("reason=" + str2 + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("result=1&");
                    stringBuffer.append("creative=" + campaignEx.getendcard_url() + Constants.RequestParameters.AMPERSAND);
                    stringBuffer.append("devid=" + com.mintegral.msdk.base.utils.c.k() + Constants.RequestParameters.AMPERSAND);
                    if (campaignEx != null && campaignEx.getAdType() == 287) {
                        stringBuffer.append("ad_type=3&");
                    } else if (campaignEx == null || campaignEx.getAdType() != 94) {
                        stringBuffer.append("ad_type=1&");
                    } else {
                        stringBuffer.append("ad_type=1&");
                    }
                    if (campaignEx != null) {
                        stringBuffer.append("rid_n=" + campaignEx.getRequestIdNotice());
                    }
                    a(context, stringBuffer.toString(), str);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }
}
