package com.mintegral.msdk.reward.c;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.click.a;
import com.mintegral.msdk.reward.a.d;
import com.mintegral.msdk.videocommon.e.c;
import java.util.List;
import java.util.Random;

/* compiled from: DeductionShowRewardListener */
public final class b extends c {
    private d a;
    private c b;
    private String c;
    private boolean d;
    private Context e;
    private CampaignEx f;
    private boolean g = false;
    private boolean h = false;
    private boolean i = false;
    private boolean j = false;
    private boolean k = false;

    public b(Context context, boolean z, c cVar, CampaignEx campaignEx, d dVar, String str) {
        this.a = dVar;
        this.g = a(cVar, campaignEx);
        this.b = cVar;
        this.c = str;
        this.d = z;
        this.e = context;
        this.f = campaignEx;
    }

    public final void a() {
        super.a();
        if (this.a != null && !this.h) {
            if (!this.g || (this.g && !a(1))) {
                this.a.a();
                if (b(1)) {
                    c();
                }
            }
            this.a.d(this.c);
            this.h = true;
        }
    }

    public final void a(boolean z, com.mintegral.msdk.videocommon.b.d dVar) {
        int i2;
        com.mintegral.msdk.videocommon.b.c K;
        super.a(z, dVar);
        if (this.a != null && !this.j) {
            if (!this.g) {
                this.a.a(z, dVar);
                this.j = true;
                return;
            }
            if (!a(5)) {
                if (this.b == null || (K = this.b.K()) == null) {
                    i2 = 1;
                } else {
                    i2 = K.a();
                }
                if (i2 == 0 && a(1)) {
                    this.a.a("mediaplayer cannot play");
                }
                this.a.a(z, dVar);
            } else {
                this.k = true;
            }
            this.j = true;
        }
    }

    public final void a(String str) {
        super.a(str);
        if (this.a != null && !this.i) {
            if (!this.g || (this.g && !a(1))) {
                this.a.a(str);
            }
            this.i = true;
        }
    }

    public final void a(boolean z, String str) {
        super.a(z, str);
        if (this.a == null) {
            return;
        }
        if ((!this.g || (this.g && !a(4))) && !a(z)) {
            this.a.a(z, str);
        }
    }

    public final void c(String str) {
        super.c(str);
        if (this.a == null) {
            return;
        }
        if (!this.g || (this.g && !a(3))) {
            this.a.c(str);
            if (b(3)) {
                c();
            }
        }
    }

    public final void b(String str) {
        super.b(str);
        if (this.a == null) {
            return;
        }
        if (!this.g || (this.g && !a(2))) {
            this.a.b(str);
            if (b(2)) {
                c();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002d A[Catch:{ Exception -> 0x0069 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(com.mintegral.msdk.videocommon.e.c r7, com.mintegral.msdk.base.entity.CampaignEx r8) {
        /*
            r0 = 0
            com.mintegral.msdk.base.controller.a r1 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Exception -> 0x0069 }
            java.lang.String r1 = r1.j()     // Catch:{ Exception -> 0x0069 }
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x0069 }
            r3 = 0
            if (r2 != 0) goto L_0x0023
            com.mintegral.msdk.d.b.a()     // Catch:{ Exception -> 0x0069 }
            com.mintegral.msdk.d.a r1 = com.mintegral.msdk.d.b.b(r1)     // Catch:{ Exception -> 0x0069 }
            if (r1 == 0) goto L_0x0023
            long r1 = r1.ak()     // Catch:{ Exception -> 0x0069 }
            r5 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 * r5
            goto L_0x0024
        L_0x0023:
            r1 = r3
        L_0x0024:
            com.mintegral.msdk.videocommon.e.b.a()     // Catch:{ Exception -> 0x0069 }
            com.mintegral.msdk.videocommon.e.a r5 = com.mintegral.msdk.videocommon.e.b.b()     // Catch:{ Exception -> 0x0069 }
            if (r5 == 0) goto L_0x0031
            long r3 = r5.e()     // Catch:{ Exception -> 0x0069 }
        L_0x0031:
            r5 = 1
            if (r8 == 0) goto L_0x003e
            boolean r1 = r8.isSpareOffer(r3, r1)     // Catch:{ Exception -> 0x0069 }
            if (r1 == 0) goto L_0x003e
            r8.setSpareOfferFlag(r5)     // Catch:{ Exception -> 0x0069 }
            return r5
        L_0x003e:
            r8.setSpareOfferFlag(r0)     // Catch:{ Exception -> 0x0069 }
            if (r8 == 0) goto L_0x0069
            boolean r8 = r8.isBidCampaign()     // Catch:{ Exception -> 0x0069 }
            if (r8 != 0) goto L_0x0069
            if (r7 == 0) goto L_0x0069
            double r1 = r7.B()     // Catch:{ Exception -> 0x0069 }
            r3 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r8 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r8 != 0) goto L_0x0056
            return r0
        L_0x0056:
            double r7 = r7.B()     // Catch:{ Exception -> 0x0069 }
            java.util.Random r1 = new java.util.Random     // Catch:{ Exception -> 0x0069 }
            r1.<init>()     // Catch:{ Exception -> 0x0069 }
            double r1 = r1.nextDouble()     // Catch:{ Exception -> 0x0069 }
            int r3 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r3 <= 0) goto L_0x0068
            return r5
        L_0x0068:
            return r0
        L_0x0069:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.reward.c.b.a(com.mintegral.msdk.videocommon.e.c, com.mintegral.msdk.base.entity.CampaignEx):boolean");
    }

    private boolean a(int i2) {
        com.mintegral.msdk.videocommon.b.c K;
        if (!(this.b == null || (K = this.b.K()) == null)) {
            if (K.a() == 0) {
                return i2 <= 4;
            }
            List<Integer> b2 = K.b();
            if (b2 != null) {
                return b2.contains(Integer.valueOf(i2));
            }
        }
        return i2 <= 4;
    }

    private boolean b(int i2) {
        return this.b != null ? this.d ? i2 == this.b.b(287) : i2 == this.b.b(94) : this.d ? i2 == 3 : i2 == 2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, int, int]
     candidates:
      com.mintegral.msdk.click.a.a(com.mintegral.msdk.click.a, com.mintegral.msdk.base.entity.CampaignEx, com.mintegral.msdk.click.CommonJumpLoader$JumpLoaderResult, boolean, boolean, java.lang.Boolean):void
      com.mintegral.msdk.click.a.a(android.content.Context, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String, java.lang.String, boolean, boolean):void */
    private void c() {
        String[] r;
        try {
            if (this.f != null && this.f.getNativeVideoTracking() != null && (r = this.f.getNativeVideoTracking().r()) != null) {
                for (String str : r) {
                    if (!TextUtils.isEmpty(str)) {
                        a.a(this.e, this.f, this.c, str, false, true);
                    }
                }
            }
        } catch (Exception e2) {
            if (MIntegralConstans.DEBUG) {
                e2.printStackTrace();
            }
        }
    }

    public final boolean b() {
        return this.k;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private boolean a(boolean z) {
        try {
            if (this.b == null) {
                return false;
            }
            switch (this.b.M()) {
                case 1:
                    return z;
                case 2:
                    if (!z || !d()) {
                        return false;
                    }
                    return true;
                case 3:
                    return d();
            }
            return false;
        } catch (Throwable th) {
            g.c("DeductionShowRewardListener", "", th);
        }
    }

    private boolean d() {
        try {
            if (this.b == null) {
                return false;
            }
            double O = this.b.O();
            if (O != 1.0d && new Random().nextDouble() > O) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            g.c("DeductionShowRewardListener", "", th);
            return false;
        }
    }
}
