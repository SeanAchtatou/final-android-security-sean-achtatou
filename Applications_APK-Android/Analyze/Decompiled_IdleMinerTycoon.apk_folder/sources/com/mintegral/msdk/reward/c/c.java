package com.mintegral.msdk.reward.c;

import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.reward.a.d;

/* compiled from: DefaultShowRewardListener */
public class c implements d {
    public boolean b() {
        return false;
    }

    public void a() {
        g.a("ShowRewardListener", "onAdShow");
    }

    public void a(boolean z, com.mintegral.msdk.videocommon.b.d dVar) {
        g.a("ShowRewardListener", "onAdClose:isCompleteView:" + z + ",reward:" + dVar);
    }

    public void a(String str) {
        g.a("ShowRewardListener", "onShowFail:" + str);
    }

    public void a(boolean z, String str) {
        g.a("ShowRewardListener", "onVideoAdClicked:" + str);
    }

    public void b(String str) {
        g.a("ShowRewardListener", "onVideoComplete: " + str);
    }

    public void c(String str) {
        g.a("ShowRewardListener", "onEndcardShow: " + str);
    }

    public final void d(String str) {
        g.a("ShowRewardListener", "onAutoLoad: " + str);
    }
}
