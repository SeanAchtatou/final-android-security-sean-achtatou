package com.mintegral.msdk.reward.b;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.f;
import com.mintegral.msdk.base.utils.e;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.r;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.videocommon.download.h;
import com.mintegral.msdk.videocommon.download.j;
import com.mintegral.msdk.videocommon.listener.InterVideoOutListener;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/* compiled from: RewardVideoController */
public class a {
    public static String a;
    public static Map<String, d> b = new HashMap();
    private static Map<String, Integer> s = new HashMap();
    /* access modifiers changed from: private */
    public Context c;
    private int d;
    private com.mintegral.msdk.reward.a.c e;
    /* access modifiers changed from: private */
    public com.mintegral.msdk.videocommon.e.c f;
    private String g;
    private com.mintegral.msdk.videocommon.e.a h;
    /* access modifiers changed from: private */
    public InterVideoOutListener i;
    /* access modifiers changed from: private */
    public c j;
    private String k;
    /* access modifiers changed from: private */
    public String l;
    /* access modifiers changed from: private */
    public String m;
    private int n = 0;
    /* access modifiers changed from: private */
    public Handler o = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            switch (message.what) {
                case 8:
                    if (a.this.c(true)) {
                        if (a.this.j != null) {
                            c.a(a.this.j, a.this.l);
                            return;
                        }
                        return;
                    } else if (a.this.j != null) {
                        c.b(a.this.j, "load timeout");
                        return;
                    } else {
                        return;
                    }
                case 9:
                    if (a.this.i != null) {
                        Object obj = message.obj;
                        String str = "";
                        if (obj instanceof String) {
                            str = obj.toString();
                        }
                        a.this.i.onVideoLoadSuccess(str);
                        return;
                    }
                    return;
                case 16:
                    if (a.this.i != null) {
                        Object obj2 = message.obj;
                        String str2 = "";
                        if (obj2 instanceof String) {
                            str2 = obj2.toString();
                        }
                        com.mintegral.msdk.reward.d.a.a(a.this.c, str2, a.this.l, a.this.r);
                        a.this.i.onVideoLoadFail(str2);
                        return;
                    }
                    return;
                case 17:
                    if (a.this.i != null) {
                        Object obj3 = message.obj;
                        String str3 = "";
                        if (obj3 instanceof String) {
                            str3 = obj3.toString();
                        }
                        a.this.i.onLoadSuccess(str3);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    private int p = 2;
    /* access modifiers changed from: private */
    public boolean q = false;
    /* access modifiers changed from: private */
    public boolean r = false;
    private Queue<Integer> t;
    private String u;
    private i v = null;

    public final void a(boolean z) {
        this.q = z;
    }

    public final void a() {
        this.r = true;
    }

    public final void a(int i2) {
        this.p = i2;
    }

    public final void a(String str, String str2, String str3, String str4) {
        if (!TextUtils.isEmpty(str)) {
            Context context = this.c;
            r.a(context, "Mintegral_ConfirmTitle" + this.l, str.trim());
        }
        if (!TextUtils.isEmpty(str2)) {
            Context context2 = this.c;
            r.a(context2, "Mintegral_ConfirmContent" + this.l, str2.trim());
        }
        if (!TextUtils.isEmpty(str4)) {
            Context context3 = this.c;
            r.a(context3, "Mintegral_CancelText" + this.l, str4.trim());
        }
        if (!TextUtils.isEmpty(str3)) {
            Context context4 = this.c;
            r.a(context4, "Mintegral_ConfirmText" + this.l, str3.trim());
        }
    }

    public static void a(String str, int i2) {
        try {
            if (s != null && s.b(str)) {
                s.put(str, Integer.valueOf(i2));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static int a(String str) {
        Integer num;
        try {
            if (!s.b(str) || s == null || !s.containsKey(str) || (num = s.get(str)) == null) {
                return 0;
            }
            return num.intValue();
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static void insertExcludeId(String str, CampaignEx campaignEx) {
        if (!TextUtils.isEmpty(str) && campaignEx != null && com.mintegral.msdk.base.controller.a.d().h() != null) {
            l a2 = l.a(i.a(com.mintegral.msdk.base.controller.a.d().h()));
            f fVar = new f();
            fVar.a(System.currentTimeMillis());
            fVar.b(str);
            fVar.a(campaignEx.getId());
            a2.a(fVar);
        }
    }

    /* compiled from: RewardVideoController */
    private static final class d implements com.mintegral.msdk.reward.a.d {
        private a a;
        private int b;

        public final boolean b() {
            return false;
        }

        /* synthetic */ d(a aVar, byte b2) {
            this(aVar);
        }

        private d(a aVar) {
            this.a = aVar;
            this.b = 1;
        }

        public final void a() {
            try {
                if (this.a != null) {
                    this.a.b(this.b);
                    if (this.a.i != null) {
                        this.a.i.onAdShow();
                    }
                }
            } catch (Throwable th) {
                g.c("RewardVideoController", th.getMessage(), th);
            }
        }

        public final void a(boolean z, com.mintegral.msdk.videocommon.b.d dVar) {
            try {
                if (this.a != null && this.a.i != null) {
                    if (dVar == null) {
                        dVar = com.mintegral.msdk.videocommon.b.d.a(this.a.m);
                    }
                    this.a.i.onAdClose(z, dVar.a(), (float) dVar.b());
                    g.a("RewardVideoController", "onAdClose start release");
                    this.a = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public final void a(String str) {
            if (this.a != null) {
                if (this.a.i != null) {
                    this.a.i.onShowFail(str);
                }
                if (!this.a.r && !this.a.q && this.a.f.c(4)) {
                    this.a.b(false);
                }
            }
        }

        public final void a(boolean z, String str) {
            if (this.a != null && this.a.i != null) {
                this.a.i.onVideoAdClicked(z, str);
            }
        }

        public final void b(String str) {
            g.a("RewardVideoController", "onVideoComplete start");
            if (this.a != null && this.a.i != null) {
                this.a.i.onVideoComplete(str);
                g.a("RewardVideoController", "onEndcardShow callback");
            }
        }

        public final void c(String str) {
            g.a("RewardVideoController", "onEndcardShow start");
            if (this.a != null && this.a.i != null) {
                this.a.i.onEndcardShow(str);
                g.a("RewardVideoController", "onEndcardShow callback");
            }
        }

        public final void d(String str) {
            if (this.a != null && !this.a.r && !this.a.q && this.a.f.c(2)) {
                this.a.b(false);
            }
        }
    }

    public final void a(InterVideoOutListener interVideoOutListener) {
        this.i = interVideoOutListener;
        this.j = new c(interVideoOutListener, this.o, (byte) 0);
    }

    public final void b(String str) {
        List<CampaignEx> a2;
        try {
            this.c = com.mintegral.msdk.base.controller.a.d().h();
            this.l = str;
            com.mintegral.msdk.videocommon.e.b.a();
            this.h = com.mintegral.msdk.videocommon.e.b.b();
            com.mintegral.msdk.reward.d.a.a(this.c, this.l);
            e.b();
            j.a().b();
            h.a().b();
            if (!TextUtils.isEmpty(this.l) && (a2 = com.mintegral.msdk.videocommon.a.a.a().a(this.l, 1)) != null && a2.size() > 0) {
                com.mintegral.msdk.videocommon.download.c.getInstance().createUnitCache(this.c, this.l, a2, 3, null);
            }
            if (this.v == null) {
                this.v = i.a(com.mintegral.msdk.base.controller.a.d().h());
            }
        } catch (Throwable th) {
            g.c("RewardVideoController", th.getMessage(), th);
        }
    }

    public static void b() {
        e.a();
    }

    public final void b(boolean z) {
        a(z, "");
    }

    public final void a(boolean z, String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                if (this.q) {
                    com.mintegral.msdk.f.b.getInstance().addInterstitialList(this.l);
                } else {
                    com.mintegral.msdk.f.b.getInstance().addRewardList(this.l);
                }
            }
            if (!this.r || !TextUtils.isEmpty(str)) {
                if (com.mintegral.msdk.system.a.a != null) {
                    com.mintegral.msdk.videocommon.e.b.a();
                    this.f = com.mintegral.msdk.videocommon.e.b.a(com.mintegral.msdk.base.controller.a.d().j(), this.l);
                    if (this.f == null) {
                        this.u = com.mintegral.msdk.base.controller.a.d().j();
                        com.mintegral.msdk.videocommon.e.b.a();
                        com.mintegral.msdk.videocommon.e.b.a(this.u, com.mintegral.msdk.base.controller.a.d().k(), this.l, new com.mintegral.msdk.videocommon.c.c() {
                        });
                        com.mintegral.msdk.videocommon.e.b.a();
                        this.f = com.mintegral.msdk.videocommon.e.b.a(com.mintegral.msdk.base.controller.a.d().j(), this.l, this.q);
                    }
                    this.t = this.f.Q();
                    if (c()) {
                        try {
                            List<com.mintegral.msdk.videocommon.b.b> J = this.f.J();
                            if (J != null && J.size() > 0) {
                                for (int i2 = 0; i2 < J.size(); i2++) {
                                    Context context = this.c;
                                    r.a(context, this.g + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + J.get(i2).a(), 0);
                                }
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                    if (this.j != null) {
                        if (this.j.b != 1 || this.i == null) {
                            if (z) {
                                this.j.b = 1;
                            }
                        } else if (z) {
                            this.i.onVideoLoadFail("current unit is loading");
                            com.mintegral.msdk.reward.d.a.a(this.c, "current unit is loading", this.l, this.r);
                            this.j.b = 1;
                            return;
                        } else {
                            return;
                        }
                    }
                    if (c(true) && this.j != null) {
                        c.c(this.j, this.l);
                        c.a(this.j, this.l);
                        if (!this.r) {
                            z = false;
                        } else {
                            return;
                        }
                    }
                    if (!z || !d()) {
                        a(this.t, z, str);
                        return;
                    }
                    if (this.j != null) {
                        c.b(this.j, "Play more than limit");
                    }
                    if (!this.r && this.f.c(3)) {
                        a(this.t, false, "");
                    }
                } else if (this.i != null && z) {
                    com.mintegral.msdk.reward.d.a.a(this.c, "init error", this.l, this.r);
                    this.i.onVideoLoadFail("init error");
                }
            } else if (this.o != null) {
                Message obtain = Message.obtain();
                obtain.obj = "bidToken is empty";
                obtain.what = 16;
                this.o.sendMessage(obtain);
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    /* compiled from: RewardVideoController */
    private static final class c {
        private WeakReference<InterVideoOutListener> a;
        /* access modifiers changed from: private */
        public int b;
        private Handler c;

        /* synthetic */ c(InterVideoOutListener interVideoOutListener, Handler handler, byte b2) {
            this(interVideoOutListener, handler);
        }

        private c(InterVideoOutListener interVideoOutListener, Handler handler) {
            this.b = 0;
            this.a = new WeakReference<>(interVideoOutListener);
            this.c = handler;
        }

        static /* synthetic */ void a(c cVar, String str) {
            if (cVar.a != null && cVar.a.get() != null && cVar.b == 1) {
                cVar.b = 2;
                if (cVar.c != null) {
                    Message obtain = Message.obtain();
                    obtain.obj = str;
                    obtain.what = 9;
                    cVar.c.sendMessage(obtain);
                }
            }
        }

        static /* synthetic */ void b(c cVar, String str) {
            if (cVar.a != null && cVar.a.get() != null && cVar.b == 1) {
                cVar.b = 2;
                if (cVar.c != null) {
                    Message obtain = Message.obtain();
                    obtain.obj = str;
                    obtain.what = 16;
                    cVar.c.sendMessage(obtain);
                }
            }
        }

        static /* synthetic */ void c(c cVar, String str) {
            if (cVar.a != null && cVar.a.get() != null && cVar.b == 1 && cVar.c != null) {
                Message obtain = Message.obtain();
                obtain.obj = str;
                obtain.what = 17;
                cVar.c.sendMessage(obtain);
            }
        }
    }

    private boolean c() {
        int i2;
        try {
            List<com.mintegral.msdk.videocommon.b.b> J = this.f.J();
            Map<String, Integer> l2 = this.h.l();
            if (J == null || J.size() <= 0) {
                return true;
            }
            for (int i3 = 0; i3 < J.size(); i3++) {
                com.mintegral.msdk.videocommon.b.b bVar = J.get(i3);
                StringBuilder sb = new StringBuilder();
                sb.append(bVar.a());
                if (l2.containsKey(sb.toString())) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(bVar.a());
                    i2 = l2.get(sb2.toString()).intValue();
                } else {
                    i2 = 0;
                }
                Context context = this.c;
                Object b2 = r.b(context, this.g + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + bVar.a(), 0);
                if ((b2 != null ? ((Integer) b2).intValue() : 0) < i2) {
                    return false;
                }
            }
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            g.d("RewardVideoController", e2.getMessage());
            return true;
        }
    }

    private void a(Queue<Integer> queue, boolean z, String str) {
        int i2 = 8;
        if (queue != null) {
            try {
                if (queue.size() > 0) {
                    i2 = queue.poll().intValue();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                if (z) {
                    c("can't show because unknow error");
                }
                g.d("RewardVideoController", e2.getMessage());
                return;
            }
        }
        try {
            if (this.e == null || !this.l.equals(this.e.a())) {
                this.e = new com.mintegral.msdk.reward.a.c(this.c, this.l);
                this.e.a(this.q);
                this.e.b(this.r);
            }
            this.e.a(this.p);
            this.e.b();
            C0066a aVar = new C0066a(this.e, z);
            b bVar = new b(this.e, z);
            bVar.a(aVar);
            this.e.a(bVar);
            this.o.postDelayed(aVar, (long) (i2 * 1000));
            this.e.a(i2, z, str);
        } catch (Exception e3) {
            if (z) {
                c("load mv api error:" + e3.getMessage());
            }
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        if (this.j != null) {
            c.b(this.j, str);
        }
    }

    public final void b(int i2) {
        try {
            com.mintegral.msdk.base.b.j a2 = com.mintegral.msdk.base.b.j.a(this.v);
            if (a2 != null) {
                a2.a(this.l);
            }
        } catch (Throwable unused) {
            g.d("RewardVideoController", "can't find DailyPlayCapDao");
        }
        if (i2 == 1) {
            Context context = this.c;
            r.a(context, this.g + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + i2, Integer.valueOf(this.d + 1));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.reward.b.a.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.mintegral.msdk.reward.b.a.a(com.mintegral.msdk.reward.b.a, java.lang.String):void
      com.mintegral.msdk.reward.b.a.a(java.lang.String, int):void
      com.mintegral.msdk.reward.b.a.a(boolean, boolean):void
      com.mintegral.msdk.reward.b.a.a(java.lang.String, java.lang.String):void
      com.mintegral.msdk.reward.b.a.a(boolean, java.lang.String):void */
    private void d(String str) {
        try {
            if (this.f == null) {
                g.b("RewardVideoController", "unitSetting==null");
                if (this.i != null) {
                    this.i.onShowFail("can't show because load is failed");
                }
                if (!this.q && !this.r && this.f.c(4)) {
                    a(false, "");
                    return;
                }
                return;
            }
            com.mintegral.msdk.reward.a.c cVar = new com.mintegral.msdk.reward.a.c(this.c, this.l);
            cVar.a(this.q);
            cVar.b(this.r);
            Context context = this.c;
            if (r.b(context, this.g + "_1", 0) != null) {
                Context context2 = this.c;
                this.d = ((Integer) r.b(context2, this.g + "_1", 0)).intValue();
            }
            g.d("RewardVideoController", "controller 819");
            if (cVar.e()) {
                g.b("RewardVideoController", "invoke adapter show");
                d dVar = new d(this, (byte) 0);
                b.put(this.l, dVar);
                cVar.a(dVar, str, this.k, this.p);
            } else if (this.d != 0) {
                Context context3 = this.c;
                r.a(context3, this.g + "_1", 0);
                d(str);
            } else {
                if (this.i != null) {
                    this.i.onShowFail("can't show because load is failed");
                }
                if (!this.q && !this.r && this.f.c(4)) {
                    b(false);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.reward.b.a.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.mintegral.msdk.reward.b.a.a(com.mintegral.msdk.reward.b.a, java.lang.String):void
      com.mintegral.msdk.reward.b.a.a(java.lang.String, int):void
      com.mintegral.msdk.reward.b.a.a(boolean, boolean):void
      com.mintegral.msdk.reward.b.a.a(java.lang.String, java.lang.String):void
      com.mintegral.msdk.reward.b.a.a(boolean, java.lang.String):void */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean c(boolean r5) {
        /*
            r4 = this;
            r0 = 0
            boolean r1 = r4.d()     // Catch:{ Throwable -> 0x0047 }
            if (r1 != 0) goto L_0x004f
            com.mintegral.msdk.reward.a.c r1 = new com.mintegral.msdk.reward.a.c     // Catch:{ Throwable -> 0x0047 }
            android.content.Context r2 = r4.c     // Catch:{ Throwable -> 0x0047 }
            java.lang.String r3 = r4.l     // Catch:{ Throwable -> 0x0047 }
            r1.<init>(r2, r3)     // Catch:{ Throwable -> 0x0047 }
            boolean r2 = r4.q     // Catch:{ Throwable -> 0x0047 }
            r1.a(r2)     // Catch:{ Throwable -> 0x0047 }
            boolean r2 = r4.r     // Catch:{ Throwable -> 0x0047 }
            r1.b(r2)     // Catch:{ Throwable -> 0x0047 }
            java.lang.String r2 = "RewardVideoController"
            java.lang.String r3 = "controller isReady"
            com.mintegral.msdk.base.utils.g.d(r2, r3)     // Catch:{ Throwable -> 0x0047 }
            boolean r2 = r1.d()     // Catch:{ Throwable -> 0x0047 }
            if (r2 == 0) goto L_0x0045
            boolean r1 = r1.c()     // Catch:{ Throwable -> 0x0042 }
            if (r1 == 0) goto L_0x0045
            if (r5 != 0) goto L_0x0045
            boolean r5 = r4.r     // Catch:{ Throwable -> 0x0042 }
            if (r5 != 0) goto L_0x0045
            com.mintegral.msdk.videocommon.e.c r5 = r4.f     // Catch:{ Throwable -> 0x0042 }
            r1 = 1
            boolean r5 = r5.c(r1)     // Catch:{ Throwable -> 0x0042 }
            if (r5 == 0) goto L_0x0045
            java.lang.String r5 = ""
            r4.a(r0, r5)     // Catch:{ Throwable -> 0x0042 }
            goto L_0x0045
        L_0x0042:
            r5 = move-exception
            r0 = r2
            goto L_0x0048
        L_0x0045:
            r0 = r2
            goto L_0x004f
        L_0x0047:
            r5 = move-exception
        L_0x0048:
            boolean r1 = com.mintegral.msdk.MIntegralConstans.DEBUG
            if (r1 == 0) goto L_0x004f
            r5.printStackTrace()
        L_0x004f:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.reward.b.a.c(boolean):boolean");
    }

    public final void a(String str, String str2) {
        try {
            this.m = str;
            this.k = str2;
            if (this.j == null || this.j.b != 1) {
                if (this.c == null) {
                    if (this.i != null) {
                        this.i.onShowFail("context is null");
                    }
                } else if (!this.q || k.b(this.c)) {
                    if (d()) {
                        if (this.i != null) {
                            this.i.onShowFail("Play more than limit");
                        }
                        if (!this.r && this.f.c(4)) {
                            a(this.t, false, "");
                            return;
                        }
                        return;
                    }
                    if (TextUtils.isEmpty(this.k)) {
                        this.k = com.mintegral.msdk.base.utils.c.k();
                    }
                    String format = new SimpleDateFormat("dd").format(new Date());
                    String str3 = (String) r.b(this.c, "reward_date", AppEventsConstants.EVENT_PARAM_VALUE_NO);
                    if (!TextUtils.isEmpty(str3) && !TextUtils.isEmpty(format) && !str3.equals(format)) {
                        r.a(this.c, "reward_date", format);
                        Context context = this.c;
                        r.a(context, this.g + "_1", 0);
                    }
                    d(str);
                } else if (this.i != null) {
                    this.i.onShowFail("network exception");
                }
            } else if (this.i != null) {
                this.i.onShowFail("campaing is loading");
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private boolean d() {
        try {
            com.mintegral.msdk.base.b.j a2 = com.mintegral.msdk.base.b.j.a(this.v);
            if (this.f == null) {
                com.mintegral.msdk.videocommon.e.b.a();
                this.f = com.mintegral.msdk.videocommon.e.b.a(com.mintegral.msdk.base.controller.a.d().j(), this.l);
            }
            int c2 = this.f.c();
            if (a2 == null || !a2.a(this.l, c2)) {
                return false;
            }
            return true;
        } catch (Throwable unused) {
            g.d("RewardVideoController", "cap check error");
            return false;
        }
    }

    public static void a(boolean z, boolean z2) {
        try {
            if (b != null) {
                b.clear();
            }
            com.mintegral.msdk.base.common.net.a.b();
            if (z) {
                if (z2) {
                    com.mintegral.msdk.videocommon.a.a(287);
                } else {
                    com.mintegral.msdk.videocommon.a.b(287);
                }
            } else if (z2) {
                com.mintegral.msdk.videocommon.a.a(94);
            } else {
                com.mintegral.msdk.videocommon.a.b(94);
            }
        } catch (Throwable unused) {
            g.d("RewardVideoController", "destory failed");
        }
    }

    /* renamed from: com.mintegral.msdk.reward.b.a$a  reason: collision with other inner class name */
    /* compiled from: RewardVideoController */
    public class C0066a implements Runnable {
        private com.mintegral.msdk.reward.a.a b;
        private int c = 1;
        private boolean d;

        public C0066a(com.mintegral.msdk.reward.a.a aVar, boolean z) {
            this.b = aVar;
            this.d = z;
        }

        public final void run() {
            g.d("RewardVideoController", "adSource=" + this.c + " CommonCancelTimeTask mIsDevCall：" + this.d);
            if (this.d) {
                a.this.c("v3 is timeout");
            }
        }
    }

    /* compiled from: RewardVideoController */
    public class b implements com.mintegral.msdk.reward.a.b {
        private com.mintegral.msdk.reward.a.a b;
        private boolean c;
        private Runnable d;

        public b(com.mintegral.msdk.reward.a.a aVar, boolean z) {
            this.b = aVar;
            this.c = z;
        }

        public final void a(Runnable runnable) {
            this.d = runnable;
        }

        public final void b() {
            if (this.d != null) {
                g.d("RewardVideoController", "onCampaignLoadSuccess remove task ");
                a.this.o.removeCallbacks(this.d);
            }
            if (a.this.j != null && this.c) {
                c.c(a.this.j, a.this.l);
            }
        }

        public final void a() {
            if (this.d != null) {
                g.d("RewardVideoController", "onVideoLoadSuccess remove task ");
                a.this.o.removeCallbacks(this.d);
            }
            if (a.this.j != null && this.c) {
                c.a(a.this.j, a.this.l);
            }
        }

        public final void a(String str) {
            if (this.d != null) {
                g.d("RewardVideoController", "onVideoLoadFail remove task");
                a.this.o.removeCallbacks(this.d);
            }
            if (this.b != null) {
                this.b.a(null);
                this.b = null;
            }
            if (a.this.j != null && this.c) {
                c.b(a.this.j, str);
            }
        }
    }
}
