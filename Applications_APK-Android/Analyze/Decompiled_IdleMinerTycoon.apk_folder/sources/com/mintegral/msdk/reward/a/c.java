package com.mintegral.msdk.reward.a;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.webkit.WebView;
import com.google.android.gms.drive.DriveFile;
import com.helpshift.common.domain.network.NetworkConstants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.l;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.mintegral.msdk.base.entity.q;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.reward.player.MTGRewardVideoActivity;
import com.mintegral.msdk.videocommon.a;
import com.mintegral.msdk.videocommon.download.g;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;

/* compiled from: RewardMVVideoAdapter */
public final class c implements a {
    public List<CampaignEx> a = new ArrayList();
    /* access modifiers changed from: private */
    public Context b;
    /* access modifiers changed from: private */
    public String c;
    private int d;
    private int e;
    private int f;
    private boolean g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String i;
    private d j;
    /* access modifiers changed from: private */
    public b k;
    private com.mintegral.msdk.videocommon.e.c l;
    /* access modifiers changed from: private */
    public i m;
    /* access modifiers changed from: private */
    public boolean n = false;
    /* access modifiers changed from: private */
    public boolean o = false;
    private int p = 2;
    /* access modifiers changed from: private */
    public boolean q;
    private boolean r;
    /* access modifiers changed from: private */
    public Handler s = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            try {
                int i = message.what;
                if (i != 16) {
                    String str = null;
                    switch (i) {
                        case 1:
                            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "handler id获取成功 开始load mTtcIds:" + c.this.h + "  mExcludes:" + c.this.i);
                            if (message.obj != null) {
                                str = message.obj.toString();
                            }
                            c.this.a(str);
                            return;
                        case 2:
                            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "handler id获取超时  开始load mTtcIds:" + c.this.h + "  mExcludes:" + c.this.i);
                            if (message.obj != null) {
                                str = message.obj.toString();
                            }
                            c.this.a(str);
                            return;
                        case 3:
                            if (c.this.k != null) {
                                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "handler 数据load成功");
                                c.this.k.b();
                            }
                            sendEmptyMessageDelayed(5, 60000);
                            return;
                        case 4:
                            if (c.this.k != null) {
                                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "handler 数据load失败");
                                try {
                                    if (message.obj == null) {
                                        c.this.k.a("data load failed");
                                        return;
                                    }
                                    String str2 = (String) message.obj;
                                    if (TextUtils.isEmpty(str2)) {
                                        c.this.k.a("data load failed, errorMsg null");
                                        return;
                                    }
                                    b c = c.this.k;
                                    c.a("data load failed, errorMsg is " + str2);
                                    return;
                                } catch (Exception e) {
                                    b c2 = c.this.k;
                                    c2.a("data load failed, exception is " + e.getMessage());
                                    return;
                                }
                            } else {
                                return;
                            }
                        case 5:
                            if (c.this.k != null) {
                                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "资源请求超时");
                                com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "adapter 177");
                                if (c.this.d()) {
                                    c.this.k.a();
                                    return;
                                } else {
                                    c.this.k.a("resource load timeout");
                                    return;
                                }
                            } else {
                                return;
                            }
                        case 6:
                            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "handler res数据load成功0");
                            if (c.this.k != null) {
                                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "handler res数据load成功1");
                                c.this.k.a();
                                CampaignEx campaignEx = (CampaignEx) message.obj;
                                if (campaignEx != null && !TextUtils.isEmpty(c.this.c)) {
                                    com.mintegral.msdk.reward.d.a.a(c.this.b, campaignEx, c.this.c);
                                    com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "2dwOnLoadSuccess");
                                    return;
                                }
                                return;
                            }
                            return;
                        default:
                            switch (i) {
                                case 8:
                                    if (c.this.k != null) {
                                        com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "adapter 202");
                                        if (c.this.d()) {
                                            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "handler endcard源码下载成功 isready为true");
                                            c.this.k.a();
                                            CampaignEx campaignEx2 = (CampaignEx) message.obj;
                                            if (campaignEx2 != null && !TextUtils.isEmpty(c.this.c)) {
                                                com.mintegral.msdk.reward.d.a.a(c.this.b, campaignEx2, c.this.c);
                                                com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "3dwOnLoadSuccess");
                                                return;
                                            }
                                            return;
                                        }
                                        return;
                                    }
                                    return;
                                case 9:
                                    break;
                                default:
                                    return;
                            }
                    }
                }
                try {
                    int i2 = message.what;
                    Object[] objArr = (Object[]) message.obj;
                    CampaignEx campaignEx3 = (CampaignEx) objArr[0];
                    String str3 = (String) objArr[1];
                    String str4 = (String) objArr[2];
                    com.mintegral.msdk.videocommon.e.c cVar = (com.mintegral.msdk.videocommon.e.c) objArr[3];
                    if (campaignEx3 != null && !TextUtils.isEmpty(str3)) {
                        c.a(c.this, campaignEx3, str3, i2, str4, cVar);
                    }
                } catch (Exception unused) {
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    };

    public final void a(boolean z) {
        this.q = z;
    }

    public final void b(boolean z) {
        this.r = z;
    }

    public final void a(int i2) {
        this.p = i2;
    }

    public final String a() {
        return this.c;
    }

    /* compiled from: RewardMVVideoAdapter */
    private static class f implements Runnable {
        private CampaignEx a;
        private String b;
        private String c;
        private com.mintegral.msdk.videocommon.e.c d;
        private int e;
        private int f;
        private c g;

        public f(CampaignEx campaignEx, String str, String str2, com.mintegral.msdk.videocommon.e.c cVar, int i, int i2, c cVar2) {
            this.a = campaignEx;
            this.b = str;
            this.c = str2;
            this.d = cVar;
            this.e = i;
            this.f = i2;
            this.g = cVar2;
        }

        public final void run() {
            try {
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "retry load template url = " + this.b);
                a.C0069a aVar = new a.C0069a();
                WindVaneWebView windVaneWebView = new WindVaneWebView(com.mintegral.msdk.base.controller.a.d().h());
                aVar.a(windVaneWebView);
                com.mintegral.msdk.video.js.a.h hVar = new com.mintegral.msdk.video.js.a.h(null, this.a);
                hVar.a(this.e);
                hVar.a(this.c);
                hVar.a(this.d);
                windVaneWebView.setWebViewListener(new j(aVar, this.a, this.g, null, null));
                windVaneWebView.setObject(hVar);
                int i = this.f;
                if (i == 9) {
                    windVaneWebView.loadDataWithBaseURL(this.a.getRewardTemplateMode().d(), this.b, "text/html", "utf-8", null);
                } else if (i == 16) {
                    windVaneWebView.loadUrl(this.b);
                }
            } catch (Exception unused) {
            }
        }
    }

    /* compiled from: RewardMVVideoAdapter */
    private static class j extends com.mintegral.msdk.mtgjscommon.b.a {
        private c a;
        private a.C0069a b;
        private CampaignEx c;
        private boolean d;
        private boolean e;
        private f f;
        private Handler g;

        public j(a.C0069a aVar, CampaignEx campaignEx, c cVar, f fVar, Handler handler) {
            this.b = aVar;
            if (cVar != null) {
                this.a = cVar;
            }
            this.c = campaignEx;
            this.f = fVar;
            this.g = handler;
        }

        public final void a(int i) {
            if (!this.e) {
                if (!(this.f == null || this.g == null)) {
                    this.g.removeCallbacks(this.f);
                }
                if (this.b != null) {
                    this.b.c();
                }
                com.mintegral.msdk.base.utils.g.a("WindVaneWebView", "templete preload readyState state = " + i);
                if (this.a == null || !this.a.q) {
                    if (this.c.isBidCampaign()) {
                        com.mintegral.msdk.base.utils.g.a("WindVaneWebView", "put templeteCache in bidRVCache ");
                        com.mintegral.msdk.videocommon.a.a(94, this.c.getRequestIdNotice(), this.b);
                    } else {
                        com.mintegral.msdk.base.utils.g.a("WindVaneWebView", "put templeteCache in rVCache ");
                        com.mintegral.msdk.videocommon.a.b(94, this.c.getRequestIdNotice(), this.b);
                    }
                } else if (this.c.isBidCampaign()) {
                    com.mintegral.msdk.base.utils.g.a("WindVaneWebView", "put templeteCache in bidIVCache ");
                    com.mintegral.msdk.videocommon.a.a(287, this.c.getRequestIdNotice(), this.b);
                } else {
                    com.mintegral.msdk.base.utils.g.a("WindVaneWebView", "put templeteCache in iVCache ");
                    com.mintegral.msdk.videocommon.a.b(287, this.c.getRequestIdNotice(), this.b);
                }
                try {
                    com.mintegral.msdk.base.utils.g.d("WindVaneWebView", "TempalteWindVaneWebviewClient tempalte load SUCCESS");
                    if (this.a != null) {
                        synchronized (this.a) {
                            com.mintegral.msdk.base.utils.g.d("WindVaneWebView", "adapter 341");
                            if (this.a != null && this.a.d()) {
                                com.mintegral.msdk.base.utils.g.d("WindVaneWebView", "TempalteWindVaneWebviewClient tempalte load  callback success");
                                if (this.a.s != null) {
                                    Message obtain = Message.obtain();
                                    obtain.what = 6;
                                    obtain.obj = this.c;
                                    this.a.s.sendMessage(obtain);
                                    this.a.s.removeMessages(5);
                                    this.a = null;
                                }
                            }
                        }
                    } else {
                        com.mintegral.msdk.base.utils.g.d("WindVaneWebView", "TempalteWindVaneWebviewClient tempalte load SUCCESS  mRewardMVVideoAdapter is null");
                    }
                } catch (Throwable th) {
                    com.mintegral.msdk.base.utils.g.c("WindVaneWebView", th.getMessage(), th);
                }
                this.e = true;
            }
        }

        public final void a(WebView webView, String str) {
            super.a(webView, str);
            if (!this.d) {
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(webView, "onJSBridgeConnected", "");
                this.d = true;
            }
        }

        public final void a(WebView webView, int i, String str, String str2) {
            super.a(webView, i, str, str2);
            try {
                com.mintegral.msdk.base.utils.g.d("WindVaneWebView", "TempalteWindVaneWebviewClient tempalte load failed");
                if (this.a != null) {
                    synchronized (this.a) {
                        com.mintegral.msdk.base.utils.g.d("WindVaneWebView", "TempalteWindVaneWebviewClient tempalte load callback failed");
                        c.a(this.a, str, str2);
                        this.a = null;
                    }
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("WindVaneWebView", th.getMessage(), th);
            }
        }
    }

    public c(Context context, String str) {
        try {
            this.b = context.getApplicationContext();
            this.c = str;
            b();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void b() {
        try {
            String j2 = com.mintegral.msdk.base.controller.a.d().j();
            com.mintegral.msdk.videocommon.e.b.a();
            this.l = com.mintegral.msdk.videocommon.e.b.a(j2, this.c);
            if (this.l == null) {
                com.mintegral.msdk.videocommon.e.b.a();
                this.l = com.mintegral.msdk.videocommon.e.b.a(j2, this.c, this.q);
            }
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
        }
    }

    public final boolean c() {
        com.mintegral.msdk.videocommon.e.b.a();
        return com.mintegral.msdk.videocommon.a.a.a().a(this.c, com.mintegral.msdk.videocommon.e.b.b().e());
    }

    public final boolean d() {
        try {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "进来 isReady");
            com.mintegral.msdk.videocommon.download.c instance = com.mintegral.msdk.videocommon.download.c.getInstance();
            if (s.b(this.c) && instance != null) {
                j();
                List<CampaignEx> a2 = com.mintegral.msdk.videocommon.a.a.a().a(this.c, 1, this.r);
                StringBuilder sb = new StringBuilder("camapignList.size() = ");
                sb.append(a2 != null ? a2.size() : 0);
                sb.append(" isBidCampaign = ");
                sb.append(this.r);
                com.mintegral.msdk.base.utils.g.a("RewardMVVideoAdapter", sb.toString());
                if (a2 != null && a2.size() > 0) {
                    instance.createUnitCache(this.b, this.c, a2, 3, new a(this, a2.get(0)));
                    return instance.a(this.q ? 287 : 94, this.c, this.r);
                }
            }
        } catch (Exception e2) {
            com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", e2.getMessage() + "isReady 出错");
        }
        return false;
    }

    public final boolean e() {
        boolean a2;
        boolean z = false;
        try {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "进来 isSpareOfferReady");
            com.mintegral.msdk.videocommon.download.c instance = com.mintegral.msdk.videocommon.download.c.getInstance();
            if (s.b(this.c) && instance != null) {
                j();
                List<CampaignEx> a3 = com.mintegral.msdk.videocommon.a.a.a().a(this.c, this.r);
                if (a3 != null && a3.size() > 0) {
                    com.mintegral.msdk.videocommon.download.c cVar = instance;
                    cVar.createUnitCache(this.b, this.c, a3, 3, new a(this, a3.get(0)));
                    if (this.q) {
                        a2 = instance.a(287, this.c, this.r);
                    } else {
                        a2 = instance.a(94, this.c, this.r);
                    }
                    z = a2;
                    if (z) {
                        a(a3, this.c, this.l);
                    }
                }
            }
        } catch (Exception e2) {
            com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", e2.getMessage() + "isSpareOfferReady 出错");
        }
        return z;
    }

    private boolean g() {
        boolean z = false;
        try {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "进来 isReadyDoSuccessful");
            com.mintegral.msdk.videocommon.download.c instance = com.mintegral.msdk.videocommon.download.c.getInstance();
            if (s.b(this.c) && instance != null) {
                j();
                List<CampaignEx> a2 = com.mintegral.msdk.videocommon.a.a.a().a(this.c, 1, this.r);
                if (a2 != null && a2.size() > 0) {
                    instance.createUnitCache(this.b, this.c, a2, 3, new a(this, a2.get(0)));
                    boolean a3 = instance.a(this.q ? 287 : 94, this.c, this.r);
                    if (a3) {
                        try {
                            com.mintegral.msdk.reward.d.a.a(this.b, a2.get(0), this.c);
                        } catch (Exception e2) {
                            boolean z2 = a3;
                            e = e2;
                            z = z2;
                        }
                    }
                    z = a3;
                }
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "isReadyDoSuccessful 出错");
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "结果 isReadyDoSuccessful：" + z);
            return z;
        }
        com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "结果 isReadyDoSuccessful：" + z);
        return z;
    }

    public final void a(d dVar, String str, String str2, int i2) {
        try {
            this.j = dVar;
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "show 进来");
            if (this.b != null) {
                if (!s.a(this.c)) {
                    com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "show isReady true 打开播放器页面");
                    Intent intent = new Intent(this.b, MTGRewardVideoActivity.class);
                    intent.addFlags(DriveFile.MODE_READ_ONLY);
                    intent.putExtra("unitId", this.c);
                    intent.putExtra(MTGRewardVideoActivity.INTENT_REWARD, str);
                    intent.putExtra("mute", i2);
                    intent.putExtra(MTGRewardVideoActivity.INTENT_ISIV, this.q);
                    intent.putExtra(MTGRewardVideoActivity.INTENT_ISBID, this.r);
                    if (!TextUtils.isEmpty(str2)) {
                        intent.putExtra("userId", str2);
                    }
                    this.b.startActivity(intent);
                    return;
                }
            }
            if (this.j != null) {
                this.j.a("context or unitid is null");
            }
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "show context munitid null");
        } catch (Exception e2) {
            e2.printStackTrace();
            if (this.j != null) {
                d dVar2 = this.j;
                dVar2.a("show failed, exception is " + e2.getMessage());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.reward.a.c.a(int, boolean, java.lang.String):void
     arg types: [int, int, java.lang.String]
     candidates:
      com.mintegral.msdk.reward.a.c.a(com.mintegral.msdk.reward.a.c, java.lang.String, java.lang.String):void
      com.mintegral.msdk.reward.a.c.a(java.lang.String, com.mintegral.msdk.base.entity.CampaignEx, java.lang.String):void
      com.mintegral.msdk.reward.a.c.a(java.util.List<com.mintegral.msdk.base.entity.CampaignEx>, java.lang.String, com.mintegral.msdk.videocommon.e.c):void
      com.mintegral.msdk.reward.a.c.a(int, boolean, java.lang.String):void */
    public final void f() {
        a(8000, false, "");
    }

    public final void a(int i2, boolean z, String str) {
        this.e = 1;
        this.f = i2;
        this.g = z;
        if (this.b == null) {
            b("Context is null");
        } else if (s.a(this.c)) {
            b("UnitId is null");
        } else if (this.l == null) {
            b("RewardUnitSetting is null");
        } else {
            try {
                if (com.mintegral.msdk.base.common.a.c.a != null && com.mintegral.msdk.base.common.a.c.a.size() > 0) {
                    com.mintegral.msdk.base.common.a.c.a.clear();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            try {
                l.a(com.mintegral.msdk.base.b.i.a(this.b)).b(this.c);
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "load 开始清除过期数据");
            j();
            if (!this.r) {
                List<CampaignEx> k2 = k();
                if (k2 != null && k2.size() > 0) {
                    com.mintegral.msdk.base.utils.g.a("RewardMVVideoAdapter", "==本地campaign条数 大于0");
                    e(k2);
                    d(k2);
                    if (k2.size() >= this.l.y()) {
                        a(k2);
                        com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "load 本地已有缓存 返回load成功 vcn：" + this.l.y());
                        h();
                        return;
                    }
                    a(k2, this.c, this.l);
                    if (z && !this.l.c(3)) {
                        return;
                    }
                }
            } else {
                CampaignEx a2 = a(this.c, str);
                if (a2 != null && !a(a2)) {
                    c(a2);
                    com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "bid load 本地已有缓存 返回load成功 ");
                    h();
                    return;
                }
            }
            new Thread(new h(str)).start();
            if (this.s != null) {
                this.m = new i(str);
                this.s.postDelayed(this.m, 90000);
                return;
            }
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "handler 为空 直接load");
            a(str);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:42|43) */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        r0 = com.mintegral.msdk.base.common.a.i;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:42:0x015b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String r18) {
        /*
            r17 = this;
            r1 = r17
            r0 = r18
            android.content.Context r2 = r1.b     // Catch:{ Exception -> 0x0177 }
            if (r2 != 0) goto L_0x000e
            java.lang.String r0 = "Context is null"
            r1.b(r0)     // Catch:{ Exception -> 0x0177 }
            return
        L_0x000e:
            java.lang.String r2 = r1.c     // Catch:{ Exception -> 0x0177 }
            boolean r2 = com.mintegral.msdk.base.utils.s.a(r2)     // Catch:{ Exception -> 0x0177 }
            if (r2 == 0) goto L_0x001c
            java.lang.String r0 = "UnitId is null"
            r1.b(r0)     // Catch:{ Exception -> 0x0177 }
            return
        L_0x001c:
            com.mintegral.msdk.videocommon.e.c r2 = r1.l     // Catch:{ Exception -> 0x0177 }
            if (r2 != 0) goto L_0x0026
            java.lang.String r0 = "RewardUnitSetting is null"
            r1.b(r0)     // Catch:{ Exception -> 0x0177 }
            return
        L_0x0026:
            java.lang.String r2 = "RewardMVVideoAdapter"
            java.lang.String r3 = "load 开始准备请求参数"
            com.mintegral.msdk.base.utils.g.b(r2, r3)     // Catch:{ Exception -> 0x0177 }
            com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Exception -> 0x0177 }
            java.lang.String r2 = r2.j()     // Catch:{ Exception -> 0x0177 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0177 }
            r3.<init>()     // Catch:{ Exception -> 0x0177 }
            com.mintegral.msdk.base.controller.a r4 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Exception -> 0x0177 }
            java.lang.String r4 = r4.j()     // Catch:{ Exception -> 0x0177 }
            r3.append(r4)     // Catch:{ Exception -> 0x0177 }
            com.mintegral.msdk.base.controller.a r4 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Exception -> 0x0177 }
            java.lang.String r4 = r4.k()     // Catch:{ Exception -> 0x0177 }
            r3.append(r4)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0177 }
            java.lang.String r3 = com.mintegral.msdk.base.utils.CommonMD5.getMD5(r3)     // Catch:{ Exception -> 0x0177 }
            boolean r4 = r1.g     // Catch:{ Exception -> 0x0177 }
            if (r4 == 0) goto L_0x005e
            r4 = 2
            goto L_0x005f
        L_0x005e:
            r4 = 3
        L_0x005f:
            com.mintegral.msdk.videocommon.e.c r5 = r1.l     // Catch:{ Exception -> 0x0177 }
            r6 = 0
            if (r5 == 0) goto L_0x0071
            com.mintegral.msdk.videocommon.e.c r5 = r1.l     // Catch:{ Exception -> 0x0177 }
            int r6 = r5.u()     // Catch:{ Exception -> 0x0177 }
            com.mintegral.msdk.videocommon.e.c r5 = r1.l     // Catch:{ Exception -> 0x0177 }
            int r5 = r5.w()     // Catch:{ Exception -> 0x0177 }
            goto L_0x0072
        L_0x0071:
            r5 = 0
        L_0x0072:
            java.lang.String r7 = "1"
            java.lang.String r8 = "1"
            java.lang.String r9 = r1.i     // Catch:{ Exception -> 0x0177 }
            java.lang.String r10 = r1.h     // Catch:{ Exception -> 0x0177 }
            java.lang.String r11 = r1.c     // Catch:{ Exception -> 0x0177 }
            java.lang.String r12 = "reward"
            java.lang.String r11 = com.mintegral.msdk.base.common.a.c.a(r11, r12)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r12 = o()     // Catch:{ Exception -> 0x0177 }
            int r13 = r17.l()     // Catch:{ Exception -> 0x0177 }
            r1.d = r13     // Catch:{ Exception -> 0x0177 }
            java.lang.String r13 = n()     // Catch:{ Exception -> 0x0177 }
            int r14 = r1.e     // Catch:{ Exception -> 0x0177 }
            boolean r15 = r1.q     // Catch:{ Exception -> 0x0177 }
            if (r15 == 0) goto L_0x009b
            r15 = 287(0x11f, float:4.02E-43)
        L_0x0098:
            r16 = r5
            goto L_0x009e
        L_0x009b:
            r15 = 94
            goto L_0x0098
        L_0x009e:
            com.mintegral.msdk.base.common.net.l r5 = new com.mintegral.msdk.base.common.net.l     // Catch:{ Exception -> 0x0177 }
            r5.<init>()     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "app_id"
            com.mintegral.msdk.base.utils.n.a(r5, r0, r2)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "unit_id"
            java.lang.String r2 = r1.c     // Catch:{ Exception -> 0x0177 }
            com.mintegral.msdk.base.utils.n.a(r5, r0, r2)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "sign"
            com.mintegral.msdk.base.utils.n.a(r5, r0, r3)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "req_type"
            java.lang.String r2 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x0177 }
            com.mintegral.msdk.base.utils.n.a(r5, r0, r2)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "ad_num"
            java.lang.String r2 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x0177 }
            com.mintegral.msdk.base.utils.n.a(r5, r0, r2)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "tnum"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0177 }
            r2.<init>()     // Catch:{ Exception -> 0x0177 }
            boolean r3 = r1.q     // Catch:{ Exception -> 0x0177 }
            r4 = 1
            if (r3 == 0) goto L_0x00d4
            r3 = 1
            goto L_0x00d6
        L_0x00d4:
            r3 = r16
        L_0x00d6:
            r2.append(r3)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0177 }
            com.mintegral.msdk.base.utils.n.a(r5, r0, r2)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "only_impression"
            com.mintegral.msdk.base.utils.n.a(r5, r0, r7)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "ping_mode"
            com.mintegral.msdk.base.utils.n.a(r5, r0, r8)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "ttc_ids"
            com.mintegral.msdk.base.utils.n.a(r5, r0, r10)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "display_info"
            com.mintegral.msdk.base.utils.n.a(r5, r0, r11)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "exclude_ids"
            com.mintegral.msdk.base.utils.n.a(r5, r0, r9)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "install_ids"
            com.mintegral.msdk.base.utils.n.a(r5, r0, r12)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "ad_source_id"
            java.lang.String r2 = java.lang.String.valueOf(r14)     // Catch:{ Exception -> 0x0177 }
            com.mintegral.msdk.base.utils.n.a(r5, r0, r2)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "session_id"
            com.mintegral.msdk.base.utils.n.a(r5, r0, r13)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "ad_type"
            java.lang.String r2 = java.lang.String.valueOf(r15)     // Catch:{ Exception -> 0x0177 }
            com.mintegral.msdk.base.utils.n.a(r5, r0, r2)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "offset"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0177 }
            r2.<init>()     // Catch:{ Exception -> 0x0177 }
            int r3 = r1.d     // Catch:{ Exception -> 0x0177 }
            r2.append(r3)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0177 }
            com.mintegral.msdk.base.utils.n.a(r5, r0, r2)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = com.mintegral.msdk.base.common.a.k     // Catch:{ Exception -> 0x0177 }
            boolean r2 = android.text.TextUtils.isEmpty(r18)     // Catch:{ Exception -> 0x0177 }
            if (r2 != 0) goto L_0x015e
            java.lang.String r0 = "token"
            r2 = r18
            r5.a(r0, r2)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r0 = "_"
            java.lang.String[] r0 = r2.split(r0)     // Catch:{ Exception -> 0x015b }
            if (r0 == 0) goto L_0x0158
            int r3 = r0.length     // Catch:{ Exception -> 0x015b }
            if (r3 <= r4) goto L_0x0158
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x015b }
            java.lang.String r6 = "https://"
            r3.<init>(r6)     // Catch:{ Exception -> 0x015b }
            r0 = r0[r4]     // Catch:{ Exception -> 0x015b }
            r3.append(r0)     // Catch:{ Exception -> 0x015b }
            java.lang.String r0 = "-hb.rayjump.com/load"
            r3.append(r0)     // Catch:{ Exception -> 0x015b }
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x015b }
            goto L_0x0160
        L_0x0158:
            java.lang.String r0 = com.mintegral.msdk.base.common.a.i     // Catch:{ Exception -> 0x015b }
            goto L_0x0160
        L_0x015b:
            java.lang.String r0 = com.mintegral.msdk.base.common.a.i     // Catch:{ Exception -> 0x0177 }
            goto L_0x0160
        L_0x015e:
            r2 = r18
        L_0x0160:
            com.mintegral.msdk.reward.e.b r3 = new com.mintegral.msdk.reward.e.b     // Catch:{ Exception -> 0x0177 }
            android.content.Context r4 = r1.b     // Catch:{ Exception -> 0x0177 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0177 }
            com.mintegral.msdk.reward.a.c$2 r4 = new com.mintegral.msdk.reward.a.c$2     // Catch:{ Exception -> 0x0177 }
            r4.<init>()     // Catch:{ Exception -> 0x0177 }
            r4.b(r2)     // Catch:{ Exception -> 0x0177 }
            java.lang.String r2 = r1.c     // Catch:{ Exception -> 0x0177 }
            r4.d = r2     // Catch:{ Exception -> 0x0177 }
            r3.a(r0, r5, r4)     // Catch:{ Exception -> 0x0177 }
            return
        L_0x0177:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r0 = "Load exception"
            r1.b(r0)
            r17.m()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.reward.a.c.a(java.lang.String):void");
    }

    private void h() {
        if (this.s != null) {
            this.s.sendEmptyMessage(3);
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        if (this.s == null) {
            return;
        }
        if (TextUtils.isEmpty(str)) {
            this.s.sendEmptyMessage(4);
            return;
        }
        Message obtain = Message.obtain();
        obtain.what = 4;
        obtain.obj = str;
        this.s.sendMessage(obtain);
    }

    /* access modifiers changed from: private */
    public String i() {
        String str = "";
        try {
            com.mintegral.msdk.d.b.a();
            com.mintegral.msdk.d.a b2 = com.mintegral.msdk.d.b.b(com.mintegral.msdk.base.controller.a.d().j());
            JSONArray jSONArray = new JSONArray();
            if (b2 != null && b2.aK() == 1) {
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "excludes cfc:" + b2.aK());
                long[] c2 = m.a(com.mintegral.msdk.base.b.i.a(com.mintegral.msdk.base.controller.a.d().h())).c();
                if (c2 != null) {
                    for (long put : c2) {
                        com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "excludes campaignIds:" + c2);
                        jSONArray.put(put);
                    }
                }
            }
            List<String> p2 = p();
            if (p2 != null && p2.size() > 0) {
                for (int i2 = 0; i2 < p2.size(); i2++) {
                    String str2 = p2.get(i2);
                    if (s.b(str2)) {
                        try {
                            jSONArray.put(Long.parseLong(str2));
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                }
            }
            if (jSONArray.length() > 0) {
                str = k.a(jSONArray);
            }
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "get excludes:" + str);
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return str;
    }

    private void j() {
        if (com.mintegral.msdk.videocommon.a.a.a() != null) {
            com.mintegral.msdk.d.b.a();
            com.mintegral.msdk.d.a b2 = com.mintegral.msdk.d.b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (b2 == null) {
                com.mintegral.msdk.d.b.a();
                b2 = com.mintegral.msdk.d.b.b();
            }
            com.mintegral.msdk.videocommon.a.a.a().b(b2.ak() * 1000, this.c);
        }
    }

    private List<CampaignEx> k() {
        try {
            if (com.mintegral.msdk.videocommon.a.a.a() != null) {
                return com.mintegral.msdk.videocommon.a.a.a().a(this.c, this.e, this.r);
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private static boolean a(CampaignEx campaignEx) {
        try {
            if (com.mintegral.msdk.videocommon.a.a.a() == null) {
                return true;
            }
            com.mintegral.msdk.videocommon.a.a.a();
            return com.mintegral.msdk.videocommon.a.a.a(campaignEx);
        } catch (Exception e2) {
            e2.printStackTrace();
            return true;
        }
    }

    private static CampaignEx a(String str, String str2) {
        try {
            if (com.mintegral.msdk.videocommon.a.a.a() != null) {
                return com.mintegral.msdk.videocommon.a.a.a().a(str, str2);
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public final void a(b bVar) {
        if (bVar != null) {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "======set listener is not null");
        } else {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "======set listener is  null");
        }
        this.k = bVar;
    }

    private static void b(CampaignEx campaignEx) {
        try {
            com.mintegral.msdk.videocommon.a.a a2 = com.mintegral.msdk.videocommon.a.a.a();
            if (a2 != null) {
                a2.b(campaignEx);
            }
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
        }
    }

    private void c(CampaignEx campaignEx) {
        if (campaignEx != null) {
            if (!TextUtils.isEmpty(campaignEx.getEndScreenUrl())) {
                com.mintegral.msdk.videocommon.download.g.a().b(campaignEx.getEndScreenUrl());
            }
            if (campaignEx.getRewardTemplateMode() != null) {
                CampaignEx.c rewardTemplateMode = campaignEx.getRewardTemplateMode();
                if (!TextUtils.isEmpty(rewardTemplateMode.c())) {
                    if (rewardTemplateMode.c().contains(".zip")) {
                        com.mintegral.msdk.videocommon.download.g.a().a(rewardTemplateMode.c(), new g(campaignEx, this, this.c, 313, this.l, false));
                    } else {
                        com.mintegral.msdk.videocommon.download.g.a().b(rewardTemplateMode.c());
                    }
                }
                if (TextUtils.isEmpty(rewardTemplateMode.d())) {
                    return;
                }
                if (rewardTemplateMode.d().contains(".zip")) {
                    com.mintegral.msdk.videocommon.download.g.a().a(rewardTemplateMode.d(), new g(campaignEx, this, this.c, 859, this.l, false));
                    return;
                }
                com.mintegral.msdk.videocommon.download.g.a().a(rewardTemplateMode.d(), new C0065c(this, campaignEx, this.c, this.l));
            }
        }
    }

    private void a(List<CampaignEx> list) {
        if (list != null && list.size() > 0) {
            for (CampaignEx c2 : list) {
                c(c2);
            }
        }
    }

    /* renamed from: com.mintegral.msdk.reward.a.c$c  reason: collision with other inner class name */
    /* compiled from: RewardMVVideoAdapter */
    private static class C0065c implements g.a {
        private CampaignEx a;
        private String b;
        private com.mintegral.msdk.videocommon.e.c c;
        private c d;

        public C0065c(c cVar, CampaignEx campaignEx, String str, com.mintegral.msdk.videocommon.e.c cVar2) {
            this.a = campaignEx;
            this.b = str;
            this.c = cVar2;
            this.d = cVar;
        }

        public final void a(String str) {
            if (this.d != null) {
                this.d.a(this.a, str, this.b, this.c);
            }
        }

        public final void a(String str, String str2) {
            if (this.d != null) {
                c.a(this.d, "TemplateUrl source download failed", str);
            }
        }
    }

    private void b(List<CampaignEx> list) {
        if (list != null && list.size() > 0) {
            for (CampaignEx next : list) {
                String str = next.getendcard_url();
                if (!TextUtils.isEmpty(str) && !next.isMraid()) {
                    if (!str.contains(".zip") || !str.contains("md5filename")) {
                        com.mintegral.msdk.videocommon.download.g.a().a(str, new e(this, next, this.c, TextUtils.isEmpty(com.mintegral.msdk.videocommon.download.h.a().a(str))));
                    } else {
                        com.mintegral.msdk.videocommon.download.g.a().a(str, new g(next, this, this.c, 497, this.l, TextUtils.isEmpty(com.mintegral.msdk.videocommon.download.j.a().a(str))));
                    }
                }
            }
        }
    }

    /* compiled from: RewardMVVideoAdapter */
    private static final class e implements g.a {
        private c a;
        private CampaignEx b;
        private long c = System.currentTimeMillis();
        private String d;
        private boolean e = true;

        public e(c cVar, CampaignEx campaignEx, String str, boolean z) {
            this.d = str;
            this.a = cVar;
            this.b = campaignEx;
            this.e = z;
        }

        public final void a(String str) {
            try {
                if (this.a.s != null) {
                    com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "H5SourceDownloadListener 源码下载成功 cid:" + this.b.getId() + "  url:" + str);
                    this.a.s.removeMessages(5);
                    Message obtain = Message.obtain();
                    obtain.what = 8;
                    obtain.obj = this.b;
                    this.a.s.sendMessage(obtain);
                }
                if (this.e) {
                    long currentTimeMillis = System.currentTimeMillis() - this.c;
                    w.a(com.mintegral.msdk.base.b.i.a(com.mintegral.msdk.base.controller.a.d().h()));
                    q qVar = new q("2000043", 20, String.valueOf(currentTimeMillis), str, this.b.getId(), this.d, "", "2");
                    qVar.k(this.b.getRequestIdNotice());
                    qVar.m(this.b.getId());
                    if (this.b.getAdType() == 287) {
                        qVar.h(NetworkConstants.apiVersion);
                    } else if (this.b.getAdType() == 94) {
                        qVar.h("1");
                    }
                    com.mintegral.msdk.base.common.e.a.a(qVar, com.mintegral.msdk.base.controller.a.d().h(), this.d);
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
            }
        }

        public final void a(String str, String str2) {
            try {
                com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "H5SourceDownloadListener 源码下载失败 cid:" + this.b.getId() + "  url:" + str);
                if (this.b != null) {
                    com.mintegral.msdk.videocommon.a.a.a().b(this.b);
                }
                if (this.a != null) {
                    c.a(this.a, "H5 code resource download failed ", str);
                }
                if (this.e) {
                    w.a(com.mintegral.msdk.base.b.i.a(com.mintegral.msdk.base.controller.a.d().h()));
                    q qVar = new q("2000043", 21, String.valueOf(System.currentTimeMillis() - this.c), str, this.b.getId(), this.d, "url download failed", "2");
                    qVar.k(this.b.getRequestIdNotice());
                    qVar.m(this.b.getId());
                    if (this.b.getAdType() == 287) {
                        qVar.h(NetworkConstants.apiVersion);
                    } else if (this.b.getAdType() == 94) {
                        qVar.h("1");
                    }
                    com.mintegral.msdk.base.common.e.a.a(qVar, com.mintegral.msdk.base.controller.a.d().h(), this.d);
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
            }
        }
    }

    private void a(List<CampaignEx> list, String str, com.mintegral.msdk.videocommon.e.c cVar) {
        a.C0069a aVar;
        if (list != null) {
            try {
                for (CampaignEx next : list) {
                    if (next.getRewardTemplateMode() != null && !TextUtils.isEmpty(next.getRewardTemplateMode().d())) {
                        if (this.q) {
                            aVar = com.mintegral.msdk.videocommon.a.a(287, next);
                        } else {
                            aVar = com.mintegral.msdk.videocommon.a.a(94, next);
                        }
                        if (aVar != null) {
                            if (!(aVar.a() == null || aVar.a().getParent() == null)) {
                                if (this.q) {
                                    com.mintegral.msdk.videocommon.a.b(287, next);
                                } else {
                                    com.mintegral.msdk.videocommon.a.b(94, next);
                                }
                            }
                        }
                        a(next, next.getRewardTemplateMode().d(), str, cVar);
                    }
                }
            } catch (Exception e2) {
                if (MIntegralConstans.DEBUG) {
                    e2.printStackTrace();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(CampaignEx campaignEx, String str, String str2, com.mintegral.msdk.videocommon.e.c cVar) {
        try {
            if (!TextUtils.isEmpty(str)) {
                Object[] objArr = new Object[4];
                int i2 = 16;
                if (str.contains("zip")) {
                    str = com.mintegral.msdk.videocommon.download.j.a().a(str);
                } else {
                    String a2 = com.mintegral.msdk.videocommon.download.h.a().a(str);
                    if (!TextUtils.isEmpty(a2)) {
                        i2 = 9;
                        str = a2;
                    }
                }
                Message obtain = Message.obtain();
                obtain.what = i2;
                objArr[0] = campaignEx;
                objArr[1] = str;
                objArr[2] = str2;
                objArr[3] = cVar;
                obtain.obj = objArr;
                this.s.sendMessage(obtain);
            }
        } catch (Exception unused) {
        }
    }

    /* compiled from: RewardMVVideoAdapter */
    private static final class g implements g.c {
        private CampaignEx a;
        private c b;
        private long c = System.currentTimeMillis();
        private String d;
        private int e = 0;
        private com.mintegral.msdk.videocommon.e.c f;
        private boolean g = true;

        public g(CampaignEx campaignEx, c cVar, String str, int i, com.mintegral.msdk.videocommon.e.c cVar2, boolean z) {
            this.d = str;
            this.a = campaignEx;
            this.e = i;
            this.f = cVar2;
            if (cVar != null) {
                this.b = cVar;
            }
            this.g = z;
        }

        /* JADX WARNING: Removed duplicated region for block: B:29:0x00df A[Catch:{ Throwable -> 0x0148 }] */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00ed A[Catch:{ Throwable -> 0x0148 }] */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x010e A[Catch:{ Throwable -> 0x0148 }] */
        /* JADX WARNING: Removed duplicated region for block: B:53:? A[ADDED_TO_REGION, ORIG_RETURN, RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a(java.lang.String r17) {
            /*
                r16 = this;
                r1 = r16
                r0 = r17
                long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0148 }
                long r4 = r1.c     // Catch:{ Throwable -> 0x0148 }
                r6 = 0
                long r2 = r2 - r4
                com.mintegral.msdk.base.controller.a r4 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0148 }
                android.content.Context r4 = r4.h()     // Catch:{ Throwable -> 0x0148 }
                com.mintegral.msdk.base.b.i r4 = com.mintegral.msdk.base.b.i.a(r4)     // Catch:{ Throwable -> 0x0148 }
                com.mintegral.msdk.base.b.w r11 = com.mintegral.msdk.base.b.w.a(r4)     // Catch:{ Throwable -> 0x0148 }
                int r4 = r1.e     // Catch:{ Throwable -> 0x0148 }
                r12 = 859(0x35b, float:1.204E-42)
                r13 = 497(0x1f1, float:6.96E-43)
                r14 = 0
                if (r4 != r13) goto L_0x007a
                boolean r4 = r1.g     // Catch:{ Throwable -> 0x0148 }
                if (r4 == 0) goto L_0x00da
                com.mintegral.msdk.base.entity.q r15 = new com.mintegral.msdk.base.entity.q     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r4 = "2000043"
                r5 = 1
                java.lang.String r6 = java.lang.String.valueOf(r2)     // Catch:{ Throwable -> 0x0148 }
                com.mintegral.msdk.base.entity.CampaignEx r2 = r1.a     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r7 = r2.getId()     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r8 = r1.d     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r9 = ""
                java.lang.String r10 = "1"
                r2 = r15
                r3 = r4
                r4 = r5
                r5 = r6
                r6 = r17
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Throwable -> 0x0148 }
                com.mintegral.msdk.base.entity.CampaignEx r2 = r1.a     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r2 = r2.getRequestIdNotice()     // Catch:{ Throwable -> 0x0148 }
                r15.k(r2)     // Catch:{ Throwable -> 0x0148 }
                com.mintegral.msdk.base.entity.CampaignEx r2 = r1.a     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r2 = r2.getId()     // Catch:{ Throwable -> 0x0148 }
                r15.m(r2)     // Catch:{ Throwable -> 0x0148 }
                com.mintegral.msdk.base.entity.CampaignEx r2 = r1.a     // Catch:{ Throwable -> 0x0148 }
                int r2 = r2.getAdType()     // Catch:{ Throwable -> 0x0148 }
                r3 = 287(0x11f, float:4.02E-43)
                if (r2 != r3) goto L_0x006a
                java.lang.String r2 = "3"
                r15.h(r2)     // Catch:{ Throwable -> 0x0148 }
                goto L_0x00db
            L_0x006a:
                com.mintegral.msdk.base.entity.CampaignEx r2 = r1.a     // Catch:{ Throwable -> 0x0148 }
                int r2 = r2.getAdType()     // Catch:{ Throwable -> 0x0148 }
                r3 = 94
                if (r2 != r3) goto L_0x00db
                java.lang.String r2 = "1"
                r15.h(r2)     // Catch:{ Throwable -> 0x0148 }
                goto L_0x00db
            L_0x007a:
                int r2 = r1.e     // Catch:{ Throwable -> 0x0148 }
                if (r2 != r12) goto L_0x00d3
                com.mintegral.msdk.base.entity.q r2 = new com.mintegral.msdk.base.entity.q     // Catch:{ Throwable -> 0x0148 }
                r2.<init>()     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r3 = "2000045"
                r2.n(r3)     // Catch:{ Throwable -> 0x0148 }
                com.mintegral.msdk.reward.a.c r3 = r1.b     // Catch:{ Throwable -> 0x0148 }
                if (r3 == 0) goto L_0x00aa
                com.mintegral.msdk.reward.a.c r3 = r1.b     // Catch:{ Throwable -> 0x0148 }
                com.mintegral.msdk.base.entity.CampaignEx r4 = r1.a     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r5 = r1.d     // Catch:{ Throwable -> 0x0148 }
                com.mintegral.msdk.videocommon.e.c r6 = r1.f     // Catch:{ Throwable -> 0x0148 }
                r3.a(r4, r0, r5, r6)     // Catch:{ Throwable -> 0x0148 }
                com.mintegral.msdk.reward.a.c r3 = r1.b     // Catch:{ Throwable -> 0x0148 }
                android.content.Context r3 = r3.b     // Catch:{ Throwable -> 0x0148 }
                if (r3 == 0) goto L_0x00aa
                android.content.Context r3 = r3.getApplicationContext()     // Catch:{ Throwable -> 0x0148 }
                int r3 = com.mintegral.msdk.base.utils.c.s(r3)     // Catch:{ Throwable -> 0x0148 }
                r2.b(r3)     // Catch:{ Throwable -> 0x0148 }
            L_0x00aa:
                r3 = 1
                r2.c(r3)     // Catch:{ Throwable -> 0x0148 }
                com.mintegral.msdk.base.entity.CampaignEx r3 = r1.a     // Catch:{ Throwable -> 0x0148 }
                if (r3 == 0) goto L_0x00c4
                com.mintegral.msdk.base.entity.CampaignEx r3 = r1.a     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r3 = r3.getId()     // Catch:{ Throwable -> 0x0148 }
                r2.m(r3)     // Catch:{ Throwable -> 0x0148 }
                com.mintegral.msdk.base.entity.CampaignEx r3 = r1.a     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r3 = r3.getRequestIdNotice()     // Catch:{ Throwable -> 0x0148 }
                r2.k(r3)     // Catch:{ Throwable -> 0x0148 }
            L_0x00c4:
                r2.i(r0)     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r3 = ""
                r2.o(r3)     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r3 = r1.d     // Catch:{ Throwable -> 0x0148 }
                r2.l(r3)     // Catch:{ Throwable -> 0x0148 }
                r15 = r2
                goto L_0x00db
            L_0x00d3:
                int r2 = r1.e     // Catch:{ Throwable -> 0x0148 }
                r3 = 313(0x139, float:4.39E-43)
                if (r2 != r3) goto L_0x00da
                return
            L_0x00da:
                r15 = r14
            L_0x00db:
                int r2 = r1.e     // Catch:{ Throwable -> 0x0148 }
                if (r2 != r13) goto L_0x00ed
                com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Throwable -> 0x0148 }
                android.content.Context r2 = r2.h()     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r3 = r1.d     // Catch:{ Throwable -> 0x0148 }
                com.mintegral.msdk.base.common.e.a.a(r15, r2, r3)     // Catch:{ Throwable -> 0x0148 }
                goto L_0x00f0
            L_0x00ed:
                r11.a(r15)     // Catch:{ Throwable -> 0x0148 }
            L_0x00f0:
                com.mintegral.msdk.videocommon.download.i r2 = com.mintegral.msdk.videocommon.download.i.a()     // Catch:{ Throwable -> 0x0148 }
                r2.b(r0)     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r2 = "RewardMVVideoAdapter"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r4 = "RewardZipDownloadListener ZIP SUCCESS:"
                r3.<init>(r4)     // Catch:{ Throwable -> 0x0148 }
                r3.append(r0)     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r0 = r3.toString()     // Catch:{ Throwable -> 0x0148 }
                com.mintegral.msdk.base.utils.g.d(r2, r0)     // Catch:{ Throwable -> 0x0148 }
                com.mintegral.msdk.reward.a.c r0 = r1.b     // Catch:{ Throwable -> 0x0148 }
                if (r0 == 0) goto L_0x0147
                int r0 = r1.e     // Catch:{ Throwable -> 0x0148 }
                if (r0 == r12) goto L_0x0147
                com.mintegral.msdk.reward.a.c r2 = r1.b     // Catch:{ Throwable -> 0x0148 }
                monitor-enter(r2)     // Catch:{ Throwable -> 0x0148 }
                java.lang.String r0 = "RewardMVVideoAdapter"
                java.lang.String r3 = "adapter 1286"
                com.mintegral.msdk.base.utils.g.d(r0, r3)     // Catch:{ all -> 0x0144 }
                com.mintegral.msdk.reward.a.c r0 = r1.b     // Catch:{ all -> 0x0144 }
                boolean r0 = r0.d()     // Catch:{ all -> 0x0144 }
                if (r0 == 0) goto L_0x0142
                com.mintegral.msdk.reward.a.c r0 = r1.b     // Catch:{ all -> 0x0144 }
                android.os.Handler r0 = r0.s     // Catch:{ all -> 0x0144 }
                if (r0 == 0) goto L_0x0142
                com.mintegral.msdk.reward.a.c r0 = r1.b     // Catch:{ all -> 0x0144 }
                android.os.Handler r0 = r0.s     // Catch:{ all -> 0x0144 }
                r3 = 6
                r0.sendEmptyMessage(r3)     // Catch:{ all -> 0x0144 }
                com.mintegral.msdk.reward.a.c r0 = r1.b     // Catch:{ all -> 0x0144 }
                android.os.Handler r0 = r0.s     // Catch:{ all -> 0x0144 }
                r3 = 5
                r0.removeMessages(r3)     // Catch:{ all -> 0x0144 }
                r1.b = r14     // Catch:{ all -> 0x0144 }
            L_0x0142:
                monitor-exit(r2)     // Catch:{ all -> 0x0144 }
                return
            L_0x0144:
                r0 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x0144 }
                throw r0     // Catch:{ Throwable -> 0x0148 }
            L_0x0147:
                return
            L_0x0148:
                r0 = move-exception
                java.lang.String r2 = "RewardMVVideoAdapter"
                java.lang.String r3 = r0.getMessage()
                com.mintegral.msdk.base.utils.g.c(r2, r3, r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.reward.a.c.g.a(java.lang.String):void");
        }

        /* JADX WARNING: Removed duplicated region for block: B:29:0x00e6 A[Catch:{ Exception -> 0x0121, Throwable -> 0x0133 }] */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00f4 A[Catch:{ Exception -> 0x0121, Throwable -> 0x0133 }] */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x00fb A[Catch:{ Exception -> 0x0121, Throwable -> 0x0133 }] */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0108 A[Catch:{ Exception -> 0x0121, Throwable -> 0x0133 }] */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x014a  */
        /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a(java.lang.String r17, java.lang.String r18) {
            /*
                r16 = this;
                r1 = r16
                r0 = r17
                r11 = r18
                java.lang.String r2 = "RewardMVVideoAdapter"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                java.lang.String r4 = "RewardZipDownloadListener ZIP failed:"
                r3.<init>(r4)
                r3.append(r11)
                java.lang.String r3 = r3.toString()
                com.mintegral.msdk.base.utils.g.d(r2, r3)
                r12 = 0
                long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0121 }
                long r4 = r1.c     // Catch:{ Exception -> 0x0121 }
                r6 = 0
                long r2 = r2 - r4
                com.mintegral.msdk.base.controller.a r4 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Exception -> 0x0121 }
                android.content.Context r4 = r4.h()     // Catch:{ Exception -> 0x0121 }
                com.mintegral.msdk.base.b.i r4 = com.mintegral.msdk.base.b.i.a(r4)     // Catch:{ Exception -> 0x0121 }
                com.mintegral.msdk.base.b.w r13 = com.mintegral.msdk.base.b.w.a(r4)     // Catch:{ Exception -> 0x0121 }
                int r4 = r1.e     // Catch:{ Exception -> 0x0121 }
                r14 = 497(0x1f1, float:6.96E-43)
                if (r4 != r14) goto L_0x008c
                boolean r4 = r1.g     // Catch:{ Exception -> 0x0121 }
                if (r4 == 0) goto L_0x00e1
                com.mintegral.msdk.base.entity.q r15 = new com.mintegral.msdk.base.entity.q     // Catch:{ Exception -> 0x0121 }
                java.lang.String r4 = "2000043"
                r5 = 3
                java.lang.String r6 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x0121 }
                com.mintegral.msdk.base.entity.CampaignEx r2 = r1.a     // Catch:{ Exception -> 0x0121 }
                java.lang.String r7 = r2.getId()     // Catch:{ Exception -> 0x0121 }
                java.lang.String r8 = r1.d     // Catch:{ Exception -> 0x0121 }
                java.lang.String r9 = "zip download failed"
                java.lang.String r10 = "1"
                r2 = r15
                r3 = r4
                r4 = r5
                r5 = r6
                r6 = r18
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x0121 }
                com.mintegral.msdk.base.entity.CampaignEx r2 = r1.a     // Catch:{ Exception -> 0x0121 }
                java.lang.String r2 = r2.getRequestIdNotice()     // Catch:{ Exception -> 0x0121 }
                r15.k(r2)     // Catch:{ Exception -> 0x0121 }
                com.mintegral.msdk.base.entity.CampaignEx r2 = r1.a     // Catch:{ Exception -> 0x0121 }
                java.lang.String r2 = r2.getId()     // Catch:{ Exception -> 0x0121 }
                r15.m(r2)     // Catch:{ Exception -> 0x0121 }
                com.mintegral.msdk.base.entity.CampaignEx r2 = r1.a     // Catch:{ Exception -> 0x0121 }
                int r2 = r2.getAdType()     // Catch:{ Exception -> 0x0121 }
                r3 = 287(0x11f, float:4.02E-43)
                if (r2 != r3) goto L_0x007c
                java.lang.String r2 = "3"
                r15.h(r2)     // Catch:{ Exception -> 0x0121 }
                goto L_0x00e2
            L_0x007c:
                com.mintegral.msdk.base.entity.CampaignEx r2 = r1.a     // Catch:{ Exception -> 0x0121 }
                int r2 = r2.getAdType()     // Catch:{ Exception -> 0x0121 }
                r3 = 94
                if (r2 != r3) goto L_0x00e2
                java.lang.String r2 = "1"
                r15.h(r2)     // Catch:{ Exception -> 0x0121 }
                goto L_0x00e2
            L_0x008c:
                int r2 = r1.e     // Catch:{ Exception -> 0x0121 }
                r3 = 859(0x35b, float:1.204E-42)
                if (r2 != r3) goto L_0x00da
                com.mintegral.msdk.base.entity.q r2 = new com.mintegral.msdk.base.entity.q     // Catch:{ Exception -> 0x0121 }
                r2.<init>()     // Catch:{ Exception -> 0x0121 }
                java.lang.String r3 = "2000045"
                r2.n(r3)     // Catch:{ Exception -> 0x0121 }
                com.mintegral.msdk.reward.a.c r3 = r1.b     // Catch:{ Exception -> 0x0121 }
                if (r3 == 0) goto L_0x00b3
                com.mintegral.msdk.reward.a.c r3 = r1.b     // Catch:{ Exception -> 0x0121 }
                android.content.Context r3 = r3.b     // Catch:{ Exception -> 0x0121 }
                if (r3 == 0) goto L_0x00b3
                android.content.Context r3 = r3.getApplicationContext()     // Catch:{ Exception -> 0x0121 }
                int r3 = com.mintegral.msdk.base.utils.c.s(r3)     // Catch:{ Exception -> 0x0121 }
                r2.b(r3)     // Catch:{ Exception -> 0x0121 }
            L_0x00b3:
                r3 = 3
                r2.c(r3)     // Catch:{ Exception -> 0x0121 }
                com.mintegral.msdk.base.entity.CampaignEx r3 = r1.a     // Catch:{ Exception -> 0x0121 }
                if (r3 == 0) goto L_0x00cd
                com.mintegral.msdk.base.entity.CampaignEx r3 = r1.a     // Catch:{ Exception -> 0x0121 }
                java.lang.String r3 = r3.getId()     // Catch:{ Exception -> 0x0121 }
                r2.m(r3)     // Catch:{ Exception -> 0x0121 }
                com.mintegral.msdk.base.entity.CampaignEx r3 = r1.a     // Catch:{ Exception -> 0x0121 }
                java.lang.String r3 = r3.getRequestIdNotice()     // Catch:{ Exception -> 0x0121 }
                r2.k(r3)     // Catch:{ Exception -> 0x0121 }
            L_0x00cd:
                r2.i(r11)     // Catch:{ Exception -> 0x0121 }
                r2.o(r0)     // Catch:{ Exception -> 0x0121 }
                java.lang.String r3 = r1.d     // Catch:{ Exception -> 0x0121 }
                r2.l(r3)     // Catch:{ Exception -> 0x0121 }
                r15 = r2
                goto L_0x00e2
            L_0x00da:
                int r2 = r1.e     // Catch:{ Exception -> 0x0121 }
                r3 = 313(0x139, float:4.39E-43)
                if (r2 != r3) goto L_0x00e1
                return
            L_0x00e1:
                r15 = r12
            L_0x00e2:
                int r2 = r1.e     // Catch:{ Exception -> 0x0121 }
                if (r2 != r14) goto L_0x00f4
                com.mintegral.msdk.base.controller.a r2 = com.mintegral.msdk.base.controller.a.d()     // Catch:{ Exception -> 0x0121 }
                android.content.Context r2 = r2.h()     // Catch:{ Exception -> 0x0121 }
                java.lang.String r3 = r1.d     // Catch:{ Exception -> 0x0121 }
                com.mintegral.msdk.base.common.e.a.a(r15, r2, r3)     // Catch:{ Exception -> 0x0121 }
                goto L_0x00f7
            L_0x00f4:
                r13.a(r15)     // Catch:{ Exception -> 0x0121 }
            L_0x00f7:
                com.mintegral.msdk.base.entity.CampaignEx r2 = r1.a     // Catch:{ Exception -> 0x0121 }
                if (r2 == 0) goto L_0x0104
                com.mintegral.msdk.videocommon.a.a r2 = com.mintegral.msdk.videocommon.a.a.a()     // Catch:{ Exception -> 0x0121 }
                com.mintegral.msdk.base.entity.CampaignEx r3 = r1.a     // Catch:{ Exception -> 0x0121 }
                r2.b(r3)     // Catch:{ Exception -> 0x0121 }
            L_0x0104:
                com.mintegral.msdk.reward.a.c r2 = r1.b     // Catch:{ Exception -> 0x0121 }
                if (r2 == 0) goto L_0x0146
                java.lang.String r2 = "RewardMVVideoAdapter"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0121 }
                java.lang.String r4 = "RewardZipDownloadListener ZIP failed:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x0121 }
                r3.append(r11)     // Catch:{ Exception -> 0x0121 }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0121 }
                com.mintegral.msdk.base.utils.g.d(r2, r3)     // Catch:{ Exception -> 0x0121 }
                com.mintegral.msdk.reward.a.c r2 = r1.b     // Catch:{ Exception -> 0x0121 }
                com.mintegral.msdk.reward.a.c.a(r2, r0, r11)     // Catch:{ Exception -> 0x0121 }
                goto L_0x0146
            L_0x0121:
                r0 = move-exception
                r2 = r0
                com.mintegral.msdk.reward.a.c r0 = r1.b     // Catch:{ Throwable -> 0x0133 }
                if (r0 == 0) goto L_0x013d
                com.mintegral.msdk.reward.a.c r0 = r1.b     // Catch:{ Throwable -> 0x0133 }
                com.mintegral.msdk.reward.a.b r0 = r0.k     // Catch:{ Throwable -> 0x0133 }
                java.lang.String r3 = "clear error info failed"
                r0.a(r3)     // Catch:{ Throwable -> 0x0133 }
                goto L_0x013d
            L_0x0133:
                r0 = move-exception
                java.lang.String r3 = "RewardMVVideoAdapter"
                java.lang.String r4 = r0.getMessage()
                com.mintegral.msdk.base.utils.g.c(r3, r4, r0)
            L_0x013d:
                java.lang.String r0 = "RewardMVVideoAdapter"
                java.lang.String r3 = r2.getMessage()
                com.mintegral.msdk.base.utils.g.c(r0, r3, r2)
            L_0x0146:
                com.mintegral.msdk.reward.a.c r0 = r1.b
                if (r0 == 0) goto L_0x014c
                r1.b = r12
            L_0x014c:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.reward.a.c.g.a(java.lang.String, java.lang.String):void");
        }
    }

    private void c(List<CampaignEx> list) {
        if (list != null && list.size() > 0) {
            for (CampaignEx next : list) {
                if (!TextUtils.isEmpty(next.getIconUrl())) {
                    com.mintegral.msdk.base.common.c.b.a(com.mintegral.msdk.base.controller.a.d().h()).a(next.getIconUrl(), new b(this, next, this.c));
                }
                if (!TextUtils.isEmpty(next.getImageUrl())) {
                    com.mintegral.msdk.base.common.c.b.a(com.mintegral.msdk.base.controller.a.d().h()).a(next.getImageUrl(), new b(this, next, this.c));
                }
            }
        }
    }

    private void d(List<CampaignEx> list) {
        List<CampaignEx.c.a> e2;
        if (list != null) {
            try {
                if (list.size() > 0) {
                    for (CampaignEx next : list) {
                        if (!(next.getRewardTemplateMode() == null || next.getRewardTemplateMode().e() == null || (e2 = next.getRewardTemplateMode().e()) == null)) {
                            for (CampaignEx.c.a next2 : e2) {
                                if (!(next2 == null || next2.b == null)) {
                                    for (String next3 : next2.b) {
                                        if (s.b(next3)) {
                                            com.mintegral.msdk.base.common.c.b.a(com.mintegral.msdk.base.controller.a.d().h()).a(next3, new d(this, next, this.c));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Throwable th) {
                if (MIntegralConstans.DEBUG) {
                    th.printStackTrace();
                }
            }
        }
    }

    /* compiled from: RewardMVVideoAdapter */
    private static final class d implements com.mintegral.msdk.base.common.c.c {
        private c a;
        private CampaignEx b;
        private String c;

        public d(c cVar, CampaignEx campaignEx, String str) {
            if (cVar != null) {
                this.a = cVar;
            }
            this.b = campaignEx;
            this.c = str;
        }

        public final void onSuccessLoad(Bitmap bitmap, String str) {
            try {
                com.mintegral.msdk.videocommon.download.i.a();
                com.mintegral.msdk.videocommon.download.i.c(str);
                com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "DownTemplateImgCommonImageLoaderListener IMAGE SUCCESS" + str);
                if (this.a != null) {
                    synchronized (this.a) {
                        com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "adapter 1433");
                        if (this.a.d() && this.a.s != null) {
                            Message obtain = Message.obtain();
                            obtain.what = 6;
                            obtain.obj = this.b;
                            this.a.s.sendMessage(obtain);
                            this.a.s.removeMessages(5);
                            this.a = null;
                        }
                    }
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
            }
        }

        public final void onFailedLoad(String str, String str2) {
            try {
                if (this.a != null) {
                    synchronized (this.a) {
                        com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "DownTemplateImgCommonImageLoaderListener IMAGE failed");
                        c.a(this.c, this.b, str);
                        c.a(this.a, str, str2);
                        this.a = null;
                    }
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
            }
        }
    }

    /* compiled from: RewardMVVideoAdapter */
    private static final class b implements com.mintegral.msdk.base.common.c.c {
        private c a;
        private CampaignEx b;
        private String c;

        public final void onSuccessLoad(Bitmap bitmap, String str) {
        }

        public b(c cVar, CampaignEx campaignEx, String str) {
            if (cVar != null) {
                this.a = cVar;
            }
            this.b = campaignEx;
            this.c = str;
        }

        public final void onFailedLoad(String str, String str2) {
            if (this.a != null) {
                c.a(this.c, this.b, str);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x010f, code lost:
        if (r8 != null) goto L_0x00f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0167, code lost:
        if (com.mintegral.msdk.base.utils.s.a(r5.getVideoUrlEncode()) != false) goto L_0x0169;
     */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0136 A[Catch:{ Exception -> 0x01b0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x016e A[Catch:{ Exception -> 0x01b0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0194 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List<com.mintegral.msdk.base.entity.CampaignEx> a(com.mintegral.msdk.base.entity.CampaignUnit r13) {
        /*
            r12 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.ArrayList r1 = r13.getAds()     // Catch:{ Exception -> 0x01b0 }
            com.mintegral.msdk.base.utils.k.a(r1)     // Catch:{ Exception -> 0x01b0 }
            com.mintegral.msdk.videocommon.e.c r1 = r12.l     // Catch:{ Exception -> 0x01b0 }
            r2 = 1
            if (r1 == 0) goto L_0x0019
            com.mintegral.msdk.videocommon.e.c r1 = r12.l     // Catch:{ Exception -> 0x01b0 }
            int r1 = r1.w()     // Catch:{ Exception -> 0x01b0 }
            if (r1 > 0) goto L_0x001a
        L_0x0019:
            r1 = 1
        L_0x001a:
            if (r13 == 0) goto L_0x01b4
            java.util.ArrayList r3 = r13.getAds()     // Catch:{ Exception -> 0x01b0 }
            if (r3 == 0) goto L_0x01b4
            java.util.ArrayList r3 = r13.getAds()     // Catch:{ Exception -> 0x01b0 }
            int r3 = r3.size()     // Catch:{ Exception -> 0x01b0 }
            if (r3 <= 0) goto L_0x01b4
            java.util.ArrayList r13 = r13.getAds()     // Catch:{ Exception -> 0x01b0 }
            java.lang.String r3 = "RewardMVVideoAdapter"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b0 }
            java.lang.String r5 = "onload 总共返回 的compaign有："
            r4.<init>(r5)     // Catch:{ Exception -> 0x01b0 }
            int r5 = r13.size()     // Catch:{ Exception -> 0x01b0 }
            r4.append(r5)     // Catch:{ Exception -> 0x01b0 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01b0 }
            com.mintegral.msdk.base.utils.g.b(r3, r4)     // Catch:{ Exception -> 0x01b0 }
            r3 = 0
            r4 = 0
        L_0x0049:
            int r5 = r13.size()     // Catch:{ Exception -> 0x01b0 }
            if (r4 >= r5) goto L_0x0198
            if (r4 >= r1) goto L_0x0198
            java.lang.Object r5 = r13.get(r4)     // Catch:{ Exception -> 0x01b0 }
            com.mintegral.msdk.base.entity.CampaignEx r5 = (com.mintegral.msdk.base.entity.CampaignEx) r5     // Catch:{ Exception -> 0x01b0 }
            boolean r6 = r5.isMraid()     // Catch:{ Exception -> 0x01b0 }
            if (r6 == 0) goto L_0x013a
            java.lang.String r6 = r5.getMraid()     // Catch:{ Exception -> 0x01b0 }
            java.lang.String r6 = r6.trim()     // Catch:{ Exception -> 0x01b0 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ Exception -> 0x01b0 }
            if (r6 != 0) goto L_0x0194
            r6 = 0
            java.lang.String r7 = ""
            int r8 = r5.getAdType()     // Catch:{ Exception -> 0x01b0 }
            r9 = 287(0x11f, float:4.02E-43)
            if (r8 != r9) goto L_0x0079
            java.lang.String r7 = "3"
            goto L_0x008e
        L_0x0079:
            int r8 = r5.getAdType()     // Catch:{ Exception -> 0x01b0 }
            r9 = 94
            if (r8 != r9) goto L_0x0084
            java.lang.String r7 = "1"
            goto L_0x008e
        L_0x0084:
            int r8 = r5.getAdType()     // Catch:{ Exception -> 0x01b0 }
            r9 = 42
            if (r8 != r9) goto L_0x008e
            java.lang.String r7 = "2"
        L_0x008e:
            com.mintegral.msdk.base.common.b.c r8 = com.mintegral.msdk.base.common.b.c.MINTEGRAL_700_HTML     // Catch:{ Exception -> 0x00fa, all -> 0x00f7 }
            java.lang.String r8 = com.mintegral.msdk.base.common.b.e.b(r8)     // Catch:{ Exception -> 0x00fa, all -> 0x00f7 }
            java.lang.String r9 = r5.getMraid()     // Catch:{ Exception -> 0x00fa, all -> 0x00f7 }
            java.lang.String r9 = com.mintegral.msdk.base.utils.CommonMD5.getMD5(r9)     // Catch:{ Exception -> 0x00fa, all -> 0x00f7 }
            boolean r10 = android.text.TextUtils.isEmpty(r9)     // Catch:{ Exception -> 0x00fa, all -> 0x00f7 }
            if (r10 == 0) goto L_0x00aa
            long r9 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00fa, all -> 0x00f7 }
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ Exception -> 0x00fa, all -> 0x00f7 }
        L_0x00aa:
            java.lang.String r10 = ".html"
            java.lang.String r9 = r9.concat(r10)     // Catch:{ Exception -> 0x00fa, all -> 0x00f7 }
            java.io.File r10 = new java.io.File     // Catch:{ Exception -> 0x00fa, all -> 0x00f7 }
            r10.<init>(r8, r9)     // Catch:{ Exception -> 0x00fa, all -> 0x00f7 }
            java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00fa, all -> 0x00f7 }
            r8.<init>(r10)     // Catch:{ Exception -> 0x00fa, all -> 0x00f7 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f5 }
            r6.<init>()     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r9 = "<script>"
            r6.append(r9)     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r9 = com.mintegral.msdk.base.utils.o.a     // Catch:{ Exception -> 0x00f5 }
            r6.append(r9)     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r9 = "</script>"
            r6.append(r9)     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r9 = r5.getMraid()     // Catch:{ Exception -> 0x00f5 }
            r6.append(r9)     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00f5 }
            byte[] r6 = r6.getBytes()     // Catch:{ Exception -> 0x00f5 }
            r8.write(r6)     // Catch:{ Exception -> 0x00f5 }
            r8.flush()     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r6 = r10.getAbsolutePath()     // Catch:{ Exception -> 0x00f5 }
            r5.setMraid(r6)     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r6 = ""
            java.lang.String r9 = r12.c     // Catch:{ Exception -> 0x00f5 }
            com.mintegral.msdk.base.common.e.a.a(r5, r6, r9, r7)     // Catch:{ Exception -> 0x00f5 }
        L_0x00f1:
            r8.close()     // Catch:{ Exception -> 0x01b0 }
            goto L_0x0112
        L_0x00f5:
            r6 = move-exception
            goto L_0x00fe
        L_0x00f7:
            r13 = move-exception
            r8 = r6
            goto L_0x0134
        L_0x00fa:
            r8 = move-exception
            r11 = r8
            r8 = r6
            r6 = r11
        L_0x00fe:
            r6.printStackTrace()     // Catch:{ all -> 0x0133 }
            java.lang.String r9 = ""
            r5.setMraid(r9)     // Catch:{ all -> 0x0133 }
            java.lang.String r6 = r6.getMessage()     // Catch:{ all -> 0x0133 }
            java.lang.String r9 = r12.c     // Catch:{ all -> 0x0133 }
            com.mintegral.msdk.base.common.e.a.a(r5, r6, r9, r7)     // Catch:{ all -> 0x0133 }
            if (r8 == 0) goto L_0x0112
            goto L_0x00f1
        L_0x0112:
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x01b0 }
            java.lang.String r7 = r5.getMraid()     // Catch:{ Exception -> 0x01b0 }
            r6.<init>(r7)     // Catch:{ Exception -> 0x01b0 }
            boolean r7 = r6.exists()     // Catch:{ Exception -> 0x01b0 }
            if (r7 == 0) goto L_0x012d
            boolean r7 = r6.isFile()     // Catch:{ Exception -> 0x01b0 }
            if (r7 == 0) goto L_0x012d
            boolean r6 = r6.canRead()     // Catch:{ Exception -> 0x01b0 }
            if (r6 != 0) goto L_0x013a
        L_0x012d:
            java.lang.String r5 = "mraid resource write fail"
            r12.b(r5)     // Catch:{ Exception -> 0x01b0 }
            goto L_0x0194
        L_0x0133:
            r13 = move-exception
        L_0x0134:
            if (r8 == 0) goto L_0x0139
            r8.close()     // Catch:{ Exception -> 0x01b0 }
        L_0x0139:
            throw r13     // Catch:{ Exception -> 0x01b0 }
        L_0x013a:
            if (r5 == 0) goto L_0x0194
            int r6 = r5.getOfferType()     // Catch:{ Exception -> 0x01b0 }
            r7 = 99
            if (r6 == r7) goto L_0x0194
            boolean r6 = d(r5)     // Catch:{ Exception -> 0x01b0 }
            if (r6 == 0) goto L_0x015f
            java.lang.String r6 = r5.getendcard_url()     // Catch:{ Exception -> 0x01b0 }
            boolean r6 = com.mintegral.msdk.base.utils.s.a(r6)     // Catch:{ Exception -> 0x01b0 }
            if (r6 == 0) goto L_0x016b
            java.lang.String r6 = r5.getMraid()     // Catch:{ Exception -> 0x01b0 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ Exception -> 0x01b0 }
            if (r6 == 0) goto L_0x016b
            goto L_0x0169
        L_0x015f:
            java.lang.String r6 = r5.getVideoUrlEncode()     // Catch:{ Exception -> 0x01b0 }
            boolean r6 = com.mintegral.msdk.base.utils.s.a(r6)     // Catch:{ Exception -> 0x01b0 }
            if (r6 == 0) goto L_0x016b
        L_0x0169:
            r6 = 0
            goto L_0x016c
        L_0x016b:
            r6 = 1
        L_0x016c:
            if (r6 == 0) goto L_0x0194
            int r6 = r5.getWtick()     // Catch:{ Exception -> 0x01b0 }
            if (r6 == r2) goto L_0x0191
            android.content.Context r6 = r12.b     // Catch:{ Exception -> 0x01b0 }
            java.lang.String r7 = r5.getPackageName()     // Catch:{ Exception -> 0x01b0 }
            boolean r6 = com.mintegral.msdk.base.utils.k.a(r6, r7)     // Catch:{ Exception -> 0x01b0 }
            if (r6 != 0) goto L_0x0181
            goto L_0x0191
        L_0x0181:
            boolean r6 = com.mintegral.msdk.base.utils.k.b(r5)     // Catch:{ Exception -> 0x01b0 }
            if (r6 != 0) goto L_0x018d
            boolean r6 = com.mintegral.msdk.base.utils.k.a(r5)     // Catch:{ Exception -> 0x01b0 }
            if (r6 == 0) goto L_0x0194
        L_0x018d:
            r0.add(r5)     // Catch:{ Exception -> 0x01b0 }
            goto L_0x0194
        L_0x0191:
            r0.add(r5)     // Catch:{ Exception -> 0x01b0 }
        L_0x0194:
            int r4 = r4 + 1
            goto L_0x0049
        L_0x0198:
            java.lang.String r13 = "RewardMVVideoAdapter"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b0 }
            java.lang.String r2 = "onload 返回有以下带有视频素材的compaign："
            r1.<init>(r2)     // Catch:{ Exception -> 0x01b0 }
            int r2 = r0.size()     // Catch:{ Exception -> 0x01b0 }
            r1.append(r2)     // Catch:{ Exception -> 0x01b0 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01b0 }
            com.mintegral.msdk.base.utils.g.b(r13, r1)     // Catch:{ Exception -> 0x01b0 }
            goto L_0x01b4
        L_0x01b0:
            r13 = move-exception
            r13.printStackTrace()
        L_0x01b4:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mintegral.msdk.reward.a.c.a(com.mintegral.msdk.base.entity.CampaignUnit):java.util.List");
    }

    private void e(List<CampaignEx> list) {
        try {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "===准备下载");
            if (list == null || list.size() <= 0) {
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onload 不用下载视频素材 size为0");
                return;
            }
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onload 开始下载视频素材 size:" + list.size());
            this.a.clear();
            this.a.addAll(list);
            com.mintegral.msdk.videocommon.download.i.a().a(list);
            if (com.mintegral.msdk.videocommon.download.c.getInstance() != null) {
                com.mintegral.msdk.videocommon.download.c.getInstance().createUnitCache(this.b, this.c, list, 3, new a(this, list.get(0)));
                com.mintegral.msdk.videocommon.download.c.getInstance().load(this.c);
            }
        } catch (Exception e2) {
            com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", e2.getMessage(), e2);
        }
    }

    /* compiled from: RewardMVVideoAdapter */
    private static final class a implements com.mintegral.msdk.videocommon.listener.a {
        private c a;
        private CampaignEx b;

        public a(c cVar, CampaignEx campaignEx) {
            if (cVar != null) {
                this.a = cVar;
            }
            this.b = campaignEx;
        }

        public final void a(String str) {
            try {
                com.mintegral.msdk.videocommon.download.i.a().a(str);
                com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "CommonVideoDownloadListener VIDEO SUCCESS");
                if (this.a != null) {
                    synchronized (this.a) {
                        com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "adapter 1613");
                        if (this.a != null && this.a.d()) {
                            com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "CommonVideoDownloadListener VIDEO SUCCESS callback success");
                            if (this.a.s != null) {
                                Message obtain = Message.obtain();
                                obtain.what = 6;
                                obtain.obj = this.b;
                                this.a.s.sendMessage(obtain);
                                this.a.s.removeMessages(5);
                                this.a = null;
                            }
                        }
                    }
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
            }
        }

        public final void a(String str, String str2) {
            try {
                if (this.a != null) {
                    synchronized (this.a) {
                        com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "CommonVideoDownloadListener VIDEO failed");
                        c.a(this.a, str, str2);
                        this.a = null;
                    }
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
            }
        }
    }

    private int l() {
        try {
            int a2 = s.b(this.c) ? com.mintegral.msdk.reward.b.a.a(this.c) : 0;
            if (this.l == null || a2 > this.l.D()) {
                return 0;
            }
            return a2;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    /* access modifiers changed from: private */
    public void m() {
        try {
            if (s.b(this.c)) {
                com.mintegral.msdk.reward.b.a.a(this.c, 0);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private static String n() {
        try {
            if (s.b(com.mintegral.msdk.reward.b.a.a)) {
                return com.mintegral.msdk.reward.b.a.a;
            }
            return "";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    private static String o() {
        try {
            JSONArray jSONArray = new JSONArray();
            com.mintegral.msdk.base.controller.a.d();
            List<Long> g2 = com.mintegral.msdk.base.controller.a.g();
            if (g2 != null && g2.size() > 0) {
                for (Long longValue : g2) {
                    jSONArray.put(longValue.longValue());
                }
            }
            if (jSONArray.length() > 0) {
                return k.a(jSONArray);
            }
            return "";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    private List<String> p() {
        try {
            return l.a(com.mintegral.msdk.base.b.i.a(this.b)).a(this.c);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* compiled from: RewardMVVideoAdapter */
    public class h implements Runnable {
        private String b;

        public h(String str) {
            this.b = str;
        }

        public final void run() {
            com.mintegral.msdk.base.b.i a2;
            try {
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "=====getTtcRunnable 开始获取 mTtcIds:" + c.this.h + "  mExcludes:" + c.this.i);
                if (!(c.this.b == null || (a2 = com.mintegral.msdk.base.b.i.a(c.this.b)) == null)) {
                    com.mintegral.msdk.base.b.d a3 = com.mintegral.msdk.base.b.d.a(a2);
                    a3.c();
                    String unused = c.this.h = a3.a(c.this.c);
                }
                String unused2 = c.this.i = c.this.i();
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "=====getTtcRunnable 获取完毕 mTtcIds:" + c.this.h + "  mExcludes:" + c.this.i);
                if (c.this.o) {
                    com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "=====getTtcRunnable 获取ttcid和excludeids超时 mIsGetTtcExcIdsTimeout：" + c.this.o + " mIsGetTtcExcIdsSuccess:" + c.this.n);
                    return;
                }
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "=====getTtcRunnable 获取ttcid和excludeids没有超时 mIsGetTtcExcIdsTimeout:" + c.this.o + " mIsGetTtcExcIdsSuccess:" + c.this.n);
                if (c.this.m != null) {
                    com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "=====getTtcRunnable 删除 获取ttcid的超时任务");
                    c.this.s.removeCallbacks(c.this.m);
                }
                boolean unused3 = c.this.n = true;
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "=====getTtcRunnable 给handler发送消息 mTtcIds:" + c.this.h + "  mExcludes:" + c.this.i);
                if (c.this.s != null) {
                    Message obtainMessage = c.this.s.obtainMessage();
                    obtainMessage.obj = this.b;
                    obtainMessage.what = 1;
                    c.this.s.sendMessage(obtainMessage);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* compiled from: RewardMVVideoAdapter */
    public class i implements Runnable {
        private String b;

        public i(String str) {
            this.b = str;
        }

        public final void run() {
            try {
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "=====超时task 开始执行 mTtcIds:" + c.this.h + "  RewardMVVideoAdapter.this.mExcludes:" + c.this.i);
                if (c.this.n) {
                    com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "超时task 已经成功获取ttcid excludeids mIsGetTtcExcIdsTimeout:" + c.this.o + " mIsGetTtcExcIdsSuccess:" + c.this.n + "超时task不做处理");
                    return;
                }
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "获取ttcid excludeids超时 mIsGetTtcExcIdsTimeout:" + c.this.o + " mIsGetTtcExcIdsSuccess:" + c.this.n);
                boolean unused = c.this.o = true;
                if (c.this.s != null) {
                    Message obtainMessage = c.this.s.obtainMessage();
                    obtainMessage.obj = this.b;
                    obtainMessage.what = 2;
                    c.this.s.sendMessage(obtainMessage);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static boolean d(CampaignEx campaignEx) {
        if (campaignEx == null) {
            return false;
        }
        try {
            if (campaignEx.getPlayable_ads_without_video() == 2) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            if (!MIntegralConstans.DEBUG) {
                return false;
            }
            th.printStackTrace();
            return false;
        }
    }

    static /* synthetic */ void a(c cVar, CampaignEx campaignEx, String str, int i2, String str2, com.mintegral.msdk.videocommon.e.c cVar2) {
        c cVar3 = cVar;
        int i3 = i2;
        try {
            a.C0069a aVar = new a.C0069a();
            WindVaneWebView windVaneWebView = new WindVaneWebView(com.mintegral.msdk.base.controller.a.d().h());
            aVar.a(windVaneWebView);
            com.mintegral.msdk.video.js.a.h hVar = new com.mintegral.msdk.video.js.a.h(null, campaignEx);
            hVar.a(cVar3.p);
            hVar.a(str2);
            hVar.a(cVar2);
            f fVar = new f(campaignEx, str, str2, cVar2, cVar3.p, i2, cVar);
            windVaneWebView.setWebViewListener(new j(aVar, campaignEx, cVar, fVar, cVar3.s));
            windVaneWebView.setObject(hVar);
            if (i3 == 9) {
                String str3 = str;
                windVaneWebView.loadDataWithBaseURL(campaignEx.getRewardTemplateMode().d(), str, "text/html", "utf-8", null);
            } else if (i3 == 16) {
                windVaneWebView.loadUrl(str);
            }
            cVar3.s.postDelayed(fVar, 5000);
        } catch (Exception unused) {
        }
    }

    static /* synthetic */ void a(c cVar, String str, String str2) {
        try {
            com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "====delCampaignFromDownLoadCampaignListByUrld");
            if (cVar.a != null && !TextUtils.isEmpty(str2)) {
                Iterator<CampaignEx> it = cVar.a.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    CampaignEx next = it.next();
                    if (next != null) {
                        String videoUrlEncode = next.getVideoUrlEncode();
                        if (!TextUtils.isEmpty(videoUrlEncode) && str2.equals(videoUrlEncode)) {
                            cVar.a.remove(next);
                            b(next);
                            break;
                        }
                        String str3 = next.getendcard_url();
                        if (!TextUtils.isEmpty(str3) && str2.equals(str3)) {
                            cVar.a.remove(next);
                            b(next);
                            break;
                        }
                        CampaignEx.c rewardTemplateMode = next.getRewardTemplateMode();
                        if (rewardTemplateMode != null) {
                            List<CampaignEx.c.a> e2 = rewardTemplateMode.e();
                            if (e2 != null) {
                                Iterator<CampaignEx.c.a> it2 = e2.iterator();
                                while (true) {
                                    if (it2.hasNext()) {
                                        CampaignEx.c.a next2 = it2.next();
                                        if (next2 != null && next2.b != null && next2.b.contains(str2)) {
                                            cVar.a.remove(next);
                                            b(next);
                                            break;
                                        }
                                    } else {
                                        break;
                                    }
                                }
                            }
                            String d2 = rewardTemplateMode.d();
                            if (!TextUtils.isEmpty(d2) && str2.equals(d2)) {
                                cVar.a.remove(next);
                                b(next);
                                break;
                            }
                        } else {
                            continue;
                        }
                    }
                }
                if (cVar.k != null && cVar.a != null && cVar.a.size() == 0) {
                    if (cVar.s != null) {
                        cVar.s.removeMessages(5);
                    }
                    com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "====del campaign and callback failed");
                    cVar.k.a(str);
                }
            } else if (cVar.k != null) {
                if (cVar.s != null) {
                    cVar.s.removeMessages(5);
                }
                com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "====del campaign and callback failed");
                cVar.k.a(str);
            }
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
        }
    }

    static /* synthetic */ void a(c cVar, final CampaignUnit campaignUnit) {
        final List<CampaignEx> a2 = cVar.a(campaignUnit);
        if (a2.size() > 0) {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onload load成功 size:" + a2.size());
            cVar.h();
        } else {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onload load失败 返回的compaign 没有带视频素材");
            cVar.b("No video campaign");
        }
        if (campaignUnit != null) {
            String sessionId = campaignUnit.getSessionId();
            if (s.b(sessionId)) {
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onload sessionId:" + sessionId);
                com.mintegral.msdk.reward.b.a.a = sessionId;
            }
        }
        try {
            if (a2.size() > 0) {
                cVar.d += a2.size();
            }
            if (cVar.l == null || cVar.d > cVar.l.D()) {
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onload 重置offset为0");
                cVar.d = 0;
            }
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onload 算出 下次的offset是:" + cVar.d);
            if (s.b(cVar.c)) {
                com.mintegral.msdk.reward.b.a.a(cVar.c, cVar.d);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        new Thread(new Runnable() {
            public final void run() {
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "在子线程处理业务逻辑 开始");
                if (a2 != null && a2.size() > 0) {
                    com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onload 把广告存在本地 size:" + a2.size());
                    String d = c.this.c;
                    List list = a2;
                    if (com.mintegral.msdk.videocommon.a.a.a() != null) {
                        com.mintegral.msdk.videocommon.a.a.a().a(d, list);
                    }
                }
                m.a(com.mintegral.msdk.base.b.i.a(c.this.b)).d();
                if (!(campaignUnit == null || campaignUnit.getAds() == null || campaignUnit.getAds().size() <= 0)) {
                    c.a(c.this, campaignUnit.getAds());
                }
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "在子线程处理业务逻辑 完成");
            }
        }).start();
        cVar.e(a2);
        cVar.b(a2);
        cVar.c(a2);
        cVar.a(a2);
        cVar.d(a2);
        com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "adapter 905");
        if (cVar.g() && cVar.k != null) {
            cVar.k.a();
        }
    }

    static /* synthetic */ void a(String str, CampaignEx campaignEx, String str2) {
        try {
            w a2 = w.a(com.mintegral.msdk.base.b.i.a(com.mintegral.msdk.base.controller.a.d().h()));
            if (campaignEx == null) {
                com.mintegral.msdk.base.utils.g.a("RewardMVVideoAdapter", "campaign is null");
                return;
            }
            q qVar = new q();
            qVar.n("2000044");
            qVar.b(com.mintegral.msdk.base.utils.c.s(com.mintegral.msdk.base.controller.a.d().h()));
            qVar.m(campaignEx.getId());
            qVar.d(campaignEx.getImageUrl());
            qVar.k(campaignEx.getRequestIdNotice());
            qVar.l(str);
            qVar.o(str2);
            a2.a(qVar);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void a(c cVar, List list) {
        com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onload 开始 更新本机已安装广告列表");
        if (cVar.b == null || list == null || list.size() == 0) {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onload 列表为空 不做更新本机已安装广告列表");
            return;
        }
        m a2 = m.a(com.mintegral.msdk.base.b.i.a(cVar.b));
        boolean z = false;
        for (int i2 = 0; i2 < list.size(); i2++) {
            CampaignEx campaignEx = (CampaignEx) list.get(i2);
            if (campaignEx != null) {
                if (k.a(cVar.b, campaignEx.getPackageName())) {
                    if (com.mintegral.msdk.base.controller.a.c() != null) {
                        com.mintegral.msdk.base.controller.a.c().add(new com.mintegral.msdk.base.entity.h(campaignEx.getId(), campaignEx.getPackageName()));
                        z = true;
                    }
                } else if (a2 != null && !a2.a(campaignEx.getId())) {
                    com.mintegral.msdk.base.entity.g gVar = new com.mintegral.msdk.base.entity.g();
                    gVar.a(campaignEx.getId());
                    gVar.a(campaignEx.getFca());
                    gVar.b(campaignEx.getFcb());
                    gVar.g();
                    gVar.e();
                    gVar.a(System.currentTimeMillis());
                    a2.a(gVar);
                }
            }
        }
        if (z) {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "更新安装列表");
            com.mintegral.msdk.base.controller.a.d().f();
        }
    }
}
