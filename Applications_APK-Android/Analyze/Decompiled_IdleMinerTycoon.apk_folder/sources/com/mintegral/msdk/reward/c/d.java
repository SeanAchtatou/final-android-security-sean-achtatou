package com.mintegral.msdk.reward.c;

import com.mintegral.msdk.video.js.activity.AbstractActivity;

/* compiled from: ReportActivityErrorListener */
public final class d extends AbstractActivity.a.C0068a {
    private com.mintegral.msdk.reward.a.d a;

    public d(com.mintegral.msdk.reward.a.d dVar) {
        this.a = dVar;
    }

    public final void a(String str) {
        super.a(str);
        if (this.a != null) {
            this.a.a(str);
        }
    }
}
