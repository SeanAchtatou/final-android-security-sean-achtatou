package com.mintegral.msdk.reward.player;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.iab.omid.library.mintegral.adsession.AdEvents;
import com.iab.omid.library.mintegral.adsession.AdSession;
import com.iab.omid.library.mintegral.adsession.video.Position;
import com.iab.omid.library.mintegral.adsession.video.VastProperties;
import com.iab.omid.library.mintegral.adsession.video.VideoEvents;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.q;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.p;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.mtgjscommon.mraid.d;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.out.Campaign;
import com.mintegral.msdk.video.js.a.b;
import com.mintegral.msdk.video.js.a.h;
import com.mintegral.msdk.video.js.activity.VideoWebViewActivity;
import com.mintegral.msdk.video.module.MintegralContainerView;
import com.mintegral.msdk.video.module.MintegralVideoView;
import com.mintegral.msdk.video.module.a.a.f;
import com.mintegral.msdk.video.module.a.a.m;
import com.mintegral.msdk.video.module.a.a.n;
import com.mintegral.msdk.videocommon.a;
import java.io.File;
import org.json.JSONException;
import org.json.JSONObject;

public class MTGRewardVideoActivity extends VideoWebViewActivity implements com.mintegral.msdk.mtgjscommon.mraid.b {
    public static final String INTENT_ISBID = "isBid";
    public static final String INTENT_ISIV = "isIV";
    public static final String INTENT_MUTE = "mute";
    public static final String INTENT_REWARD = "reward";
    public static final String INTENT_UNITID = "unitId";
    public static final String INTENT_USERID = "userId";
    /* access modifiers changed from: private */
    public View a;
    /* access modifiers changed from: private */
    public String b;
    private String c;
    private com.mintegral.msdk.videocommon.b.d d;
    private int e = 2;
    /* access modifiers changed from: private */
    public CampaignEx f;
    private com.mintegral.msdk.videocommon.e.c g;
    private com.mintegral.msdk.videocommon.download.a h;
    /* access modifiers changed from: private */
    public com.mintegral.msdk.reward.a.d i;
    /* access modifiers changed from: private */
    public boolean j = false;
    private boolean k = false;
    private boolean l = false;
    private boolean m;
    private boolean n = false;
    /* access modifiers changed from: private */
    public boolean o = false;
    /* access modifiers changed from: private */
    public boolean p = false;
    /* access modifiers changed from: private */
    public boolean q = false;
    /* access modifiers changed from: private */
    public boolean r = false;
    private com.mintegral.msdk.mtgjscommon.mraid.d s;
    private AdSession t = null;
    /* access modifiers changed from: private */
    public VideoEvents u = null;
    /* access modifiers changed from: private */
    public Runnable v = new Runnable() {
        public final void run() {
            if (MTGRewardVideoActivity.this.a != null) {
                MTGRewardVideoActivity.this.a.setVisibility(8);
            }
        }
    };

    public void expand(String str, boolean z) {
    }

    public VideoWebViewActivity.a getH5Templete() {
        return null;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            try {
                this.k = bundle.getBoolean("hasRelease");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        int a2 = p.a(getApplicationContext(), "mintegral_reward_activity_open", "anim");
        int a3 = p.a(getApplicationContext(), "mintegral_reward_activity_stay", "anim");
        if (a2 > 1 && a3 > 1) {
            overridePendingTransition(a2, a3);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("hasRelease", this.k);
        super.onSaveInstanceState(bundle);
    }

    public void onPause() {
        super.onPause();
        this.l = true;
        try {
            getJSVideoModule().videoOperate(2);
        } catch (Throwable th) {
            g.c("AbstractJSActivity", th.getMessage(), th);
        }
    }

    public void onResume() {
        super.onResume();
        try {
            if (this.l && !isShowingAlertView()) {
                getJSVideoModule().videoOperate(1);
            }
            k.a(getWindow().getDecorView());
            if (this.n && this.o) {
                finish();
            }
        } catch (Throwable th) {
            g.c("AbstractJSActivity", th.getMessage(), th);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (!this.k) {
            b();
        }
        if (!this.m) {
            a();
        }
        if (this.s != null) {
            this.s.d();
        }
        if (this.t != null) {
            this.t.removeAllFriendlyObstructions();
            this.t.finish();
            this.t = null;
        }
    }

    public void finish() {
        super.finish();
        if (!this.k) {
            b();
        }
        if (!this.m) {
            a();
        }
    }

    private void a() {
        com.mintegral.msdk.base.common.e.b bVar = new com.mintegral.msdk.base.common.e.b(getApplicationContext());
        if (this.f != null) {
            bVar.a(this.f.getRequestIdNotice(), this.f.getId(), this.unitId, com.mintegral.msdk.mtgjscommon.mraid.c.a(this.f.getId()), this.f.isBidCampaign());
            com.mintegral.msdk.mtgjscommon.mraid.c.b(this.f.getId());
            this.m = true;
        }
    }

    public boolean checkEnv(Intent intent) {
        CampaignEx.c rewardTemplateMode;
        this.b = intent.getStringExtra("unitId");
        this.unitId = this.b;
        this.c = intent.getStringExtra("userId");
        this.e = intent.getIntExtra("mute", 2);
        boolean z = false;
        this.mIsIV = intent.getBooleanExtra(INTENT_ISIV, false);
        this.isBidCampaign = intent.getBooleanExtra(INTENT_ISBID, false);
        String stringExtra = intent.getStringExtra(INTENT_REWARD);
        if (TextUtils.isEmpty(this.b)) {
            return false;
        }
        com.mintegral.msdk.videocommon.e.b.a();
        this.g = com.mintegral.msdk.videocommon.e.b.a(com.mintegral.msdk.base.controller.a.d().j(), this.b);
        if (this.g == null) {
            com.mintegral.msdk.videocommon.e.b.a();
            this.g = com.mintegral.msdk.videocommon.e.b.a(com.mintegral.msdk.base.controller.a.d().j(), this.b, this.mIsIV);
        }
        this.h = com.mintegral.msdk.videocommon.download.c.getInstance().a(this.b, this.isBidCampaign);
        if (this.h != null) {
            this.f = this.h.k();
            this.h.a(true);
            this.h.b(false);
        }
        this.d = com.mintegral.msdk.videocommon.b.d.a(stringExtra);
        this.i = com.mintegral.msdk.reward.b.a.b.get(this.b);
        if (this.h == null || this.f == null || this.d == null) {
            return false;
        }
        this.i = new com.mintegral.msdk.reward.c.b(this, this.mIsIV, this.g, this.f, this.i, this.b);
        registerErrorListener(new com.mintegral.msdk.reward.c.d(this.i));
        com.mintegral.msdk.videocommon.e.c cVar = this.g;
        CampaignEx campaignEx = this.f;
        if (getH5Orientation() != 1) {
            if (!(campaignEx == null || (rewardTemplateMode = campaignEx.getRewardTemplateMode()) == null)) {
                z = a(rewardTemplateMode.b());
            }
            if (!z && cVar != null) {
                a(this.g.a());
            }
        }
        setShowingTransparent();
        return true;
    }

    public void setShowingTransparent() {
        int a2;
        this.n = getIsShowingTransparent();
        if (!this.n && (a2 = p.a(getApplicationContext(), "mintegral_reward_theme", "style")) > 1) {
            setTheme(a2);
        }
    }

    /* access modifiers changed from: protected */
    public void loadVideoData() {
        registerJsFactory(new com.mintegral.msdk.video.js.factory.b(this, this.windVaneWebView, this.mintegralVideoView, this.mintegralContainerView, getCampaignEx()));
        WindVaneWebView windVaneWebView = this.windVaneWebView;
        com.mintegral.msdk.videocommon.download.c.getInstance().a(true);
        getJSCommon().a(this.e);
        getJSCommon().a(this.b);
        getJSCommon().a(this.g);
        getJSCommon().a(new c(this, (byte) 0));
        if (this.f != null && (this.f.isMraid() || this.f.isActiveOm())) {
            this.s = new com.mintegral.msdk.mtgjscommon.mraid.d(this);
            this.s.c();
            this.s.a();
            this.s.a(new d.b() {
                public final void a(double d) {
                    g.d("AbstractJSActivity", "volume is : " + d);
                    try {
                        if (!(!MTGRewardVideoActivity.this.f.isMraid() || MTGRewardVideoActivity.this.mintegralContainerView == null || MTGRewardVideoActivity.this.mintegralContainerView.getH5EndCardView() == null)) {
                            MTGRewardVideoActivity.this.mintegralContainerView.getH5EndCardView().volumeChange(d);
                        }
                        if (MTGRewardVideoActivity.this.f.isActiveOm() && MTGRewardVideoActivity.this.u != null && MTGRewardVideoActivity.this.p && MTGRewardVideoActivity.this.mintegralVideoView != null && MTGRewardVideoActivity.this.mintegralVideoView.getMute() == 2) {
                            MTGRewardVideoActivity.this.u.volumeChange((float) d);
                        }
                    } catch (Exception e) {
                        g.d("AbstractJSActivity", e.getMessage());
                    }
                }
            });
        }
        if (windVaneWebView != null) {
            ViewGroup viewGroup = (ViewGroup) findViewById(p.a(getApplicationContext(), "mintegral_video_templete_webview_parent", "id"));
            windVaneWebView.setApiManagerContext(this);
            if (windVaneWebView.getParent() != null) {
                defaultLoad(0, "preload template webview is null or load error");
                return;
            }
            if (windVaneWebView.getObject() instanceof h) {
                getJSContainerModule().readyStatus(((h) windVaneWebView.getObject()).l());
                super.loadVideoData();
                ((com.mintegral.msdk.video.js.a.b) getJSCommon()).j.a();
            }
            viewGroup.addView(windVaneWebView, new ViewGroup.LayoutParams(-1, -1));
            return;
        }
        defaultLoad(0, "preload template webview is null or load error");
    }

    public void loadModuleDatas() {
        int h5MuteState = getH5MuteState();
        if (h5MuteState != 0) {
            this.e = h5MuteState;
        }
        int f2 = this.g.f();
        int h5CloseType = getH5CloseType();
        int i2 = h5CloseType != 0 ? h5CloseType : f2;
        if (this.f != null) {
            this.t = com.mintegral.msdk.b.b.a(this, false, this.f.getOmid(), this.f.getRequestIdNotice(), this.f.getId(), this.b);
        }
        this.mintegralVideoView.setSoundState(this.e);
        this.mintegralVideoView.setCampaign(this.f);
        this.mintegralVideoView.setPlayURL(c());
        this.mintegralVideoView.setVideoSkipTime(this.g.e());
        this.mintegralVideoView.setCloseAlert(this.g.k());
        this.mintegralVideoView.setBufferTimeout(d());
        this.mintegralVideoView.setNotifyListener(new n(this.jsFactory, this.f, this.d, this.h, this.b, i2, this.g.e(), new d(this, (byte) 0), this.g.L()));
        this.mintegralVideoView.setShowingTransparent(this.n);
        this.mintegralVideoView.setAdSession(this.t);
        this.mintegralContainerView.setCampaign(this.f);
        this.mintegralContainerView.setUnitID(this.b);
        this.mintegralContainerView.setCloseDelayTime(this.g.m());
        this.mintegralContainerView.setPlayCloseBtnTm(this.g.j());
        this.mintegralContainerView.setVideoInteractiveType(this.g.h());
        this.mintegralContainerView.setEndscreenType(this.g.o());
        this.mintegralContainerView.setVideoSkipTime(this.g.e());
        this.mintegralContainerView.setShowingTransparent(this.n);
        if (this.f.getPlayable_ads_without_video() == 2) {
            this.mintegralContainerView.setNotifyListener(new com.mintegral.msdk.video.module.a.a.h(this.f, this.h, this.d, this.b, new b(this, (byte) 0)));
            this.mintegralContainerView.preLoadData();
            this.mintegralContainerView.showPlayableView();
        } else {
            this.mintegralContainerView.setNotifyListener(new com.mintegral.msdk.video.module.a.a.c(this.jsFactory, this.f, this.d, this.h, this.b, new a(this, this.f)));
            this.mintegralContainerView.preLoadData();
            this.mintegralVideoView.preLoadData();
        }
        if (this.n) {
            this.mintegralContainerView.setMintegralClickMiniCardViewTransparent();
        }
        if (this.t != null) {
            this.t.addFriendlyObstruction(this.mintegralContainerView);
            if (this.a != null) {
                this.t.addFriendlyObstruction(this.a);
            }
            if (this.windVaneWebView != null) {
                this.t.addFriendlyObstruction(this.windVaneWebView);
            }
            AdEvents createAdEvents = AdEvents.createAdEvents(this.t);
            this.u = VideoEvents.createVideoEvents(this.t);
            this.t.start();
            this.u.loaded(VastProperties.createVastPropertiesForNonSkippableVideo(true, Position.STANDALONE));
            this.mintegralVideoView.setVideoEvents(this.u);
            if (createAdEvents != null) {
                try {
                    createAdEvents.impressionOccurred();
                } catch (Exception e2) {
                    g.a("omsdk", e2.getMessage());
                }
            }
        }
    }

    public void receiveSuccess() {
        super.receiveSuccess();
        g.a("AbstractJSActivity", "receiveSuccess ,start hybrid");
        this.mHandler.removeCallbacks(this.jsbridgeConnectTimeout);
        this.mHandler.postDelayed(this.v, 250);
    }

    public void defaultLoad(int i2, String str) {
        super.defaultLoad(i2, str);
        g.a("AbstractJSActivity", "hybrid load error ,start defaultLoad,desc:" + str);
        if (!isLoadSuccess()) {
            a(i2, str);
            finish();
        } else if (this.f.getPlayable_ads_without_video() == 2) {
            this.mintegralContainerView.setCampaign(this.f);
            this.mintegralContainerView.setUnitID(this.b);
            this.mintegralContainerView.setCloseDelayTime(this.g.m());
            this.mintegralContainerView.setPlayCloseBtnTm(this.g.j());
            this.mintegralContainerView.setNotifyListener(new com.mintegral.msdk.video.module.a.a.h(this.f, this.h, this.d, this.b, new b(this, (byte) 0)));
            this.mintegralContainerView.preLoadData();
            this.mintegralContainerView.showPlayableView();
        } else {
            a(i2, str);
            this.a.setVisibility(8);
            loadModuleDatas();
            int f2 = this.g.f();
            int h5CloseType = getH5CloseType();
            this.mintegralVideoView.setNotifyListener(new m(this.mintegralVideoView, this.mintegralContainerView, this.f, this.d, this.h, this.b, h5CloseType != 0 ? h5CloseType : f2, this.g.e(), new d(this, (byte) 0), this.g.L()));
            this.mintegralVideoView.defaultShow();
            this.mintegralContainerView.setNotifyListener(new com.mintegral.msdk.video.module.a.a.b(this.mintegralVideoView, this.mintegralContainerView, this.f, this.d, this.h, this.b, new a(this, this.f)));
            this.mintegralContainerView.defaultShow();
        }
    }

    public int getLayoutID() {
        return findLayout(this.n ? "mintegral_reward_activity_video_templete_transparent" : "mintegral_reward_activity_video_templete");
    }

    public WindVaneWebView findWindVaneWebView() {
        a.C0069a aVar;
        try {
            if (this.mIsIV) {
                aVar = com.mintegral.msdk.videocommon.a.a(287, this.f);
            } else {
                aVar = com.mintegral.msdk.videocommon.a.a(94, this.f);
            }
            if (aVar == null || !aVar.b()) {
                return null;
            }
            if (this.mIsIV) {
                com.mintegral.msdk.videocommon.a.b(287, this.f);
            } else {
                com.mintegral.msdk.videocommon.a.b(94, this.f);
            }
            WindVaneWebView a2 = aVar.a();
            if (this.n) {
                a2.setWebViewTransparent();
            }
            return a2;
        } catch (Exception e2) {
            if (!MIntegralConstans.DEBUG) {
                return null;
            }
            e2.printStackTrace();
            return null;
        }
    }

    public MintegralVideoView findMintegralVideoView() {
        return (MintegralVideoView) findViewById(findID("mintegral_video_templete_videoview"));
    }

    public MintegralContainerView findMintegralContainerView() {
        return (MintegralContainerView) findViewById(findID("mintegral_video_templete_container"));
    }

    public boolean initViews() {
        this.a = findViewById(findID("mintegral_video_templete_progressbar"));
        return this.a != null;
    }

    public CampaignEx getCampaignEx() {
        return this.f;
    }

    public boolean canBackPress() {
        return this.mintegralContainerView == null || this.mintegralContainerView.canBackPress();
    }

    public void onBackPressed() {
        if (this.n && this.mintegralVideoView != null) {
            this.mintegralVideoView.notifyVideoClose();
        } else if (!this.p || this.mintegralVideoView == null) {
            if (this.r && this.mintegralContainerView != null) {
                this.mintegralContainerView.onPlayableBackPress();
            } else if (this.q && this.mintegralContainerView != null) {
                this.mintegralContainerView.onEndcardBackPress();
            }
        } else if (!this.mintegralVideoView.isMiniCardShowing()) {
            this.mintegralVideoView.onBackPress();
        } else if (this.mintegralContainerView != null) {
            this.mintegralContainerView.onMiniEndcardBackPress();
        }
    }

    private void b() {
        try {
            this.k = true;
            if (this.i != null) {
                this.i.a(this.j, this.d);
            }
            this.mHandler.removeCallbacks(this.v);
            com.mintegral.msdk.reward.b.a.a(this.mIsIV, this.isBidCampaign);
            if (!this.mIsIV && this.j && (this.i == null || !this.i.b())) {
                g.a("AbstractJSActivity", "sendToServerRewardInfo");
                com.mintegral.msdk.video.module.b.a.a(this.f, this.d, this.b, this.c);
            }
            if (this.mIsIV) {
                com.mintegral.msdk.videocommon.a.b(287, this.f);
            } else {
                com.mintegral.msdk.videocommon.a.b(94, this.f);
            }
            if (this.mintegralContainerView != null) {
                this.mintegralContainerView.release();
            }
        } catch (Throwable th) {
            g.c("AbstractJSActivity", th.getMessage(), th);
        }
    }

    private void a(int i2, String str) {
        try {
            q qVar = new q();
            qVar.n("2000037");
            qVar.j("code=" + i2 + ",desc=" + str);
            String str2 = "";
            if (!(this.f == null || this.f.getRewardTemplateMode() == null)) {
                str2 = this.f.getRewardTemplateMode().d();
            }
            qVar.i(str2);
            qVar.l(this.b);
            String str3 = "";
            if (this.f != null) {
                str3 = this.f.getId();
            }
            qVar.m(str3);
            if (this.f != null && !TextUtils.isEmpty(this.f.getRequestIdNotice())) {
                qVar.k(this.f.getRequestIdNotice());
            }
            int s2 = com.mintegral.msdk.base.utils.c.s(getApplicationContext());
            qVar.b(s2);
            qVar.q(com.mintegral.msdk.base.utils.c.a(getApplicationContext(), s2));
            com.mintegral.msdk.video.module.b.a.a(q.f(qVar), this.b);
        } catch (Throwable th) {
            g.c("AbstractJSActivity", th.getMessage(), th);
        }
    }

    private String c() {
        String videoUrlEncode = this.f.getVideoUrlEncode();
        try {
            if (this.h.h() != 5) {
                return videoUrlEncode;
            }
            String c2 = this.h.c();
            if (s.a(c2) || !new File(c2).exists()) {
                return videoUrlEncode;
            }
            return c2;
        } catch (Throwable th) {
            g.c("AbstractJSActivity", th.getMessage(), th);
            return videoUrlEncode;
        }
    }

    private boolean a(int i2) {
        switch (i2) {
            case 1:
                setRequestedOrientation(1);
                break;
            case 2:
                try {
                    setRequestedOrientation(0);
                    break;
                } catch (Throwable th) {
                    g.c("AbstractJSActivity", th.getMessage(), th);
                    return false;
                }
            default:
                return false;
        }
        return true;
    }

    private static int d() {
        int i2 = 5;
        try {
            com.mintegral.msdk.videocommon.e.b.a();
            com.mintegral.msdk.videocommon.e.a b2 = com.mintegral.msdk.videocommon.e.b.b();
            if (b2 == null) {
                com.mintegral.msdk.videocommon.e.b.a();
                com.mintegral.msdk.videocommon.e.b.c();
            }
            if (b2 != null) {
                i2 = (int) b2.i();
            }
            g.b("AbstractJSActivity", "MintegralBaseView buffetTimeout:" + i2);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return i2;
    }

    private final class d extends f {
        private d() {
        }

        /* synthetic */ d(MTGRewardVideoActivity mTGRewardVideoActivity, byte b) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.mintegral.msdk.reward.player.MTGRewardVideoActivity.a(com.mintegral.msdk.reward.player.MTGRewardVideoActivity, boolean):boolean
         arg types: [com.mintegral.msdk.reward.player.MTGRewardVideoActivity, int]
         candidates:
          com.mintegral.msdk.reward.player.MTGRewardVideoActivity.a(int, java.lang.String):void
          com.mintegral.msdk.video.js.activity.VideoWebViewActivity.a(com.mintegral.msdk.video.js.activity.VideoWebViewActivity, int):int
          com.mintegral.msdk.reward.player.MTGRewardVideoActivity.a(com.mintegral.msdk.reward.player.MTGRewardVideoActivity, boolean):boolean */
        public final void a(int i, Object obj) {
            super.a(i, obj);
            if (i != 2) {
                if (i != 121) {
                    switch (i) {
                        case 10:
                            boolean unused = MTGRewardVideoActivity.this.p = true;
                            MTGRewardVideoActivity.this.i.a();
                            com.mintegral.msdk.reward.d.a.b(MTGRewardVideoActivity.this, MTGRewardVideoActivity.this.f, MTGRewardVideoActivity.this.b);
                            return;
                        case 11:
                        case 12:
                            break;
                        default:
                            switch (i) {
                                case 16:
                                    MTGRewardVideoActivity.this.getJSCommon().d();
                                    return;
                                case 17:
                                    boolean unused2 = MTGRewardVideoActivity.this.j = true;
                                    return;
                                default:
                                    return;
                            }
                    }
                } else {
                    MTGRewardVideoActivity.this.i.b(MTGRewardVideoActivity.this.b);
                    boolean unused3 = MTGRewardVideoActivity.this.p = false;
                    return;
                }
            }
            if (i == 12) {
                MTGRewardVideoActivity.this.i.a("play error");
                com.mintegral.msdk.reward.d.a.a(MTGRewardVideoActivity.this, MTGRewardVideoActivity.this.f, MTGRewardVideoActivity.this.b, "play error");
            }
            boolean unused4 = MTGRewardVideoActivity.this.p = false;
            if (MTGRewardVideoActivity.this.u != null && i == 2) {
                MTGRewardVideoActivity.this.u.skipped();
            }
        }
    }

    private final class a extends com.mintegral.msdk.video.module.a.a.a {
        public a(Activity activity, CampaignEx campaignEx) {
            super(activity, campaignEx);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.mintegral.msdk.reward.a.d.a(boolean, java.lang.String):void
         arg types: [int, java.lang.String]
         candidates:
          com.mintegral.msdk.reward.a.d.a(boolean, com.mintegral.msdk.videocommon.b.d):void
          com.mintegral.msdk.reward.a.d.a(boolean, java.lang.String):void */
        public final void a(int i, Object obj) {
            boolean unused = MTGRewardVideoActivity.this.q = true;
            if (i != 108) {
                if (i != 113) {
                    if (i != 117) {
                        switch (i) {
                            case 105:
                                MTGRewardVideoActivity.this.getJSCommon().b(1, obj != null ? obj.toString() : "");
                                break;
                        }
                    } else {
                        MTGRewardVideoActivity.this.i.c(MTGRewardVideoActivity.this.b);
                    }
                }
                MTGRewardVideoActivity.this.i.a(true, MTGRewardVideoActivity.this.b);
            } else {
                MTGRewardVideoActivity.this.getJSCommon().a(new b.C0067b(MTGRewardVideoActivity.this.getJSCommon(), new c(MTGRewardVideoActivity.this, (byte) 0)));
                MTGRewardVideoActivity.this.getJSCommon().b(1, obj != null ? obj.toString() : "");
            }
            super.a(i, obj);
        }
    }

    private final class b extends f {
        private b() {
        }

        /* synthetic */ b(MTGRewardVideoActivity mTGRewardVideoActivity, byte b) {
            this();
        }

        public final void a(int i, Object obj) {
            super.a(i, obj);
            if (i != 120) {
                switch (i) {
                    case 100:
                        boolean unused = MTGRewardVideoActivity.this.r = true;
                        MTGRewardVideoActivity.this.mHandler.postDelayed(MTGRewardVideoActivity.this.v, 250);
                        MTGRewardVideoActivity.this.i.a();
                        return;
                    case 101:
                    case 102:
                        MTGRewardVideoActivity.this.getJSCommon().d();
                        return;
                    case 103:
                        boolean unused2 = MTGRewardVideoActivity.this.j = true;
                        MTGRewardVideoActivity.this.getJSCommon().d();
                        return;
                    default:
                        return;
                }
            } else {
                MTGRewardVideoActivity.this.i.c(MTGRewardVideoActivity.this.b);
            }
        }
    }

    private class c extends b.a {
        private c() {
        }

        /* synthetic */ c(MTGRewardVideoActivity mTGRewardVideoActivity, byte b) {
            this();
        }

        public final void a() {
            super.a();
            MTGRewardVideoActivity.this.receiveSuccess();
        }

        public final void onStartRedirection(Campaign campaign, String str) {
            super.onStartRedirection(campaign, str);
            MTGRewardVideoActivity.q(MTGRewardVideoActivity.this);
        }

        public final void onFinishRedirection(Campaign campaign, String str) {
            super.onFinishRedirection(campaign, str);
            boolean unused = MTGRewardVideoActivity.this.o = true;
            MTGRewardVideoActivity.s(MTGRewardVideoActivity.this);
            if (campaign != null && (campaign instanceof CampaignEx)) {
                try {
                    CampaignEx campaignEx = (CampaignEx) campaign;
                    String optString = new JSONObject(MTGRewardVideoActivity.this.getJSVideoModule().getCurrentProgress()).optString(NotificationCompat.CATEGORY_PROGRESS, "");
                    if (campaignEx.getLinkType() == 3 && campaignEx.getEndcard_click_result() == 2 && optString.equals("1.0")) {
                        MTGRewardVideoActivity.this.finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        public final void onRedirectionFailed(Campaign campaign, String str) {
            super.onRedirectionFailed(campaign, str);
            MTGRewardVideoActivity.s(MTGRewardVideoActivity.this);
            boolean unused = MTGRewardVideoActivity.this.o = true;
        }

        public final void a(boolean z) {
            super.a(z);
            MTGRewardVideoActivity.this.i.a(z, MTGRewardVideoActivity.this.b);
        }

        public final void a(int i, String str) {
            super.a(i, str);
            MTGRewardVideoActivity.this.defaultLoad(i, str);
        }

        public final void b() {
            super.b();
            if (MTGRewardVideoActivity.this.mHandler != null) {
                MTGRewardVideoActivity.this.mHandler.removeCallbacks(MTGRewardVideoActivity.this.receiveRunnable);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mintegral.msdk.reward.a.d.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.mintegral.msdk.reward.a.d.a(boolean, com.mintegral.msdk.videocommon.b.d):void
      com.mintegral.msdk.reward.a.d.a(boolean, java.lang.String):void */
    public void open(String str) {
        try {
            String clickURL = this.f.getClickURL();
            if (!TextUtils.isEmpty(str)) {
                this.f.setClickURL(str);
                CampaignEx mraidCampaign = getMraidCampaign();
                if (mraidCampaign != null) {
                    new com.mintegral.msdk.base.common.e.b(getApplicationContext()).b(mraidCampaign.getRequestIdNotice(), mraidCampaign.getId(), this.unitId, str, this.f.isBidCampaign());
                }
            }
            new com.mintegral.msdk.click.a(getApplicationContext(), this.unitId).b(this.f);
            this.f.setClickURL(clickURL);
            if (this.i != null) {
                this.i.a(false, this.b);
            }
        } catch (Exception e2) {
            g.d("AbstractJSActivity", e2.getMessage());
        }
    }

    public void close() {
        try {
            if (this.mintegralContainerView != null && this.mintegralContainerView.getH5EndCardView() != null) {
                this.mintegralContainerView.getH5EndCardView().onCloseViewClick();
            }
        } catch (Exception e2) {
            g.d("AbstractJSActivity", e2.getMessage());
        }
    }

    public void unload() {
        close();
    }

    public CampaignEx getMraidCampaign() {
        return this.f;
    }

    public void useCustomClose(boolean z) {
        try {
            if (this.mintegralContainerView != null && this.mintegralContainerView.getH5EndCardView() != null) {
                this.mintegralContainerView.getH5EndCardView().setCloseVisibleForMraid(z ? 4 : 0);
            }
        } catch (Exception e2) {
            g.d("AbstractJSActivity", e2.getMessage());
        }
    }

    static /* synthetic */ void q(MTGRewardVideoActivity mTGRewardVideoActivity) {
        if (mTGRewardVideoActivity.isLoadSuccess()) {
            mTGRewardVideoActivity.runOnUiThread(new Runnable() {
                public final void run() {
                    MTGRewardVideoActivity.this.a.setBackgroundColor(0);
                    MTGRewardVideoActivity.this.a.setVisibility(0);
                    MTGRewardVideoActivity.this.a.bringToFront();
                }
            });
        }
    }

    static /* synthetic */ void s(MTGRewardVideoActivity mTGRewardVideoActivity) {
        if (mTGRewardVideoActivity.isLoadSuccess()) {
            mTGRewardVideoActivity.runOnUiThread(new Runnable() {
                public final void run() {
                    MTGRewardVideoActivity.this.a.setVisibility(8);
                }
            });
        }
    }
}
