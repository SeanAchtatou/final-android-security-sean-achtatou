package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.AbstractSmash;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONObject;

public class ProgRvSmash extends ProgSmash implements RewardedVideoSmashListener {
    private static final int errorCode_adClosed = 5009;
    private static final int errorCode_biddingDataException = 5001;
    private static final int errorCode_initFailed = 5008;
    private static final int errorCode_initSuccess = 5007;
    private static final int errorCode_isReadyException = 5002;
    private static final int errorCode_loadException = 5005;
    private static final int errorCode_loadInProgress = 5003;
    private static final int errorCode_showFailed = 5006;
    private static final int errorCode_showInProgress = 5004;
    private Activity mActivity;
    private String mAppKey;
    private String mAuctionFallback;
    private String mAuctionFallbackToLoad;
    private String mAuctionIdToLoad;
    private String mAuctionServerDataToLoad;
    private int mAuctionTrial;
    private int mAuctionTrialToLoad;
    /* access modifiers changed from: private */
    public String mCurrentAuctionId;
    private Placement mCurrentPlacement;
    private boolean mIsShowCandidate;
    /* access modifiers changed from: private */
    public ProgRvManagerListener mListener;
    private long mLoadStartTime;
    private int mLoadTimeoutSecs;
    private int mSessionDepth;
    private int mSessionDepthToLoad;
    private boolean mShouldLoadAfterClose;
    private boolean mShouldLoadAfterLoad;
    /* access modifiers changed from: private */
    public SMASH_STATE mState = SMASH_STATE.NO_INIT;
    /* access modifiers changed from: private */
    public final Object mStateLock = new Object();
    private Timer mTimer;
    private final Object mTimerLock = new Object();
    private String mUserId;

    protected enum SMASH_STATE {
        NO_INIT,
        INIT_IN_PROGRESS,
        NOT_LOADED,
        LOAD_IN_PROGRESS,
        LOADED,
        SHOW_IN_PROGRESS
    }

    private boolean shouldAddAuctionParams(int i) {
        return i == 1001 || i == 1002 || i == 1200 || i == 1005 || i == 1203 || i == 1201 || i == 1202 || i == 1006 || i == 1010;
    }

    public void onRewardedVideoLoadSuccess() {
    }

    public ProgRvSmash(Activity activity, String str, String str2, ProviderSettings providerSettings, ProgRvManagerListener progRvManagerListener, int i, AbstractAdapter abstractAdapter) {
        super(new AdapterConfig(providerSettings, providerSettings.getRewardedVideoSettings()), abstractAdapter);
        this.mActivity = activity;
        this.mAppKey = str;
        this.mUserId = str2;
        this.mListener = progRvManagerListener;
        this.mTimer = null;
        this.mLoadTimeoutSecs = i;
        this.mAdapter.addRewardedVideoListener(this);
        this.mShouldLoadAfterClose = false;
        this.mShouldLoadAfterLoad = false;
        this.mIsShowCandidate = false;
        this.mCurrentPlacement = null;
        this.mCurrentAuctionId = "";
        this.mSessionDepth = 1;
        resetAuctionParams();
    }

    private void resetAuctionParams() {
        this.mAuctionIdToLoad = "";
        this.mAuctionTrialToLoad = -1;
        this.mAuctionFallbackToLoad = "";
        this.mAuctionServerDataToLoad = "";
        this.mSessionDepthToLoad = this.mSessionDepth;
    }

    public boolean isReadyToBid() {
        return (this.mState == SMASH_STATE.NO_INIT || this.mState == SMASH_STATE.INIT_IN_PROGRESS) ? false : true;
    }

    public boolean isLoadingInProgress() {
        return this.mState == SMASH_STATE.INIT_IN_PROGRESS || this.mState == SMASH_STATE.LOAD_IN_PROGRESS;
    }

    public Map<String, Object> getBiddingData() {
        try {
            if (isBidder()) {
                return this.mAdapter.getRvBiddingData(this.mAdUnitSettings);
            }
            return null;
        } catch (Throwable th) {
            logInternalError("getBiddingData exception: " + th.getLocalizedMessage());
            th.printStackTrace();
            sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) errorCode_biddingDataException)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, th.getLocalizedMessage()}});
            return null;
        }
    }

    public void initForBidding() {
        logInternal("initForBidding()");
        setState(SMASH_STATE.INIT_IN_PROGRESS);
        setCustomParams();
        try {
            this.mAdapter.initRvForBidding(this.mActivity, this.mAppKey, this.mUserId, this.mAdUnitSettings, this);
        } catch (Throwable th) {
            logInternalError("initForBidding exception: " + th.getLocalizedMessage());
            th.printStackTrace();
            onRewardedVideoInitFailed(new IronSourceError(IronSourceError.ERROR_RV_INIT_EXCEPTION, th.getLocalizedMessage()));
        }
    }

    public void unloadVideo() {
        if (isBidder()) {
            this.mIsShowCandidate = false;
        }
    }

    public boolean isReadyToShow() {
        try {
            if (!isBidder()) {
                return this.mAdapter.isRewardedVideoAvailable(this.mAdUnitSettings);
            }
            if (!this.mIsShowCandidate || this.mState != SMASH_STATE.LOADED || !this.mAdapter.isRewardedVideoAvailable(this.mAdUnitSettings)) {
                return false;
            }
            return true;
        } catch (Throwable th) {
            logInternalError("isReadyToShow exception: " + th.getLocalizedMessage());
            th.printStackTrace();
            sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) errorCode_isReadyException)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, th.getLocalizedMessage()}});
            return false;
        }
    }

    public void loadVideo(String str, String str2, int i, String str3, int i2) {
        SMASH_STATE smash_state;
        String str4 = str2;
        logInternal("loadVideo() auctionId: " + str2 + " state: " + this.mState);
        setIsLoadCandidate(false);
        this.mIsShowCandidate = true;
        synchronized (this.mStateLock) {
            smash_state = this.mState;
            if (!(this.mState == SMASH_STATE.LOAD_IN_PROGRESS || this.mState == SMASH_STATE.SHOW_IN_PROGRESS)) {
                setState(SMASH_STATE.LOAD_IN_PROGRESS);
            }
        }
        if (smash_state == SMASH_STATE.LOAD_IN_PROGRESS) {
            sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) errorCode_loadInProgress)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "load during load"}});
            this.mShouldLoadAfterLoad = true;
            updateFutureAuctionData(str, str2, i, str3, i2);
            this.mListener.onLoadError(this, str2);
        } else if (smash_state == SMASH_STATE.SHOW_IN_PROGRESS) {
            sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) errorCode_showInProgress)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "load during show"}});
            this.mShouldLoadAfterClose = true;
            updateFutureAuctionData(str, str2, i, str3, i2);
        } else {
            this.mCurrentAuctionId = str4;
            this.mAuctionTrial = i;
            this.mAuctionFallback = str3;
            this.mSessionDepth = i2;
            startLoadTimer();
            this.mLoadStartTime = new Date().getTime();
            sendProviderEvent(1001);
            try {
                if (isBidder()) {
                    String str5 = str;
                    this.mAdapter.loadVideo(this.mAdUnitSettings, this, str);
                } else if (smash_state == SMASH_STATE.NO_INIT) {
                    setCustomParams();
                    this.mAdapter.initRewardedVideo(this.mActivity, this.mAppKey, this.mUserId, this.mAdUnitSettings, this);
                } else {
                    this.mAdapter.fetchRewardedVideo(this.mAdUnitSettings);
                }
            } catch (Throwable th) {
                logInternalError("loadVideo exception: " + th.getLocalizedMessage());
                th.printStackTrace();
                sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) errorCode_loadException)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, th.getLocalizedMessage()}});
            }
        }
    }

    public void reportShowChance(boolean z, int i) {
        this.mSessionDepth = i;
        Object[][] objArr = new Object[1][];
        Object[] objArr2 = new Object[2];
        objArr2[0] = "status";
        objArr2[1] = z ? "true" : "false";
        objArr[0] = objArr2;
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_SHOW_CHANCE, objArr);
    }

    public void showVideo(Placement placement, int i) {
        stopLoadTimer();
        logInternal("showVideo()");
        this.mCurrentPlacement = placement;
        this.mSessionDepth = i;
        setState(SMASH_STATE.SHOW_IN_PROGRESS);
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_SHOW);
        try {
            this.mAdapter.showRewardedVideo(this.mAdUnitSettings, this);
        } catch (Throwable th) {
            logInternalError("showVideo exception: " + th.getLocalizedMessage());
            th.printStackTrace();
            onRewardedVideoAdShowFailed(new IronSourceError(IronSourceError.ERROR_RV_SHOW_EXCEPTION, th.getLocalizedMessage()));
        }
    }

    public void setCappedPerSession() {
        this.mAdapter.setMediationState(AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION, "rewardedvideo");
        sendProviderEvent(IronSourceConstants.RV_CAP_SESSION);
    }

    /* access modifiers changed from: private */
    public void setState(SMASH_STATE smash_state) {
        logInternal("current state=" + this.mState + ", new state=" + smash_state);
        synchronized (this.mStateLock) {
            this.mState = smash_state;
        }
    }

    private void setCustomParams() {
        try {
            Integer age = IronSourceObject.getInstance().getAge();
            if (age != null) {
                this.mAdapter.setAge(age.intValue());
            }
            String gender = IronSourceObject.getInstance().getGender();
            if (!TextUtils.isEmpty(gender)) {
                this.mAdapter.setGender(gender);
            }
            String mediationSegment = IronSourceObject.getInstance().getMediationSegment();
            if (!TextUtils.isEmpty(mediationSegment)) {
                this.mAdapter.setMediationSegment(mediationSegment);
            }
            String pluginType = ConfigFile.getConfigFile().getPluginType();
            if (!TextUtils.isEmpty(pluginType)) {
                this.mAdapter.setPluginData(pluginType, ConfigFile.getConfigFile().getPluginFrameworkVersion());
            }
        } catch (Exception e) {
            logInternal("setCustomParams() " + e.getMessage());
        }
    }

    public void onRewardedVideoAvailabilityChanged(boolean z) {
        boolean z2;
        stopLoadTimer();
        logAdapterCallback("onRewardedVideoAvailabilityChanged available=" + z + " state=" + this.mState.name());
        synchronized (this.mStateLock) {
            if (this.mState == SMASH_STATE.LOAD_IN_PROGRESS) {
                setState(z ? SMASH_STATE.LOADED : SMASH_STATE.NOT_LOADED);
                z2 = false;
            } else {
                z2 = true;
            }
        }
        if (!z2) {
            sendProviderEvent(z ? 1002 : IronSourceConstants.RV_INSTANCE_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(getElapsedTime())}});
            if (this.mShouldLoadAfterLoad) {
                this.mShouldLoadAfterLoad = false;
                logInternal("onRewardedVideoAvailabilityChanged to " + z + "and mShouldLoadAfterLoad is true - calling loadVideo");
                loadVideo(this.mAuctionServerDataToLoad, this.mAuctionIdToLoad, this.mAuctionTrialToLoad, this.mAuctionFallbackToLoad, this.mSessionDepthToLoad);
                resetAuctionParams();
            } else if (z) {
                this.mListener.onLoadSuccess(this, this.mCurrentAuctionId);
            } else {
                this.mListener.onLoadError(this, this.mCurrentAuctionId);
            }
        } else if (z) {
            sendProviderEvent(IronSourceConstants.RV_INSTANCE_AVAILABILITY_TRUE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_EXT1, this.mState.name()}});
        } else {
            sendProviderEvent(IronSourceConstants.RV_INSTANCE_AVAILABILITY_FALSE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) IronSourceError.ERROR_RV_LOAD_UNEXPECTED_CALLBACK)}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(getElapsedTime())}, new Object[]{IronSourceConstants.EVENTS_EXT1, this.mState.name()}});
        }
    }

    public void onRewardedVideoLoadFailed(IronSourceError ironSourceError) {
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED_REASON, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(getElapsedTime())}});
    }

    public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError) {
        logAdapterCallback("onRewardedVideoAdShowFailed error=" + ironSourceError.getErrorMessage());
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}});
        synchronized (this.mStateLock) {
            if (this.mState != SMASH_STATE.SHOW_IN_PROGRESS) {
                Object[] objArr = {IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) errorCode_showFailed)};
                sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{objArr, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "showFailed: " + this.mState}});
                return;
            }
            setState(SMASH_STATE.NOT_LOADED);
            this.mListener.onRewardedVideoAdShowFailed(ironSourceError, this);
        }
    }

    public void onRewardedVideoInitSuccess() {
        logAdapterCallback("onRewardedVideoInitSuccess");
        synchronized (this.mStateLock) {
            if (this.mState != SMASH_STATE.INIT_IN_PROGRESS) {
                Object[] objArr = {IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) errorCode_initSuccess)};
                sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{objArr, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "initSuccess: " + this.mState}});
                return;
            }
            setState(SMASH_STATE.NOT_LOADED);
        }
    }

    public void onRewardedVideoInitFailed(IronSourceError ironSourceError) {
        logAdapterCallback("onRewardedVideoInitFailed error=" + ironSourceError.getErrorMessage());
        stopLoadTimer();
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) IronSourceError.ERROR_RV_LOAD_FAIL_DUE_TO_INIT)}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(getElapsedTime())}});
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED_REASON, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(getElapsedTime())}});
        synchronized (this.mStateLock) {
            if (this.mState != SMASH_STATE.INIT_IN_PROGRESS) {
                Object[] objArr = {IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) errorCode_initFailed)};
                sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{objArr, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "initFailed: " + this.mState}});
                return;
            }
            setState(SMASH_STATE.NO_INIT);
            this.mListener.onLoadError(this, this.mCurrentAuctionId);
        }
    }

    public void onRewardedVideoAdOpened() {
        logAdapterCallback("onRewardedVideoAdOpened");
        this.mListener.onRewardedVideoAdOpened(this);
        sendProviderEventWithPlacement(1005);
    }

    public void onRewardedVideoAdStarted() {
        logAdapterCallback("onRewardedVideoAdStarted");
        this.mListener.onRewardedVideoAdStarted(this);
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_STARTED);
    }

    public void onRewardedVideoAdVisible() {
        logAdapterCallback("onRewardedVideoAdVisible");
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_VISIBLE);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0053, code lost:
        r10.mListener.onRewardedVideoAdClosed(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x005a, code lost:
        if (r10.mShouldLoadAfterClose == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x005c, code lost:
        logInternal("onRewardedVideoAdClosed and mShouldLoadAfterClose is true - calling loadVideo");
        r10.mShouldLoadAfterClose = false;
        loadVideo(r10.mAuctionServerDataToLoad, r10.mAuctionIdToLoad, r10.mAuctionTrialToLoad, r10.mAuctionFallbackToLoad, r10.mSessionDepthToLoad);
        resetAuctionParams();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onRewardedVideoAdClosed() {
        /*
            r10 = this;
            java.lang.String r0 = "onRewardedVideoAdClosed"
            r10.logAdapterCallback(r0)
            r0 = 1203(0x4b3, float:1.686E-42)
            r10.sendProviderEventWithPlacement(r0)
            java.lang.Object r0 = r10.mStateLock
            monitor-enter(r0)
            com.ironsource.mediationsdk.ProgRvSmash$SMASH_STATE r1 = r10.mState     // Catch:{ all -> 0x0075 }
            com.ironsource.mediationsdk.ProgRvSmash$SMASH_STATE r2 = com.ironsource.mediationsdk.ProgRvSmash.SMASH_STATE.SHOW_IN_PROGRESS     // Catch:{ all -> 0x0075 }
            r3 = 0
            if (r1 == r2) goto L_0x004d
            r1 = 81316(0x13da4, float:1.13948E-40)
            r2 = 2
            java.lang.Object[][] r4 = new java.lang.Object[r2][]     // Catch:{ all -> 0x0075 }
            java.lang.Object[] r5 = new java.lang.Object[r2]     // Catch:{ all -> 0x0075 }
            java.lang.String r6 = "errorCode"
            r5[r3] = r6     // Catch:{ all -> 0x0075 }
            r6 = 5009(0x1391, float:7.019E-42)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0075 }
            r7 = 1
            r5[r7] = r6     // Catch:{ all -> 0x0075 }
            r4[r3] = r5     // Catch:{ all -> 0x0075 }
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x0075 }
            java.lang.String r5 = "reason"
            r2[r3] = r5     // Catch:{ all -> 0x0075 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0075 }
            r3.<init>()     // Catch:{ all -> 0x0075 }
            java.lang.String r5 = "adClosed: "
            r3.append(r5)     // Catch:{ all -> 0x0075 }
            com.ironsource.mediationsdk.ProgRvSmash$SMASH_STATE r5 = r10.mState     // Catch:{ all -> 0x0075 }
            r3.append(r5)     // Catch:{ all -> 0x0075 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0075 }
            r2[r7] = r3     // Catch:{ all -> 0x0075 }
            r4[r7] = r2     // Catch:{ all -> 0x0075 }
            r10.sendProviderEvent(r1, r4)     // Catch:{ all -> 0x0075 }
            monitor-exit(r0)     // Catch:{ all -> 0x0075 }
            return
        L_0x004d:
            com.ironsource.mediationsdk.ProgRvSmash$SMASH_STATE r1 = com.ironsource.mediationsdk.ProgRvSmash.SMASH_STATE.NOT_LOADED     // Catch:{ all -> 0x0075 }
            r10.setState(r1)     // Catch:{ all -> 0x0075 }
            monitor-exit(r0)     // Catch:{ all -> 0x0075 }
            com.ironsource.mediationsdk.ProgRvManagerListener r0 = r10.mListener
            r0.onRewardedVideoAdClosed(r10)
            boolean r0 = r10.mShouldLoadAfterClose
            if (r0 == 0) goto L_0x0074
            java.lang.String r0 = "onRewardedVideoAdClosed and mShouldLoadAfterClose is true - calling loadVideo"
            r10.logInternal(r0)
            r10.mShouldLoadAfterClose = r3
            java.lang.String r5 = r10.mAuctionServerDataToLoad
            java.lang.String r6 = r10.mAuctionIdToLoad
            int r7 = r10.mAuctionTrialToLoad
            java.lang.String r8 = r10.mAuctionFallbackToLoad
            int r9 = r10.mSessionDepthToLoad
            r4 = r10
            r4.loadVideo(r5, r6, r7, r8, r9)
            r10.resetAuctionParams()
        L_0x0074:
            return
        L_0x0075:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0075 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.ProgRvSmash.onRewardedVideoAdClosed():void");
    }

    public void onRewardedVideoAdEnded() {
        logAdapterCallback("onRewardedVideoAdEnded");
        this.mListener.onRewardedVideoAdEnded(this);
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_ENDED);
    }

    public void onRewardedVideoAdRewarded() {
        logAdapterCallback("onRewardedVideoAdRewarded");
        this.mListener.onRewardedVideoAdRewarded(this, this.mCurrentPlacement);
        Map<String, Object> providerEventData = getProviderEventData();
        providerEventData.put("placement", this.mCurrentPlacement.getPlacementName());
        providerEventData.put(IronSourceConstants.EVENTS_REWARD_NAME, this.mCurrentPlacement.getRewardName());
        providerEventData.put(IronSourceConstants.EVENTS_REWARD_AMOUNT, Integer.valueOf(this.mCurrentPlacement.getRewardAmount()));
        if (!TextUtils.isEmpty(IronSourceObject.getInstance().getDynamicUserId())) {
            providerEventData.put(IronSourceConstants.EVENTS_DYNAMIC_USER_ID, IronSourceObject.getInstance().getDynamicUserId());
        }
        if (IronSourceObject.getInstance().getRvServerParams() != null) {
            for (String next : IronSourceObject.getInstance().getRvServerParams().keySet()) {
                providerEventData.put("custom_" + next, IronSourceObject.getInstance().getRvServerParams().get(next));
            }
        }
        if (!TextUtils.isEmpty(this.mCurrentAuctionId)) {
            providerEventData.put(IronSourceConstants.EVENTS_AUCTION_ID, this.mCurrentAuctionId);
        }
        if (shouldAddAuctionParams(1010)) {
            RewardedVideoEventsManager.getInstance().setEventAuctionParams(providerEventData, this.mAuctionTrial, this.mAuctionFallback);
        }
        providerEventData.put("sessionDepth", Integer.valueOf(this.mSessionDepth));
        EventData eventData = new EventData(1010, new JSONObject(providerEventData));
        eventData.addToAdditionalData(IronSourceConstants.EVENTS_TRANS_ID, IronSourceUtils.getTransId("" + Long.toString(eventData.getTimeStamp()) + this.mAppKey + getInstanceName()));
        RewardedVideoEventsManager.getInstance().log(eventData);
    }

    public void onRewardedVideoAdClicked() {
        logAdapterCallback("onRewardedVideoAdClicked");
        this.mListener.onRewardedVideoAdClicked(this, this.mCurrentPlacement);
        sendProviderEventWithPlacement(1006);
    }

    private void stopLoadTimer() {
        synchronized (this.mTimerLock) {
            if (this.mTimer != null) {
                this.mTimer.cancel();
                this.mTimer = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public long getElapsedTime() {
        return new Date().getTime() - this.mLoadStartTime;
    }

    private void updateFutureAuctionData(String str, String str2, int i, String str3, int i2) {
        this.mAuctionIdToLoad = str2;
        this.mAuctionServerDataToLoad = str;
        this.mAuctionTrialToLoad = i;
        this.mAuctionFallbackToLoad = str3;
        this.mSessionDepthToLoad = i2;
    }

    private void startLoadTimer() {
        synchronized (this.mTimerLock) {
            stopLoadTimer();
            this.mTimer = new Timer();
            this.mTimer.schedule(new TimerTask() {
                public void run() {
                    int i;
                    boolean z;
                    ProgRvSmash.this.logInternal("Rewarded Video - load instance time out");
                    synchronized (ProgRvSmash.this.mStateLock) {
                        if (ProgRvSmash.this.mState != SMASH_STATE.LOAD_IN_PROGRESS) {
                            if (ProgRvSmash.this.mState != SMASH_STATE.INIT_IN_PROGRESS) {
                                z = false;
                                i = IronSourceError.ERROR_CODE_GENERIC;
                            }
                        }
                        ProgRvSmash.this.setState(SMASH_STATE.NOT_LOADED);
                        i = ProgRvSmash.this.mState == SMASH_STATE.LOAD_IN_PROGRESS ? 1025 : IronSourceError.ERROR_RV_INIT_FAILED_TIMEOUT;
                        z = true;
                    }
                    if (z) {
                        ProgRvSmash.this.sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(ProgRvSmash.this.getElapsedTime())}});
                        ProgRvSmash.this.sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED_REASON, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(ProgRvSmash.this.getElapsedTime())}});
                        ProgRvSmash.this.mListener.onLoadError(ProgRvSmash.this, ProgRvSmash.this.mCurrentAuctionId);
                        return;
                    }
                    ProgRvSmash.this.sendProviderEvent(IronSourceConstants.RV_INSTANCE_AVAILABILITY_FALSE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, 1025}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(ProgRvSmash.this.getElapsedTime())}, new Object[]{IronSourceConstants.EVENTS_EXT1, ProgRvSmash.this.mState.name()}});
                }
            }, (long) (this.mLoadTimeoutSecs * 1000));
        }
    }

    private void logAdapterCallback(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, "ProgRvSmash " + getInstanceName() + " : " + str, 0);
    }

    /* access modifiers changed from: private */
    public void logInternal(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "ProgRvSmash " + getInstanceName() + " : " + str, 0);
    }

    private void logInternalError(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "ProgRvSmash " + getInstanceName() + " : " + str, 3);
    }

    private void sendProviderEventWithPlacement(int i) {
        sendProviderEventWithPlacement(i, null);
    }

    private void sendProviderEventWithPlacement(int i, Object[][] objArr) {
        sendProviderEvent(i, objArr, true);
    }

    private void sendProviderEvent(int i) {
        sendProviderEvent(i, null, false);
    }

    /* access modifiers changed from: private */
    public void sendProviderEvent(int i, Object[][] objArr) {
        sendProviderEvent(i, objArr, false);
    }

    private void sendProviderEvent(int i, Object[][] objArr, boolean z) {
        Map<String, Object> providerEventData = getProviderEventData();
        if (!TextUtils.isEmpty(this.mCurrentAuctionId)) {
            providerEventData.put(IronSourceConstants.EVENTS_AUCTION_ID, this.mCurrentAuctionId);
        }
        if (z && this.mCurrentPlacement != null && !TextUtils.isEmpty(this.mCurrentPlacement.getPlacementName())) {
            providerEventData.put("placement", this.mCurrentPlacement.getPlacementName());
        }
        if (shouldAddAuctionParams(i)) {
            RewardedVideoEventsManager.getInstance().setEventAuctionParams(providerEventData, this.mAuctionTrial, this.mAuctionFallback);
        }
        providerEventData.put("sessionDepth", Integer.valueOf(this.mSessionDepth));
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    providerEventData.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, getInstanceName() + " smash: RV sendMediationEvent " + Log.getStackTraceString(e), 3);
            }
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(i, new JSONObject(providerEventData)));
    }
}
