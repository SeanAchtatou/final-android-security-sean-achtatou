package com.ironsource.mediationsdk.config;

public class VersionInfo {
    public static final String BUILD_DATE = "2019-12-11T12:22:34Z";
    public static final long BUILD_UNIX_TIME = 1576066954536L;
    public static final String GIT_DATE = "2019-12-11T12:17:45Z";
    public static final int GIT_REVISION = 5215;
    public static final String GIT_SHA = "eb2882056103b42a5374b92c353e23a2c24ff0dc";
    public static final String MAVEN_GROUP = "";
    public static final String MAVEN_NAME = "Android-SSP-unit-testing-develop-release-master-branches";
    public static final String VERSION = "6.11.1";
}
