package com.ironsource.mediationsdk.events;

import android.os.Handler;
import android.os.HandlerThread;
import com.ironsource.mediationsdk.logger.ThreadExceptionHandler;

public class SuperLooper extends Thread {
    private static SuperLooper mInstance;
    private SupersonicSdkThread mSdkThread = new SupersonicSdkThread(getClass().getSimpleName());

    private SuperLooper() {
        this.mSdkThread.start();
        this.mSdkThread.prepareHandler();
    }

    public static synchronized SuperLooper getLooper() {
        SuperLooper superLooper;
        synchronized (SuperLooper.class) {
            if (mInstance == null) {
                mInstance = new SuperLooper();
            }
            superLooper = mInstance;
        }
        return superLooper;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0013, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void post(java.lang.Runnable r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            com.ironsource.mediationsdk.events.SuperLooper$SupersonicSdkThread r0 = r1.mSdkThread     // Catch:{ all -> 0x0014 }
            if (r0 != 0) goto L_0x0007
            monitor-exit(r1)
            return
        L_0x0007:
            com.ironsource.mediationsdk.events.SuperLooper$SupersonicSdkThread r0 = r1.mSdkThread     // Catch:{ all -> 0x0014 }
            android.os.Handler r0 = r0.getCallbackHandler()     // Catch:{ all -> 0x0014 }
            if (r0 == 0) goto L_0x0012
            r0.post(r2)     // Catch:{ all -> 0x0014 }
        L_0x0012:
            monitor-exit(r1)
            return
        L_0x0014:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.events.SuperLooper.post(java.lang.Runnable):void");
    }

    private class SupersonicSdkThread extends HandlerThread {
        private Handler mHandler;

        SupersonicSdkThread(String str) {
            super(str);
            setUncaughtExceptionHandler(new ThreadExceptionHandler());
        }

        /* access modifiers changed from: package-private */
        public void prepareHandler() {
            this.mHandler = new Handler(getLooper());
        }

        /* access modifiers changed from: package-private */
        public Handler getCallbackHandler() {
            return this.mHandler;
        }
    }
}
