package com.ironsource.mediationsdk;

import com.ironsource.mediationsdk.utils.AuctionSettings;
import java.util.Timer;
import java.util.TimerTask;

public class RvLoadTrigger {
    private AuctionSettings mAuctionSettings;
    /* access modifiers changed from: private */
    public RvLoadTriggerCallback mListener;
    private Timer mTimer = null;

    public RvLoadTrigger(AuctionSettings auctionSettings, RvLoadTriggerCallback rvLoadTriggerCallback) {
        this.mAuctionSettings = auctionSettings;
        this.mListener = rvLoadTriggerCallback;
    }

    public synchronized void showStart() {
        if (this.mAuctionSettings.getIsAuctionOnShowStart()) {
            stopTimer();
            this.mTimer = new Timer();
            this.mTimer.schedule(new TimerTask() {
                public void run() {
                    RvLoadTrigger.this.mListener.onLoadTriggered();
                }
            }, this.mAuctionSettings.getTimeToWaitBeforeAuctionMs());
        }
    }

    public synchronized void showEnd() {
        if (!this.mAuctionSettings.getIsAuctionOnShowStart()) {
            stopTimer();
            this.mTimer = new Timer();
            this.mTimer.schedule(new TimerTask() {
                public void run() {
                    RvLoadTrigger.this.mListener.onLoadTriggered();
                }
            }, this.mAuctionSettings.getTimeToWaitBeforeAuctionMs());
        }
    }

    public synchronized void showError() {
        stopTimer();
        this.mListener.onLoadTriggered();
    }

    public synchronized void loadError() {
        stopTimer();
        this.mTimer = new Timer();
        this.mTimer.schedule(new TimerTask() {
            public void run() {
                RvLoadTrigger.this.mListener.onLoadTriggered();
            }
        }, this.mAuctionSettings.getAuctionRetryInterval());
    }

    private void stopTimer() {
        if (this.mTimer != null) {
            this.mTimer.cancel();
            this.mTimer = null;
        }
    }
}
