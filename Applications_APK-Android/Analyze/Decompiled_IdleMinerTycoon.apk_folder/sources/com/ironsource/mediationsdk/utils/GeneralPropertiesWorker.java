package com.ironsource.mediationsdk.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.TelephonyManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.support.constants.FaqsColumns;
import com.ironsource.mediationsdk.IronSourceObject;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.GeneralProperties;
import com.ironsource.sdk.constants.Constants;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class GeneralPropertiesWorker implements Runnable {
    private static final int MAX_MINUTES_OFFSET = 840;
    private static final int MINUTES_OFFSET_STEP = 15;
    private static final int MIN_MINUTES_OFFSET = -720;
    public static final String SDK_VERSION = "sdkVersion";
    private final String ADVERTISING_ID = "advertisingId";
    private final String ADVERTISING_ID_IS_LIMIT_TRACKING = Constants.RequestParameters.isLAT;
    private final String ADVERTISING_ID_TYPE = "advertisingIdType";
    private final String ANDROID_OS_VERSION = "osVersion";
    private final String APPLICATION_KEY = ServerResponseWrapper.APP_KEY_FIELD;
    private final String BATTERY_LEVEL = "battery";
    private final String BUNDLE_ID = Constants.RequestParameters.PACKAGE_NAME;
    private final String CONNECTION_TYPE = Constants.RequestParameters.CONNECTION_TYPE;
    private final String DEVICE_MODEL = Constants.RequestParameters.DEVICE_MODEL;
    private final String DEVICE_OEM = Constants.RequestParameters.DEVICE_OEM;
    private final String DEVICE_OS = "deviceOS";
    private final String EXTERNAL_FREE_MEMORY = "externalFreeMemory";
    private final String GMT_MINUTES_OFFSET = "gmtMinutesOffset";
    private final String INTERNAL_FREE_MEMORY = "internalFreeMemory";
    private final String KEY_IS_ROOT = "jb";
    private final String KEY_PLUGIN_FW_VERSION = "plugin_fw_v";
    private final String KEY_PLUGIN_TYPE = "pluginType";
    private final String KEY_PLUGIN_VERSION = SDKConfigurationDM.PLUGIN_VERSION;
    private final String KEY_SESSION_ID = "sessionId";
    private final String LANGUAGE = FaqsColumns.LANGUAGE;
    private final String LOCATION_LAT = "lat";
    private final String LOCATION_LON = "lon";
    private final String MEDIATION_TYPE = "mt";
    private final String MOBILE_CARRIER = Constants.RequestParameters.MOBILE_CARRIER;
    private final String PUBLISHER_APP_VERSION = Constants.RequestParameters.APPLICATION_VERSION_NAME;
    private final String TAG = getClass().getSimpleName();
    private Context mContext;

    private String getDeviceOS() {
        return "Android";
    }

    private GeneralPropertiesWorker() {
    }

    public GeneralPropertiesWorker(Context context) {
        this.mContext = context.getApplicationContext();
    }

    public void run() {
        try {
            GeneralProperties.getProperties().putKeys(collectInformation());
            IronSourceUtils.saveGeneralProperties(this.mContext, GeneralProperties.getProperties().toJSON());
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            logger.logException(ironSourceTag, "Thread name = " + getClass().getSimpleName(), e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00e3  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0173  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0182  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0191  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01b3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.Map<java.lang.String, java.lang.Object> collectInformation() {
        /*
            r8 = this;
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            com.ironsource.mediationsdk.IronSourceObject r1 = com.ironsource.mediationsdk.IronSourceObject.getInstance()
            java.lang.String r1 = r1.getSessionId()
            java.lang.String r2 = "sessionId"
            r0.put(r2, r1)
            java.lang.String r1 = r8.getBundleId()
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x0032
            java.lang.String r2 = "bundleId"
            r0.put(r2, r1)
            android.content.Context r2 = r8.mContext
            java.lang.String r1 = com.ironsource.environment.ApplicationContext.getPublisherApplicationVersion(r2, r1)
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x0032
            java.lang.String r2 = "appVersion"
            r0.put(r2, r1)
        L_0x0032:
            java.lang.String r1 = "appKey"
            java.lang.String r2 = r8.getApplicationKey()
            r0.put(r1, r2)
            java.lang.String r1 = ""
            java.lang.String r2 = ""
            r3 = 1
            r4 = 2
            r5 = 0
            android.content.Context r6 = r8.mContext     // Catch:{ Exception -> 0x0063 }
            java.lang.String[] r6 = com.ironsource.environment.DeviceStatus.getAdvertisingIdInfo(r6)     // Catch:{ Exception -> 0x0063 }
            if (r6 == 0) goto L_0x0063
            int r7 = r6.length     // Catch:{ Exception -> 0x0063 }
            if (r7 != r4) goto L_0x0063
            r7 = r6[r5]     // Catch:{ Exception -> 0x0063 }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ Exception -> 0x0063 }
            if (r7 != 0) goto L_0x0058
            r7 = r6[r5]     // Catch:{ Exception -> 0x0063 }
            r1 = r7
        L_0x0058:
            r6 = r6[r3]     // Catch:{ Exception -> 0x0063 }
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ Exception -> 0x0063 }
            boolean r6 = r6.booleanValue()     // Catch:{ Exception -> 0x0063 }
            goto L_0x0064
        L_0x0063:
            r6 = 0
        L_0x0064:
            boolean r7 = android.text.TextUtils.isEmpty(r1)
            if (r7 != 0) goto L_0x006d
            java.lang.String r2 = "GAID"
            goto L_0x007b
        L_0x006d:
            android.content.Context r1 = r8.mContext
            java.lang.String r1 = com.ironsource.environment.DeviceStatus.getOrGenerateOnceUniqueIdentifier(r1)
            boolean r7 = android.text.TextUtils.isEmpty(r1)
            if (r7 != 0) goto L_0x007b
            java.lang.String r2 = "UUID"
        L_0x007b:
            boolean r7 = android.text.TextUtils.isEmpty(r1)
            if (r7 != 0) goto L_0x0094
            java.lang.String r7 = "advertisingId"
            r0.put(r7, r1)
            java.lang.String r1 = "advertisingIdType"
            r0.put(r1, r2)
            java.lang.String r1 = "isLimitAdTrackingEnabled"
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r6)
            r0.put(r1, r2)
        L_0x0094:
            java.lang.String r1 = "deviceOS"
            java.lang.String r2 = r8.getDeviceOS()
            r0.put(r1, r2)
            java.lang.String r1 = r8.getAndroidVersion()
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x00b0
            java.lang.String r1 = "osVersion"
            java.lang.String r2 = r8.getAndroidVersion()
            r0.put(r1, r2)
        L_0x00b0:
            android.content.Context r1 = r8.mContext
            java.lang.String r1 = com.ironsource.mediationsdk.utils.IronSourceUtils.getConnectionType(r1)
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x00c1
            java.lang.String r2 = "connectionType"
            r0.put(r2, r1)
        L_0x00c1:
            java.lang.String r1 = "sdkVersion"
            java.lang.String r2 = r8.getSDKVersion()
            r0.put(r1, r2)
            java.lang.String r1 = r8.getLanguage()
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x00d9
            java.lang.String r2 = "language"
            r0.put(r2, r1)
        L_0x00d9:
            java.lang.String r1 = r8.getDeviceOEM()
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x00e8
            java.lang.String r2 = "deviceOEM"
            r0.put(r2, r1)
        L_0x00e8:
            java.lang.String r1 = r8.getDeviceModel()
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x00f7
            java.lang.String r2 = "deviceModel"
            r0.put(r2, r1)
        L_0x00f7:
            java.lang.String r1 = r8.getMobileCarrier()
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x0106
            java.lang.String r2 = "mobileCarrier"
            r0.put(r2, r1)
        L_0x0106:
            long r1 = r8.getInternalStorageFreeSize()
            java.lang.String r6 = "internalFreeMemory"
            java.lang.Long r1 = java.lang.Long.valueOf(r1)
            r0.put(r6, r1)
            long r1 = r8.getExternalStorageFreeSize()
            java.lang.String r6 = "externalFreeMemory"
            java.lang.Long r1 = java.lang.Long.valueOf(r1)
            r0.put(r6, r1)
            int r1 = r8.getBatteryLevel()
            java.lang.String r2 = "battery"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r0.put(r2, r1)
            android.content.Context r1 = r8.mContext
            java.lang.String r2 = "GeneralProperties.ALLOW_LOCATION_SHARED_PREFS_KEY"
            boolean r1 = com.ironsource.mediationsdk.utils.IronSourceUtils.getBooleanFromSharedPrefs(r1, r2, r5)
            if (r1 == 0) goto L_0x0156
            double[] r1 = r8.getLastKnownLocation()
            if (r1 == 0) goto L_0x0156
            int r2 = r1.length
            if (r2 != r4) goto L_0x0156
            java.lang.String r2 = "lat"
            r4 = r1[r5]
            java.lang.Double r4 = java.lang.Double.valueOf(r4)
            r0.put(r2, r4)
            java.lang.String r2 = "lon"
            r3 = r1[r3]
            java.lang.Double r1 = java.lang.Double.valueOf(r3)
            r0.put(r2, r1)
        L_0x0156:
            int r1 = r8.getGmtMinutesOffset()
            boolean r2 = r8.validateGmtMinutesOffset(r1)
            if (r2 == 0) goto L_0x0169
            java.lang.String r2 = "gmtMinutesOffset"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r0.put(r2, r1)
        L_0x0169:
            java.lang.String r1 = r8.getPluginType()
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x0178
            java.lang.String r2 = "pluginType"
            r0.put(r2, r1)
        L_0x0178:
            java.lang.String r1 = r8.getPluginVersion()
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x0187
            java.lang.String r2 = "pluginVersion"
            r0.put(r2, r1)
        L_0x0187:
            java.lang.String r1 = r8.getPluginFrameworkVersion()
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x0196
            java.lang.String r2 = "plugin_fw_v"
            r0.put(r2, r1)
        L_0x0196:
            boolean r1 = com.ironsource.environment.DeviceStatus.isRootedDevice()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x01a9
            java.lang.String r2 = "jb"
            r0.put(r2, r1)
        L_0x01a9:
            java.lang.String r1 = r8.getMediationType()
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x01b8
            java.lang.String r2 = "mt"
            r0.put(r2, r1)
        L_0x01b8:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.utils.GeneralPropertiesWorker.collectInformation():java.util.Map");
    }

    private String getPluginType() {
        try {
            return ConfigFile.getConfigFile().getPluginType();
        } catch (Exception e) {
            IronSourceLoggerManager.getLogger().logException(IronSourceLogger.IronSourceTag.NATIVE, "getPluginType()", e);
            return "";
        }
    }

    private String getPluginVersion() {
        try {
            return ConfigFile.getConfigFile().getPluginVersion();
        } catch (Exception e) {
            IronSourceLoggerManager.getLogger().logException(IronSourceLogger.IronSourceTag.NATIVE, "getPluginVersion()", e);
            return "";
        }
    }

    private String getPluginFrameworkVersion() {
        try {
            return ConfigFile.getConfigFile().getPluginFrameworkVersion();
        } catch (Exception e) {
            IronSourceLoggerManager.getLogger().logException(IronSourceLogger.IronSourceTag.NATIVE, "getPluginFrameworkVersion()", e);
            return "";
        }
    }

    private String getBundleId() {
        try {
            return this.mContext.getPackageName();
        } catch (Exception unused) {
            return "";
        }
    }

    private String getApplicationKey() {
        return IronSourceObject.getInstance().getIronSourceAppKey();
    }

    private String getAndroidVersion() {
        try {
            String str = Build.VERSION.RELEASE;
            int i = Build.VERSION.SDK_INT;
            return "" + i + "(" + str + ")";
        } catch (Exception unused) {
            return "";
        }
    }

    private String getSDKVersion() {
        return IronSourceUtils.getSDKVersion();
    }

    private String getLanguage() {
        try {
            return Locale.getDefault().getLanguage();
        } catch (Exception unused) {
            return "";
        }
    }

    private String getDeviceOEM() {
        try {
            return Build.MANUFACTURER;
        } catch (Exception unused) {
            return "";
        }
    }

    private String getDeviceModel() {
        try {
            return Build.MODEL;
        } catch (Exception unused) {
            return "";
        }
    }

    private String getMobileCarrier() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.mContext.getSystemService("phone");
            if (telephonyManager == null) {
                return "";
            }
            String networkOperatorName = telephonyManager.getNetworkOperatorName();
            if (!networkOperatorName.equals("")) {
                return networkOperatorName;
            }
            return "";
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            logger.logException(ironSourceTag, this.TAG + ":getMobileCarrier()", e);
            return "";
        }
    }

    private boolean isExternalStorageAbvailable() {
        try {
            return Environment.getExternalStorageState().equals("mounted");
        } catch (Exception unused) {
            return false;
        }
    }

    private long getInternalStorageFreeSize() {
        try {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            return (((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
        } catch (Exception unused) {
            return -1;
        }
    }

    private long getExternalStorageFreeSize() {
        if (!isExternalStorageAbvailable()) {
            return -1;
        }
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return (((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
    }

    private int getBatteryLevel() {
        try {
            Intent registerReceiver = this.mContext.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int i = 0;
            int intExtra = registerReceiver != null ? registerReceiver.getIntExtra("level", -1) : 0;
            if (registerReceiver != null) {
                i = registerReceiver.getIntExtra("scale", -1);
            }
            if (intExtra == -1 || i == -1) {
                return -1;
            }
            return (int) ((((float) intExtra) / ((float) i)) * 100.0f);
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            logger.logException(ironSourceTag, this.TAG + ":getBatteryLevel()", e);
            return -1;
        }
    }

    @SuppressLint({"MissingPermission"})
    private double[] getLastKnownLocation() {
        double[] dArr = new double[0];
        try {
            if (!locationPermissionGranted()) {
                return dArr;
            }
            LocationManager locationManager = (LocationManager) this.mContext.getApplicationContext().getSystemService(FirebaseAnalytics.Param.LOCATION);
            Location location = null;
            long j = Long.MIN_VALUE;
            for (String lastKnownLocation : locationManager.getAllProviders()) {
                Location lastKnownLocation2 = locationManager.getLastKnownLocation(lastKnownLocation);
                if (lastKnownLocation2 != null && lastKnownLocation2.getTime() > j) {
                    j = lastKnownLocation2.getTime();
                    location = lastKnownLocation2;
                }
            }
            if (location == null) {
                return dArr;
            }
            return new double[]{location.getLatitude(), location.getLongitude()};
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            logger.logException(ironSourceTag, this.TAG + ":getLastLocation()", e);
            return new double[0];
        }
    }

    private boolean locationPermissionGranted() {
        try {
            return this.mContext.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0;
        } catch (Exception unused) {
            return false;
        }
    }

    private int getGmtMinutesOffset() {
        int i = 0;
        try {
            TimeZone timeZone = TimeZone.getDefault();
            int offset = (timeZone.getOffset(GregorianCalendar.getInstance(timeZone).getTimeInMillis()) / 1000) / 60;
            try {
                return Math.round((float) (offset / 15)) * 15;
            } catch (Exception e) {
                int i2 = offset;
                e = e;
                i = i2;
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
                logger.logException(ironSourceTag, this.TAG + ":getGmtMinutesOffset()", e);
                return i;
            }
        } catch (Exception e2) {
            e = e2;
            IronSourceLoggerManager logger2 = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.NATIVE;
            logger2.logException(ironSourceTag2, this.TAG + ":getGmtMinutesOffset()", e);
            return i;
        }
    }

    private boolean validateGmtMinutesOffset(int i) {
        return i <= MAX_MINUTES_OFFSET && i >= MIN_MINUTES_OFFSET && i % 15 == 0;
    }

    private String getMediationType() {
        return IronSourceObject.getInstance().getMediationType();
    }
}
