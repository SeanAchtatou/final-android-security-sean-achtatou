package com.ironsource.mediationsdk.utils;

import android.content.Context;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.nearby.messages.Strategy;
import com.helpshift.common.domain.network.NetworkConstants;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.eventsmodule.DataBaseEventsStorage;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.ConsoleLogger;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ApplicationConfigurations;
import com.ironsource.mediationsdk.model.ApplicationEvents;
import com.ironsource.mediationsdk.model.ApplicationLogger;
import com.ironsource.mediationsdk.model.BannerConfigurations;
import com.ironsource.mediationsdk.model.BannerPlacement;
import com.ironsource.mediationsdk.model.Configurations;
import com.ironsource.mediationsdk.model.InterstitialConfigurations;
import com.ironsource.mediationsdk.model.InterstitialPlacement;
import com.ironsource.mediationsdk.model.OfferwallConfigurations;
import com.ironsource.mediationsdk.model.OfferwallPlacement;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.model.PlacementAvailabilitySettings;
import com.ironsource.mediationsdk.model.PlacementCappingType;
import com.ironsource.mediationsdk.model.ProviderOrder;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.model.ProviderSettingsHolder;
import com.ironsource.mediationsdk.model.RewardedVideoConfigurations;
import com.ironsource.mediationsdk.model.ServerSegmetData;
import com.ironsource.mediationsdk.sdk.GeneralProperties;
import com.ironsource.sdk.constants.Constants;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ServerResponseWrapper {
    public static final String APP_KEY_FIELD = "appKey";
    public static final String RESPONSE_FIELD = "response";
    public static final String USER_ID_FIELD = "userId";
    private final String AB_TESTING = "abt";
    private final String ADAPTER_TIMEOUT_IN_MILLIS_FIELD = "atim";
    private final String ADAPTER_TIMEOUT_IN_SECS_FIELD = "adapterTimeOutInSeconds";
    private final String AD_SOURCE_NAME_FIELD = "adSourceName";
    private final String AD_UNITS_FIELD = "adUnits";
    private final String ALLOW_LOCATION = "allowLocation";
    private final String APPLICATION_FIELD = Constants.ParametersKeys.ORIENTATION_APPLICATION;
    private final String AUCTION_DATA_FIELD = "auctionData";
    private final String AUCTION_FIELD = "auction";
    private final String AUCTION_PROGRAMMATIC_FIELD = IronSourceConstants.EVENTS_PROGRAMMATIC;
    private final String AUCTION_RETRY_INTERVAL_FIELD = "auctionRetryInterval";
    private final String AUCTION_TIMEOUT_FIELD = "auctionTimeout";
    private final String AUCTION_TRIALS_FIELD = IronSourceConstants.AUCTION_TRIALS;
    private final String AUCTION_URL_FIELD = "auctioneerURL";
    private final String BACKFILL_FIELD = "backFill";
    private final String BACKUP_THRESHOLD_FIELD = "backupThreshold";
    private final String BN_FIELD = "banner";
    private final String CONFIGURATIONS_FIELD = TapjoyConstants.PREF_SERVER_PROVIDED_CONFIGURATIONS;
    private final String CONSOLE_FIELD = ConsoleLogger.NAME;
    private final int DEFAULT_ADAPTERS_SMARTLOAD_AMOUNT = 2;
    private final int DEFAULT_ADAPTERS_SMARTLOAD_TIMEOUT = 60;
    private final String DEFAULT_BANNER_LOAD_REFRESH_INTERVAL = "bannerInterval";
    private final int DEFAULT_BANNER_SMARTLOAD_TIMEOUT = AbstractSpiCall.DEFAULT_TIMEOUT;
    private final int DEFAULT_BN_DELAY_LOAD_FAILURE_TIMEOUT = 3;
    private final int DEFAULT_IS_DELAY_LOAD_FAILURE_TIMEOUT = 3;
    private final int DEFAULT_LOG_LEVEL = 3;
    private final int DEFAULT_MANUAL_LOAD_INTERVAL_FIELD = Strategy.TTL_SECONDS_DEFAULT;
    private final int DEFAULT_MAX_EVENTS_PER_BATCH = 5000;
    private final long DEFAULT_TIMEOUT = 10000;
    private final int DEFAULT_TRIALS = 2;
    private final String DELAY_LOAD_FAILURE = "delayLoadFailure";
    private final String ERROR_KEY = "error";
    private final String EVENTS_FIELD = DataBaseEventsStorage.EventEntry.TABLE_NAME;
    private final String GENERIC_PARAMS_FIELD = "genericParams";
    private final String INTEGRATION_FIELD = "integration";
    private final String IS_AUCTION_ON_SHOW_START_FIELD = "isAuctionOnShowStart";
    private final String IS_FIELD = "interstitial";
    private final String IS_MULTIPLE_INSTANCES_FIELD = "mpis";
    private final String LOGGERS_FIELD = "loggers";
    private final String MANUAL_LOAD_INTERVAL_FIELD = "loadRVInterval";
    private final String MAX_EVENTS_PER_BATCH = "maxEventsPerBatch";
    private final String MAX_NUM_OF_ADAPTERS_TO_LOAD_ON_START_FIELD = "maxNumOfAdaptersToLoadOnStart";
    private final String MAX_NUM_OF_EVENTS_FIELD = "maxNumberOfEvents";
    private final String MIN_TIME_BEFORE_FIRST_AUCTION_FIELD = "minTimeBeforeFirstAuction";
    private final String OPT_OUT_EVENTS_FIELD = "optOut";
    private final String OW_FIELD = "offerwall";
    private final String PLACEMENTS_FIELD = "placements";
    private final String PLACEMENT_ID_FIELD = "placementId";
    private final String PLACEMENT_NAME_FIELD = "placementName";
    private final String PLACEMENT_SETTINGS_CAPPING_FIELD = "capping";
    private final String PLACEMENT_SETTINGS_CAPPING_UNIT_FIELD = "unit";
    private final String PLACEMENT_SETTINGS_CAPPING_VALUE_FIELD = "maxImpressions";
    private final String PLACEMENT_SETTINGS_DELIVERY_FIELD = "delivery";
    private final String PLACEMENT_SETTINGS_ENABLED_FIELD = TJAdUnitConstants.String.ENABLED;
    private final String PLACEMENT_SETTINGS_IS_DEFAULT_FIELD = "isDefault";
    private final String PLACEMENT_SETTINGS_PACING_FIELD = "pacing";
    private final String PLACEMENT_SETTINGS_PACING_VALUE_FIELD = "numOfSeconds";
    private final String PREMIUM_FIELD = "premium";
    private final String PROVIDER_LOAD_NAME_FIELD = "providerLoadName";
    private final String PROVIDER_ORDER_FIELD = "providerOrder";
    private final String PROVIDER_SETTINGS_FIELD = "providerSettings";
    private final String PUBLISHER_FIELD = "publisher";
    private final String RV_FIELD = "rewardedVideo";
    private final String SEGMENT_FIELD = "segment";
    private final String SEND_EVENTS_TOGGLE_FIELD = "sendEventsToggle";
    private final String SEND_ULTRA_EVENTS_FIELD = "sendUltraEvents";
    private final String SERVER_EVENTS_TYPE = "serverEventsType";
    private final String SERVER_EVENTS_URL_FIELD = "serverEventsURL";
    private final String SERVER_FIELD = "server";
    private final String SUB_PROVIDER_ID_FIELD = "spId";
    private final String TIME_TO_WAIT_BEFORE_AUCTION_FIELD = "timeToWaitBeforeAuction";
    private final String UUID_ENABLED_FIELD = DeviceStatus.UUID_ENABLED;
    private final String VIRTUAL_ITEM_COUNT_FIELD = "virtualItemCount";
    private final String VIRTUAL_ITEM_NAME_FIELD = "virtualItemName";
    private String mAppKey;
    private Configurations mConfigurations;
    private Context mContext;
    private ProviderOrder mProviderOrder;
    private ProviderSettingsHolder mProviderSettingsHolder;
    private JSONObject mResponse;
    private String mUserId;

    public ServerResponseWrapper(Context context, String str, String str2, String str3) {
        this.mContext = context;
        try {
            if (TextUtils.isEmpty(str3)) {
                this.mResponse = new JSONObject();
            } else {
                this.mResponse = new JSONObject(str3);
            }
            parseProviderSettings();
            parseConfigurations();
            parseProviderOrder();
            this.mAppKey = TextUtils.isEmpty(str) ? "" : str;
            this.mUserId = TextUtils.isEmpty(str2) ? "" : str2;
        } catch (JSONException e) {
            e.printStackTrace();
            defaultInit();
        }
    }

    public ServerResponseWrapper(ServerResponseWrapper serverResponseWrapper) {
        try {
            this.mContext = serverResponseWrapper.getContext();
            this.mResponse = new JSONObject(serverResponseWrapper.mResponse.toString());
            this.mAppKey = serverResponseWrapper.mAppKey;
            this.mUserId = serverResponseWrapper.mUserId;
            this.mProviderOrder = serverResponseWrapper.getProviderOrder();
            this.mProviderSettingsHolder = serverResponseWrapper.getProviderSettingsHolder();
            this.mConfigurations = serverResponseWrapper.getConfigurations();
        } catch (Exception unused) {
            defaultInit();
        }
    }

    private void defaultInit() {
        this.mResponse = new JSONObject();
        this.mAppKey = "";
        this.mUserId = "";
        this.mProviderOrder = new ProviderOrder();
        this.mProviderSettingsHolder = ProviderSettingsHolder.getProviderSettingsHolder();
        this.mConfigurations = new Configurations();
    }

    public String toString() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(APP_KEY_FIELD, this.mAppKey);
            jSONObject.put("userId", this.mUserId);
            jSONObject.put("response", this.mResponse);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    public boolean isValidResponse() {
        if (!((((this.mResponse != null) && !this.mResponse.has("error")) && this.mProviderOrder != null) && this.mProviderSettingsHolder != null) || this.mConfigurations == null) {
            return false;
        }
        return true;
    }

    public List<IronSource.AD_UNIT> getInitiatedAdUnits() {
        if (this.mResponse == null || this.mConfigurations == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        if (!(this.mConfigurations.getRewardedVideoConfigurations() == null || this.mProviderOrder == null || this.mProviderOrder.getRewardedVideoProviderOrder().size() <= 0)) {
            arrayList.add(IronSource.AD_UNIT.REWARDED_VIDEO);
        }
        if (!(this.mConfigurations.getInterstitialConfigurations() == null || this.mProviderOrder == null || this.mProviderOrder.getInterstitialProviderOrder().size() <= 0)) {
            arrayList.add(IronSource.AD_UNIT.INTERSTITIAL);
        }
        if (this.mConfigurations.getOfferwallConfigurations() != null) {
            arrayList.add(IronSource.AD_UNIT.OFFERWALL);
        }
        if (this.mConfigurations.getBannerConfigurations() != null) {
            arrayList.add(IronSource.AD_UNIT.BANNER);
        }
        return arrayList;
    }

    private void parseProviderOrder() {
        try {
            JSONObject section = getSection(this.mResponse, "providerOrder");
            JSONArray optJSONArray = section.optJSONArray("rewardedVideo");
            JSONArray optJSONArray2 = section.optJSONArray("interstitial");
            JSONArray optJSONArray3 = section.optJSONArray("banner");
            this.mProviderOrder = new ProviderOrder();
            if (!(optJSONArray == null || getConfigurations() == null || getConfigurations().getRewardedVideoConfigurations() == null)) {
                String backFillProviderName = getConfigurations().getRewardedVideoConfigurations().getBackFillProviderName();
                String premiumProviderName = getConfigurations().getRewardedVideoConfigurations().getPremiumProviderName();
                for (int i = 0; i < optJSONArray.length(); i++) {
                    String optString = optJSONArray.optString(i);
                    if (optString.equals(backFillProviderName)) {
                        this.mProviderOrder.setRVBackFillProvider(backFillProviderName);
                    } else {
                        if (optString.equals(premiumProviderName)) {
                            this.mProviderOrder.setRVPremiumProvider(premiumProviderName);
                        }
                        this.mProviderOrder.addRewardedVideoProvider(optString);
                        ProviderSettings providerSettings = ProviderSettingsHolder.getProviderSettingsHolder().getProviderSettings(optString);
                        if (providerSettings != null) {
                            providerSettings.setRewardedVideoPriority(i);
                        }
                    }
                }
            }
            if (!(optJSONArray2 == null || getConfigurations() == null || getConfigurations().getInterstitialConfigurations() == null)) {
                String backFillProviderName2 = getConfigurations().getInterstitialConfigurations().getBackFillProviderName();
                String premiumProviderName2 = getConfigurations().getInterstitialConfigurations().getPremiumProviderName();
                for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                    String optString2 = optJSONArray2.optString(i2);
                    if (optString2.equals(backFillProviderName2)) {
                        this.mProviderOrder.setISBackFillProvider(backFillProviderName2);
                    } else {
                        if (optString2.equals(premiumProviderName2)) {
                            this.mProviderOrder.setISPremiumProvider(premiumProviderName2);
                        }
                        this.mProviderOrder.addInterstitialProvider(optString2);
                        ProviderSettings providerSettings2 = ProviderSettingsHolder.getProviderSettingsHolder().getProviderSettings(optString2);
                        if (providerSettings2 != null) {
                            providerSettings2.setInterstitialPriority(i2);
                        }
                    }
                }
            }
            if (optJSONArray3 != null) {
                for (int i3 = 0; i3 < optJSONArray3.length(); i3++) {
                    String optString3 = optJSONArray3.optString(i3);
                    this.mProviderOrder.addBannerProvider(optString3);
                    ProviderSettings providerSettings3 = ProviderSettingsHolder.getProviderSettingsHolder().getProviderSettings(optString3);
                    if (providerSettings3 != null) {
                        providerSettings3.setBannerPriority(i3);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseProviderSettings() {
        try {
            this.mProviderSettingsHolder = ProviderSettingsHolder.getProviderSettingsHolder();
            JSONObject section = getSection(this.mResponse, "providerSettings");
            Iterator<String> keys = section.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                JSONObject optJSONObject = section.optJSONObject(next);
                if (optJSONObject != null) {
                    boolean optBoolean = optJSONObject.optBoolean("mpis", false);
                    String optString = optJSONObject.optString("spId", AppEventsConstants.EVENT_PARAM_VALUE_NO);
                    String optString2 = optJSONObject.optString("adSourceName", null);
                    String optString3 = optJSONObject.optString("providerLoadName", next);
                    JSONObject section2 = getSection(optJSONObject, "adUnits");
                    JSONObject section3 = getSection(optJSONObject, Constants.ParametersKeys.ORIENTATION_APPLICATION);
                    JSONObject section4 = getSection(section2, "rewardedVideo");
                    JSONObject section5 = getSection(section2, "interstitial");
                    JSONObject section6 = getSection(section2, "banner");
                    JSONObject mergeJsons = IronSourceUtils.mergeJsons(section4, section3);
                    JSONObject mergeJsons2 = IronSourceUtils.mergeJsons(section5, section3);
                    JSONObject mergeJsons3 = IronSourceUtils.mergeJsons(section6, section3);
                    if (this.mProviderSettingsHolder.containsProviderSettings(next)) {
                        ProviderSettings providerSettings = this.mProviderSettingsHolder.getProviderSettings(next);
                        JSONObject rewardedVideoSettings = providerSettings.getRewardedVideoSettings();
                        JSONObject interstitialSettings = providerSettings.getInterstitialSettings();
                        JSONObject bannerSettings = providerSettings.getBannerSettings();
                        providerSettings.setRewardedVideoSettings(IronSourceUtils.mergeJsons(rewardedVideoSettings, mergeJsons));
                        providerSettings.setInterstitialSettings(IronSourceUtils.mergeJsons(interstitialSettings, mergeJsons2));
                        providerSettings.setBannerSettings(IronSourceUtils.mergeJsons(bannerSettings, mergeJsons3));
                        providerSettings.setIsMultipleInstances(optBoolean);
                        providerSettings.setSubProviderId(optString);
                        providerSettings.setAdSourceNameForEvents(optString2);
                    } else if (shouldMergeWithDebugSettings(optString3)) {
                        ProviderSettings providerSettings2 = this.mProviderSettingsHolder.getProviderSettings("Mediation");
                        JSONObject rewardedVideoSettings2 = providerSettings2.getRewardedVideoSettings();
                        JSONObject interstitialSettings2 = providerSettings2.getInterstitialSettings();
                        JSONObject bannerSettings2 = providerSettings2.getBannerSettings();
                        JSONObject jSONObject = new JSONObject(rewardedVideoSettings2.toString());
                        JSONObject jSONObject2 = new JSONObject(interstitialSettings2.toString());
                        JSONObject jSONObject3 = new JSONObject(bannerSettings2.toString());
                        ProviderSettings providerSettings3 = new ProviderSettings(next, optString3, section3, IronSourceUtils.mergeJsons(jSONObject, mergeJsons), IronSourceUtils.mergeJsons(jSONObject2, mergeJsons2), IronSourceUtils.mergeJsons(jSONObject3, mergeJsons3));
                        providerSettings3.setIsMultipleInstances(optBoolean);
                        providerSettings3.setSubProviderId(optString);
                        providerSettings3.setAdSourceNameForEvents(optString2);
                        this.mProviderSettingsHolder.addProviderSettings(providerSettings3);
                    } else {
                        ProviderSettings providerSettings4 = new ProviderSettings(next, optString3, section3, mergeJsons, mergeJsons2, mergeJsons3);
                        providerSettings4.setIsMultipleInstances(optBoolean);
                        providerSettings4.setSubProviderId(optString);
                        providerSettings4.setAdSourceNameForEvents(optString2);
                        this.mProviderSettingsHolder.addProviderSettings(providerSettings4);
                    }
                }
            }
            this.mProviderSettingsHolder.fillSubProvidersDetails();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean shouldMergeWithDebugSettings(String str) {
        String lowerCase = str.toLowerCase();
        return this.mProviderSettingsHolder.containsProviderSettings("Mediation") && (IronSourceConstants.SUPERSONIC_CONFIG_NAME.toLowerCase().equals(lowerCase) || IronSourceConstants.IRONSOURCE_CONFIG_NAME.toLowerCase().equals(lowerCase) || IronSourceConstants.RIS_CONFIG_NAME.toLowerCase().equals(lowerCase));
    }

    private void parseConfigurations() {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        JSONObject jSONObject3;
        JSONObject jSONObject4;
        RewardedVideoConfigurations rewardedVideoConfigurations;
        InterstitialConfigurations interstitialConfigurations;
        BannerConfigurations bannerConfigurations;
        int i;
        OfferwallConfigurations offerwallConfigurations;
        ServerSegmetData serverSegmetData;
        JSONObject section;
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        AuctionSettings auctionSettings;
        int[] iArr4;
        AuctionSettings auctionSettings2;
        ServerResponseWrapper serverResponseWrapper = this;
        try {
            JSONObject section2 = serverResponseWrapper.getSection(serverResponseWrapper.mResponse, TapjoyConstants.PREF_SERVER_PROVIDED_CONFIGURATIONS);
            JSONObject section3 = serverResponseWrapper.getSection(section2, "adUnits");
            JSONObject section4 = serverResponseWrapper.getSection(section2, Constants.ParametersKeys.ORIENTATION_APPLICATION);
            JSONObject section5 = serverResponseWrapper.getSection(section3, "rewardedVideo");
            JSONObject section6 = serverResponseWrapper.getSection(section3, "interstitial");
            JSONObject section7 = serverResponseWrapper.getSection(section3, "offerwall");
            JSONObject section8 = serverResponseWrapper.getSection(section3, "banner");
            JSONObject section9 = serverResponseWrapper.getSection(section4, DataBaseEventsStorage.EventEntry.TABLE_NAME);
            JSONObject section10 = serverResponseWrapper.getSection(section4, "loggers");
            JSONObject section11 = serverResponseWrapper.getSection(section4, "segment");
            JSONObject section12 = serverResponseWrapper.getSection(section4, "auction");
            if (section4 != null) {
                IronSourceUtils.saveBooleanToSharedPrefs(serverResponseWrapper.mContext, DeviceStatus.UUID_ENABLED, section4.optBoolean(DeviceStatus.UUID_ENABLED, true));
            }
            if (section9 != null) {
                String optString = section9.optString("abt");
                if (!TextUtils.isEmpty(optString)) {
                    InterstitialEventsManager.getInstance().setABT(optString);
                    RewardedVideoEventsManager.getInstance().setABT(optString);
                }
            }
            if (section5 != null) {
                JSONArray optJSONArray = section5.optJSONArray("placements");
                JSONObject section13 = serverResponseWrapper.getSection(section5, DataBaseEventsStorage.EventEntry.TABLE_NAME);
                int intConfigValue = serverResponseWrapper.getIntConfigValue(section5, section4, "maxNumOfAdaptersToLoadOnStart", 2);
                int intConfigValue2 = serverResponseWrapper.getIntConfigValue(section5, section4, "adapterTimeOutInSeconds", 60);
                int intConfigValue3 = serverResponseWrapper.getIntConfigValue(section5, section4, "loadRVInterval", Strategy.TTL_SECONDS_DEFAULT);
                JSONObject mergeJsons = IronSourceUtils.mergeJsons(section13, section9);
                boolean optBoolean = mergeJsons.optBoolean("sendUltraEvents", false);
                boolean optBoolean2 = mergeJsons.optBoolean("sendEventsToggle", false);
                String optString2 = mergeJsons.optString("serverEventsURL", "");
                String optString3 = mergeJsons.optString("serverEventsType", "");
                int optInt = mergeJsons.optInt("backupThreshold", -1);
                int optInt2 = mergeJsons.optInt("maxNumberOfEvents", -1);
                int optInt3 = mergeJsons.optInt("maxEventsPerBatch", 5000);
                JSONArray optJSONArray2 = mergeJsons.optJSONArray("optOut");
                if (optJSONArray2 != null) {
                    int[] iArr5 = new int[optJSONArray2.length()];
                    for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                        iArr5[i2] = optJSONArray2.optInt(i2);
                    }
                    iArr4 = iArr5;
                } else {
                    iArr4 = null;
                }
                ApplicationEvents applicationEvents = new ApplicationEvents(optBoolean, optBoolean2, optString2, optString3, optInt, optInt2, optInt3, iArr4);
                if (section12 != null) {
                    try {
                        JSONObject section14 = serverResponseWrapper.getSection(section12, "rewardedVideo");
                        jSONObject4 = section11;
                        jSONObject3 = section10;
                        jSONObject2 = section7;
                        jSONObject = section8;
                        auctionSettings2 = new AuctionSettings(section12.optString("auctionData", ""), section12.optString("auctioneerURL", ""), section12.optInt(IronSourceConstants.AUCTION_TRIALS, 2), section12.optLong("auctionTimeout", 10000), section14.optBoolean(IronSourceConstants.EVENTS_PROGRAMMATIC, false), (long) section14.optInt("minTimeBeforeFirstAuction", 2000), (long) section14.optInt("auctionRetryInterval", NetworkConstants.UPLOAD_CONNECT_TIMEOUT), (long) section14.optInt("timeToWaitBeforeAuction", 5000), section14.optBoolean("isAuctionOnShowStart", true));
                    } catch (Exception e) {
                        e = e;
                        e.printStackTrace();
                    }
                } else {
                    jSONObject2 = section7;
                    jSONObject = section8;
                    jSONObject3 = section10;
                    jSONObject4 = section11;
                    auctionSettings2 = new AuctionSettings();
                }
                RewardedVideoConfigurations rewardedVideoConfigurations2 = new RewardedVideoConfigurations(intConfigValue, intConfigValue2, intConfigValue3, applicationEvents, auctionSettings2);
                if (optJSONArray != null) {
                    for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
                        Placement parseSingleRVPlacement = parseSingleRVPlacement(optJSONArray.optJSONObject(i3));
                        if (parseSingleRVPlacement != null) {
                            rewardedVideoConfigurations2.addRewardedVideoPlacement(parseSingleRVPlacement);
                        }
                    }
                }
                serverResponseWrapper = this;
                String optString4 = section5.optString("backFill");
                if (!TextUtils.isEmpty(optString4)) {
                    rewardedVideoConfigurations2.setBackFillProviderName(optString4);
                }
                String optString5 = section5.optString("premium");
                if (!TextUtils.isEmpty(optString5)) {
                    rewardedVideoConfigurations2.setPremiumProviderName(optString5);
                }
                rewardedVideoConfigurations = rewardedVideoConfigurations2;
            } else {
                jSONObject2 = section7;
                jSONObject = section8;
                jSONObject3 = section10;
                jSONObject4 = section11;
                rewardedVideoConfigurations = null;
            }
            if (section6 != null) {
                JSONArray optJSONArray3 = section6.optJSONArray("placements");
                JSONObject section15 = serverResponseWrapper.getSection(section6, DataBaseEventsStorage.EventEntry.TABLE_NAME);
                int intConfigValue4 = serverResponseWrapper.getIntConfigValue(section6, section4, "maxNumOfAdaptersToLoadOnStart", 2);
                int intConfigValue5 = serverResponseWrapper.getIntConfigValue(section6, section4, "adapterTimeOutInSeconds", 60);
                int intConfigValue6 = serverResponseWrapper.getIntConfigValue(section6, section4, "delayLoadFailure", 3);
                JSONObject mergeJsons2 = IronSourceUtils.mergeJsons(section15, section9);
                boolean optBoolean3 = mergeJsons2.optBoolean("sendEventsToggle", false);
                String optString6 = mergeJsons2.optString("serverEventsURL", "");
                String optString7 = mergeJsons2.optString("serverEventsType", "");
                int optInt4 = mergeJsons2.optInt("backupThreshold", -1);
                int optInt5 = mergeJsons2.optInt("maxNumberOfEvents", -1);
                int optInt6 = mergeJsons2.optInt("maxEventsPerBatch", 5000);
                JSONArray optJSONArray4 = mergeJsons2.optJSONArray("optOut");
                if (optJSONArray4 != null) {
                    int[] iArr6 = new int[optJSONArray4.length()];
                    for (int i4 = 0; i4 < optJSONArray4.length(); i4++) {
                        iArr6[i4] = optJSONArray4.optInt(i4);
                    }
                    iArr3 = iArr6;
                } else {
                    iArr3 = null;
                }
                ApplicationEvents applicationEvents2 = new ApplicationEvents(false, optBoolean3, optString6, optString7, optInt4, optInt5, optInt6, iArr3);
                if (section12 != null) {
                    JSONObject section16 = serverResponseWrapper.getSection(section12, "interstitial");
                    auctionSettings = new AuctionSettings(section12.optString("auctionData", ""), section12.optString("auctioneerURL", ""), section12.optInt(IronSourceConstants.AUCTION_TRIALS, 2), section12.optLong("auctionTimeout", 10000), section16.optBoolean(IronSourceConstants.EVENTS_PROGRAMMATIC, false), (long) section16.optInt("minTimeBeforeFirstAuction", 2000), 0, 0, true);
                } else {
                    auctionSettings = new AuctionSettings();
                }
                InterstitialConfigurations interstitialConfigurations2 = new InterstitialConfigurations(intConfigValue4, intConfigValue5, applicationEvents2, auctionSettings, intConfigValue6);
                if (optJSONArray3 != null) {
                    for (int i5 = 0; i5 < optJSONArray3.length(); i5++) {
                        InterstitialPlacement parseSingleISPlacement = serverResponseWrapper.parseSingleISPlacement(optJSONArray3.optJSONObject(i5));
                        if (parseSingleISPlacement != null) {
                            interstitialConfigurations2.addInterstitialPlacement(parseSingleISPlacement);
                        }
                    }
                }
                String optString8 = section6.optString("backFill");
                if (!TextUtils.isEmpty(optString8)) {
                    interstitialConfigurations2.setBackFillProviderName(optString8);
                }
                String optString9 = section6.optString("premium");
                if (!TextUtils.isEmpty(optString9)) {
                    interstitialConfigurations2.setPremiumProviderName(optString9);
                }
                interstitialConfigurations = interstitialConfigurations2;
            } else {
                interstitialConfigurations = null;
            }
            if (jSONObject != null) {
                JSONObject jSONObject5 = jSONObject;
                JSONArray optJSONArray5 = jSONObject5.optJSONArray("placements");
                JSONObject section17 = serverResponseWrapper.getSection(jSONObject5, DataBaseEventsStorage.EventEntry.TABLE_NAME);
                int intConfigValue7 = serverResponseWrapper.getIntConfigValue(jSONObject5, section4, "maxNumOfAdaptersToLoadOnStart", 1);
                i = -1;
                long longConfigValue = getLongConfigValue(jSONObject5, section4, "atim", 10000);
                int intConfigValue8 = serverResponseWrapper.getIntConfigValue(jSONObject5, section4, "delayLoadFailure", 3);
                int intConfigValue9 = serverResponseWrapper.getIntConfigValue(jSONObject5, section4, "bannerInterval", 60);
                JSONObject mergeJsons3 = IronSourceUtils.mergeJsons(section17, section9);
                boolean optBoolean4 = mergeJsons3.optBoolean("sendEventsToggle", false);
                String optString10 = mergeJsons3.optString("serverEventsURL", "");
                String optString11 = mergeJsons3.optString("serverEventsType", "");
                int optInt7 = mergeJsons3.optInt("backupThreshold", -1);
                int optInt8 = mergeJsons3.optInt("maxNumberOfEvents", -1);
                int optInt9 = mergeJsons3.optInt("maxEventsPerBatch", 5000);
                JSONArray optJSONArray6 = mergeJsons3.optJSONArray("optOut");
                if (optJSONArray6 != null) {
                    int[] iArr7 = new int[optJSONArray6.length()];
                    for (int i6 = 0; i6 < optJSONArray6.length(); i6++) {
                        iArr7[i6] = optJSONArray6.optInt(i6);
                    }
                    iArr2 = iArr7;
                } else {
                    iArr2 = null;
                }
                BannerConfigurations bannerConfigurations2 = new BannerConfigurations(intConfigValue7, longConfigValue, new ApplicationEvents(false, optBoolean4, optString10, optString11, optInt7, optInt8, optInt9, iArr2), intConfigValue9, intConfigValue8);
                if (optJSONArray5 != null) {
                    for (int i7 = 0; i7 < optJSONArray5.length(); i7++) {
                        BannerPlacement parseSingleBNPlacement = serverResponseWrapper.parseSingleBNPlacement(optJSONArray5.optJSONObject(i7));
                        if (parseSingleBNPlacement != null) {
                            bannerConfigurations2.addBannerPlacement(parseSingleBNPlacement);
                        }
                    }
                }
                bannerConfigurations = bannerConfigurations2;
            } else {
                i = -1;
                bannerConfigurations = null;
            }
            if (jSONObject2 != null) {
                JSONObject jSONObject6 = jSONObject2;
                JSONObject mergeJsons4 = IronSourceUtils.mergeJsons(serverResponseWrapper.getSection(jSONObject6, DataBaseEventsStorage.EventEntry.TABLE_NAME), section9);
                boolean optBoolean5 = mergeJsons4.optBoolean("sendEventsToggle", false);
                String optString12 = mergeJsons4.optString("serverEventsURL", "");
                String optString13 = mergeJsons4.optString("serverEventsType", "");
                int optInt10 = mergeJsons4.optInt("backupThreshold", i);
                int optInt11 = mergeJsons4.optInt("maxNumberOfEvents", i);
                int optInt12 = mergeJsons4.optInt("maxEventsPerBatch", 5000);
                JSONArray optJSONArray7 = mergeJsons4.optJSONArray("optOut");
                if (optJSONArray7 != null) {
                    int[] iArr8 = new int[optJSONArray7.length()];
                    for (int i8 = 0; i8 < optJSONArray7.length(); i8++) {
                        iArr8[i8] = optJSONArray7.optInt(i8);
                    }
                    iArr = iArr8;
                } else {
                    iArr = null;
                }
                OfferwallConfigurations offerwallConfigurations2 = new OfferwallConfigurations(new ApplicationEvents(false, optBoolean5, optString12, optString13, optInt10, optInt11, optInt12, iArr));
                JSONArray optJSONArray8 = jSONObject6.optJSONArray("placements");
                if (optJSONArray8 != null) {
                    for (int i9 = 0; i9 < optJSONArray8.length(); i9++) {
                        OfferwallPlacement parseSingleOWPlacement = serverResponseWrapper.parseSingleOWPlacement(optJSONArray8.optJSONObject(i9));
                        if (parseSingleOWPlacement != null) {
                            offerwallConfigurations2.addOfferwallPlacement(parseSingleOWPlacement);
                        }
                    }
                }
                offerwallConfigurations = offerwallConfigurations2;
            } else {
                offerwallConfigurations = null;
            }
            JSONObject jSONObject7 = jSONObject3;
            ApplicationLogger applicationLogger = new ApplicationLogger(jSONObject7.optInt("server", 3), jSONObject7.optInt("publisher", 3), jSONObject7.optInt(ConsoleLogger.NAME, 3));
            if (jSONObject4 != null) {
                JSONObject jSONObject8 = jSONObject4;
                serverSegmetData = new ServerSegmetData(jSONObject8.optString("name", ""), jSONObject8.optString("id", "-1"), jSONObject8.optJSONObject("custom"));
            } else {
                serverSegmetData = null;
            }
            ApplicationConfigurations applicationConfigurations = new ApplicationConfigurations(applicationLogger, serverSegmetData, section4.optBoolean("integration", false));
            IronSourceUtils.saveBooleanToSharedPrefs(serverResponseWrapper.mContext, GeneralProperties.ALLOW_LOCATION_SHARED_PREFS_KEY, section4.optBoolean("allowLocation", false));
            serverResponseWrapper.mConfigurations = new Configurations(rewardedVideoConfigurations, interstitialConfigurations, offerwallConfigurations, bannerConfigurations, applicationConfigurations);
            JSONObject section18 = serverResponseWrapper.getSection(section9, "genericParams");
            if (!(section18 == null || (section = serverResponseWrapper.getSection(section18, DataBaseEventsStorage.EventEntry.TABLE_NAME)) == null)) {
                section18.remove(DataBaseEventsStorage.EventEntry.TABLE_NAME);
                Map<String, String> parseJsonToStringMap = IronSourceUtils.parseJsonToStringMap(section);
                RewardedVideoEventsManager.getInstance().setEventGenericParams(parseJsonToStringMap);
                InterstitialEventsManager.getInstance().setEventGenericParams(parseJsonToStringMap);
            }
            if (section18 != null) {
                Map<String, String> parseJsonToStringMap2 = IronSourceUtils.parseJsonToStringMap(section18);
                RewardedVideoEventsManager.getInstance().setBatchParams(parseJsonToStringMap2);
                InterstitialEventsManager.getInstance().setBatchParams(parseJsonToStringMap2);
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
        }
    }

    private int getIntConfigValue(JSONObject jSONObject, JSONObject jSONObject2, String str, int i) {
        int i2;
        if (jSONObject.has(str)) {
            i2 = jSONObject.optInt(str, 0);
        } else {
            i2 = jSONObject2.has(str) ? jSONObject2.optInt(str, 0) : 0;
        }
        return i2 == 0 ? i : i2;
    }

    private long getLongConfigValue(JSONObject jSONObject, JSONObject jSONObject2, String str, long j) {
        long j2;
        if (jSONObject.has(str)) {
            j2 = jSONObject.optLong(str, 0);
        } else {
            j2 = jSONObject2.has(str) ? jSONObject2.optLong(str, 0) : 0;
        }
        return j2 == 0 ? j : j2;
    }

    private Placement parseSingleRVPlacement(JSONObject jSONObject) {
        if (jSONObject != null) {
            int optInt = jSONObject.optInt("placementId", -1);
            String optString = jSONObject.optString("placementName", "");
            boolean optBoolean = jSONObject.optBoolean("isDefault", false);
            String optString2 = jSONObject.optString("virtualItemName", "");
            int optInt2 = jSONObject.optInt("virtualItemCount", -1);
            PlacementAvailabilitySettings placementAvailabilitySettings = getPlacementAvailabilitySettings(jSONObject);
            if (optInt >= 0 && !TextUtils.isEmpty(optString) && !TextUtils.isEmpty(optString2) && optInt2 > 0) {
                Placement placement = new Placement(optInt, optString, optBoolean, optString2, optInt2, placementAvailabilitySettings);
                if (placementAvailabilitySettings == null) {
                    return placement;
                }
                CappingManager.addCappingInfo(this.mContext, placement);
                return placement;
            }
        }
        return null;
    }

    private InterstitialPlacement parseSingleISPlacement(JSONObject jSONObject) {
        if (jSONObject != null) {
            int optInt = jSONObject.optInt("placementId", -1);
            String optString = jSONObject.optString("placementName", "");
            boolean optBoolean = jSONObject.optBoolean("isDefault", false);
            PlacementAvailabilitySettings placementAvailabilitySettings = getPlacementAvailabilitySettings(jSONObject);
            if (optInt >= 0 && !TextUtils.isEmpty(optString)) {
                InterstitialPlacement interstitialPlacement = new InterstitialPlacement(optInt, optString, optBoolean, placementAvailabilitySettings);
                if (placementAvailabilitySettings == null) {
                    return interstitialPlacement;
                }
                CappingManager.addCappingInfo(this.mContext, interstitialPlacement);
                return interstitialPlacement;
            }
        }
        return null;
    }

    private OfferwallPlacement parseSingleOWPlacement(JSONObject jSONObject) {
        if (jSONObject != null) {
            int optInt = jSONObject.optInt("placementId", -1);
            String optString = jSONObject.optString("placementName", "");
            boolean optBoolean = jSONObject.optBoolean("isDefault", false);
            if (optInt >= 0 && !TextUtils.isEmpty(optString)) {
                return new OfferwallPlacement(optInt, optString, optBoolean);
            }
        }
        return null;
    }

    private BannerPlacement parseSingleBNPlacement(JSONObject jSONObject) {
        if (jSONObject != null) {
            int optInt = jSONObject.optInt("placementId", -1);
            String optString = jSONObject.optString("placementName", "");
            boolean optBoolean = jSONObject.optBoolean("isDefault", false);
            PlacementAvailabilitySettings placementAvailabilitySettings = getPlacementAvailabilitySettings(jSONObject);
            if (optInt >= 0 && !TextUtils.isEmpty(optString)) {
                BannerPlacement bannerPlacement = new BannerPlacement(optInt, optString, optBoolean, placementAvailabilitySettings);
                if (placementAvailabilitySettings == null) {
                    return bannerPlacement;
                }
                CappingManager.addCappingInfo(this.mContext, bannerPlacement);
                return bannerPlacement;
            }
        }
        return null;
    }

    private PlacementAvailabilitySettings getPlacementAvailabilitySettings(JSONObject jSONObject) {
        PlacementCappingType placementCappingType = null;
        if (jSONObject == null) {
            return null;
        }
        PlacementAvailabilitySettings.PlacementAvailabilitySettingsBuilder placementAvailabilitySettingsBuilder = new PlacementAvailabilitySettings.PlacementAvailabilitySettingsBuilder();
        boolean z = true;
        placementAvailabilitySettingsBuilder.delivery(jSONObject.optBoolean("delivery", true));
        JSONObject optJSONObject = jSONObject.optJSONObject("capping");
        if (optJSONObject != null) {
            String optString = optJSONObject.optString("unit");
            if (!TextUtils.isEmpty(optString)) {
                if (PlacementCappingType.PER_DAY.toString().equals(optString)) {
                    placementCappingType = PlacementCappingType.PER_DAY;
                } else if (PlacementCappingType.PER_HOUR.toString().equals(optString)) {
                    placementCappingType = PlacementCappingType.PER_HOUR;
                }
            }
            int optInt = optJSONObject.optInt("maxImpressions", 0);
            placementAvailabilitySettingsBuilder.capping(optJSONObject.optBoolean(TJAdUnitConstants.String.ENABLED, false) && optInt > 0, placementCappingType, optInt);
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject("pacing");
        if (optJSONObject2 != null) {
            int optInt2 = optJSONObject2.optInt("numOfSeconds", 0);
            if (!optJSONObject2.optBoolean(TJAdUnitConstants.String.ENABLED, false) || optInt2 <= 0) {
                z = false;
            }
            placementAvailabilitySettingsBuilder.pacing(z, optInt2);
        }
        return placementAvailabilitySettingsBuilder.build();
    }

    private JSONObject getSection(JSONObject jSONObject, String str) {
        if (jSONObject != null) {
            return jSONObject.optJSONObject(str);
        }
        return null;
    }

    public String getRVBackFillProvider() {
        try {
            return this.mProviderOrder.getRVBackFillProvider();
        } catch (Exception e) {
            IronSourceLoggerManager.getLogger().logException(IronSourceLogger.IronSourceTag.INTERNAL, "getRVBackFillProvider", e);
            return null;
        }
    }

    public String getRVPremiumProvider() {
        try {
            return this.mProviderOrder.getRVPremiumProvider();
        } catch (Exception e) {
            IronSourceLoggerManager.getLogger().logException(IronSourceLogger.IronSourceTag.INTERNAL, "getRVPremiumProvider", e);
            return null;
        }
    }

    public ProviderSettingsHolder getProviderSettingsHolder() {
        return this.mProviderSettingsHolder;
    }

    public ProviderOrder getProviderOrder() {
        return this.mProviderOrder;
    }

    public Configurations getConfigurations() {
        return this.mConfigurations;
    }

    private Context getContext() {
        return this.mContext;
    }
}
