package com.ironsource.mediationsdk.utils;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.mediationsdk.model.InterstitialPlacement;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.model.PlacementAvailabilitySettings;
import com.ironsource.mediationsdk.model.PlacementCappingType;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.Calendar;
import java.util.TimeZone;

public class CappingManager {
    private static final String CAPPING_TIME_THRESHOLD = "CappingManager.CAPPING_TIME_THRESHOLD";
    private static final String CAPPING_TYPE = "CappingManager.CAPPING_TYPE";
    private static final String CURRENT_NUMBER_OF_SHOWS = "CappingManager.CURRENT_NUMBER_OF_SHOWS";
    private static final String IS_CAPPING_ENABLED = "CappingManager.IS_CAPPING_ENABLED";
    private static final String IS_DELIVERY_ENABLED = "CappingManager.IS_DELIVERY_ENABLED";
    private static final String IS_PACING_ENABLED = "CappingManager.IS_PACING_ENABLED";
    private static final String MAX_NUMBER_OF_SHOWS = "CappingManager.MAX_NUMBER_OF_SHOWS";
    private static final String SECONDS_BETWEEN_SHOWS = "CappingManager.SECONDS_BETWEEN_SHOWS";
    private static final String TIME_OF_THE_PREVIOUS_SHOW = "CappingManager.TIME_OF_THE_PREVIOUS_SHOW";

    public enum ECappingStatus {
        CAPPED_PER_DELIVERY,
        CAPPED_PER_COUNT,
        CAPPED_PER_PACE,
        NOT_CAPPED
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x001f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void addCappingInfo(android.content.Context r3, com.ironsource.mediationsdk.model.InterstitialPlacement r4) {
        /*
            java.lang.Class<com.ironsource.mediationsdk.utils.CappingManager> r0 = com.ironsource.mediationsdk.utils.CappingManager.class
            monitor-enter(r0)
            if (r3 == 0) goto L_0x001e
            if (r4 != 0) goto L_0x0008
            goto L_0x001e
        L_0x0008:
            com.ironsource.mediationsdk.model.PlacementAvailabilitySettings r1 = r4.getPlacementAvailabilitySettings()     // Catch:{ all -> 0x001b }
            if (r1 != 0) goto L_0x0010
            monitor-exit(r0)
            return
        L_0x0010:
            java.lang.String r2 = "Interstitial"
            java.lang.String r4 = r4.getPlacementName()     // Catch:{ all -> 0x001b }
            addCappingInfo(r3, r2, r4, r1)     // Catch:{ all -> 0x001b }
            monitor-exit(r0)
            return
        L_0x001b:
            r3 = move-exception
            monitor-exit(r0)
            throw r3
        L_0x001e:
            monitor-exit(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.utils.CappingManager.addCappingInfo(android.content.Context, com.ironsource.mediationsdk.model.InterstitialPlacement):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x001f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void addCappingInfo(android.content.Context r3, com.ironsource.mediationsdk.model.Placement r4) {
        /*
            java.lang.Class<com.ironsource.mediationsdk.utils.CappingManager> r0 = com.ironsource.mediationsdk.utils.CappingManager.class
            monitor-enter(r0)
            if (r3 == 0) goto L_0x001e
            if (r4 != 0) goto L_0x0008
            goto L_0x001e
        L_0x0008:
            com.ironsource.mediationsdk.model.PlacementAvailabilitySettings r1 = r4.getPlacementAvailabilitySettings()     // Catch:{ all -> 0x001b }
            if (r1 != 0) goto L_0x0010
            monitor-exit(r0)
            return
        L_0x0010:
            java.lang.String r2 = "Rewarded Video"
            java.lang.String r4 = r4.getPlacementName()     // Catch:{ all -> 0x001b }
            addCappingInfo(r3, r2, r4, r1)     // Catch:{ all -> 0x001b }
            monitor-exit(r0)
            return
        L_0x001b:
            r3 = move-exception
            monitor-exit(r0)
            throw r3
        L_0x001e:
            monitor-exit(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.utils.CappingManager.addCappingInfo(android.content.Context, com.ironsource.mediationsdk.model.Placement):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x001f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void addCappingInfo(android.content.Context r3, com.ironsource.mediationsdk.model.BannerPlacement r4) {
        /*
            java.lang.Class<com.ironsource.mediationsdk.utils.CappingManager> r0 = com.ironsource.mediationsdk.utils.CappingManager.class
            monitor-enter(r0)
            if (r3 == 0) goto L_0x001e
            if (r4 != 0) goto L_0x0008
            goto L_0x001e
        L_0x0008:
            com.ironsource.mediationsdk.model.PlacementAvailabilitySettings r1 = r4.getPlacementAvailabilitySettings()     // Catch:{ all -> 0x001b }
            if (r1 != 0) goto L_0x0010
            monitor-exit(r0)
            return
        L_0x0010:
            java.lang.String r2 = "Banner"
            java.lang.String r4 = r4.getPlacementName()     // Catch:{ all -> 0x001b }
            addCappingInfo(r3, r2, r4, r1)     // Catch:{ all -> 0x001b }
            monitor-exit(r0)
            return
        L_0x001b:
            r3 = move-exception
            monitor-exit(r0)
            throw r3
        L_0x001e:
            monitor-exit(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.utils.CappingManager.addCappingInfo(android.content.Context, com.ironsource.mediationsdk.model.BannerPlacement):void");
    }

    public static synchronized ECappingStatus isPlacementCapped(Context context, InterstitialPlacement interstitialPlacement) {
        synchronized (CappingManager.class) {
            if (!(context == null || interstitialPlacement == null)) {
                if (interstitialPlacement.getPlacementAvailabilitySettings() != null) {
                    ECappingStatus isPlacementCapped = isPlacementCapped(context, "Interstitial", interstitialPlacement.getPlacementName());
                    return isPlacementCapped;
                }
            }
            ECappingStatus eCappingStatus = ECappingStatus.NOT_CAPPED;
            return eCappingStatus;
        }
    }

    public static synchronized boolean isInterstitialPlacementCapped(Context context, String str) {
        boolean z;
        synchronized (CappingManager.class) {
            z = isPlacementCapped(context, "Interstitial", str) != ECappingStatus.NOT_CAPPED;
        }
        return z;
    }

    public static synchronized boolean isBnPlacementCapped(Context context, String str) {
        boolean z;
        synchronized (CappingManager.class) {
            z = isPlacementCapped(context, IronSourceConstants.BANNER_AD_UNIT, str) != ECappingStatus.NOT_CAPPED;
        }
        return z;
    }

    public static synchronized ECappingStatus isPlacementCapped(Context context, Placement placement) {
        synchronized (CappingManager.class) {
            if (!(context == null || placement == null)) {
                if (placement.getPlacementAvailabilitySettings() != null) {
                    ECappingStatus isPlacementCapped = isPlacementCapped(context, IronSourceConstants.REWARDED_VIDEO_AD_UNIT, placement.getPlacementName());
                    return isPlacementCapped;
                }
            }
            ECappingStatus eCappingStatus = ECappingStatus.NOT_CAPPED;
            return eCappingStatus;
        }
    }

    public static synchronized boolean isRvPlacementCapped(Context context, String str) {
        boolean z;
        synchronized (CappingManager.class) {
            z = isPlacementCapped(context, IronSourceConstants.REWARDED_VIDEO_AD_UNIT, str) != ECappingStatus.NOT_CAPPED;
        }
        return z;
    }

    public static synchronized void incrementShowCounter(Context context, InterstitialPlacement interstitialPlacement) {
        synchronized (CappingManager.class) {
            if (interstitialPlacement != null) {
                incrementShowCounter(context, "Interstitial", interstitialPlacement.getPlacementName());
            }
        }
    }

    public static synchronized void incrementIsShowCounter(Context context, String str) {
        synchronized (CappingManager.class) {
            incrementShowCounter(context, "Interstitial", str);
        }
    }

    public static synchronized void incrementShowCounter(Context context, Placement placement) {
        synchronized (CappingManager.class) {
            if (placement != null) {
                incrementShowCounter(context, IronSourceConstants.REWARDED_VIDEO_AD_UNIT, placement.getPlacementName());
            }
        }
    }

    public static synchronized void incrementRvShowCounter(Context context, String str) {
        synchronized (CappingManager.class) {
            incrementShowCounter(context, IronSourceConstants.REWARDED_VIDEO_AD_UNIT, str);
        }
    }

    public static synchronized void incrementBnShowCounter(Context context, String str) {
        synchronized (CappingManager.class) {
            if (!TextUtils.isEmpty(str)) {
                incrementShowCounter(context, IronSourceConstants.BANNER_AD_UNIT, str);
            }
        }
    }

    private static String constructSharedPrefsKey(String str, String str2, String str3) {
        return str + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + str2 + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + str3;
    }

    private static ECappingStatus isPlacementCapped(Context context, String str, String str2) {
        long currentTimeMillis = System.currentTimeMillis();
        if (!IronSourceUtils.getBooleanFromSharedPrefs(context, constructSharedPrefsKey(str, IS_DELIVERY_ENABLED, str2), true)) {
            return ECappingStatus.CAPPED_PER_DELIVERY;
        }
        if (IronSourceUtils.getBooleanFromSharedPrefs(context, constructSharedPrefsKey(str, IS_PACING_ENABLED, str2), false)) {
            if (currentTimeMillis - IronSourceUtils.getLongFromSharedPrefs(context, constructSharedPrefsKey(str, TIME_OF_THE_PREVIOUS_SHOW, str2), 0) < ((long) (IronSourceUtils.getIntFromSharedPrefs(context, constructSharedPrefsKey(str, SECONDS_BETWEEN_SHOWS, str2), 0) * 1000))) {
                return ECappingStatus.CAPPED_PER_PACE;
            }
        }
        if (IronSourceUtils.getBooleanFromSharedPrefs(context, constructSharedPrefsKey(str, IS_CAPPING_ENABLED, str2), false)) {
            int intFromSharedPrefs = IronSourceUtils.getIntFromSharedPrefs(context, constructSharedPrefsKey(str, MAX_NUMBER_OF_SHOWS, str2), 0);
            String constructSharedPrefsKey = constructSharedPrefsKey(str, CURRENT_NUMBER_OF_SHOWS, str2);
            int intFromSharedPrefs2 = IronSourceUtils.getIntFromSharedPrefs(context, constructSharedPrefsKey, 0);
            String constructSharedPrefsKey2 = constructSharedPrefsKey(str, CAPPING_TIME_THRESHOLD, str2);
            if (currentTimeMillis >= IronSourceUtils.getLongFromSharedPrefs(context, constructSharedPrefsKey2, 0)) {
                IronSourceUtils.saveIntToSharedPrefs(context, constructSharedPrefsKey, 0);
                IronSourceUtils.saveLongToSharedPrefs(context, constructSharedPrefsKey2, 0);
            } else if (intFromSharedPrefs2 >= intFromSharedPrefs) {
                return ECappingStatus.CAPPED_PER_COUNT;
            }
        }
        return ECappingStatus.NOT_CAPPED;
    }

    private static void incrementShowCounter(Context context, String str, String str2) {
        int i = 0;
        if (IronSourceUtils.getBooleanFromSharedPrefs(context, constructSharedPrefsKey(str, IS_PACING_ENABLED, str2), false)) {
            IronSourceUtils.saveLongToSharedPrefs(context, constructSharedPrefsKey(str, TIME_OF_THE_PREVIOUS_SHOW, str2), System.currentTimeMillis());
        }
        if (IronSourceUtils.getBooleanFromSharedPrefs(context, constructSharedPrefsKey(str, IS_CAPPING_ENABLED, str2), false)) {
            IronSourceUtils.getIntFromSharedPrefs(context, constructSharedPrefsKey(str, MAX_NUMBER_OF_SHOWS, str2), 0);
            String constructSharedPrefsKey = constructSharedPrefsKey(str, CURRENT_NUMBER_OF_SHOWS, str2);
            int intFromSharedPrefs = IronSourceUtils.getIntFromSharedPrefs(context, constructSharedPrefsKey, 0);
            if (intFromSharedPrefs == 0) {
                String stringFromSharedPrefs = IronSourceUtils.getStringFromSharedPrefs(context, constructSharedPrefsKey(str, CAPPING_TYPE, str2), PlacementCappingType.PER_DAY.toString());
                PlacementCappingType placementCappingType = null;
                PlacementCappingType[] values = PlacementCappingType.values();
                int length = values.length;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    PlacementCappingType placementCappingType2 = values[i];
                    if (placementCappingType2.value.equals(stringFromSharedPrefs)) {
                        placementCappingType = placementCappingType2;
                        break;
                    }
                    i++;
                }
                IronSourceUtils.saveLongToSharedPrefs(context, constructSharedPrefsKey(str, CAPPING_TIME_THRESHOLD, str2), initTimeThreshold(placementCappingType));
            }
            IronSourceUtils.saveIntToSharedPrefs(context, constructSharedPrefsKey, intFromSharedPrefs + 1);
        }
    }

    private static long initTimeThreshold(PlacementCappingType placementCappingType) {
        Calendar instance = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        switch (placementCappingType) {
            case PER_DAY:
                instance.set(14, 0);
                instance.set(13, 0);
                instance.set(12, 0);
                instance.set(11, 0);
                instance.add(6, 1);
                break;
            case PER_HOUR:
                instance.set(14, 0);
                instance.set(13, 0);
                instance.set(12, 0);
                instance.add(11, 1);
                break;
        }
        return instance.getTimeInMillis();
    }

    private static void addCappingInfo(Context context, String str, String str2, PlacementAvailabilitySettings placementAvailabilitySettings) {
        boolean isDeliveryEnabled = placementAvailabilitySettings.isDeliveryEnabled();
        IronSourceUtils.saveBooleanToSharedPrefs(context, constructSharedPrefsKey(str, IS_DELIVERY_ENABLED, str2), isDeliveryEnabled);
        if (isDeliveryEnabled) {
            boolean isCappingEnabled = placementAvailabilitySettings.isCappingEnabled();
            IronSourceUtils.saveBooleanToSharedPrefs(context, constructSharedPrefsKey(str, IS_CAPPING_ENABLED, str2), isCappingEnabled);
            if (isCappingEnabled) {
                IronSourceUtils.saveIntToSharedPrefs(context, constructSharedPrefsKey(str, MAX_NUMBER_OF_SHOWS, str2), placementAvailabilitySettings.getCappingValue());
                IronSourceUtils.saveStringToSharedPrefs(context, constructSharedPrefsKey(str, CAPPING_TYPE, str2), placementAvailabilitySettings.getCappingType().toString());
            }
            boolean isPacingEnabled = placementAvailabilitySettings.isPacingEnabled();
            IronSourceUtils.saveBooleanToSharedPrefs(context, constructSharedPrefsKey(str, IS_PACING_ENABLED, str2), isPacingEnabled);
            if (isPacingEnabled) {
                IronSourceUtils.saveIntToSharedPrefs(context, constructSharedPrefsKey(str, SECONDS_BETWEEN_SHOWS, str2), placementAvailabilitySettings.getPacingValue());
            }
        }
    }
}
