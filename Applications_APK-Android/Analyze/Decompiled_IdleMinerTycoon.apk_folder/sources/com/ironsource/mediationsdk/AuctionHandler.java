package com.ironsource.mediationsdk;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import android.text.TextUtils;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.ironsource.environment.ApplicationContext;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.mediationsdk.utils.AuctionSettings;
import com.ironsource.mediationsdk.utils.IronSourceAES;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.sdk.constants.Constants;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AuctionHandler {
    private static final int SERVER_REQUEST_TIMEOUT = 15000;
    private final String AUCTION_INTERNAL_ERROR_LOSS_CODE = "1";
    private final String AUCTION_LOSS_MACRO = "${AUCTION_LOSS}";
    private final String AUCTION_NOT_HIGHEST_RTB_BIDDER_LOSS_CODE = "102";
    private final String AUCTION_PRICE_MACRO = "${AUCTION_PRICE}";
    private String mAdUnit;
    private AuctionEventListener mAuctionListener;
    private String mSessionId;
    private AuctionSettings mSettings;

    public AuctionHandler(String str, AuctionSettings auctionSettings, AuctionEventListener auctionEventListener) {
        this.mAdUnit = str;
        this.mSettings = auctionSettings;
        this.mAuctionListener = auctionEventListener;
        this.mSessionId = IronSourceObject.getInstance().getSessionId();
    }

    public void executeAuction(Context context, Map<String, Object> map, List<String> list, int i) {
        try {
            boolean z = IronSourceUtils.getSerr() == 1;
            new AuctionHttpRequestTask(this.mAuctionListener).execute(this.mSettings.getUrl(), generateRequest(context, map, list, i, z), Boolean.valueOf(z), Integer.valueOf(this.mSettings.getNumOfMaxTrials()), Long.valueOf(this.mSettings.getTrialsInterval()));
        } catch (Exception e) {
            this.mAuctionListener.onAuctionFailed(1000, e.getMessage(), 0, FacebookRequestErrorClassification.KEY_OTHER, 0);
        }
    }

    public void reportImpression(AuctionResponseItem auctionResponseItem) {
        for (String str : auctionResponseItem.getBurls()) {
            new ImpressionHttpTask().execute(str);
        }
    }

    public void reportLoadSuccess(AuctionResponseItem auctionResponseItem) {
        for (String str : auctionResponseItem.getNurls()) {
            new ImpressionHttpTask().execute(str);
        }
    }

    public void reportAuctionLose(CopyOnWriteArrayList<ProgSmash> copyOnWriteArrayList, ConcurrentHashMap<String, AuctionResponseItem> concurrentHashMap, AuctionResponseItem auctionResponseItem) {
        String price = auctionResponseItem.getPrice();
        Iterator<ProgSmash> it = copyOnWriteArrayList.iterator();
        boolean z = false;
        while (it.hasNext()) {
            String instanceName = it.next().getInstanceName();
            if (instanceName.equals(auctionResponseItem.getInstanceName())) {
                z = true;
            } else {
                for (String replace : concurrentHashMap.get(instanceName).getLurls()) {
                    new ImpressionHttpTask().execute(replace.replace("${AUCTION_PRICE}", price).replace("${AUCTION_LOSS}", z ? "102" : "1"));
                }
            }
        }
    }

    private JSONObject generateRequest(Context context, Map<String, Object> map, List<String> list, int i, boolean z) throws JSONException {
        boolean z2;
        JSONObject jSONObject = new JSONObject();
        for (String next : map.keySet()) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(IronSourceConstants.EVENTS_INSTANCE_TYPE, 2);
            jSONObject2.put("biddingAdditionalData", new JSONObject((Map) map.get(next)));
            jSONObject.put(next, jSONObject2);
        }
        for (String put : list) {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put(IronSourceConstants.EVENTS_INSTANCE_TYPE, 1);
            jSONObject.put(put, jSONObject3);
        }
        JSONObject jSONObject4 = new JSONObject();
        jSONObject4.put(Constants.RequestParameters.APPLICATION_USER_ID, IronSourceObject.getInstance().getIronSourceUserId());
        String gender = IronSourceObject.getInstance().getGender();
        if (TextUtils.isEmpty(gender)) {
            gender = "unknown";
        }
        jSONObject4.put("applicationUserGender", gender);
        int age = IronSourceObject.getInstance().getAge();
        if (age == null) {
            age = -1;
        }
        jSONObject4.put("applicationUserAge", age);
        Boolean consent = IronSourceObject.getInstance().getConsent();
        if (consent != null) {
            jSONObject4.put(Constants.RequestParameters.CONSENT, consent.booleanValue() ? 1 : 0);
        }
        jSONObject4.put(Constants.RequestParameters.MOBILE_CARRIER, DeviceStatus.getMobileCarrier(context));
        jSONObject4.put(Constants.RequestParameters.CONNECTION_TYPE, IronSourceUtils.getConnectionType(context));
        jSONObject4.put("deviceOS", "android");
        jSONObject4.put("deviceWidth", context.getResources().getConfiguration().screenWidthDp);
        jSONObject4.put("deviceHeight", context.getResources().getConfiguration().screenHeightDp);
        jSONObject4.put(Constants.RequestParameters.DEVICE_OS_VERSION, Build.VERSION.SDK_INT + "(" + Build.VERSION.RELEASE + ")");
        jSONObject4.put(Constants.RequestParameters.DEVICE_MODEL, Build.MODEL);
        jSONObject4.put("deviceMake", Build.MANUFACTURER);
        jSONObject4.put(Constants.RequestParameters.PACKAGE_NAME, context.getPackageName());
        jSONObject4.put(Constants.RequestParameters.APPLICATION_VERSION_NAME, ApplicationContext.getPublisherApplicationVersion(context, context.getPackageName()));
        jSONObject4.put("clientTimestamp", new Date().getTime());
        try {
            String orGenerateOnceUniqueIdentifier = DeviceStatus.getOrGenerateOnceUniqueIdentifier(context);
            String str = IronSourceConstants.TYPE_UUID;
            String[] advertisingIdInfo = DeviceStatus.getAdvertisingIdInfo(context);
            if (advertisingIdInfo.length == 2) {
                z2 = Boolean.valueOf(advertisingIdInfo[1]).booleanValue();
                if (!TextUtils.isEmpty(advertisingIdInfo[0])) {
                    orGenerateOnceUniqueIdentifier = advertisingIdInfo[0];
                    str = IronSourceConstants.TYPE_GAID;
                }
            } else {
                z2 = false;
            }
            jSONObject4.put("advId", orGenerateOnceUniqueIdentifier);
            jSONObject4.put("advIdType", str);
            jSONObject4.put(Constants.RequestParameters.isLAT, z2 ? "true" : "false");
        } catch (Exception unused) {
        }
        JSONObject jSONObject5 = new JSONObject();
        jSONObject5.put("adUnit", this.mAdUnit);
        jSONObject5.put("auctionData", this.mSettings.getAuctionData());
        jSONObject5.put(Constants.RequestParameters.APPLICATION_KEY, IronSourceObject.getInstance().getIronSourceAppKey());
        jSONObject5.put(Constants.RequestParameters.SDK_VERSION, IronSourceUtils.getSDKVersion());
        jSONObject5.put("clientParams", jSONObject4);
        jSONObject5.put("sessionDepth", i);
        jSONObject5.put("sessionId", this.mSessionId);
        jSONObject5.put("doNotEncryptResponse", z ? "false" : "true");
        jSONObject5.put("instances", jSONObject);
        return jSONObject5;
    }

    static class AuctionHttpRequestTask extends AsyncTask<Object, Void, Boolean> {
        private String mAuctionFallback = FacebookRequestErrorClassification.KEY_OTHER;
        private String mAuctionId;
        private WeakReference<AuctionEventListener> mAuctionListener;
        private int mCurrentAuctionTrial;
        private int mErrorCode;
        private String mErrorMessage;
        private JSONObject mRequestData;
        private long mRequestStartTime;
        private List<AuctionResponseItem> mWaterfall;

        AuctionHttpRequestTask(AuctionEventListener auctionEventListener) {
            this.mAuctionListener = new WeakReference<>(auctionEventListener);
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00c3  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x00dc  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Boolean doInBackground(java.lang.Object... r14) {
            /*
                r13 = this;
                java.util.Date r0 = new java.util.Date
                r0.<init>()
                long r0 = r0.getTime()
                r13.mRequestStartTime = r0
                r0 = 0
                java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x00fb }
                r2 = r14[r0]     // Catch:{ Exception -> 0x00fb }
                java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x00fb }
                r1.<init>(r2)     // Catch:{ Exception -> 0x00fb }
                r2 = 1
                r3 = r14[r2]     // Catch:{ Exception -> 0x00fb }
                org.json.JSONObject r3 = (org.json.JSONObject) r3     // Catch:{ Exception -> 0x00fb }
                r13.mRequestData = r3     // Catch:{ Exception -> 0x00fb }
                r3 = 2
                r3 = r14[r3]     // Catch:{ Exception -> 0x00fb }
                java.lang.Boolean r3 = (java.lang.Boolean) r3     // Catch:{ Exception -> 0x00fb }
                boolean r3 = r3.booleanValue()     // Catch:{ Exception -> 0x00fb }
                r4 = 3
                r4 = r14[r4]     // Catch:{ Exception -> 0x00fb }
                java.lang.Integer r4 = (java.lang.Integer) r4     // Catch:{ Exception -> 0x00fb }
                int r4 = r4.intValue()     // Catch:{ Exception -> 0x00fb }
                r5 = 4
                r14 = r14[r5]     // Catch:{ Exception -> 0x00fb }
                java.lang.Long r14 = (java.lang.Long) r14     // Catch:{ Exception -> 0x00fb }
                long r5 = r14.longValue()     // Catch:{ Exception -> 0x00fb }
                r13.mCurrentAuctionTrial = r0
                r14 = 0
            L_0x003a:
                int r7 = r13.mCurrentAuctionTrial
                if (r7 >= r4) goto L_0x00ef
                java.util.Date r7 = new java.util.Date     // Catch:{ SocketTimeoutException -> 0x00d9, Exception -> 0x00be }
                r7.<init>()     // Catch:{ SocketTimeoutException -> 0x00d9, Exception -> 0x00be }
                long r7 = r7.getTime()     // Catch:{ SocketTimeoutException -> 0x00d9, Exception -> 0x00be }
                com.ironsource.mediationsdk.logger.IronSourceLoggerManager r9 = com.ironsource.mediationsdk.logger.IronSourceLoggerManager.getLogger()     // Catch:{ SocketTimeoutException -> 0x00d9, Exception -> 0x00be }
                com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r10 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.INTERNAL     // Catch:{ SocketTimeoutException -> 0x00d9, Exception -> 0x00be }
                java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x00d9, Exception -> 0x00be }
                r11.<init>()     // Catch:{ SocketTimeoutException -> 0x00d9, Exception -> 0x00be }
                java.lang.String r12 = "Auction Handler: auction trial "
                r11.append(r12)     // Catch:{ SocketTimeoutException -> 0x00d9, Exception -> 0x00be }
                int r12 = r13.mCurrentAuctionTrial     // Catch:{ SocketTimeoutException -> 0x00d9, Exception -> 0x00be }
                int r12 = r12 + r2
                r11.append(r12)     // Catch:{ SocketTimeoutException -> 0x00d9, Exception -> 0x00be }
                java.lang.String r12 = " out of "
                r11.append(r12)     // Catch:{ SocketTimeoutException -> 0x00d9, Exception -> 0x00be }
                r11.append(r4)     // Catch:{ SocketTimeoutException -> 0x00d9, Exception -> 0x00be }
                java.lang.String r12 = " max trials"
                r11.append(r12)     // Catch:{ SocketTimeoutException -> 0x00d9, Exception -> 0x00be }
                java.lang.String r11 = r11.toString()     // Catch:{ SocketTimeoutException -> 0x00d9, Exception -> 0x00be }
                r9.log(r10, r11, r0)     // Catch:{ SocketTimeoutException -> 0x00d9, Exception -> 0x00be }
                java.net.HttpURLConnection r9 = r13.prepareAuctionRequest(r1, r5)     // Catch:{ SocketTimeoutException -> 0x00d9, Exception -> 0x00be }
                org.json.JSONObject r14 = r13.mRequestData     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                r13.sendAuctionRequest(r9, r14)     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                int r14 = r9.getResponseCode()     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                r10 = 200(0xc8, float:2.8E-43)
                if (r14 == r10) goto L_0x0099
                r10 = 1001(0x3e9, float:1.403E-42)
                r13.mErrorCode = r10     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                java.lang.String r14 = java.lang.String.valueOf(r14)     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                r13.mErrorMessage = r14     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                r9.disconnect()     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                int r14 = r13.mCurrentAuctionTrial     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                int r10 = r4 + -1
                if (r14 >= r10) goto L_0x00e7
                r13.waitUntilNextTrial(r5, r7)     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                goto L_0x00e7
            L_0x0099:
                java.lang.String r14 = r13.readResponse(r9)     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                r13.handleResponse(r14, r3)     // Catch:{ JSONException -> 0x00a8 }
                r9.disconnect()     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                java.lang.Boolean r14 = java.lang.Boolean.valueOf(r2)     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                return r14
            L_0x00a8:
                r14 = 1002(0x3ea, float:1.404E-42)
                r13.mErrorCode = r14     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                java.lang.String r14 = "failed parsing auction response"
                r13.mErrorMessage = r14     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                java.lang.String r14 = "parsing"
                r13.mAuctionFallback = r14     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                r9.disconnect()     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                java.lang.Boolean r14 = java.lang.Boolean.valueOf(r0)     // Catch:{ SocketTimeoutException -> 0x00da, Exception -> 0x00bc }
                return r14
            L_0x00bc:
                r14 = move-exception
                goto L_0x00c1
            L_0x00be:
                r1 = move-exception
                r9 = r14
                r14 = r1
            L_0x00c1:
                if (r9 == 0) goto L_0x00c6
                r9.disconnect()
            L_0x00c6:
                r1 = 1000(0x3e8, float:1.401E-42)
                r13.mErrorCode = r1
                java.lang.String r14 = r14.getMessage()
                r13.mErrorMessage = r14
                java.lang.String r14 = "other"
                r13.mAuctionFallback = r14
                java.lang.Boolean r14 = java.lang.Boolean.valueOf(r0)
                return r14
            L_0x00d9:
                r9 = r14
            L_0x00da:
                if (r9 == 0) goto L_0x00df
                r9.disconnect()
            L_0x00df:
                r14 = 1006(0x3ee, float:1.41E-42)
                r13.mErrorCode = r14
                java.lang.String r14 = "Connection timed out"
                r13.mErrorMessage = r14
            L_0x00e7:
                r14 = r9
                int r7 = r13.mCurrentAuctionTrial
                int r7 = r7 + r2
                r13.mCurrentAuctionTrial = r7
                goto L_0x003a
            L_0x00ef:
                int r4 = r4 - r2
                r13.mCurrentAuctionTrial = r4
                java.lang.String r14 = "trials_fail"
                r13.mAuctionFallback = r14
                java.lang.Boolean r14 = java.lang.Boolean.valueOf(r0)
                return r14
            L_0x00fb:
                r14 = move-exception
                r1 = 1007(0x3ef, float:1.411E-42)
                r13.mErrorCode = r1
                java.lang.String r14 = r14.getMessage()
                r13.mErrorMessage = r14
                r13.mCurrentAuctionTrial = r0
                java.lang.String r14 = "other"
                r13.mAuctionFallback = r14
                java.lang.Boolean r14 = java.lang.Boolean.valueOf(r0)
                return r14
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.AuctionHandler.AuctionHttpRequestTask.doInBackground(java.lang.Object[]):java.lang.Boolean");
        }

        private void waitUntilNextTrial(long j, long j2) {
            long time = j - (new Date().getTime() - j2);
            if (time > 0) {
                SystemClock.sleep(time);
            }
        }

        private void sendAuctionRequest(HttpURLConnection httpURLConnection, JSONObject jSONObject) throws IOException {
            OutputStream outputStream = httpURLConnection.getOutputStream();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            bufferedWriter.write(String.format("{\"request\" : \"%1$s\"}", IronSourceAES.encode(IronSourceUtils.KEY, jSONObject.toString())));
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStreamWriter.close();
            outputStream.close();
        }

        private HttpURLConnection prepareAuctionRequest(URL url, long j) throws IOException {
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
            httpURLConnection.setRequestProperty(HttpRequest.HEADER_CONTENT_TYPE, "application/json; charset=utf-8");
            httpURLConnection.setReadTimeout((int) j);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            return httpURLConnection;
        }

        private void handleResponse(String str, boolean z) throws JSONException {
            if (!TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                if (z) {
                    jSONObject = new JSONObject(IronSourceAES.decode(IronSourceUtils.KEY, jSONObject.getString("response")));
                }
                this.mAuctionId = jSONObject.getString(IronSourceConstants.EVENTS_AUCTION_ID);
                this.mWaterfall = new ArrayList();
                JSONArray jSONArray = jSONObject.getJSONArray("waterfall");
                int i = 0;
                while (i < jSONArray.length()) {
                    AuctionResponseItem auctionResponseItem = new AuctionResponseItem(jSONArray.getJSONObject(i));
                    if (auctionResponseItem.isValid()) {
                        this.mWaterfall.add(auctionResponseItem);
                        i++;
                    } else {
                        this.mErrorCode = 1002;
                        this.mErrorMessage = "waterfall " + i;
                        throw new JSONException("invalid response");
                    }
                }
                return;
            }
            throw new JSONException("empty response");
        }

        private String readResponse(HttpURLConnection httpURLConnection) throws IOException {
            InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine);
                } else {
                    bufferedReader.close();
                    inputStreamReader.close();
                    return sb.toString();
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            AuctionEventListener auctionEventListener = this.mAuctionListener.get();
            if (auctionEventListener != null) {
                long time = new Date().getTime() - this.mRequestStartTime;
                if (bool.booleanValue()) {
                    auctionEventListener.onAuctionSuccess(this.mWaterfall, this.mAuctionId, this.mCurrentAuctionTrial + 1, time);
                } else {
                    auctionEventListener.onAuctionFailed(this.mErrorCode, this.mErrorMessage, this.mCurrentAuctionTrial + 1, this.mAuctionFallback, time);
                }
            }
        }
    }

    static class ImpressionHttpTask extends AsyncTask<String, Void, Boolean> {
        ImpressionHttpTask() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... strArr) {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(strArr[0]).openConnection();
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                httpURLConnection.setReadTimeout(AuctionHandler.SERVER_REQUEST_TIMEOUT);
                httpURLConnection.setConnectTimeout(AuctionHandler.SERVER_REQUEST_TIMEOUT);
                httpURLConnection.connect();
                int responseCode = httpURLConnection.getResponseCode();
                httpURLConnection.disconnect();
                return Boolean.valueOf(responseCode == 200);
            } catch (Exception unused) {
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute((Object) bool);
        }
    }
}
