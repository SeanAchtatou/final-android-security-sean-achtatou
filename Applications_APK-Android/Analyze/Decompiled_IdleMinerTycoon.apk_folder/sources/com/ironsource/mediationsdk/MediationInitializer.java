package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.IntentFilter;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.text.TextUtils;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.environment.NetworkStateReceiver;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.IronSourceObject;
import com.ironsource.mediationsdk.config.ConfigValidationResult;
import com.ironsource.mediationsdk.integration.IntegrationHelper;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ServerSegmetData;
import com.ironsource.mediationsdk.sdk.GeneralProperties;
import com.ironsource.mediationsdk.sdk.SegmentListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.mintegral.msdk.interstitial.view.MTGInterstitialActivity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

class MediationInitializer implements NetworkStateReceiver.NetworkStateReceiverListener {
    private static MediationInitializer sInstance;
    private final String GENERAL_PROPERTIES_APP_KEY;
    private final String GENERAL_PROPERTIES_USER_ID;
    private final String TAG;
    private InitRunnable initRunnable;
    /* access modifiers changed from: private */
    public Activity mActivity;
    /* access modifiers changed from: private */
    public String mAppKey;
    private AtomicBoolean mAtomicShouldPerformInit;
    /* access modifiers changed from: private */
    public CountDownTimer mCountDownTimer;
    /* access modifiers changed from: private */
    public boolean mDidReportInitialAvailability;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private HandlerThread mHandlerThread;
    /* access modifiers changed from: private */
    public long mInitStartTime;
    private EInitStatus mInitStatus;
    /* access modifiers changed from: private */
    public boolean mIsInProgressMoreThan15Secs;
    /* access modifiers changed from: private */
    public boolean mIsRevived;
    private boolean mListenForInit;
    private NetworkStateReceiver mNetworkStateReceiver;
    /* access modifiers changed from: private */
    public List<OnMediationInitializationListener> mOnMediationInitializationListeners;
    /* access modifiers changed from: private */
    public int mRetryAvailabilityLimit;
    /* access modifiers changed from: private */
    public int mRetryCounter;
    /* access modifiers changed from: private */
    public int mRetryDelay;
    /* access modifiers changed from: private */
    public int mRetryGrowLimit;
    /* access modifiers changed from: private */
    public int mRetryLimit;
    /* access modifiers changed from: private */
    public SegmentListener mSegmentListener;
    /* access modifiers changed from: private */
    public ServerResponseWrapper mServerResponseWrapper;
    /* access modifiers changed from: private */
    public String mUserId;
    /* access modifiers changed from: private */
    public String mUserIdType;

    enum EInitStatus {
        NOT_INIT,
        INIT_IN_PROGRESS,
        INIT_FAILED,
        INITIATED
    }

    interface OnMediationInitializationListener {
        void onInitFailed(String str);

        void onInitSuccess(List<IronSource.AD_UNIT> list, boolean z);

        void onStillInProgressAfter15Secs();
    }

    static /* synthetic */ int access$1308(MediationInitializer mediationInitializer) {
        int i = mediationInitializer.mRetryCounter;
        mediationInitializer.mRetryCounter = i + 1;
        return i;
    }

    abstract class InitRunnable implements Runnable {
        boolean isRecoverable = true;
        protected IronSourceObject.IResponseListener listener = new IronSourceObject.IResponseListener() {
            public void onUnrecoverableError(String str) {
                InitRunnable.this.isRecoverable = false;
                InitRunnable.this.reason = str;
            }
        };
        String reason;

        InitRunnable() {
        }
    }

    public static synchronized MediationInitializer getInstance() {
        MediationInitializer mediationInitializer;
        synchronized (MediationInitializer.class) {
            if (sInstance == null) {
                sInstance = new MediationInitializer();
            }
            mediationInitializer = sInstance;
        }
        return mediationInitializer;
    }

    private MediationInitializer() {
        this.GENERAL_PROPERTIES_USER_ID = "userId";
        this.GENERAL_PROPERTIES_APP_KEY = ServerResponseWrapper.APP_KEY_FIELD;
        this.TAG = getClass().getSimpleName();
        this.mDidReportInitialAvailability = false;
        this.mHandlerThread = null;
        this.mListenForInit = false;
        this.mOnMediationInitializationListeners = new ArrayList();
        this.initRunnable = new InitRunnable() {
            public void run() {
                ServerSegmetData segmetData;
                try {
                    IronSourceObject instance = IronSourceObject.getInstance();
                    if (MediationInitializer.this.validateUserId(MediationInitializer.this.mUserId).isValid()) {
                        String unused = MediationInitializer.this.mUserIdType = IronSourceConstants.TYPE_USER_GENERATED;
                    } else {
                        String unused2 = MediationInitializer.this.mUserId = instance.getAdvertiserId(MediationInitializer.this.mActivity);
                        if (!TextUtils.isEmpty(MediationInitializer.this.mUserId)) {
                            String unused3 = MediationInitializer.this.mUserIdType = IronSourceConstants.TYPE_GAID;
                        } else {
                            String unused4 = MediationInitializer.this.mUserId = DeviceStatus.getOrGenerateOnceUniqueIdentifier(MediationInitializer.this.mActivity);
                            if (!TextUtils.isEmpty(MediationInitializer.this.mUserId)) {
                                String unused5 = MediationInitializer.this.mUserIdType = IronSourceConstants.TYPE_UUID;
                            } else {
                                String unused6 = MediationInitializer.this.mUserId = "";
                            }
                        }
                        instance.setIronSourceUserId(MediationInitializer.this.mUserId);
                    }
                    GeneralProperties.getProperties().putKey(GeneralProperties.USER_ID_TYPE, MediationInitializer.this.mUserIdType);
                    if (!TextUtils.isEmpty(MediationInitializer.this.mUserId)) {
                        GeneralProperties.getProperties().putKey("userId", MediationInitializer.this.mUserId);
                    }
                    if (!TextUtils.isEmpty(MediationInitializer.this.mAppKey)) {
                        GeneralProperties.getProperties().putKey(ServerResponseWrapper.APP_KEY_FIELD, MediationInitializer.this.mAppKey);
                    }
                    long unused7 = MediationInitializer.this.mInitStartTime = new Date().getTime();
                    ServerResponseWrapper unused8 = MediationInitializer.this.mServerResponseWrapper = instance.getServerResponse(MediationInitializer.this.mActivity, MediationInitializer.this.mUserId, this.listener);
                    if (MediationInitializer.this.mServerResponseWrapper != null) {
                        MediationInitializer.this.mHandler.removeCallbacks(this);
                        if (MediationInitializer.this.mServerResponseWrapper.isValidResponse()) {
                            MediationInitializer.this.setInitStatus(EInitStatus.INITIATED);
                            instance.sendInitCompletedEvent(new Date().getTime() - MediationInitializer.this.mInitStartTime);
                            if (MediationInitializer.this.mServerResponseWrapper.getConfigurations().getApplicationConfigurations().getIntegration()) {
                                IntegrationHelper.validateIntegration(MediationInitializer.this.mActivity);
                            }
                            List<IronSource.AD_UNIT> initiatedAdUnits = MediationInitializer.this.mServerResponseWrapper.getInitiatedAdUnits();
                            for (OnMediationInitializationListener onInitSuccess : MediationInitializer.this.mOnMediationInitializationListeners) {
                                onInitSuccess.onInitSuccess(initiatedAdUnits, MediationInitializer.this.wasInitRevived());
                            }
                            if (MediationInitializer.this.mSegmentListener != null && (segmetData = MediationInitializer.this.mServerResponseWrapper.getConfigurations().getApplicationConfigurations().getSegmetData()) != null && !TextUtils.isEmpty(segmetData.getSegmentName())) {
                                MediationInitializer.this.mSegmentListener.onSegmentReceived(segmetData.getSegmentName());
                            }
                        } else if (!MediationInitializer.this.mDidReportInitialAvailability) {
                            MediationInitializer.this.setInitStatus(EInitStatus.INIT_FAILED);
                            boolean unused9 = MediationInitializer.this.mDidReportInitialAvailability = true;
                            for (OnMediationInitializationListener onInitFailed : MediationInitializer.this.mOnMediationInitializationListeners) {
                                onInitFailed.onInitFailed(IronSourceConstants.FALSE_AVAILABILITY_REASON_SERVER_RESPONSE_IS_NOT_VALID);
                            }
                        }
                    } else {
                        if (MediationInitializer.this.mRetryCounter == 3) {
                            boolean unused10 = MediationInitializer.this.mIsInProgressMoreThan15Secs = true;
                            for (OnMediationInitializationListener onStillInProgressAfter15Secs : MediationInitializer.this.mOnMediationInitializationListeners) {
                                onStillInProgressAfter15Secs.onStillInProgressAfter15Secs();
                            }
                        }
                        if (this.isRecoverable && MediationInitializer.this.mRetryCounter < MediationInitializer.this.mRetryLimit) {
                            boolean unused11 = MediationInitializer.this.mIsRevived = true;
                            MediationInitializer.this.mHandler.postDelayed(this, (long) (MediationInitializer.this.mRetryDelay * 1000));
                            if (MediationInitializer.this.mRetryCounter < MediationInitializer.this.mRetryGrowLimit) {
                                int unused12 = MediationInitializer.this.mRetryDelay = MediationInitializer.this.mRetryDelay * 2;
                            }
                        }
                        if ((!this.isRecoverable || MediationInitializer.this.mRetryCounter == MediationInitializer.this.mRetryAvailabilityLimit) && !MediationInitializer.this.mDidReportInitialAvailability) {
                            boolean unused13 = MediationInitializer.this.mDidReportInitialAvailability = true;
                            if (TextUtils.isEmpty(this.reason)) {
                                this.reason = IronSourceConstants.FALSE_AVAILABILITY_REASON_NO_SERVER_RESPONSE;
                            }
                            for (OnMediationInitializationListener onInitFailed2 : MediationInitializer.this.mOnMediationInitializationListeners) {
                                onInitFailed2.onInitFailed(this.reason);
                            }
                            MediationInitializer.this.setInitStatus(EInitStatus.INIT_FAILED);
                            IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.API, "Mediation availability false reason: No server response", 1);
                        }
                        MediationInitializer.access$1308(MediationInitializer.this);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        this.mInitStatus = EInitStatus.NOT_INIT;
        this.mHandlerThread = new HandlerThread("IronSourceInitiatorHandler");
        this.mHandlerThread.start();
        this.mHandler = new Handler(this.mHandlerThread.getLooper());
        this.mRetryDelay = 1;
        this.mRetryCounter = 0;
        this.mRetryLimit = 62;
        this.mRetryGrowLimit = 12;
        this.mRetryAvailabilityLimit = 5;
        this.mAtomicShouldPerformInit = new AtomicBoolean(true);
        this.mIsRevived = false;
        this.mIsInProgressMoreThan15Secs = false;
    }

    /* access modifiers changed from: private */
    public synchronized void setInitStatus(EInitStatus eInitStatus) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        logger.log(ironSourceTag, "setInitStatus(old status: " + this.mInitStatus + ", new status: " + eInitStatus + ")", 0);
        this.mInitStatus = eInitStatus;
    }

    public synchronized void init(Activity activity, String str, String str2, IronSource.AD_UNIT... ad_unitArr) {
        try {
            if (this.mAtomicShouldPerformInit == null || !this.mAtomicShouldPerformInit.compareAndSet(true, false)) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
                logger.log(ironSourceTag, this.TAG + ": Multiple calls to init are not allowed", 2);
            } else {
                setInitStatus(EInitStatus.INIT_IN_PROGRESS);
                this.mActivity = activity;
                this.mUserId = str2;
                this.mAppKey = str;
                if (IronSourceUtils.isNetworkConnected(activity)) {
                    this.mHandler.post(this.initRunnable);
                } else {
                    this.mListenForInit = true;
                    if (this.mNetworkStateReceiver == null) {
                        this.mNetworkStateReceiver = new NetworkStateReceiver(activity, this);
                    }
                    activity.getApplicationContext().registerReceiver(this.mNetworkStateReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {
                            CountDownTimer unused = MediationInitializer.this.mCountDownTimer = new CountDownTimer(60000, MTGInterstitialActivity.WEB_LOAD_TIME) {
                                public void onTick(long j) {
                                    if (j <= 45000) {
                                        boolean unused = MediationInitializer.this.mIsInProgressMoreThan15Secs = true;
                                        for (OnMediationInitializationListener onStillInProgressAfter15Secs : MediationInitializer.this.mOnMediationInitializationListeners) {
                                            onStillInProgressAfter15Secs.onStillInProgressAfter15Secs();
                                        }
                                    }
                                }

                                public void onFinish() {
                                    if (!MediationInitializer.this.mDidReportInitialAvailability) {
                                        boolean unused = MediationInitializer.this.mDidReportInitialAvailability = true;
                                        for (OnMediationInitializationListener onInitFailed : MediationInitializer.this.mOnMediationInitializationListeners) {
                                            onInitFailed.onInitFailed(IronSourceConstants.FALSE_AVAILABILITY_REASON_NO_INTERNET);
                                        }
                                        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.API, "Mediation availability false reason: No internet connection", 1);
                                    }
                                }
                            }.start();
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public void onNetworkAvailabilityChanged(boolean z) {
        if (this.mListenForInit && z) {
            if (this.mCountDownTimer != null) {
                this.mCountDownTimer.cancel();
            }
            this.mListenForInit = false;
            this.mIsRevived = true;
            this.mHandler.post(this.initRunnable);
        }
    }

    /* access modifiers changed from: private */
    public boolean wasInitRevived() {
        return this.mIsRevived;
    }

    public synchronized EInitStatus getCurrentInitStatus() {
        return this.mInitStatus;
    }

    /* access modifiers changed from: package-private */
    public void setInitStatusFailed() {
        setInitStatus(EInitStatus.INIT_FAILED);
    }

    public synchronized boolean isInProgressMoreThan15Secs() {
        return this.mIsInProgressMoreThan15Secs;
    }

    public void addMediationInitializationListener(OnMediationInitializationListener onMediationInitializationListener) {
        if (onMediationInitializationListener != null) {
            this.mOnMediationInitializationListeners.add(onMediationInitializationListener);
        }
    }

    public void removeMediationInitializationListener(OnMediationInitializationListener onMediationInitializationListener) {
        if (onMediationInitializationListener != null && this.mOnMediationInitializationListeners.size() != 0) {
            this.mOnMediationInitializationListeners.remove(onMediationInitializationListener);
        }
    }

    /* access modifiers changed from: package-private */
    public void setSegmentListener(SegmentListener segmentListener) {
        this.mSegmentListener = segmentListener;
    }

    /* access modifiers changed from: private */
    public ConfigValidationResult validateUserId(String str) {
        ConfigValidationResult configValidationResult = new ConfigValidationResult();
        if (str == null) {
            configValidationResult.setInvalid(ErrorBuilder.buildInvalidCredentialsError("userId", str, "it's missing"));
        } else if (!validateLength(str, 1, 64)) {
            configValidationResult.setInvalid(ErrorBuilder.buildInvalidCredentialsError("userId", str, null));
        }
        return configValidationResult;
    }

    private boolean validateLength(String str, int i, int i2) {
        return str != null && str.length() >= i && str.length() <= i2;
    }
}
