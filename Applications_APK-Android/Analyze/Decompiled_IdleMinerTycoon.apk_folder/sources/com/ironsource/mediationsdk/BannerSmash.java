package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.BannerManagerListener;
import com.ironsource.mediationsdk.sdk.BannerSmashListener;
import java.util.Timer;
import java.util.TimerTask;

public class BannerSmash implements BannerSmashListener {
    private AbstractAdapter mAdapter;
    private ProviderSettings mAdapterConfigs;
    private IronSourceBannerLayout mBannerLayout;
    private boolean mIsReadyToLoad;
    /* access modifiers changed from: private */
    public BannerManagerListener mListener;
    private long mLoadTimeoutMilisecs;
    private int mProviderPriority;
    /* access modifiers changed from: private */
    public BANNER_SMASH_STATE mState = BANNER_SMASH_STATE.NO_INIT;
    private Timer mTimer;

    protected enum BANNER_SMASH_STATE {
        NO_INIT,
        INIT_IN_PROGRESS,
        LOAD_IN_PROGRESS,
        LOADED,
        LOAD_FAILED,
        DESTROYED
    }

    BannerSmash(BannerManagerListener bannerManagerListener, ProviderSettings providerSettings, AbstractAdapter abstractAdapter, long j, int i) {
        this.mProviderPriority = i;
        this.mListener = bannerManagerListener;
        this.mAdapter = abstractAdapter;
        this.mAdapterConfigs = providerSettings;
        this.mLoadTimeoutMilisecs = j;
        this.mAdapter.addBannerListener(this);
    }

    public boolean isReadyToLoad() {
        return this.mIsReadyToLoad;
    }

    public void setReadyToLoad(boolean z) {
        this.mIsReadyToLoad = z;
    }

    public int getProviderPriority() {
        return this.mProviderPriority;
    }

    public String getName() {
        if (this.mAdapterConfigs.isMultipleInstances()) {
            return this.mAdapterConfigs.getProviderTypeForReflection();
        }
        return this.mAdapterConfigs.getProviderName();
    }

    public String getAdSourceNameForEvents() {
        if (!TextUtils.isEmpty(this.mAdapterConfigs.getAdSourceNameForEvents())) {
            return this.mAdapterConfigs.getAdSourceNameForEvents();
        }
        return getName();
    }

    public String getSubProviderId() {
        return this.mAdapterConfigs.getSubProviderId();
    }

    public AbstractAdapter getAdapter() {
        return this.mAdapter;
    }

    public void loadBanner(IronSourceBannerLayout ironSourceBannerLayout, Activity activity, String str, String str2) {
        log("loadBanner()");
        this.mIsReadyToLoad = false;
        if (ironSourceBannerLayout == null) {
            this.mListener.onBannerAdLoadFailed(new IronSourceError(IronSourceError.ERROR_BN_INSTANCE_LOAD_EMPTY_BANNER, "banner==null"), this, false);
        } else if (this.mAdapter == null) {
            this.mListener.onBannerAdLoadFailed(new IronSourceError(IronSourceError.ERROR_BN_INSTANCE_LOAD_EMPTY_ADAPTER, "adapter==null"), this, false);
        } else {
            this.mBannerLayout = ironSourceBannerLayout;
            startLoadTimer();
            if (this.mState == BANNER_SMASH_STATE.NO_INIT) {
                setState(BANNER_SMASH_STATE.INIT_IN_PROGRESS);
                setCustomParams();
                this.mAdapter.initBanners(activity, str, str2, this.mAdapterConfigs.getBannerSettings(), this);
                return;
            }
            setState(BANNER_SMASH_STATE.LOAD_IN_PROGRESS);
            this.mAdapter.loadBanner(ironSourceBannerLayout, this.mAdapterConfigs.getBannerSettings(), this);
        }
    }

    public void reloadBanner() {
        log("reloadBanner()");
        startLoadTimer();
        setState(BANNER_SMASH_STATE.LOADED);
        this.mAdapter.reloadBanner(this.mAdapterConfigs.getBannerSettings());
    }

    public void destroyBanner() {
        log("destroyBanner()");
        if (this.mAdapter == null) {
            log("destroyBanner() mAdapter == null");
            return;
        }
        this.mAdapter.destroyBanner(this.mAdapterConfigs.getBannerSettings());
        setState(BANNER_SMASH_STATE.DESTROYED);
    }

    private void setCustomParams() {
        if (this.mAdapter != null) {
            try {
                Integer age = IronSourceObject.getInstance().getAge();
                if (age != null) {
                    this.mAdapter.setAge(age.intValue());
                }
                String gender = IronSourceObject.getInstance().getGender();
                if (!TextUtils.isEmpty(gender)) {
                    this.mAdapter.setGender(gender);
                }
                String mediationSegment = IronSourceObject.getInstance().getMediationSegment();
                if (!TextUtils.isEmpty(mediationSegment)) {
                    this.mAdapter.setMediationSegment(mediationSegment);
                }
                String pluginType = ConfigFile.getConfigFile().getPluginType();
                if (!TextUtils.isEmpty(pluginType)) {
                    this.mAdapter.setPluginData(pluginType, ConfigFile.getConfigFile().getPluginFrameworkVersion());
                }
            } catch (Exception e) {
                log(":setCustomParams():" + e.toString());
            }
        }
    }

    /* access modifiers changed from: private */
    public void setState(BANNER_SMASH_STATE banner_smash_state) {
        this.mState = banner_smash_state;
        log("state=" + banner_smash_state.name());
    }

    private void stopLoadTimer() {
        try {
            if (this.mTimer != null) {
                this.mTimer.cancel();
            }
        } catch (Exception e) {
            logException("stopLoadTimer", e.getLocalizedMessage());
        } catch (Throwable th) {
            this.mTimer = null;
            throw th;
        }
        this.mTimer = null;
    }

    private void startLoadTimer() {
        try {
            stopLoadTimer();
            this.mTimer = new Timer();
            this.mTimer.schedule(new TimerTask() {
                public void run() {
                    cancel();
                    if (BannerSmash.this.mState == BANNER_SMASH_STATE.INIT_IN_PROGRESS) {
                        BannerSmash.this.setState(BANNER_SMASH_STATE.NO_INIT);
                        BannerSmash.this.log("init timed out");
                        BannerSmash.this.mListener.onBannerAdLoadFailed(new IronSourceError(IronSourceError.ERROR_BN_INSTANCE_INIT_TIMEOUT, "Timed out"), BannerSmash.this, false);
                    } else if (BannerSmash.this.mState == BANNER_SMASH_STATE.LOAD_IN_PROGRESS) {
                        BannerSmash.this.setState(BANNER_SMASH_STATE.LOAD_FAILED);
                        BannerSmash.this.log("load timed out");
                        BannerSmash.this.mListener.onBannerAdLoadFailed(new IronSourceError(IronSourceError.ERROR_BN_INSTANCE_LOAD_TIMEOUT, "Timed out"), BannerSmash.this, false);
                    } else if (BannerSmash.this.mState == BANNER_SMASH_STATE.LOADED) {
                        BannerSmash.this.setState(BANNER_SMASH_STATE.LOAD_FAILED);
                        BannerSmash.this.log("reload timed out");
                        BannerSmash.this.mListener.onBannerAdReloadFailed(new IronSourceError(IronSourceError.ERROR_BN_INSTANCE_RELOAD_TIMEOUT, "Timed out"), BannerSmash.this, false);
                    }
                }
            }, this.mLoadTimeoutMilisecs);
        } catch (Exception e) {
            logException("startLoadTimer", e.getLocalizedMessage());
        }
    }

    public void onBannerInitSuccess() {
        stopLoadTimer();
        if (this.mState == BANNER_SMASH_STATE.INIT_IN_PROGRESS) {
            startLoadTimer();
            setState(BANNER_SMASH_STATE.LOAD_IN_PROGRESS);
            this.mAdapter.loadBanner(this.mBannerLayout, this.mAdapterConfigs.getBannerSettings(), this);
        }
    }

    public void onBannerInitFailed(IronSourceError ironSourceError) {
        stopLoadTimer();
        if (this.mState == BANNER_SMASH_STATE.INIT_IN_PROGRESS) {
            this.mListener.onBannerAdLoadFailed(new IronSourceError(IronSourceError.ERROR_BN_INSTANCE_INIT_ERROR, "Banner init failed"), this, false);
            setState(BANNER_SMASH_STATE.NO_INIT);
        }
    }

    public void onBannerAdLoaded(View view, FrameLayout.LayoutParams layoutParams) {
        log("onBannerAdLoaded()");
        stopLoadTimer();
        if (this.mState == BANNER_SMASH_STATE.LOAD_IN_PROGRESS) {
            setState(BANNER_SMASH_STATE.LOADED);
            this.mListener.onBannerAdLoaded(this, view, layoutParams);
        } else if (this.mState == BANNER_SMASH_STATE.LOADED) {
            this.mListener.onBannerAdReloaded(this);
        }
    }

    public void onBannerAdLoadFailed(IronSourceError ironSourceError) {
        log("onBannerAdLoadFailed()");
        stopLoadTimer();
        boolean z = ironSourceError.getErrorCode() == 606;
        if (this.mState == BANNER_SMASH_STATE.LOAD_IN_PROGRESS) {
            setState(BANNER_SMASH_STATE.LOAD_FAILED);
            this.mListener.onBannerAdLoadFailed(ironSourceError, this, z);
        } else if (this.mState == BANNER_SMASH_STATE.LOADED) {
            this.mListener.onBannerAdReloadFailed(ironSourceError, this, z);
        }
    }

    public void onBannerAdClicked() {
        if (this.mListener != null) {
            this.mListener.onBannerAdClicked(this);
        }
    }

    public void onBannerAdScreenPresented() {
        if (this.mListener != null) {
            this.mListener.onBannerAdScreenPresented(this);
        }
    }

    public void onBannerAdScreenDismissed() {
        if (this.mListener != null) {
            this.mListener.onBannerAdScreenDismissed(this);
        }
    }

    public void onBannerAdLeftApplication() {
        if (this.mListener != null) {
            this.mListener.onBannerAdLeftApplication(this);
        }
    }

    public void onPause(Activity activity) {
        if (this.mAdapter != null) {
            this.mAdapter.onPause(activity);
        }
    }

    public void onResume(Activity activity) {
        if (this.mAdapter != null) {
            this.mAdapter.onResume(activity);
        }
    }

    /* access modifiers changed from: private */
    public void log(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        logger.log(ironSourceTag, "BannerSmash " + getName() + " " + str, 1);
    }

    private void logException(String str, String str2) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        logger.log(ironSourceTag, str + " Banner exception: " + getName() + " | " + str2, 3);
    }
}
