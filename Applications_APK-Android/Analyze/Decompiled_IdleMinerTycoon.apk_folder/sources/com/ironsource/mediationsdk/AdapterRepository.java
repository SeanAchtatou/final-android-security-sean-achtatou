package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class AdapterRepository {
    private static final Object lock = new Object();
    private static AdapterRepository mInstance = new AdapterRepository();
    private ConcurrentHashMap<String, AbstractAdapter> adapters = new ConcurrentHashMap<>();
    private String mAppKey;
    private Boolean mConsent;
    private AtomicBoolean mDidIronSourceEarlyInit = new AtomicBoolean(false);
    private String mUserId;

    public static AdapterRepository getInstance() {
        return mInstance;
    }

    private AdapterRepository() {
    }

    public void setInitParams(String str, String str2) {
        this.mAppKey = str;
        this.mUserId = str2;
    }

    public void setConsent(boolean z) {
        synchronized (lock) {
            this.mConsent = Boolean.valueOf(z);
            for (AbstractAdapter consent : this.adapters.values()) {
                setConsent(consent);
            }
        }
    }

    private void setConsent(AbstractAdapter abstractAdapter) {
        try {
            if (this.mConsent != null) {
                abstractAdapter.setConsent(this.mConsent.booleanValue());
            }
        } catch (Throwable th) {
            logInternal("error while setting consent of " + abstractAdapter.getProviderName() + ": " + th.getLocalizedMessage());
            th.printStackTrace();
        }
    }

    public AbstractAdapter getAdapter(ProviderSettings providerSettings, JSONObject jSONObject, Activity activity) {
        return getAdapterInternal(providerSettings.isMultipleInstances() ? providerSettings.getProviderTypeForReflection() : providerSettings.getProviderName(), providerSettings.getProviderTypeForReflection(), jSONObject, activity);
    }

    private AbstractAdapter getAdapterInternal(String str, String str2, JSONObject jSONObject, Activity activity) {
        logInternal(str + " (" + str2 + ") - Getting adapter");
        synchronized (lock) {
            if (this.adapters.containsKey(str)) {
                logInternal(str + " was already allocated");
                AbstractAdapter abstractAdapter = this.adapters.get(str);
                return abstractAdapter;
            }
            AbstractAdapter adapterByReflection = getAdapterByReflection(str, str2);
            if (adapterByReflection == null) {
                logErrorInternal(str + " adapter was not loaded");
                return null;
            }
            logInternal(str + " was allocated (adapter version: " + adapterByReflection.getVersion() + ", sdk version: " + adapterByReflection.getCoreSDKVersion() + ")");
            adapterByReflection.setLogListener(IronSourceLoggerManager.getLogger());
            earlyInitAdapter(jSONObject, adapterByReflection, str2, activity);
            setConsent(adapterByReflection);
            this.adapters.put(str, adapterByReflection);
            return adapterByReflection;
        }
    }

    private void earlyInitAdapter(JSONObject jSONObject, AbstractAdapter abstractAdapter, String str, Activity activity) {
        if ((str.equalsIgnoreCase(IronSourceConstants.SUPERSONIC_CONFIG_NAME) || str.equalsIgnoreCase(IronSourceConstants.IRONSOURCE_CONFIG_NAME)) && this.mDidIronSourceEarlyInit.compareAndSet(false, true)) {
            logInternal("SDK5 earlyInit  <" + str + ">");
            abstractAdapter.earlyInit(activity, this.mAppKey, this.mUserId, jSONObject);
        }
    }

    private AbstractAdapter getAdapterByReflection(String str, String str2) {
        try {
            Class<?> cls = Class.forName("com.ironsource.adapters." + str2.toLowerCase() + "." + str2 + "Adapter");
            return (AbstractAdapter) cls.getMethod(IronSourceConstants.START_ADAPTER, String.class).invoke(cls, str);
        } catch (Exception e) {
            logErrorInternal("Error while loading adapter: " + e.getLocalizedMessage());
            return null;
        }
    }

    private void logErrorInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        logger.log(ironSourceTag, "AdapterRepository: " + str, 3);
    }

    private void logInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        logger.log(ironSourceTag, "AdapterRepository: " + str, 0);
    }
}
