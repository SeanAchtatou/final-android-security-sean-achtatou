package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.MediationInitializer;
import com.ironsource.mediationsdk.config.ConfigValidationResult;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.events.SuperLooper;
import com.ironsource.mediationsdk.logger.ConsoleLogger;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.logger.LogListener;
import com.ironsource.mediationsdk.logger.PublisherLogger;
import com.ironsource.mediationsdk.model.ApplicationEvents;
import com.ironsource.mediationsdk.model.BannerConfigurations;
import com.ironsource.mediationsdk.model.BannerPlacement;
import com.ironsource.mediationsdk.model.InterstitialConfigurations;
import com.ironsource.mediationsdk.model.InterstitialPlacement;
import com.ironsource.mediationsdk.model.OfferwallPlacement;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.model.RewardedVideoConfigurations;
import com.ironsource.mediationsdk.sdk.ISDemandOnlyInterstitialListener;
import com.ironsource.mediationsdk.sdk.ISDemandOnlyRewardedVideoListener;
import com.ironsource.mediationsdk.sdk.InterstitialListener;
import com.ironsource.mediationsdk.sdk.IronSourceInterface;
import com.ironsource.mediationsdk.sdk.ListenersWrapper;
import com.ironsource.mediationsdk.sdk.OfferwallListener;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoListener;
import com.ironsource.mediationsdk.sdk.SegmentListener;
import com.ironsource.mediationsdk.server.HttpFunctions;
import com.ironsource.mediationsdk.server.ServerURL;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.GeneralPropertiesWorker;
import com.ironsource.mediationsdk.utils.IronSourceAES;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceObject implements IronSourceInterface, MediationInitializer.OnMediationInitializationListener {
    private static IronSourceObject sInstance;
    private final String IRONSOURCE_VERSION_STRING = "!SDK-VERSION-STRING!:com.ironsource:mediationsdk:​6.11.1";
    private final String KEY_INIT_COUNTER = "sessionDepth";
    private final String TAG = getClass().getName();
    private Activity mActivity;
    private Set<IronSource.AD_UNIT> mAdUnitsToInitialize;
    private String mAppKey = null;
    private AtomicBoolean mAtomicIsFirstInit;
    private BannerManager mBannerManager;
    private IronSourceBannerLayout mBnLayoutToLoad;
    private String mBnPlacementToLoad;
    private Boolean mConsent = null;
    private ServerResponseWrapper mCurrentServerResponse = null;
    private CopyOnWriteArraySet<String> mDemandOnlyIsLoadBeforeInitCompleted;
    private DemandOnlyIsManager mDemandOnlyIsManager;
    private CopyOnWriteArraySet<String> mDemandOnlyRvLoadBeforeInitCompleted;
    private DemandOnlyRvManager mDemandOnlyRvManager;
    private boolean mDidInitBanner;
    private boolean mDidInitInterstitial;
    private boolean mDidInitRewardedVideo;
    private String mDynamicUserId = null;
    private AtomicBoolean mEventManagersInit;
    private int mInitCounter;
    private boolean mInitSucceeded = false;
    private List<IronSource.AD_UNIT> mInitiatedAdUnits;
    private InterstitialManager mInterstitialManager;
    private IronSourceSegment mIronSegment;
    private Boolean mIsBnLoadBeforeInitCompleted;
    private boolean mIsDemandOnlyIs;
    private boolean mIsDemandOnlyRv;
    private boolean mIsIsLoadBeforeInitCompleted;
    private boolean mIsIsProgrammatic;
    private boolean mIsRvProgrammatic;
    private ListenersWrapper mListenersWrapper;
    private IronSourceLoggerManager mLoggerManager;
    private String mMediationType = null;
    private AbstractAdapter mOfferwallAdapter;
    private OfferwallManager mOfferwallManager;
    private ProgIsManager mProgIsManager;
    private ProgRvManager mProgRvManager;
    private PublisherLogger mPublisherLogger;
    private Set<IronSource.AD_UNIT> mRequestedAdUnits;
    private RewardedVideoManager mRewardedVideoManager;
    private Map<String, String> mRvServerParams = null;
    private String mSegment = null;
    private final Object mServerResponseLocker = new Object();
    private String mSessionId = null;
    private boolean mShouldSendGetInstanceEvent = true;
    private Integer mUserAge = null;
    private String mUserGender = null;
    private String mUserId = null;

    public interface IResponseListener {
        void onUnrecoverableError(String str);
    }

    public void initInterstitial(Activity activity, String str, String str2) {
    }

    public void initOfferwall(Activity activity, String str, String str2) {
    }

    public void initRewardedVideo(Activity activity, String str, String str2) {
    }

    public static synchronized IronSourceObject getInstance() {
        IronSourceObject ironSourceObject;
        synchronized (IronSourceObject.class) {
            if (sInstance == null) {
                sInstance = new IronSourceObject();
            }
            ironSourceObject = sInstance;
        }
        return ironSourceObject;
    }

    private IronSourceObject() {
        initializeManagers();
        this.mEventManagersInit = new AtomicBoolean();
        this.mAdUnitsToInitialize = new HashSet();
        this.mRequestedAdUnits = new HashSet();
        this.mIsDemandOnlyIs = false;
        this.mIsDemandOnlyRv = false;
        this.mAtomicIsFirstInit = new AtomicBoolean(true);
        this.mInitCounter = 0;
        this.mDidInitRewardedVideo = false;
        this.mDidInitInterstitial = false;
        this.mDidInitBanner = false;
        this.mSessionId = UUID.randomUUID().toString();
        this.mIsBnLoadBeforeInitCompleted = false;
        this.mIsIsLoadBeforeInitCompleted = false;
        this.mBnPlacementToLoad = null;
        this.mProgRvManager = null;
        this.mProgIsManager = null;
        this.mIsRvProgrammatic = false;
        this.mIsIsProgrammatic = false;
        this.mDemandOnlyIsLoadBeforeInitCompleted = new CopyOnWriteArraySet<>();
        this.mDemandOnlyRvLoadBeforeInitCompleted = new CopyOnWriteArraySet<>();
        this.mDemandOnlyIsManager = null;
        this.mDemandOnlyRvManager = null;
        this.mBannerManager = null;
    }

    public void sendInitCompletedEvent(long j) {
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(this.mIsDemandOnlyRv || this.mIsDemandOnlyIs);
        try {
            mediationAdditionalData.put(IronSourceConstants.EVENTS_DURATION, j);
            mediationAdditionalData.put("sessionDepth", this.mInitCounter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(IronSourceConstants.INIT_COMPLETE, mediationAdditionalData));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0145, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x007d A[Catch:{ Exception -> 0x00b4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0089 A[SYNTHETIC, Splitter:B:31:0x0089] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void init(android.app.Activity r8, java.lang.String r9, boolean r10, com.ironsource.mediationsdk.IronSource.AD_UNIT... r11) {
        /*
            r7 = this;
            monitor-enter(r7)
            java.util.concurrent.atomic.AtomicBoolean r0 = r7.mAtomicIsFirstInit     // Catch:{ all -> 0x0146 }
            if (r0 == 0) goto L_0x0134
            java.util.concurrent.atomic.AtomicBoolean r0 = r7.mAtomicIsFirstInit     // Catch:{ all -> 0x0146 }
            r1 = 0
            r2 = 1
            boolean r0 = r0.compareAndSet(r2, r1)     // Catch:{ all -> 0x0146 }
            if (r0 == 0) goto L_0x0134
            if (r11 == 0) goto L_0x0046
            int r0 = r11.length     // Catch:{ all -> 0x0146 }
            if (r0 != 0) goto L_0x0015
            goto L_0x0046
        L_0x0015:
            int r0 = r11.length     // Catch:{ all -> 0x0146 }
            r3 = 0
        L_0x0017:
            if (r3 >= r0) goto L_0x005e
            r4 = r11[r3]     // Catch:{ all -> 0x0146 }
            java.util.Set<com.ironsource.mediationsdk.IronSource$AD_UNIT> r5 = r7.mAdUnitsToInitialize     // Catch:{ all -> 0x0146 }
            r5.add(r4)     // Catch:{ all -> 0x0146 }
            java.util.Set<com.ironsource.mediationsdk.IronSource$AD_UNIT> r5 = r7.mRequestedAdUnits     // Catch:{ all -> 0x0146 }
            r5.add(r4)     // Catch:{ all -> 0x0146 }
            com.ironsource.mediationsdk.IronSource$AD_UNIT r5 = com.ironsource.mediationsdk.IronSource.AD_UNIT.INTERSTITIAL     // Catch:{ all -> 0x0146 }
            boolean r5 = r4.equals(r5)     // Catch:{ all -> 0x0146 }
            if (r5 == 0) goto L_0x002f
            r7.mDidInitInterstitial = r2     // Catch:{ all -> 0x0146 }
        L_0x002f:
            com.ironsource.mediationsdk.IronSource$AD_UNIT r5 = com.ironsource.mediationsdk.IronSource.AD_UNIT.BANNER     // Catch:{ all -> 0x0146 }
            boolean r5 = r4.equals(r5)     // Catch:{ all -> 0x0146 }
            if (r5 == 0) goto L_0x0039
            r7.mDidInitBanner = r2     // Catch:{ all -> 0x0146 }
        L_0x0039:
            com.ironsource.mediationsdk.IronSource$AD_UNIT r5 = com.ironsource.mediationsdk.IronSource.AD_UNIT.REWARDED_VIDEO     // Catch:{ all -> 0x0146 }
            boolean r4 = r4.equals(r5)     // Catch:{ all -> 0x0146 }
            if (r4 == 0) goto L_0x0043
            r7.mDidInitRewardedVideo = r2     // Catch:{ all -> 0x0146 }
        L_0x0043:
            int r3 = r3 + 1
            goto L_0x0017
        L_0x0046:
            com.ironsource.mediationsdk.IronSource$AD_UNIT[] r0 = com.ironsource.mediationsdk.IronSource.AD_UNIT.values()     // Catch:{ all -> 0x0146 }
            int r3 = r0.length     // Catch:{ all -> 0x0146 }
            r4 = 0
        L_0x004c:
            if (r4 >= r3) goto L_0x0058
            r5 = r0[r4]     // Catch:{ all -> 0x0146 }
            java.util.Set<com.ironsource.mediationsdk.IronSource$AD_UNIT> r6 = r7.mAdUnitsToInitialize     // Catch:{ all -> 0x0146 }
            r6.add(r5)     // Catch:{ all -> 0x0146 }
            int r4 = r4 + 1
            goto L_0x004c
        L_0x0058:
            r7.mDidInitRewardedVideo = r2     // Catch:{ all -> 0x0146 }
            r7.mDidInitInterstitial = r2     // Catch:{ all -> 0x0146 }
            r7.mDidInitBanner = r2     // Catch:{ all -> 0x0146 }
        L_0x005e:
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r7.mLoggerManager     // Catch:{ all -> 0x0146 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r3 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x0146 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0146 }
            r4.<init>()     // Catch:{ all -> 0x0146 }
            java.lang.String r5 = "init(appKey:"
            r4.append(r5)     // Catch:{ all -> 0x0146 }
            r4.append(r9)     // Catch:{ all -> 0x0146 }
            java.lang.String r5 = ")"
            r4.append(r5)     // Catch:{ all -> 0x0146 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0146 }
            r0.log(r3, r4, r2)     // Catch:{ all -> 0x0146 }
            if (r8 != 0) goto L_0x0089
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r8 = r7.mLoggerManager     // Catch:{ all -> 0x0146 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r9 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x0146 }
            java.lang.String r10 = "Init Fail - provided activity is null"
            r11 = 2
            r8.log(r9, r10, r11)     // Catch:{ all -> 0x0146 }
            monitor-exit(r7)
            return
        L_0x0089:
            r7.mActivity = r8     // Catch:{ all -> 0x0146 }
            r7.prepareEventManagers(r8)     // Catch:{ all -> 0x0146 }
            com.ironsource.mediationsdk.config.ConfigValidationResult r0 = r7.validateAppKey(r9)     // Catch:{ all -> 0x0146 }
            boolean r3 = r0.isValid()     // Catch:{ all -> 0x0146 }
            if (r3 == 0) goto L_0x00f8
            r7.mAppKey = r9     // Catch:{ all -> 0x0146 }
            boolean r0 = r7.mShouldSendGetInstanceEvent     // Catch:{ all -> 0x0146 }
            if (r0 == 0) goto L_0x00d4
            org.json.JSONObject r10 = com.ironsource.mediationsdk.utils.IronSourceUtils.getMediationAdditionalData(r10)     // Catch:{ all -> 0x0146 }
            if (r11 == 0) goto L_0x00b6
            int r0 = r11.length     // Catch:{ Exception -> 0x00b4 }
            r3 = 0
        L_0x00a6:
            if (r3 >= r0) goto L_0x00b6
            r4 = r11[r3]     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00b4 }
            r10.put(r4, r2)     // Catch:{ Exception -> 0x00b4 }
            int r3 = r3 + 1
            goto L_0x00a6
        L_0x00b4:
            r0 = move-exception
            goto L_0x00c1
        L_0x00b6:
            java.lang.String r0 = "sessionDepth"
            int r3 = r7.mInitCounter     // Catch:{ Exception -> 0x00b4 }
            int r3 = r3 + r2
            r7.mInitCounter = r3     // Catch:{ Exception -> 0x00b4 }
            r10.put(r0, r3)     // Catch:{ Exception -> 0x00b4 }
            goto L_0x00c4
        L_0x00c1:
            r0.printStackTrace()     // Catch:{ all -> 0x0146 }
        L_0x00c4:
            com.ironsource.eventsmodule.EventData r0 = new com.ironsource.eventsmodule.EventData     // Catch:{ all -> 0x0146 }
            r2 = 14
            r0.<init>(r2, r10)     // Catch:{ all -> 0x0146 }
            com.ironsource.mediationsdk.events.RewardedVideoEventsManager r10 = com.ironsource.mediationsdk.events.RewardedVideoEventsManager.getInstance()     // Catch:{ all -> 0x0146 }
            r10.log(r0)     // Catch:{ all -> 0x0146 }
            r7.mShouldSendGetInstanceEvent = r1     // Catch:{ all -> 0x0146 }
        L_0x00d4:
            java.util.Set<com.ironsource.mediationsdk.IronSource$AD_UNIT> r10 = r7.mAdUnitsToInitialize     // Catch:{ all -> 0x0146 }
            com.ironsource.mediationsdk.IronSource$AD_UNIT r0 = com.ironsource.mediationsdk.IronSource.AD_UNIT.INTERSTITIAL     // Catch:{ all -> 0x0146 }
            boolean r10 = r10.contains(r0)     // Catch:{ all -> 0x0146 }
            if (r10 == 0) goto L_0x00e7
            com.ironsource.mediationsdk.MediationInitializer r10 = com.ironsource.mediationsdk.MediationInitializer.getInstance()     // Catch:{ all -> 0x0146 }
            com.ironsource.mediationsdk.InterstitialManager r0 = r7.mInterstitialManager     // Catch:{ all -> 0x0146 }
            r10.addMediationInitializationListener(r0)     // Catch:{ all -> 0x0146 }
        L_0x00e7:
            com.ironsource.mediationsdk.MediationInitializer r10 = com.ironsource.mediationsdk.MediationInitializer.getInstance()     // Catch:{ all -> 0x0146 }
            r10.addMediationInitializationListener(r7)     // Catch:{ all -> 0x0146 }
            com.ironsource.mediationsdk.MediationInitializer r10 = com.ironsource.mediationsdk.MediationInitializer.getInstance()     // Catch:{ all -> 0x0146 }
            java.lang.String r0 = r7.mUserId     // Catch:{ all -> 0x0146 }
            r10.init(r8, r9, r0, r11)     // Catch:{ all -> 0x0146 }
            goto L_0x0144
        L_0x00f8:
            com.ironsource.mediationsdk.MediationInitializer r8 = com.ironsource.mediationsdk.MediationInitializer.getInstance()     // Catch:{ all -> 0x0146 }
            r8.setInitStatusFailed()     // Catch:{ all -> 0x0146 }
            java.util.Set<com.ironsource.mediationsdk.IronSource$AD_UNIT> r8 = r7.mAdUnitsToInitialize     // Catch:{ all -> 0x0146 }
            com.ironsource.mediationsdk.IronSource$AD_UNIT r9 = com.ironsource.mediationsdk.IronSource.AD_UNIT.REWARDED_VIDEO     // Catch:{ all -> 0x0146 }
            boolean r8 = r8.contains(r9)     // Catch:{ all -> 0x0146 }
            if (r8 == 0) goto L_0x010e
            com.ironsource.mediationsdk.sdk.ListenersWrapper r8 = r7.mListenersWrapper     // Catch:{ all -> 0x0146 }
            r8.onRewardedVideoAvailabilityChanged(r1)     // Catch:{ all -> 0x0146 }
        L_0x010e:
            java.util.Set<com.ironsource.mediationsdk.IronSource$AD_UNIT> r8 = r7.mAdUnitsToInitialize     // Catch:{ all -> 0x0146 }
            com.ironsource.mediationsdk.IronSource$AD_UNIT r9 = com.ironsource.mediationsdk.IronSource.AD_UNIT.OFFERWALL     // Catch:{ all -> 0x0146 }
            boolean r8 = r8.contains(r9)     // Catch:{ all -> 0x0146 }
            if (r8 == 0) goto L_0x0121
            com.ironsource.mediationsdk.sdk.ListenersWrapper r8 = r7.mListenersWrapper     // Catch:{ all -> 0x0146 }
            com.ironsource.mediationsdk.logger.IronSourceError r9 = r0.getIronSourceError()     // Catch:{ all -> 0x0146 }
            r8.onOfferwallAvailable(r1, r9)     // Catch:{ all -> 0x0146 }
        L_0x0121:
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r8 = com.ironsource.mediationsdk.logger.IronSourceLoggerManager.getLogger()     // Catch:{ all -> 0x0146 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r9 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x0146 }
            com.ironsource.mediationsdk.logger.IronSourceError r10 = r0.getIronSourceError()     // Catch:{ all -> 0x0146 }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x0146 }
            r8.log(r9, r10, r2)     // Catch:{ all -> 0x0146 }
            monitor-exit(r7)
            return
        L_0x0134:
            if (r11 == 0) goto L_0x013a
            r7.attachAdUnits(r10, r11)     // Catch:{ all -> 0x0146 }
            goto L_0x0144
        L_0x013a:
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r8 = r7.mLoggerManager     // Catch:{ all -> 0x0146 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r9 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x0146 }
            java.lang.String r10 = "Multiple calls to init without ad units are not allowed"
            r11 = 3
            r8.log(r9, r10, r11)     // Catch:{ all -> 0x0146 }
        L_0x0144:
            monitor-exit(r7)
            return
        L_0x0146:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.IronSourceObject.init(android.app.Activity, java.lang.String, boolean, com.ironsource.mediationsdk.IronSource$AD_UNIT[]):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00d1, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void initISDemandOnly(android.app.Activity r11, java.lang.String r12, com.ironsource.mediationsdk.IronSource.AD_UNIT... r13) {
        /*
            r10 = this;
            monitor-enter(r10)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x00d2 }
            r0.<init>()     // Catch:{ all -> 0x00d2 }
            r1 = 3
            if (r13 != 0) goto L_0x0014
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r11 = r10.mLoggerManager     // Catch:{ all -> 0x00d2 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r12 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x00d2 }
            java.lang.String r13 = "Cannot initialized demand only mode: No ad units selected"
            r11.log(r12, r13, r1)     // Catch:{ all -> 0x00d2 }
            monitor-exit(r10)
            return
        L_0x0014:
            int r2 = r13.length     // Catch:{ all -> 0x00d2 }
            if (r2 > 0) goto L_0x0022
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r11 = r10.mLoggerManager     // Catch:{ all -> 0x00d2 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r12 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x00d2 }
            java.lang.String r13 = "Cannot initialized demand only mode: No ad units selected"
            r11.log(r12, r13, r1)     // Catch:{ all -> 0x00d2 }
            monitor-exit(r10)
            return
        L_0x0022:
            int r2 = r13.length     // Catch:{ all -> 0x00d2 }
            r3 = 0
        L_0x0024:
            r4 = 1
            if (r3 >= r2) goto L_0x00bb
            r5 = r13[r3]     // Catch:{ all -> 0x00d2 }
            com.ironsource.mediationsdk.IronSource$AD_UNIT r6 = com.ironsource.mediationsdk.IronSource.AD_UNIT.BANNER     // Catch:{ all -> 0x00d2 }
            boolean r6 = r5.equals(r6)     // Catch:{ all -> 0x00d2 }
            if (r6 != 0) goto L_0x009f
            com.ironsource.mediationsdk.IronSource$AD_UNIT r6 = com.ironsource.mediationsdk.IronSource.AD_UNIT.OFFERWALL     // Catch:{ all -> 0x00d2 }
            boolean r6 = r5.equals(r6)     // Catch:{ all -> 0x00d2 }
            if (r6 == 0) goto L_0x003a
            goto L_0x009f
        L_0x003a:
            com.ironsource.mediationsdk.IronSource$AD_UNIT r6 = com.ironsource.mediationsdk.IronSource.AD_UNIT.INTERSTITIAL     // Catch:{ all -> 0x00d2 }
            boolean r6 = r5.equals(r6)     // Catch:{ all -> 0x00d2 }
            if (r6 == 0) goto L_0x006c
            boolean r6 = r10.mDidInitInterstitial     // Catch:{ all -> 0x00d2 }
            if (r6 == 0) goto L_0x005f
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r6 = r10.mLoggerManager     // Catch:{ all -> 0x00d2 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r7 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x00d2 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d2 }
            r8.<init>()     // Catch:{ all -> 0x00d2 }
            r8.append(r5)     // Catch:{ all -> 0x00d2 }
            java.lang.String r9 = " ad unit has already been initialized"
            r8.append(r9)     // Catch:{ all -> 0x00d2 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x00d2 }
            r6.log(r7, r8, r1)     // Catch:{ all -> 0x00d2 }
            goto L_0x006c
        L_0x005f:
            r10.mDidInitInterstitial = r4     // Catch:{ all -> 0x00d2 }
            r10.mIsDemandOnlyIs = r4     // Catch:{ all -> 0x00d2 }
            boolean r6 = r0.contains(r5)     // Catch:{ all -> 0x00d2 }
            if (r6 != 0) goto L_0x006c
            r0.add(r5)     // Catch:{ all -> 0x00d2 }
        L_0x006c:
            com.ironsource.mediationsdk.IronSource$AD_UNIT r6 = com.ironsource.mediationsdk.IronSource.AD_UNIT.REWARDED_VIDEO     // Catch:{ all -> 0x00d2 }
            boolean r6 = r5.equals(r6)     // Catch:{ all -> 0x00d2 }
            if (r6 == 0) goto L_0x00b7
            boolean r6 = r10.mDidInitRewardedVideo     // Catch:{ all -> 0x00d2 }
            if (r6 == 0) goto L_0x0091
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r4 = r10.mLoggerManager     // Catch:{ all -> 0x00d2 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r6 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x00d2 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d2 }
            r7.<init>()     // Catch:{ all -> 0x00d2 }
            r7.append(r5)     // Catch:{ all -> 0x00d2 }
            java.lang.String r5 = " ad unit has already been initialized"
            r7.append(r5)     // Catch:{ all -> 0x00d2 }
            java.lang.String r5 = r7.toString()     // Catch:{ all -> 0x00d2 }
            r4.log(r6, r5, r1)     // Catch:{ all -> 0x00d2 }
            goto L_0x00b7
        L_0x0091:
            r10.mDidInitRewardedVideo = r4     // Catch:{ all -> 0x00d2 }
            r10.mIsDemandOnlyRv = r4     // Catch:{ all -> 0x00d2 }
            boolean r4 = r0.contains(r5)     // Catch:{ all -> 0x00d2 }
            if (r4 != 0) goto L_0x00b7
            r0.add(r5)     // Catch:{ all -> 0x00d2 }
            goto L_0x00b7
        L_0x009f:
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r4 = r10.mLoggerManager     // Catch:{ all -> 0x00d2 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r6 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x00d2 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d2 }
            r7.<init>()     // Catch:{ all -> 0x00d2 }
            r7.append(r5)     // Catch:{ all -> 0x00d2 }
            java.lang.String r5 = " ad unit cannot be initialized in demand only mode"
            r7.append(r5)     // Catch:{ all -> 0x00d2 }
            java.lang.String r5 = r7.toString()     // Catch:{ all -> 0x00d2 }
            r4.log(r6, r5, r1)     // Catch:{ all -> 0x00d2 }
        L_0x00b7:
            int r3 = r3 + 1
            goto L_0x0024
        L_0x00bb:
            int r13 = r0.size()     // Catch:{ all -> 0x00d2 }
            if (r13 <= 0) goto L_0x00d0
            int r13 = r0.size()     // Catch:{ all -> 0x00d2 }
            com.ironsource.mediationsdk.IronSource$AD_UNIT[] r13 = new com.ironsource.mediationsdk.IronSource.AD_UNIT[r13]     // Catch:{ all -> 0x00d2 }
            java.lang.Object[] r13 = r0.toArray(r13)     // Catch:{ all -> 0x00d2 }
            com.ironsource.mediationsdk.IronSource$AD_UNIT[] r13 = (com.ironsource.mediationsdk.IronSource.AD_UNIT[]) r13     // Catch:{ all -> 0x00d2 }
            r10.init(r11, r12, r4, r13)     // Catch:{ all -> 0x00d2 }
        L_0x00d0:
            monitor-exit(r10)
            return
        L_0x00d2:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.IronSourceObject.initISDemandOnly(android.app.Activity, java.lang.String, com.ironsource.mediationsdk.IronSource$AD_UNIT[]):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private synchronized void attachAdUnits(boolean z, IronSource.AD_UNIT... ad_unitArr) {
        int i = 0;
        for (IronSource.AD_UNIT ad_unit : ad_unitArr) {
            if (ad_unit.equals(IronSource.AD_UNIT.INTERSTITIAL)) {
                this.mDidInitInterstitial = true;
            } else if (ad_unit.equals(IronSource.AD_UNIT.BANNER)) {
                this.mDidInitBanner = true;
            }
        }
        if (MediationInitializer.getInstance().getCurrentInitStatus() == MediationInitializer.EInitStatus.INIT_FAILED) {
            try {
                if (this.mListenersWrapper != null) {
                    int length = ad_unitArr.length;
                    while (i < length) {
                        IronSource.AD_UNIT ad_unit2 = ad_unitArr[i];
                        if (!this.mAdUnitsToInitialize.contains(ad_unit2)) {
                            notifyPublisherAboutInitFailed(ad_unit2, true);
                        }
                        i++;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (!this.mInitSucceeded) {
            JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(z);
            int length2 = ad_unitArr.length;
            boolean z2 = false;
            while (i < length2) {
                IronSource.AD_UNIT ad_unit3 = ad_unitArr[i];
                if (!this.mAdUnitsToInitialize.contains(ad_unit3)) {
                    this.mAdUnitsToInitialize.add(ad_unit3);
                    this.mRequestedAdUnits.add(ad_unit3);
                    try {
                        mediationAdditionalData.put(ad_unit3.toString(), true);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    z2 = true;
                } else {
                    this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, ad_unit3 + " ad unit has started initializing.", 3);
                }
                i++;
            }
            if (z2) {
                try {
                    int i2 = this.mInitCounter + 1;
                    this.mInitCounter = i2;
                    mediationAdditionalData.put("sessionDepth", i2);
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                RewardedVideoEventsManager.getInstance().log(new EventData(14, mediationAdditionalData));
            }
        } else if (this.mInitiatedAdUnits != null) {
            JSONObject mediationAdditionalData2 = IronSourceUtils.getMediationAdditionalData(z);
            boolean z3 = false;
            for (IronSource.AD_UNIT ad_unit4 : ad_unitArr) {
                if (!this.mAdUnitsToInitialize.contains(ad_unit4)) {
                    this.mAdUnitsToInitialize.add(ad_unit4);
                    this.mRequestedAdUnits.add(ad_unit4);
                    try {
                        mediationAdditionalData2.put(ad_unit4.toString(), true);
                    } catch (Exception e4) {
                        e4.printStackTrace();
                    }
                    if (this.mInitiatedAdUnits == null || !this.mInitiatedAdUnits.contains(ad_unit4)) {
                        notifyPublisherAboutInitFailed(ad_unit4, false);
                    } else {
                        startAdUnit(ad_unit4);
                    }
                    z3 = true;
                } else {
                    this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, ad_unit4 + " ad unit has already been initialized", 3);
                }
            }
            if (z3) {
                try {
                    int i3 = this.mInitCounter + 1;
                    this.mInitCounter = i3;
                    mediationAdditionalData2.put("sessionDepth", i3);
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
                RewardedVideoEventsManager.getInstance().log(new EventData(14, mediationAdditionalData2));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public void onInitSuccess(List<IronSource.AD_UNIT> list, boolean z) {
        try {
            this.mInitiatedAdUnits = list;
            this.mInitSucceeded = true;
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "onInitSuccess()", 1);
            IronSourceUtils.sendAutomationLog("init success");
            if (z) {
                JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
                try {
                    mediationAdditionalData.put("revived", true);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                RewardedVideoEventsManager.getInstance().log(new EventData(114, mediationAdditionalData));
            }
            InterstitialEventsManager.getInstance().triggerEventsSend();
            RewardedVideoEventsManager.getInstance().triggerEventsSend();
            AdapterRepository.getInstance().setInitParams(getIronSourceAppKey(), getIronSourceUserId());
            for (IronSource.AD_UNIT ad_unit : IronSource.AD_UNIT.values()) {
                if (this.mAdUnitsToInitialize.contains(ad_unit)) {
                    if (list.contains(ad_unit)) {
                        startAdUnit(ad_unit);
                    } else {
                        notifyPublisherAboutInitFailed(ad_unit, false);
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void startAdUnit(IronSource.AD_UNIT ad_unit) {
        switch (ad_unit) {
            case REWARDED_VIDEO:
                startRewardedVideo();
                return;
            case INTERSTITIAL:
                startInterstitial();
                return;
            case OFFERWALL:
                this.mOfferwallManager.initOfferwall(this.mActivity, getIronSourceAppKey(), getIronSourceUserId());
                return;
            case BANNER:
                startBanner();
                return;
            default:
                return;
        }
    }

    private void startProgrammaticRv() {
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "Rewarded Video started in programmatic mode", 0);
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < this.mCurrentServerResponse.getProviderOrder().getRewardedVideoProviderOrder().size(); i++) {
            String str = this.mCurrentServerResponse.getProviderOrder().getRewardedVideoProviderOrder().get(i);
            if (!TextUtils.isEmpty(str)) {
                arrayList.add(this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(str));
            }
        }
        if (arrayList.size() > 0) {
            this.mProgRvManager = new ProgRvManager(this.mActivity, arrayList, this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations(), getIronSourceAppKey(), getIronSourceUserId());
            return;
        }
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false, true);
        addToDictionary(mediationAdditionalData, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, 1010}});
        sentEventWithRVEventManager(IronSourceConstants.TROUBLESHOOTING_RV_INIT_FAILED, mediationAdditionalData);
        notifyPublisherAboutInitFailed(IronSource.AD_UNIT.REWARDED_VIDEO, false);
    }

    private void startDemandOnlyRv() {
        synchronized (this.mDemandOnlyRvLoadBeforeInitCompleted) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "Rewarded Video started in demand only mode", 0);
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < this.mCurrentServerResponse.getProviderOrder().getRewardedVideoProviderOrder().size(); i++) {
                String str = this.mCurrentServerResponse.getProviderOrder().getRewardedVideoProviderOrder().get(i);
                if (!TextUtils.isEmpty(str)) {
                    arrayList.add(this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(str));
                }
            }
            if (arrayList.size() > 0) {
                this.mDemandOnlyRvManager = new DemandOnlyRvManager(this.mActivity, arrayList, this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations(), getIronSourceAppKey(), getIronSourceUserId());
                if (this.mConsent != null) {
                    this.mDemandOnlyRvManager.setConsent(this.mConsent.booleanValue());
                }
                Iterator<String> it = this.mDemandOnlyRvLoadBeforeInitCompleted.iterator();
                while (it.hasNext()) {
                    this.mDemandOnlyRvManager.loadRewardedVideo(it.next());
                }
                this.mDemandOnlyRvLoadBeforeInitCompleted.clear();
            } else {
                notifyPublisherAboutInitFailed(IronSource.AD_UNIT.REWARDED_VIDEO, false);
            }
        }
    }

    private void startRewardedVideo() {
        ProviderSettings providerSettings;
        ProviderSettings providerSettings2;
        ProviderSettings providerSettings3;
        if (this.mIsDemandOnlyRv) {
            startDemandOnlyRv();
            return;
        }
        this.mIsRvProgrammatic = this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoAuctionSettings().getIsProgrammatic();
        sentEventWithRVEventManager(IronSourceConstants.TROUBLESHOOTING_RV_FORK, IronSourceUtils.getMediationAdditionalData(false, this.mIsRvProgrammatic));
        if (this.mIsRvProgrammatic) {
            startProgrammaticRv();
            return;
        }
        int rewardedVideoAdaptersSmartLoadTimeout = this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoAdaptersSmartLoadTimeout();
        for (int i = 0; i < this.mCurrentServerResponse.getProviderOrder().getRewardedVideoProviderOrder().size(); i++) {
            String str = this.mCurrentServerResponse.getProviderOrder().getRewardedVideoProviderOrder().get(i);
            if (!TextUtils.isEmpty(str) && (providerSettings3 = this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(str)) != null) {
                RewardedVideoSmash rewardedVideoSmash = new RewardedVideoSmash(providerSettings3, rewardedVideoAdaptersSmartLoadTimeout);
                if (validateSmash(rewardedVideoSmash)) {
                    rewardedVideoSmash.setRewardedVideoManagerListener(this.mRewardedVideoManager);
                    rewardedVideoSmash.setProviderPriority(i + 1);
                    this.mRewardedVideoManager.addSmashToArray(rewardedVideoSmash);
                }
            }
        }
        if (this.mRewardedVideoManager.mSmashArray.size() > 0) {
            this.mRewardedVideoManager.setIsUltraEventsEnabled(this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoEventsConfigurations().isUltraEventsEnabled());
            this.mRewardedVideoManager.setSmartLoadAmount(this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoAdaptersSmartLoadAmount());
            this.mRewardedVideoManager.setManualLoadInterval(this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations().getManualLoadIntervalInSeconds());
            String rVBackFillProvider = this.mCurrentServerResponse.getRVBackFillProvider();
            if (!TextUtils.isEmpty(rVBackFillProvider) && (providerSettings2 = this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(rVBackFillProvider)) != null) {
                RewardedVideoSmash rewardedVideoSmash2 = new RewardedVideoSmash(providerSettings2, rewardedVideoAdaptersSmartLoadTimeout);
                if (validateSmash(rewardedVideoSmash2)) {
                    rewardedVideoSmash2.setRewardedVideoManagerListener(this.mRewardedVideoManager);
                    this.mRewardedVideoManager.setBackfillSmash(rewardedVideoSmash2);
                }
            }
            String rVPremiumProvider = this.mCurrentServerResponse.getRVPremiumProvider();
            if (!TextUtils.isEmpty(rVPremiumProvider) && (providerSettings = this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(rVPremiumProvider)) != null) {
                RewardedVideoSmash rewardedVideoSmash3 = new RewardedVideoSmash(providerSettings, rewardedVideoAdaptersSmartLoadTimeout);
                if (validateSmash(rewardedVideoSmash3)) {
                    rewardedVideoSmash3.setRewardedVideoManagerListener(this.mRewardedVideoManager);
                    this.mRewardedVideoManager.setPremiumSmash(rewardedVideoSmash3);
                }
            }
            this.mRewardedVideoManager.initRewardedVideo(this.mActivity, getIronSourceAppKey(), getIronSourceUserId());
            return;
        }
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false, false);
        addToDictionary(mediationAdditionalData, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, 1010}});
        sentEventWithRVEventManager(IronSourceConstants.TROUBLESHOOTING_RV_INIT_FAILED, mediationAdditionalData);
        notifyPublisherAboutInitFailed(IronSource.AD_UNIT.REWARDED_VIDEO, false);
    }

    private void startProgrammaticIs() {
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "Interstitial started in programmatic mode", 0);
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < this.mCurrentServerResponse.getProviderOrder().getInterstitialProviderOrder().size(); i++) {
            String str = this.mCurrentServerResponse.getProviderOrder().getInterstitialProviderOrder().get(i);
            if (!TextUtils.isEmpty(str)) {
                arrayList.add(this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(str));
            }
        }
        if (arrayList.size() > 0) {
            this.mProgIsManager = new ProgIsManager(this.mActivity, arrayList, this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations(), getIronSourceAppKey(), getIronSourceUserId(), this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations().getISDelayLoadFailure());
            if (this.mIsIsLoadBeforeInitCompleted) {
                this.mIsIsLoadBeforeInitCompleted = false;
                this.mProgIsManager.loadInterstitial();
                return;
            }
            return;
        }
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false, true);
        addToDictionary(mediationAdditionalData, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, 1010}});
        sentEventWithISEventManager(IronSourceConstants.TROUBLESHOOTING_IS_INIT_FAILED, mediationAdditionalData);
        notifyPublisherAboutInitFailed(IronSource.AD_UNIT.INTERSTITIAL, false);
    }

    private void startDemandOnlyIs() {
        synchronized (this.mDemandOnlyIsLoadBeforeInitCompleted) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "Interstitial started in demand only mode", 0);
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < this.mCurrentServerResponse.getProviderOrder().getInterstitialProviderOrder().size(); i++) {
                String str = this.mCurrentServerResponse.getProviderOrder().getInterstitialProviderOrder().get(i);
                if (!TextUtils.isEmpty(str)) {
                    arrayList.add(this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(str));
                }
            }
            if (arrayList.size() > 0) {
                this.mDemandOnlyIsManager = new DemandOnlyIsManager(this.mActivity, arrayList, this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations(), getIronSourceAppKey(), getIronSourceUserId());
                if (this.mConsent != null) {
                    this.mDemandOnlyIsManager.setConsent(this.mConsent.booleanValue());
                }
                Iterator<String> it = this.mDemandOnlyIsLoadBeforeInitCompleted.iterator();
                while (it.hasNext()) {
                    this.mDemandOnlyIsManager.loadInterstitial(it.next());
                }
                this.mDemandOnlyIsLoadBeforeInitCompleted.clear();
            } else {
                JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false, false);
                addToDictionary(mediationAdditionalData, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, 1010}});
                sentEventWithISEventManager(IronSourceConstants.TROUBLESHOOTING_IS_INIT_FAILED, mediationAdditionalData);
                notifyPublisherAboutInitFailed(IronSource.AD_UNIT.INTERSTITIAL, false);
            }
        }
    }

    private void startInterstitial() {
        ProviderSettings providerSettings;
        if (this.mIsDemandOnlyIs) {
            startDemandOnlyIs();
            return;
        }
        this.mIsIsProgrammatic = this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations().getInterstitialAuctionSettings().getIsProgrammatic();
        sentEventWithISEventManager(IronSourceConstants.TROUBLESHOOTING_IS_FORK, IronSourceUtils.getMediationAdditionalData(false, this.mIsIsProgrammatic));
        if (this.mIsIsProgrammatic) {
            startProgrammaticIs();
            return;
        }
        int interstitialAdaptersSmartLoadTimeout = this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations().getInterstitialAdaptersSmartLoadTimeout();
        this.mInterstitialManager.setDelayLoadFailureNotificationInSeconds(this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations().getISDelayLoadFailure());
        for (int i = 0; i < this.mCurrentServerResponse.getProviderOrder().getInterstitialProviderOrder().size(); i++) {
            String str = this.mCurrentServerResponse.getProviderOrder().getInterstitialProviderOrder().get(i);
            if (!TextUtils.isEmpty(str) && (providerSettings = this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(str)) != null) {
                InterstitialSmash interstitialSmash = new InterstitialSmash(providerSettings, interstitialAdaptersSmartLoadTimeout);
                if (validateSmash(interstitialSmash)) {
                    interstitialSmash.setInterstitialManagerListener(this.mInterstitialManager);
                    interstitialSmash.setProviderPriority(i + 1);
                    this.mInterstitialManager.addSmashToArray(interstitialSmash);
                }
            }
        }
        if (this.mInterstitialManager.mSmashArray.size() > 0) {
            this.mInterstitialManager.setSmartLoadAmount(this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations().getInterstitialAdaptersSmartLoadAmount());
            this.mInterstitialManager.initInterstitial(this.mActivity, getIronSourceAppKey(), getIronSourceUserId());
            if (this.mIsIsLoadBeforeInitCompleted) {
                this.mIsIsLoadBeforeInitCompleted = false;
                this.mInterstitialManager.loadInterstitial();
                return;
            }
            return;
        }
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false, false);
        addToDictionary(mediationAdditionalData, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, 1010}});
        sentEventWithISEventManager(IronSourceConstants.TROUBLESHOOTING_IS_INIT_FAILED, mediationAdditionalData);
        notifyPublisherAboutInitFailed(IronSource.AD_UNIT.INTERSTITIAL, false);
    }

    private void startBanner() {
        ProviderSettings providerSettings;
        synchronized (this.mIsBnLoadBeforeInitCompleted) {
            long bannerAdaptersSmartLoadTimeout = this.mCurrentServerResponse.getConfigurations().getBannerConfigurations().getBannerAdaptersSmartLoadTimeout();
            int bannerRefreshInterval = this.mCurrentServerResponse.getConfigurations().getBannerConfigurations().getBannerRefreshInterval();
            int bannerDelayLoadFailure = this.mCurrentServerResponse.getConfigurations().getBannerConfigurations().getBannerDelayLoadFailure();
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < this.mCurrentServerResponse.getProviderOrder().getBannerProviderOrder().size(); i++) {
                String str = this.mCurrentServerResponse.getProviderOrder().getBannerProviderOrder().get(i);
                if (!TextUtils.isEmpty(str) && (providerSettings = this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(str)) != null) {
                    arrayList.add(providerSettings);
                }
            }
            this.mBannerManager = new BannerManager(arrayList, this.mActivity, getIronSourceAppKey(), getIronSourceUserId(), bannerAdaptersSmartLoadTimeout, bannerRefreshInterval, bannerDelayLoadFailure);
            if (this.mIsBnLoadBeforeInitCompleted.booleanValue()) {
                this.mIsBnLoadBeforeInitCompleted = false;
                loadBanner(this.mBnLayoutToLoad, this.mBnPlacementToLoad);
                this.mBnLayoutToLoad = null;
                this.mBnPlacementToLoad = null;
            }
        }
    }

    private boolean validateSmash(AbstractSmash abstractSmash) {
        return abstractSmash.getMaxAdsPerIteration() >= 1 && abstractSmash.getMaxAdsPerSession() >= 1;
    }

    public void onInitFailed(String str) {
        try {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager.log(ironSourceTag, "onInitFailed(reason:" + str + ")", 1);
            if (this.mListenersWrapper != null) {
                for (IronSource.AD_UNIT notifyPublisherAboutInitFailed : this.mAdUnitsToInitialize) {
                    notifyPublisherAboutInitFailed(notifyPublisherAboutInitFailed, true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onStillInProgressAfter15Secs() {
        synchronized (this.mIsBnLoadBeforeInitCompleted) {
            if (this.mIsBnLoadBeforeInitCompleted.booleanValue()) {
                this.mIsBnLoadBeforeInitCompleted = false;
                BannerCallbackThrottler.getInstance().sendBannerAdLoadFailed(this.mBnLayoutToLoad, new IronSourceError(IronSourceError.ERROR_BN_LOAD_WHILE_LONG_INITIATION, "init had failed"));
                this.mBnLayoutToLoad = null;
                this.mBnPlacementToLoad = null;
            }
        }
        if (this.mIsIsLoadBeforeInitCompleted) {
            this.mIsIsLoadBeforeInitCompleted = false;
            CallbackThrottler.getInstance().onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial"));
        }
        synchronized (this.mDemandOnlyIsLoadBeforeInitCompleted) {
            Iterator<String> it = this.mDemandOnlyIsLoadBeforeInitCompleted.iterator();
            while (it.hasNext()) {
                ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(it.next(), ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial"));
            }
            this.mDemandOnlyIsLoadBeforeInitCompleted.clear();
        }
        synchronized (this.mDemandOnlyRvLoadBeforeInitCompleted) {
            Iterator<String> it2 = this.mDemandOnlyRvLoadBeforeInitCompleted.iterator();
            while (it2.hasNext()) {
                RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(it2.next(), ErrorBuilder.buildInitFailedError("init() had failed", IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
            }
            this.mDemandOnlyRvLoadBeforeInitCompleted.clear();
        }
    }

    private void notifyPublisherAboutInitFailed(IronSource.AD_UNIT ad_unit, boolean z) {
        switch (ad_unit) {
            case REWARDED_VIDEO:
                if (this.mIsDemandOnlyRv) {
                    Iterator<String> it = this.mDemandOnlyRvLoadBeforeInitCompleted.iterator();
                    while (it.hasNext()) {
                        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(it.next(), ErrorBuilder.buildInitFailedError("initISDemandOnly() had failed", IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
                    }
                    this.mDemandOnlyRvLoadBeforeInitCompleted.clear();
                    return;
                } else if (z || isRewardedVideoConfigurationsReady() || this.mRequestedAdUnits.contains(ad_unit)) {
                    this.mListenersWrapper.onRewardedVideoAvailabilityChanged(false);
                    return;
                } else {
                    return;
                }
            case INTERSTITIAL:
                if (this.mIsDemandOnlyIs) {
                    Iterator<String> it2 = this.mDemandOnlyIsLoadBeforeInitCompleted.iterator();
                    while (it2.hasNext()) {
                        ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(it2.next(), ErrorBuilder.buildInitFailedError("initISDemandOnly() had failed", "Interstitial"));
                    }
                    this.mDemandOnlyIsLoadBeforeInitCompleted.clear();
                    return;
                } else if (this.mIsIsLoadBeforeInitCompleted) {
                    this.mIsIsLoadBeforeInitCompleted = false;
                    CallbackThrottler.getInstance().onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial"));
                    return;
                } else {
                    return;
                }
            case OFFERWALL:
                if (z || isOfferwallConfigurationsReady() || this.mRequestedAdUnits.contains(ad_unit)) {
                    this.mListenersWrapper.onOfferwallAvailable(false);
                    return;
                }
                return;
            case BANNER:
                synchronized (this.mIsBnLoadBeforeInitCompleted) {
                    if (this.mIsBnLoadBeforeInitCompleted.booleanValue()) {
                        this.mIsBnLoadBeforeInitCompleted = false;
                        BannerCallbackThrottler.getInstance().sendBannerAdLoadFailed(this.mBnLayoutToLoad, new IronSourceError(IronSourceError.ERROR_BN_INIT_FAILED_AFTER_LOAD, "Init had failed"));
                        this.mBnLayoutToLoad = null;
                        this.mBnPlacementToLoad = null;
                    }
                }
                return;
            default:
                return;
        }
    }

    private void prepareEventManagers(Activity activity) {
        if (this.mEventManagersInit != null && this.mEventManagersInit.compareAndSet(false, true)) {
            SuperLooper.getLooper().post(new GeneralPropertiesWorker(activity.getApplicationContext()));
            InterstitialEventsManager.getInstance().start(activity.getApplicationContext(), this.mIronSegment);
            RewardedVideoEventsManager.getInstance().start(activity.getApplicationContext(), this.mIronSegment);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void addOWAdapter(AbstractAdapter abstractAdapter) {
        this.mOfferwallAdapter = abstractAdapter;
    }

    /* access modifiers changed from: package-private */
    public synchronized AbstractAdapter getOfferwallAdapter(String str) {
        try {
            if (this.mOfferwallAdapter != null && this.mOfferwallAdapter.getProviderName().equals(str)) {
                return this.mOfferwallAdapter;
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            ironSourceLoggerManager.log(ironSourceTag, "getOfferwallAdapter exception: " + e, 1);
        }
        return null;
    }

    private void initializeManagers() {
        this.mLoggerManager = IronSourceLoggerManager.getLogger(0);
        this.mPublisherLogger = new PublisherLogger(null, 1);
        this.mLoggerManager.addLogger(this.mPublisherLogger);
        this.mListenersWrapper = new ListenersWrapper();
        this.mRewardedVideoManager = new RewardedVideoManager();
        this.mRewardedVideoManager.setRewardedVideoListener(this.mListenersWrapper);
        this.mInterstitialManager = new InterstitialManager();
        this.mInterstitialManager.setInterstitialListener(this.mListenersWrapper);
        this.mInterstitialManager.setRewardedInterstitialListener(this.mListenersWrapper);
        this.mOfferwallManager = new OfferwallManager();
        this.mOfferwallManager.setInternalOfferwallListener(this.mListenersWrapper);
    }

    public void onResume(Activity activity) {
        try {
            this.mActivity = activity;
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "onResume()", 1);
            if (this.mRewardedVideoManager != null) {
                this.mRewardedVideoManager.onResume(activity);
            }
            if (this.mInterstitialManager != null) {
                this.mInterstitialManager.onResume(activity);
            }
            if (this.mBannerManager != null) {
                this.mBannerManager.onResume(activity);
            }
            if (this.mProgRvManager != null) {
                this.mProgRvManager.onResume(activity);
            }
            if (this.mProgIsManager != null) {
                this.mProgIsManager.onResume(activity);
            }
            if (this.mDemandOnlyIsManager != null) {
                this.mDemandOnlyIsManager.onResume(activity);
            }
            if (this.mDemandOnlyRvManager != null) {
                this.mDemandOnlyRvManager.onResume(activity);
            }
        } catch (Throwable th) {
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, "onResume()", th);
        }
    }

    public void onPause(Activity activity) {
        try {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "onPause()", 1);
            if (this.mRewardedVideoManager != null) {
                this.mRewardedVideoManager.onPause(activity);
            }
            if (this.mInterstitialManager != null) {
                this.mInterstitialManager.onPause(activity);
            }
            if (this.mBannerManager != null) {
                this.mBannerManager.onPause(activity);
            }
            if (this.mProgRvManager != null) {
                this.mProgRvManager.onPause(activity);
            }
            if (this.mProgIsManager != null) {
                this.mProgIsManager.onPause(activity);
            }
            if (this.mDemandOnlyIsManager != null) {
                this.mDemandOnlyIsManager.onPause(activity);
            }
            if (this.mDemandOnlyRvManager != null) {
                this.mDemandOnlyRvManager.onPause(activity);
            }
        } catch (Throwable th) {
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, "onPause()", th);
        }
    }

    public synchronized void setAge(int i) {
        try {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, this.TAG + ":setAge(age:" + i + ")", 1);
            ConfigValidationResult configValidationResult = new ConfigValidationResult();
            validateAge(i, configValidationResult);
            if (configValidationResult.isValid()) {
                this.mUserAge = Integer.valueOf(i);
            } else {
                IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.API, configValidationResult.getIronSourceError().toString(), 2);
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager.logException(ironSourceTag, this.TAG + ":setAge(age:" + i + ")", e);
        }
        return;
    }

    public synchronized void setGender(String str) {
        try {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, this.TAG + ":setGender(gender:" + str + ")", 1);
            ConfigValidationResult configValidationResult = new ConfigValidationResult();
            validateGender(str, configValidationResult);
            if (configValidationResult.isValid()) {
                this.mUserGender = str;
            } else {
                IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.API, configValidationResult.getIronSourceError().toString(), 2);
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager.logException(ironSourceTag, this.TAG + ":setGender(gender:" + str + ")", e);
        }
        return;
    }

    public void setMediationSegment(String str) {
        try {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, this.TAG + ":setMediationSegment(segment:" + str + ")", 1);
            ConfigValidationResult configValidationResult = new ConfigValidationResult();
            validateSegment(str, configValidationResult);
            if (configValidationResult.isValid()) {
                this.mSegment = str;
            } else {
                IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.API, configValidationResult.getIronSourceError().toString(), 2);
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager.logException(ironSourceTag, this.TAG + ":setMediationSegment(segment:" + str + ")", e);
        }
    }

    public void setSegment(IronSourceSegment ironSourceSegment) {
        if (MediationInitializer.getInstance().getCurrentInitStatus() == MediationInitializer.EInitStatus.INIT_IN_PROGRESS || MediationInitializer.getInstance().getCurrentInitStatus() == MediationInitializer.EInitStatus.INITIATED) {
            IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.API, "Segments must be set prior to Init. Setting a segment after the init will be ignored", 0);
        } else {
            this.mIronSegment = ironSourceSegment;
        }
    }

    public boolean setDynamicUserId(String str) {
        try {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, this.TAG + ":setDynamicUserId(dynamicUserId:" + str + ")", 1);
            ConfigValidationResult configValidationResult = new ConfigValidationResult();
            validateDynamicUserId(str, configValidationResult);
            if (configValidationResult.isValid()) {
                this.mDynamicUserId = str;
                return true;
            }
            IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.API, configValidationResult.getIronSourceError().toString(), 2);
            return false;
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager.logException(ironSourceTag, this.TAG + ":setDynamicUserId(dynamicUserId:" + str + ")", e);
            return false;
        }
    }

    public void setAdaptersDebug(boolean z) {
        IronSourceLoggerManager.getLogger().setAdaptersDebug(z);
    }

    public void setMediationType(String str) {
        try {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, this.TAG + ":setMediationType(mediationType:" + str + ")", 1);
            if (!validateLength(str, 1, 64) || !validateAlphanumeric(str)) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, " mediationType value is invalid - should be alphanumeric and 1-64 chars in length", 1);
            } else {
                this.mMediationType = str;
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager.logException(ironSourceTag, this.TAG + ":setMediationType(mediationType:" + str + ")", e);
        }
    }

    public synchronized Integer getAge() {
        return this.mUserAge;
    }

    public synchronized String getGender() {
        return this.mUserGender;
    }

    /* access modifiers changed from: package-private */
    public synchronized String getMediationSegment() {
        return this.mSegment;
    }

    /* access modifiers changed from: package-private */
    public synchronized String getDynamicUserId() {
        return this.mDynamicUserId;
    }

    /* access modifiers changed from: package-private */
    public synchronized Map<String, String> getRvServerParams() {
        return this.mRvServerParams;
    }

    public synchronized String getMediationType() {
        return this.mMediationType;
    }

    public void showRewardedVideo() {
        if (!isRewardedVideoConfigurationsReady()) {
            this.mListenersWrapper.onRewardedVideoAdShowFailed(ErrorBuilder.buildInitFailedError("showRewardedVideo can't be called before the Rewarded Video ad unit initialization completed successfully", IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "showRewardedVideo can't be called before the Rewarded Video ad unit initialization completed successfully", 3);
            return;
        }
        Placement defaultRewardedVideoPlacement = getDefaultRewardedVideoPlacement();
        if (defaultRewardedVideoPlacement == null) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "showRewardedVideo error: empty default placement in response", 3);
            this.mListenersWrapper.onRewardedVideoAdShowFailed(new IronSourceError(1021, "showRewardedVideo error: empty default placement in response"));
            return;
        }
        showRewardedVideo(defaultRewardedVideoPlacement.getPlacementName());
    }

    private Placement getRewardedVideoPlacement(String str) {
        RewardedVideoConfigurations rewardedVideoConfigurations = this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations();
        if (rewardedVideoConfigurations != null) {
            return rewardedVideoConfigurations.getRewardedVideoPlacement(str);
        }
        return null;
    }

    private Placement getDefaultRewardedVideoPlacement() {
        RewardedVideoConfigurations rewardedVideoConfigurations = this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations();
        if (rewardedVideoConfigurations != null) {
            return rewardedVideoConfigurations.getDefaultRewardedVideoPlacement();
        }
        return null;
    }

    private void showProgrammaticRewardedVideo(String str) {
        Placement rewardedVideoPlacement = getRewardedVideoPlacement(str);
        if (rewardedVideoPlacement == null) {
            rewardedVideoPlacement = getDefaultRewardedVideoPlacement();
        }
        if (rewardedVideoPlacement == null) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "showProgrammaticRewardedVideo error: empty default placement in response", 3);
            this.mListenersWrapper.onRewardedVideoAdShowFailed(new IronSourceError(1021, "showProgrammaticRewardedVideo error: empty default placement in response"));
            return;
        }
        this.mProgRvManager.showRewardedVideo(rewardedVideoPlacement);
    }

    public void showRewardedVideo(String str) {
        String str2 = "showRewardedVideo(" + str + ")";
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, str2, 1);
        try {
            if (this.mIsDemandOnlyRv) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "Rewarded Video was initialized in demand only mode. Use showISDemandOnlyRewardedVideo instead", 3);
                this.mListenersWrapper.onRewardedVideoAdShowFailed(ErrorBuilder.buildInitFailedError("Rewarded Video was initialized in demand only mode. Use showISDemandOnlyRewardedVideo instead", IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
            } else if (!isRewardedVideoConfigurationsReady()) {
                this.mListenersWrapper.onRewardedVideoAdShowFailed(ErrorBuilder.buildInitFailedError("showRewardedVideo can't be called before the Rewarded Video ad unit initialization completed successfully", IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
            } else if (!this.mIsRvProgrammatic || this.mProgRvManager == null) {
                Placement placementToShowWithEvent = getPlacementToShowWithEvent(str);
                if (placementToShowWithEvent != null) {
                    this.mRewardedVideoManager.setCurrentPlacement(placementToShowWithEvent);
                    this.mRewardedVideoManager.showRewardedVideo(placementToShowWithEvent.getPlacementName());
                }
            } else {
                showProgrammaticRewardedVideo(str);
            }
        } catch (Exception e) {
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, str2, e);
            this.mListenersWrapper.onRewardedVideoAdShowFailed(new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, e.getMessage()));
        }
    }

    public boolean isRewardedVideoAvailable() {
        Throwable th;
        boolean z;
        try {
            if (this.mIsDemandOnlyRv) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "Rewarded Video was initialized in demand only mode. Use isISDemandOnlyRewardedVideoAvailable instead", 3);
                return false;
            }
            if (this.mIsRvProgrammatic) {
                z = this.mProgRvManager != null && this.mProgRvManager.isRewardedVideoAvailable();
            } else {
                z = this.mRewardedVideoManager.isRewardedVideoAvailable();
            }
            try {
                JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
                if (this.mIsRvProgrammatic) {
                    addToDictionary(mediationAdditionalData, new Object[][]{new Object[]{IronSourceConstants.EVENTS_PROGRAMMATIC, 1}});
                }
                RewardedVideoEventsManager.getInstance().log(new EventData(z ? IronSourceConstants.RV_API_HAS_AVAILABILITY_TRUE : IronSourceConstants.RV_API_HAS_AVAILABILITY_FALSE, mediationAdditionalData));
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
                ironSourceLoggerManager.log(ironSourceTag, "isRewardedVideoAvailable():" + z, 1);
                return z;
            } catch (Throwable th2) {
                th = th2;
                IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
                IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.API;
                ironSourceLoggerManager2.log(ironSourceTag2, "isRewardedVideoAvailable():" + z, 1);
                this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, "isRewardedVideoAvailable()", th);
                return false;
            }
        } catch (Throwable th3) {
            th = th3;
            z = false;
            IronSourceLoggerManager ironSourceLoggerManager22 = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag22 = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager22.log(ironSourceTag22, "isRewardedVideoAvailable():" + z, 1);
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, "isRewardedVideoAvailable()", th);
            return false;
        }
    }

    public void setRewardedVideoListener(RewardedVideoListener rewardedVideoListener) {
        if (rewardedVideoListener == null) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "setRewardedVideoListener(RVListener:null)", 1);
        } else {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "setRewardedVideoListener(RVListener)", 1);
        }
        this.mListenersWrapper.setRewardedVideoListener(rewardedVideoListener);
        RVListenerWrapper.getInstance().setListener(rewardedVideoListener);
    }

    public void setRewardedVideoServerParameters(Map<String, String> map) {
        if (map != null) {
            try {
                if (map.size() != 0) {
                    this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, this.TAG + ":setRewardedVideoServerParameters(params:" + map.toString() + ")", 1);
                    this.mRvServerParams = new HashMap(map);
                }
            } catch (Exception e) {
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
                ironSourceLoggerManager.logException(ironSourceTag, this.TAG + ":setRewardedVideoServerParameters(params:" + map.toString() + ")", e);
            }
        }
    }

    public void clearRewardedVideoServerParameters() {
        this.mRvServerParams = null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a6, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00b9, code lost:
        if (r5.mCurrentServerResponse == null) goto L_0x00d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c1, code lost:
        if (r5.mCurrentServerResponse.getConfigurations() == null) goto L_0x00d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00cd, code lost:
        if (r5.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations() != null) goto L_0x00d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00d0, code lost:
        r5.mDemandOnlyRvManager.loadRewardedVideo(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00d6, code lost:
        r5.mLoggerManager.log(com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API, "No rewarded video configurations found", 3);
        com.ironsource.mediationsdk.RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(r6, com.ironsource.mediationsdk.utils.ErrorBuilder.buildInitFailedError("the server response does not contain rewarded video data", com.ironsource.mediationsdk.utils.IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ef, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void loadDemandOnlyRewardedVideo(java.lang.String r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r5.mLoggerManager     // Catch:{ all -> 0x0111 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x0111 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0111 }
            r2.<init>()     // Catch:{ all -> 0x0111 }
            java.lang.String r3 = "loadISDemandOnlyRewardedVideo() instanceId="
            r2.append(r3)     // Catch:{ all -> 0x0111 }
            r2.append(r6)     // Catch:{ all -> 0x0111 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0111 }
            r3 = 1
            r0.log(r1, r2, r3)     // Catch:{ all -> 0x0111 }
            boolean r0 = r5.mDidInitRewardedVideo     // Catch:{ Throwable -> 0x00f3 }
            r1 = 508(0x1fc, float:7.12E-43)
            r2 = 3
            if (r0 != 0) goto L_0x0038
            java.lang.String r0 = "initISDemandOnly() must be called before loadISDemandOnlyRewardedVideo()"
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r3 = r5.mLoggerManager     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r4 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Throwable -> 0x00f3 }
            r3.log(r4, r0, r2)     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.RVDemandOnlyListenerWrapper r2 = com.ironsource.mediationsdk.RVDemandOnlyListenerWrapper.getInstance()     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.logger.IronSourceError r3 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ Throwable -> 0x00f3 }
            r3.<init>(r1, r0)     // Catch:{ Throwable -> 0x00f3 }
            r2.onRewardedVideoAdLoadFailed(r6, r3)     // Catch:{ Throwable -> 0x00f3 }
            monitor-exit(r5)
            return
        L_0x0038:
            boolean r0 = r5.mIsDemandOnlyRv     // Catch:{ Throwable -> 0x00f3 }
            if (r0 != 0) goto L_0x0053
            java.lang.String r0 = "Rewarded video was initialized in mediation mode"
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r3 = r5.mLoggerManager     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r4 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Throwable -> 0x00f3 }
            r3.log(r4, r0, r2)     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.RVDemandOnlyListenerWrapper r2 = com.ironsource.mediationsdk.RVDemandOnlyListenerWrapper.getInstance()     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.logger.IronSourceError r3 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ Throwable -> 0x00f3 }
            r3.<init>(r1, r0)     // Catch:{ Throwable -> 0x00f3 }
            r2.onRewardedVideoAdLoadFailed(r6, r3)     // Catch:{ Throwable -> 0x00f3 }
            monitor-exit(r5)
            return
        L_0x0053:
            com.ironsource.mediationsdk.MediationInitializer r0 = com.ironsource.mediationsdk.MediationInitializer.getInstance()     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r0 = r0.getCurrentInitStatus()     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r1 = com.ironsource.mediationsdk.MediationInitializer.EInitStatus.INIT_FAILED     // Catch:{ Throwable -> 0x00f3 }
            if (r0 != r1) goto L_0x0079
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r5.mLoggerManager     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r3 = "init() had failed"
            r0.log(r1, r3, r2)     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.RVDemandOnlyListenerWrapper r0 = com.ironsource.mediationsdk.RVDemandOnlyListenerWrapper.getInstance()     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r1 = "init() had failed"
            java.lang.String r2 = "Rewarded Video"
            com.ironsource.mediationsdk.logger.IronSourceError r1 = com.ironsource.mediationsdk.utils.ErrorBuilder.buildInitFailedError(r1, r2)     // Catch:{ Throwable -> 0x00f3 }
            r0.onRewardedVideoAdLoadFailed(r6, r1)     // Catch:{ Throwable -> 0x00f3 }
            monitor-exit(r5)
            return
        L_0x0079:
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r1 = com.ironsource.mediationsdk.MediationInitializer.EInitStatus.INIT_IN_PROGRESS     // Catch:{ Throwable -> 0x00f3 }
            if (r0 != r1) goto L_0x00a7
            com.ironsource.mediationsdk.MediationInitializer r0 = com.ironsource.mediationsdk.MediationInitializer.getInstance()     // Catch:{ Throwable -> 0x00f3 }
            boolean r0 = r0.isInProgressMoreThan15Secs()     // Catch:{ Throwable -> 0x00f3 }
            if (r0 == 0) goto L_0x00a0
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r5.mLoggerManager     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r3 = "init() had failed"
            r0.log(r1, r3, r2)     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.RVDemandOnlyListenerWrapper r0 = com.ironsource.mediationsdk.RVDemandOnlyListenerWrapper.getInstance()     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r1 = "init() had failed"
            java.lang.String r2 = "Rewarded Video"
            com.ironsource.mediationsdk.logger.IronSourceError r1 = com.ironsource.mediationsdk.utils.ErrorBuilder.buildInitFailedError(r1, r2)     // Catch:{ Throwable -> 0x00f3 }
            r0.onRewardedVideoAdLoadFailed(r6, r1)     // Catch:{ Throwable -> 0x00f3 }
            goto L_0x00a5
        L_0x00a0:
            java.util.concurrent.CopyOnWriteArraySet<java.lang.String> r0 = r5.mDemandOnlyRvLoadBeforeInitCompleted     // Catch:{ Throwable -> 0x00f3 }
            r0.add(r6)     // Catch:{ Throwable -> 0x00f3 }
        L_0x00a5:
            monitor-exit(r5)
            return
        L_0x00a7:
            java.util.concurrent.CopyOnWriteArraySet<java.lang.String> r0 = r5.mDemandOnlyRvLoadBeforeInitCompleted     // Catch:{ Throwable -> 0x00f3 }
            monitor-enter(r0)     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.DemandOnlyRvManager r1 = r5.mDemandOnlyRvManager     // Catch:{ all -> 0x00f0 }
            if (r1 != 0) goto L_0x00b6
            java.util.concurrent.CopyOnWriteArraySet<java.lang.String> r1 = r5.mDemandOnlyRvLoadBeforeInitCompleted     // Catch:{ all -> 0x00f0 }
            r1.add(r6)     // Catch:{ all -> 0x00f0 }
            monitor-exit(r0)     // Catch:{ all -> 0x00f0 }
            monitor-exit(r5)
            return
        L_0x00b6:
            monitor-exit(r0)     // Catch:{ all -> 0x00f0 }
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r5.mCurrentServerResponse     // Catch:{ Throwable -> 0x00f3 }
            if (r0 == 0) goto L_0x00d6
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r5.mCurrentServerResponse     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.model.Configurations r0 = r0.getConfigurations()     // Catch:{ Throwable -> 0x00f3 }
            if (r0 == 0) goto L_0x00d6
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r5.mCurrentServerResponse     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.model.Configurations r0 = r0.getConfigurations()     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.model.RewardedVideoConfigurations r0 = r0.getRewardedVideoConfigurations()     // Catch:{ Throwable -> 0x00f3 }
            if (r0 != 0) goto L_0x00d0
            goto L_0x00d6
        L_0x00d0:
            com.ironsource.mediationsdk.DemandOnlyRvManager r0 = r5.mDemandOnlyRvManager     // Catch:{ Throwable -> 0x00f3 }
            r0.loadRewardedVideo(r6)     // Catch:{ Throwable -> 0x00f3 }
            goto L_0x010f
        L_0x00d6:
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r5.mLoggerManager     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r3 = "No rewarded video configurations found"
            r0.log(r1, r3, r2)     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.RVDemandOnlyListenerWrapper r0 = com.ironsource.mediationsdk.RVDemandOnlyListenerWrapper.getInstance()     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r1 = "the server response does not contain rewarded video data"
            java.lang.String r2 = "Rewarded Video"
            com.ironsource.mediationsdk.logger.IronSourceError r1 = com.ironsource.mediationsdk.utils.ErrorBuilder.buildInitFailedError(r1, r2)     // Catch:{ Throwable -> 0x00f3 }
            r0.onRewardedVideoAdLoadFailed(r6, r1)     // Catch:{ Throwable -> 0x00f3 }
            monitor-exit(r5)
            return
        L_0x00f0:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00f0 }
            throw r1     // Catch:{ Throwable -> 0x00f3 }
        L_0x00f3:
            r0 = move-exception
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r1 = r5.mLoggerManager     // Catch:{ all -> 0x0111 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r2 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x0111 }
            java.lang.String r3 = "loadISDemandOnlyRewardedVideo"
            r1.logException(r2, r3, r0)     // Catch:{ all -> 0x0111 }
            com.ironsource.mediationsdk.RVDemandOnlyListenerWrapper r1 = com.ironsource.mediationsdk.RVDemandOnlyListenerWrapper.getInstance()     // Catch:{ all -> 0x0111 }
            com.ironsource.mediationsdk.logger.IronSourceError r2 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ all -> 0x0111 }
            r3 = 510(0x1fe, float:7.15E-43)
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0111 }
            r2.<init>(r3, r0)     // Catch:{ all -> 0x0111 }
            r1.onRewardedVideoAdLoadFailed(r6, r2)     // Catch:{ all -> 0x0111 }
        L_0x010f:
            monitor-exit(r5)
            return
        L_0x0111:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.IronSourceObject.loadDemandOnlyRewardedVideo(java.lang.String):void");
    }

    public synchronized void showDemandOnlyRewardedVideo(String str) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
        ironSourceLoggerManager.log(ironSourceTag, "showISDemandOnlyRewardedVideo() instanceId=" + str, 1);
        try {
            if (!this.mIsDemandOnlyRv) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "Rewarded video was initialized in mediation mode. Use showRewardedVideo instead", 3);
                RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdShowFailed(str, new IronSourceError(IronSourceError.ERROR_CODE_INIT_FAILED, "Rewarded video was initialized in mediation mode. Use showRewardedVideo instead"));
                return;
            } else if (this.mDemandOnlyRvManager == null) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "Rewarded video was not initiated", 3);
                RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdShowFailed(str, new IronSourceError(IronSourceError.ERROR_CODE_INIT_FAILED, "Rewarded video was not initiated"));
                return;
            } else {
                this.mDemandOnlyRvManager.showRewardedVideo(str);
            }
        } catch (Exception e) {
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, "showISDemandOnlyRewardedVideo", e);
            RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdShowFailed(str, new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, e.getMessage()));
        }
        return;
    }

    public synchronized boolean isDemandOnlyRewardedVideoAvailable(String str) {
        return this.mDemandOnlyRvManager != null && this.mDemandOnlyRvManager.isRewardedVideoAvailable(str);
    }

    /* access modifiers changed from: package-private */
    public void setISDemandOnlyRewardedVideoListener(ISDemandOnlyRewardedVideoListener iSDemandOnlyRewardedVideoListener) {
        RVDemandOnlyListenerWrapper.getInstance().setListener(iSDemandOnlyRewardedVideoListener);
    }

    private boolean isRewardedVideoConfigurationsReady() {
        return (this.mCurrentServerResponse == null || this.mCurrentServerResponse.getConfigurations() == null || this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations() == null) ? false : true;
    }

    private Placement getPlacementToShowWithEvent(String str) {
        Placement rewardedVideoPlacement = getRewardedVideoPlacement(str);
        if (rewardedVideoPlacement == null) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "Placement is not valid, please make sure you are using the right placements, using the default placement.", 3);
            rewardedVideoPlacement = getDefaultRewardedVideoPlacement();
            if (rewardedVideoPlacement == null) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "Default placement was not found, please make sure you are using the right placements.", 3);
                return null;
            }
        }
        String cappingMessage = getCappingMessage(rewardedVideoPlacement.getPlacementName(), CappingManager.isPlacementCapped(this.mActivity, rewardedVideoPlacement));
        if (TextUtils.isEmpty(cappingMessage)) {
            return rewardedVideoPlacement;
        }
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, cappingMessage, 1);
        this.mListenersWrapper.onRewardedVideoAdShowFailed(ErrorBuilder.buildCappedPerPlacementError(cappingMessage));
        return null;
    }

    public void loadInterstitial() {
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "loadInterstitial()", 1);
        try {
            if (this.mIsDemandOnlyIs) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "Interstitial was initialized in demand only mode. Use loadISDemandOnlyInterstitial instead", 3);
                CallbackThrottler.getInstance().onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("Interstitial was initialized in demand only mode. Use loadISDemandOnlyInterstitial instead", "Interstitial"));
            } else if (!this.mDidInitInterstitial) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "init() must be called before loadInterstitial()", 3);
                CallbackThrottler.getInstance().onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("init() must be called before loadInterstitial()", "Interstitial"));
            } else {
                MediationInitializer.EInitStatus currentInitStatus = MediationInitializer.getInstance().getCurrentInitStatus();
                if (currentInitStatus == MediationInitializer.EInitStatus.INIT_FAILED) {
                    this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "init() had failed", 3);
                    CallbackThrottler.getInstance().onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial"));
                } else if (currentInitStatus != MediationInitializer.EInitStatus.INIT_IN_PROGRESS) {
                    if (!(this.mCurrentServerResponse == null || this.mCurrentServerResponse.getConfigurations() == null)) {
                        if (this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations() != null) {
                            if (!this.mIsIsProgrammatic) {
                                this.mInterstitialManager.loadInterstitial();
                                return;
                            } else if (this.mProgIsManager == null) {
                                this.mIsIsLoadBeforeInitCompleted = true;
                                return;
                            } else {
                                this.mProgIsManager.loadInterstitial();
                                return;
                            }
                        }
                    }
                    this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "No interstitial configurations found", 3);
                    CallbackThrottler.getInstance().onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("the server response does not contain interstitial data", "Interstitial"));
                } else if (MediationInitializer.getInstance().isInProgressMoreThan15Secs()) {
                    this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "init() had failed", 3);
                    CallbackThrottler.getInstance().onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial"));
                } else {
                    this.mIsIsLoadBeforeInitCompleted = true;
                }
            }
        } catch (Throwable th) {
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, "loadInterstitial()", th);
            CallbackThrottler.getInstance().onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, th.getMessage()));
        }
    }

    public void showInterstitial() {
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "showInterstitial()", 1);
        try {
            if (this.mIsDemandOnlyIs) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "Interstitial was initialized in demand only mode. Use showISDemandOnlyInterstitial instead", 3);
                this.mListenersWrapper.onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, "Interstitial was initialized in demand only mode. Use showISDemandOnlyInterstitial instead"));
            } else if (!isInterstitialConfigurationsReady()) {
                this.mListenersWrapper.onInterstitialAdShowFailed(ErrorBuilder.buildInitFailedError("showInterstitial can't be called before the Interstitial ad unit initialization completed successfully", "Interstitial"));
            } else {
                InterstitialPlacement defaultInterstitialPlacement = getDefaultInterstitialPlacement();
                if (defaultInterstitialPlacement != null) {
                    showInterstitial(defaultInterstitialPlacement.getPlacementName());
                } else {
                    this.mListenersWrapper.onInterstitialAdShowFailed(new IronSourceError(1020, "showInterstitial error: empty default placement in response"));
                }
            }
        } catch (Exception e) {
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, "showInterstitial()", e);
            this.mListenersWrapper.onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, e.getMessage()));
        }
    }

    public void showInterstitial(String str) {
        String str2 = "showInterstitial(" + str + ")";
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, str2, 1);
        try {
            if (this.mIsDemandOnlyIs) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "Interstitial was initialized in demand only mode. Use showISDemandOnlyInterstitial instead", 3);
                this.mListenersWrapper.onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, "Interstitial was initialized in demand only mode. Use showISDemandOnlyInterstitial instead"));
            } else if (!isInterstitialConfigurationsReady()) {
                this.mListenersWrapper.onInterstitialAdShowFailed(ErrorBuilder.buildInitFailedError("showInterstitial can't be called before the Interstitial ad unit initialization completed successfully", "Interstitial"));
            } else if (this.mIsIsProgrammatic) {
                showProgrammaticInterstitial(str);
            } else {
                InterstitialPlacement interstitialPlacementToShowWithEvent = getInterstitialPlacementToShowWithEvent(str);
                JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
                if (interstitialPlacementToShowWithEvent != null) {
                    try {
                        mediationAdditionalData.put("placement", interstitialPlacementToShowWithEvent.getPlacementName());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (!TextUtils.isEmpty(str)) {
                    mediationAdditionalData.put("placement", str);
                }
                InterstitialEventsManager.getInstance().log(new EventData(2100, mediationAdditionalData));
                if (interstitialPlacementToShowWithEvent != null) {
                    this.mInterstitialManager.setCurrentPlacement(interstitialPlacementToShowWithEvent);
                    this.mInterstitialManager.showInterstitial(interstitialPlacementToShowWithEvent.getPlacementName());
                }
            }
        } catch (Exception e2) {
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, str2, e2);
            this.mListenersWrapper.onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, e2.getMessage()));
        }
    }

    private InterstitialPlacement getInterstitialPlacement(String str) {
        InterstitialConfigurations interstitialConfigurations = this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations();
        if (interstitialConfigurations != null) {
            return interstitialConfigurations.getInterstitialPlacement(str);
        }
        return null;
    }

    private InterstitialPlacement getDefaultInterstitialPlacement() {
        InterstitialConfigurations interstitialConfigurations = this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations();
        if (interstitialConfigurations != null) {
            return interstitialConfigurations.getDefaultInterstitialPlacement();
        }
        return null;
    }

    private void showProgrammaticInterstitial(String str) {
        String str2 = null;
        try {
            InterstitialPlacement interstitialPlacement = getInterstitialPlacement(str);
            if (interstitialPlacement == null) {
                interstitialPlacement = getDefaultInterstitialPlacement();
            }
            if (interstitialPlacement != null) {
                str2 = interstitialPlacement.getPlacementName();
            }
        } catch (Exception e) {
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, "showProgrammaticInterstitial()", e);
        }
        this.mProgIsManager.showInterstitial(str2);
    }

    public boolean isInterstitialReady() {
        Throwable th;
        boolean z;
        try {
            if (this.mIsDemandOnlyIs) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "Interstitial was initialized in demand only mode. Use isISDemandOnlyInterstitialReady instead", 3);
                return false;
            }
            z = !this.mIsIsProgrammatic ? !(this.mInterstitialManager == null || !this.mInterstitialManager.isInterstitialReady()) : !(this.mProgIsManager == null || !this.mProgIsManager.isInterstitialReady());
            try {
                InterstitialEventsManager.getInstance().log(new EventData(z ? IronSourceConstants.IS_CHECK_READY_TRUE : IronSourceConstants.IS_CHECK_READY_FALSE, IronSourceUtils.getMediationAdditionalData(false, this.mIsIsProgrammatic)));
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
                ironSourceLoggerManager.log(ironSourceTag, "isInterstitialReady():" + z, 1);
                return z;
            } catch (Throwable th2) {
                th = th2;
                IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
                IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.API;
                ironSourceLoggerManager2.log(ironSourceTag2, "isInterstitialReady():" + z, 1);
                this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, "isInterstitialReady()", th);
                return false;
            }
        } catch (Throwable th3) {
            th = th3;
            z = false;
            IronSourceLoggerManager ironSourceLoggerManager22 = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag22 = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager22.log(ironSourceTag22, "isInterstitialReady():" + z, 1);
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, "isInterstitialReady()", th);
            return false;
        }
    }

    public void setInterstitialListener(InterstitialListener interstitialListener) {
        if (interstitialListener == null) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "setInterstitialListener(ISListener:null)", 1);
        } else {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "setInterstitialListener(ISListener)", 1);
        }
        this.mListenersWrapper.setInterstitialListener(interstitialListener);
        ISListenerWrapper.getInstance().setListener(interstitialListener);
        CallbackThrottler.getInstance().setInterstitialListener(interstitialListener);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a6, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b9, code lost:
        if (r5.mCurrentServerResponse == null) goto L_0x00d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00c1, code lost:
        if (r5.mCurrentServerResponse.getConfigurations() == null) goto L_0x00d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00cd, code lost:
        if (r5.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations() != null) goto L_0x00d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00d0, code lost:
        r5.mDemandOnlyIsManager.loadInterstitial(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00d6, code lost:
        r5.mLoggerManager.log(com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API, "No interstitial configurations found", 3);
        com.ironsource.mediationsdk.ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(r6, com.ironsource.mediationsdk.utils.ErrorBuilder.buildInitFailedError("the server response does not contain interstitial data", "Interstitial"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00ef, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void loadDemandOnlyInterstitial(java.lang.String r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r5.mLoggerManager     // Catch:{ all -> 0x010f }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x010f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x010f }
            r2.<init>()     // Catch:{ all -> 0x010f }
            java.lang.String r3 = "loadISDemandOnlyInterstitial() instanceId="
            r2.append(r3)     // Catch:{ all -> 0x010f }
            r2.append(r6)     // Catch:{ all -> 0x010f }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x010f }
            r3 = 1
            r0.log(r1, r2, r3)     // Catch:{ all -> 0x010f }
            r0 = 510(0x1fe, float:7.15E-43)
            boolean r1 = r5.mDidInitInterstitial     // Catch:{ Throwable -> 0x00f3 }
            r2 = 3
            if (r1 != 0) goto L_0x0038
            java.lang.String r1 = "initISDemandOnly() must be called before loadISDemandOnlyInterstitial()"
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r3 = r5.mLoggerManager     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r4 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Throwable -> 0x00f3 }
            r3.log(r4, r1, r2)     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.ISDemandOnlyListenerWrapper r2 = com.ironsource.mediationsdk.ISDemandOnlyListenerWrapper.getInstance()     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.logger.IronSourceError r3 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ Throwable -> 0x00f3 }
            r3.<init>(r0, r1)     // Catch:{ Throwable -> 0x00f3 }
            r2.onInterstitialAdLoadFailed(r6, r3)     // Catch:{ Throwable -> 0x00f3 }
            monitor-exit(r5)
            return
        L_0x0038:
            boolean r1 = r5.mIsDemandOnlyIs     // Catch:{ Throwable -> 0x00f3 }
            if (r1 != 0) goto L_0x0053
            java.lang.String r1 = "Interstitial was initialized in mediation mode. Use loadInterstitial instead"
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r3 = r5.mLoggerManager     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r4 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Throwable -> 0x00f3 }
            r3.log(r4, r1, r2)     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.ISDemandOnlyListenerWrapper r2 = com.ironsource.mediationsdk.ISDemandOnlyListenerWrapper.getInstance()     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.logger.IronSourceError r3 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ Throwable -> 0x00f3 }
            r3.<init>(r0, r1)     // Catch:{ Throwable -> 0x00f3 }
            r2.onInterstitialAdLoadFailed(r6, r3)     // Catch:{ Throwable -> 0x00f3 }
            monitor-exit(r5)
            return
        L_0x0053:
            com.ironsource.mediationsdk.MediationInitializer r1 = com.ironsource.mediationsdk.MediationInitializer.getInstance()     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r1 = r1.getCurrentInitStatus()     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r3 = com.ironsource.mediationsdk.MediationInitializer.EInitStatus.INIT_FAILED     // Catch:{ Throwable -> 0x00f3 }
            if (r1 != r3) goto L_0x0079
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r1 = r5.mLoggerManager     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r3 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r4 = "init() had failed"
            r1.log(r3, r4, r2)     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.ISDemandOnlyListenerWrapper r1 = com.ironsource.mediationsdk.ISDemandOnlyListenerWrapper.getInstance()     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r2 = "init() had failed"
            java.lang.String r3 = "Interstitial"
            com.ironsource.mediationsdk.logger.IronSourceError r2 = com.ironsource.mediationsdk.utils.ErrorBuilder.buildInitFailedError(r2, r3)     // Catch:{ Throwable -> 0x00f3 }
            r1.onInterstitialAdLoadFailed(r6, r2)     // Catch:{ Throwable -> 0x00f3 }
            monitor-exit(r5)
            return
        L_0x0079:
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r3 = com.ironsource.mediationsdk.MediationInitializer.EInitStatus.INIT_IN_PROGRESS     // Catch:{ Throwable -> 0x00f3 }
            if (r1 != r3) goto L_0x00a7
            com.ironsource.mediationsdk.MediationInitializer r1 = com.ironsource.mediationsdk.MediationInitializer.getInstance()     // Catch:{ Throwable -> 0x00f3 }
            boolean r1 = r1.isInProgressMoreThan15Secs()     // Catch:{ Throwable -> 0x00f3 }
            if (r1 == 0) goto L_0x00a0
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r1 = r5.mLoggerManager     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r3 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r4 = "init() had failed"
            r1.log(r3, r4, r2)     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.ISDemandOnlyListenerWrapper r1 = com.ironsource.mediationsdk.ISDemandOnlyListenerWrapper.getInstance()     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r2 = "init() had failed"
            java.lang.String r3 = "Interstitial"
            com.ironsource.mediationsdk.logger.IronSourceError r2 = com.ironsource.mediationsdk.utils.ErrorBuilder.buildInitFailedError(r2, r3)     // Catch:{ Throwable -> 0x00f3 }
            r1.onInterstitialAdLoadFailed(r6, r2)     // Catch:{ Throwable -> 0x00f3 }
            goto L_0x00a5
        L_0x00a0:
            java.util.concurrent.CopyOnWriteArraySet<java.lang.String> r1 = r5.mDemandOnlyIsLoadBeforeInitCompleted     // Catch:{ Throwable -> 0x00f3 }
            r1.add(r6)     // Catch:{ Throwable -> 0x00f3 }
        L_0x00a5:
            monitor-exit(r5)
            return
        L_0x00a7:
            java.util.concurrent.CopyOnWriteArraySet<java.lang.String> r1 = r5.mDemandOnlyIsLoadBeforeInitCompleted     // Catch:{ Throwable -> 0x00f3 }
            monitor-enter(r1)     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.DemandOnlyIsManager r3 = r5.mDemandOnlyIsManager     // Catch:{ all -> 0x00f0 }
            if (r3 != 0) goto L_0x00b6
            java.util.concurrent.CopyOnWriteArraySet<java.lang.String> r2 = r5.mDemandOnlyIsLoadBeforeInitCompleted     // Catch:{ all -> 0x00f0 }
            r2.add(r6)     // Catch:{ all -> 0x00f0 }
            monitor-exit(r1)     // Catch:{ all -> 0x00f0 }
            monitor-exit(r5)
            return
        L_0x00b6:
            monitor-exit(r1)     // Catch:{ all -> 0x00f0 }
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r1 = r5.mCurrentServerResponse     // Catch:{ Throwable -> 0x00f3 }
            if (r1 == 0) goto L_0x00d6
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r1 = r5.mCurrentServerResponse     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.model.Configurations r1 = r1.getConfigurations()     // Catch:{ Throwable -> 0x00f3 }
            if (r1 == 0) goto L_0x00d6
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r1 = r5.mCurrentServerResponse     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.model.Configurations r1 = r1.getConfigurations()     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.model.InterstitialConfigurations r1 = r1.getInterstitialConfigurations()     // Catch:{ Throwable -> 0x00f3 }
            if (r1 != 0) goto L_0x00d0
            goto L_0x00d6
        L_0x00d0:
            com.ironsource.mediationsdk.DemandOnlyIsManager r1 = r5.mDemandOnlyIsManager     // Catch:{ Throwable -> 0x00f3 }
            r1.loadInterstitial(r6)     // Catch:{ Throwable -> 0x00f3 }
            goto L_0x010d
        L_0x00d6:
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r1 = r5.mLoggerManager     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r3 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r4 = "No interstitial configurations found"
            r1.log(r3, r4, r2)     // Catch:{ Throwable -> 0x00f3 }
            com.ironsource.mediationsdk.ISDemandOnlyListenerWrapper r1 = com.ironsource.mediationsdk.ISDemandOnlyListenerWrapper.getInstance()     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r2 = "the server response does not contain interstitial data"
            java.lang.String r3 = "Interstitial"
            com.ironsource.mediationsdk.logger.IronSourceError r2 = com.ironsource.mediationsdk.utils.ErrorBuilder.buildInitFailedError(r2, r3)     // Catch:{ Throwable -> 0x00f3 }
            r1.onInterstitialAdLoadFailed(r6, r2)     // Catch:{ Throwable -> 0x00f3 }
            monitor-exit(r5)
            return
        L_0x00f0:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00f0 }
            throw r2     // Catch:{ Throwable -> 0x00f3 }
        L_0x00f3:
            r1 = move-exception
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r2 = r5.mLoggerManager     // Catch:{ all -> 0x010f }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r3 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x010f }
            java.lang.String r4 = "loadDemandOnlyInterstitial"
            r2.logException(r3, r4, r1)     // Catch:{ all -> 0x010f }
            com.ironsource.mediationsdk.ISDemandOnlyListenerWrapper r2 = com.ironsource.mediationsdk.ISDemandOnlyListenerWrapper.getInstance()     // Catch:{ all -> 0x010f }
            com.ironsource.mediationsdk.logger.IronSourceError r3 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ all -> 0x010f }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x010f }
            r3.<init>(r0, r1)     // Catch:{ all -> 0x010f }
            r2.onInterstitialAdLoadFailed(r6, r3)     // Catch:{ all -> 0x010f }
        L_0x010d:
            monitor-exit(r5)
            return
        L_0x010f:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.IronSourceObject.loadDemandOnlyInterstitial(java.lang.String):void");
    }

    public void showDemandOnlyInterstitial(String str) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
        ironSourceLoggerManager.log(ironSourceTag, "showISDemandOnlyInterstitial() instanceId=" + str, 1);
        try {
            if (!this.mIsDemandOnlyIs) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "Interstitial was initialized in mediation mode. Use showInterstitial instead", 3);
            } else if (this.mDemandOnlyIsManager == null) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "Interstitial video was not initiated", 3);
                ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdShowFailed(str, new IronSourceError(IronSourceError.ERROR_CODE_INIT_FAILED, "Interstitial video was not initiated"));
            } else {
                this.mDemandOnlyIsManager.showInterstitial(str);
            }
        } catch (Exception e) {
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, "showISDemandOnlyInterstitial", e);
            ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdShowFailed(str, ErrorBuilder.buildInitFailedError("showISDemandOnlyInterstitial can't be called before the Interstitial ad unit initialization completed successfully", "Interstitial"));
        }
    }

    public synchronized boolean isDemandOnlyInterstitialReady(String str) {
        return this.mDemandOnlyIsManager != null && this.mDemandOnlyIsManager.isInterstitialReady(str);
    }

    public void setISDemandOnlyInterstitialListener(ISDemandOnlyInterstitialListener iSDemandOnlyInterstitialListener) {
        ISDemandOnlyListenerWrapper.getInstance().setListener(iSDemandOnlyInterstitialListener);
    }

    private boolean isInterstitialConfigurationsReady() {
        return (this.mCurrentServerResponse == null || this.mCurrentServerResponse.getConfigurations() == null || this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations() == null) ? false : true;
    }

    private InterstitialPlacement getInterstitialPlacementToShowWithEvent(String str) {
        InterstitialPlacement interstitialPlacement = getInterstitialPlacement(str);
        if (interstitialPlacement == null) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "Placement is not valid, please make sure you are using the right placements, using the default placement.", 3);
            interstitialPlacement = getDefaultInterstitialPlacement();
            if (interstitialPlacement == null) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "Default placement was not found, please make sure you are using the right placements.", 3);
                return null;
            }
        }
        String cappingMessage = getCappingMessage(interstitialPlacement.getPlacementName(), getInterstitialCappingStatus(interstitialPlacement.getPlacementName()));
        if (TextUtils.isEmpty(cappingMessage)) {
            return interstitialPlacement;
        }
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, cappingMessage, 1);
        this.mListenersWrapper.setInterstitialPlacement(interstitialPlacement);
        this.mListenersWrapper.onInterstitialAdShowFailed(ErrorBuilder.buildCappedPerPlacementError(cappingMessage));
        return null;
    }

    private boolean isOfferwallConfigurationsReady() {
        return (this.mCurrentServerResponse == null || this.mCurrentServerResponse.getConfigurations() == null || this.mCurrentServerResponse.getConfigurations().getOfferwallConfigurations() == null) ? false : true;
    }

    public void showOfferwall() {
        try {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "showOfferwall()", 1);
            if (!isOfferwallConfigurationsReady()) {
                this.mListenersWrapper.onOfferwallShowFailed(ErrorBuilder.buildInitFailedError("showOfferwall can't be called before the Offerwall ad unit initialization completed successfully", IronSourceConstants.OFFERWALL_AD_UNIT));
                return;
            }
            OfferwallPlacement defaultOfferwallPlacement = this.mCurrentServerResponse.getConfigurations().getOfferwallConfigurations().getDefaultOfferwallPlacement();
            if (defaultOfferwallPlacement != null) {
                showOfferwall(defaultOfferwallPlacement.getPlacementName());
            }
        } catch (Exception e) {
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, "showOfferwall()", e);
            this.mListenersWrapper.onOfferwallShowFailed(ErrorBuilder.buildInitFailedError("showOfferwall can't be called before the Offerwall ad unit initialization completed successfully", IronSourceConstants.OFFERWALL_AD_UNIT));
        }
    }

    public void showOfferwall(String str) {
        String str2 = "showOfferwall(" + str + ")";
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, str2, 1);
        try {
            if (!isOfferwallConfigurationsReady()) {
                this.mListenersWrapper.onOfferwallShowFailed(ErrorBuilder.buildInitFailedError("showOfferwall can't be called before the Offerwall ad unit initialization completed successfully", IronSourceConstants.OFFERWALL_AD_UNIT));
                return;
            }
            OfferwallPlacement offerwallPlacement = this.mCurrentServerResponse.getConfigurations().getOfferwallConfigurations().getOfferwallPlacement(str);
            if (offerwallPlacement == null) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "Placement is not valid, please make sure you are using the right placements, using the default placement.", 3);
                offerwallPlacement = this.mCurrentServerResponse.getConfigurations().getOfferwallConfigurations().getDefaultOfferwallPlacement();
                if (offerwallPlacement == null) {
                    this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "Default placement was not found, please make sure you are using the right placements.", 3);
                    return;
                }
            }
            this.mOfferwallManager.showOfferwall(offerwallPlacement.getPlacementName());
        } catch (Exception e) {
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, str2, e);
            this.mListenersWrapper.onOfferwallShowFailed(ErrorBuilder.buildInitFailedError("showOfferwall can't be called before the Offerwall ad unit initialization completed successfully", IronSourceConstants.OFFERWALL_AD_UNIT));
        }
    }

    public boolean isOfferwallAvailable() {
        try {
            if (this.mOfferwallManager != null) {
                return this.mOfferwallManager.isOfferwallAvailable();
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    public void getOfferwallCredits() {
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "getOfferwallCredits()", 1);
        try {
            this.mOfferwallManager.getOfferwallCredits();
        } catch (Throwable th) {
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, "getOfferwallCredits()", th);
        }
    }

    public void setOfferwallListener(OfferwallListener offerwallListener) {
        if (offerwallListener == null) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "setOfferwallListener(OWListener:null)", 1);
        } else {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "setOfferwallListener(OWListener)", 1);
        }
        this.mListenersWrapper.setOfferwallListener(offerwallListener);
    }

    public void setLogListener(LogListener logListener) {
        if (logListener == null) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "setLogListener(LogListener:null)", 1);
            return;
        }
        this.mPublisherLogger.setLogListener(logListener);
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
        ironSourceLoggerManager.log(ironSourceTag, "setLogListener(LogListener:" + logListener.getClass().getSimpleName() + ")", 1);
    }

    public void setRewardedInterstitialListener(RewardedInterstitialListener rewardedInterstitialListener) {
        this.mListenersWrapper.setRewardedInterstitialListener(rewardedInterstitialListener);
    }

    private boolean isBannerConfigurationsReady() {
        return (this.mCurrentServerResponse == null || this.mCurrentServerResponse.getConfigurations() == null || this.mCurrentServerResponse.getConfigurations().getBannerConfigurations() == null) ? false : true;
    }

    public IronSourceBannerLayout createBanner(Activity activity, ISBannerSize iSBannerSize) {
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "createBanner()", 1);
        if (activity != null) {
            return new IronSourceBannerLayout(activity, iSBannerSize);
        }
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "createBanner() : Activity cannot be null", 3);
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00df, code lost:
        if (r4.mCurrentServerResponse == null) goto L_0x0100;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00e7, code lost:
        if (r4.mCurrentServerResponse.getConfigurations() == null) goto L_0x0100;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00f3, code lost:
        if (r4.mCurrentServerResponse.getConfigurations().getBannerConfigurations() != null) goto L_0x00f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00f6, code lost:
        r4.mBannerManager.loadBanner(r5, getBannerPlacement(r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00ff, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0100, code lost:
        r4.mLoggerManager.log(com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API, "No banner configurations found", 3);
        com.ironsource.mediationsdk.BannerCallbackThrottler.getInstance().sendBannerAdLoadFailed(r5, new com.ironsource.mediationsdk.logger.IronSourceError(com.ironsource.mediationsdk.logger.IronSourceError.ERROR_BN_LOAD_NO_CONFIG, "No banner configurations found"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0119, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadBanner(com.ironsource.mediationsdk.IronSourceBannerLayout r5, java.lang.String r6) {
        /*
            r4 = this;
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r4.mLoggerManager
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "loadBanner("
            r2.append(r3)
            r2.append(r6)
            java.lang.String r3 = ")"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r3 = 1
            r0.log(r1, r2, r3)
            if (r5 != 0) goto L_0x002a
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r4.mLoggerManager
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r6 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            java.lang.String r0 = "loadBanner can't be called with a null parameter"
            r5.log(r6, r0, r3)
            return
        L_0x002a:
            boolean r0 = r4.mDidInitBanner
            r1 = 3
            if (r0 != 0) goto L_0x0039
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r4.mLoggerManager
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r6 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            java.lang.String r0 = "init() must be called before loadBanner()"
            r5.log(r6, r0, r1)
            return
        L_0x0039:
            com.ironsource.mediationsdk.ISBannerSize r0 = r5.getSize()
            java.lang.String r0 = r0.getDescription()
            java.lang.String r2 = "CUSTOM"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0074
            com.ironsource.mediationsdk.ISBannerSize r0 = r5.getSize()
            int r0 = r0.getWidth()
            if (r0 <= 0) goto L_0x005d
            com.ironsource.mediationsdk.ISBannerSize r0 = r5.getSize()
            int r0 = r0.getHeight()
            if (r0 > 0) goto L_0x0074
        L_0x005d:
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r6 = r4.mLoggerManager
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r0 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            java.lang.String r2 = "loadBanner: Unsupported banner size. Height and width must be bigger than 0"
            r6.log(r0, r2, r1)
            com.ironsource.mediationsdk.BannerCallbackThrottler r6 = com.ironsource.mediationsdk.BannerCallbackThrottler.getInstance()
            java.lang.String r0 = ""
            com.ironsource.mediationsdk.logger.IronSourceError r0 = com.ironsource.mediationsdk.utils.ErrorBuilder.unsupportedBannerSize(r0)
            r6.sendBannerAdLoadFailed(r5, r0)
            return
        L_0x0074:
            com.ironsource.mediationsdk.MediationInitializer r0 = com.ironsource.mediationsdk.MediationInitializer.getInstance()
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r0 = r0.getCurrentInitStatus()
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r2 = com.ironsource.mediationsdk.MediationInitializer.EInitStatus.INIT_FAILED
            if (r0 != r2) goto L_0x009a
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r6 = r4.mLoggerManager
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r0 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            java.lang.String r2 = "init() had failed"
            r6.log(r0, r2, r1)
            com.ironsource.mediationsdk.BannerCallbackThrottler r6 = com.ironsource.mediationsdk.BannerCallbackThrottler.getInstance()
            com.ironsource.mediationsdk.logger.IronSourceError r0 = new com.ironsource.mediationsdk.logger.IronSourceError
            r1 = 600(0x258, float:8.41E-43)
            java.lang.String r2 = "Init() had failed"
            r0.<init>(r1, r2)
            r6.sendBannerAdLoadFailed(r5, r0)
            return
        L_0x009a:
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r2 = com.ironsource.mediationsdk.MediationInitializer.EInitStatus.INIT_IN_PROGRESS
            if (r0 != r2) goto L_0x00cd
            com.ironsource.mediationsdk.MediationInitializer r0 = com.ironsource.mediationsdk.MediationInitializer.getInstance()
            boolean r0 = r0.isInProgressMoreThan15Secs()
            if (r0 == 0) goto L_0x00c2
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r6 = r4.mLoggerManager
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r0 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            java.lang.String r2 = "init() had failed"
            r6.log(r0, r2, r1)
            com.ironsource.mediationsdk.BannerCallbackThrottler r6 = com.ironsource.mediationsdk.BannerCallbackThrottler.getInstance()
            com.ironsource.mediationsdk.logger.IronSourceError r0 = new com.ironsource.mediationsdk.logger.IronSourceError
            r1 = 601(0x259, float:8.42E-43)
            java.lang.String r2 = "Init had failed"
            r0.<init>(r1, r2)
            r6.sendBannerAdLoadFailed(r5, r0)
            goto L_0x00cc
        L_0x00c2:
            r4.mBnLayoutToLoad = r5
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r3)
            r4.mIsBnLoadBeforeInitCompleted = r5
            r4.mBnPlacementToLoad = r6
        L_0x00cc:
            return
        L_0x00cd:
            java.lang.Boolean r0 = r4.mIsBnLoadBeforeInitCompleted
            monitor-enter(r0)
            com.ironsource.mediationsdk.BannerManager r2 = r4.mBannerManager     // Catch:{ all -> 0x011a }
            if (r2 != 0) goto L_0x00dc
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r3)     // Catch:{ all -> 0x011a }
            r4.mIsBnLoadBeforeInitCompleted = r5     // Catch:{ all -> 0x011a }
            monitor-exit(r0)     // Catch:{ all -> 0x011a }
            return
        L_0x00dc:
            monitor-exit(r0)     // Catch:{ all -> 0x011a }
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r4.mCurrentServerResponse
            if (r0 == 0) goto L_0x0100
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r4.mCurrentServerResponse
            com.ironsource.mediationsdk.model.Configurations r0 = r0.getConfigurations()
            if (r0 == 0) goto L_0x0100
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r4.mCurrentServerResponse
            com.ironsource.mediationsdk.model.Configurations r0 = r0.getConfigurations()
            com.ironsource.mediationsdk.model.BannerConfigurations r0 = r0.getBannerConfigurations()
            if (r0 != 0) goto L_0x00f6
            goto L_0x0100
        L_0x00f6:
            com.ironsource.mediationsdk.BannerManager r0 = r4.mBannerManager
            com.ironsource.mediationsdk.model.BannerPlacement r6 = r4.getBannerPlacement(r6)
            r0.loadBanner(r5, r6)
            return
        L_0x0100:
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r6 = r4.mLoggerManager
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r0 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            java.lang.String r2 = "No banner configurations found"
            r6.log(r0, r2, r1)
            com.ironsource.mediationsdk.BannerCallbackThrottler r6 = com.ironsource.mediationsdk.BannerCallbackThrottler.getInstance()
            com.ironsource.mediationsdk.logger.IronSourceError r0 = new com.ironsource.mediationsdk.logger.IronSourceError
            r1 = 615(0x267, float:8.62E-43)
            java.lang.String r2 = "No banner configurations found"
            r0.<init>(r1, r2)
            r6.sendBannerAdLoadFailed(r5, r0)
            return
        L_0x011a:
            r5 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x011a }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.IronSourceObject.loadBanner(com.ironsource.mediationsdk.IronSourceBannerLayout, java.lang.String):void");
    }

    public void loadBanner(IronSourceBannerLayout ironSourceBannerLayout) {
        loadBanner(ironSourceBannerLayout, "");
    }

    public void destroyBanner(IronSourceBannerLayout ironSourceBannerLayout) {
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "destroyBanner()", 1);
        try {
            if (this.mBannerManager != null) {
                this.mBannerManager.destroyBanner(ironSourceBannerLayout);
            }
        } catch (Throwable th) {
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.API, "destroyBanner()", th);
        }
    }

    /* access modifiers changed from: package-private */
    public ServerResponseWrapper getServerResponse(Context context, String str, IResponseListener iResponseListener) {
        synchronized (this.mServerResponseLocker) {
            if (this.mCurrentServerResponse != null) {
                ServerResponseWrapper serverResponseWrapper = new ServerResponseWrapper(this.mCurrentServerResponse);
                return serverResponseWrapper;
            }
            ServerResponseWrapper connectAndGetServerResponse = connectAndGetServerResponse(context, str, iResponseListener);
            if (connectAndGetServerResponse == null || !connectAndGetServerResponse.isValidResponse()) {
                IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "Null or invalid response. Trying to get cached response", 0);
                connectAndGetServerResponse = getCachedResponse(context, str);
            }
            if (connectAndGetServerResponse != null) {
                this.mCurrentServerResponse = connectAndGetServerResponse;
                IronSourceUtils.saveLastResponse(context, connectAndGetServerResponse.toString());
                initializeSettingsFromServerResponse(this.mCurrentServerResponse, context);
            }
            InterstitialEventsManager.getInstance().setHasServerResponse(true);
            RewardedVideoEventsManager.getInstance().setHasServerResponse(true);
            return connectAndGetServerResponse;
        }
    }

    private ServerResponseWrapper getCachedResponse(Context context, String str) {
        JSONObject jSONObject;
        try {
            jSONObject = new JSONObject(IronSourceUtils.getLastResponse(context));
        } catch (JSONException unused) {
            jSONObject = new JSONObject();
        }
        String optString = jSONObject.optString(ServerResponseWrapper.APP_KEY_FIELD);
        String optString2 = jSONObject.optString("userId");
        String optString3 = jSONObject.optString("response");
        if (TextUtils.isEmpty(optString) || TextUtils.isEmpty(optString2) || TextUtils.isEmpty(optString3) || getIronSourceAppKey() == null || !optString.equals(getIronSourceAppKey()) || !optString2.equals(str)) {
            return null;
        }
        ServerResponseWrapper serverResponseWrapper = new ServerResponseWrapper(context, optString, optString2, optString3);
        IronSourceError buildUsingCachedConfigurationError = ErrorBuilder.buildUsingCachedConfigurationError(optString, optString2);
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, buildUsingCachedConfigurationError.toString(), 1);
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        ironSourceLoggerManager.log(ironSourceTag, buildUsingCachedConfigurationError.toString() + ": " + serverResponseWrapper.toString(), 1);
        RewardedVideoEventsManager.getInstance().log(new EventData(IronSourceConstants.USING_CACHE_FOR_INIT_EVENT, IronSourceUtils.getMediationAdditionalData(false)));
        return serverResponseWrapper;
    }

    private ServerResponseWrapper connectAndGetServerResponse(Context context, String str, IResponseListener iResponseListener) {
        ServerResponseWrapper serverResponseWrapper;
        if (!IronSourceUtils.isNetworkConnected(context)) {
            return null;
        }
        try {
            String advertiserId = getAdvertiserId(context);
            if (TextUtils.isEmpty(advertiserId)) {
                advertiserId = DeviceStatus.getOrGenerateOnceUniqueIdentifier(context);
                IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "using custom identifier", 1);
            }
            String stringFromURL = HttpFunctions.getStringFromURL(ServerURL.getCPVProvidersURL(context, getIronSourceAppKey(), str, advertiserId, getMediationType(), this.mIronSegment != null ? this.mIronSegment.getSegmentData() : null), iResponseListener);
            if (stringFromURL == null) {
                return null;
            }
            if (IronSourceUtils.getSerr() == 1) {
                String optString = new JSONObject(stringFromURL).optString("response", null);
                if (TextUtils.isEmpty(optString)) {
                    return null;
                }
                stringFromURL = IronSourceAES.decode(IronSourceUtils.KEY, optString);
            }
            serverResponseWrapper = new ServerResponseWrapper(context, getIronSourceAppKey(), str, stringFromURL);
            try {
                if (!serverResponseWrapper.isValidResponse()) {
                    return null;
                }
                return serverResponseWrapper;
            } catch (Exception e) {
                e = e;
                e.printStackTrace();
                return serverResponseWrapper;
            }
        } catch (Exception e2) {
            e = e2;
            serverResponseWrapper = null;
            e.printStackTrace();
            return serverResponseWrapper;
        }
    }

    private void initializeSettingsFromServerResponse(ServerResponseWrapper serverResponseWrapper, Context context) {
        initializeLoggerManager(serverResponseWrapper);
        initializeEventsSettings(serverResponseWrapper, context);
    }

    private void initializeEventsSettings(ServerResponseWrapper serverResponseWrapper, Context context) {
        boolean isEventsEnabled = isRewardedVideoConfigurationsReady() ? serverResponseWrapper.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoEventsConfigurations().isEventsEnabled() : false;
        boolean isEventsEnabled2 = isInterstitialConfigurationsReady() ? serverResponseWrapper.getConfigurations().getInterstitialConfigurations().getInterstitialEventsConfigurations().isEventsEnabled() : false;
        boolean isEventsEnabled3 = isBannerConfigurationsReady() ? serverResponseWrapper.getConfigurations().getBannerConfigurations().getBannerEventsConfigurations().isEventsEnabled() : false;
        boolean isEventsEnabled4 = isOfferwallConfigurationsReady() ? serverResponseWrapper.getConfigurations().getOfferwallConfigurations().getOfferWallEventsConfigurations().isEventsEnabled() : false;
        if (isEventsEnabled) {
            RewardedVideoEventsManager.getInstance().setFormatterType(serverResponseWrapper.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoEventsConfigurations().getEventsType(), context);
            RewardedVideoEventsManager.getInstance().setEventsUrl(serverResponseWrapper.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoEventsConfigurations().getEventsURL(), context);
            RewardedVideoEventsManager.getInstance().setMaxNumberOfEvents(serverResponseWrapper.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoEventsConfigurations().getMaxNumberOfEvents());
            RewardedVideoEventsManager.getInstance().setMaxEventsPerBatch(serverResponseWrapper.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoEventsConfigurations().getMaxEventsPerBatch());
            RewardedVideoEventsManager.getInstance().setBackupThreshold(serverResponseWrapper.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoEventsConfigurations().getEventsBackupThreshold());
            RewardedVideoEventsManager.getInstance().setOptOutEvents(serverResponseWrapper.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoEventsConfigurations().getOptOutEvents(), context);
            RewardedVideoEventsManager.getInstance().setServerSegmentData(serverResponseWrapper.getConfigurations().getApplicationConfigurations().getSegmetData());
        } else if (isEventsEnabled4) {
            RewardedVideoEventsManager.getInstance().setFormatterType(serverResponseWrapper.getConfigurations().getOfferwallConfigurations().getOfferWallEventsConfigurations().getEventsType(), context);
            RewardedVideoEventsManager.getInstance().setEventsUrl(serverResponseWrapper.getConfigurations().getOfferwallConfigurations().getOfferWallEventsConfigurations().getEventsURL(), context);
            RewardedVideoEventsManager.getInstance().setMaxNumberOfEvents(serverResponseWrapper.getConfigurations().getOfferwallConfigurations().getOfferWallEventsConfigurations().getMaxNumberOfEvents());
            RewardedVideoEventsManager.getInstance().setMaxEventsPerBatch(serverResponseWrapper.getConfigurations().getOfferwallConfigurations().getOfferWallEventsConfigurations().getMaxEventsPerBatch());
            RewardedVideoEventsManager.getInstance().setBackupThreshold(serverResponseWrapper.getConfigurations().getOfferwallConfigurations().getOfferWallEventsConfigurations().getEventsBackupThreshold());
            RewardedVideoEventsManager.getInstance().setOptOutEvents(serverResponseWrapper.getConfigurations().getOfferwallConfigurations().getOfferWallEventsConfigurations().getOptOutEvents(), context);
            RewardedVideoEventsManager.getInstance().setServerSegmentData(serverResponseWrapper.getConfigurations().getApplicationConfigurations().getSegmetData());
        } else {
            RewardedVideoEventsManager.getInstance().setIsEventsEnabled(false);
        }
        if (isEventsEnabled2) {
            InterstitialEventsManager.getInstance().setFormatterType(serverResponseWrapper.getConfigurations().getInterstitialConfigurations().getInterstitialEventsConfigurations().getEventsType(), context);
            InterstitialEventsManager.getInstance().setEventsUrl(serverResponseWrapper.getConfigurations().getInterstitialConfigurations().getInterstitialEventsConfigurations().getEventsURL(), context);
            InterstitialEventsManager.getInstance().setMaxNumberOfEvents(serverResponseWrapper.getConfigurations().getInterstitialConfigurations().getInterstitialEventsConfigurations().getMaxNumberOfEvents());
            InterstitialEventsManager.getInstance().setMaxEventsPerBatch(serverResponseWrapper.getConfigurations().getInterstitialConfigurations().getInterstitialEventsConfigurations().getMaxEventsPerBatch());
            InterstitialEventsManager.getInstance().setBackupThreshold(serverResponseWrapper.getConfigurations().getInterstitialConfigurations().getInterstitialEventsConfigurations().getEventsBackupThreshold());
            InterstitialEventsManager.getInstance().setOptOutEvents(serverResponseWrapper.getConfigurations().getInterstitialConfigurations().getInterstitialEventsConfigurations().getOptOutEvents(), context);
            InterstitialEventsManager.getInstance().setServerSegmentData(serverResponseWrapper.getConfigurations().getApplicationConfigurations().getSegmetData());
        } else if (isEventsEnabled3) {
            ApplicationEvents bannerEventsConfigurations = serverResponseWrapper.getConfigurations().getBannerConfigurations().getBannerEventsConfigurations();
            InterstitialEventsManager.getInstance().setFormatterType(bannerEventsConfigurations.getEventsType(), context);
            InterstitialEventsManager.getInstance().setEventsUrl(bannerEventsConfigurations.getEventsURL(), context);
            InterstitialEventsManager.getInstance().setMaxNumberOfEvents(bannerEventsConfigurations.getMaxNumberOfEvents());
            InterstitialEventsManager.getInstance().setMaxEventsPerBatch(bannerEventsConfigurations.getMaxEventsPerBatch());
            InterstitialEventsManager.getInstance().setBackupThreshold(bannerEventsConfigurations.getEventsBackupThreshold());
            InterstitialEventsManager.getInstance().setOptOutEvents(bannerEventsConfigurations.getOptOutEvents(), context);
            InterstitialEventsManager.getInstance().setServerSegmentData(serverResponseWrapper.getConfigurations().getApplicationConfigurations().getSegmetData());
        } else {
            InterstitialEventsManager.getInstance().setIsEventsEnabled(false);
        }
    }

    private void initializeLoggerManager(ServerResponseWrapper serverResponseWrapper) {
        this.mPublisherLogger.setDebugLevel(serverResponseWrapper.getConfigurations().getApplicationConfigurations().getLoggerConfigurations().getPublisherLoggerLevel());
        this.mLoggerManager.setLoggerDebugLevel(ConsoleLogger.NAME, serverResponseWrapper.getConfigurations().getApplicationConfigurations().getLoggerConfigurations().getConsoleLoggerLevel());
    }

    public void removeRewardedVideoListener() {
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "removeRewardedVideoListener()", 1);
        this.mListenersWrapper.setRewardedVideoListener(null);
    }

    public void removeInterstitialListener() {
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "removeInterstitialListener()", 1);
        this.mListenersWrapper.setInterstitialListener(null);
    }

    public void removeOfferwallListener() {
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "removeOfferwallListener()", 1);
        this.mListenersWrapper.setOfferwallListener(null);
    }

    /* access modifiers changed from: package-private */
    public synchronized void setIronSourceUserId(String str) {
        this.mUserId = str;
    }

    public synchronized String getIronSourceAppKey() {
        return this.mAppKey;
    }

    public synchronized String getIronSourceUserId() {
        return this.mUserId;
    }

    private ConfigValidationResult validateAppKey(String str) {
        ConfigValidationResult configValidationResult = new ConfigValidationResult();
        if (str == null) {
            configValidationResult.setInvalid(new IronSourceError(IronSourceError.ERROR_CODE_INVALID_KEY_VALUE, "Init Fail - appKey is missing"));
        } else if (!validateLength(str, 5, 10)) {
            configValidationResult.setInvalid(ErrorBuilder.buildInvalidCredentialsError(ServerResponseWrapper.APP_KEY_FIELD, str, "length should be between 5-10 characters"));
        } else if (!validateAlphanumeric(str)) {
            configValidationResult.setInvalid(ErrorBuilder.buildInvalidCredentialsError(ServerResponseWrapper.APP_KEY_FIELD, str, "should contain only english characters and numbers"));
        }
        return configValidationResult;
    }

    private void validateGender(String str, ConfigValidationResult configValidationResult) {
        if (str != null) {
            try {
                String trim = str.toLowerCase().trim();
                if (!"male".equals(trim) && !"female".equals(trim) && !"unknown".equals(trim)) {
                    configValidationResult.setInvalid(ErrorBuilder.buildInvalidKeyValueError("gender", IronSourceConstants.SUPERSONIC_CONFIG_NAME, "gender value should be one of male/female/unknown."));
                }
            } catch (Exception unused) {
                configValidationResult.setInvalid(ErrorBuilder.buildInvalidKeyValueError("gender", IronSourceConstants.SUPERSONIC_CONFIG_NAME, "gender value should be one of male/female/unknown."));
            }
        }
    }

    private void validateAge(int i, ConfigValidationResult configValidationResult) {
        if (i < 5 || i > 120) {
            try {
                configValidationResult.setInvalid(ErrorBuilder.buildInvalidKeyValueError(IronSourceSegment.AGE, IronSourceConstants.SUPERSONIC_CONFIG_NAME, "age value should be between 5-120"));
            } catch (NumberFormatException unused) {
                configValidationResult.setInvalid(ErrorBuilder.buildInvalidKeyValueError(IronSourceSegment.AGE, IronSourceConstants.SUPERSONIC_CONFIG_NAME, "age value should be between 5-120"));
            }
        }
    }

    private void validateSegment(String str, ConfigValidationResult configValidationResult) {
        if (str != null) {
            try {
                if (str.length() > 64) {
                    configValidationResult.setInvalid(ErrorBuilder.buildInvalidKeyValueError("segment", IronSourceConstants.SUPERSONIC_CONFIG_NAME, "segment value should not exceed 64 characters."));
                }
            } catch (Exception unused) {
                configValidationResult.setInvalid(ErrorBuilder.buildInvalidKeyValueError("segment", IronSourceConstants.SUPERSONIC_CONFIG_NAME, "segment value should not exceed 64 characters."));
            }
        }
    }

    private void validateDynamicUserId(String str, ConfigValidationResult configValidationResult) {
        if (!validateLength(str, 1, 128)) {
            configValidationResult.setInvalid(ErrorBuilder.buildInvalidKeyValueError(IronSourceConstants.EVENTS_DYNAMIC_USER_ID, IronSourceConstants.SUPERSONIC_CONFIG_NAME, "dynamicUserId is invalid, should be between 1-128 chars in length."));
        }
    }

    private boolean validateLength(String str, int i, int i2) {
        return str != null && str.length() >= i && str.length() <= i2;
    }

    private boolean validateAlphanumeric(String str) {
        if (str == null) {
            return false;
        }
        return str.matches("^[a-zA-Z0-9]*$");
    }

    public InterstitialPlacement getInterstitialPlacementInfo(String str) {
        try {
            InterstitialPlacement interstitialPlacement = getInterstitialPlacement(str);
            try {
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
                ironSourceLoggerManager.log(ironSourceTag, "getPlacementInfo(placement: " + str + "):" + interstitialPlacement, 1);
                return interstitialPlacement;
            } catch (Exception unused) {
                return interstitialPlacement;
            }
        } catch (Exception unused2) {
            return null;
        }
    }

    public Placement getRewardedVideoPlacementInfo(String str) {
        try {
            Placement rewardedVideoPlacement = getRewardedVideoPlacement(str);
            try {
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
                ironSourceLoggerManager.log(ironSourceTag, "getPlacementInfo(placement: " + str + "):" + rewardedVideoPlacement, 1);
                return rewardedVideoPlacement;
            } catch (Exception unused) {
                return rewardedVideoPlacement;
            }
        } catch (Exception unused2) {
            return null;
        }
    }

    public String getAdvertiserId(Context context) {
        try {
            String[] advertisingIdInfo = DeviceStatus.getAdvertisingIdInfo(context);
            return (advertisingIdInfo.length <= 0 || advertisingIdInfo[0] == null) ? "" : advertisingIdInfo[0];
        } catch (Exception unused) {
            return "";
        }
    }

    public void shouldTrackNetworkState(Context context, boolean z) {
        if (this.mRewardedVideoManager != null) {
            this.mRewardedVideoManager.shouldTrackNetworkState(context, z);
        }
        if (this.mInterstitialManager != null) {
            this.mInterstitialManager.shouldTrackNetworkState(context, z);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isInterstitialPlacementCapped(String str) {
        boolean z = false;
        if (this.mIsDemandOnlyIs) {
            return false;
        }
        if (getInterstitialCappingStatus(str) != CappingManager.ECappingStatus.NOT_CAPPED) {
            z = true;
        }
        if (z) {
            JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(this.mIsDemandOnlyIs, this.mIsIsProgrammatic);
            try {
                mediationAdditionalData.put("placement", str);
                if (this.mIsIsProgrammatic) {
                    mediationAdditionalData.put(IronSourceConstants.EVENTS_PROGRAMMATIC, 1);
                }
            } catch (Exception unused) {
            }
            InterstitialEventsManager.getInstance().log(new EventData(IronSourceConstants.IS_CHECK_CAPPED_TRUE, mediationAdditionalData));
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public boolean isRewardedVideoPlacementCapped(String str) {
        boolean z;
        CappingManager.ECappingStatus rewardedVideoCappingStatus = getRewardedVideoCappingStatus(str);
        if (rewardedVideoCappingStatus != null) {
            switch (rewardedVideoCappingStatus) {
                case CAPPED_PER_DELIVERY:
                case CAPPED_PER_COUNT:
                case CAPPED_PER_PACE:
                    z = true;
                    break;
            }
            sendIsCappedEvent(z, str);
            return z;
        }
        z = false;
        sendIsCappedEvent(z, str);
        return z;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0052 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean isBannerPlacementCapped(java.lang.String r7) {
        /*
            r6 = this;
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r6.mCurrentServerResponse
            r1 = 0
            if (r0 == 0) goto L_0x005e
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r6.mCurrentServerResponse
            com.ironsource.mediationsdk.model.Configurations r0 = r0.getConfigurations()
            if (r0 == 0) goto L_0x005e
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r6.mCurrentServerResponse
            com.ironsource.mediationsdk.model.Configurations r0 = r0.getConfigurations()
            com.ironsource.mediationsdk.model.BannerConfigurations r0 = r0.getBannerConfigurations()
            if (r0 != 0) goto L_0x001a
            goto L_0x005e
        L_0x001a:
            r0 = 0
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r2 = r6.mCurrentServerResponse     // Catch:{ Exception -> 0x004b }
            com.ironsource.mediationsdk.model.Configurations r2 = r2.getConfigurations()     // Catch:{ Exception -> 0x004b }
            com.ironsource.mediationsdk.model.BannerConfigurations r2 = r2.getBannerConfigurations()     // Catch:{ Exception -> 0x004b }
            com.ironsource.mediationsdk.model.BannerPlacement r7 = r2.getBannerPlacement(r7)     // Catch:{ Exception -> 0x004b }
            if (r7 != 0) goto L_0x0050
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r6.mCurrentServerResponse     // Catch:{ Exception -> 0x0046 }
            com.ironsource.mediationsdk.model.Configurations r0 = r0.getConfigurations()     // Catch:{ Exception -> 0x0046 }
            com.ironsource.mediationsdk.model.BannerConfigurations r0 = r0.getBannerConfigurations()     // Catch:{ Exception -> 0x0046 }
            com.ironsource.mediationsdk.model.BannerPlacement r0 = r0.getDefaultBannerPlacement()     // Catch:{ Exception -> 0x0046 }
            if (r0 != 0) goto L_0x004f
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r7 = r6.mLoggerManager     // Catch:{ Exception -> 0x004b }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r2 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x004b }
            java.lang.String r3 = "Banner default placement was not found"
            r4 = 3
            r7.log(r2, r3, r4)     // Catch:{ Exception -> 0x004b }
            return r1
        L_0x0046:
            r0 = move-exception
            r5 = r0
            r0 = r7
            r7 = r5
            goto L_0x004c
        L_0x004b:
            r7 = move-exception
        L_0x004c:
            r7.printStackTrace()
        L_0x004f:
            r7 = r0
        L_0x0050:
            if (r7 != 0) goto L_0x0053
            return r1
        L_0x0053:
            android.app.Activity r0 = r6.mActivity
            java.lang.String r7 = r7.getPlacementName()
            boolean r7 = com.ironsource.mediationsdk.utils.CappingManager.isBnPlacementCapped(r0, r7)
            return r7
        L_0x005e:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.IronSourceObject.isBannerPlacementCapped(java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.ironsource.mediationsdk.utils.CappingManager.ECappingStatus getInterstitialCappingStatus(java.lang.String r6) {
        /*
            r5 = this;
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r5.mCurrentServerResponse
            if (r0 == 0) goto L_0x0047
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r5.mCurrentServerResponse
            com.ironsource.mediationsdk.model.Configurations r0 = r0.getConfigurations()
            if (r0 == 0) goto L_0x0047
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r5.mCurrentServerResponse
            com.ironsource.mediationsdk.model.Configurations r0 = r0.getConfigurations()
            com.ironsource.mediationsdk.model.InterstitialConfigurations r0 = r0.getInterstitialConfigurations()
            if (r0 != 0) goto L_0x0019
            goto L_0x0047
        L_0x0019:
            r0 = 0
            com.ironsource.mediationsdk.model.InterstitialPlacement r6 = r5.getInterstitialPlacement(r6)     // Catch:{ Exception -> 0x0036 }
            if (r6 != 0) goto L_0x003b
            com.ironsource.mediationsdk.model.InterstitialPlacement r0 = r5.getDefaultInterstitialPlacement()     // Catch:{ Exception -> 0x0031 }
            if (r0 != 0) goto L_0x003a
            java.lang.String r6 = "Default placement was not found"
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r1 = r5.mLoggerManager     // Catch:{ Exception -> 0x0036 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r2 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x0036 }
            r3 = 3
            r1.log(r2, r6, r3)     // Catch:{ Exception -> 0x0036 }
            goto L_0x003a
        L_0x0031:
            r0 = move-exception
            r4 = r0
            r0 = r6
            r6 = r4
            goto L_0x0037
        L_0x0036:
            r6 = move-exception
        L_0x0037:
            r6.printStackTrace()
        L_0x003a:
            r6 = r0
        L_0x003b:
            if (r6 != 0) goto L_0x0040
            com.ironsource.mediationsdk.utils.CappingManager$ECappingStatus r6 = com.ironsource.mediationsdk.utils.CappingManager.ECappingStatus.NOT_CAPPED
            return r6
        L_0x0040:
            android.app.Activity r0 = r5.mActivity
            com.ironsource.mediationsdk.utils.CappingManager$ECappingStatus r6 = com.ironsource.mediationsdk.utils.CappingManager.isPlacementCapped(r0, r6)
            return r6
        L_0x0047:
            com.ironsource.mediationsdk.utils.CappingManager$ECappingStatus r6 = com.ironsource.mediationsdk.utils.CappingManager.ECappingStatus.NOT_CAPPED
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.IronSourceObject.getInterstitialCappingStatus(java.lang.String):com.ironsource.mediationsdk.utils.CappingManager$ECappingStatus");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.ironsource.mediationsdk.utils.CappingManager.ECappingStatus getRewardedVideoCappingStatus(java.lang.String r6) {
        /*
            r5 = this;
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r5.mCurrentServerResponse
            if (r0 == 0) goto L_0x0047
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r5.mCurrentServerResponse
            com.ironsource.mediationsdk.model.Configurations r0 = r0.getConfigurations()
            if (r0 == 0) goto L_0x0047
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r5.mCurrentServerResponse
            com.ironsource.mediationsdk.model.Configurations r0 = r0.getConfigurations()
            com.ironsource.mediationsdk.model.RewardedVideoConfigurations r0 = r0.getRewardedVideoConfigurations()
            if (r0 != 0) goto L_0x0019
            goto L_0x0047
        L_0x0019:
            r0 = 0
            com.ironsource.mediationsdk.model.Placement r6 = r5.getRewardedVideoPlacement(r6)     // Catch:{ Exception -> 0x0036 }
            if (r6 != 0) goto L_0x003b
            com.ironsource.mediationsdk.model.Placement r0 = r5.getDefaultRewardedVideoPlacement()     // Catch:{ Exception -> 0x0031 }
            if (r0 != 0) goto L_0x003a
            java.lang.String r6 = "Default placement was not found"
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r1 = r5.mLoggerManager     // Catch:{ Exception -> 0x0036 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r2 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x0036 }
            r3 = 3
            r1.log(r2, r6, r3)     // Catch:{ Exception -> 0x0036 }
            goto L_0x003a
        L_0x0031:
            r0 = move-exception
            r4 = r0
            r0 = r6
            r6 = r4
            goto L_0x0037
        L_0x0036:
            r6 = move-exception
        L_0x0037:
            r6.printStackTrace()
        L_0x003a:
            r6 = r0
        L_0x003b:
            if (r6 != 0) goto L_0x0040
            com.ironsource.mediationsdk.utils.CappingManager$ECappingStatus r6 = com.ironsource.mediationsdk.utils.CappingManager.ECappingStatus.NOT_CAPPED
            return r6
        L_0x0040:
            android.app.Activity r0 = r5.mActivity
            com.ironsource.mediationsdk.utils.CappingManager$ECappingStatus r6 = com.ironsource.mediationsdk.utils.CappingManager.isPlacementCapped(r0, r6)
            return r6
        L_0x0047:
            com.ironsource.mediationsdk.utils.CappingManager$ECappingStatus r6 = com.ironsource.mediationsdk.utils.CappingManager.ECappingStatus.NOT_CAPPED
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.IronSourceObject.getRewardedVideoCappingStatus(java.lang.String):com.ironsource.mediationsdk.utils.CappingManager$ECappingStatus");
    }

    private void sentEventWithISEventManager(int i, JSONObject jSONObject) {
        InterstitialEventsManager.getInstance().log(new EventData(i, jSONObject));
    }

    private void sentEventWithRVEventManager(int i, JSONObject jSONObject) {
        RewardedVideoEventsManager.getInstance().log(new EventData(i, jSONObject));
    }

    private void sendIsCappedEvent(boolean z, String str) {
        if (z) {
            JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(this.mIsDemandOnlyRv, this.mIsRvProgrammatic);
            if (str != null) {
                addToDictionary(mediationAdditionalData, new Object[][]{new Object[]{"placement", str}});
            }
            sentEventWithRVEventManager(IronSourceConstants.RV_API_IS_CAPPED_TRUE, mediationAdditionalData);
        }
    }

    /* access modifiers changed from: package-private */
    public String getCappingMessage(String str, CappingManager.ECappingStatus eCappingStatus) {
        if (eCappingStatus == null) {
            return null;
        }
        switch (eCappingStatus) {
            case CAPPED_PER_DELIVERY:
            case CAPPED_PER_COUNT:
            case CAPPED_PER_PACE:
                return "placement " + str + " is capped";
            default:
                return null;
        }
    }

    /* access modifiers changed from: package-private */
    public ServerResponseWrapper getCurrentServerResponse() {
        return this.mCurrentServerResponse;
    }

    /* access modifiers changed from: package-private */
    public void setSegmentListener(SegmentListener segmentListener) {
        if (this.mListenersWrapper != null) {
            this.mListenersWrapper.setSegmentListener(segmentListener);
            MediationInitializer.getInstance().setSegmentListener(this.mListenersWrapper);
        }
    }

    /* access modifiers changed from: package-private */
    public HashSet<String> getAllSettingsForProvider(String str, String str2) {
        if (this.mCurrentServerResponse == null) {
            return new HashSet<>();
        }
        return this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettingsByReflectionName(str, str2);
    }

    private BannerPlacement getBannerPlacement(String str) {
        BannerConfigurations bannerConfigurations = this.mCurrentServerResponse.getConfigurations().getBannerConfigurations();
        if (bannerConfigurations == null) {
            return null;
        }
        if (TextUtils.isEmpty(str)) {
            return bannerConfigurations.getDefaultBannerPlacement();
        }
        BannerPlacement bannerPlacement = bannerConfigurations.getBannerPlacement(str);
        if (bannerPlacement != null) {
            return bannerPlacement;
        }
        return bannerConfigurations.getDefaultBannerPlacement();
    }

    public synchronized String getSessionId() {
        return this.mSessionId;
    }

    public void setConsent(boolean z) {
        this.mConsent = Boolean.valueOf(z);
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
        logger.log(ironSourceTag, "setConsent : " + z, 1);
        AdapterRepository.getInstance().setConsent(z);
        if (this.mOfferwallAdapter != null) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.ADAPTER_API;
            ironSourceLoggerManager.log(ironSourceTag2, "Offerwall | setConsent(consent:" + z + ")", 1);
            this.mOfferwallAdapter.setConsent(z);
        }
        if (this.mDemandOnlyIsManager != null) {
            this.mDemandOnlyIsManager.setConsent(z);
        }
        if (this.mDemandOnlyRvManager != null) {
            this.mDemandOnlyRvManager.setConsent(z);
        }
        int i = 40;
        if (!z) {
            i = 41;
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(i, IronSourceUtils.getMediationAdditionalData(false)));
    }

    /* access modifiers changed from: package-private */
    public Boolean getConsent() {
        return this.mConsent;
    }

    private void addToDictionary(JSONObject jSONObject, Object[][] objArr) {
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    jSONObject.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "IronSourceObject addToDictionary: " + Log.getStackTraceString(e), 3);
            }
        }
    }
}
