package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.DemandOnlySmash;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.DemandOnlyRvManagerListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import java.util.Date;
import java.util.TimerTask;

public class DemandOnlyRvSmash extends DemandOnlySmash implements RewardedVideoSmashListener {
    /* access modifiers changed from: private */
    public DemandOnlyRvManagerListener mListener;
    /* access modifiers changed from: private */
    public long mLoadStartTime;

    public void onRewardedVideoAdEnded() {
    }

    public void onRewardedVideoAdStarted() {
    }

    public void onRewardedVideoAvailabilityChanged(boolean z) {
    }

    public void onRewardedVideoInitFailed(IronSourceError ironSourceError) {
    }

    public void onRewardedVideoInitSuccess() {
    }

    DemandOnlyRvSmash(Activity activity, String str, String str2, ProviderSettings providerSettings, DemandOnlyRvManagerListener demandOnlyRvManagerListener, int i, AbstractAdapter abstractAdapter) {
        super(new AdapterConfig(providerSettings, providerSettings.getInterstitialSettings()), abstractAdapter);
        this.mAdapterConfig = new AdapterConfig(providerSettings, providerSettings.getRewardedVideoSettings());
        this.mAdUnitSettings = this.mAdapterConfig.getAdUnitSetings();
        this.mAdapter = abstractAdapter;
        this.mListener = demandOnlyRvManagerListener;
        this.mLoadTimeoutSecs = i;
        this.mAdapter.initRvForDemandOnly(activity, str, str2, this.mAdUnitSettings, this);
    }

    public void loadRewardedVideo() {
        logInternal("loadRewardedVideo state=" + getStateString());
        DemandOnlySmash.SMASH_STATE compareAndSetState = compareAndSetState(new DemandOnlySmash.SMASH_STATE[]{DemandOnlySmash.SMASH_STATE.NOT_LOADED, DemandOnlySmash.SMASH_STATE.LOADED}, DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS);
        if (compareAndSetState == DemandOnlySmash.SMASH_STATE.NOT_LOADED || compareAndSetState == DemandOnlySmash.SMASH_STATE.LOADED) {
            startLoadTimer();
            this.mLoadStartTime = new Date().getTime();
            this.mAdapter.loadVideoForDemandOnly(this.mAdUnitSettings, this);
        } else if (compareAndSetState == DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS) {
            this.mListener.onRewardedVideoAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_RV_LOAD_ALREADY_IN_PROGRESS, "load already in progress"), this, 0);
        } else {
            this.mListener.onRewardedVideoAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_RV_LOAD_ALREADY_IN_PROGRESS, "cannot load because show is in progress"), this, 0);
        }
    }

    public void showRewardedVideo() {
        logInternal("showRewardedVideo state=" + getStateString());
        if (compareAndSetState(DemandOnlySmash.SMASH_STATE.LOADED, DemandOnlySmash.SMASH_STATE.SHOW_IN_PROGRESS)) {
            this.mAdapter.showRewardedVideo(this.mAdUnitSettings, this);
            return;
        }
        this.mListener.onRewardedVideoAdShowFailed(new IronSourceError(IronSourceError.ERROR_DO_RV_CALL_LOAD_BEFORE_SHOW, "load must be called before show"), this);
    }

    public boolean isRewardedVideoAvailable() {
        return this.mAdapter.isRewardedVideoAvailable(this.mAdUnitSettings);
    }

    private void startLoadTimer() {
        logInternal("start timer");
        startTimer(new TimerTask() {
            public void run() {
                DemandOnlyRvSmash demandOnlyRvSmash = DemandOnlyRvSmash.this;
                demandOnlyRvSmash.logInternal("load timed out state=" + DemandOnlyRvSmash.this.getStateString());
                if (DemandOnlyRvSmash.this.compareAndSetState(DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS, DemandOnlySmash.SMASH_STATE.NOT_LOADED)) {
                    DemandOnlyRvSmash.this.mListener.onRewardedVideoAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_RV_LOAD_TIMED_OUT, "load timed out"), DemandOnlyRvSmash.this, new Date().getTime() - DemandOnlyRvSmash.this.mLoadStartTime);
                }
            }
        });
    }

    public void onRewardedVideoLoadSuccess() {
        logAdapterCallback("onRewardedVideoLoadSuccess state=" + getStateString());
        stopTimer();
        if (compareAndSetState(DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS, DemandOnlySmash.SMASH_STATE.LOADED)) {
            this.mListener.onRewardedVideoLoadSuccess(this, new Date().getTime() - this.mLoadStartTime);
        }
    }

    public void onRewardedVideoLoadFailed(IronSourceError ironSourceError) {
        logAdapterCallback("onRewardedVideoLoadFailed error=" + ironSourceError.getErrorMessage() + " state=" + getStateString());
        stopTimer();
        if (compareAndSetState(DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS, DemandOnlySmash.SMASH_STATE.NOT_LOADED)) {
            this.mListener.onRewardedVideoAdLoadFailed(ironSourceError, this, new Date().getTime() - this.mLoadStartTime);
        }
    }

    public void onRewardedVideoAdOpened() {
        logAdapterCallback("onRewardedVideoAdOpened");
        this.mListener.onRewardedVideoAdOpened(this);
    }

    public void onRewardedVideoAdClosed() {
        setState(DemandOnlySmash.SMASH_STATE.NOT_LOADED);
        logAdapterCallback("onRewardedVideoAdClosed");
        this.mListener.onRewardedVideoAdClosed(this);
    }

    public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError) {
        setState(DemandOnlySmash.SMASH_STATE.NOT_LOADED);
        logAdapterCallback("onRewardedVideoAdClosed error=" + ironSourceError);
        this.mListener.onRewardedVideoAdShowFailed(ironSourceError, this);
    }

    public void onRewardedVideoAdClicked() {
        logAdapterCallback("onRewardedVideoAdClicked");
        this.mListener.onRewardedVideoAdClicked(this);
    }

    public void onRewardedVideoAdVisible() {
        logAdapterCallback("onRewardedVideoAdVisible");
        this.mListener.onRewardedVideoAdVisible(this);
    }

    public void onRewardedVideoAdRewarded() {
        logAdapterCallback("onRewardedVideoAdRewarded");
        this.mListener.onRewardedVideoAdRewarded(this);
    }

    private void logAdapterCallback(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, "DemandOnlyRewardedVideoSmash " + this.mAdapterConfig.getProviderName() + " : " + str, 0);
    }

    /* access modifiers changed from: private */
    public void logInternal(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "DemandOnlyRewardedVideoSmash " + this.mAdapterConfig.getProviderName() + " : " + str, 0);
    }
}
