package com.ironsource.mediationsdk.server;

public class HttpFunctions {
    public static final String ERROR_PREFIX = "ERROR:";
    private static final String SERVER_BAD_REQUEST_ERROR = "Bad Request - 400";
    private static final String SERVER_REQUEST_ENCODING = "UTF-8";
    private static final String SERVER_REQUEST_GET_METHOD = "GET";
    private static final String SERVER_REQUEST_POST_METHOD = "POST";
    private static final int SERVER_REQUEST_TIMEOUT = 15000;

    public static String getStringFromURL(String str) throws Exception {
        return getStringFromURL(str, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x008d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getStringFromURL(java.lang.String r4, com.ironsource.mediationsdk.IronSourceObject.IResponseListener r5) throws java.lang.Exception {
        /*
            r0 = 0
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x0084, all -> 0x0077 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0084, all -> 0x0077 }
            java.net.URLConnection r4 = r1.openConnection()     // Catch:{ Exception -> 0x0084, all -> 0x0077 }
            java.net.HttpURLConnection r4 = (java.net.HttpURLConnection) r4     // Catch:{ Exception -> 0x0084, all -> 0x0077 }
            r1 = 15000(0x3a98, float:2.102E-41)
            r4.setReadTimeout(r1)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            r4.setConnectTimeout(r1)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            java.lang.String r1 = "GET"
            r4.setRequestMethod(r1)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            r1 = 1
            r4.setDoInput(r1)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            r4.connect()     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            int r1 = r4.getResponseCode()     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            r2 = 400(0x190, float:5.6E-43)
            if (r1 != r2) goto L_0x0035
            if (r5 == 0) goto L_0x002f
            java.lang.String r1 = "Bad Request - 400"
            r5.onUnrecoverableError(r1)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
        L_0x002f:
            if (r4 == 0) goto L_0x0034
            r4.disconnect()
        L_0x0034:
            return r0
        L_0x0035:
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            java.io.InputStream r2 = r4.getInputStream()     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            r5.<init>(r1)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0086, all -> 0x006e }
            r1.<init>()     // Catch:{ Exception -> 0x0086, all -> 0x006e }
        L_0x0048:
            java.lang.String r2 = r5.readLine()     // Catch:{ Exception -> 0x0086, all -> 0x006e }
            if (r2 == 0) goto L_0x0052
            r1.append(r2)     // Catch:{ Exception -> 0x0086, all -> 0x006e }
            goto L_0x0048
        L_0x0052:
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0086, all -> 0x006e }
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x0086, all -> 0x006e }
            if (r2 == 0) goto L_0x0065
            if (r4 == 0) goto L_0x0061
            r4.disconnect()
        L_0x0061:
            r5.close()
            return r0
        L_0x0065:
            if (r4 == 0) goto L_0x006a
            r4.disconnect()
        L_0x006a:
            r5.close()
            return r1
        L_0x006e:
            r0 = move-exception
            r3 = r0
            r0 = r5
            r5 = r3
            goto L_0x0079
        L_0x0073:
            r5 = move-exception
            goto L_0x0079
        L_0x0075:
            r5 = r0
            goto L_0x0086
        L_0x0077:
            r5 = move-exception
            r4 = r0
        L_0x0079:
            if (r4 == 0) goto L_0x007e
            r4.disconnect()
        L_0x007e:
            if (r0 == 0) goto L_0x0083
            r0.close()
        L_0x0083:
            throw r5
        L_0x0084:
            r4 = r0
            r5 = r4
        L_0x0086:
            if (r4 == 0) goto L_0x008b
            r4.disconnect()
        L_0x008b:
            if (r5 == 0) goto L_0x0090
            r5.close()
        L_0x0090:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.server.HttpFunctions.getStringFromURL(java.lang.String, com.ironsource.mediationsdk.IronSourceObject$IResponseListener):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0068 A[SYNTHETIC, Splitter:B:27:0x0068] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0079 A[SYNTHETIC, Splitter:B:38:0x0079] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0083  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean getStringFromPostWithAutho(java.lang.String r4, java.lang.String r5, java.lang.String r6, java.lang.String r7) {
        /*
            r0 = 0
            r1 = 0
            java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x0076, all -> 0x0064 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0076, all -> 0x0064 }
            java.lang.String r4 = com.ironsource.mediationsdk.utils.IronSourceUtils.getBase64Auth(r6, r7)     // Catch:{ Exception -> 0x0076, all -> 0x0064 }
            java.net.URLConnection r6 = r2.openConnection()     // Catch:{ Exception -> 0x0076, all -> 0x0064 }
            java.net.HttpURLConnection r6 = (java.net.HttpURLConnection) r6     // Catch:{ Exception -> 0x0076, all -> 0x0064 }
            r7 = 15000(0x3a98, float:2.102E-41)
            r6.setReadTimeout(r7)     // Catch:{ Exception -> 0x0077, all -> 0x0062 }
            r6.setConnectTimeout(r7)     // Catch:{ Exception -> 0x0077, all -> 0x0062 }
            java.lang.String r7 = "POST"
            r6.setRequestMethod(r7)     // Catch:{ Exception -> 0x0077, all -> 0x0062 }
            java.lang.String r7 = "Authorization"
            r6.setRequestProperty(r7, r4)     // Catch:{ Exception -> 0x0077, all -> 0x0062 }
            r4 = 1
            r6.setDoInput(r4)     // Catch:{ Exception -> 0x0077, all -> 0x0062 }
            r6.setDoOutput(r4)     // Catch:{ Exception -> 0x0077, all -> 0x0062 }
            java.io.OutputStream r7 = r6.getOutputStream()     // Catch:{ Exception -> 0x0077, all -> 0x0062 }
            java.io.BufferedWriter r1 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x0060, all -> 0x005d }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x0060, all -> 0x005d }
            java.lang.String r3 = "UTF-8"
            r2.<init>(r7, r3)     // Catch:{ Exception -> 0x0060, all -> 0x005d }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0060, all -> 0x005d }
            r1.write(r5)     // Catch:{ Exception -> 0x0060, all -> 0x005d }
            r1.flush()     // Catch:{ Exception -> 0x0060, all -> 0x005d }
            r1.close()     // Catch:{ Exception -> 0x0060, all -> 0x005d }
            int r5 = r6.getResponseCode()     // Catch:{ Exception -> 0x0060, all -> 0x005d }
            r1 = 200(0xc8, float:2.8E-43)
            if (r5 != r1) goto L_0x004c
            goto L_0x004d
        L_0x004c:
            r4 = 0
        L_0x004d:
            if (r7 == 0) goto L_0x0057
            r7.close()     // Catch:{ IOException -> 0x0053 }
            goto L_0x0057
        L_0x0053:
            r5 = move-exception
            r5.printStackTrace()
        L_0x0057:
            if (r6 == 0) goto L_0x005c
            r6.disconnect()
        L_0x005c:
            return r4
        L_0x005d:
            r4 = move-exception
            r1 = r7
            goto L_0x0066
        L_0x0060:
            r1 = r7
            goto L_0x0077
        L_0x0062:
            r4 = move-exception
            goto L_0x0066
        L_0x0064:
            r4 = move-exception
            r6 = r1
        L_0x0066:
            if (r1 == 0) goto L_0x0070
            r1.close()     // Catch:{ IOException -> 0x006c }
            goto L_0x0070
        L_0x006c:
            r5 = move-exception
            r5.printStackTrace()
        L_0x0070:
            if (r6 == 0) goto L_0x0075
            r6.disconnect()
        L_0x0075:
            throw r4
        L_0x0076:
            r6 = r1
        L_0x0077:
            if (r1 == 0) goto L_0x0081
            r1.close()     // Catch:{ IOException -> 0x007d }
            goto L_0x0081
        L_0x007d:
            r4 = move-exception
            r4.printStackTrace()
        L_0x0081:
            if (r6 == 0) goto L_0x0086
            r6.disconnect()
        L_0x0086:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.server.HttpFunctions.getStringFromPostWithAutho(java.lang.String, java.lang.String, java.lang.String, java.lang.String):boolean");
    }
}
