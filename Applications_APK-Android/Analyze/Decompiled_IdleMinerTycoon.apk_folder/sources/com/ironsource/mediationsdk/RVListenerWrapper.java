package com.ironsource.mediationsdk;

import android.os.Handler;
import android.os.Looper;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.sdk.RewardedVideoListener;

public class RVListenerWrapper {
    private static final RVListenerWrapper sInstance = new RVListenerWrapper();
    /* access modifiers changed from: private */
    public RewardedVideoListener mListener = null;

    public static synchronized RVListenerWrapper getInstance() {
        RVListenerWrapper rVListenerWrapper;
        synchronized (RVListenerWrapper.class) {
            rVListenerWrapper = sInstance;
        }
        return rVListenerWrapper;
    }

    private RVListenerWrapper() {
    }

    public synchronized void setListener(RewardedVideoListener rewardedVideoListener) {
        this.mListener = rewardedVideoListener;
    }

    public synchronized void onRewardedVideoAdOpened() {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.mListener.onRewardedVideoAdOpened();
                        RVListenerWrapper.this.log("onRewardedVideoAdOpened()");
                    }
                }
            });
        }
    }

    public synchronized void onRewardedVideoAdClosed() {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.mListener.onRewardedVideoAdClosed();
                        RVListenerWrapper.this.log("onRewardedVideoAdClosed()");
                    }
                }
            });
        }
    }

    public synchronized void onRewardedVideoAvailabilityChanged(final boolean z) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.mListener.onRewardedVideoAvailabilityChanged(z);
                        RVListenerWrapper rVListenerWrapper = RVListenerWrapper.this;
                        rVListenerWrapper.log("onRewardedVideoAvailabilityChanged() available=" + z);
                    }
                }
            });
        }
    }

    public synchronized void onRewardedVideoAdStarted() {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.mListener.onRewardedVideoAdStarted();
                        RVListenerWrapper.this.log("onRewardedVideoAdStarted()");
                    }
                }
            });
        }
    }

    public synchronized void onRewardedVideoAdEnded() {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.mListener.onRewardedVideoAdEnded();
                        RVListenerWrapper.this.log("onRewardedVideoAdEnded()");
                    }
                }
            });
        }
    }

    public synchronized void onRewardedVideoAdRewarded(final Placement placement) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.mListener.onRewardedVideoAdRewarded(placement);
                        RVListenerWrapper rVListenerWrapper = RVListenerWrapper.this;
                        rVListenerWrapper.log("onRewardedVideoAdRewarded() placement=" + placement.getPlacementName());
                    }
                }
            });
        }
    }

    public synchronized void onRewardedVideoAdShowFailed(final IronSourceError ironSourceError) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.mListener.onRewardedVideoAdShowFailed(ironSourceError);
                        RVListenerWrapper rVListenerWrapper = RVListenerWrapper.this;
                        rVListenerWrapper.log("onRewardedVideoAdShowFailed() error=" + ironSourceError.getErrorMessage());
                    }
                }
            });
        }
    }

    public synchronized void onRewardedVideoAdClicked(final Placement placement) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.mListener.onRewardedVideoAdClicked(placement);
                        RVListenerWrapper rVListenerWrapper = RVListenerWrapper.this;
                        rVListenerWrapper.log("onRewardedVideoAdClicked() placement=" + placement.getPlacementName());
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void log(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.CALLBACK, str, 1);
    }
}
