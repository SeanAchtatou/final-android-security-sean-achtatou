package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.helpshift.support.search.storage.TableSearchToken;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.AbstractSmash;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.MediationInitializer;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.InterstitialPlacement;
import com.ironsource.mediationsdk.sdk.InterstitialManagerListener;
import com.ironsource.mediationsdk.sdk.ListenersWrapper;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialApi;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialListener;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialManagerListener;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.DailyCappingListener;
import com.ironsource.mediationsdk.utils.DailyCappingManager;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import org.json.JSONObject;

class InterstitialManager extends AbstractAdUnitManager implements InterstitialManagerListener, MediationInitializer.OnMediationInitializationListener, RewardedInterstitialManagerListener, RewardedInterstitialApi, DailyCappingListener {
    private final String TAG = getClass().getName();
    private CallbackThrottler mCallbackThrottler = CallbackThrottler.getInstance();
    private InterstitialPlacement mCurrentPlacement;
    private boolean mDidCallLoadInterstitial = false;
    private boolean mDidFinishToInitInterstitial;
    private Map<String, InterstitialSmash> mInstanceIdToSmashMap = new ConcurrentHashMap();
    private CopyOnWriteArraySet<String> mInstancesToLoad = new CopyOnWriteArraySet<>();
    private ListenersWrapper mInterstitialListenersWrapper;
    private boolean mIsCurrentlyShowing;
    private boolean mIsLoadInterstitialInProgress = false;
    private long mLoadStartTime;
    private RewardedInterstitialListener mRewardedInterstitialListenerWrapper;
    private boolean mShouldSendAdReadyEvent = false;

    public void onInitSuccess(List<IronSource.AD_UNIT> list, boolean z) {
    }

    InterstitialManager() {
        this.mDailyCappingManager = new DailyCappingManager("interstitial", this);
        this.mIsCurrentlyShowing = false;
    }

    public void setInterstitialListener(ListenersWrapper listenersWrapper) {
        this.mInterstitialListenersWrapper = listenersWrapper;
        this.mCallbackThrottler.setInterstitialListener(listenersWrapper);
    }

    public void setRewardedInterstitialListener(RewardedInterstitialListener rewardedInterstitialListener) {
        this.mRewardedInterstitialListenerWrapper = rewardedInterstitialListener;
    }

    public synchronized void initInterstitial(Activity activity, String str, String str2) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
        ironSourceLoggerManager.log(ironSourceTag, this.TAG + ":initInterstitial(appKey: " + str + ", userId: " + str2 + ")", 1);
        long time = new Date().getTime();
        logMediationEvent(IronSourceConstants.IS_MANAGER_INIT_STARTED);
        this.mAppKey = str;
        this.mUserId = str2;
        this.mActivity = activity;
        this.mDailyCappingManager.setContext(this.mActivity);
        Iterator it = this.mSmashArray.iterator();
        int i = 0;
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (this.mDailyCappingManager.shouldSendCapReleasedEvent(abstractSmash)) {
                logProviderEvent(250, abstractSmash, new Object[][]{new Object[]{"status", "false"}});
            }
            if (this.mDailyCappingManager.isCapped(abstractSmash)) {
                abstractSmash.setMediationState(AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY);
                i++;
            }
        }
        if (i == this.mSmashArray.size()) {
            this.mDidFinishToInitInterstitial = true;
        }
        prepareSDK5();
        int i2 = 0;
        while (true) {
            if (i2 >= this.mSmartLoadAmount) {
                break;
            } else if (startNextAdapter() == null) {
                break;
            } else {
                i2++;
            }
        }
        logMediationEvent(IronSourceConstants.IS_MANAGER_INIT_ENDED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(new Date().getTime() - time)}});
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x008d, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01bb, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void loadInterstitial() {
        /*
            r10 = this;
            monitor-enter(r10)
            r0 = 2110(0x83e, float:2.957E-42)
            r1 = 2
            r2 = 3
            r3 = 0
            r4 = 1
            boolean r5 = r10.mIsCurrentlyShowing     // Catch:{ Exception -> 0x0162 }
            if (r5 == 0) goto L_0x0024
            java.lang.String r5 = "loadInterstitial cannot be invoked while showing an ad"
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r6 = r10.mLoggerManager     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r7 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x0162 }
            r6.log(r7, r5, r2)     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.logger.IronSourceError r6 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ Exception -> 0x0162 }
            r7 = 1037(0x40d, float:1.453E-42)
            r6.<init>(r7, r5)     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.ISListenerWrapper r5 = com.ironsource.mediationsdk.ISListenerWrapper.getInstance()     // Catch:{ Exception -> 0x0162 }
            r5.onInterstitialAdLoadFailed(r6)     // Catch:{ Exception -> 0x0162 }
            monitor-exit(r10)
            return
        L_0x0024:
            r5 = 0
            r10.mCurrentPlacement = r5     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.sdk.ListenersWrapper r6 = r10.mInterstitialListenersWrapper     // Catch:{ Exception -> 0x0162 }
            r6.setInterstitialPlacement(r5)     // Catch:{ Exception -> 0x0162 }
            boolean r6 = r10.mIsLoadInterstitialInProgress     // Catch:{ Exception -> 0x0162 }
            if (r6 != 0) goto L_0x0155
            com.ironsource.mediationsdk.CallbackThrottler r6 = r10.mCallbackThrottler     // Catch:{ Exception -> 0x0162 }
            boolean r6 = r6.hasPendingInvocation()     // Catch:{ Exception -> 0x0162 }
            if (r6 == 0) goto L_0x003a
            goto L_0x0155
        L_0x003a:
            com.ironsource.mediationsdk.MediationInitializer r6 = com.ironsource.mediationsdk.MediationInitializer.getInstance()     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r6 = r6.getCurrentInitStatus()     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r7 = com.ironsource.mediationsdk.MediationInitializer.EInitStatus.NOT_INIT     // Catch:{ Exception -> 0x0162 }
            if (r6 != r7) goto L_0x0051
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r10.mLoggerManager     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r6 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x0162 }
            java.lang.String r7 = "init() must be called before loadInterstitial()"
            r5.log(r6, r7, r2)     // Catch:{ Exception -> 0x0162 }
            monitor-exit(r10)
            return
        L_0x0051:
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r7 = com.ironsource.mediationsdk.MediationInitializer.EInitStatus.INIT_IN_PROGRESS     // Catch:{ Exception -> 0x0162 }
            r8 = 2001(0x7d1, float:2.804E-42)
            if (r6 != r7) goto L_0x008e
            com.ironsource.mediationsdk.MediationInitializer r6 = com.ironsource.mediationsdk.MediationInitializer.getInstance()     // Catch:{ Exception -> 0x0162 }
            boolean r6 = r6.isInProgressMoreThan15Secs()     // Catch:{ Exception -> 0x0162 }
            if (r6 == 0) goto L_0x0078
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r10.mLoggerManager     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r6 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x0162 }
            java.lang.String r7 = "init() had failed"
            r5.log(r6, r7, r2)     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.CallbackThrottler r5 = r10.mCallbackThrottler     // Catch:{ Exception -> 0x0162 }
            java.lang.String r6 = "init() had failed"
            java.lang.String r7 = "Interstitial"
            com.ironsource.mediationsdk.logger.IronSourceError r6 = com.ironsource.mediationsdk.utils.ErrorBuilder.buildInitFailedError(r6, r7)     // Catch:{ Exception -> 0x0162 }
            r5.onInterstitialAdLoadFailed(r6)     // Catch:{ Exception -> 0x0162 }
            goto L_0x008c
        L_0x0078:
            java.util.Date r6 = new java.util.Date     // Catch:{ Exception -> 0x0162 }
            r6.<init>()     // Catch:{ Exception -> 0x0162 }
            long r6 = r6.getTime()     // Catch:{ Exception -> 0x0162 }
            r10.mLoadStartTime = r6     // Catch:{ Exception -> 0x0162 }
            java.lang.Object[][] r5 = (java.lang.Object[][]) r5     // Catch:{ Exception -> 0x0162 }
            r10.logMediationEvent(r8, r5)     // Catch:{ Exception -> 0x0162 }
            r10.mDidCallLoadInterstitial = r4     // Catch:{ Exception -> 0x0162 }
            r10.mShouldSendAdReadyEvent = r4     // Catch:{ Exception -> 0x0162 }
        L_0x008c:
            monitor-exit(r10)
            return
        L_0x008e:
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r7 = com.ironsource.mediationsdk.MediationInitializer.EInitStatus.INIT_FAILED     // Catch:{ Exception -> 0x0162 }
            if (r6 != r7) goto L_0x00aa
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r10.mLoggerManager     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r6 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x0162 }
            java.lang.String r7 = "init() had failed"
            r5.log(r6, r7, r2)     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.CallbackThrottler r5 = r10.mCallbackThrottler     // Catch:{ Exception -> 0x0162 }
            java.lang.String r6 = "init() had failed"
            java.lang.String r7 = "Interstitial"
            com.ironsource.mediationsdk.logger.IronSourceError r6 = com.ironsource.mediationsdk.utils.ErrorBuilder.buildInitFailedError(r6, r7)     // Catch:{ Exception -> 0x0162 }
            r5.onInterstitialAdLoadFailed(r6)     // Catch:{ Exception -> 0x0162 }
            monitor-exit(r10)
            return
        L_0x00aa:
            java.util.concurrent.CopyOnWriteArrayList r6 = r10.mSmashArray     // Catch:{ Exception -> 0x0162 }
            int r6 = r6.size()     // Catch:{ Exception -> 0x0162 }
            if (r6 != 0) goto L_0x00ca
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r10.mLoggerManager     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r6 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x0162 }
            java.lang.String r7 = "the server response does not contain interstitial data"
            r5.log(r6, r7, r2)     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.CallbackThrottler r5 = r10.mCallbackThrottler     // Catch:{ Exception -> 0x0162 }
            java.lang.String r6 = "the server response does not contain interstitial data"
            java.lang.String r7 = "Interstitial"
            com.ironsource.mediationsdk.logger.IronSourceError r6 = com.ironsource.mediationsdk.utils.ErrorBuilder.buildInitFailedError(r6, r7)     // Catch:{ Exception -> 0x0162 }
            r5.onInterstitialAdLoadFailed(r6)     // Catch:{ Exception -> 0x0162 }
            monitor-exit(r10)
            return
        L_0x00ca:
            java.util.Date r6 = new java.util.Date     // Catch:{ Exception -> 0x0162 }
            r6.<init>()     // Catch:{ Exception -> 0x0162 }
            long r6 = r6.getTime()     // Catch:{ Exception -> 0x0162 }
            r10.mLoadStartTime = r6     // Catch:{ Exception -> 0x0162 }
            java.lang.Object[][] r5 = (java.lang.Object[][]) r5     // Catch:{ Exception -> 0x0162 }
            r10.logMediationEvent(r8, r5)     // Catch:{ Exception -> 0x0162 }
            r10.mShouldSendAdReadyEvent = r4     // Catch:{ Exception -> 0x0162 }
            r10.changeStateToInitiated()     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE[] r5 = new com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE[r4]     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r6 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INITIATED     // Catch:{ Exception -> 0x0162 }
            r5[r3] = r6     // Catch:{ Exception -> 0x0162 }
            int r5 = r10.smashesCount(r5)     // Catch:{ Exception -> 0x0162 }
            if (r5 != 0) goto L_0x0124
            boolean r5 = r10.mDidFinishToInitInterstitial     // Catch:{ Exception -> 0x0162 }
            if (r5 != 0) goto L_0x00f3
            r10.mDidCallLoadInterstitial = r4     // Catch:{ Exception -> 0x0162 }
            monitor-exit(r10)
            return
        L_0x00f3:
            java.lang.String r5 = "no ads to load"
            com.ironsource.mediationsdk.logger.IronSourceError r5 = com.ironsource.mediationsdk.utils.ErrorBuilder.buildGenericError(r5)     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r6 = r10.mLoggerManager     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r7 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x0162 }
            java.lang.String r8 = r5.getErrorMessage()     // Catch:{ Exception -> 0x0162 }
            r6.log(r7, r8, r4)     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.CallbackThrottler r6 = r10.mCallbackThrottler     // Catch:{ Exception -> 0x0162 }
            r6.onInterstitialAdLoadFailed(r5)     // Catch:{ Exception -> 0x0162 }
            java.lang.Object[][] r6 = new java.lang.Object[r4][]     // Catch:{ Exception -> 0x0162 }
            java.lang.Object[] r7 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0162 }
            java.lang.String r8 = "errorCode"
            r7[r3] = r8     // Catch:{ Exception -> 0x0162 }
            int r5 = r5.getErrorCode()     // Catch:{ Exception -> 0x0162 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x0162 }
            r7[r4] = r5     // Catch:{ Exception -> 0x0162 }
            r6[r3] = r7     // Catch:{ Exception -> 0x0162 }
            r10.logMediationEvent(r0, r6)     // Catch:{ Exception -> 0x0162 }
            r10.mShouldSendAdReadyEvent = r3     // Catch:{ Exception -> 0x0162 }
            monitor-exit(r10)
            return
        L_0x0124:
            r10.mDidCallLoadInterstitial = r4     // Catch:{ Exception -> 0x0162 }
            r10.mIsLoadInterstitialInProgress = r4     // Catch:{ Exception -> 0x0162 }
            java.util.concurrent.CopyOnWriteArrayList r5 = r10.mSmashArray     // Catch:{ Exception -> 0x0162 }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ Exception -> 0x0162 }
            r6 = 0
        L_0x012f:
            boolean r7 = r5.hasNext()     // Catch:{ Exception -> 0x0162 }
            if (r7 == 0) goto L_0x01ba
            java.lang.Object r7 = r5.next()     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.AbstractSmash r7 = (com.ironsource.mediationsdk.AbstractSmash) r7     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r8 = r7.getMediationState()     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r9 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INITIATED     // Catch:{ Exception -> 0x0162 }
            if (r8 != r9) goto L_0x012f
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r8 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.LOAD_PENDING     // Catch:{ Exception -> 0x0162 }
            r7.setMediationState(r8)     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.InterstitialSmash r7 = (com.ironsource.mediationsdk.InterstitialSmash) r7     // Catch:{ Exception -> 0x0162 }
            r10.loadAdapterAndSendEvent(r7)     // Catch:{ Exception -> 0x0162 }
            int r6 = r6 + 1
            int r7 = r10.mSmartLoadAmount     // Catch:{ Exception -> 0x0162 }
            if (r6 < r7) goto L_0x012f
            monitor-exit(r10)
            return
        L_0x0155:
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r10.mLoggerManager     // Catch:{ Exception -> 0x0162 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r6 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x0162 }
            java.lang.String r7 = "Load Interstitial is already in progress"
            r5.log(r6, r7, r2)     // Catch:{ Exception -> 0x0162 }
            monitor-exit(r10)
            return
        L_0x0160:
            r0 = move-exception
            goto L_0x01bc
        L_0x0162:
            r5 = move-exception
            r5.printStackTrace()     // Catch:{ all -> 0x0160 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0160 }
            r6.<init>()     // Catch:{ all -> 0x0160 }
            java.lang.String r7 = "loadInterstitial exception "
            r6.append(r7)     // Catch:{ all -> 0x0160 }
            java.lang.String r7 = r5.getMessage()     // Catch:{ all -> 0x0160 }
            r6.append(r7)     // Catch:{ all -> 0x0160 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0160 }
            com.ironsource.mediationsdk.logger.IronSourceError r6 = com.ironsource.mediationsdk.utils.ErrorBuilder.buildLoadFailedError(r6)     // Catch:{ all -> 0x0160 }
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r7 = r10.mLoggerManager     // Catch:{ all -> 0x0160 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r8 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x0160 }
            java.lang.String r9 = r6.getErrorMessage()     // Catch:{ all -> 0x0160 }
            r7.log(r8, r9, r2)     // Catch:{ all -> 0x0160 }
            com.ironsource.mediationsdk.CallbackThrottler r2 = r10.mCallbackThrottler     // Catch:{ all -> 0x0160 }
            r2.onInterstitialAdLoadFailed(r6)     // Catch:{ all -> 0x0160 }
            boolean r2 = r10.mShouldSendAdReadyEvent     // Catch:{ all -> 0x0160 }
            if (r2 == 0) goto L_0x01ba
            r10.mShouldSendAdReadyEvent = r3     // Catch:{ all -> 0x0160 }
            java.lang.Object[][] r2 = new java.lang.Object[r1][]     // Catch:{ all -> 0x0160 }
            java.lang.Object[] r7 = new java.lang.Object[r1]     // Catch:{ all -> 0x0160 }
            java.lang.String r8 = "errorCode"
            r7[r3] = r8     // Catch:{ all -> 0x0160 }
            int r6 = r6.getErrorCode()     // Catch:{ all -> 0x0160 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0160 }
            r7[r4] = r6     // Catch:{ all -> 0x0160 }
            r2[r3] = r7     // Catch:{ all -> 0x0160 }
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0160 }
            java.lang.String r6 = "reason"
            r1[r3] = r6     // Catch:{ all -> 0x0160 }
            java.lang.String r3 = r5.getMessage()     // Catch:{ all -> 0x0160 }
            r1[r4] = r3     // Catch:{ all -> 0x0160 }
            r2[r4] = r1     // Catch:{ all -> 0x0160 }
            r10.logMediationEvent(r0, r2)     // Catch:{ all -> 0x0160 }
        L_0x01ba:
            monitor-exit(r10)
            return
        L_0x01bc:
            monitor-exit(r10)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.InterstitialManager.loadInterstitial():void");
    }

    public void showInterstitial(String str) {
        if (this.mIsCurrentlyShowing) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "showInterstitial error: can't show ad while an ad is already showing", 3);
            this.mInterstitialListenersWrapper.onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_IS_SHOW_CALLED_DURING_SHOW, "showInterstitial error: can't show ad while an ad is already showing"));
        } else if (this.mShouldTrackNetworkState && this.mActivity != null && !IronSourceUtils.isNetworkConnected(this.mActivity)) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "showInterstitial error: can't show ad when there's no internet connection", 3);
            this.mInterstitialListenersWrapper.onInterstitialAdShowFailed(ErrorBuilder.buildNoInternetConnectionShowFailError("Interstitial"));
        } else if (!this.mDidCallLoadInterstitial) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.API, "showInterstitial failed - You need to load interstitial before showing it", 3);
            this.mInterstitialListenersWrapper.onInterstitialAdShowFailed(ErrorBuilder.buildShowFailedError("Interstitial", "showInterstitial failed - You need to load interstitial before showing it"));
        } else {
            for (int i = 0; i < this.mSmashArray.size(); i++) {
                AbstractSmash abstractSmash = (AbstractSmash) this.mSmashArray.get(i);
                if (abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.AVAILABLE) {
                    CappingManager.incrementShowCounter(this.mActivity, this.mCurrentPlacement);
                    if (CappingManager.isPlacementCapped(this.mActivity, this.mCurrentPlacement) != CappingManager.ECappingStatus.NOT_CAPPED) {
                        logMediationEventWithPlacement(IronSourceConstants.IS_CAP_PLACEMENT, null);
                    }
                    logProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_SHOW, abstractSmash, null);
                    this.mIsCurrentlyShowing = true;
                    ((InterstitialSmash) abstractSmash).showInterstitial();
                    if (abstractSmash.isCappedPerSession()) {
                        logProviderEvent(IronSourceConstants.IS_CAP_SESSION, abstractSmash);
                    }
                    this.mDailyCappingManager.increaseShowCounter(abstractSmash);
                    if (this.mDailyCappingManager.isCapped(abstractSmash)) {
                        abstractSmash.setMediationState(AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY);
                        logProviderEvent(250, abstractSmash, new Object[][]{new Object[]{"status", "true"}});
                    }
                    this.mDidCallLoadInterstitial = false;
                    if (!abstractSmash.isMediationAvailable()) {
                        startNextAdapter();
                        return;
                    }
                    return;
                }
            }
            this.mInterstitialListenersWrapper.onInterstitialAdShowFailed(ErrorBuilder.buildShowFailedError("Interstitial", "showInterstitial failed - No adapters ready to show"));
        }
    }

    public synchronized boolean isInterstitialReady() {
        if (this.mShouldTrackNetworkState && this.mActivity != null && !IronSourceUtils.isNetworkConnected(this.mActivity)) {
            return false;
        }
        Iterator it = this.mSmashArray.iterator();
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.AVAILABLE && ((InterstitialSmash) abstractSmash).isInterstitialReady()) {
                return true;
            }
        }
        return false;
    }

    public synchronized void onInterstitialInitSuccess(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, interstitialSmash.getInstanceName() + " :onInterstitialInitSuccess()", 1);
        logProviderEvent(IronSourceConstants.IS_INSTANCE_INIT_SUCCESS, interstitialSmash);
        this.mDidFinishToInitInterstitial = true;
        if (this.mDidCallLoadInterstitial) {
            if (smashesCount(AbstractSmash.MEDIATION_STATE.AVAILABLE, AbstractSmash.MEDIATION_STATE.LOAD_PENDING) < this.mSmartLoadAmount) {
                interstitialSmash.setMediationState(AbstractSmash.MEDIATION_STATE.LOAD_PENDING);
                loadAdapterAndSendEvent(interstitialSmash);
            }
        }
    }

    public synchronized void onInterstitialInitFailed(IronSourceError ironSourceError, InterstitialSmash interstitialSmash) {
        try {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceLoggerManager.log(ironSourceTag, interstitialSmash.getInstanceName() + ":onInterstitialInitFailed(" + ironSourceError + ")", 1);
            logProviderEvent(IronSourceConstants.IS_INSTANCE_INIT_FAILED, interstitialSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}});
            if (smashesCount(AbstractSmash.MEDIATION_STATE.INIT_FAILED) >= this.mSmashArray.size()) {
                IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
                IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.NATIVE;
                ironSourceLoggerManager2.log(ironSourceTag2, "Smart Loading - initialization failed - no adapters are initiated and no more left to init, error: " + ironSourceError.getErrorMessage(), 2);
                if (this.mDidCallLoadInterstitial) {
                    this.mCallbackThrottler.onInterstitialAdLoadFailed(ErrorBuilder.buildGenericError("no ads to show"));
                    logMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) IronSourceError.ERROR_CODE_GENERIC)}});
                    this.mShouldSendAdReadyEvent = false;
                }
                this.mDidFinishToInitInterstitial = true;
            } else {
                if (startNextAdapter() == null && this.mDidCallLoadInterstitial) {
                    if (smashesCount(AbstractSmash.MEDIATION_STATE.INIT_FAILED, AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE, AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION, AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY, AbstractSmash.MEDIATION_STATE.EXHAUSTED) >= this.mSmashArray.size()) {
                        this.mCallbackThrottler.onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW, "No ads to show"));
                        logMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW)}});
                        this.mShouldSendAdReadyEvent = false;
                    }
                }
                completeIterationRound();
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager3 = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag3 = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceLoggerManager3.logException(ironSourceTag3, "onInterstitialInitFailed(error:" + ironSourceError + TableSearchToken.COMMA_SEP + "provider:" + interstitialSmash.getName() + ")", e);
        }
        return;
    }

    public synchronized void onInterstitialAdReady(InterstitialSmash interstitialSmash, long j) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, interstitialSmash.getInstanceName() + ":onInterstitialAdReady()", 1);
        logProviderEvent(2003, interstitialSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(j)}});
        long time = new Date().getTime() - this.mLoadStartTime;
        interstitialSmash.setMediationState(AbstractSmash.MEDIATION_STATE.AVAILABLE);
        this.mIsLoadInterstitialInProgress = false;
        if (this.mShouldSendAdReadyEvent) {
            this.mShouldSendAdReadyEvent = false;
            this.mInterstitialListenersWrapper.onInterstitialAdReady();
            logMediationEvent(2004, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(time)}});
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ff, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void onInterstitialAdLoadFailed(com.ironsource.mediationsdk.logger.IronSourceError r8, com.ironsource.mediationsdk.InterstitialSmash r9, long r10) {
        /*
            r7 = this;
            monitor-enter(r7)
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r7.mLoggerManager     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK     // Catch:{ all -> 0x0100 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0100 }
            r2.<init>()     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = r9.getInstanceName()     // Catch:{ all -> 0x0100 }
            r2.append(r3)     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = ":onInterstitialAdLoadFailed("
            r2.append(r3)     // Catch:{ all -> 0x0100 }
            r2.append(r8)     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = ")"
            r2.append(r3)     // Catch:{ all -> 0x0100 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0100 }
            r3 = 1
            r0.log(r1, r2, r3)     // Catch:{ all -> 0x0100 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0100 }
            r0.<init>()     // Catch:{ all -> 0x0100 }
            java.lang.String r1 = r9.getInstanceName()     // Catch:{ all -> 0x0100 }
            r0.append(r1)     // Catch:{ all -> 0x0100 }
            java.lang.String r1 = ":onInterstitialAdLoadFailed("
            r0.append(r1)     // Catch:{ all -> 0x0100 }
            r0.append(r8)     // Catch:{ all -> 0x0100 }
            java.lang.String r1 = ")"
            r0.append(r1)     // Catch:{ all -> 0x0100 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.utils.IronSourceUtils.sendAutomationLog(r0)     // Catch:{ all -> 0x0100 }
            r0 = 2200(0x898, float:3.083E-42)
            r1 = 3
            java.lang.Object[][] r1 = new java.lang.Object[r1][]     // Catch:{ all -> 0x0100 }
            r2 = 2
            java.lang.Object[] r4 = new java.lang.Object[r2]     // Catch:{ all -> 0x0100 }
            java.lang.String r5 = "errorCode"
            r6 = 0
            r4[r6] = r5     // Catch:{ all -> 0x0100 }
            int r5 = r8.getErrorCode()     // Catch:{ all -> 0x0100 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0100 }
            r4[r3] = r5     // Catch:{ all -> 0x0100 }
            r1[r6] = r4     // Catch:{ all -> 0x0100 }
            java.lang.Object[] r4 = new java.lang.Object[r2]     // Catch:{ all -> 0x0100 }
            java.lang.String r5 = "reason"
            r4[r6] = r5     // Catch:{ all -> 0x0100 }
            java.lang.String r8 = r8.getErrorMessage()     // Catch:{ all -> 0x0100 }
            r4[r3] = r8     // Catch:{ all -> 0x0100 }
            r1[r3] = r4     // Catch:{ all -> 0x0100 }
            java.lang.Object[] r8 = new java.lang.Object[r2]     // Catch:{ all -> 0x0100 }
            java.lang.String r4 = "duration"
            r8[r6] = r4     // Catch:{ all -> 0x0100 }
            java.lang.Long r10 = java.lang.Long.valueOf(r10)     // Catch:{ all -> 0x0100 }
            r8[r3] = r10     // Catch:{ all -> 0x0100 }
            r1[r2] = r8     // Catch:{ all -> 0x0100 }
            r7.logProviderEvent(r0, r9, r1)     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r8 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE     // Catch:{ all -> 0x0100 }
            r9.setMediationState(r8)     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE[] r8 = new com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE[r2]     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r9 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.AVAILABLE     // Catch:{ all -> 0x0100 }
            r8[r6] = r9     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r9 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.LOAD_PENDING     // Catch:{ all -> 0x0100 }
            r8[r3] = r9     // Catch:{ all -> 0x0100 }
            int r8 = r7.smashesCount(r8)     // Catch:{ all -> 0x0100 }
            int r9 = r7.mSmartLoadAmount     // Catch:{ all -> 0x0100 }
            if (r8 < r9) goto L_0x0097
            monitor-exit(r7)
            return
        L_0x0097:
            java.util.concurrent.CopyOnWriteArrayList r9 = r7.mSmashArray     // Catch:{ all -> 0x0100 }
            java.util.Iterator r9 = r9.iterator()     // Catch:{ all -> 0x0100 }
        L_0x009d:
            boolean r10 = r9.hasNext()     // Catch:{ all -> 0x0100 }
            if (r10 == 0) goto L_0x00bd
            java.lang.Object r10 = r9.next()     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.AbstractSmash r10 = (com.ironsource.mediationsdk.AbstractSmash) r10     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r11 = r10.getMediationState()     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r0 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INITIATED     // Catch:{ all -> 0x0100 }
            if (r11 != r0) goto L_0x009d
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r8 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.LOAD_PENDING     // Catch:{ all -> 0x0100 }
            r10.setMediationState(r8)     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.InterstitialSmash r10 = (com.ironsource.mediationsdk.InterstitialSmash) r10     // Catch:{ all -> 0x0100 }
            r7.loadAdapterAndSendEvent(r10)     // Catch:{ all -> 0x0100 }
            monitor-exit(r7)
            return
        L_0x00bd:
            com.ironsource.mediationsdk.AbstractAdapter r9 = r7.startNextAdapter()     // Catch:{ all -> 0x0100 }
            if (r9 == 0) goto L_0x00c5
            monitor-exit(r7)
            return
        L_0x00c5:
            boolean r9 = r7.mDidCallLoadInterstitial     // Catch:{ all -> 0x0100 }
            if (r9 == 0) goto L_0x00fe
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE[] r9 = new com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE[r3]     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r10 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INIT_PENDING     // Catch:{ all -> 0x0100 }
            r9[r6] = r10     // Catch:{ all -> 0x0100 }
            int r9 = r7.smashesCount(r9)     // Catch:{ all -> 0x0100 }
            int r8 = r8 + r9
            if (r8 != 0) goto L_0x00fe
            r7.completeIterationRound()     // Catch:{ all -> 0x0100 }
            r7.mIsLoadInterstitialInProgress = r6     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.CallbackThrottler r8 = r7.mCallbackThrottler     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.logger.IronSourceError r9 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ all -> 0x0100 }
            java.lang.String r10 = "No ads to show"
            r11 = 509(0x1fd, float:7.13E-43)
            r9.<init>(r11, r10)     // Catch:{ all -> 0x0100 }
            r8.onInterstitialAdLoadFailed(r9)     // Catch:{ all -> 0x0100 }
            r8 = 2110(0x83e, float:2.957E-42)
            java.lang.Object[][] r9 = new java.lang.Object[r3][]     // Catch:{ all -> 0x0100 }
            java.lang.Object[] r10 = new java.lang.Object[r2]     // Catch:{ all -> 0x0100 }
            java.lang.String r0 = "errorCode"
            r10[r6] = r0     // Catch:{ all -> 0x0100 }
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x0100 }
            r10[r3] = r11     // Catch:{ all -> 0x0100 }
            r9[r6] = r10     // Catch:{ all -> 0x0100 }
            r7.logMediationEvent(r8, r9)     // Catch:{ all -> 0x0100 }
        L_0x00fe:
            monitor-exit(r7)
            return
        L_0x0100:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.InterstitialManager.onInterstitialAdLoadFailed(com.ironsource.mediationsdk.logger.IronSourceError, com.ironsource.mediationsdk.InterstitialSmash, long):void");
    }

    public void onInterstitialAdOpened(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, interstitialSmash.getInstanceName() + ":onInterstitialAdOpened()", 1);
        logProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_OPENED, interstitialSmash, null);
        this.mInterstitialListenersWrapper.onInterstitialAdOpened();
    }

    public void onInterstitialAdClosed(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, interstitialSmash.getInstanceName() + ":onInterstitialAdClosed()", 1);
        this.mIsCurrentlyShowing = false;
        verifyOnPauseOnResume();
        logProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_CLOSED, interstitialSmash, null);
        this.mInterstitialListenersWrapper.onInterstitialAdClosed();
    }

    public void onInterstitialAdShowSucceeded(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, interstitialSmash.getInstanceName() + ":onInterstitialAdShowSucceeded()", 1);
        logProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_SHOW_SUCCESS, interstitialSmash, null);
        Iterator it = this.mSmashArray.iterator();
        boolean z = false;
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.AVAILABLE) {
                completeAdapterShow(abstractSmash);
                z = true;
            }
        }
        if (!z && (interstitialSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION || interstitialSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.EXHAUSTED || interstitialSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY)) {
            completeIterationRound();
        }
        changeStateToInitiated();
        this.mInterstitialListenersWrapper.onInterstitialAdShowSucceeded();
    }

    public void onInterstitialAdShowFailed(IronSourceError ironSourceError, InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, interstitialSmash.getInstanceName() + ":onInterstitialAdShowFailed(" + ironSourceError + ")", 1);
        logProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_SHOW_FAILED, interstitialSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}});
        this.mIsCurrentlyShowing = false;
        completeAdapterShow(interstitialSmash);
        Iterator it = this.mSmashArray.iterator();
        while (it.hasNext()) {
            if (((AbstractSmash) it.next()).getMediationState() == AbstractSmash.MEDIATION_STATE.AVAILABLE) {
                this.mDidCallLoadInterstitial = true;
                showInterstitial(this.mCurrentPlacement.getPlacementName());
                return;
            }
        }
        this.mInterstitialListenersWrapper.onInterstitialAdShowFailed(ironSourceError);
    }

    public void onInterstitialAdClicked(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, interstitialSmash.getInstanceName() + ":onInterstitialAdClicked()", 1);
        logProviderEventWithPlacement(2006, interstitialSmash, null);
        this.mInterstitialListenersWrapper.onInterstitialAdClicked();
    }

    public void onInterstitialAdVisible(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, interstitialSmash.getInstanceName() + ":onInterstitialAdVisible()", 1);
    }

    public void onInterstitialAdRewarded(InterstitialSmash interstitialSmash) {
        logProviderEvent(IronSourceConstants.INTERSTITIAL_AD_REWARDED, interstitialSmash, null);
        if (this.mRewardedInterstitialListenerWrapper != null) {
            this.mRewardedInterstitialListenerWrapper.onInterstitialAdRewarded();
        }
    }

    /* access modifiers changed from: package-private */
    public void shouldTrackNetworkState(Context context, boolean z) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        ironSourceLoggerManager.log(ironSourceTag, this.TAG + " Should Track Network State: " + z, 0);
        this.mShouldTrackNetworkState = z;
    }

    public void onInitFailed(String str) {
        if (this.mDidCallLoadInterstitial) {
            this.mCallbackThrottler.onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial"));
            this.mDidCallLoadInterstitial = false;
            this.mIsLoadInterstitialInProgress = false;
        }
    }

    public void onStillInProgressAfter15Secs() {
        if (this.mDidCallLoadInterstitial) {
            IronSourceError buildInitFailedError = ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial");
            this.mCallbackThrottler.onInterstitialAdLoadFailed(buildInitFailedError);
            this.mDidCallLoadInterstitial = false;
            this.mIsLoadInterstitialInProgress = false;
            if (this.mShouldSendAdReadyEvent) {
                logMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(buildInitFailedError.getErrorCode())}});
                this.mShouldSendAdReadyEvent = false;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean isIterationRoundComplete() {
        /*
            r4 = this;
            java.util.concurrent.CopyOnWriteArrayList r0 = r4.mSmashArray
            java.util.Iterator r0 = r0.iterator()
        L_0x0006:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x003c
            java.lang.Object r1 = r0.next()
            com.ironsource.mediationsdk.AbstractSmash r1 = (com.ironsource.mediationsdk.AbstractSmash) r1
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.getMediationState()
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.NOT_INITIATED
            if (r2 == r3) goto L_0x003a
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.getMediationState()
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INIT_PENDING
            if (r2 == r3) goto L_0x003a
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.getMediationState()
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INITIATED
            if (r2 == r3) goto L_0x003a
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.getMediationState()
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.LOAD_PENDING
            if (r2 == r3) goto L_0x003a
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r1 = r1.getMediationState()
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.AVAILABLE
            if (r1 != r2) goto L_0x0006
        L_0x003a:
            r0 = 0
            return r0
        L_0x003c:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.InterstitialManager.isIterationRoundComplete():boolean");
    }

    private void completeIterationRound() {
        if (isIterationRoundComplete()) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "Reset Iteration", 0);
            Iterator it = this.mSmashArray.iterator();
            while (it.hasNext()) {
                AbstractSmash abstractSmash = (AbstractSmash) it.next();
                if (abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.EXHAUSTED) {
                    abstractSmash.completeIteration();
                }
            }
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "End of Reset Iteration", 0);
        }
    }

    private void completeAdapterShow(AbstractSmash abstractSmash) {
        if (!abstractSmash.isMediationAvailable()) {
            startNextAdapter();
            completeIterationRound();
            return;
        }
        abstractSmash.setMediationState(AbstractSmash.MEDIATION_STATE.INITIATED);
    }

    private AbstractAdapter startNextAdapter() {
        AbstractAdapter abstractAdapter = null;
        int i = 0;
        for (int i2 = 0; i2 < this.mSmashArray.size() && abstractAdapter == null; i2++) {
            if (((AbstractSmash) this.mSmashArray.get(i2)).getMediationState() == AbstractSmash.MEDIATION_STATE.AVAILABLE || ((AbstractSmash) this.mSmashArray.get(i2)).getMediationState() == AbstractSmash.MEDIATION_STATE.INITIATED || ((AbstractSmash) this.mSmashArray.get(i2)).getMediationState() == AbstractSmash.MEDIATION_STATE.INIT_PENDING || ((AbstractSmash) this.mSmashArray.get(i2)).getMediationState() == AbstractSmash.MEDIATION_STATE.LOAD_PENDING) {
                i++;
                if (i >= this.mSmartLoadAmount) {
                    break;
                }
            } else if (((AbstractSmash) this.mSmashArray.get(i2)).getMediationState() == AbstractSmash.MEDIATION_STATE.NOT_INITIATED && (abstractAdapter = startAdapter((InterstitialSmash) this.mSmashArray.get(i2))) == null) {
                ((AbstractSmash) this.mSmashArray.get(i2)).setMediationState(AbstractSmash.MEDIATION_STATE.INIT_FAILED);
            }
        }
        return abstractAdapter;
    }

    private synchronized AbstractAdapter startAdapter(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
        ironSourceLoggerManager.log(ironSourceTag, this.TAG + ":startAdapter(" + interstitialSmash.getName() + ")", 1);
        AbstractAdapter adapter = AdapterRepository.getInstance().getAdapter(interstitialSmash.mAdapterConfigs, interstitialSmash.mAdapterConfigs.getInterstitialSettings(), this.mActivity);
        if (adapter == null) {
            IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager2.log(ironSourceTag2, interstitialSmash.getInstanceName() + " is configured in IronSource's platform, but the adapter is not integrated", 2);
            return null;
        }
        interstitialSmash.setAdapterForSmash(adapter);
        interstitialSmash.setMediationState(AbstractSmash.MEDIATION_STATE.INIT_PENDING);
        if (this.mRewardedInterstitialListenerWrapper != null) {
            interstitialSmash.setRewardedInterstitialManagerListener(this);
        }
        setCustomParams(interstitialSmash);
        try {
            interstitialSmash.initInterstitial(this.mActivity, this.mAppKey, this.mUserId);
            return adapter;
        } catch (Throwable th) {
            IronSourceLoggerManager ironSourceLoggerManager3 = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag3 = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager3.logException(ironSourceTag3, this.TAG + "failed to init adapter: " + interstitialSmash.getName() + "v", th);
            interstitialSmash.setMediationState(AbstractSmash.MEDIATION_STATE.INIT_FAILED);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void setCurrentPlacement(InterstitialPlacement interstitialPlacement) {
        this.mCurrentPlacement = interstitialPlacement;
        this.mInterstitialListenersWrapper.setInterstitialPlacement(interstitialPlacement);
    }

    private synchronized void loadAdapterAndSendEvent(InterstitialSmash interstitialSmash) {
        logProviderEvent(2002, interstitialSmash, null);
        interstitialSmash.loadInterstitial();
    }

    private synchronized void changeStateToInitiatedForInstanceId(String str) {
        AbstractSmash abstractSmash;
        Iterator it = this.mSmashArray.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            abstractSmash = (AbstractSmash) it.next();
            if (!abstractSmash.getSubProviderId().equals(str) || !(abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.AVAILABLE || abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.LOAD_PENDING || abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE)) {
            }
        }
        abstractSmash.setMediationState(AbstractSmash.MEDIATION_STATE.INITIATED);
    }

    private synchronized void changeStateToInitiated() {
        Iterator it = this.mSmashArray.iterator();
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.AVAILABLE || abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.LOAD_PENDING || abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE) {
                abstractSmash.setMediationState(AbstractSmash.MEDIATION_STATE.INITIATED);
            }
        }
    }

    private void logMediationEvent(int i) {
        logMediationEvent(i, null);
    }

    private void logMediationEvent(int i, Object[][] objArr) {
        logMediationEvent(i, objArr, false);
    }

    private void logMediationEventWithPlacement(int i, Object[][] objArr) {
        logMediationEvent(i, objArr, true);
    }

    private void logMediationEvent(int i, Object[][] objArr, boolean z) {
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
        if (z) {
            try {
                if (this.mCurrentPlacement != null && !TextUtils.isEmpty(this.mCurrentPlacement.getPlacementName())) {
                    mediationAdditionalData.put("placement", this.mCurrentPlacement.getPlacementName());
                }
            } catch (Exception e) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "InterstitialManager logMediationEvent " + Log.getStackTraceString(e), 3);
            }
        }
        if (objArr != null) {
            for (Object[] objArr2 : objArr) {
                mediationAdditionalData.put(objArr2[0].toString(), objArr2[1]);
            }
        }
        InterstitialEventsManager.getInstance().log(new EventData(i, mediationAdditionalData));
    }

    private void logProviderEvent(int i, AbstractSmash abstractSmash) {
        logProviderEvent(i, abstractSmash, null);
    }

    private void logProviderEvent(int i, AbstractSmash abstractSmash, Object[][] objArr) {
        logProviderEvent(i, abstractSmash, objArr, false);
    }

    private void logProviderEventWithPlacement(int i, AbstractSmash abstractSmash, Object[][] objArr) {
        logProviderEvent(i, abstractSmash, objArr, true);
    }

    private void logProviderEvent(int i, AbstractSmash abstractSmash, Object[][] objArr, boolean z) {
        JSONObject providerAdditionalData = IronSourceUtils.getProviderAdditionalData(abstractSmash);
        if (z) {
            try {
                if (this.mCurrentPlacement != null && !TextUtils.isEmpty(this.mCurrentPlacement.getPlacementName())) {
                    providerAdditionalData.put("placement", this.mCurrentPlacement.getPlacementName());
                }
            } catch (Exception e) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "InterstitialManager logProviderEvent " + Log.getStackTraceString(e), 3);
            }
        }
        if (objArr != null) {
            for (Object[] objArr2 : objArr) {
                providerAdditionalData.put(objArr2[0].toString(), objArr2[1]);
            }
        }
        InterstitialEventsManager.getInstance().log(new EventData(i, providerAdditionalData));
    }

    private int smashesCount(AbstractSmash.MEDIATION_STATE... mediation_stateArr) {
        int i;
        synchronized (this.mSmashArray) {
            Iterator it = this.mSmashArray.iterator();
            i = 0;
            while (it.hasNext()) {
                AbstractSmash abstractSmash = (AbstractSmash) it.next();
                int i2 = i;
                for (AbstractSmash.MEDIATION_STATE mediation_state : mediation_stateArr) {
                    if (abstractSmash.getMediationState() == mediation_state) {
                        i2++;
                    }
                }
                i = i2;
            }
        }
        return i;
    }

    public void onDailyCapReleased() {
        if (this.mSmashArray != null) {
            Iterator it = this.mSmashArray.iterator();
            while (it.hasNext()) {
                AbstractSmash abstractSmash = (AbstractSmash) it.next();
                if (abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY) {
                    logProviderEvent(250, abstractSmash, new Object[][]{new Object[]{"status", "false"}});
                    if (abstractSmash.isCappedPerSession()) {
                        abstractSmash.setMediationState(AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION);
                    } else if (abstractSmash.isExhausted()) {
                        abstractSmash.setMediationState(AbstractSmash.MEDIATION_STATE.EXHAUSTED);
                    } else {
                        abstractSmash.setMediationState(AbstractSmash.MEDIATION_STATE.INITIATED);
                    }
                }
            }
        }
    }

    public void setDelayLoadFailureNotificationInSeconds(int i) {
        this.mCallbackThrottler.setDelayLoadFailureNotificationInSeconds(i);
    }

    private void prepareSDK5() {
        for (int i = 0; i < this.mSmashArray.size(); i++) {
            String providerTypeForReflection = ((AbstractSmash) this.mSmashArray.get(i)).mAdapterConfigs.getProviderTypeForReflection();
            if (providerTypeForReflection.equalsIgnoreCase(IronSourceConstants.IRONSOURCE_CONFIG_NAME) || providerTypeForReflection.equalsIgnoreCase(IronSourceConstants.SUPERSONIC_CONFIG_NAME)) {
                AdapterRepository.getInstance().getAdapter(((AbstractSmash) this.mSmashArray.get(i)).mAdapterConfigs, ((AbstractSmash) this.mSmashArray.get(i)).mAdapterConfigs.getInterstitialSettings(), this.mActivity);
                return;
            }
        }
    }
}
