package com.ironsource.adapters.supersonicads;

import android.text.TextUtils;
import com.helpshift.support.constants.FaqsColumns;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.model.ProviderSettingsHolder;
import com.ironsource.sdk.constants.Constants;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SupersonicConfig {
    private static SupersonicConfig mInstance;
    private final String APPLICATION_PRIVATE_KEY = "privateKey";
    private final String CAMPAIGN_ID = Constants.RequestParameters.CAMPAIGN_ID;
    private final String CLIENT_SIDE_CALLBACKS = Constants.ParametersKeys.USE_CLIENT_SIDE_CALLBACKS;
    private final String CUSTOM_PARAM_PREFIX = "custom_";
    private final String DYNAMIC_CONTROLLER_DEBUG_MODE = "debugMode";
    private final String DYNAMIC_CONTROLLER_URL = "controllerUrl";
    private final String ITEM_COUNT = "itemCount";
    private final String ITEM_NAME = "itemName";
    private final String LANGUAGE = FaqsColumns.LANGUAGE;
    private final String MAX_VIDEO_LENGTH = "maxVideoLength";
    private Map<String, String> mOfferwallCustomParams;
    ProviderSettings mProviderSettings = new ProviderSettings(ProviderSettingsHolder.getProviderSettingsHolder().getProviderSettings("Mediation"));
    private Map<String, String> mRewardedVideoCustomParams;

    public static SupersonicConfig getConfigObj() {
        if (mInstance == null) {
            mInstance = new SupersonicConfig();
        }
        return mInstance;
    }

    private SupersonicConfig() {
    }

    public void setClientSideCallbacks(boolean z) {
        this.mProviderSettings.setRewardedVideoSettings(Constants.ParametersKeys.USE_CLIENT_SIDE_CALLBACKS, String.valueOf(z));
    }

    public void setCustomControllerUrl(String str) {
        this.mProviderSettings.setRewardedVideoSettings("controllerUrl", str);
        this.mProviderSettings.setInterstitialSettings("controllerUrl", str);
        this.mProviderSettings.setBannerSettings("controllerUrl", str);
    }

    public void setDebugMode(int i) {
        this.mProviderSettings.setRewardedVideoSettings("debugMode", Integer.valueOf(i));
        this.mProviderSettings.setInterstitialSettings("debugMode", Integer.valueOf(i));
        this.mProviderSettings.setBannerSettings("debugMode", Integer.valueOf(i));
    }

    public void setCampaignId(String str) {
        this.mProviderSettings.setRewardedVideoSettings(Constants.RequestParameters.CAMPAIGN_ID, str);
    }

    public void setLanguage(String str) {
        this.mProviderSettings.setRewardedVideoSettings(FaqsColumns.LANGUAGE, str);
        this.mProviderSettings.setInterstitialSettings(FaqsColumns.LANGUAGE, str);
    }

    public void setRewardedVideoCustomParams(Map<String, String> map) {
        this.mRewardedVideoCustomParams = convertCustomParams(map);
    }

    public void setOfferwallCustomParams(Map<String, String> map) {
        this.mOfferwallCustomParams = convertCustomParams(map);
    }

    private Map<String, String> convertCustomParams(Map<String, String> map) {
        HashMap hashMap = new HashMap();
        if (map != null) {
            try {
                Set<String> keySet = map.keySet();
                if (keySet != null) {
                    for (String next : keySet) {
                        if (!TextUtils.isEmpty(next)) {
                            String str = map.get(next);
                            if (!TextUtils.isEmpty(str)) {
                                hashMap.put("custom_" + next, str);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                IronSourceLoggerManager.getLogger().logException(IronSourceLogger.IronSourceTag.NATIVE, ":convertCustomParams()", e);
            }
        }
        return hashMap;
    }

    public boolean getClientSideCallbacks() {
        if (this.mProviderSettings == null || this.mProviderSettings.getRewardedVideoSettings() == null || !this.mProviderSettings.getRewardedVideoSettings().has(Constants.ParametersKeys.USE_CLIENT_SIDE_CALLBACKS)) {
            return false;
        }
        return this.mProviderSettings.getRewardedVideoSettings().optBoolean(Constants.ParametersKeys.USE_CLIENT_SIDE_CALLBACKS, false);
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> getOfferwallCustomParams() {
        return this.mOfferwallCustomParams;
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> getRewardedVideoCustomParams() {
        return this.mRewardedVideoCustomParams;
    }
}
