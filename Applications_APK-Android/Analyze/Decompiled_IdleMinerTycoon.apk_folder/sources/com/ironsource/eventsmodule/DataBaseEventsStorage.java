package com.ironsource.eventsmodule;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.SystemClock;
import android.provider.BaseColumns;

public class DataBaseEventsStorage extends SQLiteOpenHelper implements IEventsStorageHelper {
    private static final String COMMA_SEP = ",";
    private static final String TYPE_INTEGER = " INTEGER";
    private static final String TYPE_TEXT = " TEXT";
    private static DataBaseEventsStorage mInstance;
    private final int DB_OPEN_BACKOFF_TIME = 400;
    private final int DB_RETRY_NUM = 4;
    private final String SQL_CREATE_ENTRIES = "CREATE TABLE events (_id INTEGER PRIMARY KEY,eventid INTEGER,timestamp INTEGER,type TEXT,data TEXT )";
    private final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS events";

    public DataBaseEventsStorage(Context context, String str, int i) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, i);
    }

    public static synchronized DataBaseEventsStorage getInstance(Context context, String str, int i) {
        DataBaseEventsStorage dataBaseEventsStorage;
        synchronized (DataBaseEventsStorage.class) {
            if (mInstance == null) {
                mInstance = new DataBaseEventsStorage(context, str, i);
            }
            dataBaseEventsStorage = mInstance;
        }
        return dataBaseEventsStorage;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0067, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x004c A[SYNTHETIC, Splitter:B:31:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0059 A[SYNTHETIC, Splitter:B:38:0x0059] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void saveEvents(java.util.List<com.ironsource.eventsmodule.EventData> r5, java.lang.String r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            if (r5 == 0) goto L_0x0066
            boolean r0 = r5.isEmpty()     // Catch:{ all -> 0x0063 }
            if (r0 == 0) goto L_0x000a
            goto L_0x0066
        L_0x000a:
            r0 = 1
            r1 = 0
            android.database.sqlite.SQLiteDatabase r0 = r4.getDataBaseWithRetries(r0)     // Catch:{ Throwable -> 0x0042 }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ Throwable -> 0x003c, all -> 0x003a }
        L_0x0014:
            boolean r2 = r5.hasNext()     // Catch:{ Throwable -> 0x003c, all -> 0x003a }
            if (r2 == 0) goto L_0x002e
            java.lang.Object r2 = r5.next()     // Catch:{ Throwable -> 0x003c, all -> 0x003a }
            com.ironsource.eventsmodule.EventData r2 = (com.ironsource.eventsmodule.EventData) r2     // Catch:{ Throwable -> 0x003c, all -> 0x003a }
            android.content.ContentValues r2 = r4.getContentValuesForEvent(r2, r6)     // Catch:{ Throwable -> 0x003c, all -> 0x003a }
            if (r0 == 0) goto L_0x0014
            if (r2 == 0) goto L_0x0014
            java.lang.String r3 = "events"
            r0.insert(r3, r1, r2)     // Catch:{ Throwable -> 0x003c, all -> 0x003a }
            goto L_0x0014
        L_0x002e:
            if (r0 == 0) goto L_0x0055
            boolean r5 = r0.isOpen()     // Catch:{ all -> 0x0063 }
            if (r5 == 0) goto L_0x0055
            r0.close()     // Catch:{ all -> 0x0063 }
            goto L_0x0055
        L_0x003a:
            r5 = move-exception
            goto L_0x0057
        L_0x003c:
            r5 = move-exception
            r1 = r0
            goto L_0x0043
        L_0x003f:
            r5 = move-exception
            r0 = r1
            goto L_0x0057
        L_0x0042:
            r5 = move-exception
        L_0x0043:
            java.lang.String r6 = "IronSource"
            java.lang.String r0 = "Exception while saving events: "
            android.util.Log.e(r6, r0, r5)     // Catch:{ all -> 0x003f }
            if (r1 == 0) goto L_0x0055
            boolean r5 = r1.isOpen()     // Catch:{ all -> 0x0063 }
            if (r5 == 0) goto L_0x0055
            r1.close()     // Catch:{ all -> 0x0063 }
        L_0x0055:
            monitor-exit(r4)
            return
        L_0x0057:
            if (r0 == 0) goto L_0x0062
            boolean r6 = r0.isOpen()     // Catch:{ all -> 0x0063 }
            if (r6 == 0) goto L_0x0062
            r0.close()     // Catch:{ all -> 0x0063 }
        L_0x0062:
            throw r5     // Catch:{ all -> 0x0063 }
        L_0x0063:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        L_0x0066:
            monitor-exit(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.eventsmodule.DataBaseEventsStorage.saveEvents(java.util.List, java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0071, code lost:
        if (r11.isOpen() != false) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0073, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x009f, code lost:
        if (r11.isOpen() != false) goto L_0x0073;
     */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0090 A[SYNTHETIC, Splitter:B:40:0x0090] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00a6 A[SYNTHETIC, Splitter:B:50:0x00a6] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.ArrayList<com.ironsource.eventsmodule.EventData> loadEvents(java.lang.String r13) {
        /*
            r12 = this;
            monitor-enter(r12)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x00bb }
            r0.<init>()     // Catch:{ all -> 0x00bb }
            r1 = 0
            r2 = 0
            android.database.sqlite.SQLiteDatabase r11 = r12.getDataBaseWithRetries(r1)     // Catch:{ Throwable -> 0x0085, all -> 0x0081 }
            java.lang.String r6 = "type = ?"
            r3 = 1
            java.lang.String[] r7 = new java.lang.String[r3]     // Catch:{ Throwable -> 0x007f }
            r7[r1] = r13     // Catch:{ Throwable -> 0x007f }
            java.lang.String r10 = "timestamp ASC"
            java.lang.String r4 = "events"
            r5 = 0
            r8 = 0
            r9 = 0
            r3 = r11
            android.database.Cursor r13 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Throwable -> 0x007f }
            int r1 = r13.getCount()     // Catch:{ Throwable -> 0x0079, all -> 0x0077 }
            if (r1 <= 0) goto L_0x0060
            r13.moveToFirst()     // Catch:{ Throwable -> 0x0079, all -> 0x0077 }
        L_0x0028:
            boolean r1 = r13.isAfterLast()     // Catch:{ Throwable -> 0x0079, all -> 0x0077 }
            if (r1 != 0) goto L_0x005d
            java.lang.String r1 = "eventid"
            int r1 = r13.getColumnIndex(r1)     // Catch:{ Throwable -> 0x0079, all -> 0x0077 }
            int r1 = r13.getInt(r1)     // Catch:{ Throwable -> 0x0079, all -> 0x0077 }
            java.lang.String r2 = "timestamp"
            int r2 = r13.getColumnIndex(r2)     // Catch:{ Throwable -> 0x0079, all -> 0x0077 }
            long r2 = r13.getLong(r2)     // Catch:{ Throwable -> 0x0079, all -> 0x0077 }
            java.lang.String r4 = "data"
            int r4 = r13.getColumnIndex(r4)     // Catch:{ Throwable -> 0x0079, all -> 0x0077 }
            java.lang.String r4 = r13.getString(r4)     // Catch:{ Throwable -> 0x0079, all -> 0x0077 }
            com.ironsource.eventsmodule.EventData r5 = new com.ironsource.eventsmodule.EventData     // Catch:{ Throwable -> 0x0079, all -> 0x0077 }
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0079, all -> 0x0077 }
            r6.<init>(r4)     // Catch:{ Throwable -> 0x0079, all -> 0x0077 }
            r5.<init>(r1, r2, r6)     // Catch:{ Throwable -> 0x0079, all -> 0x0077 }
            r0.add(r5)     // Catch:{ Throwable -> 0x0079, all -> 0x0077 }
            r13.moveToNext()     // Catch:{ Throwable -> 0x0079, all -> 0x0077 }
            goto L_0x0028
        L_0x005d:
            r13.close()     // Catch:{ Throwable -> 0x0079, all -> 0x0077 }
        L_0x0060:
            if (r13 == 0) goto L_0x006b
            boolean r1 = r13.isClosed()     // Catch:{ all -> 0x00bb }
            if (r1 != 0) goto L_0x006b
            r13.close()     // Catch:{ all -> 0x00bb }
        L_0x006b:
            if (r11 == 0) goto L_0x00a2
            boolean r13 = r11.isOpen()     // Catch:{ all -> 0x00bb }
            if (r13 == 0) goto L_0x00a2
        L_0x0073:
            r11.close()     // Catch:{ all -> 0x00bb }
            goto L_0x00a2
        L_0x0077:
            r0 = move-exception
            goto L_0x00a4
        L_0x0079:
            r1 = move-exception
            r2 = r13
            goto L_0x0087
        L_0x007c:
            r0 = move-exception
            r13 = r2
            goto L_0x00a4
        L_0x007f:
            r1 = move-exception
            goto L_0x0087
        L_0x0081:
            r0 = move-exception
            r13 = r2
            r11 = r13
            goto L_0x00a4
        L_0x0085:
            r1 = move-exception
            r11 = r2
        L_0x0087:
            java.lang.String r13 = "IronSource"
            java.lang.String r3 = "Exception while loading events: "
            android.util.Log.e(r13, r3, r1)     // Catch:{ all -> 0x007c }
            if (r2 == 0) goto L_0x0099
            boolean r13 = r2.isClosed()     // Catch:{ all -> 0x00bb }
            if (r13 != 0) goto L_0x0099
            r2.close()     // Catch:{ all -> 0x00bb }
        L_0x0099:
            if (r11 == 0) goto L_0x00a2
            boolean r13 = r11.isOpen()     // Catch:{ all -> 0x00bb }
            if (r13 == 0) goto L_0x00a2
            goto L_0x0073
        L_0x00a2:
            monitor-exit(r12)
            return r0
        L_0x00a4:
            if (r13 == 0) goto L_0x00af
            boolean r1 = r13.isClosed()     // Catch:{ all -> 0x00bb }
            if (r1 != 0) goto L_0x00af
            r13.close()     // Catch:{ all -> 0x00bb }
        L_0x00af:
            if (r11 == 0) goto L_0x00ba
            boolean r13 = r11.isOpen()     // Catch:{ all -> 0x00bb }
            if (r13 == 0) goto L_0x00ba
            r11.close()     // Catch:{ all -> 0x00bb }
        L_0x00ba:
            throw r0     // Catch:{ all -> 0x00bb }
        L_0x00bb:
            r13 = move-exception
            monitor-exit(r12)
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.eventsmodule.DataBaseEventsStorage.loadEvents(java.lang.String):java.util.ArrayList");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0019, code lost:
        if (r7.isOpen() != false) goto L_0x001b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001b, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0039, code lost:
        if (r7.isOpen() != false) goto L_0x001b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0035 A[SYNTHETIC, Splitter:B:24:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0040 A[SYNTHETIC, Splitter:B:30:0x0040] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void clearEvents(java.lang.String r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            r0 = 0
            java.lang.String r1 = "type = ?"
            r2 = 1
            java.lang.String[] r3 = new java.lang.String[r2]     // Catch:{ all -> 0x004a }
            r4 = 0
            r3[r4] = r7     // Catch:{ all -> 0x004a }
            android.database.sqlite.SQLiteDatabase r7 = r6.getDataBaseWithRetries(r2)     // Catch:{ Throwable -> 0x0028, all -> 0x0023 }
            java.lang.String r0 = "events"
            r7.delete(r0, r1, r3)     // Catch:{ Throwable -> 0x0021 }
            if (r7 == 0) goto L_0x003c
            boolean r0 = r7.isOpen()     // Catch:{ all -> 0x004a }
            if (r0 == 0) goto L_0x003c
        L_0x001b:
            r7.close()     // Catch:{ all -> 0x004a }
            goto L_0x003c
        L_0x001f:
            r0 = move-exception
            goto L_0x003e
        L_0x0021:
            r0 = move-exception
            goto L_0x002c
        L_0x0023:
            r7 = move-exception
            r5 = r0
            r0 = r7
            r7 = r5
            goto L_0x003e
        L_0x0028:
            r7 = move-exception
            r5 = r0
            r0 = r7
            r7 = r5
        L_0x002c:
            java.lang.String r1 = "IronSource"
            java.lang.String r2 = "Exception while clearing events: "
            android.util.Log.e(r1, r2, r0)     // Catch:{ all -> 0x001f }
            if (r7 == 0) goto L_0x003c
            boolean r0 = r7.isOpen()     // Catch:{ all -> 0x004a }
            if (r0 == 0) goto L_0x003c
            goto L_0x001b
        L_0x003c:
            monitor-exit(r6)
            return
        L_0x003e:
            if (r7 == 0) goto L_0x0049
            boolean r1 = r7.isOpen()     // Catch:{ all -> 0x004a }
            if (r1 == 0) goto L_0x0049
            r7.close()     // Catch:{ all -> 0x004a }
        L_0x0049:
            throw r0     // Catch:{ all -> 0x004a }
        L_0x004a:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.eventsmodule.DataBaseEventsStorage.clearEvents(java.lang.String):void");
    }

    private ContentValues getContentValuesForEvent(EventData eventData, String str) {
        if (eventData == null) {
            return null;
        }
        ContentValues contentValues = new ContentValues(4);
        contentValues.put(EventEntry.COLUMN_NAME_EVENT_ID, Integer.valueOf(eventData.getEventId()));
        contentValues.put("timestamp", Long.valueOf(eventData.getTimeStamp()));
        contentValues.put("type", str);
        contentValues.put("data", eventData.getAdditionalData());
        return contentValues;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE events (_id INTEGER PRIMARY KEY,eventid INTEGER,timestamp INTEGER,type TEXT,data TEXT )");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS events");
        onCreate(sQLiteDatabase);
    }

    private synchronized SQLiteDatabase getDataBaseWithRetries(boolean z) throws Throwable {
        int i = 0;
        while (true) {
            if (z) {
                try {
                    return getWritableDatabase();
                } catch (Throwable th) {
                    i++;
                    if (i < 4) {
                        SystemClock.sleep((long) (i * 400));
                    } else {
                        throw th;
                    }
                }
            } else {
                return getReadableDatabase();
            }
        }
    }

    static abstract class EventEntry implements BaseColumns {
        public static final String COLUMN_NAME_DATA = "data";
        public static final String COLUMN_NAME_EVENT_ID = "eventid";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final int NUMBER_OF_COLUMNS = 4;
        public static final String TABLE_NAME = "events";

        EventEntry() {
        }
    }
}
