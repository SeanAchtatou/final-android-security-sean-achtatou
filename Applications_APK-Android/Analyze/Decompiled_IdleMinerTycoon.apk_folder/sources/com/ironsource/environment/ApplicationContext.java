package com.ironsource.environment;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ApplicationContext {
    public static String getPackageName(Context context) {
        return context.getPackageName();
    }

    public static int getAppOrientation(Activity activity) {
        return activity.getRequestedOrientation();
    }

    public static String getDiskCacheDirPath(Context context) {
        File cacheDir = context.getCacheDir();
        if (cacheDir != null) {
            return cacheDir.getPath();
        }
        return null;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [boolean] */
    /* JADX WARN: Type inference failed for: r1v2, types: [int] */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r1v6 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean isValidPermission(android.content.Context r3, java.lang.String r4) {
        /*
            boolean r0 = android.text.TextUtils.isEmpty(r4)
            r1 = 0
            if (r0 != 0) goto L_0x0036
            android.content.pm.PackageManager r0 = r3.getPackageManager()     // Catch:{ Exception -> 0x0032 }
            java.lang.String r3 = r3.getPackageName()     // Catch:{ Exception -> 0x0032 }
            r2 = 4096(0x1000, float:5.74E-42)
            android.content.pm.PackageInfo r3 = r0.getPackageInfo(r3, r2)     // Catch:{ Exception -> 0x0032 }
            java.lang.String[] r0 = r3.requestedPermissions     // Catch:{ Exception -> 0x0032 }
            if (r0 == 0) goto L_0x0036
            r0 = 0
        L_0x001a:
            java.lang.String[] r2 = r3.requestedPermissions     // Catch:{ Exception -> 0x002f }
            int r2 = r2.length     // Catch:{ Exception -> 0x002f }
            if (r1 >= r2) goto L_0x002d
            if (r0 != 0) goto L_0x002d
            java.lang.String[] r2 = r3.requestedPermissions     // Catch:{ Exception -> 0x002f }
            r2 = r2[r1]     // Catch:{ Exception -> 0x002f }
            boolean r2 = r4.equals(r2)     // Catch:{ Exception -> 0x002f }
            int r1 = r1 + 1
            r0 = r2
            goto L_0x001a
        L_0x002d:
            r1 = r0
            goto L_0x0036
        L_0x002f:
            r3 = move-exception
            r1 = r0
            goto L_0x0033
        L_0x0032:
            r3 = move-exception
        L_0x0033:
            r3.printStackTrace()
        L_0x0036:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.environment.ApplicationContext.isValidPermission(android.content.Context, java.lang.String):boolean");
    }

    public static boolean isPermissionGranted(Context context, String str) {
        return context.checkCallingOrSelfPermission(str) == 0;
    }

    public static JSONObject getPermissions(Context context, JSONArray jSONArray) {
        JSONObject jSONObject = new JSONObject();
        if (Build.VERSION.SDK_INT >= 16) {
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096);
                int i = 0;
                if (jSONArray.length() == 0) {
                    while (i < packageInfo.requestedPermissions.length) {
                        jSONObject.put(packageInfo.requestedPermissions[i], (packageInfo.requestedPermissionsFlags[i] & 2) != 0 ? "Granted" : "Rejected");
                        i++;
                    }
                } else {
                    List asList = Arrays.asList(packageInfo.requestedPermissions);
                    while (i < jSONArray.length()) {
                        String string = jSONArray.getString(i);
                        int indexOf = asList.indexOf(string);
                        if (indexOf != -1) {
                            jSONObject.put(string, (packageInfo.requestedPermissionsFlags[indexOf] & 2) != 0 ? "Granted" : "Rejected");
                        } else {
                            jSONObject.put(string, "notFoundInManifest");
                        }
                        i++;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jSONObject;
    }

    static PackageInfo getAppPackageInfo(Context context) throws PackageManager.NameNotFoundException {
        return context.getPackageManager().getPackageInfo(getPackageName(context), 0);
    }

    public static long getFirstInstallTime(Context context) {
        try {
            return getAppPackageInfo(context).firstInstallTime;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static long getLastUpdateTime(Context context) {
        try {
            return getAppPackageInfo(context).lastUpdateTime;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static String getApplicationVersionName(Context context) {
        try {
            return getAppPackageInfo(context).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getPublisherApplicationVersion(Context context, String str) {
        try {
            return context.getPackageManager().getPackageInfo(str, 0).versionName;
        } catch (Exception unused) {
            return "";
        }
    }

    public static String getInstallerPackageName(Context context) {
        String str;
        try {
            str = context.getPackageManager().getInstallerPackageName(context.getPackageName());
        } catch (Throwable unused) {
            str = null;
        }
        return TextUtils.isEmpty(str) ? "" : str;
    }
}
