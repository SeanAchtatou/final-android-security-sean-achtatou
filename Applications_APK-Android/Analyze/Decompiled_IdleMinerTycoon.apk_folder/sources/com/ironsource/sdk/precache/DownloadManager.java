package com.ironsource.sdk.precache;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.ironsource.sdk.data.SSAFile;
import com.ironsource.sdk.utils.IronSourceSharedPrefHelper;
import com.ironsource.sdk.utils.IronSourceStorageUtils;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Callable;

public class DownloadManager {
    public static final String CAMPAIGNS = "campaigns";
    public static final String FILE_ALREADY_EXIST = "file_already_exist";
    protected static final String FILE_NOT_FOUND_EXCEPTION = "file not found exception";
    public static final String GLOBAL_ASSETS = "globalAssets";
    protected static final String HTTP_EMPTY_RESPONSE = "http empty response";
    protected static final String HTTP_ERROR_CODE = "http error code";
    protected static final String HTTP_NOT_FOUND = "http not found";
    protected static final String HTTP_OK = "http ok";
    protected static final String IO_EXCEPTION = "io exception";
    protected static final String MALFORMED_URL_EXCEPTION = "malformed url exception";
    static final int MESSAGE_EMPTY_URL = 1007;
    static final int MESSAGE_FILE_DOWNLOAD_FAIL = 1017;
    static final int MESSAGE_FILE_DOWNLOAD_SUCCESS = 1016;
    static final int MESSAGE_FILE_NOT_FOUND_EXCEPTION = 1018;
    static final int MESSAGE_GENERAL_HTTP_ERROR_CODE = 1011;
    static final int MESSAGE_HTTP_EMPTY_RESPONSE = 1006;
    static final int MESSAGE_HTTP_NOT_FOUND = 1005;
    static final int MESSAGE_INIT_BC_FAIL = 1014;
    static final int MESSAGE_IO_EXCEPTION = 1009;
    static final int MESSAGE_MALFORMED_URL_EXCEPTION = 1004;
    static final int MESSAGE_NUM_OF_BANNERS_TO_CACHE = 1013;
    static final int MESSAGE_NUM_OF_BANNERS_TO_INIT_SUCCESS = 1012;
    static final int MESSAGE_OUT_OF_MEMORY_EXCEPTION = 1019;
    static final int MESSAGE_SOCKET_TIMEOUT_EXCEPTION = 1008;
    static final int MESSAGE_TMP_FILE_RENAME_FAILED = 1020;
    static final int MESSAGE_URI_SYNTAX_EXCEPTION = 1010;
    static final int MESSAGE_ZERO_CAMPAIGNS_TO_INIT_SUCCESS = 1015;
    public static final String NO_DISK_SPACE = "no_disk_space";
    public static final String NO_NETWORK_CONNECTION = "no_network_connection";
    public static final int OPERATION_TIMEOUT = 5000;
    protected static final String OUT_OF_MEMORY_EXCEPTION = "out of memory exception";
    public static final String SETTINGS = "settings";
    protected static final String SOCKET_TIMEOUT_EXCEPTION = "socket timeout exception";
    public static final String STORAGE_UNAVAILABLE = "sotrage_unavailable";
    private static final String TAG = "DownloadManager";
    private static final String TEMP_DIR_FOR_FILES = "temp";
    private static final String TEMP_PREFIX_FOR_FILES = "tmp_";
    private static final String UNABLE_TO_CREATE_FOLDER = "unable_to_create_folder";
    protected static final String URI_SYNTAX_EXCEPTION = "uri syntax exception";
    public static final String UTF8_CHARSET = "UTF-8";
    private static DownloadManager mDownloadManager;
    private String mCacheRootDirectory;
    private DownloadHandler mDownloadHandler = getDownloadHandler();
    private Thread mMobileControllerThread;

    public interface OnPreCacheCompletion {
        void onFileDownloadFail(SSAFile sSAFile);

        void onFileDownloadSuccess(SSAFile sSAFile);
    }

    private DownloadManager(String str) {
        this.mCacheRootDirectory = str;
        IronSourceStorageUtils.deleteFolder(this.mCacheRootDirectory, TEMP_DIR_FOR_FILES);
        IronSourceStorageUtils.makeDir(this.mCacheRootDirectory, TEMP_DIR_FOR_FILES);
    }

    public static synchronized DownloadManager getInstance(String str) {
        DownloadManager downloadManager;
        synchronized (DownloadManager.class) {
            if (mDownloadManager == null) {
                mDownloadManager = new DownloadManager(str);
            }
            downloadManager = mDownloadManager;
        }
        return downloadManager;
    }

    static class DownloadHandler extends Handler {
        OnPreCacheCompletion mListener;

        DownloadHandler() {
        }

        /* access modifiers changed from: package-private */
        public void setOnPreCacheCompletion(OnPreCacheCompletion onPreCacheCompletion) {
            if (onPreCacheCompletion != null) {
                this.mListener = onPreCacheCompletion;
                return;
            }
            throw new IllegalArgumentException();
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1016:
                    this.mListener.onFileDownloadSuccess((SSAFile) message.obj);
                    return;
                case 1017:
                    this.mListener.onFileDownloadFail((SSAFile) message.obj);
                    return;
                default:
                    return;
            }
        }

        public void release() {
            this.mListener = null;
        }
    }

    /* access modifiers changed from: package-private */
    public DownloadHandler getDownloadHandler() {
        return new DownloadHandler();
    }

    public void setOnPreCacheCompletion(OnPreCacheCompletion onPreCacheCompletion) {
        this.mDownloadHandler.setOnPreCacheCompletion(onPreCacheCompletion);
    }

    public void release() {
        mDownloadManager = null;
        this.mDownloadHandler.release();
        this.mDownloadHandler = null;
    }

    public void downloadFile(SSAFile sSAFile) {
        new Thread(new SingleFileWorkerThread(sSAFile, this.mDownloadHandler, this.mCacheRootDirectory, getTempFilesDirectory())).start();
    }

    public void downloadMobileControllerFile(SSAFile sSAFile) {
        this.mMobileControllerThread = new Thread(new SingleFileWorkerThread(sSAFile, this.mDownloadHandler, this.mCacheRootDirectory, getTempFilesDirectory()));
        this.mMobileControllerThread.start();
    }

    public boolean isMobileControllerThreadLive() {
        return this.mMobileControllerThread != null && this.mMobileControllerThread.isAlive();
    }

    static class SingleFileWorkerThread implements Runnable {
        private String mCacheRootDirectory;
        private long mConnectionRetries = getConnectionRetries();
        Handler mDownloadHandler;
        private String mFile;
        private String mFileName = guessFileName(this.mFile);
        private String mPath;
        private final String mTempFilesDirectory;

        SingleFileWorkerThread(SSAFile sSAFile, Handler handler, String str, String str2) {
            this.mFile = sSAFile.getFile();
            this.mPath = sSAFile.getPath();
            this.mCacheRootDirectory = str;
            this.mDownloadHandler = handler;
            this.mTempFilesDirectory = str2;
        }

        /* access modifiers changed from: package-private */
        public String guessFileName(String str) {
            return SDKUtils.getFileName(this.mFile);
        }

        /* access modifiers changed from: package-private */
        public FileWorkerThread getFileWorkerThread(String str, String str2, String str3, long j, String str4) {
            return new FileWorkerThread(str, str2, str3, j, str4);
        }

        /* access modifiers changed from: package-private */
        public Message getMessage() {
            return new Message();
        }

        /* access modifiers changed from: package-private */
        public String makeDir(String str, String str2) {
            return IronSourceStorageUtils.makeDir(str, str2);
        }

        public void run() {
            SSAFile sSAFile = new SSAFile(this.mFileName, this.mPath);
            Message message = getMessage();
            message.obj = sSAFile;
            String makeDir = makeDir(this.mCacheRootDirectory, this.mPath);
            if (makeDir == null) {
                message.what = 1017;
                sSAFile.setErrMsg(DownloadManager.UNABLE_TO_CREATE_FOLDER);
                this.mDownloadHandler.sendMessage(message);
                return;
            }
            int i = getFileWorkerThread(this.mFile, makeDir, sSAFile.getFile(), this.mConnectionRetries, this.mTempFilesDirectory).call().responseCode;
            if (i != 200) {
                if (i != 404) {
                    switch (i) {
                        case 1004:
                        case 1005:
                        case 1006:
                            break;
                        default:
                            switch (i) {
                                case 1008:
                                case 1009:
                                case 1010:
                                case 1011:
                                    break;
                                default:
                                    switch (i) {
                                        case 1018:
                                        case 1019:
                                            break;
                                        default:
                                            return;
                                    }
                            }
                    }
                }
                String convertErrorCodeToMessage = convertErrorCodeToMessage(i);
                message.what = 1017;
                sSAFile.setErrMsg(convertErrorCodeToMessage);
                this.mDownloadHandler.sendMessage(message);
                return;
            }
            message.what = 1016;
            this.mDownloadHandler.sendMessage(message);
        }

        /* access modifiers changed from: package-private */
        public String convertErrorCodeToMessage(int i) {
            String str = "not defined message for " + i;
            if (i != 404) {
                switch (i) {
                    case 1004:
                        return DownloadManager.MALFORMED_URL_EXCEPTION;
                    case 1005:
                        break;
                    case 1006:
                        return DownloadManager.HTTP_EMPTY_RESPONSE;
                    default:
                        switch (i) {
                            case 1008:
                                return DownloadManager.SOCKET_TIMEOUT_EXCEPTION;
                            case 1009:
                                return DownloadManager.IO_EXCEPTION;
                            case 1010:
                                return DownloadManager.URI_SYNTAX_EXCEPTION;
                            case 1011:
                                return DownloadManager.HTTP_ERROR_CODE;
                            default:
                                switch (i) {
                                    case 1018:
                                        return DownloadManager.FILE_NOT_FOUND_EXCEPTION;
                                    case 1019:
                                        return DownloadManager.OUT_OF_MEMORY_EXCEPTION;
                                    default:
                                        return str;
                                }
                        }
                }
            }
            return DownloadManager.HTTP_NOT_FOUND;
        }

        public long getConnectionRetries() {
            return Long.parseLong(IronSourceSharedPrefHelper.getSupersonicPrefHelper().getConnectionRetries());
        }
    }

    static class FileWorkerThread implements Callable<Result> {
        private long mConnectionRetries;
        private String mDirectory;
        private String mFileName;
        private String mFileUrl;
        private String mTmpFilesDirectory;

        public FileWorkerThread(String str, String str2, String str3, long j, String str4) {
            this.mFileUrl = str;
            this.mDirectory = str2;
            this.mFileName = str3;
            this.mConnectionRetries = j;
            this.mTmpFilesDirectory = str4;
        }

        /* access modifiers changed from: package-private */
        public int saveFile(byte[] bArr, String str) throws Exception {
            return IronSourceStorageUtils.saveFile(bArr, str);
        }

        /* access modifiers changed from: package-private */
        public boolean renameFile(String str, String str2) throws Exception {
            return IronSourceStorageUtils.renameFile(str, str2);
        }

        /* access modifiers changed from: package-private */
        public byte[] getBytes(InputStream inputStream) throws IOException {
            return DownloadManager.getBytes(inputStream);
        }

        public Result call() {
            if (this.mConnectionRetries == 0) {
                this.mConnectionRetries = 1;
            }
            int i = 0;
            Result result = null;
            while (((long) i) < this.mConnectionRetries && ((r3 = (result = downloadContent(this.mFileUrl, i)).responseCode) == 1008 || r3 == 1009)) {
                i++;
            }
            if (!(result == null || result.body == null)) {
                String str = this.mDirectory + File.separator + this.mFileName;
                String str2 = this.mTmpFilesDirectory + File.separator + DownloadManager.TEMP_PREFIX_FOR_FILES + this.mFileName;
                try {
                    if (saveFile(result.body, str2) == 0) {
                        result.responseCode = 1006;
                    } else if (!renameFile(str2, str)) {
                        result.responseCode = 1020;
                    }
                } catch (FileNotFoundException unused) {
                    result.responseCode = 1018;
                } catch (Exception e) {
                    if (!TextUtils.isEmpty(e.getMessage())) {
                        Logger.i(DownloadManager.TAG, e.getMessage());
                    }
                    result.responseCode = 1009;
                } catch (Error e2) {
                    if (!TextUtils.isEmpty(e2.getMessage())) {
                        Logger.i(DownloadManager.TAG, e2.getMessage());
                    }
                    result.responseCode = 1019;
                }
            }
            return result;
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:100:0x0122, code lost:
            r1.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:101:0x0125, code lost:
            if (r3 == null) goto L_0x015a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:108:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:109:0x0131, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:110:0x0132, code lost:
            r1.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:111:0x0135, code lost:
            if (r3 == null) goto L_0x015a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:118:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:119:0x0141, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:120:0x0142, code lost:
            r1.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:121:0x0145, code lost:
            if (r3 == null) goto L_0x015a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:128:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:129:0x0151, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:130:0x0152, code lost:
            r1.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:131:0x0155, code lost:
            if (r3 == null) goto L_0x015a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:132:0x0157, code lost:
            r3.disconnect();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:133:0x015a, code lost:
            r0.url = r8;
            r0.responseCode = r9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0065, code lost:
            r9 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0066, code lost:
            r2 = r4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x0069, code lost:
            r9 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x006a, code lost:
            r2 = r4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x00ac, code lost:
            r9 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:60:0x00c1, code lost:
            com.ironsource.sdk.utils.Logger.i(com.ironsource.sdk.precache.DownloadManager.TAG, r9.getMessage());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:64:0x00d0, code lost:
            r9 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:65:0x00d1, code lost:
            r9.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:67:0x00d6, code lost:
            r3.disconnect();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:0x00eb, code lost:
            com.ironsource.sdk.utils.Logger.i(com.ironsource.sdk.precache.DownloadManager.TAG, r9.getMessage());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:78:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:79:0x00fc, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:80:0x00fd, code lost:
            r1.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:81:0x0100, code lost:
            if (r3 != null) goto L_0x0157;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:98:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:99:0x0121, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:105:? A[ExcHandler: SocketTimeoutException (unused java.net.SocketTimeoutException), PHI: r1 
          PHI: (r1v18 java.io.InputStream) = (r1v1 java.io.InputStream), (r1v21 java.io.InputStream), (r1v21 java.io.InputStream), (r1v1 java.io.InputStream) binds: [B:8:0x0024, B:40:0x0073, B:41:?, B:14:0x003f] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:8:0x0024] */
        /* JADX WARNING: Removed duplicated region for block: B:107:0x012d A[SYNTHETIC, Splitter:B:107:0x012d] */
        /* JADX WARNING: Removed duplicated region for block: B:115:? A[ExcHandler: URISyntaxException (unused java.net.URISyntaxException), PHI: r1 
          PHI: (r1v17 java.io.InputStream) = (r1v1 java.io.InputStream), (r1v21 java.io.InputStream), (r1v21 java.io.InputStream), (r1v1 java.io.InputStream) binds: [B:8:0x0024, B:40:0x0073, B:41:?, B:14:0x003f] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:8:0x0024] */
        /* JADX WARNING: Removed duplicated region for block: B:117:0x013d A[SYNTHETIC, Splitter:B:117:0x013d] */
        /* JADX WARNING: Removed duplicated region for block: B:125:? A[ExcHandler: MalformedURLException (unused java.net.MalformedURLException), PHI: r1 
          PHI: (r1v16 java.io.InputStream) = (r1v1 java.io.InputStream), (r1v21 java.io.InputStream), (r1v21 java.io.InputStream), (r1v1 java.io.InputStream) binds: [B:8:0x0024, B:40:0x0073, B:41:?, B:14:0x003f] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:8:0x0024] */
        /* JADX WARNING: Removed duplicated region for block: B:127:0x014d A[SYNTHETIC, Splitter:B:127:0x014d] */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x00ac A[ExcHandler: Error (e java.lang.Error), PHI: r1 
          PHI: (r1v20 java.io.InputStream) = (r1v1 java.io.InputStream), (r1v21 java.io.InputStream), (r1v21 java.io.InputStream), (r1v1 java.io.InputStream) binds: [B:8:0x0024, B:40:0x0073, B:41:?, B:14:0x003f] A[DONT_GENERATE, DONT_INLINE], Splitter:B:8:0x0024] */
        /* JADX WARNING: Removed duplicated region for block: B:60:0x00c1 A[Catch:{ all -> 0x0103 }] */
        /* JADX WARNING: Removed duplicated region for block: B:62:0x00cc A[SYNTHETIC, Splitter:B:62:0x00cc] */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x00d6  */
        /* JADX WARNING: Removed duplicated region for block: B:74:0x00eb A[Catch:{ all -> 0x0103 }] */
        /* JADX WARNING: Removed duplicated region for block: B:77:0x00f8 A[SYNTHETIC, Splitter:B:77:0x00f8] */
        /* JADX WARNING: Removed duplicated region for block: B:84:0x0106 A[SYNTHETIC, Splitter:B:84:0x0106] */
        /* JADX WARNING: Removed duplicated region for block: B:89:0x0110  */
        /* JADX WARNING: Removed duplicated region for block: B:95:? A[ExcHandler: FileNotFoundException (unused java.io.FileNotFoundException), PHI: r1 
          PHI: (r1v19 java.io.InputStream) = (r1v1 java.io.InputStream), (r1v21 java.io.InputStream), (r1v21 java.io.InputStream), (r1v1 java.io.InputStream) binds: [B:8:0x0024, B:40:0x0073, B:41:?, B:14:0x003f] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:8:0x0024] */
        /* JADX WARNING: Removed duplicated region for block: B:97:0x011d A[SYNTHETIC, Splitter:B:97:0x011d] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.ironsource.sdk.precache.DownloadManager.Result downloadContent(java.lang.String r8, int r9) {
            /*
                r7 = this;
                com.ironsource.sdk.precache.DownloadManager$Result r0 = new com.ironsource.sdk.precache.DownloadManager$Result
                r0.<init>()
                boolean r1 = android.text.TextUtils.isEmpty(r8)
                if (r1 == 0) goto L_0x0012
                r0.url = r8
                r8 = 1007(0x3ef, float:1.411E-42)
                r0.responseCode = r8
                return r0
            L_0x0012:
                r1 = 0
                r2 = 0
                java.net.URL r3 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0148, URISyntaxException -> 0x0138, SocketTimeoutException -> 0x0128, FileNotFoundException -> 0x0118, Exception -> 0x00df, Error -> 0x00b3, all -> 0x00b0 }
                r3.<init>(r8)     // Catch:{ MalformedURLException -> 0x0148, URISyntaxException -> 0x0138, SocketTimeoutException -> 0x0128, FileNotFoundException -> 0x0118, Exception -> 0x00df, Error -> 0x00b3, all -> 0x00b0 }
                r3.toURI()     // Catch:{ MalformedURLException -> 0x0148, URISyntaxException -> 0x0138, SocketTimeoutException -> 0x0128, FileNotFoundException -> 0x0118, Exception -> 0x00df, Error -> 0x00b3, all -> 0x00b0 }
                java.net.URLConnection r3 = r3.openConnection()     // Catch:{ MalformedURLException -> 0x0148, URISyntaxException -> 0x0138, SocketTimeoutException -> 0x0128, FileNotFoundException -> 0x0118, Exception -> 0x00df, Error -> 0x00b3, all -> 0x00b0 }
                java.net.HttpURLConnection r3 = (java.net.HttpURLConnection) r3     // Catch:{ MalformedURLException -> 0x0148, URISyntaxException -> 0x0138, SocketTimeoutException -> 0x0128, FileNotFoundException -> 0x0118, Exception -> 0x00df, Error -> 0x00b3, all -> 0x00b0 }
                java.lang.String r4 = "GET"
                r3.setRequestMethod(r4)     // Catch:{ MalformedURLException -> 0x0149, URISyntaxException -> 0x0139, SocketTimeoutException -> 0x0129, FileNotFoundException -> 0x0119, Exception -> 0x00ae, Error -> 0x00ac }
                r4 = 5000(0x1388, float:7.006E-42)
                r3.setConnectTimeout(r4)     // Catch:{ MalformedURLException -> 0x0149, URISyntaxException -> 0x0139, SocketTimeoutException -> 0x0129, FileNotFoundException -> 0x0119, Exception -> 0x00ae, Error -> 0x00ac }
                r3.setReadTimeout(r4)     // Catch:{ MalformedURLException -> 0x0149, URISyntaxException -> 0x0139, SocketTimeoutException -> 0x0129, FileNotFoundException -> 0x0119, Exception -> 0x00ae, Error -> 0x00ac }
                r3.connect()     // Catch:{ MalformedURLException -> 0x0149, URISyntaxException -> 0x0139, SocketTimeoutException -> 0x0129, FileNotFoundException -> 0x0119, Exception -> 0x00ae, Error -> 0x00ac }
                int r4 = r3.getResponseCode()     // Catch:{ MalformedURLException -> 0x0149, URISyntaxException -> 0x0139, SocketTimeoutException -> 0x0129, FileNotFoundException -> 0x0119, Exception -> 0x00ae, Error -> 0x00ac }
                r2 = 200(0xc8, float:2.8E-43)
                if (r4 < r2) goto L_0x006d
                r5 = 400(0x190, float:5.6E-43)
                if (r4 < r5) goto L_0x003f
                goto L_0x006d
            L_0x003f:
                java.io.InputStream r5 = r3.getInputStream()     // Catch:{ MalformedURLException -> 0x0149, URISyntaxException -> 0x0139, SocketTimeoutException -> 0x0129, FileNotFoundException -> 0x0119, Exception -> 0x0069, Error -> 0x00ac, all -> 0x0065 }
                byte[] r1 = r7.getBytes(r5)     // Catch:{ MalformedURLException -> 0x0062, URISyntaxException -> 0x005f, SocketTimeoutException -> 0x005c, FileNotFoundException -> 0x0059, Exception -> 0x0054, Error -> 0x0050, all -> 0x004b }
                r0.body = r1     // Catch:{ MalformedURLException -> 0x0062, URISyntaxException -> 0x005f, SocketTimeoutException -> 0x005c, FileNotFoundException -> 0x0059, Exception -> 0x0054, Error -> 0x0050, all -> 0x004b }
                r1 = r5
                goto L_0x006f
            L_0x004b:
                r9 = move-exception
                r2 = r4
                r1 = r5
                goto L_0x0104
            L_0x0050:
                r9 = move-exception
                r1 = r5
                goto L_0x00b5
            L_0x0054:
                r9 = move-exception
                r2 = r4
                r1 = r5
                goto L_0x00e1
            L_0x0059:
                r1 = r5
                goto L_0x0119
            L_0x005c:
                r1 = r5
                goto L_0x0129
            L_0x005f:
                r1 = r5
                goto L_0x0139
            L_0x0062:
                r1 = r5
                goto L_0x0149
            L_0x0065:
                r9 = move-exception
                r2 = r4
                goto L_0x0104
            L_0x0069:
                r9 = move-exception
                r2 = r4
                goto L_0x00e1
            L_0x006d:
                r4 = 1011(0x3f3, float:1.417E-42)
            L_0x006f:
                if (r4 == r2) goto L_0x0097
                java.lang.String r2 = "DownloadManager"
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0149, URISyntaxException -> 0x0139, SocketTimeoutException -> 0x0129, FileNotFoundException -> 0x0119, Exception -> 0x0069, Error -> 0x00ac, all -> 0x0065 }
                r5.<init>()     // Catch:{ MalformedURLException -> 0x0149, URISyntaxException -> 0x0139, SocketTimeoutException -> 0x0129, FileNotFoundException -> 0x0119, Exception -> 0x0069, Error -> 0x00ac, all -> 0x0065 }
                java.lang.String r6 = " RESPONSE CODE: "
                r5.append(r6)     // Catch:{ MalformedURLException -> 0x0149, URISyntaxException -> 0x0139, SocketTimeoutException -> 0x0129, FileNotFoundException -> 0x0119, Exception -> 0x0069, Error -> 0x00ac, all -> 0x0065 }
                r5.append(r4)     // Catch:{ MalformedURLException -> 0x0149, URISyntaxException -> 0x0139, SocketTimeoutException -> 0x0129, FileNotFoundException -> 0x0119, Exception -> 0x0069, Error -> 0x00ac, all -> 0x0065 }
                java.lang.String r6 = " URL: "
                r5.append(r6)     // Catch:{ MalformedURLException -> 0x0149, URISyntaxException -> 0x0139, SocketTimeoutException -> 0x0129, FileNotFoundException -> 0x0119, Exception -> 0x0069, Error -> 0x00ac, all -> 0x0065 }
                r5.append(r8)     // Catch:{ MalformedURLException -> 0x0149, URISyntaxException -> 0x0139, SocketTimeoutException -> 0x0129, FileNotFoundException -> 0x0119, Exception -> 0x0069, Error -> 0x00ac, all -> 0x0065 }
                java.lang.String r6 = " ATTEMPT: "
                r5.append(r6)     // Catch:{ MalformedURLException -> 0x0149, URISyntaxException -> 0x0139, SocketTimeoutException -> 0x0129, FileNotFoundException -> 0x0119, Exception -> 0x0069, Error -> 0x00ac, all -> 0x0065 }
                r5.append(r9)     // Catch:{ MalformedURLException -> 0x0149, URISyntaxException -> 0x0139, SocketTimeoutException -> 0x0129, FileNotFoundException -> 0x0119, Exception -> 0x0069, Error -> 0x00ac, all -> 0x0065 }
                java.lang.String r9 = r5.toString()     // Catch:{ MalformedURLException -> 0x0149, URISyntaxException -> 0x0139, SocketTimeoutException -> 0x0129, FileNotFoundException -> 0x0119, Exception -> 0x0069, Error -> 0x00ac, all -> 0x0065 }
                com.ironsource.sdk.utils.Logger.i(r2, r9)     // Catch:{ MalformedURLException -> 0x0149, URISyntaxException -> 0x0139, SocketTimeoutException -> 0x0129, FileNotFoundException -> 0x0119, Exception -> 0x0069, Error -> 0x00ac, all -> 0x0065 }
            L_0x0097:
                if (r1 == 0) goto L_0x00a1
                r1.close()     // Catch:{ IOException -> 0x009d }
                goto L_0x00a1
            L_0x009d:
                r9 = move-exception
                r9.printStackTrace()
            L_0x00a1:
                if (r3 == 0) goto L_0x00a6
                r3.disconnect()
            L_0x00a6:
                r0.url = r8
                r0.responseCode = r4
                goto L_0x015e
            L_0x00ac:
                r9 = move-exception
                goto L_0x00b5
            L_0x00ae:
                r9 = move-exception
                goto L_0x00e1
            L_0x00b0:
                r9 = move-exception
                r3 = r1
                goto L_0x0104
            L_0x00b3:
                r9 = move-exception
                r3 = r1
            L_0x00b5:
                r2 = 1019(0x3fb, float:1.428E-42)
                java.lang.String r4 = r9.getMessage()     // Catch:{ all -> 0x0103 }
                boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x0103 }
                if (r4 != 0) goto L_0x00ca
                java.lang.String r4 = "DownloadManager"
                java.lang.String r9 = r9.getMessage()     // Catch:{ all -> 0x0103 }
                com.ironsource.sdk.utils.Logger.i(r4, r9)     // Catch:{ all -> 0x0103 }
            L_0x00ca:
                if (r1 == 0) goto L_0x00d4
                r1.close()     // Catch:{ IOException -> 0x00d0 }
                goto L_0x00d4
            L_0x00d0:
                r9 = move-exception
                r9.printStackTrace()
            L_0x00d4:
                if (r3 == 0) goto L_0x00d9
                r3.disconnect()
            L_0x00d9:
                r0.url = r8
                r0.responseCode = r2
                goto L_0x015e
            L_0x00df:
                r9 = move-exception
                r3 = r1
            L_0x00e1:
                java.lang.String r4 = r9.getMessage()     // Catch:{ all -> 0x0103 }
                boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x0103 }
                if (r4 != 0) goto L_0x00f4
                java.lang.String r4 = "DownloadManager"
                java.lang.String r9 = r9.getMessage()     // Catch:{ all -> 0x0103 }
                com.ironsource.sdk.utils.Logger.i(r4, r9)     // Catch:{ all -> 0x0103 }
            L_0x00f4:
                r9 = 1009(0x3f1, float:1.414E-42)
                if (r1 == 0) goto L_0x0100
                r1.close()     // Catch:{ IOException -> 0x00fc }
                goto L_0x0100
            L_0x00fc:
                r1 = move-exception
                r1.printStackTrace()
            L_0x0100:
                if (r3 == 0) goto L_0x015a
                goto L_0x0157
            L_0x0103:
                r9 = move-exception
            L_0x0104:
                if (r1 == 0) goto L_0x010e
                r1.close()     // Catch:{ IOException -> 0x010a }
                goto L_0x010e
            L_0x010a:
                r1 = move-exception
                r1.printStackTrace()
            L_0x010e:
                if (r3 == 0) goto L_0x0113
                r3.disconnect()
            L_0x0113:
                r0.url = r8
                r0.responseCode = r2
                throw r9
            L_0x0118:
                r3 = r1
            L_0x0119:
                r9 = 1018(0x3fa, float:1.427E-42)
                if (r1 == 0) goto L_0x0125
                r1.close()     // Catch:{ IOException -> 0x0121 }
                goto L_0x0125
            L_0x0121:
                r1 = move-exception
                r1.printStackTrace()
            L_0x0125:
                if (r3 == 0) goto L_0x015a
                goto L_0x0157
            L_0x0128:
                r3 = r1
            L_0x0129:
                r9 = 1008(0x3f0, float:1.413E-42)
                if (r1 == 0) goto L_0x0135
                r1.close()     // Catch:{ IOException -> 0x0131 }
                goto L_0x0135
            L_0x0131:
                r1 = move-exception
                r1.printStackTrace()
            L_0x0135:
                if (r3 == 0) goto L_0x015a
                goto L_0x0157
            L_0x0138:
                r3 = r1
            L_0x0139:
                r9 = 1010(0x3f2, float:1.415E-42)
                if (r1 == 0) goto L_0x0145
                r1.close()     // Catch:{ IOException -> 0x0141 }
                goto L_0x0145
            L_0x0141:
                r1 = move-exception
                r1.printStackTrace()
            L_0x0145:
                if (r3 == 0) goto L_0x015a
                goto L_0x0157
            L_0x0148:
                r3 = r1
            L_0x0149:
                r9 = 1004(0x3ec, float:1.407E-42)
                if (r1 == 0) goto L_0x0155
                r1.close()     // Catch:{ IOException -> 0x0151 }
                goto L_0x0155
            L_0x0151:
                r1 = move-exception
                r1.printStackTrace()
            L_0x0155:
                if (r3 == 0) goto L_0x015a
            L_0x0157:
                r3.disconnect()
            L_0x015a:
                r0.url = r8
                r0.responseCode = r9
            L_0x015e:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.precache.DownloadManager.FileWorkerThread.downloadContent(java.lang.String, int):com.ironsource.sdk.precache.DownloadManager$Result");
        }
    }

    /* access modifiers changed from: package-private */
    public String getTempFilesDirectory() {
        return this.mCacheRootDirectory + File.separator + TEMP_DIR_FOR_FILES;
    }

    static byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[8192];
        while (true) {
            int read = inputStream.read(bArr, 0, bArr.length);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                byteArrayOutputStream.flush();
                return byteArrayOutputStream.toByteArray();
            }
        }
    }

    static class Result {
        byte[] body;
        int responseCode;
        public String url;

        Result() {
        }
    }
}
