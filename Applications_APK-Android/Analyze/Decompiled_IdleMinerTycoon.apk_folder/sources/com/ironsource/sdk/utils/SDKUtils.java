package com.ironsource.sdk.utils;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import com.facebook.appevents.AppEventsConstants;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.data.SSAEnums;
import com.ironsource.sdk.data.SSAObj;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SDKUtils {
    private static final String TAG = "SDKUtils";
    private static String mAdvertisingId = null;
    private static String mControllerConfig = null;
    private static String mControllerUrl = null;
    private static int mDebugMode = 0;
    private static Map<String, String> mInitSDKParams = null;
    private static boolean mIsLimitedTrackingEnabled = true;
    private static String mUserGroup = "";
    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    public static String getSDKVersion() {
        return Constants.SDK_VERSION;
    }

    public static String translateOrientation(int i) {
        switch (i) {
            case 1:
                return "portrait";
            case 2:
                return "landscape";
            default:
                return Constants.ParametersKeys.ORIENTATION_NONE;
        }
    }

    public static String translateRequestedOrientation(int i) {
        switch (i) {
            case 0:
            case 6:
            case 8:
            case 11:
                return "landscape";
            case 1:
            case 7:
            case 9:
            case 12:
                return "portrait";
            case 2:
            case 3:
            case 4:
            case 5:
            case 10:
            default:
                return Constants.ParametersKeys.ORIENTATION_NONE;
        }
    }

    public static String getFileName(String str) {
        String[] split = str.split(File.separator);
        try {
            return URLEncoder.encode(split[split.length - 1].split("\\?")[0], "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int pxToDp(long j) {
        return (int) ((((float) j) / Resources.getSystem().getDisplayMetrics().density) + 0.5f);
    }

    public static int dpToPx(long j) {
        return (int) ((((float) j) * Resources.getSystem().getDisplayMetrics().density) + 0.5f);
    }

    public static int convertPxToDp(int i) {
        return (int) TypedValue.applyDimension(1, (float) i, Resources.getSystem().getDisplayMetrics());
    }

    public static int convertDpToPx(int i) {
        return (int) TypedValue.applyDimension(0, (float) i, Resources.getSystem().getDisplayMetrics());
    }

    public static JSONObject getOrientation(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("orientation", translateOrientation(DeviceStatus.getDeviceOrientation(context)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject;
    }

    public static String createErrorMessage(String str, String str2) {
        return String.format("%s Failure occurred during initiation at: %s", str, str2);
    }

    public static Long getCurrentTimeMillis() {
        return Long.valueOf(System.currentTimeMillis());
    }

    public static boolean isApplicationVisible(Context context) {
        String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
            if (next.processName.equalsIgnoreCase(packageName) && next.importance == 100) {
                return true;
            }
        }
        return false;
    }

    public static void showNoInternetDialog(Context context) {
        new AlertDialog.Builder(context).setMessage("No Internet Connection").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show();
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002a A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] encrypt(java.lang.String r3) {
        /*
            r0 = 0
            java.lang.String r1 = "SHA-1"
            java.security.MessageDigest r1 = java.security.MessageDigest.getInstance(r1)     // Catch:{ NoSuchAlgorithmException -> 0x001e, UnsupportedEncodingException -> 0x0018 }
            r1.reset()     // Catch:{ NoSuchAlgorithmException -> 0x0016, UnsupportedEncodingException -> 0x0014 }
            java.lang.String r2 = "UTF-8"
            byte[] r3 = r3.getBytes(r2)     // Catch:{ NoSuchAlgorithmException -> 0x0016, UnsupportedEncodingException -> 0x0014 }
            r1.update(r3)     // Catch:{ NoSuchAlgorithmException -> 0x0016, UnsupportedEncodingException -> 0x0014 }
            goto L_0x0023
        L_0x0014:
            r3 = move-exception
            goto L_0x001a
        L_0x0016:
            r3 = move-exception
            goto L_0x0020
        L_0x0018:
            r3 = move-exception
            r1 = r0
        L_0x001a:
            r3.printStackTrace()
            goto L_0x0023
        L_0x001e:
            r3 = move-exception
            r1 = r0
        L_0x0020:
            r3.printStackTrace()
        L_0x0023:
            if (r1 == 0) goto L_0x002a
            byte[] r3 = r1.digest()
            return r3
        L_0x002a:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.utils.SDKUtils.encrypt(java.lang.String):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0063  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String convertStreamToString(java.io.InputStream r5, boolean r6, java.lang.String r7, java.lang.String r8) throws java.io.IOException {
        /*
            if (r6 == 0) goto L_0x0008
            java.util.zip.GZIPInputStream r0 = new java.util.zip.GZIPInputStream
            r0.<init>(r5)
            goto L_0x0009
        L_0x0008:
            r0 = r5
        L_0x0009:
            r1 = 0
            java.io.File r2 = new java.io.File
            r2.<init>(r7, r8)
            java.io.BufferedWriter r7 = new java.io.BufferedWriter
            java.io.FileWriter r8 = new java.io.FileWriter
            r8.<init>(r2)
            r7.<init>(r8)
            java.io.BufferedReader r8 = new java.io.BufferedReader     // Catch:{ all -> 0x0055 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ all -> 0x0055 }
            java.lang.String r3 = "UTF-8"
            r2.<init>(r0, r3)     // Catch:{ all -> 0x0055 }
            r8.<init>(r2)     // Catch:{ all -> 0x0055 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0053 }
            r1.<init>()     // Catch:{ all -> 0x0053 }
        L_0x002a:
            java.lang.String r2 = r8.readLine()     // Catch:{ all -> 0x0053 }
            if (r2 == 0) goto L_0x0039
            r1.append(r2)     // Catch:{ all -> 0x0053 }
            java.lang.String r2 = "\n"
            r1.append(r2)     // Catch:{ all -> 0x0053 }
            goto L_0x002a
        L_0x0039:
            java.lang.String r2 = r1.toString()     // Catch:{ all -> 0x0053 }
            r7.write(r2)     // Catch:{ all -> 0x0053 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0053 }
            r8.close()
            r0.close()
            if (r6 == 0) goto L_0x004f
            r5.close()
        L_0x004f:
            r7.close()
            return r1
        L_0x0053:
            r1 = move-exception
            goto L_0x0059
        L_0x0055:
            r8 = move-exception
            r4 = r1
            r1 = r8
            r8 = r4
        L_0x0059:
            if (r8 == 0) goto L_0x005e
            r8.close()
        L_0x005e:
            r0.close()
            if (r6 == 0) goto L_0x0066
            r5.close()
        L_0x0066:
            r7.close()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.utils.SDKUtils.convertStreamToString(java.io.InputStream, boolean, java.lang.String, java.lang.String):java.lang.String");
    }

    public static String encodeString(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException unused) {
            return "";
        }
    }

    public static String decodeString(String str) {
        try {
            return URLDecoder.decode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            String str2 = TAG;
            Logger.d(str2, "Failed decoding string " + e.getMessage());
            return "";
        }
    }

    public static String getMD5(String str) {
        try {
            String bigInteger = new BigInteger(1, MessageDigest.getInstance("MD5").digest(str.getBytes())).toString(16);
            while (bigInteger.length() < 32) {
                bigInteger = AppEventsConstants.EVENT_PARAM_VALUE_NO + bigInteger;
            }
            return bigInteger;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x004b, code lost:
        r0 = com.ironsource.sdk.utils.SDKUtils.TAG;
        com.ironsource.sdk.utils.Logger.i(r0, r3.getClass().getSimpleName() + ": " + r3.getCause());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0019, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001e, code lost:
        if (r3.getMessage() != null) goto L_0x0020;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0020, code lost:
        r0 = com.ironsource.sdk.utils.SDKUtils.TAG;
        com.ironsource.sdk.utils.Logger.i(r0, r3.getClass().getSimpleName() + ": " + r3.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0049, code lost:
        if (r3.getCause() != null) goto L_0x004b;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void loadGoogleAdvertiserInfo(android.content.Context r3) {
        /*
            java.lang.String[] r3 = com.ironsource.environment.DeviceStatus.getAdvertisingIdInfo(r3)     // Catch:{ Exception -> 0x0019, all -> 0x0017 }
            r0 = 0
            r0 = r3[r0]     // Catch:{ Exception -> 0x0019, all -> 0x0017 }
            com.ironsource.sdk.utils.SDKUtils.mAdvertisingId = r0     // Catch:{ Exception -> 0x0019, all -> 0x0017 }
            r0 = 1
            r3 = r3[r0]     // Catch:{ Exception -> 0x0019, all -> 0x0017 }
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)     // Catch:{ Exception -> 0x0019, all -> 0x0017 }
            boolean r3 = r3.booleanValue()     // Catch:{ Exception -> 0x0019, all -> 0x0017 }
            com.ironsource.sdk.utils.SDKUtils.mIsLimitedTrackingEnabled = r3     // Catch:{ Exception -> 0x0019, all -> 0x0017 }
            goto L_0x0070
        L_0x0017:
            r3 = move-exception
            throw r3
        L_0x0019:
            r3 = move-exception
            java.lang.String r0 = r3.getMessage()
            if (r0 == 0) goto L_0x0045
            java.lang.String r0 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.Class r2 = r3.getClass()
            java.lang.String r2 = r2.getSimpleName()
            r1.append(r2)
            java.lang.String r2 = ": "
            r1.append(r2)
            java.lang.String r2 = r3.getMessage()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.ironsource.sdk.utils.Logger.i(r0, r1)
        L_0x0045:
            java.lang.Throwable r0 = r3.getCause()
            if (r0 == 0) goto L_0x0070
            java.lang.String r0 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.Class r2 = r3.getClass()
            java.lang.String r2 = r2.getSimpleName()
            r1.append(r2)
            java.lang.String r2 = ": "
            r1.append(r2)
            java.lang.Throwable r3 = r3.getCause()
            r1.append(r3)
            java.lang.String r3 = r1.toString()
            com.ironsource.sdk.utils.Logger.i(r0, r3)
        L_0x0070:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.utils.SDKUtils.loadGoogleAdvertiserInfo(android.content.Context):void");
    }

    public static String getAdvertiserId() {
        return mAdvertisingId;
    }

    public static boolean isLimitAdTrackingEnabled() {
        return mIsLimitedTrackingEnabled;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0050, code lost:
        if (r7.getCause() != null) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0052, code lost:
        r1 = com.ironsource.sdk.utils.SDKUtils.TAG;
        r2 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005b, code lost:
        r7 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0060, code lost:
        if (r7.getMessage() != null) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0062, code lost:
        r1 = com.ironsource.sdk.utils.SDKUtils.TAG;
        com.ironsource.sdk.utils.Logger.i(r1, r7.getClass().getSimpleName() + ": " + r7.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x008b, code lost:
        if (r7.getCause() != null) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x008d, code lost:
        r1 = com.ironsource.sdk.utils.SDKUtils.TAG;
        r2 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0096, code lost:
        r7 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x009b, code lost:
        if (r7.getMessage() != null) goto L_0x009d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x009d, code lost:
        r1 = com.ironsource.sdk.utils.SDKUtils.TAG;
        com.ironsource.sdk.utils.Logger.i(r1, r7.getClass().getSimpleName() + ": " + r7.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00c6, code lost:
        if (r7.getCause() != null) goto L_0x00c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00c8, code lost:
        r1 = com.ironsource.sdk.utils.SDKUtils.TAG;
        r2 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00d0, code lost:
        r7 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00d5, code lost:
        if (r7.getMessage() != null) goto L_0x00d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00d7, code lost:
        r1 = com.ironsource.sdk.utils.SDKUtils.TAG;
        com.ironsource.sdk.utils.Logger.i(r1, r7.getClass().getSimpleName() + ": " + r7.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0100, code lost:
        if (r7.getCause() != null) goto L_0x0102;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0102, code lost:
        r1 = com.ironsource.sdk.utils.SDKUtils.TAG;
        r2 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x010a, code lost:
        r7 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x010f, code lost:
        if (r7.getMessage() != null) goto L_0x0111;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0111, code lost:
        r1 = com.ironsource.sdk.utils.SDKUtils.TAG;
        com.ironsource.sdk.utils.Logger.i(r1, r7.getClass().getSimpleName() + ": " + r7.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x013a, code lost:
        if (r7.getCause() != null) goto L_0x013c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x013c, code lost:
        r1 = com.ironsource.sdk.utils.SDKUtils.TAG;
        r2 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0143, code lost:
        r2.append(r7.getClass().getSimpleName());
        r2.append(": ");
        r2.append(r7.getCause());
        com.ironsource.sdk.utils.Logger.i(r1, r2.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0161, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0020, code lost:
        r7 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0025, code lost:
        if (r7.getMessage() != null) goto L_0x0027;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0027, code lost:
        r1 = com.ironsource.sdk.utils.SDKUtils.TAG;
        com.ironsource.sdk.utils.Logger.i(r1, r7.getClass().getSimpleName() + ": " + r7.getMessage());
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object getIInAppBillingServiceClass(android.os.IBinder r7) {
        /*
            r0 = 0
            java.lang.String r1 = "com.android.vending.billing.IInAppBillingService$Stub"
            java.lang.Class r1 = java.lang.Class.forName(r1)     // Catch:{ ClassNotFoundException -> 0x010a, NoSuchMethodException -> 0x00d0, IllegalAccessException -> 0x0096, IllegalArgumentException -> 0x005b, InvocationTargetException -> 0x0020, all -> 0x001e }
            java.lang.String r2 = "asInterface"
            r3 = 1
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch:{ ClassNotFoundException -> 0x010a, NoSuchMethodException -> 0x00d0, IllegalAccessException -> 0x0096, IllegalArgumentException -> 0x005b, InvocationTargetException -> 0x0020, all -> 0x001e }
            java.lang.Class<android.os.IBinder> r5 = android.os.IBinder.class
            r6 = 0
            r4[r6] = r5     // Catch:{ ClassNotFoundException -> 0x010a, NoSuchMethodException -> 0x00d0, IllegalAccessException -> 0x0096, IllegalArgumentException -> 0x005b, InvocationTargetException -> 0x0020, all -> 0x001e }
            java.lang.reflect.Method r1 = r1.getMethod(r2, r4)     // Catch:{ ClassNotFoundException -> 0x010a, NoSuchMethodException -> 0x00d0, IllegalAccessException -> 0x0096, IllegalArgumentException -> 0x005b, InvocationTargetException -> 0x0020, all -> 0x001e }
            java.lang.Object[] r2 = new java.lang.Object[r3]     // Catch:{ ClassNotFoundException -> 0x010a, NoSuchMethodException -> 0x00d0, IllegalAccessException -> 0x0096, IllegalArgumentException -> 0x005b, InvocationTargetException -> 0x0020, all -> 0x001e }
            r2[r6] = r7     // Catch:{ ClassNotFoundException -> 0x010a, NoSuchMethodException -> 0x00d0, IllegalAccessException -> 0x0096, IllegalArgumentException -> 0x005b, InvocationTargetException -> 0x0020, all -> 0x001e }
            java.lang.Object r7 = r1.invoke(r0, r2)     // Catch:{ ClassNotFoundException -> 0x010a, NoSuchMethodException -> 0x00d0, IllegalAccessException -> 0x0096, IllegalArgumentException -> 0x005b, InvocationTargetException -> 0x0020, all -> 0x001e }
            return r7
        L_0x001e:
            r7 = move-exception
            throw r7
        L_0x0020:
            r7 = move-exception
            java.lang.String r1 = r7.getMessage()
            if (r1 == 0) goto L_0x004c
            java.lang.String r1 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r7.getClass()
            java.lang.String r3 = r3.getSimpleName()
            r2.append(r3)
            java.lang.String r3 = ": "
            r2.append(r3)
            java.lang.String r3 = r7.getMessage()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.ironsource.sdk.utils.Logger.i(r1, r2)
        L_0x004c:
            java.lang.Throwable r1 = r7.getCause()
            if (r1 == 0) goto L_0x0161
            java.lang.String r1 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            goto L_0x0143
        L_0x005b:
            r7 = move-exception
            java.lang.String r1 = r7.getMessage()
            if (r1 == 0) goto L_0x0087
            java.lang.String r1 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r7.getClass()
            java.lang.String r3 = r3.getSimpleName()
            r2.append(r3)
            java.lang.String r3 = ": "
            r2.append(r3)
            java.lang.String r3 = r7.getMessage()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.ironsource.sdk.utils.Logger.i(r1, r2)
        L_0x0087:
            java.lang.Throwable r1 = r7.getCause()
            if (r1 == 0) goto L_0x0161
            java.lang.String r1 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            goto L_0x0143
        L_0x0096:
            r7 = move-exception
            java.lang.String r1 = r7.getMessage()
            if (r1 == 0) goto L_0x00c2
            java.lang.String r1 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r7.getClass()
            java.lang.String r3 = r3.getSimpleName()
            r2.append(r3)
            java.lang.String r3 = ": "
            r2.append(r3)
            java.lang.String r3 = r7.getMessage()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.ironsource.sdk.utils.Logger.i(r1, r2)
        L_0x00c2:
            java.lang.Throwable r1 = r7.getCause()
            if (r1 == 0) goto L_0x0161
            java.lang.String r1 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            goto L_0x0143
        L_0x00d0:
            r7 = move-exception
            java.lang.String r1 = r7.getMessage()
            if (r1 == 0) goto L_0x00fc
            java.lang.String r1 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r7.getClass()
            java.lang.String r3 = r3.getSimpleName()
            r2.append(r3)
            java.lang.String r3 = ": "
            r2.append(r3)
            java.lang.String r3 = r7.getMessage()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.ironsource.sdk.utils.Logger.i(r1, r2)
        L_0x00fc:
            java.lang.Throwable r1 = r7.getCause()
            if (r1 == 0) goto L_0x0161
            java.lang.String r1 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            goto L_0x0143
        L_0x010a:
            r7 = move-exception
            java.lang.String r1 = r7.getMessage()
            if (r1 == 0) goto L_0x0136
            java.lang.String r1 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r7.getClass()
            java.lang.String r3 = r3.getSimpleName()
            r2.append(r3)
            java.lang.String r3 = ": "
            r2.append(r3)
            java.lang.String r3 = r7.getMessage()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.ironsource.sdk.utils.Logger.i(r1, r2)
        L_0x0136:
            java.lang.Throwable r1 = r7.getCause()
            if (r1 == 0) goto L_0x0161
            java.lang.String r1 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
        L_0x0143:
            java.lang.Class r3 = r7.getClass()
            java.lang.String r3 = r3.getSimpleName()
            r2.append(r3)
            java.lang.String r3 = ": "
            r2.append(r3)
            java.lang.Throwable r7 = r7.getCause()
            r2.append(r7)
            java.lang.String r7 = r2.toString()
            com.ironsource.sdk.utils.Logger.i(r1, r7)
        L_0x0161:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.utils.SDKUtils.getIInAppBillingServiceClass(android.os.IBinder):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00f0, code lost:
        r10 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00f5, code lost:
        if (r10.getMessage() != null) goto L_0x00f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00f7, code lost:
        r11 = com.ironsource.sdk.utils.SDKUtils.TAG;
        com.ironsource.sdk.utils.Logger.i(r11, r10.getClass().getSimpleName() + ": " + r10.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0120, code lost:
        if (r10.getCause() != null) goto L_0x0122;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0122, code lost:
        r11 = com.ironsource.sdk.utils.SDKUtils.TAG;
        r1 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x012b, code lost:
        r10 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0130, code lost:
        if (r10.getMessage() != null) goto L_0x0132;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0132, code lost:
        r11 = com.ironsource.sdk.utils.SDKUtils.TAG;
        com.ironsource.sdk.utils.Logger.i(r11, r10.getClass().getSimpleName() + ": " + r10.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x015b, code lost:
        if (r10.getCause() != null) goto L_0x015d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x015d, code lost:
        r11 = com.ironsource.sdk.utils.SDKUtils.TAG;
        r1 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0165, code lost:
        r10 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x016a, code lost:
        if (r10.getMessage() != null) goto L_0x016c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x016c, code lost:
        r11 = com.ironsource.sdk.utils.SDKUtils.TAG;
        com.ironsource.sdk.utils.Logger.i(r11, r10.getClass().getSimpleName() + ": " + r10.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0195, code lost:
        if (r10.getCause() != null) goto L_0x0197;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0197, code lost:
        r11 = com.ironsource.sdk.utils.SDKUtils.TAG;
        r1 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x019f, code lost:
        r10 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x01a4, code lost:
        if (r10.getMessage() != null) goto L_0x01a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x01a6, code lost:
        r11 = com.ironsource.sdk.utils.SDKUtils.TAG;
        com.ironsource.sdk.utils.Logger.i(r11, r10.getClass().getSimpleName() + ": " + r10.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x01cf, code lost:
        if (r10.getCause() != null) goto L_0x01d1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x01d1, code lost:
        r11 = com.ironsource.sdk.utils.SDKUtils.TAG;
        r1 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x01d8, code lost:
        r1.append(r10.getClass().getSimpleName());
        r1.append(": ");
        r1.append(r10.getCause());
        com.ironsource.sdk.utils.Logger.i(r11, r1.toString());
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String queryingPurchasedItems(java.lang.Object r10, java.lang.String r11) {
        /*
            org.json.JSONArray r0 = new org.json.JSONArray
            r0.<init>()
            java.lang.Class r1 = r10.getClass()     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r2 = "getPurchases"
            r3 = 4
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Class r5 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            r6 = 0
            r4[r6] = r5     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Class<java.lang.String> r5 = java.lang.String.class
            r7 = 1
            r4[r7] = r5     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Class<java.lang.String> r5 = java.lang.String.class
            r8 = 2
            r4[r8] = r5     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Class<java.lang.String> r5 = java.lang.String.class
            r9 = 3
            r4[r9] = r5     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.reflect.Method r1 = r1.getMethod(r2, r4)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Object[] r2 = new java.lang.Object[r3]     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r9)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            r2[r6] = r3     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            r2[r7] = r11     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r11 = "inapp"
            r2[r8] = r11     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            r11 = 0
            r2[r9] = r11     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Object r10 = r1.invoke(r10, r2)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Class r11 = r10.getClass()     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r1 = "getInt"
            java.lang.Class[] r2 = new java.lang.Class[r7]     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Class<java.lang.String> r3 = java.lang.String.class
            r2[r6] = r3     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.reflect.Method r11 = r11.getMethod(r1, r2)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Class r1 = r10.getClass()     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r2 = "getStringArrayList"
            java.lang.Class[] r3 = new java.lang.Class[r7]     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Class<java.lang.String> r4 = java.lang.String.class
            r3[r6] = r4     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.reflect.Method r1 = r1.getMethod(r2, r3)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Class r2 = r10.getClass()     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r3 = "getString"
            java.lang.Class[] r4 = new java.lang.Class[r7]     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Class<java.lang.String> r5 = java.lang.String.class
            r4[r6] = r5     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.reflect.Method r2 = r2.getMethod(r3, r4)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Object[] r3 = new java.lang.Object[r7]     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r4 = "RESPONSE_CODE"
            r3[r6] = r4     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Object r11 = r11.invoke(r10, r3)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Integer r11 = (java.lang.Integer) r11     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            int r11 = r11.intValue()     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            if (r11 != 0) goto L_0x01f6
            java.lang.Object[] r11 = new java.lang.Object[r7]     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r3 = "INAPP_PURCHASE_ITEM_LIST"
            r11[r6] = r3     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Object r11 = r1.invoke(r10, r11)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.util.ArrayList r11 = (java.util.ArrayList) r11     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Object[] r3 = new java.lang.Object[r7]     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r4 = "INAPP_PURCHASE_DATA_LIST"
            r3[r6] = r4     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Object r3 = r1.invoke(r10, r3)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.util.ArrayList r3 = (java.util.ArrayList) r3     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Object[] r4 = new java.lang.Object[r7]     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r5 = "INAPP_DATA_SIGNATURE_LIST"
            r4[r6] = r5     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Object r1 = r1.invoke(r10, r4)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.util.ArrayList r1 = (java.util.ArrayList) r1     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Object[] r4 = new java.lang.Object[r7]     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r5 = "INAPP_CONTINUATION_TOKEN"
            r4[r6] = r5     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Object r10 = r2.invoke(r10, r4)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
        L_0x00ad:
            int r10 = r3.size()     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            if (r6 >= r10) goto L_0x01f6
            java.lang.Object r10 = r3.get(r6)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Object r2 = r1.get(r6)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.Object r4 = r11.get(r6)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r5 = com.ironsource.sdk.utils.SDKUtils.TAG     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            com.ironsource.sdk.utils.Logger.i(r5, r10)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r5 = com.ironsource.sdk.utils.SDKUtils.TAG     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            com.ironsource.sdk.utils.Logger.i(r5, r2)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r2 = com.ironsource.sdk.utils.SDKUtils.TAG     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            com.ironsource.sdk.utils.Logger.i(r2, r4)     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            r2.<init>()     // Catch:{ NoSuchMethodException -> 0x019f, IllegalAccessException -> 0x0165, IllegalArgumentException -> 0x012b, InvocationTargetException -> 0x00f0, all -> 0x00ee }
            java.lang.String r4 = "purchaseData"
            r2.put(r4, r10)     // Catch:{ JSONException -> 0x00eb }
            java.lang.String r4 = "signature"
            r2.put(r4, r10)     // Catch:{ JSONException -> 0x00eb }
            java.lang.String r4 = "sku"
            r2.put(r4, r10)     // Catch:{ JSONException -> 0x00eb }
            r0.put(r2)     // Catch:{ JSONException -> 0x00eb }
        L_0x00eb:
            int r6 = r6 + 1
            goto L_0x00ad
        L_0x00ee:
            r10 = move-exception
            throw r10
        L_0x00f0:
            r10 = move-exception
            java.lang.String r11 = r10.getMessage()
            if (r11 == 0) goto L_0x011c
            java.lang.String r11 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.Class r2 = r10.getClass()
            java.lang.String r2 = r2.getSimpleName()
            r1.append(r2)
            java.lang.String r2 = ": "
            r1.append(r2)
            java.lang.String r2 = r10.getMessage()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.ironsource.sdk.utils.Logger.i(r11, r1)
        L_0x011c:
            java.lang.Throwable r11 = r10.getCause()
            if (r11 == 0) goto L_0x01f6
            java.lang.String r11 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            goto L_0x01d8
        L_0x012b:
            r10 = move-exception
            java.lang.String r11 = r10.getMessage()
            if (r11 == 0) goto L_0x0157
            java.lang.String r11 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.Class r2 = r10.getClass()
            java.lang.String r2 = r2.getSimpleName()
            r1.append(r2)
            java.lang.String r2 = ": "
            r1.append(r2)
            java.lang.String r2 = r10.getMessage()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.ironsource.sdk.utils.Logger.i(r11, r1)
        L_0x0157:
            java.lang.Throwable r11 = r10.getCause()
            if (r11 == 0) goto L_0x01f6
            java.lang.String r11 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            goto L_0x01d8
        L_0x0165:
            r10 = move-exception
            java.lang.String r11 = r10.getMessage()
            if (r11 == 0) goto L_0x0191
            java.lang.String r11 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.Class r2 = r10.getClass()
            java.lang.String r2 = r2.getSimpleName()
            r1.append(r2)
            java.lang.String r2 = ": "
            r1.append(r2)
            java.lang.String r2 = r10.getMessage()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.ironsource.sdk.utils.Logger.i(r11, r1)
        L_0x0191:
            java.lang.Throwable r11 = r10.getCause()
            if (r11 == 0) goto L_0x01f6
            java.lang.String r11 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            goto L_0x01d8
        L_0x019f:
            r10 = move-exception
            java.lang.String r11 = r10.getMessage()
            if (r11 == 0) goto L_0x01cb
            java.lang.String r11 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.Class r2 = r10.getClass()
            java.lang.String r2 = r2.getSimpleName()
            r1.append(r2)
            java.lang.String r2 = ": "
            r1.append(r2)
            java.lang.String r2 = r10.getMessage()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.ironsource.sdk.utils.Logger.i(r11, r1)
        L_0x01cb:
            java.lang.Throwable r11 = r10.getCause()
            if (r11 == 0) goto L_0x01f6
            java.lang.String r11 = com.ironsource.sdk.utils.SDKUtils.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
        L_0x01d8:
            java.lang.Class r2 = r10.getClass()
            java.lang.String r2 = r2.getSimpleName()
            r1.append(r2)
            java.lang.String r2 = ": "
            r1.append(r2)
            java.lang.Throwable r10 = r10.getCause()
            r1.append(r10)
            java.lang.String r10 = r1.toString()
            com.ironsource.sdk.utils.Logger.i(r11, r10)
        L_0x01f6:
            java.lang.String r10 = r0.toString()
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.utils.SDKUtils.queryingPurchasedItems(java.lang.Object, java.lang.String):java.lang.String");
    }

    public static String getControllerUrl() {
        return !TextUtils.isEmpty(mControllerUrl) ? mControllerUrl : "";
    }

    public static void setControllerUrl(String str) {
        mControllerUrl = str;
    }

    public static String getControllerConfig() {
        return mControllerConfig;
    }

    public static void setControllerConfig(String str) {
        mControllerConfig = str;
    }

    public static Map<String, String> getInitSDKParams() {
        return mInitSDKParams;
    }

    public static void setInitSDKParams(Map<String, String> map) {
        mInitSDKParams = map;
    }

    public static int getDebugMode() {
        return mDebugMode;
    }

    public static void setDebugMode(int i) {
        mDebugMode = i;
    }

    public static String getValueFromJsonObject(String str, String str2) {
        try {
            return new JSONObject(str).getString(str2);
        } catch (Exception unused) {
            return null;
        }
    }

    public static boolean isExternalStorageAvailable() {
        String externalStorageState = Environment.getExternalStorageState();
        return "mounted".equals(externalStorageState) || "mounted_ro".equals(externalStorageState);
    }

    public static int getActivityUIFlags(boolean z) {
        int i = Build.VERSION.SDK_INT >= 14 ? 2 : 0;
        if (Build.VERSION.SDK_INT >= 16) {
            i |= 1796;
        }
        return (Build.VERSION.SDK_INT < 19 || !z) ? i : i | 4096;
    }

    private static int generateViewIdForOldOS() {
        int i;
        int i2;
        do {
            i = sNextGeneratedId.get();
            i2 = i + 1;
            if (i2 > 16777215) {
                i2 = 1;
            }
        } while (!sNextGeneratedId.compareAndSet(i, i2));
        return i;
    }

    public static int generateViewId() {
        if (Build.VERSION.SDK_INT < 17) {
            return generateViewIdForOldOS();
        }
        return View.generateViewId();
    }

    public static JSONObject getControllerConfigAsJSONObject() {
        try {
            return new JSONObject(getControllerConfig());
        } catch (JSONException e) {
            e.printStackTrace();
            return new JSONObject();
        }
    }

    public static SSAEnums.ProductType getProductType(String str) {
        if (str.equalsIgnoreCase(SSAEnums.ProductType.RewardedVideo.toString())) {
            return SSAEnums.ProductType.RewardedVideo;
        }
        if (str.equalsIgnoreCase(SSAEnums.ProductType.Interstitial.toString())) {
            return SSAEnums.ProductType.Interstitial;
        }
        if (str.equalsIgnoreCase(SSAEnums.ProductType.OfferWall.toString())) {
            return SSAEnums.ProductType.OfferWall;
        }
        return null;
    }

    public static void setTesterParameters(String str) {
        mUserGroup = str;
    }

    public static String getTesterParameters() {
        return mUserGroup;
    }

    public static JSONObject mergeJSONObjects(JSONObject jSONObject, JSONObject jSONObject2) throws Exception {
        JSONObject jSONObject3 = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        if (jSONObject != null) {
            jSONObject3 = new JSONObject(jSONObject.toString());
        }
        if (jSONObject2 != null) {
            jSONArray = jSONObject2.names();
        }
        if (jSONArray != null) {
            for (int i = 0; i < jSONArray.length(); i++) {
                String string = jSONArray.getString(i);
                jSONObject3.putOpt(string, jSONObject2.opt(string));
            }
        }
        return jSONObject3;
    }

    public static String flatMapToJsonAsString(Map<String, String> map) {
        JSONObject jSONObject = new JSONObject();
        if (map != null) {
            Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry next = it.next();
                try {
                    jSONObject.putOpt((String) next.getKey(), encodeString((String) next.getValue()));
                } catch (JSONException e) {
                    String str = TAG;
                    Logger.i(str, "flatMapToJsonAsStringfailed " + e.toString());
                }
                it.remove();
            }
        }
        return jSONObject.toString();
    }

    public static Map<String, String> mergeHashMaps(Map<String, String>[] mapArr) {
        HashMap hashMap = new HashMap();
        if (mapArr == null) {
            return hashMap;
        }
        for (Map<String, String> map : mapArr) {
            if (map != null) {
                hashMap.putAll(map);
            }
        }
        return hashMap;
    }

    public static String fetchDemandSourceId(SSAObj sSAObj) {
        return fetchDemandSourceId(sSAObj.getJsonObject());
    }

    public static String fetchDemandSourceId(JSONObject jSONObject) {
        String optString = jSONObject.optString("demandSourceId");
        return !TextUtils.isEmpty(optString) ? optString : jSONObject.optString("demandSourceName");
    }

    public static <T> T requireNonNull(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    public static String requireNonEmptyOrNull(String str, String str2) {
        if (str != null) {
            return str;
        }
        throw new NullPointerException(str2);
    }
}
