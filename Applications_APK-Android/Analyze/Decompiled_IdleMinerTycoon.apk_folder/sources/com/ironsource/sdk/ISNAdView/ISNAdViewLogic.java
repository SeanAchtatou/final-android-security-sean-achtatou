package com.ironsource.sdk.ISNAdView;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;
import org.json.JSONException;
import org.json.JSONObject;

class ISNAdViewLogic {
    private static Handler mUIThreadHandler;
    /* access modifiers changed from: private */
    public String TAG = ISNAdViewLogic.class.getSimpleName();
    private String[] commandsToHandleInAdView = {ISNAdViewConstants.HANDLE_GET_VIEW_VISIBILITY};
    /* access modifiers changed from: private */
    public JSONObject mAdViewConfiguration = null;
    /* access modifiers changed from: private */
    public ViewVisibilityParameters mAdViewVisibilityParameters = new ViewVisibilityParameters();
    /* access modifiers changed from: private */
    public ISNAdViewDelegate mDelegate;
    private WebView mWebView;
    private final String[] supportedCommandsFromController = {ISNAdViewConstants.LOAD_WITH_URL, ISNAdViewConstants.UPDATE_AD, ISNAdViewConstants.IS_EXTERNAL_AD_VIEW_INITIATED, ISNAdViewConstants.HANDLE_GET_VIEW_VISIBILITY, ISNAdViewConstants.SEND_MESSAGE};

    ISNAdViewLogic() {
    }

    /* access modifiers changed from: package-private */
    public void setControllerDelegate(ISNAdViewDelegate iSNAdViewDelegate) {
        this.mDelegate = iSNAdViewDelegate;
    }

    private Handler getUIThreadHandler() {
        try {
            if (mUIThreadHandler == null) {
                mUIThreadHandler = new Handler(Looper.getMainLooper());
            }
        } catch (Exception e) {
            Log.e(this.TAG, "Error while trying execute method getUIThreadHandler");
            e.printStackTrace();
        }
        return mUIThreadHandler;
    }

    /* access modifiers changed from: package-private */
    public void setAdViewWebView(WebView webView) {
        this.mWebView = webView;
    }

    /* access modifiers changed from: package-private */
    public void destroy() {
        this.mAdViewConfiguration = null;
        this.mDelegate = null;
        this.mAdViewVisibilityParameters = null;
        mUIThreadHandler = null;
    }

    /* access modifiers changed from: package-private */
    public JSONObject buildDataForLoadingAd(JSONObject jSONObject, String str) throws Exception {
        try {
            boolean isInReload = isInReload();
            if (this.mAdViewConfiguration == null) {
                this.mAdViewConfiguration = new JSONObject(jSONObject.toString());
            }
            this.mAdViewConfiguration.put(ISNAdViewConstants.EXTERNAL_AD_VIEW_ID, str);
            this.mAdViewConfiguration.put(ISNAdViewConstants.IS_IN_RELOAD, isInReload);
            return this.mAdViewConfiguration;
        } catch (Exception unused) {
            throw new Exception("ISNAdViewLogic | buildDataForLoadingAd | Could not build load parameters");
        }
    }

    private boolean isInReload() {
        return this.mAdViewConfiguration != null;
    }

    private void sendMessageToController(String str, JSONObject jSONObject) {
        if (this.mDelegate != null) {
            this.mDelegate.sendMessageToController(str, jSONObject);
        }
    }

    /* access modifiers changed from: package-private */
    public void sendErrorMessageToController(String str, String str2) {
        if (this.mDelegate != null) {
            this.mDelegate.sendErrorMessageToController(str, str2);
        }
    }

    /* access modifiers changed from: package-private */
    public void handleMessageFromController(String str, JSONObject jSONObject, String str2, String str3) {
        final String str4 = str;
        final String str5 = str3;
        final String str6 = str2;
        final JSONObject jSONObject2 = jSONObject;
        getUIThreadHandler().post(new Runnable() {
            public void run() {
                try {
                    if (!ISNAdViewLogic.this.canHandleCommandFromController(str4)) {
                        String str = "ISNAdViewLogic | handleMessageFromController | cannot handle command: " + str4;
                        Log.e(ISNAdViewLogic.this.TAG, str);
                        ISNAdViewLogic.this.mDelegate.sendErrorMessageToController(str5, str);
                    } else if (str4.equalsIgnoreCase(ISNAdViewConstants.IS_EXTERNAL_AD_VIEW_INITIATED)) {
                        ISNAdViewLogic.this.sendIsExternalAdViewInitiated(str6);
                    } else if (str4.equalsIgnoreCase(ISNAdViewConstants.HANDLE_GET_VIEW_VISIBILITY)) {
                        ISNAdViewLogic.this.sendHandleGetViewVisibilityParams(str6);
                    } else {
                        if (!str4.equalsIgnoreCase(ISNAdViewConstants.SEND_MESSAGE)) {
                            if (!str4.equalsIgnoreCase(ISNAdViewConstants.UPDATE_AD)) {
                                String str2 = "ISNAdViewLogic | handleMessageFromController | unhandled API request " + str4 + " " + jSONObject2.toString();
                                Log.e(ISNAdViewLogic.this.TAG, str2);
                                ISNAdViewLogic.this.mDelegate.sendErrorMessageToController(str5, str2);
                                return;
                            }
                        }
                        ISNAdViewLogic.this.sendMessageToAdunit(jSONObject2.getString("params"), str6, str5);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    String str3 = "ISNAdViewLogic | handleMessageFromController | Error while trying handle message: " + str4;
                    Log.e(ISNAdViewLogic.this.TAG, str3);
                    ISNAdViewLogic.this.mDelegate.sendErrorMessageToController(str5, str3);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean canHandleCommandFromController(String str) {
        boolean z = false;
        for (int i = 0; i < this.supportedCommandsFromController.length && !z; i++) {
            if (this.supportedCommandsFromController[i].equalsIgnoreCase(str)) {
                z = true;
            }
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void updateViewVisibilityParameters(String str, int i, boolean z) {
        this.mAdViewVisibilityParameters.updateViewVisibilityParameters(str, i, z);
        if (shouldReportVisibilityToController(str)) {
            reportAdContainerIsVisible();
        }
    }

    private boolean shouldReportVisibilityToController(String str) {
        if (Build.VERSION.SDK_INT <= 22) {
            return str.equalsIgnoreCase(ISNAdViewConstants.IS_WINDOW_VISIBLE_KEY);
        }
        return str.equalsIgnoreCase(ISNAdViewConstants.IS_VISIBLE_KEY);
    }

    private void reportAdContainerIsVisible() {
        if (this.mDelegate != null && this.mAdViewVisibilityParameters != null) {
            sendMessageToController(ISNAdViewConstants.CONTAINER_IMPRESSION_MESSAGE, buildParamsObjectForAdViewVisibility());
        }
    }

    /* access modifiers changed from: package-private */
    public void reportAdContainerWasRemoved() {
        if (this.mDelegate != null && this.mAdViewVisibilityParameters != null) {
            sendMessageToController(ISNAdViewConstants.CONTAINER_DESTRUCTION_MESSAGE, buildParamsObjectForAdViewVisibility());
        }
    }

    private JSONObject buildParamsObjectForAdViewVisibility() {
        return new JSONObject() {
            {
                try {
                    put(ISNAdViewConstants.CONFIGS, ISNAdViewLogic.this.extendConfigurationWithVisibilityParams(ISNAdViewLogic.this.mAdViewConfiguration, ISNAdViewLogic.this.mAdViewVisibilityParameters.collectVisibilityParameters()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /* access modifiers changed from: private */
    public JSONObject extendConfigurationWithVisibilityParams(JSONObject jSONObject, JSONObject jSONObject2) {
        try {
            JSONObject jSONObject3 = new JSONObject(jSONObject.toString());
            jSONObject3.put(ISNAdViewConstants.VISIBILITY_PARAMS_KEY, jSONObject2);
            return jSONObject3;
        } catch (JSONException e) {
            e.printStackTrace();
            return jSONObject;
        }
    }

    /* access modifiers changed from: private */
    public void sendIsExternalAdViewInitiated(String str) {
        try {
            boolean z = (this.mWebView == null || this.mWebView.getUrl() == null) ? false : true;
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(ISNAdViewConstants.IS_EXTERNAL_AD_VIEW_INITIATED, z);
            sendMessageToController(str, jSONObject);
        } catch (Exception e) {
            Log.e(this.TAG, "Error while trying execute method sendIsExternalAdViewInitiated");
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void sendHandleGetViewVisibilityParams(String str) {
        sendMessageToController(str, this.mAdViewVisibilityParameters.collectVisibilityParameters());
    }

    /* access modifiers changed from: private */
    public void sendMessageToAdunit(String str, String str2, String str3) {
        if (this.mWebView == null) {
            String str4 = "No external adunit attached to ISNAdView while trying to send message: " + str;
            Log.e(this.TAG, str4);
            this.mDelegate.sendErrorMessageToController(str3, str4);
            return;
        }
        try {
            new JSONObject(str);
        } catch (JSONException unused) {
            str = "\"" + str + "\"";
        }
        final String buildCommandForWebView = buildCommandForWebView(str);
        getUIThreadHandler().post(new Runnable() {
            public void run() {
                ISNAdViewLogic.this.injectJavaScriptIntoWebView(buildCommandForWebView);
            }
        });
    }

    private String buildCommandForWebView(String str) {
        return String.format(ISNAdViewConstants.ADUNIT_MESSAGE_FORMAT, str);
    }

    /* access modifiers changed from: private */
    public void injectJavaScriptIntoWebView(String str) {
        try {
            String str2 = "javascript:try{" + str + "}catch(e){console.log(\"JS exception: \" + JSON.stringify(e));}";
            if (Build.VERSION.SDK_INT >= 19) {
                this.mWebView.evaluateJavascript(str2, null);
            } else {
                this.mWebView.loadUrl(str2);
            }
        } catch (Throwable th) {
            Log.e(this.TAG, "injectJavaScriptIntoWebView | Error while trying inject JS into external adunit: " + str + "Android API level: " + Build.VERSION.SDK_INT);
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public void handleMessageFromWebView(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("method");
            if (TextUtils.isEmpty(optString) || !shouldHandleMessageInContainer(optString)) {
                sendMessageToController(ISNAdViewConstants.CONTAINER_SEND_MESSAGE, jSONObject);
            } else if (optString.equalsIgnoreCase(ISNAdViewConstants.HANDLE_GET_VIEW_VISIBILITY)) {
                sendHandleGetViewVisibilityParamsForWebView(jSONObject);
            }
        } catch (JSONException e) {
            String str2 = this.TAG;
            Log.e(str2, "ISNAdViewLogic | receiveMessageFromExternal | Error while trying handle message: " + str);
            e.printStackTrace();
        }
    }

    private boolean shouldHandleMessageInContainer(String str) {
        for (String equalsIgnoreCase : this.commandsToHandleInAdView) {
            if (equalsIgnoreCase.equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    private void sendHandleGetViewVisibilityParamsForWebView(JSONObject jSONObject) {
        sendMessageToAdunit(buildVisibilityMessageForAdunit(jSONObject).toString(), null, null);
    }

    private JSONObject buildVisibilityMessageForAdunit(JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.put("id", jSONObject.getString("id"));
            jSONObject2.put("data", this.mAdViewVisibilityParameters.collectVisibilityParameters());
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error while trying execute method buildVisibilityMessageForAdunit | params: " + jSONObject);
            e.printStackTrace();
        }
        return jSONObject2;
    }
}
