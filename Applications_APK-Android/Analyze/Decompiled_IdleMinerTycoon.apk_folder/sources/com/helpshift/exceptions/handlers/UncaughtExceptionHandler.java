package com.helpshift.exceptions.handlers;

import android.content.Context;
import android.util.Log;
import com.helpshift.logger.logmodels.ILogExtrasModel;
import com.helpshift.util.ErrorReportProvider;
import com.helpshift.util.HSLogger;
import java.lang.Thread;

public class UncaughtExceptionHandler {
    private static final CharSequence HELPSHIFT_BASE_PACKAGE_NAME = "com.helpshift";

    public static void init(final Context context) {
        final Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread thread, Throwable th) {
                if (UncaughtExceptionHandler.isCausedByHelpshift(th)) {
                    HSLogger.f("UncaughtExceptionHandler", "UNCAUGHT EXCEPTION ", th, (ILogExtrasModel[]) ErrorReportProvider.getErrorReportExtras(context, thread).toArray(new ILogExtrasModel[0]));
                }
                if (defaultUncaughtExceptionHandler != null) {
                    defaultUncaughtExceptionHandler.uncaughtException(thread, th);
                }
            }
        });
    }

    static boolean isCausedByHelpshift(Throwable th) {
        if (th == null) {
            return false;
        }
        return Log.getStackTraceString(th).contains(HELPSHIFT_BASE_PACKAGE_NAME);
    }
}
