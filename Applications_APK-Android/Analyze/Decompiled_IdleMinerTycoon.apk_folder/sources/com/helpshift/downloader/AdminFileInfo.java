package com.helpshift.downloader;

public class AdminFileInfo {
    public final String contentType;
    public final String fileName;
    public final boolean isSecureAttachment;
    public final String url;

    public AdminFileInfo(String str, String str2, String str3, boolean z) {
        this.url = str;
        this.fileName = str2;
        this.contentType = str3;
        this.isSecureAttachment = z;
    }
}
