package com.helpshift.network.util;

import com.helpshift.util.HelpshiftContext;
import com.helpshift.util.LocaleUtil;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;
import java.util.Map;

public class HeaderUtil {
    public static Map<String, String> getCommonHeaders() {
        HashMap hashMap = new HashMap();
        hashMap.put("Accept-Language", String.format("%s;q=1.0", LocaleUtil.getAcceptLanguageHeader()));
        hashMap.put(HttpRequest.HEADER_ACCEPT_ENCODING, HttpRequest.ENCODING_GZIP);
        hashMap.put("X-HS-V", "Helpshift-Android/" + HelpshiftContext.getPlatform().getDevice().getSDKVersion());
        return hashMap;
    }
}
