package com.helpshift.network;

public class HttpStatus {
    public static final int SC_FORBIDDEN = 403;
    public static final int SC_NOT_MODIFIED = 304;
    public static final int SC_OK = 200;
    public static final int SC_REQUEST_TOO_LONG = 413;
    public static final int SC_UNAUTHORIZED = 401;
    public static final int SC_UNPROCESSABLE_ENTITY = 422;
}
