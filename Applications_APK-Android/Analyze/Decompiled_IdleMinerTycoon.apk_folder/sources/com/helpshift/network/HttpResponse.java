package com.helpshift.network;

import com.helpshift.android.commons.downloader.HelpshiftSSLSocketFactory;
import java.util.ArrayList;
import java.util.List;

public class HttpResponse {
    private HttpEntity entity;
    private List<Header> headers;
    private HelpshiftSSLSocketFactory helpshiftSSLSocketFactory;
    private StatusLine statusLine;

    public HttpResponse(StatusLine statusLine2) {
        if (statusLine2 != null) {
            this.statusLine = statusLine2;
            this.headers = new ArrayList(16);
            return;
        }
        throw new IllegalArgumentException("Status line may not be null.");
    }

    public HttpEntity getEntity() {
        return this.entity;
    }

    public void setEntity(HttpEntity httpEntity) {
        this.entity = httpEntity;
    }

    public StatusLine getStatusLine() {
        return this.statusLine;
    }

    public void addHeader(Header header) {
        if (header != null) {
            this.headers.add(header);
        }
    }

    public Header[] getAllHeaders() {
        return (Header[]) this.headers.toArray(new Header[this.headers.size()]);
    }

    public HelpshiftSSLSocketFactory getHelpshiftSSLSocketFactory() {
        return this.helpshiftSSLSocketFactory;
    }

    public void setHelpshiftSSLSocketFactory(HelpshiftSSLSocketFactory helpshiftSSLSocketFactory2) {
        this.helpshiftSSLSocketFactory = helpshiftSSLSocketFactory2;
    }
}
