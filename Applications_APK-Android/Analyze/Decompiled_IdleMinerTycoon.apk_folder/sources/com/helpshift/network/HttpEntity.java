package com.helpshift.network;

import java.io.IOException;
import java.io.InputStream;

public class HttpEntity {
    public InputStream content;
    public long contentLength;

    public void consumeContent() throws IOException {
        if (this.content != null) {
            this.content.close();
        }
    }
}
