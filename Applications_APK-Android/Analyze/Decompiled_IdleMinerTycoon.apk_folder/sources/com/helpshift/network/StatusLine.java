package com.helpshift.network;

public class StatusLine {
    private int statusCode;

    public StatusLine(int i, String str) {
        this.statusCode = i;
    }

    public int getStatusCode() {
        return this.statusCode;
    }
}
