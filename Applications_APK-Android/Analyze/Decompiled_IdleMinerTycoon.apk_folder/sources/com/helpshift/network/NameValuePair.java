package com.helpshift.network;

public class NameValuePair {
    public final String name;
    public final String value;

    public NameValuePair(String str, String str2) {
        this.name = str;
        this.value = str2;
    }
}
