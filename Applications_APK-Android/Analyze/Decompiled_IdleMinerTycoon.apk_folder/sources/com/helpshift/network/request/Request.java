package com.helpshift.network.request;

import android.net.Uri;
import android.text.TextUtils;
import com.appsflyer.share.Constants;
import com.facebook.share.internal.ShareConstants;
import com.helpshift.common.domain.network.NetworkConstants;
import com.helpshift.exceptions.InstallException;
import com.helpshift.model.InfoModelFactory;
import com.helpshift.network.NameValuePair;
import com.helpshift.network.errors.NetworkError;
import com.helpshift.network.response.NetworkResponse;
import com.helpshift.network.response.Response;
import com.helpshift.network.response.ResponseParser;
import com.helpshift.network.util.HeaderUtil;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.util.SchemaUtil;
import com.helpshift.util.StringUtil;
import com.helpshift.util.TimeUtil;
import com.ironsource.sdk.constants.Constants;
import com.unity3d.ads.metadata.InAppPurchaseMetaData;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Request {
    private static final String TAG = "HS_Request";
    private static AtomicInteger sequenceGenerator = new AtomicInteger();
    private final Response.ErrorListener errorListener;
    private Response.Listener listener;
    public final int method;
    private Map<String, String> requestData;
    private boolean responseDelivered = false;
    private ResponseParser responseParser;
    private Integer sequence;
    public final String url;

    public interface Method {
        public static final int GET = 0;
        public static final int POST = 1;
    }

    /* access modifiers changed from: protected */
    public NetworkError parseNetworkError(NetworkError networkError) {
        return networkError;
    }

    public <T> Request(int i, String str, Map<String, String> map, Response.Listener<T> listener2, Response.ErrorListener errorListener2, ResponseParser<T> responseParser2) {
        this.method = i;
        this.url = sanitiseUrl(str);
        this.listener = listener2;
        this.errorListener = errorListener2;
        this.requestData = map;
        this.sequence = Integer.valueOf(sequenceGenerator.incrementAndGet());
        this.responseParser = responseParser2;
    }

    public Map<String, String> getRequestData() {
        return this.requestData;
    }

    private String sanitiseUrl(String str) {
        if (str.startsWith(Constants.URL_PATH_DELIMITER)) {
            return str;
        }
        return Constants.URL_PATH_DELIMITER + str;
    }

    public String getMethodString() {
        switch (this.method) {
            case 0:
                return HttpRequest.METHOD_GET;
            case 1:
                return HttpRequest.METHOD_POST;
            default:
                return "";
        }
    }

    public int getSequence() {
        if (this.sequence != null) {
            return this.sequence.intValue();
        }
        throw new IllegalStateException("getSequence called before setSequence");
    }

    public Map<String, String> getHeaders() {
        Map<String, String> commonHeaders = HeaderUtil.getCommonHeaders();
        if (this.method == 0) {
            String etag = InfoModelFactory.getInstance().sdkInfoModel.getEtag(this.url);
            if (!TextUtils.isEmpty(etag)) {
                commonHeaders.put(HttpRequest.HEADER_IF_NONE_MATCH, etag);
            }
        } else if (this.method == 1) {
            commonHeaders.put("Content-type", "application/x-www-form-urlencoded");
        }
        return commonHeaders;
    }

    private String getApiUri() {
        return "/api/lib/3" + this.url;
    }

    public String getFullUri() throws InstallException {
        if (InfoModelFactory.getInstance().appInfoModel.isInstalled()) {
            return NetworkConstants.scheme + InfoModelFactory.getInstance().appInfoModel.domainName + getApiUri();
        }
        throw new InstallException("Install information missing");
    }

    public URL getParsedURL() throws InstallException, MalformedURLException {
        String fullUri = getFullUri();
        if (this.method == 0) {
            fullUri = fullUri + "?" + encodeGetParameters(addAuth());
        }
        return new URL(fullUri);
    }

    private Map<String, String> addAuth() throws InstallException {
        HashMap hashMap;
        String stringUtil;
        String apiUri = getApiUri();
        if (this.requestData != null) {
            hashMap = new HashMap(this.requestData);
        } else {
            hashMap = new HashMap();
        }
        if (InfoModelFactory.getInstance().appInfoModel.isInstalled()) {
            hashMap.put("platform-id", InfoModelFactory.getInstance().appInfoModel.platformId);
            hashMap.put("method", getMethodString());
            hashMap.put(ShareConstants.MEDIA_URI, apiUri);
            String currentTimestamp = TimeUtil.getCurrentTimestamp();
            if (SchemaUtil.validateTimestamp(currentTimestamp)) {
                hashMap.put("timestamp", currentTimestamp);
            }
            ArrayList<String> arrayList = new ArrayList<>(hashMap.keySet());
            ArrayList arrayList2 = new ArrayList();
            Collections.sort(arrayList);
            for (String str : arrayList) {
                if (!str.equals("screenshot") && !str.equals("meta") && (stringUtil = StringUtil.toString(hashMap.get(str))) != null) {
                    arrayList2.add(str + Constants.RequestParameters.EQUAL + stringUtil);
                }
            }
            try {
                String str2 = InfoModelFactory.getInstance().appInfoModel.apiKey;
                if (InfoModelFactory.getInstance().appInfoModel.isInstalled()) {
                    hashMap.put(InAppPurchaseMetaData.KEY_SIGNATURE, HelpshiftContext.getCoreApi().getCryptoDM().getSignature(TextUtils.join(Constants.RequestParameters.AMPERSAND, arrayList2), str2));
                    hashMap.remove("method");
                    hashMap.remove(ShareConstants.MEDIA_URI);
                    return hashMap;
                }
                throw new InstallException("Install information missing");
            } catch (GeneralSecurityException unused) {
            }
        } else {
            throw new InstallException("appId Missing");
        }
    }

    private String encodeGetParameters(Map<String, String> map) {
        ArrayList arrayList = new ArrayList();
        for (String str : new ArrayList(map.keySet())) {
            arrayList.add(str + Constants.RequestParameters.EQUAL + Uri.encode(map.get(str)));
        }
        return TextUtils.join(Constants.RequestParameters.AMPERSAND, arrayList);
    }

    private List<NameValuePair> encodePostParameters(Map<String, String> map) {
        ArrayList<String> arrayList = new ArrayList<>(map.keySet());
        ArrayList arrayList2 = new ArrayList(arrayList.size());
        for (String str : arrayList) {
            String stringUtil = StringUtil.toString(map.get(str));
            if (stringUtil != null) {
                arrayList2.add(new NameValuePair(str, stringUtil));
            }
        }
        return arrayList2;
    }

    public String getPOSTParametersQuery() throws InstallException {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (NameValuePair next : encodePostParameters(addAuth())) {
            if (z) {
                z = false;
            } else {
                sb.append(Constants.RequestParameters.AMPERSAND);
            }
            try {
                sb.append(URLEncoder.encode(next.name, "UTF-8"));
                sb.append(Constants.RequestParameters.EQUAL);
                sb.append(URLEncoder.encode(next.value, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                HSLogger.w(TAG, "Exception Unsupported Encoding", e);
            }
        }
        return sb.toString();
    }

    public void markDelivered() {
        this.responseDelivered = true;
    }

    public boolean hasHadResponseDelivered() {
        return this.responseDelivered;
    }

    /* access modifiers changed from: protected */
    public <T> Response<T> parseNetworkResponse(NetworkResponse networkResponse) {
        return this.responseParser.parseResponse(networkResponse);
    }

    public <T> void deliverResponse(T t) {
        this.listener.onResponse(t, Integer.valueOf(getSequence()));
    }

    public void deliverError(NetworkError networkError) {
        if (this.errorListener != null) {
            this.errorListener.onErrorResponse(networkError, Integer.valueOf(getSequence()));
        }
    }

    public boolean isDoOutput() {
        return this.method == 1;
    }

    public String toString() {
        return this.url + " " + TAG + "  " + this.sequence;
    }
}
