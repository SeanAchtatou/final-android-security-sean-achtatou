package com.helpshift.network;

import com.helpshift.common.domain.network.NetworkErrorCodes;
import com.helpshift.network.errors.NetworkError;
import com.helpshift.util.HSLogger;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.TreeMap;

public class BasicNetwork implements Network {
    private static final String TAG = "Helpshift_BasicNetwork";
    protected final HttpStack httpStack;

    public BasicNetwork(HttpStack httpStack2) {
        this.httpStack = httpStack2;
    }

    protected static Map<String, String> convertHeaders(Header[] headerArr) {
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        for (Header header : headerArr) {
            treeMap.put(header.name, header.value);
        }
        return treeMap;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x009f, code lost:
        if (r7 == null) goto L_0x00d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        r0 = r7.entrySet().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ad, code lost:
        if (r0.hasNext() == false) goto L_0x00d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00af, code lost:
        r2 = r0.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00bf, code lost:
        if (r2.getKey().equals("HS-UEpoch") != false) goto L_0x00c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00c2, code lost:
        com.helpshift.model.InfoModelFactory.getInstance().sdkInfoModel.setServerTimeDelta(java.lang.Float.valueOf(com.helpshift.common.util.HSDateFormatSpec.calculateTimeDelta((java.lang.String) r2.getValue())));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00e0, code lost:
        throw new com.helpshift.network.errors.NetworkError(com.helpshift.common.domain.network.NetworkErrorCodes.TIMESTAMP_MISMATCH);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:104:0x0185 */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0145 A[SYNTHETIC, Splitter:B:87:0x0145] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0151 A[Catch:{ SocketTimeoutException -> 0x0185, MalformedURLException -> 0x0166, InstallException -> 0x015f, SecurityException -> 0x0157, IOException -> 0x0142, all -> 0x013f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.helpshift.network.response.NetworkResponse performRequest(com.helpshift.network.request.Request r12) throws com.helpshift.network.errors.NetworkError {
        /*
            r11 = this;
        L_0x0000:
            r0 = 0
            com.helpshift.network.HttpStack r1 = r11.httpStack     // Catch:{ SocketTimeoutException -> 0x0185, MalformedURLException -> 0x0166, InstallException -> 0x015f, SecurityException -> 0x0157, IOException -> 0x0142 }
            com.helpshift.network.HttpResponse r1 = r1.performRequest(r12)     // Catch:{ SocketTimeoutException -> 0x0185, MalformedURLException -> 0x0166, InstallException -> 0x015f, SecurityException -> 0x0157, IOException -> 0x0142 }
            com.helpshift.network.StatusLine r0 = r1.getStatusLine()     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            int r3 = r0.getStatusCode()     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            com.helpshift.network.Header[] r0 = r1.getAllHeaders()     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.util.Map r7 = convertHeaders(r0)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            if (r7 == 0) goto L_0x0034
            java.lang.String r0 = "ETag"
            boolean r0 = r7.containsKey(r0)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            if (r0 == 0) goto L_0x0034
            com.helpshift.model.InfoModelFactory r0 = com.helpshift.model.InfoModelFactory.getInstance()     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            com.helpshift.model.SdkInfoModel r0 = r0.sdkInfoModel     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.lang.String r2 = "ETag"
            java.lang.Object r2 = r7.get(r2)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.lang.String r4 = r12.url     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            r0.addEtag(r2, r4)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
        L_0x0034:
            r0 = 304(0x130, float:4.26E-43)
            if (r3 != r0) goto L_0x005a
            com.helpshift.network.response.NetworkResponse r0 = new com.helpshift.network.response.NetworkResponse     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            r5 = 304(0x130, float:4.26E-43)
            r6 = 0
            r8 = 1
            int r2 = r12.getSequence()     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r2)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            r4 = r0
            r4.<init>(r5, r6, r7, r8, r9)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            if (r1 == 0) goto L_0x0059
            com.helpshift.android.commons.downloader.HelpshiftSSLSocketFactory r12 = r1.getHelpshiftSSLSocketFactory()
            if (r12 == 0) goto L_0x0059
            com.helpshift.android.commons.downloader.HelpshiftSSLSocketFactory r12 = r1.getHelpshiftSSLSocketFactory()
            r12.closeSockets()
        L_0x0059:
            return r0
        L_0x005a:
            com.helpshift.network.HttpEntity r0 = r1.getEntity()     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            if (r0 == 0) goto L_0x006a
            com.helpshift.network.HttpEntity r0 = r1.getEntity()     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            byte[] r0 = r11.entityToBytes(r0)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
        L_0x0068:
            r4 = r0
            goto L_0x0072
        L_0x006a:
            int r0 = r12.method     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            if (r0 != 0) goto L_0x012b
            r0 = 0
            byte[] r0 = new byte[r0]     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            goto L_0x0068
        L_0x0072:
            r0 = 200(0xc8, float:2.8E-43)
            if (r3 < r0) goto L_0x009b
            r0 = 300(0x12c, float:4.2E-43)
            if (r3 > r0) goto L_0x009b
            com.helpshift.network.response.NetworkResponse r0 = new com.helpshift.network.response.NetworkResponse     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            r6 = 0
            int r2 = r12.getSequence()     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r2)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            r2 = r0
            r5 = r7
            r7 = r8
            r2.<init>(r3, r4, r5, r6, r7)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            if (r1 == 0) goto L_0x009a
            com.helpshift.android.commons.downloader.HelpshiftSSLSocketFactory r12 = r1.getHelpshiftSSLSocketFactory()
            if (r12 == 0) goto L_0x009a
            com.helpshift.android.commons.downloader.HelpshiftSSLSocketFactory r12 = r1.getHelpshiftSSLSocketFactory()
            r12.closeSockets()
        L_0x009a:
            return r0
        L_0x009b:
            r0 = 422(0x1a6, float:5.91E-43)
            if (r3 != r0) goto L_0x00e1
            if (r7 == 0) goto L_0x00d9
            java.util.Set r0 = r7.entrySet()     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
        L_0x00a9:
            boolean r2 = r0.hasNext()     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            if (r2 == 0) goto L_0x00d9
            java.lang.Object r2 = r0.next()     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.lang.Object r3 = r2.getKey()     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.lang.String r4 = "HS-UEpoch"
            boolean r3 = r3.equals(r4)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            if (r3 != 0) goto L_0x00c2
            goto L_0x00a9
        L_0x00c2:
            com.helpshift.model.InfoModelFactory r0 = com.helpshift.model.InfoModelFactory.getInstance()     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            com.helpshift.model.SdkInfoModel r0 = r0.sdkInfoModel     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.lang.Object r2 = r2.getValue()     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            float r2 = com.helpshift.common.util.HSDateFormatSpec.calculateTimeDelta(r2)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.lang.Float r2 = java.lang.Float.valueOf(r2)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            r0.setServerTimeDelta(r2)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
        L_0x00d9:
            com.helpshift.network.errors.NetworkError r0 = new com.helpshift.network.errors.NetworkError     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.lang.Integer r2 = com.helpshift.common.domain.network.NetworkErrorCodes.TIMESTAMP_MISMATCH     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            r0.<init>(r2)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            throw r0     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
        L_0x00e1:
            r0 = 413(0x19d, float:5.79E-43)
            if (r3 == r0) goto L_0x0123
            r0 = 403(0x193, float:5.65E-43)
            if (r3 == r0) goto L_0x011b
            r0 = 401(0x191, float:5.62E-43)
            if (r3 == r0) goto L_0x011b
            r0 = 400(0x190, float:5.6E-43)
            r2 = 500(0x1f4, float:7.0E-43)
            if (r3 < r0) goto L_0x0100
            if (r3 < r2) goto L_0x00f6
            goto L_0x0100
        L_0x00f6:
            com.helpshift.network.errors.NetworkError r0 = new com.helpshift.network.errors.NetworkError     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r3)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            r0.<init>(r2)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            throw r0     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
        L_0x0100:
            if (r3 >= r2) goto L_0x0113
            if (r1 == 0) goto L_0x0000
            com.helpshift.android.commons.downloader.HelpshiftSSLSocketFactory r0 = r1.getHelpshiftSSLSocketFactory()
            if (r0 == 0) goto L_0x0000
            com.helpshift.android.commons.downloader.HelpshiftSSLSocketFactory r0 = r1.getHelpshiftSSLSocketFactory()
            r0.closeSockets()
            goto L_0x0000
        L_0x0113:
            com.helpshift.network.errors.NetworkError r0 = new com.helpshift.network.errors.NetworkError     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.lang.Integer r2 = com.helpshift.common.domain.network.NetworkErrorCodes.SERVER_ERROR     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            r0.<init>(r2)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            throw r0     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
        L_0x011b:
            com.helpshift.network.errors.NetworkError r0 = new com.helpshift.network.errors.NetworkError     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.lang.Integer r2 = com.helpshift.common.domain.network.NetworkErrorCodes.UNAUTHORIZED_ACCESS     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            r0.<init>(r2)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            throw r0     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
        L_0x0123:
            com.helpshift.network.errors.NetworkError r0 = new com.helpshift.network.errors.NetworkError     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.lang.Integer r2 = com.helpshift.common.domain.network.NetworkErrorCodes.ENTITY_TOO_LARGE     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            r0.<init>(r2)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            throw r0     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
        L_0x012b:
            com.helpshift.network.errors.NetworkError r0 = new com.helpshift.network.errors.NetworkError     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            java.lang.Integer r2 = com.helpshift.common.domain.network.NetworkErrorCodes.CONTENT_NOT_FOUND     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            r0.<init>(r2)     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
            throw r0     // Catch:{ SocketTimeoutException -> 0x013d, MalformedURLException -> 0x013b, InstallException -> 0x0138, SecurityException -> 0x0136, IOException -> 0x0133 }
        L_0x0133:
            r12 = move-exception
            r0 = r1
            goto L_0x0143
        L_0x0136:
            r0 = r1
            goto L_0x0157
        L_0x0138:
            r12 = move-exception
            r0 = r1
            goto L_0x0160
        L_0x013b:
            r0 = move-exception
            goto L_0x016a
        L_0x013d:
            r0 = r1
            goto L_0x0185
        L_0x013f:
            r12 = move-exception
            r1 = r0
            goto L_0x018d
        L_0x0142:
            r12 = move-exception
        L_0x0143:
            if (r0 != 0) goto L_0x0151
            com.helpshift.network.errors.NetworkError r1 = new com.helpshift.network.errors.NetworkError     // Catch:{ all -> 0x013f }
            java.lang.Integer r2 = com.helpshift.common.domain.network.NetworkErrorCodes.NO_CONNECTION     // Catch:{ all -> 0x013f }
            java.lang.String r12 = r12.getMessage()     // Catch:{ all -> 0x013f }
            r1.<init>(r2, r12)     // Catch:{ all -> 0x013f }
            throw r1     // Catch:{ all -> 0x013f }
        L_0x0151:
            com.helpshift.network.errors.NetworkError r1 = new com.helpshift.network.errors.NetworkError     // Catch:{ all -> 0x013f }
            r1.<init>(r12)     // Catch:{ all -> 0x013f }
            throw r1     // Catch:{ all -> 0x013f }
        L_0x0157:
            com.helpshift.network.errors.NetworkError r12 = new com.helpshift.network.errors.NetworkError     // Catch:{ all -> 0x013f }
            java.lang.Integer r1 = com.helpshift.common.domain.network.NetworkErrorCodes.NO_CONNECTION     // Catch:{ all -> 0x013f }
            r12.<init>(r1)     // Catch:{ all -> 0x013f }
            throw r12     // Catch:{ all -> 0x013f }
        L_0x015f:
            r12 = move-exception
        L_0x0160:
            com.helpshift.network.errors.NetworkError r1 = new com.helpshift.network.errors.NetworkError     // Catch:{ all -> 0x013f }
            r1.<init>(r12)     // Catch:{ all -> 0x013f }
            throw r1     // Catch:{ all -> 0x013f }
        L_0x0166:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x016a:
            com.helpshift.network.errors.NetworkError r2 = new com.helpshift.network.errors.NetworkError     // Catch:{ all -> 0x0183 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0183 }
            r3.<init>()     // Catch:{ all -> 0x0183 }
            java.lang.String r4 = "Bad URL "
            r3.append(r4)     // Catch:{ all -> 0x0183 }
            java.lang.String r12 = r12.url     // Catch:{ all -> 0x0183 }
            r3.append(r12)     // Catch:{ all -> 0x0183 }
            java.lang.String r12 = r3.toString()     // Catch:{ all -> 0x0183 }
            r2.<init>(r12, r0)     // Catch:{ all -> 0x0183 }
            throw r2     // Catch:{ all -> 0x0183 }
        L_0x0183:
            r12 = move-exception
            goto L_0x018d
        L_0x0185:
            com.helpshift.network.errors.NetworkError r12 = new com.helpshift.network.errors.NetworkError     // Catch:{ all -> 0x013f }
            java.lang.Integer r1 = com.helpshift.common.domain.network.NetworkErrorCodes.REQUEST_TIMEOUT     // Catch:{ all -> 0x013f }
            r12.<init>(r1)     // Catch:{ all -> 0x013f }
            throw r12     // Catch:{ all -> 0x013f }
        L_0x018d:
            if (r1 == 0) goto L_0x019c
            com.helpshift.android.commons.downloader.HelpshiftSSLSocketFactory r0 = r1.getHelpshiftSSLSocketFactory()
            if (r0 == 0) goto L_0x019c
            com.helpshift.android.commons.downloader.HelpshiftSSLSocketFactory r0 = r1.getHelpshiftSSLSocketFactory()
            r0.closeSockets()
        L_0x019c:
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.network.BasicNetwork.performRequest(com.helpshift.network.request.Request):com.helpshift.network.response.NetworkResponse");
    }

    /* access modifiers changed from: protected */
    public byte[] entityToBytes(HttpEntity httpEntity) throws IOException, NetworkError {
        String str;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            InputStream inputStream = httpEntity.content;
            if (inputStream != null) {
                byte[] bArr = new byte[8192];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                return byteArrayOutputStream.toByteArray();
            }
            throw new NetworkError(NetworkErrorCodes.SERVER_ERROR);
        } finally {
            try {
                httpEntity.consumeContent();
            } catch (IOException e) {
                str = "Error occurred when calling consumingContent";
                HSLogger.w(TAG, str, e);
            }
            byteArrayOutputStream.close();
        }
    }
}
