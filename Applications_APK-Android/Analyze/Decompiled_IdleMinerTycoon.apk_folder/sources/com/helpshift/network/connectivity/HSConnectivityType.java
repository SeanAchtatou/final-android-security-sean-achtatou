package com.helpshift.network.connectivity;

public enum HSConnectivityType {
    UNKNOWN,
    WIFI,
    MOBILE_4G,
    MOBILE_2G
}
