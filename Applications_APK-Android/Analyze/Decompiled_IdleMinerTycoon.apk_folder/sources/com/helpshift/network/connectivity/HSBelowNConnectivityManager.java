package com.helpshift.network.connectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.telephony.TelephonyManager;
import com.helpshift.util.HSLogger;

class HSBelowNConnectivityManager extends BroadcastReceiver implements HSAndroidConnectivityManager {
    private static final String TAG = "Helpshift_BelowNConnMan";
    private Context context;
    private HSNetworkConnectivityCallback networkListener;

    HSBelowNConnectivityManager(Context context2) {
        this.context = context2;
    }

    public void onReceive(Context context2, Intent intent) {
        if (intent != null && intent.getExtras() != null && this.networkListener != null) {
            switch (getConnectivityStatus()) {
                case CONNECTED:
                    this.networkListener.onNetworkAvailable();
                    return;
                case NOT_CONNECTED:
                    this.networkListener.onNetworkUnavailable();
                    return;
                default:
                    return;
            }
        }
    }

    public void startListeningConnectivityChange(HSNetworkConnectivityCallback hSNetworkConnectivityCallback) {
        this.networkListener = hSNetworkConnectivityCallback;
        try {
            this.context.registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        } catch (Exception e) {
            HSLogger.e(TAG, "Exception while registering network receiver", e);
        }
    }

    public void stopListeningConnectivityChange() {
        try {
            this.context.unregisterReceiver(this);
        } catch (Exception e) {
            HSLogger.e(TAG, "Exception while unregistering network receiver", e);
        }
    }

    @NonNull
    public HSConnectivityStatus getConnectivityStatus() {
        HSConnectivityStatus hSConnectivityStatus = HSConnectivityStatus.UNKNOWN;
        ConnectivityManager connectivityManager = getConnectivityManager();
        if (connectivityManager == null) {
            return hSConnectivityStatus;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnectedOrConnecting()) {
            return HSConnectivityStatus.NOT_CONNECTED;
        }
        return HSConnectivityStatus.CONNECTED;
    }

    @NonNull
    public HSConnectivityType getConnectivityType() {
        TelephonyManager telephonyManager;
        ConnectivityManager connectivityManager = getConnectivityManager();
        if (connectivityManager == null) {
            return HSConnectivityType.UNKNOWN;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return HSConnectivityType.UNKNOWN;
        }
        HSConnectivityType hSConnectivityType = HSConnectivityType.UNKNOWN;
        int type = activeNetworkInfo.getType();
        if (type == 1) {
            return HSConnectivityType.WIFI;
        }
        if (type != 0 || (telephonyManager = getTelephonyManager()) == null) {
            return hSConnectivityType;
        }
        int networkType = telephonyManager.getNetworkType();
        if (networkType == 13 || networkType == 15) {
            return HSConnectivityType.MOBILE_4G;
        }
        switch (networkType) {
            case 1:
            case 2:
                return HSConnectivityType.MOBILE_2G;
            default:
                return hSConnectivityType;
        }
    }

    private ConnectivityManager getConnectivityManager() {
        try {
            return (ConnectivityManager) this.context.getSystemService("connectivity");
        } catch (Exception e) {
            HSLogger.e(TAG, "Exception while getting connectivity manager", e);
            return null;
        }
    }

    private TelephonyManager getTelephonyManager() {
        try {
            return (TelephonyManager) this.context.getSystemService("phone");
        } catch (Exception e) {
            HSLogger.e(TAG, "Exception while getting telephony manager", e);
            return null;
        }
    }
}
