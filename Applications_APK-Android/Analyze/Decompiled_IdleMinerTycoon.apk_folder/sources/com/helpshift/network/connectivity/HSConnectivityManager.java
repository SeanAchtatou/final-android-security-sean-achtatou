package com.helpshift.network.connectivity;

import android.content.Context;
import android.support.annotation.NonNull;
import com.helpshift.util.HelpshiftContext;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

public class HSConnectivityManager implements HSNetworkConnectivityCallback {
    private static HSConnectivityManager instance;
    private Set<HSNetworkConnectivityCallback> connectivityCallbacks = Collections.synchronizedSet(new LinkedHashSet());
    private Context context = HelpshiftContext.getApplicationContext();
    private HSAndroidConnectivityManager hsAndroidConnectivityManager;
    private HSAndroidConnectivityManagerProvider hsAndroidConnectivityManagerProvider = new HSAndroidConnectivityManagerProvider();

    private HSConnectivityManager() {
    }

    public static HSConnectivityManager getInstance() {
        if (instance == null) {
            instance = new HSConnectivityManager();
        }
        return instance;
    }

    public synchronized void registerNetworkConnectivityListener(@NonNull HSNetworkConnectivityCallback hSNetworkConnectivityCallback) {
        boolean isEmpty = this.connectivityCallbacks.isEmpty();
        this.connectivityCallbacks.add(hSNetworkConnectivityCallback);
        if (!isEmpty) {
            switch (this.hsAndroidConnectivityManager.getConnectivityStatus()) {
                case CONNECTED:
                    hSNetworkConnectivityCallback.onNetworkAvailable();
                    break;
                case NOT_CONNECTED:
                    hSNetworkConnectivityCallback.onNetworkUnavailable();
                    break;
            }
        } else {
            startListenNetworkStatus();
        }
    }

    public synchronized void unregisterNetworkConnectivityListener(@NonNull HSNetworkConnectivityCallback hSNetworkConnectivityCallback) {
        this.connectivityCallbacks.remove(hSNetworkConnectivityCallback);
        if (this.connectivityCallbacks.isEmpty()) {
            stopListenNetworkStatus();
        }
    }

    private void startListenNetworkStatus() {
        if (this.hsAndroidConnectivityManager == null) {
            this.hsAndroidConnectivityManager = this.hsAndroidConnectivityManagerProvider.getOSConnectivityManager(this.context);
        }
        this.hsAndroidConnectivityManager.startListeningConnectivityChange(this);
    }

    private void stopListenNetworkStatus() {
        if (this.hsAndroidConnectivityManager != null) {
            this.hsAndroidConnectivityManager.stopListeningConnectivityChange();
            this.hsAndroidConnectivityManager = null;
        }
    }

    public void onNetworkAvailable() {
        if (!this.connectivityCallbacks.isEmpty()) {
            for (HSNetworkConnectivityCallback onNetworkAvailable : this.connectivityCallbacks) {
                onNetworkAvailable.onNetworkAvailable();
            }
        }
    }

    public void onNetworkUnavailable() {
        if (!this.connectivityCallbacks.isEmpty()) {
            for (HSNetworkConnectivityCallback onNetworkUnavailable : this.connectivityCallbacks) {
                onNetworkUnavailable.onNetworkUnavailable();
            }
        }
    }

    public HSConnectivityType getConnectivityType() {
        if (this.hsAndroidConnectivityManager == null) {
            this.hsAndroidConnectivityManager = this.hsAndroidConnectivityManagerProvider.getOSConnectivityManager(this.context);
        }
        return this.hsAndroidConnectivityManager.getConnectivityType();
    }
}
