package com.helpshift.network.connectivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.telephony.TelephonyManager;
import com.helpshift.util.HSLogger;

@RequiresApi(api = 24)
class HSOnAndAboveNConnectivityManager extends ConnectivityManager.NetworkCallback implements HSAndroidConnectivityManager {
    private static final String TAG = "Helpshift_AboveNConnMan";
    private Context context;
    private HSNetworkConnectivityCallback networkListener;

    HSOnAndAboveNConnectivityManager(Context context2) {
        this.context = context2;
    }

    @RequiresApi(api = 24)
    public void startListeningConnectivityChange(HSNetworkConnectivityCallback hSNetworkConnectivityCallback) {
        this.networkListener = hSNetworkConnectivityCallback;
        ConnectivityManager connectivityManager = getConnectivityManager();
        if (connectivityManager != null) {
            try {
                connectivityManager.registerDefaultNetworkCallback(this);
            } catch (Exception e) {
                HSLogger.e(TAG, "Exception while registering network callback", e);
            }
        }
        if (getConnectivityStatus() == HSConnectivityStatus.NOT_CONNECTED) {
            hSNetworkConnectivityCallback.onNetworkUnavailable();
        }
    }

    @RequiresApi(api = 24)
    public void stopListeningConnectivityChange() {
        ConnectivityManager connectivityManager = getConnectivityManager();
        if (connectivityManager != null) {
            try {
                connectivityManager.unregisterNetworkCallback(this);
            } catch (Exception e) {
                HSLogger.e(TAG, "Exception while unregistering network callback", e);
            }
        }
        this.networkListener = null;
    }

    @RequiresApi(api = 24)
    @NonNull
    public HSConnectivityStatus getConnectivityStatus() {
        HSConnectivityStatus hSConnectivityStatus = HSConnectivityStatus.UNKNOWN;
        ConnectivityManager connectivityManager = getConnectivityManager();
        if (connectivityManager == null) {
            return hSConnectivityStatus;
        }
        if (connectivityManager.getActiveNetwork() != null) {
            return HSConnectivityStatus.CONNECTED;
        }
        return HSConnectivityStatus.NOT_CONNECTED;
    }

    @NonNull
    public HSConnectivityType getConnectivityType() {
        TelephonyManager telephonyManager;
        ConnectivityManager connectivityManager = getConnectivityManager();
        if (connectivityManager == null) {
            return HSConnectivityType.UNKNOWN;
        }
        Network activeNetwork = connectivityManager.getActiveNetwork();
        if (activeNetwork == null) {
            return HSConnectivityType.UNKNOWN;
        }
        NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);
        if (networkCapabilities == null) {
            return HSConnectivityType.UNKNOWN;
        }
        HSConnectivityType hSConnectivityType = HSConnectivityType.UNKNOWN;
        if (networkCapabilities.hasTransport(1)) {
            return HSConnectivityType.WIFI;
        }
        if (!networkCapabilities.hasTransport(0) || (telephonyManager = getTelephonyManager()) == null) {
            return hSConnectivityType;
        }
        int networkType = telephonyManager.getNetworkType();
        if (networkType == 13 || networkType == 15) {
            return HSConnectivityType.MOBILE_4G;
        }
        switch (networkType) {
            case 1:
            case 2:
                return HSConnectivityType.MOBILE_2G;
            default:
                return hSConnectivityType;
        }
    }

    public void onAvailable(@NonNull Network network) {
        if (this.networkListener != null) {
            this.networkListener.onNetworkAvailable();
        }
    }

    public void onLost(@NonNull Network network) {
        if (this.networkListener != null) {
            this.networkListener.onNetworkUnavailable();
        }
    }

    public void onUnavailable() {
        if (this.networkListener != null) {
            this.networkListener.onNetworkUnavailable();
        }
    }

    private ConnectivityManager getConnectivityManager() {
        try {
            return (ConnectivityManager) this.context.getSystemService("connectivity");
        } catch (Exception e) {
            HSLogger.e(TAG, "Exception while getting connectivity manager", e);
            return null;
        }
    }

    private TelephonyManager getTelephonyManager() {
        try {
            return (TelephonyManager) this.context.getSystemService("phone");
        } catch (Exception e) {
            HSLogger.e(TAG, "Exception while getting telephony manager", e);
            return null;
        }
    }
}
