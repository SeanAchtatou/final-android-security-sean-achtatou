package com.helpshift.network;

public class Header {
    public final String name;
    public final String value;

    public Header(String str, String str2) {
        this.name = str;
        this.value = str2;
    }
}
