package com.helpshift.network;

import com.helpshift.network.request.Request;

public interface NetworkDataProvider {
    Request getRequest();

    Request getRequestWithFullData();

    void setBatchSize(Integer num);
}
