package com.helpshift.network.response;

import com.helpshift.network.errors.NetworkError;

public class Response<T> {
    public final NetworkError error;
    public final T result;

    public interface ErrorListener {
        void onErrorResponse(NetworkError networkError, Integer num);
    }

    public interface Listener<T> {
        void onResponse(T t, Integer num);
    }

    private Response(T t, Integer num) {
        this.result = t;
        this.error = null;
    }

    private Response(NetworkError networkError, Integer num) {
        this.result = null;
        this.error = networkError;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.network.response.Response.<init>(java.lang.Object, java.lang.Integer):void
     arg types: [T, java.lang.Integer]
     candidates:
      com.helpshift.network.response.Response.<init>(com.helpshift.network.errors.NetworkError, java.lang.Integer):void
      com.helpshift.network.response.Response.<init>(java.lang.Object, java.lang.Integer):void */
    public static <T> Response<T> success(T t, Integer num) {
        return new Response<>((Object) t, num);
    }

    public static <T> Response<T> error(NetworkError networkError, Integer num) {
        return new Response<>(networkError, num);
    }

    public boolean isSuccess() {
        return this.error == null;
    }
}
