package com.helpshift.network.response;

import android.os.Handler;
import com.helpshift.network.errors.NetworkError;
import com.helpshift.network.request.Request;
import java.util.concurrent.Executor;

public class ExecutorDelivery implements ResponseDelivery {
    private final Executor responsePoster;

    public ExecutorDelivery(final Handler handler) {
        this.responsePoster = new Executor() {
            public void execute(Runnable runnable) {
                handler.post(runnable);
            }
        };
    }

    public void postResponse(Request request, Response<?> response) {
        this.responsePoster.execute(new ResponseDeliveryRunnable(request, response, null));
    }

    public void postError(Request request, NetworkError networkError) {
        this.responsePoster.execute(new ResponseDeliveryRunnable(request, Response.error(networkError, Integer.valueOf(request.getSequence())), null));
    }

    private class ResponseDeliveryRunnable implements Runnable {
        private final Request request;
        private final Response response;
        private final Runnable runnable;

        public ResponseDeliveryRunnable(Request request2, Response response2, Runnable runnable2) {
            this.request = request2;
            this.response = response2;
            this.runnable = runnable2;
        }

        public void run() {
            try {
                if (this.response.isSuccess()) {
                    this.request.deliverResponse(this.response.result);
                } else {
                    this.request.deliverError(this.response.error);
                }
            } catch (Throwable unused) {
            }
            this.request.markDelivered();
            if (this.runnable != null) {
                this.runnable.run();
            }
        }
    }
}
