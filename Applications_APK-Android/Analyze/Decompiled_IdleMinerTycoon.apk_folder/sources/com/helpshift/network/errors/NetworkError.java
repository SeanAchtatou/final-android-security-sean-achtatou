package com.helpshift.network.errors;

import com.helpshift.common.domain.network.NetworkErrorCodes;

public class NetworkError extends Exception {
    private Integer reason = NetworkErrorCodes.GENERIC_NETWORK_ERROR;

    public NetworkError(Integer num) {
        this.reason = num;
    }

    public NetworkError(Integer num, String str) {
        super(str);
        this.reason = num;
    }

    public NetworkError(String str, Throwable th) {
        super(str, th);
    }

    public NetworkError(Integer num, Throwable th) {
        super(th);
        this.reason = num;
    }

    public NetworkError(Throwable th) {
        super(th);
    }

    public Integer getReason() {
        return this.reason;
    }
}
