package com.helpshift.network;

import android.os.Build;
import com.helpshift.android.commons.downloader.HelpshiftSSLSocketFactory;
import com.helpshift.common.domain.network.NetworkConstants;
import com.helpshift.exceptions.InstallException;
import com.helpshift.network.request.Request;
import com.helpshift.util.HelpshiftContext;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.HttpsURLConnection;

public class HurlStack implements HttpStack {
    private static HttpEntity entityFromConnection(HttpURLConnection httpURLConnection, boolean z) {
        InputStream inputStream;
        HttpEntity httpEntity = new HttpEntity();
        if (z) {
            try {
                inputStream = new GZIPInputStream(new BufferedInputStream(httpURLConnection.getInputStream()));
            } catch (IOException unused) {
                inputStream = httpURLConnection.getErrorStream();
            }
        } else {
            inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
        }
        httpEntity.content = inputStream;
        httpEntity.contentLength = (long) httpURLConnection.getContentLength();
        return httpEntity;
    }

    public HttpResponse performRequest(Request request) throws IOException, InstallException {
        HttpURLConnection httpURLConnection;
        HttpResponse httpResponse;
        HelpshiftSSLSocketFactory helpshiftSSLSocketFactory;
        URL parsedURL = request.getParsedURL();
        if ("https://".equals(NetworkConstants.scheme)) {
            httpURLConnection = (HttpsURLConnection) parsedURL.openConnection();
            fixSSLSocketProtocols((HttpsURLConnection) httpURLConnection);
        } else {
            httpURLConnection = (HttpURLConnection) parsedURL.openConnection();
        }
        try {
            configureConnectionForRequest(httpURLConnection, request);
            if (httpURLConnection.getResponseCode() != -1) {
                httpResponse = new HttpResponse(new StatusLine(httpURLConnection.getResponseCode(), httpURLConnection.getResponseMessage()));
                try {
                    boolean z = false;
                    for (Map.Entry next : httpURLConnection.getHeaderFields().entrySet()) {
                        if (next.getKey() != null) {
                            if (((String) next.getKey()).equals(HttpRequest.HEADER_CONTENT_ENCODING) && ((String) ((List) next.getValue()).get(0)).equalsIgnoreCase(HttpRequest.ENCODING_GZIP)) {
                                z = true;
                            }
                            httpResponse.addHeader(new Header((String) next.getKey(), (String) ((List) next.getValue()).get(0)));
                        }
                    }
                    httpResponse.setEntity(entityFromConnection(httpURLConnection, z));
                    httpResponse.setHelpshiftSSLSocketFactory(getHelpshiftSSLSocketFactory(httpURLConnection));
                    return httpResponse;
                } catch (Throwable th) {
                    th = th;
                    helpshiftSSLSocketFactory.closeSockets();
                    throw th;
                }
            } else {
                throw new IOException("Could not retrieve response code from HttpUrlConnection.");
            }
        } catch (Throwable th2) {
            th = th2;
            httpResponse = null;
            if (httpResponse == null && (helpshiftSSLSocketFactory = getHelpshiftSSLSocketFactory(httpURLConnection)) != null) {
                helpshiftSSLSocketFactory.closeSockets();
            }
            throw th;
        }
    }

    private HelpshiftSSLSocketFactory getHelpshiftSSLSocketFactory(HttpURLConnection httpURLConnection) {
        if (Build.VERSION.SDK_INT < 16 || Build.VERSION.SDK_INT > 19 || !(httpURLConnection instanceof HttpsURLConnection)) {
            return null;
        }
        HttpsURLConnection httpsURLConnection = (HttpsURLConnection) httpURLConnection;
        if (httpsURLConnection.getSSLSocketFactory() instanceof HelpshiftSSLSocketFactory) {
            return (HelpshiftSSLSocketFactory) httpsURLConnection.getSSLSocketFactory();
        }
        return null;
    }

    private void fixSSLSocketProtocols(HttpsURLConnection httpsURLConnection) {
        if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT <= 19) {
            ArrayList arrayList = new ArrayList();
            arrayList.add("TLSv1.2");
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add("SSLv3");
            httpsURLConnection.setSSLSocketFactory(new HelpshiftSSLSocketFactory(httpsURLConnection.getSSLSocketFactory(), arrayList, arrayList2));
        }
    }

    private void configureConnectionForRequest(HttpURLConnection httpURLConnection, Request request) throws InstallException, IOException {
        Map<String, String> headers = request.getHeaders();
        for (String next : headers.keySet()) {
            httpURLConnection.addRequestProperty(next, headers.get(next));
        }
        httpURLConnection.setConnectTimeout(5000);
        httpURLConnection.setReadTimeout(5000);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setRequestProperty("User-Agent", String.format(Locale.ENGLISH, "Helpshift-%s/%s/%s", HelpshiftContext.getPlatform().getDevice().getPlatformName(), HelpshiftContext.getPlatform().getDevice().getSDKVersion(), HelpshiftContext.getPlatform().getDevice().getOSVersion()));
        httpURLConnection.setRequestMethod(request.getMethodString());
        if (request.method == 1) {
            httpURLConnection.setDoOutput(request.isDoOutput());
            httpURLConnection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
            OutputStream outputStream = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            bufferedWriter.write(request.getPOSTParametersQuery());
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();
        }
    }
}
