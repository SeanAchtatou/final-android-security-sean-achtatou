package com.helpshift.specifications;

import com.helpshift.util.ErrorReportProvider;
import java.util.concurrent.TimeUnit;

public class DecayingIntervalSyncSpecification implements SyncSpecification {
    private final String dataType;
    private long elapsedTimeThreshold;
    private long maxTimeThresholdLimit = ErrorReportProvider.BATCH_TIME;

    public DecayingIntervalSyncSpecification(int i, TimeUnit timeUnit, String str) {
        this.elapsedTimeThreshold = TimeUnit.MILLISECONDS.convert((long) i, timeUnit);
        this.dataType = str;
    }

    public String getDataType() {
        return this.dataType;
    }

    public boolean isSatisfied(int i, long j) {
        return i > 0 && Math.abs(j) > this.elapsedTimeThreshold;
    }

    public void decayElapsedTimeThreshold() {
        double d = (double) this.elapsedTimeThreshold;
        Double.isNaN(d);
        this.elapsedTimeThreshold = (long) (d * 1.618d);
        if (this.elapsedTimeThreshold > this.maxTimeThresholdLimit) {
            this.elapsedTimeThreshold = this.maxTimeThresholdLimit;
        }
    }
}
