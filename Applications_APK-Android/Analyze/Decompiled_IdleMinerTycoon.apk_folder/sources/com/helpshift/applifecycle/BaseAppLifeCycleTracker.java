package com.helpshift.applifecycle;

import android.content.Context;
import android.support.annotation.NonNull;

public abstract class BaseAppLifeCycleTracker {
    private Context context;
    private HSAppLifeCycleListener lifeCycleListener;

    public abstract boolean isAppInForeground();

    public abstract void onManualAppBackgroundAPI();

    public abstract void onManualAppForegroundAPI();

    BaseAppLifeCycleTracker(Context context2) {
        this.context = context2;
    }

    /* access modifiers changed from: package-private */
    public void registerAppLifeCycleListener(@NonNull HSAppLifeCycleListener hSAppLifeCycleListener) {
        this.lifeCycleListener = hSAppLifeCycleListener;
    }

    /* access modifiers changed from: package-private */
    public void unregisterAppLifeCycleListener() {
        this.lifeCycleListener = null;
    }

    /* access modifiers changed from: package-private */
    public void notifyAppForeground() {
        if (this.lifeCycleListener != null) {
            this.lifeCycleListener.onAppForeground(this.context);
        }
    }

    /* access modifiers changed from: package-private */
    public void notifyAppBackground() {
        if (this.lifeCycleListener != null) {
            this.lifeCycleListener.onAppBackground(this.context);
        }
    }
}
