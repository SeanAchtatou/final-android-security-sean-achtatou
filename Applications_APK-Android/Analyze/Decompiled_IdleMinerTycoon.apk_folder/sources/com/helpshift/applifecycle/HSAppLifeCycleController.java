package com.helpshift.applifecycle;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import com.helpshift.util.concurrent.ApiExecutorFactory;
import java.util.ArrayList;
import java.util.List;

public class HSAppLifeCycleController implements HSAppLifeCycleListener {
    private static HSAppLifeCycleController instance = new HSAppLifeCycleController();
    /* access modifiers changed from: private */
    public static final Object lock = new Object();
    /* access modifiers changed from: private */
    public List<HSAppLifeCycleListener> appLifeCycleListeners = new ArrayList();
    private BaseAppLifeCycleTracker lifeCycleTracker;

    private HSAppLifeCycleController() {
    }

    public static HSAppLifeCycleController getInstance() {
        return instance;
    }

    public synchronized void init(Application application, boolean z) {
        if (this.lifeCycleTracker == null) {
            if (z) {
                this.lifeCycleTracker = new ManualAppLifeCycleTracker(application);
            } else {
                this.lifeCycleTracker = new DefaultAppLifeCycleTracker(application);
            }
            this.lifeCycleTracker.registerAppLifeCycleListener(this);
        }
    }

    public void registerAppLifeCycleListener(@NonNull HSAppLifeCycleListener hSAppLifeCycleListener) {
        synchronized (lock) {
            this.appLifeCycleListeners.add(hSAppLifeCycleListener);
        }
    }

    public void onManualAppForegroundAPI() {
        if (this.lifeCycleTracker != null) {
            this.lifeCycleTracker.onManualAppForegroundAPI();
        }
    }

    public void onManualAppBackgroundAPI() {
        if (this.lifeCycleTracker != null) {
            this.lifeCycleTracker.onManualAppBackgroundAPI();
        }
    }

    public boolean isAppInForeground() {
        if (this.lifeCycleTracker == null) {
            return false;
        }
        return this.lifeCycleTracker.isAppInForeground();
    }

    public void onAppForeground(final Context context) {
        ApiExecutorFactory.getHandlerExecutor().runAsync(new Runnable() {
            public void run() {
                synchronized (HSAppLifeCycleController.lock) {
                    for (HSAppLifeCycleListener onAppForeground : HSAppLifeCycleController.this.appLifeCycleListeners) {
                        onAppForeground.onAppForeground(context);
                    }
                }
            }
        });
    }

    public void onAppBackground(final Context context) {
        ApiExecutorFactory.getHandlerExecutor().runAsync(new Runnable() {
            public void run() {
                synchronized (HSAppLifeCycleController.lock) {
                    for (HSAppLifeCycleListener onAppBackground : HSAppLifeCycleController.this.appLifeCycleListeners) {
                        onAppBackground.onAppBackground(context);
                    }
                }
            }
        });
    }
}
