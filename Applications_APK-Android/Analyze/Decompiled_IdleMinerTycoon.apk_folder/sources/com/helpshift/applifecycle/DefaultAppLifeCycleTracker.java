package com.helpshift.applifecycle;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import com.helpshift.PluginEventBridge;
import com.helpshift.util.HSLogger;

class DefaultAppLifeCycleTracker extends BaseAppLifeCycleTracker implements Application.ActivityLifecycleCallbacks {
    private static String TAG = "DALCTracker";
    private boolean isAppForeground;
    private boolean isConfigurationChanged = false;
    private int started;
    private int stopped;

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
    }

    public void onActivityResumed(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    DefaultAppLifeCycleTracker(Application application) {
        super(application);
        application.unregisterActivityLifecycleCallbacks(this);
        application.registerActivityLifecycleCallbacks(this);
        if (PluginEventBridge.shouldCallFirstForegroundEvent()) {
            this.started++;
            this.isAppForeground = true;
        }
    }

    public boolean isAppInForeground() {
        return this.started > this.stopped;
    }

    public void onManualAppForegroundAPI() {
        HSLogger.e(TAG, "Install API is called with manualLifeCycleTracking config as false, Ignore this event");
    }

    public void onManualAppBackgroundAPI() {
        HSLogger.e(TAG, "Install API is called with manualLifeCycleTracking config as false, Ignore this event");
    }

    public void onActivityStarted(Activity activity) {
        this.started++;
        if (!this.isConfigurationChanged) {
            if (!this.isAppForeground) {
                notifyAppForeground();
            }
            this.isAppForeground = true;
        }
        this.isConfigurationChanged = false;
    }

    public void onActivityStopped(Activity activity) {
        boolean z = true;
        this.stopped++;
        if (activity == null || !activity.isChangingConfigurations()) {
            z = false;
        }
        this.isConfigurationChanged = z;
        if (!this.isConfigurationChanged && this.started == this.stopped) {
            this.isAppForeground = false;
            notifyAppBackground();
        }
    }
}
