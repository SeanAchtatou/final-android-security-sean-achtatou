package com.helpshift.applifecycle;

import android.content.Context;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftContext;

class ManualAppLifeCycleTracker extends BaseAppLifeCycleTracker {
    private static String TAG = "MALCTracker";
    private boolean isAppInForeground = false;

    ManualAppLifeCycleTracker(Context context) {
        super(context);
    }

    public boolean isAppInForeground() {
        return this.isAppInForeground;
    }

    public void onManualAppForegroundAPI() {
        if (this.isAppInForeground) {
            HSLogger.d(TAG, "Application is already in foreground, so ignore this event");
        } else if (HelpshiftContext.installCallSuccessful.get()) {
            this.isAppInForeground = true;
            notifyAppForeground();
        } else {
            HSLogger.e(TAG, "onManualAppForegroundAPI is called without calling install API");
        }
    }

    public void onManualAppBackgroundAPI() {
        if (!this.isAppInForeground) {
            HSLogger.d(TAG, "Application is already in background, so ignore this event");
        } else if (HelpshiftContext.installCallSuccessful.get()) {
            this.isAppInForeground = false;
            notifyAppBackground();
        } else {
            HSLogger.e(TAG, "onManualAppBackgroundAPI is called without calling install API");
        }
    }
}
