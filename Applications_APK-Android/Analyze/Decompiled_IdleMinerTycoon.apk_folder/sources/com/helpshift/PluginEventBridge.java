package com.helpshift;

import android.app.PendingIntent;
import android.content.Context;

public final class PluginEventBridge {
    private static PluginEventsAPI pluginEventsAPI;

    public interface PluginEventsAPI {
        PendingIntent getPendingIntentForNotification(Context context, PendingIntent pendingIntent);

        void sendMessage(String str, String str2);

        boolean shouldCallFirstForegroundEvent();
    }

    public static void setPluginEventsAPI(PluginEventsAPI pluginEventsAPI2) {
        pluginEventsAPI = pluginEventsAPI2;
    }

    public static void sendMessage(String str, String str2) {
        if (pluginEventsAPI != null) {
            pluginEventsAPI.sendMessage(str, str2);
        }
    }

    public static PendingIntent getPendingIntentForNotification(Context context, PendingIntent pendingIntent) {
        return pluginEventsAPI != null ? pluginEventsAPI.getPendingIntentForNotification(context, pendingIntent) : pendingIntent;
    }

    public static boolean shouldCallFirstForegroundEvent() {
        if (pluginEventsAPI != null) {
            return pluginEventsAPI.shouldCallFirstForegroundEvent();
        }
        return false;
    }
}
