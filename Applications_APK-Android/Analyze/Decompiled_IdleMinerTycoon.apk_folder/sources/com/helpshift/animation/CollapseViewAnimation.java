package com.helpshift.animation;

import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

public class CollapseViewAnimation extends Animation {
    private LinearLayout view;
    private int viewHeight;

    public boolean willChangeBounds() {
        return true;
    }

    public CollapseViewAnimation(LinearLayout linearLayout) {
        this.view = linearLayout;
        this.viewHeight = linearLayout.getHeight();
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f, Transformation transformation) {
        this.view.getLayoutParams().height = (int) (((float) this.viewHeight) * (1.0f - f));
        this.view.requestLayout();
    }
}
