package com.helpshift.unityproxy;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.helpshift.unityproxy";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 1;
    public static final String VERSION_NAME = "1.0";
}
