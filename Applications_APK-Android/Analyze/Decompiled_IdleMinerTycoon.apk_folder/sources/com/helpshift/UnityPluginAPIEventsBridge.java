package com.helpshift;

import android.app.PendingIntent;
import android.content.Context;
import com.helpshift.PluginEventBridge;
import com.helpshift.storage.HelpshiftUnityStorage;
import com.helpshift.util.IntentProvider;
import com.helpshift.util.UnityUtils;
import com.unity3d.player.UnityPlayer;

public class UnityPluginAPIEventsBridge implements PluginEventBridge.PluginEventsAPI {
    private HelpshiftUnityStorage storage;

    public UnityPluginAPIEventsBridge(HelpshiftUnityStorage helpshiftUnityStorage) {
        this.storage = helpshiftUnityStorage;
    }

    public void sendMessage(String str, String str2) {
        UnityUtils.sendUnityMessage(this.storage.getString(HelpshiftUnityStorage.UNITY_MESSAGE_HANDLER_KEY), str, str2);
    }

    public PendingIntent getPendingIntentForNotification(Context context, PendingIntent pendingIntent) {
        return IntentProvider.wrapPendingIntentWithUnityDelegateActivity(context, pendingIntent);
    }

    public boolean shouldCallFirstForegroundEvent() {
        return UnityPlayer.currentActivity != null;
    }
}
