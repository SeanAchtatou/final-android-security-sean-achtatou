package com.helpshift;

import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.account.domainmodel.UserLoginManager;
import com.helpshift.account.domainmodel.UserManagerDM;
import com.helpshift.analytics.domainmodel.AnalyticsEventDM;
import com.helpshift.cif.CustomIssueFieldDM;
import com.helpshift.common.AutoRetryFailedEventDM;
import com.helpshift.common.FetchDataFromThread;
import com.helpshift.common.domain.AttachmentFileManagerDM;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.F;
import com.helpshift.common.domain.Threader;
import com.helpshift.common.domain.network.POSTNetwork;
import com.helpshift.common.domain.network.TSCorrectedNetwork;
import com.helpshift.common.platform.Platform;
import com.helpshift.common.platform.network.RequestData;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.configuration.response.RootServerConfig;
import com.helpshift.conversation.ConversationInboxPoller;
import com.helpshift.conversation.activeconversation.ConversationInfoRenderer;
import com.helpshift.conversation.activeconversation.ConversationRenderer;
import com.helpshift.conversation.activeconversation.ConversationalRenderer;
import com.helpshift.conversation.activeconversation.ScreenshotPreviewRenderer;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.activeconversation.usersetup.UserSetupRenderer;
import com.helpshift.conversation.domainmodel.ConversationController;
import com.helpshift.conversation.domainmodel.ConversationInboxManagerDM;
import com.helpshift.conversation.usersetup.UserSetupVM;
import com.helpshift.conversation.viewmodel.ConversationInfoVM;
import com.helpshift.conversation.viewmodel.ConversationVM;
import com.helpshift.conversation.viewmodel.ConversationalVM;
import com.helpshift.conversation.viewmodel.NewConversationRenderer;
import com.helpshift.conversation.viewmodel.NewConversationVM;
import com.helpshift.conversation.viewmodel.ScreenshotPreviewVM;
import com.helpshift.crypto.CryptoDM;
import com.helpshift.delegate.RootDelegate;
import com.helpshift.delegate.UIThreadDelegateDecorator;
import com.helpshift.faq.FaqsDM;
import com.helpshift.localeprovider.domainmodel.LocaleProviderDM;
import com.helpshift.logger.ErrorReportsDM;
import com.helpshift.meta.MetaDataDM;
import com.helpshift.redaction.RedactionAgent;
import com.helpshift.util.ValuePair;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class JavaCore implements CoreApi {
    private static final String TAG = "Helpshift_JavaCore";
    final AnalyticsEventDM analyticsEventDM;
    /* access modifiers changed from: private */
    public Domain domain;
    private boolean isSDKSessionActive = false;
    private final MetaDataDM metaDataDM;
    private final Threader parallelThreader;
    final Platform platform;
    final SDKConfigurationDM sdkConfigurationDM;
    /* access modifiers changed from: private */
    public UserManagerDM userManagerDM;

    public JavaCore(Platform platform2) {
        this.platform = platform2;
        this.domain = new Domain(platform2);
        this.userManagerDM = this.domain.getUserManagerDM();
        this.parallelThreader = this.domain.getParallelThreader();
        this.sdkConfigurationDM = this.domain.getSDKConfigurationDM();
        this.analyticsEventDM = this.domain.getAnalyticsEventDM();
        this.metaDataDM = this.domain.getMetaDataDM();
    }

    public Domain getDomain() {
        return this.domain;
    }

    public NewConversationVM getNewConversationViewModel(NewConversationRenderer newConversationRenderer) {
        return new NewConversationVM(this.platform, this.domain, getConversationController(), newConversationRenderer);
    }

    public ConversationVM getConversationViewModel(Long l, ConversationRenderer conversationRenderer, boolean z) {
        return new ConversationVM(this.platform, this.domain, getConversationController(), getConversationController().getViewableConversation(false, l), conversationRenderer, z);
    }

    public ConversationalVM getConversationalViewModel(boolean z, Long l, ConversationalRenderer conversationalRenderer, boolean z2) {
        return new ConversationalVM(this.platform, this.domain, getConversationController(), getConversationController().getViewableConversation(z, l), conversationalRenderer, z, z2);
    }

    public ConversationInfoVM getConversationInfoViewModel(ConversationInfoRenderer conversationInfoRenderer) {
        return new ConversationInfoVM(this.domain, conversationInfoRenderer);
    }

    public ScreenshotPreviewVM getScreenshotPreviewModel(ScreenshotPreviewRenderer screenshotPreviewRenderer) {
        return new ScreenshotPreviewVM(this.domain, screenshotPreviewRenderer);
    }

    public Conversation getActiveConversation() {
        return getConversationController().getActiveConversationFromStorage();
    }

    public Conversation getActiveConversationOrPreIssue() {
        return getConversationController().getActiveConversationOrPreIssue();
    }

    public UserSetupVM getUserSetupVM(UserSetupRenderer userSetupRenderer) {
        return new UserSetupVM(this.platform, this.domain, getUserManagerDM().getActiveUserSetupDM(), userSetupRenderer);
    }

    public boolean isActiveConversationActionable() {
        return getConversationController().isActiveConversationActionable();
    }

    public boolean isSDKSessionActive() {
        return this.isSDKSessionActive;
    }

    public void setDelegateListener(RootDelegate rootDelegate) {
        this.domain.setDelegate(rootDelegate);
    }

    public synchronized boolean clearAnonymousUser() {
        return new UserLoginManager(this, this.domain, this.platform).clearAnonymousUser();
    }

    public synchronized boolean login(HelpshiftUser helpshiftUser) {
        return new UserLoginManager(this, this.domain, this.platform).login(helpshiftUser);
    }

    public void setNameAndEmail(String str, String str2) {
        getConversationController().saveName(str);
        getConversationController().saveEmail(str2);
    }

    public synchronized boolean logout() {
        return new UserLoginManager(this, this.domain, this.platform).logout();
    }

    public void setPushToken(String str) {
        if (str != null && !str.equals(this.platform.getDevice().getPushToken())) {
            this.platform.getDevice().setPushToken(str);
            getUserManagerDM().resetPushTokenSyncStatusForUsers();
            getUserManagerDM().sendPushToken();
        }
    }

    public void onSDKSessionStarted() {
        this.isSDKSessionActive = true;
        getDelegate().sessionBegan();
    }

    public void onSDKSessionEnded() {
        this.isSDKSessionActive = false;
        getDelegate().sessionEnded();
    }

    public void fetchServerConfig() {
        runParallel(new F() {
            public void f() {
                UserDM activeUser = JavaCore.this.userManagerDM.getActiveUser();
                RootServerConfig fetchServerConfig = JavaCore.this.sdkConfigurationDM.fetchServerConfig(JavaCore.this.userManagerDM);
                if (fetchServerConfig != null) {
                    new RedactionAgent(JavaCore.this.platform, JavaCore.this.domain).checkAndUpdateRedactionState(activeUser, fetchServerConfig.profileCreatedAt, fetchServerConfig.lastRedactionAt);
                }
            }
        });
    }

    public void refreshPoller() {
        getConversationInboxPoller().refreshPoller(false);
    }

    public void updateInstallConfig(Map<String, Object> map) {
        this.sdkConfigurationDM.updateInstallConfig(map);
    }

    public void updateApiConfig(Map<String, Object> map) {
        this.sdkConfigurationDM.updateApiConfig(map);
        if (map.containsKey("enableFullPrivacy") && ((Boolean) map.get("enableFullPrivacy")).booleanValue()) {
            new UserLoginManager(this, this.domain, this.platform).clearPersonallyIdentifiableInformation();
        }
    }

    public void sendAnalyticsEvent() {
        runParallel(new F() {
            public void f() {
                if (JavaCore.this.analyticsEventDM != null) {
                    JavaCore.this.analyticsEventDM.sendEventsToServer(JavaCore.this.getUserManagerDM().getActiveUser());
                }
            }
        });
    }

    public AnalyticsEventDM getAnalyticsEventDM() {
        return this.analyticsEventDM;
    }

    public void sendAppStartEvent() {
        runParallel(new F() {
            public void f() {
                JavaCore.this.analyticsEventDM.sendAppStartEvent(JavaCore.this.getUserManagerDM().getActiveUser());
            }
        });
    }

    public UIThreadDelegateDecorator getDelegate() {
        return this.domain.getDelegate();
    }

    public void sendFailedApiCalls() {
        getFaqDM();
        UserManagerDM userManagerDM2 = getUserManagerDM();
        getConversationController();
        userManagerDM2.getActiveUserSetupDM();
        getAnalyticsEventDM();
        this.domain.getAutoRetryFailedEventDM().sendAllEvents();
    }

    public UserManagerDM getUserManagerDM() {
        return this.userManagerDM;
    }

    public MetaDataDM getMetaDataDM() {
        return this.metaDataDM;
    }

    public CustomIssueFieldDM getCustomIssueFieldDM() {
        return this.domain.getCustomIssueFieldDM();
    }

    public SDKConfigurationDM getSDKConfigurationDM() {
        return this.sdkConfigurationDM;
    }

    public ConversationInboxPoller getConversationInboxPoller() {
        return getConversationController().getConversationInboxPoller();
    }

    public void handlePushNotification(String str, String str2, String str3) {
        getConversationController().handlePushNotification(str, str2, str3);
    }

    public int getNotificationCountSync() {
        return getConversationController().getNotificationCountSync();
    }

    public void getNotificationCountAsync(final FetchDataFromThread<ValuePair<Integer, Boolean>, Object> fetchDataFromThread) {
        this.domain.runParallel(new F() {
            /* JADX INFO: finally extract failed */
            public void f() {
                try {
                    ValuePair<Integer, Boolean> fetchConversationsAndGetNotificationCount = JavaCore.this.getConversationController().fetchConversationsAndGetNotificationCount();
                    if (fetchDataFromThread == null) {
                        return;
                    }
                    if (fetchConversationsAndGetNotificationCount == null || ((Integer) fetchConversationsAndGetNotificationCount.first).intValue() < 0) {
                        fetchDataFromThread.onFailure(fetchConversationsAndGetNotificationCount);
                    } else {
                        fetchDataFromThread.onDataFetched(fetchConversationsAndGetNotificationCount);
                    }
                } catch (Throwable th) {
                    if (fetchDataFromThread != null) {
                        fetchDataFromThread.onFailure(null);
                    }
                    throw th;
                }
            }
        });
    }

    public ConversationController getConversationController() {
        return getConversationInboxManagerDM().getActiveConversationInboxDM();
    }

    public FaqsDM getFaqDM() {
        return this.domain.getFaqsDM();
    }

    public CryptoDM getCryptoDM() {
        return this.domain.getCryptoDM();
    }

    public LocaleProviderDM getLocaleProviderDM() {
        return this.domain.getLocaleProviderDM();
    }

    public AttachmentFileManagerDM getAttachmentFileManagerDM() {
        return this.domain.getAttachmentFileManagerDM();
    }

    public AutoRetryFailedEventDM getAutoRetryFailedEventDM() {
        return this.domain.getAutoRetryFailedEventDM();
    }

    public void sendRequestIdsForSuccessfulApiCalls() {
        this.domain.runParallel(new F() {
            public void f() {
                TSCorrectedNetwork tSCorrectedNetwork = new TSCorrectedNetwork(new POSTNetwork("/clear-idempotent-cache/", JavaCore.this.domain, JavaCore.this.platform), JavaCore.this.platform);
                Set<String> allSuccessfulRequestIds = JavaCore.this.platform.getNetworkRequestDAO().getAllSuccessfulRequestIds();
                if (!allSuccessfulRequestIds.isEmpty()) {
                    String jsonify = JavaCore.this.platform.getJsonifier().jsonify(allSuccessfulRequestIds);
                    HashMap hashMap = new HashMap();
                    hashMap.put("request_ids", jsonify);
                    tSCorrectedNetwork.makeRequest(new RequestData(hashMap));
                    JavaCore.this.platform.getNetworkRequestDAO().clearSuccessfulRequestIds();
                }
            }
        });
    }

    public void resetPreIssueConversations() {
        this.domain.getConversationInboxManagerDM().resetPreIssueConversations();
    }

    /* access modifiers changed from: package-private */
    public ConversationInboxManagerDM getConversationInboxManagerDM() {
        return this.domain.getConversationInboxManagerDM();
    }

    public ErrorReportsDM getErrorReportsDM() {
        return this.domain.getErrorReportsDM();
    }

    private void runParallel(F f) {
        this.parallelThreader.thread(f).f();
    }

    public void resetUsersSyncStatusAndStartSetupForActiveUser() {
        getUserManagerDM().resetSyncStateForAllUsers();
        getUserManagerDM().getActiveUserSetupDM().startSetup();
    }
}
