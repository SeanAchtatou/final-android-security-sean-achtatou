package com.helpshift.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.helpshift.model.InfoModelFactory;
import com.helpshift.util.ApplicationUtil;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftContext;

public class MainActivity extends AppCompatActivity {
    public static final String SHOW_IN_FULLSCREEN = "showInFullScreen";
    private static final String TAG = "Helpshift_MainActvty";

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (HelpshiftContext.installCallSuccessful.get()) {
            if (getIntent().getBooleanExtra(SHOW_IN_FULLSCREEN, false)) {
                getWindow().setFlags(1024, 1024);
            }
            try {
                Integer num = InfoModelFactory.getInstance().appInfoModel.screenOrientation;
                if (num != null && num.intValue() != -1) {
                    setRequestedOrientation(num.intValue());
                }
            } catch (Exception e) {
                HSLogger.e(TAG, "Unable to set the requested orientation : " + e.getMessage());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        if (HelpshiftContext.installCallSuccessful.get()) {
            context = ApplicationUtil.getContextWithUpdatedLocale(context);
        }
        super.attachBaseContext(context);
    }
}
