package com.helpshift.activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.os.Bundle;
import android.util.Log;
import com.helpshift.util.HelpshiftUtil;
import com.helpshift.util.concurrent.ApiExecutorFactory;

public class UnityDelegateActivity extends Activity {
    private static final String TAG = "UnityDelegateActivity";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            if (!HelpshiftUtil.installWithCachedCreds(getApplicationContext())) {
                finish();
                finish();
                return;
            }
            ApiExecutorFactory.getHandlerExecutor().awaitForSyncExecution();
            PendingIntent pendingIntent = (PendingIntent) getIntent().getParcelableExtra("delegateIntent");
            if (pendingIntent != null) {
                pendingIntent.send();
            }
            finish();
        } catch (PendingIntent.CanceledException e) {
            String str = TAG;
            Log.e(str, "Error in sending pending intent : " + e.getMessage());
        } catch (Throwable th) {
            finish();
            throw th;
        }
    }
}
