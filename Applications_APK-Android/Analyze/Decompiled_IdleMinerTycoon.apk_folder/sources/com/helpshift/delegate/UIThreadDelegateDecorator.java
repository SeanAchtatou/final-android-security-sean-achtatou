package com.helpshift.delegate;

import com.helpshift.HelpshiftUser;
import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.F;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class UIThreadDelegateDecorator {
    private Map<String, Boolean> authenticationFailedCalledList = new HashMap();
    /* access modifiers changed from: private */
    public RootDelegate delegate;
    private Domain domain;

    public UIThreadDelegateDecorator(Domain domain2) {
        this.domain = domain2;
    }

    public void sessionBegan() {
        if (this.delegate != null) {
            this.domain.runOnUI(new F() {
                public void f() {
                    UIThreadDelegateDecorator.this.delegate.sessionBegan();
                }
            });
        }
    }

    public void sessionEnded() {
        if (this.delegate != null) {
            this.domain.runOnUI(new F() {
                public void f() {
                    UIThreadDelegateDecorator.this.delegate.sessionEnded();
                }
            });
        }
    }

    public void newConversationStarted(final String str) {
        if (this.delegate != null) {
            this.domain.runOnUI(new F() {
                public void f() {
                    UIThreadDelegateDecorator.this.delegate.newConversationStarted(str);
                }
            });
        }
    }

    public void conversationEnded() {
        if (this.delegate != null) {
            this.domain.runOnUI(new F() {
                public void f() {
                    UIThreadDelegateDecorator.this.delegate.conversationEnded();
                }
            });
        }
    }

    public void userRepliedToConversation(final String str) {
        if (this.delegate != null) {
            this.domain.runOnUI(new F() {
                public void f() {
                    UIThreadDelegateDecorator.this.delegate.userRepliedToConversation(str);
                }
            });
        }
    }

    public void userCompletedCustomerSatisfactionSurvey(final int i, final String str) {
        if (this.delegate != null) {
            this.domain.runOnUI(new F() {
                public void f() {
                    UIThreadDelegateDecorator.this.delegate.userCompletedCustomerSatisfactionSurvey(i, str);
                }
            });
        }
    }

    public void displayAttachmentFile(final File file) {
        if (this.delegate != null) {
            this.domain.runOnUI(new F() {
                public void f() {
                    UIThreadDelegateDecorator.this.delegate.displayAttachmentFile(file);
                }
            });
        }
    }

    public void didReceiveNotification(final int i) {
        if (this.delegate != null) {
            this.domain.runOnUI(new F() {
                public void f() {
                    UIThreadDelegateDecorator.this.delegate.didReceiveNotification(i);
                }
            });
        }
    }

    public void authenticationFailed(UserDM userDM, final AuthenticationFailureReason authenticationFailureReason) {
        if (this.delegate != null && userDM.isActiveUser()) {
            String str = userDM.getLocalId() + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + userDM.getAuthToken();
            if (!this.authenticationFailedCalledList.containsKey(str) || !this.authenticationFailedCalledList.get(str).booleanValue()) {
                this.authenticationFailedCalledList.put(str, true);
                final HelpshiftUser build = new HelpshiftUser.Builder(userDM.getIdentifier(), userDM.getEmail()).setName(userDM.getName()).setAuthToken(userDM.getAuthToken()).build();
                this.domain.runOnUI(new F() {
                    public void f() {
                        UIThreadDelegateDecorator.this.delegate.authenticationFailed(build, authenticationFailureReason);
                    }
                });
            }
        }
    }

    public boolean isDelegateRegistered() {
        return this.delegate != null;
    }

    public void setDelegate(RootDelegate rootDelegate) {
        this.delegate = rootDelegate;
    }
}
