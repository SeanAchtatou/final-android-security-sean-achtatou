package com.helpshift.storage;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import com.helpshift.util.HSLogger;

public class RetryKeyValueDbStorage extends BaseRetryKeyValueStorage {
    private static final String backupFileName = "__hs__kv_backup";
    private final Context context;
    private SQLiteOpenHelper sqLiteOpenHelper;

    RetryKeyValueDbStorage(Context context2) {
        this.context = context2;
        this.sqLiteOpenHelper = new KeyValueDbStorageHelper(context2);
        this.keyValueStorage = new KeyValueDbStorage(this.sqLiteOpenHelper, backupFileName);
    }

    /* access modifiers changed from: protected */
    public void reInitiateDbInstance() {
        try {
            if (this.sqLiteOpenHelper != null) {
                this.sqLiteOpenHelper.close();
            }
        } catch (Exception e) {
            HSLogger.e("Helpshift_RetryKeyValue", "Error in closing DB", e);
        }
        this.sqLiteOpenHelper = new KeyValueDbStorageHelper(this.context);
        this.keyValueStorage = new KeyValueDbStorage(this.sqLiteOpenHelper, backupFileName);
    }
}
