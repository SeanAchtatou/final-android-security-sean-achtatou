package com.helpshift.storage;

import android.support.annotation.NonNull;
import com.helpshift.constants.SwitchUserKeys;
import com.helpshift.controllers.DataSyncCoordinatorImpl;
import com.helpshift.controllers.SyncController;
import com.helpshift.model.AppInfoModel;
import com.helpshift.model.SdkInfoModel;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.util.constants.KeyValueStorageKeys;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class StorageFactory {
    private static StorageFactory storageFactoryInstance;
    public final KeyValueStorage keyValueStorage = new CachedKeyValueStorage(new RetryKeyValueDbStorage(HelpshiftContext.getApplicationContext()), getCacheWhitelistKeys());

    StorageFactory() {
    }

    public static synchronized StorageFactory getInstance() {
        StorageFactory storageFactory;
        synchronized (StorageFactory.class) {
            if (storageFactoryInstance == null) {
                storageFactoryInstance = new StorageFactory();
            }
            storageFactory = storageFactoryInstance;
        }
        return storageFactory;
    }

    @NonNull
    private Set<String> getCacheWhitelistKeys() {
        return new HashSet(Arrays.asList(DataSyncCoordinatorImpl.FIRST_DEVICE_SYNC_COMPLETE_KEY, KeyValueStorageKeys.CAMPAIGNS_IMAGE_URL_RETRY_COUNTS, SdkInfoModel.SDK_LANGUAGE, "disableHelpshiftBranding", AppInfoModel.SCREEN_ORIENTATION_KEY, SyncController.DataTypes.DEVICE, SyncController.DataTypes.USER, SyncController.DataTypes.SESSION, SyncController.DataTypes.SWITCH_USER, SyncController.DataTypes.ANALYTICS_EVENT, SwitchUserKeys.CURRENT_USER, SwitchUserKeys.PREV_USER));
    }
}
