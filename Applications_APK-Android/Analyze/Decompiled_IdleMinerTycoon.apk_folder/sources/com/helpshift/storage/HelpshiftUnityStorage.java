package com.helpshift.storage;

import android.content.Context;
import android.content.SharedPreferences;
import com.helpshift.util.TextUtils;

public class HelpshiftUnityStorage {
    public static final String API_KEY = "apiKey";
    public static final String APP_ID = "appId";
    public static final String DOMAIN_NAME = "domainName";
    public static final String INSTALL_CONFIG = "installConfig";
    private static final String TAG = "Helpshift_UnityStore";
    public static final String UNITY_MESSAGE_HANDLER_KEY = "unityMessageHandler";
    private static HelpshiftUnityStorage storage;
    private final SharedPreferences sharedPreferences;

    private HelpshiftUnityStorage(Context context) {
        this.sharedPreferences = context.getSharedPreferences("__helpshift_unity_prefs", 0);
    }

    public static HelpshiftUnityStorage getInstance(Context context) {
        if (storage == null) {
            synchronized (HelpshiftUnityStorage.class) {
                if (storage == null) {
                    storage = new HelpshiftUnityStorage(context);
                }
            }
        }
        return storage;
    }

    public void put(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            SharedPreferences.Editor edit = this.sharedPreferences.edit();
            edit.putString(str, str2);
            edit.commit();
        }
    }

    public String getString(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        return this.sharedPreferences.getString(str, "");
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0055 A[SYNTHETIC, Splitter:B:26:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x005f A[SYNTHETIC, Splitter:B:32:0x005f] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0064 A[Catch:{ Exception -> 0x0067 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void putMap(java.lang.String r6, java.util.HashMap<java.lang.String, java.lang.Object> r7) {
        /*
            r5 = this;
            boolean r0 = com.helpshift.util.TextUtils.isEmpty(r6)
            if (r0 == 0) goto L_0x0007
            return
        L_0x0007:
            r0 = 0
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            r1.<init>()     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x0036 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0036 }
            r2.writeObject(r7)     // Catch:{ Exception -> 0x0033, all -> 0x0030 }
            byte[] r7 = r1.toByteArray()     // Catch:{ Exception -> 0x0033, all -> 0x0030 }
            java.lang.String r7 = java.util.Arrays.toString(r7)     // Catch:{ Exception -> 0x0033, all -> 0x0030 }
            android.content.SharedPreferences r0 = r5.sharedPreferences     // Catch:{ Exception -> 0x0033, all -> 0x0030 }
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ Exception -> 0x0033, all -> 0x0030 }
            r0.putString(r6, r7)     // Catch:{ Exception -> 0x0033, all -> 0x0030 }
            r0.commit()     // Catch:{ Exception -> 0x0033, all -> 0x0030 }
            r2.close()     // Catch:{ Exception -> 0x005b }
        L_0x002c:
            r1.close()     // Catch:{ Exception -> 0x005b }
            goto L_0x005b
        L_0x0030:
            r6 = move-exception
            r0 = r2
            goto L_0x005d
        L_0x0033:
            r7 = move-exception
            r0 = r2
            goto L_0x003d
        L_0x0036:
            r7 = move-exception
            goto L_0x003d
        L_0x0038:
            r6 = move-exception
            r1 = r0
            goto L_0x005d
        L_0x003b:
            r7 = move-exception
            r1 = r0
        L_0x003d:
            java.lang.String r2 = "Helpshift_UnityStore"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x005c }
            r3.<init>()     // Catch:{ all -> 0x005c }
            java.lang.String r4 = "Error storing value: "
            r3.append(r4)     // Catch:{ all -> 0x005c }
            r3.append(r6)     // Catch:{ all -> 0x005c }
            java.lang.String r6 = r3.toString()     // Catch:{ all -> 0x005c }
            android.util.Log.e(r2, r6, r7)     // Catch:{ all -> 0x005c }
            if (r0 == 0) goto L_0x0058
            r0.close()     // Catch:{ Exception -> 0x005b }
        L_0x0058:
            if (r1 == 0) goto L_0x005b
            goto L_0x002c
        L_0x005b:
            return
        L_0x005c:
            r6 = move-exception
        L_0x005d:
            if (r0 == 0) goto L_0x0062
            r0.close()     // Catch:{ Exception -> 0x0067 }
        L_0x0062:
            if (r1 == 0) goto L_0x0067
            r1.close()     // Catch:{ Exception -> 0x0067 }
        L_0x0067:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.storage.HelpshiftUnityStorage.putMap(java.lang.String, java.util.HashMap):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0084 A[SYNTHETIC, Splitter:B:37:0x0084] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0089 A[Catch:{ Exception -> 0x008c }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0090 A[SYNTHETIC, Splitter:B:44:0x0090] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0095 A[Catch:{ Exception -> 0x0098 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.HashMap<java.lang.String, java.lang.Object> getMap(java.lang.String r9) {
        /*
            r8 = this;
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            boolean r1 = com.helpshift.util.TextUtils.isEmpty(r9)
            if (r1 == 0) goto L_0x000c
            return r0
        L_0x000c:
            r1 = 0
            android.content.SharedPreferences r2 = r8.sharedPreferences     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            java.lang.String r3 = ""
            java.lang.String r2 = r2.getString(r9, r3)     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            boolean r3 = com.helpshift.util.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            if (r3 == 0) goto L_0x001c
            return r0
        L_0x001c:
            int r3 = r2.length()     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            r4 = 1
            int r3 = r3 - r4
            java.lang.String r2 = r2.substring(r4, r3)     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            java.lang.String r3 = ", "
            java.lang.String[] r2 = r2.split(r3)     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            int r3 = r2.length     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            r4 = 0
        L_0x0030:
            int r5 = r3.length     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            if (r4 >= r5) goto L_0x003e
            r5 = r2[r4]     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            byte r5 = java.lang.Byte.parseByte(r5)     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            r3[r4] = r5     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            int r4 = r4 + 1
            goto L_0x0030
        L_0x003e:
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            java.io.ObjectInputStream r3 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0062, all -> 0x005f }
            java.lang.Object r1 = r3.readObject()     // Catch:{ Exception -> 0x0059, all -> 0x0056 }
            java.util.HashMap r1 = (java.util.HashMap) r1     // Catch:{ Exception -> 0x0059, all -> 0x0056 }
            r3.close()     // Catch:{ Exception -> 0x0054 }
            r2.close()     // Catch:{ Exception -> 0x0054 }
        L_0x0054:
            r0 = r1
            goto L_0x008c
        L_0x0056:
            r9 = move-exception
            r1 = r3
            goto L_0x0060
        L_0x0059:
            r1 = move-exception
            r7 = r2
            r2 = r1
            r1 = r3
            r3 = r7
            goto L_0x006c
        L_0x005f:
            r9 = move-exception
        L_0x0060:
            r3 = r2
            goto L_0x008e
        L_0x0062:
            r3 = move-exception
            r7 = r3
            r3 = r2
            r2 = r7
            goto L_0x006c
        L_0x0067:
            r9 = move-exception
            r3 = r1
            goto L_0x008e
        L_0x006a:
            r2 = move-exception
            r3 = r1
        L_0x006c:
            java.lang.String r4 = "Helpshift_UnityStore"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x008d }
            r5.<init>()     // Catch:{ all -> 0x008d }
            java.lang.String r6 = "Error getting value: "
            r5.append(r6)     // Catch:{ all -> 0x008d }
            r5.append(r9)     // Catch:{ all -> 0x008d }
            java.lang.String r9 = r5.toString()     // Catch:{ all -> 0x008d }
            android.util.Log.e(r4, r9, r2)     // Catch:{ all -> 0x008d }
            if (r1 == 0) goto L_0x0087
            r1.close()     // Catch:{ Exception -> 0x008c }
        L_0x0087:
            if (r3 == 0) goto L_0x008c
            r3.close()     // Catch:{ Exception -> 0x008c }
        L_0x008c:
            return r0
        L_0x008d:
            r9 = move-exception
        L_0x008e:
            if (r1 == 0) goto L_0x0093
            r1.close()     // Catch:{ Exception -> 0x0098 }
        L_0x0093:
            if (r3 == 0) goto L_0x0098
            r3.close()     // Catch:{ Exception -> 0x0098 }
        L_0x0098:
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.storage.HelpshiftUnityStorage.getMap(java.lang.String):java.util.HashMap");
    }
}
