package com.helpshift.util;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.TextView;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HSLinkify {
    public static final int ALL = 15;
    private static final int EMAIL_ADDRESSES = 2;
    private static final int MAP_ADDRESSES = 8;
    private static final int PHONE_NUMBERS = 4;
    public static final int WEB_URLS = 1;
    private static final MatchFilter sUrlMatchFilter = new MatchFilter() {
        public final boolean acceptMatch(CharSequence charSequence, int i, int i2) {
            return i == 0 || charSequence.charAt(i - 1) != '@';
        }
    };

    public interface LinkClickListener {
        void onLinkClicked(String str);
    }

    public interface MatchFilter {
        boolean acceptMatch(CharSequence charSequence, int i, int i2);
    }

    public interface TransformFilter {
        String transformUrl(Matcher matcher, String str);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:51|52|(1:113)(2:54|(2:112|56)(8:57|58|59|60|61|62|114|111))) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x0179 */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0179 A[LOOP:5: B:51:0x0179->B:111:0x0179, LOOP_START, PHI: r4 r6 
      PHI: (r4v19 java.lang.String) = (r4v18 java.lang.String), (r4v20 java.lang.String) binds: [B:50:0x0174, B:111:0x0179] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r6v14 int) = (r6v13 int), (r6v15 int) binds: [B:50:0x0174, B:111:0x0179] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:51:0x0179] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean addLinks(android.text.Spannable r19, int r20, com.helpshift.util.HSLinkify.LinkClickListener r21) {
        /*
            r0 = r19
            r2 = 0
            if (r20 != 0) goto L_0x0006
            return r2
        L_0x0006:
            int r3 = r19.length()
            java.lang.Class<android.text.style.URLSpan> r4 = android.text.style.URLSpan.class
            java.lang.Object[] r3 = r0.getSpans(r2, r3, r4)
            android.text.style.URLSpan[] r3 = (android.text.style.URLSpan[]) r3
            int r4 = r3.length
            r5 = 1
            int r4 = r4 - r5
        L_0x0015:
            if (r4 < 0) goto L_0x001f
            r6 = r3[r4]
            r0.removeSpan(r6)
            int r4 = r4 + -1
            goto L_0x0015
        L_0x001f:
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r4 = r20 & 1
            if (r4 == 0) goto L_0x00d5
            java.util.regex.Pattern r4 = android.util.Patterns.WEB_URL
            java.util.regex.Matcher r4 = r4.matcher(r0)
        L_0x002e:
            boolean r6 = r4.find()
            if (r6 == 0) goto L_0x00d5
            int r6 = r4.start()
            int r7 = r4.end()
            com.helpshift.util.HSLinkify$MatchFilter r8 = com.helpshift.util.HSLinkify.sUrlMatchFilter
            if (r8 == 0) goto L_0x0048
            com.helpshift.util.HSLinkify$MatchFilter r8 = com.helpshift.util.HSLinkify.sUrlMatchFilter
            boolean r8 = r8.acceptMatch(r0, r6, r7)
            if (r8 == 0) goto L_0x002e
        L_0x0048:
            com.helpshift.util.LinkSpec r8 = new com.helpshift.util.LinkSpec
            r8.<init>()
            java.lang.String r15 = r4.group(r2)
            r9 = 3
            java.lang.String[] r14 = new java.lang.String[r9]
            java.lang.String r9 = "http://"
            r14[r2] = r9
            java.lang.String r9 = "https://"
            r14[r5] = r9
            java.lang.String r9 = "rtsp://"
            r10 = 2
            r14[r10] = r9
            r13 = 0
        L_0x0062:
            int r9 = r14.length
            if (r13 >= r9) goto L_0x00b4
            r10 = 1
            r11 = 0
            r12 = r14[r13]
            r16 = 0
            r9 = r14[r13]
            int r17 = r9.length()
            r9 = r15
            r18 = r13
            r13 = r16
            r16 = r14
            r14 = r17
            boolean r9 = r9.regionMatches(r10, r11, r12, r13, r14)
            if (r9 == 0) goto L_0x00af
            r10 = 0
            r11 = 0
            r12 = r16[r18]
            r13 = 0
            r9 = r16[r18]
            int r14 = r9.length()
            r9 = r15
            boolean r9 = r9.regionMatches(r10, r11, r12, r13, r14)
            if (r9 != 0) goto L_0x00ad
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r10 = r16[r18]
            r9.append(r10)
            r10 = r16[r18]
            int r10 = r10.length()
            java.lang.String r10 = r15.substring(r10)
            r9.append(r10)
            java.lang.String r15 = r9.toString()
        L_0x00ad:
            r9 = 1
            goto L_0x00b7
        L_0x00af:
            int r13 = r18 + 1
            r14 = r16
            goto L_0x0062
        L_0x00b4:
            r16 = r14
            r9 = 0
        L_0x00b7:
            if (r9 != 0) goto L_0x00ca
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r10 = r16[r2]
            r9.append(r10)
            r9.append(r15)
            java.lang.String r15 = r9.toString()
        L_0x00ca:
            r8.url = r15
            r8.start = r6
            r8.end = r7
            r3.add(r8)
            goto L_0x002e
        L_0x00d5:
            r4 = r20 & 2
            if (r4 == 0) goto L_0x0170
            java.util.regex.Pattern r4 = android.util.Patterns.EMAIL_ADDRESS
            java.util.regex.Matcher r4 = r4.matcher(r0)
        L_0x00df:
            boolean r6 = r4.find()
            if (r6 == 0) goto L_0x0170
            int r6 = r4.start()
            int r7 = r4.end()
            com.helpshift.util.LinkSpec r8 = new com.helpshift.util.LinkSpec
            r8.<init>()
            java.lang.String r15 = r4.group(r2)
            java.lang.String[] r14 = new java.lang.String[r5]
            java.lang.String r9 = "mailto:"
            r14[r2] = r9
            r13 = 0
        L_0x00fd:
            int r9 = r14.length
            if (r13 >= r9) goto L_0x014f
            r10 = 1
            r11 = 0
            r12 = r14[r13]
            r16 = 0
            r9 = r14[r13]
            int r17 = r9.length()
            r9 = r15
            r18 = r13
            r13 = r16
            r16 = r14
            r14 = r17
            boolean r9 = r9.regionMatches(r10, r11, r12, r13, r14)
            if (r9 == 0) goto L_0x014a
            r10 = 0
            r11 = 0
            r12 = r16[r18]
            r13 = 0
            r9 = r16[r18]
            int r14 = r9.length()
            r9 = r15
            boolean r9 = r9.regionMatches(r10, r11, r12, r13, r14)
            if (r9 != 0) goto L_0x0148
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r10 = r16[r18]
            r9.append(r10)
            r10 = r16[r18]
            int r10 = r10.length()
            java.lang.String r10 = r15.substring(r10)
            r9.append(r10)
            java.lang.String r15 = r9.toString()
        L_0x0148:
            r9 = 1
            goto L_0x0152
        L_0x014a:
            int r13 = r18 + 1
            r14 = r16
            goto L_0x00fd
        L_0x014f:
            r16 = r14
            r9 = 0
        L_0x0152:
            if (r9 != 0) goto L_0x0165
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r10 = r16[r2]
            r9.append(r10)
            r9.append(r15)
            java.lang.String r15 = r9.toString()
        L_0x0165:
            r8.url = r15
            r8.start = r6
            r8.end = r7
            r3.add(r8)
            goto L_0x00df
        L_0x0170:
            r4 = r20 & 8
            if (r4 == 0) goto L_0x01b7
            java.lang.String r4 = r19.toString()
            r6 = 0
        L_0x0179:
            java.lang.String r7 = android.webkit.WebView.findAddress(r4)     // Catch:{ UnsupportedOperationException -> 0x01b7 }
            if (r7 == 0) goto L_0x01b7
            int r8 = r4.indexOf(r7)     // Catch:{ UnsupportedOperationException -> 0x01b7 }
            if (r8 >= 0) goto L_0x0186
            goto L_0x01b7
        L_0x0186:
            com.helpshift.util.LinkSpec r9 = new com.helpshift.util.LinkSpec     // Catch:{ UnsupportedOperationException -> 0x01b7 }
            r9.<init>()     // Catch:{ UnsupportedOperationException -> 0x01b7 }
            int r10 = r7.length()     // Catch:{ UnsupportedOperationException -> 0x01b7 }
            int r10 = r10 + r8
            int r8 = r8 + r6
            r9.start = r8     // Catch:{ UnsupportedOperationException -> 0x01b7 }
            int r6 = r6 + r10
            r9.end = r6     // Catch:{ UnsupportedOperationException -> 0x01b7 }
            java.lang.String r4 = r4.substring(r10)     // Catch:{ UnsupportedOperationException -> 0x01b7 }
            java.lang.String r8 = "UTF-8"
            java.lang.String r7 = java.net.URLEncoder.encode(r7, r8)     // Catch:{ UnsupportedEncodingException -> 0x0179 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ UnsupportedOperationException -> 0x01b7 }
            r8.<init>()     // Catch:{ UnsupportedOperationException -> 0x01b7 }
            java.lang.String r10 = "geo:0,0?q="
            r8.append(r10)     // Catch:{ UnsupportedOperationException -> 0x01b7 }
            r8.append(r7)     // Catch:{ UnsupportedOperationException -> 0x01b7 }
            java.lang.String r7 = r8.toString()     // Catch:{ UnsupportedOperationException -> 0x01b7 }
            r9.url = r7     // Catch:{ UnsupportedOperationException -> 0x01b7 }
            r3.add(r9)     // Catch:{ UnsupportedOperationException -> 0x01b7 }
            goto L_0x0179
        L_0x01b7:
            r1 = r20 & 4
            if (r1 == 0) goto L_0x01fa
            java.util.regex.Pattern r1 = android.util.Patterns.PHONE
            java.util.regex.Matcher r1 = r1.matcher(r0)
        L_0x01c1:
            boolean r4 = r1.find()
            if (r4 == 0) goto L_0x01fa
            java.lang.String r4 = r1.group()
            int r6 = r4.length()
            r7 = 6
            if (r6 < r7) goto L_0x01c1
            com.helpshift.util.LinkSpec r6 = new com.helpshift.util.LinkSpec
            r6.<init>()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "tel:"
            r7.append(r8)
            r7.append(r4)
            java.lang.String r4 = r7.toString()
            r6.url = r4
            int r4 = r1.start()
            r6.start = r4
            int r4 = r1.end()
            r6.end = r4
            r3.add(r6)
            goto L_0x01c1
        L_0x01fa:
            com.helpshift.util.HSLinkify$2 r1 = new com.helpshift.util.HSLinkify$2
            r1.<init>()
            java.util.Collections.sort(r3, r1)
            int r1 = r3.size()
            r4 = r1
            r1 = 0
        L_0x0208:
            int r6 = r4 + -1
            if (r1 >= r6) goto L_0x0255
            java.lang.Object r6 = r3.get(r1)
            com.helpshift.util.LinkSpec r6 = (com.helpshift.util.LinkSpec) r6
            int r7 = r1 + 1
            java.lang.Object r8 = r3.get(r7)
            com.helpshift.util.LinkSpec r8 = (com.helpshift.util.LinkSpec) r8
            int r9 = r6.start
            int r10 = r8.start
            if (r9 > r10) goto L_0x0253
            int r9 = r6.end
            int r10 = r8.start
            if (r9 <= r10) goto L_0x0253
            int r9 = r8.end
            int r10 = r6.end
            r11 = -1
            if (r9 > r10) goto L_0x022f
        L_0x022d:
            r6 = r7
            goto L_0x024b
        L_0x022f:
            int r9 = r6.end
            int r10 = r6.start
            int r9 = r9 - r10
            int r10 = r8.end
            int r12 = r8.start
            int r10 = r10 - r12
            if (r9 <= r10) goto L_0x023c
            goto L_0x022d
        L_0x023c:
            int r9 = r6.end
            int r6 = r6.start
            int r9 = r9 - r6
            int r6 = r8.end
            int r8 = r8.start
            int r6 = r6 - r8
            if (r9 >= r6) goto L_0x024a
            r6 = r1
            goto L_0x024b
        L_0x024a:
            r6 = -1
        L_0x024b:
            if (r6 == r11) goto L_0x0253
            r3.remove(r6)
            int r4 = r4 + -1
            goto L_0x0208
        L_0x0253:
            r1 = r7
            goto L_0x0208
        L_0x0255:
            int r1 = r3.size()
            if (r1 != 0) goto L_0x025c
            return r2
        L_0x025c:
            java.util.Iterator r1 = r3.iterator()
        L_0x0260:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x027f
            java.lang.Object r2 = r1.next()
            com.helpshift.util.LinkSpec r2 = (com.helpshift.util.LinkSpec) r2
            com.helpshift.util.HSLinkify$3 r3 = new com.helpshift.util.HSLinkify$3
            java.lang.String r4 = r2.url
            r6 = r21
            r3.<init>(r4, r6, r2)
            int r4 = r2.start
            int r2 = r2.end
            r7 = 33
            r0.setSpan(r3, r4, r2, r7)
            goto L_0x0260
        L_0x027f:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.util.HSLinkify.addLinks(android.text.Spannable, int, com.helpshift.util.HSLinkify$LinkClickListener):boolean");
    }

    public static boolean addLinks(TextView textView, int i, LinkClickListener linkClickListener) {
        if (i == 0) {
            return false;
        }
        CharSequence text = textView.getText();
        if (!(text instanceof Spannable)) {
            SpannableString valueOf = SpannableString.valueOf(text);
            if (!addLinks(valueOf, i, linkClickListener)) {
                return false;
            }
            MovementMethod movementMethod = textView.getMovementMethod();
            if ((movementMethod == null || !(movementMethod instanceof LinkMovementMethod)) && textView.getLinksClickable()) {
                textView.setMovementMethod(LinkMovementMethod.getInstance());
            }
            textView.setText(valueOf);
            return true;
        } else if (!addLinks((Spannable) text, i, linkClickListener)) {
            return false;
        } else {
            MovementMethod movementMethod2 = textView.getMovementMethod();
            if ((movementMethod2 == null || !(movementMethod2 instanceof LinkMovementMethod)) && textView.getLinksClickable()) {
                textView.setMovementMethod(LinkMovementMethod.getInstance());
            }
            return true;
        }
    }

    public static void addLinks(TextView textView, Pattern pattern, String str, MatchFilter matchFilter, TransformFilter transformFilter, LinkClickListener linkClickListener) {
        SpannableString valueOf = SpannableString.valueOf(textView.getText());
        if (addLinks(valueOf, pattern, str, matchFilter, transformFilter, linkClickListener)) {
            textView.setText(valueOf);
            MovementMethod movementMethod = textView.getMovementMethod();
            if ((movementMethod == null || !(movementMethod instanceof LinkMovementMethod)) && textView.getLinksClickable()) {
                textView.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }
    }

    private static boolean addLinks(Spannable spannable, Pattern pattern, String str, MatchFilter matchFilter, TransformFilter transformFilter, LinkClickListener linkClickListener) {
        final String str2;
        boolean z;
        Spannable spannable2 = spannable;
        String str3 = str;
        MatchFilter matchFilter2 = matchFilter;
        TransformFilter transformFilter2 = transformFilter;
        String lowerCase = str3 == null ? "" : str3.toLowerCase(Locale.ROOT);
        Matcher matcher = pattern.matcher(spannable2);
        boolean z2 = false;
        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            if (matchFilter2 != null ? matchFilter2.acceptMatch(spannable2, start, end) : true) {
                String group = matcher.group(0);
                String[] strArr = {lowerCase};
                if (transformFilter2 != null) {
                    group = transformFilter2.transformUrl(matcher, group);
                }
                int i = 0;
                while (true) {
                    if (i >= strArr.length) {
                        str2 = group;
                        z = false;
                        break;
                    }
                    int i2 = i;
                    if (group.regionMatches(true, 0, strArr[i], 0, strArr[i].length())) {
                        if (!group.regionMatches(false, 0, strArr[i2], 0, strArr[i2].length())) {
                            group = strArr[i2] + group.substring(strArr[i2].length());
                        }
                        str2 = group;
                        z = true;
                    } else {
                        i = i2 + 1;
                    }
                }
                if (!z) {
                    str2 = strArr[0] + str2;
                }
                final LinkClickListener linkClickListener2 = linkClickListener;
                spannable2.setSpan(new URLSpan(str2) {
                    public void onClick(View view) {
                        super.onClick(view);
                        if (linkClickListener2 != null) {
                            linkClickListener2.onLinkClicked(str2);
                        }
                    }
                }, start, end, 33);
                z2 = true;
            }
        }
        return z2;
    }
}
