package com.helpshift.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HSJSONUtils {
    private static final String TAG = "HelpshiftDebug";

    public static HashMap<String, Object> toMap(JSONObject jSONObject) throws JSONException {
        HashMap<String, Object> hashMap = new HashMap<>();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            hashMap.put(next, fromJson(jSONObject.get(next)));
        }
        return hashMap;
    }

    public static List toList(JSONArray jSONArray) throws JSONException {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(fromJson(jSONArray.get(i)));
        }
        return arrayList;
    }

    private static Object fromJson(Object obj) throws JSONException {
        if (obj == JSONObject.NULL) {
            return null;
        }
        if (obj instanceof JSONObject) {
            return toMap((JSONObject) obj);
        }
        return obj instanceof JSONArray ? toList((JSONArray) obj) : obj;
    }

    public static JSONObject fromNestedMap(Map<String, ArrayList> map) {
        JSONObject jSONObject = new JSONObject();
        try {
            for (Map.Entry next : map.entrySet()) {
                jSONObject.put((String) next.getKey(), new JSONArray((Collection) next.getValue()));
            }
        } catch (JSONException e) {
            HSLogger.d(TAG, "JSON Exception in parsing complex object", e);
        }
        return jSONObject;
    }

    public static JSONArray fromListOfMaps(List<HashMap> list) {
        JSONArray jSONArray = new JSONArray();
        try {
            for (HashMap next : list) {
                JSONObject jSONObject = new JSONObject();
                for (Object next2 : next.keySet()) {
                    if (next2 instanceof String) {
                        jSONObject.put((String) next2, next.get(next2));
                    }
                }
                jSONArray.put(jSONObject);
            }
        } catch (JSONException e) {
            HSLogger.d(TAG, "JSON Exception in parsing complex list", e);
        }
        return jSONArray;
    }

    public static ArrayList<String> jsonToStringArrayList(String str) {
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            JSONArray jSONArray = new JSONArray(str);
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                arrayList.add(jSONArray.getString(i));
            }
        } catch (JSONException e) {
            HSLogger.d(TAG, "jsonToStringArrayList", e);
        }
        return arrayList;
    }
}
