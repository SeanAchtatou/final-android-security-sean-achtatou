package com.helpshift.util;

import android.text.TextUtils;
import com.unity3d.player.UnityPlayer;

public class UnityUtils {
    private static final String TAG = "Helpshift_UnityUtil";

    public static void sendUnityMessage(String str, String str2, String str3) {
        try {
            if (!TextUtils.isEmpty(str)) {
                HSLogger.d(TAG, "Sending Unity message : " + str + ", Api : " + str2 + ", Message : " + str3);
                UnityPlayer.UnitySendMessage(str, str2, str3);
            }
        } catch (UnsatisfiedLinkError e) {
            HSLogger.e(TAG, "UnsatisfiedLinkError : " + e.getMessage());
        }
    }
}
