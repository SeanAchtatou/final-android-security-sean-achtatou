package com.helpshift.util.constants;

public class KeyValueStoreColumns {
    public static final String key = "key";
    public static final String value = "value";

    private KeyValueStoreColumns() {
    }
}
