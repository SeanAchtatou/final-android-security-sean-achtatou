package com.helpshift.util;

import android.app.Activity;

public class ActivityUtil {
    public static Boolean isFullScreen(Activity activity) {
        return Boolean.valueOf((activity.getWindow().getAttributes().flags & 1024) == 1024);
    }
}
