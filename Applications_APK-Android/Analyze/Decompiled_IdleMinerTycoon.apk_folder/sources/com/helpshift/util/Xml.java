package com.helpshift.util;

import android.content.Context;
import com.ironsource.sdk.constants.Constants;

public class Xml {
    private static final String TAG = "Helpshift_xml";

    public static int getLogoResourceValue(Context context) {
        int attributeResourceValue = getAttributeResourceValue(context, "AndroidManifest.xml", Constants.ParametersKeys.ORIENTATION_APPLICATION, "logo");
        return attributeResourceValue == 0 ? context.getApplicationInfo().icon : attributeResourceValue;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0050, code lost:
        r6 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0051, code lost:
        r1 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0053, code lost:
        r7 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0054, code lost:
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0073, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0050 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x0014] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0079  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int getAttributeResourceValue(android.content.Context r5, java.lang.String r6, java.lang.String r7, java.lang.String r8) {
        /*
            r0 = 0
            r1 = 0
            android.content.pm.ApplicationInfo r2 = r5.getApplicationInfo()     // Catch:{ Exception -> 0x0059 }
            java.lang.String r2 = r2.packageName     // Catch:{ Exception -> 0x0059 }
            android.content.Context r5 = r5.createPackageContext(r2, r0)     // Catch:{ Exception -> 0x0059 }
            android.content.res.AssetManager r5 = r5.getAssets()     // Catch:{ Exception -> 0x0059 }
            android.content.res.XmlResourceParser r5 = r5.openXmlResourceParser(r6)     // Catch:{ Exception -> 0x0059 }
            int r1 = r5.getEventType()     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
            r2 = 0
        L_0x0019:
            r3 = 1
            if (r1 == r3) goto L_0x004a
            r4 = 2
            if (r1 != r4) goto L_0x0045
            java.lang.String r1 = r5.getName()     // Catch:{ Exception -> 0x0043, all -> 0x0050 }
            boolean r1 = r7.equals(r1)     // Catch:{ Exception -> 0x0043, all -> 0x0050 }
            if (r1 == 0) goto L_0x0045
            int r1 = r5.getAttributeCount()     // Catch:{ Exception -> 0x0043, all -> 0x0050 }
            int r1 = r1 - r3
        L_0x002e:
            if (r1 < 0) goto L_0x0045
            java.lang.String r3 = r5.getAttributeName(r1)     // Catch:{ Exception -> 0x0043, all -> 0x0050 }
            boolean r3 = r8.equals(r3)     // Catch:{ Exception -> 0x0043, all -> 0x0050 }
            if (r3 == 0) goto L_0x0040
            int r1 = r5.getAttributeResourceValue(r1, r0)     // Catch:{ Exception -> 0x0043, all -> 0x0050 }
            r2 = r1
            goto L_0x0045
        L_0x0040:
            int r1 = r1 + -1
            goto L_0x002e
        L_0x0043:
            r7 = move-exception
            goto L_0x0055
        L_0x0045:
            int r1 = r5.nextToken()     // Catch:{ Exception -> 0x0043, all -> 0x0050 }
            goto L_0x0019
        L_0x004a:
            if (r5 == 0) goto L_0x0076
            r5.close()
            goto L_0x0076
        L_0x0050:
            r6 = move-exception
            r1 = r5
            goto L_0x0077
        L_0x0053:
            r7 = move-exception
            r2 = 0
        L_0x0055:
            r1 = r5
            goto L_0x005b
        L_0x0057:
            r6 = move-exception
            goto L_0x0077
        L_0x0059:
            r7 = move-exception
            r2 = 0
        L_0x005b:
            java.lang.String r5 = "Helpshift_xml"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r8.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r0 = "Exception parsing xml :"
            r8.append(r0)     // Catch:{ all -> 0x0057 }
            r8.append(r6)     // Catch:{ all -> 0x0057 }
            java.lang.String r6 = r8.toString()     // Catch:{ all -> 0x0057 }
            com.helpshift.util.HSLogger.e(r5, r6, r7)     // Catch:{ all -> 0x0057 }
            if (r1 == 0) goto L_0x0076
            r1.close()
        L_0x0076:
            return r2
        L_0x0077:
            if (r1 == 0) goto L_0x007c
            r1.close()
        L_0x007c:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.util.Xml.getAttributeResourceValue(android.content.Context, java.lang.String, java.lang.String, java.lang.String):int");
    }
}
