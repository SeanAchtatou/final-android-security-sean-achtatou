package com.helpshift.util;

import android.app.Application;
import android.content.Context;
import com.helpshift.HelpshiftUnityAPI;
import com.helpshift.storage.HelpshiftUnityStorage;
import java.util.HashMap;

public class HelpshiftUtil {
    private static final String TAG = "Helpshift_InitUtil";
    private static boolean isHelpshiftInitSuccessful;

    public static boolean installWithCachedCreds(Context context) {
        if (isHelpshiftInitSuccessful) {
            return true;
        }
        try {
            HelpshiftContext.setApplicationContext(context);
            HelpshiftUnityStorage instance = HelpshiftUnityStorage.getInstance(context);
            String string = instance.getString("apiKey");
            String string2 = instance.getString("domainName");
            String string3 = instance.getString("appId");
            HashMap<String, Object> map = instance.getMap(HelpshiftUnityStorage.INSTALL_CONFIG);
            if (map == null) {
                HelpshiftUnityAPI.install((Application) context.getApplicationContext(), string, string2, string3);
            } else {
                HelpshiftUnityAPI.install((Application) context.getApplicationContext(), string, string2, string3, map);
            }
            isHelpshiftInitSuccessful = true;
            return true;
        } catch (Exception e) {
            HSLogger.e(TAG, "Error initializing Helpshift : ", e);
            return false;
        }
    }
}
