package com.helpshift.util;

import android.os.SystemClock;
import com.helpshift.analytics.AnalyticsEventKey;
import com.helpshift.model.InfoModelFactory;
import com.mintegral.msdk.base.b.d;
import java.text.DecimalFormat;

public class TimeUtil {
    @Deprecated
    public static String getAdjustedTimestamp(Float f) {
        DecimalFormat decimalFormat = HSFormat.tsSecFormatter;
        double currentTimeMillis = (double) System.currentTimeMillis();
        Double.isNaN(currentTimeMillis);
        String format = decimalFormat.format(currentTimeMillis / 1000.0d);
        if (f == null || f.floatValue() == 0.0f) {
            return format;
        }
        double currentTimeMillis2 = (double) System.currentTimeMillis();
        Double.isNaN(currentTimeMillis2);
        Double valueOf = Double.valueOf(currentTimeMillis2 / 1000.0d);
        DecimalFormat decimalFormat2 = HSFormat.tsSecFormatter;
        double doubleValue = valueOf.doubleValue();
        double floatValue = (double) f.floatValue();
        Double.isNaN(floatValue);
        return decimalFormat2.format(doubleValue + floatValue);
    }

    public static long getAdjustedTimeInMillis(Float f) {
        long currentTimeMillis = System.currentTimeMillis();
        return (f == null || f.floatValue() == 0.0f) ? currentTimeMillis : (long) (((float) currentTimeMillis) + (f.floatValue() * 1000.0f));
    }

    public static long getCurrentTimeInMillis() {
        return getAdjustedTimeInMillis(InfoModelFactory.getInstance().sdkInfoModel.getServerTimeDelta());
    }

    public static String getCurrentTimestamp() {
        return getAdjustedTimestamp(InfoModelFactory.getInstance().sdkInfoModel.getServerTimeDelta());
    }

    public static String getSinceText(long j) {
        long currentTimeMillis = (System.currentTimeMillis() / 1000) - j;
        if (currentTimeMillis < 60) {
            return currentTimeMillis + AnalyticsEventKey.SEARCH_QUERY;
        } else if (currentTimeMillis < 3600) {
            return (currentTimeMillis / 60) + "m";
        } else if (currentTimeMillis < 86400) {
            return (currentTimeMillis / 3600) + "h";
        } else {
            return (currentTimeMillis / 86400) + d.b;
        }
    }

    public long elapsedTimeMillis() {
        return SystemClock.elapsedRealtime();
    }
}
