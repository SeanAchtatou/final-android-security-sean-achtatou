package com.helpshift.util;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.drive.DriveFile;
import com.helpshift.activities.UnityDelegateActivity;
import java.util.Random;

public class IntentProvider {
    public static PendingIntent wrapPendingIntentWithUnityDelegateActivity(Context context, PendingIntent pendingIntent) {
        Intent intent = new Intent();
        intent.setClass(context, UnityDelegateActivity.class);
        intent.putExtra("delegateIntent", pendingIntent);
        return PendingIntent.getActivity(context, new Random().nextInt(), intent, DriveFile.MODE_READ_ONLY);
    }
}
