package com.helpshift.util;

import android.content.Context;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.webkit.MimeTypeMap;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TJAdUnitConstants;
import com.vungle.warren.model.Advertisement;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class FileUtil {
    public static final String TAG = "FileUtil";
    private static final Set<String> imageMimeTypes = new HashSet(Arrays.asList("image/jpeg", "image/png", "image/gif", "image/x-png", "image/x-citrix-pjpeg", "image/x-citrix-gif", "image/pjpeg"));

    public static boolean isSupportedMimeType(String str) {
        return imageMimeTypes.contains(str);
    }

    public static void saveFile(URL url, File file) {
        InputStream inputStream;
        FileOutputStream fileOutputStream;
        InputStream inputStream2 = null;
        try {
            inputStream = url.openStream();
            try {
                fileOutputStream = new FileOutputStream(file);
            } catch (Exception e) {
                e = e;
                fileOutputStream = null;
                inputStream2 = inputStream;
                try {
                    HSLogger.d(TAG, "saveFile Exception :", e);
                    IOUtils.closeQuitely(inputStream2);
                    IOUtils.closeQuitely(fileOutputStream);
                } catch (Throwable th) {
                    th = th;
                    inputStream = inputStream2;
                    IOUtils.closeQuitely(inputStream);
                    IOUtils.closeQuitely(fileOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream = null;
                IOUtils.closeQuitely(inputStream);
                IOUtils.closeQuitely(fileOutputStream);
                throw th;
            }
            try {
                byte[] bArr = new byte[TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL];
                while (true) {
                    int read = inputStream.read(bArr, 0, bArr.length);
                    if (read < 0) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
                IOUtils.closeQuitely(inputStream);
            } catch (Exception e2) {
                e = e2;
                inputStream2 = inputStream;
                HSLogger.d(TAG, "saveFile Exception :", e);
                IOUtils.closeQuitely(inputStream2);
                IOUtils.closeQuitely(fileOutputStream);
            } catch (Throwable th3) {
                th = th3;
                IOUtils.closeQuitely(inputStream);
                IOUtils.closeQuitely(fileOutputStream);
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            fileOutputStream = null;
            HSLogger.d(TAG, "saveFile Exception :", e);
            IOUtils.closeQuitely(inputStream2);
            IOUtils.closeQuitely(fileOutputStream);
        } catch (Throwable th4) {
            th = th4;
            inputStream = null;
            fileOutputStream = null;
            IOUtils.closeQuitely(inputStream);
            IOUtils.closeQuitely(fileOutputStream);
            throw th;
        }
        IOUtils.closeQuitely(fileOutputStream);
    }

    public static String getMimeType(URL url) {
        try {
            return url.openConnection().getContentType();
        } catch (Exception e) {
            HSLogger.d(TAG, "openConnection() Exception :", e);
            return null;
        }
    }

    public static String getMimeType(String str) {
        try {
            return getMimeType(new URL(Advertisement.FILE_SCHEME + str));
        } catch (MalformedURLException e) {
            HSLogger.d(TAG, "error in getting mimeType :", e);
            return null;
        }
    }

    public static String getFileExtension(String str) {
        int lastIndexOf = str.lastIndexOf(47);
        int lastIndexOf2 = str.lastIndexOf(46);
        return lastIndexOf < lastIndexOf2 ? str.substring(lastIndexOf2) : "";
    }

    public static boolean doesFileFromUriExistAndCanRead(Uri uri, Context context) {
        boolean z = false;
        try {
            ParcelFileDescriptor openFileDescriptor = context.getContentResolver().openFileDescriptor(uri, CampaignEx.JSON_KEY_AD_R);
            if (openFileDescriptor != null) {
                z = true;
            }
            if (openFileDescriptor != null) {
                try {
                    openFileDescriptor.close();
                } catch (IOException unused) {
                }
            }
            return z;
        } catch (Exception e) {
            String str = TAG;
            HSLogger.d(str, "Unable to open input file descriptor for doesFileFromUriExistAndCanRead: " + uri, e);
            return false;
        }
    }

    public static String getFileExtensionFromMimeType(Context context, @NonNull Uri uri) {
        if ("content".equals(uri.getScheme())) {
            return MimeTypeMap.getSingleton().getExtensionFromMimeType(context.getContentResolver().getType(uri));
        }
        return MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());
    }
}
