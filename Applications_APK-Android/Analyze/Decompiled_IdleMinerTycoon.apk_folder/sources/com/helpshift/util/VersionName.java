package com.helpshift.util;

import android.support.annotation.NonNull;

public class VersionName implements Comparable<VersionName> {
    @NonNull
    public final int[] numbers;

    public VersionName(@NonNull String str) {
        String[] split = str.split("-")[0].split("\\.");
        this.numbers = new int[split.length];
        for (int i = 0; i < split.length; i++) {
            this.numbers[i] = Integer.valueOf(split[i]).intValue();
        }
    }

    public int compareTo(@NonNull VersionName versionName) {
        int max = Math.max(this.numbers.length, versionName.numbers.length);
        int i = 0;
        while (i < max) {
            int i2 = i < this.numbers.length ? this.numbers[i] : 0;
            int i3 = i < versionName.numbers.length ? versionName.numbers[i] : 0;
            if (i2 != i3) {
                return i2 < i3 ? -1 : 1;
            }
            i++;
        }
        return 0;
    }

    public boolean isGreaterThanOrEqualTo(VersionName versionName) {
        int compareTo = compareTo(versionName);
        return compareTo == 1 || compareTo == 0;
    }

    public boolean isLessThanOrEqualTo(VersionName versionName) {
        int compareTo = compareTo(versionName);
        return compareTo == -1 || compareTo == 0;
    }
}
