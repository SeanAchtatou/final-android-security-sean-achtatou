package com.helpshift.util;

import android.content.Context;
import android.util.Log;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.support.Support;
import com.helpshift.support.constants.FaqsColumns;
import com.helpshift.support.util.DynamicFormUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class ConfigParserUtil {
    private static final String TAG = "ConfigParserUtil";

    public static Map<String, Object> parseConfigDictionary(Context context, String str) throws JSONException {
        return parseConfigDictionary(context, HSJSONUtils.toMap(new JSONObject(str)));
    }

    public static HashMap parseMetaDataMap(String str) throws JSONException {
        HashMap<String, Object> map = HSJSONUtils.toMap(new JSONObject(str));
        fixMetaData(map);
        return map;
    }

    public static Map<String, Object> parseConfigDictionary(Context context, Map<String, Object> map) {
        if (map == null) {
            return new HashMap();
        }
        Object obj = map.get(SDKConfigurationDM.ENABLE_CONTACT_US);
        if (obj != null) {
            if (obj.equals("yes") || obj.equals("always")) {
                map.put(SDKConfigurationDM.ENABLE_CONTACT_US, Support.EnableContactUs.ALWAYS);
            } else if (obj.equals("no") || obj.equals("never")) {
                map.put(SDKConfigurationDM.ENABLE_CONTACT_US, Support.EnableContactUs.NEVER);
            } else if (obj.equals("after_viewing_faqs")) {
                map.put(SDKConfigurationDM.ENABLE_CONTACT_US, Support.EnableContactUs.AFTER_VIEWING_FAQS);
            } else if (obj.equals("after_marking_answer_unhelpful")) {
                map.put(SDKConfigurationDM.ENABLE_CONTACT_US, Support.EnableContactUs.AFTER_MARKING_ANSWER_UNHELPFUL);
            }
        }
        Object obj2 = map.get("notificationIcon");
        if (obj2 instanceof String) {
            int identifier = context.getResources().getIdentifier((String) obj2, "drawable", context.getPackageName());
            if (identifier != 0) {
                map.put("notificationIcon", Integer.valueOf(identifier));
            } else {
                map.remove("notificationIcon");
            }
        }
        Object obj3 = map.get("largeNotificationIcon");
        if (obj3 instanceof String) {
            int identifier2 = context.getResources().getIdentifier((String) obj3, "drawable", context.getPackageName());
            if (identifier2 != 0) {
                map.put("largeNotificationIcon", Integer.valueOf(identifier2));
            } else {
                map.remove("largeNotificationIcon");
            }
        }
        Object obj4 = map.get("notificationSound");
        if (obj4 instanceof String) {
            int identifier3 = context.getResources().getIdentifier((String) obj4, "raw", context.getPackageName());
            if (identifier3 != 0) {
                map.put("notificationSound", Integer.valueOf(identifier3));
            } else {
                map.remove("notificationSound");
            }
        }
        HashSet hashSet = new HashSet();
        hashSet.add(SDKConfigurationDM.ENABLE_IN_APP_NOTIFICATION);
        hashSet.add(SDKConfigurationDM.REQUIRE_EMAIL);
        hashSet.add(SDKConfigurationDM.HIDE_NAME_AND_EMAIL);
        hashSet.add("enableFullPrivacy");
        hashSet.add(SDKConfigurationDM.SHOW_SEARCH_ON_NEW_CONVERSATION);
        hashSet.add(SDKConfigurationDM.GOTO_CONVERSATION_AFTER_CONTACT_US);
        hashSet.add(SDKConfigurationDM.SHOW_CONVERSATION_RESOLUTION_QUESTION_API);
        hashSet.add("enableDefaultFallbackLanguage");
        hashSet.add("enableChat");
        hashSet.add("enableInboxPolling");
        hashSet.add("enableLogging");
        hashSet.add("disableErrorReporting");
        hashSet.add("disableHelpshiftBranding");
        hashSet.add(SDKConfigurationDM.SHOW_CONVERSATION_INFO_SCREEN);
        hashSet.add(SDKConfigurationDM.DISABLE_APP_LAUNCH_EVENT);
        hashSet.add(SDKConfigurationDM.ENABLE_TYPING_INDICATOR);
        Map<String, Object> replaceWithBoolean = replaceWithBoolean(hashSet, map);
        HashMap hashMap = (HashMap) replaceWithBoolean.get("hs-custom-metadata");
        fixMetaData(hashMap);
        if (hashMap != null) {
            replaceWithBoolean.put("hs-custom-metadata", hashMap);
        }
        HashMap hashMap2 = new HashMap();
        HashMap hashMap3 = (HashMap) replaceWithBoolean.remove(Support.CustomIssueFieldKey);
        if (hashMap3 != null && hashMap3.size() > 0) {
            for (Map.Entry entry : hashMap3.entrySet()) {
                if (entry.getValue() != null) {
                    hashMap2.put(entry.getKey(), ((ArrayList) entry.getValue()).toArray(new String[((ArrayList) entry.getValue()).size()]));
                }
            }
        }
        if (hashMap2.size() > 0) {
            replaceWithBoolean.put(Support.CustomIssueFieldKey, hashMap2);
        }
        HashMap hashMap4 = (HashMap) replaceWithBoolean.get("withTagsMatching");
        if (hashMap4 != null) {
            ArrayList arrayList = (ArrayList) hashMap4.get(FaqsColumns.TAGS);
            if (arrayList != null && arrayList.size() > 0) {
                hashMap4.put(FaqsColumns.TAGS, (String[]) arrayList.toArray(new String[0]));
            }
            replaceWithBoolean.put("withTagsMatching", hashMap4);
        }
        List parseFlowListForKey = parseFlowListForKey(context, replaceWithBoolean, "customContactUsFlows");
        if (parseFlowListForKey != null) {
            replaceWithBoolean.put("customContactUsFlows", parseFlowListForKey);
        }
        return replaceWithBoolean;
    }

    private static List parseFlowListForKey(Context context, Map<String, Object> map, String str) {
        List list;
        try {
            list = (List) map.get(str);
        } catch (ClassCastException e) {
            Log.i(TAG, "parseFlowListForKey", e);
            list = null;
        }
        if (list != null) {
            return parseFlowList(context, list);
        }
        return null;
    }

    public static List parseFlowList(Context context, List<HashMap<String, Object>> list) {
        for (HashMap next : list) {
            if ("dynamicFormFlow".equals((String) next.get("type"))) {
                parseFlowListForKey(context, next, "data");
            } else {
                next.put("config", parseConfigDictionary(context, (HashMap) next.get("config")));
            }
        }
        return DynamicFormUtil.toFlowList(context, list);
    }

    private static Map<String, Object> replaceWithBoolean(HashSet<String> hashSet, Map<String, Object> map) {
        Iterator<String> it = hashSet.iterator();
        while (it.hasNext()) {
            String next = it.next();
            Object obj = map.get(next);
            if (obj instanceof String) {
                if (obj.equals("yes")) {
                    map.put(next, true);
                } else if (obj.equals("no")) {
                    map.put(next, false);
                }
            }
        }
        return map;
    }

    private static void fixMetaData(HashMap<String, Object> hashMap) {
        if (hashMap != null) {
            for (String next : hashMap.keySet()) {
                Object obj = hashMap.get(next);
                if (obj instanceof String) {
                    String obj2 = obj.toString();
                    if (obj2.equalsIgnoreCase("yes") || obj2.equalsIgnoreCase("no")) {
                        hashMap.put(next, Boolean.valueOf(obj2.equalsIgnoreCase("yes") ? "true" : "false"));
                    }
                }
            }
            ArrayList arrayList = (ArrayList) hashMap.get(Support.TagsKey);
            if (arrayList != null && arrayList.size() > 0) {
                hashMap.put(Support.TagsKey, (String[]) arrayList.toArray(new String[0]));
            }
        }
    }
}
