package com.helpshift.util;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.support.annotation.Nullable;
import com.appsflyer.share.Constants;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class AssetsUtil {
    private static final String TAG = "AssetsUtil";

    public static String readFileAsString(Context context, String str) {
        BufferedReader bufferedReader;
        InputStream inputStream;
        try {
            inputStream = context.getAssets().open(str);
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                try {
                    StringBuilder sb = new StringBuilder();
                    while (true) {
                        String readLine = bufferedReader.readLine();
                        if (readLine != null) {
                            sb.append(readLine);
                        } else {
                            String sb2 = sb.toString();
                            IOUtils.closeQuitely(inputStream);
                            IOUtils.closeQuitely(bufferedReader);
                            return sb2;
                        }
                    }
                } catch (IOException e) {
                    e = e;
                    try {
                        HSLogger.e(TAG, "Error reading the file : " + e.getMessage());
                        IOUtils.closeQuitely(inputStream);
                        IOUtils.closeQuitely(bufferedReader);
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        IOUtils.closeQuitely(inputStream);
                        IOUtils.closeQuitely(bufferedReader);
                        throw th;
                    }
                }
            } catch (IOException e2) {
                e = e2;
                bufferedReader = null;
                HSLogger.e(TAG, "Error reading the file : " + e.getMessage());
                IOUtils.closeQuitely(inputStream);
                IOUtils.closeQuitely(bufferedReader);
                return null;
            } catch (Throwable th2) {
                th = th2;
                bufferedReader = null;
                IOUtils.closeQuitely(inputStream);
                IOUtils.closeQuitely(bufferedReader);
                throw th;
            }
        } catch (IOException e3) {
            e = e3;
            inputStream = null;
            bufferedReader = null;
            HSLogger.e(TAG, "Error reading the file : " + e.getMessage());
            IOUtils.closeQuitely(inputStream);
            IOUtils.closeQuitely(bufferedReader);
            return null;
        } catch (Throwable th3) {
            bufferedReader = null;
            th = th3;
            inputStream = null;
            IOUtils.closeQuitely(inputStream);
            IOUtils.closeQuitely(bufferedReader);
            throw th;
        }
    }

    @Nullable
    public static Uri getNotificationSoundUri() {
        Integer num = HelpshiftContext.getCoreApi().getSDKConfigurationDM().getInt(SDKConfigurationDM.NOTIFICATION_SOUND_ID);
        if (num == null) {
            return null;
        }
        return Uri.parse("android.resource://" + HelpshiftContext.getApplicationContext().getPackageName() + Constants.URL_PATH_DELIMITER + num);
    }

    @Nullable
    public static boolean resourceExists(Context context, Integer num) {
        if (!(context == null || num == null)) {
            try {
                return context.getResources().getResourceName(num.intValue()) != null;
            } catch (Resources.NotFoundException unused) {
            }
        }
        return false;
    }
}
