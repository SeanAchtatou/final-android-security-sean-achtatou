package com.helpshift.util;

import com.helpshift.network.connectivity.HSConnectivityManager;

public class ConnectivityUtil {
    private final int defaultBatchSize;
    private final int maximumBatchSize;

    public ConnectivityUtil(int i, int i2) {
        this.defaultBatchSize = i;
        this.maximumBatchSize = i2;
    }

    public int getBatchSize() {
        switch (HSConnectivityManager.getInstance().getConnectivityType()) {
            case UNKNOWN:
                return this.defaultBatchSize;
            case WIFI:
                return this.maximumBatchSize;
            case MOBILE_4G:
                return this.defaultBatchSize * 4;
            case MOBILE_2G:
                return this.defaultBatchSize / 2;
            default:
                return this.defaultBatchSize;
        }
    }
}
