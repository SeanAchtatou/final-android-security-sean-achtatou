package com.helpshift.util;

import android.content.Context;
import android.os.Build;
import android.os.LocaleList;
import android.text.TextUtils;
import com.helpshift.model.InfoModelFactory;
import java.util.Locale;

public class LocaleUtil {
    public static String getAcceptLanguageHeader() {
        String sdkLanguage = InfoModelFactory.getInstance().sdkInfoModel.getSdkLanguage();
        return TextUtils.isEmpty(sdkLanguage) ? Locale.getDefault().toString() : sdkLanguage;
    }

    public static String getCountry(Context context) {
        Locale locale;
        if (Build.VERSION.SDK_INT >= 24) {
            LocaleList locales = context.getResources().getConfiguration().getLocales();
            locale = locales.size() > 0 ? locales.get(0) : null;
        } else {
            locale = context.getResources().getConfiguration().locale;
        }
        if (locale != null) {
            return locale.getCountry();
        }
        return "";
    }
}
