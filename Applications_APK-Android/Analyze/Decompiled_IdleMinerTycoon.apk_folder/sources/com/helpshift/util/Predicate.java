package com.helpshift.util;

public interface Predicate<T> {
    boolean matches(Object obj);
}
