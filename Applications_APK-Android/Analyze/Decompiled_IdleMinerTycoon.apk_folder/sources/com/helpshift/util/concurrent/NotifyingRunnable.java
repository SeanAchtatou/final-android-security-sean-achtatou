package com.helpshift.util.concurrent;

import com.helpshift.util.HSLogger;

public class NotifyingRunnable implements Runnable {
    private static final String TAG = "Helpshift_NotiRunnable";
    private boolean isFinished;
    private final Runnable runnable;
    private final Object syncLock = new Object();

    NotifyingRunnable(Runnable runnable2) {
        this.runnable = runnable2;
    }

    public void waitForCompletion() {
        synchronized (this.syncLock) {
            try {
                if (!this.isFinished) {
                    this.syncLock.wait();
                }
            } catch (InterruptedException e) {
                HSLogger.d(TAG, "Exception in NotifyingRunnable", e);
                Thread.currentThread().interrupt();
            }
        }
    }

    public void run() {
        synchronized (this.syncLock) {
            try {
                this.runnable.run();
                this.isFinished = true;
                this.syncLock.notifyAll();
            } catch (Throwable th) {
                this.isFinished = true;
                this.syncLock.notifyAll();
                throw th;
            }
        }
    }
}
