package com.helpshift.widget;

import com.helpshift.conversation.activeconversation.message.HistoryLoadingState;

public class HistoryLoadingViewState extends HSBaseObservable {
    protected HistoryLoadingState state = HistoryLoadingState.NONE;

    public HistoryLoadingState getState() {
        return this.state;
    }

    /* access modifiers changed from: protected */
    public void notifyInitialState() {
        notifyChange(this);
    }
}
