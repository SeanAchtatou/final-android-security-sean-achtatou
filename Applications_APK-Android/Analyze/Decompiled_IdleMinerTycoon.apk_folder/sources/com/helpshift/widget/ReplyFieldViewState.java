package com.helpshift.widget;

public class ReplyFieldViewState extends HSBaseObservable {
    protected boolean isEnabled;
    protected String replyText = "";

    protected ReplyFieldViewState(boolean z) {
        this.isEnabled = z;
    }

    public boolean isEnabled() {
        return this.isEnabled;
    }

    public String getReplyText() {
        return this.replyText;
    }

    /* access modifiers changed from: protected */
    public void notifyInitialState() {
        notifyChange(this);
    }
}
