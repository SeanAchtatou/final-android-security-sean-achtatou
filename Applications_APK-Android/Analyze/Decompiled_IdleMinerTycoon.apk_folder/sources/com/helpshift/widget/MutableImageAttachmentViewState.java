package com.helpshift.widget;

import com.helpshift.conversation.dto.ImagePickerFile;

public class MutableImageAttachmentViewState extends ImageAttachmentViewState {
    public void setImagePickerFile(ImagePickerFile imagePickerFile) {
        this.imagePickerFile = imagePickerFile;
        notifyChange(this);
    }

    public void setClickable(boolean z) {
        this.clickable = z;
        notifyChange(this);
    }
}
