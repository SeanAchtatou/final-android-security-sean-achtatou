package com.helpshift.widget;

public class ProgressDescriptionViewState extends HSBaseObservable {
    protected boolean isVisible = false;

    public boolean isVisible() {
        return this.isVisible;
    }

    /* access modifiers changed from: protected */
    public void notifyInitialState() {
        notifyChange(this);
    }
}
