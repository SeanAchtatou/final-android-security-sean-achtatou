package com.helpshift.widget;

public class MutableReplyFieldViewState extends ReplyFieldViewState {
    MutableReplyFieldViewState(boolean z) {
        super(z);
    }

    public void setEnabled(boolean z) {
        if (this.isEnabled != z) {
            this.isEnabled = z;
            notifyChange(this);
        }
    }

    public void clearReplyText() {
        this.replyText = "";
        notifyChange(this);
    }

    public void setReplyText(String str) {
        if (str != null && !str.equals(this.replyText)) {
            this.replyText = str;
            notifyChange(this);
        }
    }
}
