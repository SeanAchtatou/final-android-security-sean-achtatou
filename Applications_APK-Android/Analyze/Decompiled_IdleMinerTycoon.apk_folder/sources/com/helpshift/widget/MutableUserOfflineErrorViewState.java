package com.helpshift.widget;

public class MutableUserOfflineErrorViewState extends UserOfflineErrorViewState {
    public void setVisible(boolean z) {
        if (this.isVisible != z) {
            this.isVisible = z;
            notifyChange(this);
        }
    }
}
