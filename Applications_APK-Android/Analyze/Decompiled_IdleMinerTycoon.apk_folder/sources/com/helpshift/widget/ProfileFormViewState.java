package com.helpshift.widget;

public class ProfileFormViewState extends HSBaseObservable {
    protected boolean isVisible;

    public boolean isVisible() {
        return this.isVisible;
    }

    /* access modifiers changed from: protected */
    public void notifyInitialState() {
        notifyChange(this);
    }

    public void setVisible(boolean z) {
        this.isVisible = z;
    }
}
