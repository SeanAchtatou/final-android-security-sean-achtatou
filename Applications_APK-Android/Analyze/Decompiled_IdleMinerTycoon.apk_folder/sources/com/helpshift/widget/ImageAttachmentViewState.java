package com.helpshift.widget;

import com.helpshift.conversation.dto.ImagePickerFile;

public class ImageAttachmentViewState extends HSBaseObservable {
    protected boolean clickable = true;
    protected ImagePickerFile imagePickerFile;

    public ImagePickerFile getImagePickerFile() {
        return this.imagePickerFile;
    }

    public String getImagePath() {
        return (this.imagePickerFile == null || this.imagePickerFile.filePath == null) ? "" : this.imagePickerFile.filePath;
    }

    public boolean isClickable() {
        return this.clickable;
    }

    /* access modifiers changed from: protected */
    public void notifyInitialState() {
        notifyChange(this);
    }
}
