package com.helpshift.widget;

import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.F;

public abstract class HSBaseObservable {
    private Domain domain;
    /* access modifiers changed from: private */
    public HSObserver viewObserver;

    /* access modifiers changed from: protected */
    public abstract void notifyInitialState();

    public void subscribe(Domain domain2, HSObserver hSObserver) {
        this.domain = domain2;
        this.viewObserver = hSObserver;
        notifyInitialState();
    }

    public void unsubscribe() {
        this.viewObserver = null;
    }

    public void notifyChange(final Object obj) {
        if (this.viewObserver != null && this.domain != null) {
            this.domain.runOnUI(new F() {
                public void f() {
                    if (HSBaseObservable.this.viewObserver != null) {
                        HSBaseObservable.this.viewObserver.onChanged(obj);
                    }
                }
            });
        }
    }
}
