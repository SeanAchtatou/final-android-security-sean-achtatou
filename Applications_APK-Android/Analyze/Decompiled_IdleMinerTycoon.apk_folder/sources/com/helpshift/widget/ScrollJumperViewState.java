package com.helpshift.widget;

public class ScrollJumperViewState extends HSBaseObservable {
    protected boolean isVisible;
    protected boolean shouldShowUnreadMessagesIndicator;

    protected ScrollJumperViewState(boolean z, boolean z2) {
        this.isVisible = z;
        this.shouldShowUnreadMessagesIndicator = z2;
    }

    public boolean isVisible() {
        return this.isVisible;
    }

    public boolean shouldShowUnreadMessagesIndicator() {
        return this.shouldShowUnreadMessagesIndicator;
    }

    /* access modifiers changed from: protected */
    public void notifyInitialState() {
        notifyChange(this);
    }
}
