package com.helpshift.widget;

import com.helpshift.conversation.activeconversation.message.ConversationFooterState;

public class ConversationFooterViewState extends HSBaseObservable {
    protected ConversationFooterState state = ConversationFooterState.NONE;

    public ConversationFooterState getState() {
        return this.state;
    }

    /* access modifiers changed from: protected */
    public void notifyInitialState() {
        notifyChange(this);
    }
}
