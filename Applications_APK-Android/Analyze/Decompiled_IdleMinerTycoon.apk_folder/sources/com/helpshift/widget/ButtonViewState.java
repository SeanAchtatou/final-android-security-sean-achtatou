package com.helpshift.widget;

public class ButtonViewState extends HSBaseObservable {
    protected boolean isEnabled = true;
    protected boolean isVisible = true;

    public boolean isEnabled() {
        return this.isEnabled;
    }

    public boolean isVisible() {
        return this.isVisible;
    }

    /* access modifiers changed from: protected */
    public void notifyInitialState() {
        notifyChange(this);
    }
}
