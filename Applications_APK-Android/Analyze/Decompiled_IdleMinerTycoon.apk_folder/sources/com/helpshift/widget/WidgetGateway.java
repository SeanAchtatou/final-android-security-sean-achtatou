package com.helpshift.widget;

import com.helpshift.common.StringUtils;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.conversation.activeconversation.message.ConversationFooterState;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.domainmodel.ConversationController;
import com.helpshift.conversation.dto.ConversationDetailDTO;
import com.helpshift.conversation.dto.IssueState;
import java.util.concurrent.TimeUnit;

public class WidgetGateway {
    private final SDKConfigurationDM config;
    private final ConversationController conversationController;

    public WidgetGateway(SDKConfigurationDM sDKConfigurationDM, ConversationController conversationController2) {
        this.config = sDKConfigurationDM;
        this.conversationController = conversationController2;
    }

    public MutableButtonViewState makeStartConversationButtonViewState() {
        MutableButtonViewState mutableButtonViewState = new MutableButtonViewState();
        mutableButtonViewState.setVisible(!this.conversationController.isCreateConversationInProgress());
        return mutableButtonViewState;
    }

    public MutableReplyFieldViewState makeReplyFieldViewState() {
        return new MutableReplyFieldViewState(true);
    }

    public MutableButtonViewState makeReplyBoxViewState(Conversation conversation, boolean z) {
        MutableButtonViewState mutableButtonViewState = new MutableButtonViewState();
        updateReplyBoxWidget(mutableButtonViewState, conversation, z);
        return mutableButtonViewState;
    }

    public void updateReplyBoxWidget(MutableButtonViewState mutableButtonViewState, Conversation conversation, boolean z) {
        boolean z2 = true;
        if (conversation.isRedacted || (!conversation.isIssueInProgress() && (conversation.state != IssueState.RESOLUTION_REJECTED || !z))) {
            z2 = false;
        }
        mutableButtonViewState.setVisible(z2);
    }

    public MutableScrollJumperViewState makeScrollJumperViewState() {
        return new MutableScrollJumperViewState(false, false);
    }

    public boolean getDefaultVisibilityForConversationInfoButtonWidget(Conversation conversation) {
        return this.config.getBoolean(SDKConfigurationDM.SHOW_CONVERSATION_INFO_SCREEN);
    }

    public MutableConversationFooterViewState makeConversationFooterViewState(Conversation conversation, boolean z) {
        MutableConversationFooterViewState mutableConversationFooterViewState = new MutableConversationFooterViewState();
        updateConversationFooterViewState(mutableConversationFooterViewState, conversation, z);
        return mutableConversationFooterViewState;
    }

    public void updateConversationFooterViewState(MutableConversationFooterViewState mutableConversationFooterViewState, Conversation conversation, boolean z) {
        ConversationFooterState conversationFooterState = ConversationFooterState.NONE;
        if (conversation.isRedacted) {
            conversationFooterState = ConversationFooterState.REDACTED_STATE;
        } else if (conversation.state == IssueState.RESOLUTION_ACCEPTED) {
            if (this.conversationController.conversationManager.shouldShowCSATInFooter(conversation)) {
                conversationFooterState = ConversationFooterState.CSAT_RATING;
            } else {
                conversationFooterState = ConversationFooterState.START_NEW_CONVERSATION;
            }
        } else if (conversation.state == IssueState.REJECTED) {
            conversationFooterState = ConversationFooterState.REJECTED_MESSAGE;
        } else if (conversation.state == IssueState.ARCHIVED) {
            conversationFooterState = ConversationFooterState.ARCHIVAL_MESSAGE;
        } else if (conversation.state == IssueState.RESOLUTION_REQUESTED && this.config.shouldShowConversationResolutionQuestion()) {
            conversationFooterState = ConversationFooterState.CONVERSATION_ENDED_MESSAGE;
        } else if (conversation.state == IssueState.RESOLUTION_REJECTED) {
            if (z) {
                conversationFooterState = ConversationFooterState.NONE;
            } else if (this.conversationController.conversationManager.shouldShowCSATInFooter(conversation)) {
                conversationFooterState = ConversationFooterState.CSAT_RATING;
            } else {
                conversationFooterState = ConversationFooterState.START_NEW_CONVERSATION;
            }
        } else if (conversation.state == IssueState.AUTHOR_MISMATCH) {
            conversationFooterState = ConversationFooterState.AUTHOR_MISMATCH;
        }
        mutableConversationFooterViewState.setState(conversationFooterState);
    }

    public MutableButtonViewState makeConfirmationBoxViewState(Conversation conversation) {
        MutableButtonViewState mutableButtonViewState = new MutableButtonViewState();
        updateConfirmationBoxViewState(mutableButtonViewState, conversation);
        return mutableButtonViewState;
    }

    public void updateConfirmationBoxViewState(MutableButtonViewState mutableButtonViewState, Conversation conversation) {
        mutableButtonViewState.setVisible(!conversation.isRedacted && conversation.state == IssueState.RESOLUTION_REQUESTED && this.config.shouldShowConversationResolutionQuestion());
    }

    public MutableButtonViewState makeAttachImageButtonViewState(Conversation conversation) {
        MutableButtonViewState mutableButtonViewState = new MutableButtonViewState();
        mutableButtonViewState.setVisible(getDefaultVisibilityForAttachImageButton(conversation));
        return mutableButtonViewState;
    }

    public MutableButtonViewState makeNewConversationAttachImageButtonViewState(MutableImageAttachmentViewState mutableImageAttachmentViewState) {
        MutableButtonViewState mutableButtonViewState = new MutableButtonViewState();
        mutableButtonViewState.setVisible(getVisibilityForNewConversationAttachImageButton(mutableImageAttachmentViewState));
        return mutableButtonViewState;
    }

    private boolean getVisibilityForNewConversationAttachImageButton(MutableImageAttachmentViewState mutableImageAttachmentViewState) {
        if (!getDefaultVisibilityForAttachImageButtonNewConversation() || !StringUtils.isEmpty(mutableImageAttachmentViewState.getImagePath()) || this.conversationController.isCreateConversationInProgress()) {
            return false;
        }
        return true;
    }

    public boolean getDefaultVisibilityForAttachImageButton(Conversation conversation) {
        return getDefaultVisibilityForAttachImageButtonNewConversation();
    }

    public boolean getDefaultVisibilityForAttachImageButtonNewConversation() {
        return !this.config.getBoolean(SDKConfigurationDM.ENABLE_FULL_PRIVACY) && this.config.getBoolean(SDKConfigurationDM.ALLOW_USER_ATTACHMENTS);
    }

    public MutableTextViewState makeDescriptionViewState() {
        String str;
        MutableTextViewState mutableTextViewState = new MutableTextViewState(true);
        String conversationArchivalPrefillText = this.conversationController.getConversationArchivalPrefillText();
        String string = this.config.getString(SDKConfigurationDM.CONVERSATION_PRE_FILL_TEXT);
        ConversationDetailDTO conversationDetail = this.conversationController.getConversationDetail();
        if (conversationDetail == null || conversationDetail.type != 1) {
            str = "";
        } else {
            str = conversationDetail.title;
            long nanoTime = System.nanoTime() - conversationDetail.timestamp;
            if (nanoTime < 0 || TimeUnit.NANOSECONDS.toSeconds(nanoTime) > 7200) {
                this.conversationController.saveDescriptionDetail("", 0);
                str = "";
            }
        }
        if (StringUtils.isEmpty(str)) {
            if (!StringUtils.isEmpty(conversationArchivalPrefillText)) {
                this.conversationController.saveDescriptionDetail(conversationArchivalPrefillText, 3);
                str = conversationArchivalPrefillText;
            } else if (!StringUtils.isEmpty(string)) {
                this.conversationController.saveDescriptionDetail(string, 2);
                str = string;
            } else {
                str = "";
            }
        }
        mutableTextViewState.setText(str);
        return mutableTextViewState;
    }

    public void save(MutableTextViewState mutableTextViewState) {
        this.conversationController.saveDescriptionDetail(mutableTextViewState.getText(), 1);
    }

    public MutableTextViewState makeNameViewState() {
        MutableTextViewState mutableTextViewState = new MutableTextViewState(true);
        mutableTextViewState.setText(!this.config.shouldCreateConversationAnonymously() ? this.conversationController.getName() : "Anonymous");
        return mutableTextViewState;
    }

    public MutableTextViewState makeEmailViewState() {
        MutableTextViewState mutableTextViewState = new MutableTextViewState(isEmailRequired());
        if (!this.config.shouldCreateConversationAnonymously()) {
            mutableTextViewState.setText(this.conversationController.getEmail());
        }
        return mutableTextViewState;
    }

    private boolean isEmailRequired() {
        if (this.config.getBoolean(SDKConfigurationDM.ENABLE_FULL_PRIVACY)) {
            return false;
        }
        if (this.config.getBoolean(SDKConfigurationDM.REQUIRE_NAME_AND_EMAIL)) {
            return true;
        }
        if (!this.config.getBoolean(SDKConfigurationDM.PROFILE_FORM_ENABLE) || !this.config.getBoolean(SDKConfigurationDM.REQUIRE_EMAIL)) {
            return false;
        }
        return true;
    }

    public MutableImageAttachmentViewState makeImageAttachmentWidget() {
        MutableImageAttachmentViewState mutableImageAttachmentViewState = new MutableImageAttachmentViewState();
        if (this.config.getBoolean(SDKConfigurationDM.ENABLE_FULL_PRIVACY)) {
            mutableImageAttachmentViewState.setImagePickerFile(null);
            save(mutableImageAttachmentViewState);
        } else {
            mutableImageAttachmentViewState.setImagePickerFile(this.conversationController.getImageAttachmentDraft());
            mutableImageAttachmentViewState.setClickable(!this.conversationController.isCreateConversationInProgress());
        }
        return mutableImageAttachmentViewState;
    }

    public void save(MutableImageAttachmentViewState mutableImageAttachmentViewState) {
        this.conversationController.saveImageAttachmentDraft(mutableImageAttachmentViewState.getImagePickerFile());
    }

    public MutableProfileFormViewState makeProfileFormViewState(TextViewState textViewState, TextViewState textViewState2) {
        MutableProfileFormViewState mutableProfileFormViewState = new MutableProfileFormViewState();
        mutableProfileFormViewState.setVisible(isProfileFormVisible(textViewState, textViewState2));
        return mutableProfileFormViewState;
    }

    private boolean isProfileFormVisible(TextViewState textViewState, TextViewState textViewState2) {
        if (this.config.getBoolean(SDKConfigurationDM.ENABLE_FULL_PRIVACY)) {
            return false;
        }
        boolean z = this.config.getBoolean(SDKConfigurationDM.PROFILE_FORM_ENABLE);
        boolean z2 = this.config.getBoolean(SDKConfigurationDM.HIDE_NAME_AND_EMAIL);
        boolean z3 = textViewState.getText().length() > 0;
        boolean z4 = textViewState2.getText().length() > 0;
        if (!this.config.getBoolean(SDKConfigurationDM.REQUIRE_NAME_AND_EMAIL) || !z2) {
            if (!z) {
                return false;
            }
            if (!z2 || ((this.config.getBoolean(SDKConfigurationDM.REQUIRE_EMAIL) && !z4) || !z3)) {
                return true;
            }
            return false;
        } else if (!z3 || !z4) {
            return true;
        } else {
            return false;
        }
    }

    public MutableProgressBarViewState makeProgressBarViewState() {
        MutableProgressBarViewState mutableProgressBarViewState = new MutableProgressBarViewState();
        mutableProgressBarViewState.setVisible(this.conversationController.isCreateConversationInProgress());
        return mutableProgressBarViewState;
    }
}
