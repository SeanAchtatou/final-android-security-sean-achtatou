package com.helpshift.widget;

public class MutableProgressDescriptionViewState extends ProgressDescriptionViewState {
    public void setVisible(boolean z) {
        if (this.isVisible != z) {
            this.isVisible = z;
            notifyChange(this);
        }
    }
}
