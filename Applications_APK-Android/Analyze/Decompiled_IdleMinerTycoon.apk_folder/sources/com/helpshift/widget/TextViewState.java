package com.helpshift.widget;

import java.util.regex.Pattern;

public class TextViewState extends HSBaseObservable {
    public static final Pattern specialCharactersPattern = Pattern.compile("\\W+");
    protected TextViewStatesError error;
    private boolean isRequired;
    protected String text;

    public enum TextViewStatesError {
        EMPTY,
        LESS_THAN_MINIMUM_LENGTH,
        ONLY_SPECIAL_CHARACTERS,
        INVALID_EMAIL
    }

    protected TextViewState(boolean z) {
        this.isRequired = z;
    }

    public TextViewStatesError getError() {
        return this.error;
    }

    public String getText() {
        return this.text == null ? "" : this.text.trim();
    }

    public boolean isRequired() {
        return this.isRequired;
    }

    /* access modifiers changed from: protected */
    public void notifyInitialState() {
        notifyChange(this);
    }
}
