package com.helpshift.widget;

public class MutableProgressBarViewState extends ProgressBarViewState {
    public void setVisible(boolean z) {
        if (this.isVisible != z) {
            this.isVisible = z;
            notifyChange(this);
        }
    }
}
