package com.helpshift.widget;

import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.domainmodel.ConversationController;

public class ConversationalWidgetGateway extends WidgetGateway {
    public ConversationalWidgetGateway(SDKConfigurationDM sDKConfigurationDM, ConversationController conversationController) {
        super(sDKConfigurationDM, conversationController);
    }

    public boolean getDefaultVisibilityForConversationInfoButtonWidget(Conversation conversation) {
        return !conversation.isInPreIssueMode() && super.getDefaultVisibilityForConversationInfoButtonWidget(conversation);
    }

    public boolean getDefaultVisibilityForAttachImageButton(Conversation conversation) {
        return !conversation.isInPreIssueMode() && super.getDefaultVisibilityForAttachImageButton(conversation);
    }
}
