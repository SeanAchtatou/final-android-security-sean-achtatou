package com.helpshift.android.commons.downloader.runnable;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.text.TextUtils;
import com.appsflyer.share.Constants;
import com.helpshift.android.commons.downloader.HsUriUtils;
import com.helpshift.android.commons.downloader.contracts.DownloadRequestedFileInfo;
import com.helpshift.android.commons.downloader.contracts.NetworkAuthDataFetcher;
import com.helpshift.android.commons.downloader.contracts.OnDownloadFinishListener;
import com.helpshift.android.commons.downloader.contracts.OnProgressChangedListener;
import com.helpshift.android.commons.downloader.storage.DownloadInProgressCacheDbStorage;
import com.helpshift.util.HSLogger;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.io.EOFException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

public class MediaStoreDownloadRunnable extends BaseDownloadRunnable {
    private static final String TAG = "Helpshift_mediaRun";
    private Context context;
    private DownloadInProgressCacheDbStorage downloadInProgressCacheDbStorage;

    /* access modifiers changed from: protected */
    public boolean isGzipSupported() {
        return false;
    }

    public MediaStoreDownloadRunnable(Context context2, DownloadRequestedFileInfo downloadRequestedFileInfo, DownloadInProgressCacheDbStorage downloadInProgressCacheDbStorage2, NetworkAuthDataFetcher networkAuthDataFetcher, OnProgressChangedListener onProgressChangedListener, OnDownloadFinishListener onDownloadFinishListener) {
        super(downloadRequestedFileInfo, networkAuthDataFetcher, onProgressChangedListener, onDownloadFinishListener);
        this.context = context2;
        this.downloadInProgressCacheDbStorage = downloadInProgressCacheDbStorage2;
    }

    /* access modifiers changed from: protected */
    public long getAlreadyDownloadedBytes() {
        Uri cachedFileUri = getCachedFileUri();
        if (cachedFileUri == null) {
            return 0;
        }
        try {
            ParcelFileDescriptor openFileDescriptor = this.context.getContentResolver().openFileDescriptor(cachedFileUri, CampaignEx.JSON_KEY_AD_R);
            if (openFileDescriptor != null) {
                return openFileDescriptor.getStatSize();
            }
            return 0;
        } catch (Exception e) {
            HSLogger.e(TAG, "Exception while getting file size via Uri", e);
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    public void clearCache() {
        Uri cachedFileUri = getCachedFileUri();
        this.downloadInProgressCacheDbStorage.removeFilePath();
        deleteUri(cachedFileUri);
    }

    /* access modifiers changed from: protected */
    public void processHttpResponse(InputStream inputStream, int i) throws IOException {
        FileOutputStream fileOutputStream;
        ParcelFileDescriptor parcelFileDescriptor;
        long alreadyDownloadedBytes = getAlreadyDownloadedBytes();
        Uri fileUriToWriteResponseData = getFileUriToWriteResponseData();
        if (fileUriToWriteResponseData == null) {
            notifyDownloadFinish(false, null);
            return;
        }
        this.downloadInProgressCacheDbStorage.insertFilePath(fileUriToWriteResponseData.toString());
        try {
            parcelFileDescriptor = this.context.getContentResolver().openFileDescriptor(fileUriToWriteResponseData, "w");
            if (parcelFileDescriptor == null) {
                try {
                    notifyDownloadFinish(false, null);
                    closeFileStream(null);
                    HsUriUtils.closeParcelFileDescriptor(parcelFileDescriptor);
                } catch (Throwable th) {
                    th = th;
                    fileOutputStream = null;
                    closeFileStream(fileOutputStream);
                    HsUriUtils.closeParcelFileDescriptor(parcelFileDescriptor);
                    throw th;
                }
            } else {
                fileOutputStream = new FileOutputStream(parcelFileDescriptor.getFileDescriptor());
                try {
                    byte[] bArr = new byte[8192];
                    long j = 0;
                    while (true) {
                        int read = inputStream.read(bArr, 0, 8192);
                        if (read == -1) {
                            updateIsPendingFlag(fileUriToWriteResponseData, this.requestInfo.contentType);
                            this.downloadInProgressCacheDbStorage.removeFilePath();
                            HSLogger.d(TAG, "Download finished : " + this.requestInfo.url + "\n URI : " + fileUriToWriteResponseData);
                            notifyDownloadFinish(true, fileUriToWriteResponseData);
                            closeFileStream(fileOutputStream);
                            HsUriUtils.closeParcelFileDescriptor(parcelFileDescriptor);
                            return;
                        } else if (read >= 0) {
                            fileOutputStream.write(bArr, 0, read);
                            long statSize = (long) ((((float) parcelFileDescriptor.getStatSize()) / ((float) (((long) i) + alreadyDownloadedBytes))) * 100.0f);
                            if (statSize != j) {
                                notifyProgressChange((int) statSize);
                                j = statSize;
                            }
                        } else {
                            throw new EOFException();
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    closeFileStream(fileOutputStream);
                    HsUriUtils.closeParcelFileDescriptor(parcelFileDescriptor);
                    throw th;
                }
            }
        } catch (Throwable th3) {
            th = th3;
            parcelFileDescriptor = null;
            fileOutputStream = null;
            closeFileStream(fileOutputStream);
            HsUriUtils.closeParcelFileDescriptor(parcelFileDescriptor);
            throw th;
        }
    }

    private Uri getFileUriToWriteResponseData() {
        Uri cachedFileUri = getCachedFileUri();
        if (cachedFileUri != null) {
            return cachedFileUri;
        }
        return createFile(generateFileName(), this.requestInfo.contentType);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private Uri createFile(String str, String str2) {
        Uri uri;
        if (Build.VERSION.SDK_INT < 29) {
            return null;
        }
        ContentValues contentValues = new ContentValues();
        ContentResolver contentResolver = this.context.getContentResolver();
        if (isImageType(str2)) {
            contentValues.put("_display_name", str);
            contentValues.put("mime_type", str2);
            contentValues.put("is_pending", (Integer) 1);
            uri = MediaStore.Images.Media.getContentUri("external_primary");
        } else {
            contentValues.put("_display_name", str);
            contentValues.put("mime_type", str2);
            contentValues.put("is_pending", (Integer) 1);
            uri = MediaStore.Downloads.getContentUri("external_primary");
        }
        return contentResolver.insert(uri, contentValues);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void updateIsPendingFlag(Uri uri, String str) {
        if (Build.VERSION.SDK_INT >= 29) {
            ContentValues contentValues = new ContentValues();
            if (isImageType(str)) {
                contentValues.put("is_pending", (Integer) 0);
            } else {
                contentValues.put("is_pending", (Integer) 0);
            }
            this.context.getContentResolver().update(uri, contentValues, null, null);
        }
    }

    private String generateFileName() {
        return "Support_" + System.currentTimeMillis() + this.requestInfo.url.substring(this.requestInfo.url.lastIndexOf(Constants.URL_PATH_DELIMITER) + 1);
    }

    private Uri getCachedFileUri() {
        String filePath = this.downloadInProgressCacheDbStorage.getFilePath();
        if (TextUtils.isEmpty(filePath)) {
            return null;
        }
        Uri buildUri = buildUri(filePath);
        if (buildUri != null) {
            return buildUri;
        }
        this.downloadInProgressCacheDbStorage.removeFilePath();
        return null;
    }

    private Uri buildUri(String str) {
        if (!HsUriUtils.canReadFileAtUri(this.context, str)) {
            return null;
        }
        try {
            return Uri.parse(str);
        } catch (Exception e) {
            HSLogger.e(TAG, "Error while converting filePath to uri", e);
            return null;
        }
    }

    private boolean isImageType(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            return Pattern.compile("image/.*").matcher(str).matches();
        } catch (Exception e) {
            HSLogger.e(TAG, "Error when check image mime type", e);
            return false;
        }
    }

    private void deleteUri(Uri uri) {
        if (uri != null) {
            try {
                this.context.getContentResolver().delete(uri, null, null);
            } catch (Exception e) {
                HSLogger.e(TAG, "Error when deleting a file via uri", e);
            }
        }
    }
}
