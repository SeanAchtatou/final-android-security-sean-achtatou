package com.helpshift.android.commons.downloader.storage;

import com.helpshift.android.commons.downloader.contracts.DownloaderKeyValueStorage;

public class DownloadManagerCacheDbStorage extends BaseCacheDbStorage {
    private static final String KEY_CACHED_DOWNLOADS = "hs-cached-downloads";

    /* access modifiers changed from: package-private */
    public String getStorageKey() {
        return KEY_CACHED_DOWNLOADS;
    }

    public DownloadManagerCacheDbStorage(DownloaderKeyValueStorage downloaderKeyValueStorage) {
        super(downloaderKeyValueStorage);
    }
}
