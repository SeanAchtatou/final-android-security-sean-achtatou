package com.helpshift.android.commons.downloader.runnable;

import android.os.Build;
import com.facebook.share.internal.ShareConstants;
import com.helpshift.android.commons.downloader.HelpshiftSSLSocketFactory;
import com.helpshift.android.commons.downloader.contracts.DownloadRequestedFileInfo;
import com.helpshift.android.commons.downloader.contracts.NetworkAuthDataFetcher;
import com.helpshift.android.commons.downloader.contracts.OnDownloadFinishListener;
import com.helpshift.android.commons.downloader.contracts.OnProgressChangedListener;
import com.ironsource.sdk.constants.Constants;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;

public abstract class BaseDownloadRunnable implements Runnable {
    protected static final String DOWNLOAD_MANAGER_DB_KEY = "kDownloadManagerCachedFiles";
    private static final String TAG = "Helpshift_DownloadRun";
    private NetworkAuthDataFetcher networkAuthDataFetcher;
    private OnDownloadFinishListener onDownloadFinishListener;
    private OnProgressChangedListener onProgressChangedListener;
    protected DownloadRequestedFileInfo requestInfo;

    /* access modifiers changed from: protected */
    public abstract void clearCache();

    /* access modifiers changed from: protected */
    public abstract long getAlreadyDownloadedBytes() throws FileNotFoundException;

    /* access modifiers changed from: protected */
    public abstract boolean isGzipSupported();

    /* access modifiers changed from: protected */
    public abstract void processHttpResponse(InputStream inputStream, int i) throws IOException;

    BaseDownloadRunnable(DownloadRequestedFileInfo downloadRequestedFileInfo, NetworkAuthDataFetcher networkAuthDataFetcher2, OnProgressChangedListener onProgressChangedListener2, OnDownloadFinishListener onDownloadFinishListener2) {
        this.requestInfo = downloadRequestedFileInfo;
        this.networkAuthDataFetcher = networkAuthDataFetcher2;
        this.onProgressChangedListener = onProgressChangedListener2;
        this.onDownloadFinishListener = onDownloadFinishListener2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00d7, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        notifyDownloadFinish(false, r3);
        com.helpshift.util.HSLogger.e(com.helpshift.android.commons.downloader.runnable.BaseDownloadRunnable.TAG, "Exception in closing download response", r3, com.helpshift.logger.logmodels.LogExtrasModelProvider.fromString("route", r11.requestInfo.url));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x014b, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        notifyDownloadFinish(false, r3);
        com.helpshift.util.HSLogger.e(com.helpshift.android.commons.downloader.runnable.BaseDownloadRunnable.TAG, "Exception in closing download response", r3, com.helpshift.logger.logmodels.LogExtrasModelProvider.fromString("route", r11.requestInfo.url));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x016e, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x016f, code lost:
        notifyDownloadFinish(false, r2);
        com.helpshift.util.HSLogger.e(com.helpshift.android.commons.downloader.runnable.BaseDownloadRunnable.TAG, "Unknown Exception", r2, com.helpshift.logger.logmodels.LogExtrasModelProvider.fromString("route", r11.requestInfo.url));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0188, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0189, code lost:
        notifyDownloadFinish(false, r2);
        com.helpshift.util.HSLogger.e(com.helpshift.android.commons.downloader.runnable.BaseDownloadRunnable.TAG, "GeneralSecurityException", r2, com.helpshift.logger.logmodels.LogExtrasModelProvider.fromString("route", r11.requestInfo.url));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x01a2, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x01a3, code lost:
        notifyDownloadFinish(false, r2);
        com.helpshift.util.HSLogger.e(com.helpshift.android.commons.downloader.runnable.BaseDownloadRunnable.TAG, "Exception IO", r2, com.helpshift.logger.logmodels.LogExtrasModelProvider.fromString("route", r11.requestInfo.url));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x01bc, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x01bd, code lost:
        notifyDownloadFinish(false, r2);
        com.helpshift.util.HSLogger.e(com.helpshift.android.commons.downloader.runnable.BaseDownloadRunnable.TAG, "MalformedURLException", r2, com.helpshift.logger.logmodels.LogExtrasModelProvider.fromString("route", r11.requestInfo.url));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01d6, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01d7, code lost:
        notifyDownloadFinish(false, r2);
        com.helpshift.util.HSLogger.e(com.helpshift.android.commons.downloader.runnable.BaseDownloadRunnable.TAG, "Exception Interrupted", r2, com.helpshift.logger.logmodels.LogExtrasModelProvider.fromString("route", r11.requestInfo.url));
        java.lang.Thread.currentThread().interrupt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0127 A[SYNTHETIC, Splitter:B:50:0x0127] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0147 A[SYNTHETIC, Splitter:B:56:0x0147] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x016e A[ExcHandler: Exception (r2v7 'e' java.lang.Exception A[CUSTOM_DECLARE]), Splitter:B:1:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0188 A[ExcHandler: GeneralSecurityException (r2v6 'e' java.security.GeneralSecurityException A[CUSTOM_DECLARE]), Splitter:B:1:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01bc A[ExcHandler: MalformedURLException (r2v4 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:1:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x01d6 A[ExcHandler: InterruptedException (r2v3 'e' java.lang.InterruptedException A[CUSTOM_DECLARE]), Splitter:B:1:0x0021] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r11 = this;
            java.lang.String r0 = "Helpshift_DownloadRun"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Starting download : "
            r1.append(r2)
            com.helpshift.android.commons.downloader.contracts.DownloadRequestedFileInfo r2 = r11.requestInfo
            java.lang.String r2 = r2.url
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.helpshift.util.HSLogger.d(r0, r1)
            r0 = 10
            android.os.Process.setThreadPriority(r0)
            r0 = 1
            r1 = 0
            boolean r2 = java.lang.Thread.interrupted()     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            if (r2 != 0) goto L_0x0168
            java.net.URL r2 = r11.buildUrl()     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            java.lang.String r3 = "https"
            java.lang.String r4 = r2.getProtocol()     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            boolean r3 = r3.equals(r4)     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            if (r3 == 0) goto L_0x0044
            java.net.URLConnection r2 = r2.openConnection()     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            javax.net.ssl.HttpsURLConnection r2 = (javax.net.ssl.HttpsURLConnection) r2     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            r3 = r2
            javax.net.ssl.HttpsURLConnection r3 = (javax.net.ssl.HttpsURLConnection) r3     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            r11.fixSSLSocketProtocols(r3)     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            goto L_0x004a
        L_0x0044:
            java.net.URLConnection r2 = r2.openConnection()     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
        L_0x004a:
            r2.setInstanceFollowRedirects(r0)     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            r3 = 0
            long r4 = r11.getAlreadyDownloadedBytes()     // Catch:{ IOException -> 0x010c }
            java.lang.String r6 = "Range"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x010c }
            r7.<init>()     // Catch:{ IOException -> 0x010c }
            java.lang.String r8 = "bytes="
            r7.append(r8)     // Catch:{ IOException -> 0x010c }
            r7.append(r4)     // Catch:{ IOException -> 0x010c }
            java.lang.String r4 = "-"
            r7.append(r4)     // Catch:{ IOException -> 0x010c }
            java.lang.String r4 = r7.toString()     // Catch:{ IOException -> 0x010c }
            r2.setRequestProperty(r6, r4)     // Catch:{ IOException -> 0x010c }
            int r4 = r2.getResponseCode()     // Catch:{ IOException -> 0x010c }
            r5 = 416(0x1a0, float:5.83E-43)
            if (r4 == r5) goto L_0x00ff
            java.io.InputStream r4 = r2.getInputStream()     // Catch:{ IOException -> 0x010c }
            boolean r3 = r11.isGzipSupported()     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            if (r3 == 0) goto L_0x00c6
            java.util.Map r3 = r2.getHeaderFields()     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            java.util.Set r3 = r3.entrySet()     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
        L_0x008b:
            boolean r5 = r3.hasNext()     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            if (r5 == 0) goto L_0x00c6
            java.lang.Object r5 = r3.next()     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            java.lang.Object r6 = r5.getKey()     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            if (r6 == 0) goto L_0x008b
            java.lang.Object r6 = r5.getKey()     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            java.lang.String r7 = "Content-Encoding"
            boolean r6 = r6.equals(r7)     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            if (r6 == 0) goto L_0x008b
            java.lang.Object r5 = r5.getValue()     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            java.util.List r5 = (java.util.List) r5     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            java.lang.Object r5 = r5.get(r1)     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            java.lang.String r6 = "gzip"
            boolean r5 = r5.equalsIgnoreCase(r6)     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            if (r5 == 0) goto L_0x008b
            java.util.zip.GZIPInputStream r5 = new java.util.zip.GZIPInputStream     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            r5.<init>(r4)     // Catch:{ IOException -> 0x00fa, all -> 0x00f5 }
            r4 = r5
            goto L_0x008b
        L_0x00c6:
            r3 = r4
            int r4 = r2.getContentLength()     // Catch:{ IOException -> 0x010c }
            r11.processHttpResponse(r3, r4)     // Catch:{ IOException -> 0x010c }
            java.lang.Thread.interrupted()     // Catch:{ IOException -> 0x010c }
            if (r3 == 0) goto L_0x00f0
            r3.close()     // Catch:{ IOException -> 0x00d7, InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            goto L_0x00f0
        L_0x00d7:
            r3 = move-exception
            r11.notifyDownloadFinish(r1, r3)     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            java.lang.String r4 = "Helpshift_DownloadRun"
            java.lang.String r5 = "Exception in closing download response"
            com.helpshift.logger.logmodels.ILogExtrasModel[] r6 = new com.helpshift.logger.logmodels.ILogExtrasModel[r0]     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            java.lang.String r7 = "route"
            com.helpshift.android.commons.downloader.contracts.DownloadRequestedFileInfo r8 = r11.requestInfo     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            java.lang.String r8 = r8.url     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            com.helpshift.logger.logmodels.ILogExtrasModel r7 = com.helpshift.logger.logmodels.LogExtrasModelProvider.fromString(r7, r8)     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            r6[r1] = r7     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            com.helpshift.util.HSLogger.e(r4, r5, r3, r6)     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
        L_0x00f0:
            r2.disconnect()     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            goto L_0x01f6
        L_0x00f5:
            r3 = move-exception
            r10 = r4
            r4 = r3
            r3 = r10
            goto L_0x0145
        L_0x00fa:
            r3 = move-exception
            r10 = r4
            r4 = r3
            r3 = r10
            goto L_0x010d
        L_0x00ff:
            r11.clearCache()     // Catch:{ IOException -> 0x010c }
            java.io.IOException r4 = new java.io.IOException     // Catch:{ IOException -> 0x010c }
            java.lang.String r5 = "Requested Range Not Satisfiable, failed with 416 status"
            r4.<init>(r5)     // Catch:{ IOException -> 0x010c }
            throw r4     // Catch:{ IOException -> 0x010c }
        L_0x010a:
            r4 = move-exception
            goto L_0x0145
        L_0x010c:
            r4 = move-exception
        L_0x010d:
            r11.notifyDownloadFinish(r1, r4)     // Catch:{ all -> 0x010a }
            java.lang.String r5 = "Helpshift_DownloadRun"
            java.lang.String r6 = "Exception in download"
            com.helpshift.logger.logmodels.ILogExtrasModel[] r7 = new com.helpshift.logger.logmodels.ILogExtrasModel[r0]     // Catch:{ all -> 0x010a }
            java.lang.String r8 = "route"
            com.helpshift.android.commons.downloader.contracts.DownloadRequestedFileInfo r9 = r11.requestInfo     // Catch:{ all -> 0x010a }
            java.lang.String r9 = r9.url     // Catch:{ all -> 0x010a }
            com.helpshift.logger.logmodels.ILogExtrasModel r8 = com.helpshift.logger.logmodels.LogExtrasModelProvider.fromString(r8, r9)     // Catch:{ all -> 0x010a }
            r7[r1] = r8     // Catch:{ all -> 0x010a }
            com.helpshift.util.HSLogger.e(r5, r6, r4, r7)     // Catch:{ all -> 0x010a }
            if (r3 == 0) goto L_0x00f0
            r3.close()     // Catch:{ IOException -> 0x012b, InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            goto L_0x00f0
        L_0x012b:
            r3 = move-exception
            r11.notifyDownloadFinish(r1, r3)     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            java.lang.String r4 = "Helpshift_DownloadRun"
            java.lang.String r5 = "Exception in closing download response"
            com.helpshift.logger.logmodels.ILogExtrasModel[] r6 = new com.helpshift.logger.logmodels.ILogExtrasModel[r0]     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            java.lang.String r7 = "route"
            com.helpshift.android.commons.downloader.contracts.DownloadRequestedFileInfo r8 = r11.requestInfo     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            java.lang.String r8 = r8.url     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            com.helpshift.logger.logmodels.ILogExtrasModel r7 = com.helpshift.logger.logmodels.LogExtrasModelProvider.fromString(r7, r8)     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            r6[r1] = r7     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            com.helpshift.util.HSLogger.e(r4, r5, r3, r6)     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            goto L_0x00f0
        L_0x0145:
            if (r3 == 0) goto L_0x0164
            r3.close()     // Catch:{ IOException -> 0x014b, InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            goto L_0x0164
        L_0x014b:
            r3 = move-exception
            r11.notifyDownloadFinish(r1, r3)     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            java.lang.String r5 = "Helpshift_DownloadRun"
            java.lang.String r6 = "Exception in closing download response"
            com.helpshift.logger.logmodels.ILogExtrasModel[] r7 = new com.helpshift.logger.logmodels.ILogExtrasModel[r0]     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            java.lang.String r8 = "route"
            com.helpshift.android.commons.downloader.contracts.DownloadRequestedFileInfo r9 = r11.requestInfo     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            java.lang.String r9 = r9.url     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            com.helpshift.logger.logmodels.ILogExtrasModel r8 = com.helpshift.logger.logmodels.LogExtrasModelProvider.fromString(r8, r9)     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            r7[r1] = r8     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            com.helpshift.util.HSLogger.e(r5, r6, r3, r7)     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
        L_0x0164:
            r2.disconnect()     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            throw r4     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
        L_0x0168:
            java.lang.InterruptedException r2 = new java.lang.InterruptedException     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            r2.<init>()     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
            throw r2     // Catch:{ InterruptedException -> 0x01d6, MalformedURLException -> 0x01bc, IOException -> 0x01a2, GeneralSecurityException -> 0x0188, Exception -> 0x016e }
        L_0x016e:
            r2 = move-exception
            r11.notifyDownloadFinish(r1, r2)
            java.lang.String r3 = "Helpshift_DownloadRun"
            java.lang.String r4 = "Unknown Exception"
            com.helpshift.logger.logmodels.ILogExtrasModel[] r0 = new com.helpshift.logger.logmodels.ILogExtrasModel[r0]
            java.lang.String r5 = "route"
            com.helpshift.android.commons.downloader.contracts.DownloadRequestedFileInfo r6 = r11.requestInfo
            java.lang.String r6 = r6.url
            com.helpshift.logger.logmodels.ILogExtrasModel r5 = com.helpshift.logger.logmodels.LogExtrasModelProvider.fromString(r5, r6)
            r0[r1] = r5
            com.helpshift.util.HSLogger.e(r3, r4, r2, r0)
            goto L_0x01f6
        L_0x0188:
            r2 = move-exception
            r11.notifyDownloadFinish(r1, r2)
            java.lang.String r3 = "Helpshift_DownloadRun"
            java.lang.String r4 = "GeneralSecurityException"
            com.helpshift.logger.logmodels.ILogExtrasModel[] r0 = new com.helpshift.logger.logmodels.ILogExtrasModel[r0]
            java.lang.String r5 = "route"
            com.helpshift.android.commons.downloader.contracts.DownloadRequestedFileInfo r6 = r11.requestInfo
            java.lang.String r6 = r6.url
            com.helpshift.logger.logmodels.ILogExtrasModel r5 = com.helpshift.logger.logmodels.LogExtrasModelProvider.fromString(r5, r6)
            r0[r1] = r5
            com.helpshift.util.HSLogger.e(r3, r4, r2, r0)
            goto L_0x01f6
        L_0x01a2:
            r2 = move-exception
            r11.notifyDownloadFinish(r1, r2)
            java.lang.String r3 = "Helpshift_DownloadRun"
            java.lang.String r4 = "Exception IO"
            com.helpshift.logger.logmodels.ILogExtrasModel[] r0 = new com.helpshift.logger.logmodels.ILogExtrasModel[r0]
            java.lang.String r5 = "route"
            com.helpshift.android.commons.downloader.contracts.DownloadRequestedFileInfo r6 = r11.requestInfo
            java.lang.String r6 = r6.url
            com.helpshift.logger.logmodels.ILogExtrasModel r5 = com.helpshift.logger.logmodels.LogExtrasModelProvider.fromString(r5, r6)
            r0[r1] = r5
            com.helpshift.util.HSLogger.e(r3, r4, r2, r0)
            goto L_0x01f6
        L_0x01bc:
            r2 = move-exception
            r11.notifyDownloadFinish(r1, r2)
            java.lang.String r3 = "Helpshift_DownloadRun"
            java.lang.String r4 = "MalformedURLException"
            com.helpshift.logger.logmodels.ILogExtrasModel[] r0 = new com.helpshift.logger.logmodels.ILogExtrasModel[r0]
            java.lang.String r5 = "route"
            com.helpshift.android.commons.downloader.contracts.DownloadRequestedFileInfo r6 = r11.requestInfo
            java.lang.String r6 = r6.url
            com.helpshift.logger.logmodels.ILogExtrasModel r5 = com.helpshift.logger.logmodels.LogExtrasModelProvider.fromString(r5, r6)
            r0[r1] = r5
            com.helpshift.util.HSLogger.e(r3, r4, r2, r0)
            goto L_0x01f6
        L_0x01d6:
            r2 = move-exception
            r11.notifyDownloadFinish(r1, r2)
            java.lang.String r3 = "Helpshift_DownloadRun"
            java.lang.String r4 = "Exception Interrupted"
            com.helpshift.logger.logmodels.ILogExtrasModel[] r0 = new com.helpshift.logger.logmodels.ILogExtrasModel[r0]
            java.lang.String r5 = "route"
            com.helpshift.android.commons.downloader.contracts.DownloadRequestedFileInfo r6 = r11.requestInfo
            java.lang.String r6 = r6.url
            com.helpshift.logger.logmodels.ILogExtrasModel r5 = com.helpshift.logger.logmodels.LogExtrasModelProvider.fromString(r5, r6)
            r0[r1] = r5
            com.helpshift.util.HSLogger.e(r3, r4, r2, r0)
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
        L_0x01f6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.android.commons.downloader.runnable.BaseDownloadRunnable.run():void");
    }

    private URL buildUrl() throws MalformedURLException, URISyntaxException, GeneralSecurityException {
        URI uri;
        if (this.requestInfo.isSecured) {
            URI uri2 = new URI(this.requestInfo.url);
            String path = uri2.getPath();
            Map<String, String> queryMap = getQueryMap(uri2.getQuery());
            queryMap.put("v", "1");
            queryMap.put(ShareConstants.MEDIA_URI, path);
            Map<String, String> authData = this.networkAuthDataFetcher.getAuthData(queryMap);
            ArrayList arrayList = new ArrayList();
            for (Map.Entry next : authData.entrySet()) {
                arrayList.add(((String) next.getKey()) + Constants.RequestParameters.EQUAL + ((String) next.getValue()));
            }
            uri = new URI(uri2.getScheme(), uri2.getAuthority(), uri2.getPath(), join(Constants.RequestParameters.AMPERSAND, arrayList), null);
        } else {
            uri = new URI(this.requestInfo.url);
        }
        return new URL(uri.toASCIIString());
    }

    private void fixSSLSocketProtocols(HttpsURLConnection httpsURLConnection) {
        if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT <= 19) {
            ArrayList arrayList = new ArrayList();
            arrayList.add("TLSv1.2");
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add("SSLv3");
            httpsURLConnection.setSSLSocketFactory(new HelpshiftSSLSocketFactory(httpsURLConnection.getSSLSocketFactory(), arrayList, arrayList2));
        }
    }

    private Map<String, String> getQueryMap(String str) {
        String[] split = str.split(Constants.RequestParameters.AMPERSAND);
        HashMap hashMap = new HashMap();
        for (String split2 : split) {
            String[] split3 = split2.split(Constants.RequestParameters.EQUAL);
            if (split3.length == 2) {
                hashMap.put(split3[0], split3[1]);
            }
        }
        return hashMap;
    }

    private String join(CharSequence charSequence, Iterable iterable) {
        if (iterable == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (Object next : iterable) {
            if (z) {
                z = false;
            } else {
                sb.append(charSequence);
            }
            sb.append(next);
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public void closeFileStream(Closeable closeable) throws IOException {
        if (closeable != null) {
            closeable.close();
        }
    }

    /* access modifiers changed from: package-private */
    public void notifyProgressChange(int i) {
        if (this.onProgressChangedListener != null) {
            this.onProgressChangedListener.onProgressChanged(this.requestInfo.url, i);
        }
    }

    /* access modifiers changed from: package-private */
    public void notifyDownloadFinish(boolean z, Object obj) {
        if (this.onDownloadFinishListener != null) {
            this.onDownloadFinishListener.onDownloadFinish(z, this.requestInfo.url, obj);
        }
    }
}
