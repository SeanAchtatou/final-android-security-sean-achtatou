package com.helpshift.android.commons.downloader.runnable;

import com.helpshift.android.commons.downloader.contracts.DownloadRequestedFileInfo;
import com.helpshift.android.commons.downloader.contracts.NetworkAuthDataFetcher;
import com.helpshift.android.commons.downloader.contracts.OnDownloadFinishListener;
import com.helpshift.android.commons.downloader.contracts.OnProgressChangedListener;

public class RawResponseDownloadRunnable extends BaseDownloadRunnable {
    private static final String TAG = "Helpshift_RawDownRun";

    /* access modifiers changed from: protected */
    public void clearCache() {
    }

    /* access modifiers changed from: protected */
    public long getAlreadyDownloadedBytes() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public boolean isGzipSupported() {
        return true;
    }

    public RawResponseDownloadRunnable(DownloadRequestedFileInfo downloadRequestedFileInfo, NetworkAuthDataFetcher networkAuthDataFetcher, OnProgressChangedListener onProgressChangedListener, OnDownloadFinishListener onDownloadFinishListener) {
        super(downloadRequestedFileInfo, networkAuthDataFetcher, onProgressChangedListener, onDownloadFinishListener);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:0|(3:1|2|(1:4)(1:14))|7|8|9|10|11|17) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003c, code lost:
        notifyDownloadFinish(true, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x002f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void processHttpResponse(java.io.InputStream r3, int r4) {
        /*
            r2 = this;
            java.io.InputStreamReader r4 = new java.io.InputStreamReader
            r4.<init>(r3)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.io.BufferedReader r0 = new java.io.BufferedReader
            r0.<init>(r4)
        L_0x000f:
            java.lang.String r4 = r0.readLine()     // Catch:{ IOException -> 0x0019 }
            if (r4 == 0) goto L_0x0021
            r3.append(r4)     // Catch:{ IOException -> 0x0019 }
            goto L_0x000f
        L_0x0019:
            r4 = move-exception
            java.lang.String r0 = "Helpshift_RawDownRun"
            java.lang.String r1 = "IO Exception while reading response"
            com.helpshift.util.HSLogger.d(r0, r1, r4)
        L_0x0021:
            r4 = 1
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ JSONException -> 0x002f }
            java.lang.String r1 = r3.toString()     // Catch:{ JSONException -> 0x002f }
            r0.<init>(r1)     // Catch:{ JSONException -> 0x002f }
            r2.notifyDownloadFinish(r4, r0)     // Catch:{ JSONException -> 0x002f }
            goto L_0x003f
        L_0x002f:
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ JSONException -> 0x003c }
            java.lang.String r1 = r3.toString()     // Catch:{ JSONException -> 0x003c }
            r0.<init>(r1)     // Catch:{ JSONException -> 0x003c }
            r2.notifyDownloadFinish(r4, r0)     // Catch:{ JSONException -> 0x003c }
            goto L_0x003f
        L_0x003c:
            r2.notifyDownloadFinish(r4, r3)
        L_0x003f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.android.commons.downloader.runnable.RawResponseDownloadRunnable.processHttpResponse(java.io.InputStream, int):void");
    }
}
