package com.helpshift.android.commons.downloader;

import com.helpshift.android.commons.downloader.contracts.DownloadDirType;

public class DownloadConfig {
    DownloadDirType downloadDirType;
    String externalStorageDirectoryPath;
    boolean isNoMedia;
    boolean useCache;
    boolean writeToFile;

    public static class Builder {
        private DownloadDirType downloadDirType = DownloadDirType.INTERNAL_ONLY;
        private String externalStorageDirectoryPath = "";
        private boolean isNoMedia = false;
        private boolean useCache = true;
        private boolean writeToFile = true;

        public Builder setUseCache(boolean z) {
            this.useCache = z;
            return this;
        }

        public Builder setIsNoMedia(boolean z) {
            this.isNoMedia = z;
            return this;
        }

        public Builder setWriteToFile(boolean z) {
            this.writeToFile = z;
            return this;
        }

        public Builder setExternalStorageDirectoryPath(String str) {
            this.externalStorageDirectoryPath = str;
            return this;
        }

        public Builder setDownloadDirType(DownloadDirType downloadDirType2) {
            this.downloadDirType = downloadDirType2;
            return this;
        }

        public DownloadConfig create() {
            DownloadConfig downloadConfig = new DownloadConfig();
            downloadConfig.useCache = this.useCache;
            downloadConfig.isNoMedia = this.isNoMedia;
            downloadConfig.writeToFile = this.writeToFile;
            downloadConfig.externalStorageDirectoryPath = this.externalStorageDirectoryPath;
            downloadConfig.downloadDirType = this.downloadDirType;
            return downloadConfig;
        }
    }
}
