package com.helpshift.android.commons.downloader.contracts;

public class DownloadRequestedFileInfo {
    public final String contentType;
    public final boolean isSecured;
    public final String url;

    public DownloadRequestedFileInfo(String str, boolean z, String str2) {
        this.url = str;
        this.isSecured = z;
        this.contentType = str2;
    }
}
