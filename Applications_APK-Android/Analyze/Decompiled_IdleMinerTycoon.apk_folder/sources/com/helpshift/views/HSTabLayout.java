package com.helpshift.views;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;

public class HSTabLayout extends TabLayout {
    public HSTabLayout(Context context) {
        super(context);
        init();
    }

    public HSTabLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public HSTabLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    private void init() {
        FontApplier.apply(this);
    }
}
