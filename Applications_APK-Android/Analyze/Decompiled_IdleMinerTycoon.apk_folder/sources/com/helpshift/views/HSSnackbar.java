package com.helpshift.views;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.view.View;

public class HSSnackbar {
    @NonNull
    public static Snackbar make(View view, CharSequence charSequence, int i) {
        Snackbar make = Snackbar.make(view, charSequence, i);
        FontApplier.apply(make.getView());
        return make;
    }

    @NonNull
    public static Snackbar make(View view, int i, int i2) {
        return make(view, view.getResources().getText(i), i2);
    }
}
