package com.helpshift.views;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.Toast;

public class HSTooltip {
    private Context context;
    private View targetView;
    private String text;

    public HSTooltip(View view, String str) {
        this.targetView = view;
        this.context = view.getContext();
        this.text = str;
    }

    public void show() {
        int[] iArr = new int[2];
        Rect rect = new Rect();
        this.targetView.getLocationOnScreen(iArr);
        this.targetView.getWindowVisibleDisplayFrame(rect);
        int width = this.targetView.getWidth();
        int height = this.targetView.getHeight();
        int i = iArr[0] + (width / 2);
        int i2 = iArr[1] + (height / 2);
        if (ViewCompat.getLayoutDirection(this.targetView) == 0) {
            i = this.context.getResources().getDisplayMetrics().widthPixels - i;
        }
        Toast makeText = HSToast.makeText(this.context, this.text, 0);
        if (i2 < rect.height()) {
            makeText.setGravity(8388661, i, i2);
        } else {
            makeText.setGravity(81, 0, height);
        }
        makeText.show();
    }
}
