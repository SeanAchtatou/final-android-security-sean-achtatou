package com.helpshift.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import com.helpshift.model.InfoModelFactory;
import com.helpshift.util.HSLogger;
import java.lang.ref.WeakReference;

public final class FontApplier {
    public static final String TAG = "HS_FontApplier";
    private static Typeface typeface = null;
    private static boolean typefaceInitialisationTried = false;
    private static HSTypefaceSpan typefaceSpan;

    private FontApplier() {
    }

    public static void apply(TextView textView) {
        initTypeface(textView.getContext());
        if (typeface != null) {
            textView.setTypeface(typeface);
        }
    }

    public static void apply(Dialog dialog) {
        apply(dialog.findViewById(16908290));
    }

    public static void apply(View view) {
        initTypeface(view.getContext());
        if (typeface != null) {
            view.getViewTreeObserver().addOnGlobalLayoutListener(new FontLayoutListener(view));
        }
    }

    static void applyInternal(View view) {
        if (view instanceof TextView) {
            apply((TextView) view);
        } else if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                applyInternal(viewGroup.getChildAt(i));
            }
        }
    }

    @Nullable
    public static HSTypefaceSpan getTypefaceSpan() {
        if (typeface != null && typefaceSpan == null) {
            typefaceSpan = new HSTypefaceSpan(typeface);
        }
        return typefaceSpan;
    }

    @Nullable
    public static String getFontPath() {
        return InfoModelFactory.getInstance().appInfoModel.getFontPath();
    }

    private static void initTypeface(Context context) {
        String fontPath = getFontPath();
        if (fontPath != null && typeface == null && !typefaceInitialisationTried) {
            try {
                typeface = Typeface.createFromAsset(context.getAssets(), fontPath);
            } catch (Exception e) {
                HSLogger.e(TAG, "Typeface initialisation failed. Using default typeface. " + e.getMessage());
            } catch (Throwable th) {
                typefaceInitialisationTried = true;
                throw th;
            }
            typefaceInitialisationTried = true;
        }
    }

    private static class FontLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {
        private final WeakReference<View> viewRef;

        public FontLayoutListener(View view) {
            this.viewRef = new WeakReference<>(view);
        }

        public void onGlobalLayout() {
            View view = this.viewRef.get();
            if (view != null) {
                FontApplier.applyInternal(view);
            }
        }
    }
}
