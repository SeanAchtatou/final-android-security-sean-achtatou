package com.helpshift.views;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;

public class HSTextInputLayout extends TextInputLayout {
    public HSTextInputLayout(Context context) {
        super(context);
        init();
    }

    public HSTextInputLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public HSTextInputLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    private void init() {
        FontApplier.apply(this);
    }
}
