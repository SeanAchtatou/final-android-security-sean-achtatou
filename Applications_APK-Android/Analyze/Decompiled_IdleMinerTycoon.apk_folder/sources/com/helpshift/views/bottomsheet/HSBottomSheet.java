package com.helpshift.views.bottomsheet;

import android.annotation.SuppressLint;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import com.helpshift.R;
import java.util.ArrayList;
import java.util.List;

public class HSBottomSheet {
    final ViewGroup bottomSheet;
    final View bottomSheetParentView;
    final Window bottomSheetWindow;
    List<BottomSheetBehavior.BottomSheetCallback> callbacks = new ArrayList();
    final View contentView;
    final float dimOpacity;
    final boolean enableDimAnimation;
    final View referenceWindowView;
    final View viewToDim;

    HSBottomSheet(View view, Window window, View view2, @Nullable View view3, boolean z, float f, View view4, ViewGroup viewGroup) {
        this.contentView = view;
        this.bottomSheetWindow = window;
        this.referenceWindowView = view2;
        this.viewToDim = view3;
        this.enableDimAnimation = z;
        this.dimOpacity = f;
        this.bottomSheetParentView = view4;
        this.bottomSheet = viewGroup;
    }

    public void show() {
        this.bottomSheet.addView(this.contentView);
        attachBehaviourCallback();
        if (this.referenceWindowView != null) {
            initiateReferenceViewAttachment();
        } else {
            this.bottomSheetWindow.addContentView(this.bottomSheetParentView, getParamsForWindow());
        }
    }

    public void remove() {
        if (ViewCompat.isAttachedToWindow(this.bottomSheetParentView)) {
            ((ViewGroup) this.bottomSheetParentView.getParent()).removeView(this.bottomSheetParentView);
        }
        if (this.viewToDim != null && ViewCompat.isAttachedToWindow(this.viewToDim)) {
            ((ViewGroup) this.viewToDim.getParent()).removeView(this.viewToDim);
        }
    }

    public void setDraggable(boolean z) {
        ((HSBottomSheetBehaviour) getBottomSheetBehaviour()).setDraggable(z);
    }

    public void addBottomSheetCallback(@Nullable BottomSheetBehavior.BottomSheetCallback bottomSheetCallback) {
        this.callbacks.add(bottomSheetCallback);
    }

    public void removeAllBottomSheetCallbacks() {
        this.callbacks.clear();
    }

    private void initiateReferenceViewAttachment() {
        if (ViewCompat.isLaidOut(this.referenceWindowView)) {
            showOnReferenceView();
        } else {
            this.referenceWindowView.post(new Runnable() {
                public void run() {
                    HSBottomSheet.this.showOnReferenceView();
                }
            });
        }
    }

    public View getBottomSheetContentView() {
        return this.contentView;
    }

    /* access modifiers changed from: package-private */
    public void showOnReferenceView() {
        setupBottomSheetView();
        attachBottomSheetToWindow(getParamsForReferenceView());
    }

    /* access modifiers changed from: package-private */
    public ViewGroup.LayoutParams getParamsForWindow() {
        ViewGroup.LayoutParams layoutParams = this.contentView.getLayoutParams();
        layoutParams.height = -1;
        layoutParams.width = -1;
        return layoutParams;
    }

    /* access modifiers changed from: package-private */
    public ViewGroup.LayoutParams getParamsForReferenceView() {
        ViewGroup.LayoutParams layoutParams = this.contentView.getLayoutParams();
        layoutParams.height = -1;
        layoutParams.width = this.referenceWindowView.getWidth();
        return layoutParams;
    }

    private void attachBottomSheetToWindow(ViewGroup.LayoutParams layoutParams) {
        this.bottomSheetWindow.addContentView(this.bottomSheetParentView, layoutParams);
    }

    private void attachBehaviourCallback() {
        getBottomSheetBehaviour().setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            public void onStateChanged(@NonNull View view, int i) {
                if (HSBottomSheet.this.viewToDim != null) {
                    if (i == 3) {
                        HSBottomSheet.this.viewToDim.setClickable(true);
                    } else if (i == 4) {
                        HSBottomSheet.this.viewToDim.setClickable(false);
                    }
                }
                if (HSBottomSheet.this.callbacks.size() > 0) {
                    for (BottomSheetBehavior.BottomSheetCallback onStateChanged : HSBottomSheet.this.callbacks) {
                        onStateChanged.onStateChanged(view, i);
                    }
                }
            }

            public void onSlide(@NonNull View view, float f) {
                if (HSBottomSheet.this.enableDimAnimation && HSBottomSheet.this.viewToDim != null) {
                    float f2 = 0.0f;
                    if (f > 0.0f) {
                        f2 = f;
                    }
                    HSBottomSheet.this.viewToDim.setBackgroundColor(ColorUtils.blendARGB(0, ViewCompat.MEASURED_STATE_MASK, f2 * HSBottomSheet.this.dimOpacity));
                }
                if (HSBottomSheet.this.callbacks.size() > 0) {
                    for (BottomSheetBehavior.BottomSheetCallback onSlide : HSBottomSheet.this.callbacks) {
                        onSlide.onSlide(view, f);
                    }
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void setupBottomSheetView() {
        int i;
        View findViewById;
        int[] iArr = new int[2];
        this.referenceWindowView.getLocationInWindow(iArr);
        View decorView = this.bottomSheetWindow.getDecorView();
        if (decorView == null || (findViewById = decorView.findViewById(16908290)) == null) {
            i = 0;
        } else {
            int[] iArr2 = new int[2];
            findViewById.getLocationInWindow(iArr2);
            i = iArr2[0];
        }
        this.bottomSheetParentView.setX((float) Math.max(0, iArr[0] - i));
    }

    public BottomSheetBehavior getBottomSheetBehaviour() {
        return BottomSheetBehavior.from(this.bottomSheet);
    }

    public static class Builder {
        private View content;
        private float dimOpacity = 1.0f;
        private boolean enableDimAnimation;
        private int layoutId;
        private Window layoutWindow;
        private View referenceView;

        public Builder(@NonNull Window window) {
            this.layoutWindow = window;
        }

        public Builder contentView(@LayoutRes int i) {
            this.layoutId = i;
            return this;
        }

        public Builder referenceView(@Nullable View view) {
            this.referenceView = view;
            return this;
        }

        public Builder enableDimAnimation(boolean z) {
            this.enableDimAnimation = z;
            return this;
        }

        public Builder dimOpacity(float f) {
            this.dimOpacity = f;
            return this;
        }

        @SuppressLint({"InflateParams"})
        public HSBottomSheet inflateAndBuild() {
            View view;
            if (this.layoutWindow != null) {
                if (this.enableDimAnimation) {
                    View view2 = new View(this.referenceView.getContext());
                    this.layoutWindow.addContentView(view2, this.layoutWindow.getAttributes());
                    view = view2;
                } else {
                    view = null;
                }
                LayoutInflater from = LayoutInflater.from(this.layoutWindow.getContext());
                this.content = from.inflate(this.layoutId, (ViewGroup) null);
                CoordinatorLayout coordinatorLayout = (CoordinatorLayout) from.inflate(R.layout.hs__bottomsheet_wrapper, (ViewGroup) null);
                return new HSBottomSheet(this.content, this.layoutWindow, this.referenceView, view, this.enableDimAnimation, this.dimOpacity, coordinatorLayout, (FrameLayout) coordinatorLayout.findViewById(R.id.hs__bottom_sheet));
            }
            throw new IllegalArgumentException("Bottomsheet layout window can not be null");
        }
    }
}
