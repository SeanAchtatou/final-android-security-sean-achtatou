package com.helpshift.views;

import android.content.Context;
import android.content.res.Resources;
import android.widget.Toast;

public class HSToast {
    public static Toast makeText(Context context, CharSequence charSequence, int i) {
        Toast makeText = Toast.makeText(context, charSequence, i);
        FontApplier.apply(makeText.getView());
        return makeText;
    }

    public static Toast makeText(Context context, int i, int i2) throws Resources.NotFoundException {
        return makeText(context, context.getResources().getText(i), i2);
    }
}
