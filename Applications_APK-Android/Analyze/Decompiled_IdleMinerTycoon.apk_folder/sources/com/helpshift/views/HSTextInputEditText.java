package com.helpshift.views;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;
import android.widget.TextView;

public class HSTextInputEditText extends TextInputEditText {
    public HSTextInputEditText(Context context) {
        super(context);
        init();
    }

    public HSTextInputEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public HSTextInputEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    private void init() {
        setGravity(51);
        FontApplier.apply((TextView) this);
    }
}
