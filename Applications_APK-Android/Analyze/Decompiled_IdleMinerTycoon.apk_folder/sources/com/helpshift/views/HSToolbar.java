package com.helpshift.views;

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;

public class HSToolbar extends Toolbar {
    public HSToolbar(Context context) {
        super(context);
        init();
    }

    public HSToolbar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public HSToolbar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    private void init() {
        FontApplier.apply(this);
    }
}
