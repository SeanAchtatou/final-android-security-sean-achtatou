package com.helpshift;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.helpshift.HelpshiftUser;
import com.helpshift.common.exception.PlatformException;
import com.helpshift.delegate.AuthenticationFailureReason;
import com.helpshift.exceptions.InstallException;
import com.helpshift.storage.HelpshiftUnityStorage;
import com.helpshift.support.AlertToRateAppListener;
import com.helpshift.support.Callable;
import com.helpshift.support.Support;
import com.helpshift.support.util.SnackbarUtil;
import com.helpshift.util.ConfigParserUtil;
import com.helpshift.util.HSJSONUtils;
import com.helpshift.util.HSLogger;
import com.helpshift.util.UnityUtils;
import com.helpshift.util.concurrent.ApiExecutorFactory;
import com.mintegral.msdk.base.entity.CampaignEx;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HelpshiftUnityAPI {
    public static final String TAG = "Helpshift_UnityAPI";
    /* access modifiers changed from: private */
    public static ArrayList<String> fileFormats;
    private static HelpshiftUnityStorage storage;

    public static void install(Application application, String str, String str2, String str3) throws InstallException {
        install(application, str, str2, str3, new HashMap());
    }

    public static void install(Application application, String str, String str2, String str3, Map<String, Object> map) throws InstallException {
        Object obj = map.get("supportedFileFormats");
        if (obj instanceof ArrayList) {
            fileFormats = (ArrayList) obj;
        }
        storage = HelpshiftUnityStorage.getInstance(application.getApplicationContext());
        PluginEventBridge.setPluginEventsAPI(new UnityPluginAPIEventsBridge(storage));
        Core.init(Support.getInstance());
        Core.install(application, str, str2, str3, map);
        storage.putMap(HelpshiftUnityStorage.INSTALL_CONFIG, new HashMap(map));
        storage.put("apiKey", str);
        storage.put("domainName", str2);
        storage.put("appId", str3);
        storage.put(HelpshiftUnityStorage.UNITY_MESSAGE_HANDLER_KEY, (String) map.get("unityGameObject"));
        HSLogger.d(TAG, "Install called.");
    }

    public static void install(Application application, String str, String str2, String str3, String str4) throws InstallException, JSONException {
        install(application, str, str2, str3, ConfigParserUtil.parseConfigDictionary(application, str4));
    }

    public static void setNameAndEmail(String str, String str2) {
        Core.setNameAndEmail(str, str2);
    }

    public static void registerDeviceToken(Context context, String str) {
        Core.registerDeviceToken(context, str);
    }

    public static void login(String str, String str2, String str3) {
        Core.login(str, str2, str3);
    }

    public static void loginHelpshiftUser(String str) throws JSONException {
        HashMap<String, Object> map = HSJSONUtils.toMap(new JSONObject(str));
        HelpshiftUser.Builder builder = new HelpshiftUser.Builder((String) map.get(SettingsJsonConstants.APP_IDENTIFIER_KEY), (String) map.get("email"));
        builder.setAuthToken((String) map.get("authToken"));
        builder.setName((String) map.get("name"));
        Core.login(builder.build());
    }

    public static void clearAnonymousUser() {
        Core.clearAnonymousUser();
    }

    public static void logout() {
        Core.logout();
    }

    public static void setUserIdentifier(String str) {
        Support.setUserIdentifier(str);
    }

    public static void leaveBreadCrumb(String str) {
        Support.leaveBreadCrumb(str);
    }

    public static void clearBreadCrumbs() {
        Support.clearBreadCrumbs();
    }

    public static void setSDKLanguage(String str) {
        Support.setSDKLanguage(str);
    }

    private static Map<String, Object> getSanitisedApiConfig(Context context, String str) throws JSONException {
        return !TextUtils.isEmpty(str) ? ConfigParserUtil.parseConfigDictionary(context, str) : new HashMap();
    }

    public static void showConversationUnity(Activity activity, String str) throws JSONException {
        Support.showConversation(activity, getSanitisedApiConfig(activity, str));
    }

    public static void showFAQSectionUnity(Activity activity, String str, String str2) throws JSONException {
        Support.showFAQSection(activity, str, getSanitisedApiConfig(activity, str2));
    }

    public static void showSingleFAQUnity(Activity activity, String str, String str2) throws JSONException {
        Support.showSingleFAQ(activity, str, getSanitisedApiConfig(activity, str2));
    }

    public static void showFAQsUnity(Activity activity, String str) throws JSONException {
        Support.showFAQs(activity, getSanitisedApiConfig(activity, str));
    }

    public static void showDynamicFormFromDataJson(Activity activity, String str, String str2) {
        try {
            HSLogger.d(TAG, "showDynamicFormFromDataJson : flows : " + str2);
            JSONArray jSONArray = new JSONArray(str2);
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(HSJSONUtils.toMap(jSONArray.getJSONObject(i)));
            }
            Support.showDynamicForm(activity, str, ConfigParserUtil.parseFlowList(activity.getApplicationContext(), arrayList));
        } catch (JSONException e) {
            HSLogger.d(TAG, "JSON Exception in parsing dynamic form data : ", e);
        }
    }

    public static void handlePush(Context context, Map<String, String> map) {
        Core.handlePush(context, map);
    }

    public static void showAlertToRateApp(String str) {
        Support.showAlertToRateApp(str, new AlertToRateAppListener() {
            public void onAction(int i) {
                String str = "";
                switch (i) {
                    case 0:
                        str = "HS_RATE_ALERT_SUCCESS";
                        break;
                    case 1:
                        str = "HS_RATE_ALERT_FEEDBACK";
                        break;
                    case 2:
                        str = "HS_RATE_ALERT_CLOSE";
                        break;
                    case 3:
                        str = "HS_RATE_ALERT_FAIL";
                        break;
                }
                UnityUtils.sendUnityMessage(HelpshiftUnityAPI.getUnityMessageHandler(), "alertToRateAppAction", str);
            }
        });
    }

    public static Integer getNotificationCount() {
        return Support.getNotificationCount();
    }

    public static int getNotificationCount(boolean z) {
        HSLogger.d(TAG, "getNotificationCount : isAsync : " + z);
        if (!z) {
            return Support.getNotificationCount().intValue();
        }
        ApiExecutorFactory.getHandlerExecutor().runSync(new Runnable() {
            public void run() {
                Support.getNotificationCount(new Handler() {
                    public void handleMessage(Message message) {
                        super.handleMessage(message);
                        Bundle bundle = (Bundle) message.obj;
                        Integer valueOf = Integer.valueOf(bundle.getInt("value"));
                        Boolean.valueOf(bundle.getBoolean("cache"));
                        String access$000 = HelpshiftUnityAPI.getUnityMessageHandler();
                        UnityUtils.sendUnityMessage(access$000, "didReceiveNotificationCount", "" + valueOf);
                    }
                }, new Handler());
            }
        });
        return -1;
    }

    public static void requestUnreadMessagesCount(boolean z) {
        HSLogger.d(TAG, "requestUnreadMessagesCount : isAsync : " + z);
        if (z) {
            ApiExecutorFactory.getHandlerExecutor().runSync(new Runnable() {
                public void run() {
                    Support.getNotificationCount(new Handler() {
                        public void handleMessage(Message message) {
                            super.handleMessage(message);
                            Integer valueOf = Integer.valueOf(((Bundle) message.obj).getInt("value"));
                            String access$000 = HelpshiftUnityAPI.getUnityMessageHandler();
                            UnityUtils.sendUnityMessage(access$000, "didReceiveUnreadMessagesCount", "" + valueOf);
                        }
                    }, new Handler() {
                        public void handleMessage(Message message) {
                            super.handleMessage(message);
                            Integer valueOf = Integer.valueOf(((Bundle) message.obj).getInt("value"));
                            String access$000 = HelpshiftUnityAPI.getUnityMessageHandler();
                            UnityUtils.sendUnityMessage(access$000, "didReceiveUnreadMessagesCount", "" + valueOf);
                        }
                    });
                }
            });
            return;
        }
        int intValue = Support.getNotificationCount().intValue();
        String unityMessageHandler = getUnityMessageHandler();
        UnityUtils.sendUnityMessage(unityMessageHandler, "didReceiveUnreadMessagesCount", "" + intValue);
    }

    public static void checkIfConversationActive() {
        HSLogger.d(TAG, "checkIfConversationActive");
        UnityUtils.sendUnityMessage(getUnityMessageHandler(), "didCheckIfConversationActive", Support.isConversationActive() ? "true" : "false");
    }

    public static void registerDelegates() {
        Support.setDelegate(new HelpshiftDelegate());
    }

    public static void setMetaData(final String str) {
        Support.setMetadataCallback(new Callable() {
            public Map<String, Serializable> call() {
                try {
                    HashMap<String, Object> map = HSJSONUtils.toMap(new JSONObject(str));
                    Object remove = map.remove(Support.TagsKey);
                    if (remove instanceof ArrayList) {
                        map.put(Support.TagsKey, (String[]) ((ArrayList) remove).toArray(new String[0]));
                    }
                    HashMap hashMap = new HashMap();
                    for (Map.Entry next : map.entrySet()) {
                        hashMap.put(next.getKey(), (Serializable) next.getValue());
                    }
                    return hashMap;
                } catch (JSONException e) {
                    HSLogger.d(HelpshiftUnityAPI.TAG, "Error setting metaData ", e);
                    return null;
                } catch (ClassCastException e2) {
                    HSLogger.d(HelpshiftUnityAPI.TAG, "Error setting metaData ", e2);
                    return null;
                }
            }
        });
    }

    public static void handlePush(Context context, Intent intent) {
        Core.handlePush(context, intent);
    }

    public static void handlePush(Context context, Bundle bundle) {
        Core.handlePush(context, bundle);
    }

    public static void handlePush(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                Bundle bundle = new Bundle();
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    bundle.putString(next, jSONObject.getString(next));
                }
                Intent intent = new Intent();
                intent.putExtras(bundle);
                handlePush(context, intent);
            } catch (Exception e) {
                HSLogger.e(TAG, "Error handling push : " + e.getMessage());
            }
        }
    }

    /* access modifiers changed from: private */
    public static String getUnityMessageHandler() {
        return storage.getString(HelpshiftUnityStorage.UNITY_MESSAGE_HANDLER_KEY);
    }

    private static class HelpshiftDelegate implements Support.Delegate {
        private HelpshiftDelegate() {
        }

        public void sessionBegan() {
            UnityUtils.sendUnityMessage(HelpshiftUnityAPI.getUnityMessageHandler(), "helpshiftSessionBegan", "");
        }

        public void sessionEnded() {
            UnityUtils.sendUnityMessage(HelpshiftUnityAPI.getUnityMessageHandler(), "helpshiftSessionEnded", "");
        }

        public void newConversationStarted(String str) {
            UnityUtils.sendUnityMessage(HelpshiftUnityAPI.getUnityMessageHandler(), "newConversationStarted", str);
        }

        public void userRepliedToConversation(String str) {
            UnityUtils.sendUnityMessage(HelpshiftUnityAPI.getUnityMessageHandler(), "userRepliedToConversation", str);
        }

        public void userCompletedCustomerSatisfactionSurvey(int i, String str) {
            String access$000 = HelpshiftUnityAPI.getUnityMessageHandler();
            if (!TextUtils.isEmpty(access$000)) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put(CampaignEx.JSON_KEY_STAR, i);
                    jSONObject.put("feedback", str);
                    UnityUtils.sendUnityMessage(access$000, "userCompletedCustomerSatisfactionSurvey", jSONObject.toString());
                } catch (JSONException e) {
                    HSLogger.d(HelpshiftUnityAPI.TAG, "Error in userCompletedCustomerSatisfactionSurvey", e);
                }
            }
        }

        public void displayAttachmentFile(File file) {
            String absolutePath = file.getAbsolutePath();
            String name = file.getName();
            String substring = (name.lastIndexOf(".") == -1 || name.lastIndexOf(".") == 0) ? null : name.substring(name.lastIndexOf(".") + 1);
            if (substring == null || HelpshiftUnityAPI.fileFormats == null || !HelpshiftUnityAPI.fileFormats.contains(substring)) {
                SnackbarUtil.showSnackbar(PlatformException.NO_APPS_FOR_OPENING_ATTACHMENT, null);
            } else {
                UnityUtils.sendUnityMessage(HelpshiftUnityAPI.getUnityMessageHandler(), "displayAttachmentFile", absolutePath);
            }
        }

        public void didReceiveNotification(int i) {
            UnityUtils.sendUnityMessage(HelpshiftUnityAPI.getUnityMessageHandler(), "didReceiveNotificationCount", Integer.toString(i));
        }

        public void authenticationFailed(HelpshiftUser helpshiftUser, AuthenticationFailureReason authenticationFailureReason) {
            String access$000 = HelpshiftUnityAPI.getUnityMessageHandler();
            Map<String, String> convertHelpshiftUserModelToMap = convertHelpshiftUserModelToMap(helpshiftUser);
            convertHelpshiftUserModelToMap.put("authFailureReason", Integer.toString(authenticationFailureReason.getValue()));
            UnityUtils.sendUnityMessage(access$000, "authenticationFailed", new JSONObject(convertHelpshiftUserModelToMap).toString());
        }

        private Map<String, String> convertHelpshiftUserModelToMap(HelpshiftUser helpshiftUser) {
            HashMap hashMap = new HashMap();
            hashMap.put(SettingsJsonConstants.APP_IDENTIFIER_KEY, helpshiftUser.getIdentifier());
            hashMap.put("email", helpshiftUser.getEmail());
            hashMap.put("name", helpshiftUser.getName());
            hashMap.put("authToken", helpshiftUser.getAuthToken());
            return hashMap;
        }

        public void conversationEnded() {
            UnityUtils.sendUnityMessage(HelpshiftUnityAPI.getUnityMessageHandler(), "conversationEnded", "");
        }
    }
}
