package com.helpshift.common;

import java.util.List;

public final class ListUtils {
    public static boolean isEmpty(List list) {
        return list == null || list.isEmpty();
    }
}
