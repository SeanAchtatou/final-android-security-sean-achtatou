package com.helpshift.common.platform;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import com.helpshift.PluginEventBridge;
import com.helpshift.R;
import com.helpshift.account.dao.AndroidClearedUserDAO;
import com.helpshift.account.dao.AndroidLegacyAnalyticsEventIDDAO;
import com.helpshift.account.dao.AndroidLegacyProfileMigrationDAO;
import com.helpshift.account.dao.AndroidRedactionDAO;
import com.helpshift.account.dao.AndroidUserDAO;
import com.helpshift.account.dao.AndroidUserManagerDAO;
import com.helpshift.account.dao.ClearedUserDAO;
import com.helpshift.account.dao.UserDAO;
import com.helpshift.account.dao.UserDB;
import com.helpshift.account.dao.UserManagerDAO;
import com.helpshift.analytics.AnalyticsEventDAO;
import com.helpshift.android.commons.downloader.HsUriUtils;
import com.helpshift.cif.dao.CustomIssueFieldDAO;
import com.helpshift.common.dao.BackupDAO;
import com.helpshift.common.domain.F;
import com.helpshift.common.domain.Threader;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.platform.network.HTTPTransport;
import com.helpshift.common.platform.network.NetworkRequestDAO;
import com.helpshift.common.platform.network.ResponseParser;
import com.helpshift.conversation.dao.ConversationDAO;
import com.helpshift.conversation.dao.ConversationInboxDAO;
import com.helpshift.conversation.dao.FAQSuggestionsDAO;
import com.helpshift.conversation.dto.ImagePickerFile;
import com.helpshift.downloader.SupportDownloader;
import com.helpshift.faq.dao.FaqEventDAO;
import com.helpshift.faq.domainmodel.FAQSearchDM;
import com.helpshift.meta.dao.MetaDataDAO;
import com.helpshift.migration.LegacyAnalyticsEventIDDAO;
import com.helpshift.migration.LegacyProfileMigrationDAO;
import com.helpshift.notifications.NotificationChannelsManager;
import com.helpshift.providers.CrossModuleDataProvider;
import com.helpshift.providers.ICampaignsModuleAPIs;
import com.helpshift.redaction.RedactionDAO;
import com.helpshift.support.HSApiData;
import com.helpshift.support.storage.AndroidAnalyticsEventDAO;
import com.helpshift.support.storage.SupportKeyValueDBStorage;
import com.helpshift.support.util.AttachmentUtil;
import com.helpshift.support.util.SupportNotification;
import com.helpshift.util.ApplicationUtil;
import com.helpshift.util.FileUtil;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftConnectionUtil;
import java.io.IOException;

public class AndroidPlatform implements Platform {
    private static final String TAG = "AndroidPlatform";
    private AnalyticsEventDAO analyticsEventDAO;
    private String apiKey;
    private String appId;
    private BackupDAO backupDAO = new AndroidBackupDAO();
    private ClearedUserDAO clearedUserDAO;
    private final Context context;
    private ConversationDAO conversationDAO;
    private ConversationInboxDAO conversationInboxDAO;
    private CustomIssueFieldDAO customIssueFieldDAO;
    private HSApiData data;
    private Device device;
    private String domain;
    private SupportDownloader downloader;
    private FaqEventDAO faqEventDAO;
    private FAQSearchDM faqSearchDM;
    private Jsonifier jsonifier;
    private LegacyAnalyticsEventIDDAO legacyAnalyticsEventIDDAO;
    private LegacyProfileMigrationDAO legacyProfileMigrationDAO;
    private MetaDataDAO metaDataDAO;
    private NetworkRequestDAO networkRequestDAO;
    private RedactionDAO redactionDAO;
    private KVStore storage;
    private Context uiContext;
    private Threader uiThreader;
    private UserDAO userDAO;
    private AndroidUserManagerDAO userManagerDAO;

    public AndroidPlatform(Context context2, String str, String str2, String str3) {
        this.context = context2;
        this.apiKey = str;
        this.domain = str2;
        this.appId = str3;
        this.storage = new SupportKeyValueDBStorage(context2);
        AndroidDevice androidDevice = new AndroidDevice(context2, this.storage, this.backupDAO);
        androidDevice.updateDeviceIdInBackupDAO();
        this.device = androidDevice;
        this.userDAO = new AndroidUserDAO(UserDB.getInstance(context2));
        this.userManagerDAO = new AndroidUserManagerDAO(this.storage);
        this.clearedUserDAO = new AndroidClearedUserDAO(UserDB.getInstance(context2));
        this.jsonifier = new AndroidJsonifier();
        this.analyticsEventDAO = new AndroidAnalyticsEventDAO(this.storage);
        this.metaDataDAO = new AndroidMetadataDAO(this.storage);
    }

    public String getAPIKey() {
        return this.apiKey;
    }

    public String getDomain() {
        return this.domain;
    }

    public String getAppId() {
        return this.appId;
    }

    public Device getDevice() {
        return this.device;
    }

    public synchronized ConversationInboxDAO getConversationInboxDAO() {
        if (this.conversationInboxDAO == null) {
            this.conversationInboxDAO = new AndroidConversationInboxDAO(this.context, getKVStore());
        }
        return this.conversationInboxDAO;
    }

    public synchronized ConversationDAO getConversationDAO() {
        if (this.conversationDAO == null) {
            this.conversationDAO = new AndroidConversationDAO(this.context);
        }
        return this.conversationDAO;
    }

    public synchronized FAQSuggestionsDAO getFAQSuggestionsDAO() {
        if (this.conversationDAO == null) {
            this.conversationDAO = new AndroidConversationDAO(this.context);
        }
        return (FAQSuggestionsDAO) this.conversationDAO;
    }

    public MetaDataDAO getMetaDataDAO() {
        return this.metaDataDAO;
    }

    public AnalyticsEventDAO getAnalyticsEventDAO() {
        return this.analyticsEventDAO;
    }

    public synchronized CustomIssueFieldDAO getCustomIssueFieldDAO() {
        if (this.customIssueFieldDAO == null) {
            this.customIssueFieldDAO = new AndroidCustomIssueFieldDAO(getKVStore());
        }
        return this.customIssueFieldDAO;
    }

    public BackupDAO getBackupDAO() {
        return this.backupDAO;
    }

    public ResponseParser getResponseParser() {
        return new AndroidResponseParser();
    }

    public HTTPTransport getHTTPTransport() {
        return new AndroidHTTPTransport();
    }

    public synchronized FAQSearchDM getFAQSearchDM() {
        if (this.faqSearchDM == null) {
            this.faqSearchDM = new AndroidFAQSearchDM(getData());
        }
        return this.faqSearchDM;
    }

    public KVStore getKVStore() {
        return this.storage;
    }

    public Jsonifier getJsonifier() {
        return this.jsonifier;
    }

    public UserManagerDAO getUserManagerDAO() {
        return this.userManagerDAO;
    }

    public UserDAO getUserDAO() {
        return this.userDAO;
    }

    public ClearedUserDAO getClearedUserDAO() {
        return this.clearedUserDAO;
    }

    public synchronized NetworkRequestDAO getNetworkRequestDAO() {
        if (this.networkRequestDAO == null) {
            this.networkRequestDAO = new AndroidNetworkRequestDAO(getKVStore());
        }
        return this.networkRequestDAO;
    }

    public synchronized FaqEventDAO getFaqEventDAO() {
        if (this.faqEventDAO == null) {
            this.faqEventDAO = new AndroidFaqEventDAO(getKVStore());
        }
        return this.faqEventDAO;
    }

    public boolean isCurrentThreadUIThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    public synchronized Threader getUIThreader() {
        if (this.uiThreader == null) {
            this.uiThreader = new Threader() {
                public F thread(final F f) {
                    return new F() {
                        public void f() {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                public void run() {
                                    f.f();
                                }
                            });
                        }
                    };
                }
            };
        }
        return this.uiThreader;
    }

    public synchronized SupportDownloader getDownloader() {
        if (this.downloader == null) {
            this.downloader = new AndroidSupportDownloader(this.context, getKVStore());
        }
        return this.downloader;
    }

    public boolean isSupportedMimeType(String str) {
        return FileUtil.isSupportedMimeType(str);
    }

    public String getMimeTypeForFile(String str) {
        return FileUtil.getMimeType(str);
    }

    public String compressAndStoreScreenshot(String str, String str2) {
        try {
            String copyAttachment = AttachmentUtil.copyAttachment(str, str2);
            if (copyAttachment == null) {
                return str;
            }
            return copyAttachment;
        } catch (IOException e) {
            HSLogger.d(TAG, "Saving attachment", e);
            return str;
        }
    }

    public void compressAndCopyScreenshot(ImagePickerFile imagePickerFile, String str) throws RootAPIException {
        try {
            AttachmentUtil.copyAttachment(imagePickerFile, str);
        } catch (Exception e) {
            throw RootAPIException.wrap(e);
        }
    }

    public int getMinimumConversationDescriptionLength() {
        Context context2;
        if (this.uiContext != null) {
            context2 = this.uiContext;
        } else {
            context2 = this.context;
        }
        return context2.getResources().getInteger(R.integer.hs__issue_description_min_chars);
    }

    public void showNotification(Long l, String str, int i, String str2, boolean z) {
        Context context2;
        if (this.uiContext != null) {
            context2 = this.uiContext;
        } else {
            context2 = ApplicationUtil.getContextWithUpdatedLocale(this.context);
        }
        NotificationCompat.Builder createNotification = SupportNotification.createNotification(context2, l, str, i, str2);
        if (createNotification != null) {
            ApplicationUtil.showNotification(this.context, str, new NotificationChannelsManager(this.context).attachChannelId(createNotification.build(), NotificationChannelsManager.NotificationChannelType.SUPPORT));
            if (z) {
                PluginEventBridge.sendMessage("didReceiveInAppNotificationCount", "" + i);
            }
        }
    }

    public void clearNotifications(String str) {
        ApplicationUtil.cancelNotification(this.context, str, 1);
    }

    public ICampaignsModuleAPIs getCampaignModuleAPIs() {
        return CrossModuleDataProvider.getCampaignModuleAPIs();
    }

    public boolean isOnline() {
        return HelpshiftConnectionUtil.isOnline(this.context);
    }

    public void setUIContext(Object obj) {
        if (obj == null) {
            this.uiContext = null;
        } else if (obj instanceof Context) {
            this.uiContext = (Context) obj;
        }
    }

    public synchronized LegacyProfileMigrationDAO getLegacyUserMigrationDataSource() {
        if (this.legacyProfileMigrationDAO == null) {
            this.legacyProfileMigrationDAO = new AndroidLegacyProfileMigrationDAO(UserDB.getInstance(this.context));
        }
        return this.legacyProfileMigrationDAO;
    }

    public synchronized LegacyAnalyticsEventIDDAO getLegacyAnalyticsEventIDDAO() {
        if (this.legacyAnalyticsEventIDDAO == null) {
            this.legacyAnalyticsEventIDDAO = new AndroidLegacyAnalyticsEventIDDAO(UserDB.getInstance(this.context));
        }
        return this.legacyAnalyticsEventIDDAO;
    }

    public synchronized RedactionDAO getRedactionDAO() {
        if (this.redactionDAO == null) {
            this.redactionDAO = new AndroidRedactionDAO(UserDB.getInstance(this.context));
        }
        return this.redactionDAO;
    }

    public boolean canReadFileAtUri(String str) {
        return HsUriUtils.canReadFileAtUri(this.context, str);
    }

    private synchronized HSApiData getData() {
        if (this.data == null) {
            this.data = new HSApiData(this.context);
        }
        return this.data;
    }
}
