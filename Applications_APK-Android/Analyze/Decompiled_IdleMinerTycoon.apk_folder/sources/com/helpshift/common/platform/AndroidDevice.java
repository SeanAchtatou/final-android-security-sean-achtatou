package com.helpshift.common.platform;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import com.facebook.internal.AnalyticsEvents;
import com.helpshift.common.StringUtils;
import com.helpshift.common.dao.BackupDAO;
import com.helpshift.common.domain.network.NetworkConstants;
import com.helpshift.common.platform.Device;
import com.helpshift.common.util.HSDateFormatSpec;
import com.helpshift.meta.dto.DeviceDiskSpaceDTO;
import com.helpshift.support.util.PermissionUtil;
import com.helpshift.util.ApplicationUtil;
import com.helpshift.util.HSLogger;
import com.tapjoy.TapjoyConstants;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

public class AndroidDevice implements Device {
    public static final String KEY_DEVICE_ID = "key_support_device_id";
    private static final String KEY_PUSH_TOKEN = "key_push_token";
    private BackupDAO backupDAO;
    private String cacheDeviceId;
    private String cachedPushToken;
    private final Context context;
    private KVStore kvStore;

    public String getApiVersion() {
        return NetworkConstants.apiVersion;
    }

    public String getPlatformName() {
        return "Android";
    }

    public String getSDKVersion() {
        return "7.6.0";
    }

    AndroidDevice(Context context2, KVStore kVStore, BackupDAO backupDAO2) {
        this.context = context2;
        this.kvStore = kVStore;
        this.backupDAO = backupDAO2;
    }

    public String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    public int getOSVersionNumber() {
        return Build.VERSION.SDK_INT;
    }

    public String getAppVersion() {
        return ApplicationUtil.getApplicationVersion(this.context);
    }

    public String getAppName() {
        return ApplicationUtil.getApplicationName(this.context);
    }

    public String getAppIdentifier() {
        return this.context.getPackageName();
    }

    public String getLanguage() {
        return Locale.getDefault().toString();
    }

    public String getDeviceModel() {
        return Build.MODEL;
    }

    public String getRom() {
        return System.getProperty("os.version") + ":" + Build.FINGERPRINT;
    }

    public String getSimCountryIso() {
        TelephonyManager telephonyManager = (TelephonyManager) this.context.getSystemService("phone");
        if (telephonyManager == null) {
            return "";
        }
        return telephonyManager.getSimCountryIso();
    }

    public String getTimeStamp() {
        return new SimpleDateFormat(HSDateFormatSpec.STORAGE_TIME_PATTERN, Locale.ENGLISH).format(new Date());
    }

    public String getCarrierName() {
        TelephonyManager telephonyManager = (TelephonyManager) this.context.getSystemService("phone");
        if (telephonyManager == null) {
            return "";
        }
        return telephonyManager.getNetworkOperatorName();
    }

    public String getNetworkType() {
        NetworkInfo activeNetworkInfo;
        String str = null;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) this.context.getSystemService("connectivity");
            if (!(connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null)) {
                str = activeNetworkInfo.getTypeName();
            }
        } catch (SecurityException unused) {
        }
        return str == null ? AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN : str;
    }

    public String getBatteryStatus() {
        Intent registerReceiver = this.context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return "Not charging";
        }
        int intExtra = registerReceiver.getIntExtra("status", -1);
        return intExtra == 2 || intExtra == 5 ? "Charging" : "Not charging";
    }

    public String getBatteryLevel() {
        Intent registerReceiver = this.context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return "";
        }
        int intExtra = (int) ((((float) registerReceiver.getIntExtra("level", -1)) / ((float) registerReceiver.getIntExtra("scale", -1))) * 100.0f);
        return intExtra + "%";
    }

    public DeviceDiskSpaceDTO getDiskSpace() {
        double d;
        double d2;
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        if (Build.VERSION.SDK_INT >= 18) {
            double availableBlocksLong = (double) statFs.getAvailableBlocksLong();
            double blockSizeLong = (double) statFs.getBlockSizeLong();
            Double.isNaN(availableBlocksLong);
            Double.isNaN(blockSizeLong);
            double round = (double) Math.round(((availableBlocksLong * blockSizeLong) / 1.073741824E9d) * 100.0d);
            Double.isNaN(round);
            d = round / 100.0d;
            double blockCountLong = (double) statFs.getBlockCountLong();
            double blockSizeLong2 = (double) statFs.getBlockSizeLong();
            Double.isNaN(blockCountLong);
            Double.isNaN(blockSizeLong2);
            double round2 = (double) Math.round(((blockCountLong * blockSizeLong2) / 1.073741824E9d) * 100.0d);
            Double.isNaN(round2);
            d2 = round2 / 100.0d;
        } else {
            double availableBlocks = (double) statFs.getAvailableBlocks();
            double blockSize = (double) statFs.getBlockSize();
            Double.isNaN(availableBlocks);
            Double.isNaN(blockSize);
            double round3 = (double) Math.round(((availableBlocks * blockSize) / 1.073741824E9d) * 100.0d);
            Double.isNaN(round3);
            d = round3 / 100.0d;
            double blockCount = (double) statFs.getBlockCount();
            double blockSize2 = (double) statFs.getBlockSize();
            Double.isNaN(blockCount);
            Double.isNaN(blockSize2);
            double round4 = (double) Math.round(((blockCount * blockSize2) / 1.073741824E9d) * 100.0d);
            Double.isNaN(round4);
            d2 = round4 / 100.0d;
        }
        return new DeviceDiskSpaceDTO(d2 + " GB", d + " GB", null, null);
    }

    public Device.PermissionState checkPermission(Device.PermissionType permissionType) {
        switch (permissionType) {
            case READ_STORAGE:
                return checkStoragePermissions("android.permission.READ_EXTERNAL_STORAGE");
            case WRITE_STORAGE:
                return checkStoragePermissions("android.permission.WRITE_EXTERNAL_STORAGE");
            default:
                return null;
        }
    }

    public Locale getLocale() {
        Configuration configuration = this.context.getResources().getConfiguration();
        if (Build.VERSION.SDK_INT >= 24) {
            return configuration.getLocales().get(0);
        }
        return configuration.locale;
    }

    public void changeLocale(Locale locale) {
        Resources resources = this.context.getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, displayMetrics);
    }

    public boolean is24HourFormat() {
        return DateFormat.is24HourFormat(this.context);
    }

    public String getTimeZoneId() {
        return TimeZone.getDefault().getID();
    }

    public long getTimeZoneOffSet() {
        TimeZone timeZone = new GregorianCalendar().getTimeZone();
        return (long) (timeZone.getRawOffset() + timeZone.getDSTSavings());
    }

    public String getDeviceId() {
        if (this.cacheDeviceId != null) {
            return this.cacheDeviceId;
        }
        this.cacheDeviceId = this.kvStore.getString(KEY_DEVICE_ID);
        if (StringUtils.isEmpty(this.cacheDeviceId)) {
            this.cacheDeviceId = (String) this.backupDAO.getValue(KEY_DEVICE_ID);
            if (!StringUtils.isEmpty(this.cacheDeviceId)) {
                this.kvStore.setString(KEY_DEVICE_ID, this.cacheDeviceId);
            }
        } else {
            this.backupDAO.storeValue(KEY_DEVICE_ID, this.cacheDeviceId);
        }
        if (StringUtils.isEmpty(this.cacheDeviceId)) {
            this.cacheDeviceId = UUID.randomUUID().toString();
            this.kvStore.setString(KEY_DEVICE_ID, this.cacheDeviceId);
            this.backupDAO.storeValue(KEY_DEVICE_ID, this.cacheDeviceId);
        }
        return this.cacheDeviceId;
    }

    public String getPushToken() {
        if (this.cachedPushToken == null) {
            this.cachedPushToken = this.kvStore.getString(KEY_PUSH_TOKEN);
        }
        return this.cachedPushToken;
    }

    public void setPushToken(String str) {
        this.kvStore.setString(KEY_PUSH_TOKEN, str);
        this.cachedPushToken = str;
    }

    public String getAndroidId() {
        try {
            return Settings.Secure.getString(this.context.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
        } catch (Exception e) {
            HSLogger.e("AndroidDevice", "Exception while getting android_id", e);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void updateDeviceIdInBackupDAO() {
        String string = this.kvStore.getString(KEY_DEVICE_ID);
        if (!StringUtils.isEmpty(string)) {
            this.backupDAO.storeValue(KEY_DEVICE_ID, string);
        }
    }

    private Device.PermissionState checkStoragePermissions(String str) {
        int oSVersionNumber = getOSVersionNumber();
        if (oSVersionNumber < 19) {
            return Device.PermissionState.AVAILABLE;
        }
        if (ApplicationUtil.isPermissionGranted(this.context, str)) {
            return Device.PermissionState.AVAILABLE;
        }
        if (oSVersionNumber < 23) {
            return Device.PermissionState.UNAVAILABLE;
        }
        if (PermissionUtil.hasPermissionInManifest(this.context, str)) {
            return Device.PermissionState.REQUESTABLE;
        }
        return Device.PermissionState.UNAVAILABLE;
    }
}
