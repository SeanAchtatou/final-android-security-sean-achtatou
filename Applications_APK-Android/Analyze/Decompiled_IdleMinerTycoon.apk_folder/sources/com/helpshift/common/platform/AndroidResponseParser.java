package com.helpshift.common.platform;

import com.helpshift.analytics.AnalyticsEventKey;
import com.helpshift.auth.dto.WebSocketAuthData;
import com.helpshift.bots.BotControlActions;
import com.helpshift.common.exception.ParseException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.platform.network.ResponseParser;
import com.helpshift.common.util.HSDateFormatSpec;
import com.helpshift.configuration.response.PeriodicReview;
import com.helpshift.configuration.response.RootServerConfig;
import com.helpshift.constants.Tables;
import com.helpshift.conversation.IssueType;
import com.helpshift.conversation.activeconversation.message.AcceptedAppReviewMessageDM;
import com.helpshift.conversation.activeconversation.message.AdminAttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.AdminBotControlMessageDM;
import com.helpshift.conversation.activeconversation.message.AdminImageAttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.AdminMessageDM;
import com.helpshift.conversation.activeconversation.message.AdminMessageWithOptionInputDM;
import com.helpshift.conversation.activeconversation.message.AdminMessageWithTextInputDM;
import com.helpshift.conversation.activeconversation.message.ConfirmationAcceptedMessageDM;
import com.helpshift.conversation.activeconversation.message.ConfirmationRejectedMessageDM;
import com.helpshift.conversation.activeconversation.message.FAQListMessageDM;
import com.helpshift.conversation.activeconversation.message.FAQListMessageWithOptionInputDM;
import com.helpshift.conversation.activeconversation.message.FollowupAcceptedMessageDM;
import com.helpshift.conversation.activeconversation.message.FollowupRejectedMessageDM;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.message.MessageType;
import com.helpshift.conversation.activeconversation.message.RequestAppReviewMessageDM;
import com.helpshift.conversation.activeconversation.message.RequestForReopenMessageDM;
import com.helpshift.conversation.activeconversation.message.RequestScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.ScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.UnsupportedAdminMessageWithInputDM;
import com.helpshift.conversation.activeconversation.message.UserBotControlMessageDM;
import com.helpshift.conversation.activeconversation.message.UserMessageDM;
import com.helpshift.conversation.activeconversation.message.UserResponseMessageForOptionInput;
import com.helpshift.conversation.activeconversation.message.UserResponseMessageForTextInputDM;
import com.helpshift.conversation.activeconversation.message.input.OptionInput;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.dto.ConversationHistory;
import com.helpshift.conversation.dto.ConversationInbox;
import com.helpshift.conversation.dto.IssueState;
import com.helpshift.conversation.dto.WSPingMessage;
import com.helpshift.conversation.dto.WSTypingActionMessage;
import com.helpshift.conversation.dto.WebSocketMessage;
import com.helpshift.conversation.states.ConversationCSATState;
import com.helpshift.faq.FaqCore;
import com.helpshift.support.constants.FaqsColumns;
import com.helpshift.util.HSJSONUtils;
import com.helpshift.util.HSLogger;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import com.unity.purchasing.googleplay.Consts;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class AndroidResponseParser implements ResponseParser {
    private static final int OPTIONS_MAX_LIMIT = 500;
    private static final String TAG = "Helpshift_AResponseParser";

    AndroidResponseParser() {
    }

    public UserMessageDM parseReadableUserMessage(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("created_at");
            UserMessageDM userMessageDM = new UserMessageDM(jSONObject.getString("body"), string, HSDateFormatSpec.convertToEpochTime(string), jSONObject.getJSONObject("author").getString("name"));
            userMessageDM.serverId = jSONObject.getString("id");
            userMessageDM.deliveryState = convertDeliveryStateToInt(jSONObject.optString("md_state", ""));
            userMessageDM.isRedacted = jSONObject.optBoolean("redacted", false);
            parseAndSetDataForUserSentMessages(userMessageDM, jSONObject);
            return userMessageDM;
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading user text message");
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public UserResponseMessageForTextInputDM parseResponseMessageForTextInput(String str) {
        boolean z;
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("type");
            char c = 65535;
            int i = 3;
            boolean z2 = true;
            switch (string.hashCode()) {
                case -831290677:
                    if (string.equals("rsp_txt_msg_with_email_input")) {
                        c = 2;
                        break;
                    }
                    break;
                case -94670724:
                    if (string.equals("rsp_txt_msg_with_numeric_input")) {
                        c = 3;
                        break;
                    }
                    break;
                case 493654943:
                    if (string.equals("rsp_txt_msg_with_txt_input")) {
                        c = 1;
                        break;
                    }
                    break;
                case 919037346:
                    if (string.equals("rsp_empty_msg_with_txt_input")) {
                        c = 0;
                        break;
                    }
                    break;
                case 2071762039:
                    if (string.equals("rsp_txt_msg_with_dt_input")) {
                        c = 4;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    i = 1;
                    z = true;
                    break;
                case 1:
                    i = 1;
                    z = false;
                    break;
                case 2:
                    i = 2;
                    z = false;
                    break;
                case 3:
                    z = false;
                    break;
                case 4:
                    i = 4;
                    z = false;
                    break;
                default:
                    return null;
            }
            if (z || !jSONObject.getBoolean(TJAdUnitConstants.String.VIDEO_SKIPPED)) {
                z2 = false;
            }
            JSONObject jSONObject2 = jSONObject.getJSONObject("meta");
            String string2 = jSONObject.getString("created_at");
            UserResponseMessageForTextInputDM userResponseMessageForTextInputDM = new UserResponseMessageForTextInputDM(jSONObject.getString("body"), string2, HSDateFormatSpec.convertToEpochTime(string2), jSONObject.getJSONObject("author").getString("name"), i, jSONObject.getJSONObject("chatbot_info").toString(), z2, jSONObject2.getString("refers"), z);
            if (i == 4 && !z2) {
                userResponseMessageForTextInputDM.dateInMillis = jSONObject2.getLong("dt");
                userResponseMessageForTextInputDM.timeZoneId = jSONObject2.optString(TapjoyConstants.TJC_DEVICE_TIMEZONE);
            }
            userResponseMessageForTextInputDM.serverId = jSONObject.getString("id");
            userResponseMessageForTextInputDM.isRedacted = jSONObject.optBoolean("redacted", false);
            try {
                parseAndSetDataForUserSentMessages(userResponseMessageForTextInputDM, jSONObject);
                return userResponseMessageForTextInputDM;
            } catch (JSONException e) {
                e = e;
                throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading user response for text input");
            }
        } catch (JSONException e2) {
            e = e2;
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading user response for text input");
        }
    }

    public ScreenshotMessageDM parseScreenshotMessageDM(String str) {
        try {
            return parseScreenshotMessageDM(new JSONObject(str));
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading reopen message");
        }
    }

    public AcceptedAppReviewMessageDM parseAcceptedAppReviewMessageDM(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("created_at");
            AcceptedAppReviewMessageDM acceptedAppReviewMessageDM = new AcceptedAppReviewMessageDM(jSONObject.getString("body"), string, HSDateFormatSpec.convertToEpochTime(string), jSONObject.getJSONObject("author").getString("name"), jSONObject.getJSONObject("meta").getString("refers"), 2);
            acceptedAppReviewMessageDM.serverId = jSONObject.getString("id");
            acceptedAppReviewMessageDM.deliveryState = convertDeliveryStateToInt(jSONObject.optString("md_state", ""));
            acceptedAppReviewMessageDM.isRedacted = jSONObject.optBoolean("redacted", false);
            parseAndSetDataForUserSentMessages(acceptedAppReviewMessageDM, jSONObject);
            return acceptedAppReviewMessageDM;
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading accepted review message");
        }
    }

    public ConfirmationAcceptedMessageDM parseConfirmationAcceptedMessageDM(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("created_at");
            ConfirmationAcceptedMessageDM confirmationAcceptedMessageDM = new ConfirmationAcceptedMessageDM(jSONObject.getString("body"), string, HSDateFormatSpec.convertToEpochTime(string), jSONObject.getJSONObject("author").getString("name"), 2);
            confirmationAcceptedMessageDM.serverId = jSONObject.getString("id");
            confirmationAcceptedMessageDM.deliveryState = convertDeliveryStateToInt(jSONObject.optString("md_state", ""));
            confirmationAcceptedMessageDM.isRedacted = jSONObject.optBoolean("redacted", false);
            parseAndSetDataForUserSentMessages(confirmationAcceptedMessageDM, jSONObject);
            return confirmationAcceptedMessageDM;
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading confirmation accepted message");
        }
    }

    public ConfirmationRejectedMessageDM parseConfirmationRejectedMessageDM(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("created_at");
            ConfirmationRejectedMessageDM confirmationRejectedMessageDM = new ConfirmationRejectedMessageDM(jSONObject.getString("body"), string, HSDateFormatSpec.convertToEpochTime(string), jSONObject.getJSONObject("author").getString("name"), 2);
            confirmationRejectedMessageDM.serverId = jSONObject.getString("id");
            confirmationRejectedMessageDM.deliveryState = convertDeliveryStateToInt(jSONObject.optString("md_state", ""));
            confirmationRejectedMessageDM.isRedacted = jSONObject.optBoolean("redacted", false);
            parseAndSetDataForUserSentMessages(confirmationRejectedMessageDM, jSONObject);
            return confirmationRejectedMessageDM;
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading confirmation rejected message");
        }
    }

    public ConversationInbox parseConversationInbox(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            ArrayList arrayList = new ArrayList();
            JSONArray jSONArray = jSONObject.getJSONArray("issues");
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(parseReadableConversation(jSONArray.getJSONObject(i).toString()));
            }
            Boolean bool = null;
            if (jSONObject.has("has_older_messages")) {
                bool = Boolean.valueOf(jSONObject.getBoolean("has_older_messages"));
            }
            return new ConversationInbox(jSONObject.getString("cursor"), arrayList, jSONObject.getBoolean("issue_exists"), bool);
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading conversation inbox");
        }
    }

    public ConversationHistory parseConversationHistory(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            ArrayList arrayList = new ArrayList();
            JSONArray jSONArray = jSONObject.getJSONArray("issues");
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(parseReadableConversation(jSONArray.getJSONObject(i).toString()));
            }
            return new ConversationHistory(arrayList, jSONObject.getBoolean("has_older_messages"));
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading conversation history");
        }
    }

    public FollowupRejectedMessageDM parseFollowupRejectedMessage(String str) {
        try {
            return parseFollowupRejectedMessageDM(new JSONObject(str));
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading follow-up rejected message");
        }
    }

    public FollowupAcceptedMessageDM parseFollowupAcceptedMessage(String str) {
        try {
            return parseFollowupAcceptedMessageDM(new JSONObject(str));
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading follow-up accepted message");
        }
    }

    public WebSocketAuthData parseAuthToken(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            return new WebSocketAuthData(jSONObject.getString("token"), jSONObject.getString("endpoint"));
        } catch (JSONException e) {
            HSLogger.e(TAG, "Exception in parsing auth token", e);
            return null;
        }
    }

    public WebSocketMessage parseWebSocketMessage(String str) {
        WSTypingActionMessage wSTypingActionMessage;
        WSTypingActionMessage wSTypingActionMessage2;
        try {
            JSONArray jSONArray = new JSONArray(str);
            int i = jSONArray.getInt(0);
            if (i == 100) {
                JSONArray jSONArray2 = jSONArray.getJSONArray(2);
                wSTypingActionMessage = null;
                int i2 = 0;
                while (i2 < jSONArray2.length()) {
                    try {
                        JSONObject jSONObject = new JSONObject(jSONArray2.getJSONObject(i2).getString("m"));
                        if ("agent_type_activity".equals(jSONObject.getString("stream"))) {
                            String string = jSONObject.getString("action");
                            if ("start".equals(string)) {
                                wSTypingActionMessage2 = new WSTypingActionMessage(true, TimeUnit.SECONDS.toMillis(jSONObject.getLong("ttl")));
                            } else if ("stop".equals(string)) {
                                wSTypingActionMessage2 = new WSTypingActionMessage(false, 0);
                            }
                            wSTypingActionMessage = wSTypingActionMessage2;
                        }
                        i2++;
                    } catch (JSONException e) {
                        e = e;
                        HSLogger.e(TAG, "Exception in parsing web-socket message", e);
                        return wSTypingActionMessage;
                    }
                }
                return wSTypingActionMessage;
            } else if (i != 107) {
                return null;
            } else {
                return new WSPingMessage(TimeUnit.SECONDS.toMillis(jSONArray.getLong(1)));
            }
        } catch (JSONException e2) {
            e = e2;
            wSTypingActionMessage = null;
            HSLogger.e(TAG, "Exception in parsing web-socket message", e);
            return wSTypingActionMessage;
        }
    }

    private boolean parseDisableHelpshiftBrandingValue(JSONObject jSONObject) {
        if (jSONObject != null) {
            return !jSONObject.optString("hl", "true").equals("true");
        }
        return false;
    }

    private List<MessageDM> parseMessageDMs(JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            try {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                String string = jSONObject.getString("type");
                String string2 = jSONObject.getString("origin");
                if ("admin".equals(string2)) {
                    parseAdminMessage(string, jSONObject, arrayList);
                } else if (TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE.equals(string2)) {
                    parseMobileMessage(string, jSONObject, arrayList);
                } else {
                    HSLogger.e(TAG, "Unknown message type received.");
                }
            } catch (RootAPIException | JSONException e) {
                HSLogger.e(TAG, "Exception while parsing messages: ", e);
            }
        }
        return arrayList;
    }

    private void parseAdminMessage(String str, JSONObject jSONObject, List<MessageDM> list) {
        char c = 65535;
        try {
            switch (str.hashCode()) {
                case -1052767485:
                    if (str.equals("faq_list_msg_with_option_input")) {
                        c = 11;
                        break;
                    }
                    break;
                case -185416997:
                    if (str.equals("txt_msg_with_email_input")) {
                        c = 2;
                        break;
                    }
                    break;
                case 112675:
                    if (str.equals("rar")) {
                        c = 7;
                        break;
                    }
                    break;
                case 112830:
                    if (str.equals("rfr")) {
                        c = 9;
                        break;
                    }
                    break;
                case 113218:
                    if (str.equals("rsc")) {
                        c = 8;
                        break;
                    }
                    break;
                case 115312:
                    if (str.equals("txt")) {
                        c = 0;
                        break;
                    }
                    break;
                case 133418599:
                    if (str.equals("txt_msg_with_dt_input")) {
                        c = 4;
                        break;
                    }
                    break;
                case 373335180:
                    if (str.equals("txt_msg_with_option_input")) {
                        c = 6;
                        break;
                    }
                    break;
                case 534550447:
                    if (str.equals("txt_msg_with_txt_input")) {
                        c = 1;
                        break;
                    }
                    break;
                case 892689447:
                    if (str.equals("faq_list")) {
                        c = 10;
                        break;
                    }
                    break;
                case 903982601:
                    if (str.equals(BotControlActions.BOT_STARTED)) {
                        c = 12;
                        break;
                    }
                    break;
                case 1564911026:
                    if (str.equals("empty_msg_with_txt_input")) {
                        c = 5;
                        break;
                    }
                    break;
                case 1829173826:
                    if (str.equals(BotControlActions.BOT_ENDED)) {
                        c = 13;
                        break;
                    }
                    break;
                case 2114645132:
                    if (str.equals("txt_msg_with_numeric_input")) {
                        c = 3;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    list.addAll(parseAdminMessageDM(jSONObject));
                    return;
                case 1:
                    list.add(parseAdminMessageWithTextInputDM(jSONObject, 1));
                    return;
                case 2:
                    list.add(parseAdminMessageWithTextInputDM(jSONObject, 2));
                    return;
                case 3:
                    list.add(parseAdminMessageWithTextInputDM(jSONObject, 3));
                    return;
                case 4:
                    list.add(parseAdminMessageWithTextInputDM(jSONObject, 4));
                    return;
                case 5:
                    list.add(parseAdminEmptyMessageWithTextInputDM(jSONObject));
                    return;
                case 6:
                    list.add(parseAdminMessageWithOptionInputDM(jSONObject));
                    return;
                case 7:
                    list.add(parseRequestAppReviewMessageDM(jSONObject));
                    return;
                case 8:
                    list.addAll(parseRequestScreenshotMessageDM(jSONObject));
                    return;
                case 9:
                    list.add(parseRequestForReopenMessageDM(jSONObject));
                    return;
                case 10:
                    list.add(parseFAQListMessageDM(jSONObject));
                    return;
                case 11:
                    list.add(parseFAQListMessageWitOptionInputDM(jSONObject));
                    return;
                case 12:
                case 13:
                    list.add(parseBotControlMessage(jSONObject.toString(), true));
                    return;
                default:
                    if (jSONObject.has("input")) {
                        list.add(parseUnsupportedAdminMessageWithInput(jSONObject.toString()));
                        return;
                    }
                    return;
            }
        } catch (RootAPIException e) {
            HSLogger.e(TAG, "Exception while parsing messages: ", e);
        }
    }

    private void parseMobileMessage(String str, JSONObject jSONObject, List<MessageDM> list) {
        char c = 65535;
        try {
            switch (str.hashCode()) {
                case -831290677:
                    if (str.equals("rsp_txt_msg_with_email_input")) {
                        c = 9;
                        break;
                    }
                    break;
                case -657647885:
                    if (str.equals("rsp_faq_list_msg_with_option_input")) {
                        c = 13;
                        break;
                    }
                    break;
                case -545696551:
                    if (str.equals(BotControlActions.BOT_CANCELLED)) {
                        c = 14;
                        break;
                    }
                    break;
                case -94670724:
                    if (str.equals("rsp_txt_msg_with_numeric_input")) {
                        c = 10;
                        break;
                    }
                    break;
                case 3121:
                    if (str.equals("ar")) {
                        c = 1;
                        break;
                    }
                    break;
                case 3166:
                    if (str.equals("ca")) {
                        c = 3;
                        break;
                    }
                    break;
                case 3631:
                    if (str.equals("ra")) {
                        c = 5;
                        break;
                    }
                    break;
                case 3640:
                    if (str.equals("rj")) {
                        c = 6;
                        break;
                    }
                    break;
                case 3664:
                    if (str.equals("sc")) {
                        c = 4;
                        break;
                    }
                    break;
                case 108893:
                    if (str.equals("ncr")) {
                        c = 2;
                        break;
                    }
                    break;
                case 115312:
                    if (str.equals("txt")) {
                        c = 0;
                        break;
                    }
                    break;
                case 493654943:
                    if (str.equals("rsp_txt_msg_with_txt_input")) {
                        c = 8;
                        break;
                    }
                    break;
                case 919037346:
                    if (str.equals("rsp_empty_msg_with_txt_input")) {
                        c = 7;
                        break;
                    }
                    break;
                case 1826087580:
                    if (str.equals("rsp_txt_msg_with_option_input")) {
                        c = 12;
                        break;
                    }
                    break;
                case 2071762039:
                    if (str.equals("rsp_txt_msg_with_dt_input")) {
                        c = 11;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    list.add(parseReadableUserMessage(jSONObject.toString()));
                    return;
                case 1:
                    list.add(parseAcceptedAppReviewMessageDM(jSONObject.toString()));
                    return;
                case 2:
                    list.add(parseConfirmationRejectedMessageDM(jSONObject.toString()));
                    return;
                case 3:
                    list.add(parseConfirmationAcceptedMessageDM(jSONObject.toString()));
                    return;
                case 4:
                    list.add(parseScreenshotMessageDM(jSONObject));
                    return;
                case 5:
                    list.add(parseFollowupAcceptedMessageDM(jSONObject));
                    return;
                case 6:
                    list.add(parseFollowupRejectedMessageDM(jSONObject));
                    return;
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                    list.add(parseResponseMessageForTextInput(jSONObject.toString()));
                    return;
                case 12:
                case 13:
                    list.add(parseResponseMessageForOptionInput(jSONObject.toString()));
                    return;
                case 14:
                    list.add(parseBotControlMessage(jSONObject.toString(), false));
                    return;
                default:
                    return;
            }
        } catch (RootAPIException e) {
            HSLogger.e(TAG, "Exception while parsing messages: ", e);
        }
    }

    public MessageDM parseBotControlMessage(String str, boolean z) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("type");
            String string2 = jSONObject.getString("id");
            String jSONObject2 = jSONObject.getJSONObject("chatbot_info").toString();
            boolean optBoolean = jSONObject.optBoolean("redacted", false);
            String string3 = jSONObject.getString("created_at");
            long convertToEpochTime = HSDateFormatSpec.convertToEpochTime(string3);
            if (z) {
                AdminBotControlMessageDM adminBotControlMessageDM = new AdminBotControlMessageDM(string2, jSONObject.getString("body"), string3, convertToEpochTime, jSONObject.getJSONObject("author").getString("name"), string, jSONObject2);
                adminBotControlMessageDM.hasNextBot = jSONObject.optBoolean("has_next_bot", false);
                adminBotControlMessageDM.isRedacted = optBoolean;
                return adminBotControlMessageDM;
            }
            JSONObject jSONObject3 = jSONObject.getJSONObject("meta");
            UserBotControlMessageDM userBotControlMessageDM = new UserBotControlMessageDM(jSONObject.getString("body"), string3, convertToEpochTime, jSONObject.getJSONObject("author").getString("name"), string, jSONObject3.getString("chatbot_cancelled_reason"), jSONObject2, jSONObject3.getString("refers"), 2);
            userBotControlMessageDM.serverId = string2;
            userBotControlMessageDM.isRedacted = optBoolean;
            try {
                parseAndSetDataForUserSentMessages(userBotControlMessageDM, jSONObject);
                return userBotControlMessageDM;
            } catch (JSONException e) {
                e = e;
                throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading bot control messages.");
            }
        } catch (JSONException e2) {
            e = e2;
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading bot control messages.");
        }
    }

    private List<FAQListMessageDM.FAQ> parseFAQList(JSONArray jSONArray) throws JSONException {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject = jSONArray.getJSONObject(i);
            JSONObject jSONObject2 = jSONObject.getJSONObject("data");
            arrayList.add(new FAQListMessageDM.FAQ(jSONObject.getString("title"), jSONObject2.getString("publish_id"), jSONObject2.getString(FaqsColumns.LANGUAGE)));
        }
        return arrayList;
    }

    private MessageDM parseFAQListMessageWitOptionInputDM(JSONObject jSONObject) {
        JSONObject jSONObject2 = jSONObject;
        try {
            JSONObject jSONObject3 = jSONObject2.getJSONObject("input");
            String string = jSONObject2.getString("created_at");
            FAQListMessageWithOptionInputDM fAQListMessageWithOptionInputDM = new FAQListMessageWithOptionInputDM(jSONObject2.getString("id"), jSONObject2.getString("body"), string, HSDateFormatSpec.convertToEpochTime(string), jSONObject2.getJSONObject("author").getString("name"), parseFAQList(jSONObject2.getJSONArray(Tables.FAQS)), jSONObject2.getJSONObject("chatbot_info").toString(), jSONObject3.getBoolean("required"), jSONObject3.getString("label"), jSONObject3.optString("skip_label"), parseOptions(jSONObject3));
            fAQListMessageWithOptionInputDM.deliveryState = convertDeliveryStateToInt(jSONObject2.optString("md_state", ""));
            fAQListMessageWithOptionInputDM.isRedacted = jSONObject2.optBoolean("redacted", false);
            return fAQListMessageWithOptionInputDM;
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading list message with option input");
        }
    }

    private FAQListMessageDM parseFAQListMessageDM(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("created_at");
            FAQListMessageDM fAQListMessageDM = new FAQListMessageDM(jSONObject.getString("id"), jSONObject.getString("body"), string, HSDateFormatSpec.convertToEpochTime(string), jSONObject.getJSONObject("author").getString("name"), parseFAQList(jSONObject.getJSONArray(Tables.FAQS)));
            fAQListMessageDM.deliveryState = convertDeliveryStateToInt(jSONObject.optString("md_state", ""));
            fAQListMessageDM.isRedacted = jSONObject.optBoolean("redacted", false);
            return fAQListMessageDM;
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading faq list message");
        }
    }

    private FollowupRejectedMessageDM parseFollowupRejectedMessageDM(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("created_at");
            FollowupRejectedMessageDM followupRejectedMessageDM = new FollowupRejectedMessageDM(jSONObject.getString("body"), string, HSDateFormatSpec.convertToEpochTime(string), jSONObject.getJSONObject("author").getString("name"), jSONObject.getJSONObject("meta").getString("refers"), 2);
            followupRejectedMessageDM.serverId = jSONObject.getString("id");
            followupRejectedMessageDM.deliveryState = convertDeliveryStateToInt(jSONObject.optString("md_state", ""));
            followupRejectedMessageDM.isRedacted = jSONObject.optBoolean("redacted", false);
            parseAndSetDataForUserSentMessages(followupRejectedMessageDM, jSONObject);
            return followupRejectedMessageDM;
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading follow-up rejected message");
        }
    }

    private FollowupAcceptedMessageDM parseFollowupAcceptedMessageDM(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("created_at");
            FollowupAcceptedMessageDM followupAcceptedMessageDM = new FollowupAcceptedMessageDM(jSONObject.getString("body"), string, HSDateFormatSpec.convertToEpochTime(string), jSONObject.getJSONObject("author").getString("name"), jSONObject.getJSONObject("meta").getString("refers"), 2);
            followupAcceptedMessageDM.serverId = jSONObject.getString("id");
            followupAcceptedMessageDM.deliveryState = convertDeliveryStateToInt(jSONObject.optString("md_state", ""));
            followupAcceptedMessageDM.isRedacted = jSONObject.optBoolean("redacted", false);
            parseAndSetDataForUserSentMessages(followupAcceptedMessageDM, jSONObject);
            return followupAcceptedMessageDM;
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading follow-up accepted message");
        }
    }

    private RequestForReopenMessageDM parseRequestForReopenMessageDM(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("created_at");
            RequestForReopenMessageDM requestForReopenMessageDM = new RequestForReopenMessageDM(jSONObject.getString("id"), jSONObject.getString("body"), string, HSDateFormatSpec.convertToEpochTime(string), jSONObject.getJSONObject("author").getString("name"));
            requestForReopenMessageDM.deliveryState = convertDeliveryStateToInt(jSONObject.optString("md_state", ""));
            requestForReopenMessageDM.isRedacted = jSONObject.optBoolean("redacted", false);
            return requestForReopenMessageDM;
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading reopen message");
        }
    }

    private List<OptionInput.Option> parseOptions(JSONObject jSONObject) throws JSONException {
        ArrayList arrayList = new ArrayList();
        JSONArray jSONArray = jSONObject.getJSONArray("options");
        int min = Math.min(jSONArray.length(), 500);
        for (int i = 0; i < min; i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            arrayList.add(new OptionInput.Option(jSONObject2.getString("title"), jSONObject2.getJSONObject("data").toString()));
        }
        return arrayList;
    }

    private OptionInput.Type parseOptionType(JSONObject jSONObject) throws JSONException {
        return OptionInput.Type.getType(jSONObject.optString("type"), jSONObject.getJSONArray("options").length());
    }

    private MessageDM parseAdminMessageWithOptionInputDM(JSONObject jSONObject) {
        JSONObject jSONObject2 = jSONObject;
        try {
            JSONObject jSONObject3 = jSONObject2.getJSONObject("input");
            String string = jSONObject2.getString("created_at");
            AdminMessageWithOptionInputDM adminMessageWithOptionInputDM = new AdminMessageWithOptionInputDM(jSONObject2.getString("id"), jSONObject2.getString("body"), string, HSDateFormatSpec.convertToEpochTime(string), jSONObject2.getJSONObject("author").getString("name"), jSONObject2.getJSONObject("chatbot_info").toString(), jSONObject3.getBoolean("required"), jSONObject3.getString("label"), jSONObject3.optString("skip_label"), parseOptions(jSONObject3), parseOptionType(jSONObject3));
            adminMessageWithOptionInputDM.deliveryState = convertDeliveryStateToInt(jSONObject2.optString("md_state", ""));
            adminMessageWithOptionInputDM.isRedacted = jSONObject2.optBoolean("redacted", false);
            return adminMessageWithOptionInputDM;
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading admin text message with option input");
        }
    }

    public UserResponseMessageForOptionInput parseResponseMessageForOptionInput(String str) {
        MessageType messageType;
        String jSONObject;
        try {
            JSONObject jSONObject2 = new JSONObject(str);
            String string = jSONObject2.getString("type");
            char c = 65535;
            int hashCode = string.hashCode();
            if (hashCode != -657647885) {
                if (hashCode == 1826087580) {
                    if (string.equals("rsp_txt_msg_with_option_input")) {
                        c = 0;
                    }
                }
            } else if (string.equals("rsp_faq_list_msg_with_option_input")) {
                c = 1;
            }
            switch (c) {
                case 0:
                    messageType = MessageType.ADMIN_TEXT_WITH_OPTION_INPUT;
                    break;
                case 1:
                    messageType = MessageType.FAQ_LIST_WITH_OPTION_INPUT;
                    break;
                default:
                    return null;
            }
            MessageType messageType2 = messageType;
            boolean z = jSONObject2.getBoolean(TJAdUnitConstants.String.VIDEO_SKIPPED);
            if (z) {
                jSONObject = "{}";
            } else {
                jSONObject = jSONObject2.getJSONObject("option_data").toString();
            }
            String str2 = jSONObject;
            String string2 = jSONObject2.getString("created_at");
            UserResponseMessageForOptionInput userResponseMessageForOptionInput = new UserResponseMessageForOptionInput(jSONObject2.getString("body"), string2, HSDateFormatSpec.convertToEpochTime(string2), jSONObject2.getJSONObject("author").getString("name"), jSONObject2.getJSONObject("chatbot_info").toString(), z, str2, jSONObject2.getJSONObject("meta").getString("refers"), messageType2);
            userResponseMessageForOptionInput.serverId = jSONObject2.getString("id");
            userResponseMessageForOptionInput.isRedacted = jSONObject2.optBoolean("redacted", false);
            try {
                parseAndSetDataForUserSentMessages(userResponseMessageForOptionInput, jSONObject2);
                return userResponseMessageForOptionInput;
            } catch (JSONException e) {
                e = e;
                throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading user response for option input");
            }
        } catch (JSONException e2) {
            e = e2;
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading user response for option input");
        }
    }

    private AdminMessageWithTextInputDM parseAdminEmptyMessageWithTextInputDM(JSONObject jSONObject) {
        JSONObject jSONObject2 = jSONObject;
        try {
            JSONObject jSONObject3 = jSONObject2.getJSONObject("input");
            String string = jSONObject2.getString("created_at");
            AdminMessageWithTextInputDM adminMessageWithTextInputDM = new AdminMessageWithTextInputDM(jSONObject2.getString("id"), "", string, HSDateFormatSpec.convertToEpochTime(string), jSONObject2.getJSONObject("author").getString("name"), jSONObject2.getJSONObject("chatbot_info").toString(), jSONObject3.getString("placeholder"), jSONObject3.getBoolean("required"), jSONObject3.getString("label"), jSONObject3.optString("skip_label"), 1, true);
            try {
                adminMessageWithTextInputDM.deliveryState = convertDeliveryStateToInt(jSONObject2.optString("md_state", ""));
                adminMessageWithTextInputDM.isRedacted = jSONObject2.optBoolean("redacted", false);
                return adminMessageWithTextInputDM;
            } catch (JSONException e) {
                e = e;
                throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading admin empty message with text input");
            }
        } catch (JSONException e2) {
            e = e2;
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading admin empty message with text input");
        }
    }

    private AdminMessageWithTextInputDM parseAdminMessageWithTextInputDM(JSONObject jSONObject, int i) {
        JSONObject jSONObject2 = jSONObject;
        try {
            JSONObject jSONObject3 = jSONObject2.getJSONObject("input");
            String string = jSONObject2.getString("created_at");
            AdminMessageWithTextInputDM adminMessageWithTextInputDM = new AdminMessageWithTextInputDM(jSONObject2.getString("id"), jSONObject2.getString("body"), string, HSDateFormatSpec.convertToEpochTime(string), jSONObject2.getJSONObject("author").getString("name"), jSONObject2.getJSONObject("chatbot_info").toString(), jSONObject3.getString("placeholder"), jSONObject3.getBoolean("required"), jSONObject3.getString("label"), jSONObject3.optString("skip_label"), i, false);
            try {
                adminMessageWithTextInputDM.deliveryState = convertDeliveryStateToInt(jSONObject2.optString("md_state", ""));
                adminMessageWithTextInputDM.isRedacted = jSONObject2.optBoolean("redacted", false);
                return adminMessageWithTextInputDM;
            } catch (JSONException e) {
                e = e;
                throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading admin message with text input");
            }
        } catch (JSONException e2) {
            e = e2;
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading admin message with text input");
        }
    }

    private List<MessageDM> parseAdminMessageDM(JSONObject jSONObject) {
        try {
            ArrayList arrayList = new ArrayList();
            JSONArray optJSONArray = jSONObject.getJSONObject("meta").optJSONArray("attachments");
            String string = jSONObject.getString("created_at");
            AdminMessageDM adminMessageDM = new AdminMessageDM(jSONObject.getString("id"), jSONObject.getString("body"), string, HSDateFormatSpec.convertToEpochTime(string), jSONObject.getJSONObject("author").getString("name"));
            adminMessageDM.deliveryState = convertDeliveryStateToInt(jSONObject.optString("md_state", ""));
            adminMessageDM.isRedacted = jSONObject.optBoolean("redacted", false);
            arrayList.add(adminMessageDM);
            if (optJSONArray != null) {
                arrayList.addAll(parseAdminAttachmentEntities(jSONObject, optJSONArray));
            }
            return arrayList;
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading admin text message");
        }
    }

    private UnsupportedAdminMessageWithInputDM parseUnsupportedAdminMessageWithInput(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("created_at");
            return new UnsupportedAdminMessageWithInputDM(jSONObject.getString("id"), jSONObject.getString("body"), string, HSDateFormatSpec.convertToEpochTime(string), jSONObject.getJSONObject("author").getString("name"), jSONObject.getString("type"), jSONObject.getJSONObject("chatbot_info").toString(), jSONObject.getJSONObject("input").toString());
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading unsupported admin message with input");
        }
    }

    public RootServerConfig parseConfigResponse(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            Long l = null;
            Long valueOf = jSONObject.has("last_redaction_at") ? Long.valueOf(jSONObject.getLong("last_redaction_at")) : null;
            if (jSONObject.has("profile_created_at")) {
                l = Long.valueOf(jSONObject.getLong("profile_created_at"));
            }
            boolean optBoolean = jSONObject.optBoolean("afp", false);
            return new RootServerConfig(jSONObject.optBoolean("rne", false), jSONObject.optBoolean("pfe", true), jSONObject.optBoolean("san", true), jSONObject.optBoolean("csat", false), jSONObject.optBoolean("dia", false), parseDisableHelpshiftBrandingValue(jSONObject.optJSONObject("t")), jSONObject.optBoolean("issue_exists", true), jSONObject.optInt("dbgl", 100), jSONObject.optInt("bcl", 100), jSONObject.optString("rurl", ""), parsePeriodicReview(jSONObject.getJSONObject("pr")), jSONObject.optBoolean("ic", false), jSONObject.optString("gm", ""), jSONObject.optBoolean("tyi", true), jSONObject.optBoolean("rq", true), jSONObject.optBoolean("conversation_history_enabled", false), valueOf, l, jSONObject.optBoolean("allow_user_attachments", true), jSONObject.optLong("pfi", 0) / 1000, jSONObject.optLong("pri", 0) / 1000, optBoolean);
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while fetching config");
        }
    }

    public Conversation parseReadableConversation(String str) {
        String str2;
        try {
            JSONObject jSONObject = new JSONObject(str);
            try {
                List<MessageDM> parseMessageDMs = parseMessageDMs(jSONObject.getJSONArray("messages"));
                int size = parseMessageDMs.size() - 1;
                while (true) {
                    if (size < 0) {
                        str2 = null;
                        break;
                    }
                    MessageDM messageDM = parseMessageDMs.get(size);
                    if (!(messageDM instanceof AdminAttachmentMessageDM) && !(messageDM instanceof AdminImageAttachmentMessageDM)) {
                        str2 = messageDM.getCreatedAt();
                        break;
                    }
                    size--;
                }
                IssueState fromInt = IssueState.fromInt(jSONObject.getInt("state"));
                String string = jSONObject.getString("created_at");
                long convertToEpochTime = HSDateFormatSpec.convertToEpochTime(string);
                String string2 = jSONObject.getString("type");
                Conversation conversation = r6;
                Conversation conversation2 = new Conversation(jSONObject.optString("title", ""), fromInt, string, convertToEpochTime, jSONObject.getString("updated_at"), jSONObject.getString("publish_id"), str2, jSONObject.optBoolean("show-agent-name", true), string2);
                conversation.isRedacted = jSONObject.optBoolean("redacted", false);
                conversation.serverId = jSONObject.isNull(AnalyticsEventKey.ISSUE_ID) ? null : jSONObject.getString(AnalyticsEventKey.ISSUE_ID);
                conversation.preConversationServerId = jSONObject.isNull(AnalyticsEventKey.PREISSUE_ID) ? null : jSONObject.getString(AnalyticsEventKey.PREISSUE_ID);
                conversation.issueType = string2;
                conversation.createdRequestId = jSONObject.optString(Consts.INAPP_REQUEST_ID);
                if (IssueType.ISSUE.equals(string2)) {
                    conversation.csatState = jSONObject.optBoolean("csat_received") ? ConversationCSATState.SUBMITTED_SYNCED : ConversationCSATState.NONE;
                }
                conversation.setMessageDMs(parseMessageDMs);
                return conversation;
            } catch (JSONException e) {
                e = e;
                throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception in reading conversation");
            }
        } catch (JSONException e2) {
            e = e2;
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception in reading conversation");
        }
    }

    public FaqCore parseSingleFAQ(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            return new FaqCore(jSONObject.getString("id"), jSONObject.getString("publish_id"), jSONObject.getString(FaqsColumns.LANGUAGE), jSONObject.getString("section_id"), jSONObject.getString("title"), jSONObject.getString("body"), 0, Boolean.valueOf(jSONObject.getString("is_rtl") == "true"), jSONObject.has("stags") ? HSJSONUtils.jsonToStringArrayList(jSONObject.getString("stags")) : new ArrayList<>(), jSONObject.has("issue_tags") ? HSJSONUtils.jsonToStringArrayList(jSONObject.getString("issue_tags")) : new ArrayList<>());
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading single faq");
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v6, types: [com.helpshift.conversation.activeconversation.message.MessageDM, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r8v11, types: [com.helpshift.conversation.activeconversation.message.AdminAttachmentMessageDM] */
    /* JADX WARN: Type inference failed for: r8v12, types: [com.helpshift.conversation.activeconversation.message.AdminImageAttachmentMessageDM] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00ac A[Catch:{ JSONException -> 0x00e1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00c6 A[Catch:{ JSONException -> 0x00e1 }] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List<com.helpshift.conversation.activeconversation.message.MessageDM> parseAdminAttachmentEntities(org.json.JSONObject r23, org.json.JSONArray r24) {
        /*
            r22 = this;
            r0 = r23
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ JSONException -> 0x00e1 }
            r1.<init>()     // Catch:{ JSONException -> 0x00e1 }
            r2 = 0
            r3 = 0
        L_0x0009:
            int r4 = r24.length()     // Catch:{ JSONException -> 0x00e1 }
            if (r3 >= r4) goto L_0x00e0
            r4 = r24
            org.json.JSONObject r5 = r4.getJSONObject(r3)     // Catch:{ JSONException -> 0x00e1 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x00e1 }
            r6.<init>()     // Catch:{ JSONException -> 0x00e1 }
            java.lang.String r7 = "id"
            java.lang.String r7 = r0.getString(r7)     // Catch:{ JSONException -> 0x00e1 }
            r6.append(r7)     // Catch:{ JSONException -> 0x00e1 }
            java.lang.String r7 = "_"
            r6.append(r7)     // Catch:{ JSONException -> 0x00e1 }
            r6.append(r3)     // Catch:{ JSONException -> 0x00e1 }
            java.lang.String r9 = r6.toString()     // Catch:{ JSONException -> 0x00e1 }
            java.lang.String r6 = "created_at"
            java.lang.String r6 = r0.getString(r6)     // Catch:{ JSONException -> 0x00e1 }
            com.helpshift.common.util.HSSimpleDateFormat r7 = com.helpshift.common.util.HSDateFormatSpec.STORAGE_TIME_FORMAT     // Catch:{ JSONException -> 0x00e1 }
            int r3 = r3 + 1
            java.lang.String r11 = com.helpshift.common.util.HSDateFormatSpec.addMilliSeconds(r7, r6, r3)     // Catch:{ JSONException -> 0x00e1 }
            long r12 = com.helpshift.common.util.HSDateFormatSpec.convertToEpochTime(r11)     // Catch:{ JSONException -> 0x00e1 }
            java.lang.String r6 = "body"
            boolean r6 = r5.has(r6)     // Catch:{ JSONException -> 0x00e1 }
            if (r6 == 0) goto L_0x0050
            java.lang.String r6 = "body"
            java.lang.String r6 = r5.getString(r6)     // Catch:{ JSONException -> 0x00e1 }
            goto L_0x0056
        L_0x0050:
            java.lang.String r6 = "body"
            java.lang.String r6 = r0.getString(r6)     // Catch:{ JSONException -> 0x00e1 }
        L_0x0056:
            r10 = r6
            java.lang.String r6 = "author"
            org.json.JSONObject r6 = r0.getJSONObject(r6)     // Catch:{ JSONException -> 0x00e1 }
            java.lang.String r7 = "name"
            java.lang.String r14 = r6.getString(r7)     // Catch:{ JSONException -> 0x00e1 }
            java.lang.String r6 = "url"
            java.lang.String r17 = r5.getString(r6)     // Catch:{ JSONException -> 0x00e1 }
            java.lang.String r6 = "content-type"
            java.lang.String r6 = r5.getString(r6)     // Catch:{ JSONException -> 0x00e1 }
            java.lang.String r7 = "size"
            int r7 = r5.getInt(r7)     // Catch:{ JSONException -> 0x00e1 }
            java.lang.String r8 = "file-name"
            java.lang.String r18 = r5.getString(r8)     // Catch:{ JSONException -> 0x00e1 }
            java.lang.String r8 = "secure?"
            boolean r19 = r5.optBoolean(r8, r2)     // Catch:{ JSONException -> 0x00e1 }
            java.lang.String r8 = "redacted"
            boolean r8 = r0.optBoolean(r8, r2)     // Catch:{ JSONException -> 0x00e1 }
            if (r8 != 0) goto L_0x0094
            java.lang.String r8 = "redacted"
            boolean r8 = r5.optBoolean(r8, r2)     // Catch:{ JSONException -> 0x00e1 }
            if (r8 == 0) goto L_0x0092
            goto L_0x0094
        L_0x0092:
            r15 = 0
            goto L_0x0096
        L_0x0094:
            r8 = 1
            r15 = 1
        L_0x0096:
            java.lang.String r8 = "md_state"
            java.lang.String r2 = ""
            java.lang.String r2 = r0.optString(r8, r2)     // Catch:{ JSONException -> 0x00e1 }
            r8 = r22
            int r2 = r8.convertDeliveryStateToInt(r2)     // Catch:{ JSONException -> 0x00e1 }
            java.lang.String r0 = "image"
            boolean r0 = r5.optBoolean(r0)     // Catch:{ JSONException -> 0x00e1 }
            if (r0 == 0) goto L_0x00c6
            com.helpshift.conversation.activeconversation.message.AdminImageAttachmentMessageDM r0 = new com.helpshift.conversation.activeconversation.message.AdminImageAttachmentMessageDM     // Catch:{ JSONException -> 0x00e1 }
            r21 = r3
            java.lang.String r3 = "thumbnail"
            java.lang.String r3 = r5.getString(r3)     // Catch:{ JSONException -> 0x00e1 }
            r8 = r0
            r5 = r15
            r15 = r17
            r16 = r18
            r17 = r3
            r18 = r6
            r20 = r7
            r8.<init>(r9, r10, r11, r12, r14, r15, r16, r17, r18, r19, r20)     // Catch:{ JSONException -> 0x00e1 }
            goto L_0x00d2
        L_0x00c6:
            r21 = r3
            r5 = r15
            com.helpshift.conversation.activeconversation.message.AdminAttachmentMessageDM r0 = new com.helpshift.conversation.activeconversation.message.AdminAttachmentMessageDM     // Catch:{ JSONException -> 0x00e1 }
            r8 = r0
            r15 = r7
            r16 = r6
            r8.<init>(r9, r10, r11, r12, r14, r15, r16, r17, r18, r19)     // Catch:{ JSONException -> 0x00e1 }
        L_0x00d2:
            r0.deliveryState = r2     // Catch:{ JSONException -> 0x00e1 }
            r0.isRedacted = r5     // Catch:{ JSONException -> 0x00e1 }
            r1.add(r0)     // Catch:{ JSONException -> 0x00e1 }
            r3 = r21
            r0 = r23
            r2 = 0
            goto L_0x0009
        L_0x00e0:
            return r1
        L_0x00e1:
            r0 = move-exception
            com.helpshift.common.exception.ParseException r1 = com.helpshift.common.exception.ParseException.GENERIC
            java.lang.String r2 = "Parsing exception while reading admin attachment message"
            com.helpshift.common.exception.RootAPIException r0 = com.helpshift.common.exception.RootAPIException.wrap(r0, r1, r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.platform.AndroidResponseParser.parseAdminAttachmentEntities(org.json.JSONObject, org.json.JSONArray):java.util.List");
    }

    private RequestAppReviewMessageDM parseRequestAppReviewMessageDM(JSONObject jSONObject) {
        boolean z;
        try {
            JSONObject optJSONObject = jSONObject.getJSONObject("meta").optJSONObject("response");
            boolean optBoolean = optJSONObject != null ? optJSONObject.optBoolean("state") : false;
            if (!jSONObject.optBoolean("invisible")) {
                if (!optBoolean) {
                    z = false;
                    String string = jSONObject.getString("created_at");
                    RequestAppReviewMessageDM requestAppReviewMessageDM = new RequestAppReviewMessageDM(jSONObject.getString("id"), jSONObject.getString("body"), string, HSDateFormatSpec.convertToEpochTime(string), jSONObject.getJSONObject("author").getString("name"), z);
                    requestAppReviewMessageDM.deliveryState = convertDeliveryStateToInt(jSONObject.optString("md_state", ""));
                    requestAppReviewMessageDM.isRedacted = jSONObject.optBoolean("redacted", false);
                    return requestAppReviewMessageDM;
                }
            }
            z = true;
            String string2 = jSONObject.getString("created_at");
            RequestAppReviewMessageDM requestAppReviewMessageDM2 = new RequestAppReviewMessageDM(jSONObject.getString("id"), jSONObject.getString("body"), string2, HSDateFormatSpec.convertToEpochTime(string2), jSONObject.getJSONObject("author").getString("name"), z);
            requestAppReviewMessageDM2.deliveryState = convertDeliveryStateToInt(jSONObject.optString("md_state", ""));
            requestAppReviewMessageDM2.isRedacted = jSONObject.optBoolean("redacted", false);
            return requestAppReviewMessageDM2;
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading request review message");
        }
    }

    private List<MessageDM> parseRequestScreenshotMessageDM(JSONObject jSONObject) {
        try {
            ArrayList arrayList = new ArrayList();
            JSONObject jSONObject2 = jSONObject.getJSONObject("meta");
            JSONObject optJSONObject = jSONObject2.optJSONObject("response");
            JSONArray optJSONArray = jSONObject2.optJSONArray("attachments");
            boolean z = optJSONObject != null ? optJSONObject.getBoolean("state") : false;
            String string = jSONObject.getString("created_at");
            RequestScreenshotMessageDM requestScreenshotMessageDM = new RequestScreenshotMessageDM(jSONObject.getString("id"), jSONObject.getString("body"), string, HSDateFormatSpec.convertToEpochTime(string), jSONObject.getJSONObject("author").getString("name"), z);
            requestScreenshotMessageDM.deliveryState = convertDeliveryStateToInt(jSONObject.optString("md_state", ""));
            requestScreenshotMessageDM.isRedacted = jSONObject.optBoolean("redacted", false);
            arrayList.add(requestScreenshotMessageDM);
            if (optJSONArray != null) {
                arrayList.addAll(parseAdminAttachmentEntities(jSONObject, optJSONArray));
            }
            return arrayList;
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading request screenshot message");
        }
    }

    private ScreenshotMessageDM parseScreenshotMessageDM(JSONObject jSONObject) {
        JSONObject jSONObject2 = jSONObject;
        try {
            boolean z = false;
            JSONObject jSONObject3 = jSONObject2.getJSONObject("meta").getJSONArray("attachments").getJSONObject(0);
            String string = jSONObject2.getString("created_at");
            ScreenshotMessageDM screenshotMessageDM = new ScreenshotMessageDM(jSONObject3.has("body") ? jSONObject3.getString("body") : jSONObject2.getString("body"), string, HSDateFormatSpec.convertToEpochTime(string), jSONObject2.getJSONObject("author").getString("name"), jSONObject3.getString("content-type"), jSONObject3.optString("thumbnail", ""), jSONObject3.getString("file-name"), jSONObject3.getString("url"), jSONObject3.getInt("size"), jSONObject3.optBoolean("secure?", false));
            screenshotMessageDM.serverId = jSONObject2.getString("id");
            screenshotMessageDM.deliveryState = convertDeliveryStateToInt(jSONObject2.optString("md_state", ""));
            if (jSONObject2.optBoolean("redacted", false) || jSONObject3.optBoolean("redacted", false)) {
                z = true;
            }
            screenshotMessageDM.isRedacted = z;
            parseAndSetDataForUserSentMessages(screenshotMessageDM, jSONObject2);
            return screenshotMessageDM;
        } catch (JSONException e) {
            throw RootAPIException.wrap(e, ParseException.GENERIC, "Parsing exception while reading screenshot message");
        }
    }

    private PeriodicReview parsePeriodicReview(JSONObject jSONObject) throws JSONException {
        return new PeriodicReview(jSONObject.optBoolean(AnalyticsEventKey.SEARCH_QUERY), jSONObject.optInt("i"), jSONObject.optString("t", ""));
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0039 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003a A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003b A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003c A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int convertDeliveryStateToInt(java.lang.String r6) {
        /*
            r5 = this;
            int r0 = r6.hashCode()
            r1 = -840272977(0xffffffffcdea73af, float:-4.91681248E8)
            r2 = 2
            r3 = 1
            r4 = 0
            if (r0 == r1) goto L_0x002b
            r1 = 3496342(0x355996, float:4.899419E-39)
            if (r0 == r1) goto L_0x0021
            r1 = 3526552(0x35cf98, float:4.941752E-39)
            if (r0 == r1) goto L_0x0017
            goto L_0x0035
        L_0x0017:
            java.lang.String r0 = "sent"
            boolean r6 = r6.equals(r0)
            if (r6 == 0) goto L_0x0035
            r6 = 2
            goto L_0x0036
        L_0x0021:
            java.lang.String r0 = "read"
            boolean r6 = r6.equals(r0)
            if (r6 == 0) goto L_0x0035
            r6 = 1
            goto L_0x0036
        L_0x002b:
            java.lang.String r0 = "unread"
            boolean r6 = r6.equals(r0)
            if (r6 == 0) goto L_0x0035
            r6 = 0
            goto L_0x0036
        L_0x0035:
            r6 = -1
        L_0x0036:
            switch(r6) {
                case 0: goto L_0x003c;
                case 1: goto L_0x003b;
                case 2: goto L_0x003a;
                default: goto L_0x0039;
            }
        L_0x0039:
            return r4
        L_0x003a:
            return r2
        L_0x003b:
            return r3
        L_0x003c:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.platform.AndroidResponseParser.convertDeliveryStateToInt(java.lang.String):int");
    }

    private void parseAndSetDataForUserSentMessages(MessageDM messageDM, JSONObject jSONObject) throws JSONException {
        messageDM.authorId = jSONObject.getJSONObject("author").optString("id");
        messageDM.createdRequestId = jSONObject.optString(Consts.INAPP_REQUEST_ID);
    }
}
