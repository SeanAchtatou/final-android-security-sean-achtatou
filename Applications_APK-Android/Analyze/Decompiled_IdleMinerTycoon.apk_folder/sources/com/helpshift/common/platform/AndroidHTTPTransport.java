package com.helpshift.common.platform;

import android.os.Build;
import com.helpshift.android.commons.downloader.HelpshiftSSLSocketFactory;
import com.helpshift.common.platform.network.HTTPTransport;
import com.helpshift.common.platform.network.Request;
import com.helpshift.common.platform.network.Response;
import com.helpshift.common.platform.network.UploadRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

public class AndroidHTTPTransport implements HTTPTransport {
    private static final String TAG = "Helpshift_HTTPTrnsport";

    public Response makeRequest(Request request) {
        if (request instanceof UploadRequest) {
            return upload(UploadRequest.class.cast(request));
        }
        return makeNetworkRequest(request);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (3) to help type inference */
    /* JADX WARN: Type inference failed for: r1v17, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r1v32 */
    /* JADX WARN: Type inference failed for: r1v33 */
    /* JADX WARN: Type inference failed for: r1v35 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0244 A[Catch:{ Exception -> 0x0250 }] */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x024c A[Catch:{ Exception -> 0x0250 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00dc A[Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01a5 A[Catch:{ Exception -> 0x01b1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x01ad A[Catch:{ Exception -> 0x01b1 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.helpshift.common.platform.network.Response makeNetworkRequest(com.helpshift.common.platform.network.Request r12) {
        /*
            r11 = this;
            r0 = 0
            java.lang.String r1 = "https://"
            java.lang.String r2 = com.helpshift.common.domain.network.NetworkConstants.scheme     // Catch:{ UnknownHostException -> 0x0227, SecurityException | SocketException -> 0x0217, SSLPeerUnverifiedException -> 0x0207, SSLHandshakeException -> 0x01f7, IOException -> 0x01e7, all -> 0x01e3 }
            boolean r1 = r1.equals(r2)     // Catch:{ UnknownHostException -> 0x0227, SecurityException | SocketException -> 0x0217, SSLPeerUnverifiedException -> 0x0207, SSLHandshakeException -> 0x01f7, IOException -> 0x01e7, all -> 0x01e3 }
            if (r1 == 0) goto L_0x0041
            java.net.URL r1 = new java.net.URL     // Catch:{ UnknownHostException -> 0x0227, SecurityException | SocketException -> 0x0217, SSLPeerUnverifiedException -> 0x0207, SSLHandshakeException -> 0x01f7, IOException -> 0x01e7, all -> 0x01e3 }
            java.lang.String r2 = r12.url     // Catch:{ UnknownHostException -> 0x0227, SecurityException | SocketException -> 0x0217, SSLPeerUnverifiedException -> 0x0207, SSLHandshakeException -> 0x01f7, IOException -> 0x01e7, all -> 0x01e3 }
            r1.<init>(r2)     // Catch:{ UnknownHostException -> 0x0227, SecurityException | SocketException -> 0x0217, SSLPeerUnverifiedException -> 0x0207, SSLHandshakeException -> 0x01f7, IOException -> 0x01e7, all -> 0x01e3 }
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ UnknownHostException -> 0x0227, SecurityException | SocketException -> 0x0217, SSLPeerUnverifiedException -> 0x0207, SSLHandshakeException -> 0x01f7, IOException -> 0x01e7, all -> 0x01e3 }
            javax.net.ssl.HttpsURLConnection r1 = (javax.net.ssl.HttpsURLConnection) r1     // Catch:{ UnknownHostException -> 0x0227, SecurityException | SocketException -> 0x0217, SSLPeerUnverifiedException -> 0x0207, SSLHandshakeException -> 0x01f7, IOException -> 0x01e7, all -> 0x01e3 }
            r2 = r1
            javax.net.ssl.HttpsURLConnection r2 = (javax.net.ssl.HttpsURLConnection) r2     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            r11.fixSSLSocketProtocols(r2)     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            goto L_0x004e
        L_0x001f:
            r12 = move-exception
            r3 = r0
            goto L_0x023a
        L_0x0023:
            r2 = move-exception
            r3 = r0
            r5 = r3
        L_0x0026:
            r0 = r1
            goto L_0x01ea
        L_0x0029:
            r2 = move-exception
            r3 = r0
            r5 = r3
        L_0x002c:
            r0 = r1
            goto L_0x01fa
        L_0x002f:
            r2 = move-exception
            r3 = r0
            r5 = r3
        L_0x0032:
            r0 = r1
            goto L_0x020a
        L_0x0035:
            r2 = move-exception
            r3 = r0
            r5 = r3
        L_0x0038:
            r0 = r1
            goto L_0x021a
        L_0x003b:
            r2 = move-exception
            r3 = r0
            r5 = r3
        L_0x003e:
            r0 = r1
            goto L_0x022a
        L_0x0041:
            java.net.URL r1 = new java.net.URL     // Catch:{ UnknownHostException -> 0x0227, SecurityException | SocketException -> 0x0217, SSLPeerUnverifiedException -> 0x0207, SSLHandshakeException -> 0x01f7, IOException -> 0x01e7, all -> 0x01e3 }
            java.lang.String r2 = r12.url     // Catch:{ UnknownHostException -> 0x0227, SecurityException | SocketException -> 0x0217, SSLPeerUnverifiedException -> 0x0207, SSLHandshakeException -> 0x01f7, IOException -> 0x01e7, all -> 0x01e3 }
            r1.<init>(r2)     // Catch:{ UnknownHostException -> 0x0227, SecurityException | SocketException -> 0x0217, SSLPeerUnverifiedException -> 0x0207, SSLHandshakeException -> 0x01f7, IOException -> 0x01e7, all -> 0x01e3 }
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ UnknownHostException -> 0x0227, SecurityException | SocketException -> 0x0217, SSLPeerUnverifiedException -> 0x0207, SSLHandshakeException -> 0x01f7, IOException -> 0x01e7, all -> 0x01e3 }
            java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1     // Catch:{ UnknownHostException -> 0x0227, SecurityException | SocketException -> 0x0217, SSLPeerUnverifiedException -> 0x0207, SSLHandshakeException -> 0x01f7, IOException -> 0x01e7, all -> 0x01e3 }
        L_0x004e:
            com.helpshift.common.platform.network.Method r2 = r12.method     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            java.lang.String r2 = r2.name()     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            r1.setRequestMethod(r2)     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            int r2 = r12.timeout     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            r1.setConnectTimeout(r2)     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            java.util.List<com.helpshift.common.platform.network.KeyValuePair> r2 = r12.headers     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
        L_0x0062:
            boolean r3 = r2.hasNext()     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            if (r3 == 0) goto L_0x0076
            java.lang.Object r3 = r2.next()     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            com.helpshift.common.platform.network.KeyValuePair r3 = (com.helpshift.common.platform.network.KeyValuePair) r3     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            java.lang.String r4 = r3.key     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            java.lang.String r3 = r3.value     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            r1.setRequestProperty(r4, r3)     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            goto L_0x0062
        L_0x0076:
            com.helpshift.common.platform.network.Method r2 = r12.method     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            com.helpshift.common.platform.network.Method r3 = com.helpshift.common.platform.network.Method.POST     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            if (r2 == r3) goto L_0x0085
            com.helpshift.common.platform.network.Method r2 = r12.method     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            com.helpshift.common.platform.network.Method r3 = com.helpshift.common.platform.network.Method.PUT     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            if (r2 != r3) goto L_0x0083
            goto L_0x0085
        L_0x0083:
            r3 = r0
            goto L_0x00c0
        L_0x0085:
            com.helpshift.common.platform.network.Method r2 = r12.method     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            com.helpshift.common.platform.network.Method r3 = com.helpshift.common.platform.network.Method.PUT     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            if (r2 != r3) goto L_0x0096
            java.lang.Class<com.helpshift.common.platform.network.PUTRequest> r2 = com.helpshift.common.platform.network.PUTRequest.class
            java.lang.Object r2 = r2.cast(r12)     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            com.helpshift.common.platform.network.PUTRequest r2 = (com.helpshift.common.platform.network.PUTRequest) r2     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            java.lang.String r2 = r2.query     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            goto L_0x00a0
        L_0x0096:
            java.lang.Class<com.helpshift.common.platform.network.POSTRequest> r2 = com.helpshift.common.platform.network.POSTRequest.class
            java.lang.Object r2 = r2.cast(r12)     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            com.helpshift.common.platform.network.POSTRequest r2 = (com.helpshift.common.platform.network.POSTRequest) r2     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            java.lang.String r2 = r2.query     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
        L_0x00a0:
            r3 = 1
            r1.setDoOutput(r3)     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            java.io.OutputStream r3 = r1.getOutputStream()     // Catch:{ UnknownHostException -> 0x003b, SecurityException | SocketException -> 0x0035, SSLPeerUnverifiedException -> 0x002f, SSLHandshakeException -> 0x0029, IOException -> 0x0023, all -> 0x001f }
            java.io.BufferedWriter r4 = new java.io.BufferedWriter     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.io.OutputStreamWriter r5 = new java.io.OutputStreamWriter     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.lang.String r6 = "UTF-8"
            r5.<init>(r3, r6)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r4.<init>(r5)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r4.write(r2)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r4.flush()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r4.close()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r3.flush()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
        L_0x00c0:
            int r2 = r1.getResponseCode()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r4.<init>()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.util.Map r5 = r1.getHeaderFields()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.util.Set r6 = r5.keySet()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
        L_0x00d5:
            boolean r7 = r6.hasNext()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r8 = 0
            if (r7 == 0) goto L_0x00fd
            java.lang.Object r7 = r6.next()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            boolean r9 = com.helpshift.common.StringUtils.isEmpty(r7)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            if (r9 != 0) goto L_0x00d5
            com.helpshift.common.platform.network.KeyValuePair r9 = new com.helpshift.common.platform.network.KeyValuePair     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.lang.Object r10 = r5.get(r7)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.util.List r10 = (java.util.List) r10     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.lang.Object r8 = r10.get(r8)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r9.<init>(r7, r8)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r4.add(r9)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            goto L_0x00d5
        L_0x00fd:
            r6 = 200(0xc8, float:2.8E-43)
            if (r2 < r6) goto L_0x015e
            r6 = 300(0x12c, float:4.2E-43)
            if (r2 >= r6) goto L_0x015e
            java.io.BufferedInputStream r6 = new java.io.BufferedInputStream     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.io.InputStream r7 = r1.getInputStream()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r6.<init>(r7)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.lang.String r7 = "Content-Encoding"
            java.lang.Object r5 = r5.get(r7)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.util.List r5 = (java.util.List) r5     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            if (r5 == 0) goto L_0x0132
            int r7 = r5.size()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            if (r7 <= 0) goto L_0x0132
            java.lang.Object r5 = r5.get(r8)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.lang.String r7 = "gzip"
            boolean r5 = r5.equals(r7)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            if (r5 == 0) goto L_0x0132
            java.util.zip.GZIPInputStream r5 = new java.util.zip.GZIPInputStream     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r5.<init>(r6)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            goto L_0x0133
        L_0x0132:
            r5 = r6
        L_0x0133:
            java.lang.String r6 = r11.readInputStream(r5)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r5.close()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            com.helpshift.common.platform.network.Response r5 = new com.helpshift.common.platform.network.Response     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r5.<init>(r2, r6, r4)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            com.helpshift.util.IOUtils.closeQuitely(r0)
            com.helpshift.util.IOUtils.closeQuitely(r3)
            boolean r12 = r1 instanceof javax.net.ssl.HttpsURLConnection     // Catch:{ Exception -> 0x0155 }
            if (r12 == 0) goto L_0x014f
            r12 = r1
            javax.net.ssl.HttpsURLConnection r12 = (javax.net.ssl.HttpsURLConnection) r12     // Catch:{ Exception -> 0x0155 }
            r11.closeHelpshiftSSLSocketFactorySockets(r12)     // Catch:{ Exception -> 0x0155 }
        L_0x014f:
            if (r1 == 0) goto L_0x015d
            r1.disconnect()     // Catch:{ Exception -> 0x0155 }
            goto L_0x015d
        L_0x0155:
            r12 = move-exception
            java.lang.String r0 = "Helpshift_HTTPTrnsport"
            java.lang.String r1 = "Error in finally closing resources"
            com.helpshift.util.HSLogger.e(r0, r1, r12)
        L_0x015d:
            return r5
        L_0x015e:
            java.lang.String r5 = "Helpshift_HTTPTrnsport"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r6.<init>()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.lang.String r7 = "Api : "
            r6.append(r7)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.lang.String r7 = r12.url     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r6.append(r7)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.lang.String r7 = " \t Status : "
            r6.append(r7)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r6.append(r2)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.lang.String r7 = "\t Thread : "
            r6.append(r7)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.lang.Thread r7 = java.lang.Thread.currentThread()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.lang.String r7 = r7.getName()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            r6.append(r7)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.lang.String r6 = r6.toString()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            com.helpshift.util.HSLogger.d(r5, r6)     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.io.InputStream r5 = r1.getErrorStream()     // Catch:{ UnknownHostException -> 0x01df, SecurityException | SocketException -> 0x01db, SSLPeerUnverifiedException -> 0x01d7, SSLHandshakeException -> 0x01d3, IOException -> 0x01cf, all -> 0x01cc }
            java.lang.String r0 = r11.readInputStream(r5)     // Catch:{ UnknownHostException -> 0x01c9, SecurityException | SocketException -> 0x01c6, SSLPeerUnverifiedException -> 0x01c3, SSLHandshakeException -> 0x01c0, IOException -> 0x01bd, all -> 0x01ba }
            com.helpshift.common.platform.network.Response r6 = new com.helpshift.common.platform.network.Response     // Catch:{ UnknownHostException -> 0x01c9, SecurityException | SocketException -> 0x01c6, SSLPeerUnverifiedException -> 0x01c3, SSLHandshakeException -> 0x01c0, IOException -> 0x01bd, all -> 0x01ba }
            r6.<init>(r2, r0, r4)     // Catch:{ UnknownHostException -> 0x01c9, SecurityException | SocketException -> 0x01c6, SSLPeerUnverifiedException -> 0x01c3, SSLHandshakeException -> 0x01c0, IOException -> 0x01bd, all -> 0x01ba }
            com.helpshift.util.IOUtils.closeQuitely(r5)
            com.helpshift.util.IOUtils.closeQuitely(r3)
            boolean r12 = r1 instanceof javax.net.ssl.HttpsURLConnection     // Catch:{ Exception -> 0x01b1 }
            if (r12 == 0) goto L_0x01ab
            r12 = r1
            javax.net.ssl.HttpsURLConnection r12 = (javax.net.ssl.HttpsURLConnection) r12     // Catch:{ Exception -> 0x01b1 }
            r11.closeHelpshiftSSLSocketFactorySockets(r12)     // Catch:{ Exception -> 0x01b1 }
        L_0x01ab:
            if (r1 == 0) goto L_0x01b9
            r1.disconnect()     // Catch:{ Exception -> 0x01b1 }
            goto L_0x01b9
        L_0x01b1:
            r12 = move-exception
            java.lang.String r0 = "Helpshift_HTTPTrnsport"
            java.lang.String r1 = "Error in finally closing resources"
            com.helpshift.util.HSLogger.e(r0, r1, r12)
        L_0x01b9:
            return r6
        L_0x01ba:
            r12 = move-exception
            goto L_0x0239
        L_0x01bd:
            r2 = move-exception
            goto L_0x0026
        L_0x01c0:
            r2 = move-exception
            goto L_0x002c
        L_0x01c3:
            r2 = move-exception
            goto L_0x0032
        L_0x01c6:
            r2 = move-exception
            goto L_0x0038
        L_0x01c9:
            r2 = move-exception
            goto L_0x003e
        L_0x01cc:
            r12 = move-exception
            goto L_0x023a
        L_0x01cf:
            r2 = move-exception
            r5 = r0
            goto L_0x0026
        L_0x01d3:
            r2 = move-exception
            r5 = r0
            goto L_0x002c
        L_0x01d7:
            r2 = move-exception
            r5 = r0
            goto L_0x0032
        L_0x01db:
            r2 = move-exception
            r5 = r0
            goto L_0x0038
        L_0x01df:
            r2 = move-exception
            r5 = r0
            goto L_0x003e
        L_0x01e3:
            r12 = move-exception
            r1 = r0
            r3 = r1
            goto L_0x023a
        L_0x01e7:
            r2 = move-exception
            r3 = r0
            r5 = r3
        L_0x01ea:
            com.helpshift.common.exception.NetworkException r1 = com.helpshift.common.exception.NetworkException.GENERIC     // Catch:{ all -> 0x0237 }
            java.lang.String r12 = r12.url     // Catch:{ all -> 0x0237 }
            r1.route = r12     // Catch:{ all -> 0x0237 }
            java.lang.String r12 = "Network error"
            com.helpshift.common.exception.RootAPIException r12 = com.helpshift.common.exception.RootAPIException.wrap(r2, r1, r12)     // Catch:{ all -> 0x0237 }
            throw r12     // Catch:{ all -> 0x0237 }
        L_0x01f7:
            r2 = move-exception
            r3 = r0
            r5 = r3
        L_0x01fa:
            com.helpshift.common.exception.NetworkException r1 = com.helpshift.common.exception.NetworkException.SSL_HANDSHAKE     // Catch:{ all -> 0x0237 }
            java.lang.String r12 = r12.url     // Catch:{ all -> 0x0237 }
            r1.route = r12     // Catch:{ all -> 0x0237 }
            java.lang.String r12 = "Network error"
            com.helpshift.common.exception.RootAPIException r12 = com.helpshift.common.exception.RootAPIException.wrap(r2, r1, r12)     // Catch:{ all -> 0x0237 }
            throw r12     // Catch:{ all -> 0x0237 }
        L_0x0207:
            r2 = move-exception
            r3 = r0
            r5 = r3
        L_0x020a:
            com.helpshift.common.exception.NetworkException r1 = com.helpshift.common.exception.NetworkException.SSL_PEER_UNVERIFIED     // Catch:{ all -> 0x0237 }
            java.lang.String r12 = r12.url     // Catch:{ all -> 0x0237 }
            r1.route = r12     // Catch:{ all -> 0x0237 }
            java.lang.String r12 = "Network error"
            com.helpshift.common.exception.RootAPIException r12 = com.helpshift.common.exception.RootAPIException.wrap(r2, r1, r12)     // Catch:{ all -> 0x0237 }
            throw r12     // Catch:{ all -> 0x0237 }
        L_0x0217:
            r2 = move-exception
            r3 = r0
            r5 = r3
        L_0x021a:
            com.helpshift.common.exception.NetworkException r1 = com.helpshift.common.exception.NetworkException.NO_CONNECTION     // Catch:{ all -> 0x0237 }
            java.lang.String r12 = r12.url     // Catch:{ all -> 0x0237 }
            r1.route = r12     // Catch:{ all -> 0x0237 }
            java.lang.String r12 = "Network error"
            com.helpshift.common.exception.RootAPIException r12 = com.helpshift.common.exception.RootAPIException.wrap(r2, r1, r12)     // Catch:{ all -> 0x0237 }
            throw r12     // Catch:{ all -> 0x0237 }
        L_0x0227:
            r2 = move-exception
            r3 = r0
            r5 = r3
        L_0x022a:
            com.helpshift.common.exception.NetworkException r1 = com.helpshift.common.exception.NetworkException.UNKNOWN_HOST     // Catch:{ all -> 0x0237 }
            java.lang.String r12 = r12.url     // Catch:{ all -> 0x0237 }
            r1.route = r12     // Catch:{ all -> 0x0237 }
            java.lang.String r12 = "Network error"
            com.helpshift.common.exception.RootAPIException r12 = com.helpshift.common.exception.RootAPIException.wrap(r2, r1, r12)     // Catch:{ all -> 0x0237 }
            throw r12     // Catch:{ all -> 0x0237 }
        L_0x0237:
            r12 = move-exception
            r1 = r0
        L_0x0239:
            r0 = r5
        L_0x023a:
            com.helpshift.util.IOUtils.closeQuitely(r0)
            com.helpshift.util.IOUtils.closeQuitely(r3)
            boolean r0 = r1 instanceof javax.net.ssl.HttpsURLConnection     // Catch:{ Exception -> 0x0250 }
            if (r0 == 0) goto L_0x024a
            r0 = r1
            javax.net.ssl.HttpsURLConnection r0 = (javax.net.ssl.HttpsURLConnection) r0     // Catch:{ Exception -> 0x0250 }
            r11.closeHelpshiftSSLSocketFactorySockets(r0)     // Catch:{ Exception -> 0x0250 }
        L_0x024a:
            if (r1 == 0) goto L_0x0258
            r1.disconnect()     // Catch:{ Exception -> 0x0250 }
            goto L_0x0258
        L_0x0250:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_HTTPTrnsport"
            java.lang.String r2 = "Error in finally closing resources"
            com.helpshift.util.HSLogger.e(r1, r2, r0)
        L_0x0258:
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.platform.AndroidHTTPTransport.makeNetworkRequest(com.helpshift.common.platform.network.Request):com.helpshift.common.platform.network.Response");
    }

    private String readInputStream(InputStream inputStream) throws IOException {
        if (inputStream == null) {
            return null;
        }
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        StringBuilder sb = new StringBuilder();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                sb.append(readLine);
            } else {
                inputStreamReader.close();
                return sb.toString();
            }
        }
    }

    private void fixSSLSocketProtocols(HttpsURLConnection httpsURLConnection) {
        if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT <= 19) {
            ArrayList arrayList = new ArrayList();
            arrayList.add("TLSv1.2");
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add("SSLv3");
            httpsURLConnection.setSSLSocketFactory(new HelpshiftSSLSocketFactory(httpsURLConnection.getSSLSocketFactory(), arrayList, arrayList2));
        }
    }

    private void closeHelpshiftSSLSocketFactorySockets(HttpsURLConnection httpsURLConnection) {
        if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT <= 19 && httpsURLConnection != null) {
            SSLSocketFactory sSLSocketFactory = httpsURLConnection.getSSLSocketFactory();
            if (sSLSocketFactory instanceof HelpshiftSSLSocketFactory) {
                ((HelpshiftSSLSocketFactory) sSLSocketFactory).closeSockets();
            }
        }
    }

    private boolean isInvalidKeyForHeader(String str) {
        return "screenshot".equals(str) || "originalFileName".equals(str);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (4) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Type inference failed for: r6v0, types: [java.io.Closeable] */
    /* JADX WARN: Type inference failed for: r1v0, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r6v1 */
    /* JADX WARN: Type inference failed for: r6v7 */
    /* JADX WARN: Type inference failed for: r6v8 */
    /* JADX WARN: Type inference failed for: r6v9 */
    /* JADX WARN: Type inference failed for: r6v17, types: [java.io.DataOutputStream, java.io.Closeable] */
    /* JADX WARN: Type inference failed for: r6v22 */
    /* JADX WARN: Type inference failed for: r6v23 */
    /* JADX WARN: Type inference failed for: r1v30 */
    /* JADX WARN: Type inference failed for: r1v31 */
    /* JADX WARN: Type inference failed for: r1v32 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x0310 A[SYNTHETIC, Splitter:B:140:0x0310] */
    private com.helpshift.common.platform.network.Response upload(com.helpshift.common.platform.network.UploadRequest r14) {
        /*
            r13 = this;
            r0 = 0
            java.net.URL r1 = new java.net.URL     // Catch:{ UnknownHostException -> 0x02f1, SecurityException | SocketException -> 0x02e0, SSLPeerUnverifiedException -> 0x02cf, SSLHandshakeException -> 0x02be, Exception -> 0x02ad, all -> 0x02a8 }
            java.lang.String r2 = r14.url     // Catch:{ UnknownHostException -> 0x02f1, SecurityException | SocketException -> 0x02e0, SSLPeerUnverifiedException -> 0x02cf, SSLHandshakeException -> 0x02be, Exception -> 0x02ad, all -> 0x02a8 }
            r1.<init>(r2)     // Catch:{ UnknownHostException -> 0x02f1, SecurityException | SocketException -> 0x02e0, SSLPeerUnverifiedException -> 0x02cf, SSLHandshakeException -> 0x02be, Exception -> 0x02ad, all -> 0x02a8 }
            java.lang.String r2 = "--"
            java.lang.String r3 = "*****"
            java.lang.String r4 = "\r\n"
            java.lang.String r5 = "https://"
            java.lang.String r6 = com.helpshift.common.domain.network.NetworkConstants.scheme     // Catch:{ UnknownHostException -> 0x02f1, SecurityException | SocketException -> 0x02e0, SSLPeerUnverifiedException -> 0x02cf, SSLHandshakeException -> 0x02be, Exception -> 0x02ad, all -> 0x02a8 }
            boolean r5 = r5.equals(r6)     // Catch:{ UnknownHostException -> 0x02f1, SecurityException | SocketException -> 0x02e0, SSLPeerUnverifiedException -> 0x02cf, SSLHandshakeException -> 0x02be, Exception -> 0x02ad, all -> 0x02a8 }
            if (r5 == 0) goto L_0x004c
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ UnknownHostException -> 0x02f1, SecurityException | SocketException -> 0x02e0, SSLPeerUnverifiedException -> 0x02cf, SSLHandshakeException -> 0x02be, Exception -> 0x02ad, all -> 0x02a8 }
            javax.net.ssl.HttpsURLConnection r1 = (javax.net.ssl.HttpsURLConnection) r1     // Catch:{ UnknownHostException -> 0x02f1, SecurityException | SocketException -> 0x02e0, SSLPeerUnverifiedException -> 0x02cf, SSLHandshakeException -> 0x02be, Exception -> 0x02ad, all -> 0x02a8 }
            r5 = r1
            javax.net.ssl.HttpsURLConnection r5 = (javax.net.ssl.HttpsURLConnection) r5     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            r13.fixSSLSocketProtocols(r5)     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            goto L_0x0052
        L_0x0025:
            r14 = move-exception
            r3 = r0
            goto L_0x02ab
        L_0x0029:
            r2 = move-exception
            r3 = r0
            r6 = r3
            r9 = r6
        L_0x002d:
            r0 = r1
            goto L_0x02b1
        L_0x0030:
            r2 = move-exception
            r3 = r0
            r6 = r3
            r9 = r6
        L_0x0034:
            r0 = r1
            goto L_0x02c2
        L_0x0037:
            r2 = move-exception
            r3 = r0
            r6 = r3
            r9 = r6
        L_0x003b:
            r0 = r1
            goto L_0x02d3
        L_0x003e:
            r2 = move-exception
            r3 = r0
            r6 = r3
            r9 = r6
        L_0x0042:
            r0 = r1
            goto L_0x02e4
        L_0x0045:
            r2 = move-exception
            r3 = r0
            r6 = r3
            r9 = r6
        L_0x0049:
            r0 = r1
            goto L_0x02f5
        L_0x004c:
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ UnknownHostException -> 0x02f1, SecurityException | SocketException -> 0x02e0, SSLPeerUnverifiedException -> 0x02cf, SSLHandshakeException -> 0x02be, Exception -> 0x02ad, all -> 0x02a8 }
            java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1     // Catch:{ UnknownHostException -> 0x02f1, SecurityException | SocketException -> 0x02e0, SSLPeerUnverifiedException -> 0x02cf, SSLHandshakeException -> 0x02be, Exception -> 0x02ad, all -> 0x02a8 }
        L_0x0052:
            r5 = 1
            r1.setDoInput(r5)     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            r1.setDoOutput(r5)     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            r5 = 0
            r1.setUseCaches(r5)     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            com.helpshift.common.platform.network.Method r6 = r14.method     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            java.lang.String r6 = r6.name()     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            r1.setRequestMethod(r6)     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            int r6 = r14.timeout     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            r1.setConnectTimeout(r6)     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            int r6 = r14.timeout     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            r1.setReadTimeout(r6)     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            java.util.List r6 = r14.headers     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
        L_0x0076:
            boolean r7 = r6.hasNext()     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            if (r7 == 0) goto L_0x008a
            java.lang.Object r7 = r6.next()     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            com.helpshift.common.platform.network.KeyValuePair r7 = (com.helpshift.common.platform.network.KeyValuePair) r7     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            java.lang.String r8 = r7.key     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            java.lang.String r7 = r7.value     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            r1.setRequestProperty(r8, r7)     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            goto L_0x0076
        L_0x008a:
            java.io.DataOutputStream r6 = new java.io.DataOutputStream     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            java.io.OutputStream r7 = r1.getOutputStream()     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            r6.<init>(r7)     // Catch:{ UnknownHostException -> 0x0045, SecurityException | SocketException -> 0x003e, SSLPeerUnverifiedException -> 0x0037, SSLHandshakeException -> 0x0030, Exception -> 0x0029, all -> 0x0025 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r7.<init>()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r7.append(r2)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r7.append(r3)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r7.append(r4)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.String r7 = r7.toString()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r6.writeBytes(r7)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.util.Map<java.lang.String, java.lang.String> r7 = r14.data     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.util.Set r8 = r7.entrySet()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.util.Iterator r8 = r8.iterator()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
        L_0x00b2:
            boolean r9 = r8.hasNext()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            if (r9 == 0) goto L_0x0147
            java.lang.Object r9 = r8.next()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.util.Map$Entry r9 = (java.util.Map.Entry) r9     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.Object r10 = r9.getKey()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            boolean r11 = r13.isInvalidKeyForHeader(r10)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            if (r11 != 0) goto L_0x00b2
            java.lang.Object r9 = r9.getValue()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r11.<init>()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.String r12 = "Content-Disposition: form-data; name=\""
            r11.append(r12)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r11.append(r10)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.String r10 = "\"; "
            r11.append(r10)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r11.append(r4)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.String r10 = r11.toString()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r6.writeBytes(r10)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r10.<init>()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.String r11 = "Content-Type: text/plain;charset=UTF-8"
            r10.append(r11)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r10.append(r4)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.String r10 = r10.toString()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r6.writeBytes(r10)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r10.<init>()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.String r11 = "Content-Length: "
            r10.append(r11)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            int r11 = r9.length()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r10.append(r11)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r10.append(r4)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.String r10 = r10.toString()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r6.writeBytes(r10)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r6.writeBytes(r4)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r10.<init>()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r10.append(r9)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r10.append(r4)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.String r9 = r10.toString()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r6.writeBytes(r9)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r9.<init>()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r9.append(r2)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r9.append(r3)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r9.append(r4)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.String r9 = r9.toString()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r6.writeBytes(r9)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            goto L_0x00b2
        L_0x0147:
            java.io.File r8 = new java.io.File     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.String r9 = "screenshot"
            java.lang.Object r9 = r7.get(r9)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r8.<init>(r9)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.String r9 = "originalFileName"
            java.lang.Object r7 = r7.get(r9)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            if (r7 != 0) goto L_0x0162
            java.lang.String r7 = r8.getName()     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
        L_0x0162:
            java.io.FileInputStream r9 = new java.io.FileInputStream     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            r9.<init>(r8)     // Catch:{ UnknownHostException -> 0x02a3, SecurityException | SocketException -> 0x029e, SSLPeerUnverifiedException -> 0x0299, SSLHandshakeException -> 0x0294, Exception -> 0x028f, all -> 0x028b }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r10.<init>()     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r10.append(r2)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r10.append(r3)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r10.append(r4)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            java.lang.String r10 = r10.toString()     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r6.writeBytes(r10)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r10.<init>()     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            java.lang.String r11 = "Content-Disposition: form-data; name=\"screenshot\"; filename=\""
            r10.append(r11)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r10.append(r7)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            java.lang.String r7 = "\""
            r10.append(r7)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r10.append(r4)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            java.lang.String r7 = r10.toString()     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r6.writeBytes(r7)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r7.<init>()     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            java.lang.String r10 = "Content-Type: "
            r7.append(r10)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            java.lang.String r10 = r14.mimeType     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r7.append(r10)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r7.append(r4)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            java.lang.String r7 = r7.toString()     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r6.writeBytes(r7)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r7.<init>()     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            java.lang.String r10 = "Content-Length: "
            r7.append(r10)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            long r10 = r8.length()     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r7.append(r10)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r7.append(r4)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            java.lang.String r7 = r7.toString()     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r6.writeBytes(r7)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r6.writeBytes(r4)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r7 = 1048576(0x100000, float:1.469368E-39)
            int r8 = r9.available()     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            int r8 = java.lang.Math.min(r8, r7)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            byte[] r10 = new byte[r8]     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            int r11 = r9.read(r10, r5, r8)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
        L_0x01df:
            if (r11 <= 0) goto L_0x01f1
            r6.write(r10, r5, r8)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            int r8 = r9.available()     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            int r8 = java.lang.Math.min(r8, r7)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            int r11 = r9.read(r10, r5, r8)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            goto L_0x01df
        L_0x01f1:
            r6.writeBytes(r4)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r5.<init>()     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r5.append(r2)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r5.append(r3)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r5.append(r2)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r5.append(r4)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            java.lang.String r2 = r5.toString()     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r6.writeBytes(r2)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r6.flush()     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            int r2 = r1.getResponseCode()     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 < r3) goto L_0x0256
            r3 = 300(0x12c, float:4.2E-43)
            if (r2 >= r3) goto L_0x0256
            java.io.InputStream r3 = r1.getInputStream()     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            if (r3 == 0) goto L_0x0238
            java.lang.String r4 = r13.readInputStream(r3)     // Catch:{ UnknownHostException -> 0x0235, SecurityException | SocketException -> 0x0232, SSLPeerUnverifiedException -> 0x022f, SSLHandshakeException -> 0x022c, Exception -> 0x0229, all -> 0x0226 }
            goto L_0x0239
        L_0x0226:
            r14 = move-exception
            goto L_0x0304
        L_0x0229:
            r2 = move-exception
            goto L_0x002d
        L_0x022c:
            r2 = move-exception
            goto L_0x0034
        L_0x022f:
            r2 = move-exception
            goto L_0x003b
        L_0x0232:
            r2 = move-exception
            goto L_0x0042
        L_0x0235:
            r2 = move-exception
            goto L_0x0049
        L_0x0238:
            r4 = r0
        L_0x0239:
            com.helpshift.common.platform.network.Response r5 = new com.helpshift.common.platform.network.Response     // Catch:{ UnknownHostException -> 0x0235, SecurityException | SocketException -> 0x0232, SSLPeerUnverifiedException -> 0x022f, SSLHandshakeException -> 0x022c, Exception -> 0x0229, all -> 0x0226 }
            r5.<init>(r2, r4, r0)     // Catch:{ UnknownHostException -> 0x0235, SecurityException | SocketException -> 0x0232, SSLPeerUnverifiedException -> 0x022f, SSLHandshakeException -> 0x022c, Exception -> 0x0229, all -> 0x0226 }
            com.helpshift.util.IOUtils.closeQuitely(r9)
            com.helpshift.util.IOUtils.closeQuitely(r6)
            com.helpshift.util.IOUtils.closeQuitely(r3)
            if (r1 == 0) goto L_0x0255
            r1.disconnect()     // Catch:{ Exception -> 0x024d }
            goto L_0x0255
        L_0x024d:
            r14 = move-exception
            java.lang.String r0 = "Helpshift_HTTPTrnsport"
            java.lang.String r1 = "Error in finally closing resources"
            com.helpshift.util.HSLogger.e(r0, r1, r14)
        L_0x0255:
            return r5
        L_0x0256:
            com.helpshift.common.platform.network.Response r3 = new com.helpshift.common.platform.network.Response     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            r3.<init>(r2, r0, r0)     // Catch:{ UnknownHostException -> 0x0287, SecurityException | SocketException -> 0x0283, SSLPeerUnverifiedException -> 0x027f, SSLHandshakeException -> 0x027b, Exception -> 0x0277, all -> 0x0273 }
            com.helpshift.util.IOUtils.closeQuitely(r9)
            com.helpshift.util.IOUtils.closeQuitely(r6)
            com.helpshift.util.IOUtils.closeQuitely(r0)
            if (r1 == 0) goto L_0x0272
            r1.disconnect()     // Catch:{ Exception -> 0x026a }
            goto L_0x0272
        L_0x026a:
            r14 = move-exception
            java.lang.String r0 = "Helpshift_HTTPTrnsport"
            java.lang.String r1 = "Error in finally closing resources"
            com.helpshift.util.HSLogger.e(r0, r1, r14)
        L_0x0272:
            return r3
        L_0x0273:
            r14 = move-exception
            r3 = r0
            goto L_0x0304
        L_0x0277:
            r2 = move-exception
            r3 = r0
            goto L_0x002d
        L_0x027b:
            r2 = move-exception
            r3 = r0
            goto L_0x0034
        L_0x027f:
            r2 = move-exception
            r3 = r0
            goto L_0x003b
        L_0x0283:
            r2 = move-exception
            r3 = r0
            goto L_0x0042
        L_0x0287:
            r2 = move-exception
            r3 = r0
            goto L_0x0049
        L_0x028b:
            r14 = move-exception
            r3 = r0
            goto L_0x0305
        L_0x028f:
            r2 = move-exception
            r3 = r0
            r9 = r3
            goto L_0x002d
        L_0x0294:
            r2 = move-exception
            r3 = r0
            r9 = r3
            goto L_0x0034
        L_0x0299:
            r2 = move-exception
            r3 = r0
            r9 = r3
            goto L_0x003b
        L_0x029e:
            r2 = move-exception
            r3 = r0
            r9 = r3
            goto L_0x0042
        L_0x02a3:
            r2 = move-exception
            r3 = r0
            r9 = r3
            goto L_0x0049
        L_0x02a8:
            r14 = move-exception
            r1 = r0
            r3 = r1
        L_0x02ab:
            r6 = r3
            goto L_0x0305
        L_0x02ad:
            r2 = move-exception
            r3 = r0
            r6 = r3
            r9 = r6
        L_0x02b1:
            com.helpshift.common.exception.NetworkException r1 = com.helpshift.common.exception.NetworkException.GENERIC     // Catch:{ all -> 0x0302 }
            java.lang.String r14 = r14.url     // Catch:{ all -> 0x0302 }
            r1.route = r14     // Catch:{ all -> 0x0302 }
            java.lang.String r14 = "Upload error"
            com.helpshift.common.exception.RootAPIException r14 = com.helpshift.common.exception.RootAPIException.wrap(r2, r1, r14)     // Catch:{ all -> 0x0302 }
            throw r14     // Catch:{ all -> 0x0302 }
        L_0x02be:
            r2 = move-exception
            r3 = r0
            r6 = r3
            r9 = r6
        L_0x02c2:
            com.helpshift.common.exception.NetworkException r1 = com.helpshift.common.exception.NetworkException.SSL_HANDSHAKE     // Catch:{ all -> 0x0302 }
            java.lang.String r14 = r14.url     // Catch:{ all -> 0x0302 }
            r1.route = r14     // Catch:{ all -> 0x0302 }
            java.lang.String r14 = "Upload error"
            com.helpshift.common.exception.RootAPIException r14 = com.helpshift.common.exception.RootAPIException.wrap(r2, r1, r14)     // Catch:{ all -> 0x0302 }
            throw r14     // Catch:{ all -> 0x0302 }
        L_0x02cf:
            r2 = move-exception
            r3 = r0
            r6 = r3
            r9 = r6
        L_0x02d3:
            com.helpshift.common.exception.NetworkException r1 = com.helpshift.common.exception.NetworkException.SSL_PEER_UNVERIFIED     // Catch:{ all -> 0x0302 }
            java.lang.String r14 = r14.url     // Catch:{ all -> 0x0302 }
            r1.route = r14     // Catch:{ all -> 0x0302 }
            java.lang.String r14 = "Upload error"
            com.helpshift.common.exception.RootAPIException r14 = com.helpshift.common.exception.RootAPIException.wrap(r2, r1, r14)     // Catch:{ all -> 0x0302 }
            throw r14     // Catch:{ all -> 0x0302 }
        L_0x02e0:
            r2 = move-exception
            r3 = r0
            r6 = r3
            r9 = r6
        L_0x02e4:
            com.helpshift.common.exception.NetworkException r1 = com.helpshift.common.exception.NetworkException.NO_CONNECTION     // Catch:{ all -> 0x0302 }
            java.lang.String r14 = r14.url     // Catch:{ all -> 0x0302 }
            r1.route = r14     // Catch:{ all -> 0x0302 }
            java.lang.String r14 = "Upload error"
            com.helpshift.common.exception.RootAPIException r14 = com.helpshift.common.exception.RootAPIException.wrap(r2, r1, r14)     // Catch:{ all -> 0x0302 }
            throw r14     // Catch:{ all -> 0x0302 }
        L_0x02f1:
            r2 = move-exception
            r3 = r0
            r6 = r3
            r9 = r6
        L_0x02f5:
            com.helpshift.common.exception.NetworkException r1 = com.helpshift.common.exception.NetworkException.UNKNOWN_HOST     // Catch:{ all -> 0x0302 }
            java.lang.String r14 = r14.url     // Catch:{ all -> 0x0302 }
            r1.route = r14     // Catch:{ all -> 0x0302 }
            java.lang.String r14 = "Upload error"
            com.helpshift.common.exception.RootAPIException r14 = com.helpshift.common.exception.RootAPIException.wrap(r2, r1, r14)     // Catch:{ all -> 0x0302 }
            throw r14     // Catch:{ all -> 0x0302 }
        L_0x0302:
            r14 = move-exception
            r1 = r0
        L_0x0304:
            r0 = r9
        L_0x0305:
            com.helpshift.util.IOUtils.closeQuitely(r0)
            com.helpshift.util.IOUtils.closeQuitely(r6)
            com.helpshift.util.IOUtils.closeQuitely(r3)
            if (r1 == 0) goto L_0x031c
            r1.disconnect()     // Catch:{ Exception -> 0x0314 }
            goto L_0x031c
        L_0x0314:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_HTTPTrnsport"
            java.lang.String r2 = "Error in finally closing resources"
            com.helpshift.util.HSLogger.e(r1, r2, r0)
        L_0x031c:
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.platform.AndroidHTTPTransport.upload(com.helpshift.common.platform.network.UploadRequest):com.helpshift.common.platform.network.Response");
    }
}
