package com.helpshift.common.platform;

import android.content.Context;
import com.helpshift.common.StringUtils;
import com.helpshift.common.conversation.ConversationDB;
import com.helpshift.conversation.dao.ConversationInboxDAO;
import com.helpshift.conversation.dao.PushNotificationData;
import com.helpshift.conversation.dto.ConversationDetailDTO;
import com.helpshift.conversation.dto.ImagePickerFile;
import com.helpshift.conversation.dto.dao.ConversationInboxRecord;
import org.json.JSONException;
import org.json.JSONObject;

public class AndroidConversationInboxDAO implements ConversationInboxDAO {
    private static final String KEY_NOTIFICATION_COUNT = "notification_count";
    private static final String KEY_NOTIFICATION_TITLE = "notification_title";
    private static final String KEY_PUSH_NOTIFICATION_DATA = "push_notification_data";
    private ConversationDB conversationDB;
    private KVStore kvStore;

    public AndroidConversationInboxDAO(Context context, KVStore kVStore) {
        this.conversationDB = ConversationDB.getInstance(context);
        this.kvStore = kVStore;
    }

    private synchronized ConversationInboxRecord.Builder getConversationInboxRecordBuilder(long j) {
        ConversationInboxRecord.Builder builder;
        ConversationInboxRecord readConversationInboxRecord = this.conversationDB.readConversationInboxRecord(j);
        if (readConversationInboxRecord == null) {
            builder = new ConversationInboxRecord.Builder(j);
        } else {
            builder = new ConversationInboxRecord.Builder(readConversationInboxRecord);
        }
        return builder;
    }

    public synchronized void saveDescriptionDetail(long j, ConversationDetailDTO conversationDetailDTO) {
        ConversationInboxRecord.Builder conversationInboxRecordBuilder = getConversationInboxRecordBuilder(j);
        conversationInboxRecordBuilder.setDescription(conversationDetailDTO.title);
        conversationInboxRecordBuilder.setDescriptionTimeStamp(conversationDetailDTO.timestamp);
        conversationInboxRecordBuilder.setDescriptionType(conversationDetailDTO.type);
        this.conversationDB.storeConversationInboxRecord(conversationInboxRecordBuilder.build());
    }

    public synchronized ConversationDetailDTO getDescriptionDetail(long j) {
        ConversationDetailDTO conversationDetailDTO;
        ConversationInboxRecord readConversationInboxRecord = this.conversationDB.readConversationInboxRecord(j);
        conversationDetailDTO = null;
        if (readConversationInboxRecord != null) {
            String str = readConversationInboxRecord.description;
            long j2 = readConversationInboxRecord.descriptionTimeStamp;
            int i = readConversationInboxRecord.descriptionType;
            if (!StringUtils.isEmpty(str)) {
                conversationDetailDTO = new ConversationDetailDTO(str, j2, i);
            }
        }
        return conversationDetailDTO;
    }

    public synchronized void saveName(long j, String str) {
        ConversationInboxRecord.Builder conversationInboxRecordBuilder = getConversationInboxRecordBuilder(j);
        conversationInboxRecordBuilder.setFormName(str);
        this.conversationDB.storeConversationInboxRecord(conversationInboxRecordBuilder.build());
    }

    public synchronized String getName(long j) {
        String str;
        ConversationInboxRecord readConversationInboxRecord = this.conversationDB.readConversationInboxRecord(j);
        str = null;
        if (readConversationInboxRecord != null) {
            str = readConversationInboxRecord.formName;
        }
        return str;
    }

    public synchronized void saveEmail(long j, String str) {
        ConversationInboxRecord.Builder conversationInboxRecordBuilder = getConversationInboxRecordBuilder(j);
        conversationInboxRecordBuilder.setFormEmail(str);
        this.conversationDB.storeConversationInboxRecord(conversationInboxRecordBuilder.build());
    }

    public synchronized String getEmail(long j) {
        String str;
        ConversationInboxRecord readConversationInboxRecord = this.conversationDB.readConversationInboxRecord(j);
        str = null;
        if (readConversationInboxRecord != null) {
            str = readConversationInboxRecord.formEmail;
        }
        return str;
    }

    public synchronized void saveImageAttachment(long j, ImagePickerFile imagePickerFile) {
        ConversationInboxRecord.Builder conversationInboxRecordBuilder = getConversationInboxRecordBuilder(j);
        conversationInboxRecordBuilder.setImageAttachmentDraft(imagePickerFile);
        this.conversationDB.storeConversationInboxRecord(conversationInboxRecordBuilder.build());
    }

    public synchronized ImagePickerFile getImageAttachment(long j) {
        ImagePickerFile imagePickerFile;
        ConversationInboxRecord readConversationInboxRecord = this.conversationDB.readConversationInboxRecord(j);
        imagePickerFile = null;
        if (readConversationInboxRecord != null) {
            imagePickerFile = readConversationInboxRecord.imageAttachmentDraft;
        }
        return imagePickerFile;
    }

    public synchronized void saveConversationInboxTimestamp(long j, String str) {
        ConversationInboxRecord.Builder conversationInboxRecordBuilder = getConversationInboxRecordBuilder(j);
        conversationInboxRecordBuilder.setLastSyncTimestamp(str);
        this.conversationDB.storeConversationInboxRecord(conversationInboxRecordBuilder.build());
    }

    public synchronized String getConversationInboxTimestamp(long j) {
        String str;
        ConversationInboxRecord readConversationInboxRecord = this.conversationDB.readConversationInboxRecord(j);
        str = null;
        if (readConversationInboxRecord != null) {
            str = readConversationInboxRecord.lastSyncTimestamp;
        }
        return str;
    }

    public synchronized void saveConversationArchivalPrefillText(long j, String str) {
        ConversationInboxRecord.Builder conversationInboxRecordBuilder = getConversationInboxRecordBuilder(j);
        conversationInboxRecordBuilder.setArchivalText(str);
        this.conversationDB.storeConversationInboxRecord(conversationInboxRecordBuilder.build());
    }

    public synchronized String getConversationArchivalPrefillText(long j) {
        String str;
        ConversationInboxRecord readConversationInboxRecord = this.conversationDB.readConversationInboxRecord(j);
        str = null;
        if (readConversationInboxRecord != null) {
            str = readConversationInboxRecord.archivalText;
        }
        return str;
    }

    public synchronized void saveUserReplyDraft(long j, String str) {
        if (str == null) {
            str = "";
        }
        ConversationInboxRecord.Builder conversationInboxRecordBuilder = getConversationInboxRecordBuilder(j);
        conversationInboxRecordBuilder.setReplyText(str);
        this.conversationDB.storeConversationInboxRecord(conversationInboxRecordBuilder.build());
    }

    public synchronized String getUserReplyDraft(long j) {
        String str;
        ConversationInboxRecord readConversationInboxRecord = this.conversationDB.readConversationInboxRecord(j);
        str = "";
        if (readConversationInboxRecord != null) {
            str = readConversationInboxRecord.replyText;
        }
        return str;
    }

    public synchronized boolean getPersistMessageBox(long j) {
        boolean z;
        ConversationInboxRecord readConversationInboxRecord = this.conversationDB.readConversationInboxRecord(j);
        z = false;
        if (readConversationInboxRecord != null) {
            z = readConversationInboxRecord.persistMessageBox;
        }
        return z;
    }

    public synchronized void savePersistMessageBox(long j, boolean z) {
        ConversationInboxRecord.Builder conversationInboxRecordBuilder = getConversationInboxRecordBuilder(j);
        conversationInboxRecordBuilder.setPersistMessageBox(z);
        this.conversationDB.storeConversationInboxRecord(conversationInboxRecordBuilder.build());
    }

    public PushNotificationData getPushNotificationData(String str) {
        String string = this.kvStore.getString(KEY_PUSH_NOTIFICATION_DATA);
        if (StringUtils.isEmpty(string)) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject(string);
            if (!jSONObject.has(str)) {
                return null;
            }
            JSONObject jSONObject2 = jSONObject.getJSONObject(str);
            return new PushNotificationData(jSONObject2.getInt(KEY_NOTIFICATION_COUNT), jSONObject2.getString(KEY_NOTIFICATION_TITLE));
        } catch (JSONException unused) {
            return null;
        }
    }

    public void setPushNotificationData(String str, PushNotificationData pushNotificationData) {
        String string = this.kvStore.getString(KEY_PUSH_NOTIFICATION_DATA);
        if (StringUtils.isEmpty(string)) {
            string = "{}";
        }
        try {
            JSONObject jSONObject = new JSONObject(string);
            if (pushNotificationData == null) {
                jSONObject.remove(str);
            } else {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put(KEY_NOTIFICATION_COUNT, pushNotificationData.count);
                jSONObject2.put(KEY_NOTIFICATION_TITLE, pushNotificationData.title);
                jSONObject.put(str, jSONObject2);
            }
            this.kvStore.setString(KEY_PUSH_NOTIFICATION_DATA, jSONObject.toString());
        } catch (JSONException unused) {
        }
    }

    public void deleteUserData(long j) {
        if (j > 0) {
            this.conversationDB.deleteConversationInboxData(j);
        }
    }

    public void saveHasOlderMessages(long j, boolean z) {
        ConversationInboxRecord.Builder conversationInboxRecordBuilder = getConversationInboxRecordBuilder(j);
        conversationInboxRecordBuilder.setHasOlderMessages(z);
        this.conversationDB.storeConversationInboxRecord(conversationInboxRecordBuilder.build());
    }

    public boolean getHasOlderMessages(long j) {
        ConversationInboxRecord readConversationInboxRecord = this.conversationDB.readConversationInboxRecord(j);
        if (readConversationInboxRecord == null || readConversationInboxRecord.hasOlderMessages == null) {
            return true;
        }
        return readConversationInboxRecord.hasOlderMessages.booleanValue();
    }

    public void saveLastConversationsRedactionTime(long j, long j2) {
        ConversationInboxRecord.Builder conversationInboxRecordBuilder = getConversationInboxRecordBuilder(j);
        conversationInboxRecordBuilder.setLastConversationsRedactionTime(Long.valueOf(j2));
        this.conversationDB.storeConversationInboxRecord(conversationInboxRecordBuilder.build());
    }

    public Long getLastConversationsRedactionTime(long j) {
        ConversationInboxRecord readConversationInboxRecord = this.conversationDB.readConversationInboxRecord(j);
        if (readConversationInboxRecord != null) {
            return readConversationInboxRecord.lastConversationsRedactionTime;
        }
        return null;
    }

    public void resetDataAfterConversationsDeletion(long j) {
        ConversationInboxRecord.Builder conversationInboxRecordBuilder = getConversationInboxRecordBuilder(j);
        conversationInboxRecordBuilder.setHasOlderMessages(true);
        conversationInboxRecordBuilder.setLastSyncTimestamp(null);
        this.conversationDB.storeConversationInboxRecord(conversationInboxRecordBuilder.build());
    }
}
