package com.helpshift.common.platform;

import android.content.Context;
import com.helpshift.common.conversation.ConversationDB;
import com.helpshift.conversation.activeconversation.ConversationUpdate;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.message.MessageType;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.dao.ConversationDAO;
import com.helpshift.conversation.dao.FAQSuggestionsDAO;
import com.helpshift.support.Faq;
import com.helpshift.util.HSLogger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

class AndroidConversationDAO implements ConversationDAO, FAQSuggestionsDAO {
    private final String TAG = "Helpshift_CnDAO";
    private final ConversationDB conversationDB;

    AndroidConversationDAO(Context context) {
        this.conversationDB = ConversationDB.getInstance(context);
    }

    public void dropAndCreateDatabase() {
        this.conversationDB.dropAndCreateDatabase();
    }

    public synchronized Conversation readConversationWithoutMessages(String str) {
        return this.conversationDB.readConversationWithServerId(str);
    }

    public synchronized Conversation readPreConversationWithoutMessages(String str) {
        return this.conversationDB.readPreConversationWithServerId(str);
    }

    public Conversation readConversationWithoutMessages(Long l) {
        return this.conversationDB.readConversationWithLocalId(l);
    }

    public synchronized Conversation readConversation(long j) {
        Conversation readConversationWithLocalId = this.conversationDB.readConversationWithLocalId(Long.valueOf(j));
        if (readConversationWithLocalId == null) {
            return null;
        }
        readConversationWithLocalId.setMessageDMs(this.conversationDB.readMessages(j));
        return readConversationWithLocalId;
    }

    public synchronized void deleteConversation(long j) {
        if (j != 0) {
            this.conversationDB.deleteConversationWithLocalId(j);
        }
    }

    public synchronized List<Conversation> readConversationsWithoutMessages(long j) {
        return this.conversationDB.readConversationsWithLocalId(j);
    }

    public synchronized void insertPreIssueConversation(Conversation conversation) {
        if (conversation.localUUID == null) {
            conversation.localUUID = UUID.randomUUID().toString();
        }
        long insertConversation = this.conversationDB.insertConversation(conversation);
        if (insertConversation != -1) {
            conversation.setLocalId(insertConversation);
        }
    }

    public synchronized Map<Long, Integer> getMessagesCountForConversations(List<Long> list) {
        return this.conversationDB.getMessagesCountForConversations(list, null);
    }

    public synchronized Map<Long, Integer> getMessagesCountForConversations(List<Long> list, String[] strArr) {
        return this.conversationDB.getMessagesCountForConversations(list, strArr);
    }

    public synchronized List<MessageDM> readMessages(long j) {
        return this.conversationDB.readMessages(j);
    }

    public List<MessageDM> readMessages(long j, MessageType messageType) {
        return this.conversationDB.readMessages(j, messageType);
    }

    public List<MessageDM> readMessagesForConversations(List<Long> list) {
        return this.conversationDB.readMessagesForConversations(list);
    }

    public MessageDM readMessage(String str) {
        return this.conversationDB.readMessageWithServerId(str);
    }

    public synchronized void insertOrUpdateMessage(MessageDM messageDM) {
        Long l = messageDM.localId;
        String str = messageDM.serverId;
        if (l == null && str == null) {
            long insertMessage = this.conversationDB.insertMessage(messageDM);
            if (insertMessage != -1) {
                messageDM.localId = Long.valueOf(insertMessage);
            }
        } else if (l == null && str != null) {
            MessageDM readMessageWithServerId = this.conversationDB.readMessageWithServerId(str);
            if (readMessageWithServerId == null) {
                long insertMessage2 = this.conversationDB.insertMessage(messageDM);
                if (insertMessage2 != -1) {
                    messageDM.localId = Long.valueOf(insertMessage2);
                }
            } else {
                messageDM.localId = readMessageWithServerId.localId;
                this.conversationDB.updateMessage(messageDM);
            }
        } else if (this.conversationDB.readMessageWithLocalId(l) == null) {
            long insertMessage3 = this.conversationDB.insertMessage(messageDM);
            if (insertMessage3 != -1) {
                messageDM.localId = Long.valueOf(insertMessage3);
            }
        } else {
            this.conversationDB.updateMessage(messageDM);
        }
    }

    public synchronized void insertOrUpdateMessages(List<MessageDM> list) {
        if (list.size() != 0) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (MessageDM next : list) {
                Long l = next.localId;
                String str = next.serverId;
                if (l == null && str == null) {
                    arrayList.add(next);
                } else if (l == null && str != null) {
                    MessageDM readMessageWithServerId = this.conversationDB.readMessageWithServerId(str);
                    if (readMessageWithServerId == null) {
                        arrayList.add(next);
                    } else {
                        next.localId = readMessageWithServerId.localId;
                        arrayList2.add(next);
                    }
                } else if (this.conversationDB.readMessageWithLocalId(l) == null) {
                    arrayList.add(next);
                } else {
                    arrayList2.add(next);
                }
            }
            List<Long> insertMessages = this.conversationDB.insertMessages(arrayList);
            for (int i = 0; i < arrayList.size(); i++) {
                long longValue = insertMessages.get(i).longValue();
                if (longValue != -1) {
                    ((MessageDM) arrayList.get(i)).localId = Long.valueOf(longValue);
                }
            }
            this.conversationDB.updateMessages(arrayList2);
        }
    }

    public void insertConversation(Conversation conversation) {
        String str = conversation.serverId;
        String str2 = conversation.preConversationServerId;
        if (str != null || str2 != null) {
            if (conversation.localUUID == null) {
                conversation.localUUID = UUID.randomUUID().toString();
            }
            long insertConversation = this.conversationDB.insertConversation(conversation);
            if (insertConversation != -1) {
                conversation.setLocalId(insertConversation);
            }
            insertOrUpdateMessages(conversation.messageDMs);
        }
    }

    public void updateConversation(Conversation conversation) {
        String str = conversation.serverId;
        String str2 = conversation.preConversationServerId;
        if (str != null || str2 != null) {
            this.conversationDB.updateConversation(conversation);
            insertOrUpdateMessages(conversation.messageDMs);
        }
    }

    public void updateLastUserActivityTimeInConversation(Long l, long j) {
        if (l == null) {
            HSLogger.e("Helpshift_CnDAO", "Trying to update last user activity time but localId is null");
        } else {
            this.conversationDB.updateLastUserActivityTimeInConversation(l, j);
        }
    }

    public void updateConversationWithoutMessages(Conversation conversation) {
        this.conversationDB.updateConversation(conversation);
    }

    public void insertConversations(List<Conversation> list) {
        if (list.size() != 0) {
            for (Conversation next : list) {
                if (next.localUUID == null) {
                    next.localUUID = UUID.randomUUID().toString();
                }
            }
            List<Long> insertConversations = this.conversationDB.insertConversations(list);
            HashSet hashSet = new HashSet();
            for (int i = 0; i < list.size(); i++) {
                long longValue = insertConversations.get(i).longValue();
                Conversation conversation = list.get(i);
                if (longValue == -1) {
                    hashSet.add(conversation);
                } else {
                    conversation.setLocalId(longValue);
                }
            }
            ArrayList arrayList = new ArrayList();
            for (Conversation next2 : list) {
                if (!hashSet.contains(next2)) {
                    arrayList.addAll(next2.messageDMs);
                }
            }
            insertOrUpdateMessages(arrayList);
        }
    }

    public void updateConversations(List<Conversation> list, Map<Long, ConversationUpdate> map) {
        if (list.size() != 0) {
            this.conversationDB.updateConversations(list);
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (Conversation next : list) {
                if (map.containsKey(next.localId)) {
                    ConversationUpdate conversationUpdate = map.get(next.localId);
                    arrayList.addAll(conversationUpdate.newMessageDMs);
                    arrayList2.addAll(conversationUpdate.updatedMessageDMs);
                }
            }
            List<Long> insertMessages = this.conversationDB.insertMessages(arrayList);
            for (int i = 0; i < arrayList.size(); i++) {
                long longValue = insertMessages.get(i).longValue();
                if (longValue != -1) {
                    ((MessageDM) arrayList.get(i)).localId = Long.valueOf(longValue);
                }
            }
            this.conversationDB.updateMessages(arrayList2);
        }
    }

    public void deleteConversations(long j) {
        if (j > 0) {
            this.conversationDB.deleteConversations(j);
        }
    }

    public String getOldestMessageCursor(long j) {
        return this.conversationDB.getOldestMessageCursor(j);
    }

    public Long getOldestConversationCreatedAtTime(long j) {
        return this.conversationDB.getOldestConversationEpochCreatedAtTime(j);
    }

    public boolean deleteMessagesForConversation(long j) {
        return this.conversationDB.deleteMessagesForConversation(j);
    }

    public Object getFAQ(String str, String str2) {
        return this.conversationDB.getAdminFAQSuggestion(str, str2);
    }

    public void insertOrUpdateFAQ(Object obj) {
        this.conversationDB.insertOrUpdateAdminFAQSuggestion((Faq) obj);
    }

    public void removeFAQ(String str, String str2) {
        this.conversationDB.removeAdminFAQSuggestion(str, str2);
    }
}
