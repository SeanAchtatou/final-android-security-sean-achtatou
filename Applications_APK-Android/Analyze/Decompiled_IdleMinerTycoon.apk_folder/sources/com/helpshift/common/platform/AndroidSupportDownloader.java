package com.helpshift.common.platform;

import android.content.Context;
import com.helpshift.android.commons.downloader.DownloadConfig;
import com.helpshift.android.commons.downloader.DownloadManager;
import com.helpshift.android.commons.downloader.contracts.DownloadDirType;
import com.helpshift.android.commons.downloader.contracts.DownloadRequestedFileInfo;
import com.helpshift.android.commons.downloader.contracts.NetworkAuthDataFetcher;
import com.helpshift.android.commons.downloader.contracts.OnDownloadFinishListener;
import com.helpshift.android.commons.downloader.contracts.OnProgressChangedListener;
import com.helpshift.common.domain.HSThreadFactory;
import com.helpshift.common.domain.network.AuthDataProvider;
import com.helpshift.common.platform.network.Method;
import com.helpshift.downloader.AdminFileInfo;
import com.helpshift.downloader.SupportDownloadStateChangeListener;
import com.helpshift.downloader.SupportDownloader;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class AndroidSupportDownloader implements SupportDownloader {
    private static final int CORE_POOL_SIZE = 5;
    private static final int KEEP_ALIVE_TIME = 1;
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
    private static final int MAXIMUM_POOL_SIZE = 5;
    private Map<String, Set<SupportDownloadStateChangeListener>> callbackManager = new HashMap();
    private Context context;
    private final DownloadManager downloadManager;

    public AndroidSupportDownloader(Context context2, KVStore kVStore) {
        this.context = context2;
        this.downloadManager = new DownloadManager(context2, new SupportDownloaderKVStorage(kVStore), new ThreadPoolExecutor(5, 5, 1, KEEP_ALIVE_TIME_UNIT, new LinkedBlockingQueue(), new HSThreadFactory("sp-dwnld")));
    }

    private DownloadConfig buildDownloadConfig(SupportDownloader.StorageDirType storageDirType) {
        DownloadDirType downloadDirType;
        boolean z = false;
        switch (storageDirType) {
            case INTERNAL_ONLY:
                downloadDirType = DownloadDirType.INTERNAL_ONLY;
                z = true;
                break;
            case EXTERNAL_ONLY:
                downloadDirType = DownloadDirType.EXTERNAL_ONLY;
                break;
            case EXTERNAL_OR_INTERNAL:
                downloadDirType = DownloadDirType.EXTERNAL_OR_INTERNAL;
                break;
            default:
                throw new IllegalStateException("Unsupported download Dir type");
        }
        return new DownloadConfig.Builder().setUseCache(true).setIsNoMedia(z).setWriteToFile(true).setDownloadDirType(downloadDirType).create();
    }

    public void startDownload(AdminFileInfo adminFileInfo, SupportDownloader.StorageDirType storageDirType, final AuthDataProvider authDataProvider, SupportDownloadStateChangeListener supportDownloadStateChangeListener) {
        addCallback(adminFileInfo.url, supportDownloadStateChangeListener);
        this.downloadManager.startDownload(new DownloadRequestedFileInfo(adminFileInfo.url, adminFileInfo.isSecureAttachment, adminFileInfo.contentType), buildDownloadConfig(storageDirType), new NetworkAuthDataFetcher() {
            public Map<String, String> getAuthData(Map<String, String> map) throws GeneralSecurityException {
                return authDataProvider.getAuthData(Method.GET, map);
            }
        }, new OnProgressChangedListener() {
            public void onProgressChanged(String str, int i) {
                AndroidSupportDownloader.this.handleProgressChange(str, i);
            }
        }, new OnDownloadFinishListener() {
            public void onDownloadFinish(boolean z, String str, Object obj) {
                if (z) {
                    AndroidSupportDownloader.this.handleDownloadSuccess(str, obj.toString());
                    return;
                }
                AndroidSupportDownloader.this.handleDownloadFailure(str);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void handleDownloadSuccess(String str, String str2) {
        for (SupportDownloadStateChangeListener onSuccess : getCallbacks(str)) {
            onSuccess.onSuccess(str, str2);
        }
        removeCallbacks(str);
    }

    /* access modifiers changed from: package-private */
    public void handleProgressChange(String str, int i) {
        for (SupportDownloadStateChangeListener onProgressChange : getCallbacks(str)) {
            onProgressChange.onProgressChange(str, i);
        }
    }

    /* access modifiers changed from: package-private */
    public void handleDownloadFailure(String str) {
        for (SupportDownloadStateChangeListener onFailure : getCallbacks(str)) {
            onFailure.onFailure(str);
        }
        removeCallbacks(str);
    }

    private synchronized void addCallback(String str, SupportDownloadStateChangeListener supportDownloadStateChangeListener) {
        if (supportDownloadStateChangeListener != null) {
            Set set = this.callbackManager.get(str);
            if (set == null) {
                set = new HashSet();
            }
            set.add(supportDownloadStateChangeListener);
            this.callbackManager.put(str, set);
        }
    }

    private synchronized void removeCallbacks(String str) {
        this.callbackManager.remove(str);
    }

    private synchronized Set<SupportDownloadStateChangeListener> getCallbacks(String str) {
        HashSet hashSet;
        Set set = this.callbackManager.get(str);
        if (set == null) {
            hashSet = new HashSet();
        } else {
            hashSet = new HashSet(set);
        }
        return hashSet;
    }
}
