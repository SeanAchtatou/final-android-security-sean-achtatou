package com.helpshift.common.exception;

public class RootAPIException extends RuntimeException {
    public final Exception exception;
    public final ExceptionType exceptionType;
    public final String message;

    private RootAPIException(Exception exc, ExceptionType exceptionType2, String str) {
        super(str, exc);
        this.exception = exc;
        this.exceptionType = exceptionType2;
        this.message = str;
    }

    public static RootAPIException wrap(Exception exc) {
        return wrap(exc, null);
    }

    public static RootAPIException wrap(Exception exc, ExceptionType exceptionType2) {
        return wrap(exc, exceptionType2, null);
    }

    public static RootAPIException wrap(Exception exc, ExceptionType exceptionType2, String str) {
        if (exc instanceof RootAPIException) {
            RootAPIException rootAPIException = (RootAPIException) exc;
            Exception exc2 = rootAPIException.exception;
            if (exceptionType2 == null) {
                exceptionType2 = rootAPIException.exceptionType;
            }
            if (str == null) {
                str = rootAPIException.message;
            }
            exc = exc2;
        } else if (exceptionType2 == null) {
            exceptionType2 = UnexpectedException.GENERIC;
        }
        return new RootAPIException(exc, exceptionType2, str);
    }

    public int getServerStatusCode() {
        if (this.exceptionType instanceof NetworkException) {
            return ((NetworkException) this.exceptionType).serverStatusCode;
        }
        return 0;
    }

    public boolean shouldLog() {
        return this.exception != null;
    }
}
