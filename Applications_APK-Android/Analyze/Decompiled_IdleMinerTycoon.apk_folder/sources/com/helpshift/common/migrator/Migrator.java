package com.helpshift.common.migrator;

import android.database.sqlite.SQLiteDatabase;

public abstract class Migrator {
    protected SQLiteDatabase db;

    public abstract void migrate();

    public Migrator(SQLiteDatabase sQLiteDatabase) {
        this.db = sQLiteDatabase;
    }
}
