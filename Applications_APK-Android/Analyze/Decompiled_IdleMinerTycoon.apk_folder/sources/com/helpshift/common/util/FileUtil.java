package com.helpshift.common.util;

import com.helpshift.common.StringUtils;
import java.io.File;

public class FileUtil {
    public static File validateAndCreateFile(String str) {
        if (StringUtils.isEmpty(str)) {
            return null;
        }
        File file = new File(str);
        if (file.exists()) {
            return file;
        }
        return null;
    }

    public static boolean doesFilePathExistAndCanRead(String str) {
        File validateAndCreateFile = validateAndCreateFile(str);
        return validateAndCreateFile != null && validateAndCreateFile.canRead();
    }

    public static boolean deleteFile(String str) {
        File validateAndCreateFile = validateAndCreateFile(str);
        return validateAndCreateFile != null && validateAndCreateFile.delete();
    }
}
