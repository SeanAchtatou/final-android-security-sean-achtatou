package com.helpshift.common.util;

import java.util.Collection;

public interface HSListObserver<T> {
    void add(Object obj);

    void addAll(Collection<? extends T> collection);

    void update(Object obj);
}
