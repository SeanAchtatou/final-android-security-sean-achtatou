package com.helpshift.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class HSObservableList<T> extends ArrayList<T> {
    private HSListObserver<T> observer;

    public HSObservableList() {
    }

    public HSObservableList(List<T> list) {
        super(list);
    }

    public void setObserver(HSListObserver<T> hSListObserver) {
        this.observer = hSListObserver;
    }

    public boolean add(T t) {
        boolean add = super.add(t);
        if (add && this.observer != null) {
            this.observer.add(t);
        }
        return add;
    }

    public boolean addAll(Collection<? extends T> collection) {
        boolean addAll = super.addAll(collection);
        if (addAll && this.observer != null) {
            this.observer.addAll(collection);
        }
        return addAll;
    }

    public void prependItems(Collection<? extends T> collection) {
        super.addAll(0, collection);
    }

    public T setAndNotifyObserver(int i, T t) {
        T t2 = super.set(i, t);
        if (!(t2 == null || this.observer == null)) {
            this.observer.update(t);
        }
        return t2;
    }
}
