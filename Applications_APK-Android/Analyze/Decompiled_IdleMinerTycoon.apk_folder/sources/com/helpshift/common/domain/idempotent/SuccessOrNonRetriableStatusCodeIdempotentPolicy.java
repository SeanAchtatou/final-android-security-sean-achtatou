package com.helpshift.common.domain.idempotent;

import com.helpshift.common.domain.network.NetworkErrorCodes;

public class SuccessOrNonRetriableStatusCodeIdempotentPolicy extends BaseIdempotentPolicy {
    /* access modifiers changed from: package-private */
    public boolean shouldMarkRequestCompleted(int i) {
        return (i >= 200 && i < 300) || NetworkErrorCodes.NOT_RETRIABLE_STATUS_CODES.contains(Integer.valueOf(i));
    }
}
