package com.helpshift.common.domain;

import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.platform.Platform;
import com.helpshift.conversation.dto.ImagePickerFile;
import java.io.File;

public class AttachmentFileManagerDM {
    public static final String LOCAL_RSC_MESSAGE_PREFIX = "localRscMessage_";
    private Domain domain;
    Platform platform;

    public interface Listener {
        void onCompressAndCopyFailure(RootAPIException rootAPIException);

        void onCompressAndCopySuccess(ImagePickerFile imagePickerFile);
    }

    public AttachmentFileManagerDM(Domain domain2, Platform platform2) {
        this.domain = domain2;
        this.platform = platform2;
    }

    public void compressAndCopyScreenshot(final ImagePickerFile imagePickerFile, final String str, final Listener listener) {
        this.domain.runParallel(new F() {
            public void f() {
                try {
                    AttachmentFileManagerDM.this.platform.compressAndCopyScreenshot(imagePickerFile, str);
                    listener.onCompressAndCopySuccess(imagePickerFile);
                } catch (RootAPIException e) {
                    listener.onCompressAndCopyFailure(e);
                    throw e;
                }
            }
        });
    }

    public void deleteAttachmentLocalCopy(ImagePickerFile imagePickerFile) {
        if (imagePickerFile != null && imagePickerFile.filePath != null && imagePickerFile.isFileCompressionAndCopyingDone) {
            new File(imagePickerFile.filePath).delete();
        }
    }
}
