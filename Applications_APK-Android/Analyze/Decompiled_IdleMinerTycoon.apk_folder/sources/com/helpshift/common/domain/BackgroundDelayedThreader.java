package com.helpshift.common.domain;

import com.helpshift.common.exception.NetworkException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.logger.logmodels.ILogExtrasModel;
import com.helpshift.logger.logmodels.LogExtrasModelProvider;
import com.helpshift.util.HSLogger;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

class BackgroundDelayedThreader implements DelayedThreader {
    private static final String TAG = "Helpshift_CoreDelayTh";
    final ScheduledExecutorService service;

    BackgroundDelayedThreader(ScheduledExecutorService scheduledExecutorService) {
        this.service = scheduledExecutorService;
    }

    public F thread(final F f, final long j) {
        return new F() {
            public void f() {
                f.cause = new Throwable();
                try {
                    BackgroundDelayedThreader.this.service.schedule(new Runnable() {
                        public void run() {
                            try {
                                f.f();
                            } catch (RootAPIException e) {
                                if (e.shouldLog()) {
                                    ILogExtrasModel iLogExtrasModel = null;
                                    String str = e.message == null ? "" : e.message;
                                    if (e.exceptionType instanceof NetworkException) {
                                        iLogExtrasModel = LogExtrasModelProvider.fromString("route", ((NetworkException) e.exceptionType).route);
                                    }
                                    HSLogger.e(BackgroundDelayedThreader.TAG, str, new Throwable[]{e.exception, f.cause}, iLogExtrasModel);
                                }
                            } catch (Exception e2) {
                                HSLogger.f(BackgroundDelayedThreader.TAG, "Caught unhandled exception inside BackgroundThreader", new Throwable[]{e2, f.cause}, new ILogExtrasModel[0]);
                            }
                        }
                    }, j, TimeUnit.MILLISECONDS);
                } catch (RejectedExecutionException e) {
                    HSLogger.e(BackgroundDelayedThreader.TAG, "Rejected execution of task in BackgroundDelayedThreader", e);
                }
            }
        };
    }
}
