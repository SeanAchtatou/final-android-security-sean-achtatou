package com.helpshift.common.domain;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class HSThreadFactory implements ThreadFactory {
    private final String poolName;
    private final AtomicInteger threadNumber = new AtomicInteger(1);

    public HSThreadFactory(String str) {
        this.poolName = str;
    }

    public Thread newThread(Runnable runnable) {
        return new Thread(runnable, "HS-" + this.poolName + "-t-" + this.threadNumber.getAndIncrement());
    }
}
