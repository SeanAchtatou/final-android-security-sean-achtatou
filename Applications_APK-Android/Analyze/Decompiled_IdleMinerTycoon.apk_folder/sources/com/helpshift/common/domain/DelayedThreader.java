package com.helpshift.common.domain;

interface DelayedThreader {
    F thread(F f, long j);
}
