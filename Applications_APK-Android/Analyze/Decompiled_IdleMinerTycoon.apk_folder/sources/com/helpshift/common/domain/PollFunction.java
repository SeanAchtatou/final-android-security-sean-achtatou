package com.helpshift.common.domain;

import com.helpshift.common.domain.network.NetworkErrorCodes;
import com.helpshift.common.exception.NetworkException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.poller.HttpBackoff;
import com.helpshift.util.HSLogger;

public class PollFunction extends F {
    private static final String TAG = "Helpshift_PollFunc";
    private HttpBackoff backoff;
    private final Domain domain;
    private final PollFunctionListener listener;
    private final F poll;
    private final PollingInterval pollingInterval;
    private boolean shouldPoll;

    public interface PollFunctionListener {
        void onPollingStoppedViaBackoffStrategy();
    }

    public PollFunction(Domain domain2, HttpBackoff httpBackoff, F f, PollingInterval pollingInterval2, PollFunctionListener pollFunctionListener) {
        this.backoff = httpBackoff;
        this.poll = f;
        this.domain = domain2;
        this.pollingInterval = pollingInterval2;
        this.listener = pollFunctionListener;
    }

    public void f() {
        int i;
        if (this.shouldPoll) {
            try {
                HSLogger.d(TAG, "Running:" + this.pollingInterval.name());
                this.poll.f();
                i = NetworkErrorCodes.OK.intValue();
            } catch (RootAPIException e) {
                if (e.exceptionType instanceof NetworkException) {
                    i = e.getServerStatusCode();
                } else {
                    throw e;
                }
            }
            long nextIntervalMillis = this.backoff.nextIntervalMillis(i);
            if (nextIntervalMillis != -100) {
                schedulePoll(nextIntervalMillis);
            } else if (this.listener != null) {
                this.listener.onPollingStoppedViaBackoffStrategy();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void schedulePoll(long j) {
        this.domain.runDelayedInParallel(this, j);
    }

    public void start(long j) {
        HSLogger.d(TAG, "Start: " + this.pollingInterval.name());
        if (!this.shouldPoll) {
            this.shouldPoll = true;
            schedulePoll(j);
        }
    }

    public void stop() {
        HSLogger.d(TAG, "Stop: " + this.pollingInterval.name());
        this.shouldPoll = false;
        this.backoff.reset();
    }
}
