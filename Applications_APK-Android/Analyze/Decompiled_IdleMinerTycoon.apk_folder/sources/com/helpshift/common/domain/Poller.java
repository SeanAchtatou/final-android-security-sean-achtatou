package com.helpshift.common.domain;

import com.helpshift.common.domain.PollFunction;
import com.helpshift.common.domain.network.NetworkErrorCodes;
import com.helpshift.common.poller.Delay;
import com.helpshift.common.poller.HttpBackoff;
import java.util.concurrent.TimeUnit;

public class Poller {
    private PollFunction activePollFunction;
    private HttpBackoff aggressiveBackoff = new HttpBackoff.Builder().setBaseInterval(Delay.of(3, TimeUnit.SECONDS)).setMaxInterval(Delay.of(3, TimeUnit.SECONDS)).setRandomness(0.0f).setMultiplier(1.0f).setRetryPolicy(getPollerRetryPollicy()).build();
    private HttpBackoff conservativeBackoff = new HttpBackoff.Builder().setBaseInterval(Delay.of(5, TimeUnit.SECONDS)).setMaxInterval(Delay.of(1, TimeUnit.MINUTES)).setRandomness(0.1f).setMultiplier(2.0f).setRetryPolicy(getPollerRetryPollicy()).build();
    private final Domain domain;
    private HttpBackoff passiveBackoff = new HttpBackoff.Builder().setBaseInterval(Delay.of(30, TimeUnit.SECONDS)).setMaxInterval(Delay.of(5, TimeUnit.MINUTES)).setRandomness(0.1f).setMultiplier(4.0f).setRetryPolicy(getPollerRetryPollicy()).build();
    private final F poll;

    public Poller(Domain domain2, F f) {
        this.domain = domain2;
        this.poll = f;
    }

    public synchronized void start(PollingInterval pollingInterval, long j, PollFunction.PollFunctionListener pollFunctionListener) {
        stop();
        if (pollingInterval != null) {
            switch (pollingInterval) {
                case AGGRESSIVE:
                    this.activePollFunction = new PollFunction(this.domain, this.aggressiveBackoff, this.poll, PollingInterval.AGGRESSIVE, pollFunctionListener);
                    break;
                case PASSIVE:
                    this.activePollFunction = new PollFunction(this.domain, this.passiveBackoff, this.poll, PollingInterval.PASSIVE, pollFunctionListener);
                    break;
                case CONSERVATIVE:
                    this.activePollFunction = new PollFunction(this.domain, this.conservativeBackoff, this.poll, PollingInterval.CONSERVATIVE, pollFunctionListener);
                    break;
            }
            this.activePollFunction.start(j);
        }
    }

    public synchronized void stop() {
        if (this.activePollFunction != null) {
            this.activePollFunction.stop();
            this.activePollFunction = null;
        }
    }

    private HttpBackoff.RetryPolicy getPollerRetryPollicy() {
        return new HttpBackoff.RetryPolicy() {
            public boolean shouldRetry(int i) {
                if (i == NetworkErrorCodes.AUTH_TOKEN_NOT_PROVIDED.intValue() || i == NetworkErrorCodes.INVALID_AUTH_TOKEN.intValue() || NetworkErrorCodes.NOT_RETRIABLE_STATUS_CODES.contains(Integer.valueOf(i))) {
                    return false;
                }
                return true;
            }
        };
    }
}
