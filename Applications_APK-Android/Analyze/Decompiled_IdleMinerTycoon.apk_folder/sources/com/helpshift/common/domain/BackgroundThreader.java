package com.helpshift.common.domain;

import com.helpshift.common.exception.NetworkException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.logger.logmodels.ILogExtrasModel;
import com.helpshift.logger.logmodels.LogExtrasModelProvider;
import com.helpshift.util.HSLogger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;

class BackgroundThreader implements Threader {
    private static final String TAG = "Helpshift_CoreBgTh";
    final ExecutorService service;

    BackgroundThreader(ExecutorService executorService) {
        this.service = executorService;
    }

    public F thread(final F f) {
        return new F() {
            public void f() {
                f.cause = new Throwable();
                try {
                    BackgroundThreader.this.service.submit(new Runnable() {
                        public void run() {
                            try {
                                f.f();
                            } catch (RootAPIException e) {
                                if (e.shouldLog()) {
                                    ILogExtrasModel iLogExtrasModel = null;
                                    String str = e.message == null ? "" : e.message;
                                    if (e.exceptionType instanceof NetworkException) {
                                        iLogExtrasModel = LogExtrasModelProvider.fromString("route", ((NetworkException) e.exceptionType).route);
                                    }
                                    HSLogger.e(BackgroundThreader.TAG, str, new Throwable[]{e.exception, f.cause}, iLogExtrasModel);
                                }
                            } catch (Exception e2) {
                                HSLogger.f(BackgroundThreader.TAG, "Caught unhandled exception inside BackgroundThreader", new Throwable[]{e2, f.cause}, new ILogExtrasModel[0]);
                            }
                        }
                    });
                } catch (RejectedExecutionException e) {
                    HSLogger.e(BackgroundThreader.TAG, "Rejected execution of task in BackgroundThreader", e);
                }
            }
        };
    }
}
