package com.helpshift.common.domain.network;

import com.helpshift.common.platform.Platform;
import com.helpshift.common.platform.network.KeyValuePair;
import com.helpshift.common.platform.network.NetworkRequestDAO;
import com.helpshift.common.platform.network.RequestData;
import com.helpshift.common.platform.network.Response;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.List;

public class ETagNetwork implements Network {
    private final Network network;
    private final NetworkRequestDAO networkRequestDAO;
    private final String route;

    public ETagNetwork(Network network2, Platform platform, String str) {
        this.network = network2;
        this.networkRequestDAO = platform.getNetworkRequestDAO();
        this.route = str;
    }

    public Response makeRequest(RequestData requestData) {
        String headerValue;
        Response makeRequest = this.network.makeRequest(requestData);
        int i = makeRequest.status;
        if (i >= 200 && i < 300 && (headerValue = getHeaderValue(makeRequest.headers, HttpRequest.HEADER_ETAG)) != null) {
            this.networkRequestDAO.storeETag(this.route, headerValue);
        }
        return makeRequest;
    }

    private String getHeaderValue(List<KeyValuePair> list, String str) {
        for (KeyValuePair next : list) {
            if (next.key != null && next.key.equals(str)) {
                return next.value;
            }
        }
        return null;
    }
}
