package com.helpshift.common.domain.network;

import com.helpshift.common.domain.Domain;
import com.helpshift.common.exception.NetworkException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.platform.Platform;
import com.helpshift.common.platform.network.KeyValuePair;
import com.helpshift.common.platform.network.Method;
import com.helpshift.common.platform.network.Request;
import com.helpshift.common.platform.network.RequestData;
import com.helpshift.common.platform.network.Response;
import com.helpshift.common.platform.network.UploadRequest;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.util.List;

public class UploadNetwork extends BaseNetwork {
    Platform platform;

    public /* bridge */ /* synthetic */ Response makeRequest(RequestData requestData) {
        return super.makeRequest(requestData);
    }

    public UploadNetwork(String str, Domain domain, Platform platform2) {
        super(str, domain, platform2);
        this.platform = platform2;
    }

    /* access modifiers changed from: package-private */
    public Request getRequest(RequestData requestData) {
        String mimeTypeForFile = this.platform.getMimeTypeForFile(new File(requestData.body.get("screenshot")).getPath());
        if (this.platform.isSupportedMimeType(mimeTypeForFile)) {
            return new UploadRequest(Method.POST, getURL(), getAuthData(Method.POST, NetworkDataRequestUtil.cleanData(requestData.body)), mimeTypeForFile, getHeaders(requestData.getRequestId(), requestData), NetworkConstants.UPLOAD_CONNECT_TIMEOUT);
        }
        throw RootAPIException.wrap(null, NetworkException.UNSUPPORTED_MIME_TYPE);
    }

    /* access modifiers changed from: package-private */
    public List<KeyValuePair> getHeaders(String str, RequestData requestData) {
        List<KeyValuePair> headers = super.getHeaders(str, requestData);
        headers.add(new KeyValuePair("Connection", "Keep-Alive"));
        headers.add(new KeyValuePair(HttpRequest.HEADER_CONTENT_TYPE, "multipart/form-data;boundary=*****"));
        return headers;
    }
}
