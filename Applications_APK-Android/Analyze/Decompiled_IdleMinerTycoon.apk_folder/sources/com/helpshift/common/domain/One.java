package com.helpshift.common.domain;

import java.util.concurrent.atomic.AtomicBoolean;

public class One extends F {
    private final F f;
    private final AtomicBoolean running = new AtomicBoolean(false);

    public One(F f2) {
        this.f = f2;
    }

    public void f() {
        if (this.running.compareAndSet(false, true)) {
            try {
                this.f.f();
            } finally {
                this.running.set(false);
            }
        }
    }

    public F getF() {
        return this.f;
    }
}
