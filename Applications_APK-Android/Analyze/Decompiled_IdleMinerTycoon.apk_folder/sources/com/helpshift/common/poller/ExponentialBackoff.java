package com.helpshift.common.poller;

import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;

public class ExponentialBackoff {
    public static final long STOP = -100;
    private int attempts;
    private final long baseIntervalMillis;
    private long currentBaseIntervalMillis;
    private final int maxAttempts;
    private final long maxIntervalMillis;
    private final float multiplier;
    private final SecureRandom random = new SecureRandom();
    private final float randomness;

    ExponentialBackoff(Builder builder) {
        this.baseIntervalMillis = builder.baseIntervalMillis;
        this.maxIntervalMillis = builder.maxIntervalMillis;
        this.randomness = builder.randomness;
        this.multiplier = builder.multiplier;
        this.maxAttempts = builder.maxAttempts;
        reset();
    }

    public void reset() {
        this.currentBaseIntervalMillis = this.baseIntervalMillis;
        this.attempts = 0;
    }

    public long nextIntervalMillis() {
        if (this.attempts >= this.maxAttempts) {
            return -100;
        }
        this.attempts++;
        float f = ((float) this.currentBaseIntervalMillis) * (1.0f - this.randomness);
        float f2 = ((float) this.currentBaseIntervalMillis) * (this.randomness + 1.0f);
        if (this.currentBaseIntervalMillis <= this.maxIntervalMillis) {
            this.currentBaseIntervalMillis = Math.min((long) (((float) this.currentBaseIntervalMillis) * this.multiplier), this.maxIntervalMillis);
        }
        return (long) (f + (this.random.nextFloat() * (f2 - f)));
    }

    public static class Builder {
        public static final String ERROR_BASE_INTERVAL_RANGE = "Base interval can't be negative or zero";
        public static final String ERROR_MAX_ATTEMPTS_RANGE = "Max attempts can't be negative or zero";
        public static final String ERROR_MAX_INTERVAL_LESS_THAN_BASE_INTERVAL = "Max interval can't be less than base interval";
        public static final String ERROR_MAX_INTERVAL_RANGE = "Max interval can't be negative or zero";
        public static final String ERROR_MULTIPLIER_RANGE = "Multiplier can't be less than 1";
        public static final String ERROR_RANDOMNESS_RANGE = "Randomness must be between 0 and 1 (both inclusive)";
        long baseIntervalMillis = TimeUnit.SECONDS.toMillis(10);
        int maxAttempts = Integer.MAX_VALUE;
        long maxIntervalMillis = TimeUnit.SECONDS.toMillis(60);
        float multiplier = 2.0f;
        float randomness = 0.5f;

        public Builder setBaseInterval(Delay delay) {
            this.baseIntervalMillis = delay.timeUnit.toMillis(delay.delay);
            return this;
        }

        public Builder setMaxInterval(Delay delay) {
            this.maxIntervalMillis = delay.timeUnit.toMillis(delay.delay);
            return this;
        }

        public Builder setRandomness(float f) {
            this.randomness = f;
            return this;
        }

        public Builder setMultiplier(float f) {
            this.multiplier = f;
            return this;
        }

        public Builder setMaxAttempts(int i) {
            this.maxAttempts = i;
            return this;
        }

        /* access modifiers changed from: package-private */
        public void validate() {
            if (this.baseIntervalMillis <= 0) {
                throw new IllegalArgumentException(ERROR_BASE_INTERVAL_RANGE);
            } else if (this.maxIntervalMillis <= 0) {
                throw new IllegalArgumentException(ERROR_MAX_INTERVAL_RANGE);
            } else if (this.maxIntervalMillis < this.baseIntervalMillis) {
                throw new IllegalArgumentException(ERROR_MAX_INTERVAL_LESS_THAN_BASE_INTERVAL);
            } else if (this.randomness < 0.0f || this.randomness > 1.0f) {
                throw new IllegalArgumentException(ERROR_RANDOMNESS_RANGE);
            } else if (this.multiplier < 1.0f) {
                throw new IllegalArgumentException(ERROR_MULTIPLIER_RANGE);
            } else if (this.maxAttempts <= 0) {
                throw new IllegalArgumentException(ERROR_MAX_ATTEMPTS_RANGE);
            }
        }

        public ExponentialBackoff build() throws IllegalArgumentException {
            validate();
            return new ExponentialBackoff(this);
        }
    }
}
