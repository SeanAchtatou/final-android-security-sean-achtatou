package com.helpshift.common.conversation.migration;

import android.database.sqlite.SQLiteDatabase;
import com.helpshift.common.migrator.Migrator;

public class ConversationMigrationFactory {
    public static Migrator getMigrationForDbVersion(int i, SQLiteDatabase sQLiteDatabase) {
        if (i == 6) {
            return new MigrationFromDb_6_to_7(sQLiteDatabase);
        }
        if (i == 7) {
            return new MigrationFromDb_7_to_8(sQLiteDatabase);
        }
        throw new IllegalStateException("Unsupported version for database migration");
    }
}
