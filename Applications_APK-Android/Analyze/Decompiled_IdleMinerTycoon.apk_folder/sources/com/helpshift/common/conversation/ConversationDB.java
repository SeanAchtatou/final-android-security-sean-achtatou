package com.helpshift.common.conversation;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.helpshift.common.ListUtils;
import com.helpshift.common.StringUtils;
import com.helpshift.constants.Tables;
import com.helpshift.conversation.activeconversation.message.AcceptedAppReviewMessageDM;
import com.helpshift.conversation.activeconversation.message.AdminBotControlMessageDM;
import com.helpshift.conversation.activeconversation.message.AdminMessageWithOptionInputDM;
import com.helpshift.conversation.activeconversation.message.AdminMessageWithTextInputDM;
import com.helpshift.conversation.activeconversation.message.AttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.AutoRetriableMessageDM;
import com.helpshift.conversation.activeconversation.message.ConfirmationAcceptedMessageDM;
import com.helpshift.conversation.activeconversation.message.ConfirmationRejectedMessageDM;
import com.helpshift.conversation.activeconversation.message.FAQListMessageDM;
import com.helpshift.conversation.activeconversation.message.FAQListMessageWithOptionInputDM;
import com.helpshift.conversation.activeconversation.message.FollowupAcceptedMessageDM;
import com.helpshift.conversation.activeconversation.message.FollowupRejectedMessageDM;
import com.helpshift.conversation.activeconversation.message.ImageAttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.message.MessageType;
import com.helpshift.conversation.activeconversation.message.RequestAppReviewMessageDM;
import com.helpshift.conversation.activeconversation.message.RequestForReopenMessageDM;
import com.helpshift.conversation.activeconversation.message.RequestScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.ScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.UserBotControlMessageDM;
import com.helpshift.conversation.activeconversation.message.UserResponseMessageForOptionInput;
import com.helpshift.conversation.activeconversation.message.UserResponseMessageForTextInputDM;
import com.helpshift.conversation.activeconversation.message.input.OptionInput;
import com.helpshift.conversation.activeconversation.message.input.TextInput;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.dto.ImagePickerFile;
import com.helpshift.conversation.dto.IssueState;
import com.helpshift.conversation.dto.dao.ConversationInboxRecord;
import com.helpshift.conversation.states.ConversationCSATState;
import com.helpshift.platform.db.ConversationDBHelper;
import com.helpshift.support.Faq;
import com.helpshift.support.constants.FaqsColumns;
import com.helpshift.util.HSJSONUtils;
import com.helpshift.util.HSLogger;
import com.unity3d.ads.metadata.PlayerMetaData;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ConversationDB {
    private static final String TAG = "Helpshift_ConverDB";
    private static ConversationDB instance;
    private final String KEY_BOT_ACTION_TYPE = "bot_action_type";
    private final String KEY_BOT_ENDED_REASON = "bot_ended_reason";
    private final String KEY_CHATBOT_INFO = "chatbot_info";
    private final String KEY_CONTENT_TYPE = FirebaseAnalytics.Param.CONTENT_TYPE;
    private final String KEY_CONVERSATION_ENDED_DELEGATE_SENT = "ended_delegate_sent";
    private final String KEY_CSAT_FEEDBACK = "csat_feedback";
    private final String KEY_CSAT_RATING = "csat_rating";
    private final String KEY_CSAT_STATE = "csat_state";
    private final String KEY_DATE_TIME = "dt";
    private final String KEY_FAQS = Tables.FAQS;
    private final String KEY_FAQ_LANGUAGE = "faq_language";
    private final String KEY_FAQ_PUBLISH_ID = "faq_publish_id";
    private final String KEY_FAQ_TITLE = "faq_title";
    private final String KEY_FILE_NAME = "file_name";
    private final String KEY_FILE_PATH = "filePath";
    private final String KEY_FOLLOW_UP_REJECTED_OPEN_CONVERSATION = "rejected_conv_id";
    private final String KEY_FOLLOW_UP_REJECTED_REASON = "rejected_reason";
    private final String KEY_HAS_NEXT_BOT = "has_next_bot";
    private final String KEY_IMAGE_ATTACHMENT_COMPRESSION_COPYING_DONE = "image_copy_done";
    private final String KEY_IMAGE_ATTACHMENT_DRAFT_FILE_PATH = "image_draft_file_path";
    private final String KEY_IMAGE_ATTACHMENT_DRAFT_ORIGINAL_NAME = "image_draft_orig_name";
    private final String KEY_IMAGE_ATTACHMENT_DRAFT_ORIGINAL_SIZE = "image_draft_orig_size";
    private final String KEY_INCREMENT_MESSAGE_COUNT = "increment_message_count";
    private final String KEY_INPUT_KEYBOARD = "input_keyboard";
    private final String KEY_INPUT_LABEL = "input_label";
    private final String KEY_INPUT_OPTIONS = "input_options";
    private final String KEY_INPUT_PLACEHOLDER = "input_placeholder";
    private final String KEY_INPUT_REQUIRED = "input_required";
    private final String KEY_INPUT_SKIP_LABEL = "input_skip_label";
    private final String KEY_IS_ANSWERED = "is_answered";
    private final String KEY_IS_AUTO_FILLED_PREISSUE = "is_autofilled_preissue";
    private final String KEY_IS_MESSAGE_EMPTY = "is_message_empty";
    private final String KEY_IS_RESPONSE_SKIPPED = "is_response_skipped";
    private final String KEY_IS_SUGGESTION_READ_EVENT_SENT = "is_suggestion_read_event_sent";
    private final String KEY_MESSAGE_SYNC_STATUS = "message_sync_status";
    private final String KEY_OPTION_DATA = "option_data";
    private final String KEY_OPTION_TITLE = "option_title";
    private final String KEY_OPTION_TYPE = "option_type";
    private final String KEY_READ_AT = "read_at";
    private final String KEY_REFERRED_MESSAGE_ID = "referredMessageId";
    private final String KEY_REFERRED_MESSAGE_TYPE = "referred_message_type";
    private final String KEY_SECURE_ATTACHMENT = "is_secure";
    private final String KEY_SEEN_AT_MESSAGE_CURSOR = "seen_cursor";
    private final String KEY_SEEN_SYNC_STATUS = "seen_sync_status";
    private final String KEY_SELECTED_OPTION_DATA = "selected_option_data";
    private final String KEY_SIZE = "size";
    private final String KEY_SUGGESTION_READ_FAQ_PUBLISH_ID = "suggestion_read_faq_publish_id";
    private final String KEY_THUMBNAIL_FILE_PATH = "thumbnailFilePath";
    private final String KEY_THUMBNAIL_URL = "thumbnail_url";
    private final String KEY_TIMEZONE_ID = "timezone_id";
    private final String KEY_URL = "url";
    private final ConversationDBHelper dbHelper;
    private final ConversationDBInfo dbInfo = new ConversationDBInfo();

    private ConversationDB(Context context) {
        this.dbHelper = new ConversationDBHelper(context, this.dbInfo);
    }

    public static synchronized ConversationDB getInstance(Context context) {
        ConversationDB conversationDB;
        synchronized (ConversationDB.class) {
            if (instance == null) {
                instance = new ConversationDB(context);
            }
            conversationDB = instance;
        }
        return conversationDB;
    }

    private static ContentValues faqToContentValues(Faq faq) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FaqsColumns.QUESTION_ID, faq.getId());
        contentValues.put("publish_id", faq.publish_id);
        contentValues.put(FaqsColumns.LANGUAGE, faq.language);
        contentValues.put("section_id", faq.section_publish_id);
        contentValues.put("title", faq.title);
        contentValues.put("body", faq.body);
        contentValues.put(FaqsColumns.HELPFUL, Integer.valueOf(faq.is_helpful));
        contentValues.put(FaqsColumns.RTL, faq.is_rtl);
        contentValues.put(FaqsColumns.TAGS, String.valueOf(new JSONArray((Collection) faq.getTags())));
        contentValues.put(FaqsColumns.CATEGORY_TAGS, String.valueOf(new JSONArray((Collection) faq.getCategoryTags())));
        return contentValues;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:9:0x0024 */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r0v2, types: [com.helpshift.conversation.activeconversation.model.Conversation] */
    /* JADX WARN: Type inference failed for: r0v3 */
    /* JADX WARN: Type inference failed for: r0v4 */
    /* JADX WARN: Type inference failed for: r0v5 */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARN: Type inference failed for: r0v7 */
    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r11.close();
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0037, code lost:
        if (r11 != null) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0044, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0048, code lost:
        throw r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        if (r11 != null) goto L_0x0026;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0040 A[SYNTHETIC, Splitter:B:25:0x0040] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized com.helpshift.conversation.activeconversation.model.Conversation readConversation(java.lang.String r11, java.lang.String[] r12) {
        /*
            r10 = this;
            monitor-enter(r10)
            r0 = 0
            com.helpshift.platform.db.ConversationDBHelper r1 = r10.dbHelper     // Catch:{ Exception -> 0x002e, all -> 0x002c }
            android.database.sqlite.SQLiteDatabase r2 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x002e, all -> 0x002c }
            com.helpshift.common.conversation.ConversationDBInfo r1 = r10.dbInfo     // Catch:{ Exception -> 0x002e, all -> 0x002c }
            r1.getClass()     // Catch:{ Exception -> 0x002e, all -> 0x002c }
            java.lang.String r3 = "issues"
            r4 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r5 = r11
            r6 = r12
            android.database.Cursor r11 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x002e, all -> 0x002c }
            boolean r12 = r11.moveToFirst()     // Catch:{ Exception -> 0x002a }
            if (r12 == 0) goto L_0x0024
            com.helpshift.conversation.activeconversation.model.Conversation r12 = r10.cursorToReadableConversation(r11)     // Catch:{ Exception -> 0x002a }
            r0 = r12
        L_0x0024:
            if (r11 == 0) goto L_0x003a
        L_0x0026:
            r11.close()     // Catch:{ all -> 0x0044 }
            goto L_0x003a
        L_0x002a:
            r12 = move-exception
            goto L_0x0030
        L_0x002c:
            r12 = move-exception
            goto L_0x003e
        L_0x002e:
            r12 = move-exception
            r11 = r0
        L_0x0030:
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in read conversations with localId"
            com.helpshift.util.HSLogger.e(r1, r2, r12)     // Catch:{ all -> 0x003c }
            if (r11 == 0) goto L_0x003a
            goto L_0x0026
        L_0x003a:
            monitor-exit(r10)
            return r0
        L_0x003c:
            r12 = move-exception
            r0 = r11
        L_0x003e:
            if (r0 == 0) goto L_0x0046
            r0.close()     // Catch:{ all -> 0x0044 }
            goto L_0x0046
        L_0x0044:
            r11 = move-exception
            goto L_0x0047
        L_0x0046:
            throw r12     // Catch:{ all -> 0x0044 }
        L_0x0047:
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.conversation.ConversationDB.readConversation(java.lang.String, java.lang.String[]):com.helpshift.conversation.activeconversation.model.Conversation");
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0069 A[SYNTHETIC, Splitter:B:23:0x0069] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0070 A[SYNTHETIC, Splitter:B:28:0x0070] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<com.helpshift.conversation.activeconversation.model.Conversation> readConversationsWithLocalId(long r13) {
        /*
            r12 = this;
            monitor-enter(r12)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0074 }
            r0.<init>()     // Catch:{ all -> 0x0074 }
            r1 = 0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0074 }
            r2.<init>()     // Catch:{ all -> 0x0074 }
            com.helpshift.common.conversation.ConversationDBInfo r3 = r12.dbInfo     // Catch:{ all -> 0x0074 }
            r3.getClass()     // Catch:{ all -> 0x0074 }
            java.lang.String r3 = "user_local_id"
            r2.append(r3)     // Catch:{ all -> 0x0074 }
            java.lang.String r3 = " = ?"
            r2.append(r3)     // Catch:{ all -> 0x0074 }
            java.lang.String r7 = r2.toString()     // Catch:{ all -> 0x0074 }
            r2 = 1
            java.lang.String[] r8 = new java.lang.String[r2]     // Catch:{ all -> 0x0074 }
            r2 = 0
            java.lang.String r13 = java.lang.String.valueOf(r13)     // Catch:{ all -> 0x0074 }
            r8[r2] = r13     // Catch:{ all -> 0x0074 }
            com.helpshift.platform.db.ConversationDBHelper r13 = r12.dbHelper     // Catch:{ Exception -> 0x005f }
            android.database.sqlite.SQLiteDatabase r4 = r13.getReadableDatabase()     // Catch:{ Exception -> 0x005f }
            com.helpshift.common.conversation.ConversationDBInfo r13 = r12.dbInfo     // Catch:{ Exception -> 0x005f }
            r13.getClass()     // Catch:{ Exception -> 0x005f }
            java.lang.String r5 = "issues"
            r6 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            android.database.Cursor r13 = r4.query(r5, r6, r7, r8, r9, r10, r11)     // Catch:{ Exception -> 0x005f }
            boolean r14 = r13.moveToFirst()     // Catch:{ Exception -> 0x005a, all -> 0x0057 }
            if (r14 == 0) goto L_0x0051
        L_0x0044:
            com.helpshift.conversation.activeconversation.model.Conversation r14 = r12.cursorToReadableConversation(r13)     // Catch:{ Exception -> 0x005a, all -> 0x0057 }
            r0.add(r14)     // Catch:{ Exception -> 0x005a, all -> 0x0057 }
            boolean r14 = r13.moveToNext()     // Catch:{ Exception -> 0x005a, all -> 0x0057 }
            if (r14 != 0) goto L_0x0044
        L_0x0051:
            if (r13 == 0) goto L_0x006c
            r13.close()     // Catch:{ all -> 0x0074 }
            goto L_0x006c
        L_0x0057:
            r14 = move-exception
            r1 = r13
            goto L_0x006e
        L_0x005a:
            r14 = move-exception
            r1 = r13
            goto L_0x0060
        L_0x005d:
            r14 = move-exception
            goto L_0x006e
        L_0x005f:
            r14 = move-exception
        L_0x0060:
            java.lang.String r13 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in read conversations with localId"
            com.helpshift.util.HSLogger.e(r13, r2, r14)     // Catch:{ all -> 0x005d }
            if (r1 == 0) goto L_0x006c
            r1.close()     // Catch:{ all -> 0x0074 }
        L_0x006c:
            monitor-exit(r12)
            return r0
        L_0x006e:
            if (r1 == 0) goto L_0x0073
            r1.close()     // Catch:{ all -> 0x0074 }
        L_0x0073:
            throw r14     // Catch:{ all -> 0x0074 }
        L_0x0074:
            r13 = move-exception
            monitor-exit(r12)
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.conversation.ConversationDB.readConversationsWithLocalId(long):java.util.List");
    }

    public synchronized Conversation readConversationWithLocalId(Long l) {
        StringBuilder sb;
        sb = new StringBuilder();
        this.dbInfo.getClass();
        sb.append("_id");
        sb.append(" = ?");
        return readConversation(sb.toString(), new String[]{String.valueOf(l)});
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x008c A[SYNTHETIC, Splitter:B:26:0x008c] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a9 A[SYNTHETIC, Splitter:B:35:0x00a9] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void deleteConversationWithLocalId(long r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c5 }
            r0.<init>()     // Catch:{ all -> 0x00c5 }
            com.helpshift.common.conversation.ConversationDBInfo r1 = r5.dbInfo     // Catch:{ all -> 0x00c5 }
            r1.getClass()     // Catch:{ all -> 0x00c5 }
            java.lang.String r1 = "_id"
            r0.append(r1)     // Catch:{ all -> 0x00c5 }
            java.lang.String r1 = " = ?"
            r0.append(r1)     // Catch:{ all -> 0x00c5 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00c5 }
            r1 = 1
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ all -> 0x00c5 }
            r2 = 0
            java.lang.String r3 = java.lang.String.valueOf(r6)     // Catch:{ all -> 0x00c5 }
            r1[r2] = r3     // Catch:{ all -> 0x00c5 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c5 }
            r2.<init>()     // Catch:{ all -> 0x00c5 }
            com.helpshift.common.conversation.ConversationDBInfo r3 = r5.dbInfo     // Catch:{ all -> 0x00c5 }
            r3.getClass()     // Catch:{ all -> 0x00c5 }
            java.lang.String r3 = "conversation_id"
            r2.append(r3)     // Catch:{ all -> 0x00c5 }
            java.lang.String r3 = " = ?"
            r2.append(r3)     // Catch:{ all -> 0x00c5 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00c5 }
            r3 = 0
            com.helpshift.platform.db.ConversationDBHelper r4 = r5.dbHelper     // Catch:{ Exception -> 0x0082 }
            android.database.sqlite.SQLiteDatabase r4 = r4.getWritableDatabase()     // Catch:{ Exception -> 0x0082 }
            r4.beginTransaction()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            com.helpshift.common.conversation.ConversationDBInfo r3 = r5.dbInfo     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r3.getClass()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            java.lang.String r3 = "issues"
            r4.delete(r3, r0, r1)     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            com.helpshift.common.conversation.ConversationDBInfo r0 = r5.dbInfo     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r0.getClass()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            java.lang.String r0 = "messages"
            r4.delete(r0, r2, r1)     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r4.setTransactionSuccessful()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            if (r4 == 0) goto L_0x00a5
            r4.endTransaction()     // Catch:{ Exception -> 0x0062 }
            goto L_0x00a5
        L_0x0062:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c5 }
            r2.<init>()     // Catch:{ all -> 0x00c5 }
            java.lang.String r3 = "Exception in ending transaction deleteConversationWithLocalId : "
            r2.append(r3)     // Catch:{ all -> 0x00c5 }
            r2.append(r6)     // Catch:{ all -> 0x00c5 }
            java.lang.String r6 = r2.toString()     // Catch:{ all -> 0x00c5 }
        L_0x0076:
            com.helpshift.util.HSLogger.e(r1, r6, r0)     // Catch:{ all -> 0x00c5 }
            goto L_0x00a5
        L_0x007a:
            r0 = move-exception
            goto L_0x00a7
        L_0x007c:
            r0 = move-exception
            r3 = r4
            goto L_0x0083
        L_0x007f:
            r0 = move-exception
            r4 = r3
            goto L_0x00a7
        L_0x0082:
            r0 = move-exception
        L_0x0083:
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in delete conversation with localId"
            com.helpshift.util.HSLogger.e(r1, r2, r0)     // Catch:{ all -> 0x007f }
            if (r3 == 0) goto L_0x00a5
            r3.endTransaction()     // Catch:{ Exception -> 0x0090 }
            goto L_0x00a5
        L_0x0090:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c5 }
            r2.<init>()     // Catch:{ all -> 0x00c5 }
            java.lang.String r3 = "Exception in ending transaction deleteConversationWithLocalId : "
            r2.append(r3)     // Catch:{ all -> 0x00c5 }
            r2.append(r6)     // Catch:{ all -> 0x00c5 }
            java.lang.String r6 = r2.toString()     // Catch:{ all -> 0x00c5 }
            goto L_0x0076
        L_0x00a5:
            monitor-exit(r5)
            return
        L_0x00a7:
            if (r4 == 0) goto L_0x00c4
            r4.endTransaction()     // Catch:{ Exception -> 0x00ad }
            goto L_0x00c4
        L_0x00ad:
            r1 = move-exception
            java.lang.String r2 = "Helpshift_ConverDB"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c5 }
            r3.<init>()     // Catch:{ all -> 0x00c5 }
            java.lang.String r4 = "Exception in ending transaction deleteConversationWithLocalId : "
            r3.append(r4)     // Catch:{ all -> 0x00c5 }
            r3.append(r6)     // Catch:{ all -> 0x00c5 }
            java.lang.String r6 = r3.toString()     // Catch:{ all -> 0x00c5 }
            com.helpshift.util.HSLogger.e(r2, r6, r1)     // Catch:{ all -> 0x00c5 }
        L_0x00c4:
            throw r0     // Catch:{ all -> 0x00c5 }
        L_0x00c5:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.conversation.ConversationDB.deleteConversationWithLocalId(long):void");
    }

    public synchronized Conversation readConversationWithServerId(String str) {
        StringBuilder sb;
        sb = new StringBuilder();
        this.dbInfo.getClass();
        sb.append(PlayerMetaData.KEY_SERVER_ID);
        sb.append(" = ?");
        return readConversation(sb.toString(), new String[]{String.valueOf(str)});
    }

    public synchronized Conversation readPreConversationWithServerId(String str) {
        StringBuilder sb;
        sb = new StringBuilder();
        this.dbInfo.getClass();
        sb.append("pre_conv_server_id");
        sb.append(" = ?");
        return readConversation(sb.toString(), new String[]{String.valueOf(str)});
    }

    public synchronized long insertConversation(Conversation conversation) {
        long j;
        ContentValues readableConversationToContentValues = readableConversationToContentValues(conversation);
        j = -1;
        try {
            SQLiteDatabase writableDatabase = this.dbHelper.getWritableDatabase();
            this.dbInfo.getClass();
            j = writableDatabase.insert("issues", null, readableConversationToContentValues);
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in insert conversation", e);
        }
        return j;
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x007c A[SYNTHETIC, Splitter:B:38:0x007c] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x008a A[SYNTHETIC, Splitter:B:45:0x008a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<java.lang.Long> insertConversations(java.util.List<com.helpshift.conversation.activeconversation.model.Conversation> r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            int r0 = r6.size()     // Catch:{ all -> 0x0097 }
            r1 = 0
            if (r0 != 0) goto L_0x000a
            monitor-exit(r5)
            return r1
        L_0x000a:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0097 }
            r0.<init>()     // Catch:{ all -> 0x0097 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ all -> 0x0097 }
        L_0x0013:
            boolean r2 = r6.hasNext()     // Catch:{ all -> 0x0097 }
            if (r2 == 0) goto L_0x0027
            java.lang.Object r2 = r6.next()     // Catch:{ all -> 0x0097 }
            com.helpshift.conversation.activeconversation.model.Conversation r2 = (com.helpshift.conversation.activeconversation.model.Conversation) r2     // Catch:{ all -> 0x0097 }
            android.content.ContentValues r2 = r5.readableConversationToContentValues(r2)     // Catch:{ all -> 0x0097 }
            r0.add(r2)     // Catch:{ all -> 0x0097 }
            goto L_0x0013
        L_0x0027:
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ all -> 0x0097 }
            r6.<init>()     // Catch:{ all -> 0x0097 }
            com.helpshift.platform.db.ConversationDBHelper r2 = r5.dbHelper     // Catch:{ Exception -> 0x0072 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Exception -> 0x0072 }
            r2.beginTransaction()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
        L_0x0039:
            boolean r3 = r0.hasNext()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            if (r3 == 0) goto L_0x0058
            java.lang.Object r3 = r0.next()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            android.content.ContentValues r3 = (android.content.ContentValues) r3     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            com.helpshift.common.conversation.ConversationDBInfo r4 = r5.dbInfo     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            r4.getClass()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            java.lang.String r4 = "issues"
            long r3 = r2.insert(r4, r1, r3)     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            r6.add(r3)     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            goto L_0x0039
        L_0x0058:
            r2.setTransactionSuccessful()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            if (r2 == 0) goto L_0x0086
            r2.endTransaction()     // Catch:{ Exception -> 0x0061 }
            goto L_0x0086
        L_0x0061:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in insert conversations inside finally block"
        L_0x0066:
            com.helpshift.util.HSLogger.e(r1, r2, r0)     // Catch:{ all -> 0x0097 }
            goto L_0x0086
        L_0x006a:
            r6 = move-exception
            goto L_0x0088
        L_0x006c:
            r0 = move-exception
            r1 = r2
            goto L_0x0073
        L_0x006f:
            r6 = move-exception
            r2 = r1
            goto L_0x0088
        L_0x0072:
            r0 = move-exception
        L_0x0073:
            java.lang.String r2 = "Helpshift_ConverDB"
            java.lang.String r3 = "Error in insert conversations"
            com.helpshift.util.HSLogger.e(r2, r3, r0)     // Catch:{ all -> 0x006f }
            if (r1 == 0) goto L_0x0086
            r1.endTransaction()     // Catch:{ Exception -> 0x0080 }
            goto L_0x0086
        L_0x0080:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in insert conversations inside finally block"
            goto L_0x0066
        L_0x0086:
            monitor-exit(r5)
            return r6
        L_0x0088:
            if (r2 == 0) goto L_0x0096
            r2.endTransaction()     // Catch:{ Exception -> 0x008e }
            goto L_0x0096
        L_0x008e:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in insert conversations inside finally block"
            com.helpshift.util.HSLogger.e(r1, r2, r0)     // Catch:{ all -> 0x0097 }
        L_0x0096:
            throw r6     // Catch:{ all -> 0x0097 }
        L_0x0097:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.conversation.ConversationDB.insertConversations(java.util.List):java.util.List");
    }

    public synchronized void updateConversation(Conversation conversation) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(conversation);
        updateConversations(arrayList);
    }

    public synchronized void updateLastUserActivityTimeInConversation(Long l, long j) {
        ContentValues contentValues = new ContentValues();
        this.dbInfo.getClass();
        contentValues.put("last_user_activity_time", Long.valueOf(j));
        StringBuilder sb = new StringBuilder();
        this.dbInfo.getClass();
        sb.append("_id");
        sb.append(" = ?");
        String sb2 = sb.toString();
        String[] strArr = {String.valueOf(l)};
        try {
            SQLiteDatabase writableDatabase = this.dbHelper.getWritableDatabase();
            this.dbInfo.getClass();
            writableDatabase.update("issues", contentValues, sb2, strArr);
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in updateLastUserActivityTimeInConversation", e);
        }
        return;
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x009f A[SYNTHETIC, Splitter:B:37:0x009f] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00ad A[SYNTHETIC, Splitter:B:44:0x00ad] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void updateConversations(java.util.List<com.helpshift.conversation.activeconversation.model.Conversation> r9) {
        /*
            r8 = this;
            monitor-enter(r8)
            int r0 = r9.size()     // Catch:{ all -> 0x00ba }
            if (r0 != 0) goto L_0x0009
            monitor-exit(r8)
            return
        L_0x0009:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x00ba }
            r0.<init>()     // Catch:{ all -> 0x00ba }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x00ba }
            r1.<init>()     // Catch:{ all -> 0x00ba }
            java.util.Iterator r2 = r9.iterator()     // Catch:{ all -> 0x00ba }
        L_0x0017:
            boolean r3 = r2.hasNext()     // Catch:{ all -> 0x00ba }
            r4 = 0
            if (r3 == 0) goto L_0x003a
            java.lang.Object r3 = r2.next()     // Catch:{ all -> 0x00ba }
            com.helpshift.conversation.activeconversation.model.Conversation r3 = (com.helpshift.conversation.activeconversation.model.Conversation) r3     // Catch:{ all -> 0x00ba }
            android.content.ContentValues r5 = r8.readableConversationToContentValues(r3)     // Catch:{ all -> 0x00ba }
            r0.add(r5)     // Catch:{ all -> 0x00ba }
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ all -> 0x00ba }
            java.lang.Long r3 = r3.localId     // Catch:{ all -> 0x00ba }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ all -> 0x00ba }
            r5[r4] = r3     // Catch:{ all -> 0x00ba }
            r1.add(r5)     // Catch:{ all -> 0x00ba }
            goto L_0x0017
        L_0x003a:
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ba }
            r3.<init>()     // Catch:{ all -> 0x00ba }
            com.helpshift.common.conversation.ConversationDBInfo r5 = r8.dbInfo     // Catch:{ all -> 0x00ba }
            r5.getClass()     // Catch:{ all -> 0x00ba }
            java.lang.String r5 = "_id"
            r3.append(r5)     // Catch:{ all -> 0x00ba }
            java.lang.String r5 = " = ?"
            r3.append(r5)     // Catch:{ all -> 0x00ba }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00ba }
            com.helpshift.platform.db.ConversationDBHelper r5 = r8.dbHelper     // Catch:{ Exception -> 0x0095 }
            android.database.sqlite.SQLiteDatabase r5 = r5.getWritableDatabase()     // Catch:{ Exception -> 0x0095 }
            r5.beginTransaction()     // Catch:{ Exception -> 0x008f, all -> 0x008d }
        L_0x005c:
            int r2 = r9.size()     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            if (r4 >= r2) goto L_0x007b
            com.helpshift.common.conversation.ConversationDBInfo r2 = r8.dbInfo     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            r2.getClass()     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            java.lang.String r2 = "issues"
            java.lang.Object r6 = r0.get(r4)     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            android.content.ContentValues r6 = (android.content.ContentValues) r6     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            java.lang.Object r7 = r1.get(r4)     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            java.lang.String[] r7 = (java.lang.String[]) r7     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            r5.update(r2, r6, r3, r7)     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            int r4 = r4 + 1
            goto L_0x005c
        L_0x007b:
            r5.setTransactionSuccessful()     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            if (r5 == 0) goto L_0x00a9
            r5.endTransaction()     // Catch:{ Exception -> 0x0084 }
            goto L_0x00a9
        L_0x0084:
            r9 = move-exception
            java.lang.String r0 = "Helpshift_ConverDB"
            java.lang.String r1 = "Error in update conversations inside finally block"
        L_0x0089:
            com.helpshift.util.HSLogger.e(r0, r1, r9)     // Catch:{ all -> 0x00ba }
            goto L_0x00a9
        L_0x008d:
            r9 = move-exception
            goto L_0x00ab
        L_0x008f:
            r9 = move-exception
            r2 = r5
            goto L_0x0096
        L_0x0092:
            r9 = move-exception
            r5 = r2
            goto L_0x00ab
        L_0x0095:
            r9 = move-exception
        L_0x0096:
            java.lang.String r0 = "Helpshift_ConverDB"
            java.lang.String r1 = "Error in update conversations"
            com.helpshift.util.HSLogger.e(r0, r1, r9)     // Catch:{ all -> 0x0092 }
            if (r2 == 0) goto L_0x00a9
            r2.endTransaction()     // Catch:{ Exception -> 0x00a3 }
            goto L_0x00a9
        L_0x00a3:
            r9 = move-exception
            java.lang.String r0 = "Helpshift_ConverDB"
            java.lang.String r1 = "Error in update conversations inside finally block"
            goto L_0x0089
        L_0x00a9:
            monitor-exit(r8)
            return
        L_0x00ab:
            if (r5 == 0) goto L_0x00b9
            r5.endTransaction()     // Catch:{ Exception -> 0x00b1 }
            goto L_0x00b9
        L_0x00b1:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in update conversations inside finally block"
            com.helpshift.util.HSLogger.e(r1, r2, r0)     // Catch:{ all -> 0x00ba }
        L_0x00b9:
            throw r9     // Catch:{ all -> 0x00ba }
        L_0x00ba:
            r9 = move-exception
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.conversation.ConversationDB.updateConversations(java.util.List):void");
    }

    public synchronized ConversationInboxRecord storeConversationInboxRecord(ConversationInboxRecord conversationInboxRecord) {
        StringBuilder sb = new StringBuilder();
        this.dbInfo.getClass();
        sb.append("user_local_id");
        sb.append(" = ?");
        String sb2 = sb.toString();
        String[] strArr = {String.valueOf(conversationInboxRecord.userLocalId)};
        ContentValues conversationInboxRecordToContentValues = conversationInboxRecordToContentValues(conversationInboxRecord);
        try {
            SQLiteDatabase writableDatabase = this.dbHelper.getWritableDatabase();
            this.dbInfo.getClass();
            if (exists(writableDatabase, "conversation_inbox", sb2, strArr)) {
                this.dbInfo.getClass();
                writableDatabase.update("conversation_inbox", conversationInboxRecordToContentValues, sb2, strArr);
            } else {
                this.dbInfo.getClass();
                writableDatabase.insert("conversation_inbox", null, conversationInboxRecordToContentValues);
            }
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in store conversation inbox record", e);
        }
        return conversationInboxRecord;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:11:0x0044 */
    /* JADX WARN: Type inference failed for: r12v3 */
    /* JADX WARN: Type inference failed for: r12v4, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r12v5, types: [com.helpshift.conversation.dto.dao.ConversationInboxRecord] */
    /* JADX WARN: Type inference failed for: r12v7 */
    /* JADX WARN: Type inference failed for: r12v8 */
    /* JADX WARN: Type inference failed for: r12v9 */
    /* JADX WARN: Type inference failed for: r12v10 */
    /* JADX WARN: Type inference failed for: r12v11 */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0044, code lost:
        if (r13 != null) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r13.close();
        r12 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0057, code lost:
        if (r13 != null) goto L_0x0046;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0062 A[SYNTHETIC, Splitter:B:27:0x0062] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.helpshift.conversation.dto.dao.ConversationInboxRecord readConversationInboxRecord(long r12) {
        /*
            r11 = this;
            monitor-enter(r11)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0066 }
            r0.<init>()     // Catch:{ all -> 0x0066 }
            com.helpshift.common.conversation.ConversationDBInfo r1 = r11.dbInfo     // Catch:{ all -> 0x0066 }
            r1.getClass()     // Catch:{ all -> 0x0066 }
            java.lang.String r1 = "user_local_id"
            r0.append(r1)     // Catch:{ all -> 0x0066 }
            java.lang.String r1 = " = ?"
            r0.append(r1)     // Catch:{ all -> 0x0066 }
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x0066 }
            r0 = 1
            java.lang.String[] r6 = new java.lang.String[r0]     // Catch:{ all -> 0x0066 }
            r0 = 0
            java.lang.String r12 = java.lang.String.valueOf(r12)     // Catch:{ all -> 0x0066 }
            r6[r0] = r12     // Catch:{ all -> 0x0066 }
            r12 = 0
            com.helpshift.platform.db.ConversationDBHelper r13 = r11.dbHelper     // Catch:{ Exception -> 0x004e, all -> 0x004c }
            android.database.sqlite.SQLiteDatabase r2 = r13.getReadableDatabase()     // Catch:{ Exception -> 0x004e, all -> 0x004c }
            com.helpshift.common.conversation.ConversationDBInfo r13 = r11.dbInfo     // Catch:{ Exception -> 0x004e, all -> 0x004c }
            r13.getClass()     // Catch:{ Exception -> 0x004e, all -> 0x004c }
            java.lang.String r3 = "conversation_inbox"
            r4 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r13 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x004e, all -> 0x004c }
            boolean r0 = r13.moveToFirst()     // Catch:{ Exception -> 0x004a }
            if (r0 == 0) goto L_0x0044
            com.helpshift.conversation.dto.dao.ConversationInboxRecord r0 = r11.cursorToConversationInboxRecord(r13)     // Catch:{ Exception -> 0x004a }
            r12 = r0
        L_0x0044:
            if (r13 == 0) goto L_0x005a
        L_0x0046:
            r13.close()     // Catch:{ all -> 0x0066 }
            goto L_0x005a
        L_0x004a:
            r0 = move-exception
            goto L_0x0050
        L_0x004c:
            r13 = move-exception
            goto L_0x0060
        L_0x004e:
            r0 = move-exception
            r13 = r12
        L_0x0050:
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in read conversation inbox record"
            com.helpshift.util.HSLogger.e(r1, r2, r0)     // Catch:{ all -> 0x005c }
            if (r13 == 0) goto L_0x005a
            goto L_0x0046
        L_0x005a:
            monitor-exit(r11)
            return r12
        L_0x005c:
            r12 = move-exception
            r10 = r13
            r13 = r12
            r12 = r10
        L_0x0060:
            if (r12 == 0) goto L_0x0065
            r12.close()     // Catch:{ all -> 0x0066 }
        L_0x0065:
            throw r13     // Catch:{ all -> 0x0066 }
        L_0x0066:
            r12 = move-exception
            monitor-exit(r11)
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.conversation.ConversationDB.readConversationInboxRecord(long):com.helpshift.conversation.dto.dao.ConversationInboxRecord");
    }

    public synchronized long insertMessage(MessageDM messageDM) {
        long j;
        ContentValues readableMessageToContentValues = readableMessageToContentValues(messageDM);
        j = -1;
        try {
            SQLiteDatabase writableDatabase = this.dbHelper.getWritableDatabase();
            this.dbInfo.getClass();
            j = writableDatabase.insert("messages", null, readableMessageToContentValues);
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in insert message", e);
        }
        return j;
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x007c A[SYNTHETIC, Splitter:B:38:0x007c] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x008a A[SYNTHETIC, Splitter:B:45:0x008a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<java.lang.Long> insertMessages(java.util.List<com.helpshift.conversation.activeconversation.message.MessageDM> r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            int r0 = r6.size()     // Catch:{ all -> 0x0097 }
            r1 = 0
            if (r0 != 0) goto L_0x000a
            monitor-exit(r5)
            return r1
        L_0x000a:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0097 }
            r0.<init>()     // Catch:{ all -> 0x0097 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ all -> 0x0097 }
        L_0x0013:
            boolean r2 = r6.hasNext()     // Catch:{ all -> 0x0097 }
            if (r2 == 0) goto L_0x0027
            java.lang.Object r2 = r6.next()     // Catch:{ all -> 0x0097 }
            com.helpshift.conversation.activeconversation.message.MessageDM r2 = (com.helpshift.conversation.activeconversation.message.MessageDM) r2     // Catch:{ all -> 0x0097 }
            android.content.ContentValues r2 = r5.readableMessageToContentValues(r2)     // Catch:{ all -> 0x0097 }
            r0.add(r2)     // Catch:{ all -> 0x0097 }
            goto L_0x0013
        L_0x0027:
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ all -> 0x0097 }
            r6.<init>()     // Catch:{ all -> 0x0097 }
            com.helpshift.platform.db.ConversationDBHelper r2 = r5.dbHelper     // Catch:{ Exception -> 0x0072 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Exception -> 0x0072 }
            r2.beginTransaction()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
        L_0x0039:
            boolean r3 = r0.hasNext()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            if (r3 == 0) goto L_0x0058
            java.lang.Object r3 = r0.next()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            android.content.ContentValues r3 = (android.content.ContentValues) r3     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            com.helpshift.common.conversation.ConversationDBInfo r4 = r5.dbInfo     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            r4.getClass()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            java.lang.String r4 = "messages"
            long r3 = r2.insert(r4, r1, r3)     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            r6.add(r3)     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            goto L_0x0039
        L_0x0058:
            r2.setTransactionSuccessful()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            if (r2 == 0) goto L_0x0086
            r2.endTransaction()     // Catch:{ Exception -> 0x0061 }
            goto L_0x0086
        L_0x0061:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in insert messages inside finally block"
        L_0x0066:
            com.helpshift.util.HSLogger.e(r1, r2, r0)     // Catch:{ all -> 0x0097 }
            goto L_0x0086
        L_0x006a:
            r6 = move-exception
            goto L_0x0088
        L_0x006c:
            r0 = move-exception
            r1 = r2
            goto L_0x0073
        L_0x006f:
            r6 = move-exception
            r2 = r1
            goto L_0x0088
        L_0x0072:
            r0 = move-exception
        L_0x0073:
            java.lang.String r2 = "Helpshift_ConverDB"
            java.lang.String r3 = "Error in insert messages"
            com.helpshift.util.HSLogger.e(r2, r3, r0)     // Catch:{ all -> 0x006f }
            if (r1 == 0) goto L_0x0086
            r1.endTransaction()     // Catch:{ Exception -> 0x0080 }
            goto L_0x0086
        L_0x0080:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in insert messages inside finally block"
            goto L_0x0066
        L_0x0086:
            monitor-exit(r5)
            return r6
        L_0x0088:
            if (r2 == 0) goto L_0x0096
            r2.endTransaction()     // Catch:{ Exception -> 0x008e }
            goto L_0x0096
        L_0x008e:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in insert messages inside finally block"
            com.helpshift.util.HSLogger.e(r1, r2, r0)     // Catch:{ all -> 0x0097 }
        L_0x0096:
            throw r6     // Catch:{ all -> 0x0097 }
        L_0x0097:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.conversation.ConversationDB.insertMessages(java.util.List):java.util.List");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:26:0x0099 */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r2v2 */
    /* JADX WARN: Type inference failed for: r2v3, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r2v6 */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARN: Type inference failed for: r2v8, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r2v10 */
    /* JADX WARN: Type inference failed for: r2v13 */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a8, code lost:
        r12 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b2, code lost:
        if (r2 == 0) goto L_0x00f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b8, code lost:
        if (r2 != 0) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00ba, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00bd, code lost:
        throw r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00be, code lost:
        if (r2 != 0) goto L_0x00b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00df, code lost:
        r12 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00e9, code lost:
        if (r3 == null) goto L_0x00f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00ef, code lost:
        if (r3 != null) goto L_0x00f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x00f1, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00f4, code lost:
        throw r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x00f5, code lost:
        if (r3 != null) goto L_0x00eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0109, code lost:
        r12 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0113, code lost:
        if (r2 == 0) goto L_0x0122;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0119, code lost:
        if (r2 != 0) goto L_0x011b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x011b, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x011e, code lost:
        throw r12;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:36:0x00af, B:64:0x00e6, B:86:0x0110] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00d5 A[SYNTHETIC, Splitter:B:57:0x00d5] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x00ff A[SYNTHETIC, Splitter:B:79:0x00ff] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0121 A[Catch:{ all -> 0x0109, all -> 0x00df, all -> 0x00a8 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:39:0x00b4=Splitter:B:39:0x00b4, B:89:0x0115=Splitter:B:89:0x0115, B:67:0x00eb=Splitter:B:67:0x00eb} */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<com.helpshift.conversation.activeconversation.message.MessageDM> readMessagesForConversations(java.util.Collection<java.lang.Long> r12) {
        /*
            r11 = this;
            monitor-enter(r11)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0123 }
            r0.<init>()     // Catch:{ all -> 0x0123 }
            r1 = 900(0x384, float:1.261E-42)
            r2 = 0
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x00ca, all -> 0x00c7 }
            r3.<init>(r12)     // Catch:{ Exception -> 0x00ca, all -> 0x00c7 }
            java.util.List r12 = com.helpshift.util.DatabaseUtils.createBatches(r1, r3)     // Catch:{ Exception -> 0x00ca, all -> 0x00c7 }
            com.helpshift.platform.db.ConversationDBHelper r1 = r11.dbHelper     // Catch:{ Exception -> 0x00ca, all -> 0x00c7 }
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x00ca, all -> 0x00c7 }
            r1.beginTransaction()     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            java.util.Iterator r12 = r12.iterator()     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
        L_0x001f:
            boolean r3 = r12.hasNext()     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            if (r3 == 0) goto L_0x0099
            java.lang.Object r3 = r12.next()     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            java.util.List r3 = (java.util.List) r3     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            int r4 = r3.size()     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            java.lang.String r4 = com.helpshift.util.DatabaseUtils.makePlaceholders(r4)     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            r5.<init>()     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            com.helpshift.common.conversation.ConversationDBInfo r6 = r11.dbInfo     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            r6.getClass()     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            java.lang.String r6 = "conversation_id"
            r5.append(r6)     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            java.lang.String r6 = " IN ("
            r5.append(r6)     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            r5.append(r4)     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            java.lang.String r4 = ")"
            r5.append(r4)     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            java.lang.String r6 = r5.toString()     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            int r4 = r3.size()     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            java.lang.String[] r7 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            r4 = 0
        L_0x005a:
            int r5 = r3.size()     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            if (r4 >= r5) goto L_0x006d
            java.lang.Object r5 = r3.get(r4)     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            r7[r4] = r5     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            int r4 = r4 + 1
            goto L_0x005a
        L_0x006d:
            com.helpshift.common.conversation.ConversationDBInfo r3 = r11.dbInfo     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            r3.getClass()     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            java.lang.String r4 = "messages"
            r5 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r3 = r1
            android.database.Cursor r3 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            boolean r2 = r3.moveToFirst()     // Catch:{ Exception -> 0x0097, all -> 0x0094 }
            if (r2 == 0) goto L_0x0092
        L_0x0083:
            com.helpshift.conversation.activeconversation.message.MessageDM r2 = r11.cursorToMessageDM(r3)     // Catch:{ Exception -> 0x0097, all -> 0x0094 }
            if (r2 == 0) goto L_0x008c
            r0.add(r2)     // Catch:{ Exception -> 0x0097, all -> 0x0094 }
        L_0x008c:
            boolean r2 = r3.moveToNext()     // Catch:{ Exception -> 0x0097, all -> 0x0094 }
            if (r2 != 0) goto L_0x0083
        L_0x0092:
            r2 = r3
            goto L_0x001f
        L_0x0094:
            r12 = move-exception
            goto L_0x00fc
        L_0x0097:
            r12 = move-exception
            goto L_0x00c5
        L_0x0099:
            r1.setTransactionSuccessful()     // Catch:{ Exception -> 0x00c3, all -> 0x00c1 }
            if (r1 == 0) goto L_0x00be
            boolean r12 = r1.inTransaction()     // Catch:{ Exception -> 0x00aa }
            if (r12 == 0) goto L_0x00be
            r1.endTransaction()     // Catch:{ Exception -> 0x00aa }
            goto L_0x00be
        L_0x00a8:
            r12 = move-exception
            goto L_0x00b8
        L_0x00aa:
            r12 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r3 = "Error in read messages inside finally block, "
            com.helpshift.util.HSLogger.e(r1, r3, r12)     // Catch:{ all -> 0x00a8 }
            if (r2 == 0) goto L_0x00f8
        L_0x00b4:
            r2.close()     // Catch:{ all -> 0x0123 }
            goto L_0x00f8
        L_0x00b8:
            if (r2 == 0) goto L_0x00bd
            r2.close()     // Catch:{ all -> 0x0123 }
        L_0x00bd:
            throw r12     // Catch:{ all -> 0x0123 }
        L_0x00be:
            if (r2 == 0) goto L_0x00f8
            goto L_0x00b4
        L_0x00c1:
            r12 = move-exception
            goto L_0x00fd
        L_0x00c3:
            r12 = move-exception
            r3 = r2
        L_0x00c5:
            r2 = r1
            goto L_0x00cc
        L_0x00c7:
            r12 = move-exception
            r1 = r2
            goto L_0x00fd
        L_0x00ca:
            r12 = move-exception
            r3 = r2
        L_0x00cc:
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r4 = "Error in read messages"
            com.helpshift.util.HSLogger.e(r1, r4, r12)     // Catch:{ all -> 0x00fa }
            if (r2 == 0) goto L_0x00f5
            boolean r12 = r2.inTransaction()     // Catch:{ Exception -> 0x00e1 }
            if (r12 == 0) goto L_0x00f5
            r2.endTransaction()     // Catch:{ Exception -> 0x00e1 }
            goto L_0x00f5
        L_0x00df:
            r12 = move-exception
            goto L_0x00ef
        L_0x00e1:
            r12 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in read messages inside finally block, "
            com.helpshift.util.HSLogger.e(r1, r2, r12)     // Catch:{ all -> 0x00df }
            if (r3 == 0) goto L_0x00f8
        L_0x00eb:
            r3.close()     // Catch:{ all -> 0x0123 }
            goto L_0x00f8
        L_0x00ef:
            if (r3 == 0) goto L_0x00f4
            r3.close()     // Catch:{ all -> 0x0123 }
        L_0x00f4:
            throw r12     // Catch:{ all -> 0x0123 }
        L_0x00f5:
            if (r3 == 0) goto L_0x00f8
            goto L_0x00eb
        L_0x00f8:
            monitor-exit(r11)
            return r0
        L_0x00fa:
            r12 = move-exception
            r1 = r2
        L_0x00fc:
            r2 = r3
        L_0x00fd:
            if (r1 == 0) goto L_0x011f
            boolean r0 = r1.inTransaction()     // Catch:{ Exception -> 0x010b }
            if (r0 == 0) goto L_0x011f
            r1.endTransaction()     // Catch:{ Exception -> 0x010b }
            goto L_0x011f
        L_0x0109:
            r12 = move-exception
            goto L_0x0119
        L_0x010b:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r3 = "Error in read messages inside finally block, "
            com.helpshift.util.HSLogger.e(r1, r3, r0)     // Catch:{ all -> 0x0109 }
            if (r2 == 0) goto L_0x0122
        L_0x0115:
            r2.close()     // Catch:{ all -> 0x0123 }
            goto L_0x0122
        L_0x0119:
            if (r2 == 0) goto L_0x011e
            r2.close()     // Catch:{ all -> 0x0123 }
        L_0x011e:
            throw r12     // Catch:{ all -> 0x0123 }
        L_0x011f:
            if (r2 == 0) goto L_0x0122
            goto L_0x0115
        L_0x0122:
            throw r12     // Catch:{ all -> 0x0123 }
        L_0x0123:
            r12 = move-exception
            monitor-exit(r11)
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.conversation.ConversationDB.readMessagesForConversations(java.util.Collection):java.util.List");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:28:0x0137 */
    /* JADX WARN: Type inference failed for: r2v1 */
    /* JADX WARN: Type inference failed for: r2v2, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v4, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r2v6 */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARN: Type inference failed for: r2v8, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r2v10 */
    /* JADX WARN: Type inference failed for: r2v21 */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0146, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0150, code lost:
        if (r2 == 0) goto L_0x0196;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0156, code lost:
        if (r2 != 0) goto L_0x0158;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0158, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x015b, code lost:
        throw r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x015c, code lost:
        if (r2 != 0) goto L_0x0152;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x017d, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0187, code lost:
        if (r4 == null) goto L_0x0196;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x018d, code lost:
        if (r4 != null) goto L_0x018f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x018f, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0192, code lost:
        throw r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0193, code lost:
        if (r4 != null) goto L_0x0189;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01a7, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01b1, code lost:
        if (r2 == 0) goto L_0x01c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01b7, code lost:
        if (r2 != 0) goto L_0x01b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01b9, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01bc, code lost:
        throw r14;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:38:0x014d, B:66:0x0184, B:88:0x01ae] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0173 A[SYNTHETIC, Splitter:B:59:0x0173] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x019d A[SYNTHETIC, Splitter:B:81:0x019d] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x01bf A[Catch:{ all -> 0x01a7, all -> 0x017d, all -> 0x0146 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:41:0x0152=Splitter:B:41:0x0152, B:91:0x01b3=Splitter:B:91:0x01b3, B:69:0x0189=Splitter:B:69:0x0189} */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.Map<java.lang.Long, java.lang.Integer> getMessagesCountForConversations(java.util.List<java.lang.Long> r14, java.lang.String[] r15) {
        /*
            r13 = this;
            monitor-enter(r13)
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ all -> 0x01c1 }
            r0.<init>()     // Catch:{ all -> 0x01c1 }
            java.util.Iterator r1 = r14.iterator()     // Catch:{ all -> 0x01c1 }
        L_0x000a:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x01c1 }
            r3 = 0
            if (r2 == 0) goto L_0x001f
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x01c1 }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ all -> 0x01c1 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x01c1 }
            r0.put(r2, r3)     // Catch:{ all -> 0x01c1 }
            goto L_0x000a
        L_0x001f:
            r1 = 900(0x384, float:1.261E-42)
            r2 = 0
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ Exception -> 0x0168, all -> 0x0165 }
            r4.<init>(r14)     // Catch:{ Exception -> 0x0168, all -> 0x0165 }
            java.util.List r14 = com.helpshift.util.DatabaseUtils.createBatches(r1, r4)     // Catch:{ Exception -> 0x0168, all -> 0x0165 }
            com.helpshift.platform.db.ConversationDBHelper r1 = r13.dbHelper     // Catch:{ Exception -> 0x0168, all -> 0x0165 }
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x0168, all -> 0x0165 }
            r1.beginTransaction()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.util.Iterator r14 = r14.iterator()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
        L_0x0038:
            boolean r4 = r14.hasNext()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            if (r4 == 0) goto L_0x0137
            java.lang.Object r4 = r14.next()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.util.List r4 = (java.util.List) r4     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            int r5 = r4.size()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r5 = com.helpshift.util.DatabaseUtils.makePlaceholders(r5)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r6.<init>()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r7.<init>()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            com.helpshift.common.conversation.ConversationDBInfo r8 = r13.dbInfo     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r8.getClass()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r8 = "conversation_id"
            r7.append(r8)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r8 = " IN ("
            r7.append(r8)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r7.append(r5)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r5 = ")"
            r7.append(r5)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r5 = r7.toString()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r6.append(r5)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r5.<init>()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
        L_0x007d:
            boolean r7 = r4.hasNext()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            if (r7 == 0) goto L_0x0091
            java.lang.Object r7 = r4.next()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.Long r7 = (java.lang.Long) r7     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r5.add(r7)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            goto L_0x007d
        L_0x0091:
            if (r15 == 0) goto L_0x00c7
            int r4 = r15.length     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r4 = com.helpshift.util.DatabaseUtils.makePlaceholders(r4)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r7.<init>()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            com.helpshift.common.conversation.ConversationDBInfo r8 = r13.dbInfo     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r8.getClass()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r8 = "type"
            r7.append(r8)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r8 = " IN ("
            r7.append(r8)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r7.append(r4)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r4 = ")"
            r7.append(r4)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r4 = r7.toString()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r7 = " AND "
            r6.append(r7)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r6.append(r4)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.util.List r4 = java.util.Arrays.asList(r15)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r5.addAll(r4)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
        L_0x00c7:
            int r4 = r5.size()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String[] r8 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r5.toArray(r8)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            com.helpshift.common.conversation.ConversationDBInfo r4 = r13.dbInfo     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r4.getClass()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r5 = "messages"
            r4 = 2
            java.lang.String[] r7 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r4 = "COUNT(*) AS COUNT"
            r7[r3] = r4     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r4 = 1
            com.helpshift.common.conversation.ConversationDBInfo r9 = r13.dbInfo     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r9.getClass()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r9 = "conversation_id"
            r7[r4] = r9     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r9 = r6.toString()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            com.helpshift.common.conversation.ConversationDBInfo r4 = r13.dbInfo     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            r4.getClass()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            java.lang.String r10 = "conversation_id"
            r11 = 0
            r12 = 0
            r4 = r1
            r6 = r7
            r7 = r9
            r9 = r10
            r10 = r11
            r11 = r12
            android.database.Cursor r4 = r4.query(r5, r6, r7, r8, r9, r10, r11)     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            boolean r2 = r4.moveToFirst()     // Catch:{ Exception -> 0x0135, all -> 0x0132 }
            if (r2 == 0) goto L_0x012f
        L_0x0105:
            com.helpshift.common.conversation.ConversationDBInfo r2 = r13.dbInfo     // Catch:{ Exception -> 0x0135, all -> 0x0132 }
            r2.getClass()     // Catch:{ Exception -> 0x0135, all -> 0x0132 }
            java.lang.String r2 = "conversation_id"
            int r2 = r4.getColumnIndex(r2)     // Catch:{ Exception -> 0x0135, all -> 0x0132 }
            long r5 = r4.getLong(r2)     // Catch:{ Exception -> 0x0135, all -> 0x0132 }
            java.lang.String r2 = "COUNT"
            int r2 = r4.getColumnIndex(r2)     // Catch:{ Exception -> 0x0135, all -> 0x0132 }
            int r2 = r4.getInt(r2)     // Catch:{ Exception -> 0x0135, all -> 0x0132 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ Exception -> 0x0135, all -> 0x0132 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0135, all -> 0x0132 }
            r0.put(r5, r2)     // Catch:{ Exception -> 0x0135, all -> 0x0132 }
            boolean r2 = r4.moveToNext()     // Catch:{ Exception -> 0x0135, all -> 0x0132 }
            if (r2 != 0) goto L_0x0105
        L_0x012f:
            r2 = r4
            goto L_0x0038
        L_0x0132:
            r14 = move-exception
            goto L_0x019a
        L_0x0135:
            r14 = move-exception
            goto L_0x0163
        L_0x0137:
            r1.setTransactionSuccessful()     // Catch:{ Exception -> 0x0161, all -> 0x015f }
            if (r1 == 0) goto L_0x015c
            boolean r14 = r1.inTransaction()     // Catch:{ Exception -> 0x0148 }
            if (r14 == 0) goto L_0x015c
            r1.endTransaction()     // Catch:{ Exception -> 0x0148 }
            goto L_0x015c
        L_0x0146:
            r14 = move-exception
            goto L_0x0156
        L_0x0148:
            r14 = move-exception
            java.lang.String r15 = "Helpshift_ConverDB"
            java.lang.String r1 = "Error in get messages count inside finally block, "
            com.helpshift.util.HSLogger.e(r15, r1, r14)     // Catch:{ all -> 0x0146 }
            if (r2 == 0) goto L_0x0196
        L_0x0152:
            r2.close()     // Catch:{ all -> 0x01c1 }
            goto L_0x0196
        L_0x0156:
            if (r2 == 0) goto L_0x015b
            r2.close()     // Catch:{ all -> 0x01c1 }
        L_0x015b:
            throw r14     // Catch:{ all -> 0x01c1 }
        L_0x015c:
            if (r2 == 0) goto L_0x0196
            goto L_0x0152
        L_0x015f:
            r14 = move-exception
            goto L_0x019b
        L_0x0161:
            r14 = move-exception
            r4 = r2
        L_0x0163:
            r2 = r1
            goto L_0x016a
        L_0x0165:
            r14 = move-exception
            r1 = r2
            goto L_0x019b
        L_0x0168:
            r14 = move-exception
            r4 = r2
        L_0x016a:
            java.lang.String r15 = "Helpshift_ConverDB"
            java.lang.String r1 = "Error in get messages count"
            com.helpshift.util.HSLogger.e(r15, r1, r14)     // Catch:{ all -> 0x0198 }
            if (r2 == 0) goto L_0x0193
            boolean r14 = r2.inTransaction()     // Catch:{ Exception -> 0x017f }
            if (r14 == 0) goto L_0x0193
            r2.endTransaction()     // Catch:{ Exception -> 0x017f }
            goto L_0x0193
        L_0x017d:
            r14 = move-exception
            goto L_0x018d
        L_0x017f:
            r14 = move-exception
            java.lang.String r15 = "Helpshift_ConverDB"
            java.lang.String r1 = "Error in get messages count inside finally block, "
            com.helpshift.util.HSLogger.e(r15, r1, r14)     // Catch:{ all -> 0x017d }
            if (r4 == 0) goto L_0x0196
        L_0x0189:
            r4.close()     // Catch:{ all -> 0x01c1 }
            goto L_0x0196
        L_0x018d:
            if (r4 == 0) goto L_0x0192
            r4.close()     // Catch:{ all -> 0x01c1 }
        L_0x0192:
            throw r14     // Catch:{ all -> 0x01c1 }
        L_0x0193:
            if (r4 == 0) goto L_0x0196
            goto L_0x0189
        L_0x0196:
            monitor-exit(r13)
            return r0
        L_0x0198:
            r14 = move-exception
            r1 = r2
        L_0x019a:
            r2 = r4
        L_0x019b:
            if (r1 == 0) goto L_0x01bd
            boolean r15 = r1.inTransaction()     // Catch:{ Exception -> 0x01a9 }
            if (r15 == 0) goto L_0x01bd
            r1.endTransaction()     // Catch:{ Exception -> 0x01a9 }
            goto L_0x01bd
        L_0x01a7:
            r14 = move-exception
            goto L_0x01b7
        L_0x01a9:
            r15 = move-exception
            java.lang.String r0 = "Helpshift_ConverDB"
            java.lang.String r1 = "Error in get messages count inside finally block, "
            com.helpshift.util.HSLogger.e(r0, r1, r15)     // Catch:{ all -> 0x01a7 }
            if (r2 == 0) goto L_0x01c0
        L_0x01b3:
            r2.close()     // Catch:{ all -> 0x01c1 }
            goto L_0x01c0
        L_0x01b7:
            if (r2 == 0) goto L_0x01bc
            r2.close()     // Catch:{ all -> 0x01c1 }
        L_0x01bc:
            throw r14     // Catch:{ all -> 0x01c1 }
        L_0x01bd:
            if (r2 == 0) goto L_0x01c0
            goto L_0x01b3
        L_0x01c0:
            throw r14     // Catch:{ all -> 0x01c1 }
        L_0x01c1:
            r14 = move-exception
            monitor-exit(r13)
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.conversation.ConversationDB.getMessagesCountForConversations(java.util.List, java.lang.String[]):java.util.Map");
    }

    public synchronized List<MessageDM> readMessages(long j) {
        StringBuilder sb;
        sb = new StringBuilder();
        this.dbInfo.getClass();
        sb.append("conversation_id");
        sb.append(" = ?");
        return readMessages(sb.toString(), new String[]{String.valueOf(j)});
    }

    public synchronized List<MessageDM> readMessages(long j, MessageType messageType) {
        StringBuilder sb;
        sb = new StringBuilder();
        this.dbInfo.getClass();
        sb.append("conversation_id");
        sb.append(" = ? AND ");
        this.dbInfo.getClass();
        sb.append("type");
        sb.append(" = ?");
        return readMessages(sb.toString(), new String[]{String.valueOf(j), messageType.getValue()});
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0050  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List<com.helpshift.conversation.activeconversation.message.MessageDM> readMessages(java.lang.String r12, java.lang.String[] r13) {
        /*
            r11 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1 = 0
            com.helpshift.platform.db.ConversationDBHelper r2 = r11.dbHelper     // Catch:{ Exception -> 0x0040 }
            android.database.sqlite.SQLiteDatabase r3 = r2.getReadableDatabase()     // Catch:{ Exception -> 0x0040 }
            com.helpshift.common.conversation.ConversationDBInfo r2 = r11.dbInfo     // Catch:{ Exception -> 0x0040 }
            r2.getClass()     // Catch:{ Exception -> 0x0040 }
            java.lang.String r4 = "messages"
            r5 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r6 = r12
            r7 = r13
            android.database.Cursor r12 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x0040 }
            boolean r13 = r12.moveToFirst()     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            if (r13 == 0) goto L_0x0032
        L_0x0023:
            com.helpshift.conversation.activeconversation.message.MessageDM r13 = r11.cursorToMessageDM(r12)     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            if (r13 == 0) goto L_0x002c
            r0.add(r13)     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
        L_0x002c:
            boolean r13 = r12.moveToNext()     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            if (r13 != 0) goto L_0x0023
        L_0x0032:
            if (r12 == 0) goto L_0x004d
            r12.close()
            goto L_0x004d
        L_0x0038:
            r13 = move-exception
            r1 = r12
            goto L_0x004e
        L_0x003b:
            r13 = move-exception
            r1 = r12
            goto L_0x0041
        L_0x003e:
            r13 = move-exception
            goto L_0x004e
        L_0x0040:
            r13 = move-exception
        L_0x0041:
            java.lang.String r12 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in read messages"
            com.helpshift.util.HSLogger.e(r12, r2, r13)     // Catch:{ all -> 0x003e }
            if (r1 == 0) goto L_0x004d
            r1.close()
        L_0x004d:
            return r0
        L_0x004e:
            if (r1 == 0) goto L_0x0053
            r1.close()
        L_0x0053:
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.conversation.ConversationDB.readMessages(java.lang.String, java.lang.String[]):java.util.List");
    }

    public synchronized void updateMessage(MessageDM messageDM) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(messageDM);
        updateMessages(arrayList);
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x009f A[SYNTHETIC, Splitter:B:37:0x009f] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00ad A[SYNTHETIC, Splitter:B:44:0x00ad] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void updateMessages(java.util.List<com.helpshift.conversation.activeconversation.message.MessageDM> r9) {
        /*
            r8 = this;
            monitor-enter(r8)
            int r0 = r9.size()     // Catch:{ all -> 0x00ba }
            if (r0 != 0) goto L_0x0009
            monitor-exit(r8)
            return
        L_0x0009:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x00ba }
            r0.<init>()     // Catch:{ all -> 0x00ba }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x00ba }
            r1.<init>()     // Catch:{ all -> 0x00ba }
            java.util.Iterator r2 = r9.iterator()     // Catch:{ all -> 0x00ba }
        L_0x0017:
            boolean r3 = r2.hasNext()     // Catch:{ all -> 0x00ba }
            r4 = 0
            if (r3 == 0) goto L_0x003a
            java.lang.Object r3 = r2.next()     // Catch:{ all -> 0x00ba }
            com.helpshift.conversation.activeconversation.message.MessageDM r3 = (com.helpshift.conversation.activeconversation.message.MessageDM) r3     // Catch:{ all -> 0x00ba }
            android.content.ContentValues r5 = r8.readableMessageToContentValues(r3)     // Catch:{ all -> 0x00ba }
            r0.add(r5)     // Catch:{ all -> 0x00ba }
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ all -> 0x00ba }
            java.lang.Long r3 = r3.localId     // Catch:{ all -> 0x00ba }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ all -> 0x00ba }
            r5[r4] = r3     // Catch:{ all -> 0x00ba }
            r1.add(r5)     // Catch:{ all -> 0x00ba }
            goto L_0x0017
        L_0x003a:
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ba }
            r3.<init>()     // Catch:{ all -> 0x00ba }
            com.helpshift.common.conversation.ConversationDBInfo r5 = r8.dbInfo     // Catch:{ all -> 0x00ba }
            r5.getClass()     // Catch:{ all -> 0x00ba }
            java.lang.String r5 = "_id"
            r3.append(r5)     // Catch:{ all -> 0x00ba }
            java.lang.String r5 = " = ?"
            r3.append(r5)     // Catch:{ all -> 0x00ba }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00ba }
            com.helpshift.platform.db.ConversationDBHelper r5 = r8.dbHelper     // Catch:{ Exception -> 0x0095 }
            android.database.sqlite.SQLiteDatabase r5 = r5.getWritableDatabase()     // Catch:{ Exception -> 0x0095 }
            r5.beginTransaction()     // Catch:{ Exception -> 0x008f, all -> 0x008d }
        L_0x005c:
            int r2 = r9.size()     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            if (r4 >= r2) goto L_0x007b
            com.helpshift.common.conversation.ConversationDBInfo r2 = r8.dbInfo     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            r2.getClass()     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            java.lang.String r2 = "messages"
            java.lang.Object r6 = r0.get(r4)     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            android.content.ContentValues r6 = (android.content.ContentValues) r6     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            java.lang.Object r7 = r1.get(r4)     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            java.lang.String[] r7 = (java.lang.String[]) r7     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            r5.update(r2, r6, r3, r7)     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            int r4 = r4 + 1
            goto L_0x005c
        L_0x007b:
            r5.setTransactionSuccessful()     // Catch:{ Exception -> 0x008f, all -> 0x008d }
            if (r5 == 0) goto L_0x00a9
            r5.endTransaction()     // Catch:{ Exception -> 0x0084 }
            goto L_0x00a9
        L_0x0084:
            r9 = move-exception
            java.lang.String r0 = "Helpshift_ConverDB"
            java.lang.String r1 = "Error in update messages"
        L_0x0089:
            com.helpshift.util.HSLogger.e(r0, r1, r9)     // Catch:{ all -> 0x00ba }
            goto L_0x00a9
        L_0x008d:
            r9 = move-exception
            goto L_0x00ab
        L_0x008f:
            r9 = move-exception
            r2 = r5
            goto L_0x0096
        L_0x0092:
            r9 = move-exception
            r5 = r2
            goto L_0x00ab
        L_0x0095:
            r9 = move-exception
        L_0x0096:
            java.lang.String r0 = "Helpshift_ConverDB"
            java.lang.String r1 = "Error in update messages"
            com.helpshift.util.HSLogger.e(r0, r1, r9)     // Catch:{ all -> 0x0092 }
            if (r2 == 0) goto L_0x00a9
            r2.endTransaction()     // Catch:{ Exception -> 0x00a3 }
            goto L_0x00a9
        L_0x00a3:
            r9 = move-exception
            java.lang.String r0 = "Helpshift_ConverDB"
            java.lang.String r1 = "Error in update messages"
            goto L_0x0089
        L_0x00a9:
            monitor-exit(r8)
            return
        L_0x00ab:
            if (r5 == 0) goto L_0x00b9
            r5.endTransaction()     // Catch:{ Exception -> 0x00b1 }
            goto L_0x00b9
        L_0x00b1:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in update messages"
            com.helpshift.util.HSLogger.e(r1, r2, r0)     // Catch:{ all -> 0x00ba }
        L_0x00b9:
            throw r9     // Catch:{ all -> 0x00ba }
        L_0x00ba:
            r9 = move-exception
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.conversation.ConversationDB.updateMessages(java.util.List):void");
    }

    public synchronized boolean deleteMessagesForConversation(long j) {
        StringBuilder sb = new StringBuilder();
        this.dbInfo.getClass();
        sb.append("conversation_id");
        sb.append("= ? ");
        String sb2 = sb.toString();
        String[] strArr = {String.valueOf(j)};
        try {
            SQLiteDatabase writableDatabase = this.dbHelper.getWritableDatabase();
            this.dbInfo.getClass();
            writableDatabase.delete("messages", sb2, strArr);
        } catch (Exception e) {
            HSLogger.e(TAG, "Error deleting messages for : " + j, e);
            return false;
        }
        return true;
    }

    private boolean exists(SQLiteDatabase sQLiteDatabase, String str, String str2, String[] strArr) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT COUNT(*) FROM ");
        sb.append(str);
        sb.append(" WHERE ");
        sb.append(str2);
        sb.append(" LIMIT 1");
        return DatabaseUtils.longForQuery(sQLiteDatabase, sb.toString(), strArr) > 0;
    }

    private ConversationInboxRecord cursorToConversationInboxRecord(Cursor cursor) {
        Cursor cursor2 = cursor;
        this.dbInfo.getClass();
        long j = cursor2.getLong(cursor2.getColumnIndex("user_local_id"));
        this.dbInfo.getClass();
        String string = cursor2.getString(cursor2.getColumnIndex("form_name"));
        this.dbInfo.getClass();
        String string2 = cursor2.getString(cursor2.getColumnIndex("form_email"));
        this.dbInfo.getClass();
        String string3 = cursor2.getString(cursor2.getColumnIndex("description_draft"));
        this.dbInfo.getClass();
        long j2 = cursor2.getLong(cursor2.getColumnIndex("description_draft_timestamp"));
        this.dbInfo.getClass();
        ImagePickerFile parseAndGetImageAttachmentDraft = parseAndGetImageAttachmentDraft(cursor2.getString(cursor2.getColumnIndex("attachment_draft")));
        this.dbInfo.getClass();
        int i = cursor2.getInt(cursor2.getColumnIndex("description_type"));
        this.dbInfo.getClass();
        String string4 = cursor2.getString(cursor2.getColumnIndex("archival_text"));
        this.dbInfo.getClass();
        String string5 = cursor2.getString(cursor2.getColumnIndex("reply_text"));
        this.dbInfo.getClass();
        boolean z = cursor2.getInt(cursor2.getColumnIndex("persist_message_box")) == 1;
        this.dbInfo.getClass();
        String string6 = cursor2.getString(cursor2.getColumnIndex("since"));
        this.dbInfo.getClass();
        Boolean parseBooleanColumnSafe = com.helpshift.util.DatabaseUtils.parseBooleanColumnSafe(cursor2, "has_older_messages");
        this.dbInfo.getClass();
        return new ConversationInboxRecord(j, string, string2, string3, j2, parseAndGetImageAttachmentDraft, i, string4, string5, z, string6, parseBooleanColumnSafe, (Long) com.helpshift.util.DatabaseUtils.parseColumnSafe(cursor2, "last_conv_redaction_time", Long.class));
    }

    private ContentValues conversationInboxRecordToContentValues(ConversationInboxRecord conversationInboxRecord) {
        ContentValues contentValues = new ContentValues();
        this.dbInfo.getClass();
        contentValues.put("user_local_id", Long.valueOf(conversationInboxRecord.userLocalId));
        this.dbInfo.getClass();
        contentValues.put("form_name", conversationInboxRecord.formName);
        this.dbInfo.getClass();
        contentValues.put("form_email", conversationInboxRecord.formEmail);
        this.dbInfo.getClass();
        contentValues.put("description_draft", conversationInboxRecord.description);
        this.dbInfo.getClass();
        contentValues.put("description_draft_timestamp", Long.valueOf(conversationInboxRecord.descriptionTimeStamp));
        this.dbInfo.getClass();
        contentValues.put("description_type", Integer.valueOf(conversationInboxRecord.descriptionType));
        this.dbInfo.getClass();
        contentValues.put("archival_text", conversationInboxRecord.archivalText);
        this.dbInfo.getClass();
        contentValues.put("reply_text", conversationInboxRecord.replyText);
        this.dbInfo.getClass();
        contentValues.put("persist_message_box", Integer.valueOf(conversationInboxRecord.persistMessageBox ? 1 : 0));
        this.dbInfo.getClass();
        contentValues.put("since", conversationInboxRecord.lastSyncTimestamp);
        if (conversationInboxRecord.hasOlderMessages != null) {
            this.dbInfo.getClass();
            contentValues.put("has_older_messages", Integer.valueOf(conversationInboxRecord.hasOlderMessages.booleanValue() ? 1 : 0));
        }
        this.dbInfo.getClass();
        contentValues.put("last_conv_redaction_time", conversationInboxRecord.lastConversationsRedactionTime);
        try {
            String imageAttachmentDraftMeta = getImageAttachmentDraftMeta(conversationInboxRecord.imageAttachmentDraft);
            this.dbInfo.getClass();
            contentValues.put("attachment_draft", imageAttachmentDraftMeta);
        } catch (JSONException e) {
            HSLogger.e(TAG, "Error in generating meta string for image attachment", e);
        }
        return contentValues;
    }

    private ContentValues readableConversationToContentValues(Conversation conversation) {
        ContentValues contentValues = new ContentValues();
        this.dbInfo.getClass();
        contentValues.put("user_local_id", Long.valueOf(conversation.userLocalId));
        this.dbInfo.getClass();
        contentValues.put(PlayerMetaData.KEY_SERVER_ID, conversation.serverId);
        this.dbInfo.getClass();
        contentValues.put("pre_conv_server_id", conversation.preConversationServerId);
        this.dbInfo.getClass();
        contentValues.put("publish_id", conversation.publishId);
        this.dbInfo.getClass();
        contentValues.put("uuid", conversation.localUUID);
        this.dbInfo.getClass();
        contentValues.put("title", conversation.title);
        this.dbInfo.getClass();
        contentValues.put("message_cursor", conversation.messageCursor);
        this.dbInfo.getClass();
        contentValues.put("show_agent_name", Integer.valueOf(conversation.showAgentName ? 1 : 0));
        this.dbInfo.getClass();
        contentValues.put("start_new_conversation_action", Integer.valueOf(conversation.isStartNewConversationClicked ? 1 : 0));
        this.dbInfo.getClass();
        contentValues.put("created_at", conversation.getCreatedAt());
        this.dbInfo.getClass();
        contentValues.put("updated_at", conversation.updatedAt);
        this.dbInfo.getClass();
        contentValues.put("epoch_time_created_at", Long.valueOf(conversation.getEpochCreatedAtTime()));
        this.dbInfo.getClass();
        contentValues.put("last_user_activity_time", Long.valueOf(conversation.lastUserActivityTime));
        this.dbInfo.getClass();
        contentValues.put("issue_type", conversation.issueType);
        this.dbInfo.getClass();
        contentValues.put("full_privacy_enabled", Integer.valueOf(conversation.wasFullPrivacyEnabledAtCreation ? 1 : 0));
        this.dbInfo.getClass();
        contentValues.put("state", Integer.valueOf(conversation.state == null ? -1 : conversation.state.getValue()));
        this.dbInfo.getClass();
        contentValues.put("is_redacted", Integer.valueOf(conversation.isRedacted ? 1 : 0));
        try {
            String conversationMeta = getConversationMeta(conversation);
            this.dbInfo.getClass();
            contentValues.put("meta", conversationMeta);
        } catch (JSONException e) {
            HSLogger.e(TAG, "Error in generating meta string for conversation", e);
        }
        return contentValues;
    }

    private String getImageAttachmentDraftMeta(ImagePickerFile imagePickerFile) throws JSONException {
        if (imagePickerFile == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("image_draft_orig_name", imagePickerFile.originalFileName);
        jSONObject.put("image_draft_orig_size", imagePickerFile.originalFileSize);
        jSONObject.put("image_draft_file_path", imagePickerFile.filePath);
        jSONObject.put("image_copy_done", imagePickerFile.isFileCompressionAndCopyingDone);
        return jSONObject.toString();
    }

    private String getConversationMeta(Conversation conversation) throws JSONException {
        ConversationCSATState conversationCSATState = conversation.csatState;
        JSONObject jSONObject = new JSONObject();
        String str = conversation.csatFeedback;
        int i = conversation.csatRating;
        jSONObject.put("csat_feedback", str);
        jSONObject.put("csat_rating", i);
        jSONObject.put("csat_state", conversationCSATState.getValue());
        jSONObject.put("increment_message_count", conversation.shouldIncrementMessageCount);
        jSONObject.put("ended_delegate_sent", conversation.isConversationEndedDelegateSent);
        jSONObject.put("is_autofilled_preissue", conversation.isAutoFilledPreIssue);
        return jSONObject.toString();
    }

    private Conversation cursorToReadableConversation(Cursor cursor) {
        Cursor cursor2 = cursor;
        this.dbInfo.getClass();
        Long valueOf = Long.valueOf(cursor2.getLong(cursor2.getColumnIndex("_id")));
        this.dbInfo.getClass();
        long j = cursor2.getLong(cursor2.getColumnIndex("user_local_id"));
        this.dbInfo.getClass();
        String string = cursor2.getString(cursor2.getColumnIndex(PlayerMetaData.KEY_SERVER_ID));
        this.dbInfo.getClass();
        String string2 = cursor2.getString(cursor2.getColumnIndex("publish_id"));
        this.dbInfo.getClass();
        String string3 = cursor2.getString(cursor2.getColumnIndex("uuid"));
        this.dbInfo.getClass();
        String string4 = cursor2.getString(cursor2.getColumnIndex("title"));
        this.dbInfo.getClass();
        boolean z = cursor2.getInt(cursor2.getColumnIndex("show_agent_name")) == 1;
        this.dbInfo.getClass();
        String string5 = cursor2.getString(cursor2.getColumnIndex("message_cursor"));
        this.dbInfo.getClass();
        boolean z2 = cursor2.getInt(cursor2.getColumnIndex("start_new_conversation_action")) == 1;
        this.dbInfo.getClass();
        String string6 = cursor2.getString(cursor2.getColumnIndex("meta"));
        this.dbInfo.getClass();
        String string7 = cursor2.getString(cursor2.getColumnIndex("created_at"));
        this.dbInfo.getClass();
        long j2 = cursor2.getLong(cursor2.getColumnIndex("epoch_time_created_at"));
        this.dbInfo.getClass();
        String string8 = cursor2.getString(cursor2.getColumnIndex("updated_at"));
        this.dbInfo.getClass();
        String string9 = cursor2.getString(cursor2.getColumnIndex("pre_conv_server_id"));
        this.dbInfo.getClass();
        String str = string7;
        long j3 = cursor2.getLong(cursor2.getColumnIndex("last_user_activity_time"));
        this.dbInfo.getClass();
        String string10 = cursor2.getString(cursor2.getColumnIndex("issue_type"));
        this.dbInfo.getClass();
        boolean parseBooleanColumnSafe = com.helpshift.util.DatabaseUtils.parseBooleanColumnSafe(cursor2, "full_privacy_enabled", false);
        this.dbInfo.getClass();
        IssueState fromInt = IssueState.fromInt(cursor2.getInt(cursor2.getColumnIndex("state")));
        this.dbInfo.getClass();
        IssueState issueState = fromInt;
        boolean parseBooleanColumnSafe2 = com.helpshift.util.DatabaseUtils.parseBooleanColumnSafe(cursor2, "is_redacted", false);
        Conversation conversation = r7;
        Conversation conversation2 = new Conversation(string4, issueState, str, j2, string8, string2, string5, z, string10);
        conversation.serverId = string;
        conversation.preConversationServerId = string9;
        conversation.setLocalId(valueOf.longValue());
        conversation.localUUID = string3;
        conversation.state = issueState;
        conversation.userLocalId = j;
        conversation.isStartNewConversationClicked = z2;
        conversation.lastUserActivityTime = j3;
        conversation.wasFullPrivacyEnabledAtCreation = parseBooleanColumnSafe;
        conversation.isRedacted = parseBooleanColumnSafe2;
        parseAndSetMetaData(conversation, string6);
        return conversation;
    }

    private ContentValues readableMessageToContentValues(MessageDM messageDM) {
        ContentValues contentValues = new ContentValues();
        this.dbInfo.getClass();
        contentValues.put(PlayerMetaData.KEY_SERVER_ID, messageDM.serverId);
        this.dbInfo.getClass();
        contentValues.put("conversation_id", messageDM.conversationLocalId);
        this.dbInfo.getClass();
        contentValues.put("body", messageDM.body);
        this.dbInfo.getClass();
        contentValues.put("author_name", messageDM.authorName);
        this.dbInfo.getClass();
        contentValues.put("created_at", messageDM.getCreatedAt());
        this.dbInfo.getClass();
        contentValues.put("epoch_time_created_at", Long.valueOf(messageDM.getEpochCreatedAtTime()));
        this.dbInfo.getClass();
        contentValues.put("type", messageDM.messageType.getValue());
        this.dbInfo.getClass();
        contentValues.put("md_state", Integer.valueOf(messageDM.deliveryState));
        this.dbInfo.getClass();
        contentValues.put("is_redacted", Integer.valueOf(messageDM.isRedacted ? 1 : 0));
        try {
            this.dbInfo.getClass();
            contentValues.put("meta", getMessageMeta(messageDM));
        } catch (JSONException e) {
            HSLogger.e(TAG, "Error in generating meta string for message", e);
        }
        return contentValues;
    }

    private void parseAndSetMetaData(Conversation conversation, String str) {
        if (str != null) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                int optInt = jSONObject.optInt("csat_rating", 0);
                int optInt2 = jSONObject.optInt("csat_state", ConversationCSATState.NONE.getValue());
                String optString = jSONObject.optString("csat_feedback", null);
                conversation.csatRating = optInt;
                conversation.csatState = ConversationCSATState.fromInt(optInt2);
                conversation.csatFeedback = optString;
                conversation.shouldIncrementMessageCount = jSONObject.optBoolean("increment_message_count", false);
                conversation.isConversationEndedDelegateSent = jSONObject.optBoolean("ended_delegate_sent", false);
                conversation.isAutoFilledPreIssue = jSONObject.optBoolean("is_autofilled_preissue", false);
            } catch (JSONException e) {
                HSLogger.e(TAG, "Error in parseAndSetMetaData", e);
            }
        }
    }

    private ImagePickerFile parseAndGetImageAttachmentDraft(String str) {
        ImagePickerFile imagePickerFile;
        if (str == null) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("image_draft_orig_name", null);
            Long valueOf = Long.valueOf(jSONObject.optLong("image_draft_orig_size", -1));
            String optString2 = jSONObject.optString("image_draft_file_path", null);
            boolean optBoolean = jSONObject.optBoolean("image_copy_done", false);
            if (valueOf.longValue() == -1) {
                valueOf = null;
            }
            imagePickerFile = new ImagePickerFile(optString2, optString, valueOf);
            try {
                imagePickerFile.isFileCompressionAndCopyingDone = optBoolean;
            } catch (JSONException e) {
                e = e;
            }
        } catch (JSONException e2) {
            e = e2;
            imagePickerFile = null;
            HSLogger.e(TAG, "Error in parseAndGetImageAttachmentDraft", e);
            return imagePickerFile;
        }
        return imagePickerFile;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v2, types: [com.helpshift.conversation.activeconversation.message.MessageDM] */
    /* JADX WARN: Type inference failed for: r2v17 */
    /* JADX WARN: Type inference failed for: r1v17 */
    /* JADX WARN: Type inference failed for: r1v26 */
    /* JADX WARN: Type inference failed for: r1v32 */
    /* JADX WARN: Type inference failed for: r9v34, types: [com.helpshift.conversation.activeconversation.message.UserMessageDM] */
    /* JADX WARN: Type inference failed for: r9v35, types: [com.helpshift.conversation.activeconversation.message.UserResponseMessageForTextInputDM] */
    /* JADX WARN: Type inference failed for: r9v36, types: [com.helpshift.conversation.activeconversation.message.UserResponseMessageForOptionInput] */
    /* JADX WARN: Type inference failed for: r7v22, types: [com.helpshift.conversation.activeconversation.message.AdminMessageDM] */
    /* JADX WARN: Type inference failed for: r7v23, types: [com.helpshift.conversation.activeconversation.message.AdminMessageWithTextInputDM] */
    /* JADX WARN: Type inference failed for: r7v24, types: [com.helpshift.conversation.activeconversation.message.AdminMessageWithOptionInputDM] */
    /* JADX WARN: Type inference failed for: r7v25, types: [com.helpshift.conversation.activeconversation.message.FAQListMessageDM] */
    /* JADX WARN: Type inference failed for: r7v26, types: [com.helpshift.conversation.activeconversation.message.FAQListMessageWithOptionInputDM] */
    /* JADX WARN: Type inference failed for: r9v37, types: [com.helpshift.conversation.activeconversation.message.AcceptedAppReviewMessageDM] */
    /* JADX WARN: Type inference failed for: r7v27, types: [com.helpshift.conversation.activeconversation.message.RequestAppReviewMessageDM] */
    /* JADX WARN: Type inference failed for: r9v38, types: [com.helpshift.conversation.activeconversation.message.FollowupAcceptedMessageDM] */
    /* JADX WARN: Type inference failed for: r9v39, types: [com.helpshift.conversation.activeconversation.message.FollowupRejectedMessageDM] */
    /* JADX WARN: Type inference failed for: r9v40, types: [com.helpshift.conversation.activeconversation.message.ConfirmationAcceptedMessageDM] */
    /* JADX WARN: Type inference failed for: r9v41, types: [com.helpshift.conversation.activeconversation.message.ConfirmationRejectedMessageDM] */
    /* JADX WARN: Type inference failed for: r9v42, types: [com.helpshift.conversation.activeconversation.message.ScreenshotMessageDM] */
    /* JADX WARN: Type inference failed for: r7v28, types: [com.helpshift.conversation.activeconversation.message.RequestScreenshotMessageDM] */
    /* JADX WARN: Type inference failed for: r7v29, types: [com.helpshift.conversation.activeconversation.message.AdminAttachmentMessageDM] */
    /* JADX WARN: Type inference failed for: r1v33 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 2 */
    @android.support.annotation.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.helpshift.conversation.activeconversation.message.MessageDM cursorToMessageDM(android.database.Cursor r30) {
        /*
            r29 = this;
            r0 = r29
            r1 = r30
            com.helpshift.common.conversation.ConversationDBInfo r2 = r0.dbInfo
            r2.getClass()
            java.lang.String r2 = "_id"
            int r2 = r1.getColumnIndex(r2)
            long r2 = r1.getLong(r2)
            com.helpshift.common.conversation.ConversationDBInfo r4 = r0.dbInfo
            r4.getClass()
            java.lang.String r4 = "conversation_id"
            int r4 = r1.getColumnIndex(r4)
            long r4 = r1.getLong(r4)
            com.helpshift.common.conversation.ConversationDBInfo r6 = r0.dbInfo
            r6.getClass()
            java.lang.String r6 = "server_id"
            int r6 = r1.getColumnIndex(r6)
            java.lang.String r8 = r1.getString(r6)
            com.helpshift.common.conversation.ConversationDBInfo r6 = r0.dbInfo
            r6.getClass()
            java.lang.String r6 = "body"
            int r6 = r1.getColumnIndex(r6)
            java.lang.String r10 = r1.getString(r6)
            com.helpshift.common.conversation.ConversationDBInfo r6 = r0.dbInfo
            r6.getClass()
            java.lang.String r6 = "author_name"
            int r6 = r1.getColumnIndex(r6)
            java.lang.String r14 = r1.getString(r6)
            com.helpshift.common.conversation.ConversationDBInfo r6 = r0.dbInfo
            r6.getClass()
            java.lang.String r6 = "meta"
            int r6 = r1.getColumnIndex(r6)
            java.lang.String r6 = r1.getString(r6)
            com.helpshift.common.conversation.ConversationDBInfo r7 = r0.dbInfo
            r7.getClass()
            java.lang.String r7 = "type"
            int r7 = r1.getColumnIndex(r7)
            java.lang.String r7 = r1.getString(r7)
            com.helpshift.common.conversation.ConversationDBInfo r9 = r0.dbInfo
            r9.getClass()
            java.lang.String r9 = "created_at"
            int r9 = r1.getColumnIndex(r9)
            java.lang.String r11 = r1.getString(r9)
            long r12 = com.helpshift.common.util.HSDateFormatSpec.convertToEpochTime(r11)
            com.helpshift.common.conversation.ConversationDBInfo r9 = r0.dbInfo
            r9.getClass()
            java.lang.String r9 = "md_state"
            int r9 = r1.getColumnIndex(r9)
            int r15 = r1.getInt(r9)
            com.helpshift.common.conversation.ConversationDBInfo r9 = r0.dbInfo
            r9.getClass()
            java.lang.String r9 = "is_redacted"
            r22 = r15
            r15 = 0
            boolean r1 = com.helpshift.util.DatabaseUtils.parseBooleanColumnSafe(r1, r9, r15)
            com.helpshift.conversation.activeconversation.message.MessageType r7 = com.helpshift.conversation.activeconversation.message.MessageType.fromValue(r7)
            org.json.JSONObject r6 = r0.jsonify(r6)
            int[] r9 = com.helpshift.common.conversation.ConversationDB.AnonymousClass1.$SwitchMap$com$helpshift$conversation$activeconversation$message$MessageType
            int r7 = r7.ordinal()
            r7 = r9[r7]
            switch(r7) {
                case 1: goto L_0x039a;
                case 2: goto L_0x0369;
                case 3: goto L_0x0344;
                case 4: goto L_0x0331;
                case 5: goto L_0x0300;
                case 6: goto L_0x02cf;
                case 7: goto L_0x02ae;
                case 8: goto L_0x0279;
                case 9: goto L_0x025f;
                case 10: goto L_0x0246;
                case 11: goto L_0x022c;
                case 12: goto L_0x020c;
                case 13: goto L_0x01f6;
                case 14: goto L_0x01e0;
                case 15: goto L_0x01a8;
                case 16: goto L_0x018f;
                case 17: goto L_0x0160;
                case 18: goto L_0x011d;
                case 19: goto L_0x0102;
                case 20: goto L_0x00db;
                case 21: goto L_0x00b2;
                default: goto L_0x00b0;
            }
        L_0x00b0:
            r1 = 0
            return r1
        L_0x00b2:
            java.lang.String r15 = r0.parseBotActionTypeFromMeta(r6)
            java.lang.String r17 = r0.parseBotInfoFromMeta(r6)
            java.lang.String r16 = r0.parseBotEndedReasonFromMeta(r6)
            java.lang.String r18 = r0.parseReferredMessageIdFromMeta(r6)
            int r19 = r0.parseAndGetMessageSyncState(r8, r6)
            com.helpshift.conversation.activeconversation.message.UserBotControlMessageDM r7 = new com.helpshift.conversation.activeconversation.message.UserBotControlMessageDM
            r9 = r7
            r23 = r1
            r1 = r22
            r9.<init>(r10, r11, r12, r14, r15, r16, r17, r18, r19)
            r7.serverId = r8
            r24 = r1
            r25 = r2
            r27 = r4
            r1 = r7
            goto L_0x03aa
        L_0x00db:
            r23 = r1
            r1 = r22
            java.lang.String r15 = r0.parseBotActionTypeFromMeta(r6)
            java.lang.String r16 = r0.parseBotInfoFromMeta(r6)
            java.lang.Boolean r17 = r0.parseHasNextBotFromMeta(r6)
            com.helpshift.conversation.activeconversation.message.AdminBotControlMessageDM r9 = new com.helpshift.conversation.activeconversation.message.AdminBotControlMessageDM
            r7 = r9
            r24 = r1
            r1 = r9
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r15
            r15 = r16
            r7.<init>(r8, r9, r10, r11, r13, r14, r15)
            boolean r7 = r17.booleanValue()
            r1.hasNextBot = r7
            goto L_0x0117
        L_0x0102:
            r23 = r1
            r24 = r22
            com.helpshift.conversation.activeconversation.message.RequestForReopenMessageDM r1 = new com.helpshift.conversation.activeconversation.message.RequestForReopenMessageDM
            r7 = r1
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r7.<init>(r8, r9, r10, r11, r13)
            boolean r7 = r0.parseIsAnsweredFromMeta(r6)
            r1.setAnswered(r7)
        L_0x0117:
            r25 = r2
            r27 = r4
            goto L_0x03aa
        L_0x011d:
            r23 = r1
            r24 = r22
            com.helpshift.common.conversation.ConversationDB$ImageAttachmentInfo r1 = r0.parseImageAttachmentInfoFromMeta(r6)
            com.helpshift.conversation.activeconversation.message.AdminImageAttachmentMessageDM r15 = new com.helpshift.conversation.activeconversation.message.AdminImageAttachmentMessageDM
            java.lang.String r9 = r1.url
            java.lang.String r7 = r1.fileName
            r25 = r2
            java.lang.String r2 = r1.thumbnailUrl
            java.lang.String r3 = r1.contentType
            r27 = r4
            boolean r4 = r1.isSecure
            int r5 = r1.size
            r16 = r7
            r7 = r15
            r17 = r9
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r17
            r0 = r15
            r15 = r16
            r16 = r2
            r17 = r3
            r18 = r4
            r19 = r5
            r7.<init>(r8, r9, r10, r11, r13, r14, r15, r16, r17, r18, r19)
            java.lang.String r2 = r1.filePath
            r0.filePath = r2
            java.lang.String r1 = r1.thumbnailFilePath
            r0.thumbnailFilePath = r1
            r0.updateState()
            r1 = r0
            r0 = r29
            goto L_0x03aa
        L_0x0160:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            com.helpshift.common.conversation.ConversationDB$AttachmentInfo r1 = r0.parseAttachmentInfoFromMeta(r6)
            com.helpshift.conversation.activeconversation.message.AdminAttachmentMessageDM r2 = new com.helpshift.conversation.activeconversation.message.AdminAttachmentMessageDM
            int r3 = r1.size
            java.lang.String r15 = r1.contentType
            java.lang.String r4 = r1.url
            java.lang.String r5 = r1.fileName
            boolean r9 = r1.isSecure
            r7 = r2
            r18 = r9
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r3
            r16 = r4
            r17 = r5
            r7.<init>(r8, r9, r10, r11, r13, r14, r15, r16, r17, r18)
            java.lang.String r1 = r1.filePath
            r2.filePath = r1
            r2.updateState()
            goto L_0x01dd
        L_0x018f:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            com.helpshift.conversation.activeconversation.message.RequestScreenshotMessageDM r1 = new com.helpshift.conversation.activeconversation.message.RequestScreenshotMessageDM
            boolean r2 = r0.parseIsAnsweredFromMeta(r6)
            r7 = r1
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r2
            r7.<init>(r8, r9, r10, r11, r13, r14)
            goto L_0x03aa
        L_0x01a8:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            com.helpshift.common.conversation.ConversationDB$ImageAttachmentInfo r1 = r0.parseImageAttachmentInfoFromMeta(r6)
            com.helpshift.conversation.activeconversation.message.ScreenshotMessageDM r2 = new com.helpshift.conversation.activeconversation.message.ScreenshotMessageDM
            java.lang.String r15 = r1.contentType
            java.lang.String r3 = r1.thumbnailUrl
            java.lang.String r4 = r1.fileName
            java.lang.String r5 = r1.url
            int r7 = r1.size
            boolean r9 = r1.isSecure
            r20 = r9
            r9 = r2
            r16 = r3
            r17 = r4
            r18 = r5
            r19 = r7
            r9.<init>(r10, r11, r12, r14, r15, r16, r17, r18, r19, r20)
            java.lang.String r1 = r1.filePath
            r2.filePath = r1
            r2.serverId = r8
            java.lang.String r1 = r0.parseReferredMessageIdFromMeta(r6)
            r2.setRefersMessageId(r1)
        L_0x01dd:
            r1 = r2
            goto L_0x03aa
        L_0x01e0:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            com.helpshift.conversation.activeconversation.message.ConfirmationRejectedMessageDM r1 = new com.helpshift.conversation.activeconversation.message.ConfirmationRejectedMessageDM
            int r15 = r0.parseAndGetMessageSyncState(r8, r6)
            r9 = r1
            r9.<init>(r10, r11, r12, r14, r15)
            r1.serverId = r8
            goto L_0x03aa
        L_0x01f6:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            com.helpshift.conversation.activeconversation.message.ConfirmationAcceptedMessageDM r1 = new com.helpshift.conversation.activeconversation.message.ConfirmationAcceptedMessageDM
            int r15 = r0.parseAndGetMessageSyncState(r8, r6)
            r9 = r1
            r9.<init>(r10, r11, r12, r14, r15)
            r1.serverId = r8
            goto L_0x03aa
        L_0x020c:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            com.helpshift.conversation.activeconversation.message.FollowupRejectedMessageDM r1 = new com.helpshift.conversation.activeconversation.message.FollowupRejectedMessageDM
            java.lang.String r15 = r0.parseReferredMessageIdFromMeta(r6)
            int r16 = r0.parseAndGetMessageSyncState(r8, r6)
            r9 = r1
            r9.<init>(r10, r11, r12, r14, r15, r16)
            r1.serverId = r8
            r2 = r1
            com.helpshift.conversation.activeconversation.message.FollowupRejectedMessageDM r2 = (com.helpshift.conversation.activeconversation.message.FollowupRejectedMessageDM) r2
            r0.parseAndSetFollowUpRejectedDataFromMeta(r2, r6)
            goto L_0x03aa
        L_0x022c:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            com.helpshift.conversation.activeconversation.message.FollowupAcceptedMessageDM r1 = new com.helpshift.conversation.activeconversation.message.FollowupAcceptedMessageDM
            java.lang.String r15 = r0.parseReferredMessageIdFromMeta(r6)
            int r16 = r0.parseAndGetMessageSyncState(r8, r6)
            r9 = r1
            r9.<init>(r10, r11, r12, r14, r15, r16)
            r1.serverId = r8
            goto L_0x03aa
        L_0x0246:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            com.helpshift.conversation.activeconversation.message.RequestAppReviewMessageDM r1 = new com.helpshift.conversation.activeconversation.message.RequestAppReviewMessageDM
            boolean r2 = r0.parseIsAnsweredFromMeta(r6)
            r7 = r1
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r2
            r7.<init>(r8, r9, r10, r11, r13, r14)
            goto L_0x03aa
        L_0x025f:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            com.helpshift.conversation.activeconversation.message.AcceptedAppReviewMessageDM r1 = new com.helpshift.conversation.activeconversation.message.AcceptedAppReviewMessageDM
            java.lang.String r15 = r0.parseReferredMessageIdFromMeta(r6)
            int r16 = r0.parseAndGetMessageSyncState(r8, r6)
            r9 = r1
            r9.<init>(r10, r11, r12, r14, r15, r16)
            r1.serverId = r8
            goto L_0x03aa
        L_0x0279:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            com.helpshift.conversation.activeconversation.message.FAQListMessageWithOptionInputDM r1 = new com.helpshift.conversation.activeconversation.message.FAQListMessageWithOptionInputDM
            java.util.List r2 = r0.parseFAQListFromMeta(r6)
            java.lang.String r15 = r0.parseBotInfoFromMeta(r6)
            boolean r16 = r0.parseInputRequiredFromMeta(r6)
            java.lang.String r17 = r0.parseInputLabelFromMeta(r6)
            java.lang.String r18 = r0.parseInputSkipLabelFromMeta(r6)
            java.util.List r19 = r0.parseInputOptionsFromMeta(r6)
            boolean r20 = r0.parseIsSuggestionsReadEventSent(r6)
            java.lang.String r21 = r0.parseSuggestionReadFAQPublishId(r6)
            r7 = r1
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r2
            r7.<init>(r8, r9, r10, r11, r13, r14, r15, r16, r17, r18, r19, r20, r21)
            goto L_0x03aa
        L_0x02ae:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            com.helpshift.conversation.activeconversation.message.FAQListMessageDM r1 = new com.helpshift.conversation.activeconversation.message.FAQListMessageDM
            java.util.List r2 = r0.parseFAQListFromMeta(r6)
            boolean r15 = r0.parseIsSuggestionsReadEventSent(r6)
            java.lang.String r16 = r0.parseSuggestionReadFAQPublishId(r6)
            r7 = r1
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r2
            r7.<init>(r8, r9, r10, r11, r13, r14, r15, r16)
            goto L_0x03aa
        L_0x02cf:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            java.util.List r18 = r0.parseInputOptionsFromMeta(r6)
            com.helpshift.conversation.activeconversation.message.AdminMessageWithOptionInputDM r1 = new com.helpshift.conversation.activeconversation.message.AdminMessageWithOptionInputDM
            java.lang.String r2 = r0.parseBotInfoFromMeta(r6)
            boolean r15 = r0.parseInputRequiredFromMeta(r6)
            java.lang.String r16 = r0.parseInputLabelFromMeta(r6)
            java.lang.String r17 = r0.parseInputSkipLabelFromMeta(r6)
            int r3 = r18.size()
            com.helpshift.conversation.activeconversation.message.input.OptionInput$Type r19 = r0.parseInputOptionTypeFromMeta(r6, r3)
            r7 = r1
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r2
            r7.<init>(r8, r9, r10, r11, r13, r14, r15, r16, r17, r18, r19)
            goto L_0x03aa
        L_0x0300:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            com.helpshift.conversation.activeconversation.message.AdminMessageWithTextInputDM r1 = new com.helpshift.conversation.activeconversation.message.AdminMessageWithTextInputDM
            java.lang.String r2 = r0.parseBotInfoFromMeta(r6)
            java.lang.String r15 = r0.parseInputPlaceholderFromMeta(r6)
            boolean r16 = r0.parseInputRequiredFromMeta(r6)
            java.lang.String r17 = r0.parseInputLabelFromMeta(r6)
            java.lang.String r18 = r0.parseInputSkipLabelFromMeta(r6)
            int r19 = r0.parseInputKeyboardFromMeta(r6)
            boolean r20 = r0.parseIsMessageEmptyFromMeta(r6)
            r7 = r1
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r2
            r7.<init>(r8, r9, r10, r11, r13, r14, r15, r16, r17, r18, r19, r20)
            goto L_0x03aa
        L_0x0331:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            com.helpshift.conversation.activeconversation.message.AdminMessageDM r1 = new com.helpshift.conversation.activeconversation.message.AdminMessageDM
            r7 = r1
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r7.<init>(r8, r9, r10, r11, r13)
            goto L_0x03aa
        L_0x0344:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            com.helpshift.conversation.activeconversation.message.UserResponseMessageForOptionInput r1 = new com.helpshift.conversation.activeconversation.message.UserResponseMessageForOptionInput
            java.lang.String r15 = r0.parseBotInfoFromMeta(r6)
            boolean r16 = r0.parseIsResponseSkippedFromMeta(r6)
            java.lang.String r17 = r0.parseSelectedOptionDataFromMeta(r6)
            java.lang.String r18 = r0.parseReferredMessageIdFromMeta(r6)
            com.helpshift.conversation.activeconversation.message.MessageType r19 = r0.parseReferredMessageTypeFromMeta(r6)
            r9 = r1
            r9.<init>(r10, r11, r12, r14, r15, r16, r17, r18, r19)
            r1.serverId = r8
            goto L_0x03aa
        L_0x0369:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            com.helpshift.conversation.activeconversation.message.UserResponseMessageForTextInputDM r1 = new com.helpshift.conversation.activeconversation.message.UserResponseMessageForTextInputDM
            int r15 = r0.parseInputKeyboardFromMeta(r6)
            java.lang.String r16 = r0.parseBotInfoFromMeta(r6)
            boolean r17 = r0.parseIsResponseSkippedFromMeta(r6)
            java.lang.String r18 = r0.parseReferredMessageIdFromMeta(r6)
            boolean r19 = r0.parseIsMessageEmptyFromMeta(r6)
            r9 = r1
            r9.<init>(r10, r11, r12, r14, r15, r16, r17, r18, r19)
            r1.serverId = r8
            long r2 = r0.parseDateTimeFromMeta(r6)
            r1.dateInMillis = r2
            java.lang.String r2 = r0.parseTimeZoneIdFromMeta(r6)
            r1.timeZoneId = r2
            goto L_0x03aa
        L_0x039a:
            r23 = r1
            r25 = r2
            r27 = r4
            r24 = r22
            com.helpshift.conversation.activeconversation.message.UserMessageDM r1 = new com.helpshift.conversation.activeconversation.message.UserMessageDM
            r9 = r1
            r9.<init>(r10, r11, r12, r14)
            r1.serverId = r8
        L_0x03aa:
            java.lang.Long r2 = java.lang.Long.valueOf(r27)
            r1.conversationLocalId = r2
            java.lang.Long r2 = java.lang.Long.valueOf(r25)
            r1.localId = r2
            r2 = r24
            r1.deliveryState = r2
            r2 = r23
            r1.isRedacted = r2
            r0.parseAndSetMessageSeenData(r1, r6)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.conversation.ConversationDB.cursorToMessageDM(android.database.Cursor):com.helpshift.conversation.activeconversation.message.MessageDM");
    }

    private OptionInput.Type parseInputOptionTypeFromMeta(JSONObject jSONObject, int i) {
        return OptionInput.Type.getType(getStringFromJson(jSONObject, "option_type", ""), i);
    }

    private boolean parseIsMessageEmptyFromMeta(JSONObject jSONObject) {
        return getBooleanFromJson(jSONObject, "is_message_empty", false);
    }

    private String parseBotActionTypeFromMeta(JSONObject jSONObject) {
        return jSONObject.optString("bot_action_type", "");
    }

    private String parseBotEndedReasonFromMeta(JSONObject jSONObject) {
        return jSONObject.optString("bot_ended_reason", "");
    }

    private String parseSuggestionReadFAQPublishId(JSONObject jSONObject) {
        return getStringFromJson(jSONObject, "suggestion_read_faq_publish_id", "");
    }

    private boolean parseIsSuggestionsReadEventSent(JSONObject jSONObject) {
        return getBooleanFromJson(jSONObject, "is_suggestion_read_event_sent", false);
    }

    private List<FAQListMessageDM.FAQ> parseFAQListFromMeta(@NonNull JSONObject jSONObject) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = jSONObject.getJSONArray(Tables.FAQS);
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                arrayList.add(new FAQListMessageDM.FAQ(jSONObject2.getString("faq_title"), jSONObject2.getString("faq_publish_id"), jSONObject2.getString("faq_language")));
            }
            return arrayList;
        } catch (JSONException unused) {
            return arrayList;
        }
    }

    private List<OptionInput.Option> parseInputOptionsFromMeta(@NonNull JSONObject jSONObject) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = jSONObject.getJSONArray("input_options");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                arrayList.add(new OptionInput.Option(jSONObject2.getString("option_title"), jSONObject2.getString("option_data")));
            }
            return arrayList;
        } catch (JSONException unused) {
            return arrayList;
        }
    }

    private int getIntFromJson(@NonNull JSONObject jSONObject, String str, int i) {
        return jSONObject.optInt(str, i);
    }

    private String getStringFromJson(@NonNull JSONObject jSONObject, String str, String str2) {
        return jSONObject.optString(str, str2);
    }

    private boolean getBooleanFromJson(@NonNull JSONObject jSONObject, String str, boolean z) {
        return jSONObject.optBoolean(str, z);
    }

    private MessageType parseReferredMessageTypeFromMeta(JSONObject jSONObject) {
        return MessageType.fromValue(getStringFromJson(jSONObject, "referred_message_type", ""));
    }

    private String parseSelectedOptionDataFromMeta(JSONObject jSONObject) {
        return getStringFromJson(jSONObject, "selected_option_data", "{}");
    }

    private boolean parseIsResponseSkippedFromMeta(JSONObject jSONObject) {
        return getBooleanFromJson(jSONObject, "is_response_skipped", false);
    }

    private int parseInputKeyboardFromMeta(JSONObject jSONObject) {
        return getIntFromJson(jSONObject, "input_keyboard", 1);
    }

    private String parseInputSkipLabelFromMeta(JSONObject jSONObject) {
        return getStringFromJson(jSONObject, "input_skip_label", "");
    }

    private String parseInputLabelFromMeta(JSONObject jSONObject) {
        return getStringFromJson(jSONObject, "input_label", "");
    }

    private boolean parseInputRequiredFromMeta(JSONObject jSONObject) {
        return getBooleanFromJson(jSONObject, "input_required", false);
    }

    private String parseInputPlaceholderFromMeta(JSONObject jSONObject) {
        return getStringFromJson(jSONObject, "input_placeholder", "");
    }

    private String parseBotInfoFromMeta(@NonNull JSONObject jSONObject) {
        return jSONObject.optString("chatbot_info", "{}");
    }

    private long parseDateTimeFromMeta(@NonNull JSONObject jSONObject) {
        return jSONObject.optLong("dt", 0);
    }

    private String parseTimeZoneIdFromMeta(@NonNull JSONObject jSONObject) {
        return jSONObject.optString("timezone_id");
    }

    private Boolean parseHasNextBotFromMeta(@NonNull JSONObject jSONObject) {
        return Boolean.valueOf(jSONObject.optBoolean("has_next_bot", false));
    }

    private int parseAndGetMessageSyncState(String str, @NonNull JSONObject jSONObject) {
        if (!StringUtils.isEmpty(str)) {
            return 2;
        }
        return jSONObject.optInt("message_sync_status", 1);
    }

    private void parseAndSetMessageSeenData(MessageDM messageDM, @NonNull JSONObject jSONObject) {
        String optString = jSONObject.optString("read_at", "");
        String optString2 = jSONObject.optString("seen_cursor", null);
        boolean optBoolean = jSONObject.optBoolean("seen_sync_status", false);
        messageDM.seenAtMessageCursor = optString2;
        messageDM.isMessageSeenSynced = optBoolean;
        messageDM.readAt = optString;
    }

    private JSONObject jsonify(String str) {
        JSONObject jSONObject = new JSONObject();
        if (StringUtils.isEmpty(str)) {
            return jSONObject;
        }
        try {
            return new JSONObject(str);
        } catch (JSONException e) {
            HSLogger.e(TAG, "Exception in jsonify", e);
            return jSONObject;
        }
    }

    private String parseReferredMessageIdFromMeta(@NonNull JSONObject jSONObject) {
        return jSONObject.optString("referredMessageId", null);
    }

    private boolean parseIsAnsweredFromMeta(@NonNull JSONObject jSONObject) {
        return jSONObject.optBoolean("is_answered", false);
    }

    private AttachmentInfo parseAttachmentInfoFromMeta(@NonNull JSONObject jSONObject) {
        return new AttachmentInfo(jSONObject);
    }

    private ImageAttachmentInfo parseImageAttachmentInfoFromMeta(@NonNull JSONObject jSONObject) {
        return new ImageAttachmentInfo(jSONObject);
    }

    private void parseAndSetFollowUpRejectedDataFromMeta(FollowupRejectedMessageDM followupRejectedMessageDM, @NonNull JSONObject jSONObject) {
        int optInt = jSONObject.optInt("rejected_reason");
        String optString = jSONObject.optString("rejected_conv_id", null);
        followupRejectedMessageDM.reason = optInt;
        followupRejectedMessageDM.openConversationId = optString;
    }

    private String getMessageMeta(MessageDM messageDM) throws JSONException {
        JSONObject jSONObject;
        switch (messageDM.messageType) {
            case USER_RESP_FOR_TEXT_INPUT:
                jSONObject = new JSONObject();
                UserResponseMessageForTextInputDM userResponseMessageForTextInputDM = (UserResponseMessageForTextInputDM) messageDM;
                buildMetaForBotInfo(jSONObject, userResponseMessageForTextInputDM.botInfo);
                buildMetaForInputKeyboard(jSONObject, userResponseMessageForTextInputDM.keyboard);
                buildMetaForIsResponseSkipped(jSONObject, userResponseMessageForTextInputDM.skipped);
                buildMetaForReferredMessageId(jSONObject, userResponseMessageForTextInputDM.getReferredMessageId());
                buildMetaForIsMessageEmpty(jSONObject, userResponseMessageForTextInputDM.isMessageEmpty);
                buildMetaForDateTime(jSONObject, userResponseMessageForTextInputDM);
                break;
            case USER_RESP_FOR_OPTION_INPUT:
                jSONObject = new JSONObject();
                UserResponseMessageForOptionInput userResponseMessageForOptionInput = (UserResponseMessageForOptionInput) messageDM;
                buildMetaForBotInfo(jSONObject, userResponseMessageForOptionInput.botInfo);
                buildMetaForIsResponseSkipped(jSONObject, userResponseMessageForOptionInput.skipped);
                buildMetaForReferredMessageId(jSONObject, userResponseMessageForOptionInput.getReferredMessageId());
                buildMetaForReferredMessageType(jSONObject, userResponseMessageForOptionInput.referredMessageType);
                buildMetaForSelectedOptionData(jSONObject, userResponseMessageForOptionInput.optionData);
                break;
            case ADMIN_TEXT:
                jSONObject = new JSONObject();
                buildMetaForMessageSeenData(jSONObject, messageDM);
                break;
            case ADMIN_TEXT_WITH_TEXT_INPUT:
                jSONObject = new JSONObject();
                AdminMessageWithTextInputDM adminMessageWithTextInputDM = (AdminMessageWithTextInputDM) messageDM;
                buildMetaForMessageSeenData(jSONObject, messageDM);
                buildMetaForInput(jSONObject, adminMessageWithTextInputDM.input);
                buildMetaForIsMessageEmpty(jSONObject, adminMessageWithTextInputDM.isMessageEmpty);
                break;
            case ADMIN_TEXT_WITH_OPTION_INPUT:
                jSONObject = new JSONObject();
                buildMetaForMessageSeenData(jSONObject, messageDM);
                buildMetaForInput(jSONObject, ((AdminMessageWithOptionInputDM) messageDM).input);
                break;
            case FAQ_LIST:
                jSONObject = new JSONObject();
                buildMetaForMessageSeenData(jSONObject, messageDM);
                FAQListMessageDM fAQListMessageDM = (FAQListMessageDM) messageDM;
                buildMetaForFAQList(jSONObject, fAQListMessageDM);
                buildMetaForIsSuggestionsReadEvent(jSONObject, fAQListMessageDM);
                break;
            case FAQ_LIST_WITH_OPTION_INPUT:
                jSONObject = new JSONObject();
                buildMetaForMessageSeenData(jSONObject, messageDM);
                buildMetaForFAQList(jSONObject, (FAQListMessageDM) messageDM);
                FAQListMessageWithOptionInputDM fAQListMessageWithOptionInputDM = (FAQListMessageWithOptionInputDM) messageDM;
                buildMetaForInput(jSONObject, fAQListMessageWithOptionInputDM.input);
                buildMetaForIsSuggestionsReadEvent(jSONObject, fAQListMessageWithOptionInputDM);
                break;
            case ACCEPTED_APP_REVIEW:
                jSONObject = new JSONObject();
                AcceptedAppReviewMessageDM acceptedAppReviewMessageDM = (AcceptedAppReviewMessageDM) messageDM;
                buildMetaForReferredMessageId(jSONObject, acceptedAppReviewMessageDM.referredMessageId);
                buildMetaForAutoRetriableMessage(jSONObject, acceptedAppReviewMessageDM);
                break;
            case REQUESTED_APP_REVIEW:
                jSONObject = new JSONObject();
                buildMetaForIsAnswered(jSONObject, ((RequestAppReviewMessageDM) messageDM).isAnswered);
                buildMetaForMessageSeenData(jSONObject, messageDM);
                break;
            case FOLLOWUP_ACCEPTED:
                jSONObject = new JSONObject();
                FollowupAcceptedMessageDM followupAcceptedMessageDM = (FollowupAcceptedMessageDM) messageDM;
                buildMetaForReferredMessageId(jSONObject, followupAcceptedMessageDM.referredMessageId);
                buildMetaForAutoRetriableMessage(jSONObject, followupAcceptedMessageDM);
                break;
            case FOLLOWUP_REJECTED:
                jSONObject = new JSONObject();
                FollowupRejectedMessageDM followupRejectedMessageDM = (FollowupRejectedMessageDM) messageDM;
                buildMetaForFollowUpRejected(jSONObject, followupRejectedMessageDM);
                buildMetaForAutoRetriableMessage(jSONObject, followupRejectedMessageDM);
                break;
            case CONFIRMATION_ACCEPTED:
                jSONObject = new JSONObject();
                buildMetaForAutoRetriableMessage(jSONObject, (ConfirmationAcceptedMessageDM) messageDM);
                break;
            case CONFIRMATION_REJECTED:
                jSONObject = new JSONObject();
                buildMetaForAutoRetriableMessage(jSONObject, (ConfirmationRejectedMessageDM) messageDM);
                break;
            case SCREENSHOT:
                jSONObject = new JSONObject();
                buildMetaForScreenshotAttachmentMessage(jSONObject, (ScreenshotMessageDM) messageDM);
                break;
            case REQUESTED_SCREENSHOT:
                jSONObject = new JSONObject();
                buildMetaForIsAnswered(jSONObject, ((RequestScreenshotMessageDM) messageDM).isAnswered);
                buildMetaForMessageSeenData(jSONObject, messageDM);
                break;
            case ADMIN_ATTACHMENT:
                jSONObject = new JSONObject();
                buildJsonObjectForAttachmentMessage(jSONObject, (AttachmentMessageDM) messageDM);
                buildMetaForMessageSeenData(jSONObject, messageDM);
                break;
            case ADMIN_IMAGE_ATTACHMENT:
                jSONObject = new JSONObject();
                buildMetaForImageAttachmentMessage(jSONObject, (ImageAttachmentMessageDM) messageDM);
                buildMetaForMessageSeenData(jSONObject, messageDM);
                break;
            case REQUEST_FOR_REOPEN:
                jSONObject = new JSONObject();
                buildMetaForIsAnswered(jSONObject, ((RequestForReopenMessageDM) messageDM).isAnswered());
                buildMetaForMessageSeenData(jSONObject, messageDM);
                break;
            case ADMIN_BOT_CONTROL:
                jSONObject = new JSONObject();
                buildMetaForAdminBotControlMessage(jSONObject, (AdminBotControlMessageDM) messageDM);
                break;
            case USER_BOT_CONTROL:
                jSONObject = new JSONObject();
                UserBotControlMessageDM userBotControlMessageDM = (UserBotControlMessageDM) messageDM;
                buildMetaForUserBotControlMessage(jSONObject, userBotControlMessageDM);
                buildMetaForAutoRetriableMessage(jSONObject, userBotControlMessageDM);
                break;
            default:
                jSONObject = null;
                break;
        }
        if (jSONObject == null) {
            return null;
        }
        return jSONObject.toString();
    }

    private void buildMetaForIsSuggestionsReadEvent(JSONObject jSONObject, FAQListMessageDM fAQListMessageDM) throws JSONException {
        jSONObject.put("is_suggestion_read_event_sent", fAQListMessageDM.isSuggestionsReadEventSent);
        jSONObject.put("suggestion_read_faq_publish_id", fAQListMessageDM.suggestionsReadFAQPublishId);
    }

    private void buildMetaForIsMessageEmpty(JSONObject jSONObject, boolean z) throws JSONException {
        jSONObject.put("is_message_empty", z);
    }

    private void buildMetaForAdminBotControlMessage(JSONObject jSONObject, AdminBotControlMessageDM adminBotControlMessageDM) throws JSONException {
        jSONObject.put("bot_action_type", adminBotControlMessageDM.actionType);
        jSONObject.put("has_next_bot", adminBotControlMessageDM.hasNextBot);
    }

    private void buildMetaForUserBotControlMessage(JSONObject jSONObject, UserBotControlMessageDM userBotControlMessageDM) throws JSONException {
        jSONObject.put("bot_action_type", userBotControlMessageDM.actionType);
        jSONObject.put("chatbot_info", userBotControlMessageDM.botInfo);
        jSONObject.put("bot_ended_reason", userBotControlMessageDM.reason);
        jSONObject.put("referredMessageId", userBotControlMessageDM.refersMessageId);
    }

    private void buildMetaForFAQList(JSONObject jSONObject, FAQListMessageDM fAQListMessageDM) throws JSONException {
        if (fAQListMessageDM.faqs != null) {
            JSONArray jSONArray = new JSONArray();
            for (FAQListMessageDM.FAQ next : fAQListMessageDM.faqs) {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("faq_title", next.title);
                jSONObject2.put("faq_publish_id", next.publishId);
                jSONObject2.put("faq_language", next.language);
                jSONArray.put(jSONObject2);
            }
            jSONObject.put(Tables.FAQS, jSONArray);
        }
    }

    private void buildMetaForReferredMessageType(JSONObject jSONObject, MessageType messageType) throws JSONException {
        jSONObject.put("referred_message_type", messageType.getValue());
    }

    private void buildMetaForSelectedOptionData(JSONObject jSONObject, String str) throws JSONException {
        jSONObject.put("selected_option_data", str);
    }

    private void buildMetaForIsResponseSkipped(JSONObject jSONObject, boolean z) throws JSONException {
        jSONObject.put("is_response_skipped", z);
    }

    private void buildMetaForInputKeyboard(JSONObject jSONObject, int i) throws JSONException {
        jSONObject.put("input_keyboard", i);
    }

    private void buildMetaForDateTime(JSONObject jSONObject, UserResponseMessageForTextInputDM userResponseMessageForTextInputDM) throws JSONException {
        if (userResponseMessageForTextInputDM.keyboard == 4) {
            jSONObject.put("dt", userResponseMessageForTextInputDM.dateInMillis);
            jSONObject.put("timezone_id", userResponseMessageForTextInputDM.timeZoneId);
        }
    }

    private void buildMetaForBotInfo(JSONObject jSONObject, String str) throws JSONException {
        jSONObject.put("chatbot_info", str);
    }

    private void buildMetaForInput(JSONObject jSONObject, TextInput textInput) throws JSONException {
        buildMetaForBotInfo(jSONObject, textInput.botInfo);
        jSONObject.put("input_required", textInput.required);
        jSONObject.put("input_skip_label", textInput.skipLabel);
        jSONObject.put("input_label", textInput.inputLabel);
        jSONObject.put("input_placeholder", textInput.placeholder);
        buildMetaForInputKeyboard(jSONObject, textInput.keyboard);
    }

    private void buildMetaForInput(JSONObject jSONObject, OptionInput optionInput) throws JSONException {
        buildMetaForBotInfo(jSONObject, optionInput.botInfo);
        jSONObject.put("input_required", optionInput.required);
        jSONObject.put("input_label", optionInput.inputLabel);
        jSONObject.put("input_skip_label", optionInput.skipLabel);
        if (optionInput.options != null) {
            JSONArray jSONArray = new JSONArray();
            for (OptionInput.Option next : optionInput.options) {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("option_title", next.title);
                jSONObject2.put("option_data", next.jsonData);
                jSONArray.put(jSONObject2);
            }
            jSONObject.put("input_options", jSONArray);
        }
        jSONObject.put("option_type", optionInput.type.toString());
    }

    private void buildMetaForMessageSeenData(JSONObject jSONObject, MessageDM messageDM) throws JSONException {
        jSONObject.put("seen_cursor", messageDM.seenAtMessageCursor);
        jSONObject.put("seen_sync_status", messageDM.isMessageSeenSynced);
        jSONObject.put("read_at", messageDM.readAt);
    }

    private void buildMetaForReferredMessageId(JSONObject jSONObject, String str) throws JSONException {
        jSONObject.put("referredMessageId", str);
    }

    private void buildMetaForFollowUpRejected(JSONObject jSONObject, FollowupRejectedMessageDM followupRejectedMessageDM) throws JSONException {
        jSONObject.put("referredMessageId", followupRejectedMessageDM.referredMessageId);
        jSONObject.put("rejected_reason", followupRejectedMessageDM.reason);
        jSONObject.put("rejected_conv_id", followupRejectedMessageDM.openConversationId);
    }

    private void buildMetaForIsAnswered(JSONObject jSONObject, boolean z) throws JSONException {
        jSONObject.put("is_answered", z);
    }

    private void buildJsonObjectForAttachmentMessage(JSONObject jSONObject, AttachmentMessageDM attachmentMessageDM) throws JSONException {
        jSONObject.put(FirebaseAnalytics.Param.CONTENT_TYPE, attachmentMessageDM.contentType);
        jSONObject.put("file_name", attachmentMessageDM.fileName);
        jSONObject.put("filePath", attachmentMessageDM.filePath);
        jSONObject.put("url", attachmentMessageDM.attachmentUrl);
        jSONObject.put("size", attachmentMessageDM.size);
        jSONObject.put("is_secure", attachmentMessageDM.isSecureAttachment);
    }

    private void buildMetaForScreenshotAttachmentMessage(JSONObject jSONObject, ScreenshotMessageDM screenshotMessageDM) throws JSONException {
        buildJsonObjectForAttachmentMessage(jSONObject, screenshotMessageDM);
        jSONObject.put("thumbnail_url", screenshotMessageDM.thumbnailUrl);
        jSONObject.put("referredMessageId", screenshotMessageDM.refersMessageId);
        jSONObject.put("is_secure", screenshotMessageDM.isSecureAttachment);
    }

    private void buildMetaForImageAttachmentMessage(JSONObject jSONObject, ImageAttachmentMessageDM imageAttachmentMessageDM) throws JSONException {
        buildJsonObjectForAttachmentMessage(jSONObject, imageAttachmentMessageDM);
        jSONObject.put("thumbnail_url", imageAttachmentMessageDM.thumbnailUrl);
        jSONObject.put("thumbnailFilePath", imageAttachmentMessageDM.thumbnailFilePath);
        jSONObject.put("is_secure", imageAttachmentMessageDM.isSecureAttachment);
    }

    private void buildMetaForAutoRetriableMessage(JSONObject jSONObject, AutoRetriableMessageDM autoRetriableMessageDM) throws JSONException {
        jSONObject.put("message_sync_status", autoRetriableMessageDM.getSyncStatus());
    }

    public synchronized MessageDM readMessageWithServerId(String str) {
        List<MessageDM> readMessages;
        StringBuilder sb = new StringBuilder();
        this.dbInfo.getClass();
        sb.append(PlayerMetaData.KEY_SERVER_ID);
        sb.append(" = ?");
        readMessages = readMessages(sb.toString(), new String[]{String.valueOf(str)});
        return ListUtils.isEmpty(readMessages) ? null : readMessages.get(0);
    }

    public synchronized MessageDM readMessageWithLocalId(Long l) {
        List<MessageDM> readMessages;
        StringBuilder sb = new StringBuilder();
        this.dbInfo.getClass();
        sb.append("_id");
        sb.append(" = ?");
        readMessages = readMessages(sb.toString(), new String[]{String.valueOf(l)});
        return ListUtils.isEmpty(readMessages) ? null : readMessages.get(0);
    }

    public synchronized void dropAndCreateDatabase() {
        this.dbHelper.dropAndCreateDatabase(this.dbHelper.getWritableDatabase());
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:13:0x0035 */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r1v2, types: [com.helpshift.support.Faq] */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r1v4 */
    /* JADX WARN: Type inference failed for: r1v5 */
    /* JADX WARN: Type inference failed for: r1v6 */
    /* JADX WARN: Type inference failed for: r1v7 */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0035, code lost:
        if (r11 != null) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r11.close();
        r1 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0048, code lost:
        if (r11 != null) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004c, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0056, code lost:
        return null;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0051 A[SYNTHETIC, Splitter:B:29:0x0051] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.helpshift.support.Faq getAdminFAQSuggestion(java.lang.String r11, java.lang.String r12) {
        /*
            r10 = this;
            monitor-enter(r10)
            boolean r0 = com.helpshift.common.StringUtils.isEmpty(r11)     // Catch:{ all -> 0x0057 }
            r1 = 0
            if (r0 != 0) goto L_0x0055
            boolean r0 = com.helpshift.common.StringUtils.isEmpty(r12)     // Catch:{ all -> 0x0057 }
            if (r0 == 0) goto L_0x000f
            goto L_0x0055
        L_0x000f:
            com.helpshift.platform.db.ConversationDBHelper r0 = r10.dbHelper     // Catch:{ Exception -> 0x003f, all -> 0x003d }
            android.database.sqlite.SQLiteDatabase r2 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x003f, all -> 0x003d }
            java.lang.String r3 = "faq_suggestions"
            r4 = 0
            java.lang.String r5 = "publish_id = ? AND language = ?"
            r0 = 2
            java.lang.String[] r6 = new java.lang.String[r0]     // Catch:{ Exception -> 0x003f, all -> 0x003d }
            r0 = 0
            r6[r0] = r11     // Catch:{ Exception -> 0x003f, all -> 0x003d }
            r11 = 1
            r6[r11] = r12     // Catch:{ Exception -> 0x003f, all -> 0x003d }
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r11 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x003f, all -> 0x003d }
            boolean r12 = r11.moveToFirst()     // Catch:{ Exception -> 0x003b }
            if (r12 == 0) goto L_0x0035
            com.helpshift.support.Faq r12 = r10.cursorToFaq(r11)     // Catch:{ Exception -> 0x003b }
            r1 = r12
        L_0x0035:
            if (r11 == 0) goto L_0x004b
        L_0x0037:
            r11.close()     // Catch:{ all -> 0x0057 }
            goto L_0x004b
        L_0x003b:
            r12 = move-exception
            goto L_0x0041
        L_0x003d:
            r12 = move-exception
            goto L_0x004f
        L_0x003f:
            r12 = move-exception
            r11 = r1
        L_0x0041:
            java.lang.String r0 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in getAdminFAQSuggestion"
            com.helpshift.util.HSLogger.e(r0, r2, r12)     // Catch:{ all -> 0x004d }
            if (r11 == 0) goto L_0x004b
            goto L_0x0037
        L_0x004b:
            monitor-exit(r10)
            return r1
        L_0x004d:
            r12 = move-exception
            r1 = r11
        L_0x004f:
            if (r1 == 0) goto L_0x0054
            r1.close()     // Catch:{ all -> 0x0057 }
        L_0x0054:
            throw r12     // Catch:{ all -> 0x0057 }
        L_0x0055:
            monitor-exit(r10)
            return r1
        L_0x0057:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.conversation.ConversationDB.getAdminFAQSuggestion(java.lang.String, java.lang.String):com.helpshift.support.Faq");
    }

    public synchronized void insertOrUpdateAdminFAQSuggestion(Faq faq) {
        ContentValues faqToContentValues = faqToContentValues(faq);
        String[] strArr = {faq.publish_id, faq.language};
        try {
            SQLiteDatabase writableDatabase = this.dbHelper.getWritableDatabase();
            if (!exists(writableDatabase, "faq_suggestions", "publish_id = ? AND language = ?", strArr)) {
                writableDatabase.insert("faq_suggestions", null, faqToContentValues);
            } else {
                writableDatabase.update("faq_suggestions", faqToContentValues, "publish_id = ? AND language = ?", strArr);
            }
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in insertOrUpdateAdminFAQSuggestion", e);
        }
        return;
    }

    private Faq cursorToFaq(Cursor cursor) {
        long j = cursor.getLong(cursor.getColumnIndex("_id"));
        String string = cursor.getString(cursor.getColumnIndex(FaqsColumns.QUESTION_ID));
        String string2 = cursor.getString(cursor.getColumnIndex("publish_id"));
        String string3 = cursor.getString(cursor.getColumnIndex(FaqsColumns.LANGUAGE));
        String string4 = cursor.getString(cursor.getColumnIndex("section_id"));
        String string5 = cursor.getString(cursor.getColumnIndex("title"));
        String string6 = cursor.getString(cursor.getColumnIndex("body"));
        int i = cursor.getInt(cursor.getColumnIndex(FaqsColumns.HELPFUL));
        boolean z = true;
        if (cursor.getInt(cursor.getColumnIndex(FaqsColumns.RTL)) != 1) {
            z = false;
        }
        return new Faq(j, string, string2, string3, string4, string5, string6, i, Boolean.valueOf(z), HSJSONUtils.jsonToStringArrayList(cursor.getString(cursor.getColumnIndex(FaqsColumns.TAGS))), HSJSONUtils.jsonToStringArrayList(cursor.getString(cursor.getColumnIndex(FaqsColumns.CATEGORY_TAGS))));
    }

    public synchronized void removeAdminFAQSuggestion(String str, String str2) {
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            try {
                this.dbHelper.getWritableDatabase().delete("faq_suggestions", "publish_id = ? AND language = ?", new String[]{str, str2});
            } catch (Exception e) {
                HSLogger.e(TAG, "Error in removeAdminFAQSuggestion", e);
            }
        }
        return;
    }

    public synchronized void deleteConversationInboxData(long j) {
        StringBuilder sb = new StringBuilder();
        sb.append("delete from ");
        this.dbInfo.getClass();
        sb.append("conversation_inbox");
        sb.append(" where ");
        this.dbInfo.getClass();
        sb.append("user_local_id");
        sb.append(" = ?");
        String sb2 = sb.toString();
        try {
            this.dbHelper.getWritableDatabase().execSQL(sb2, new String[]{String.valueOf(j)});
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in delete conversationInboxData with UserLocalId", e);
        }
        return;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0123 A[SYNTHETIC, Splitter:B:21:0x0123] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x012a A[SYNTHETIC, Splitter:B:26:0x012a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void deleteConversations(long r8) {
        /*
            r7 = this;
            monitor-enter(r7)
            com.helpshift.common.conversation.ConversationDBInfo r0 = r7.dbInfo     // Catch:{ all -> 0x012e }
            r0.getClass()     // Catch:{ all -> 0x012e }
            java.lang.String r0 = "issues"
            com.helpshift.common.conversation.ConversationDBInfo r1 = r7.dbInfo     // Catch:{ all -> 0x012e }
            r1.getClass()     // Catch:{ all -> 0x012e }
            java.lang.String r1 = "messages"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x012e }
            r2.<init>()     // Catch:{ all -> 0x012e }
            r2.append(r0)     // Catch:{ all -> 0x012e }
            java.lang.String r3 = "."
            r2.append(r3)     // Catch:{ all -> 0x012e }
            com.helpshift.common.conversation.ConversationDBInfo r3 = r7.dbInfo     // Catch:{ all -> 0x012e }
            r3.getClass()     // Catch:{ all -> 0x012e }
            java.lang.String r3 = "_id"
            r2.append(r3)     // Catch:{ all -> 0x012e }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x012e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x012e }
            r3.<init>()     // Catch:{ all -> 0x012e }
            r3.append(r0)     // Catch:{ all -> 0x012e }
            java.lang.String r0 = "."
            r3.append(r0)     // Catch:{ all -> 0x012e }
            com.helpshift.common.conversation.ConversationDBInfo r0 = r7.dbInfo     // Catch:{ all -> 0x012e }
            r0.getClass()     // Catch:{ all -> 0x012e }
            java.lang.String r0 = "user_local_id"
            r3.append(r0)     // Catch:{ all -> 0x012e }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x012e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x012e }
            r3.<init>()     // Catch:{ all -> 0x012e }
            r3.append(r1)     // Catch:{ all -> 0x012e }
            java.lang.String r1 = "."
            r3.append(r1)     // Catch:{ all -> 0x012e }
            com.helpshift.common.conversation.ConversationDBInfo r1 = r7.dbInfo     // Catch:{ all -> 0x012e }
            r1.getClass()     // Catch:{ all -> 0x012e }
            java.lang.String r1 = "conversation_id"
            r3.append(r1)     // Catch:{ all -> 0x012e }
            java.lang.String r1 = r3.toString()     // Catch:{ all -> 0x012e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x012e }
            r3.<init>()     // Catch:{ all -> 0x012e }
            java.lang.String r4 = "select "
            r3.append(r4)     // Catch:{ all -> 0x012e }
            r3.append(r2)     // Catch:{ all -> 0x012e }
            java.lang.String r2 = " from  "
            r3.append(r2)     // Catch:{ all -> 0x012e }
            com.helpshift.common.conversation.ConversationDBInfo r2 = r7.dbInfo     // Catch:{ all -> 0x012e }
            r2.getClass()     // Catch:{ all -> 0x012e }
            java.lang.String r2 = "issues"
            r3.append(r2)     // Catch:{ all -> 0x012e }
            java.lang.String r2 = "  where "
            r3.append(r2)     // Catch:{ all -> 0x012e }
            r3.append(r0)     // Catch:{ all -> 0x012e }
            java.lang.String r0 = " = ?"
            r3.append(r0)     // Catch:{ all -> 0x012e }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x012e }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x012e }
            r2.<init>()     // Catch:{ all -> 0x012e }
            java.lang.String r3 = "delete from "
            r2.append(r3)     // Catch:{ all -> 0x012e }
            com.helpshift.common.conversation.ConversationDBInfo r3 = r7.dbInfo     // Catch:{ all -> 0x012e }
            r3.getClass()     // Catch:{ all -> 0x012e }
            java.lang.String r3 = "messages"
            r2.append(r3)     // Catch:{ all -> 0x012e }
            java.lang.String r3 = " where "
            r2.append(r3)     // Catch:{ all -> 0x012e }
            r2.append(r1)     // Catch:{ all -> 0x012e }
            java.lang.String r1 = " IN  ( "
            r2.append(r1)     // Catch:{ all -> 0x012e }
            r2.append(r0)     // Catch:{ all -> 0x012e }
            java.lang.String r0 = " )"
            r2.append(r0)     // Catch:{ all -> 0x012e }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x012e }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x012e }
            r1.<init>()     // Catch:{ all -> 0x012e }
            java.lang.String r2 = "delete from "
            r1.append(r2)     // Catch:{ all -> 0x012e }
            com.helpshift.common.conversation.ConversationDBInfo r2 = r7.dbInfo     // Catch:{ all -> 0x012e }
            r2.getClass()     // Catch:{ all -> 0x012e }
            java.lang.String r2 = "issues"
            r1.append(r2)     // Catch:{ all -> 0x012e }
            java.lang.String r2 = " where "
            r1.append(r2)     // Catch:{ all -> 0x012e }
            com.helpshift.common.conversation.ConversationDBInfo r2 = r7.dbInfo     // Catch:{ all -> 0x012e }
            r2.getClass()     // Catch:{ all -> 0x012e }
            java.lang.String r2 = "user_local_id"
            r1.append(r2)     // Catch:{ all -> 0x012e }
            java.lang.String r2 = " = ?"
            r1.append(r2)     // Catch:{ all -> 0x012e }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x012e }
            r2 = 0
            com.helpshift.platform.db.ConversationDBHelper r3 = r7.dbHelper     // Catch:{ Exception -> 0x0119 }
            android.database.sqlite.SQLiteDatabase r3 = r3.getWritableDatabase()     // Catch:{ Exception -> 0x0119 }
            r3.beginTransaction()     // Catch:{ Exception -> 0x0113, all -> 0x0111 }
            r2 = 1
            java.lang.String[] r4 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0113, all -> 0x0111 }
            java.lang.String r5 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x0113, all -> 0x0111 }
            r6 = 0
            r4[r6] = r5     // Catch:{ Exception -> 0x0113, all -> 0x0111 }
            r3.execSQL(r0, r4)     // Catch:{ Exception -> 0x0113, all -> 0x0111 }
            java.lang.String[] r0 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0113, all -> 0x0111 }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x0113, all -> 0x0111 }
            r0[r6] = r8     // Catch:{ Exception -> 0x0113, all -> 0x0111 }
            r3.execSQL(r1, r0)     // Catch:{ Exception -> 0x0113, all -> 0x0111 }
            r3.setTransactionSuccessful()     // Catch:{ Exception -> 0x0113, all -> 0x0111 }
            if (r3 == 0) goto L_0x0126
            r3.endTransaction()     // Catch:{ all -> 0x012e }
            goto L_0x0126
        L_0x0111:
            r8 = move-exception
            goto L_0x0128
        L_0x0113:
            r8 = move-exception
            r2 = r3
            goto L_0x011a
        L_0x0116:
            r8 = move-exception
            r3 = r2
            goto L_0x0128
        L_0x0119:
            r8 = move-exception
        L_0x011a:
            java.lang.String r9 = "Helpshift_ConverDB"
            java.lang.String r0 = "Error in delete conversations with UserLocalId"
            com.helpshift.util.HSLogger.e(r9, r0, r8)     // Catch:{ all -> 0x0116 }
            if (r2 == 0) goto L_0x0126
            r2.endTransaction()     // Catch:{ all -> 0x012e }
        L_0x0126:
            monitor-exit(r7)
            return
        L_0x0128:
            if (r3 == 0) goto L_0x012d
            r3.endTransaction()     // Catch:{ all -> 0x012e }
        L_0x012d:
            throw r8     // Catch:{ all -> 0x012e }
        L_0x012e:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.conversation.ConversationDB.deleteConversations(long):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x012d, code lost:
        if (r11 != null) goto L_0x012f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0143, code lost:
        if (r11 != null) goto L_0x012f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x014b A[SYNTHETIC, Splitter:B:28:0x014b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.String getOldestMessageCursor(long r10) {
        /*
            r9 = this;
            monitor-enter(r9)
            java.lang.String r0 = "message_create_at"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x014f }
            r1.<init>()     // Catch:{ all -> 0x014f }
            com.helpshift.common.conversation.ConversationDBInfo r2 = r9.dbInfo     // Catch:{ all -> 0x014f }
            r2.getClass()     // Catch:{ all -> 0x014f }
            java.lang.String r2 = "issues"
            r1.append(r2)     // Catch:{ all -> 0x014f }
            java.lang.String r2 = "."
            r1.append(r2)     // Catch:{ all -> 0x014f }
            com.helpshift.common.conversation.ConversationDBInfo r2 = r9.dbInfo     // Catch:{ all -> 0x014f }
            r2.getClass()     // Catch:{ all -> 0x014f }
            java.lang.String r2 = "user_local_id"
            r1.append(r2)     // Catch:{ all -> 0x014f }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x014f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x014f }
            r2.<init>()     // Catch:{ all -> 0x014f }
            com.helpshift.common.conversation.ConversationDBInfo r3 = r9.dbInfo     // Catch:{ all -> 0x014f }
            r3.getClass()     // Catch:{ all -> 0x014f }
            java.lang.String r3 = "issues"
            r2.append(r3)     // Catch:{ all -> 0x014f }
            java.lang.String r3 = "."
            r2.append(r3)     // Catch:{ all -> 0x014f }
            com.helpshift.common.conversation.ConversationDBInfo r3 = r9.dbInfo     // Catch:{ all -> 0x014f }
            r3.getClass()     // Catch:{ all -> 0x014f }
            java.lang.String r3 = "_id"
            r2.append(r3)     // Catch:{ all -> 0x014f }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x014f }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x014f }
            r3.<init>()     // Catch:{ all -> 0x014f }
            com.helpshift.common.conversation.ConversationDBInfo r4 = r9.dbInfo     // Catch:{ all -> 0x014f }
            r4.getClass()     // Catch:{ all -> 0x014f }
            java.lang.String r4 = "messages"
            r3.append(r4)     // Catch:{ all -> 0x014f }
            java.lang.String r4 = "."
            r3.append(r4)     // Catch:{ all -> 0x014f }
            com.helpshift.common.conversation.ConversationDBInfo r4 = r9.dbInfo     // Catch:{ all -> 0x014f }
            r4.getClass()     // Catch:{ all -> 0x014f }
            java.lang.String r4 = "conversation_id"
            r3.append(r4)     // Catch:{ all -> 0x014f }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x014f }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x014f }
            r4.<init>()     // Catch:{ all -> 0x014f }
            com.helpshift.common.conversation.ConversationDBInfo r5 = r9.dbInfo     // Catch:{ all -> 0x014f }
            r5.getClass()     // Catch:{ all -> 0x014f }
            java.lang.String r5 = "messages"
            r4.append(r5)     // Catch:{ all -> 0x014f }
            java.lang.String r5 = "."
            r4.append(r5)     // Catch:{ all -> 0x014f }
            com.helpshift.common.conversation.ConversationDBInfo r5 = r9.dbInfo     // Catch:{ all -> 0x014f }
            r5.getClass()     // Catch:{ all -> 0x014f }
            java.lang.String r5 = "created_at"
            r4.append(r5)     // Catch:{ all -> 0x014f }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x014f }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x014f }
            r5.<init>()     // Catch:{ all -> 0x014f }
            com.helpshift.common.conversation.ConversationDBInfo r6 = r9.dbInfo     // Catch:{ all -> 0x014f }
            r6.getClass()     // Catch:{ all -> 0x014f }
            java.lang.String r6 = "messages"
            r5.append(r6)     // Catch:{ all -> 0x014f }
            java.lang.String r6 = "."
            r5.append(r6)     // Catch:{ all -> 0x014f }
            com.helpshift.common.conversation.ConversationDBInfo r6 = r9.dbInfo     // Catch:{ all -> 0x014f }
            r6.getClass()     // Catch:{ all -> 0x014f }
            java.lang.String r6 = "epoch_time_created_at"
            r5.append(r6)     // Catch:{ all -> 0x014f }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x014f }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x014f }
            r6.<init>()     // Catch:{ all -> 0x014f }
            java.lang.String r7 = "SELECT "
            r6.append(r7)     // Catch:{ all -> 0x014f }
            r6.append(r4)     // Catch:{ all -> 0x014f }
            java.lang.String r4 = " AS "
            r6.append(r4)     // Catch:{ all -> 0x014f }
            r6.append(r0)     // Catch:{ all -> 0x014f }
            java.lang.String r4 = " FROM "
            r6.append(r4)     // Catch:{ all -> 0x014f }
            com.helpshift.common.conversation.ConversationDBInfo r4 = r9.dbInfo     // Catch:{ all -> 0x014f }
            r4.getClass()     // Catch:{ all -> 0x014f }
            java.lang.String r4 = "issues"
            r6.append(r4)     // Catch:{ all -> 0x014f }
            java.lang.String r4 = " INNER JOIN "
            r6.append(r4)     // Catch:{ all -> 0x014f }
            com.helpshift.common.conversation.ConversationDBInfo r4 = r9.dbInfo     // Catch:{ all -> 0x014f }
            r4.getClass()     // Catch:{ all -> 0x014f }
            java.lang.String r4 = "messages"
            r6.append(r4)     // Catch:{ all -> 0x014f }
            java.lang.String r4 = " ON "
            r6.append(r4)     // Catch:{ all -> 0x014f }
            r6.append(r2)     // Catch:{ all -> 0x014f }
            java.lang.String r2 = " = "
            r6.append(r2)     // Catch:{ all -> 0x014f }
            r6.append(r3)     // Catch:{ all -> 0x014f }
            java.lang.String r2 = " WHERE "
            r6.append(r2)     // Catch:{ all -> 0x014f }
            r6.append(r1)     // Catch:{ all -> 0x014f }
            java.lang.String r1 = " = ? ORDER BY "
            r6.append(r1)     // Catch:{ all -> 0x014f }
            r6.append(r5)     // Catch:{ all -> 0x014f }
            java.lang.String r1 = "  ASC LIMIT 1"
            r6.append(r1)     // Catch:{ all -> 0x014f }
            java.lang.String r1 = r6.toString()     // Catch:{ all -> 0x014f }
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ all -> 0x014f }
            r3 = 0
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ all -> 0x014f }
            r2[r3] = r10     // Catch:{ all -> 0x014f }
            r10 = 0
            com.helpshift.platform.db.ConversationDBHelper r11 = r9.dbHelper     // Catch:{ Exception -> 0x013a, all -> 0x0135 }
            android.database.sqlite.SQLiteDatabase r11 = r11.getReadableDatabase()     // Catch:{ Exception -> 0x013a, all -> 0x0135 }
            android.database.Cursor r11 = r11.rawQuery(r1, r2)     // Catch:{ Exception -> 0x013a, all -> 0x0135 }
            boolean r1 = r11.moveToFirst()     // Catch:{ Exception -> 0x0133 }
            if (r1 == 0) goto L_0x012d
            int r0 = r11.getColumnIndex(r0)     // Catch:{ Exception -> 0x0133 }
            java.lang.String r0 = r11.getString(r0)     // Catch:{ Exception -> 0x0133 }
            r10 = r0
        L_0x012d:
            if (r11 == 0) goto L_0x0146
        L_0x012f:
            r11.close()     // Catch:{ all -> 0x014f }
            goto L_0x0146
        L_0x0133:
            r0 = move-exception
            goto L_0x013c
        L_0x0135:
            r11 = move-exception
            r8 = r11
            r11 = r10
            r10 = r8
            goto L_0x0149
        L_0x013a:
            r0 = move-exception
            r11 = r10
        L_0x013c:
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in read messages"
            com.helpshift.util.HSLogger.e(r1, r2, r0)     // Catch:{ all -> 0x0148 }
            if (r11 == 0) goto L_0x0146
            goto L_0x012f
        L_0x0146:
            monitor-exit(r9)
            return r10
        L_0x0148:
            r10 = move-exception
        L_0x0149:
            if (r11 == 0) goto L_0x014e
            r11.close()     // Catch:{ all -> 0x014f }
        L_0x014e:
            throw r10     // Catch:{ all -> 0x014f }
        L_0x014f:
            r10 = move-exception
            monitor-exit(r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.conversation.ConversationDB.getOldestMessageCursor(long):java.lang.String");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:11:0x0072 */
    /* JADX WARN: Type inference failed for: r13v3 */
    /* JADX WARN: Type inference failed for: r13v4, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r13v5, types: [java.lang.Long] */
    /* JADX WARN: Type inference failed for: r13v7 */
    /* JADX WARN: Type inference failed for: r13v8 */
    /* JADX WARN: Type inference failed for: r13v9 */
    /* JADX WARN: Type inference failed for: r13v10 */
    /* JADX WARN: Type inference failed for: r13v11 */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0072, code lost:
        if (r14 != null) goto L_0x0074;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r14.close();
        r13 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0085, code lost:
        if (r14 != null) goto L_0x0074;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0090 A[SYNTHETIC, Splitter:B:27:0x0090] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.Long getOldestConversationEpochCreatedAtTime(long r13) {
        /*
            r12 = this;
            monitor-enter(r12)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0094 }
            r0.<init>()     // Catch:{ all -> 0x0094 }
            com.helpshift.common.conversation.ConversationDBInfo r1 = r12.dbInfo     // Catch:{ all -> 0x0094 }
            r1.getClass()     // Catch:{ all -> 0x0094 }
            java.lang.String r1 = "user_local_id"
            r0.append(r1)     // Catch:{ all -> 0x0094 }
            java.lang.String r1 = " = ?"
            r0.append(r1)     // Catch:{ all -> 0x0094 }
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x0094 }
            r0 = 1
            java.lang.String[] r6 = new java.lang.String[r0]     // Catch:{ all -> 0x0094 }
            java.lang.String r13 = java.lang.String.valueOf(r13)     // Catch:{ all -> 0x0094 }
            r14 = 0
            r6[r14] = r13     // Catch:{ all -> 0x0094 }
            r13 = 0
            com.helpshift.platform.db.ConversationDBHelper r1 = r12.dbHelper     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            android.database.sqlite.SQLiteDatabase r2 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            com.helpshift.common.conversation.ConversationDBInfo r1 = r12.dbInfo     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r1.getClass()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            java.lang.String r3 = "issues"
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            com.helpshift.common.conversation.ConversationDBInfo r0 = r12.dbInfo     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r0.getClass()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            java.lang.String r0 = "epoch_time_created_at"
            r4[r14] = r0     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r7 = 0
            r8 = 0
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r14.<init>()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            com.helpshift.common.conversation.ConversationDBInfo r0 = r12.dbInfo     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r0.getClass()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            java.lang.String r0 = "epoch_time_created_at"
            r14.append(r0)     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            java.lang.String r0 = " ASC"
            r14.append(r0)     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            java.lang.String r9 = r14.toString()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            java.lang.String r10 = "1"
            android.database.Cursor r14 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            boolean r0 = r14.moveToFirst()     // Catch:{ Exception -> 0x0078 }
            if (r0 == 0) goto L_0x0072
            com.helpshift.common.conversation.ConversationDBInfo r0 = r12.dbInfo     // Catch:{ Exception -> 0x0078 }
            r0.getClass()     // Catch:{ Exception -> 0x0078 }
            java.lang.String r0 = "epoch_time_created_at"
            java.lang.Class<java.lang.Long> r1 = java.lang.Long.class
            java.lang.Object r0 = com.helpshift.util.DatabaseUtils.parseColumnSafe(r14, r0, r1)     // Catch:{ Exception -> 0x0078 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ Exception -> 0x0078 }
            r13 = r0
        L_0x0072:
            if (r14 == 0) goto L_0x0088
        L_0x0074:
            r14.close()     // Catch:{ all -> 0x0094 }
            goto L_0x0088
        L_0x0078:
            r0 = move-exception
            goto L_0x007e
        L_0x007a:
            r14 = move-exception
            goto L_0x008e
        L_0x007c:
            r0 = move-exception
            r14 = r13
        L_0x007e:
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in getting latest conversation created_at time"
            com.helpshift.util.HSLogger.e(r1, r2, r0)     // Catch:{ all -> 0x008a }
            if (r14 == 0) goto L_0x0088
            goto L_0x0074
        L_0x0088:
            monitor-exit(r12)
            return r13
        L_0x008a:
            r13 = move-exception
            r11 = r14
            r14 = r13
            r13 = r11
        L_0x008e:
            if (r13 == 0) goto L_0x0093
            r13.close()     // Catch:{ all -> 0x0094 }
        L_0x0093:
            throw r14     // Catch:{ all -> 0x0094 }
        L_0x0094:
            r13 = move-exception
            monitor-exit(r12)
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.conversation.ConversationDB.getOldestConversationEpochCreatedAtTime(long):java.lang.Long");
    }

    private class ImageAttachmentInfo extends AttachmentInfo {
        final String thumbnailFilePath;
        final String thumbnailUrl;

        ImageAttachmentInfo(JSONObject jSONObject) {
            super(jSONObject);
            this.thumbnailUrl = jSONObject.optString("thumbnail_url", null);
            this.thumbnailFilePath = jSONObject.optString("thumbnailFilePath", null);
        }
    }

    private class AttachmentInfo {
        final String contentType;
        final String fileName;
        final String filePath;
        final boolean isSecure;
        final int size;
        final String url;

        AttachmentInfo(JSONObject jSONObject) {
            this.fileName = jSONObject.optString("file_name", null);
            this.contentType = jSONObject.optString(FirebaseAnalytics.Param.CONTENT_TYPE, null);
            this.url = jSONObject.optString("url", null);
            this.size = jSONObject.optInt("size", 0);
            this.filePath = jSONObject.optString("filePath", null);
            this.isSecure = jSONObject.optBoolean("is_secure", false);
        }
    }
}
