package com.helpshift.common.conversation;

import android.support.annotation.NonNull;
import com.unity3d.ads.metadata.PlayerMetaData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConversationDBInfo {
    public static final String DATABASE_NAME = "__hs__db_issues";
    public static final Integer DATABASE_VERSION = 8;
    static final String TABLE_FAQ_LIST_SUGGESTIONS = "faq_suggestions";
    final String ARCHIVAL_TEXT = "archival_text";
    final String ATTACHMENT_DRAFT = "attachment_draft";
    public final String AUTHOR_NAME = "author_name";
    public final String BODY = "body";
    public final String CONVERSATION_ID = "conversation_id";
    public final String CONVERSATION_META = "meta";
    public final String CREATED_AT = "created_at";
    public String CREATE_CONVERSATION_INBOX_TABLE = "CREATE TABLE conversation_inbox ( user_local_id TEXT PRIMARY KEY NOT NULL, form_name TEXT,form_email TEXT,description_draft TEXT,description_draft_timestamp TEXT,attachment_draft TEXT,description_type TEXT,archival_text TEXT, reply_text TEXT, persist_message_box INT, since TEXT, has_older_messages INT, last_conv_redaction_time INT );";
    public String CREATE_CONVERSATION_TABLE = "CREATE TABLE issues ( _id INTEGER PRIMARY KEY AUTOINCREMENT,server_id TEXT UNIQUE, pre_conv_server_id TEXT UNIQUE, publish_id TEXT, uuid TEXT NOT NULL, user_local_id TEXT NOT NULL, title TEXT NOT NULL,issue_type TEXT NOT NULL, state INTEGER NOT NULL, show_agent_name INTEGER,message_cursor TEXT,start_new_conversation_action INTEGER, is_redacted INTEGER, meta TEXT,last_user_activity_time INTEGER, full_privacy_enabled INTEGER, epoch_time_created_at INTEGER NOT NULL, created_at TEXT NOT NULL,updated_at TEXT NOT NULL  );";
    private String CREATE_FAQ_LIST_SUGGESTIONS_CACHE_TABLE = "CREATE TABLE faq_suggestions ( _id INTEGER PRIMARY KEY AUTOINCREMENT,question_id TEXT NOT NULL,publish_id TEXT NOT NULL,language TEXT NOT NULL,section_id TEXT NOT NULL,title TEXT NOT NULL,body TEXT NOT NULL,helpful INTEGER,rtl INTEGER,tags TEXT,c_tags TEXT );";
    public String CREATE_MESSAGES_TABLE = "CREATE TABLE messages ( _id INTEGER PRIMARY KEY AUTOINCREMENT, server_id TEXT, conversation_id TEXT, body TEXT, author_name TEXT, type TEXT, meta TEXT, is_redacted INTEGER, created_at TEXT, epoch_time_created_at INTEGER NOT NULL, md_state INTEGER  );";
    private final String CREATE_SERVER_ID_INDEX_MESSAGES_TABLE = "CREATE INDEX SERVER_IDX ON messages(server_id)";
    public final String DELIVERY_STATE = "md_state";
    final String DESCRIPTION_DRAFT = "description_draft";
    final String DESCRIPTION_DRAFT_TIMESTAMP = "description_draft_timestamp";
    final String DESCRIPTION_TYPE = "description_type";
    public final String EPOCH_TIME_CREATE_AT = "epoch_time_created_at";
    final String FORM_EMAIL = "form_email";
    final String FORM_NAME = "form_name";
    public final String FULL_PRIVACY_ENABLED = "full_privacy_enabled";
    public final String HAS_OLDER_MESSAGES = "has_older_messages";
    public final String ID = "_id";
    public final String ISSUE_TYPE = "issue_type";
    public final String IS_REDACTED_CONVERSATION = "is_redacted";
    public final String IS_REDACTED_MESSAGE = "is_redacted";
    public final String IS_START_NEW_CONVERSATION_CLICKED = "start_new_conversation_action";
    public final String LAST_CONVERSATIONS_REDACTION_TIME = "last_conv_redaction_time";
    final String LAST_SYNC_TIMESTAMP = "since";
    public final String LAST_USER_ACTIVITY_TIME = "last_user_activity_time";
    public final String LOCAL_UUID = "uuid";
    public final String MESSAGE_CURSOR = "message_cursor";
    public final String MESSAGE_META = "meta";
    final String PERSIST_MESSAGE_BOX = "persist_message_box";
    public final String PRE_CONVERSATION_SERVER_ID = "pre_conv_server_id";
    public final String PUBLISH_ID = "publish_id";
    final String REPLY_TEXT = "reply_text";
    public final String SERVER_ID = PlayerMetaData.KEY_SERVER_ID;
    public final String SHOW_AGENT_NAME = "show_agent_name";
    public final String STATE = "state";
    public final String TABLE_CONVERSATIONS = "issues";
    public final String TABLE_CONVERSATION_INBOX = "conversation_inbox";
    public final String TABLE_MESSAGES = "messages";
    public final String TITLE = "title";
    public final String TYPE = "type";
    public final String UPDATED_AT = "updated_at";
    public final String USER_LOCAL_ID = "user_local_id";

    public int getMinimumDBVersionSupportedForMigration() {
        return 6;
    }

    @NonNull
    public List<String> getQueriesForOnCreate() {
        return new ArrayList(Arrays.asList(this.CREATE_CONVERSATION_TABLE, this.CREATE_CONVERSATION_INBOX_TABLE, this.CREATE_MESSAGES_TABLE, "CREATE INDEX SERVER_IDX ON messages(server_id)", this.CREATE_FAQ_LIST_SUGGESTIONS_CACHE_TABLE));
    }

    @NonNull
    public List<String> getQueriesForDropAndCreate() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("DROP TABLE IF EXISTS issues");
        arrayList.add("DROP TABLE IF EXISTS conversation_inbox");
        arrayList.add("DROP TABLE IF EXISTS messages");
        arrayList.add("DROP TABLE IF EXISTS faq_suggestions");
        arrayList.add("DROP TABLE IF EXISTS issues_old");
        arrayList.addAll(getQueriesForOnCreate());
        return arrayList;
    }
}
