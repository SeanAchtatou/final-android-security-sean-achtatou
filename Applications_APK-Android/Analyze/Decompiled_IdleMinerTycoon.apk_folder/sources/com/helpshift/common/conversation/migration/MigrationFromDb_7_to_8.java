package com.helpshift.common.conversation.migration;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.helpshift.common.conversation.ConversationDBInfo;
import com.helpshift.common.migrator.Migrator;
import com.helpshift.support.search.storage.TableSearchToken;
import com.helpshift.util.HelpshiftContext;
import com.unity3d.ads.metadata.PlayerMetaData;

public class MigrationFromDb_7_to_8 extends Migrator {
    private String RENAME_CONVERSATION_TABLE;
    private String TEMP_TABLE_CONVERSATIONS;
    private ConversationDBInfo dbInfo = new ConversationDBInfo();

    MigrationFromDb_7_to_8(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase);
        StringBuilder sb = new StringBuilder();
        this.dbInfo.getClass();
        sb.append("issues");
        sb.append("_old");
        this.TEMP_TABLE_CONVERSATIONS = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("ALTER TABLE ");
        this.dbInfo.getClass();
        sb2.append("issues");
        sb2.append(" RENAME TO ");
        sb2.append(this.TEMP_TABLE_CONVERSATIONS);
        this.RENAME_CONVERSATION_TABLE = sb2.toString();
    }

    public void migrate() {
        migrateTable();
        if (getDuplicacyRecord() == 0) {
            migrateData();
        } else {
            HelpshiftContext.getCoreApi().resetUsersSyncStatusAndStartSetupForActiveUser();
        }
        dropTemporaryTables();
    }

    private void migrateTable() {
        this.db.execSQL(this.RENAME_CONVERSATION_TABLE);
        this.db.execSQL(this.dbInfo.CREATE_CONVERSATION_TABLE);
    }

    private void migrateData() {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        this.dbInfo.getClass();
        sb.append("issues");
        sb.append(" (");
        this.dbInfo.getClass();
        sb.append("_id");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append(PlayerMetaData.KEY_SERVER_ID);
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("pre_conv_server_id");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("publish_id");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("uuid");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("user_local_id");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("title");
        sb.append(",");
        this.dbInfo.getClass();
        sb.append("issue_type");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("state");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("show_agent_name");
        sb.append(",");
        this.dbInfo.getClass();
        sb.append("message_cursor");
        sb.append(",");
        this.dbInfo.getClass();
        sb.append("start_new_conversation_action");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("is_redacted");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("meta");
        sb.append(",");
        this.dbInfo.getClass();
        sb.append("last_user_activity_time");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("full_privacy_enabled");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("epoch_time_created_at");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("created_at");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("updated_at");
        sb.append(" ) SELECT ");
        this.dbInfo.getClass();
        sb.append("_id");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append(PlayerMetaData.KEY_SERVER_ID);
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("pre_conv_server_id");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("publish_id");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("uuid");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("user_local_id");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("title");
        sb.append(",");
        this.dbInfo.getClass();
        sb.append("issue_type");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("state");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("show_agent_name");
        sb.append(",");
        this.dbInfo.getClass();
        sb.append("message_cursor");
        sb.append(",");
        this.dbInfo.getClass();
        sb.append("start_new_conversation_action");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("is_redacted");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("meta");
        sb.append(",");
        this.dbInfo.getClass();
        sb.append("last_user_activity_time");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("full_privacy_enabled");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("epoch_time_created_at");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("created_at");
        sb.append(TableSearchToken.COMMA_SEP);
        this.dbInfo.getClass();
        sb.append("updated_at");
        sb.append(" FROM ");
        sb.append(this.TEMP_TABLE_CONVERSATIONS);
        this.db.execSQL(sb.toString());
    }

    private int getDuplicacyRecord() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT Count(");
        this.dbInfo.getClass();
        sb.append(PlayerMetaData.KEY_SERVER_ID);
        sb.append(") , Count(DISTINCT ");
        this.dbInfo.getClass();
        sb.append(PlayerMetaData.KEY_SERVER_ID);
        sb.append(") FROM ");
        sb.append(this.TEMP_TABLE_CONVERSATIONS);
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append("SELECT Count(");
        this.dbInfo.getClass();
        sb3.append("pre_conv_server_id");
        sb3.append(") , Count(DISTINCT ");
        this.dbInfo.getClass();
        sb3.append("pre_conv_server_id");
        sb3.append(") FROM ");
        sb3.append(this.TEMP_TABLE_CONVERSATIONS);
        return getDuplicateRecord(sb2) + getDuplicateRecord(sb3.toString());
    }

    private int getDuplicateRecord(String str) {
        Cursor rawQuery = this.db.rawQuery(str, null);
        try {
            int i = 0;
            if (rawQuery.moveToFirst()) {
                i = rawQuery.getInt(0) - rawQuery.getInt(1);
            }
            return i;
        } finally {
            if (rawQuery != null) {
                rawQuery.close();
            }
        }
    }

    private void dropTemporaryTables() {
        SQLiteDatabase sQLiteDatabase = this.db;
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + this.TEMP_TABLE_CONVERSATIONS);
    }
}
