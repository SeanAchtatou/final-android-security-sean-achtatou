package com.helpshift.common.conversation.migration;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.helpshift.common.conversation.ConversationDBInfo;
import com.helpshift.common.migrator.Migrator;
import com.helpshift.common.util.HSDateFormatSpec;
import java.util.HashMap;
import java.util.Map;

public class MigrationFromDb_6_to_7 extends Migrator {
    private String ADD_EPOCH_TIME_CREATE_AT_COLUMN_INTO_CONVERSATIONS_TABLE;
    private String ADD_EPOCH_TIME_CREATE_AT_COLUMN_INTO_MESSAGES_TABLE;
    private String ADD_FULL_PRIVACY_ENABLED_COLUMN_INTO_CONVERSATIONS_TABLE;
    private String ADD_HAS_OLDER_MESSAGES_COLUMN_INTO_INBOX_TABLE;
    private String ADD_IS_REDACTED_COLUMN_INTO_CONVERSATIONS_TABLE;
    private String ADD_IS_REDACTED_COLUMN_INTO_MESSAGES_TABLE;
    private String ADD_LAST_CONVERSATIONS_REDACTED_TIME_COLUMN_INTO_INBOX_TABLE;
    private ConversationDBInfo dbInfo = new ConversationDBInfo();

    MigrationFromDb_6_to_7(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase);
        StringBuilder sb = new StringBuilder();
        sb.append("ALTER TABLE ");
        this.dbInfo.getClass();
        sb.append("conversation_inbox");
        sb.append(" ADD COLUMN ");
        this.dbInfo.getClass();
        sb.append("has_older_messages");
        sb.append(" INT ;");
        this.ADD_HAS_OLDER_MESSAGES_COLUMN_INTO_INBOX_TABLE = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("ALTER TABLE ");
        this.dbInfo.getClass();
        sb2.append("conversation_inbox");
        sb2.append(" ADD COLUMN ");
        this.dbInfo.getClass();
        sb2.append("last_conv_redaction_time");
        sb2.append(" INT ;");
        this.ADD_LAST_CONVERSATIONS_REDACTED_TIME_COLUMN_INTO_INBOX_TABLE = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append("ALTER TABLE ");
        this.dbInfo.getClass();
        sb3.append("issues");
        sb3.append(" ADD COLUMN ");
        this.dbInfo.getClass();
        sb3.append("full_privacy_enabled");
        sb3.append(" INTEGER ;");
        this.ADD_FULL_PRIVACY_ENABLED_COLUMN_INTO_CONVERSATIONS_TABLE = sb3.toString();
        StringBuilder sb4 = new StringBuilder();
        sb4.append("ALTER TABLE ");
        this.dbInfo.getClass();
        sb4.append("issues");
        sb4.append(" ADD COLUMN ");
        this.dbInfo.getClass();
        sb4.append("is_redacted");
        sb4.append(" INTEGER ;");
        this.ADD_IS_REDACTED_COLUMN_INTO_CONVERSATIONS_TABLE = sb4.toString();
        StringBuilder sb5 = new StringBuilder();
        sb5.append("ALTER TABLE ");
        this.dbInfo.getClass();
        sb5.append("issues");
        sb5.append(" ADD COLUMN ");
        this.dbInfo.getClass();
        sb5.append("epoch_time_created_at");
        sb5.append(" INTEGER NOT NULL DEFAULT 0 ;");
        this.ADD_EPOCH_TIME_CREATE_AT_COLUMN_INTO_CONVERSATIONS_TABLE = sb5.toString();
        StringBuilder sb6 = new StringBuilder();
        sb6.append("ALTER TABLE ");
        this.dbInfo.getClass();
        sb6.append("messages");
        sb6.append(" ADD COLUMN ");
        this.dbInfo.getClass();
        sb6.append("is_redacted");
        sb6.append(" INTEGER ;");
        this.ADD_IS_REDACTED_COLUMN_INTO_MESSAGES_TABLE = sb6.toString();
        StringBuilder sb7 = new StringBuilder();
        sb7.append("ALTER TABLE ");
        this.dbInfo.getClass();
        sb7.append("messages");
        sb7.append(" ADD COLUMN ");
        this.dbInfo.getClass();
        sb7.append("epoch_time_created_at");
        sb7.append(" INTEGER NOT NULL DEFAULT 0 ;");
        this.ADD_EPOCH_TIME_CREATE_AT_COLUMN_INTO_MESSAGES_TABLE = sb7.toString();
    }

    public void migrate() {
        migrateTable();
        migrateData();
    }

    private void migrateTable() {
        this.db.execSQL(this.ADD_HAS_OLDER_MESSAGES_COLUMN_INTO_INBOX_TABLE);
        this.db.execSQL(this.ADD_LAST_CONVERSATIONS_REDACTED_TIME_COLUMN_INTO_INBOX_TABLE);
        this.db.execSQL(this.ADD_FULL_PRIVACY_ENABLED_COLUMN_INTO_CONVERSATIONS_TABLE);
        this.db.execSQL(this.ADD_IS_REDACTED_COLUMN_INTO_CONVERSATIONS_TABLE);
        this.db.execSQL(this.ADD_EPOCH_TIME_CREATE_AT_COLUMN_INTO_MESSAGES_TABLE);
        this.db.execSQL(this.ADD_IS_REDACTED_COLUMN_INTO_MESSAGES_TABLE);
        this.db.execSQL(this.ADD_EPOCH_TIME_CREATE_AT_COLUMN_INTO_CONVERSATIONS_TABLE);
    }

    private void migrateData() {
        HashMap hashMap = new HashMap();
        this.dbInfo.getClass();
        this.dbInfo.getClass();
        String[] strArr = {String.valueOf("_id"), "created_at"};
        SQLiteDatabase sQLiteDatabase = this.db;
        this.dbInfo.getClass();
        Cursor query = sQLiteDatabase.query("issues", strArr, null, null, null, null, null);
        if (query.moveToFirst()) {
            do {
                this.dbInfo.getClass();
                long j = query.getLong(query.getColumnIndex("_id"));
                this.dbInfo.getClass();
                hashMap.put(Long.valueOf(j), query.getString(query.getColumnIndex("created_at")));
            } while (query.moveToNext());
        }
        query.close();
        HashMap hashMap2 = new HashMap();
        SQLiteDatabase sQLiteDatabase2 = this.db;
        this.dbInfo.getClass();
        Cursor query2 = sQLiteDatabase2.query("messages", strArr, null, null, null, null, null);
        if (query2.moveToFirst()) {
            do {
                this.dbInfo.getClass();
                long j2 = query2.getLong(query2.getColumnIndex("_id"));
                this.dbInfo.getClass();
                hashMap2.put(Long.valueOf(j2), query2.getString(query2.getColumnIndex("created_at")));
            } while (query2.moveToNext());
        }
        query2.close();
        HashMap hashMap3 = new HashMap();
        for (Map.Entry entry : hashMap.entrySet()) {
            hashMap3.put(entry.getKey(), Long.valueOf(HSDateFormatSpec.convertToEpochTime((String) entry.getValue())));
        }
        HashMap hashMap4 = new HashMap();
        for (Map.Entry entry2 : hashMap2.entrySet()) {
            hashMap4.put(entry2.getKey(), Long.valueOf(HSDateFormatSpec.convertToEpochTime((String) entry2.getValue())));
        }
        for (Map.Entry entry3 : hashMap3.entrySet()) {
            ContentValues contentValues = new ContentValues();
            this.dbInfo.getClass();
            contentValues.put("epoch_time_created_at", (Long) entry3.getValue());
            StringBuilder sb = new StringBuilder();
            this.dbInfo.getClass();
            sb.append("_id");
            sb.append(" = ?");
            String sb2 = sb.toString();
            String[] strArr2 = {String.valueOf((Long) entry3.getKey())};
            SQLiteDatabase sQLiteDatabase3 = this.db;
            this.dbInfo.getClass();
            sQLiteDatabase3.update("issues", contentValues, sb2, strArr2);
        }
        for (Map.Entry entry4 : hashMap4.entrySet()) {
            ContentValues contentValues2 = new ContentValues();
            this.dbInfo.getClass();
            contentValues2.put("epoch_time_created_at", (Long) entry4.getValue());
            StringBuilder sb3 = new StringBuilder();
            this.dbInfo.getClass();
            sb3.append("_id");
            sb3.append(" = ?");
            String sb4 = sb3.toString();
            String[] strArr3 = {String.valueOf((Long) entry4.getKey())};
            SQLiteDatabase sQLiteDatabase4 = this.db;
            this.dbInfo.getClass();
            sQLiteDatabase4.update("messages", contentValues2, sb4, strArr3);
        }
    }
}
