package com.helpshift.common;

public interface FetchDataFromThread<T, R> {
    void onDataFetched(T t);

    void onFailure(R r);
}
