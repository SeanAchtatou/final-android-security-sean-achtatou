package com.helpshift.configuration.dto;

import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import java.util.Map;

public class RootInstallConfig {
    public final Boolean disableAnimations;
    public final Boolean disableAppLaunchEvent;
    public final Boolean disableErrorLogging;
    public final Boolean disableHelpshiftBranding;
    public final Boolean enableDefaultFallbackLanguage;
    public final Boolean enableInAppNotification;
    public final Boolean enableInboxPolling;
    public final Boolean enableNotificationMute;
    public final String fontPath;
    public final Integer largeNotificationIcon;
    public final Integer notificationIcon;
    public final Integer notificationSound;
    public final String pluginVersion;
    public final String runtimeVersion;
    public final String sdkType;
    public final String supportNotificationChannelId;

    public RootInstallConfig(Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4, Boolean bool5, Boolean bool6, Boolean bool7, Boolean bool8, Integer num, Integer num2, Integer num3, String str, String str2, String str3, String str4, String str5) {
        this.enableInAppNotification = bool;
        this.enableNotificationMute = bool4;
        this.disableHelpshiftBranding = bool5;
        this.disableAnimations = bool6;
        this.disableErrorLogging = bool7;
        this.disableAppLaunchEvent = bool8;
        this.notificationIcon = num;
        this.largeNotificationIcon = num2;
        this.notificationSound = num3;
        this.enableDefaultFallbackLanguage = bool2;
        this.enableInboxPolling = bool3;
        this.fontPath = str;
        this.sdkType = str2;
        this.pluginVersion = str3;
        this.runtimeVersion = str4;
        this.supportNotificationChannelId = str5;
    }

    public static final class RootInstallConfigBuilder {
        private Boolean disableAnimations;
        private Boolean disableAppLaunchEvent;
        private Boolean disableErrorLogging;
        private Boolean disableHelpshiftBranding;
        private Boolean enableDefaultFallbackLanguage;
        private Boolean enableInAppNotification;
        private Boolean enableInboxPolling;
        private Boolean enableNotificationMute;
        private String fontPath;
        private Integer largeNotificationIcon;
        private Integer notificationIcon;
        private Integer notificationSound;
        private String pluginVersion;
        private String runtimeVersion;
        private String sdkType;
        private String supportNotificationChannelId;

        public RootInstallConfigBuilder applyMap(Map<String, Object> map) {
            if (map.get(SDKConfigurationDM.ENABLE_IN_APP_NOTIFICATION) instanceof Boolean) {
                this.enableInAppNotification = (Boolean) map.get(SDKConfigurationDM.ENABLE_IN_APP_NOTIFICATION);
            }
            if (map.get("enableDefaultFallbackLanguage") instanceof Boolean) {
                this.enableDefaultFallbackLanguage = (Boolean) map.get("enableDefaultFallbackLanguage");
            }
            if (map.get("enableInboxPolling") instanceof Boolean) {
                this.enableInboxPolling = (Boolean) map.get("enableInboxPolling");
            }
            if (map.get("enableNotificationMute") instanceof Boolean) {
                this.enableNotificationMute = (Boolean) map.get("enableNotificationMute");
            }
            if (map.get("disableHelpshiftBranding") instanceof Boolean) {
                this.disableHelpshiftBranding = (Boolean) map.get("disableHelpshiftBranding");
            }
            if (map.get(SDKConfigurationDM.DISABLE_ERROR_LOGGING) instanceof Boolean) {
                this.disableErrorLogging = (Boolean) map.get(SDKConfigurationDM.DISABLE_ERROR_LOGGING);
            }
            if (map.get(SDKConfigurationDM.DISABLE_APP_LAUNCH_EVENT) instanceof Boolean) {
                this.disableAppLaunchEvent = (Boolean) map.get(SDKConfigurationDM.DISABLE_APP_LAUNCH_EVENT);
            }
            if (map.get(SDKConfigurationDM.DISABLE_ANIMATION) instanceof Boolean) {
                this.disableAnimations = (Boolean) map.get(SDKConfigurationDM.DISABLE_ANIMATION);
            }
            if (map.get("notificationIcon") instanceof Integer) {
                this.notificationIcon = (Integer) map.get("notificationIcon");
            }
            if (map.get("largeNotificationIcon") instanceof Integer) {
                this.largeNotificationIcon = (Integer) map.get("largeNotificationIcon");
            }
            if (map.get("notificationSound") instanceof Integer) {
                this.notificationSound = (Integer) map.get("notificationSound");
            }
            if (map.get("font") instanceof String) {
                this.fontPath = (String) map.get("font");
            }
            if (map.get(SDKConfigurationDM.SDK_TYPE) instanceof String) {
                this.sdkType = (String) map.get(SDKConfigurationDM.SDK_TYPE);
            }
            if (map.get(SDKConfigurationDM.PLUGIN_VERSION) instanceof String) {
                this.pluginVersion = (String) map.get(SDKConfigurationDM.PLUGIN_VERSION);
            }
            if (map.get(SDKConfigurationDM.RUNTIME_VERSION) instanceof String) {
                this.runtimeVersion = (String) map.get(SDKConfigurationDM.RUNTIME_VERSION);
            }
            if (map.get(SDKConfigurationDM.SUPPORT_NOTIFICATION_CHANNEL_ID) instanceof String) {
                this.supportNotificationChannelId = (String) map.get(SDKConfigurationDM.SUPPORT_NOTIFICATION_CHANNEL_ID);
            }
            return this;
        }

        public RootInstallConfig build() {
            return new RootInstallConfig(this.enableInAppNotification, this.enableDefaultFallbackLanguage, this.enableInboxPolling, this.enableNotificationMute, this.disableHelpshiftBranding, this.disableAnimations, this.disableErrorLogging, this.disableAppLaunchEvent, this.notificationIcon, this.largeNotificationIcon, this.notificationSound, this.fontPath, this.sdkType, this.pluginVersion, this.runtimeVersion, this.supportNotificationChannelId);
        }
    }
}
