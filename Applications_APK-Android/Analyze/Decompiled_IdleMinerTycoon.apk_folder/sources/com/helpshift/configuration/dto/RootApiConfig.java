package com.helpshift.configuration.dto;

import com.helpshift.common.StringUtils;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import java.util.Map;

public class RootApiConfig {
    public final String conversationPrefillText;
    public final EnableContactUs enableContactUs;
    public final Boolean enableDefaultConversationalFiling;
    public final Boolean enableFullPrivacy;
    public final Boolean enableTypingIndicator;
    public final Boolean gotoConversationAfterContactUs;
    public final Boolean hideNameAndEmail;
    public final String initialUserMessageToAutoSend;
    public final Boolean requireEmail;
    public final Boolean showConversationInfoScreen;
    public final Boolean showConversationResolutionQuestion;
    public final Boolean showSearchOnNewConversation;

    public RootApiConfig(Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4, Boolean bool5, Boolean bool6, EnableContactUs enableContactUs2, String str, Boolean bool7, Boolean bool8, Boolean bool9, String str2) {
        this.enableContactUs = enableContactUs2;
        this.gotoConversationAfterContactUs = bool;
        this.requireEmail = bool2;
        this.hideNameAndEmail = bool3;
        this.conversationPrefillText = str;
        this.enableFullPrivacy = bool4;
        this.showSearchOnNewConversation = bool5;
        this.showConversationResolutionQuestion = bool6;
        this.showConversationInfoScreen = bool7;
        this.enableTypingIndicator = bool8;
        this.enableDefaultConversationalFiling = bool9;
        this.initialUserMessageToAutoSend = str2;
    }

    public enum EnableContactUs {
        ALWAYS(0),
        NEVER(1),
        AFTER_VIEWING_FAQS(2),
        AFTER_MARKING_ANSWER_UNHELPFUL(3);
        
        private final int value;

        private EnableContactUs(int i) {
            this.value = i;
        }

        public static EnableContactUs fromInt(int i) {
            for (EnableContactUs enableContactUs : values()) {
                if (enableContactUs.getValue() == i) {
                    return enableContactUs;
                }
            }
            return null;
        }

        public int getValue() {
            return this.value;
        }
    }

    public static class RootApiConfigBuilder {
        private String conversationPrefillText = "";
        private EnableContactUs enableContactUs;
        private Boolean enableDefaultConversationalFiling;
        private Boolean enableFullPrivacy;
        private Boolean enableTypingIndicator;
        private Boolean gotoConversationAfterContactUs;
        private Boolean hideNameAndEmail;
        private String initialUserMessageToAutoSend = "";
        private Boolean requireEmail;
        private Boolean showConversationInfoScreen;
        private Boolean showConversationResolutionQuestion;
        private Boolean showSearchOnNewConversation;

        public RootApiConfigBuilder applyMap(Map<String, Object> map) {
            if (map.get(SDKConfigurationDM.ENABLE_CONTACT_US) instanceof Integer) {
                this.enableContactUs = EnableContactUs.fromInt(((Integer) map.get(SDKConfigurationDM.ENABLE_CONTACT_US)).intValue());
            }
            if (map.get(SDKConfigurationDM.GOTO_CONVERSATION_AFTER_CONTACT_US) instanceof Boolean) {
                this.gotoConversationAfterContactUs = (Boolean) map.get(SDKConfigurationDM.GOTO_CONVERSATION_AFTER_CONTACT_US);
            } else if (map.get("gotoCoversationAfterContactUs") instanceof Boolean) {
                this.gotoConversationAfterContactUs = (Boolean) map.get("gotoCoversationAfterContactUs");
            }
            if (map.get(SDKConfigurationDM.REQUIRE_EMAIL) instanceof Boolean) {
                this.requireEmail = (Boolean) map.get(SDKConfigurationDM.REQUIRE_EMAIL);
            }
            if (map.get(SDKConfigurationDM.HIDE_NAME_AND_EMAIL) instanceof Boolean) {
                this.hideNameAndEmail = (Boolean) map.get(SDKConfigurationDM.HIDE_NAME_AND_EMAIL);
            }
            if (map.get("enableFullPrivacy") instanceof Boolean) {
                this.enableFullPrivacy = (Boolean) map.get("enableFullPrivacy");
            }
            if (map.get(SDKConfigurationDM.SHOW_SEARCH_ON_NEW_CONVERSATION) instanceof Boolean) {
                this.showSearchOnNewConversation = (Boolean) map.get(SDKConfigurationDM.SHOW_SEARCH_ON_NEW_CONVERSATION);
            }
            if (map.get(SDKConfigurationDM.SHOW_CONVERSATION_RESOLUTION_QUESTION_API) instanceof Boolean) {
                this.showConversationResolutionQuestion = (Boolean) map.get(SDKConfigurationDM.SHOW_CONVERSATION_RESOLUTION_QUESTION_API);
            }
            if (map.get(SDKConfigurationDM.CONVERSATION_PRE_FILL_TEXT) instanceof String) {
                String str = (String) map.get(SDKConfigurationDM.CONVERSATION_PRE_FILL_TEXT);
                if (!StringUtils.isEmpty(str)) {
                    this.conversationPrefillText = str;
                }
            }
            if (map.get(SDKConfigurationDM.SHOW_CONVERSATION_INFO_SCREEN) instanceof Boolean) {
                this.showConversationInfoScreen = (Boolean) map.get(SDKConfigurationDM.SHOW_CONVERSATION_INFO_SCREEN);
            }
            if (map.get(SDKConfigurationDM.ENABLE_TYPING_INDICATOR) instanceof Boolean) {
                this.enableTypingIndicator = (Boolean) map.get(SDKConfigurationDM.ENABLE_TYPING_INDICATOR);
            }
            if (map.get(SDKConfigurationDM.ENABLE_DEFAULT_CONVERSATIONAL_FILING) instanceof Boolean) {
                this.enableDefaultConversationalFiling = (Boolean) map.get(SDKConfigurationDM.ENABLE_DEFAULT_CONVERSATIONAL_FILING);
            }
            if (map.get("initialUserMessage") instanceof String) {
                String str2 = (String) map.get("initialUserMessage");
                if (!StringUtils.isEmpty(str2)) {
                    this.initialUserMessageToAutoSend = str2.trim();
                }
            }
            return this;
        }

        public RootApiConfig build() {
            return new RootApiConfig(this.gotoConversationAfterContactUs, this.requireEmail, this.hideNameAndEmail, this.enableFullPrivacy, this.showSearchOnNewConversation, this.showConversationResolutionQuestion, this.enableContactUs, this.conversationPrefillText, this.showConversationInfoScreen, this.enableTypingIndicator, this.enableDefaultConversationalFiling, this.initialUserMessageToAutoSend);
        }
    }
}
