package com.helpshift.configuration.domainmodel;

import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.account.domainmodel.UserManagerDM;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.network.ETagNetwork;
import com.helpshift.common.domain.network.GETNetwork;
import com.helpshift.common.domain.network.GuardOKNetwork;
import com.helpshift.common.domain.network.NetworkConstants;
import com.helpshift.common.domain.network.NetworkDataRequestUtil;
import com.helpshift.common.domain.network.NetworkErrorCodes;
import com.helpshift.common.domain.network.TSCorrectedNetwork;
import com.helpshift.common.exception.NetworkException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.platform.KVStore;
import com.helpshift.common.platform.Platform;
import com.helpshift.common.platform.network.RequestData;
import com.helpshift.common.platform.network.Response;
import com.helpshift.common.platform.network.ResponseParser;
import com.helpshift.configuration.dto.RootApiConfig;
import com.helpshift.configuration.dto.RootInstallConfig;
import com.helpshift.configuration.response.PeriodicReview;
import com.helpshift.configuration.response.RootServerConfig;
import com.helpshift.util.HSLogger;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;

public class SDKConfigurationDM extends Observable {
    public static final String ALLOW_USER_ATTACHMENTS = "allowUserAttachments";
    public static final String API_KEY = "apiKey";
    public static final String APP_REVIEWED = "app_reviewed";
    public static final String AUTO_FILL_FIRST_PREISSUE_MESSAGE = "autoFillFirstPreIssueMessage";
    public static final String BREADCRUMB_LIMIT = "breadcrumbLimit";
    public static final String CONVERSATIONAL_ISSUE_FILING = "conversationalIssueFiling";
    public static final String CONVERSATION_GREETING_MESSAGE = "conversationGreetingMessage";
    public static final String CONVERSATION_PRE_FILL_TEXT = "conversationPrefillText";
    public static final String CUSTOMER_SATISFACTION_SURVEY = "customerSatisfactionSurvey";
    public static final String DEBUG_LOG_LIMIT = "debugLogLimit";
    public static final String DEFAULT_FALLBACK_LANGUAGE_ENABLE = "defaultFallbackLanguageEnable";
    public static final String DISABLE_ANIMATION = "disableAnimations";
    public static final String DISABLE_APP_LAUNCH_EVENT = "disableAppLaunchEvent";
    public static final String DISABLE_ERROR_LOGGING = "disableErrorLogging";
    public static final String DISABLE_IN_APP_CONVERSATION = "disableInAppConversation";
    public static final String DOMAIN_NAME = "domainName";
    public static final String ENABLE_CONTACT_US = "enableContactUs";
    public static final String ENABLE_DEFAULT_CONVERSATIONAL_FILING = "enableDefaultConversationalFiling";
    public static final String ENABLE_FULL_PRIVACY = "fullPrivacy";
    public static final String ENABLE_IN_APP_NOTIFICATION = "enableInAppNotification";
    public static final String ENABLE_TYPING_INDICATOR = "enableTypingIndicator";
    public static final String ENABLE_TYPING_INDICATOR_AGENT = "enableTypingIndicatorAgent";
    public static final String FONT_PATH = "fontPath";
    public static final String GOTO_CONVERSATION_AFTER_CONTACT_US = "gotoConversationAfterContactUs";
    public static final String HELPSHIFT_BRANDING_DISABLE_AGENT = "disableHelpshiftBrandingAgent";
    public static final String HELPSHIFT_BRANDING_DISABLE_INSTALL = "disableHelpshiftBranding";
    public static final String HIDE_NAME_AND_EMAIL = "hideNameAndEmail";
    public static final String INBOX_POLLING_ENABLE = "inboxPollingEnable";
    public static final String INITIAL_USER_MESSAGE_TO_AUTOSEND_IN_PREISSUE = "initialUserMessageToAutoSendInPreissue";
    public static final String LAST_SUCCESSFUL_CONFIG_FETCH_TIME = "lastSuccessfulConfigFetchTime";
    private static final Long MINIMUM_PERIODIC_FETCH_INTERVAL = 60L;
    private static final Long MINIMUM_PREISSUE_RESET_INTERVAL = 43200L;
    public static final String NOTIFICATION_ICON_ID = "notificationIconId";
    public static final String NOTIFICATION_LARGE_ICON_ID = "notificationLargeIconId";
    public static final String NOTIFICATION_MUTE_ENABLE = "notificationMute";
    public static final String NOTIFICATION_SOUND_ID = "notificationSoundId";
    public static final String PERIODIC_FETCH_INTERVAL = "periodicFetchInterval";
    public static final String PERIODIC_REVIEW_ENABLED = "periodicReviewEnabled";
    public static final String PERIODIC_REVIEW_INTERVAL = "periodicReviewInterval";
    public static final String PERIODIC_REVIEW_TYPE = "periodicReviewType";
    public static final String PLATFORM_ID = "platformId";
    public static final String PLUGIN_VERSION = "pluginVersion";
    public static final String PREISSUE_RESET_INTERVAL = "preissueResetInterval";
    public static final String PROFILE_FORM_ENABLE = "profileFormEnable";
    public static final String REQUIRE_EMAIL = "requireEmail";
    public static final String REQUIRE_NAME_AND_EMAIL = "requireNameAndEmail";
    public static final String REVIEW_URL = "reviewUrl";
    public static final String RUNTIME_VERSION = "runtimeVersion";
    public static final String SDK_LANGUAGE = "sdkLanguage";
    public static final String SDK_TYPE = "sdkType";
    public static final String SHOULD_SHOW_CONVERSATION_HISTORY_AGENT = "showConversationHistoryAgent";
    public static final String SHOW_AGENT_NAME = "showAgentName";
    public static final String SHOW_CONVERSATION_INFO_SCREEN = "showConversationInfoScreen";
    public static final String SHOW_CONVERSATION_RESOLUTION_QUESTION_AGENT = "showConversationResolutionQuestionAgent";
    public static final String SHOW_CONVERSATION_RESOLUTION_QUESTION_API = "showConversationResolutionQuestion";
    public static final String SHOW_SEARCH_ON_NEW_CONVERSATION = "showSearchOnNewConversation";
    public static final String SUPPORT_NOTIFICATION_CHANNEL_ID = "supportNotificationChannelId";
    private static final String TAG = "Helpshift_SDKConfigDM";
    private final Domain domain;
    private final KVStore kvStore;
    private final Platform platform;
    private final ResponseParser responseParser;

    public SDKConfigurationDM(Domain domain2, Platform platform2) {
        this.domain = domain2;
        this.platform = platform2;
        this.responseParser = platform2.getResponseParser();
        this.kvStore = platform2.getKVStore();
    }

    public RootServerConfig fetchServerConfig(UserManagerDM userManagerDM) {
        UserDM activeUser = userManagerDM.getActiveUser();
        String str = NetworkConstants.SUPPORT_CONFIG_ROUTE;
        try {
            Response makeRequest = new GuardOKNetwork(new ETagNetwork(new TSCorrectedNetwork(new GETNetwork(str, this.domain, this.platform), this.platform), this.platform, str)).makeRequest(new RequestData(NetworkDataRequestUtil.getUserRequestData(userManagerDM)));
            if (makeRequest.responseString == null) {
                HSLogger.d(TAG, "SDK config data fetched but nothing to update.");
                updateLastSuccessfulConfigFetchTime();
                return null;
            }
            HSLogger.d(TAG, "SDK config data updated successfully");
            RootServerConfig parseConfigResponse = this.responseParser.parseConfigResponse(makeRequest.responseString);
            updateServerConfig(parseConfigResponse);
            updateUserConfig(activeUser, parseConfigResponse, userManagerDM);
            updateLastSuccessfulConfigFetchTime();
            return parseConfigResponse;
        } catch (RootAPIException e) {
            if ((e.exceptionType instanceof NetworkException) && ((NetworkException) e.exceptionType).serverStatusCode == NetworkErrorCodes.CONTENT_UNCHANGED.intValue()) {
                updateLastSuccessfulConfigFetchTime();
            }
            throw e;
        }
    }

    private void updateUserConfig(UserDM userDM, RootServerConfig rootServerConfig, UserManagerDM userManagerDM) {
        userManagerDM.updateIssueExists(userDM, rootServerConfig.issueExists);
    }

    public void updateServerConfig(RootServerConfig rootServerConfig) {
        HashMap hashMap = new HashMap();
        hashMap.put(REQUIRE_NAME_AND_EMAIL, Boolean.valueOf(rootServerConfig.requireNameAndEmail));
        hashMap.put(PROFILE_FORM_ENABLE, Boolean.valueOf(rootServerConfig.profileFormEnable));
        hashMap.put(SHOW_AGENT_NAME, Boolean.valueOf(rootServerConfig.showAgentName));
        hashMap.put(CUSTOMER_SATISFACTION_SURVEY, Boolean.valueOf(rootServerConfig.customerSatisfactionSurvey));
        hashMap.put(DISABLE_IN_APP_CONVERSATION, Boolean.valueOf(rootServerConfig.disableInAppConversation));
        hashMap.put(HELPSHIFT_BRANDING_DISABLE_AGENT, Boolean.valueOf(rootServerConfig.disableHelpshiftBranding));
        hashMap.put(DEBUG_LOG_LIMIT, Integer.valueOf(rootServerConfig.debugLogLimit));
        hashMap.put(BREADCRUMB_LIMIT, Integer.valueOf(rootServerConfig.breadcrumbLimit));
        hashMap.put(REVIEW_URL, rootServerConfig.reviewUrl);
        PeriodicReview periodicReview = rootServerConfig.periodicReview;
        if (periodicReview == null) {
            periodicReview = new PeriodicReview(false, 0, null);
        }
        hashMap.put(PERIODIC_REVIEW_ENABLED, Boolean.valueOf(periodicReview.isEnabled));
        hashMap.put(PERIODIC_REVIEW_INTERVAL, Integer.valueOf(periodicReview.interval));
        hashMap.put(PERIODIC_REVIEW_TYPE, periodicReview.type);
        hashMap.put(CONVERSATION_GREETING_MESSAGE, rootServerConfig.conversationGreetingMessage);
        hashMap.put(CONVERSATIONAL_ISSUE_FILING, Boolean.valueOf(rootServerConfig.conversationalIssueFiling));
        hashMap.put(ENABLE_TYPING_INDICATOR_AGENT, Boolean.valueOf(rootServerConfig.enableTypingIndicator));
        hashMap.put(SHOW_CONVERSATION_RESOLUTION_QUESTION_AGENT, Boolean.valueOf(rootServerConfig.showConversationResolutionQuestion));
        hashMap.put(SHOULD_SHOW_CONVERSATION_HISTORY_AGENT, Boolean.valueOf(rootServerConfig.shouldShowConversationHistory));
        hashMap.put(ALLOW_USER_ATTACHMENTS, Boolean.valueOf(rootServerConfig.allowUserAttachments));
        hashMap.put(PERIODIC_FETCH_INTERVAL, Long.valueOf(rootServerConfig.periodicFetchInterval));
        hashMap.put(PREISSUE_RESET_INTERVAL, Long.valueOf(rootServerConfig.preissueResetInterval));
        hashMap.put(AUTO_FILL_FIRST_PREISSUE_MESSAGE, Boolean.valueOf(rootServerConfig.autoFillFirstPreissueMessage));
        this.kvStore.setKeyValues(hashMap);
        setChanged();
        notifyObservers();
    }

    private void updateLastSuccessfulConfigFetchTime() {
        this.kvStore.setLong(LAST_SUCCESSFUL_CONFIG_FETCH_TIME, Long.valueOf(System.currentTimeMillis() / 1000));
    }

    public Long getLastSuccessfulConfigFetchTime() {
        return this.kvStore.getLong(LAST_SUCCESSFUL_CONFIG_FETCH_TIME, 0L);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean getBoolean(String str) {
        char c;
        boolean z = true;
        switch (str.hashCode()) {
            case -1703140188:
                if (str.equals(CONVERSATIONAL_ISSUE_FILING)) {
                    c = 7;
                    break;
                }
                c = 65535;
                break;
            case -591814160:
                if (str.equals(PROFILE_FORM_ENABLE)) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case -423624397:
                if (str.equals(SHOW_CONVERSATION_RESOLUTION_QUESTION_AGENT)) {
                    c = 4;
                    break;
                }
                c = 65535;
                break;
            case -366496336:
                if (str.equals(ENABLE_TYPING_INDICATOR_AGENT)) {
                    c = 5;
                    break;
                }
                c = 65535;
                break;
            case -338380156:
                if (str.equals(ENABLE_IN_APP_NOTIFICATION)) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case 1262906910:
                if (str.equals(DEFAULT_FALLBACK_LANGUAGE_ENABLE)) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case 1423623260:
                if (str.equals(ALLOW_USER_ATTACHMENTS)) {
                    c = 6;
                    break;
                }
                c = 65535;
                break;
            case 1952413875:
                if (str.equals(SHOW_AGENT_NAME)) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                break;
            case 7:
                z = this.kvStore.getBoolean(ENABLE_DEFAULT_CONVERSATIONAL_FILING, false).booleanValue();
                break;
            default:
                z = false;
                break;
        }
        return this.kvStore.getBoolean(str, Boolean.valueOf(z)).booleanValue();
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Integer getInt(java.lang.String r3) {
        /*
            r2 = this;
            int r0 = r3.hashCode()
            r1 = -71624118(0xfffffffffbbb1a4a, float:-1.9429854E36)
            if (r0 == r1) goto L_0x0019
            r1 = 1384494456(0x5285b578, float:2.87137595E11)
            if (r0 == r1) goto L_0x000f
            goto L_0x0023
        L_0x000f:
            java.lang.String r0 = "breadcrumbLimit"
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0023
            r0 = 1
            goto L_0x0024
        L_0x0019:
            java.lang.String r0 = "debugLogLimit"
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0023
            r0 = 0
            goto L_0x0024
        L_0x0023:
            r0 = -1
        L_0x0024:
            switch(r0) {
                case 0: goto L_0x0029;
                case 1: goto L_0x0029;
                default: goto L_0x0027;
            }
        L_0x0027:
            r0 = 0
            goto L_0x002f
        L_0x0029:
            r0 = 100
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
        L_0x002f:
            com.helpshift.common.platform.KVStore r1 = r2.kvStore
            java.lang.Integer r3 = r1.getInt(r3, r0)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.configuration.domainmodel.SDKConfigurationDM.getInt(java.lang.String):java.lang.Integer");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getString(java.lang.String r3) {
        /*
            r2 = this;
            int r0 = r3.hashCode()
            r1 = -340534862(0xffffffffebb3d9b2, float:-4.348515E26)
            if (r0 == r1) goto L_0x0028
            r1 = 493025015(0x1d62f6f7, float:3.0038529E-21)
            if (r0 == r1) goto L_0x001e
            r1 = 1948062356(0x741d1294, float:4.9778285E31)
            if (r0 == r1) goto L_0x0014
            goto L_0x0032
        L_0x0014:
            java.lang.String r0 = "sdkType"
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0032
            r0 = 2
            goto L_0x0033
        L_0x001e:
            java.lang.String r0 = "reviewUrl"
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0032
            r0 = 0
            goto L_0x0033
        L_0x0028:
            java.lang.String r0 = "sdkLanguage"
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0032
            r0 = 1
            goto L_0x0033
        L_0x0032:
            r0 = -1
        L_0x0033:
            switch(r0) {
                case 0: goto L_0x003b;
                case 1: goto L_0x003b;
                case 2: goto L_0x0038;
                default: goto L_0x0036;
            }
        L_0x0036:
            r0 = 0
            goto L_0x003d
        L_0x0038:
            java.lang.String r0 = "android"
            goto L_0x003d
        L_0x003b:
            java.lang.String r0 = ""
        L_0x003d:
            com.helpshift.common.platform.KVStore r1 = r2.kvStore
            java.lang.String r3 = r1.getString(r3, r0)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.configuration.domainmodel.SDKConfigurationDM.getString(java.lang.String):java.lang.String");
    }

    public PeriodicReview getPeriodicReview() {
        return new PeriodicReview(this.kvStore.getBoolean(PERIODIC_REVIEW_ENABLED, false).booleanValue(), this.kvStore.getInt(PERIODIC_REVIEW_INTERVAL, 0).intValue(), this.kvStore.getString(PERIODIC_REVIEW_TYPE, ""));
    }

    public void updateInstallConfig(Map<String, Object> map) {
        updateInstallConfig(new RootInstallConfig.RootInstallConfigBuilder().applyMap(map).build());
    }

    public void updateInstallConfig(RootInstallConfig rootInstallConfig) {
        HashMap hashMap = new HashMap();
        hashMap.put(SUPPORT_NOTIFICATION_CHANNEL_ID, rootInstallConfig.supportNotificationChannelId);
        hashMap.put(FONT_PATH, rootInstallConfig.fontPath);
        HashMap hashMap2 = new HashMap();
        hashMap2.put(ENABLE_IN_APP_NOTIFICATION, rootInstallConfig.enableInAppNotification);
        hashMap2.put(DEFAULT_FALLBACK_LANGUAGE_ENABLE, rootInstallConfig.enableDefaultFallbackLanguage);
        hashMap2.put(INBOX_POLLING_ENABLE, rootInstallConfig.enableInboxPolling);
        hashMap2.put(NOTIFICATION_MUTE_ENABLE, rootInstallConfig.enableNotificationMute);
        hashMap2.put(DISABLE_ANIMATION, rootInstallConfig.disableAnimations);
        hashMap2.put("disableHelpshiftBranding", rootInstallConfig.disableHelpshiftBranding);
        hashMap2.put(DISABLE_ERROR_LOGGING, rootInstallConfig.disableErrorLogging);
        hashMap2.put(DISABLE_APP_LAUNCH_EVENT, rootInstallConfig.disableAppLaunchEvent);
        hashMap2.put(NOTIFICATION_SOUND_ID, rootInstallConfig.notificationSound);
        hashMap2.put(NOTIFICATION_ICON_ID, rootInstallConfig.notificationIcon);
        hashMap2.put(NOTIFICATION_LARGE_ICON_ID, rootInstallConfig.largeNotificationIcon);
        hashMap2.put(SDK_TYPE, rootInstallConfig.sdkType);
        hashMap2.put(PLUGIN_VERSION, rootInstallConfig.pluginVersion);
        hashMap2.put(RUNTIME_VERSION, rootInstallConfig.runtimeVersion);
        removeNullValues(hashMap2);
        hashMap2.putAll(hashMap);
        this.kvStore.setKeyValues(hashMap2);
    }

    public boolean isHelpshiftBrandingDisabled() {
        if (this.kvStore.getBoolean("disableHelpshiftBranding", false).booleanValue() || this.kvStore.getBoolean(HELPSHIFT_BRANDING_DISABLE_AGENT, false).booleanValue()) {
            return true;
        }
        return false;
    }

    public boolean shouldShowConversationResolutionQuestion() {
        return getBoolean(SHOW_CONVERSATION_RESOLUTION_QUESTION_AGENT) || getBoolean(SHOW_CONVERSATION_RESOLUTION_QUESTION_API);
    }

    public void updateApiConfig(Map<String, Object> map) {
        updateApiConfig(new RootApiConfig.RootApiConfigBuilder().applyMap(map).build());
    }

    public void updateApiConfig(RootApiConfig rootApiConfig) {
        HashMap hashMap = new HashMap();
        hashMap.put(CONVERSATION_PRE_FILL_TEXT, rootApiConfig.conversationPrefillText);
        hashMap.put(INITIAL_USER_MESSAGE_TO_AUTOSEND_IN_PREISSUE, rootApiConfig.initialUserMessageToAutoSend);
        HashMap hashMap2 = new HashMap();
        hashMap2.put(ENABLE_FULL_PRIVACY, rootApiConfig.enableFullPrivacy);
        hashMap2.put(HIDE_NAME_AND_EMAIL, rootApiConfig.hideNameAndEmail);
        hashMap2.put(REQUIRE_EMAIL, rootApiConfig.requireEmail);
        hashMap2.put(SHOW_SEARCH_ON_NEW_CONVERSATION, rootApiConfig.showSearchOnNewConversation);
        hashMap2.put(GOTO_CONVERSATION_AFTER_CONTACT_US, rootApiConfig.gotoConversationAfterContactUs);
        hashMap2.put(SHOW_CONVERSATION_RESOLUTION_QUESTION_API, rootApiConfig.showConversationResolutionQuestion);
        hashMap2.put(SHOW_CONVERSATION_INFO_SCREEN, rootApiConfig.showConversationInfoScreen);
        hashMap2.put(ENABLE_TYPING_INDICATOR, rootApiConfig.enableTypingIndicator);
        if (rootApiConfig.enableContactUs != null) {
            hashMap2.put(ENABLE_CONTACT_US, Integer.valueOf(rootApiConfig.enableContactUs.getValue()));
        }
        hashMap2.put(ENABLE_DEFAULT_CONVERSATIONAL_FILING, rootApiConfig.enableDefaultConversationalFiling);
        removeNullValues(hashMap2);
        hashMap2.putAll(hashMap);
        this.kvStore.setKeyValues(hashMap2);
    }

    public RootApiConfig.EnableContactUs getEnableContactUs() {
        return RootApiConfig.EnableContactUs.fromInt(this.kvStore.getInt(ENABLE_CONTACT_US, 0).intValue());
    }

    public void setAppReviewed(boolean z) {
        this.kvStore.setBoolean(APP_REVIEWED, Boolean.valueOf(z));
    }

    public void setSdkLanguage(String str) {
        this.kvStore.setString(SDK_LANGUAGE, str);
    }

    public boolean shouldEnableTypingIndicator() {
        return getBoolean(ENABLE_TYPING_INDICATOR_AGENT) || getBoolean(ENABLE_TYPING_INDICATOR);
    }

    public boolean shouldCreateConversationAnonymously() {
        return getBoolean(ENABLE_FULL_PRIVACY) || ((!getBoolean(REQUIRE_NAME_AND_EMAIL) || !getBoolean(HIDE_NAME_AND_EMAIL)) && !getBoolean(PROFILE_FORM_ENABLE));
    }

    public int getMinimumConversationDescriptionLength() {
        return this.platform.getMinimumConversationDescriptionLength();
    }

    private void removeNullValues(Map<String, Serializable> map) {
        Iterator<Map.Entry<String, Serializable>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            if (it.next().getValue() == null) {
                it.remove();
            }
        }
    }

    public boolean shouldShowConversationHistory() {
        if (!getBoolean(SHOULD_SHOW_CONVERSATION_HISTORY_AGENT) || !getBoolean(CONVERSATIONAL_ISSUE_FILING)) {
            return false;
        }
        return !getBoolean(ENABLE_FULL_PRIVACY);
    }

    public long getPeriodicFetchInterval() {
        return Math.max(this.kvStore.getLong(PERIODIC_FETCH_INTERVAL, 0L).longValue(), MINIMUM_PERIODIC_FETCH_INTERVAL.longValue());
    }

    public long getPreissueResetInterval() {
        return Math.max(this.kvStore.getLong(PREISSUE_RESET_INTERVAL, 0L).longValue(), MINIMUM_PREISSUE_RESET_INTERVAL.longValue());
    }

    public boolean shouldAutoFillPreissueFirstMessage() {
        return this.kvStore.getBoolean(AUTO_FILL_FIRST_PREISSUE_MESSAGE, false).booleanValue();
    }
}
