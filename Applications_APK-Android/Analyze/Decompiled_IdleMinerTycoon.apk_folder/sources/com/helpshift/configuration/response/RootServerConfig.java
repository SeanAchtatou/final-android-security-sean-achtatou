package com.helpshift.configuration.response;

public class RootServerConfig {
    public final boolean allowUserAttachments;
    public final boolean autoFillFirstPreissueMessage;
    public final int breadcrumbLimit;
    public final String conversationGreetingMessage;
    public final boolean conversationalIssueFiling;
    public final boolean customerSatisfactionSurvey;
    public final int debugLogLimit;
    public final boolean disableHelpshiftBranding;
    public final boolean disableInAppConversation;
    public final boolean enableTypingIndicator;
    public final boolean issueExists;
    public final Long lastRedactionAt;
    public final long periodicFetchInterval;
    public final PeriodicReview periodicReview;
    public final long preissueResetInterval;
    public final Long profileCreatedAt;
    public final boolean profileFormEnable;
    public final boolean requireNameAndEmail;
    public final String reviewUrl;
    public final boolean shouldShowConversationHistory;
    public final boolean showAgentName;
    public final boolean showConversationResolutionQuestion;

    public RootServerConfig(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, int i, int i2, String str, PeriodicReview periodicReview2, boolean z8, String str2, boolean z9, boolean z10, boolean z11, Long l, Long l2, boolean z12, long j, long j2, boolean z13) {
        this.requireNameAndEmail = z;
        this.profileFormEnable = z2;
        this.showAgentName = z3;
        this.customerSatisfactionSurvey = z4;
        this.disableInAppConversation = z5;
        this.disableHelpshiftBranding = z6;
        this.issueExists = z7;
        this.debugLogLimit = i;
        this.breadcrumbLimit = i2;
        this.reviewUrl = str;
        this.periodicReview = periodicReview2;
        this.conversationalIssueFiling = z8;
        this.conversationGreetingMessage = str2;
        this.enableTypingIndicator = z9;
        this.showConversationResolutionQuestion = z10;
        this.shouldShowConversationHistory = z11;
        this.lastRedactionAt = l;
        this.profileCreatedAt = l2;
        this.allowUserAttachments = z12;
        this.periodicFetchInterval = j;
        this.preissueResetInterval = j2;
        this.autoFillFirstPreissueMessage = z13;
    }
}
