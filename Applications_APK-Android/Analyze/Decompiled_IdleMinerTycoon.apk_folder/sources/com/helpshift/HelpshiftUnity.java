package com.helpshift;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.helpshift.campaigns.Inbox;
import com.helpshift.campaigns.delegates.InboxPushNotificationDelegate;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.util.HelpshiftUtil;
import java.util.HashMap;
import java.util.Map;

public class HelpshiftUnity {
    private static final String TAG = "Helpshift_UnityJava";
    private static final String UNITY_PLUGIN_VERSION = "5.1.0";

    public static void install(Application application, String str, String str2, String str3, Map<String, Object> map) throws Exception {
        if (map == null) {
            map = new HashMap<>();
        }
        map.put(SDKConfigurationDM.SDK_TYPE, "unity");
        map.put(SDKConfigurationDM.PLUGIN_VERSION, UNITY_PLUGIN_VERSION);
        HelpshiftUnityAPI.install(application, str, str2, str3, map);
    }

    public static void handlePush(Context context, Intent intent) {
        HelpshiftUtil.installWithCachedCreds(context);
        HelpshiftUnityAPI.handlePush(context, intent);
    }

    public static void handlePush(Context context, Bundle bundle) {
        HelpshiftUtil.installWithCachedCreds(context);
        HelpshiftUnityAPI.handlePush(context, bundle);
    }

    public static void handlePush(Context context, Map<String, String> map) {
        HelpshiftUtil.installWithCachedCreds(context);
        HelpshiftUnityAPI.handlePush(context, map);
    }

    public static void setInboxPushNotificationDelegate(InboxPushNotificationDelegate inboxPushNotificationDelegate) {
        Inbox.getInstance().setInboxPushNotificationDelegate(inboxPushNotificationDelegate);
    }

    public static void registerDeviceToken(Context context, String str) {
        Core.registerDeviceToken(context, str);
    }
}
