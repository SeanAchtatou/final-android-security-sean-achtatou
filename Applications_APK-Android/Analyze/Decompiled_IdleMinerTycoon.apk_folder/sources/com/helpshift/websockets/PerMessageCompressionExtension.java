package com.helpshift.websockets;

abstract class PerMessageCompressionExtension extends WebSocketExtension {
    /* access modifiers changed from: protected */
    public abstract byte[] compress(byte[] bArr) throws WebSocketException;

    /* access modifiers changed from: protected */
    public abstract byte[] decompress(byte[] bArr) throws WebSocketException;

    public PerMessageCompressionExtension(String str) {
        super(str);
    }
}
