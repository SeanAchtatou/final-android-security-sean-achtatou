package com.helpshift.websockets;

class CounterPayloadGenerator implements PayloadGenerator {
    private long mCount;

    CounterPayloadGenerator() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public byte[] generate() {
        this.mCount = Math.max(this.mCount + 1, 1L);
        return Misc.getBytesUTF8(String.valueOf(this.mCount));
    }
}
