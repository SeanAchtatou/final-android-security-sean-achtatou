package com.helpshift.websockets;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

class SocketFactorySettings {
    private SSLContext mSSLContext;
    private SSLSocketFactory mSSLSocketFactory;
    private SocketFactory mSocketFactory;

    SocketFactorySettings() {
    }

    public SocketFactory getSocketFactory() {
        return this.mSocketFactory;
    }

    public void setSocketFactory(SocketFactory socketFactory) {
        this.mSocketFactory = socketFactory;
    }

    public SSLSocketFactory getSSLSocketFactory() {
        return this.mSSLSocketFactory;
    }

    public void setSSLSocketFactory(SSLSocketFactory sSLSocketFactory) {
        this.mSSLSocketFactory = sSLSocketFactory;
    }

    public SSLContext getSSLContext() {
        return this.mSSLContext;
    }

    public void setSSLContext(SSLContext sSLContext) {
        this.mSSLContext = sSLContext;
    }

    public SocketFactory selectSocketFactory(boolean z) {
        if (z) {
            if (this.mSSLContext != null) {
                return this.mSSLContext.getSocketFactory();
            }
            if (this.mSSLSocketFactory != null) {
                return this.mSSLSocketFactory;
            }
            return SSLSocketFactory.getDefault();
        } else if (this.mSocketFactory != null) {
            return this.mSocketFactory;
        } else {
            return SocketFactory.getDefault();
        }
    }
}
