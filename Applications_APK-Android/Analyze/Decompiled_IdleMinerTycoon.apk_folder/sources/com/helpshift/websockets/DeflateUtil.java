package com.helpshift.websockets;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.InputDeviceCompat;

class DeflateUtil {
    private static int[] INDICES_FROM_CODE_LENGTH_ORDER = {16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15};

    DeflateUtil() {
    }

    public static void readDynamicTables(ByteArray byteArray, int[] iArr, Huffman[] huffmanArr) throws FormatException {
        int readBits = byteArray.readBits(iArr, 5) + 257;
        int readBits2 = byteArray.readBits(iArr, 5) + 1;
        int readBits3 = byteArray.readBits(iArr, 4) + 4;
        int[] iArr2 = new int[19];
        for (int i = 0; i < readBits3; i++) {
            iArr2[INDICES_FROM_CODE_LENGTH_ORDER[i]] = (byte) byteArray.readBits(iArr, 3);
        }
        Huffman huffman = new Huffman(iArr2);
        int[] iArr3 = new int[readBits];
        readCodeLengths(byteArray, iArr, iArr3, huffman);
        Huffman huffman2 = new Huffman(iArr3);
        int[] iArr4 = new int[readBits2];
        readCodeLengths(byteArray, iArr, iArr4, huffman);
        Huffman huffman3 = new Huffman(iArr4);
        huffmanArr[0] = huffman2;
        huffmanArr[1] = huffman3;
    }

    private static void readCodeLengths(ByteArray byteArray, int[] iArr, int[] iArr2, Huffman huffman) throws FormatException {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        while (i4 < iArr2.length) {
            int readSym = huffman.readSym(byteArray, iArr);
            if (readSym < 0 || readSym > 15) {
                switch (readSym) {
                    case 16:
                        i2 = iArr2[i4 - 1];
                        i = byteArray.readBits(iArr, 2) + 3;
                        break;
                    case 17:
                        i3 = byteArray.readBits(iArr, 3) + 3;
                        i = i3;
                        i2 = 0;
                        break;
                    case 18:
                        i3 = byteArray.readBits(iArr, 7) + 11;
                        i = i3;
                        i2 = 0;
                        break;
                    default:
                        throw new FormatException(String.format("[%s] Bad code length '%d' at the bit index '%d'.", DeflateUtil.class.getSimpleName(), Integer.valueOf(readSym), iArr));
                }
                for (int i5 = 0; i5 < i; i5++) {
                    iArr2[i4 + i5] = i2;
                }
                i4 += i - 1;
            } else {
                iArr2[i4] = readSym;
            }
            i4++;
        }
    }

    public static int readLength(ByteArray byteArray, int[] iArr, int i) throws FormatException {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6 = 5;
        switch (i) {
            case 257:
            case 258:
            case 259:
            case 260:
            case 261:
            case 262:
            case 263:
            case 264:
                return i - 254;
            case 265:
                i3 = 11;
                i6 = 1;
                break;
            case 266:
                i3 = 13;
                i6 = 1;
                break;
            case 267:
                i3 = 15;
                i6 = 1;
                break;
            case 268:
                i3 = 17;
                i6 = 1;
                break;
            case 269:
                i4 = 19;
                i6 = 2;
                break;
            case 270:
                i4 = 23;
                i6 = 2;
                break;
            case 271:
                i4 = 27;
                i6 = 2;
                break;
            case 272:
                i4 = 31;
                i6 = 2;
                break;
            case 273:
                i5 = 35;
                i6 = 3;
                break;
            case 274:
                i5 = 43;
                i6 = 3;
                break;
            case 275:
                i5 = 51;
                i6 = 3;
                break;
            case 276:
                i5 = 59;
                i6 = 3;
                break;
            case 277:
                i2 = 67;
                i6 = 4;
                break;
            case 278:
                i2 = 83;
                i6 = 4;
                break;
            case 279:
                i2 = 99;
                i6 = 4;
                break;
            case 280:
                i2 = 115;
                i6 = 4;
                break;
            case 281:
                i2 = 131;
                break;
            case 282:
                i2 = 163;
                break;
            case 283:
                i2 = 195;
                break;
            case 284:
                i2 = 227;
                break;
            case 285:
                return 258;
            default:
                throw new FormatException(String.format("[%s] Bad literal/length code '%d' at the bit index '%d'.", DeflateUtil.class.getSimpleName(), Integer.valueOf(i), Integer.valueOf(iArr[0])));
        }
        return i2 + byteArray.readBits(iArr, i6);
    }

    public static int readDistance(ByteArray byteArray, int[] iArr, Huffman huffman) throws FormatException {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int readSym = huffman.readSym(byteArray, iArr);
        int i9 = 12;
        int i10 = 13;
        switch (readSym) {
            case 0:
            case 1:
            case 2:
            case 3:
                return readSym + 1;
            case 4:
                i9 = 1;
                i10 = 5;
                break;
            case 5:
                i9 = 1;
                i10 = 7;
                break;
            case 6:
                i9 = 2;
                i10 = 9;
                break;
            case 7:
                i9 = 2;
                break;
            case 8:
                i = 17;
                i9 = 3;
                break;
            case 9:
                i = 25;
                i9 = 3;
                break;
            case 10:
                i2 = 33;
                i9 = 4;
                break;
            case 11:
                i2 = 49;
                i9 = 4;
                break;
            case 12:
                i3 = 65;
                i9 = 5;
                break;
            case 13:
                i3 = 97;
                i9 = 5;
                break;
            case 14:
                i4 = 129;
                i9 = 6;
                break;
            case 15:
                i4 = 193;
                i9 = 6;
                break;
            case 16:
                i5 = 257;
                i9 = 7;
                break;
            case 17:
                i5 = 385;
                i9 = 7;
                break;
            case 18:
                i6 = InputDeviceCompat.SOURCE_DPAD;
                i9 = 8;
                break;
            case 19:
                i6 = 769;
                i9 = 8;
                break;
            case 20:
                i7 = 1025;
                i9 = 9;
                break;
            case 21:
                i7 = 1537;
                i9 = 9;
                break;
            case 22:
                i8 = 2049;
                i9 = 10;
                break;
            case 23:
                i8 = 3073;
                i9 = 10;
                break;
            case 24:
                i10 = FragmentTransaction.TRANSIT_FRAGMENT_OPEN;
                i9 = 11;
                break;
            case 25:
                i10 = 6145;
                i9 = 11;
                break;
            case 26:
                i10 = 8193;
                break;
            case 27:
                i10 = 12289;
                break;
            case 28:
                i9 = 13;
                i10 = 16385;
                break;
            case 29:
                i9 = 13;
                i10 = 24577;
                break;
            default:
                throw new FormatException(String.format("[%s] Bad distance code '%d' at the bit index '%d'.", DeflateUtil.class.getSimpleName(), Integer.valueOf(readSym), Integer.valueOf(iArr[0])));
        }
        return i10 + byteArray.readBits(iArr, i9);
    }
}
