package com.helpshift.websockets;

import java.io.IOException;
import java.net.Socket;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

class SocketConnector {
    private final Address mAddress;
    private final int mConnectionTimeout;
    private final String mHost;
    private final int mPort;
    private final ProxyHandshaker mProxyHandshaker;
    private final SSLSocketFactory mSSLSocketFactory;
    private Socket mSocket;

    SocketConnector(Socket socket, Address address, int i) {
        this(socket, address, i, null, null, null, 0);
    }

    SocketConnector(Socket socket, Address address, int i, ProxyHandshaker proxyHandshaker, SSLSocketFactory sSLSocketFactory, String str, int i2) {
        this.mSocket = socket;
        this.mAddress = address;
        this.mConnectionTimeout = i;
        this.mProxyHandshaker = proxyHandshaker;
        this.mSSLSocketFactory = sSLSocketFactory;
        this.mHost = str;
        this.mPort = i2;
    }

    public Socket getSocket() {
        return this.mSocket;
    }

    public int getConnectionTimeout() {
        return this.mConnectionTimeout;
    }

    public void connect() throws WebSocketException {
        try {
            doConnect();
        } catch (WebSocketException e) {
            try {
                this.mSocket.close();
            } catch (IOException unused) {
            }
            throw e;
        }
    }

    private void doConnect() throws WebSocketException {
        boolean z = this.mProxyHandshaker != null;
        try {
            this.mSocket.connect(this.mAddress.toInetSocketAddress(), this.mConnectionTimeout);
            if (this.mSocket instanceof SSLSocket) {
                verifyHostname((SSLSocket) this.mSocket, this.mAddress.getHostname());
            }
            if (z) {
                handshake();
            }
        } catch (IOException e) {
            Object[] objArr = new Object[3];
            objArr[0] = z ? "the proxy " : "";
            objArr[1] = this.mAddress;
            objArr[2] = e.getMessage();
            throw new WebSocketException(WebSocketError.SOCKET_CONNECT_ERROR, String.format("Failed to connect to %s'%s': %s", objArr), e);
        }
    }

    private void verifyHostname(SSLSocket sSLSocket, String str) throws HostnameUnverifiedException {
        if (!OkHostnameVerifier.INSTANCE.verify(str, sSLSocket.getSession())) {
            throw new HostnameUnverifiedException(sSLSocket, str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    private void handshake() throws WebSocketException {
        try {
            this.mProxyHandshaker.perform();
            if (this.mSSLSocketFactory != null) {
                try {
                    this.mSocket = this.mSSLSocketFactory.createSocket(this.mSocket, this.mHost, this.mPort, true);
                    try {
                        ((SSLSocket) this.mSocket).startHandshake();
                        if (this.mSocket instanceof SSLSocket) {
                            verifyHostname((SSLSocket) this.mSocket, this.mProxyHandshaker.getProxiedHostname());
                        }
                    } catch (IOException e) {
                        throw new WebSocketException(WebSocketError.SSL_HANDSHAKE_ERROR, String.format("SSL handshake with the WebSocket endpoint (%s) failed: %s", this.mAddress, e.getMessage()), e);
                    }
                } catch (IOException e2) {
                    throw new WebSocketException(WebSocketError.SOCKET_OVERLAY_ERROR, "Failed to overlay an existing socket: " + e2.getMessage(), e2);
                }
            }
        } catch (IOException e3) {
            throw new WebSocketException(WebSocketError.PROXY_HANDSHAKE_ERROR, String.format("Handshake with the proxy server (%s) failed: %s", this.mAddress, e3.getMessage()), e3);
        }
    }

    /* access modifiers changed from: package-private */
    public void closeSilently() {
        try {
            this.mSocket.close();
        } catch (Throwable unused) {
        }
    }
}
