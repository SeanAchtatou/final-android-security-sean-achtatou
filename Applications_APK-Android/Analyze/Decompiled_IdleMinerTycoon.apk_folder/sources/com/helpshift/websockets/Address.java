package com.helpshift.websockets;

import java.net.InetSocketAddress;

class Address {
    private final String mHost;
    private final int mPort;
    private transient String mString;

    Address(String str, int i) {
        this.mHost = str;
        this.mPort = i;
    }

    /* access modifiers changed from: package-private */
    public InetSocketAddress toInetSocketAddress() {
        return new InetSocketAddress(this.mHost, this.mPort);
    }

    /* access modifiers changed from: package-private */
    public String getHostname() {
        return this.mHost;
    }

    public String toString() {
        if (this.mString == null) {
            this.mString = String.format("%s:%d", this.mHost, Integer.valueOf(this.mPort));
        }
        return this.mString;
    }
}
