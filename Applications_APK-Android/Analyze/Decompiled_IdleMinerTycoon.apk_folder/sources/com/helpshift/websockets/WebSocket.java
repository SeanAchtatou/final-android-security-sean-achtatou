package com.helpshift.websockets;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.URI;
import java.util.List;
import java.util.Map;

public class WebSocket {
    private static final long DEFAULT_CLOSE_DELAY = 10000;
    private List<WebSocketExtension> mAgreedExtensions;
    private String mAgreedProtocol;
    private boolean mAutoFlush = true;
    private WebSocketFrame mClientCloseFrame;
    private boolean mExtended;
    private int mFrameQueueSize;
    private HandshakeBuilder mHandshakeBuilder;
    private WebSocketInputStream mInput;
    private final ListenerManager mListenerManager;
    private int mMaxPayloadSize;
    private boolean mMissingCloseFrameAllowed = true;
    private boolean mOnConnectedCalled;
    private Object mOnConnectedCalledLock = new Object();
    private WebSocketOutputStream mOutput;
    private PerMessageCompressionExtension mPerMessageCompressionExtension;
    private final PingSender mPingSender;
    private final PongSender mPongSender;
    private ReadingThread mReadingThread;
    private boolean mReadingThreadFinished;
    private boolean mReadingThreadStarted;
    private WebSocketFrame mServerCloseFrame;
    private Map<String, List<String>> mServerHeaders;
    private final SocketConnector mSocketConnector;
    private final StateManager mStateManager;
    private final Object mThreadsLock = new Object();
    private final WebSocketFactory mWebSocketFactory;
    private WritingThread mWritingThread;
    private boolean mWritingThreadFinished;
    private boolean mWritingThreadStarted;

    WebSocket(WebSocketFactory webSocketFactory, boolean z, String str, String str2, String str3, SocketConnector socketConnector) {
        this.mWebSocketFactory = webSocketFactory;
        this.mSocketConnector = socketConnector;
        this.mStateManager = new StateManager();
        this.mHandshakeBuilder = new HandshakeBuilder(z, str, str2, str3);
        this.mListenerManager = new ListenerManager(this);
        this.mPingSender = new PingSender(this, new CounterPayloadGenerator());
        this.mPongSender = new PongSender(this, new CounterPayloadGenerator());
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        if (isInState(WebSocketState.CREATED)) {
            finish();
        }
        super.finalize();
    }

    public WebSocketState getState() {
        WebSocketState state;
        synchronized (this.mStateManager) {
            state = this.mStateManager.getState();
        }
        return state;
    }

    public boolean isOpen() {
        return isInState(WebSocketState.OPEN);
    }

    private boolean isInState(WebSocketState webSocketState) {
        boolean z;
        synchronized (this.mStateManager) {
            z = this.mStateManager.getState() == webSocketState;
        }
        return z;
    }

    public WebSocket addProtocol(String str) {
        this.mHandshakeBuilder.addProtocol(str);
        return this;
    }

    public WebSocket removeProtocol(String str) {
        this.mHandshakeBuilder.removeProtocol(str);
        return this;
    }

    public WebSocket clearProtocols() {
        this.mHandshakeBuilder.clearProtocols();
        return this;
    }

    public WebSocket addExtension(String str) {
        this.mHandshakeBuilder.addExtension(str);
        return this;
    }

    public WebSocket removeExtension(WebSocketExtension webSocketExtension) {
        this.mHandshakeBuilder.removeExtension(webSocketExtension);
        return this;
    }

    public WebSocket clearExtensions() {
        this.mHandshakeBuilder.clearExtensions();
        return this;
    }

    public WebSocket addHeader(String str, String str2) {
        this.mHandshakeBuilder.addHeader(str, str2);
        return this;
    }

    public WebSocket removeHeaders(String str) {
        this.mHandshakeBuilder.removeHeaders(str);
        return this;
    }

    public WebSocket clearHeaders() {
        this.mHandshakeBuilder.clearHeaders();
        return this;
    }

    public WebSocket setUserInfo(String str) {
        this.mHandshakeBuilder.setUserInfo(str);
        return this;
    }

    public WebSocket setUserInfo(String str, String str2) {
        this.mHandshakeBuilder.setUserInfo(str, str2);
        return this;
    }

    public WebSocket clearUserInfo() {
        this.mHandshakeBuilder.clearUserInfo();
        return this;
    }

    public boolean isExtended() {
        return this.mExtended;
    }

    public WebSocket setExtended(boolean z) {
        this.mExtended = z;
        return this;
    }

    public boolean isAutoFlush() {
        return this.mAutoFlush;
    }

    public WebSocket setAutoFlush(boolean z) {
        this.mAutoFlush = z;
        return this;
    }

    public boolean isMissingCloseFrameAllowed() {
        return this.mMissingCloseFrameAllowed;
    }

    public WebSocket setMissingCloseFrameAllowed(boolean z) {
        this.mMissingCloseFrameAllowed = z;
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0014, code lost:
        r0 = r3.mWritingThread;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0016, code lost:
        if (r0 == null) goto L_0x001b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0018, code lost:
        r0.queueFlush();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001b, code lost:
        return r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.helpshift.websockets.WebSocket flush() {
        /*
            r3 = this;
            com.helpshift.websockets.StateManager r0 = r3.mStateManager
            monitor-enter(r0)
            com.helpshift.websockets.StateManager r1 = r3.mStateManager     // Catch:{ all -> 0x001c }
            com.helpshift.websockets.WebSocketState r1 = r1.getState()     // Catch:{ all -> 0x001c }
            com.helpshift.websockets.WebSocketState r2 = com.helpshift.websockets.WebSocketState.OPEN     // Catch:{ all -> 0x001c }
            if (r1 == r2) goto L_0x0013
            com.helpshift.websockets.WebSocketState r2 = com.helpshift.websockets.WebSocketState.CLOSING     // Catch:{ all -> 0x001c }
            if (r1 == r2) goto L_0x0013
            monitor-exit(r0)     // Catch:{ all -> 0x001c }
            return r3
        L_0x0013:
            monitor-exit(r0)     // Catch:{ all -> 0x001c }
            com.helpshift.websockets.WritingThread r0 = r3.mWritingThread
            if (r0 == 0) goto L_0x001b
            r0.queueFlush()
        L_0x001b:
            return r3
        L_0x001c:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x001c }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.websockets.WebSocket.flush():com.helpshift.websockets.WebSocket");
    }

    public int getFrameQueueSize() {
        return this.mFrameQueueSize;
    }

    public WebSocket setFrameQueueSize(int i) throws IllegalArgumentException {
        if (i >= 0) {
            this.mFrameQueueSize = i;
            return this;
        }
        throw new IllegalArgumentException("size must not be negative.");
    }

    public int getMaxPayloadSize() {
        return this.mMaxPayloadSize;
    }

    public WebSocket setMaxPayloadSize(int i) throws IllegalArgumentException {
        if (i >= 0) {
            this.mMaxPayloadSize = i;
            return this;
        }
        throw new IllegalArgumentException("size must not be negative.");
    }

    public long getPingInterval() {
        return this.mPingSender.getInterval();
    }

    public WebSocket setPingInterval(long j) {
        this.mPingSender.setInterval(j);
        return this;
    }

    public long getPongInterval() {
        return this.mPongSender.getInterval();
    }

    public WebSocket setPongInterval(long j) {
        this.mPongSender.setInterval(j);
        return this;
    }

    public PayloadGenerator getPingPayloadGenerator() {
        return this.mPingSender.getPayloadGenerator();
    }

    public WebSocket setPingPayloadGenerator(PayloadGenerator payloadGenerator) {
        this.mPingSender.setPayloadGenerator(payloadGenerator);
        return this;
    }

    public PayloadGenerator getPongPayloadGenerator() {
        return this.mPongSender.getPayloadGenerator();
    }

    public WebSocket setPongPayloadGenerator(PayloadGenerator payloadGenerator) {
        this.mPongSender.setPayloadGenerator(payloadGenerator);
        return this;
    }

    public WebSocket addListener(WebSocketListener webSocketListener) {
        this.mListenerManager.addListener(webSocketListener);
        return this;
    }

    public WebSocket addListeners(List<WebSocketListener> list) {
        this.mListenerManager.addListeners(list);
        return this;
    }

    public WebSocket removeListener(WebSocketListener webSocketListener) {
        this.mListenerManager.removeListener(webSocketListener);
        return this;
    }

    public WebSocket removeListeners(List<WebSocketListener> list) {
        this.mListenerManager.removeListeners(list);
        return this;
    }

    public WebSocket clearListeners() {
        this.mListenerManager.clearListeners();
        return this;
    }

    public Socket getSocket() {
        return this.mSocketConnector.getSocket();
    }

    public URI getURI() {
        return this.mHandshakeBuilder.getURI();
    }

    public WebSocket connect() throws WebSocketException {
        changeStateOnConnect();
        try {
            this.mSocketConnector.connect();
            this.mServerHeaders = shakeHands();
            this.mPerMessageCompressionExtension = findAgreedPerMessageCompressionExtension();
            this.mStateManager.setState(WebSocketState.OPEN);
            this.mListenerManager.callOnStateChanged(WebSocketState.OPEN);
            startThreads();
            return this;
        } catch (WebSocketException e) {
            this.mSocketConnector.closeSilently();
            this.mStateManager.setState(WebSocketState.CLOSED);
            this.mListenerManager.callOnStateChanged(WebSocketState.CLOSED);
            throw e;
        }
    }

    public WebSocket disconnect() {
        return disconnect(1000, null);
    }

    public WebSocket disconnect(int i) {
        return disconnect(i, null);
    }

    public WebSocket disconnect(String str) {
        return disconnect(1000, str);
    }

    public WebSocket disconnect(int i, String str) {
        return disconnect(i, str, 10000);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0030, code lost:
        if (r6 >= 0) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        r6 = 10000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0034, code lost:
        stopThreads(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0037, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0025, code lost:
        r3.mListenerManager.callOnStateChanged(com.helpshift.websockets.WebSocketState.CLOSING);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.helpshift.websockets.WebSocket disconnect(int r4, java.lang.String r5, long r6) {
        /*
            r3 = this;
            com.helpshift.websockets.StateManager r0 = r3.mStateManager
            monitor-enter(r0)
            int[] r1 = com.helpshift.websockets.WebSocket.AnonymousClass1.$SwitchMap$com$helpshift$websockets$WebSocketState     // Catch:{ all -> 0x003e }
            com.helpshift.websockets.StateManager r2 = r3.mStateManager     // Catch:{ all -> 0x003e }
            com.helpshift.websockets.WebSocketState r2 = r2.getState()     // Catch:{ all -> 0x003e }
            int r2 = r2.ordinal()     // Catch:{ all -> 0x003e }
            r1 = r1[r2]     // Catch:{ all -> 0x003e }
            switch(r1) {
                case 1: goto L_0x0038;
                case 2: goto L_0x0016;
                default: goto L_0x0014;
            }     // Catch:{ all -> 0x003e }
        L_0x0014:
            monitor-exit(r0)     // Catch:{ all -> 0x003e }
            goto L_0x003d
        L_0x0016:
            com.helpshift.websockets.StateManager r1 = r3.mStateManager     // Catch:{ all -> 0x003e }
            com.helpshift.websockets.StateManager$CloseInitiator r2 = com.helpshift.websockets.StateManager.CloseInitiator.CLIENT     // Catch:{ all -> 0x003e }
            r1.changeToClosing(r2)     // Catch:{ all -> 0x003e }
            com.helpshift.websockets.WebSocketFrame r4 = com.helpshift.websockets.WebSocketFrame.createCloseFrame(r4, r5)     // Catch:{ all -> 0x003e }
            r3.sendFrame(r4)     // Catch:{ all -> 0x003e }
            monitor-exit(r0)     // Catch:{ all -> 0x003e }
            com.helpshift.websockets.ListenerManager r4 = r3.mListenerManager
            com.helpshift.websockets.WebSocketState r5 = com.helpshift.websockets.WebSocketState.CLOSING
            r4.callOnStateChanged(r5)
            r4 = 0
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 >= 0) goto L_0x0034
            r6 = 10000(0x2710, double:4.9407E-320)
        L_0x0034:
            r3.stopThreads(r6)
            return r3
        L_0x0038:
            r3.finishAsynchronously()     // Catch:{ all -> 0x003e }
            monitor-exit(r0)     // Catch:{ all -> 0x003e }
            return r3
        L_0x003d:
            return r3
        L_0x003e:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x003e }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.websockets.WebSocket.disconnect(int, java.lang.String, long):com.helpshift.websockets.WebSocket");
    }

    public List<WebSocketExtension> getAgreedExtensions() {
        return this.mAgreedExtensions;
    }

    /* access modifiers changed from: package-private */
    public void setAgreedExtensions(List<WebSocketExtension> list) {
        this.mAgreedExtensions = list;
    }

    public String getAgreedProtocol() {
        return this.mAgreedProtocol;
    }

    /* access modifiers changed from: package-private */
    public void setAgreedProtocol(String str) {
        this.mAgreedProtocol = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0017, code lost:
        r0 = r3.mWritingThread;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0019, code lost:
        if (r0 != null) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001b, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001c, code lost:
        r1 = splitIfNecessary(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0020, code lost:
        if (r1 != null) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0022, code lost:
        r0.queueFrame(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0026, code lost:
        r4 = r1.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002e, code lost:
        if (r4.hasNext() == false) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0030, code lost:
        r0.queueFrame(r4.next());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003a, code lost:
        return r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.helpshift.websockets.WebSocket sendFrame(com.helpshift.websockets.WebSocketFrame r4) {
        /*
            r3 = this;
            if (r4 != 0) goto L_0x0003
            return r3
        L_0x0003:
            com.helpshift.websockets.StateManager r0 = r3.mStateManager
            monitor-enter(r0)
            com.helpshift.websockets.StateManager r1 = r3.mStateManager     // Catch:{ all -> 0x003b }
            com.helpshift.websockets.WebSocketState r1 = r1.getState()     // Catch:{ all -> 0x003b }
            com.helpshift.websockets.WebSocketState r2 = com.helpshift.websockets.WebSocketState.OPEN     // Catch:{ all -> 0x003b }
            if (r1 == r2) goto L_0x0016
            com.helpshift.websockets.WebSocketState r2 = com.helpshift.websockets.WebSocketState.CLOSING     // Catch:{ all -> 0x003b }
            if (r1 == r2) goto L_0x0016
            monitor-exit(r0)     // Catch:{ all -> 0x003b }
            return r3
        L_0x0016:
            monitor-exit(r0)     // Catch:{ all -> 0x003b }
            com.helpshift.websockets.WritingThread r0 = r3.mWritingThread
            if (r0 != 0) goto L_0x001c
            return r3
        L_0x001c:
            java.util.List r1 = r3.splitIfNecessary(r4)
            if (r1 != 0) goto L_0x0026
            r0.queueFrame(r4)
            goto L_0x003a
        L_0x0026:
            java.util.Iterator r4 = r1.iterator()
        L_0x002a:
            boolean r1 = r4.hasNext()
            if (r1 == 0) goto L_0x003a
            java.lang.Object r1 = r4.next()
            com.helpshift.websockets.WebSocketFrame r1 = (com.helpshift.websockets.WebSocketFrame) r1
            r0.queueFrame(r1)
            goto L_0x002a
        L_0x003a:
            return r3
        L_0x003b:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x003b }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.websockets.WebSocket.sendFrame(com.helpshift.websockets.WebSocketFrame):com.helpshift.websockets.WebSocket");
    }

    private List<WebSocketFrame> splitIfNecessary(WebSocketFrame webSocketFrame) {
        return WebSocketFrame.splitIfNecessary(webSocketFrame, this.mMaxPayloadSize, this.mPerMessageCompressionExtension);
    }

    public WebSocket sendContinuation() {
        return sendFrame(WebSocketFrame.createContinuationFrame());
    }

    public WebSocket sendContinuation(boolean z) {
        return sendFrame(WebSocketFrame.createContinuationFrame().setFin(z));
    }

    public WebSocket sendContinuation(String str) {
        return sendFrame(WebSocketFrame.createContinuationFrame(str));
    }

    public WebSocket sendContinuation(String str, boolean z) {
        return sendFrame(WebSocketFrame.createContinuationFrame(str).setFin(z));
    }

    public WebSocket sendContinuation(byte[] bArr) {
        return sendFrame(WebSocketFrame.createContinuationFrame(bArr));
    }

    public WebSocket sendContinuation(byte[] bArr, boolean z) {
        return sendFrame(WebSocketFrame.createContinuationFrame(bArr).setFin(z));
    }

    public WebSocket sendText(String str) {
        return sendFrame(WebSocketFrame.createTextFrame(str));
    }

    public WebSocket sendText(String str, boolean z) {
        return sendFrame(WebSocketFrame.createTextFrame(str).setFin(z));
    }

    public WebSocket sendBinary(byte[] bArr) {
        return sendFrame(WebSocketFrame.createBinaryFrame(bArr));
    }

    public WebSocket sendBinary(byte[] bArr, boolean z) {
        return sendFrame(WebSocketFrame.createBinaryFrame(bArr).setFin(z));
    }

    public WebSocket sendClose() {
        return sendFrame(WebSocketFrame.createCloseFrame());
    }

    public WebSocket sendClose(int i) {
        return sendFrame(WebSocketFrame.createCloseFrame(i));
    }

    public WebSocket sendClose(int i, String str) {
        return sendFrame(WebSocketFrame.createCloseFrame(i, str));
    }

    public WebSocket sendPing() {
        return sendFrame(WebSocketFrame.createPingFrame());
    }

    public WebSocket sendPing(byte[] bArr) {
        return sendFrame(WebSocketFrame.createPingFrame(bArr));
    }

    public WebSocket sendPing(String str) {
        return sendFrame(WebSocketFrame.createPingFrame(str));
    }

    public WebSocket sendPong() {
        return sendFrame(WebSocketFrame.createPongFrame());
    }

    public WebSocket sendPong(byte[] bArr) {
        return sendFrame(WebSocketFrame.createPongFrame(bArr));
    }

    public WebSocket sendPong(String str) {
        return sendFrame(WebSocketFrame.createPongFrame(str));
    }

    private void changeStateOnConnect() throws WebSocketException {
        synchronized (this.mStateManager) {
            if (this.mStateManager.getState() == WebSocketState.CREATED) {
                this.mStateManager.setState(WebSocketState.CONNECTING);
            } else {
                throw new WebSocketException(WebSocketError.NOT_IN_CREATED_STATE, "The current state of the WebSocket is not CREATED.");
            }
        }
        this.mListenerManager.callOnStateChanged(WebSocketState.CONNECTING);
    }

    private Map<String, List<String>> shakeHands() throws WebSocketException {
        Socket socket = this.mSocketConnector.getSocket();
        WebSocketInputStream openInputStream = openInputStream(socket);
        WebSocketOutputStream openOutputStream = openOutputStream(socket);
        byte[] bArr = new byte[16];
        Misc.nextBytes(bArr);
        String encode = Base64.encode(bArr);
        writeHandshake(openOutputStream, encode);
        Map<String, List<String>> readHandshake = readHandshake(openInputStream, encode);
        this.mInput = openInputStream;
        this.mOutput = openOutputStream;
        return readHandshake;
    }

    private WebSocketInputStream openInputStream(Socket socket) throws WebSocketException {
        try {
            return new WebSocketInputStream(new BufferedInputStream(socket.getInputStream()));
        } catch (IOException e) {
            WebSocketError webSocketError = WebSocketError.SOCKET_INPUT_STREAM_FAILURE;
            throw new WebSocketException(webSocketError, "Failed to get the input stream of the raw socket: " + e.getMessage(), e);
        }
    }

    private WebSocketOutputStream openOutputStream(Socket socket) throws WebSocketException {
        try {
            return new WebSocketOutputStream(new BufferedOutputStream(socket.getOutputStream()));
        } catch (IOException e) {
            WebSocketError webSocketError = WebSocketError.SOCKET_OUTPUT_STREAM_FAILURE;
            throw new WebSocketException(webSocketError, "Failed to get the output stream from the raw socket: " + e.getMessage(), e);
        }
    }

    private void writeHandshake(WebSocketOutputStream webSocketOutputStream, String str) throws WebSocketException {
        this.mHandshakeBuilder.setKey(str);
        String buildRequestLine = this.mHandshakeBuilder.buildRequestLine();
        List<String[]> buildHeaders = this.mHandshakeBuilder.buildHeaders();
        String build = HandshakeBuilder.build(buildRequestLine, buildHeaders);
        this.mListenerManager.callOnSendingHandshake(buildRequestLine, buildHeaders);
        try {
            webSocketOutputStream.write(build);
            webSocketOutputStream.flush();
        } catch (IOException e) {
            WebSocketError webSocketError = WebSocketError.OPENING_HAHDSHAKE_REQUEST_FAILURE;
            throw new WebSocketException(webSocketError, "Failed to send an opening handshake request to the server: " + e.getMessage(), e);
        }
    }

    private Map<String, List<String>> readHandshake(WebSocketInputStream webSocketInputStream, String str) throws WebSocketException {
        return new HandshakeReader(this).readHandshake(webSocketInputStream, str);
    }

    private void startThreads() {
        ReadingThread readingThread = new ReadingThread(this);
        WritingThread writingThread = new WritingThread(this);
        synchronized (this.mThreadsLock) {
            this.mReadingThread = readingThread;
            this.mWritingThread = writingThread;
        }
        readingThread.callOnThreadCreated();
        writingThread.callOnThreadCreated();
        readingThread.start();
        writingThread.start();
    }

    private void stopThreads(long j) {
        ReadingThread readingThread;
        WritingThread writingThread;
        synchronized (this.mThreadsLock) {
            readingThread = this.mReadingThread;
            writingThread = this.mWritingThread;
            this.mReadingThread = null;
            this.mWritingThread = null;
        }
        if (readingThread != null) {
            readingThread.requestStop(j);
        }
        if (writingThread != null) {
            writingThread.requestStop();
        }
    }

    /* access modifiers changed from: package-private */
    public WebSocketInputStream getInput() {
        return this.mInput;
    }

    /* access modifiers changed from: package-private */
    public WebSocketOutputStream getOutput() {
        return this.mOutput;
    }

    /* access modifiers changed from: package-private */
    public StateManager getStateManager() {
        return this.mStateManager;
    }

    /* access modifiers changed from: package-private */
    public ListenerManager getListenerManager() {
        return this.mListenerManager;
    }

    /* access modifiers changed from: package-private */
    public HandshakeBuilder getHandshakeBuilder() {
        return this.mHandshakeBuilder;
    }

    /* access modifiers changed from: package-private */
    public void onReadingThreadStarted() {
        boolean z;
        synchronized (this.mThreadsLock) {
            this.mReadingThreadStarted = true;
            z = this.mWritingThreadStarted;
        }
        callOnConnectedIfNotYet();
        if (z) {
            onThreadsStarted();
        }
    }

    /* access modifiers changed from: package-private */
    public void onWritingThreadStarted() {
        boolean z;
        synchronized (this.mThreadsLock) {
            this.mWritingThreadStarted = true;
            z = this.mReadingThreadStarted;
        }
        callOnConnectedIfNotYet();
        if (z) {
            onThreadsStarted();
        }
    }

    private void callOnConnectedIfNotYet() {
        synchronized (this.mOnConnectedCalledLock) {
            if (!this.mOnConnectedCalled) {
                this.mOnConnectedCalled = true;
                this.mListenerManager.callOnConnected(this.mServerHeaders);
            }
        }
    }

    private void onThreadsStarted() {
        this.mPingSender.start();
        this.mPongSender.start();
    }

    /* access modifiers changed from: package-private */
    public void onReadingThreadFinished(WebSocketFrame webSocketFrame) {
        synchronized (this.mThreadsLock) {
            this.mReadingThreadFinished = true;
            this.mServerCloseFrame = webSocketFrame;
            if (this.mWritingThreadFinished) {
                onThreadsFinished();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void onWritingThreadFinished(WebSocketFrame webSocketFrame) {
        synchronized (this.mThreadsLock) {
            this.mWritingThreadFinished = true;
            this.mClientCloseFrame = webSocketFrame;
            if (this.mReadingThreadFinished) {
                onThreadsFinished();
            }
        }
    }

    private void onThreadsFinished() {
        finish();
    }

    /* access modifiers changed from: package-private */
    public void finish() {
        this.mPingSender.stop();
        this.mPongSender.stop();
        try {
            this.mSocketConnector.getSocket().close();
        } catch (Throwable unused) {
        }
        synchronized (this.mStateManager) {
            this.mStateManager.setState(WebSocketState.CLOSED);
        }
        this.mListenerManager.callOnStateChanged(WebSocketState.CLOSED);
        this.mListenerManager.callOnDisconnected(this.mServerCloseFrame, this.mClientCloseFrame, this.mStateManager.getClosedByServer());
    }

    private void finishAsynchronously() {
        FinishThread finishThread = new FinishThread(this);
        finishThread.callOnThreadCreated();
        finishThread.start();
    }

    private PerMessageCompressionExtension findAgreedPerMessageCompressionExtension() {
        if (this.mAgreedExtensions == null) {
            return null;
        }
        for (WebSocketExtension next : this.mAgreedExtensions) {
            if (next instanceof PerMessageCompressionExtension) {
                return (PerMessageCompressionExtension) next;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public PerMessageCompressionExtension getPerMessageCompressionExtension() {
        return this.mPerMessageCompressionExtension;
    }
}
