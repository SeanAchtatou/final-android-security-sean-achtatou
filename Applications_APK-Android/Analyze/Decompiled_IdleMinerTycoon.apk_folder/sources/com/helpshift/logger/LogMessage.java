package com.helpshift.logger;

import com.helpshift.logger.logmodels.ILogExtrasModel;

/* compiled from: Logger */
class LogMessage {
    ILogExtrasModel[] extras;
    String level;
    String message;
    String sdkVersion;
    String stacktrace;
    long timeStamp;

    LogMessage() {
    }
}
