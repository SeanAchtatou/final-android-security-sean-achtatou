package com.helpshift.logger.logmodels;

import org.json.JSONException;
import org.json.JSONObject;

class StringExtrasModel implements ILogExtrasModel {
    private String key;
    private String value;

    StringExtrasModel(String str, String str2) {
        this.key = str;
        this.value = str2;
    }

    public String getConsoleLoggingMessage() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.key);
        sb.append(" : ");
        sb.append(this.value == null ? "" : this.value);
        return sb.toString();
    }

    public Object toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(this.key, this.value == null ? "" : this.value);
        } catch (JSONException unused) {
        }
        return jSONObject;
    }
}
