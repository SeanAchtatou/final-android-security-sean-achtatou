package com.helpshift.logger.logmodels;

import java.util.Map;

public class LogExtrasModelProvider {
    private static ILogExtrasModelFactory factory;

    public static void initialize(ILogExtrasModelFactory iLogExtrasModelFactory) {
        factory = iLogExtrasModelFactory;
    }

    public static ILogExtrasModel fromString(String str, String str2) {
        if (factory != null) {
            return factory.fromString(str, str2);
        }
        return null;
    }

    public static ILogExtrasModel fromMap(String str, Map map) {
        if (factory != null) {
            return factory.fromMap(str, map);
        }
        return null;
    }
}
