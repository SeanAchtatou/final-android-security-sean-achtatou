package com.helpshift.logger;

import android.content.Context;
import java.util.HashMap;

public class LoggerProvider {
    private static HashMap<String, ILogger> hsLoggerInstances = new HashMap<>();
    private static final Object loggerInstanceLock = new Object();

    public static ILogger getLoggerInstance(Context context, String str, String str2) {
        ILogger iLogger;
        if (context == null) {
            return null;
        }
        synchronized (loggerInstanceLock) {
            iLogger = hsLoggerInstances.get(str);
            if (iLogger == null) {
                iLogger = new Logger(context, str, str2);
                hsLoggerInstances.put(str, iLogger);
            }
        }
        return iLogger;
    }
}
