package com.helpshift.logger;

import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.common.FetchDataFromThread;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.F;
import com.helpshift.common.domain.network.CustomAuthDataPOSTNetwork;
import com.helpshift.common.domain.network.ETagNetwork;
import com.helpshift.common.domain.network.FailedAPICallNetworkDecorator;
import com.helpshift.common.domain.network.GuardOKNetwork;
import com.helpshift.common.domain.network.NetworkDataRequestUtil;
import com.helpshift.common.domain.network.TSCorrectedNetwork;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.platform.Platform;
import com.helpshift.common.platform.network.RequestData;
import com.helpshift.common.platform.network.Response;
import com.helpshift.common.util.HSDateFormatSpec;
import com.helpshift.logger.model.LogModel;
import com.helpshift.support.res.values.HSConsts;
import com.helpshift.util.HSFormat;
import com.ironsource.sdk.constants.Constants;
import com.unity3d.ads.metadata.InAppPurchaseMetaData;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ErrorReportsDM {
    Domain domain;
    Platform platform;

    public ErrorReportsDM(Platform platform2, Domain domain2) {
        this.platform = platform2;
        this.domain = domain2;
    }

    public void sendErrorReport(FetchDataFromThread<Response, Float> fetchDataFromThread, List<LogModel> list, UserDM userDM, String str, String str2, String str3, String str4, String str5, String str6) {
        final List<LogModel> list2 = list;
        final String str7 = str;
        final String str8 = str4;
        final UserDM userDM2 = userDM;
        final String str9 = str5;
        final String str10 = str6;
        final String str11 = str2;
        final String str12 = str3;
        final FetchDataFromThread<Response, Float> fetchDataFromThread2 = fetchDataFromThread;
        this.domain.runParallel(new F() {
            public void f() {
                try {
                    Object jsonifyLogModelList = ErrorReportsDM.this.platform.getJsonifier().jsonifyLogModelList(list2);
                    ArrayList arrayList = new ArrayList(5);
                    arrayList.add(ErrorReportsDM.this.platform.getJsonifier().jsonifyToObject("domain", str7).toString());
                    arrayList.add(ErrorReportsDM.this.platform.getJsonifier().jsonifyToObject("dm", str8).toString());
                    arrayList.add(ErrorReportsDM.this.platform.getJsonifier().jsonifyToObject("did", userDM2.getDeviceId()).toString());
                    if (!StringUtils.isEmpty(str9)) {
                        arrayList.add(ErrorReportsDM.this.platform.getJsonifier().jsonifyToObject("cdid", str9).toString());
                    }
                    arrayList.add(ErrorReportsDM.this.platform.getJsonifier().jsonifyToObject("os", str10).toString());
                    Object jsonifyListToJsonArray = ErrorReportsDM.this.platform.getJsonifier().jsonifyListToJsonArray(arrayList);
                    HashMap hashMap = new HashMap();
                    hashMap.put("id", UUID.randomUUID().toString());
                    hashMap.put("v", str11);
                    hashMap.put("ctime", HSFormat.errorLogReportingTimeFormat.format(HSDateFormatSpec.getCurrentAdjustedTime(ErrorReportsDM.this.platform)));
                    hashMap.put("src", "sdk.android." + str12);
                    hashMap.put("logs", jsonifyLogModelList.toString());
                    hashMap.put("md", jsonifyListToJsonArray.toString());
                    fetchDataFromThread2.onDataFetched(new ETagNetwork(new GuardOKNetwork(new TSCorrectedNetwork(new FailedAPICallNetworkDecorator(new CustomAuthDataPOSTNetwork("/events/crash-log", ErrorReportsDM.this.domain, ErrorReportsDM.this.platform, ErrorReportsDM.this.getAuthDataForErrorReports())), ErrorReportsDM.this.platform)), ErrorReportsDM.this.platform, "/faqs").makeRequest(new RequestData(hashMap)));
                } catch (RootAPIException unused) {
                    fetchDataFromThread2.onFailure(Float.valueOf(ErrorReportsDM.this.platform.getNetworkRequestDAO().getServerTimeDelta()));
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> getAuthDataForErrorReports() {
        try {
            HashMap hashMap = new HashMap();
            ArrayList arrayList = new ArrayList();
            arrayList.add("platform-id=sdk");
            String uuid = UUID.randomUUID().toString();
            arrayList.add("token=" + uuid);
            hashMap.put("token", uuid);
            hashMap.put(HSConsts.SDK_META, this.platform.getJsonifier().jsonify(NetworkDataRequestUtil.getSdkMeta()));
            arrayList.add("sm=" + this.platform.getJsonifier().jsonify(NetworkDataRequestUtil.getSdkMeta()));
            hashMap.put(InAppPurchaseMetaData.KEY_SIGNATURE, this.domain.getCryptoDM().getSignature(StringUtils.join(Constants.RequestParameters.AMPERSAND, arrayList), "sdk"));
            return hashMap;
        } catch (GeneralSecurityException e) {
            throw RootAPIException.wrap(e, null, "SecurityException while creating signature");
        }
    }
}
