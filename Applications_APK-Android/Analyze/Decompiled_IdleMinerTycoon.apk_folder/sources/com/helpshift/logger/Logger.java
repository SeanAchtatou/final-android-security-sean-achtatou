package com.helpshift.logger;

import android.content.Context;
import android.util.Log;
import com.helpshift.logger.database.LogSQLiteStorage;
import com.helpshift.logger.database.LogStorage;
import com.helpshift.logger.logmodels.ILogExtrasModel;
import com.helpshift.logger.model.LogModel;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

class Logger implements ILogger {
    private static final String ERROR = "ERROR";
    private static final String FATAL = "FATAL";
    static final int MAX_EXTRAS_COUNT = 20;
    static final int MAX_LOG_SIZE = 5000;
    private static final String WARN = "WARN";
    private final String TAG = Logger.class.getSimpleName();
    private boolean enableConsoleLogging;
    private boolean enableLogCaching;
    private LogStorage logStorage;
    private int loggingLevel = 4;
    private final String sdkVersion;
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
    private ThreadPoolExecutor threadPoolExecutor;
    private long timestampDelta;

    Logger(Context context, String str, String str2) {
        this.logStorage = new LogSQLiteStorage(context, str);
        this.sdkVersion = str2;
    }

    public void enableLogging(boolean z, boolean z2) {
        this.enableConsoleLogging = z;
        if (this.enableLogCaching != z2) {
            this.enableLogCaching = z2;
            if (this.enableLogCaching) {
                this.threadPoolExecutor = new ThreadPoolExecutor(1, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new ThreadFactory() {
                    public Thread newThread(Runnable runnable) {
                        return new Thread(runnable, "HS-Logger");
                    }
                });
            } else if (this.threadPoolExecutor != null) {
                this.threadPoolExecutor.shutdown();
            }
        }
    }

    public void setLoggingLevel(int i) {
        this.loggingLevel = i;
    }

    public void setTimestampDelta(long j) {
        this.timestampDelta = j;
    }

    public void d(String str, String str2) {
        d(str, str2, null, null);
    }

    public void w(String str, String str2) {
        w(str, str2, null, null);
    }

    public void e(String str, String str2) {
        e(str, str2, null, null);
    }

    public void d(String str, String str2, Throwable[] thArr) {
        d(str, str2, thArr, null);
    }

    public void w(String str, String str2, Throwable[] thArr) {
        w(str, str2, thArr, null);
    }

    public void e(String str, String str2, Throwable[] thArr) {
        e(str, str2, thArr, null);
    }

    public void f(String str, String str2, Throwable[] thArr) {
        f(str, str2, thArr, null);
    }

    public void d(String str, String str2, ILogExtrasModel... iLogExtrasModelArr) {
        d(str, str2, null, iLogExtrasModelArr);
    }

    public void w(String str, String str2, ILogExtrasModel... iLogExtrasModelArr) {
        w(str, str2, null, iLogExtrasModelArr);
    }

    public void e(String str, String str2, ILogExtrasModel... iLogExtrasModelArr) {
        e(str, str2, null, iLogExtrasModelArr);
    }

    public void d(String str, String str2, Throwable[] thArr, ILogExtrasModel... iLogExtrasModelArr) {
        if (isConsoleLoggingEnabled() && this.loggingLevel <= 2) {
            String stackTraceString = getStackTraceString(thArr);
            Log.d(str, str2 + getExtrasForConsoleLogging(iLogExtrasModelArr) + stackTraceString);
        }
    }

    public void w(String str, String str2, Throwable[] thArr, ILogExtrasModel... iLogExtrasModelArr) {
        String str3;
        if (!isConsoleLoggingEnabled() || this.loggingLevel > 4) {
            str3 = null;
        } else {
            str3 = getStackTraceString(thArr);
            Log.w(str, str2 + getExtrasForConsoleLogging(iLogExtrasModelArr) + str3);
        }
        if (isLogCachingEnabled()) {
            if (str3 == null) {
                str3 = getStackTraceString(thArr);
            }
            logMessageToDatabase(WARN, str2, str3, iLogExtrasModelArr);
        }
    }

    public void e(String str, String str2, Throwable[] thArr, ILogExtrasModel... iLogExtrasModelArr) {
        String str3;
        if (!isConsoleLoggingEnabled() || this.loggingLevel > 8) {
            str3 = null;
        } else {
            str3 = getStackTraceString(thArr);
            Log.e(str, str2 + getExtrasForConsoleLogging(iLogExtrasModelArr) + str3);
        }
        if (isLogCachingEnabled() && !containsUnknownHostException(thArr)) {
            if (str3 == null) {
                str3 = getStackTraceString(thArr);
            }
            logMessageToDatabase(ERROR, str2, str3, iLogExtrasModelArr);
        }
    }

    public void f(String str, String str2, Throwable[] thArr, ILogExtrasModel... iLogExtrasModelArr) {
        String str3;
        if (!isConsoleLoggingEnabled() || this.loggingLevel > 16) {
            str3 = null;
        } else {
            str3 = getStackTraceString(thArr);
            Log.e(str, str2 + getExtrasForConsoleLogging(iLogExtrasModelArr) + str3);
        }
        if (isLogCachingEnabled()) {
            if (str3 == null) {
                str3 = getStackTraceString(thArr);
            }
            Future logMessageToDatabase = logMessageToDatabase(FATAL, str2, str3, iLogExtrasModelArr);
            if (logMessageToDatabase != null) {
                try {
                    logMessageToDatabase.get();
                } catch (Exception e) {
                    String str4 = this.TAG;
                    Log.e(str4, "Error logging fatal log : " + e.getMessage());
                }
            }
        }
    }

    public List<LogModel> getAll() {
        return this.logStorage.getAll();
    }

    public void deleteAllCachedLogs() {
        this.logStorage.deleteAll();
    }

    public int getCount(int i) {
        return this.logStorage.getCount(convertMaskToLogLevel(i));
    }

    private boolean isConsoleLoggingEnabled() {
        return this.enableConsoleLogging;
    }

    private boolean isLogCachingEnabled() {
        return this.enableLogCaching;
    }

    private Future logMessageToDatabase(String str, String str2, String str3, ILogExtrasModel[] iLogExtrasModelArr) {
        LogMessage logMessage = new LogMessage();
        logMessage.level = str;
        logMessage.extras = iLogExtrasModelArr;
        logMessage.message = str2;
        logMessage.timeStamp = System.currentTimeMillis() + this.timestampDelta;
        logMessage.stacktrace = str3;
        logMessage.sdkVersion = this.sdkVersion;
        try {
            return this.threadPoolExecutor.submit(new WorkerThread(logMessage, this.logStorage, this.simpleDateFormat));
        } catch (RejectedExecutionException e) {
            String str4 = this.TAG;
            Log.e(str4, "Rejected execution of log message : " + logMessage.message, e);
            return null;
        }
    }

    private boolean containsUnknownHostException(Throwable[] thArr) {
        if (thArr == null) {
            return false;
        }
        for (Throwable th : thArr) {
            if (th instanceof UnknownHostException) {
                return true;
            }
        }
        return false;
    }

    private String getExtrasForConsoleLogging(ILogExtrasModel[] iLogExtrasModelArr) {
        if (iLogExtrasModelArr == null || iLogExtrasModelArr.length <= 0) {
            return " ";
        }
        StringBuilder sb = new StringBuilder(" ");
        for (ILogExtrasModel iLogExtrasModel : iLogExtrasModelArr) {
            if (iLogExtrasModel != null) {
                sb.append(iLogExtrasModel.getConsoleLoggingMessage());
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    private String getStackTraceString(Throwable[] thArr) {
        if (thArr == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        if (containsUnknownHostException(thArr)) {
            return "UnknownHostException";
        }
        for (Throwable stackTraceString : thArr) {
            sb.append(Log.getStackTraceString(stackTraceString));
        }
        return sb.toString();
    }

    private List<String> convertMaskToLogLevel(int i) {
        if (i == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        if ((i & 8) != 0) {
            arrayList.add(ERROR);
        }
        if ((i & 4) != 0) {
            arrayList.add(WARN);
        }
        if ((i & 16) != 0) {
            arrayList.add(FATAL);
        }
        return arrayList;
    }
}
