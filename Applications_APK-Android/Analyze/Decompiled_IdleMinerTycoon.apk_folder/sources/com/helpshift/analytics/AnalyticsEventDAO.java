package com.helpshift.analytics;

import java.util.HashMap;
import java.util.Map;

public interface AnalyticsEventDAO {
    Map<String, HashMap<String, String>> getUnsentAnalytics();

    void removeAnalyticsData(String str);

    void saveUnsentAnalyticsData(String str, HashMap<String, String> hashMap);
}
