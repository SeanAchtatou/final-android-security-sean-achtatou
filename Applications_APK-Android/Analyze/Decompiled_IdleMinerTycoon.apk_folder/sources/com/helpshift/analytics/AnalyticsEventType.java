package com.helpshift.analytics;

import com.mintegral.msdk.base.b.d;
import com.mintegral.msdk.base.entity.CampaignEx;
import im.getsocial.sdk.consts.LanguageCodes;

public enum AnalyticsEventType {
    APP_START("a"),
    LIBRARY_OPENED("o"),
    LIBRARY_OPENED_DECOMP(d.b),
    SUPPORT_LAUNCH("l"),
    PERFORMED_SEARCH(AnalyticsEventKey.SEARCH_QUERY),
    BROWSED_FAQ_LIST("b"),
    READ_FAQ("f"),
    MARKED_HELPFUL("h"),
    MARKED_UNHELPFUL(AnalyticsEventKey.URL),
    REPORTED_ISSUE("i"),
    CONVERSATION_POSTED(AnalyticsEventKey.PROTOCOL),
    REVIEWED_APP(CampaignEx.JSON_KEY_AD_R),
    OPEN_ISSUE("c"),
    OPEN_INBOX("x"),
    LIBRARY_QUIT(CampaignEx.JSON_KEY_AD_Q),
    MESSAGE_ADDED("m"),
    RESOLUTION_ACCEPTED("y"),
    RESOLUTION_REJECTED(AnalyticsEventKey.FAQ_SEARCH_RESULT_COUNT),
    START_CSAT_RATING("sr"),
    CANCEL_CSAT_RATING("cr"),
    LINK_VIA_FAQ("fl"),
    TICKET_AVOIDED(LanguageCodes.TAMIL),
    TICKET_AVOIDANCE_FAILED("taf"),
    DYNAMIC_FORM_OPEN("dfo"),
    ADMIN_MESSAGE_DEEPLINK_CLICKED(LanguageCodes.MALAYALAM),
    DYNAMIC_FORM_CLOSE("dfc");
    
    public final String key;

    private AnalyticsEventType(String str) {
        this.key = str;
    }
}
