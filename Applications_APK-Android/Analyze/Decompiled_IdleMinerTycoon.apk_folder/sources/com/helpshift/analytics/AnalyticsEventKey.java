package com.helpshift.analytics;

public interface AnalyticsEventKey {
    public static final String BODY = "body";
    public static final String FAQ_SEARCH_RESULT_COUNT = "n";
    public static final String ID = "id";
    public static final String ISSUE_ID = "issue_id";
    public static final String NETWORK_CONNECTIVITY = "nt";
    public static final String PREISSUE_ID = "preissue_id";
    public static final String PROTOCOL = "p";
    public static final String RESPONSE = "response";
    public static final String SEARCH_QUERY = "s";
    public static final String STR = "str";
    public static final String TYPE = "type";
    public static final String URL = "u";
}
