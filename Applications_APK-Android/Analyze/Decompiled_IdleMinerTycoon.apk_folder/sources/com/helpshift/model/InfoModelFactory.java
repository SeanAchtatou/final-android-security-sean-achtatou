package com.helpshift.model;

import com.helpshift.storage.KeyValueStorage;
import com.helpshift.storage.StorageFactory;
import com.helpshift.util.HelpshiftContext;

public class InfoModelFactory {
    public final AppInfoModel appInfoModel;
    public final SdkInfoModel sdkInfoModel;

    InfoModelFactory() {
        KeyValueStorage keyValueStorage = StorageFactory.getInstance().keyValueStorage;
        this.appInfoModel = new AppInfoModel(keyValueStorage);
        this.sdkInfoModel = new SdkInfoModel(keyValueStorage, HelpshiftContext.getPlatform());
    }

    public static InfoModelFactory getInstance() {
        return LazyHolder.INSTANCE;
    }

    private static final class LazyHolder {
        static final InfoModelFactory INSTANCE = new InfoModelFactory();

        private LazyHolder() {
        }
    }
}
