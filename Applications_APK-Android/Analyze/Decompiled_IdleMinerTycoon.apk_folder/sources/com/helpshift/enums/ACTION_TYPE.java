package com.helpshift.enums;

import com.helpshift.common.domain.network.NetworkConstants;
import com.mintegral.msdk.base.entity.CampaignEx;

public enum ACTION_TYPE {
    OPEN_DEEP_LINK,
    SHOW_FAQS,
    SHOW_FAQ_SECTION,
    SHOW_CONVERSATION,
    SHOW_SINGLE_FAQ,
    SHOW_ALERT_TO_RATE_APP,
    LAUNCH_APP,
    SHOW_INBOX;

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static ACTION_TYPE getEnum(String str) {
        char c;
        switch (str.hashCode()) {
            case 49:
                if (str.equals("1")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case 50:
                if (str.equals("2")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            case 51:
                if (str.equals(NetworkConstants.apiVersion)) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case 52:
                if (str.equals("4")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case 53:
                if (str.equals(CampaignEx.CLICKMODE_ON)) {
                    c = 4;
                    break;
                }
                c = 65535;
                break;
            case 54:
                if (str.equals("6")) {
                    c = 5;
                    break;
                }
                c = 65535;
                break;
            case 55:
                if (str.equals("7")) {
                    c = 6;
                    break;
                }
                c = 65535;
                break;
            case 56:
                if (str.equals("8")) {
                    c = 7;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
                return OPEN_DEEP_LINK;
            case 1:
                return SHOW_FAQS;
            case 2:
                return SHOW_FAQ_SECTION;
            case 3:
                return SHOW_CONVERSATION;
            case 4:
                return SHOW_SINGLE_FAQ;
            case 5:
                return SHOW_ALERT_TO_RATE_APP;
            case 6:
                return LAUNCH_APP;
            case 7:
                return SHOW_INBOX;
            default:
                return LAUNCH_APP;
        }
    }

    public static ACTION_TYPE getEnum(int i) {
        switch (i) {
            case 1:
                return OPEN_DEEP_LINK;
            case 2:
                return SHOW_FAQS;
            case 3:
                return SHOW_FAQ_SECTION;
            case 4:
                return SHOW_CONVERSATION;
            case 5:
                return SHOW_SINGLE_FAQ;
            case 6:
                return SHOW_ALERT_TO_RATE_APP;
            case 7:
                return LAUNCH_APP;
            case 8:
                return SHOW_INBOX;
            default:
                return LAUNCH_APP;
        }
    }
}
