package com.helpshift.controllers;

public interface DataSyncCompletionListener {
    void firstDeviceSyncComplete();

    void switchUserComplete(String str);
}
