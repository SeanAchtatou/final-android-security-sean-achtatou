package com.helpshift.controllers;

import android.os.Handler;
import android.os.HandlerThread;
import com.helpshift.app.CampaignAppLifeCycleListener;
import com.helpshift.app.LifecycleListener;
import com.helpshift.common.poller.Delay;
import com.helpshift.common.poller.HttpBackoff;
import com.helpshift.listeners.SyncListener;
import com.helpshift.model.InfoModelFactory;
import com.helpshift.network.errors.NetworkError;
import com.helpshift.specifications.DecayingIntervalSyncSpecification;
import com.helpshift.specifications.SyncSpecification;
import com.helpshift.storage.KeyValueStorage;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.util.TimeUtil;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class SyncController implements LifecycleListener, DataSyncCompletionListener {
    static final String COUNT = "count";
    static final String FULL_SYNC_TIME = "full_sync_time";
    static final long FULL_SYNC_TIME_THRESHOLD = 86400000;
    private static final int MAX_RETRY_ATTEMPTS = 10;
    private static final long REALTIME_SYNC_BATCHER_DELAY_MS = 60000;
    static final String SYNC_TIME = "sync_time";
    private static final String TAG = "Helpshift_SyncControl";
    private Runnable batcherJob;
    Set<String> dataTypesWithChangedData;
    private Handler handler;
    private AtomicBoolean isBatcherScheduled;
    private final KeyValueStorage keyValueStorage;
    private HttpBackoff retryBackoff;
    private final LinkedBlockingQueue<SyncListener> syncListeners = new LinkedBlockingQueue<>();
    private final Map<String, SyncSpecification> syncSpecificationMap = new HashMap();
    private final TimeUtil timeUtil;

    public static class DataTypes {
        public static final String ANALYTICS_EVENT = "data_type_analytics_event";
        public static final String DEVICE = "data_type_device";
        public static final String SESSION = "data_type_session";
        public static final String SWITCH_USER = "data_type_switch_user";
        public static final String USER = "data_type_user";
    }

    public SyncController(KeyValueStorage keyValueStorage2, TimeUtil timeUtil2, SyncSpecification... syncSpecificationArr) {
        this.isBatcherScheduled = new AtomicBoolean(false);
        this.keyValueStorage = keyValueStorage2;
        this.timeUtil = timeUtil2;
        CampaignAppLifeCycleListener campaignAppLifeCycleListener = HelpshiftContext.getCampaignAppLifeCycleListener();
        if (campaignAppLifeCycleListener != null) {
            campaignAppLifeCycleListener.addLifecycleListener(this);
        }
        for (SyncSpecification syncSpecification : syncSpecificationArr) {
            this.syncSpecificationMap.put(syncSpecification.getDataType(), syncSpecification);
        }
    }

    private Runnable getBatcherJob() {
        if (this.batcherJob == null) {
            this.batcherJob = new Runnable() {
                public void run() {
                    if (SyncController.this.dataTypesWithChangedData != null) {
                        SyncController.this.triggerSync(false, (String[]) SyncController.this.dataTypesWithChangedData.toArray(new String[SyncController.this.dataTypesWithChangedData.size()]));
                    }
                    SyncController.this.cleanUpBatcherJob();
                }
            };
        }
        return this.batcherJob;
    }

    public void addSpecification(SyncSpecification syncSpecification) {
        this.syncSpecificationMap.put(syncSpecification.getDataType(), syncSpecification);
    }

    public void addSyncListeners(SyncListener... syncListenerArr) {
        for (SyncListener syncListener : syncListenerArr) {
            if (this.syncSpecificationMap.containsKey(syncListener.getDataType())) {
                this.syncListeners.add(syncListener);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void triggerSync(boolean z, String... strArr) {
        for (String str : strArr) {
            HSLogger.d(TAG, "Triggering sync for  type : " + str);
            if (isFullSyncSatisfied(str)) {
                dispatchSync(str, true);
            } else if (z) {
                SyncSpecification syncSpecification = this.syncSpecificationMap.get(str);
                if (syncSpecification != null && syncSpecification.isSatisfied(getDataChangeCount(str), getElapsedTimeSinceLastSync(str))) {
                    dispatchSync(str, false);
                }
            } else {
                dispatchSync(str, false);
            }
        }
    }

    private void dispatchSync(String str, boolean z) {
        HSLogger.d(TAG, "Dispatching sync for type :" + str + ", isFullSync : " + z);
        SyncListener syncListener = getSyncListener(str);
        if (syncListener == null) {
            return;
        }
        if (z) {
            syncListener.fullSync();
        } else {
            syncListener.sync();
        }
    }

    private SyncListener getSyncListener(String str) {
        Iterator<SyncListener> it = this.syncListeners.iterator();
        while (it.hasNext()) {
            SyncListener next = it.next();
            if (next.getDataType().equals(str)) {
                return next;
            }
        }
        return null;
    }

    public void scheduleSync(String str, long j) {
        HSLogger.d(TAG, "Scheduling sync : " + str + ", Delay : " + j);
        if (isDataTypeAllowedForImmediateSync(str)) {
            if (this.isBatcherScheduled.compareAndSet(false, true)) {
                startBatcherThread();
                this.handler.postDelayed(getBatcherJob(), j);
            }
            addDataTypeWithChangedData(str);
        }
    }

    private void onDataChanged(String str) {
        scheduleSync(str, REALTIME_SYNC_BATCHER_DELAY_MS);
    }

    public void incrementDataChangeCount(String str, int i) {
        if (i > 0) {
            HashMap<String, String> syncInformation = getSyncInformation(str);
            syncInformation.put("count", Integer.toString(Integer.valueOf(syncInformation.get("count")).intValue() + i));
            this.keyValueStorage.set(str, syncInformation);
            onDataChanged(str);
        }
    }

    public void setDataChangeCount(String str, int i) {
        HashMap<String, String> syncInformation = getSyncInformation(str);
        int intValue = Integer.valueOf(syncInformation.get("count")).intValue();
        syncInformation.put("count", Integer.toString(i));
        this.keyValueStorage.set(str, syncInformation);
        if (intValue != i && i > 0) {
            onDataChanged(str);
        }
    }

    public void dataSynced(String str, boolean z) {
        HSLogger.d(TAG, "Data sync complete : " + str + ", Full sync : " + z);
        String l = Long.toString(this.timeUtil.elapsedTimeMillis());
        HashMap<String, String> syncInformation = getSyncInformation(str);
        syncInformation.put("count", Integer.toString(0));
        syncInformation.put(SYNC_TIME, l);
        if (z) {
            syncInformation.put(FULL_SYNC_TIME, l);
        }
        this.keyValueStorage.set(str, syncInformation);
        if (this.retryBackoff != null) {
            this.retryBackoff.reset();
        }
    }

    public void dataSyncFailed(String str, NetworkError networkError) {
        HSLogger.w(TAG, "Data sync failed : " + str + ", Error : " + networkError.getMessage());
        SyncSpecification syncSpecification = this.syncSpecificationMap.get(str);
        if (syncSpecification != null) {
            char c = 65535;
            int hashCode = str.hashCode();
            if (hashCode != 1056354406) {
                if (hashCode == 1192222481 && str.equals(DataTypes.ANALYTICS_EVENT)) {
                    c = 1;
                }
            } else if (str.equals(DataTypes.SWITCH_USER)) {
                c = 0;
            }
            switch (c) {
                case 0:
                case 1:
                    if (syncSpecification instanceof DecayingIntervalSyncSpecification) {
                        ((DecayingIntervalSyncSpecification) syncSpecification).decayElapsedTimeThreshold();
                        break;
                    }
                    break;
            }
        }
        if (this.retryBackoff == null) {
            this.retryBackoff = new HttpBackoff.Builder().setBaseInterval(Delay.of(5, TimeUnit.SECONDS)).setMaxAttempts(10).setRetryPolicy(HttpBackoff.RetryPolicy.FAILURE).build();
        }
        Integer reason = networkError.getReason();
        long nextIntervalMillis = reason != null ? this.retryBackoff.nextIntervalMillis(reason.intValue()) : -100;
        if (nextIntervalMillis != -100) {
            scheduleSync(str, nextIntervalMillis);
        }
    }

    private int getDataChangeCount(String str) {
        return Integer.valueOf(getSyncInformation(str).get("count")).intValue();
    }

    private long getElapsedTimeSinceLastSync(String str) {
        return this.timeUtil.elapsedTimeMillis() - Long.valueOf(getSyncInformation(str).get(SYNC_TIME)).longValue();
    }

    private long getElapsedTimeSinceFullSync(String str) {
        return this.timeUtil.elapsedTimeMillis() - Long.valueOf(getSyncInformation(str).get(FULL_SYNC_TIME)).longValue();
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0065  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.HashMap<java.lang.String, java.lang.String> getSyncInformation(java.lang.String r11) {
        /*
            r10 = this;
            com.helpshift.storage.KeyValueStorage r0 = r10.keyValueStorage
            java.lang.Object r0 = r0.get(r11)
            java.util.HashMap r0 = (java.util.HashMap) r0
            r1 = 0
            r2 = 1
            r3 = 0
            if (r0 != 0) goto L_0x0030
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            java.lang.String r5 = "count"
            java.lang.String r1 = java.lang.Integer.toString(r1)
            r0.put(r5, r1)
            java.lang.String r1 = "sync_time"
            java.lang.String r5 = java.lang.Long.toString(r3)
            r0.put(r1, r5)
            java.lang.String r1 = "full_sync_time"
            java.lang.String r5 = java.lang.Long.toString(r3)
            r0.put(r1, r5)
        L_0x002e:
            r1 = 1
            goto L_0x005d
        L_0x0030:
            com.helpshift.util.TimeUtil r5 = r10.timeUtil
            long r5 = r5.elapsedTimeMillis()
            java.lang.String r7 = "sync_time"
            java.lang.Object r7 = r0.get(r7)
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Long r7 = java.lang.Long.valueOf(r7)
            long r7 = r7.longValue()
            int r9 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r9 >= 0) goto L_0x005d
            java.lang.String r1 = "sync_time"
            java.lang.String r5 = java.lang.Long.toString(r3)
            r0.put(r1, r5)
            java.lang.String r1 = "full_sync_time"
            java.lang.String r5 = java.lang.Long.toString(r3)
            r0.put(r1, r5)
            goto L_0x002e
        L_0x005d:
            java.lang.String r5 = "full_sync_time"
            boolean r5 = r0.containsKey(r5)
            if (r5 != 0) goto L_0x006f
            java.lang.String r1 = "full_sync_time"
            java.lang.String r3 = java.lang.Long.toString(r3)
            r0.put(r1, r3)
            r1 = 1
        L_0x006f:
            if (r1 == 0) goto L_0x0076
            com.helpshift.storage.KeyValueStorage r1 = r10.keyValueStorage
            r1.set(r11, r0)
        L_0x0076:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.controllers.SyncController.getSyncInformation(java.lang.String):java.util.HashMap");
    }

    public boolean isFullSyncSatisfied(String str) {
        SyncListener syncListener = getSyncListener(str);
        return syncListener != null && syncListener.isFullSyncEnabled() && getElapsedTimeSinceFullSync(str) > 86400000;
    }

    public void switchUserComplete(String str) {
        retryForDependentDataTypes(getSyncListener(DataTypes.SWITCH_USER));
    }

    public void firstDeviceSyncComplete() {
        retryForDependentDataTypes(getSyncListener(DataTypes.DEVICE));
    }

    private void retryForDependentDataTypes(SyncListener syncListener) {
        Set<String> dependentChildDataTypes;
        if (syncListener != null && (dependentChildDataTypes = syncListener.getDependentChildDataTypes()) != null) {
            for (String str : dependentChildDataTypes) {
                triggerSync(false, str);
            }
        }
    }

    public Set<String> getDataTypesWithChangedData() {
        return this.dataTypesWithChangedData;
    }

    private void addDataTypeWithChangedData(String str) {
        if (this.dataTypesWithChangedData == null) {
            this.dataTypesWithChangedData = new HashSet();
        }
        this.dataTypesWithChangedData.add(str);
    }

    public void onForeground() {
        triggerSync(true, DataTypes.SWITCH_USER, DataTypes.USER, DataTypes.ANALYTICS_EVENT);
    }

    public void onBackground() {
        cancelBatcherJob();
        triggerSync(true, DataTypes.SWITCH_USER, DataTypes.DEVICE, DataTypes.USER, DataTypes.SESSION, DataTypes.ANALYTICS_EVENT);
    }

    private void startBatcherThread() {
        if (this.handler == null) {
            HandlerThread handlerThread = new HandlerThread("HS-cm-agg-sync");
            handlerThread.start();
            this.handler = new Handler(handlerThread.getLooper());
        }
    }

    /* access modifiers changed from: package-private */
    public void cleanUpBatcherJob() {
        this.isBatcherScheduled.compareAndSet(true, false);
        if (this.dataTypesWithChangedData != null) {
            this.dataTypesWithChangedData.clear();
        }
    }

    private void cancelBatcherJob() {
        cleanUpBatcherJob();
        if (this.handler != null && this.batcherJob != null) {
            this.handler.removeCallbacks(getBatcherJob());
        }
    }

    private boolean isDataTypeAllowedForImmediateSync(String str) {
        return str.equals(DataTypes.USER) || str.equals(DataTypes.ANALYTICS_EVENT) || (str.equals(DataTypes.DEVICE) && InfoModelFactory.getInstance().sdkInfoModel.getDevicePropertiesSyncImmediately().booleanValue()) || str.equals(DataTypes.SWITCH_USER);
    }
}
