package com.helpshift.controllers;

import com.helpshift.specifications.SyncSpecification;
import com.helpshift.storage.KeyValueStorage;
import com.helpshift.storage.StorageFactory;
import com.helpshift.util.TimeUtil;

public class ControllerFactory {
    public final DataSyncCoordinator dataSyncCoordinator;
    public final SyncController syncController;

    ControllerFactory() {
        KeyValueStorage keyValueStorage = StorageFactory.getInstance().keyValueStorage;
        this.syncController = new SyncController(keyValueStorage, new TimeUtil(), new SyncSpecification[0]);
        this.dataSyncCoordinator = new DataSyncCoordinatorImpl(keyValueStorage, this.syncController);
    }

    public static ControllerFactory getInstance() {
        return LazyHolder.INSTANCE;
    }

    private static class LazyHolder {
        static final ControllerFactory INSTANCE = new ControllerFactory();

        private LazyHolder() {
        }
    }
}
