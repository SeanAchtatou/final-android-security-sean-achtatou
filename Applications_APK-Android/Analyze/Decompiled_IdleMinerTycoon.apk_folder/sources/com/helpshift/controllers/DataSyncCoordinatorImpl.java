package com.helpshift.controllers;

import com.helpshift.storage.KeyValueStorage;

public class DataSyncCoordinatorImpl implements DataSyncCoordinator {
    public static final String FIRST_DEVICE_SYNC_COMPLETE_KEY = "firstDeviceSyncComplete";
    private DataSyncCompletionListener dataSyncCompletionListener;
    private KeyValueStorage storage;

    protected DataSyncCoordinatorImpl(KeyValueStorage keyValueStorage) {
        this.storage = keyValueStorage;
    }

    protected DataSyncCoordinatorImpl(KeyValueStorage keyValueStorage, DataSyncCompletionListener dataSyncCompletionListener2) {
        this(keyValueStorage);
        this.dataSyncCompletionListener = dataSyncCompletionListener2;
    }

    private static boolean isBoolean(Boolean bool) {
        return bool != null && bool.booleanValue();
    }

    private boolean canSyncProperties(String str) {
        boolean isBoolean = isBoolean((Boolean) this.storage.get(FIRST_DEVICE_SYNC_COMPLETE_KEY));
        KeyValueStorage keyValueStorage = this.storage;
        StringBuilder sb = new StringBuilder();
        sb.append("switchUserCompleteFor");
        sb.append(str);
        return isBoolean && isBoolean((Boolean) keyValueStorage.get(sb.toString()));
    }

    public boolean canSyncUserProperties(String str) {
        return canSyncProperties(str);
    }

    public boolean canSyncSessionProperties(String str) {
        return canSyncProperties(str);
    }

    public void firstDeviceSyncComplete() {
        this.storage.set(FIRST_DEVICE_SYNC_COMPLETE_KEY, true);
        if (this.dataSyncCompletionListener != null) {
            this.dataSyncCompletionListener.firstDeviceSyncComplete();
        }
    }

    public boolean isFirstDeviceSyncComplete() {
        return isBoolean((Boolean) this.storage.get(FIRST_DEVICE_SYNC_COMPLETE_KEY));
    }

    public void switchUserPending(String str) {
        KeyValueStorage keyValueStorage = this.storage;
        keyValueStorage.set("switchUserCompleteFor" + str, false);
    }

    public void switchUserComplete(String str) {
        KeyValueStorage keyValueStorage = this.storage;
        keyValueStorage.set("switchUserCompleteFor" + str, true);
        if (this.dataSyncCompletionListener != null) {
            this.dataSyncCompletionListener.switchUserComplete(str);
        }
    }
}
