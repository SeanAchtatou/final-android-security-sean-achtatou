package com.helpshift.account.dao;

import android.support.annotation.NonNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserDBInfo {
    static final String ACTIVE = "active";
    static final String ANALYTICS_EVENT_ID = "analytics_event_id";
    static final String ANONOYMOUS = "anonymous";
    static final String AUTH_TOKEN = "auth_token";
    static final String COLUMN_ID = "_id";
    static final String CREATE_CLEARED_USER_TABLE = "CREATE TABLE cleared_user_table ( _id INTEGER PRIMARY KEY AUTOINCREMENT, identifier TEXT, name TEXT, email TEXT, deviceid TEXT, auth_token TEXT, sync_state INTEGER );";
    static final String CREATE_LEGACY_ANALYTICS_EVENT_IDS_TABLE = "CREATE TABLE legacy_analytics_event_id_table ( identifier TEXT, analytics_event_id TEXT );";
    static final String CREATE_LEGACY_PROFILE_TABLE = "CREATE TABLE legacy_profile_table ( identifier TEXT PRIMARY KEY, name TEXT, email TEXT, serverid TEXT, migration_state INTEGER );";
    static final String CREATE_REDACTION_INFO_TABLE = "CREATE TABLE redaction_info_table ( user_local_id INTEGER PRIMARY KEY, redaction_state INTEGER , redaction_type INTEGER );";
    static final String CREATE_USER_TABLE = "CREATE TABLE user_table(_id INTEGER PRIMARY KEY AUTOINCREMENT, identifier TEXT, name TEXT, email TEXT, deviceid TEXT, auth_token TEXT, active INTEGER DEFAULT 0, anonymous INTEGER DEFAULT 0, issue_exists INTEGER DEFAULT 1, initial_state_synced INTEGER DEFAULT 0, push_token_synced INTEGER DEFAULT 0 );";
    static final String DATABASE_NAME = "__hs_db_helpshift_users";
    static final Integer DATABASE_VERSION = 2;
    static final String DEVICE_ID = "deviceid";
    static final String EMAIL = "email";
    static final String IDENTIFIER = "identifier";
    static final String INITIAL_STATE_SYNCED = "initial_state_synced";
    static final Integer INT_TRUE = 1;
    static final String ISSUE_EXISTS = "issue_exists";
    static final String MIGRATION_STATE = "migration_state";
    static final String NAME = "name";
    static final String PUSH_TOKEN_SYNCED = "push_token_synced";
    static final String REDACTION_STATE = "redaction_state";
    static final String REDACTION_TYPE = "redaction_type";
    static final String SERVER_ID = "serverid";
    static final String SYNC_STATE = "sync_state";
    static final String TABLE_CLEARED_USERS = "cleared_user_table";
    static final String TABLE_LEGACY_ANALYTICS_EVENT_IDS = "legacy_analytics_event_id_table";
    static final String TABLE_LEGACY_PROFILES = "legacy_profile_table";
    static final String TABLE_REDACTION_INFO = "redaction_info_table";
    static final String TABLE_USERS = "user_table";
    static final String USER_LOCAL_ID = "user_local_id";
    static final String WHERE_LOCAL_ID_IS = "_id = ?";

    @NonNull
    public List<String> getQueriesForOnCreate() {
        return new ArrayList(Arrays.asList(CREATE_USER_TABLE, CREATE_CLEARED_USER_TABLE, CREATE_LEGACY_PROFILE_TABLE, CREATE_LEGACY_ANALYTICS_EVENT_IDS_TABLE, CREATE_REDACTION_INFO_TABLE));
    }

    @NonNull
    public List<String> getQueriesForOnUpgrade(int i) {
        ArrayList arrayList = new ArrayList();
        if (i == 1) {
            arrayList.add(CREATE_REDACTION_INFO_TABLE);
        }
        return arrayList;
    }

    @NonNull
    public List<String> getQueriesForDropAndCreate() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("DROP TABLE IF EXISTS user_table");
        arrayList.add("DROP TABLE IF EXISTS legacy_profile_table");
        arrayList.add("DROP TABLE IF EXISTS cleared_user_table");
        arrayList.add("DROP TABLE IF EXISTS legacy_analytics_event_id_table");
        arrayList.add("DROP TABLE IF EXISTS redaction_info_table");
        arrayList.addAll(getQueriesForOnCreate());
        return arrayList;
    }
}
