package com.helpshift.account.dao.legacy;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.helpshift.account.dao.ProfileDTO;
import com.helpshift.migration.legacyUser.LegacyProfileDAO;

public class AndroidLegacyProfileDAO extends SQLiteOpenHelper implements LegacyProfileDAO {
    private static final String ALTER_PROFILES_TABLE_ADD_DEVICE_ID = "ALTER TABLE profiles ADD did TEXT";
    private static final String ALTER_PROFILES_TABLE_ADD_PUSH_TOKEN_SYNC_STATUS = "ALTER TABLE profiles ADD push_token_sync INTEGER";
    private static final String ALTER_PROFILES_TABLE_ADD_USER_ID = "ALTER TABLE profiles ADD uid TEXT";
    private static final String COLUMN_DID = "did";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_IDENTIFIER = "IDENTIFIER";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_PROFILE_ID = "profile_id";
    private static final String COLUMN_PUSH_TOKEN_SYNC_STATUS = "push_token_sync";
    private static final String COLUMN_SALT = "salt";
    private static final String COLUMN_UID = "uid";
    private static final String DATABASE_CREATE = "CREATE TABLE profiles(_id INTEGER PRIMARY KEY AUTOINCREMENT, IDENTIFIER TEXT NOT NULL UNIQUE, profile_id TEXT UNIQUE, name TEXT, email TEXT, salt TEXT, uid TEXT, did TEXT, push_token_sync INTEGER );";
    private static final String DATABASE_NAME = "__hs__db_profiles";
    private static final int DATABASE_VERSION = 3;
    private static final String TABLE_PROFILES = "profiles";
    private static final String TAG = "Helpshift_ALProfileDAO";
    private static AndroidLegacyProfileDAO instance;

    private AndroidLegacyProfileDAO(Context context) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 3);
    }

    public static synchronized AndroidLegacyProfileDAO getInstance(Context context) {
        AndroidLegacyProfileDAO androidLegacyProfileDAO;
        synchronized (AndroidLegacyProfileDAO.class) {
            if (instance == null) {
                instance = new AndroidLegacyProfileDAO(context);
            }
            androidLegacyProfileDAO = instance;
        }
        return androidLegacyProfileDAO;
    }

    private static int getColumnIndexForIdentifier(Cursor cursor) {
        int columnIndex = cursor.getColumnIndex(COLUMN_IDENTIFIER);
        return columnIndex == -1 ? cursor.getColumnIndex(COLUMN_IDENTIFIER.toLowerCase()) : columnIndex;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0052  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.helpshift.account.dao.ProfileDTO> fetchProfiles() {
        /*
            r10 = this;
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r10.getReadableDatabase()     // Catch:{ Exception -> 0x003d, all -> 0x0038 }
            java.lang.String r2 = "profiles"
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r1 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x003d, all -> 0x0038 }
            boolean r2 = r1.moveToFirst()     // Catch:{ Exception -> 0x0033 }
            if (r2 == 0) goto L_0x002d
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Exception -> 0x0033 }
            r2.<init>()     // Catch:{ Exception -> 0x0033 }
        L_0x001c:
            com.helpshift.account.dao.ProfileDTO r0 = r10.cursorToProfile(r1)     // Catch:{ Exception -> 0x002b }
            r2.add(r0)     // Catch:{ Exception -> 0x002b }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x002b }
            if (r0 != 0) goto L_0x001c
            r0 = r2
            goto L_0x002d
        L_0x002b:
            r0 = move-exception
            goto L_0x0041
        L_0x002d:
            if (r1 == 0) goto L_0x004e
            r1.close()
            goto L_0x004e
        L_0x0033:
            r2 = move-exception
            r9 = r2
            r2 = r0
            r0 = r9
            goto L_0x0041
        L_0x0038:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0050
        L_0x003d:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r2
        L_0x0041:
            java.lang.String r3 = "Helpshift_ALProfileDAO"
            java.lang.String r4 = "Error in fetchProfiles"
            com.helpshift.util.HSLogger.e(r3, r4, r0)     // Catch:{ all -> 0x004f }
            if (r1 == 0) goto L_0x004d
            r1.close()
        L_0x004d:
            r0 = r2
        L_0x004e:
            return r0
        L_0x004f:
            r0 = move-exception
        L_0x0050:
            if (r1 == 0) goto L_0x0055
            r1.close()
        L_0x0055:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.account.dao.legacy.AndroidLegacyProfileDAO.fetchProfiles():java.util.List");
    }

    public void deleteProfiles() {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        if (writableDatabase != null) {
            writableDatabase.execSQL("DROP TABLE IF EXISTS profiles");
        }
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL(DATABASE_CREATE);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (sQLiteDatabase.isOpen()) {
            if (i < 2) {
                sQLiteDatabase.execSQL(ALTER_PROFILES_TABLE_ADD_USER_ID);
                sQLiteDatabase.execSQL(ALTER_PROFILES_TABLE_ADD_DEVICE_ID);
            }
            if (i < 3) {
                sQLiteDatabase.execSQL(ALTER_PROFILES_TABLE_ADD_PUSH_TOKEN_SYNC_STATUS);
            }
        }
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (sQLiteDatabase.isOpen()) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS profiles");
            onCreate(sQLiteDatabase);
        }
    }

    private ProfileDTO cursorToProfile(Cursor cursor) {
        return new ProfileDTO(Long.valueOf(cursor.getLong(cursor.getColumnIndex(COLUMN_ID))), cursor.getString(getColumnIndexForIdentifier(cursor)), cursor.getString(cursor.getColumnIndex(COLUMN_PROFILE_ID)), cursor.getString(cursor.getColumnIndex("name")), cursor.getString(cursor.getColumnIndex("email")), cursor.getString(cursor.getColumnIndex(COLUMN_SALT)), cursor.getString(cursor.getColumnIndex(COLUMN_UID)), cursor.getString(cursor.getColumnIndex(COLUMN_DID)), cursor.getInt(cursor.getColumnIndex(COLUMN_PUSH_TOKEN_SYNC_STATUS)) == 1);
    }
}
