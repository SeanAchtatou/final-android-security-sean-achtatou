package com.helpshift.account.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.google.android.gms.measurement.api.AppMeasurementSdk;
import com.helpshift.account.domainmodel.ClearedUserDM;
import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.account.domainmodel.UserSyncStatus;
import com.helpshift.common.platform.network.KeyValuePair;
import com.helpshift.migration.MigrationState;
import com.helpshift.migration.legacyUser.LegacyProfile;
import com.helpshift.redaction.RedactionDetail;
import com.helpshift.redaction.RedactionState;
import com.helpshift.redaction.RedactionType;
import com.helpshift.util.HSLogger;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;

public class UserDB {
    private static final String TAG = "Helpshift_UserDB";
    private static UserDB instance;
    private final UserDBHelper userDBHelper;
    private final UserDBInfo userDBInfo = new UserDBInfo();

    private UserDB(Context context) {
        this.userDBHelper = new UserDBHelper(context, this.userDBInfo);
    }

    public static synchronized UserDB getInstance(Context context) {
        UserDB userDB;
        synchronized (UserDB.class) {
            if (instance == null) {
                instance = new UserDB(context);
            }
            userDB = instance;
        }
        return userDB;
    }

    /* access modifiers changed from: package-private */
    public UserDM createUser(UserDM userDM) {
        Long l;
        try {
            l = Long.valueOf(this.userDBHelper.getWritableDatabase().insert("user_table", null, userDMToContentValues(userDM)));
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in creating user", e);
            l = null;
        }
        if (l == null) {
            return null;
        }
        return getUserDMWithLocalId(userDM, l.longValue());
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean updateUser(UserDM userDM) {
        boolean z;
        if (userDM.getLocalId() == null) {
            return false;
        }
        z = true;
        try {
            this.userDBHelper.getWritableDatabase().update("user_table", userDMToContentValues(userDM), "_id = ?", new String[]{String.valueOf(userDM.getLocalId())});
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in updating user", e);
            z = false;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0033, code lost:
        if (r11 != null) goto L_0x0021;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x003f, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0043, code lost:
        throw r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001f, code lost:
        if (r11 != null) goto L_0x0021;
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x003b A[SYNTHETIC, Splitter:B:25:0x003b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized com.helpshift.account.domainmodel.UserDM getUser(java.lang.String r11, java.lang.String[] r12) {
        /*
            r10 = this;
            monitor-enter(r10)
            r0 = 0
            com.helpshift.account.dao.UserDBHelper r1 = r10.userDBHelper     // Catch:{ Exception -> 0x002a, all -> 0x0027 }
            android.database.sqlite.SQLiteDatabase r2 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x002a, all -> 0x0027 }
            java.lang.String r3 = "user_table"
            r4 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r5 = r11
            r6 = r12
            android.database.Cursor r11 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x002a, all -> 0x0027 }
            boolean r12 = r11.moveToFirst()     // Catch:{ Exception -> 0x0025 }
            if (r12 == 0) goto L_0x001f
            com.helpshift.account.domainmodel.UserDM r12 = r10.cursorToUserDM(r11)     // Catch:{ Exception -> 0x0025 }
            r0 = r12
        L_0x001f:
            if (r11 == 0) goto L_0x0036
        L_0x0021:
            r11.close()     // Catch:{ all -> 0x003f }
            goto L_0x0036
        L_0x0025:
            r12 = move-exception
            goto L_0x002c
        L_0x0027:
            r12 = move-exception
            r11 = r0
            goto L_0x0039
        L_0x002a:
            r12 = move-exception
            r11 = r0
        L_0x002c:
            java.lang.String r1 = "Helpshift_UserDB"
            java.lang.String r2 = "Error in reading user"
            com.helpshift.util.HSLogger.e(r1, r2, r12)     // Catch:{ all -> 0x0038 }
            if (r11 == 0) goto L_0x0036
            goto L_0x0021
        L_0x0036:
            monitor-exit(r10)
            return r0
        L_0x0038:
            r12 = move-exception
        L_0x0039:
            if (r11 == 0) goto L_0x0041
            r11.close()     // Catch:{ all -> 0x003f }
            goto L_0x0041
        L_0x003f:
            r11 = move-exception
            goto L_0x0042
        L_0x0041:
            throw r12     // Catch:{ all -> 0x003f }
        L_0x0042:
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.account.dao.UserDB.getUser(java.lang.String, java.lang.String[]):com.helpshift.account.domainmodel.UserDM");
    }

    /* access modifiers changed from: package-private */
    public synchronized UserDM fetchUser(Long l) {
        if (l == null) {
            return null;
        }
        return getUser("_id = ?", new String[]{l.toString()});
    }

    /* access modifiers changed from: package-private */
    public synchronized UserDM fetchUser(String str, String str2) {
        if (str == null && str2 == null) {
            return null;
        }
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "";
        }
        return getUser("identifier = ? AND email = ?", new String[]{str, str2});
    }

    /* access modifiers changed from: package-private */
    public synchronized UserDM getActiveUser() {
        return getUser("active = ?", new String[]{UserDBInfo.INT_TRUE.toString()});
    }

    /* access modifiers changed from: package-private */
    public synchronized UserDM getAnonymousUser() {
        return getUser("anonymous = ?", new String[]{UserDBInfo.INT_TRUE.toString()});
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002c, code lost:
        if (r2 != null) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0042, code lost:
        if (r2 != null) goto L_0x002e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x004a A[SYNTHETIC, Splitter:B:27:0x004a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<com.helpshift.account.domainmodel.UserDM> fetchUsers() {
        /*
            r12 = this;
            monitor-enter(r12)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x004e }
            r0.<init>()     // Catch:{ all -> 0x004e }
            r1 = 0
            com.helpshift.account.dao.UserDBHelper r2 = r12.userDBHelper     // Catch:{ Exception -> 0x0037, all -> 0x0034 }
            android.database.sqlite.SQLiteDatabase r3 = r2.getReadableDatabase()     // Catch:{ Exception -> 0x0037, all -> 0x0034 }
            java.lang.String r4 = "user_table"
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r2 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x0037, all -> 0x0034 }
            boolean r1 = r2.moveToFirst()     // Catch:{ Exception -> 0x0032 }
            if (r1 == 0) goto L_0x002c
        L_0x001f:
            com.helpshift.account.domainmodel.UserDM r1 = r12.cursorToUserDM(r2)     // Catch:{ Exception -> 0x0032 }
            r0.add(r1)     // Catch:{ Exception -> 0x0032 }
            boolean r1 = r2.moveToNext()     // Catch:{ Exception -> 0x0032 }
            if (r1 != 0) goto L_0x001f
        L_0x002c:
            if (r2 == 0) goto L_0x0045
        L_0x002e:
            r2.close()     // Catch:{ all -> 0x004e }
            goto L_0x0045
        L_0x0032:
            r1 = move-exception
            goto L_0x003b
        L_0x0034:
            r0 = move-exception
            r2 = r1
            goto L_0x0048
        L_0x0037:
            r2 = move-exception
            r11 = r2
            r2 = r1
            r1 = r11
        L_0x003b:
            java.lang.String r3 = "Helpshift_UserDB"
            java.lang.String r4 = "Error in reading all users"
            com.helpshift.util.HSLogger.e(r3, r4, r1)     // Catch:{ all -> 0x0047 }
            if (r2 == 0) goto L_0x0045
            goto L_0x002e
        L_0x0045:
            monitor-exit(r12)
            return r0
        L_0x0047:
            r0 = move-exception
        L_0x0048:
            if (r2 == 0) goto L_0x004d
            r2.close()     // Catch:{ all -> 0x004e }
        L_0x004d:
            throw r0     // Catch:{ all -> 0x004e }
        L_0x004e:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.account.dao.UserDB.fetchUsers():java.util.List");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0073 A[SYNTHETIC, Splitter:B:31:0x0073] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0083 A[SYNTHETIC, Splitter:B:40:0x0083] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean activateUser(java.lang.Long r10) {
        /*
            r9 = this;
            monitor-enter(r9)
            r0 = 0
            if (r10 != 0) goto L_0x0006
            monitor-exit(r9)
            return r0
        L_0x0006:
            r1 = 0
            r2 = 1
            com.helpshift.account.dao.UserDBHelper r3 = r9.userDBHelper     // Catch:{ Exception -> 0x0069 }
            android.database.sqlite.SQLiteDatabase r3 = r3.getWritableDatabase()     // Catch:{ Exception -> 0x0069 }
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            r1.<init>()     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            java.lang.String r4 = "active"
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            r1.put(r4, r5)     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            android.content.ContentValues r4 = new android.content.ContentValues     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            r4.<init>()     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            java.lang.String r5 = "active"
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r0)     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            r3.beginTransaction()     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            java.lang.String r5 = "user_table"
            java.lang.String r6 = "_id = ?"
            java.lang.String[] r7 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            java.lang.String r8 = r10.toString()     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            r7[r0] = r8     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            int r1 = r3.update(r5, r1, r6, r7)     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            if (r1 <= 0) goto L_0x004e
            java.lang.String r1 = "user_table"
            java.lang.String r5 = "_id != ?"
            java.lang.String[] r6 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            r6[r0] = r10     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            r3.update(r1, r4, r5, r6)     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
        L_0x004e:
            r3.setTransactionSuccessful()     // Catch:{ Exception -> 0x0063, all -> 0x0061 }
            if (r3 == 0) goto L_0x005f
            r3.endTransaction()     // Catch:{ Exception -> 0x0057 }
            goto L_0x005f
        L_0x0057:
            r10 = move-exception
            java.lang.String r0 = "Helpshift_UserDB"
            java.lang.String r1 = "Error in activating user in finally block"
            com.helpshift.util.HSLogger.e(r0, r1, r10)     // Catch:{ all -> 0x0087 }
        L_0x005f:
            r0 = 1
            goto L_0x007f
        L_0x0061:
            r10 = move-exception
            goto L_0x0081
        L_0x0063:
            r10 = move-exception
            r1 = r3
            goto L_0x006a
        L_0x0066:
            r10 = move-exception
            r3 = r1
            goto L_0x0081
        L_0x0069:
            r10 = move-exception
        L_0x006a:
            java.lang.String r2 = "Helpshift_UserDB"
            java.lang.String r3 = "Error in activating user"
            com.helpshift.util.HSLogger.e(r2, r3, r10)     // Catch:{ all -> 0x0066 }
            if (r1 == 0) goto L_0x007f
            r1.endTransaction()     // Catch:{ Exception -> 0x0077 }
            goto L_0x007f
        L_0x0077:
            r10 = move-exception
            java.lang.String r1 = "Helpshift_UserDB"
            java.lang.String r2 = "Error in activating user in finally block"
            com.helpshift.util.HSLogger.e(r1, r2, r10)     // Catch:{ all -> 0x0087 }
        L_0x007f:
            monitor-exit(r9)
            return r0
        L_0x0081:
            if (r3 == 0) goto L_0x0091
            r3.endTransaction()     // Catch:{ Exception -> 0x0089 }
            goto L_0x0091
        L_0x0087:
            r10 = move-exception
            goto L_0x0092
        L_0x0089:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_UserDB"
            java.lang.String r2 = "Error in activating user in finally block"
            com.helpshift.util.HSLogger.e(r1, r2, r0)     // Catch:{ all -> 0x0087 }
        L_0x0091:
            throw r10     // Catch:{ all -> 0x0087 }
        L_0x0092:
            monitor-exit(r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.account.dao.UserDB.activateUser(java.lang.Long):boolean");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0032, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean deleteUser(java.lang.Long r9) {
        /*
            r8 = this;
            monitor-enter(r8)
            r0 = 0
            if (r9 != 0) goto L_0x0006
            monitor-exit(r8)
            return r0
        L_0x0006:
            r1 = 1
            r2 = 0
            com.helpshift.account.dao.UserDBHelper r4 = r8.userDBHelper     // Catch:{ Exception -> 0x0023 }
            android.database.sqlite.SQLiteDatabase r4 = r4.getWritableDatabase()     // Catch:{ Exception -> 0x0023 }
            java.lang.String r5 = "user_table"
            java.lang.String r6 = "_id = ?"
            java.lang.String[] r7 = new java.lang.String[r1]     // Catch:{ Exception -> 0x0023 }
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ Exception -> 0x0023 }
            r7[r0] = r9     // Catch:{ Exception -> 0x0023 }
            int r9 = r4.delete(r5, r6, r7)     // Catch:{ Exception -> 0x0023 }
            long r4 = (long) r9
            goto L_0x002c
        L_0x0021:
            r9 = move-exception
            goto L_0x0033
        L_0x0023:
            r9 = move-exception
            java.lang.String r4 = "Helpshift_UserDB"
            java.lang.String r5 = "Error in deleting user"
            com.helpshift.util.HSLogger.e(r4, r5, r9)     // Catch:{ all -> 0x0021 }
            r4 = r2
        L_0x002c:
            int r9 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r9 <= 0) goto L_0x0031
            r0 = 1
        L_0x0031:
            monitor-exit(r8)
            return r0
        L_0x0033:
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.account.dao.UserDB.deleteUser(java.lang.Long):boolean");
    }

    private UserDM cursorToUserDM(Cursor cursor) {
        Long valueOf = Long.valueOf(cursor.getLong(cursor.getColumnIndex("_id")));
        String string = cursor.getString(cursor.getColumnIndex(SettingsJsonConstants.APP_IDENTIFIER_KEY));
        String string2 = cursor.getString(cursor.getColumnIndex("name"));
        String string3 = cursor.getString(cursor.getColumnIndex("email"));
        String string4 = cursor.getString(cursor.getColumnIndex("deviceid"));
        String string5 = cursor.getString(cursor.getColumnIndex("auth_token"));
        return new UserDM(valueOf, string, string3, string2, string4, cursor.getInt(cursor.getColumnIndex(AppMeasurementSdk.ConditionalUserProperty.ACTIVE)) == UserDBInfo.INT_TRUE.intValue(), cursor.getInt(cursor.getColumnIndex("anonymous")) == UserDBInfo.INT_TRUE.intValue(), cursor.getInt(cursor.getColumnIndex("push_token_synced")) == UserDBInfo.INT_TRUE.intValue(), string5, cursor.getInt(cursor.getColumnIndex("issue_exists")) == UserDBInfo.INT_TRUE.intValue(), intToUserSyncStatus(cursor.getInt(cursor.getColumnIndex("initial_state_synced"))));
    }

    private ClearedUserDM cursorToClearedUserDM(Cursor cursor) {
        return new ClearedUserDM(Long.valueOf(cursor.getLong(cursor.getColumnIndex("_id"))), cursor.getString(cursor.getColumnIndex(SettingsJsonConstants.APP_IDENTIFIER_KEY)), cursor.getString(cursor.getColumnIndex("email")), cursor.getString(cursor.getColumnIndex("auth_token")), cursor.getString(cursor.getColumnIndex("deviceid")), intToClearedUserSyncState(cursor.getInt(cursor.getColumnIndex("sync_state"))));
    }

    private LegacyProfile cursorToLegacyProfile(Cursor cursor) {
        return new LegacyProfile(cursor.getString(cursor.getColumnIndex(SettingsJsonConstants.APP_IDENTIFIER_KEY)), cursor.getString(cursor.getColumnIndex("email")), cursor.getString(cursor.getColumnIndex("name")), cursor.getString(cursor.getColumnIndex("serverid")), intToMigrationState(cursor.getInt(cursor.getColumnIndex("migration_state"))));
    }

    private ContentValues clearedUserDMtoContentValues(ClearedUserDM clearedUserDM) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("_id", clearedUserDM.localId);
        contentValues.put(SettingsJsonConstants.APP_IDENTIFIER_KEY, clearedUserDM.identifier);
        contentValues.put("email", clearedUserDM.email);
        contentValues.put("auth_token", clearedUserDM.authToken);
        contentValues.put("deviceid", clearedUserDM.deviceId);
        contentValues.put("sync_state", Integer.valueOf(clearedUserDM.syncState.ordinal()));
        return contentValues;
    }

    private ContentValues userDMToContentValues(UserDM userDM) {
        ContentValues contentValues = new ContentValues();
        if (userDM.getLocalId() != null) {
            contentValues.put("_id", userDM.getLocalId());
        }
        if (userDM.getIdentifier() != null) {
            contentValues.put(SettingsJsonConstants.APP_IDENTIFIER_KEY, userDM.getIdentifier());
        } else {
            contentValues.put(SettingsJsonConstants.APP_IDENTIFIER_KEY, "");
        }
        if (userDM.getName() != null) {
            contentValues.put("name", userDM.getName());
        } else {
            contentValues.put("name", "");
        }
        if (userDM.getEmail() != null) {
            contentValues.put("email", userDM.getEmail());
        } else {
            contentValues.put("email", "");
        }
        if (userDM.getDeviceId() != null) {
            contentValues.put("deviceid", userDM.getDeviceId());
        } else {
            contentValues.put("deviceid", "");
        }
        if (userDM.getAuthToken() != null) {
            contentValues.put("auth_token", userDM.getAuthToken());
        } else {
            contentValues.put("auth_token", "");
        }
        contentValues.put(AppMeasurementSdk.ConditionalUserProperty.ACTIVE, Boolean.valueOf(userDM.isActiveUser()));
        contentValues.put("anonymous", Boolean.valueOf(userDM.isAnonymousUser()));
        contentValues.put("issue_exists", Boolean.valueOf(userDM.issueExists()));
        contentValues.put("push_token_synced", Boolean.valueOf(userDM.isPushTokenSynced()));
        contentValues.put("initial_state_synced", Integer.valueOf(userDM.getSyncState().ordinal()));
        return contentValues;
    }

    private ContentValues legacyProfileToContentValues(LegacyProfile legacyProfile) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SettingsJsonConstants.APP_IDENTIFIER_KEY, legacyProfile.identifier);
        contentValues.put("name", legacyProfile.name);
        contentValues.put("email", legacyProfile.email);
        contentValues.put("serverid", legacyProfile.serverId);
        contentValues.put("migration_state", Integer.valueOf(legacyProfile.migrationState.ordinal()));
        return contentValues;
    }

    private ContentValues legacyAnalyticsIDPairToContentValues(KeyValuePair keyValuePair) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SettingsJsonConstants.APP_IDENTIFIER_KEY, keyValuePair.key);
        contentValues.put("analytics_event_id", keyValuePair.value);
        return contentValues;
    }

    private UserDM getUserDMWithLocalId(UserDM userDM, long j) {
        return new UserDM(Long.valueOf(j), userDM.getIdentifier(), userDM.getEmail(), userDM.getName(), userDM.getDeviceId(), userDM.isActiveUser(), userDM.isAnonymousUser(), userDM.isPushTokenSynced(), userDM.getAuthToken(), userDM.issueExists(), userDM.getSyncState());
    }

    private ClearedUserDM getClearUserDMWithLocalId(ClearedUserDM clearedUserDM, long j) {
        return new ClearedUserDM(Long.valueOf(j), clearedUserDM.identifier, clearedUserDM.email, clearedUserDM.authToken, clearedUserDM.deviceId, clearedUserDM.syncState);
    }

    /* access modifiers changed from: package-private */
    public synchronized ClearedUserDM insertClearedUser(ClearedUserDM clearedUserDM) {
        Long l;
        try {
            l = Long.valueOf(this.userDBHelper.getWritableDatabase().insert("cleared_user_table", null, clearedUserDMtoContentValues(clearedUserDM)));
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in creating cleared user", e);
            l = null;
        }
        if (l == null) {
            return null;
        }
        return getClearUserDMWithLocalId(clearedUserDM, l.longValue());
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002c, code lost:
        if (r2 != null) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0042, code lost:
        if (r2 != null) goto L_0x002e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x004a A[SYNTHETIC, Splitter:B:27:0x004a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<com.helpshift.account.domainmodel.ClearedUserDM> fetchClearedUsers() {
        /*
            r12 = this;
            monitor-enter(r12)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x004e }
            r0.<init>()     // Catch:{ all -> 0x004e }
            r1 = 0
            com.helpshift.account.dao.UserDBHelper r2 = r12.userDBHelper     // Catch:{ Exception -> 0x0037, all -> 0x0034 }
            android.database.sqlite.SQLiteDatabase r3 = r2.getReadableDatabase()     // Catch:{ Exception -> 0x0037, all -> 0x0034 }
            java.lang.String r4 = "cleared_user_table"
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r2 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x0037, all -> 0x0034 }
            boolean r1 = r2.moveToFirst()     // Catch:{ Exception -> 0x0032 }
            if (r1 == 0) goto L_0x002c
        L_0x001f:
            com.helpshift.account.domainmodel.ClearedUserDM r1 = r12.cursorToClearedUserDM(r2)     // Catch:{ Exception -> 0x0032 }
            r0.add(r1)     // Catch:{ Exception -> 0x0032 }
            boolean r1 = r2.moveToNext()     // Catch:{ Exception -> 0x0032 }
            if (r1 != 0) goto L_0x001f
        L_0x002c:
            if (r2 == 0) goto L_0x0045
        L_0x002e:
            r2.close()     // Catch:{ all -> 0x004e }
            goto L_0x0045
        L_0x0032:
            r1 = move-exception
            goto L_0x003b
        L_0x0034:
            r0 = move-exception
            r2 = r1
            goto L_0x0048
        L_0x0037:
            r2 = move-exception
            r11 = r2
            r2 = r1
            r1 = r11
        L_0x003b:
            java.lang.String r3 = "Helpshift_UserDB"
            java.lang.String r4 = "Error in reading all cleared users"
            com.helpshift.util.HSLogger.e(r3, r4, r1)     // Catch:{ all -> 0x0047 }
            if (r2 == 0) goto L_0x0045
            goto L_0x002e
        L_0x0045:
            monitor-exit(r12)
            return r0
        L_0x0047:
            r0 = move-exception
        L_0x0048:
            if (r2 == 0) goto L_0x004d
            r2.close()     // Catch:{ all -> 0x004e }
        L_0x004d:
            throw r0     // Catch:{ all -> 0x004e }
        L_0x004e:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.account.dao.UserDB.fetchClearedUsers():java.util.List");
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean updateClearedUserSyncState(Long l, ClearedUserSyncState clearedUserSyncState) {
        boolean z;
        z = true;
        try {
            SQLiteDatabase writableDatabase = this.userDBHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("sync_state", Integer.valueOf(clearedUserSyncState.ordinal()));
            writableDatabase.update("cleared_user_table", contentValues, "_id = ?", new String[]{l.toString()});
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in updating cleared user sync status", e);
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean deleteClearedUser(Long l) {
        boolean z;
        long j;
        z = false;
        try {
            j = (long) this.userDBHelper.getWritableDatabase().delete("user_table", "_id = ?", new String[]{String.valueOf(l)});
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in deleting cleared user", e);
            j = 0;
        }
        if (j > 0) {
            z = true;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0049 A[SYNTHETIC, Splitter:B:27:0x0049] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0057 A[SYNTHETIC, Splitter:B:34:0x0057] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void storeLegacyProfiles(java.util.List<com.helpshift.migration.legacyUser.LegacyProfile> r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            r0 = 0
            com.helpshift.account.dao.UserDBHelper r1 = r4.userDBHelper     // Catch:{ Exception -> 0x003f }
            android.database.sqlite.SQLiteDatabase r1 = r1.getWritableDatabase()     // Catch:{ Exception -> 0x003f }
            r1.beginTransaction()     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
        L_0x000f:
            boolean r2 = r5.hasNext()     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
            if (r2 == 0) goto L_0x0025
            java.lang.Object r2 = r5.next()     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
            com.helpshift.migration.legacyUser.LegacyProfile r2 = (com.helpshift.migration.legacyUser.LegacyProfile) r2     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
            android.content.ContentValues r2 = r4.legacyProfileToContentValues(r2)     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
            java.lang.String r3 = "legacy_profile_table"
            r1.insert(r3, r0, r2)     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
            goto L_0x000f
        L_0x0025:
            r1.setTransactionSuccessful()     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
            if (r1 == 0) goto L_0x0053
            r1.endTransaction()     // Catch:{ Exception -> 0x002e }
            goto L_0x0053
        L_0x002e:
            r5 = move-exception
            java.lang.String r0 = "Helpshift_UserDB"
            java.lang.String r1 = "Error in storing legacy profiles in finally block"
        L_0x0033:
            com.helpshift.util.HSLogger.e(r0, r1, r5)     // Catch:{ all -> 0x005b }
            goto L_0x0053
        L_0x0037:
            r5 = move-exception
            goto L_0x0055
        L_0x0039:
            r5 = move-exception
            r0 = r1
            goto L_0x0040
        L_0x003c:
            r5 = move-exception
            r1 = r0
            goto L_0x0055
        L_0x003f:
            r5 = move-exception
        L_0x0040:
            java.lang.String r1 = "Helpshift_UserDB"
            java.lang.String r2 = "Error in storing legacy profiles"
            com.helpshift.util.HSLogger.e(r1, r2, r5)     // Catch:{ all -> 0x003c }
            if (r0 == 0) goto L_0x0053
            r0.endTransaction()     // Catch:{ Exception -> 0x004d }
            goto L_0x0053
        L_0x004d:
            r5 = move-exception
            java.lang.String r0 = "Helpshift_UserDB"
            java.lang.String r1 = "Error in storing legacy profiles in finally block"
            goto L_0x0033
        L_0x0053:
            monitor-exit(r4)
            return
        L_0x0055:
            if (r1 == 0) goto L_0x0065
            r1.endTransaction()     // Catch:{ Exception -> 0x005d }
            goto L_0x0065
        L_0x005b:
            r5 = move-exception
            goto L_0x0066
        L_0x005d:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_UserDB"
            java.lang.String r2 = "Error in storing legacy profiles in finally block"
            com.helpshift.util.HSLogger.e(r1, r2, r0)     // Catch:{ all -> 0x005b }
        L_0x0065:
            throw r5     // Catch:{ all -> 0x005b }
        L_0x0066:
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.account.dao.UserDB.storeLegacyProfiles(java.util.List):void");
    }

    /* access modifiers changed from: package-private */
    public synchronized void deleteLegacyProfile(String str) {
        try {
            this.userDBHelper.getWritableDatabase().delete("legacy_profile_table", "identifier = ?", new String[]{str});
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in deleting legacy profile", e);
        }
        return;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        if (r0 != null) goto L_0x0027;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003b, code lost:
        if (r0 != null) goto L_0x0027;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0043 A[SYNTHETIC, Splitter:B:28:0x0043] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.helpshift.migration.legacyUser.LegacyProfile fetchLegacyProfile(java.lang.String r11) {
        /*
            r10 = this;
            monitor-enter(r10)
            r0 = 1
            java.lang.String[] r5 = new java.lang.String[r0]     // Catch:{ all -> 0x0047 }
            r0 = 0
            r5[r0] = r11     // Catch:{ all -> 0x0047 }
            r11 = 0
            com.helpshift.account.dao.UserDBHelper r0 = r10.userDBHelper     // Catch:{ Exception -> 0x0032, all -> 0x002d }
            android.database.sqlite.SQLiteDatabase r1 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x0032, all -> 0x002d }
            java.lang.String r2 = "legacy_profile_table"
            r3 = 0
            java.lang.String r4 = "identifier = ?"
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r0 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0032, all -> 0x002d }
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x002b }
            if (r1 == 0) goto L_0x0025
            com.helpshift.migration.legacyUser.LegacyProfile r1 = r10.cursorToLegacyProfile(r0)     // Catch:{ Exception -> 0x002b }
            r11 = r1
        L_0x0025:
            if (r0 == 0) goto L_0x003e
        L_0x0027:
            r0.close()     // Catch:{ all -> 0x0047 }
            goto L_0x003e
        L_0x002b:
            r1 = move-exception
            goto L_0x0034
        L_0x002d:
            r0 = move-exception
            r9 = r0
            r0 = r11
            r11 = r9
            goto L_0x0041
        L_0x0032:
            r1 = move-exception
            r0 = r11
        L_0x0034:
            java.lang.String r2 = "Helpshift_UserDB"
            java.lang.String r3 = "Error in reading legacy profile with identifier"
            com.helpshift.util.HSLogger.e(r2, r3, r1)     // Catch:{ all -> 0x0040 }
            if (r0 == 0) goto L_0x003e
            goto L_0x0027
        L_0x003e:
            monitor-exit(r10)
            return r11
        L_0x0040:
            r11 = move-exception
        L_0x0041:
            if (r0 == 0) goto L_0x0046
            r0.close()     // Catch:{ all -> 0x0047 }
        L_0x0046:
            throw r11     // Catch:{ all -> 0x0047 }
        L_0x0047:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.account.dao.UserDB.fetchLegacyProfile(java.lang.String):com.helpshift.migration.legacyUser.LegacyProfile");
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean updateUserMigrationState(String str, MigrationState migrationState) {
        boolean z;
        z = true;
        try {
            SQLiteDatabase writableDatabase = this.userDBHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("migration_state", Integer.valueOf(migrationState.ordinal()));
            writableDatabase.update("legacy_profile_table", contentValues, "identifier = ?", new String[]{str});
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in updating user migration sync status", e);
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0049 A[SYNTHETIC, Splitter:B:27:0x0049] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0057 A[SYNTHETIC, Splitter:B:34:0x0057] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void storeLegacyAnalyticsEventIds(java.util.List<com.helpshift.common.platform.network.KeyValuePair> r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            r0 = 0
            com.helpshift.account.dao.UserDBHelper r1 = r4.userDBHelper     // Catch:{ Exception -> 0x003f }
            android.database.sqlite.SQLiteDatabase r1 = r1.getWritableDatabase()     // Catch:{ Exception -> 0x003f }
            r1.beginTransaction()     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
        L_0x000f:
            boolean r2 = r5.hasNext()     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
            if (r2 == 0) goto L_0x0025
            java.lang.Object r2 = r5.next()     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
            com.helpshift.common.platform.network.KeyValuePair r2 = (com.helpshift.common.platform.network.KeyValuePair) r2     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
            android.content.ContentValues r2 = r4.legacyAnalyticsIDPairToContentValues(r2)     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
            java.lang.String r3 = "legacy_analytics_event_id_table"
            r1.insert(r3, r0, r2)     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
            goto L_0x000f
        L_0x0025:
            r1.setTransactionSuccessful()     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
            if (r1 == 0) goto L_0x0053
            r1.endTransaction()     // Catch:{ Exception -> 0x002e }
            goto L_0x0053
        L_0x002e:
            r5 = move-exception
            java.lang.String r0 = "Helpshift_UserDB"
            java.lang.String r1 = "Error in storing legacy analytics events in finally block"
        L_0x0033:
            com.helpshift.util.HSLogger.e(r0, r1, r5)     // Catch:{ all -> 0x005b }
            goto L_0x0053
        L_0x0037:
            r5 = move-exception
            goto L_0x0055
        L_0x0039:
            r5 = move-exception
            r0 = r1
            goto L_0x0040
        L_0x003c:
            r5 = move-exception
            r1 = r0
            goto L_0x0055
        L_0x003f:
            r5 = move-exception
        L_0x0040:
            java.lang.String r1 = "Helpshift_UserDB"
            java.lang.String r2 = "Error in storing legacy analytics events"
            com.helpshift.util.HSLogger.e(r1, r2, r5)     // Catch:{ all -> 0x003c }
            if (r0 == 0) goto L_0x0053
            r0.endTransaction()     // Catch:{ Exception -> 0x004d }
            goto L_0x0053
        L_0x004d:
            r5 = move-exception
            java.lang.String r0 = "Helpshift_UserDB"
            java.lang.String r1 = "Error in storing legacy analytics events in finally block"
            goto L_0x0033
        L_0x0053:
            monitor-exit(r4)
            return
        L_0x0055:
            if (r1 == 0) goto L_0x0065
            r1.endTransaction()     // Catch:{ Exception -> 0x005d }
            goto L_0x0065
        L_0x005b:
            r5 = move-exception
            goto L_0x0066
        L_0x005d:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_UserDB"
            java.lang.String r2 = "Error in storing legacy analytics events in finally block"
            com.helpshift.util.HSLogger.e(r1, r2, r0)     // Catch:{ all -> 0x005b }
        L_0x0065:
            throw r5     // Catch:{ all -> 0x005b }
        L_0x0066:
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.account.dao.UserDB.storeLegacyAnalyticsEventIds(java.util.List):void");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002b, code lost:
        if (r0 != null) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0041, code lost:
        if (r0 != null) goto L_0x002d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0049 A[SYNTHETIC, Splitter:B:28:0x0049] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.String fetchLegacyAnalyticsEventId(java.lang.String r11) {
        /*
            r10 = this;
            monitor-enter(r10)
            r0 = 1
            java.lang.String[] r5 = new java.lang.String[r0]     // Catch:{ all -> 0x004d }
            r0 = 0
            r5[r0] = r11     // Catch:{ all -> 0x004d }
            r11 = 0
            com.helpshift.account.dao.UserDBHelper r0 = r10.userDBHelper     // Catch:{ Exception -> 0x0038, all -> 0x0033 }
            android.database.sqlite.SQLiteDatabase r1 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x0038, all -> 0x0033 }
            java.lang.String r2 = "legacy_analytics_event_id_table"
            r3 = 0
            java.lang.String r4 = "identifier = ?"
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r0 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0038, all -> 0x0033 }
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x0031 }
            if (r1 == 0) goto L_0x002b
            java.lang.String r1 = "analytics_event_id"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x0031 }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x0031 }
            r11 = r1
        L_0x002b:
            if (r0 == 0) goto L_0x0044
        L_0x002d:
            r0.close()     // Catch:{ all -> 0x004d }
            goto L_0x0044
        L_0x0031:
            r1 = move-exception
            goto L_0x003a
        L_0x0033:
            r0 = move-exception
            r9 = r0
            r0 = r11
            r11 = r9
            goto L_0x0047
        L_0x0038:
            r1 = move-exception
            r0 = r11
        L_0x003a:
            java.lang.String r2 = "Helpshift_UserDB"
            java.lang.String r3 = "Error in reading legacy analytics eventID with identifier"
            com.helpshift.util.HSLogger.e(r2, r3, r1)     // Catch:{ all -> 0x0046 }
            if (r0 == 0) goto L_0x0044
            goto L_0x002d
        L_0x0044:
            monitor-exit(r10)
            return r11
        L_0x0046:
            r11 = move-exception
        L_0x0047:
            if (r0 == 0) goto L_0x004c
            r0.close()     // Catch:{ all -> 0x004d }
        L_0x004c:
            throw r11     // Catch:{ all -> 0x004d }
        L_0x004d:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.account.dao.UserDB.fetchLegacyAnalyticsEventId(java.lang.String):java.lang.String");
    }

    private UserSyncStatus intToUserSyncStatus(int i) {
        if (i < 0 || i > 3) {
            i = 0;
        }
        return UserSyncStatus.values()[i];
    }

    private MigrationState intToMigrationState(int i) {
        if (i < 0 || i > 3) {
            i = 0;
        }
        return MigrationState.values()[i];
    }

    private ClearedUserSyncState intToClearedUserSyncState(int i) {
        if (i < 0 || i > 3) {
            i = 0;
        }
        return ClearedUserSyncState.values()[i];
    }

    private RedactionState intToRedactionState(int i) {
        RedactionState[] values = RedactionState.values();
        if (i < 0 || i > values.length) {
            i = 0;
        }
        return values[i];
    }

    private RedactionType intToRedactionType(int i) {
        RedactionType[] values = RedactionType.values();
        if (i < 0 || i > values.length) {
            i = 0;
        }
        return values[i];
    }

    private ContentValues redactionDetailToContentValues(RedactionDetail redactionDetail) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("user_local_id", Long.valueOf(redactionDetail.userLocalId));
        contentValues.put("redaction_state", Integer.valueOf(redactionDetail.redactionState.ordinal()));
        contentValues.put("redaction_type", Integer.valueOf(redactionDetail.redactionType.ordinal()));
        return contentValues;
    }

    private RedactionDetail cursorToRedactionDetail(Cursor cursor) {
        return new RedactionDetail(cursor.getLong(cursor.getColumnIndex("user_local_id")), intToRedactionState(cursor.getInt(cursor.getColumnIndex("redaction_state"))), intToRedactionType(cursor.getInt(cursor.getColumnIndex("redaction_type"))));
    }

    /* access modifiers changed from: package-private */
    public synchronized void insertRedactionDetail(RedactionDetail redactionDetail) {
        try {
            this.userDBHelper.getWritableDatabase().insert("redaction_info_table", null, redactionDetailToContentValues(redactionDetail));
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in inserting redaction info of the user", e);
        }
        return;
    }

    /* access modifiers changed from: package-private */
    public synchronized void updateRedactionDetail(RedactionDetail redactionDetail) {
        try {
            this.userDBHelper.getWritableDatabase().update("redaction_info_table", redactionDetailToContentValues(redactionDetail), "user_local_id = ?", new String[]{String.valueOf(redactionDetail.userLocalId)});
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in updating redaction detail", e);
        }
        return;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0029, code lost:
        if (r12 != null) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r12.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003f, code lost:
        if (r12 != null) goto L_0x002b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0047 A[SYNTHETIC, Splitter:B:28:0x0047] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.helpshift.redaction.RedactionDetail fetchRedactionDetail(long r11) {
        /*
            r10 = this;
            monitor-enter(r10)
            r0 = 1
            java.lang.String[] r5 = new java.lang.String[r0]     // Catch:{ all -> 0x004b }
            r0 = 0
            java.lang.String r11 = java.lang.String.valueOf(r11)     // Catch:{ all -> 0x004b }
            r5[r0] = r11     // Catch:{ all -> 0x004b }
            r11 = 0
            com.helpshift.account.dao.UserDBHelper r12 = r10.userDBHelper     // Catch:{ Exception -> 0x0036, all -> 0x0031 }
            android.database.sqlite.SQLiteDatabase r1 = r12.getReadableDatabase()     // Catch:{ Exception -> 0x0036, all -> 0x0031 }
            java.lang.String r2 = "redaction_info_table"
            r3 = 0
            java.lang.String r4 = "user_local_id = ?"
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r12 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0036, all -> 0x0031 }
            boolean r0 = r12.moveToFirst()     // Catch:{ Exception -> 0x002f }
            if (r0 == 0) goto L_0x0029
            com.helpshift.redaction.RedactionDetail r0 = r10.cursorToRedactionDetail(r12)     // Catch:{ Exception -> 0x002f }
            r11 = r0
        L_0x0029:
            if (r12 == 0) goto L_0x0042
        L_0x002b:
            r12.close()     // Catch:{ all -> 0x004b }
            goto L_0x0042
        L_0x002f:
            r0 = move-exception
            goto L_0x0038
        L_0x0031:
            r12 = move-exception
            r9 = r12
            r12 = r11
            r11 = r9
            goto L_0x0045
        L_0x0036:
            r0 = move-exception
            r12 = r11
        L_0x0038:
            java.lang.String r1 = "Helpshift_UserDB"
            java.lang.String r2 = "Error in reading redaction detail of the user"
            com.helpshift.util.HSLogger.e(r1, r2, r0)     // Catch:{ all -> 0x0044 }
            if (r12 == 0) goto L_0x0042
            goto L_0x002b
        L_0x0042:
            monitor-exit(r10)
            return r11
        L_0x0044:
            r11 = move-exception
        L_0x0045:
            if (r12 == 0) goto L_0x004a
            r12.close()     // Catch:{ all -> 0x004b }
        L_0x004a:
            throw r11     // Catch:{ all -> 0x004b }
        L_0x004b:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.account.dao.UserDB.fetchRedactionDetail(long):com.helpshift.redaction.RedactionDetail");
    }

    /* access modifiers changed from: package-private */
    public synchronized void updateRedactionState(long j, RedactionState redactionState) {
        try {
            SQLiteDatabase writableDatabase = this.userDBHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("redaction_state", Integer.valueOf(redactionState.ordinal()));
            writableDatabase.update("redaction_info_table", contentValues, "user_local_id = ?", new String[]{String.valueOf(j)});
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in updating redaction status", e);
        }
        return;
    }

    /* access modifiers changed from: package-private */
    public synchronized void deleteRedactionDetail(long j) {
        try {
            this.userDBHelper.getWritableDatabase().delete("redaction_info_table", "user_local_id = ?", new String[]{String.valueOf(j)});
        } catch (Exception e) {
            HSLogger.e(TAG, "Error in deleting redaction detail", e);
        }
        return;
    }
}
