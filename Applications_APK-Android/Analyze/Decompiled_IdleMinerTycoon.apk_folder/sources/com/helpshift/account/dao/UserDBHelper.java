package com.helpshift.account.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.helpshift.logger.logmodels.ILogExtrasModel;
import com.helpshift.util.HSLogger;
import java.util.List;

public class UserDBHelper extends SQLiteOpenHelper {
    private static final String TAG = "Helpshift_UserDBDB";
    private UserDBInfo dbInfo;

    UserDBHelper(Context context, UserDBInfo userDBInfo) {
        super(context, "__hs_db_helpshift_users", (SQLiteDatabase.CursorFactory) null, UserDBInfo.DATABASE_VERSION.intValue());
        this.dbInfo = userDBInfo;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        if (sQLiteDatabase.isOpen()) {
            List<String> queriesForOnCreate = this.dbInfo.getQueriesForOnCreate();
            try {
                sQLiteDatabase.beginTransaction();
                for (String execSQL : queriesForOnCreate) {
                    sQLiteDatabase.execSQL(execSQL);
                }
                sQLiteDatabase.setTransactionSuccessful();
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e) {
                    HSLogger.f(TAG, "Error in onCreate inside finally block, ", e, new ILogExtrasModel[0]);
                }
            } catch (Exception e2) {
                HSLogger.f(TAG, "Exception while creating userDB, version: " + UserDBInfo.DATABASE_VERSION, e2, new ILogExtrasModel[0]);
                if (sQLiteDatabase.inTransaction()) {
                    sQLiteDatabase.endTransaction();
                }
            } catch (Throwable th) {
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e3) {
                    HSLogger.f(TAG, "Error in onCreate inside finally block, ", e3, new ILogExtrasModel[0]);
                }
                throw th;
            }
        }
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (sQLiteDatabase.isOpen()) {
            List<String> queriesForOnUpgrade = this.dbInfo.getQueriesForOnUpgrade(i);
            try {
                sQLiteDatabase.beginTransaction();
                for (String execSQL : queriesForOnUpgrade) {
                    sQLiteDatabase.execSQL(execSQL);
                }
                sQLiteDatabase.setTransactionSuccessful();
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e) {
                    HSLogger.f(TAG, "Exception while migrating userDB inside finally block, ", e, new ILogExtrasModel[0]);
                }
            } catch (Exception e2) {
                HSLogger.f(TAG, "Exception while migrating userDB, version: " + UserDBInfo.DATABASE_VERSION, e2, new ILogExtrasModel[0]);
                if (sQLiteDatabase.inTransaction()) {
                    sQLiteDatabase.endTransaction();
                }
            } catch (Throwable th) {
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e3) {
                    HSLogger.f(TAG, "Exception while migrating userDB inside finally block, ", e3, new ILogExtrasModel[0]);
                }
                throw th;
            }
        }
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (sQLiteDatabase.isOpen()) {
            List<String> queriesForDropAndCreate = this.dbInfo.getQueriesForDropAndCreate();
            try {
                sQLiteDatabase.beginTransaction();
                for (String execSQL : queriesForDropAndCreate) {
                    sQLiteDatabase.execSQL(execSQL);
                }
                sQLiteDatabase.setTransactionSuccessful();
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e) {
                    HSLogger.f(TAG, "Exception while downgrading userDB inside finally block, ", e, new ILogExtrasModel[0]);
                }
            } catch (Exception e2) {
                HSLogger.f(TAG, "Exception while downgrading userDB, version: " + i2, e2, new ILogExtrasModel[0]);
                if (sQLiteDatabase.inTransaction()) {
                    sQLiteDatabase.endTransaction();
                }
            } catch (Throwable th) {
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e3) {
                    HSLogger.f(TAG, "Exception while downgrading userDB inside finally block, ", e3, new ILogExtrasModel[0]);
                }
                throw th;
            }
        }
    }
}
