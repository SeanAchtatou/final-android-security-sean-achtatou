package com.helpshift.account.domainmodel;

import com.helpshift.account.domainmodel.UserSyncDM;
import com.helpshift.common.AutoRetriableDM;
import com.helpshift.common.AutoRetryFailedEventDM;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.F;
import com.helpshift.common.platform.Platform;
import com.helpshift.conversation.ConversationInboxPoller;
import com.helpshift.migration.MigrationState;
import com.helpshift.migration.RemoteDataMigrator;
import com.helpshift.redaction.RedactionManager;
import com.helpshift.redaction.RedactionState;
import java.lang.ref.WeakReference;

public class UserSetupDM implements UserSyncDM.UserSyncListener, RemoteDataMigrator.RemoteDataMigratorListener, RedactionManager.RedactionManagerListener, AutoRetriableDM {
    Domain domain;
    private RedactionManager redactionManager;
    private RemoteDataMigrator remoteDataMigrator;
    UserDM userDM;
    private WeakReference<UserSetupListener> userSetupListener;
    private UserSyncDM userSyncDM;

    public interface UserSetupListener {
        void userSetupStateChanged(UserDM userDM, UserSetupState userSetupState);
    }

    public UserSetupDM(Platform platform, Domain domain2, UserDM userDM2, UserManagerDM userManagerDM, IUserSyncExecutor iUserSyncExecutor) {
        this.domain = domain2;
        this.userDM = userDM2;
        this.userSyncDM = new UserSyncDM(platform, domain2, userDM2, userManagerDM, iUserSyncExecutor, this);
        this.remoteDataMigrator = new RemoteDataMigrator(platform, domain2, userDM2, this);
        this.redactionManager = new RedactionManager(platform, domain2, userDM2, this);
    }

    public void init() {
        this.redactionManager.setAppropriateInitialState();
        this.remoteDataMigrator.setAppropriateInitialState();
        this.userSyncDM.setAppropriateInitialState();
        this.domain.getAutoRetryFailedEventDM().register(AutoRetryFailedEventDM.EventType.MIGRATION, this);
        this.domain.getAutoRetryFailedEventDM().register(AutoRetryFailedEventDM.EventType.SYNC_USER, this);
    }

    public UserSetupState getState() {
        RedactionState redactionState = this.redactionManager.getRedactionState();
        if (redactionState == RedactionState.PENDING) {
            return UserSetupState.NON_STARTED;
        }
        if (redactionState == RedactionState.IN_PROGRESS) {
            return UserSetupState.IN_PROGRESS;
        }
        MigrationState profileMigrationState = this.remoteDataMigrator.getProfileMigrationState();
        if (profileMigrationState == MigrationState.NOT_STARTED) {
            return UserSetupState.NON_STARTED;
        }
        if (profileMigrationState == MigrationState.FAILED) {
            return UserSetupState.FAILED;
        }
        if (profileMigrationState == MigrationState.IN_PROGRESS) {
            return UserSetupState.IN_PROGRESS;
        }
        UserSyncStatus syncState = this.userSyncDM.getSyncState();
        if (syncState == UserSyncStatus.NOT_STARTED) {
            return UserSetupState.NON_STARTED;
        }
        if (syncState == UserSyncStatus.FAILED) {
            return UserSetupState.FAILED;
        }
        if (syncState == UserSyncStatus.IN_PROGRESS) {
            return UserSetupState.IN_PROGRESS;
        }
        return UserSetupState.COMPLETED;
    }

    public void startSetup() {
        UserSetupState state = getState();
        if (state != UserSetupState.IN_PROGRESS && state != UserSetupState.COMPLETED) {
            RedactionState redactionState = this.redactionManager.getRedactionState();
            onRedactionStateChange(redactionState);
            if (redactionState == RedactionState.PENDING) {
                this.redactionManager.executeRedaction();
            }
        }
    }

    public void redactionStateChanged(UserDM userDM2, RedactionState redactionState, RedactionState redactionState2) {
        onRedactionStateChange(redactionState2);
    }

    public void userSyncStateChanged(UserDM userDM2, UserSyncStatus userSyncStatus, UserSyncStatus userSyncStatus2) {
        onUserSyncStateChange(userSyncStatus2);
    }

    public void onMigrationStateChanged(UserDM userDM2, MigrationState migrationState, MigrationState migrationState2) {
        onMigrationStateChange(migrationState2);
    }

    private void onRedactionStateChange(RedactionState redactionState) {
        if (redactionState == RedactionState.COMPLETED) {
            MigrationState profileMigrationState = this.remoteDataMigrator.getProfileMigrationState();
            if (profileMigrationState == MigrationState.COMPLETED || profileMigrationState == MigrationState.IN_PROGRESS) {
                onMigrationStateChange(profileMigrationState);
            } else {
                this.remoteDataMigrator.startProfileMigration();
            }
        } else if (redactionState == RedactionState.IN_PROGRESS) {
            updateUserSetupStateChange(UserSetupState.IN_PROGRESS);
        } else if (redactionState == RedactionState.PENDING) {
            updateUserSetupStateChange(UserSetupState.NON_STARTED);
        }
    }

    private void onMigrationStateChange(MigrationState migrationState) {
        if (migrationState == MigrationState.COMPLETED) {
            UserSyncStatus syncState = this.userSyncDM.getSyncState();
            if (syncState == UserSyncStatus.COMPLETED || syncState == UserSyncStatus.IN_PROGRESS) {
                onUserSyncStateChange(syncState);
            } else {
                this.userSyncDM.syncUser();
            }
        } else if (migrationState == MigrationState.IN_PROGRESS) {
            updateUserSetupStateChange(UserSetupState.IN_PROGRESS);
        } else if (migrationState == MigrationState.FAILED) {
            updateUserSetupStateChange(UserSetupState.FAILED);
        } else if (migrationState == MigrationState.NOT_STARTED) {
            updateUserSetupStateChange(UserSetupState.NON_STARTED);
        }
    }

    private void onUserSyncStateChange(UserSyncStatus userSyncStatus) {
        if (userSyncStatus == UserSyncStatus.COMPLETED) {
            updateUserSetupStateChange(UserSetupState.COMPLETED);
        } else if (userSyncStatus == UserSyncStatus.IN_PROGRESS) {
            updateUserSetupStateChange(UserSetupState.IN_PROGRESS);
        } else if (userSyncStatus == UserSyncStatus.FAILED) {
            updateUserSetupStateChange(UserSetupState.FAILED);
        } else if (userSyncStatus == UserSyncStatus.NOT_STARTED) {
            updateUserSetupStateChange(UserSetupState.NON_STARTED);
        }
    }

    private void updateUserSetupStateChange(final UserSetupState userSetupState) {
        final UserSetupListener userSetupListener2 = this.userSetupListener == null ? null : this.userSetupListener.get();
        if (userSetupListener2 != null) {
            this.domain.runSerial(new F() {
                public void f() {
                    userSetupListener2.userSetupStateChanged(UserSetupDM.this.userDM, userSetupState);
                }
            });
        }
        if (userSetupState == UserSetupState.COMPLETED) {
            this.domain.runParallel(new F() {
                public void f() {
                    try {
                        UserSetupDM.this.domain.getUserManagerDM().sendPushTokenSync();
                    } finally {
                        ConversationInboxPoller conversationInboxPoller = UserSetupDM.this.domain.getConversationInboxManagerDM().getActiveConversationInboxDM().getConversationInboxPoller();
                        UserSetupDM.this.userDM.addObserver(conversationInboxPoller);
                        conversationInboxPoller.refreshPoller(false);
                    }
                }
            });
        }
    }

    public void registerUserSetupListener(UserSetupListener userSetupListener2) {
        if (userSetupListener2 == null) {
            this.userSetupListener = null;
        } else {
            this.userSetupListener = new WeakReference<>(userSetupListener2);
        }
    }

    public void sendFailedApiCalls(AutoRetryFailedEventDM.EventType eventType) {
        if (this.redactionManager.getRedactionState() == RedactionState.COMPLETED) {
            switch (eventType) {
                case MIGRATION:
                    this.remoteDataMigrator.retry();
                    if (this.remoteDataMigrator.getProfileMigrationState() == MigrationState.COMPLETED) {
                        this.userSyncDM.syncUser();
                        return;
                    }
                    return;
                case SYNC_USER:
                    if (this.remoteDataMigrator.getProfileMigrationState() == MigrationState.COMPLETED) {
                        this.userSyncDM.retry();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }
}
