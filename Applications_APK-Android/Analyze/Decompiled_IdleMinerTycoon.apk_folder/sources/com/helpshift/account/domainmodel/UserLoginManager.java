package com.helpshift.account.domainmodel;

import com.helpshift.CoreApi;
import com.helpshift.HelpshiftUser;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.network.NetworkConstants;
import com.helpshift.common.platform.Platform;
import com.helpshift.conversation.domainmodel.ConversationController;
import com.helpshift.util.HSLogger;

public class UserLoginManager {
    private static final String TAG = "Helpshift_ULoginM";
    private CoreApi coreApi;
    private Domain domain;
    private Platform platform;

    public UserLoginManager(CoreApi coreApi2, Domain domain2, Platform platform2) {
        this.coreApi = coreApi2;
        this.domain = domain2;
        this.platform = platform2;
    }

    public boolean login(HelpshiftUser helpshiftUser) {
        UserManagerDM userManagerDM = this.coreApi.getUserManagerDM();
        boolean z = false;
        if (userManagerDM.isActiveUser(helpshiftUser)) {
            UserDM activeUser = userManagerDM.getActiveUser();
            String authToken = activeUser.getAuthToken();
            if (!(authToken == null && helpshiftUser.getAuthToken() == null)) {
                if (authToken == null || !authToken.equals(helpshiftUser.getAuthToken())) {
                    userManagerDM.updateAuthToken(activeUser, helpshiftUser.getAuthToken());
                }
                z = true;
            }
            String name = activeUser.getName();
            if ((!StringUtils.isEmpty(name) || !StringUtils.isEmpty(helpshiftUser.getName())) && (StringUtils.isEmpty(name) || !name.equals(helpshiftUser.getName()))) {
                userManagerDM.updateUserName(activeUser, helpshiftUser.getName());
            }
        } else if (this.coreApi.isSDKSessionActive()) {
            HSLogger.d(TAG, "Login should be called before starting a Helpshift session");
            return false;
        } else {
            cleanUpActiveUser();
            userManagerDM.login(helpshiftUser);
            for (UserDM deleteUser : userManagerDM.getInactiveLoggedInUsers()) {
                deleteUser(deleteUser);
            }
            setUpActiveUser();
            z = true;
        }
        clearConfigRouteETag();
        if (z) {
            this.domain.getAutoRetryFailedEventDM().onUserAuthenticationUpdated();
        }
        return true;
    }

    private void cleanUpActiveUser() {
        ConversationController activeConversationInboxDM = this.domain.getConversationInboxManagerDM().getActiveConversationInboxDM();
        activeConversationInboxDM.clearPushNotifications();
        activeConversationInboxDM.getConversationInboxPoller().stop();
    }

    private void setUpActiveUser() {
        ConversationController activeConversationInboxDM = this.domain.getConversationInboxManagerDM().getActiveConversationInboxDM();
        activeConversationInboxDM.showPushNotifications();
        UserSetupDM activeUserSetupDM = this.coreApi.getUserManagerDM().getActiveUserSetupDM();
        if (UserSetupState.COMPLETED == activeUserSetupDM.getState()) {
            activeConversationInboxDM.getConversationInboxPoller().startAppPoller(false);
        } else {
            activeUserSetupDM.startSetup();
        }
    }

    private boolean deleteUser(UserDM userDM) {
        boolean deleteUser = this.coreApi.getUserManagerDM().deleteUser(userDM);
        if (deleteUser) {
            this.platform.getRedactionDAO().deleteRedactionDetail(userDM.getLocalId().longValue());
            this.domain.getConversationInboxManagerDM().deleteConversations(userDM);
        }
        return deleteUser;
    }

    public boolean clearAnonymousUser() {
        UserManagerDM userManagerDM = this.coreApi.getUserManagerDM();
        UserDM anonymousUser = this.coreApi.getUserManagerDM().getAnonymousUser();
        if (anonymousUser == null) {
            return true;
        }
        if (anonymousUser.isActiveUser()) {
            HSLogger.d(TAG, "clearAnonymousUser should be called when a logged-in user is active");
            return false;
        } else if (!deleteUser(anonymousUser)) {
            return true;
        } else {
            userManagerDM.clearAnonymousUser(anonymousUser);
            return true;
        }
    }

    public boolean logout() {
        if (this.coreApi.isSDKSessionActive()) {
            HSLogger.d(TAG, "Logout should be called before starting a Helpshift session");
            return false;
        }
        UserManagerDM userManagerDM = this.coreApi.getUserManagerDM();
        UserDM activeUser = userManagerDM.getActiveUser();
        if (activeUser != null && activeUser.isAnonymousUser()) {
            return true;
        }
        cleanUpActiveUser();
        boolean loginWithAnonymousUser = userManagerDM.loginWithAnonymousUser();
        setUpActiveUser();
        if (loginWithAnonymousUser) {
            clearConfigRouteETag();
            this.domain.getAutoRetryFailedEventDM().onUserAuthenticationUpdated();
        }
        return loginWithAnonymousUser;
    }

    public void clearPersonallyIdentifiableInformation() {
        if (this.coreApi.isSDKSessionActive()) {
            HSLogger.d(TAG, "clear PII should not be called after starting a Helpshift session");
            return;
        }
        UserManagerDM userManagerDM = this.coreApi.getUserManagerDM();
        UserDM activeUser = userManagerDM.getActiveUser();
        if (!StringUtils.isEmpty(activeUser.getIdentifier())) {
            userManagerDM.resetNameAndEmail(activeUser);
            this.coreApi.getConversationController().saveName(null);
            this.coreApi.getConversationController().saveEmail(null);
        } else if (logout()) {
            deleteUser(activeUser);
            this.platform.getCampaignModuleAPIs().logout();
        }
    }

    private void clearConfigRouteETag() {
        this.platform.getNetworkRequestDAO().removeETag(NetworkConstants.SUPPORT_CONFIG_ROUTE);
    }
}
