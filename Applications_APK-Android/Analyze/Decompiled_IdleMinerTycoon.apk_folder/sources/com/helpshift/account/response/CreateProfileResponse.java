package com.helpshift.account.response;

public class CreateProfileResponse {
    public final String id;
    public final String name;

    public CreateProfileResponse(String str, String str2) {
        this.id = str;
        this.name = str2;
    }
}
