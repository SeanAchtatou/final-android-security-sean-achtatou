package com.helpshift.platform.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.helpshift.common.conversation.ConversationDBInfo;
import com.helpshift.common.conversation.migration.ConversationMigrationFactory;
import com.helpshift.logger.logmodels.ILogExtrasModel;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftContext;
import java.util.List;

public class ConversationDBHelper extends SQLiteOpenHelper {
    private static final String TAG = "Helpshift_ConversationDB";
    private final ConversationDBInfo dbInfo;

    public ConversationDBHelper(Context context, ConversationDBInfo conversationDBInfo) {
        super(context, ConversationDBInfo.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, ConversationDBInfo.DATABASE_VERSION.intValue());
        this.dbInfo = conversationDBInfo;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        if (sQLiteDatabase.isOpen()) {
            try {
                List<String> queriesForOnCreate = this.dbInfo.getQueriesForOnCreate();
                sQLiteDatabase.beginTransaction();
                for (String execSQL : queriesForOnCreate) {
                    sQLiteDatabase.execSQL(execSQL);
                }
                sQLiteDatabase.setTransactionSuccessful();
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e) {
                    HSLogger.f(TAG, "Error in onCreate inside finally block, ", e, new ILogExtrasModel[0]);
                }
            } catch (Exception e2) {
                HSLogger.f(TAG, "Exception while creating tables: version: " + ConversationDBInfo.DATABASE_VERSION, e2, new ILogExtrasModel[0]);
                if (sQLiteDatabase.inTransaction()) {
                    sQLiteDatabase.endTransaction();
                }
            } catch (Throwable th) {
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e3) {
                    HSLogger.f(TAG, "Error in onCreate inside finally block, ", e3, new ILogExtrasModel[0]);
                }
                throw th;
            }
        }
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (i < this.dbInfo.getMinimumDBVersionSupportedForMigration()) {
            dropAndCreateDatabase(sQLiteDatabase);
        } else if (sQLiteDatabase.isOpen()) {
            boolean z = false;
            try {
                sQLiteDatabase.beginTransaction();
                for (int i3 = i; i3 < i2; i3++) {
                    ConversationMigrationFactory.getMigrationForDbVersion(i3, sQLiteDatabase).migrate();
                }
                sQLiteDatabase.setTransactionSuccessful();
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e) {
                    HSLogger.f(TAG, "Exception while migrating conversationDB inside finally block, ", e, new ILogExtrasModel[0]);
                }
            } catch (Exception e2) {
                HSLogger.f(TAG, "Exception while migrating conversationDB, old: " + i + ", new: " + i2, e2, new ILogExtrasModel[0]);
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e3) {
                    HSLogger.f(TAG, "Exception while migrating conversationDB inside finally block, ", e3, new ILogExtrasModel[0]);
                }
                z = true;
            } catch (Throwable th) {
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e4) {
                    HSLogger.f(TAG, "Exception while migrating conversationDB inside finally block, ", e4, new ILogExtrasModel[0]);
                }
                throw th;
            }
            if (z) {
                dropAndCreateDatabase(sQLiteDatabase);
                HelpshiftContext.getCoreApi().resetUsersSyncStatusAndStartSetupForActiveUser();
            }
        }
    }

    public void dropAndCreateDatabase(SQLiteDatabase sQLiteDatabase) {
        if (sQLiteDatabase.isOpen()) {
            try {
                List<String> queriesForDropAndCreate = this.dbInfo.getQueriesForDropAndCreate();
                sQLiteDatabase.beginTransaction();
                for (String execSQL : queriesForDropAndCreate) {
                    sQLiteDatabase.execSQL(execSQL);
                }
                sQLiteDatabase.setTransactionSuccessful();
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e) {
                    HSLogger.f(TAG, "Error in dropAndCreateDatabase inside finally block, ", e, new ILogExtrasModel[0]);
                }
            } catch (Exception e2) {
                HSLogger.f(TAG, "Exception while upgrading tables, version: " + ConversationDBInfo.DATABASE_VERSION, e2, new ILogExtrasModel[0]);
                if (sQLiteDatabase.inTransaction()) {
                    sQLiteDatabase.endTransaction();
                }
            } catch (Throwable th) {
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e3) {
                    HSLogger.f(TAG, "Error in dropAndCreateDatabase inside finally block, ", e3, new ILogExtrasModel[0]);
                }
                throw th;
            }
        }
    }
}
