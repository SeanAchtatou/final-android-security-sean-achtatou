package com.helpshift;

import com.helpshift.account.domainmodel.UserManagerDM;
import com.helpshift.analytics.domainmodel.AnalyticsEventDM;
import com.helpshift.cif.CustomIssueFieldDM;
import com.helpshift.common.AutoRetryFailedEventDM;
import com.helpshift.common.FetchDataFromThread;
import com.helpshift.common.domain.AttachmentFileManagerDM;
import com.helpshift.common.domain.Domain;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.conversation.ConversationInboxPoller;
import com.helpshift.conversation.activeconversation.ConversationInfoRenderer;
import com.helpshift.conversation.activeconversation.ConversationRenderer;
import com.helpshift.conversation.activeconversation.ConversationalRenderer;
import com.helpshift.conversation.activeconversation.ScreenshotPreviewRenderer;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.activeconversation.usersetup.UserSetupRenderer;
import com.helpshift.conversation.domainmodel.ConversationController;
import com.helpshift.conversation.usersetup.UserSetupVM;
import com.helpshift.conversation.viewmodel.ConversationInfoVM;
import com.helpshift.conversation.viewmodel.ConversationVM;
import com.helpshift.conversation.viewmodel.ConversationalVM;
import com.helpshift.conversation.viewmodel.NewConversationRenderer;
import com.helpshift.conversation.viewmodel.NewConversationVM;
import com.helpshift.conversation.viewmodel.ScreenshotPreviewVM;
import com.helpshift.crypto.CryptoDM;
import com.helpshift.delegate.RootDelegate;
import com.helpshift.delegate.UIThreadDelegateDecorator;
import com.helpshift.faq.FaqsDM;
import com.helpshift.localeprovider.domainmodel.LocaleProviderDM;
import com.helpshift.logger.ErrorReportsDM;
import com.helpshift.meta.MetaDataDM;
import com.helpshift.util.ValuePair;
import java.util.Map;

public interface CoreApi {
    boolean clearAnonymousUser();

    void fetchServerConfig();

    Conversation getActiveConversation();

    Conversation getActiveConversationOrPreIssue();

    AnalyticsEventDM getAnalyticsEventDM();

    AttachmentFileManagerDM getAttachmentFileManagerDM();

    AutoRetryFailedEventDM getAutoRetryFailedEventDM();

    ConversationController getConversationController();

    ConversationInboxPoller getConversationInboxPoller();

    ConversationInfoVM getConversationInfoViewModel(ConversationInfoRenderer conversationInfoRenderer);

    ConversationVM getConversationViewModel(Long l, ConversationRenderer conversationRenderer, boolean z);

    ConversationalVM getConversationalViewModel(boolean z, Long l, ConversationalRenderer conversationalRenderer, boolean z2);

    CryptoDM getCryptoDM();

    CustomIssueFieldDM getCustomIssueFieldDM();

    UIThreadDelegateDecorator getDelegate();

    Domain getDomain();

    ErrorReportsDM getErrorReportsDM();

    FaqsDM getFaqDM();

    LocaleProviderDM getLocaleProviderDM();

    MetaDataDM getMetaDataDM();

    NewConversationVM getNewConversationViewModel(NewConversationRenderer newConversationRenderer);

    void getNotificationCountAsync(FetchDataFromThread<ValuePair<Integer, Boolean>, Object> fetchDataFromThread);

    int getNotificationCountSync();

    SDKConfigurationDM getSDKConfigurationDM();

    ScreenshotPreviewVM getScreenshotPreviewModel(ScreenshotPreviewRenderer screenshotPreviewRenderer);

    UserManagerDM getUserManagerDM();

    UserSetupVM getUserSetupVM(UserSetupRenderer userSetupRenderer);

    void handlePushNotification(String str, String str2, String str3);

    boolean isActiveConversationActionable();

    boolean isSDKSessionActive();

    boolean login(HelpshiftUser helpshiftUser);

    boolean logout();

    void onSDKSessionEnded();

    void onSDKSessionStarted();

    void refreshPoller();

    void resetPreIssueConversations();

    void resetUsersSyncStatusAndStartSetupForActiveUser();

    void sendAnalyticsEvent();

    void sendAppStartEvent();

    void sendFailedApiCalls();

    void sendRequestIdsForSuccessfulApiCalls();

    void setDelegateListener(RootDelegate rootDelegate);

    void setNameAndEmail(String str, String str2);

    void setPushToken(String str);

    void updateApiConfig(Map<String, Object> map);

    void updateInstallConfig(Map<String, Object> map);
}
