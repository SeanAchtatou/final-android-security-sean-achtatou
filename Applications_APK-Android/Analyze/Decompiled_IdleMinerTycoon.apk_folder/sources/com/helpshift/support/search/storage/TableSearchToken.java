package com.helpshift.support.search.storage;

import android.provider.BaseColumns;

public interface TableSearchToken extends BaseColumns {
    public static final String COLUMN_SCORE = "score";
    public static final String COLUMN_TOKEN = "token";
    public static final String COLUMN_TYPE = "type";
    public static final String COMMA_SEP = ", ";
    public static final String INT_TYPE = " INTEGER ";
    public static final String NOT_NULL = " NOT NULL ";
    public static final String PRIMARY_KEY = " PRIMARY KEY ";
    public static final String SQL_CREATE_TABLE = "CREATE TABLE search_token_table (token TEXT  PRIMARY KEY , type INTEGER , score TEXT  NOT NULL  )";
    public static final String TABLE_NAME = "search_token_table";
    public static final String TEXT_TYPE = " TEXT ";
}
