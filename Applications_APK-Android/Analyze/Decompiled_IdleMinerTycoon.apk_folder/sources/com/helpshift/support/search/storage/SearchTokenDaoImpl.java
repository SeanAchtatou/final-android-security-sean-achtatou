package com.helpshift.support.search.storage;

import android.database.sqlite.SQLiteOpenHelper;
import com.helpshift.support.search.SearchTokenDao;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftContext;
import java.util.HashMap;
import java.util.Map;

public class SearchTokenDaoImpl implements SearchTokenDao {
    private static final String TAG = "Helpshift_SearchToknDao";
    private final SQLiteOpenHelper dbHelper = new SearchDBHelper(HelpshiftContext.getApplicationContext());
    private final char scoreMapKeyValueStringSeparator = ':';
    private final char scoreMapStringSeparator = '$';

    SearchTokenDaoImpl() {
    }

    public static SearchTokenDao getInstance() {
        return LazyHolder.INSTANCE;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x008b A[SYNTHETIC, Splitter:B:36:0x008b] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x009f A[SYNTHETIC, Splitter:B:46:0x009f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void save(java.util.List<com.helpshift.support.search.SearchTokenDto> r7) {
        /*
            r6 = this;
            if (r7 != 0) goto L_0x0003
            return
        L_0x0003:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.Iterator r7 = r7.iterator()
        L_0x000c:
            boolean r1 = r7.hasNext()
            if (r1 == 0) goto L_0x003e
            java.lang.Object r1 = r7.next()
            com.helpshift.support.search.SearchTokenDto r1 = (com.helpshift.support.search.SearchTokenDto) r1
            java.util.Map<java.lang.Integer, java.lang.Double> r2 = r1.scoreMap
            java.lang.String r2 = r6.convertScoreMapToScoreString(r2)
            android.content.ContentValues r3 = new android.content.ContentValues
            r3.<init>()
            java.lang.String r4 = "token"
            java.lang.String r5 = r1.wordValue
            r3.put(r4, r5)
            java.lang.String r4 = "type"
            int r1 = r1.wordType
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r3.put(r4, r1)
            java.lang.String r1 = "score"
            r3.put(r1, r2)
            r0.add(r3)
            goto L_0x000c
        L_0x003e:
            android.database.sqlite.SQLiteOpenHelper r7 = r6.dbHelper
            monitor-enter(r7)
            r1 = 0
            android.database.sqlite.SQLiteOpenHelper r2 = r6.dbHelper     // Catch:{ Exception -> 0x0081 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Exception -> 0x0081 }
            r2.beginTransaction()     // Catch:{ Exception -> 0x007b, all -> 0x0079 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x007b, all -> 0x0079 }
        L_0x004f:
            boolean r3 = r0.hasNext()     // Catch:{ Exception -> 0x007b, all -> 0x0079 }
            if (r3 == 0) goto L_0x0061
            java.lang.Object r3 = r0.next()     // Catch:{ Exception -> 0x007b, all -> 0x0079 }
            android.content.ContentValues r3 = (android.content.ContentValues) r3     // Catch:{ Exception -> 0x007b, all -> 0x0079 }
            java.lang.String r4 = "search_token_table"
            r2.insert(r4, r1, r3)     // Catch:{ Exception -> 0x007b, all -> 0x0079 }
            goto L_0x004f
        L_0x0061:
            r2.setTransactionSuccessful()     // Catch:{ Exception -> 0x007b, all -> 0x0079 }
            if (r2 == 0) goto L_0x009b
            boolean r0 = r2.inTransaction()     // Catch:{ Exception -> 0x0070 }
            if (r0 == 0) goto L_0x009b
            r2.endTransaction()     // Catch:{ Exception -> 0x0070 }
            goto L_0x009b
        L_0x0070:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_SearchToknDao"
            java.lang.String r2 = "Error occurred when calling save method inside finally block"
        L_0x0075:
            com.helpshift.util.HSLogger.e(r1, r2, r0)     // Catch:{ all -> 0x00a9 }
            goto L_0x009b
        L_0x0079:
            r0 = move-exception
            goto L_0x009d
        L_0x007b:
            r0 = move-exception
            r1 = r2
            goto L_0x0082
        L_0x007e:
            r0 = move-exception
            r2 = r1
            goto L_0x009d
        L_0x0081:
            r0 = move-exception
        L_0x0082:
            java.lang.String r2 = "Helpshift_SearchToknDao"
            java.lang.String r3 = "Error occurred when calling save method"
            com.helpshift.util.HSLogger.e(r2, r3, r0)     // Catch:{ all -> 0x007e }
            if (r1 == 0) goto L_0x009b
            boolean r0 = r1.inTransaction()     // Catch:{ Exception -> 0x0095 }
            if (r0 == 0) goto L_0x009b
            r1.endTransaction()     // Catch:{ Exception -> 0x0095 }
            goto L_0x009b
        L_0x0095:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_SearchToknDao"
            java.lang.String r2 = "Error occurred when calling save method inside finally block"
            goto L_0x0075
        L_0x009b:
            monitor-exit(r7)     // Catch:{ all -> 0x00a9 }
            return
        L_0x009d:
            if (r2 == 0) goto L_0x00b3
            boolean r1 = r2.inTransaction()     // Catch:{ Exception -> 0x00ab }
            if (r1 == 0) goto L_0x00b3
            r2.endTransaction()     // Catch:{ Exception -> 0x00ab }
            goto L_0x00b3
        L_0x00a9:
            r0 = move-exception
            goto L_0x00b4
        L_0x00ab:
            r1 = move-exception
            java.lang.String r2 = "Helpshift_SearchToknDao"
            java.lang.String r3 = "Error occurred when calling save method inside finally block"
            com.helpshift.util.HSLogger.e(r2, r3, r1)     // Catch:{ all -> 0x00a9 }
        L_0x00b3:
            throw r0     // Catch:{ all -> 0x00a9 }
        L_0x00b4:
            monitor-exit(r7)     // Catch:{ all -> 0x00a9 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.search.storage.SearchTokenDaoImpl.save(java.util.List):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0060, code lost:
        if (r13 != null) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r13.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0076, code lost:
        if (r13 != null) goto L_0x0062;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x007e A[Catch:{ all -> 0x007b, all -> 0x0082 }] */
    @android.support.annotation.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.helpshift.support.search.SearchTokenDto get(java.lang.String r13) {
        /*
            r12 = this;
            android.database.sqlite.SQLiteOpenHelper r0 = r12.dbHelper
            monitor-enter(r0)
            r1 = 0
            android.database.sqlite.SQLiteOpenHelper r2 = r12.dbHelper     // Catch:{ Exception -> 0x006d, all -> 0x0068 }
            android.database.sqlite.SQLiteDatabase r3 = r2.getWritableDatabase()     // Catch:{ Exception -> 0x006d, all -> 0x0068 }
            r2 = 3
            java.lang.String[] r5 = new java.lang.String[r2]     // Catch:{ Exception -> 0x006d, all -> 0x0068 }
            java.lang.String r2 = "token"
            r4 = 0
            r5[r4] = r2     // Catch:{ Exception -> 0x006d, all -> 0x0068 }
            java.lang.String r2 = "type"
            r6 = 1
            r5[r6] = r2     // Catch:{ Exception -> 0x006d, all -> 0x0068 }
            r2 = 2
            java.lang.String r7 = "score"
            r5[r2] = r7     // Catch:{ Exception -> 0x006d, all -> 0x0068 }
            java.lang.String r2 = "search_token_table"
            java.lang.String r7 = "token=?"
            java.lang.String[] r8 = new java.lang.String[r6]     // Catch:{ Exception -> 0x006d, all -> 0x0068 }
            r8[r4] = r13     // Catch:{ Exception -> 0x006d, all -> 0x0068 }
            r13 = 0
            r9 = 0
            r10 = 0
            r4 = r2
            r6 = r7
            r7 = r8
            r8 = r13
            android.database.Cursor r13 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x006d, all -> 0x0068 }
            int r2 = r13.getCount()     // Catch:{ Exception -> 0x0066 }
            if (r2 <= 0) goto L_0x0060
            r13.moveToFirst()     // Catch:{ Exception -> 0x0066 }
            java.lang.String r2 = "token"
            int r2 = r13.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0066 }
            java.lang.String r2 = r13.getString(r2)     // Catch:{ Exception -> 0x0066 }
            java.lang.String r3 = "type"
            int r3 = r13.getColumnIndexOrThrow(r3)     // Catch:{ Exception -> 0x0066 }
            int r3 = r13.getInt(r3)     // Catch:{ Exception -> 0x0066 }
            java.lang.String r4 = "score"
            int r4 = r13.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0066 }
            java.lang.String r4 = r13.getString(r4)     // Catch:{ Exception -> 0x0066 }
            java.util.Map r4 = r12.convertScoreStringToScoreMap(r4)     // Catch:{ Exception -> 0x0066 }
            com.helpshift.support.search.SearchTokenDto r5 = new com.helpshift.support.search.SearchTokenDto     // Catch:{ Exception -> 0x0066 }
            r5.<init>(r2, r3, r4)     // Catch:{ Exception -> 0x0066 }
            r1 = r5
        L_0x0060:
            if (r13 == 0) goto L_0x0079
        L_0x0062:
            r13.close()     // Catch:{ all -> 0x0082 }
            goto L_0x0079
        L_0x0066:
            r2 = move-exception
            goto L_0x006f
        L_0x0068:
            r13 = move-exception
            r11 = r1
            r1 = r13
            r13 = r11
            goto L_0x007c
        L_0x006d:
            r2 = move-exception
            r13 = r1
        L_0x006f:
            java.lang.String r3 = "Helpshift_SearchToknDao"
            java.lang.String r4 = "Error occurred when calling get method"
            com.helpshift.util.HSLogger.e(r3, r4, r2)     // Catch:{ all -> 0x007b }
            if (r13 == 0) goto L_0x0079
            goto L_0x0062
        L_0x0079:
            monitor-exit(r0)     // Catch:{ all -> 0x0082 }
            return r1
        L_0x007b:
            r1 = move-exception
        L_0x007c:
            if (r13 == 0) goto L_0x0084
            r13.close()     // Catch:{ all -> 0x0082 }
            goto L_0x0084
        L_0x0082:
            r13 = move-exception
            goto L_0x0085
        L_0x0084:
            throw r1     // Catch:{ all -> 0x0082 }
        L_0x0085:
            monitor-exit(r0)     // Catch:{ all -> 0x0082 }
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.search.storage.SearchTokenDaoImpl.get(java.lang.String):com.helpshift.support.search.SearchTokenDto");
    }

    public void clear() {
        synchronized (this.dbHelper) {
            try {
                this.dbHelper.getWritableDatabase().delete(TableSearchToken.TABLE_NAME, null, null);
            } catch (Exception e) {
                HSLogger.e(TAG, "Error occurred when calling clear method", e);
            }
        }
    }

    private String convertScoreMapToScoreString(Map<Integer, Double> map) {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (Map.Entry next : map.entrySet()) {
            if (z) {
                z = false;
            } else {
                sb.append('$');
            }
            sb.append(next.getKey());
            sb.append(':');
            sb.append(next.getValue());
        }
        return sb.toString();
    }

    private Map<Integer, Double> convertScoreStringToScoreMap(String str) {
        String[] split;
        HashMap hashMap = new HashMap();
        if (str == null) {
            return hashMap;
        }
        String[] split2 = str.split("[$]");
        for (String str2 : split2) {
            if (str2 != null && str2.length() > 0 && (split = str2.split("[:]")) != null && split.length == 2) {
                hashMap.put(Integer.valueOf(Integer.valueOf(split[0]).intValue()), Double.valueOf(Double.valueOf(split[1]).doubleValue()));
            }
        }
        return hashMap;
    }

    private static class LazyHolder {
        static final SearchTokenDao INSTANCE = new SearchTokenDaoImpl();

        private LazyHolder() {
        }
    }
}
