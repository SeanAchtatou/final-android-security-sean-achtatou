package com.helpshift.support.model;

import java.util.Locale;

public final class AttachmentFileSize {
    private static final String FILE_SIZE_UNIT_B = " B";
    private static final String FILE_SIZE_UNIT_KB = " KB";
    private static final String FILE_SIZE_UNIT_MB = " MB";
    private double fileSize;
    private String fileSizeUnit;

    public AttachmentFileSize(double d) {
        if (d < 1024.0d) {
            this.fileSize = d;
            this.fileSizeUnit = FILE_SIZE_UNIT_B;
        } else if (d < 1048576.0d) {
            this.fileSize = d / 1024.0d;
            this.fileSizeUnit = FILE_SIZE_UNIT_KB;
        } else {
            this.fileSize = d / 1048576.0d;
            this.fileSizeUnit = FILE_SIZE_UNIT_MB;
        }
    }

    public String getFormattedFileSize() {
        if (this.fileSizeUnit.equals(FILE_SIZE_UNIT_MB)) {
            return String.format(Locale.US, "%.1f", Double.valueOf(this.fileSize)) + this.fileSizeUnit;
        }
        return String.format(Locale.US, "%.0f", Double.valueOf(this.fileSize)) + this.fileSizeUnit;
    }
}
