package com.helpshift.support.model;

import java.io.Serializable;

public class FuzzySearchToken implements Serializable {
    private static final long serialVersionUID = 1;
    public final String docId;
    public final String word;

    public FuzzySearchToken(String str, String str2) {
        this.word = str;
        this.docId = str2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !(obj instanceof FuzzySearchToken)) {
            return false;
        }
        FuzzySearchToken fuzzySearchToken = (FuzzySearchToken) obj;
        if (this.word != null ? this.word.equals(fuzzySearchToken.word) : fuzzySearchToken.word == null) {
            return this.docId != null ? this.docId.equals(fuzzySearchToken.docId) : fuzzySearchToken.docId == null;
        }
        return false;
    }
}
