package com.helpshift.support.compositions;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.helpshift.support.FaqTagFilter;
import com.helpshift.support.Section;
import com.helpshift.support.fragments.QuestionListFragment;
import com.helpshift.util.HSLogger;
import java.util.List;

public class SectionPagerAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = "Helpshift_SectionPager";
    private FaqTagFilter faqTagFilter;
    private List<Section> sections;

    public SectionPagerAdapter(FragmentManager fragmentManager, List<Section> list, FaqTagFilter faqTagFilter2) {
        super(fragmentManager);
        this.sections = list;
        this.faqTagFilter = faqTagFilter2;
    }

    public Fragment getItem(int i) {
        Bundle bundle = new Bundle();
        bundle.putString("sectionPublishId", this.sections.get(i).getPublishId());
        bundle.putSerializable("withTagsMatching", this.faqTagFilter);
        return QuestionListFragment.newInstance(bundle);
    }

    public void restoreState(Parcelable parcelable, ClassLoader classLoader) {
        try {
            super.restoreState(parcelable, classLoader);
        } catch (Exception e) {
            HSLogger.e(TAG, "Exception in restoreState: ", e);
        }
    }

    public int getCount() {
        return this.sections.size();
    }

    public CharSequence getPageTitle(int i) {
        return this.sections.get(i).getTitle();
    }
}
