package com.helpshift.support.webkit;

import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;

public class CustomWebChromeClient extends WebChromeClient {
    private WebChromeClient.CustomViewCallback callback;
    private View customView;
    private final View decorView;
    private final ViewGroup fullScreenContainer;
    private final View webviewContentView;

    public CustomWebChromeClient(View view, View view2) {
        this.decorView = view;
        this.webviewContentView = view2;
        this.fullScreenContainer = (ViewGroup) view.findViewById(16908290);
    }

    public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        if (this.customView != null) {
            customViewCallback.onCustomViewHidden();
            return;
        }
        this.fullScreenContainer.addView(view);
        this.customView = view;
        this.customView.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        this.callback = customViewCallback;
        this.webviewContentView.setVisibility(8);
        enableFullScreen();
    }

    public void onHideCustomView() {
        if (this.customView != null) {
            this.customView.setVisibility(8);
            this.fullScreenContainer.removeView(this.customView);
            this.customView = null;
            if (this.callback != null) {
                this.callback.onCustomViewHidden();
            }
            this.webviewContentView.setVisibility(0);
            disableFullScreen();
        }
    }

    private void enableFullScreen() {
        int systemUiVisibility = this.decorView.getSystemUiVisibility();
        if (Build.VERSION.SDK_INT >= 14) {
            systemUiVisibility |= 2;
        }
        if (Build.VERSION.SDK_INT >= 16) {
            systemUiVisibility |= 4;
        }
        if (Build.VERSION.SDK_INT >= 18) {
            systemUiVisibility |= 4096;
        }
        this.decorView.setSystemUiVisibility(systemUiVisibility);
    }

    private void disableFullScreen() {
        int systemUiVisibility = this.decorView.getSystemUiVisibility();
        if (Build.VERSION.SDK_INT >= 14) {
            systemUiVisibility &= -3;
        }
        if (Build.VERSION.SDK_INT >= 16) {
            systemUiVisibility &= -5;
        }
        if (Build.VERSION.SDK_INT >= 18) {
            systemUiVisibility &= -4097;
        }
        this.decorView.setSystemUiVisibility(systemUiVisibility);
    }
}
