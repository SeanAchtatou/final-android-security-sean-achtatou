package com.helpshift.support;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import com.helpshift.support.flows.CustomContactUsFlowListHolder;
import com.helpshift.support.flows.Flow;
import com.helpshift.util.ApplicationUtil;
import java.util.List;

public final class HSReview extends FragmentActivity {
    private List<Flow> flowList;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(new View(this));
        this.flowList = CustomContactUsFlowListHolder.getFlowList();
        CustomContactUsFlowListHolder.setFlowList(null);
        new HSReviewFragment().show(getSupportFragmentManager(), "hs__review_dialog");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        CustomContactUsFlowListHolder.setFlowList(this.flowList);
        ApplicationUtil.restoreApplicationLocale();
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(ApplicationUtil.getContextWithUpdatedLocaleLegacy(context));
    }
}
