package com.helpshift.support.widget;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import com.helpshift.common.platform.Device;
import com.helpshift.conversation.dto.ImagePickerFile;
import com.helpshift.support.widget.ImagePicker.ImagePickerListener;
import com.helpshift.util.FileUtil;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.util.ImageUtil;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.HashSet;

public class ImagePicker<T extends Fragment & ImagePickerListener> {
    public static final int IMAGE_FILE_NOT_FOUND = -1;
    public static final int IMAGE_FILE_SIZE_LIMIT_EXCEEDED = -3;
    public static final int INVALID_IMAGE_URI = -2;
    private static final long MAX_IMAGE_FILE_SIZE_LIMIT = 26214400;
    public static final int NO_APPS_TO_OPEN_IMAGES_INTENT = -4;
    public static final int PICK_IMAGE_REQUEST_ID = 1;
    public static final int PICK_IMAGE_WITHOUT_PERMISSIONS_REQUEST_ID = 2;
    private static final String TAG = "Helpshift_ImagePicker";
    private final String KEY_EXTRA_DATA = "key_extra_data";
    private Device device = HelpshiftContext.getPlatform().getDevice();
    private Bundle extraData;
    private WeakReference<T> imagePickerListenerRef;

    public interface ImagePickerListener {
        void askForReadStoragePermission();

        void onImagePickerResultFailure(int i, Long l);

        void onImagePickerResultSuccess(ImagePickerFile imagePickerFile, Bundle bundle);
    }

    public ImagePicker(T t) {
        this.imagePickerListenerRef = new WeakReference<>(t);
    }

    public void checkPermissionAndLaunchImagePicker(Bundle bundle) {
        HSLogger.d(TAG, "Checking permission before launching attachment picker");
        switch (this.device.checkPermission(Device.PermissionType.READ_STORAGE)) {
            case AVAILABLE:
                launchImagePicker(bundle);
                return;
            case UNAVAILABLE:
                HSLogger.d(TAG, "READ_STORAGE permission is not granted and can't be requested, starting alternate flow");
                launchImagePicker(bundle, 2);
                return;
            case REQUESTABLE:
                HSLogger.d(TAG, "READ_STORAGE permission is not granted but can be requested");
                Fragment fragment = (Fragment) this.imagePickerListenerRef.get();
                if (fragment != null) {
                    ((ImagePickerListener) fragment).askForReadStoragePermission();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void launchImagePicker(Bundle bundle) {
        launchImagePicker(bundle, 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void launchImagePicker(Bundle bundle, int i) {
        Intent intent;
        this.extraData = bundle;
        HSLogger.d(TAG, "Launching attachment picker now, flowRequestCode: " + i);
        Context applicationContext = HelpshiftContext.getApplicationContext();
        int oSVersionNumber = HelpshiftContext.getPlatform().getDevice().getOSVersionNumber();
        if (i != 2 || oSVersionNumber < 19) {
            Intent intent2 = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            if (intent2.resolveActivity(applicationContext.getPackageManager()) != null) {
                startActivityForResult(intent2, i);
                return;
            }
            Intent intent3 = new Intent("android.intent.action.GET_CONTENT");
            intent3.setType("image/*");
            if (oSVersionNumber >= 11) {
                intent3.putExtra("android.intent.extra.LOCAL_ONLY", true);
            }
            intent = intent3;
        } else {
            intent = new Intent("android.intent.action.OPEN_DOCUMENT");
            intent.setType("image/*");
            intent.addFlags(1);
            intent.putExtra("android.intent.extra.LOCAL_ONLY", true);
        }
        if (intent.resolveActivity(applicationContext.getPackageManager()) == null) {
            HSLogger.e(TAG, "No app found for handling image pick intent " + intent);
            sendImagePickerResultFailureCallback(-4, null);
            return;
        }
        startActivityForResult(intent, i);
    }

    public void onImagePickRequestResult(int i, Intent intent) {
        Uri data = intent.getData();
        if (i == 1) {
            HSLogger.d(TAG, "Processing image uri with flow when permissions are available");
            processUriForAttachment(data);
        } else if (i == 2) {
            HSLogger.d(TAG, "Processing image uri with flow when permissions are not available");
            int flags = intent.getFlags() & 1;
            if (Build.VERSION.SDK_INT >= 19) {
                HelpshiftContext.getApplicationContext().getContentResolver().takePersistableUriPermission(data, flags);
            }
            processUriForAttachment(data);
        }
    }

    private void processUriForAttachment(Uri uri) {
        if (isImageUriValid(uri)) {
            Context applicationContext = HelpshiftContext.getApplicationContext();
            if (FileUtil.doesFileFromUriExistAndCanRead(uri, applicationContext)) {
                ImagePickerFile createImagePickerFileFromUri = createImagePickerFileFromUri(uri);
                Long l = createImagePickerFileFromUri.originalFileSize;
                if (l == null || l.longValue() <= MAX_IMAGE_FILE_SIZE_LIMIT || ImageUtil.isResizableImage(uri, applicationContext)) {
                    HSLogger.d(TAG, "Image picker result success, path: " + uri);
                    sendImagePickerResultSuccessCallback(createImagePickerFileFromUri, this.extraData);
                    return;
                }
                HSLogger.d(TAG, "Image picker file size limit exceeded (in bytes): " + l + ", returning failure");
                sendImagePickerResultFailureCallback(-3, Long.valueOf((long) MAX_IMAGE_FILE_SIZE_LIMIT));
                return;
            }
            HSLogger.d(TAG, "Image picker file reading error, returning failure");
            sendImagePickerResultFailureCallback(-1, null);
            return;
        }
        HSLogger.d(TAG, "Image picker file invalid mime type, returning failure");
        sendImagePickerResultFailureCallback(-2, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x006c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.helpshift.conversation.dto.ImagePickerFile createImagePickerFileFromUri(android.net.Uri r8) {
        /*
            r7 = this;
            android.content.Context r0 = com.helpshift.util.HelpshiftContext.getApplicationContext()
            android.content.ContentResolver r1 = r0.getContentResolver()
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r2 = r8
            android.database.Cursor r0 = r1.query(r2, r3, r4, r5, r6)
            r1 = 0
            if (r0 == 0) goto L_0x0069
            boolean r2 = r0.moveToFirst()     // Catch:{ all -> 0x0062 }
            if (r2 == 0) goto L_0x0069
            java.lang.String r2 = "_display_name"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ all -> 0x0062 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ all -> 0x0062 }
            boolean r3 = com.helpshift.common.StringUtils.isEmpty(r2)     // Catch:{ all -> 0x0062 }
            if (r3 == 0) goto L_0x0032
            java.util.UUID r2 = java.util.UUID.randomUUID()     // Catch:{ all -> 0x0062 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0062 }
        L_0x0032:
            java.lang.String r3 = "_size"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ all -> 0x0062 }
            boolean r4 = r0.isNull(r3)     // Catch:{ all -> 0x0062 }
            if (r4 != 0) goto L_0x006a
            java.lang.String r3 = r0.getString(r3)     // Catch:{ all -> 0x0062 }
            if (r3 == 0) goto L_0x006a
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ NumberFormatException -> 0x004a }
            r1 = r3
            goto L_0x006a
        L_0x004a:
            r3 = move-exception
            java.lang.String r4 = "Helpshift_ImagePicker"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0062 }
            r5.<init>()     // Catch:{ all -> 0x0062 }
            java.lang.String r6 = "Error getting size due to "
            r5.append(r6)     // Catch:{ all -> 0x0062 }
            r5.append(r3)     // Catch:{ all -> 0x0062 }
            java.lang.String r3 = r5.toString()     // Catch:{ all -> 0x0062 }
            com.helpshift.util.HSLogger.d(r4, r3)     // Catch:{ all -> 0x0062 }
            goto L_0x006a
        L_0x0062:
            r8 = move-exception
            if (r0 == 0) goto L_0x0068
            r0.close()
        L_0x0068:
            throw r8
        L_0x0069:
            r2 = r1
        L_0x006a:
            if (r0 == 0) goto L_0x006f
            r0.close()
        L_0x006f:
            com.helpshift.conversation.dto.ImagePickerFile r0 = new com.helpshift.conversation.dto.ImagePickerFile
            r0.<init>(r8, r2, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.widget.ImagePicker.createImagePickerFileFromUri(android.net.Uri):com.helpshift.conversation.dto.ImagePickerFile");
    }

    private boolean isImageUriValid(Uri uri) {
        return new HashSet(Arrays.asList("image/jpeg", "image/png", "image/gif", "image/x-png", "image/x-citrix-pjpeg", "image/x-citrix-gif", "image/pjpeg")).contains(HelpshiftContext.getApplicationContext().getContentResolver().getType(uri));
    }

    private void sendImagePickerResultSuccessCallback(ImagePickerFile imagePickerFile, Bundle bundle) {
        Fragment fragment = (Fragment) this.imagePickerListenerRef.get();
        if (fragment != null) {
            ((ImagePickerListener) fragment).onImagePickerResultSuccess(imagePickerFile, bundle);
        }
    }

    private void sendImagePickerResultFailureCallback(int i, Long l) {
        Fragment fragment = (Fragment) this.imagePickerListenerRef.get();
        if (fragment != null) {
            ((ImagePickerListener) fragment).onImagePickerResultFailure(i, l);
        }
    }

    private void startActivityForResult(Intent intent, int i) {
        try {
            Fragment fragment = (Fragment) this.imagePickerListenerRef.get();
            if (fragment != null && fragment.getActivity() != null) {
                fragment.startActivityForResult(intent, i);
            }
        } catch (ActivityNotFoundException e) {
            HSLogger.e(TAG, "Error occurred while starting app for handling image pick intent " + e);
            sendImagePickerResultFailureCallback(-4, null);
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBundle("key_extra_data", this.extraData);
    }

    public void onViewStateRestored(Bundle bundle) {
        if (bundle.containsKey("key_extra_data")) {
            this.extraData = bundle.getBundle("key_extra_data");
        }
    }
}
