package com.helpshift.support.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import com.helpshift.R;
import com.helpshift.support.util.Styles;

public class CSATView extends RelativeLayout implements RatingBar.OnRatingBarChangeListener {
    private CSATDialog csatDialog;
    private CSATListener csatListener = null;
    private RatingBar ratingBar;

    public interface CSATListener {
        void sendCSATSurvey(int i, String str);
    }

    public CSATView(Context context) {
        super(context);
        initView(context);
    }

    public CSATView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initView(context);
    }

    public CSATView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initView(context);
    }

    private void initView(Context context) {
        View.inflate(context, R.layout.hs__csat_view, this);
        this.csatDialog = new CSATDialog(context);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        Styles.setAccentColor(getContext(), this.ratingBar.getProgressDrawable());
        this.ratingBar.setOnRatingBarChangeListener(this);
    }

    public void onRatingChanged(RatingBar ratingBar2, float f, boolean z) {
        if (z) {
            this.csatDialog.show(this);
        }
    }

    /* access modifiers changed from: protected */
    public RatingBar getRatingBar() {
        return this.ratingBar;
    }

    /* access modifiers changed from: protected */
    public void dismiss() {
        setVisibility(8);
        this.csatDialog = null;
    }

    /* access modifiers changed from: protected */
    public void sendCSATSurvey(float f, String str) {
        if (this.csatListener != null) {
            this.csatListener.sendCSATSurvey(Math.round(f), str);
        }
    }

    public void setCSATListener(CSATListener cSATListener) {
        this.csatListener = cSATListener;
    }
}
