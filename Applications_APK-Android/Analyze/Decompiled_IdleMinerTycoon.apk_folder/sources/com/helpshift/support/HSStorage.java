package com.helpshift.support;

import android.content.Context;
import android.content.SharedPreferences;
import com.helpshift.support.model.FaqSearchIndex;
import com.helpshift.support.search.storage.SearchTokenDaoImpl;
import com.helpshift.support.storage.FaqsDataSource;
import com.helpshift.util.HSLogger;
import com.helpshift.util.IOUtils;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class HSStorage {
    static final String TAG = "HelpShiftDebug";
    private static FaqSearchIndex cachedSearchIndex;
    private Context context;
    private final String dbFile = "fullIndex.db";
    private SharedPreferences storage;

    public HSStorage(Context context2) {
        this.context = context2;
        this.storage = context2.getSharedPreferences(Support.JSON_PREFS, 0);
    }

    private JSONObject storageGetObj(String str) throws JSONException {
        return new JSONObject(this.storage.getString(str, "{}"));
    }

    private JSONArray storageGetArr(String str) throws JSONException {
        return new JSONArray(this.storage.getString(str, "[]"));
    }

    private String storageGet(String str) {
        return this.storage.getString(str, "");
    }

    private Integer storageGetInt(String str) {
        return storageGetInt(str, 0);
    }

    private Integer storageGetInt(String str, int i) {
        return Integer.valueOf(this.storage.getInt(str, i));
    }

    public Float storageGetFloat(String str) {
        return Float.valueOf(this.storage.getFloat(str, 0.0f));
    }

    public boolean contains(String str) {
        return this.storage.contains(str);
    }

    public Boolean storageGetBoolean(String str) {
        return Boolean.valueOf(this.storage.getBoolean(str, false));
    }

    private Long storageGetLong(String str) {
        return Long.valueOf(this.storage.getLong(str, 0));
    }

    private void storageSet(String str, JSONArray jSONArray) {
        SharedPreferences.Editor edit = this.storage.edit();
        edit.putString(str, jSONArray.toString());
        edit.apply();
    }

    private void storageSet(String str, JSONObject jSONObject) {
        SharedPreferences.Editor edit = this.storage.edit();
        edit.putString(str, jSONObject.toString());
        edit.apply();
    }

    private void storageSet(String str, String str2) {
        SharedPreferences.Editor edit = this.storage.edit();
        edit.putString(str, str2);
        edit.apply();
    }

    private void storageSet(String str, Integer num) {
        SharedPreferences.Editor edit = this.storage.edit();
        edit.putInt(str, num.intValue());
        edit.apply();
    }

    private void storageSet(String str, Boolean bool) {
        SharedPreferences.Editor edit = this.storage.edit();
        edit.putBoolean(str, bool.booleanValue());
        edit.apply();
    }

    private void storageSet(String str, Long l) {
        SharedPreferences.Editor edit = this.storage.edit();
        edit.putLong(str, l.longValue());
        edit.apply();
    }

    /* access modifiers changed from: protected */
    public void clearDatabase() {
        FaqsDataSource.getInstance().clearDB();
        SharedPreferences.Editor edit = this.storage.edit();
        edit.clear();
        edit.apply();
    }

    /* access modifiers changed from: protected */
    public void clearLegacySearchIndexFile() {
        this.context.deleteFile("tfidf.db");
    }

    /* access modifiers changed from: protected */
    public String getApiKey() {
        return storageGet("apiKey");
    }

    /* access modifiers changed from: protected */
    public void setApiKey(String str) {
        storageSet("apiKey", str);
    }

    /* access modifiers changed from: protected */
    public String getDomain() {
        return storageGet("domain");
    }

    /* access modifiers changed from: protected */
    public void setDomain(String str) {
        storageSet("domain", str);
    }

    /* access modifiers changed from: protected */
    public String getAppId() {
        return storageGet("appId");
    }

    /* access modifiers changed from: protected */
    public void setAppId(String str) {
        storageSet("appId", str);
    }

    /* access modifiers changed from: protected */
    public String getLibraryVersion() {
        return storageGet("libraryVersion");
    }

    /* access modifiers changed from: protected */
    public void setLibraryVersion(String str) {
        storageSet("libraryVersion", str);
    }

    /* access modifiers changed from: protected */
    public String getApplicationVersion() {
        return storageGet("applicationVersion");
    }

    /* access modifiers changed from: protected */
    public void setApplicationVersion(String str) {
        storageSet("applicationVersion", str);
    }

    /* access modifiers changed from: protected */
    public int getReviewCounter() {
        return storageGetInt("reviewCounter").intValue();
    }

    /* access modifiers changed from: protected */
    public void setReviewCounter(int i) {
        storageSet("reviewCounter", Integer.valueOf(i));
    }

    /* access modifiers changed from: protected */
    public int getLaunchReviewCounter() {
        return storageGetInt("launchReviewCounter").intValue();
    }

    /* access modifiers changed from: protected */
    public void setLaunchReviewCounter(int i) {
        storageSet("launchReviewCounter", Integer.valueOf(i));
    }

    /* access modifiers changed from: protected */
    public JSONArray getStoredFiles() throws JSONException {
        return storageGetArr("cachedImages");
    }

    /* access modifiers changed from: protected */
    public void setStoredFiles(JSONArray jSONArray) {
        storageSet("cachedImages", jSONArray);
    }

    /* access modifiers changed from: protected */
    public void storeIndex(FaqSearchIndex faqSearchIndex) {
        ObjectOutputStream objectOutputStream;
        cachedSearchIndex = faqSearchIndex;
        FileOutputStream fileOutputStream = null;
        try {
            FileOutputStream openFileOutput = this.context.openFileOutput("fullIndex.db", 0);
            try {
                objectOutputStream = new ObjectOutputStream(openFileOutput);
            } catch (Exception e) {
                e = e;
                objectOutputStream = null;
                fileOutputStream = openFileOutput;
                try {
                    HSLogger.d("HelpShiftDebug", "store index", e);
                    IOUtils.closeQuitely(fileOutputStream);
                    IOUtils.closeQuitely(objectOutputStream);
                } catch (Throwable th) {
                    th = th;
                    IOUtils.closeQuitely(fileOutputStream);
                    IOUtils.closeQuitely(objectOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                objectOutputStream = null;
                fileOutputStream = openFileOutput;
                IOUtils.closeQuitely(fileOutputStream);
                IOUtils.closeQuitely(objectOutputStream);
                throw th;
            }
            try {
                objectOutputStream.writeObject(faqSearchIndex);
                objectOutputStream.flush();
                setDBFlag();
                IOUtils.closeQuitely(openFileOutput);
            } catch (Exception e2) {
                e = e2;
                fileOutputStream = openFileOutput;
                HSLogger.d("HelpShiftDebug", "store index", e);
                IOUtils.closeQuitely(fileOutputStream);
                IOUtils.closeQuitely(objectOutputStream);
            } catch (Throwable th3) {
                th = th3;
                fileOutputStream = openFileOutput;
                IOUtils.closeQuitely(fileOutputStream);
                IOUtils.closeQuitely(objectOutputStream);
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            objectOutputStream = null;
            HSLogger.d("HelpShiftDebug", "store index", e);
            IOUtils.closeQuitely(fileOutputStream);
            IOUtils.closeQuitely(objectOutputStream);
        } catch (Throwable th4) {
            th = th4;
            objectOutputStream = null;
            IOUtils.closeQuitely(fileOutputStream);
            IOUtils.closeQuitely(objectOutputStream);
            throw th;
        }
        IOUtils.closeQuitely(objectOutputStream);
    }

    /* access modifiers changed from: protected */
    public void loadIndex() throws IOException, ClassCastException, ClassNotFoundException {
        ObjectInputStream objectInputStream;
        FileInputStream fileInputStream;
        Throwable th;
        if (cachedSearchIndex == null) {
            try {
                fileInputStream = this.context.openFileInput("fullIndex.db");
                try {
                    objectInputStream = new ObjectInputStream(fileInputStream);
                    try {
                        cachedSearchIndex = (FaqSearchIndex) objectInputStream.readObject();
                        IOUtils.closeQuitely(fileInputStream);
                        IOUtils.closeQuitely(objectInputStream);
                    } catch (Throwable th2) {
                        th = th2;
                        IOUtils.closeQuitely(fileInputStream);
                        IOUtils.closeQuitely(objectInputStream);
                        throw th;
                    }
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    objectInputStream = null;
                    th = th4;
                    IOUtils.closeQuitely(fileInputStream);
                    IOUtils.closeQuitely(objectInputStream);
                    throw th;
                }
            } catch (Throwable th5) {
                objectInputStream = null;
                th = th5;
                fileInputStream = null;
                IOUtils.closeQuitely(fileInputStream);
                IOUtils.closeQuitely(objectInputStream);
                throw th;
            }
        }
    }

    /* access modifiers changed from: protected */
    public FaqSearchIndex readIndex() {
        return cachedSearchIndex;
    }

    /* access modifiers changed from: protected */
    public Boolean getDBFlag() {
        return storageGetBoolean("dbFlag");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.HSStorage.storageSet(java.lang.String, java.lang.Boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.helpshift.support.HSStorage.storageSet(java.lang.String, java.lang.Integer):void
      com.helpshift.support.HSStorage.storageSet(java.lang.String, java.lang.Long):void
      com.helpshift.support.HSStorage.storageSet(java.lang.String, java.lang.String):void
      com.helpshift.support.HSStorage.storageSet(java.lang.String, org.json.JSONArray):void
      com.helpshift.support.HSStorage.storageSet(java.lang.String, org.json.JSONObject):void
      com.helpshift.support.HSStorage.storageSet(java.lang.String, java.lang.Boolean):void */
    /* access modifiers changed from: protected */
    public void setDBFlag() {
        storageSet("dbFlag", (Boolean) true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.HSStorage.storageSet(java.lang.String, java.lang.Boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.helpshift.support.HSStorage.storageSet(java.lang.String, java.lang.Integer):void
      com.helpshift.support.HSStorage.storageSet(java.lang.String, java.lang.Long):void
      com.helpshift.support.HSStorage.storageSet(java.lang.String, java.lang.String):void
      com.helpshift.support.HSStorage.storageSet(java.lang.String, org.json.JSONArray):void
      com.helpshift.support.HSStorage.storageSet(java.lang.String, org.json.JSONObject):void
      com.helpshift.support.HSStorage.storageSet(java.lang.String, java.lang.Boolean):void */
    /* access modifiers changed from: protected */
    public void unsetDBFlag() {
        storageSet("dbFlag", (Boolean) false);
    }

    /* access modifiers changed from: protected */
    public void deleteIndex() {
        cachedSearchIndex = null;
        SearchTokenDaoImpl.getInstance().clear();
        this.context.deleteFile("fullIndex.db");
        unsetDBFlag();
    }

    /* access modifiers changed from: protected */
    public JSONObject getFailedApiCalls() throws JSONException {
        return storageGetObj("failedApiCalls");
    }

    /* access modifiers changed from: protected */
    public void storeFailedApiCall(String str, JSONObject jSONObject) throws JSONException {
        JSONObject failedApiCalls = getFailedApiCalls();
        failedApiCalls.put(str, jSONObject);
        storageSet("failedApiCalls", failedApiCalls);
    }

    /* access modifiers changed from: package-private */
    public long getLastErrorReportedTime() {
        return storageGetLong("lastErrorReportedTime").longValue();
    }

    /* access modifiers changed from: package-private */
    public void setLastErrorReportedTime(long j) {
        storageSet("lastErrorReportedTime", Long.valueOf(j));
    }

    /* access modifiers changed from: package-private */
    public boolean isCacheSearchIndexNull() {
        return cachedSearchIndex == null;
    }

    public String getString(String str) {
        return this.storage.getString(str, "");
    }
}
