package com.helpshift.support.external;

public class DoubleMetaphone {
    private static final String[] ES_EP_EB_EL_EY_IB_IL_IN_IE_EI_ER = {"ES", "EP", "EB", "EL", "EY", "IB", "IL", "IN", "IE", "EI", "ER"};
    private static final String[] L_R_N_M_B_H_F_V_W_SPACE = {"L", "R", "N", "M", "B", "H", "F", "V", "W", " "};
    private static final String[] L_T_K_S_N_M_B_Z = {"L", "T", "K", "S", "N", "M", "B", "Z"};
    private static final String[] SILENT_START = {"GN", "KN", "PN", "WR", "PS"};
    private static final String VOWELS = "AEIOUY";
    int maxCodeLen = 4;

    protected static boolean contains(String str, int i, int i2, String[] strArr) {
        int i3;
        if (i < 0 || (i3 = i2 + i) > str.length()) {
            return false;
        }
        String substring = str.substring(i, i3);
        for (String equals : strArr) {
            if (substring.equals(equals)) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v1, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v2, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v1, resolved type: com.helpshift.support.external.DoubleMetaphone$DoubleMetaphoneResult} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v3, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v4, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v5, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v6, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v7, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v25, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v8, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v33, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v9, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v36, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v37, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v38, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v10, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v11, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v59, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v60, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v18, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v19, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v20, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v106, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v41, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v42, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v43, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v44, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v45, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v46, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v47, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v155, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v60, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v175, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v61, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v166, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v62, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v65, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v66, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v67, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v68, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v69, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v70, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v71, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v72, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v73, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v74, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v75, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v76, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v296, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v77, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v296, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v78, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v299, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v85, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v328, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v86, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v335, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v87, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v338, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v339, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v88, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v350, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v89, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v355, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v90, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v91, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v393, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v394, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v92, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v93, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v94, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v95, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v96, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v97, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v98, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v101, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v563, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v109, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v572, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v110, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v586, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v112, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v115, resolved type: int} */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x024d, code lost:
        if (contains(r1, r7, 2, new java.lang.String[]{"WH"}) != false) goto L_0x024f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:247:0x059b, code lost:
        if (contains(r1, r7 + 1, 1, new java.lang.String[]{"M", "N", "L", "W"}) == false) goto L_0x059d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:419:0x09b8, code lost:
        if (contains(r1, r7 + 2, 2, new java.lang.String[]{"ER"}) != false) goto L_0x0995;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:428:0x09e8, code lost:
        if (contains(r1, r7 - 1, 4, new java.lang.String[]{"ILLO", "ILLA", "ALLE"}) != false) goto L_0x09ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:429:0x09ea, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:433:0x0a14, code lost:
        if (contains(r1, r1.length() - 1, 1, new java.lang.String[]{"A", "O"}) != false) goto L_0x0a16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:435:0x0a23, code lost:
        if (contains(r1, r7 - 1, 4, new java.lang.String[]{"ALLE"}) != false) goto L_0x09ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:436:0x0a26, code lost:
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:437:0x0a27, code lost:
        if (r4 == false) goto L_0x0a3b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:439:0x0a31, code lost:
        if (r8.primary.length() >= r8.maxLength) goto L_0x0a5d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:440:0x0a33, code lost:
        r8.primary.append('L');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:442:0x0a43, code lost:
        if (r8.primary.length() >= r8.maxLength) goto L_0x0a4c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:443:0x0a45, code lost:
        r8.primary.append('L');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:445:0x0a54, code lost:
        if (r8.alternate.length() >= r8.maxLength) goto L_0x0a5d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:446:0x0a56, code lost:
        r8.alternate.append('L');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:447:0x0a5d, code lost:
        r7 = r7 + 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:575:0x0cf7, code lost:
        if (contains(r1, r7 - 2, 1, new java.lang.String[]{"B", "H", "D"}) == false) goto L_0x0cf9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:578:0x0d0f, code lost:
        if (contains(r1, r7 - 3, 1, new java.lang.String[]{"B", "H", "D"}) == false) goto L_0x0d11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:581:0x0d23, code lost:
        if (contains(r1, r7 - 4, 1, new java.lang.String[]{"B", "H"}) != false) goto L_0x0d25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:582:0x0d25, code lost:
        r7 = r7 + 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:788:0x122e, code lost:
        if (contains(r1, r9, 6, new java.lang.String[]{"BACHER", "MACHER"}) != false) goto L_0x11ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:816:0x12c6, code lost:
        if (contains(r1, r4, 3, new java.lang.String[]{"HOR", "HYM", "HIA", "HEM"}) == false) goto L_0x129a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01a0, code lost:
        if (contains(r1, r7 - 2, 2, new java.lang.String[]{"AU", "OU"}) == false) goto L_0x01a2;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:333:0x07ef  */
    /* JADX WARNING: Removed duplicated region for block: B:366:0x089f  */
    /* JADX WARNING: Removed duplicated region for block: B:422:0x09be  */
    /* JADX WARNING: Removed duplicated region for block: B:510:0x0ba8  */
    /* JADX WARNING: Removed duplicated region for block: B:790:0x1233  */
    /* JADX WARNING: Removed duplicated region for block: B:799:0x125c  */
    /* JADX WARNING: Removed duplicated region for block: B:821:0x12da  */
    /* JADX WARNING: Removed duplicated region for block: B:831:0x130f  */
    /* JADX WARNING: Removed duplicated region for block: B:839:0x1336  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x01fc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String doubleMetaphone(java.lang.String r17, boolean r18) {
        /*
            r16 = this;
            r0 = r16
            if (r17 != 0) goto L_0x0006
            r1 = 0
            return r1
        L_0x0006:
            java.lang.String r1 = r17.trim()
            int r2 = r1.length()
            if (r2 != 0) goto L_0x0012
            r1 = 0
            return r1
        L_0x0012:
            java.util.Locale r2 = java.util.Locale.ENGLISH
            java.lang.String r1 = r1.toUpperCase(r2)
            r2 = 87
            int r2 = r1.indexOf(r2)
            r3 = -1
            r4 = 75
            r5 = 0
            r6 = 1
            if (r2 > r3) goto L_0x003e
            int r2 = r1.indexOf(r4)
            if (r2 > r3) goto L_0x003e
            java.lang.String r2 = "CZ"
            int r2 = r1.indexOf(r2)
            if (r2 > r3) goto L_0x003e
            java.lang.String r2 = "WITZ"
            int r2 = r1.indexOf(r2)
            if (r2 <= r3) goto L_0x003c
            goto L_0x003e
        L_0x003c:
            r2 = 0
            goto L_0x003f
        L_0x003e:
            r2 = 1
        L_0x003f:
            java.lang.String[] r7 = com.helpshift.support.external.DoubleMetaphone.SILENT_START
            int r8 = r7.length
            r9 = 0
        L_0x0043:
            if (r9 >= r8) goto L_0x0052
            r10 = r7[r9]
            boolean r10 = r1.startsWith(r10)
            if (r10 == 0) goto L_0x004f
            r7 = 1
            goto L_0x0053
        L_0x004f:
            int r9 = r9 + 1
            goto L_0x0043
        L_0x0052:
            r7 = 0
        L_0x0053:
            com.helpshift.support.external.DoubleMetaphone$DoubleMetaphoneResult r8 = new com.helpshift.support.external.DoubleMetaphone$DoubleMetaphoneResult
            int r9 = r0.maxCodeLen
            r8.<init>(r9)
        L_0x005a:
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 < r10) goto L_0x006e
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x175a
        L_0x006e:
            int r9 = r1.length()
            int r9 = r9 - r6
            if (r7 > r9) goto L_0x175a
            char r9 = r1.charAt(r7)
            r10 = 199(0xc7, float:2.79E-43)
            r11 = 83
            if (r9 == r10) goto L_0x1736
            r10 = 209(0xd1, float:2.93E-43)
            if (r9 == r10) goto L_0x170e
            r12 = 65
            r13 = 84
            r14 = 72
            r4 = 74
            r10 = 3
            r15 = 2
            switch(r9) {
                case 65: goto L_0x16e8;
                case 66: goto L_0x16b6;
                case 67: goto L_0x11e0;
                case 68: goto L_0x10fd;
                case 69: goto L_0x16e8;
                case 70: goto L_0x10ce;
                case 71: goto L_0x0c51;
                case 72: goto L_0x0c0d;
                case 73: goto L_0x16e8;
                case 74: goto L_0x0ab4;
                case 75: goto L_0x0a85;
                case 76: goto L_0x09c2;
                case 77: goto L_0x0969;
                case 78: goto L_0x0939;
                case 79: goto L_0x16e8;
                case 80: goto L_0x08d4;
                case 81: goto L_0x08a3;
                case 82: goto L_0x0838;
                case 83: goto L_0x04ac;
                case 84: goto L_0x0382;
                case 85: goto L_0x16e8;
                case 86: goto L_0x0351;
                case 87: goto L_0x0201;
                case 88: goto L_0x0153;
                case 89: goto L_0x16e8;
                case 90: goto L_0x0095;
                default: goto L_0x0090;
            }
        L_0x0090:
            int r7 = r7 + 1
        L_0x0092:
            r4 = 75
            goto L_0x005a
        L_0x0095:
            int r9 = r7 + 1
            char r12 = r0.charAt(r1, r9)
            if (r12 != r14) goto L_0x00be
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x00ac
            java.lang.StringBuilder r9 = r8.primary
            r9.append(r4)
        L_0x00ac:
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x00bb
            java.lang.StringBuilder r9 = r8.alternate
            r9.append(r4)
        L_0x00bb:
            int r7 = r7 + 2
            goto L_0x0092
        L_0x00be:
            java.lang.String[] r4 = new java.lang.String[r10]
            java.lang.String r10 = "ZO"
            r4[r5] = r10
            java.lang.String r10 = "ZI"
            r4[r6] = r10
            java.lang.String r10 = "ZA"
            r4[r15] = r10
            boolean r4 = contains(r1, r9, r15, r4)
            if (r4 != 0) goto L_0x00fe
            if (r2 == 0) goto L_0x00df
            if (r7 <= 0) goto L_0x00df
            int r4 = r7 + -1
            char r4 = r0.charAt(r1, r4)
            if (r4 == r13) goto L_0x00df
            goto L_0x00fe
        L_0x00df:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x00ee
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r11)
        L_0x00ee:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x0146
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r11)
            goto L_0x0146
        L_0x00fe:
            int r4 = r8.maxLength
            java.lang.StringBuilder r10 = r8.primary
            int r10 = r10.length()
            int r4 = r4 - r10
            java.lang.String r10 = "S"
            int r10 = r10.length()
            if (r10 > r4) goto L_0x0117
            java.lang.StringBuilder r4 = r8.primary
            java.lang.String r10 = "S"
            r4.append(r10)
            goto L_0x0122
        L_0x0117:
            java.lang.StringBuilder r10 = r8.primary
            java.lang.String r11 = "S"
            java.lang.String r4 = r11.substring(r5, r4)
            r10.append(r4)
        L_0x0122:
            int r4 = r8.maxLength
            java.lang.StringBuilder r10 = r8.alternate
            int r10 = r10.length()
            int r4 = r4 - r10
            java.lang.String r10 = "TS"
            int r10 = r10.length()
            if (r10 > r4) goto L_0x013b
            java.lang.StringBuilder r4 = r8.alternate
            java.lang.String r10 = "TS"
            r4.append(r10)
            goto L_0x0146
        L_0x013b:
            java.lang.StringBuilder r10 = r8.alternate
            java.lang.String r11 = "TS"
            java.lang.String r4 = r11.substring(r5, r4)
            r10.append(r4)
        L_0x0146:
            char r4 = r0.charAt(r1, r9)
            r10 = 90
            if (r4 != r10) goto L_0x0150
            int r9 = r7 + 2
        L_0x0150:
            r7 = r9
            goto L_0x0092
        L_0x0153:
            if (r7 != 0) goto L_0x0177
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0164
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r11)
        L_0x0164:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0173
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r11)
        L_0x0173:
            int r7 = r7 + 1
            goto L_0x0092
        L_0x0177:
            int r4 = r1.length()
            int r4 = r4 - r6
            if (r7 != r4) goto L_0x01a2
            int r4 = r7 + -3
            java.lang.String[] r9 = new java.lang.String[r15]
            java.lang.String r11 = "IAU"
            r9[r5] = r11
            java.lang.String r11 = "EAU"
            r9[r6] = r11
            boolean r4 = contains(r1, r4, r10, r9)
            if (r4 != 0) goto L_0x01ea
            int r4 = r7 + -2
            java.lang.String[] r9 = new java.lang.String[r15]
            java.lang.String r10 = "AU"
            r9[r5] = r10
            java.lang.String r10 = "OU"
            r9[r6] = r10
            boolean r4 = contains(r1, r4, r15, r9)
            if (r4 != 0) goto L_0x01ea
        L_0x01a2:
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "KS"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x01bb
            java.lang.StringBuilder r4 = r8.primary
            java.lang.String r9 = "KS"
            r4.append(r9)
            goto L_0x01c6
        L_0x01bb:
            java.lang.StringBuilder r9 = r8.primary
            java.lang.String r10 = "KS"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x01c6:
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "KS"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x01df
            java.lang.StringBuilder r4 = r8.alternate
            java.lang.String r9 = "KS"
            r4.append(r9)
            goto L_0x01ea
        L_0x01df:
            java.lang.StringBuilder r9 = r8.alternate
            java.lang.String r10 = "KS"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x01ea:
            int r4 = r7 + 1
            java.lang.String[] r9 = new java.lang.String[r15]
            java.lang.String r10 = "C"
            r9[r5] = r10
            java.lang.String r10 = "X"
            r9[r6] = r10
            boolean r9 = contains(r1, r4, r6, r9)
            if (r9 == 0) goto L_0x01fe
            int r4 = r7 + 2
        L_0x01fe:
            r7 = r4
            goto L_0x0092
        L_0x0201:
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r9 = "WR"
            r4[r5] = r9
            boolean r4 = contains(r1, r7, r15, r4)
            if (r4 == 0) goto L_0x0233
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x021e
            java.lang.StringBuilder r4 = r8.primary
            r9 = 82
            r4.append(r9)
        L_0x021e:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x022f
            java.lang.StringBuilder r4 = r8.alternate
            r9 = 82
            r4.append(r9)
        L_0x022f:
            int r7 = r7 + 2
            goto L_0x0092
        L_0x0233:
            if (r7 != 0) goto L_0x029d
            java.lang.String r4 = "AEIOUY"
            int r9 = r7 + 1
            char r11 = r0.charAt(r1, r9)
            int r4 = r4.indexOf(r11)
            if (r4 != r3) goto L_0x024f
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r11 = "WH"
            r4[r5] = r11
            boolean r4 = contains(r1, r7, r15, r4)
            if (r4 == 0) goto L_0x029d
        L_0x024f:
            java.lang.String r4 = "AEIOUY"
            char r7 = r0.charAt(r1, r9)
            int r4 = r4.indexOf(r7)
            if (r4 == r3) goto L_0x027d
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r7 = r8.maxLength
            if (r4 >= r7) goto L_0x026a
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r12)
        L_0x026a:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r7 = r8.maxLength
            if (r4 >= r7) goto L_0x0150
            java.lang.StringBuilder r4 = r8.alternate
            r7 = 70
            r4.append(r7)
            goto L_0x0150
        L_0x027d:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r7 = r8.maxLength
            if (r4 >= r7) goto L_0x028c
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r12)
        L_0x028c:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r7 = r8.maxLength
            if (r4 >= r7) goto L_0x0150
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r12)
            goto L_0x0150
        L_0x029d:
            int r4 = r1.length()
            int r4 = r4 - r6
            if (r7 != r4) goto L_0x02b2
            java.lang.String r4 = "AEIOUY"
            int r9 = r7 + -1
            char r9 = r0.charAt(r1, r9)
            int r4 = r4.indexOf(r9)
            if (r4 != r3) goto L_0x033c
        L_0x02b2:
            int r4 = r7 + -1
            r9 = 4
            java.lang.String[] r11 = new java.lang.String[r9]
            java.lang.String r9 = "EWSKI"
            r11[r5] = r9
            java.lang.String r9 = "EWSKY"
            r11[r6] = r9
            java.lang.String r9 = "OWSKI"
            r11[r15] = r9
            java.lang.String r9 = "OWSKY"
            r11[r10] = r9
            r9 = 5
            boolean r4 = contains(r1, r4, r9, r11)
            if (r4 != 0) goto L_0x033c
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r9 = "SCH"
            r4[r5] = r9
            boolean r4 = contains(r1, r5, r10, r4)
            if (r4 == 0) goto L_0x02db
            goto L_0x033c
        L_0x02db:
            java.lang.String[] r4 = new java.lang.String[r15]
            java.lang.String r9 = "WICZ"
            r4[r5] = r9
            java.lang.String r9 = "WITZ"
            r4[r6] = r9
            r9 = 4
            boolean r4 = contains(r1, r7, r9, r4)
            if (r4 == 0) goto L_0x0338
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "TS"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x0305
            java.lang.StringBuilder r4 = r8.primary
            java.lang.String r9 = "TS"
            r4.append(r9)
            goto L_0x0310
        L_0x0305:
            java.lang.StringBuilder r9 = r8.primary
            java.lang.String r10 = "TS"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x0310:
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "FX"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x0329
            java.lang.StringBuilder r4 = r8.alternate
            java.lang.String r9 = "FX"
            r4.append(r9)
            goto L_0x0334
        L_0x0329:
            java.lang.StringBuilder r9 = r8.alternate
            java.lang.String r10 = "FX"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x0334:
            int r7 = r7 + 4
            goto L_0x0092
        L_0x0338:
            int r7 = r7 + 1
            goto L_0x0092
        L_0x033c:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x034d
            java.lang.StringBuilder r4 = r8.alternate
            r9 = 70
            r4.append(r9)
        L_0x034d:
            int r7 = r7 + 1
            goto L_0x0092
        L_0x0351:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0363
            java.lang.StringBuilder r4 = r8.primary
            r9 = 70
            r4.append(r9)
            goto L_0x0365
        L_0x0363:
            r9 = 70
        L_0x0365:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x0374
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x0374:
            int r4 = r7 + 1
            char r9 = r0.charAt(r1, r4)
            r10 = 86
            if (r9 != r10) goto L_0x01fe
            int r7 = r7 + 2
            goto L_0x0092
        L_0x0382:
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r9 = "TION"
            r4[r5] = r9
            r9 = 4
            boolean r4 = contains(r1, r7, r9, r4)
            if (r4 == 0) goto L_0x03b6
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x03a1
            java.lang.StringBuilder r4 = r8.primary
            r9 = 88
            r4.append(r9)
            goto L_0x03a3
        L_0x03a1:
            r9 = 88
        L_0x03a3:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x03b2
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x03b2:
            int r7 = r7 + 3
            goto L_0x0092
        L_0x03b6:
            java.lang.String[] r4 = new java.lang.String[r15]
            java.lang.String r9 = "TIA"
            r4[r5] = r9
            java.lang.String r9 = "TCH"
            r4[r6] = r9
            boolean r4 = contains(r1, r7, r10, r4)
            if (r4 == 0) goto L_0x03ed
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x03d8
            java.lang.StringBuilder r4 = r8.primary
            r9 = 88
            r4.append(r9)
            goto L_0x03da
        L_0x03d8:
            r9 = 88
        L_0x03da:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x03e9
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x03e9:
            int r7 = r7 + 3
            goto L_0x0092
        L_0x03ed:
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r9 = "TH"
            r4[r5] = r9
            boolean r4 = contains(r1, r7, r15, r4)
            if (r4 != 0) goto L_0x043a
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r9 = "TTH"
            r4[r5] = r9
            boolean r4 = contains(r1, r7, r10, r4)
            if (r4 == 0) goto L_0x0406
            goto L_0x043a
        L_0x0406:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0415
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r13)
        L_0x0415:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0424
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r13)
        L_0x0424:
            int r4 = r7 + 1
            java.lang.String[] r9 = new java.lang.String[r15]
            java.lang.String r10 = "T"
            r9[r5] = r10
            java.lang.String r10 = "D"
            r9[r6] = r10
            boolean r9 = contains(r1, r4, r6, r9)
            if (r9 == 0) goto L_0x01fe
            int r4 = r7 + 2
            goto L_0x01fe
        L_0x043a:
            int r7 = r7 + 2
            java.lang.String[] r4 = new java.lang.String[r15]
            java.lang.String r9 = "OM"
            r4[r5] = r9
            java.lang.String r9 = "AM"
            r4[r6] = r9
            boolean r4 = contains(r1, r7, r15, r4)
            if (r4 != 0) goto L_0x048c
            java.lang.String[] r4 = new java.lang.String[r15]
            java.lang.String r9 = "VAN "
            r4[r5] = r9
            java.lang.String r9 = "VON "
            r4[r6] = r9
            r9 = 4
            boolean r4 = contains(r1, r5, r9, r4)
            if (r4 != 0) goto L_0x048c
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r9 = "SCH"
            r4[r5] = r9
            boolean r4 = contains(r1, r5, r10, r4)
            if (r4 == 0) goto L_0x046a
            goto L_0x048c
        L_0x046a:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x047b
            java.lang.StringBuilder r4 = r8.primary
            r9 = 48
            r4.append(r9)
        L_0x047b:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0092
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r13)
            goto L_0x0092
        L_0x048c:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x049b
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r13)
        L_0x049b:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0092
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r13)
            goto L_0x0092
        L_0x04ac:
            int r4 = r7 + -1
            java.lang.String[] r9 = new java.lang.String[r15]
            java.lang.String r12 = "ISL"
            r9[r5] = r12
            java.lang.String r12 = "YSL"
            r9[r6] = r12
            boolean r4 = contains(r1, r4, r10, r9)
            if (r4 == 0) goto L_0x04c2
            int r7 = r7 + 1
            goto L_0x0092
        L_0x04c2:
            if (r7 != 0) goto L_0x04f5
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r9 = "SUGAR"
            r4[r5] = r9
            r9 = 5
            boolean r4 = contains(r1, r7, r9, r4)
            if (r4 == 0) goto L_0x04f5
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x04e2
            java.lang.StringBuilder r4 = r8.primary
            r9 = 88
            r4.append(r9)
        L_0x04e2:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x04f1
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r11)
        L_0x04f1:
            int r7 = r7 + 1
            goto L_0x0092
        L_0x04f5:
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r9 = "SH"
            r4[r5] = r9
            boolean r4 = contains(r1, r7, r15, r4)
            if (r4 == 0) goto L_0x0562
            int r4 = r7 + 1
            r9 = 4
            java.lang.String[] r12 = new java.lang.String[r9]
            java.lang.String r13 = "HEIM"
            r12[r5] = r13
            java.lang.String r13 = "HOEK"
            r12[r6] = r13
            java.lang.String r13 = "HOLM"
            r12[r15] = r13
            java.lang.String r13 = "HOLZ"
            r12[r10] = r13
            boolean r4 = contains(r1, r4, r9, r12)
            if (r4 == 0) goto L_0x053b
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x052b
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r11)
        L_0x052b:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x055e
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r11)
            goto L_0x055e
        L_0x053b:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x054d
            java.lang.StringBuilder r4 = r8.primary
            r9 = 88
            r4.append(r9)
            goto L_0x054f
        L_0x054d:
            r9 = 88
        L_0x054f:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x055e
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x055e:
            int r7 = r7 + 2
            goto L_0x0092
        L_0x0562:
            java.lang.String[] r4 = new java.lang.String[r15]
            java.lang.String r9 = "SIO"
            r4[r5] = r9
            java.lang.String r9 = "SIA"
            r4[r6] = r9
            boolean r4 = contains(r1, r7, r10, r4)
            if (r4 != 0) goto L_0x07f3
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r9 = "SIAN"
            r4[r5] = r9
            r9 = 4
            boolean r4 = contains(r1, r7, r9, r4)
            if (r4 == 0) goto L_0x0581
            goto L_0x07f3
        L_0x0581:
            if (r7 != 0) goto L_0x059d
            int r4 = r7 + 1
            java.lang.String[] r12 = new java.lang.String[r9]
            java.lang.String r9 = "M"
            r12[r5] = r9
            java.lang.String r9 = "N"
            r12[r6] = r9
            java.lang.String r9 = "L"
            r12[r15] = r9
            java.lang.String r9 = "W"
            r12[r10] = r9
            boolean r4 = contains(r1, r4, r6, r12)
            if (r4 != 0) goto L_0x05ab
        L_0x059d:
            int r4 = r7 + 1
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r12 = "Z"
            r9[r5] = r12
            boolean r9 = contains(r1, r4, r6, r9)
            if (r9 == 0) goto L_0x05dd
        L_0x05ab:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x05ba
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r11)
        L_0x05ba:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x05cb
            java.lang.StringBuilder r4 = r8.alternate
            r9 = 88
            r4.append(r9)
        L_0x05cb:
            int r4 = r7 + 1
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r10 = "Z"
            r9[r5] = r10
            boolean r9 = contains(r1, r4, r6, r9)
            if (r9 == 0) goto L_0x01fe
            int r4 = r7 + 2
            goto L_0x01fe
        L_0x05dd:
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r12 = "SC"
            r9[r5] = r12
            boolean r9 = contains(r1, r7, r15, r9)
            if (r9 == 0) goto L_0x0798
            int r4 = r7 + 2
            char r9 = r0.charAt(r1, r4)
            if (r9 != r14) goto L_0x0719
            int r4 = r7 + 3
            r9 = 6
            java.lang.String[] r9 = new java.lang.String[r9]
            java.lang.String r12 = "OO"
            r9[r5] = r12
            java.lang.String r12 = "ER"
            r9[r6] = r12
            java.lang.String r12 = "EN"
            r9[r15] = r12
            java.lang.String r12 = "UY"
            r9[r10] = r12
            java.lang.String r12 = "ED"
            r13 = 4
            r9[r13] = r12
            java.lang.String r12 = "EM"
            r13 = 5
            r9[r13] = r12
            boolean r9 = contains(r1, r4, r15, r9)
            if (r9 == 0) goto L_0x06bc
            java.lang.String[] r9 = new java.lang.String[r15]
            java.lang.String r10 = "ER"
            r9[r5] = r10
            java.lang.String r10 = "EN"
            r9[r6] = r10
            boolean r4 = contains(r1, r4, r15, r9)
            if (r4 == 0) goto L_0x0671
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "X"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x063f
            java.lang.StringBuilder r4 = r8.primary
            java.lang.String r9 = "X"
            r4.append(r9)
            goto L_0x064a
        L_0x063f:
            java.lang.StringBuilder r9 = r8.primary
            java.lang.String r10 = "X"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x064a:
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "SK"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x0664
            java.lang.StringBuilder r4 = r8.alternate
            java.lang.String r9 = "SK"
            r4.append(r9)
            goto L_0x0794
        L_0x0664:
            java.lang.StringBuilder r9 = r8.alternate
            java.lang.String r10 = "SK"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
            goto L_0x0794
        L_0x0671:
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "SK"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x068a
            java.lang.StringBuilder r4 = r8.primary
            java.lang.String r9 = "SK"
            r4.append(r9)
            goto L_0x0695
        L_0x068a:
            java.lang.StringBuilder r9 = r8.primary
            java.lang.String r10 = "SK"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x0695:
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "SK"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x06af
            java.lang.StringBuilder r4 = r8.alternate
            java.lang.String r9 = "SK"
            r4.append(r9)
            goto L_0x0794
        L_0x06af:
            java.lang.StringBuilder r9 = r8.alternate
            java.lang.String r10 = "SK"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
            goto L_0x0794
        L_0x06bc:
            if (r7 != 0) goto L_0x06f4
            java.lang.String r4 = "AEIOUY"
            char r9 = r0.charAt(r1, r10)
            int r4 = r4.indexOf(r9)
            if (r4 != r3) goto L_0x06f4
            char r4 = r0.charAt(r1, r10)
            r9 = 87
            if (r4 == r9) goto L_0x06f4
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x06e3
            java.lang.StringBuilder r4 = r8.primary
            r9 = 88
            r4.append(r9)
        L_0x06e3:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0794
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r11)
            goto L_0x0794
        L_0x06f4:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0706
            java.lang.StringBuilder r4 = r8.primary
            r9 = 88
            r4.append(r9)
            goto L_0x0708
        L_0x0706:
            r9 = 88
        L_0x0708:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x0794
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
            goto L_0x0794
        L_0x0719:
            java.lang.String[] r9 = new java.lang.String[r10]
            java.lang.String r10 = "I"
            r9[r5] = r10
            java.lang.String r10 = "E"
            r9[r6] = r10
            java.lang.String r10 = "Y"
            r9[r15] = r10
            boolean r4 = contains(r1, r4, r6, r9)
            if (r4 == 0) goto L_0x074c
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x073c
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r11)
        L_0x073c:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0794
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r11)
            goto L_0x0794
        L_0x074c:
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "SK"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x0765
            java.lang.StringBuilder r4 = r8.primary
            java.lang.String r9 = "SK"
            r4.append(r9)
            goto L_0x0770
        L_0x0765:
            java.lang.StringBuilder r9 = r8.primary
            java.lang.String r10 = "SK"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x0770:
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "SK"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x0789
            java.lang.StringBuilder r4 = r8.alternate
            java.lang.String r9 = "SK"
            r4.append(r9)
            goto L_0x0794
        L_0x0789:
            java.lang.StringBuilder r9 = r8.alternate
            java.lang.String r10 = "SK"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x0794:
            int r7 = r7 + 3
            goto L_0x0092
        L_0x0798:
            int r9 = r1.length()
            int r9 = r9 - r6
            if (r7 != r9) goto L_0x07c1
            int r9 = r7 + -2
            java.lang.String[] r10 = new java.lang.String[r15]
            java.lang.String r12 = "AI"
            r10[r5] = r12
            java.lang.String r12 = "OI"
            r10[r6] = r12
            boolean r9 = contains(r1, r9, r15, r10)
            if (r9 == 0) goto L_0x07c1
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x07df
            java.lang.StringBuilder r9 = r8.alternate
            r9.append(r11)
            goto L_0x07df
        L_0x07c1:
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x07d0
            java.lang.StringBuilder r9 = r8.primary
            r9.append(r11)
        L_0x07d0:
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x07df
            java.lang.StringBuilder r9 = r8.alternate
            r9.append(r11)
        L_0x07df:
            java.lang.String[] r9 = new java.lang.String[r15]
            java.lang.String r10 = "S"
            r9[r5] = r10
            java.lang.String r10 = "Z"
            r9[r6] = r10
            boolean r9 = contains(r1, r4, r6, r9)
            if (r9 == 0) goto L_0x01fe
            int r4 = r7 + 2
            goto L_0x01fe
        L_0x07f3:
            if (r2 == 0) goto L_0x0814
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0804
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r11)
        L_0x0804:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0834
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r11)
            goto L_0x0834
        L_0x0814:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0823
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r11)
        L_0x0823:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0834
            java.lang.StringBuilder r4 = r8.alternate
            r9 = 88
            r4.append(r9)
        L_0x0834:
            int r7 = r7 + 3
            goto L_0x0092
        L_0x0838:
            int r4 = r1.length()
            int r4 = r4 - r6
            if (r7 != r4) goto L_0x0873
            if (r2 != 0) goto L_0x0873
            int r4 = r7 + -2
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r10 = "IE"
            r9[r5] = r10
            boolean r4 = contains(r1, r4, r15, r9)
            if (r4 == 0) goto L_0x0873
            int r4 = r7 + -4
            java.lang.String[] r9 = new java.lang.String[r15]
            java.lang.String r10 = "ME"
            r9[r5] = r10
            java.lang.String r10 = "MA"
            r9[r6] = r10
            boolean r4 = contains(r1, r4, r15, r9)
            if (r4 != 0) goto L_0x0873
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0895
            java.lang.StringBuilder r4 = r8.alternate
            r9 = 82
            r4.append(r9)
            goto L_0x0895
        L_0x0873:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0884
            java.lang.StringBuilder r4 = r8.primary
            r9 = 82
            r4.append(r9)
        L_0x0884:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0895
            java.lang.StringBuilder r4 = r8.alternate
            r9 = 82
            r4.append(r9)
        L_0x0895:
            int r4 = r7 + 1
            char r9 = r0.charAt(r1, r4)
            r10 = 82
            if (r9 != r10) goto L_0x01fe
            int r7 = r7 + 2
            goto L_0x0092
        L_0x08a3:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x08b5
            java.lang.StringBuilder r4 = r8.primary
            r9 = 75
            r4.append(r9)
            goto L_0x08b7
        L_0x08b5:
            r9 = 75
        L_0x08b7:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x08c6
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x08c6:
            int r4 = r7 + 1
            char r9 = r0.charAt(r1, r4)
            r10 = 81
            if (r9 != r10) goto L_0x01fe
            int r7 = r7 + 2
            goto L_0x0092
        L_0x08d4:
            int r4 = r7 + 1
            char r9 = r0.charAt(r1, r4)
            if (r9 != r14) goto L_0x0903
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x08ee
            java.lang.StringBuilder r4 = r8.primary
            r9 = 70
            r4.append(r9)
            goto L_0x08f0
        L_0x08ee:
            r9 = 70
        L_0x08f0:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x08ff
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x08ff:
            int r7 = r7 + 2
            goto L_0x0092
        L_0x0903:
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0914
            java.lang.StringBuilder r9 = r8.primary
            r10 = 80
            r9.append(r10)
        L_0x0914:
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0925
            java.lang.StringBuilder r9 = r8.alternate
            r10 = 80
            r9.append(r10)
        L_0x0925:
            java.lang.String[] r9 = new java.lang.String[r15]
            java.lang.String r10 = "P"
            r9[r5] = r10
            java.lang.String r10 = "B"
            r9[r6] = r10
            boolean r9 = contains(r1, r4, r6, r9)
            if (r9 == 0) goto L_0x01fe
            int r4 = r7 + 2
            goto L_0x01fe
        L_0x0939:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x094a
            java.lang.StringBuilder r4 = r8.primary
            r9 = 78
            r4.append(r9)
        L_0x094a:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x095b
            java.lang.StringBuilder r4 = r8.alternate
            r9 = 78
            r4.append(r9)
        L_0x095b:
            int r4 = r7 + 1
            char r9 = r0.charAt(r1, r4)
            r10 = 78
            if (r9 != r10) goto L_0x01fe
            int r7 = r7 + 2
            goto L_0x0092
        L_0x0969:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x097a
            java.lang.StringBuilder r4 = r8.primary
            r9 = 77
            r4.append(r9)
        L_0x097a:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x098b
            java.lang.StringBuilder r4 = r8.alternate
            r9 = 77
            r4.append(r9)
        L_0x098b:
            int r4 = r7 + 1
            char r9 = r0.charAt(r1, r4)
            r11 = 77
            if (r9 != r11) goto L_0x0997
        L_0x0995:
            r9 = 1
            goto L_0x09bc
        L_0x0997:
            int r9 = r7 + -1
            java.lang.String[] r11 = new java.lang.String[r6]
            java.lang.String r12 = "UMB"
            r11[r5] = r12
            boolean r9 = contains(r1, r9, r10, r11)
            if (r9 == 0) goto L_0x09bb
            int r9 = r1.length()
            int r9 = r9 - r6
            if (r4 == r9) goto L_0x0995
            int r9 = r7 + 2
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r11 = "ER"
            r10[r5] = r11
            boolean r9 = contains(r1, r9, r15, r10)
            if (r9 == 0) goto L_0x09bb
            goto L_0x0995
        L_0x09bb:
            r9 = 0
        L_0x09bc:
            if (r9 == 0) goto L_0x01fe
            int r7 = r7 + 2
            goto L_0x0092
        L_0x09c2:
            int r4 = r7 + 1
            char r9 = r0.charAt(r1, r4)
            r11 = 76
            if (r9 != r11) goto L_0x0a61
            int r4 = r1.length()
            int r4 = r4 - r10
            if (r7 != r4) goto L_0x09ec
            int r4 = r7 + -1
            java.lang.String[] r9 = new java.lang.String[r10]
            java.lang.String r10 = "ILLO"
            r9[r5] = r10
            java.lang.String r10 = "ILLA"
            r9[r6] = r10
            java.lang.String r10 = "ALLE"
            r9[r15] = r10
            r10 = 4
            boolean r4 = contains(r1, r4, r10, r9)
            if (r4 == 0) goto L_0x09ec
        L_0x09ea:
            r4 = 1
            goto L_0x0a27
        L_0x09ec:
            int r4 = r1.length()
            int r4 = r4 - r15
            java.lang.String[] r9 = new java.lang.String[r15]
            java.lang.String r10 = "AS"
            r9[r5] = r10
            java.lang.String r10 = "OS"
            r9[r6] = r10
            boolean r4 = contains(r1, r4, r15, r9)
            if (r4 != 0) goto L_0x0a16
            int r4 = r1.length()
            int r4 = r4 - r6
            java.lang.String[] r9 = new java.lang.String[r15]
            java.lang.String r10 = "A"
            r9[r5] = r10
            java.lang.String r10 = "O"
            r9[r6] = r10
            boolean r4 = contains(r1, r4, r6, r9)
            if (r4 == 0) goto L_0x0a26
        L_0x0a16:
            int r4 = r7 + -1
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r10 = "ALLE"
            r9[r5] = r10
            r10 = 4
            boolean r4 = contains(r1, r4, r10, r9)
            if (r4 == 0) goto L_0x0a26
            goto L_0x09ea
        L_0x0a26:
            r4 = 0
        L_0x0a27:
            if (r4 == 0) goto L_0x0a3b
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0a5d
            java.lang.StringBuilder r4 = r8.primary
            r9 = 76
            r4.append(r9)
            goto L_0x0a5d
        L_0x0a3b:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0a4c
            java.lang.StringBuilder r4 = r8.primary
            r9 = 76
            r4.append(r9)
        L_0x0a4c:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0a5d
            java.lang.StringBuilder r4 = r8.alternate
            r9 = 76
            r4.append(r9)
        L_0x0a5d:
            int r7 = r7 + 2
            goto L_0x0092
        L_0x0a61:
            java.lang.StringBuilder r7 = r8.primary
            int r7 = r7.length()
            int r9 = r8.maxLength
            if (r7 >= r9) goto L_0x0a72
            java.lang.StringBuilder r7 = r8.primary
            r9 = 76
            r7.append(r9)
        L_0x0a72:
            java.lang.StringBuilder r7 = r8.alternate
            int r7 = r7.length()
            int r9 = r8.maxLength
            if (r7 >= r9) goto L_0x01fe
            java.lang.StringBuilder r7 = r8.alternate
            r9 = 76
            r7.append(r9)
            goto L_0x01fe
        L_0x0a85:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0a97
            java.lang.StringBuilder r4 = r8.primary
            r9 = 75
            r4.append(r9)
            goto L_0x0a99
        L_0x0a97:
            r9 = 75
        L_0x0a99:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x0aa8
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x0aa8:
            int r4 = r7 + 1
            char r10 = r0.charAt(r1, r4)
            if (r10 != r9) goto L_0x01fe
            int r7 = r7 + 2
            goto L_0x0092
        L_0x0ab4:
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r11 = "JOSE"
            r9[r5] = r11
            r11 = 4
            boolean r9 = contains(r1, r7, r11, r9)
            if (r9 != 0) goto L_0x0bac
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r13 = "SAN "
            r9[r5] = r13
            boolean r9 = contains(r1, r5, r11, r9)
            if (r9 == 0) goto L_0x0acf
            goto L_0x0bac
        L_0x0acf:
            if (r7 != 0) goto L_0x0afd
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r13 = "JOSE"
            r9[r5] = r13
            boolean r9 = contains(r1, r7, r11, r9)
            if (r9 != 0) goto L_0x0afd
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0aec
            java.lang.StringBuilder r9 = r8.primary
            r9.append(r4)
        L_0x0aec:
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0ba0
            java.lang.StringBuilder r9 = r8.alternate
            r9.append(r12)
            goto L_0x0ba0
        L_0x0afd:
            java.lang.String r9 = "AEIOUY"
            int r11 = r7 + -1
            char r13 = r0.charAt(r1, r11)
            int r9 = r9.indexOf(r13)
            if (r9 == r3) goto L_0x0b3c
            if (r2 != 0) goto L_0x0b3c
            int r9 = r7 + 1
            char r13 = r0.charAt(r1, r9)
            if (r13 == r12) goto L_0x0b1d
            char r9 = r0.charAt(r1, r9)
            r12 = 79
            if (r9 != r12) goto L_0x0b3c
        L_0x0b1d:
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0b2c
            java.lang.StringBuilder r9 = r8.primary
            r9.append(r4)
        L_0x0b2c:
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0ba0
            java.lang.StringBuilder r9 = r8.alternate
            r9.append(r14)
            goto L_0x0ba0
        L_0x0b3c:
            int r9 = r1.length()
            int r9 = r9 - r6
            if (r7 != r9) goto L_0x0b64
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0b52
            java.lang.StringBuilder r9 = r8.primary
            r9.append(r4)
        L_0x0b52:
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0ba0
            java.lang.StringBuilder r9 = r8.alternate
            r10 = 32
            r9.append(r10)
            goto L_0x0ba0
        L_0x0b64:
            int r9 = r7 + 1
            java.lang.String[] r12 = com.helpshift.support.external.DoubleMetaphone.L_T_K_S_N_M_B_Z
            boolean r9 = contains(r1, r9, r6, r12)
            if (r9 != 0) goto L_0x0ba0
            java.lang.String[] r9 = new java.lang.String[r10]
            java.lang.String r10 = "S"
            r9[r5] = r10
            java.lang.String r10 = "K"
            r9[r6] = r10
            java.lang.String r10 = "L"
            r9[r15] = r10
            boolean r9 = contains(r1, r11, r6, r9)
            if (r9 != 0) goto L_0x0ba0
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0b91
            java.lang.StringBuilder r9 = r8.primary
            r9.append(r4)
        L_0x0b91:
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0ba0
            java.lang.StringBuilder r9 = r8.alternate
            r9.append(r4)
        L_0x0ba0:
            int r9 = r7 + 1
            char r10 = r0.charAt(r1, r9)
            if (r10 != r4) goto L_0x0150
            int r7 = r7 + 2
            goto L_0x0092
        L_0x0bac:
            if (r7 != 0) goto L_0x0bb8
            int r9 = r7 + 4
            char r9 = r0.charAt(r1, r9)
            r10 = 32
            if (r9 == r10) goto L_0x0beb
        L_0x0bb8:
            int r9 = r1.length()
            r10 = 4
            if (r9 == r10) goto L_0x0beb
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r11 = "SAN "
            r9[r5] = r11
            boolean r9 = contains(r1, r5, r10, r9)
            if (r9 == 0) goto L_0x0bcc
            goto L_0x0beb
        L_0x0bcc:
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0bdb
            java.lang.StringBuilder r9 = r8.primary
            r9.append(r4)
        L_0x0bdb:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0c09
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r14)
            goto L_0x0c09
        L_0x0beb:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0bfa
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r14)
        L_0x0bfa:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0c09
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r14)
        L_0x0c09:
            int r7 = r7 + 1
            goto L_0x0092
        L_0x0c0d:
            if (r7 == 0) goto L_0x0c1d
            java.lang.String r4 = "AEIOUY"
            int r9 = r7 + -1
            char r9 = r0.charAt(r1, r9)
            int r4 = r4.indexOf(r9)
            if (r4 == r3) goto L_0x0c4d
        L_0x0c1d:
            java.lang.String r4 = "AEIOUY"
            int r9 = r7 + 1
            char r9 = r0.charAt(r1, r9)
            int r4 = r4.indexOf(r9)
            if (r4 == r3) goto L_0x0c4d
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0c3a
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r14)
        L_0x0c3a:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0c49
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r14)
        L_0x0c49:
            int r7 = r7 + 2
            goto L_0x0092
        L_0x0c4d:
            int r7 = r7 + 1
            goto L_0x0092
        L_0x0c51:
            int r9 = r7 + 1
            char r11 = r0.charAt(r1, r9)
            if (r11 != r14) goto L_0x0dac
            if (r7 <= 0) goto L_0x0c90
            java.lang.String r9 = "AEIOUY"
            int r11 = r7 + -1
            char r11 = r0.charAt(r1, r11)
            int r9 = r9.indexOf(r11)
            if (r9 != r3) goto L_0x0c90
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0c7b
            java.lang.StringBuilder r4 = r8.primary
            r9 = 75
            r4.append(r9)
            goto L_0x0c7d
        L_0x0c7b:
            r9 = 75
        L_0x0c7d:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x0c8c
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x0c8c:
            int r7 = r7 + 2
            goto L_0x0092
        L_0x0c90:
            if (r7 != 0) goto L_0x0ce1
            int r7 = r7 + 2
            char r9 = r0.charAt(r1, r7)
            r10 = 73
            if (r9 != r10) goto L_0x0cbc
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0cab
            java.lang.StringBuilder r9 = r8.primary
            r9.append(r4)
        L_0x0cab:
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0092
            java.lang.StringBuilder r9 = r8.alternate
            r9.append(r4)
            goto L_0x0092
        L_0x0cbc:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0cce
            java.lang.StringBuilder r4 = r8.primary
            r9 = 75
            r4.append(r9)
            goto L_0x0cd0
        L_0x0cce:
            r9 = 75
        L_0x0cd0:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x0092
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
            goto L_0x0092
        L_0x0ce1:
            if (r7 <= r6) goto L_0x0cf9
            int r4 = r7 + -2
            java.lang.String[] r9 = new java.lang.String[r10]
            java.lang.String r11 = "B"
            r9[r5] = r11
            java.lang.String r11 = "H"
            r9[r6] = r11
            java.lang.String r11 = "D"
            r9[r15] = r11
            boolean r4 = contains(r1, r4, r6, r9)
            if (r4 != 0) goto L_0x0d25
        L_0x0cf9:
            if (r7 <= r15) goto L_0x0d11
            int r4 = r7 + -3
            java.lang.String[] r9 = new java.lang.String[r10]
            java.lang.String r11 = "B"
            r9[r5] = r11
            java.lang.String r11 = "H"
            r9[r6] = r11
            java.lang.String r11 = "D"
            r9[r15] = r11
            boolean r4 = contains(r1, r4, r6, r9)
            if (r4 != 0) goto L_0x0d25
        L_0x0d11:
            if (r7 <= r10) goto L_0x0d29
            int r4 = r7 + -4
            java.lang.String[] r9 = new java.lang.String[r15]
            java.lang.String r11 = "B"
            r9[r5] = r11
            java.lang.String r11 = "H"
            r9[r6] = r11
            boolean r4 = contains(r1, r4, r6, r9)
            if (r4 == 0) goto L_0x0d29
        L_0x0d25:
            int r7 = r7 + 2
            goto L_0x0092
        L_0x0d29:
            if (r7 <= r15) goto L_0x0d79
            int r4 = r7 + -1
            char r4 = r0.charAt(r1, r4)
            r9 = 85
            if (r4 != r9) goto L_0x0d79
            int r4 = r7 + -3
            r9 = 5
            java.lang.String[] r9 = new java.lang.String[r9]
            java.lang.String r11 = "C"
            r9[r5] = r11
            java.lang.String r11 = "G"
            r9[r6] = r11
            java.lang.String r11 = "L"
            r9[r15] = r11
            java.lang.String r11 = "R"
            r9[r10] = r11
            java.lang.String r10 = "T"
            r11 = 4
            r9[r11] = r10
            boolean r4 = contains(r1, r4, r6, r9)
            if (r4 == 0) goto L_0x0d79
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0d67
            java.lang.StringBuilder r4 = r8.primary
            r9 = 70
            r4.append(r9)
            goto L_0x0d69
        L_0x0d67:
            r9 = 70
        L_0x0d69:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x0da8
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
            goto L_0x0da8
        L_0x0d79:
            if (r7 <= 0) goto L_0x0da8
            int r4 = r7 + -1
            char r4 = r0.charAt(r1, r4)
            r9 = 73
            if (r4 == r9) goto L_0x0da8
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0d97
            java.lang.StringBuilder r4 = r8.primary
            r9 = 75
            r4.append(r9)
            goto L_0x0d99
        L_0x0d97:
            r9 = 75
        L_0x0d99:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x0da8
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x0da8:
            int r7 = r7 + 2
            goto L_0x0092
        L_0x0dac:
            char r11 = r0.charAt(r1, r9)
            r12 = 78
            if (r11 != r12) goto L_0x0ebc
            if (r7 != r6) goto L_0x0e0f
            java.lang.String r4 = "AEIOUY"
            char r10 = r0.charAt(r1, r5)
            int r4 = r4.indexOf(r10)
            if (r4 == r3) goto L_0x0e0f
            if (r2 != 0) goto L_0x0e0f
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "KN"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x0ddd
            java.lang.StringBuilder r4 = r8.primary
            java.lang.String r9 = "KN"
            r4.append(r9)
            goto L_0x0de8
        L_0x0ddd:
            java.lang.StringBuilder r9 = r8.primary
            java.lang.String r10 = "KN"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x0de8:
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "N"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x0e02
            java.lang.StringBuilder r4 = r8.alternate
            java.lang.String r9 = "N"
            r4.append(r9)
            goto L_0x0eb8
        L_0x0e02:
            java.lang.StringBuilder r9 = r8.alternate
            java.lang.String r10 = "N"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
            goto L_0x0eb8
        L_0x0e0f:
            int r4 = r7 + 2
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r11 = "EY"
            r10[r5] = r11
            boolean r4 = contains(r1, r4, r15, r10)
            if (r4 != 0) goto L_0x0e70
            char r4 = r0.charAt(r1, r9)
            r9 = 89
            if (r4 == r9) goto L_0x0e70
            if (r2 != 0) goto L_0x0e70
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "N"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x0e40
            java.lang.StringBuilder r4 = r8.primary
            java.lang.String r9 = "N"
            r4.append(r9)
            goto L_0x0e4b
        L_0x0e40:
            java.lang.StringBuilder r9 = r8.primary
            java.lang.String r10 = "N"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x0e4b:
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "KN"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x0e64
            java.lang.StringBuilder r4 = r8.alternate
            java.lang.String r9 = "KN"
            r4.append(r9)
            goto L_0x0eb8
        L_0x0e64:
            java.lang.StringBuilder r9 = r8.alternate
            java.lang.String r10 = "KN"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
            goto L_0x0eb8
        L_0x0e70:
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "KN"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x0e89
            java.lang.StringBuilder r4 = r8.primary
            java.lang.String r9 = "KN"
            r4.append(r9)
            goto L_0x0e94
        L_0x0e89:
            java.lang.StringBuilder r9 = r8.primary
            java.lang.String r10 = "KN"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x0e94:
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "KN"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x0ead
            java.lang.StringBuilder r4 = r8.alternate
            java.lang.String r9 = "KN"
            r4.append(r9)
            goto L_0x0eb8
        L_0x0ead:
            java.lang.StringBuilder r9 = r8.alternate
            java.lang.String r10 = "KN"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x0eb8:
            int r7 = r7 + 2
            goto L_0x0092
        L_0x0ebc:
            java.lang.String[] r11 = new java.lang.String[r6]
            java.lang.String r12 = "LI"
            r11[r5] = r12
            boolean r11 = contains(r1, r9, r15, r11)
            if (r11 == 0) goto L_0x0f16
            if (r2 != 0) goto L_0x0f16
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "KL"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x0ee3
            java.lang.StringBuilder r4 = r8.primary
            java.lang.String r9 = "KL"
            r4.append(r9)
            goto L_0x0eee
        L_0x0ee3:
            java.lang.StringBuilder r9 = r8.primary
            java.lang.String r10 = "KL"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x0eee:
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "L"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x0f07
            java.lang.StringBuilder r4 = r8.alternate
            java.lang.String r9 = "L"
            r4.append(r9)
            goto L_0x0f12
        L_0x0f07:
            java.lang.StringBuilder r9 = r8.alternate
            java.lang.String r10 = "L"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x0f12:
            int r7 = r7 + 2
            goto L_0x0092
        L_0x0f16:
            if (r7 != 0) goto L_0x0f4c
            char r11 = r0.charAt(r1, r9)
            r12 = 89
            if (r11 == r12) goto L_0x0f28
            java.lang.String[] r11 = com.helpshift.support.external.DoubleMetaphone.ES_EP_EB_EL_EY_IB_IL_IN_IE_EI_ER
            boolean r11 = contains(r1, r9, r15, r11)
            if (r11 == 0) goto L_0x0f4c
        L_0x0f28:
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0f39
            java.lang.StringBuilder r9 = r8.primary
            r10 = 75
            r9.append(r10)
        L_0x0f39:
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0f48
            java.lang.StringBuilder r9 = r8.alternate
            r9.append(r4)
        L_0x0f48:
            int r7 = r7 + 2
            goto L_0x0092
        L_0x0f4c:
            java.lang.String[] r11 = new java.lang.String[r6]
            java.lang.String r12 = "ER"
            r11[r5] = r12
            boolean r11 = contains(r1, r9, r15, r11)
            if (r11 != 0) goto L_0x0f60
            char r11 = r0.charAt(r1, r9)
            r12 = 89
            if (r11 != r12) goto L_0x0fbb
        L_0x0f60:
            r11 = 6
            java.lang.String[] r12 = new java.lang.String[r10]
            java.lang.String r13 = "DANGER"
            r12[r5] = r13
            java.lang.String r13 = "RANGER"
            r12[r6] = r13
            java.lang.String r13 = "MANGER"
            r12[r15] = r13
            boolean r11 = contains(r1, r5, r11, r12)
            if (r11 != 0) goto L_0x0fbb
            int r11 = r7 + -1
            java.lang.String[] r12 = new java.lang.String[r15]
            java.lang.String r13 = "E"
            r12[r5] = r13
            java.lang.String r13 = "I"
            r12[r6] = r13
            boolean r12 = contains(r1, r11, r6, r12)
            if (r12 != 0) goto L_0x0fbb
            java.lang.String[] r12 = new java.lang.String[r15]
            java.lang.String r13 = "RGY"
            r12[r5] = r13
            java.lang.String r13 = "OGY"
            r12[r6] = r13
            boolean r11 = contains(r1, r11, r10, r12)
            if (r11 != 0) goto L_0x0fbb
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0fa8
            java.lang.StringBuilder r9 = r8.primary
            r10 = 75
            r9.append(r10)
        L_0x0fa8:
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x0fb7
            java.lang.StringBuilder r9 = r8.alternate
            r9.append(r4)
        L_0x0fb7:
            int r7 = r7 + 2
            goto L_0x0092
        L_0x0fbb:
            java.lang.String[] r11 = new java.lang.String[r10]
            java.lang.String r12 = "E"
            r11[r5] = r12
            java.lang.String r12 = "I"
            r11[r6] = r12
            java.lang.String r12 = "Y"
            r11[r15] = r12
            boolean r11 = contains(r1, r9, r6, r11)
            if (r11 != 0) goto L_0x1034
            int r11 = r7 + -1
            java.lang.String[] r12 = new java.lang.String[r15]
            java.lang.String r13 = "AGGI"
            r12[r5] = r13
            java.lang.String r13 = "OGGI"
            r12[r6] = r13
            r13 = 4
            boolean r11 = contains(r1, r11, r13, r12)
            if (r11 == 0) goto L_0x0fe3
            goto L_0x1034
        L_0x0fe3:
            char r4 = r0.charAt(r1, r9)
            r10 = 71
            if (r4 != r10) goto L_0x1012
            int r7 = r7 + 2
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0fff
            java.lang.StringBuilder r4 = r8.primary
            r10 = 75
            r4.append(r10)
            goto L_0x1001
        L_0x0fff:
            r10 = 75
        L_0x1001:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x0092
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r10)
            goto L_0x0092
        L_0x1012:
            r10 = 75
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r7 = r8.maxLength
            if (r4 >= r7) goto L_0x1023
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r10)
        L_0x1023:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r7 = r8.maxLength
            if (r4 >= r7) goto L_0x0150
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r10)
            goto L_0x0150
        L_0x1034:
            java.lang.String[] r11 = new java.lang.String[r15]
            java.lang.String r12 = "VAN "
            r11[r5] = r12
            java.lang.String r12 = "VON "
            r11[r6] = r12
            r12 = 4
            boolean r11 = contains(r1, r5, r12, r11)
            if (r11 != 0) goto L_0x10aa
            java.lang.String[] r11 = new java.lang.String[r6]
            java.lang.String r12 = "SCH"
            r11[r5] = r12
            boolean r11 = contains(r1, r5, r10, r11)
            if (r11 != 0) goto L_0x10aa
            java.lang.String[] r11 = new java.lang.String[r6]
            java.lang.String r12 = "ET"
            r11[r5] = r12
            boolean r11 = contains(r1, r9, r15, r11)
            if (r11 == 0) goto L_0x105e
            goto L_0x10aa
        L_0x105e:
            java.lang.String[] r11 = new java.lang.String[r6]
            java.lang.String r12 = "IER"
            r11[r5] = r12
            boolean r9 = contains(r1, r9, r10, r11)
            if (r9 == 0) goto L_0x1089
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x1079
            java.lang.StringBuilder r9 = r8.primary
            r9.append(r4)
        L_0x1079:
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x10ca
            java.lang.StringBuilder r9 = r8.alternate
            r9.append(r4)
            goto L_0x10ca
        L_0x1089:
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x1098
            java.lang.StringBuilder r9 = r8.primary
            r9.append(r4)
        L_0x1098:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x10ca
            java.lang.StringBuilder r4 = r8.alternate
            r9 = 75
            r4.append(r9)
            goto L_0x10ca
        L_0x10aa:
            r9 = 75
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x10bb
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r9)
        L_0x10bb:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x10ca
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x10ca:
            int r7 = r7 + 2
            goto L_0x0092
        L_0x10ce:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x10e0
            java.lang.StringBuilder r4 = r8.primary
            r9 = 70
            r4.append(r9)
            goto L_0x10e2
        L_0x10e0:
            r9 = 70
        L_0x10e2:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x10f1
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x10f1:
            int r4 = r7 + 1
            char r10 = r0.charAt(r1, r4)
            if (r10 != r9) goto L_0x01fe
            int r7 = r7 + 2
            goto L_0x0092
        L_0x10fd:
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r11 = "DG"
            r9[r5] = r11
            boolean r9 = contains(r1, r7, r15, r9)
            if (r9 == 0) goto L_0x118c
            int r9 = r7 + 2
            java.lang.String[] r10 = new java.lang.String[r10]
            java.lang.String r11 = "I"
            r10[r5] = r11
            java.lang.String r11 = "E"
            r10[r6] = r11
            java.lang.String r11 = "Y"
            r10[r15] = r11
            boolean r10 = contains(r1, r9, r6, r10)
            if (r10 == 0) goto L_0x1141
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x112e
            java.lang.StringBuilder r9 = r8.primary
            r9.append(r4)
        L_0x112e:
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r10 = r8.maxLength
            if (r9 >= r10) goto L_0x113d
            java.lang.StringBuilder r9 = r8.alternate
            r9.append(r4)
        L_0x113d:
            int r7 = r7 + 3
            goto L_0x0092
        L_0x1141:
            int r4 = r8.maxLength
            java.lang.StringBuilder r7 = r8.primary
            int r7 = r7.length()
            int r4 = r4 - r7
            java.lang.String r7 = "TK"
            int r7 = r7.length()
            if (r7 > r4) goto L_0x115a
            java.lang.StringBuilder r4 = r8.primary
            java.lang.String r7 = "TK"
            r4.append(r7)
            goto L_0x1165
        L_0x115a:
            java.lang.StringBuilder r7 = r8.primary
            java.lang.String r10 = "TK"
            java.lang.String r4 = r10.substring(r5, r4)
            r7.append(r4)
        L_0x1165:
            int r4 = r8.maxLength
            java.lang.StringBuilder r7 = r8.alternate
            int r7 = r7.length()
            int r4 = r4 - r7
            java.lang.String r7 = "TK"
            int r7 = r7.length()
            if (r7 > r4) goto L_0x117f
            java.lang.StringBuilder r4 = r8.alternate
            java.lang.String r7 = "TK"
            r4.append(r7)
            goto L_0x0150
        L_0x117f:
            java.lang.StringBuilder r7 = r8.alternate
            java.lang.String r10 = "TK"
            java.lang.String r4 = r10.substring(r5, r4)
            r7.append(r4)
            goto L_0x0150
        L_0x118c:
            java.lang.String[] r4 = new java.lang.String[r15]
            java.lang.String r9 = "DT"
            r4[r5] = r9
            java.lang.String r9 = "DD"
            r4[r6] = r9
            boolean r4 = contains(r1, r7, r15, r4)
            if (r4 == 0) goto L_0x11be
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x11ab
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r13)
        L_0x11ab:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x11ba
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r13)
        L_0x11ba:
            int r7 = r7 + 2
            goto L_0x0092
        L_0x11be:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x11cd
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r13)
        L_0x11cd:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x11dc
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r13)
        L_0x11dc:
            int r7 = r7 + 1
            goto L_0x0092
        L_0x11e0:
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r9 = "CHIA"
            r4[r5] = r9
            r9 = 4
            boolean r4 = contains(r1, r7, r9, r4)
            if (r4 == 0) goto L_0x11ef
        L_0x11ed:
            r4 = 1
            goto L_0x1231
        L_0x11ef:
            if (r7 > r6) goto L_0x11f3
        L_0x11f1:
            r4 = 0
            goto L_0x1231
        L_0x11f3:
            java.lang.String r4 = "AEIOUY"
            int r9 = r7 + -2
            char r13 = r0.charAt(r1, r9)
            int r4 = r4.indexOf(r13)
            if (r4 == r3) goto L_0x1202
            goto L_0x11f1
        L_0x1202:
            int r4 = r7 + -1
            java.lang.String[] r13 = new java.lang.String[r6]
            java.lang.String r14 = "ACH"
            r13[r5] = r14
            boolean r4 = contains(r1, r4, r10, r13)
            if (r4 != 0) goto L_0x1211
            goto L_0x11f1
        L_0x1211:
            int r4 = r7 + 2
            char r4 = r0.charAt(r1, r4)
            r13 = 73
            if (r4 == r13) goto L_0x121f
            r13 = 69
            if (r4 != r13) goto L_0x11ed
        L_0x121f:
            r4 = 6
            java.lang.String[] r13 = new java.lang.String[r15]
            java.lang.String r14 = "BACHER"
            r13[r5] = r14
            java.lang.String r14 = "MACHER"
            r13[r6] = r14
            boolean r4 = contains(r1, r9, r4, r13)
            if (r4 == 0) goto L_0x11f1
            goto L_0x11ed
        L_0x1231:
            if (r4 == 0) goto L_0x125c
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x1245
            java.lang.StringBuilder r4 = r8.primary
            r9 = 75
            r4.append(r9)
            goto L_0x1247
        L_0x1245:
            r9 = 75
        L_0x1247:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x1256
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x1256:
            int r7 = r7 + 2
        L_0x1258:
            r13 = 75
            goto L_0x0092
        L_0x125c:
            if (r7 != 0) goto L_0x128c
            r4 = 6
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r13 = "CAESAR"
            r9[r5] = r13
            boolean r4 = contains(r1, r7, r4, r9)
            if (r4 == 0) goto L_0x128c
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x127a
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r11)
        L_0x127a:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x1289
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r11)
        L_0x1289:
            int r7 = r7 + 2
            goto L_0x1258
        L_0x128c:
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r9 = "CH"
            r4[r5] = r9
            boolean r4 = contains(r1, r7, r15, r4)
            if (r4 == 0) goto L_0x144f
            if (r7 == 0) goto L_0x129c
        L_0x129a:
            r4 = 0
            goto L_0x12d8
        L_0x129c:
            int r4 = r7 + 1
            java.lang.String[] r9 = new java.lang.String[r15]
            java.lang.String r11 = "HARAC"
            r9[r5] = r11
            java.lang.String r11 = "HARIS"
            r9[r6] = r11
            r11 = 5
            boolean r9 = contains(r1, r4, r11, r9)
            if (r9 != 0) goto L_0x12c9
            r9 = 4
            java.lang.String[] r11 = new java.lang.String[r9]
            java.lang.String r9 = "HOR"
            r11[r5] = r9
            java.lang.String r9 = "HYM"
            r11[r6] = r9
            java.lang.String r9 = "HIA"
            r11[r15] = r9
            java.lang.String r9 = "HEM"
            r11[r10] = r9
            boolean r4 = contains(r1, r4, r10, r11)
            if (r4 != 0) goto L_0x12c9
            goto L_0x129a
        L_0x12c9:
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r9 = "CHORE"
            r4[r5] = r9
            r9 = 5
            boolean r4 = contains(r1, r5, r9, r4)
            if (r4 == 0) goto L_0x12d7
            goto L_0x129a
        L_0x12d7:
            r4 = 1
        L_0x12d8:
            if (r7 <= 0) goto L_0x130d
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r11 = "CHAE"
            r9[r5] = r11
            r11 = 4
            boolean r9 = contains(r1, r7, r11, r9)
            if (r9 == 0) goto L_0x130d
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x12f8
            java.lang.StringBuilder r4 = r8.primary
            r9 = 75
            r4.append(r9)
        L_0x12f8:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x1309
            java.lang.StringBuilder r4 = r8.alternate
            r9 = 88
            r4.append(r9)
        L_0x1309:
            int r7 = r7 + 2
            goto L_0x1258
        L_0x130d:
            if (r4 == 0) goto L_0x1336
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x1321
            java.lang.StringBuilder r4 = r8.primary
            r9 = 75
            r4.append(r9)
            goto L_0x1323
        L_0x1321:
            r9 = 75
        L_0x1323:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x1332
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x1332:
            int r7 = r7 + 2
            goto L_0x1258
        L_0x1336:
            java.lang.String[] r4 = new java.lang.String[r15]
            java.lang.String r9 = "VAN "
            r4[r5] = r9
            java.lang.String r9 = "VON "
            r4[r6] = r9
            r9 = 4
            boolean r4 = contains(r1, r5, r9, r4)
            if (r4 != 0) goto L_0x1428
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r9 = "SCH"
            r4[r5] = r9
            boolean r4 = contains(r1, r5, r10, r4)
            if (r4 != 0) goto L_0x1428
            int r4 = r7 + -2
            r9 = 6
            java.lang.String[] r11 = new java.lang.String[r10]
            java.lang.String r12 = "ORCHES"
            r11[r5] = r12
            java.lang.String r12 = "ARCHIT"
            r11[r6] = r12
            java.lang.String r12 = "ORCHID"
            r11[r15] = r12
            boolean r4 = contains(r1, r4, r9, r11)
            if (r4 != 0) goto L_0x1428
            int r4 = r7 + 2
            java.lang.String[] r9 = new java.lang.String[r15]
            java.lang.String r11 = "T"
            r9[r5] = r11
            java.lang.String r11 = "S"
            r9[r6] = r11
            boolean r9 = contains(r1, r4, r6, r9)
            if (r9 != 0) goto L_0x1428
            int r9 = r7 + -1
            r11 = 4
            java.lang.String[] r11 = new java.lang.String[r11]
            java.lang.String r12 = "A"
            r11[r5] = r12
            java.lang.String r12 = "O"
            r11[r6] = r12
            java.lang.String r12 = "U"
            r11[r15] = r12
            java.lang.String r12 = "E"
            r11[r10] = r12
            boolean r9 = contains(r1, r9, r6, r11)
            if (r9 != 0) goto L_0x1399
            if (r7 != 0) goto L_0x13ac
        L_0x1399:
            java.lang.String[] r9 = com.helpshift.support.external.DoubleMetaphone.L_R_N_M_B_H_F_V_W_SPACE
            boolean r9 = contains(r1, r4, r6, r9)
            if (r9 != 0) goto L_0x1428
            int r9 = r7 + 1
            int r10 = r1.length()
            int r10 = r10 - r6
            if (r9 != r10) goto L_0x13ac
            goto L_0x1428
        L_0x13ac:
            if (r7 <= 0) goto L_0x1403
            java.lang.String[] r7 = new java.lang.String[r6]
            java.lang.String r9 = "MC"
            r7[r5] = r9
            boolean r7 = contains(r1, r5, r15, r7)
            if (r7 == 0) goto L_0x13df
            java.lang.StringBuilder r7 = r8.primary
            int r7 = r7.length()
            int r9 = r8.maxLength
            if (r7 >= r9) goto L_0x13cc
            java.lang.StringBuilder r7 = r8.primary
            r9 = 75
            r7.append(r9)
            goto L_0x13ce
        L_0x13cc:
            r9 = 75
        L_0x13ce:
            java.lang.StringBuilder r7 = r8.alternate
            int r7 = r7.length()
            int r10 = r8.maxLength
            if (r7 >= r10) goto L_0x15aa
            java.lang.StringBuilder r7 = r8.alternate
            r7.append(r9)
            goto L_0x15aa
        L_0x13df:
            java.lang.StringBuilder r7 = r8.primary
            int r7 = r7.length()
            int r9 = r8.maxLength
            if (r7 >= r9) goto L_0x13f0
            java.lang.StringBuilder r7 = r8.primary
            r9 = 88
            r7.append(r9)
        L_0x13f0:
            java.lang.StringBuilder r7 = r8.alternate
            int r7 = r7.length()
            int r9 = r8.maxLength
            if (r7 >= r9) goto L_0x15aa
            java.lang.StringBuilder r7 = r8.alternate
            r9 = 75
            r7.append(r9)
            goto L_0x15aa
        L_0x1403:
            java.lang.StringBuilder r7 = r8.primary
            int r7 = r7.length()
            int r9 = r8.maxLength
            if (r7 >= r9) goto L_0x1415
            java.lang.StringBuilder r7 = r8.primary
            r9 = 88
            r7.append(r9)
            goto L_0x1417
        L_0x1415:
            r9 = 88
        L_0x1417:
            java.lang.StringBuilder r7 = r8.alternate
            int r7 = r7.length()
            int r10 = r8.maxLength
            if (r7 >= r10) goto L_0x15aa
            java.lang.StringBuilder r7 = r8.alternate
            r7.append(r9)
            goto L_0x15aa
        L_0x1428:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x143a
            java.lang.StringBuilder r4 = r8.primary
            r9 = 75
            r4.append(r9)
            goto L_0x143c
        L_0x143a:
            r9 = 75
        L_0x143c:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x144b
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x144b:
            int r7 = r7 + 2
            goto L_0x1258
        L_0x144f:
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r9 = "CZ"
            r4[r5] = r9
            boolean r4 = contains(r1, r7, r15, r4)
            if (r4 == 0) goto L_0x148e
            int r4 = r7 + -2
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r13 = "WICZ"
            r9[r5] = r13
            r13 = 4
            boolean r4 = contains(r1, r4, r13, r9)
            if (r4 != 0) goto L_0x148e
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x1479
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r11)
        L_0x1479:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x148a
            java.lang.StringBuilder r4 = r8.alternate
            r9 = 88
            r4.append(r9)
        L_0x148a:
            int r7 = r7 + 2
            goto L_0x1258
        L_0x148e:
            int r4 = r7 + 1
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r13 = "CIA"
            r9[r5] = r13
            boolean r9 = contains(r1, r4, r10, r9)
            if (r9 == 0) goto L_0x14c3
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x14ae
            java.lang.StringBuilder r4 = r8.primary
            r9 = 88
            r4.append(r9)
            goto L_0x14b0
        L_0x14ae:
            r9 = 88
        L_0x14b0:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x14bf
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x14bf:
            int r7 = r7 + 3
            goto L_0x1258
        L_0x14c3:
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r13 = "CC"
            r9[r5] = r13
            boolean r9 = contains(r1, r7, r15, r9)
            if (r9 == 0) goto L_0x15ad
            if (r7 != r6) goto L_0x14d9
            char r9 = r0.charAt(r1, r5)
            r13 = 77
            if (r9 == r13) goto L_0x15ad
        L_0x14d9:
            int r4 = r7 + 2
            java.lang.String[] r9 = new java.lang.String[r10]
            java.lang.String r10 = "I"
            r9[r5] = r10
            java.lang.String r10 = "E"
            r9[r6] = r10
            java.lang.String r10 = "H"
            r9[r15] = r10
            boolean r9 = contains(r1, r4, r6, r9)
            if (r9 == 0) goto L_0x1587
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r10 = "HU"
            r9[r5] = r10
            boolean r9 = contains(r1, r4, r15, r9)
            if (r9 != 0) goto L_0x1587
            if (r7 != r6) goto L_0x1505
            int r4 = r7 + -1
            char r4 = r0.charAt(r1, r4)
            if (r4 == r12) goto L_0x1518
        L_0x1505:
            int r4 = r7 + -1
            java.lang.String[] r9 = new java.lang.String[r15]
            java.lang.String r10 = "UCCEE"
            r9[r5] = r10
            java.lang.String r10 = "UCCES"
            r9[r6] = r10
            r10 = 5
            boolean r4 = contains(r1, r4, r10, r9)
            if (r4 == 0) goto L_0x1561
        L_0x1518:
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "KS"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x1531
            java.lang.StringBuilder r4 = r8.primary
            java.lang.String r9 = "KS"
            r4.append(r9)
            goto L_0x153c
        L_0x1531:
            java.lang.StringBuilder r9 = r8.primary
            java.lang.String r10 = "KS"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x153c:
            int r4 = r8.maxLength
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r4 = r4 - r9
            java.lang.String r9 = "KS"
            int r9 = r9.length()
            if (r9 > r4) goto L_0x1555
            java.lang.StringBuilder r4 = r8.alternate
            java.lang.String r9 = "KS"
            r4.append(r9)
            goto L_0x1584
        L_0x1555:
            java.lang.StringBuilder r9 = r8.alternate
            java.lang.String r10 = "KS"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
            goto L_0x1584
        L_0x1561:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x1573
            java.lang.StringBuilder r4 = r8.primary
            r9 = 88
            r4.append(r9)
            goto L_0x1575
        L_0x1573:
            r9 = 88
        L_0x1575:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x1584
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x1584:
            int r4 = r7 + 3
            goto L_0x15aa
        L_0x1587:
            java.lang.StringBuilder r7 = r8.primary
            int r7 = r7.length()
            int r9 = r8.maxLength
            if (r7 >= r9) goto L_0x1599
            java.lang.StringBuilder r7 = r8.primary
            r9 = 75
            r7.append(r9)
            goto L_0x159b
        L_0x1599:
            r9 = 75
        L_0x159b:
            java.lang.StringBuilder r7 = r8.alternate
            int r7 = r7.length()
            int r10 = r8.maxLength
            if (r7 >= r10) goto L_0x15aa
            java.lang.StringBuilder r7 = r8.alternate
            r7.append(r9)
        L_0x15aa:
            r7 = r4
            goto L_0x1258
        L_0x15ad:
            java.lang.String[] r9 = new java.lang.String[r10]
            java.lang.String r12 = "CK"
            r9[r5] = r12
            java.lang.String r12 = "CG"
            r9[r6] = r12
            java.lang.String r12 = "CQ"
            r9[r15] = r12
            boolean r9 = contains(r1, r7, r15, r9)
            if (r9 == 0) goto L_0x15e8
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x15d3
            java.lang.StringBuilder r4 = r8.primary
            r9 = 75
            r4.append(r9)
            goto L_0x15d5
        L_0x15d3:
            r9 = 75
        L_0x15d5:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r10 = r8.maxLength
            if (r4 >= r10) goto L_0x15e4
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r9)
        L_0x15e4:
            int r7 = r7 + 2
            goto L_0x1258
        L_0x15e8:
            java.lang.String[] r9 = new java.lang.String[r10]
            java.lang.String r12 = "CI"
            r9[r5] = r12
            java.lang.String r12 = "CE"
            r9[r6] = r12
            java.lang.String r12 = "CY"
            r9[r15] = r12
            boolean r9 = contains(r1, r7, r15, r9)
            if (r9 == 0) goto L_0x1653
            java.lang.String[] r4 = new java.lang.String[r10]
            java.lang.String r9 = "CIO"
            r4[r5] = r9
            java.lang.String r9 = "CIE"
            r4[r6] = r9
            java.lang.String r9 = "CIA"
            r4[r15] = r9
            boolean r4 = contains(r1, r7, r10, r4)
            if (r4 == 0) goto L_0x1631
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x161f
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r11)
        L_0x161f:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x164f
            java.lang.StringBuilder r4 = r8.alternate
            r9 = 88
            r4.append(r9)
            goto L_0x164f
        L_0x1631:
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x1640
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r11)
        L_0x1640:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x164f
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r11)
        L_0x164f:
            int r7 = r7 + 2
            goto L_0x1258
        L_0x1653:
            java.lang.StringBuilder r9 = r8.primary
            int r9 = r9.length()
            int r11 = r8.maxLength
            if (r9 >= r11) goto L_0x1665
            java.lang.StringBuilder r9 = r8.primary
            r13 = 75
            r9.append(r13)
            goto L_0x1667
        L_0x1665:
            r13 = 75
        L_0x1667:
            java.lang.StringBuilder r9 = r8.alternate
            int r9 = r9.length()
            int r11 = r8.maxLength
            if (r9 >= r11) goto L_0x1676
            java.lang.StringBuilder r9 = r8.alternate
            r9.append(r13)
        L_0x1676:
            java.lang.String[] r9 = new java.lang.String[r10]
            java.lang.String r11 = " C"
            r9[r5] = r11
            java.lang.String r11 = " Q"
            r9[r6] = r11
            java.lang.String r11 = " G"
            r9[r15] = r11
            boolean r9 = contains(r1, r4, r15, r9)
            if (r9 == 0) goto L_0x168e
            int r4 = r7 + 3
            goto L_0x01fe
        L_0x168e:
            java.lang.String[] r9 = new java.lang.String[r10]
            java.lang.String r10 = "C"
            r9[r5] = r10
            java.lang.String r10 = "K"
            r9[r6] = r10
            java.lang.String r10 = "Q"
            r9[r15] = r10
            boolean r9 = contains(r1, r4, r6, r9)
            if (r9 == 0) goto L_0x01fe
            java.lang.String[] r9 = new java.lang.String[r15]
            java.lang.String r10 = "CE"
            r9[r5] = r10
            java.lang.String r10 = "CI"
            r9[r6] = r10
            boolean r9 = contains(r1, r4, r15, r9)
            if (r9 != 0) goto L_0x01fe
            int r4 = r7 + 2
            goto L_0x01fe
        L_0x16b6:
            r13 = 75
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x16c9
            java.lang.StringBuilder r4 = r8.primary
            r9 = 80
            r4.append(r9)
        L_0x16c9:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x16da
            java.lang.StringBuilder r4 = r8.alternate
            r9 = 80
            r4.append(r9)
        L_0x16da:
            int r4 = r7 + 1
            char r9 = r0.charAt(r1, r4)
            r10 = 66
            if (r9 != r10) goto L_0x01fe
            int r7 = r7 + 2
            goto L_0x0092
        L_0x16e8:
            r13 = 75
            if (r7 != 0) goto L_0x170a
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x16fb
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r12)
        L_0x16fb:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x170a
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r12)
        L_0x170a:
            int r7 = r7 + 1
            goto L_0x0092
        L_0x170e:
            r13 = 75
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x1721
            java.lang.StringBuilder r4 = r8.primary
            r9 = 78
            r4.append(r9)
        L_0x1721:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x1732
            java.lang.StringBuilder r4 = r8.alternate
            r9 = 78
            r4.append(r9)
        L_0x1732:
            int r7 = r7 + 1
            goto L_0x0092
        L_0x1736:
            r13 = 75
            java.lang.StringBuilder r4 = r8.primary
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x1747
            java.lang.StringBuilder r4 = r8.primary
            r4.append(r11)
        L_0x1747:
            java.lang.StringBuilder r4 = r8.alternate
            int r4 = r4.length()
            int r9 = r8.maxLength
            if (r4 >= r9) goto L_0x1756
            java.lang.StringBuilder r4 = r8.alternate
            r4.append(r11)
        L_0x1756:
            int r7 = r7 + 1
            goto L_0x0092
        L_0x175a:
            if (r18 == 0) goto L_0x1763
            java.lang.StringBuilder r1 = r8.alternate
        L_0x175e:
            java.lang.String r1 = r1.toString()
            goto L_0x1766
        L_0x1763:
            java.lang.StringBuilder r1 = r8.primary
            goto L_0x175e
        L_0x1766:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.external.DoubleMetaphone.doubleMetaphone(java.lang.String, boolean):java.lang.String");
    }

    /* access modifiers changed from: protected */
    public char charAt(String str, int i) {
        if (i < 0 || i >= str.length()) {
            return 0;
        }
        return str.charAt(i);
    }

    public class DoubleMetaphoneResult {
        final StringBuilder alternate = new StringBuilder(DoubleMetaphone.this.maxCodeLen);
        final int maxLength;
        final StringBuilder primary = new StringBuilder(DoubleMetaphone.this.maxCodeLen);

        public DoubleMetaphoneResult(int i) {
            this.maxLength = i;
        }
    }
}
