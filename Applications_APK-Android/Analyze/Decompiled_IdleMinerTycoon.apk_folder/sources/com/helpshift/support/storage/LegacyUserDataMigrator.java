package com.helpshift.support.storage;

import android.provider.Settings;
import com.applovin.sdk.AppLovinEventParameters;
import com.helpshift.CoreApi;
import com.helpshift.HelpshiftUser;
import com.helpshift.account.dao.ProfileDTO;
import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.common.ListUtils;
import com.helpshift.common.StringUtils;
import com.helpshift.common.dao.BackupDAO;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.platform.KVStore;
import com.helpshift.common.platform.network.KeyValuePair;
import com.helpshift.migration.LegacyAnalyticsEventIDDAO;
import com.helpshift.migration.LegacyProfileMigrationDAO;
import com.helpshift.migration.MigrationState;
import com.helpshift.migration.legacyUser.LegacyProfile;
import com.helpshift.migration.legacyUser.LegacyProfileDAO;
import com.helpshift.support.HSStorage;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.util.VersionName;
import com.tapjoy.TapjoyConstants;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.List;

public class LegacyUserDataMigrator implements SDKMigrator {
    private static final String DEFAULT_USER_LOGIN_KEY = "default_user_login";
    private static final String DEFAULT_USER_PROFILE = "default_user_profile";
    private static final String KEY_DEVICE_ID = "key_support_device_id";
    private static final String LOGIN_IDENTIFIER = "loginIdentifier";
    private BackupDAO backupDAO;
    private CoreApi coreApi;
    private String defaultIdentifier;
    private ProfileDTO defaultProfileDto;
    private Domain domain;
    private VersionName fromVersion;
    private HSStorage hsStorage;
    private KVStore kvStore;
    private LegacyAnalyticsEventIDDAO legacyAnalyticsEventIDDAO;
    private LegacyProfileDAO legacyProfileDAO;
    private LegacyProfileMigrationDAO legacyProfileMigrationDAO;
    private String loginIdentifier;
    private List<ProfileDTO> loginProfileDtos;

    public LegacyUserDataMigrator(CoreApi coreApi2, HSStorage hSStorage, KVStore kVStore, LegacyProfileDAO legacyProfileDAO2, BackupDAO backupDAO2, LegacyProfileMigrationDAO legacyProfileMigrationDAO2, LegacyAnalyticsEventIDDAO legacyAnalyticsEventIDDAO2, VersionName versionName) {
        this.coreApi = coreApi2;
        this.domain = coreApi2.getDomain();
        this.hsStorage = hSStorage;
        this.kvStore = kVStore;
        this.legacyProfileDAO = legacyProfileDAO2;
        this.backupDAO = backupDAO2;
        this.legacyProfileMigrationDAO = legacyProfileMigrationDAO2;
        this.legacyAnalyticsEventIDDAO = legacyAnalyticsEventIDDAO2;
        this.fromVersion = versionName;
    }

    public void backup(VersionName versionName) {
        if (!versionName.isGreaterThanOrEqualTo(new VersionName("7.0.0"))) {
            if (versionName.isLessThanOrEqualTo(new VersionName("4.9.1"))) {
                this.loginIdentifier = this.hsStorage.getString(LOGIN_IDENTIFIER);
                String string = this.hsStorage.getString("identity");
                this.defaultIdentifier = this.hsStorage.getString("uuid");
                if (StringUtils.isEmpty(this.defaultIdentifier)) {
                    this.defaultIdentifier = Settings.Secure.getString(HelpshiftContext.getApplicationContext().getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
                }
                this.defaultProfileDto = new ProfileDTO(null, this.defaultIdentifier, string, this.hsStorage.getString(AppLovinEventParameters.USER_ACCOUNT_IDENTIFIER), this.hsStorage.getString("email"), null, null, null, true);
                List<ProfileDTO> fetchProfiles = this.legacyProfileDAO.fetchProfiles();
                if (!ListUtils.isEmpty(fetchProfiles)) {
                    this.loginProfileDtos = new ArrayList();
                    for (ProfileDTO next : fetchProfiles) {
                        this.loginProfileDtos.add(new ProfileDTO(next.localId, next.identifier, next.serverId, next.name, next.email, next.identifier + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + next.saltedIdentifier, next.uid, next.did, next.isPushTokenSynced));
                    }
                    return;
                }
                return;
            }
            this.loginIdentifier = this.kvStore.getString(LOGIN_IDENTIFIER);
            this.defaultIdentifier = this.kvStore.getString(DEFAULT_USER_LOGIN_KEY);
            if (!StringUtils.isEmpty(this.defaultIdentifier)) {
                Object serializable = this.kvStore.getSerializable(DEFAULT_USER_PROFILE);
                if (serializable instanceof ProfileDTO) {
                    this.defaultProfileDto = (ProfileDTO) serializable;
                }
            }
            this.loginProfileDtos = this.legacyProfileDAO.fetchProfiles();
        }
    }

    public void restore() {
        if (!this.fromVersion.isGreaterThanOrEqualTo(new VersionName("7.0.0"))) {
            if (this.defaultIdentifier != null) {
                this.kvStore.setString("key_support_device_id", this.defaultIdentifier);
                this.backupDAO.storeValue("key_support_device_id", this.defaultIdentifier);
            }
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (this.defaultProfileDto != null && !StringUtils.isEmpty(this.defaultProfileDto.serverId)) {
                UserDM anonymousUser = this.domain.getUserManagerDM().getAnonymousUser();
                if (anonymousUser == null) {
                    anonymousUser = this.domain.getUserManagerDM().createAnonymousUser();
                }
                arrayList2.add(new LegacyProfile(anonymousUser.getIdentifier(), this.defaultProfileDto.email, this.defaultProfileDto.name, this.defaultProfileDto.serverId, MigrationState.NOT_STARTED));
            }
            if (!ListUtils.isEmpty(this.loginProfileDtos)) {
                for (ProfileDTO next : this.loginProfileDtos) {
                    if (!StringUtils.isEmpty(next.serverId)) {
                        arrayList2.add(new LegacyProfile(next.identifier, next.email, next.name, next.serverId, MigrationState.NOT_STARTED));
                    }
                    arrayList.add(new KeyValuePair(next.identifier, next.saltedIdentifier));
                }
            }
            if (!ListUtils.isEmpty(arrayList2)) {
                this.legacyProfileMigrationDAO.storeLegacyProfiles(arrayList2);
            }
            if (!ListUtils.isEmpty(arrayList)) {
                this.legacyAnalyticsEventIDDAO.storeLegacyAnalyticsEventIDs(arrayList);
            }
            if (StringUtils.isEmpty(this.loginIdentifier)) {
                this.coreApi.logout();
            } else if (this.loginProfileDtos != null) {
                for (ProfileDTO next2 : this.loginProfileDtos) {
                    if (this.loginIdentifier.equals(next2.identifier)) {
                        this.coreApi.login(new HelpshiftUser.Builder(next2.identifier, next2.email).setName(next2.email).build());
                        return;
                    }
                }
            }
        }
    }

    public void dropProfileDB() {
        this.legacyProfileDAO.deleteProfiles();
    }
}
