package com.helpshift.support.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.helpshift.util.HelpshiftContext;

public class FaqsDBHelper extends SQLiteOpenHelper {
    private static final int CUR_DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "__hs__db_faq";

    FaqsDBHelper(Context context) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 3);
    }

    public static FaqsDBHelper getInstance() {
        return LazyHolder.INSTANCE;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        createTables(sQLiteDatabase);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        HelpshiftContext.getPlatform().getNetworkRequestDAO().storeETag("/faqs/", null);
        clearDatabase(sQLiteDatabase);
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        HelpshiftContext.getPlatform().getNetworkRequestDAO().storeETag("/faqs/", null);
        clearDatabase(sQLiteDatabase);
    }

    private void createTables(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE faqs (_id INTEGER PRIMARY KEY AUTOINCREMENT,question_id TEXT NOT NULL,publish_id TEXT NOT NULL,language TEXT NOT NULL,section_id TEXT NOT NULL,title TEXT NOT NULL,body TEXT NOT NULL,helpful INTEGER,rtl INTEGER,tags TEXT,c_tags TEXT,FOREIGN KEY(section_id) REFERENCES sections (_id));");
        sQLiteDatabase.execSQL("CREATE TABLE sections (_id INTEGER PRIMARY KEY AUTOINCREMENT,section_id TEXT NOT NULL,publish_id INTEGER NOT NULL,title TEXT NOT NULL);");
    }

    private void dropTables(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS faqs");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS sections");
    }

    public void clearDatabase(SQLiteDatabase sQLiteDatabase) {
        dropTables(sQLiteDatabase);
        createTables(sQLiteDatabase);
    }

    private static class LazyHolder {
        static final FaqsDBHelper INSTANCE = new FaqsDBHelper(HelpshiftContext.getApplicationContext());

        private LazyHolder() {
        }
    }
}
