package com.helpshift.support.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class SupportKeyValueDBStorageHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "__hs__db_support_key_values";
    private static final Integer DATABASE_VERSION = 1;

    SupportKeyValueDBStorageHelper(Context context) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, DATABASE_VERSION.intValue());
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE key_value_store(key text primary key,value blob not null);");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        dropTable(sQLiteDatabase);
        onCreate(sQLiteDatabase);
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        dropTable(sQLiteDatabase);
        onCreate(sQLiteDatabase);
    }

    private void dropTable(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS key_value_store;");
    }
}
