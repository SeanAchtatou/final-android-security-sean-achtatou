package com.helpshift.support.storage;

import com.helpshift.analytics.AnalyticsEventDAO;
import com.helpshift.common.StringUtils;
import com.helpshift.common.platform.KVStore;
import java.util.HashMap;
import java.util.Map;

public class AndroidAnalyticsEventDAO implements AnalyticsEventDAO {
    private static final String KEY_UNSENT_ANALYTICS_EVENTS = "unsent_analytics_events";
    private KVStore kvStore;

    public AndroidAnalyticsEventDAO(KVStore kVStore) {
        this.kvStore = kVStore;
    }

    public void saveUnsentAnalyticsData(String str, HashMap<String, String> hashMap) {
        HashMap<String, HashMap<String, String>> unSentAnalyticFromDB = getUnSentAnalyticFromDB();
        unSentAnalyticFromDB.put(str, hashMap);
        this.kvStore.setSerializable(KEY_UNSENT_ANALYTICS_EVENTS, unSentAnalyticFromDB);
    }

    public void removeAnalyticsData(String str) {
        if (!StringUtils.isEmpty(str)) {
            HashMap<String, HashMap<String, String>> unSentAnalyticFromDB = getUnSentAnalyticFromDB();
            unSentAnalyticFromDB.remove(str);
            if (unSentAnalyticFromDB.size() == 0) {
                this.kvStore.setSerializable(KEY_UNSENT_ANALYTICS_EVENTS, null);
            } else {
                this.kvStore.setSerializable(KEY_UNSENT_ANALYTICS_EVENTS, unSentAnalyticFromDB);
            }
        }
    }

    public Map<String, HashMap<String, String>> getUnsentAnalytics() {
        return getUnSentAnalyticFromDB();
    }

    private HashMap<String, HashMap<String, String>> getUnSentAnalyticFromDB() {
        Object serializable = this.kvStore.getSerializable(KEY_UNSENT_ANALYTICS_EVENTS);
        if (serializable == null) {
            return new HashMap<>();
        }
        return (HashMap) serializable;
    }
}
