package com.helpshift.support.storage;

import android.content.Context;
import com.helpshift.storage.BaseRetryKeyValueStorage;
import com.helpshift.storage.KeyValueDbStorage;
import com.helpshift.util.HSLogger;

class SupportRetryKeyValueDBStorage extends BaseRetryKeyValueStorage {
    private final Context context;
    private SupportKeyValueDBStorageHelper sqLiteOpenHelper;

    SupportRetryKeyValueDBStorage(Context context2) {
        this.context = context2;
        this.sqLiteOpenHelper = new SupportKeyValueDBStorageHelper(context2);
        this.keyValueStorage = new KeyValueDbStorage(this.sqLiteOpenHelper, null);
    }

    /* access modifiers changed from: protected */
    public void reInitiateDbInstance() {
        try {
            if (this.sqLiteOpenHelper != null) {
                this.sqLiteOpenHelper.close();
            }
        } catch (Exception e) {
            HSLogger.e("Helpshift_RetryKeyValue", "Error in closing DB", e);
        }
        this.sqLiteOpenHelper = new SupportKeyValueDBStorageHelper(this.context);
        this.keyValueStorage = new KeyValueDbStorage(this.sqLiteOpenHelper, null);
    }
}
