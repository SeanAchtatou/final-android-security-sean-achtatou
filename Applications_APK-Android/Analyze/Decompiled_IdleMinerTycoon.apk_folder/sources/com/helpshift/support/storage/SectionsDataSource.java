package com.helpshift.support.storage;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.helpshift.constants.Tables;
import com.helpshift.support.FaqTagFilter;
import com.helpshift.support.Section;
import com.helpshift.util.HSLogger;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SectionsDataSource implements SectionDAO {
    private static final String TAG = "HelpShiftDebug";
    private final FaqsDBHelper dbHelper;
    private FaqDAO faqDAO;

    private SectionsDataSource() {
        this.dbHelper = FaqsDBHelper.getInstance();
        this.faqDAO = FaqsDataSource.getInstance();
    }

    private static Section cursorToSection(Cursor cursor) {
        return new Section(cursor.getLong(0), cursor.getString(1), cursor.getString(3), cursor.getString(2));
    }

    private static ContentValues sectionToContentValues(JSONObject jSONObject) throws JSONException {
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", jSONObject.getString("title"));
        contentValues.put("publish_id", jSONObject.getString("publish_id"));
        contentValues.put("section_id", jSONObject.getString("id"));
        return contentValues;
    }

    public static SectionsDataSource getInstance() {
        return LazyHolder.INSTANCE;
    }

    public synchronized void storeSections(JSONArray jSONArray) {
        String str;
        String str2;
        SQLiteDatabase writableDatabase = this.dbHelper.getWritableDatabase();
        try {
            writableDatabase.beginTransaction();
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                writableDatabase.insert(Tables.SECTIONS, null, sectionToContentValues(jSONObject));
                JSONArray optJSONArray = jSONObject.optJSONArray(Tables.FAQS);
                if (optJSONArray != null) {
                    FaqsDataSource.addFaqsUnsafe(writableDatabase, jSONObject.getString("publish_id"), optJSONArray);
                }
            }
            writableDatabase.setTransactionSuccessful();
            if (writableDatabase != null) {
                try {
                    if (writableDatabase.inTransaction()) {
                        writableDatabase.endTransaction();
                    }
                } catch (Exception e) {
                    e = e;
                    str = "HelpShiftDebug";
                    str2 = "Error in storeSections inside finally block";
                    HSLogger.e(str, str2, e);
                }
            }
        } catch (JSONException e2) {
            try {
                HSLogger.e("HelpShiftDebug", "Error in storeSections", e2);
                if (writableDatabase != null) {
                    try {
                    } catch (Exception e3) {
                        e = e3;
                        str = "HelpShiftDebug";
                        str2 = "Error in storeSections inside finally block";
                        HSLogger.e(str, str2, e);
                    }
                }
            } finally {
                if (writableDatabase != null) {
                    try {
                        if (writableDatabase.inTransaction()) {
                            writableDatabase.endTransaction();
                        }
                    } catch (Exception e4) {
                        HSLogger.e("HelpShiftDebug", "Error in storeSections inside finally block", e4);
                    }
                }
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:14:0x0033 */
    /* JADX WARN: Type inference failed for: r0v2 */
    /* JADX WARN: Type inference failed for: r0v3, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r0v4, types: [com.helpshift.support.Section] */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARN: Type inference failed for: r0v7 */
    /* JADX WARN: Type inference failed for: r0v8 */
    /* JADX WARN: Type inference failed for: r0v9 */
    /* JADX WARN: Type inference failed for: r0v10 */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0033, code lost:
        if (r12 != null) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r12.close();
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0046, code lost:
        if (r12 != null) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004a, code lost:
        return r0;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0051 A[SYNTHETIC, Splitter:B:30:0x0051] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.helpshift.support.Section getSection(java.lang.String r12) {
        /*
            r11 = this;
            monitor-enter(r11)
            if (r12 == 0) goto L_0x0057
            java.lang.String r0 = ""
            boolean r0 = r12.equals(r0)     // Catch:{ all -> 0x0055 }
            if (r0 == 0) goto L_0x000c
            goto L_0x0057
        L_0x000c:
            r0 = 0
            com.helpshift.support.storage.FaqsDBHelper r1 = r11.dbHelper     // Catch:{ Exception -> 0x003d, all -> 0x003b }
            android.database.sqlite.SQLiteDatabase r2 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x003d, all -> 0x003b }
            java.lang.String r3 = "sections"
            r4 = 0
            java.lang.String r5 = "publish_id = ?"
            r1 = 1
            java.lang.String[] r6 = new java.lang.String[r1]     // Catch:{ Exception -> 0x003d, all -> 0x003b }
            r1 = 0
            r6[r1] = r12     // Catch:{ Exception -> 0x003d, all -> 0x003b }
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r12 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x003d, all -> 0x003b }
            r12.moveToFirst()     // Catch:{ Exception -> 0x0039 }
            boolean r1 = r12.isAfterLast()     // Catch:{ Exception -> 0x0039 }
            if (r1 != 0) goto L_0x0033
            com.helpshift.support.Section r1 = cursorToSection(r12)     // Catch:{ Exception -> 0x0039 }
            r0 = r1
        L_0x0033:
            if (r12 == 0) goto L_0x0049
        L_0x0035:
            r12.close()     // Catch:{ all -> 0x0055 }
            goto L_0x0049
        L_0x0039:
            r1 = move-exception
            goto L_0x003f
        L_0x003b:
            r12 = move-exception
            goto L_0x004f
        L_0x003d:
            r1 = move-exception
            r12 = r0
        L_0x003f:
            java.lang.String r2 = "HelpShiftDebug"
            java.lang.String r3 = "Error in getSection"
            com.helpshift.util.HSLogger.e(r2, r3, r1)     // Catch:{ all -> 0x004b }
            if (r12 == 0) goto L_0x0049
            goto L_0x0035
        L_0x0049:
            monitor-exit(r11)
            return r0
        L_0x004b:
            r0 = move-exception
            r10 = r0
            r0 = r12
            r12 = r10
        L_0x004f:
            if (r0 == 0) goto L_0x0054
            r0.close()     // Catch:{ all -> 0x0055 }
        L_0x0054:
            throw r12     // Catch:{ all -> 0x0055 }
        L_0x0055:
            r12 = move-exception
            goto L_0x005e
        L_0x0057:
            com.helpshift.support.Section r12 = new com.helpshift.support.Section     // Catch:{ all -> 0x0055 }
            r12.<init>()     // Catch:{ all -> 0x0055 }
            monitor-exit(r11)
            return r12
        L_0x005e:
            monitor-exit(r11)
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.storage.SectionsDataSource.getSection(java.lang.String):com.helpshift.support.Section");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002d, code lost:
        if (r2 != null) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0043, code lost:
        if (r2 != null) goto L_0x002f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x004b A[SYNTHETIC, Splitter:B:27:0x004b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<com.helpshift.support.Section> getAllSections() {
        /*
            r12 = this;
            monitor-enter(r12)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x004f }
            r0.<init>()     // Catch:{ all -> 0x004f }
            r1 = 0
            com.helpshift.support.storage.FaqsDBHelper r2 = r12.dbHelper     // Catch:{ Exception -> 0x0038, all -> 0x0035 }
            android.database.sqlite.SQLiteDatabase r3 = r2.getReadableDatabase()     // Catch:{ Exception -> 0x0038, all -> 0x0035 }
            java.lang.String r4 = "sections"
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r2 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x0038, all -> 0x0035 }
            r2.moveToFirst()     // Catch:{ Exception -> 0x0033 }
        L_0x001c:
            boolean r1 = r2.isAfterLast()     // Catch:{ Exception -> 0x0033 }
            if (r1 != 0) goto L_0x002d
            com.helpshift.support.Section r1 = cursorToSection(r2)     // Catch:{ Exception -> 0x0033 }
            r0.add(r1)     // Catch:{ Exception -> 0x0033 }
            r2.moveToNext()     // Catch:{ Exception -> 0x0033 }
            goto L_0x001c
        L_0x002d:
            if (r2 == 0) goto L_0x0046
        L_0x002f:
            r2.close()     // Catch:{ all -> 0x004f }
            goto L_0x0046
        L_0x0033:
            r1 = move-exception
            goto L_0x003c
        L_0x0035:
            r0 = move-exception
            r2 = r1
            goto L_0x0049
        L_0x0038:
            r2 = move-exception
            r11 = r2
            r2 = r1
            r1 = r11
        L_0x003c:
            java.lang.String r3 = "HelpShiftDebug"
            java.lang.String r4 = "Error in getAllSections"
            com.helpshift.util.HSLogger.e(r3, r4, r1)     // Catch:{ all -> 0x0048 }
            if (r2 == 0) goto L_0x0046
            goto L_0x002f
        L_0x0046:
            monitor-exit(r12)
            return r0
        L_0x0048:
            r0 = move-exception
        L_0x0049:
            if (r2 == 0) goto L_0x004e
            r2.close()     // Catch:{ all -> 0x004f }
        L_0x004e:
            throw r0     // Catch:{ all -> 0x004f }
        L_0x004f:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.storage.SectionsDataSource.getAllSections():java.util.List");
    }

    public List<Section> getAllSections(FaqTagFilter faqTagFilter) {
        List<Section> allSections = getAllSections();
        if (faqTagFilter == null) {
            return allSections;
        }
        ArrayList arrayList = new ArrayList();
        for (Section next : allSections) {
            if (!this.faqDAO.getFaqsForSection(next.getPublishId(), faqTagFilter).isEmpty()) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public synchronized void clearSectionsData() {
        try {
            this.dbHelper.clearDatabase(this.dbHelper.getWritableDatabase());
        } catch (Exception e) {
            HSLogger.e("HelpShiftDebug", "Error in clearSectionsData", e);
        }
        return;
    }

    private static final class LazyHolder {
        static final SectionsDataSource INSTANCE = new SectionsDataSource();

        private LazyHolder() {
        }
    }
}
