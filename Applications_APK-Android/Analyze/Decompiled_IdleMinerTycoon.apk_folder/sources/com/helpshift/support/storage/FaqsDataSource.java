package com.helpshift.support.storage;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.helpshift.constants.Tables;
import com.helpshift.support.Faq;
import com.helpshift.support.FaqTagFilter;
import com.helpshift.support.constants.FaqsColumns;
import com.helpshift.util.DatabaseUtils;
import com.helpshift.util.HSJSONUtils;
import com.helpshift.util.HSLogger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FaqsDataSource implements FaqDAO {
    private static final String TAG = "HelpShiftDebug";
    private final FaqsDBHelper dbHelper;

    private FaqsDataSource() {
        this.dbHelper = FaqsDBHelper.getInstance();
    }

    public static void addFaqsUnsafe(SQLiteDatabase sQLiteDatabase, String str, JSONArray jSONArray) {
        int i = 0;
        while (i < jSONArray.length()) {
            try {
                sQLiteDatabase.insert(Tables.FAQS, null, faqToContentValues(str, jSONArray.getJSONObject(i)));
                i++;
            } catch (JSONException e) {
                HSLogger.d("HelpShiftDebug", "addFaqsUnsafe", e);
                return;
            }
        }
    }

    private static Faq cursorToFaq(Cursor cursor) {
        long j = cursor.getLong(cursor.getColumnIndex("_id"));
        String string = cursor.getString(cursor.getColumnIndex(FaqsColumns.QUESTION_ID));
        String string2 = cursor.getString(cursor.getColumnIndex("publish_id"));
        String string3 = cursor.getString(cursor.getColumnIndex(FaqsColumns.LANGUAGE));
        String string4 = cursor.getString(cursor.getColumnIndex("section_id"));
        String string5 = cursor.getString(cursor.getColumnIndex("title"));
        String string6 = cursor.getString(cursor.getColumnIndex("body"));
        int i = cursor.getInt(cursor.getColumnIndex(FaqsColumns.HELPFUL));
        boolean z = true;
        if (cursor.getInt(cursor.getColumnIndex(FaqsColumns.RTL)) != 1) {
            z = false;
        }
        return new Faq(j, string, string2, string3, string4, string5, string6, i, Boolean.valueOf(z), HSJSONUtils.jsonToStringArrayList(cursor.getString(cursor.getColumnIndex(FaqsColumns.TAGS))), HSJSONUtils.jsonToStringArrayList(cursor.getString(cursor.getColumnIndex(FaqsColumns.CATEGORY_TAGS))));
    }

    private static ContentValues faqToContentValues(Faq faq) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FaqsColumns.QUESTION_ID, faq.getId());
        contentValues.put("publish_id", faq.publish_id);
        contentValues.put(FaqsColumns.LANGUAGE, faq.language);
        contentValues.put("section_id", faq.section_publish_id);
        contentValues.put("title", faq.title);
        contentValues.put("body", faq.body);
        contentValues.put(FaqsColumns.HELPFUL, Integer.valueOf(faq.is_helpful));
        contentValues.put(FaqsColumns.RTL, faq.is_rtl);
        contentValues.put(FaqsColumns.TAGS, String.valueOf(new JSONArray((Collection) faq.getTags())));
        contentValues.put(FaqsColumns.CATEGORY_TAGS, String.valueOf(new JSONArray((Collection) faq.getCategoryTags())));
        return contentValues;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private static ContentValues faqToContentValues(String str, JSONObject jSONObject) throws JSONException {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FaqsColumns.QUESTION_ID, jSONObject.getString("id"));
        contentValues.put("publish_id", jSONObject.getString("publish_id"));
        contentValues.put(FaqsColumns.LANGUAGE, jSONObject.getString(FaqsColumns.LANGUAGE));
        contentValues.put("section_id", str);
        contentValues.put("title", jSONObject.getString("title"));
        contentValues.put("body", jSONObject.getString("body"));
        contentValues.put(FaqsColumns.HELPFUL, (Integer) 0);
        contentValues.put(FaqsColumns.RTL, Boolean.valueOf(jSONObject.getString("is_rtl").equals("true")));
        contentValues.put(FaqsColumns.TAGS, (jSONObject.has("stags") ? jSONObject.optJSONArray("stags") : new JSONArray()).toString());
        contentValues.put(FaqsColumns.CATEGORY_TAGS, (jSONObject.has("issue_tags") ? jSONObject.optJSONArray("issue_tags") : new JSONArray()).toString());
        return contentValues;
    }

    public static FaqsDataSource getInstance() {
        return LazyHolder.INSTANCE;
    }

    public synchronized void clearDB() {
        try {
            this.dbHelper.clearDatabase(this.dbHelper.getWritableDatabase());
        } catch (Exception e) {
            HSLogger.e("HelpShiftDebug", "Error in clearDB", e);
        }
        return;
    }

    public synchronized void addFaq(Faq faq) {
        ContentValues faqToContentValues = faqToContentValues(faq);
        String[] strArr = {faq.getId()};
        try {
            SQLiteDatabase writableDatabase = this.dbHelper.getWritableDatabase();
            if (!DatabaseUtils.exists(writableDatabase, Tables.FAQS, "question_id=?", strArr)) {
                writableDatabase.insert(Tables.FAQS, null, faqToContentValues);
            } else {
                writableDatabase.update(Tables.FAQS, faqToContentValues, "question_id=?", strArr);
            }
        } catch (Exception e) {
            HSLogger.e("HelpShiftDebug", "Error in addFaq", e);
        }
        return;
    }

    public synchronized void removeFaq(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                this.dbHelper.getWritableDatabase().delete(Tables.FAQS, "publish_id=?", new String[]{str});
            } catch (Exception e) {
                HSLogger.e("HelpShiftDebug", "Error in removeFaq", e);
            }
        }
        return;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:14:0x002d */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r1v2, types: [com.helpshift.support.Faq] */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r1v4 */
    /* JADX WARN: Type inference failed for: r1v5 */
    /* JADX WARN: Type inference failed for: r1v6 */
    /* JADX WARN: Type inference failed for: r1v7 */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002d, code lost:
        if (r11 != null) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r11.close();
        r1 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0040, code lost:
        if (r11 != null) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0044, code lost:
        return r1;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0049 A[SYNTHETIC, Splitter:B:30:0x0049] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.helpshift.support.Faq getFaq(java.lang.String r11) {
        /*
            r10 = this;
            monitor-enter(r10)
            boolean r0 = android.text.TextUtils.isEmpty(r11)     // Catch:{ all -> 0x004d }
            r1 = 0
            if (r0 == 0) goto L_0x000a
            monitor-exit(r10)
            return r1
        L_0x000a:
            com.helpshift.support.storage.FaqsDBHelper r0 = r10.dbHelper     // Catch:{ Exception -> 0x0037, all -> 0x0035 }
            android.database.sqlite.SQLiteDatabase r2 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x0037, all -> 0x0035 }
            java.lang.String r3 = "faqs"
            r4 = 0
            java.lang.String r5 = "publish_id = ?"
            r0 = 1
            java.lang.String[] r6 = new java.lang.String[r0]     // Catch:{ Exception -> 0x0037, all -> 0x0035 }
            r0 = 0
            r6[r0] = r11     // Catch:{ Exception -> 0x0037, all -> 0x0035 }
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r11 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x0037, all -> 0x0035 }
            boolean r0 = r11.moveToFirst()     // Catch:{ Exception -> 0x0033 }
            if (r0 == 0) goto L_0x002d
            com.helpshift.support.Faq r0 = cursorToFaq(r11)     // Catch:{ Exception -> 0x0033 }
            r1 = r0
        L_0x002d:
            if (r11 == 0) goto L_0x0043
        L_0x002f:
            r11.close()     // Catch:{ all -> 0x004d }
            goto L_0x0043
        L_0x0033:
            r0 = move-exception
            goto L_0x0039
        L_0x0035:
            r0 = move-exception
            goto L_0x0047
        L_0x0037:
            r0 = move-exception
            r11 = r1
        L_0x0039:
            java.lang.String r2 = "HelpShiftDebug"
            java.lang.String r3 = "Error in getFaq"
            com.helpshift.util.HSLogger.e(r2, r3, r0)     // Catch:{ all -> 0x0045 }
            if (r11 == 0) goto L_0x0043
            goto L_0x002f
        L_0x0043:
            monitor-exit(r10)
            return r1
        L_0x0045:
            r0 = move-exception
            r1 = r11
        L_0x0047:
            if (r1 == 0) goto L_0x004c
            r1.close()     // Catch:{ all -> 0x004d }
        L_0x004c:
            throw r0     // Catch:{ all -> 0x004d }
        L_0x004d:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.storage.FaqsDataSource.getFaq(java.lang.String):com.helpshift.support.Faq");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:13:0x0035 */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r1v2, types: [com.helpshift.support.Faq] */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r1v4 */
    /* JADX WARN: Type inference failed for: r1v5 */
    /* JADX WARN: Type inference failed for: r1v6 */
    /* JADX WARN: Type inference failed for: r1v7 */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0035, code lost:
        if (r11 != null) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r11.close();
        r1 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0048, code lost:
        if (r11 != null) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004c, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0056, code lost:
        return null;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0051 A[SYNTHETIC, Splitter:B:29:0x0051] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.helpshift.support.Faq getFaq(java.lang.String r11, java.lang.String r12) {
        /*
            r10 = this;
            monitor-enter(r10)
            boolean r0 = android.text.TextUtils.isEmpty(r11)     // Catch:{ all -> 0x0057 }
            r1 = 0
            if (r0 != 0) goto L_0x0055
            boolean r0 = android.text.TextUtils.isEmpty(r12)     // Catch:{ all -> 0x0057 }
            if (r0 == 0) goto L_0x000f
            goto L_0x0055
        L_0x000f:
            com.helpshift.support.storage.FaqsDBHelper r0 = r10.dbHelper     // Catch:{ Exception -> 0x003f, all -> 0x003d }
            android.database.sqlite.SQLiteDatabase r2 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x003f, all -> 0x003d }
            java.lang.String r3 = "faqs"
            r4 = 0
            java.lang.String r5 = "publish_id = ? AND language = ?"
            r0 = 2
            java.lang.String[] r6 = new java.lang.String[r0]     // Catch:{ Exception -> 0x003f, all -> 0x003d }
            r0 = 0
            r6[r0] = r11     // Catch:{ Exception -> 0x003f, all -> 0x003d }
            r11 = 1
            r6[r11] = r12     // Catch:{ Exception -> 0x003f, all -> 0x003d }
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r11 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x003f, all -> 0x003d }
            boolean r12 = r11.moveToFirst()     // Catch:{ Exception -> 0x003b }
            if (r12 == 0) goto L_0x0035
            com.helpshift.support.Faq r12 = cursorToFaq(r11)     // Catch:{ Exception -> 0x003b }
            r1 = r12
        L_0x0035:
            if (r11 == 0) goto L_0x004b
        L_0x0037:
            r11.close()     // Catch:{ all -> 0x0057 }
            goto L_0x004b
        L_0x003b:
            r12 = move-exception
            goto L_0x0041
        L_0x003d:
            r12 = move-exception
            goto L_0x004f
        L_0x003f:
            r12 = move-exception
            r11 = r1
        L_0x0041:
            java.lang.String r0 = "HelpShiftDebug"
            java.lang.String r2 = "Error in getFaq"
            com.helpshift.util.HSLogger.e(r0, r2, r12)     // Catch:{ all -> 0x004d }
            if (r11 == 0) goto L_0x004b
            goto L_0x0037
        L_0x004b:
            monitor-exit(r10)
            return r1
        L_0x004d:
            r12 = move-exception
            r1 = r11
        L_0x004f:
            if (r1 == 0) goto L_0x0054
            r1.close()     // Catch:{ all -> 0x0057 }
        L_0x0054:
            throw r12     // Catch:{ all -> 0x0057 }
        L_0x0055:
            monitor-exit(r10)
            return r1
        L_0x0057:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.storage.FaqsDataSource.getFaq(java.lang.String, java.lang.String):com.helpshift.support.Faq");
    }

    public List<Faq> getFilteredFaqs(List<Faq> list, FaqTagFilter faqTagFilter) {
        if (faqTagFilter == null) {
            return list;
        }
        String operator = faqTagFilter.getOperator();
        char c = 65535;
        int hashCode = operator.hashCode();
        if (hashCode != -1038130864) {
            if (hashCode != 3555) {
                if (hashCode != 96727) {
                    if (hashCode == 109267 && operator.equals(FaqTagFilter.Operator.NOT)) {
                        c = 2;
                    }
                } else if (operator.equals(FaqTagFilter.Operator.AND)) {
                    c = 0;
                }
            } else if (operator.equals(FaqTagFilter.Operator.OR)) {
                c = 1;
            }
        } else if (operator.equals(FaqTagFilter.Operator.UNDEFINED)) {
            c = 3;
        }
        switch (c) {
            case 0:
                return getANDFilteredFaqs(list, faqTagFilter);
            case 1:
                return getORFilteredFaqs(list, faqTagFilter);
            case 2:
                return getNOTFilteredFaqs(list, faqTagFilter);
            case 3:
                return list;
            default:
                return list;
        }
    }

    public List<Faq> getFaqsForSection(String str, FaqTagFilter faqTagFilter) {
        return getFilteredFaqs(getFaqsDataForSection(str), faqTagFilter);
    }

    public synchronized int setIsHelpful(String str, Boolean bool) {
        int i;
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(FaqsColumns.HELPFUL, Integer.valueOf(bool.booleanValue() ? 1 : -1));
        try {
            i = this.dbHelper.getWritableDatabase().update(Tables.FAQS, contentValues, "question_id = ?", new String[]{str});
        } catch (Exception e) {
            HSLogger.e("HelpShiftDebug", "Error in setIsHelpful", e);
            i = 0;
        }
        return i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x005d A[SYNTHETIC, Splitter:B:31:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0064 A[SYNTHETIC, Splitter:B:36:0x0064] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<com.helpshift.support.Faq> getFaqsDataForSection(java.lang.String r13) {
        /*
            r12 = this;
            monitor-enter(r12)
            boolean r0 = android.text.TextUtils.isEmpty(r13)     // Catch:{ all -> 0x0068 }
            if (r0 == 0) goto L_0x000e
            java.util.ArrayList r13 = new java.util.ArrayList     // Catch:{ all -> 0x0068 }
            r13.<init>()     // Catch:{ all -> 0x0068 }
            monitor-exit(r12)
            return r13
        L_0x000e:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0068 }
            r0.<init>()     // Catch:{ all -> 0x0068 }
            r1 = 0
            com.helpshift.support.storage.FaqsDBHelper r2 = r12.dbHelper     // Catch:{ Exception -> 0x0053 }
            android.database.sqlite.SQLiteDatabase r3 = r2.getReadableDatabase()     // Catch:{ Exception -> 0x0053 }
            java.lang.String r4 = "faqs"
            r5 = 0
            java.lang.String r6 = "section_id = ?"
            r2 = 1
            java.lang.String[] r7 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0053 }
            r2 = 0
            r7[r2] = r13     // Catch:{ Exception -> 0x0053 }
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r13 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x0053 }
            boolean r1 = r13.moveToFirst()     // Catch:{ Exception -> 0x004c, all -> 0x0049 }
            if (r1 == 0) goto L_0x0043
        L_0x0032:
            boolean r1 = r13.isAfterLast()     // Catch:{ Exception -> 0x004c, all -> 0x0049 }
            if (r1 != 0) goto L_0x0043
            com.helpshift.support.Faq r1 = cursorToFaq(r13)     // Catch:{ Exception -> 0x004c, all -> 0x0049 }
            r0.add(r1)     // Catch:{ Exception -> 0x004c, all -> 0x0049 }
            r13.moveToNext()     // Catch:{ Exception -> 0x004c, all -> 0x0049 }
            goto L_0x0032
        L_0x0043:
            if (r13 == 0) goto L_0x0060
            r13.close()     // Catch:{ all -> 0x0068 }
            goto L_0x0060
        L_0x0049:
            r0 = move-exception
            r1 = r13
            goto L_0x0062
        L_0x004c:
            r1 = move-exception
            r11 = r1
            r1 = r13
            r13 = r11
            goto L_0x0054
        L_0x0051:
            r0 = move-exception
            goto L_0x0062
        L_0x0053:
            r13 = move-exception
        L_0x0054:
            java.lang.String r2 = "HelpShiftDebug"
            java.lang.String r3 = "Error in getFaqsDataForSection"
            com.helpshift.util.HSLogger.e(r2, r3, r13)     // Catch:{ all -> 0x0051 }
            if (r1 == 0) goto L_0x0060
            r1.close()     // Catch:{ all -> 0x0068 }
        L_0x0060:
            monitor-exit(r12)
            return r0
        L_0x0062:
            if (r1 == 0) goto L_0x0067
            r1.close()     // Catch:{ all -> 0x0068 }
        L_0x0067:
            throw r0     // Catch:{ all -> 0x0068 }
        L_0x0068:
            r13 = move-exception
            monitor-exit(r12)
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.storage.FaqsDataSource.getFaqsDataForSection(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003d, code lost:
        if (r2 != null) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0053, code lost:
        if (r2 != null) goto L_0x003f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005b A[SYNTHETIC, Splitter:B:28:0x005b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<java.lang.String> getAllFaqPublishIds() {
        /*
            r12 = this;
            monitor-enter(r12)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x005f }
            r0.<init>()     // Catch:{ all -> 0x005f }
            r1 = 0
            r2 = 1
            java.lang.String[] r5 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0048, all -> 0x0045 }
            r2 = 0
            java.lang.String r3 = "publish_id"
            r5[r2] = r3     // Catch:{ Exception -> 0x0048, all -> 0x0045 }
            com.helpshift.support.storage.FaqsDBHelper r2 = r12.dbHelper     // Catch:{ Exception -> 0x0048, all -> 0x0045 }
            android.database.sqlite.SQLiteDatabase r3 = r2.getReadableDatabase()     // Catch:{ Exception -> 0x0048, all -> 0x0045 }
            java.lang.String r4 = "faqs"
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r2 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x0048, all -> 0x0045 }
            boolean r1 = r2.moveToFirst()     // Catch:{ Exception -> 0x0043 }
            if (r1 == 0) goto L_0x003d
        L_0x0026:
            boolean r1 = r2.isAfterLast()     // Catch:{ Exception -> 0x0043 }
            if (r1 != 0) goto L_0x003d
            java.lang.String r1 = "publish_id"
            int r1 = r2.getColumnIndex(r1)     // Catch:{ Exception -> 0x0043 }
            java.lang.String r1 = r2.getString(r1)     // Catch:{ Exception -> 0x0043 }
            r0.add(r1)     // Catch:{ Exception -> 0x0043 }
            r2.moveToNext()     // Catch:{ Exception -> 0x0043 }
            goto L_0x0026
        L_0x003d:
            if (r2 == 0) goto L_0x0056
        L_0x003f:
            r2.close()     // Catch:{ all -> 0x005f }
            goto L_0x0056
        L_0x0043:
            r1 = move-exception
            goto L_0x004c
        L_0x0045:
            r0 = move-exception
            r2 = r1
            goto L_0x0059
        L_0x0048:
            r2 = move-exception
            r11 = r2
            r2 = r1
            r1 = r11
        L_0x004c:
            java.lang.String r3 = "HelpShiftDebug"
            java.lang.String r4 = "Error in getFaqsDataForSection"
            com.helpshift.util.HSLogger.e(r3, r4, r1)     // Catch:{ all -> 0x0058 }
            if (r2 == 0) goto L_0x0056
            goto L_0x003f
        L_0x0056:
            monitor-exit(r12)
            return r0
        L_0x0058:
            r0 = move-exception
        L_0x0059:
            if (r2 == 0) goto L_0x005e
            r2.close()     // Catch:{ all -> 0x005f }
        L_0x005e:
            throw r0     // Catch:{ all -> 0x005f }
        L_0x005f:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.storage.FaqsDataSource.getAllFaqPublishIds():java.util.List");
    }

    private List<Faq> getANDFilteredFaqs(List<Faq> list, FaqTagFilter faqTagFilter) {
        ArrayList arrayList = new ArrayList();
        for (Faq next : list) {
            ArrayList arrayList2 = new ArrayList(Arrays.asList(faqTagFilter.getTags()));
            arrayList2.removeAll(next.getCategoryTags());
            if (arrayList2.isEmpty()) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    private List<Faq> getORFilteredFaqs(List<Faq> list, FaqTagFilter faqTagFilter) {
        ArrayList arrayList = new ArrayList();
        for (Faq next : list) {
            if (new ArrayList(Arrays.asList(faqTagFilter.getTags())).removeAll(next.getCategoryTags())) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    private List<Faq> getNOTFilteredFaqs(List<Faq> list, FaqTagFilter faqTagFilter) {
        ArrayList arrayList = new ArrayList();
        for (Faq next : list) {
            if (!new ArrayList(Arrays.asList(faqTagFilter.getTags())).removeAll(next.getCategoryTags())) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    private static final class LazyHolder {
        static final FaqsDataSource INSTANCE = new FaqsDataSource();

        private LazyHolder() {
        }
    }
}
