package com.helpshift.support;

import android.content.Context;
import android.content.Intent;
import com.google.android.gms.drive.DriveFile;
import com.helpshift.app.AppLifeCycleStateHolder;
import com.helpshift.applifecycle.HSAppLifeCycleListener;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.logger.model.LogModel;
import com.helpshift.static_classes.ErrorReporting;
import com.helpshift.util.ApplicationUtil;
import com.helpshift.util.ErrorReportProvider;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftConnectionUtil;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.util.TimeUtil;
import java.util.List;

public class SupportAppLifeCycleListener implements HSAppLifeCycleListener {
    HSApiData data = null;
    HSStorage storage = null;

    public void onAppForeground(Context context) {
        List<LogModel> all;
        boolean z = true;
        AppLifeCycleStateHolder.setAppInForeground(true);
        if (this.data == null) {
            this.data = new HSApiData(context);
            this.storage = this.data.storage;
        }
        this.data.updateReviewCounter();
        if (this.data.shouldShowReviewPopup().booleanValue()) {
            Intent intent = new Intent(context, HSReview.class);
            intent.setFlags(DriveFile.MODE_READ_ONLY);
            context.startActivity(intent);
        }
        tryFetchingServerConfig(context);
        HelpshiftContext.getCoreApi().sendFailedApiCalls();
        HelpshiftContext.getCoreApi().sendAppStartEvent();
        HelpshiftContext.getCoreApi().resetPreIssueConversations();
        boolean isOnline = HelpshiftConnectionUtil.isOnline(context);
        synchronized (this) {
            if (isOnline) {
                try {
                    if (ErrorReporting.isEnabled()) {
                        long lastErrorReportedTime = this.storage.getLastErrorReportedTime();
                        long adjustedTimeInMillis = TimeUtil.getAdjustedTimeInMillis(Float.valueOf(HelpshiftContext.getPlatform().getNetworkRequestDAO().getServerTimeDelta()));
                        if (adjustedTimeInMillis - lastErrorReportedTime <= ErrorReportProvider.BATCH_TIME) {
                            z = false;
                        }
                        if (z && HSLogger.getFatalLogsCount() > 0 && (all = HSLogger.getAll()) != null && !all.isEmpty()) {
                            this.storage.setLastErrorReportedTime(adjustedTimeInMillis);
                            this.data.sendErrorReports(all);
                        }
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
    }

    public void onAppBackground(Context context) {
        AppLifeCycleStateHolder.setAppInForeground(false);
        HelpshiftContext.getCoreApi().getConversationInboxPoller().stop();
        HelpshiftContext.getCoreApi().sendRequestIdsForSuccessfulApiCalls();
    }

    private void tryFetchingServerConfig(Context context) {
        boolean isApplicationDebuggable = ApplicationUtil.isApplicationDebuggable(context);
        SDKConfigurationDM sDKConfigurationDM = HelpshiftContext.getCoreApi().getDomain().getSDKConfigurationDM();
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        if (isApplicationDebuggable || Math.abs(currentTimeMillis - sDKConfigurationDM.getLastSuccessfulConfigFetchTime().longValue()) >= sDKConfigurationDM.getPeriodicFetchInterval()) {
            HelpshiftContext.getCoreApi().fetchServerConfig();
        }
        HelpshiftContext.getCoreApi().refreshPoller();
    }
}
