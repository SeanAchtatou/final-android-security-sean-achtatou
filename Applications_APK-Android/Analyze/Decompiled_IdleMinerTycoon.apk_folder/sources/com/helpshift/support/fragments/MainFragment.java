package com.helpshift.support.fragments;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import com.helpshift.R;
import com.helpshift.model.InfoModelFactory;
import com.helpshift.support.util.FragmentUtil;
import com.helpshift.support.util.Styles;
import com.helpshift.util.ApplicationUtil;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.views.HSToast;
import java.lang.reflect.Field;

public abstract class MainFragment extends Fragment {
    private static final String TAG = SupportFragment.class.getSimpleName();
    public static final String TOOLBAR_ID = "toolbarId";
    private static boolean shouldRetainChildFragmentManager;
    protected String fragmentName = getClass().getName();
    private boolean isChangingConfigurations;
    private boolean isScreenLarge;
    private FragmentManager retainedChildFragmentManager;

    public abstract boolean shouldRefreshMenu();

    public FragmentManager getRetainedChildFragmentManager() {
        if (!shouldRetainChildFragmentManager) {
            return getChildFragmentManager();
        }
        if (this.retainedChildFragmentManager == null) {
            this.retainedChildFragmentManager = getChildFragmentManager();
        }
        return this.retainedChildFragmentManager;
    }

    public boolean isChangingConfigurations() {
        return this.isChangingConfigurations;
    }

    public Context getContext() {
        Context context = super.getContext();
        if (context != null) {
            return context;
        }
        return HelpshiftContext.getApplicationContext();
    }

    public void onAttach(Context context) {
        super.onAttach(ApplicationUtil.getContextWithUpdatedLocaleLegacy(context));
        try {
            setRetainInstance(true);
        } catch (Exception unused) {
            shouldRetainChildFragmentManager = true;
        }
        if (HelpshiftContext.getApplicationContext() == null) {
            HelpshiftContext.setApplicationContext(context.getApplicationContext());
        }
        this.isScreenLarge = Styles.isTablet(getContext());
        if (shouldRetainChildFragmentManager && this.retainedChildFragmentManager != null) {
            try {
                Field declaredField = Fragment.class.getDeclaredField("mChildFragmentManager");
                declaredField.setAccessible(true);
                declaredField.set(this, this.retainedChildFragmentManager);
            } catch (NoSuchFieldException e) {
                HSLogger.d(TAG, "NoSuchFieldException", e);
            } catch (IllegalAccessException e2) {
                HSLogger.d(TAG, "IllegalAccessException", e2);
            }
        }
    }

    public Animation onCreateAnimation(int i, boolean z, int i2) {
        if (InfoModelFactory.getInstance().appInfoModel.disableAnimations.booleanValue() || z || isRemoving()) {
            return super.onCreateAnimation(i, z, i2);
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 1.0f);
        alphaAnimation.setDuration((long) getResources().getInteger(R.integer.hs_animation_duration));
        return alphaAnimation;
    }

    public void onStart() {
        SupportFragment supportFragment;
        super.onStart();
        if (shouldRefreshMenu() && (supportFragment = FragmentUtil.getSupportFragment(this)) != null) {
            supportFragment.addVisibleFragment(this.fragmentName);
        }
    }

    public void onPause() {
        this.isChangingConfigurations = getActivity(this).isChangingConfigurations();
        super.onPause();
    }

    public void onStop() {
        SupportFragment supportFragment;
        if (shouldRefreshMenu() && (supportFragment = FragmentUtil.getSupportFragment(this)) != null) {
            supportFragment.removeVisibleFragment(this.fragmentName);
        }
        super.onStop();
    }

    public Activity getActivity(Fragment fragment) {
        if (fragment == null) {
            return null;
        }
        while (fragment.getParentFragment() != null) {
            fragment = fragment.getParentFragment();
        }
        return fragment.getActivity();
    }

    public boolean isScreenLarge() {
        return this.isScreenLarge;
    }

    public void setToolbarTitle(String str) {
        SupportFragment supportFragment = FragmentUtil.getSupportFragment(this);
        if (supportFragment != null) {
            supportFragment.setTitle(str);
        }
    }

    /* access modifiers changed from: protected */
    public void copyToClipboard(String str) {
        ((ClipboardManager) getContext().getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("Copy Text", str));
        HSToast.makeText(getContext(), getString(R.string.hs__copied_to_clipboard), 0).show();
    }
}
