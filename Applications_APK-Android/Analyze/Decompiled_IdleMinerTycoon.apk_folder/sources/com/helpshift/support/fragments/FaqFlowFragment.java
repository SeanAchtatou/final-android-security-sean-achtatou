package com.helpshift.support.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.helpshift.R;
import com.helpshift.support.compositions.FaqFragment;
import com.helpshift.support.contracts.FaqFlowView;
import com.helpshift.support.contracts.FaqFragmentListener;
import com.helpshift.support.controllers.FaqFlowController;
import com.helpshift.support.flows.CustomContactUsFlowListHolder;
import com.helpshift.support.flows.Flow;
import com.helpshift.support.util.FragmentUtil;
import java.util.List;

public class FaqFlowFragment extends MainFragment implements FaqFlowView {
    public static final String FRAGMENT_TAG = "Helpshift_FaqFlowFrag";
    private List<Flow> customContactUsFlows;
    private FaqFlowController faqFlowController;
    private View selectQuestionView;
    private View verticalDivider;

    public boolean shouldRefreshMenu() {
        return false;
    }

    public static FaqFlowFragment newInstance(Bundle bundle, List<Flow> list) {
        FaqFlowFragment faqFlowFragment = new FaqFlowFragment();
        faqFlowFragment.setArguments(bundle);
        faqFlowFragment.customContactUsFlows = list;
        return faqFlowFragment;
    }

    public FaqFlowController getFaqFlowController() {
        return this.faqFlowController;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (this.faqFlowController == null) {
            this.faqFlowController = new FaqFlowController(this, context, getRetainedChildFragmentManager(), getArguments());
        } else {
            this.faqFlowController.onFragmentManagerUpdate(getRetainedChildFragmentManager());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__faq_flow_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.verticalDivider = view.findViewById(R.id.vertical_divider);
        this.selectQuestionView = view.findViewById(R.id.select_question_view);
    }

    public void onViewStateRestored(@Nullable Bundle bundle) {
        super.onViewStateRestored(bundle);
        if (bundle != null && this.faqFlowController != null) {
            this.faqFlowController.onViewStateRestored(bundle);
        }
    }

    public void onResume() {
        super.onResume();
        CustomContactUsFlowListHolder.setFlowList(this.customContactUsFlows);
        getSupportFragment().setSearchListeners(this.faqFlowController);
        this.faqFlowController.start();
        updateSelectQuestionUI();
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.faqFlowController != null) {
            this.faqFlowController.onSaveInstanceState(bundle);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.faqFlowController = null;
        this.verticalDivider = null;
        this.selectQuestionView = null;
        getSupportFragment().unRegisterSearchListener();
    }

    public void updateSelectQuestionUI() {
        if (isScreenLarge() && this.selectQuestionView != null) {
            if (getRetainedChildFragmentManager().findFragmentById(R.id.details_fragment_container) == null) {
                updateSelectQuestionUI(true);
            } else {
                updateSelectQuestionUI(false);
            }
        }
    }

    public void updateSelectQuestionUI(boolean z) {
        if (this.selectQuestionView == null) {
            return;
        }
        if (z) {
            this.selectQuestionView.setVisibility(0);
        } else {
            this.selectQuestionView.setVisibility(8);
        }
    }

    public SupportFragment getSupportFragment() {
        return (SupportFragment) getParentFragment();
    }

    public FaqFragmentListener getFaqFlowListener() {
        return getFaqFlowController();
    }

    public void showVerticalDivider(boolean z) {
        if (this.verticalDivider == null) {
            return;
        }
        if (z) {
            this.verticalDivider.setVisibility(0);
        } else {
            this.verticalDivider.setVisibility(8);
        }
    }

    public void retryGetSections() {
        FaqFragment faqFragment = FragmentUtil.getFaqFragment(getRetainedChildFragmentManager());
        if (faqFragment != null) {
            faqFragment.retryGetSections();
        }
    }

    public List<Flow> getCustomContactUsFlows() {
        return this.customContactUsFlows;
    }
}
