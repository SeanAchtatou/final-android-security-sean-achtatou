package com.helpshift.support.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.helpshift.R;
import com.helpshift.constants.Tables;
import com.helpshift.support.FaqTagFilter;
import com.helpshift.support.adapters.SectionListAdapter;
import com.helpshift.support.contracts.FaqFlowViewParent;
import com.helpshift.support.contracts.FaqFragmentListener;
import java.util.ArrayList;

public class SectionListFragment extends MainFragment {
    private RecyclerView sectionList;

    public boolean shouldRefreshMenu() {
        return false;
    }

    public static SectionListFragment newInstance(@Nullable Bundle bundle) {
        SectionListFragment sectionListFragment = new SectionListFragment();
        sectionListFragment.setArguments(bundle);
        return sectionListFragment;
    }

    public FaqFragmentListener getFaqFlowListener() {
        return ((FaqFlowViewParent) getParentFragment()).getFaqFlowListener();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__section_list_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        final ArrayList parcelableArrayList = getArguments().getParcelableArrayList(Tables.SECTIONS);
        final FaqTagFilter faqTagFilter = (FaqTagFilter) getArguments().getSerializable("withTagsMatching");
        this.sectionList = (RecyclerView) view.findViewById(R.id.section_list);
        this.sectionList.setLayoutManager(new LinearLayoutManager(view.getContext()));
        this.sectionList.setAdapter(new SectionListAdapter(parcelableArrayList, new View.OnClickListener() {
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(Tables.SECTIONS, parcelableArrayList);
                bundle.putString("sectionPublishId", (String) view.getTag());
                bundle.putSerializable("withTagsMatching", faqTagFilter);
                SectionListFragment.this.getFaqFlowListener().onSectionSelected(bundle);
            }
        }));
    }

    public void onDestroyView() {
        this.sectionList.setAdapter(null);
        this.sectionList = null;
        super.onDestroyView();
    }
}
