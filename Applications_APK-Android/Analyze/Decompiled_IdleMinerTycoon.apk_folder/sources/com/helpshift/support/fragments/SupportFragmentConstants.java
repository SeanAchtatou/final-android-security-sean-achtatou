package com.helpshift.support.fragments;

public interface SupportFragmentConstants {
    public static final String DECOMPOSED = "decomp";
    public static final String FLOW_TITLE = "flow_title";
}
