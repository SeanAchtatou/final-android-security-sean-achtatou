package com.helpshift.support.fragments;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.helpshift.R;
import com.helpshift.common.domain.AttachmentFileManagerDM;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.conversation.activeconversation.ScreenshotPreviewRenderer;
import com.helpshift.conversation.dto.ImagePickerFile;
import com.helpshift.conversation.viewmodel.ScreenshotPreviewVM;
import com.helpshift.support.contracts.ScreenshotPreviewListener;
import com.helpshift.support.controllers.SupportController;
import com.helpshift.support.storage.IMAppSessionStorage;
import com.helpshift.support.util.AppSessionConstants;
import com.helpshift.support.util.AttachmentUtil;
import com.helpshift.support.util.SnackbarUtil;
import com.helpshift.util.HelpshiftContext;

public class ScreenshotPreviewFragment extends MainFragment implements View.OnClickListener, AttachmentFileManagerDM.Listener, ScreenshotPreviewRenderer {
    public static final String FRAGMENT_TAG = "ScreenshotPreviewFragment";
    public static final String KEY_MESSAGE_REFERS_ID = "key_refers_id";
    public static final String KEY_SCREENSHOT_MODE = "key_screenshot_mode";
    private static final AppSessionConstants.Screen screenType = AppSessionConstants.Screen.SCREENSHOT_PREVIEW;
    private String attachmentMessageRefersId;
    private View buttonsContainer;
    private View buttonsSeparator;
    ImagePickerFile imagePickerFile;
    LaunchSource launchSource;
    private int mode;
    ProgressBar progressBar;
    private ImageView screenshotPreview;
    private ScreenshotPreviewListener screenshotPreviewListener;
    private ScreenshotPreviewVM screenshotPreviewVM;
    private Button secondaryButton;

    public enum LaunchSource {
        ATTACHMENT_DRAFT,
        GALLERY_APP
    }

    public static class Modes {
        public static final int ADD = 1;
        public static final int REMOVE = 2;
        public static final int SEND = 3;
    }

    public enum ScreenshotAction {
        ADD,
        SEND,
        REMOVE,
        CHANGE
    }

    public boolean shouldRefreshMenu() {
        return true;
    }

    public static ScreenshotPreviewFragment newInstance(ScreenshotPreviewListener screenshotPreviewListener2) {
        ScreenshotPreviewFragment screenshotPreviewFragment = new ScreenshotPreviewFragment();
        screenshotPreviewFragment.screenshotPreviewListener = screenshotPreviewListener2;
        return screenshotPreviewFragment;
    }

    private static void setSecondaryButtonText(Button button, int i) {
        String str;
        Resources resources = button.getResources();
        switch (i) {
            case 1:
                str = resources.getString(R.string.hs__screenshot_add);
                break;
            case 2:
                str = resources.getString(R.string.hs__screenshot_remove);
                break;
            case 3:
                str = resources.getString(R.string.hs__send_msg_btn);
                break;
            default:
                str = "";
                break;
        }
        button.setText(str);
    }

    public void setParams(@NonNull Bundle bundle, ImagePickerFile imagePickerFile2, LaunchSource launchSource2) {
        this.mode = bundle.getInt(KEY_SCREENSHOT_MODE);
        this.attachmentMessageRefersId = bundle.getString(KEY_MESSAGE_REFERS_ID);
        this.imagePickerFile = imagePickerFile2;
        this.launchSource = launchSource2;
        setScreenshotPreview();
    }

    public void setScreenshotPreviewListener(ScreenshotPreviewListener screenshotPreviewListener2) {
        this.screenshotPreviewListener = screenshotPreviewListener2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__screenshot_preview_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.screenshotPreviewVM = HelpshiftContext.getCoreApi().getScreenshotPreviewModel(this);
        this.screenshotPreview = (ImageView) view.findViewById(R.id.screenshot_preview);
        ((Button) view.findViewById(R.id.change)).setOnClickListener(this);
        this.secondaryButton = (Button) view.findViewById(R.id.secondary_button);
        this.secondaryButton.setOnClickListener(this);
        this.progressBar = (ProgressBar) view.findViewById(R.id.screenshot_loading_indicator);
        this.buttonsContainer = view.findViewById(R.id.button_containers);
        this.buttonsSeparator = view.findViewById(R.id.buttons_separator);
    }

    public void onResume() {
        super.onResume();
        setSecondaryButtonText(this.secondaryButton, this.mode);
        setScreenshotPreview();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
    }

    public void onDestroyView() {
        this.screenshotPreviewVM.unregisterRenderer();
        super.onDestroyView();
    }

    public void onStart() {
        super.onStart();
        IMAppSessionStorage.getInstance().set(AppSessionConstants.CURRENT_OPEN_SCREEN, screenType);
    }

    public void onPause() {
        SnackbarUtil.hideSnackbar(getView());
        super.onPause();
    }

    public void onStop() {
        super.onStop();
        AppSessionConstants.Screen screen = (AppSessionConstants.Screen) IMAppSessionStorage.getInstance().get(AppSessionConstants.CURRENT_OPEN_SCREEN);
        if (screen != null && screen.equals(screenType)) {
            IMAppSessionStorage.getInstance().removeKey(AppSessionConstants.CURRENT_OPEN_SCREEN);
        }
    }

    public void deleteAttachmentLocalCopy() {
        if (this.launchSource == LaunchSource.GALLERY_APP) {
            HelpshiftContext.getCoreApi().getAttachmentFileManagerDM().deleteAttachmentLocalCopy(this.imagePickerFile);
        }
    }

    private void setScreenshotPreview() {
        if (!isResumed()) {
            return;
        }
        if (this.imagePickerFile == null) {
            if (this.screenshotPreviewListener != null) {
                this.screenshotPreviewListener.removeScreenshotPreviewFragment();
            }
        } else if (this.imagePickerFile.filePath != null) {
            renderScreenshotPreview(this.imagePickerFile.filePath);
        } else if (this.imagePickerFile.transientUri != null) {
            toggleProgressBarViewsVisibility(true);
            HelpshiftContext.getCoreApi().getAttachmentFileManagerDM().compressAndCopyScreenshot(this.imagePickerFile, this.attachmentMessageRefersId, this);
        }
    }

    /* access modifiers changed from: package-private */
    public void renderScreenshotPreview(String str) {
        Bitmap bitmap = AttachmentUtil.getBitmap(str, -1);
        if (bitmap != null) {
            this.screenshotPreview.setImageBitmap(bitmap);
        } else if (this.screenshotPreviewListener != null) {
            this.screenshotPreviewListener.removeScreenshotPreviewFragment();
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.secondary_button && this.imagePickerFile != null) {
            switch (this.mode) {
                case 1:
                    this.screenshotPreviewListener.addScreenshot(this.imagePickerFile);
                    return;
                case 2:
                    HelpshiftContext.getCoreApi().getAttachmentFileManagerDM().deleteAttachmentLocalCopy(this.imagePickerFile);
                    this.screenshotPreviewListener.removeScreenshot();
                    return;
                case 3:
                    this.screenshotPreviewListener.sendScreenshot(this.imagePickerFile, this.attachmentMessageRefersId);
                    return;
                default:
                    return;
            }
        } else if (id == R.id.change) {
            if (this.mode == 2) {
                this.mode = 1;
            }
            HelpshiftContext.getCoreApi().getAttachmentFileManagerDM().deleteAttachmentLocalCopy(this.imagePickerFile);
            Bundle bundle = new Bundle();
            bundle.putInt(KEY_SCREENSHOT_MODE, this.mode);
            bundle.putString(KEY_MESSAGE_REFERS_ID, this.attachmentMessageRefersId);
            this.screenshotPreviewListener.changeScreenshot(bundle);
        }
    }

    public void onCompressAndCopyFailure(RootAPIException rootAPIException) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    ScreenshotPreviewFragment.this.progressBar.setVisibility(8);
                    SnackbarUtil.showSnackbar(ScreenshotPreviewFragment.this.getView(), R.string.hs__screenshot_cloud_attach_error, -2);
                }
            });
        }
    }

    public void onCompressAndCopySuccess(final ImagePickerFile imagePickerFile2) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    ScreenshotPreviewFragment.this.toggleProgressBarViewsVisibility(false);
                    ScreenshotPreviewFragment.this.renderScreenshotPreview(imagePickerFile2.filePath);
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    public void toggleProgressBarViewsVisibility(boolean z) {
        if (z) {
            this.progressBar.setVisibility(0);
            this.buttonsContainer.setVisibility(8);
            this.buttonsSeparator.setVisibility(8);
            this.screenshotPreview.setVisibility(8);
            return;
        }
        this.progressBar.setVisibility(8);
        this.buttonsContainer.setVisibility(0);
        this.buttonsSeparator.setVisibility(0);
        this.screenshotPreview.setVisibility(0);
    }

    public void onAuthenticationFailure() {
        SupportController supportController = ((SupportFragment) getParentFragment()).getSupportController();
        if (supportController != null) {
            supportController.onAuthenticationFailure();
        }
    }
}
