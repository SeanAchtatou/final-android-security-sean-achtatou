package com.helpshift.support.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.helpshift.R;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.support.Faq;
import com.helpshift.support.FaqTagFilter;
import com.helpshift.support.HSApiData;
import com.helpshift.support.HSSearch;
import com.helpshift.support.adapters.SearchListAdapter;
import com.helpshift.support.contracts.FaqFlowViewParent;
import com.helpshift.support.contracts.FaqFragmentListener;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftContext;
import im.getsocial.sdk.consts.LanguageCodes;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SearchFragment extends MainFragment {
    private static final String KEY_SEARCH_QUERY = "key_search_query";
    public static final String TAG = "Helpshift_SearchFrag";
    String currentQuery;
    HSApiData data;
    FaqTagFilter faqTagFilter;
    private View.OnClickListener onContactUsClickedListener;
    private View.OnClickListener onQuestionClickedListener;
    RecyclerView searchList;
    private final Handler searchResultSuccessHandler = new Handler() {
        public void handleMessage(Message message) {
            if (message != null && message.getData() != null && message.obj != null) {
                String string = message.getData().getString(SearchFragment.KEY_SEARCH_QUERY);
                if (SearchFragment.this.currentQuery != null && SearchFragment.this.currentQuery.equals(string)) {
                    List list = (List) message.obj;
                    if (list == null) {
                        list = new ArrayList();
                    }
                    SearchFragment.this.updateSearchResult(list);
                }
            }
        }
    };
    private String sectionId;

    public boolean shouldRefreshMenu() {
        return true;
    }

    public static SearchFragment newInstance(Bundle bundle) {
        SearchFragment searchFragment = new SearchFragment();
        searchFragment.setArguments(bundle);
        return searchFragment;
    }

    public FaqFragmentListener getFaqFlowListener() {
        return ((FaqFlowViewParent) getParentFragment()).getFaqFlowListener();
    }

    public String getCurrentQuery() {
        return this.currentQuery;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.faqTagFilter = (FaqTagFilter) arguments.getSerializable("withTagsMatching");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__search_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.searchList = (RecyclerView) view.findViewById(R.id.search_list);
        this.searchList.setLayoutManager(new LinearLayoutManager(view.getContext()));
        this.onQuestionClickedListener = new View.OnClickListener() {
            public void onClick(View view) {
                String str = (String) view.getTag();
                Faq faq = ((SearchListAdapter) SearchFragment.this.searchList.getAdapter()).getFaq(str);
                SearchFragment.this.getFaqFlowListener().onQuestionSelected(str, faq != null ? faq.searchTerms : null);
            }
        };
        this.onContactUsClickedListener = new View.OnClickListener() {
            public void onClick(View view) {
                SearchFragment.this.getFaqFlowListener().onContactUsClicked(SearchFragment.this.currentQuery);
            }
        };
        if (getArguments() != null) {
            this.sectionId = getArguments().getString("sectionPublishId");
        }
        onQuery(this.currentQuery, this.sectionId);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.data = new HSApiData(context);
        this.data.loadIndex();
    }

    public void onDestroyView() {
        this.searchList.setAdapter(null);
        this.searchList = null;
        super.onDestroyView();
    }

    public void onQuery(String str, String str2) {
        String trim;
        this.sectionId = str2;
        if (this.searchList != null) {
            String string = HelpshiftContext.getCoreApi().getSDKConfigurationDM().getString(SDKConfigurationDM.SDK_LANGUAGE);
            if (TextUtils.isEmpty(string)) {
                string = Locale.getDefault().getLanguage();
            }
            boolean z = string.startsWith("zh") || string.equals(LanguageCodes.JAPANESE) || string.equals(LanguageCodes.KOREAN);
            if (str == null) {
                trim = "";
            } else {
                trim = str.trim();
            }
            String str3 = trim;
            this.currentQuery = str3;
            new Thread(new SearchResultFetcherTask(str3, z, str2, this.searchResultSuccessHandler), "HS-search-query").start();
            HSLogger.d(TAG, "Performing search : Query : " + this.currentQuery);
        }
    }

    public int getNumberOfSearchResults() {
        SearchListAdapter searchListAdapter;
        if (this.searchList == null || (searchListAdapter = (SearchListAdapter) this.searchList.getAdapter()) == null) {
            return -1;
        }
        return searchListAdapter.getItemCount() - searchListAdapter.getFooterCount();
    }

    /* access modifiers changed from: package-private */
    public void updateSearchResult(@NonNull List<Faq> list) {
        if (this.searchList != null) {
            SearchListAdapter searchListAdapter = new SearchListAdapter(this.currentQuery, list, this.onQuestionClickedListener, this.onContactUsClickedListener);
            searchListAdapter.setHasStableIds(true);
            if (this.searchList.getAdapter() == null) {
                this.searchList.setAdapter(searchListAdapter);
            } else {
                this.searchList.swapAdapter(new SearchListAdapter(this.currentQuery, list, this.onQuestionClickedListener, this.onContactUsClickedListener), true);
            }
        }
    }

    private class SearchResultFetcherTask implements Runnable {
        private Handler callback;
        private String query;
        private boolean searchWithAny;
        private String sectionId;

        public SearchResultFetcherTask(String str, boolean z, String str2, Handler handler) {
            this.query = str;
            this.searchWithAny = z;
            this.sectionId = str2;
            this.callback = handler;
        }

        public void run() {
            List<Faq> list;
            if (TextUtils.isEmpty(this.query) || (this.query.length() < 3 && !this.searchWithAny)) {
                list = SearchFragment.this.data.getAllFaqs(SearchFragment.this.faqTagFilter);
            } else {
                list = SearchFragment.this.data.localFaqSearch(this.query, HSSearch.HS_SEARCH_OPTIONS.FULL_SEARCH, SearchFragment.this.faqTagFilter);
            }
            if (!TextUtils.isEmpty(this.sectionId)) {
                ArrayList arrayList = new ArrayList();
                for (Faq faq : list) {
                    if (faq.section_publish_id.equals(this.sectionId)) {
                        arrayList.add(faq);
                    }
                }
                list = arrayList;
            }
            Message message = new Message();
            message.obj = list;
            Bundle bundle = new Bundle();
            bundle.putString(SearchFragment.KEY_SEARCH_QUERY, this.query);
            message.setData(bundle);
            this.callback.sendMessage(message);
        }
    }
}
