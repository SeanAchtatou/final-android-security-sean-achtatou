package com.helpshift.support.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.analytics.AnalyticsEventType;
import com.helpshift.common.domain.F;
import com.helpshift.support.ContactUsFilter;
import com.helpshift.support.Faq;
import com.helpshift.support.HSApiData;
import com.helpshift.support.contracts.FaqFlowViewParent;
import com.helpshift.support.contracts.FaqFragmentListener;
import com.helpshift.support.controllers.SupportController;
import com.helpshift.support.util.FragmentUtil;
import com.helpshift.support.util.SnackbarUtil;
import com.helpshift.support.webkit.CustomWebChromeClient;
import com.helpshift.support.webkit.CustomWebView;
import com.helpshift.support.webkit.CustomWebViewClient;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftConnectionUtil;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.util.Styles;
import com.helpshift.views.FontApplier;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

public class SingleQuestionFragment extends MainFragment implements View.OnClickListener, CustomWebViewClient.CustomWebViewClientListeners {
    public static final String BUNDLE_ARG_QUESTION_LANGUAGE = "questionLanguage";
    public static final String BUNDLE_ARG_QUESTION_PUBLISH_ID = "questionPublishId";
    private static final String TAG = "Helpshift_SingleQstn";
    private Button contactUsButton;
    private HSApiData data;
    private boolean decomp;
    boolean eventSent;
    /* access modifiers changed from: private */
    public Faq highlightedQuestion;
    private int isHelpful = 0;
    private boolean isHighlighted;
    private Button noButton;
    private View progressBar;
    /* access modifiers changed from: private */
    public Faq question;
    private View questionFooter;
    private TextView questionFooterMessage;
    private String questionPublishId;
    private QuestionReadListener questionReadListener;
    private boolean showRootLayoutInsideCardView = false;
    private int singleQuestionMode = 1;
    private SupportController supportController;
    private String textColor;
    private String textColorLink;
    private CustomWebView webView;
    private Button yesButton;

    public interface QuestionReadListener {
        void onQuestionRead(String str);
    }

    public static class SingleQuestionModes {
        public static final int ADMIN_SUGGESTED = 3;
        public static final int DONE = 2;
        public static final int STANDARD = 1;
    }

    public boolean shouldRefreshMenu() {
        return true;
    }

    public static SingleQuestionFragment newInstance(Bundle bundle, int i, boolean z, QuestionReadListener questionReadListener2) {
        SingleQuestionFragment singleQuestionFragment = new SingleQuestionFragment();
        singleQuestionFragment.setArguments(bundle);
        singleQuestionFragment.singleQuestionMode = i;
        singleQuestionFragment.showRootLayoutInsideCardView = z;
        singleQuestionFragment.questionReadListener = questionReadListener2;
        return singleQuestionFragment;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.data = new HSApiData(context);
        getColorsFromTheme(context);
        SupportFragment supportFragment = FragmentUtil.getSupportFragment(this);
        if (supportFragment != null) {
            this.supportController = supportFragment.getSupportController();
        }
        this.fragmentName = getClass().getName() + this.singleQuestionMode;
    }

    public void onStart() {
        super.onStart();
        if (!isChangingConfigurations()) {
            this.eventSent = false;
        }
    }

    public void onPause() {
        super.onPause();
        this.webView.onPause();
    }

    public void onStop() {
        super.onStop();
        if (this.decomp || !isScreenLarge()) {
            setToolbarTitle(getString(R.string.hs__help_header));
        }
    }

    private void getColorsFromTheme(Context context) {
        int i = Build.VERSION.SDK_INT >= 21 ? 16843829 : 16842907;
        this.textColor = Styles.getHexColor(context, 16842806);
        this.textColorLink = Styles.getHexColor(context, i);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.decomp = arguments.getBoolean(SupportFragmentConstants.DECOMPOSED, false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int i = R.layout.hs__single_question_fragment;
        if (this.showRootLayoutInsideCardView) {
            i = R.layout.hs__single_question_layout_with_cardview;
        }
        return layoutInflater.inflate(i, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.webView = (CustomWebView) view.findViewById(R.id.web_view);
        this.webView.setWebViewClient(new CustomWebViewClient(HelpshiftContext.getApplicationContext(), this));
        this.webView.setWebChromeClient(new CustomWebChromeClient(getActivity().getWindow().getDecorView(), view.findViewById(R.id.faq_content_view)));
        this.yesButton = (Button) view.findViewById(R.id.helpful_button);
        this.yesButton.setOnClickListener(this);
        this.noButton = (Button) view.findViewById(R.id.unhelpful_button);
        this.noButton.setOnClickListener(this);
        this.questionFooter = view.findViewById(R.id.question_footer);
        this.questionFooterMessage = (TextView) view.findViewById(R.id.question_footer_message);
        this.contactUsButton = (Button) view.findViewById(R.id.contact_us_button);
        this.contactUsButton.setOnClickListener(this);
        if (Build.VERSION.SDK_INT >= 24) {
            this.yesButton.setText(R.string.hs__mark_yes);
            this.noButton.setText(R.string.hs__mark_no);
            this.contactUsButton.setText(R.string.hs__contact_us_btn);
        }
        if (this.singleQuestionMode == 2) {
            this.contactUsButton.setText(getResources().getString(R.string.hs__send_anyway));
        }
        this.questionPublishId = getArguments().getString(BUNDLE_ARG_QUESTION_PUBLISH_ID);
        int i = getArguments().getInt(SupportFragment.SUPPORT_MODE);
        String string = getArguments().getString(BUNDLE_ARG_QUESTION_LANGUAGE, "");
        boolean z = this.singleQuestionMode == 3;
        this.data.getQuestion(new Success(this), new Failure(this), z || i == 3, z, this.questionPublishId, string);
        this.progressBar = view.findViewById(R.id.progress_bar);
    }

    public void onResume() {
        super.onResume();
        if (isScreenLarge()) {
            Fragment parentFragment = getParentFragment();
            if (parentFragment instanceof FaqFlowFragment) {
                ((FaqFlowFragment) parentFragment).updateSelectQuestionUI(false);
            }
        }
        this.webView.onResume();
        if (this.decomp || !isScreenLarge()) {
            setToolbarTitle(getString(R.string.hs__question_header));
        }
        if (this.question != null && !TextUtils.isEmpty(this.question.getId()) && !this.eventSent) {
            reportReadFaqEvent();
        }
    }

    public void onDestroyView() {
        SnackbarUtil.hideSnackbar(getView());
        this.questionFooter = null;
        this.webView.setWebViewClient(null);
        this.webView = null;
        this.noButton = null;
        this.yesButton = null;
        this.contactUsButton = null;
        super.onDestroyView();
    }

    /* access modifiers changed from: package-private */
    public void setQuestion(Faq faq) {
        this.question = faq;
        if (this.webView != null) {
            this.webView.loadDataWithBaseURL(null, getStyledBody(faq), "text/html", "utf-8", null);
        }
    }

    private String getStyledBody(Faq faq) {
        StringBuilder sb;
        String fontPath = FontApplier.getFontPath();
        String str = "";
        String str2 = "";
        if (!TextUtils.isEmpty(fontPath)) {
            str = "@font-face {    font-family: custom;    src: url('" + ("file:///android_asset/" + fontPath) + "');}";
            str2 = "font-family: custom, sans-serif;";
        }
        String str3 = faq.body;
        String str4 = faq.title;
        if (faq.is_rtl.booleanValue()) {
            sb = new StringBuilder("<html dir=\"rtl\">");
        } else {
            sb = new StringBuilder("<html>");
        }
        sb.append("<head>");
        sb.append("    <style type='text/css'>");
        sb.append(str);
        sb.append("        img,");
        sb.append("        object,");
        sb.append("        embed {");
        sb.append("            max-width: 100%;");
        sb.append("        }");
        sb.append("        a,");
        sb.append("        a:visited,");
        sb.append("        a:active,");
        sb.append("        a:hover {");
        sb.append("            color: ");
        sb.append(this.textColorLink);
        sb.append(";");
        sb.append("        }");
        sb.append("        body {");
        sb.append("            background-color: transparent;");
        sb.append("            margin: 0;");
        sb.append("            padding: ");
        sb.append(16 + "px " + 16 + "px " + 96 + "px " + 16 + "px;");
        sb.append("            font-size: ");
        sb.append("16px");
        sb.append(";");
        sb.append(str2);
        sb.append("            line-height: ");
        sb.append("1.5");
        sb.append(";");
        sb.append("            white-space: normal;");
        sb.append("            word-wrap: break-word;");
        sb.append("            color: ");
        sb.append(this.textColor);
        sb.append(";");
        sb.append("        }");
        sb.append("        .title {");
        sb.append("            display: block;");
        sb.append("            margin: 0;");
        sb.append("            padding: 0 0 ");
        sb.append(16);
        sb.append(" 0;");
        sb.append("            font-size: ");
        sb.append("24px");
        sb.append(";");
        sb.append(str2);
        sb.append("            line-height: ");
        sb.append("32px");
        sb.append(";");
        sb.append("        }");
        sb.append("        h1, h2, h3 { ");
        sb.append("            line-height: 1.4; ");
        sb.append("        }");
        sb.append("    </style>");
        sb.append("    <script language='javascript'>");
        sb.append("     window.onload = function () {");
        sb.append("        var w = window,");
        sb.append("            d = document,");
        sb.append("            e = d.documentElement,");
        sb.append("            g = d.getElementsByTagName('body')[0],");
        sb.append("            sWidth = Math.min (w.innerWidth || Infinity, e.clientWidth || Infinity, g.clientWidth || Infinity),");
        sb.append("            sHeight = Math.min (w.innerHeight || Infinity, e.clientHeight || Infinity, g.clientHeight || Infinity);");
        sb.append("        var frame, fw, fh;");
        sb.append("        var iframes = document.getElementsByTagName('iframe');");
        sb.append("        var padding = ");
        sb.append(32);
        sb.append(";");
        sb.append("        for (var i=0; i < iframes.length; i++) {");
        sb.append("            frame = iframes[i];");
        sb.append("            fw = frame.offsetWidth;");
        sb.append("            fh = frame.offsetHeight;");
        sb.append("            if (fw >= fh && fw > (sWidth - padding)) {");
        sb.append("                frame.style.width = sWidth - padding;");
        sb.append("                frame.style.height = ((sWidth - padding) * fh/fw).toString();");
        sb.append("            }");
        sb.append("        }");
        sb.append("        document.addEventListener('click', function (event) {");
        sb.append("            if (event.target instanceof HTMLImageElement) {");
        sb.append("                event.preventDefault();");
        sb.append("                event.stopPropagation();");
        sb.append("            }");
        sb.append("        }, false);");
        sb.append("    };");
        sb.append("    </script>");
        sb.append("</head>");
        sb.append("<body>");
        sb.append("    <strong class='title'> ");
        sb.append(str4);
        sb.append(" </strong> ");
        sb.append(str3);
        sb.append("</body>");
        sb.append("</html>");
        return sb.toString();
    }

    private void markQuestion(boolean z) {
        if (this.question != null) {
            String id = this.question.getId();
            this.data.markFaqInDB(id, z);
            HelpshiftContext.getCoreApi().getFaqDM().markHelpful(id, z);
        }
    }

    public void onClick(View view) {
        SupportFragment supportFragment;
        if (view.getId() == R.id.helpful_button) {
            markQuestion(true);
            setIsHelpful(1);
            if (this.singleQuestionMode == 2 && (supportFragment = FragmentUtil.getSupportFragment(this)) != null) {
                supportFragment.getSupportController().actionDone();
            }
        } else if (view.getId() == R.id.unhelpful_button) {
            markQuestion(false);
            setIsHelpful(-1);
        } else if (view.getId() == R.id.contact_us_button && this.supportController != null) {
            if (this.singleQuestionMode == 1) {
                FaqFragmentListener faqFlowListener = getFaqFlowListener();
                if (faqFlowListener != null) {
                    faqFlowListener.onContactUsClicked(null);
                    return;
                }
                return;
            }
            SupportFragment supportFragment2 = FragmentUtil.getSupportFragment(this);
            if (supportFragment2 != null) {
                supportFragment2.getSupportController().sendAnyway();
            }
        }
    }

    public FaqFragmentListener getFaqFlowListener() {
        FaqFlowViewParent faqFlowViewParent = (FaqFlowViewParent) getParentFragment();
        if (faqFlowViewParent != null) {
            return faqFlowViewParent.getFaqFlowListener();
        }
        return null;
    }

    private void setIsHelpful(int i) {
        if (i != 0) {
            this.isHelpful = i;
        }
        updateFooter();
    }

    public void onPageStarted() {
        showProgress(true);
        this.webView.setBackgroundColor(0);
    }

    public void onPageFinished() {
        if (isVisible()) {
            showProgress(false);
            setIsHelpful(this.question.is_helpful);
            if (this.isHighlighted) {
                this.isHighlighted = false;
            } else {
                highlightAndReloadQuestion();
            }
            this.webView.setBackgroundColor(0);
        }
    }

    private void highlightAndReloadQuestion() {
        this.isHighlighted = true;
        final ArrayList<String> stringArrayList = getArguments().getStringArrayList("searchTerms");
        HelpshiftContext.getCoreApi().getDomain().runParallel(new F() {
            public void f() {
                Faq unused = SingleQuestionFragment.this.highlightedQuestion = com.helpshift.support.util.Styles.getQuestionWithHighlightedSearchTerms(SingleQuestionFragment.this.getContext(), SingleQuestionFragment.this.question, stringArrayList);
                HelpshiftContext.getCoreApi().getDomain().runOnUI(new F() {
                    public void f() {
                        if (SingleQuestionFragment.this.highlightedQuestion != null) {
                            SingleQuestionFragment.this.setQuestion(SingleQuestionFragment.this.highlightedQuestion);
                        }
                    }
                });
            }
        });
    }

    private void showProgress(boolean z) {
        if (this.progressBar == null) {
            return;
        }
        if (z) {
            this.progressBar.setVisibility(0);
        } else {
            this.progressBar.setVisibility(8);
        }
    }

    private void updateFooter() {
        if (this.singleQuestionMode == 3) {
            hideQuestionFooter();
            return;
        }
        switch (this.isHelpful) {
            case -1:
                showUnhelpfulFooter();
                return;
            case 0:
                showQuestionFooter();
                return;
            case 1:
                showHelpfulFooter();
                return;
            default:
                return;
        }
    }

    private void hideQuestionFooter() {
        this.questionFooter.setVisibility(8);
    }

    private void showQuestionFooter() {
        this.questionFooter.setVisibility(0);
        this.questionFooterMessage.setText(getResources().getString(R.string.hs__mark_yes_no_question));
        this.contactUsButton.setVisibility(8);
        this.yesButton.setVisibility(0);
        this.noButton.setVisibility(0);
    }

    private void showHelpfulFooter() {
        this.questionFooter.setVisibility(0);
        this.questionFooterMessage.setText(getResources().getString(R.string.hs__question_helpful_message));
        this.questionFooterMessage.setGravity(17);
        this.contactUsButton.setVisibility(8);
        this.yesButton.setVisibility(8);
        this.noButton.setVisibility(8);
    }

    private void showUnhelpfulFooter() {
        this.questionFooter.setVisibility(0);
        this.questionFooterMessage.setText(getResources().getString(R.string.hs__question_unhelpful_message));
        showQuestionFooterContactUs();
        this.yesButton.setVisibility(8);
        this.noButton.setVisibility(8);
    }

    private void showQuestionFooterContactUs() {
        if (ContactUsFilter.showContactUs(ContactUsFilter.LOCATION.QUESTION_FOOTER)) {
            this.contactUsButton.setVisibility(0);
        } else {
            this.contactUsButton.setVisibility(8);
        }
    }

    public String getQuestionId() {
        if (this.question != null) {
            return this.question.getId();
        }
        return "";
    }

    public String getQuestionPublishId() {
        return this.questionPublishId;
    }

    /* access modifiers changed from: package-private */
    public void reportReadFaqEvent() {
        HashMap hashMap = new HashMap();
        hashMap.put("id", this.question.getId());
        hashMap.put("nt", Boolean.valueOf(HelpshiftConnectionUtil.isOnline(getContext())));
        HelpshiftContext.getCoreApi().getAnalyticsEventDM().pushEvent(AnalyticsEventType.READ_FAQ, hashMap);
        if (this.questionReadListener != null) {
            this.questionReadListener.onQuestionRead(this.question.getId());
        }
        this.eventSent = true;
    }

    private static class Success extends Handler {
        private WeakReference<SingleQuestionFragment> singleQuestionFragmentWeakReference;

        public Success(SingleQuestionFragment singleQuestionFragment) {
            this.singleQuestionFragmentWeakReference = new WeakReference<>(singleQuestionFragment);
        }

        public void handleMessage(Message message) {
            Faq faq;
            super.handleMessage(message);
            SingleQuestionFragment singleQuestionFragment = this.singleQuestionFragmentWeakReference.get();
            if (singleQuestionFragment != null && (faq = (Faq) message.obj) != null) {
                singleQuestionFragment.setQuestion(faq);
                String id = faq.getId();
                HSLogger.d(SingleQuestionFragment.TAG, "FAQ question loaded : " + faq.title);
                if (!singleQuestionFragment.eventSent && !TextUtils.isEmpty(id)) {
                    singleQuestionFragment.reportReadFaqEvent();
                }
            }
        }
    }

    private static class Failure extends Handler {
        private WeakReference<SingleQuestionFragment> singleQuestionFragmentWeakReference;

        public Failure(SingleQuestionFragment singleQuestionFragment) {
            this.singleQuestionFragmentWeakReference = new WeakReference<>(singleQuestionFragment);
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
            SingleQuestionFragment singleQuestionFragment = this.singleQuestionFragmentWeakReference.get();
            if (singleQuestionFragment != null && !singleQuestionFragment.isDetached() && singleQuestionFragment.question == null) {
                SnackbarUtil.showErrorSnackbar(102, singleQuestionFragment.getView());
            }
        }
    }
}
