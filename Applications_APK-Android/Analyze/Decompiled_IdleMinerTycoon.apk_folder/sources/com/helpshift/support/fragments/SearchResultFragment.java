package com.helpshift.support.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.helpshift.R;
import com.helpshift.support.Faq;
import com.helpshift.support.adapters.SearchResultAdapter;
import com.helpshift.support.contracts.SearchResultListener;
import java.util.List;

public class SearchResultFragment extends MainFragment {
    public static final String BUNDLE_ARG_SEARCH_RESULTS = "search_fragment_results";
    public static final String FRAGMENT_TAG = "HSSearchResultFragment";
    private View.OnClickListener onQuestionClickedListener;
    RecyclerView searchResultList;
    SearchResultListener searchResultListener;
    private View.OnClickListener sendAnywayClickedListener;

    public boolean shouldRefreshMenu() {
        return true;
    }

    public static SearchResultFragment newInstance(Bundle bundle, SearchResultListener searchResultListener2) {
        SearchResultFragment searchResultFragment = new SearchResultFragment();
        searchResultFragment.setArguments(bundle);
        searchResultFragment.searchResultListener = searchResultListener2;
        return searchResultFragment;
    }

    public void setSearchResultListener(SearchResultListener searchResultListener2) {
        this.searchResultListener = searchResultListener2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__search_result_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.searchResultList = (RecyclerView) view.findViewById(R.id.search_result);
        this.searchResultList.setLayoutManager(new LinearLayoutManager(view.getContext()));
        this.onQuestionClickedListener = new View.OnClickListener() {
            public void onClick(View view) {
                String str = (String) view.getTag();
                Faq faq = ((SearchResultAdapter) SearchResultFragment.this.searchResultList.getAdapter()).getFaq(str);
                SearchResultFragment.this.searchResultListener.onQuestionSelected(str, faq != null ? faq.searchTerms : null);
            }
        };
        this.sendAnywayClickedListener = new View.OnClickListener() {
            public void onClick(View view) {
                SearchResultFragment.this.searchResultListener.sendAnyway();
            }
        };
    }

    public void onResume() {
        super.onResume();
        setToolbarTitle(getString(R.string.hs__search_result_title));
        showResults();
    }

    private void showResults() {
        List parcelableArrayList = getArguments().getParcelableArrayList(BUNDLE_ARG_SEARCH_RESULTS);
        if (parcelableArrayList != null && parcelableArrayList.size() > 3) {
            parcelableArrayList = parcelableArrayList.subList(0, 3);
        }
        this.searchResultList.setAdapter(new SearchResultAdapter(parcelableArrayList, this.onQuestionClickedListener, this.sendAnywayClickedListener));
    }
}
