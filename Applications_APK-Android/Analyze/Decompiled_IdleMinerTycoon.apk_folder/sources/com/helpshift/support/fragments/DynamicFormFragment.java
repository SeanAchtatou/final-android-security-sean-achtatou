package com.helpshift.support.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.helpshift.R;
import com.helpshift.analytics.AnalyticsEventType;
import com.helpshift.support.adapters.FlowListAdapter;
import com.helpshift.support.controllers.SupportController;
import com.helpshift.support.flows.ConversationFlow;
import com.helpshift.support.flows.DynamicFormFlow;
import com.helpshift.support.flows.FAQSectionFlow;
import com.helpshift.support.flows.FAQsFlow;
import com.helpshift.support.flows.Flow;
import com.helpshift.support.flows.SingleFAQFlow;
import com.helpshift.util.HelpshiftContext;
import java.util.List;

public class DynamicFormFragment extends MainFragment implements View.OnClickListener {
    public static final String FRAGMENT_TAG = "HSDynamicFormFragment";
    private List<Flow> flowList;
    private RecyclerView flowListView;
    private boolean sendAnalyticsEvent = true;
    private SupportController supportController;
    private String title;

    public boolean shouldRefreshMenu() {
        return true;
    }

    public static DynamicFormFragment newInstance(Bundle bundle, List<Flow> list, SupportController supportController2) {
        DynamicFormFragment dynamicFormFragment = new DynamicFormFragment();
        dynamicFormFragment.setArguments(bundle);
        dynamicFormFragment.flowList = list;
        dynamicFormFragment.supportController = supportController2;
        return dynamicFormFragment;
    }

    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.title = arguments.getString(SupportFragmentConstants.FLOW_TITLE);
            if (TextUtils.isEmpty(this.title)) {
                this.title = getString(R.string.hs__help_header);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__dynamic_form_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.flowListView = (RecyclerView) view.findViewById(R.id.flow_list);
        this.flowListView.setLayoutManager(new LinearLayoutManager(view.getContext()));
    }

    public void onResume() {
        super.onResume();
        setToolbarTitle(this.title);
        showFlows();
    }

    public void setSupportController(SupportController supportController2) {
        this.supportController = supportController2;
    }

    private void showFlows() {
        if (this.flowList != null) {
            this.flowListView.setAdapter(new FlowListAdapter(this.flowList, this));
        }
    }

    public void onClick(View view) {
        int intValue = ((Integer) view.getTag()).intValue();
        this.sendAnalyticsEvent = false;
        performAction(this.flowList.get(intValue));
    }

    private void performAction(Flow flow) {
        if (flow instanceof ConversationFlow) {
            ((ConversationFlow) flow).setSupportController(this.supportController);
        } else if (flow instanceof FAQSectionFlow) {
            ((FAQSectionFlow) flow).setSupportController(this.supportController);
        } else if (flow instanceof SingleFAQFlow) {
            ((SingleFAQFlow) flow).setSupportController(this.supportController);
        } else if (flow instanceof DynamicFormFlow) {
            ((DynamicFormFlow) flow).setSupportController(this.supportController);
        } else if (flow instanceof FAQsFlow) {
            ((FAQsFlow) flow).setSupportController(this.supportController);
        }
        flow.performAction();
    }

    public void onStart() {
        super.onStart();
        if (!isChangingConfigurations() && this.sendAnalyticsEvent) {
            HelpshiftContext.getCoreApi().getAnalyticsEventDM().pushEvent(AnalyticsEventType.DYNAMIC_FORM_OPEN);
        }
        this.sendAnalyticsEvent = true;
    }

    public void onStop() {
        super.onStop();
        if (!isChangingConfigurations() && this.sendAnalyticsEvent) {
            HelpshiftContext.getCoreApi().getAnalyticsEventDM().pushEvent(AnalyticsEventType.DYNAMIC_FORM_CLOSE);
        }
    }

    public void onDestroyView() {
        this.flowListView = null;
        super.onDestroyView();
    }
}
